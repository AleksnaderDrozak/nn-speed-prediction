export const train2 = 
[
	{
		id: 1,
		time: 0,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 32.0103880577831,
		road: 0.101944444444444,
		acceleration: 0.203888888888889
	},
	{
		id: 3,
		time: 2,
		velocity: 0.611666666666667,
		power: 24.6380275775544,
		road: 0.305833333333333,
		acceleration: 0
	},
	{
		id: 4,
		time: 3,
		velocity: 0,
		power: 24.6380275775544,
		road: 0.509722222222222,
		acceleration: 0
	},
	{
		id: 5,
		time: 4,
		velocity: 0,
		power: -7.37466111111111,
		road: 0.611666666666667,
		acceleration: -0.203888888888889
	},
	{
		id: 6,
		time: 5,
		velocity: 0,
		power: 0,
		road: 0.611666666666667,
		acceleration: 0
	},
	{
		id: 7,
		time: 6,
		velocity: 0,
		power: 0,
		road: 0.611666666666667,
		acceleration: 0
	},
	{
		id: 8,
		time: 7,
		velocity: 0,
		power: 0,
		road: 0.611666666666667,
		acceleration: 0
	},
	{
		id: 9,
		time: 8,
		velocity: 0,
		power: 0,
		road: 0.611666666666667,
		acceleration: 0
	},
	{
		id: 10,
		time: 9,
		velocity: 0,
		power: 0,
		road: 0.611666666666667,
		acceleration: 0
	},
	{
		id: 11,
		time: 10,
		velocity: 0,
		power: 0,
		road: 0.611666666666667,
		acceleration: 0
	},
	{
		id: 12,
		time: 11,
		velocity: 0,
		power: 0,
		road: 0.611666666666667,
		acceleration: 0
	},
	{
		id: 13,
		time: 12,
		velocity: 0,
		power: 0,
		road: 0.611666666666667,
		acceleration: 0
	},
	{
		id: 14,
		time: 13,
		velocity: 0,
		power: 0,
		road: 0.611666666666667,
		acceleration: 0
	},
	{
		id: 15,
		time: 14,
		velocity: 0,
		power: 0,
		road: 0.611666666666667,
		acceleration: 0
	},
	{
		id: 16,
		time: 15,
		velocity: 0,
		power: 63.9938234965673,
		road: 0.766296296296296,
		acceleration: 0.309259259259259
	},
	{
		id: 17,
		time: 16,
		velocity: 0.927777777777778,
		power: 376.469434770889,
		road: 1.35125,
		acceleration: 0.551388888888889
	},
	{
		id: 18,
		time: 17,
		velocity: 1.65416666666667,
		power: 943.165626461023,
		road: 2.55944444444444,
		acceleration: 0.695092592592593
	},
	{
		id: 19,
		time: 18,
		velocity: 2.08527777777778,
		power: 742.496618044005,
		road: 4.27837962962963,
		acceleration: 0.326388888888889
	},
	{
		id: 20,
		time: 19,
		velocity: 1.90694444444444,
		power: 142.458125326373,
		road: 6.13625,
		acceleration: -0.0485185185185184
	},
	{
		id: 21,
		time: 20,
		velocity: 1.50861111111111,
		power: -305.302549273597,
		road: 7.80912037037037,
		acceleration: -0.321481481481482
	},
	{
		id: 22,
		time: 21,
		velocity: 1.12083333333333,
		power: -292.84859058421,
		road: 9.14106481481481,
		acceleration: -0.36037037037037
	},
	{
		id: 23,
		time: 22,
		velocity: 0.825833333333333,
		power: -319.936493244433,
		road: 10.0413888888889,
		acceleration: -0.50287037037037
	},
	{
		id: 24,
		time: 23,
		velocity: 0,
		power: -107.70623531555,
		road: 10.5034722222222,
		acceleration: -0.373611111111111
	},
	{
		id: 25,
		time: 24,
		velocity: 0,
		power: -19.2655326023392,
		road: 10.6411111111111,
		acceleration: -0.275277777777778
	},
	{
		id: 26,
		time: 25,
		velocity: 0,
		power: 0,
		road: 10.6411111111111,
		acceleration: 0
	},
	{
		id: 27,
		time: 26,
		velocity: 0,
		power: 0,
		road: 10.6411111111111,
		acceleration: 0
	},
	{
		id: 28,
		time: 27,
		velocity: 0,
		power: 75.2721826985964,
		road: 10.8110648148148,
		acceleration: 0.339907407407407
	},
	{
		id: 29,
		time: 28,
		velocity: 1.01972222222222,
		power: 164.477630605337,
		road: 11.2744444444444,
		acceleration: 0.246944444444444
	},
	{
		id: 30,
		time: 29,
		velocity: 0.740833333333333,
		power: 408.149113147802,
		road: 12.0685185185185,
		acceleration: 0.414444444444444
	},
	{
		id: 31,
		time: 30,
		velocity: 1.24333333333333,
		power: -167.068418413845,
		road: 12.8998611111111,
		acceleration: -0.339907407407407
	},
	{
		id: 32,
		time: 31,
		velocity: 0,
		power: 31.1644542525266,
		road: 13.52375,
		acceleration: -0.075
	},
	{
		id: 33,
		time: 32,
		velocity: 0.515833333333333,
		power: 247.233590130931,
		road: 14.2307407407407,
		acceleration: 0.241203703703704
	},
	{
		id: 34,
		time: 33,
		velocity: 1.96694444444444,
		power: 1397.82558996948,
		road: 15.5519907407407,
		acceleration: 0.987314814814815
	},
	{
		id: 35,
		time: 34,
		velocity: 2.96194444444444,
		power: 2622.78326542658,
		road: 17.8922685185185,
		acceleration: 1.05074074074074
	},
	{
		id: 36,
		time: 35,
		velocity: 3.66805555555556,
		power: 3087.73487031794,
		road: 21.1850925925926,
		acceleration: 0.854351851851852
	},
	{
		id: 37,
		time: 36,
		velocity: 4.53,
		power: 2607.63936817471,
		road: 25.1805555555556,
		acceleration: 0.550925925925926
	},
	{
		id: 38,
		time: 37,
		velocity: 4.61472222222222,
		power: 1498.78170036723,
		road: 29.5624537037037,
		acceleration: 0.221944444444444
	},
	{
		id: 39,
		time: 38,
		velocity: 4.33388888888889,
		power: 202.979643275421,
		road: 34.0100925925926,
		acceleration: -0.0904629629629632
	},
	{
		id: 40,
		time: 39,
		velocity: 4.25861111111111,
		power: -449.546026180033,
		road: 38.2883333333333,
		acceleration: -0.248333333333333
	},
	{
		id: 41,
		time: 40,
		velocity: 3.86972222222222,
		power: -395.931366606069,
		road: 42.3224537037037,
		acceleration: -0.239907407407407
	},
	{
		id: 42,
		time: 41,
		velocity: 3.61416666666667,
		power: -992.686574406868,
		road: 46.027962962963,
		acceleration: -0.417314814814815
	},
	{
		id: 43,
		time: 42,
		velocity: 3.00666666666667,
		power: 54.5910154369395,
		road: 49.4661574074074,
		acceleration: -0.117314814814814
	},
	{
		id: 44,
		time: 43,
		velocity: 3.51777777777778,
		power: 1385.7274447152,
		road: 52.9858796296296,
		acceleration: 0.28037037037037
	},
	{
		id: 45,
		time: 44,
		velocity: 4.45527777777778,
		power: 4357.96501102527,
		road: 57.130787037037,
		acceleration: 0.97
	},
	{
		id: 46,
		time: 45,
		velocity: 5.91666666666667,
		power: 4488.70461679237,
		road: 62.1595833333333,
		acceleration: 0.797777777777778
	},
	{
		id: 47,
		time: 46,
		velocity: 5.91111111111111,
		power: 3624.92471180437,
		road: 67.8495833333333,
		acceleration: 0.524629629629629
	},
	{
		id: 48,
		time: 47,
		velocity: 6.02916666666667,
		power: 1690.20428071956,
		road: 73.8755092592593,
		acceleration: 0.147222222222222
	},
	{
		id: 49,
		time: 48,
		velocity: 6.35833333333333,
		power: 2959.52230464218,
		road: 80.1483796296296,
		acceleration: 0.346666666666666
	},
	{
		id: 50,
		time: 49,
		velocity: 6.95111111111111,
		power: 3645.10998637293,
		road: 86.80625,
		acceleration: 0.423333333333335
	},
	{
		id: 51,
		time: 50,
		velocity: 7.29916666666667,
		power: 4457.6393937502,
		road: 93.9268518518519,
		acceleration: 0.502129629629628
	},
	{
		id: 52,
		time: 51,
		velocity: 7.86472222222222,
		power: 4952.36145787948,
		road: 101.559351851852,
		acceleration: 0.521666666666667
	},
	{
		id: 53,
		time: 52,
		velocity: 8.51611111111111,
		power: 5490.46924033944,
		road: 109.723472222222,
		acceleration: 0.541574074074073
	},
	{
		id: 54,
		time: 53,
		velocity: 8.92388888888889,
		power: 4749.31227238817,
		road: 118.362361111111,
		acceleration: 0.407962962962964
	},
	{
		id: 55,
		time: 54,
		velocity: 9.08861111111111,
		power: 3140.34787955859,
		road: 127.303425925926,
		acceleration: 0.19638888888889
	},
	{
		id: 56,
		time: 55,
		velocity: 9.10527777777778,
		power: 1660.65331176316,
		road: 136.352268518519,
		acceleration: 0.0191666666666652
	},
	{
		id: 57,
		time: 56,
		velocity: 8.98138888888889,
		power: 807.17638144237,
		road: 145.371064814815,
		acceleration: -0.0792592592592598
	},
	{
		id: 58,
		time: 57,
		velocity: 8.85083333333333,
		power: -2536.67745592554,
		road: 154.112592592593,
		acceleration: -0.475277777777778
	},
	{
		id: 59,
		time: 58,
		velocity: 7.67944444444444,
		power: -5377.97081392571,
		road: 162.184351851852,
		acceleration: -0.864259259259259
	},
	{
		id: 60,
		time: 59,
		velocity: 6.38861111111111,
		power: -8391.14493194111,
		road: 169.109675925926,
		acceleration: -1.42861111111111
	},
	{
		id: 61,
		time: 60,
		velocity: 4.565,
		power: -7984.85336533434,
		road: 174.464074074074,
		acceleration: -1.71324074074074
	},
	{
		id: 62,
		time: 61,
		velocity: 2.53972222222222,
		power: -6304.99295354044,
		road: 177.938657407407,
		acceleration: -2.04638888888889
	},
	{
		id: 63,
		time: 62,
		velocity: 0.249444444444444,
		power: -2232.02664480611,
		road: 179.629212962963,
		acceleration: -1.52166666666667
	},
	{
		id: 64,
		time: 63,
		velocity: 0,
		power: -157.740087424463,
		road: 180.384907407407,
		acceleration: -0.348055555555556
	},
	{
		id: 65,
		time: 64,
		velocity: 1.49555555555556,
		power: 1130.57045049055,
		road: 181.458101851852,
		acceleration: 0.983055555555556
	},
	{
		id: 66,
		time: 65,
		velocity: 3.19861111111111,
		power: 3188.77941427946,
		road: 183.705462962963,
		acceleration: 1.36527777777778
	},
	{
		id: 67,
		time: 66,
		velocity: 4.09583333333333,
		power: 4603.63792699556,
		road: 187.251851851852,
		acceleration: 1.23277777777778
	},
	{
		id: 68,
		time: 67,
		velocity: 5.19388888888889,
		power: 4913.76000286602,
		road: 191.901111111111,
		acceleration: 0.972962962962963
	},
	{
		id: 69,
		time: 68,
		velocity: 6.1175,
		power: 6772.23586671847,
		road: 197.590185185185,
		acceleration: 1.10666666666667
	},
	{
		id: 70,
		time: 69,
		velocity: 7.41583333333333,
		power: 9132.63218601914,
		road: 204.454907407407,
		acceleration: 1.24462962962963
	},
	{
		id: 71,
		time: 70,
		velocity: 8.92777777777778,
		power: 12275.0673094304,
		road: 212.646388888889,
		acceleration: 1.40888888888889
	},
	{
		id: 72,
		time: 71,
		velocity: 10.3441666666667,
		power: 11395.3483837669,
		road: 222.087037037037,
		acceleration: 1.08944444444445
	},
	{
		id: 73,
		time: 72,
		velocity: 10.6841666666667,
		power: 8262.09114914297,
		road: 232.399074074074,
		acceleration: 0.653333333333332
	},
	{
		id: 74,
		time: 73,
		velocity: 10.8877777777778,
		power: 5498.60807949977,
		road: 243.207962962963,
		acceleration: 0.340370370370369
	},
	{
		id: 75,
		time: 74,
		velocity: 11.3652777777778,
		power: 7381.26569282631,
		road: 254.432638888889,
		acceleration: 0.491203703703704
	},
	{
		id: 76,
		time: 75,
		velocity: 12.1577777777778,
		power: 9440.15799645769,
		road: 266.219861111111,
		acceleration: 0.63388888888889
	},
	{
		id: 77,
		time: 76,
		velocity: 12.7894444444444,
		power: 10448.8270097486,
		road: 278.656944444444,
		acceleration: 0.665833333333335
	},
	{
		id: 78,
		time: 77,
		velocity: 13.3627777777778,
		power: 9311.83686366403,
		road: 291.68962962963,
		acceleration: 0.525370370370368
	},
	{
		id: 79,
		time: 78,
		velocity: 13.7338888888889,
		power: 6994.04431993146,
		road: 305.142546296296,
		acceleration: 0.315092592592594
	},
	{
		id: 80,
		time: 79,
		velocity: 13.7347222222222,
		power: 5973.12965164695,
		road: 318.864166666667,
		acceleration: 0.222314814814816
	},
	{
		id: 81,
		time: 80,
		velocity: 14.0297222222222,
		power: 3919.13518522122,
		road: 332.727083333333,
		acceleration: 0.0602777777777774
	},
	{
		id: 82,
		time: 81,
		velocity: 13.9147222222222,
		power: 2994.5053693868,
		road: 346.614953703704,
		acceleration: -0.0103703703703726
	},
	{
		id: 83,
		time: 82,
		velocity: 13.7036111111111,
		power: -1623.50502719073,
		road: 360.318935185185,
		acceleration: -0.357407407407404
	},
	{
		id: 84,
		time: 83,
		velocity: 12.9575,
		power: -3871.8029260021,
		road: 373.577962962963,
		acceleration: -0.532500000000002
	},
	{
		id: 85,
		time: 84,
		velocity: 12.3172222222222,
		power: -5671.6195238169,
		road: 386.226944444444,
		acceleration: -0.687592592592591
	},
	{
		id: 86,
		time: 85,
		velocity: 11.6408333333333,
		power: -5431.56385604247,
		road: 398.190046296296,
		acceleration: -0.684166666666668
	},
	{
		id: 87,
		time: 86,
		velocity: 10.905,
		power: -4050.81200557753,
		road: 409.523703703704,
		acceleration: -0.574722222222222
	},
	{
		id: 88,
		time: 87,
		velocity: 10.5930555555556,
		power: -4536.91725163918,
		road: 420.251990740741,
		acceleration: -0.636018518518519
	},
	{
		id: 89,
		time: 88,
		velocity: 9.73277777777778,
		power: 707.531037972621,
		road: 430.604212962963,
		acceleration: -0.116111111111111
	},
	{
		id: 90,
		time: 89,
		velocity: 10.5566666666667,
		power: 5262.93942734893,
		road: 441.067685185185,
		acceleration: 0.338611111111112
	},
	{
		id: 91,
		time: 90,
		velocity: 11.6088888888889,
		power: 8368.72757611795,
		road: 452.004351851852,
		acceleration: 0.607777777777777
	},
	{
		id: 92,
		time: 91,
		velocity: 11.5561111111111,
		power: 4479.61741098169,
		road: 463.351898148148,
		acceleration: 0.213981481481483
	},
	{
		id: 93,
		time: 92,
		velocity: 11.1986111111111,
		power: -2402.00004615111,
		road: 474.595046296296,
		acceleration: -0.422777777777778
	},
	{
		id: 94,
		time: 93,
		velocity: 10.3405555555556,
		power: -5786.03796971236,
		road: 485.246111111111,
		acceleration: -0.761388888888888
	},
	{
		id: 95,
		time: 94,
		velocity: 9.27194444444444,
		power: -7018.41739414086,
		road: 495.049907407407,
		acceleration: -0.933148148148149
	},
	{
		id: 96,
		time: 95,
		velocity: 8.39916666666667,
		power: -1805.46977272384,
		road: 504.196203703704,
		acceleration: -0.381851851851852
	},
	{
		id: 97,
		time: 96,
		velocity: 9.195,
		power: 7665.10433981025,
		road: 513.496157407407,
		acceleration: 0.689166666666667
	},
	{
		id: 98,
		time: 97,
		velocity: 11.3394444444444,
		power: 20687.3209607744,
		road: 524.071435185185,
		acceleration: 1.86148148148148
	},
	{
		id: 99,
		time: 98,
		velocity: 13.9836111111111,
		power: 22606.1834316727,
		road: 536.429166666666,
		acceleration: 1.70342592592593
	},
	{
		id: 100,
		time: 99,
		velocity: 14.3052777777778,
		power: 21083.7669546568,
		road: 550.315416666666,
		acceleration: 1.35361111111111
	},
	{
		id: 101,
		time: 100,
		velocity: 15.4002777777778,
		power: 8440.12686605216,
		road: 565.053240740741,
		acceleration: 0.349537037037038
	},
	{
		id: 102,
		time: 101,
		velocity: 15.0322222222222,
		power: 3414.30268889398,
		road: 579.959351851852,
		acceleration: -0.0129629629629644
	},
	{
		id: 103,
		time: 102,
		velocity: 14.2663888888889,
		power: -3892.99369877251,
		road: 594.595648148148,
		acceleration: -0.526666666666666
	},
	{
		id: 104,
		time: 103,
		velocity: 13.8202777777778,
		power: -4314.276493982,
		road: 608.688611111111,
		acceleration: -0.560000000000001
	},
	{
		id: 105,
		time: 104,
		velocity: 13.3522222222222,
		power: -260.202870243761,
		road: 622.375092592592,
		acceleration: -0.252962962962963
	},
	{
		id: 106,
		time: 105,
		velocity: 13.5075,
		power: 680.406697539505,
		road: 635.846666666666,
		acceleration: -0.176851851851854
	},
	{
		id: 107,
		time: 106,
		velocity: 13.2897222222222,
		power: -923.142156314154,
		road: 649.080185185185,
		acceleration: -0.299259259259259
	},
	{
		id: 108,
		time: 107,
		velocity: 12.4544444444444,
		power: -4393.78668340245,
		road: 661.874259259259,
		acceleration: -0.579629629629629
	},
	{
		id: 109,
		time: 108,
		velocity: 11.7686111111111,
		power: -6769.17239597545,
		road: 673.98037037037,
		acceleration: -0.796296296296298
	},
	{
		id: 110,
		time: 109,
		velocity: 10.9008333333333,
		power: -13430.2379296407,
		road: 684.948333333333,
		acceleration: -1.48
	},
	{
		id: 111,
		time: 110,
		velocity: 8.01444444444444,
		power: -15732.2178183315,
		road: 694.195,
		acceleration: -1.96259259259259
	},
	{
		id: 112,
		time: 111,
		velocity: 5.88083333333333,
		power: -17695.7017533838,
		road: 701.019861111111,
		acceleration: -2.88101851851852
	},
	{
		id: 113,
		time: 112,
		velocity: 2.25777777777778,
		power: -9741.2172528748,
		road: 705.068472222222,
		acceleration: -2.67148148148148
	},
	{
		id: 114,
		time: 113,
		velocity: 0,
		power: -2623.90376444095,
		road: 707.00037037037,
		acceleration: -1.56194444444444
	},
	{
		id: 115,
		time: 114,
		velocity: 1.195,
		power: 490.793191408935,
		road: 708.288101851851,
		acceleration: 0.273611111111111
	},
	{
		id: 116,
		time: 115,
		velocity: 3.07861111111111,
		power: 3423.12438288742,
		road: 710.473194444444,
		acceleration: 1.52111111111111
	},
	{
		id: 117,
		time: 116,
		velocity: 4.56333333333333,
		power: 4868.69231562028,
		road: 714.065277777777,
		acceleration: 1.29287037037037
	},
	{
		id: 118,
		time: 117,
		velocity: 5.07361111111111,
		power: 4385.71416642755,
		road: 718.728935185185,
		acceleration: 0.850277777777777
	},
	{
		id: 119,
		time: 118,
		velocity: 5.62944444444444,
		power: 2777.78715454236,
		road: 724.022268518518,
		acceleration: 0.409074074074075
	},
	{
		id: 120,
		time: 119,
		velocity: 5.79055555555556,
		power: 2215.89406682593,
		road: 729.65449074074,
		acceleration: 0.268703703703704
	},
	{
		id: 121,
		time: 120,
		velocity: 5.87972222222222,
		power: 1347.78305168548,
		road: 735.469768518518,
		acceleration: 0.0974074074074078
	},
	{
		id: 122,
		time: 121,
		velocity: 5.92166666666667,
		power: 538.616646759982,
		road: 741.308981481481,
		acceleration: -0.0495370370370374
	},
	{
		id: 123,
		time: 122,
		velocity: 5.64194444444444,
		power: 324.662994823117,
		road: 747.079953703703,
		acceleration: -0.0869444444444447
	},
	{
		id: 124,
		time: 123,
		velocity: 5.61888888888889,
		power: 1977.71667579891,
		road: 752.912546296296,
		acceleration: 0.210185185185185
	},
	{
		id: 125,
		time: 124,
		velocity: 6.55222222222222,
		power: 5045.39462765561,
		road: 759.197546296296,
		acceleration: 0.694629629629629
	},
	{
		id: 126,
		time: 125,
		velocity: 7.72583333333333,
		power: 9660.87111883511,
		road: 766.45125,
		acceleration: 1.24277777777778
	},
	{
		id: 127,
		time: 126,
		velocity: 9.34722222222222,
		power: 12035.4870060828,
		road: 774.982916666666,
		acceleration: 1.31314814814815
	},
	{
		id: 128,
		time: 127,
		velocity: 10.4916666666667,
		power: 14152.1330752076,
		road: 784.833935185185,
		acceleration: 1.32555555555556
	},
	{
		id: 129,
		time: 128,
		velocity: 11.7025,
		power: 9698.41113575919,
		road: 795.717962962962,
		acceleration: 0.740462962962962
	},
	{
		id: 130,
		time: 129,
		velocity: 11.5686111111111,
		power: 5242.71569029078,
		road: 807.113148148148,
		acceleration: 0.281851851851853
	},
	{
		id: 131,
		time: 130,
		velocity: 11.3372222222222,
		power: 1587.5722493683,
		road: 818.620555555555,
		acceleration: -0.0574074074074087
	},
	{
		id: 132,
		time: 131,
		velocity: 11.5302777777778,
		power: 2803.04497222163,
		road: 830.125972222222,
		acceleration: 0.0534259259259269
	},
	{
		id: 133,
		time: 132,
		velocity: 11.7288888888889,
		power: 4563.76260729029,
		road: 841.761851851851,
		acceleration: 0.207499999999998
	},
	{
		id: 134,
		time: 133,
		velocity: 11.9597222222222,
		power: 5402.92012762182,
		road: 853.636527777777,
		acceleration: 0.270092592592595
	},
	{
		id: 135,
		time: 134,
		velocity: 12.3405555555556,
		power: 4478.2451108086,
		road: 865.735277777777,
		acceleration: 0.178055555555554
	},
	{
		id: 136,
		time: 135,
		velocity: 12.2630555555556,
		power: 2372.24079185623,
		road: 877.919537037036,
		acceleration: -0.00703703703703695
	},
	{
		id: 137,
		time: 136,
		velocity: 11.9386111111111,
		power: -616.911843332987,
		road: 889.968796296296,
		acceleration: -0.262962962962963
	},
	{
		id: 138,
		time: 137,
		velocity: 11.5516666666667,
		power: -165.646120768439,
		road: 901.776203703703,
		acceleration: -0.220740740740743
	},
	{
		id: 139,
		time: 138,
		velocity: 11.6008333333333,
		power: -58.8289915258404,
		road: 913.368981481481,
		acceleration: -0.208518518518517
	},
	{
		id: 140,
		time: 139,
		velocity: 11.3130555555556,
		power: 2891.72973074604,
		road: 924.888009259259,
		acceleration: 0.0610185185185177
	},
	{
		id: 141,
		time: 140,
		velocity: 11.7347222222222,
		power: 2508.51302042842,
		road: 936.449907407407,
		acceleration: 0.0247222222222234
	},
	{
		id: 142,
		time: 141,
		velocity: 11.675,
		power: 4259.48496508989,
		road: 948.113564814814,
		acceleration: 0.178796296296296
	},
	{
		id: 143,
		time: 142,
		velocity: 11.8494444444444,
		power: 2752.11489653182,
		road: 959.886388888888,
		acceleration: 0.0395370370370376
	},
	{
		id: 144,
		time: 143,
		velocity: 11.8533333333333,
		power: 4286.76800901886,
		road: 971.764675925925,
		acceleration: 0.171388888888888
	},
	{
		id: 145,
		time: 144,
		velocity: 12.1891666666667,
		power: 5214.0699480807,
		road: 983.849907407407,
		acceleration: 0.2425
	},
	{
		id: 146,
		time: 145,
		velocity: 12.5769444444444,
		power: 4370.16466928946,
		road: 996.136527777777,
		acceleration: 0.160277777777779
	},
	{
		id: 147,
		time: 146,
		velocity: 12.3341666666667,
		power: 6789.74002369692,
		road: 1008.67888888889,
		acceleration: 0.351203703703703
	},
	{
		id: 148,
		time: 147,
		velocity: 13.2427777777778,
		power: 6026.86203597202,
		road: 1021.53222222222,
		acceleration: 0.270740740740742
	},
	{
		id: 149,
		time: 148,
		velocity: 13.3891666666667,
		power: 10259.465383788,
		road: 1034.81212962963,
		acceleration: 0.582407407407405
	},
	{
		id: 150,
		time: 149,
		velocity: 14.0813888888889,
		power: 9132.85452640025,
		road: 1048.61236111111,
		acceleration: 0.458240740740742
	},
	{
		id: 151,
		time: 150,
		velocity: 14.6175,
		power: 13157.4312734569,
		road: 1062.99837962963,
		acceleration: 0.713333333333335
	},
	{
		id: 152,
		time: 151,
		velocity: 15.5291666666667,
		power: 8998.45392220503,
		road: 1077.92986111111,
		acceleration: 0.377592592592592
	},
	{
		id: 153,
		time: 152,
		velocity: 15.2141666666667,
		power: 3082.47119103223,
		road: 1093.02898148148,
		acceleration: -0.042314814814814
	},
	{
		id: 154,
		time: 153,
		velocity: 14.4905555555556,
		power: -916.467139283134,
		road: 1107.94833333333,
		acceleration: -0.317222222222226
	},
	{
		id: 155,
		time: 154,
		velocity: 14.5775,
		power: 1349.86779430362,
		road: 1122.63268518518,
		acceleration: -0.152777777777775
	},
	{
		id: 156,
		time: 155,
		velocity: 14.7558333333333,
		power: 6653.15331826326,
		road: 1137.35240740741,
		acceleration: 0.223518518518517
	},
	{
		id: 157,
		time: 156,
		velocity: 15.1611111111111,
		power: 6037.66057349139,
		road: 1152.26925925926,
		acceleration: 0.170740740740742
	},
	{
		id: 158,
		time: 157,
		velocity: 15.0897222222222,
		power: 5082.4706761434,
		road: 1167.32060185185,
		acceleration: 0.0982407407407404
	},
	{
		id: 159,
		time: 158,
		velocity: 15.0505555555556,
		power: 1617.71924846376,
		road: 1182.35,
		acceleration: -0.142129629629629
	},
	{
		id: 160,
		time: 159,
		velocity: 14.7347222222222,
		power: 1594.88039717382,
		road: 1197.23819444444,
		acceleration: -0.140277777777779
	},
	{
		id: 161,
		time: 160,
		velocity: 14.6688888888889,
		power: 1021.91466084925,
		road: 1211.96768518518,
		acceleration: -0.177129629629629
	},
	{
		id: 162,
		time: 161,
		velocity: 14.5191666666667,
		power: 1341.26057975822,
		road: 1226.53328703704,
		acceleration: -0.150648148148148
	},
	{
		id: 163,
		time: 162,
		velocity: 14.2827777777778,
		power: 1422.38333660682,
		road: 1240.95287037037,
		acceleration: -0.141388888888889
	},
	{
		id: 164,
		time: 163,
		velocity: 14.2447222222222,
		power: 1588.01440506004,
		road: 1255.2387037037,
		acceleration: -0.12611111111111
	},
	{
		id: 165,
		time: 164,
		velocity: 14.1408333333333,
		power: 2562.98952331193,
		road: 1269.43546296296,
		acceleration: -0.0520370370370387
	},
	{
		id: 166,
		time: 165,
		velocity: 14.1266666666667,
		power: 2223.99548613215,
		road: 1283.56856481481,
		acceleration: -0.075277777777778
	},
	{
		id: 167,
		time: 166,
		velocity: 14.0188888888889,
		power: 1608.40369252613,
		road: 1297.60476851852,
		acceleration: -0.118518518518519
	},
	{
		id: 168,
		time: 167,
		velocity: 13.7852777777778,
		power: 503.137043688461,
		road: 1311.48268518518,
		acceleration: -0.198055555555555
	},
	{
		id: 169,
		time: 168,
		velocity: 13.5325,
		power: -1076.30748374767,
		road: 1325.10416666667,
		acceleration: -0.314814814814813
	},
	{
		id: 170,
		time: 169,
		velocity: 13.0744444444444,
		power: -2850.63091881601,
		road: 1338.34231481481,
		acceleration: -0.451851851851854
	},
	{
		id: 171,
		time: 170,
		velocity: 12.4297222222222,
		power: -8213.37404957052,
		road: 1350.90365740741,
		acceleration: -0.901759259259258
	},
	{
		id: 172,
		time: 171,
		velocity: 10.8272222222222,
		power: -13892.2408527871,
		road: 1362.27310185185,
		acceleration: -1.48203703703704
	},
	{
		id: 173,
		time: 172,
		velocity: 8.62833333333333,
		power: -14746.730193197,
		road: 1372.01648148148,
		acceleration: -1.77009259259259
	},
	{
		id: 174,
		time: 173,
		velocity: 7.11944444444444,
		power: -10171.8927974897,
		road: 1380.13412037037,
		acceleration: -1.48138888888889
	},
	{
		id: 175,
		time: 174,
		velocity: 6.38305555555556,
		power: -5752.45560733082,
		road: 1386.99314814815,
		acceleration: -1.03583333333333
	},
	{
		id: 176,
		time: 175,
		velocity: 5.52083333333333,
		power: -3606.49313095573,
		road: 1392.94166666667,
		acceleration: -0.785185185185186
	},
	{
		id: 177,
		time: 176,
		velocity: 4.76388888888889,
		power: -3633.09267554953,
		road: 1398.05240740741,
		acceleration: -0.89037037037037
	},
	{
		id: 178,
		time: 177,
		velocity: 3.71194444444444,
		power: -3394.55013632199,
		road: 1402.22050925926,
		acceleration: -0.994907407407407
	},
	{
		id: 179,
		time: 178,
		velocity: 2.53611111111111,
		power: -2071.34932413702,
		road: 1405.49074074074,
		acceleration: -0.800833333333334
	},
	{
		id: 180,
		time: 179,
		velocity: 2.36138888888889,
		power: -1064.83901586489,
		road: 1408.07805555556,
		acceleration: -0.565
	},
	{
		id: 181,
		time: 180,
		velocity: 2.01694444444444,
		power: 37.8840801871272,
		road: 1410.32662037037,
		acceleration: -0.1125
	},
	{
		id: 182,
		time: 181,
		velocity: 2.19861111111111,
		power: 856.861182035944,
		road: 1412.64824074074,
		acceleration: 0.258611111111111
	},
	{
		id: 183,
		time: 182,
		velocity: 3.13722222222222,
		power: 2572.42615317212,
		road: 1415.50722222222,
		acceleration: 0.816111111111111
	},
	{
		id: 184,
		time: 183,
		velocity: 4.46527777777778,
		power: 4350.69029940997,
		road: 1419.30907407407,
		acceleration: 1.06962962962963
	},
	{
		id: 185,
		time: 184,
		velocity: 5.4075,
		power: 3900.55139619548,
		road: 1424.01231481481,
		acceleration: 0.733148148148148
	},
	{
		id: 186,
		time: 185,
		velocity: 5.33666666666667,
		power: 2061.27848940396,
		road: 1429.21912037037,
		acceleration: 0.273981481481481
	},
	{
		id: 187,
		time: 186,
		velocity: 5.28722222222222,
		power: -48.471907049877,
		road: 1434.48657407407,
		acceleration: -0.152685185185184
	},
	{
		id: 188,
		time: 187,
		velocity: 4.94944444444444,
		power: 584.056211647678,
		road: 1439.66578703704,
		acceleration: -0.0237962962962968
	},
	{
		id: 189,
		time: 188,
		velocity: 5.26527777777778,
		power: -20.847826971539,
		road: 1444.7599537037,
		acceleration: -0.146296296296295
	},
	{
		id: 190,
		time: 189,
		velocity: 4.84833333333333,
		power: 805.961052221939,
		road: 1449.79439814815,
		acceleration: 0.026851851851851
	},
	{
		id: 191,
		time: 190,
		velocity: 5.03,
		power: 472.836991930027,
		road: 1454.82097222222,
		acceleration: -0.0425925925925927
	},
	{
		id: 192,
		time: 191,
		velocity: 5.1375,
		power: 129.095175578915,
		road: 1459.76939814815,
		acceleration: -0.113703703703703
	},
	{
		id: 193,
		time: 192,
		velocity: 4.50722222222222,
		power: -992.835959667415,
		road: 1464.48009259259,
		acceleration: -0.36175925925926
	},
	{
		id: 194,
		time: 193,
		velocity: 3.94472222222222,
		power: -2583.07672511138,
		road: 1468.61222222222,
		acceleration: -0.79537037037037
	},
	{
		id: 195,
		time: 194,
		velocity: 2.75138888888889,
		power: -3055.48678580021,
		road: 1471.77037037037,
		acceleration: -1.15259259259259
	},
	{
		id: 196,
		time: 195,
		velocity: 1.04944444444444,
		power: -2163.04751958766,
		road: 1473.69476851852,
		acceleration: -1.31490740740741
	},
	{
		id: 197,
		time: 196,
		velocity: 0,
		power: -604.648014659664,
		road: 1474.50314814815,
		acceleration: -0.91712962962963
	},
	{
		id: 198,
		time: 197,
		velocity: 0,
		power: -36.8329837881741,
		road: 1474.67805555556,
		acceleration: -0.349814814814815
	},
	{
		id: 199,
		time: 198,
		velocity: 0,
		power: 0,
		road: 1474.67805555556,
		acceleration: 0
	},
	{
		id: 200,
		time: 199,
		velocity: 0,
		power: 0,
		road: 1474.67805555556,
		acceleration: 0
	},
	{
		id: 201,
		time: 200,
		velocity: 0,
		power: 0,
		road: 1474.67805555556,
		acceleration: 0
	},
	{
		id: 202,
		time: 201,
		velocity: 0,
		power: 0,
		road: 1474.67805555556,
		acceleration: 0
	},
	{
		id: 203,
		time: 202,
		velocity: 0,
		power: 0,
		road: 1474.67805555556,
		acceleration: 0
	},
	{
		id: 204,
		time: 203,
		velocity: 0,
		power: 0,
		road: 1474.67805555556,
		acceleration: 0
	},
	{
		id: 205,
		time: 204,
		velocity: 0,
		power: 5.54862023839489,
		road: 1474.70898148148,
		acceleration: 0.0618518518518519
	},
	{
		id: 206,
		time: 205,
		velocity: 0.185555555555556,
		power: 7.47293898231285,
		road: 1474.77083333333,
		acceleration: 0
	},
	{
		id: 207,
		time: 206,
		velocity: 0,
		power: 7.47293898231285,
		road: 1474.83268518518,
		acceleration: 0
	},
	{
		id: 208,
		time: 207,
		velocity: 0,
		power: 1.92425451591943,
		road: 1474.86361111111,
		acceleration: -0.0618518518518519
	},
	{
		id: 209,
		time: 208,
		velocity: 0,
		power: 0,
		road: 1474.86361111111,
		acceleration: 0
	},
	{
		id: 210,
		time: 209,
		velocity: 0,
		power: 0,
		road: 1474.86361111111,
		acceleration: 0
	},
	{
		id: 211,
		time: 210,
		velocity: 0,
		power: 0,
		road: 1474.86361111111,
		acceleration: 0
	},
	{
		id: 212,
		time: 211,
		velocity: 0,
		power: 0,
		road: 1474.86361111111,
		acceleration: 0
	},
	{
		id: 213,
		time: 212,
		velocity: 0,
		power: 0,
		road: 1474.86361111111,
		acceleration: 0
	},
	{
		id: 214,
		time: 213,
		velocity: 0,
		power: 0,
		road: 1474.86361111111,
		acceleration: 0
	},
	{
		id: 215,
		time: 214,
		velocity: 0,
		power: 77.3042434763601,
		road: 1475.0362037037,
		acceleration: 0.345185185185185
	},
	{
		id: 216,
		time: 215,
		velocity: 1.03555555555556,
		power: 1146.93830269914,
		road: 1475.96717592593,
		acceleration: 1.17157407407407
	},
	{
		id: 217,
		time: 216,
		velocity: 3.51472222222222,
		power: 3713.56362792991,
		road: 1478.26893518518,
		acceleration: 1.57
	},
	{
		id: 218,
		time: 217,
		velocity: 4.71,
		power: 6239.63276773972,
		road: 1482.13708333333,
		acceleration: 1.56277777777778
	},
	{
		id: 219,
		time: 218,
		velocity: 5.72388888888889,
		power: 3213.20555100309,
		road: 1487.05962962963,
		acceleration: 0.546018518518518
	},
	{
		id: 220,
		time: 219,
		velocity: 5.15277777777778,
		power: 2821.25635638729,
		road: 1492.45824074074,
		acceleration: 0.406111111111112
	},
	{
		id: 221,
		time: 220,
		velocity: 5.92833333333333,
		power: 1648.00014099558,
		road: 1498.13972222222,
		acceleration: 0.15962962962963
	},
	{
		id: 222,
		time: 221,
		velocity: 6.20277777777778,
		power: 5154.26301793255,
		road: 1504.26898148148,
		acceleration: 0.735925925925926
	},
	{
		id: 223,
		time: 222,
		velocity: 7.36055555555556,
		power: 5074.1679410609,
		road: 1511.08101851852,
		acceleration: 0.629629629629628
	},
	{
		id: 224,
		time: 223,
		velocity: 7.81722222222222,
		power: 4364.11083992054,
		road: 1518.44055555555,
		acceleration: 0.465370370370372
	},
	{
		id: 225,
		time: 224,
		velocity: 7.59888888888889,
		power: 1423.63164722332,
		road: 1526.05106481481,
		acceleration: 0.0365740740740739
	},
	{
		id: 226,
		time: 225,
		velocity: 7.47027777777778,
		power: 164.943765457368,
		road: 1533.61152777778,
		acceleration: -0.136666666666667
	},
	{
		id: 227,
		time: 226,
		velocity: 7.40722222222222,
		power: 604.891844738305,
		road: 1541.06694444444,
		acceleration: -0.0734259259259265
	},
	{
		id: 228,
		time: 227,
		velocity: 7.37861111111111,
		power: 363.281450003571,
		road: 1548.43259259259,
		acceleration: -0.106111111111112
	},
	{
		id: 229,
		time: 228,
		velocity: 7.15194444444444,
		power: 1185.41649860635,
		road: 1555.75152777778,
		acceleration: 0.0126851851851866
	},
	{
		id: 230,
		time: 229,
		velocity: 7.44527777777778,
		power: 850.144377782321,
		road: 1563.05921296296,
		acceleration: -0.0351851851851848
	},
	{
		id: 231,
		time: 230,
		velocity: 7.27305555555556,
		power: 119.562880037919,
		road: 1570.27962962963,
		acceleration: -0.139351851851853
	},
	{
		id: 232,
		time: 231,
		velocity: 6.73388888888889,
		power: -1846.20810876727,
		road: 1577.21314814815,
		acceleration: -0.434444444444445
	},
	{
		id: 233,
		time: 232,
		velocity: 6.14194444444444,
		power: -2493.56662933753,
		road: 1583.65037037037,
		acceleration: -0.558148148148148
	},
	{
		id: 234,
		time: 233,
		velocity: 5.59861111111111,
		power: -2234.11906558662,
		road: 1589.53537037037,
		acceleration: -0.546296296296296
	},
	{
		id: 235,
		time: 234,
		velocity: 5.095,
		power: -1702.26356430458,
		road: 1594.9087037037,
		acceleration: -0.477037037037038
	},
	{
		id: 236,
		time: 235,
		velocity: 4.71083333333333,
		power: -727.847201943036,
		road: 1599.89601851852,
		acceleration: -0.294999999999999
	},
	{
		id: 237,
		time: 236,
		velocity: 4.71361111111111,
		power: -58.8058169922365,
		road: 1604.65925925926,
		acceleration: -0.153148148148149
	},
	{
		id: 238,
		time: 237,
		velocity: 4.63555555555556,
		power: 241.743491315456,
		road: 1609.30356481481,
		acceleration: -0.0847222222222221
	},
	{
		id: 239,
		time: 238,
		velocity: 4.45666666666667,
		power: 21.8287523390979,
		road: 1613.83856481481,
		acceleration: -0.133888888888889
	},
	{
		id: 240,
		time: 239,
		velocity: 4.31194444444444,
		power: 58.3938940621291,
		road: 1618.24444444444,
		acceleration: -0.124351851851852
	},
	{
		id: 241,
		time: 240,
		velocity: 4.2625,
		power: -1624.86305763533,
		road: 1622.30930555556,
		acceleration: -0.557685185185185
	},
	{
		id: 242,
		time: 241,
		velocity: 2.78361111111111,
		power: -2830.44412354835,
		road: 1625.57148148148,
		acceleration: -1.04768518518519
	},
	{
		id: 243,
		time: 242,
		velocity: 1.16888888888889,
		power: -2482.76300093705,
		road: 1627.59939814815,
		acceleration: -1.42083333333333
	},
	{
		id: 244,
		time: 243,
		velocity: 0,
		power: -647.117172352939,
		road: 1628.45296296296,
		acceleration: -0.927870370370371
	},
	{
		id: 245,
		time: 244,
		velocity: 0,
		power: -48.3734755035737,
		road: 1628.64777777778,
		acceleration: -0.38962962962963
	},
	{
		id: 246,
		time: 245,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 247,
		time: 246,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 248,
		time: 247,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 249,
		time: 248,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 250,
		time: 249,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 251,
		time: 250,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 252,
		time: 251,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 253,
		time: 252,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 254,
		time: 253,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 255,
		time: 254,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 256,
		time: 255,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 257,
		time: 256,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 258,
		time: 257,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 259,
		time: 258,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 260,
		time: 259,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 261,
		time: 260,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 262,
		time: 261,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 263,
		time: 262,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 264,
		time: 263,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 265,
		time: 264,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 266,
		time: 265,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 267,
		time: 266,
		velocity: 0,
		power: 0,
		road: 1628.64777777778,
		acceleration: 0
	},
	{
		id: 268,
		time: 267,
		velocity: 0,
		power: 366.795247374794,
		road: 1629.05694444444,
		acceleration: 0.818333333333333
	},
	{
		id: 269,
		time: 268,
		velocity: 2.455,
		power: 1656.89500919466,
		road: 1630.44185185185,
		acceleration: 1.13314814814815
	},
	{
		id: 270,
		time: 269,
		velocity: 3.39944444444444,
		power: 4534.35975883593,
		road: 1633.19513888889,
		acceleration: 1.60361111111111
	},
	{
		id: 271,
		time: 270,
		velocity: 4.81083333333333,
		power: 3997.00583133543,
		road: 1637.20662037037,
		acceleration: 0.912777777777778
	},
	{
		id: 272,
		time: 271,
		velocity: 5.19333333333333,
		power: 3695.36214620957,
		road: 1642.00925925926,
		acceleration: 0.669537037037037
	},
	{
		id: 273,
		time: 272,
		velocity: 5.40805555555556,
		power: 3007.3575453711,
		road: 1647.3700462963,
		acceleration: 0.446759259259259
	},
	{
		id: 274,
		time: 273,
		velocity: 6.15111111111111,
		power: 4610.24096392268,
		road: 1653.29023148148,
		acceleration: 0.672037037037038
	},
	{
		id: 275,
		time: 274,
		velocity: 7.20944444444444,
		power: 8190.01822664392,
		road: 1660.10171296296,
		acceleration: 1.11055555555556
	},
	{
		id: 276,
		time: 275,
		velocity: 8.73972222222222,
		power: 9622.98460684299,
		road: 1668.02509259259,
		acceleration: 1.11324074074074
	},
	{
		id: 277,
		time: 276,
		velocity: 9.49083333333333,
		power: 11118.3576255919,
		road: 1677.06412037037,
		acceleration: 1.11805555555556
	},
	{
		id: 278,
		time: 277,
		velocity: 10.5636111111111,
		power: 10505.6343024805,
		road: 1687.11824074074,
		acceleration: 0.912129629629632
	},
	{
		id: 279,
		time: 278,
		velocity: 11.4761111111111,
		power: 8273.77011059171,
		road: 1697.93300925926,
		acceleration: 0.609166666666665
	},
	{
		id: 280,
		time: 279,
		velocity: 11.3183333333333,
		power: 5102.47693270311,
		road: 1709.19060185185,
		acceleration: 0.276481481481483
	},
	{
		id: 281,
		time: 280,
		velocity: 11.3930555555556,
		power: 3537.99455034211,
		road: 1720.64763888889,
		acceleration: 0.122407407407408
	},
	{
		id: 282,
		time: 281,
		velocity: 11.8433333333333,
		power: 6299.09954111104,
		road: 1732.34587962963,
		acceleration: 0.359999999999999
	},
	{
		id: 283,
		time: 282,
		velocity: 12.3983333333333,
		power: 7953.94603568687,
		road: 1744.46305555556,
		acceleration: 0.47787037037037
	},
	{
		id: 284,
		time: 283,
		velocity: 12.8266666666667,
		power: 9968.2813163015,
		road: 1757.12282407407,
		acceleration: 0.607314814814814
	},
	{
		id: 285,
		time: 284,
		velocity: 13.6652777777778,
		power: 8452.20220734104,
		road: 1770.30925925926,
		acceleration: 0.446018518518519
	},
	{
		id: 286,
		time: 285,
		velocity: 13.7363888888889,
		power: 10062.0369437173,
		road: 1783.9874537037,
		acceleration: 0.537500000000001
	},
	{
		id: 287,
		time: 286,
		velocity: 14.4391666666667,
		power: 7361.6290629969,
		road: 1798.08796296296,
		acceleration: 0.307129629629628
	},
	{
		id: 288,
		time: 287,
		velocity: 14.5866666666667,
		power: 7887.31513533283,
		road: 1812.50606481481,
		acceleration: 0.328055555555554
	},
	{
		id: 289,
		time: 288,
		velocity: 14.7205555555556,
		power: 4768.63786100841,
		road: 1827.13476851852,
		acceleration: 0.0931481481481491
	},
	{
		id: 290,
		time: 289,
		velocity: 14.7186111111111,
		power: 3480.87147189266,
		road: 1841.80976851852,
		acceleration: -0.000555555555555642
	},
	{
		id: 291,
		time: 290,
		velocity: 14.585,
		power: 615.450591538583,
		road: 1856.38300925926,
		acceleration: -0.202962962962964
	},
	{
		id: 292,
		time: 291,
		velocity: 14.1116666666667,
		power: -1121.0280470777,
		road: 1870.69231481481,
		acceleration: -0.324907407407407
	},
	{
		id: 293,
		time: 292,
		velocity: 13.7438888888889,
		power: -2249.58204775246,
		road: 1884.63615740741,
		acceleration: -0.406018518518518
	},
	{
		id: 294,
		time: 293,
		velocity: 13.3669444444444,
		power: -399.229795354016,
		road: 1898.24569444444,
		acceleration: -0.262592592592593
	},
	{
		id: 295,
		time: 294,
		velocity: 13.3238888888889,
		power: 855.271376473392,
		road: 1911.64305555555,
		acceleration: -0.161759259259258
	},
	{
		id: 296,
		time: 295,
		velocity: 13.2586111111111,
		power: 1452.15157651447,
		road: 1924.90361111111,
		acceleration: -0.111851851851853
	},
	{
		id: 297,
		time: 296,
		velocity: 13.0313888888889,
		power: 2227.40421788938,
		road: 1938.08407407407,
		acceleration: -0.0483333333333338
	},
	{
		id: 298,
		time: 297,
		velocity: 13.1788888888889,
		power: 3460.17541860959,
		road: 1951.26518518518,
		acceleration: 0.0496296296296297
	},
	{
		id: 299,
		time: 298,
		velocity: 13.4075,
		power: 5204.30000521808,
		road: 1964.5625462963,
		acceleration: 0.182870370370372
	},
	{
		id: 300,
		time: 299,
		velocity: 13.58,
		power: 6142.88355676291,
		road: 1978.07425925926,
		acceleration: 0.245833333333332
	},
	{
		id: 301,
		time: 300,
		velocity: 13.9163888888889,
		power: 5974.94757116814,
		road: 1991.81953703704,
		acceleration: 0.221296296296295
	},
	{
		id: 302,
		time: 301,
		velocity: 14.0713888888889,
		power: 6705.95911042473,
		road: 2005.80759259259,
		acceleration: 0.26425925925926
	},
	{
		id: 303,
		time: 302,
		velocity: 14.3727777777778,
		power: 6551.10689114951,
		road: 2020.04773148148,
		acceleration: 0.239907407407408
	},
	{
		id: 304,
		time: 303,
		velocity: 14.6361111111111,
		power: 8242.06231297576,
		road: 2034.58138888889,
		acceleration: 0.347129629629629
	},
	{
		id: 305,
		time: 304,
		velocity: 15.1127777777778,
		power: 7859.74678620542,
		road: 2049.43949074074,
		acceleration: 0.301759259259258
	},
	{
		id: 306,
		time: 305,
		velocity: 15.2780555555556,
		power: 7493.34537407279,
		road: 2064.57912037037,
		acceleration: 0.261296296296299
	},
	{
		id: 307,
		time: 306,
		velocity: 15.42,
		power: 5984.96204738469,
		road: 2079.92337962963,
		acceleration: 0.147962962962962
	},
	{
		id: 308,
		time: 307,
		velocity: 15.5566666666667,
		power: 6549.76291690199,
		road: 2095.43106481481,
		acceleration: 0.178888888888888
	},
	{
		id: 309,
		time: 308,
		velocity: 15.8147222222222,
		power: 6454.44001774504,
		road: 2111.11050925926,
		acceleration: 0.16462962962963
	},
	{
		id: 310,
		time: 309,
		velocity: 15.9138888888889,
		power: 5951.07235295998,
		road: 2126.93467592593,
		acceleration: 0.124814814814815
	},
	{
		id: 311,
		time: 310,
		velocity: 15.9311111111111,
		power: 5970.4758930304,
		road: 2142.88166666667,
		acceleration: 0.120833333333335
	},
	{
		id: 312,
		time: 311,
		velocity: 16.1772222222222,
		power: 5560.4232487228,
		road: 2158.93388888889,
		acceleration: 0.0896296296296271
	},
	{
		id: 313,
		time: 312,
		velocity: 16.1827777777778,
		power: 6230.06777236087,
		road: 2175.09518518518,
		acceleration: 0.128518518518522
	},
	{
		id: 314,
		time: 313,
		velocity: 16.3166666666667,
		power: 5334.10968335695,
		road: 2191.35407407407,
		acceleration: 0.0666666666666664
	},
	{
		id: 315,
		time: 314,
		velocity: 16.3772222222222,
		power: 4348.36306418804,
		road: 2207.64731481481,
		acceleration: 0.00203703703703439
	},
	{
		id: 316,
		time: 315,
		velocity: 16.1888888888889,
		power: 4180.6763439731,
		road: 2223.93726851852,
		acceleration: -0.00861111111111157
	},
	{
		id: 317,
		time: 316,
		velocity: 16.2908333333333,
		power: 2927.95405246215,
		road: 2240.17912037037,
		acceleration: -0.0875925925925927
	},
	{
		id: 318,
		time: 317,
		velocity: 16.1144444444444,
		power: 2853.96370776765,
		road: 2256.33231481481,
		acceleration: -0.0897222222222247
	},
	{
		id: 319,
		time: 318,
		velocity: 15.9197222222222,
		power: 779.485970717538,
		road: 2272.33027777778,
		acceleration: -0.220740740740739
	},
	{
		id: 320,
		time: 319,
		velocity: 15.6286111111111,
		power: 203.102690728495,
		road: 2288.09087962963,
		acceleration: -0.253981481481482
	},
	{
		id: 321,
		time: 320,
		velocity: 15.3525,
		power: -45.021724568365,
		road: 2303.59152777778,
		acceleration: -0.265925925925924
	},
	{
		id: 322,
		time: 321,
		velocity: 15.1219444444444,
		power: 2504.21119981248,
		road: 2318.91481481481,
		acceleration: -0.088796296296298
	},
	{
		id: 323,
		time: 322,
		velocity: 15.3622222222222,
		power: 2415.37939323872,
		road: 2334.1475462963,
		acceleration: -0.0923148148148147
	},
	{
		id: 324,
		time: 323,
		velocity: 15.0755555555556,
		power: 3753.98781062873,
		road: 2349.33472222222,
		acceleration: 0.0012037037037036
	},
	{
		id: 325,
		time: 324,
		velocity: 15.1255555555556,
		power: 3671.21847304683,
		road: 2364.52027777778,
		acceleration: -0.00444444444444514
	},
	{
		id: 326,
		time: 325,
		velocity: 15.3488888888889,
		power: 6127.69384958257,
		road: 2379.78425925926,
		acceleration: 0.161296296296296
	},
	{
		id: 327,
		time: 326,
		velocity: 15.5594444444444,
		power: 7898.32091694343,
		road: 2395.26453703704,
		acceleration: 0.271296296296299
	},
	{
		id: 328,
		time: 327,
		velocity: 15.9394444444444,
		power: 7743.83832388143,
		road: 2411.00425925926,
		acceleration: 0.247592592592591
	},
	{
		id: 329,
		time: 328,
		velocity: 16.0916666666667,
		power: 9122.85692289411,
		road: 2427.02939814815,
		acceleration: 0.323240740740744
	},
	{
		id: 330,
		time: 329,
		velocity: 16.5291666666667,
		power: 9606.07253326937,
		road: 2443.38416666667,
		acceleration: 0.336018518518518
	},
	{
		id: 331,
		time: 330,
		velocity: 16.9475,
		power: 6942.33057718487,
		road: 2459.98421296296,
		acceleration: 0.154537037037038
	},
	{
		id: 332,
		time: 331,
		velocity: 16.5552777777778,
		power: 3669.92810183936,
		road: 2476.63490740741,
		acceleration: -0.053240740740744
	},
	{
		id: 333,
		time: 332,
		velocity: 16.3694444444444,
		power: 417.780747441595,
		road: 2493.13180555555,
		acceleration: -0.254351851851851
	},
	{
		id: 334,
		time: 333,
		velocity: 16.1844444444444,
		power: 1981.77020065621,
		road: 2509.42657407407,
		acceleration: -0.149907407407408
	},
	{
		id: 335,
		time: 334,
		velocity: 16.1055555555556,
		power: 5370.20194403449,
		road: 2525.68097222222,
		acceleration: 0.0691666666666677
	},
	{
		id: 336,
		time: 335,
		velocity: 16.5769444444444,
		power: 9094.71876937128,
		road: 2542.11935185185,
		acceleration: 0.298796296296295
	},
	{
		id: 337,
		time: 336,
		velocity: 17.0808333333333,
		power: 13320.8133363376,
		road: 2558.97638888889,
		acceleration: 0.538518518518519
	},
	{
		id: 338,
		time: 337,
		velocity: 17.7211111111111,
		power: 12528.7155453017,
		road: 2576.33134259259,
		acceleration: 0.457314814814815
	},
	{
		id: 339,
		time: 338,
		velocity: 17.9488888888889,
		power: 14411.9499367107,
		road: 2594.18319444444,
		acceleration: 0.536481481481484
	},
	{
		id: 340,
		time: 339,
		velocity: 18.6902777777778,
		power: 12769.4518558047,
		road: 2612.50888888889,
		acceleration: 0.411203703703698
	},
	{
		id: 341,
		time: 340,
		velocity: 18.9547222222222,
		power: 12407.3482327268,
		road: 2631.22398148148,
		acceleration: 0.367592592592594
	},
	{
		id: 342,
		time: 341,
		velocity: 19.0516666666667,
		power: 6417.83834905845,
		road: 2650.13560185185,
		acceleration: 0.0254629629629619
	},
	{
		id: 343,
		time: 342,
		velocity: 18.7666666666667,
		power: 2336.15553345704,
		road: 2668.9612037037,
		acceleration: -0.197499999999998
	},
	{
		id: 344,
		time: 343,
		velocity: 18.3622222222222,
		power: -306.231199195652,
		road: 2687.5187037037,
		acceleration: -0.338703703703704
	},
	{
		id: 345,
		time: 344,
		velocity: 18.0355555555556,
		power: -324.882721118291,
		road: 2705.74027777778,
		acceleration: -0.333148148148148
	},
	{
		id: 346,
		time: 345,
		velocity: 17.7672222222222,
		power: 1129.2366276461,
		road: 2723.67384259259,
		acceleration: -0.242870370370369
	},
	{
		id: 347,
		time: 346,
		velocity: 17.6336111111111,
		power: 663.748535831681,
		road: 2741.3537962963,
		acceleration: -0.264351851851853
	},
	{
		id: 348,
		time: 347,
		velocity: 17.2425,
		power: 1972.13019699134,
		road: 2758.81101851852,
		acceleration: -0.181111111111115
	},
	{
		id: 349,
		time: 348,
		velocity: 17.2238888888889,
		power: 3491.22096275963,
		road: 2776.13472222222,
		acceleration: -0.0859259259259204
	},
	{
		id: 350,
		time: 349,
		velocity: 17.3758333333333,
		power: 6143.85444612815,
		road: 2793.45263888889,
		acceleration: 0.0743518518518513
	},
	{
		id: 351,
		time: 350,
		velocity: 17.4655555555556,
		power: 6202.5144182273,
		road: 2810.84513888889,
		acceleration: 0.0748148148148147
	},
	{
		id: 352,
		time: 351,
		velocity: 17.4483333333333,
		power: 4496.47777301346,
		road: 2828.26078703704,
		acceleration: -0.0285185185185206
	},
	{
		id: 353,
		time: 352,
		velocity: 17.2902777777778,
		power: 3512.66288985663,
		road: 2845.61930555555,
		acceleration: -0.0857407407407429
	},
	{
		id: 354,
		time: 353,
		velocity: 17.2083333333333,
		power: 2423.36014697545,
		road: 2862.86092592593,
		acceleration: -0.148055555555551
	},
	{
		id: 355,
		time: 354,
		velocity: 17.0041666666667,
		power: 2719.627807561,
		road: 2879.96546296296,
		acceleration: -0.126111111111115
	},
	{
		id: 356,
		time: 355,
		velocity: 16.9119444444444,
		power: 2800.48358536213,
		road: 2896.94814814815,
		acceleration: -0.117592592592594
	},
	{
		id: 357,
		time: 356,
		velocity: 16.8555555555556,
		power: 4674.64241263802,
		road: 2913.87199074074,
		acceleration: -9.25925925905347E-05
	},
	{
		id: 358,
		time: 357,
		velocity: 17.0038888888889,
		power: 5772.51757756325,
		road: 2930.82898148148,
		acceleration: 0.0663888888888877
	},
	{
		id: 359,
		time: 358,
		velocity: 17.1111111111111,
		power: 5868.3131583036,
		road: 2947.85393518519,
		acceleration: 0.0695370370370405
	},
	{
		id: 360,
		time: 359,
		velocity: 17.0641666666667,
		power: 5192.22195949139,
		road: 2964.92675925926,
		acceleration: 0.0262037037037004
	},
	{
		id: 361,
		time: 360,
		velocity: 17.0825,
		power: 3144.8229026753,
		road: 2981.96365740741,
		acceleration: -0.098055555555554
	},
	{
		id: 362,
		time: 361,
		velocity: 16.8169444444444,
		power: 3442.1342802104,
		road: 2998.91300925926,
		acceleration: -0.0770370370370408
	},
	{
		id: 363,
		time: 362,
		velocity: 16.8330555555556,
		power: 2248.47414162444,
		road: 3015.7500462963,
		acceleration: -0.147592592592588
	},
	{
		id: 364,
		time: 363,
		velocity: 16.6397222222222,
		power: 5039.34896834723,
		road: 3032.52726851852,
		acceleration: 0.0279629629629632
	},
	{
		id: 365,
		time: 364,
		velocity: 16.9008333333333,
		power: 5602.80318265315,
		road: 3049.34912037037,
		acceleration: 0.0612962962962982
	},
	{
		id: 366,
		time: 365,
		velocity: 17.0169444444444,
		power: 8943.86536478963,
		road: 3066.33189814815,
		acceleration: 0.260555555555555
	},
	{
		id: 367,
		time: 366,
		velocity: 17.4213888888889,
		power: 9538.45535840068,
		road: 3083.58625,
		acceleration: 0.282592592592593
	},
	{
		id: 368,
		time: 367,
		velocity: 17.7486111111111,
		power: 8753.85352571935,
		road: 3101.09310185185,
		acceleration: 0.222407407407403
	},
	{
		id: 369,
		time: 368,
		velocity: 17.6841666666667,
		power: 5262.71301102479,
		road: 3118.71597222222,
		acceleration: 0.00962962962962877
	},
	{
		id: 370,
		time: 369,
		velocity: 17.4502777777778,
		power: 3331.53949102508,
		road: 3136.29194444445,
		acceleration: -0.103425925925926
	},
	{
		id: 371,
		time: 370,
		velocity: 17.4383333333333,
		power: 4225.5062154056,
		road: 3153.79236111111,
		acceleration: -0.0476851851851841
	},
	{
		id: 372,
		time: 371,
		velocity: 17.5411111111111,
		power: 5670.76918031257,
		road: 3171.28833333333,
		acceleration: 0.0387962962962973
	},
	{
		id: 373,
		time: 372,
		velocity: 17.5666666666667,
		power: 7617.67343107251,
		road: 3188.87907407407,
		acceleration: 0.150740740740741
	},
	{
		id: 374,
		time: 373,
		velocity: 17.8905555555556,
		power: 7671.75250247821,
		road: 3206.61875,
		acceleration: 0.147129629629628
	},
	{
		id: 375,
		time: 374,
		velocity: 17.9825,
		power: 7064.15528414811,
		road: 3224.48490740741,
		acceleration: 0.105833333333333
	},
	{
		id: 376,
		time: 375,
		velocity: 17.8841666666667,
		power: 6507.42579866576,
		road: 3242.4387962963,
		acceleration: 0.069629629629631
	},
	{
		id: 377,
		time: 376,
		velocity: 18.0994444444444,
		power: 6937.37494620179,
		road: 3260.47310185185,
		acceleration: 0.0912037037037052
	},
	{
		id: 378,
		time: 377,
		velocity: 18.2561111111111,
		power: 11326.6953904666,
		road: 3278.71972222222,
		acceleration: 0.333425925925923
	},
	{
		id: 379,
		time: 378,
		velocity: 18.8844444444444,
		power: 14062.2811504009,
		road: 3297.36523148148,
		acceleration: 0.464351851851855
	},
	{
		id: 380,
		time: 379,
		velocity: 19.4925,
		power: 13550.581252761,
		road: 3316.44736111111,
		acceleration: 0.408888888888885
	},
	{
		id: 381,
		time: 380,
		velocity: 19.4827777777778,
		power: 8120.38433492281,
		road: 3335.78412037037,
		acceleration: 0.100370370370374
	},
	{
		id: 382,
		time: 381,
		velocity: 19.1855555555556,
		power: 1369.74593954154,
		road: 3355.0400462963,
		acceleration: -0.262037037037036
	},
	{
		id: 383,
		time: 382,
		velocity: 18.7063888888889,
		power: -1142.35123302118,
		road: 3373.9687962963,
		acceleration: -0.392314814814817
	},
	{
		id: 384,
		time: 383,
		velocity: 18.3058333333333,
		power: -1569.78046922319,
		road: 3392.49671296296,
		acceleration: -0.409351851851852
	},
	{
		id: 385,
		time: 384,
		velocity: 17.9575,
		power: -492.969820826426,
		road: 3410.64921296296,
		acceleration: -0.34148148148148
	},
	{
		id: 386,
		time: 385,
		velocity: 17.6819444444444,
		power: -204.545457572619,
		road: 3428.47175925926,
		acceleration: -0.318425925925929
	},
	{
		id: 387,
		time: 386,
		velocity: 17.3505555555556,
		power: 2017.29012588507,
		road: 3446.04435185185,
		acceleration: -0.181481481481477
	},
	{
		id: 388,
		time: 387,
		velocity: 17.4130555555556,
		power: 3077.02991187644,
		road: 3463.46921296296,
		acceleration: -0.113981481481485
	},
	{
		id: 389,
		time: 388,
		velocity: 17.34,
		power: 4483.62380681796,
		road: 3480.82351851852,
		acceleration: -0.0271296296296271
	},
	{
		id: 390,
		time: 389,
		velocity: 17.2691666666667,
		power: 2497.01540380902,
		road: 3498.09208333333,
		acceleration: -0.144351851851852
	},
	{
		id: 391,
		time: 390,
		velocity: 16.98,
		power: 3018.33413192758,
		road: 3515.23398148148,
		acceleration: -0.108981481481482
	},
	{
		id: 392,
		time: 391,
		velocity: 17.0130555555556,
		power: 3162.41714375016,
		road: 3532.27287037037,
		acceleration: -0.0970370370370368
	},
	{
		id: 393,
		time: 392,
		velocity: 16.9780555555556,
		power: 4560.619394593,
		road: 3549.25856481482,
		acceleration: -0.00935185185185006
	},
	{
		id: 394,
		time: 393,
		velocity: 16.9519444444444,
		power: 4599.57140242137,
		road: 3566.23625,
		acceleration: -0.00666666666667126
	},
	{
		id: 395,
		time: 394,
		velocity: 16.9930555555556,
		power: 4791.61163848653,
		road: 3583.21319444445,
		acceleration: 0.00518518518518718
	},
	{
		id: 396,
		time: 395,
		velocity: 16.9936111111111,
		power: 6001.24414555677,
		road: 3600.23171296296,
		acceleration: 0.0779629629629639
	},
	{
		id: 397,
		time: 396,
		velocity: 17.1858333333333,
		power: 5835.641305983,
		road: 3617.32166666667,
		acceleration: 0.0649074074074036
	},
	{
		id: 398,
		time: 397,
		velocity: 17.1877777777778,
		power: 6222.92301982515,
		road: 3634.48680555556,
		acceleration: 0.0854629629629642
	},
	{
		id: 399,
		time: 398,
		velocity: 17.25,
		power: 5236.62004978787,
		road: 3651.70634259259,
		acceleration: 0.0233333333333334
	},
	{
		id: 400,
		time: 399,
		velocity: 17.2558333333333,
		power: 4840.61533694397,
		road: 3668.93699074074,
		acceleration: -0.00111111111110773
	},
	{
		id: 401,
		time: 400,
		velocity: 17.1844444444444,
		power: 4138.30435793634,
		road: 3686.14560185185,
		acceleration: -0.0429629629629638
	},
	{
		id: 402,
		time: 401,
		velocity: 17.1211111111111,
		power: 3405.30090772419,
		road: 3703.29,
		acceleration: -0.0854629629629642
	},
	{
		id: 403,
		time: 402,
		velocity: 16.9994444444444,
		power: 3551.69106260617,
		road: 3720.35467592593,
		acceleration: -0.0739814814814821
	},
	{
		id: 404,
		time: 403,
		velocity: 16.9625,
		power: 3713.46870044072,
		road: 3737.35143518519,
		acceleration: -0.061851851851852
	},
	{
		id: 405,
		time: 404,
		velocity: 16.9355555555556,
		power: 3724.90573208828,
		road: 3754.28768518519,
		acceleration: -0.0591666666666661
	},
	{
		id: 406,
		time: 405,
		velocity: 16.8219444444444,
		power: 3799.95709513465,
		road: 3771.16800925926,
		acceleration: -0.0526851851851831
	},
	{
		id: 407,
		time: 406,
		velocity: 16.8044444444444,
		power: 4008.32747828059,
		road: 3788.00287037037,
		acceleration: -0.0382407407407435
	},
	{
		id: 408,
		time: 407,
		velocity: 16.8208333333333,
		power: 3911.88382028255,
		road: 3804.79717592593,
		acceleration: -0.0428703703703697
	},
	{
		id: 409,
		time: 408,
		velocity: 16.6933333333333,
		power: 4044.24109384973,
		road: 3821.55337962963,
		acceleration: -0.033333333333335
	},
	{
		id: 410,
		time: 409,
		velocity: 16.7044444444444,
		power: 3200.46901214389,
		road: 3838.25083333333,
		acceleration: -0.0841666666666683
	},
	{
		id: 411,
		time: 410,
		velocity: 16.5683333333333,
		power: 4654.1118398459,
		road: 3854.91032407407,
		acceleration: 0.00824074074074233
	},
	{
		id: 412,
		time: 411,
		velocity: 16.7180555555556,
		power: 4429.04580807351,
		road: 3871.57097222222,
		acceleration: -0.00592592592592567
	},
	{
		id: 413,
		time: 412,
		velocity: 16.6866666666667,
		power: 4748.62222213023,
		road: 3888.23564814815,
		acceleration: 0.0139814814814834
	},
	{
		id: 414,
		time: 413,
		velocity: 16.6102777777778,
		power: 4833.55533315642,
		road: 3904.91666666667,
		acceleration: 0.0187037037037037
	},
	{
		id: 415,
		time: 414,
		velocity: 16.7741666666667,
		power: 5265.24395713899,
		road: 3921.62930555556,
		acceleration: 0.0445370370370348
	},
	{
		id: 416,
		time: 415,
		velocity: 16.8202777777778,
		power: 7242.26468041371,
		road: 3938.44592592593,
		acceleration: 0.163425925925928
	},
	{
		id: 417,
		time: 416,
		velocity: 17.1005555555556,
		power: 6664.57925205356,
		road: 3955.40490740741,
		acceleration: 0.121296296296297
	},
	{
		id: 418,
		time: 417,
		velocity: 17.1380555555556,
		power: 6814.06022165036,
		road: 3972.48708333333,
		acceleration: 0.125092592592594
	},
	{
		id: 419,
		time: 418,
		velocity: 17.1955555555556,
		power: 5536.52626649676,
		road: 3989.65361111111,
		acceleration: 0.0436111111111082
	},
	{
		id: 420,
		time: 419,
		velocity: 17.2313888888889,
		power: 4921.79980177639,
		road: 4006.84458333333,
		acceleration: 0.00527777777777771
	},
	{
		id: 421,
		time: 420,
		velocity: 17.1538888888889,
		power: 5967.91890529637,
		road: 4024.07189814815,
		acceleration: 0.0674074074074049
	},
	{
		id: 422,
		time: 421,
		velocity: 17.3977777777778,
		power: 6067.88195619769,
		road: 4041.36824074074,
		acceleration: 0.0706481481481482
	},
	{
		id: 423,
		time: 422,
		velocity: 17.4433333333333,
		power: 7584.38778788911,
		road: 4058.77842592593,
		acceleration: 0.157037037037039
	},
	{
		id: 424,
		time: 423,
		velocity: 17.625,
		power: 6929.30406315156,
		road: 4076.32305555556,
		acceleration: 0.111851851851853
	},
	{
		id: 425,
		time: 424,
		velocity: 17.7333333333333,
		power: 7580.99087448734,
		road: 4093.99601851852,
		acceleration: 0.144814814814815
	},
	{
		id: 426,
		time: 425,
		velocity: 17.8777777777778,
		power: 8397.96996401362,
		road: 4111.83398148148,
		acceleration: 0.185185185185187
	},
	{
		id: 427,
		time: 426,
		velocity: 18.1805555555556,
		power: 8856.50181877083,
		road: 4129.86578703704,
		acceleration: 0.202500000000001
	},
	{
		id: 428,
		time: 427,
		velocity: 18.3408333333333,
		power: 9456.60282758445,
		road: 4148.11203703704,
		acceleration: 0.226388888888888
	},
	{
		id: 429,
		time: 428,
		velocity: 18.5569444444444,
		power: 7590.54621828304,
		road: 4166.5275,
		acceleration: 0.112037037037034
	},
	{
		id: 430,
		time: 429,
		velocity: 18.5166666666667,
		power: 4621.71675269128,
		road: 4184.97032407407,
		acceleration: -0.0573148148148128
	},
	{
		id: 431,
		time: 430,
		velocity: 18.1688888888889,
		power: -38.2949880815091,
		road: 4203.22578703704,
		acceleration: -0.317407407407408
	},
	{
		id: 432,
		time: 431,
		velocity: 17.6047222222222,
		power: -1302.0668659473,
		road: 4221.13050925926,
		acceleration: -0.384074074074075
	},
	{
		id: 433,
		time: 432,
		velocity: 17.3644444444444,
		power: -1694.41280357519,
		road: 4238.6425,
		acceleration: -0.401388888888889
	},
	{
		id: 434,
		time: 433,
		velocity: 16.9647222222222,
		power: 86.919817601915,
		road: 4255.80967592593,
		acceleration: -0.28824074074074
	},
	{
		id: 435,
		time: 434,
		velocity: 16.74,
		power: -1575.99424051092,
		road: 4272.64023148148,
		acceleration: -0.384999999999998
	},
	{
		id: 436,
		time: 435,
		velocity: 16.2094444444444,
		power: -1418.51927953956,
		road: 4289.09319444445,
		acceleration: -0.370185185185186
	},
	{
		id: 437,
		time: 436,
		velocity: 15.8541666666667,
		power: -1596.52191819274,
		road: 4305.1725462963,
		acceleration: -0.377037037037038
	},
	{
		id: 438,
		time: 437,
		velocity: 15.6088888888889,
		power: -994.098871510301,
		road: 4320.89689814815,
		acceleration: -0.332962962962961
	},
	{
		id: 439,
		time: 438,
		velocity: 15.2105555555556,
		power: 209.859298585164,
		road: 4336.33101851852,
		acceleration: -0.247500000000002
	},
	{
		id: 440,
		time: 439,
		velocity: 15.1116666666667,
		power: 2248.9465019315,
		road: 4351.58916666667,
		acceleration: -0.104444444444443
	},
	{
		id: 441,
		time: 440,
		velocity: 15.2955555555556,
		power: 5631.33291763486,
		road: 4366.85861111111,
		acceleration: 0.127037037037034
	},
	{
		id: 442,
		time: 441,
		velocity: 15.5916666666667,
		power: 7702.549210816,
		road: 4382.32106481482,
		acceleration: 0.258981481481484
	},
	{
		id: 443,
		time: 442,
		velocity: 15.8886111111111,
		power: 7327.92694715101,
		road: 4398.02388888889,
		acceleration: 0.221759259259258
	},
	{
		id: 444,
		time: 443,
		velocity: 15.9608333333333,
		power: 7369.73292518874,
		road: 4413.94453703704,
		acceleration: 0.213888888888887
	},
	{
		id: 445,
		time: 444,
		velocity: 16.2333333333333,
		power: 6115.25415482883,
		road: 4430.03421296296,
		acceleration: 0.124166666666667
	},
	{
		id: 446,
		time: 445,
		velocity: 16.2611111111111,
		power: 6026.3652242622,
		road: 4446.24263888889,
		acceleration: 0.113333333333333
	},
	{
		id: 447,
		time: 446,
		velocity: 16.3008333333333,
		power: 3664.33754396175,
		road: 4462.48759259259,
		acceleration: -0.0402777777777743
	},
	{
		id: 448,
		time: 447,
		velocity: 16.1125,
		power: 2715.12666399327,
		road: 4478.66273148148,
		acceleration: -0.099351851851857
	},
	{
		id: 449,
		time: 448,
		velocity: 15.9630555555556,
		power: 1111.93183882702,
		road: 4494.68837962963,
		acceleration: -0.199629629629625
	},
	{
		id: 450,
		time: 449,
		velocity: 15.7019444444444,
		power: 2712.35503065505,
		road: 4510.56875,
		acceleration: -0.0909259259259283
	},
	{
		id: 451,
		time: 450,
		velocity: 15.8397222222222,
		power: 2326.50431193097,
		road: 4526.34689814815,
		acceleration: -0.113518518518518
	},
	{
		id: 452,
		time: 451,
		velocity: 15.6225,
		power: 3431.959687756,
		road: 4542.04939814815,
		acceleration: -0.0377777777777784
	},
	{
		id: 453,
		time: 452,
		velocity: 15.5886111111111,
		power: 1745.88927530792,
		road: 4557.65912037037,
		acceleration: -0.147777777777776
	},
	{
		id: 454,
		time: 453,
		velocity: 15.3963888888889,
		power: 3088.86299004889,
		road: 4573.16763888889,
		acceleration: -0.0546296296296305
	},
	{
		id: 455,
		time: 454,
		velocity: 15.4586111111111,
		power: 2984.67179192298,
		road: 4588.61888888889,
		acceleration: -0.0599074074074082
	},
	{
		id: 456,
		time: 455,
		velocity: 15.4088888888889,
		power: 3830.51333743398,
		road: 4604.03939814815,
		acceleration: -0.00157407407407462
	},
	{
		id: 457,
		time: 456,
		velocity: 15.3916666666667,
		power: 3763.10474726675,
		road: 4619.45611111111,
		acceleration: -0.00601851851851798
	},
	{
		id: 458,
		time: 457,
		velocity: 15.4405555555556,
		power: 3428.83530813486,
		road: 4634.85574074074,
		acceleration: -0.0281481481481478
	},
	{
		id: 459,
		time: 458,
		velocity: 15.3244444444444,
		power: 2587.42371801307,
		road: 4650.19944444445,
		acceleration: -0.0837037037037049
	},
	{
		id: 460,
		time: 459,
		velocity: 15.1405555555556,
		power: 2050.03700221794,
		road: 4665.44245370371,
		acceleration: -0.117685185185186
	},
	{
		id: 461,
		time: 460,
		velocity: 15.0875,
		power: 2266.80914491748,
		road: 4680.57671296297,
		acceleration: -0.099814814814815
	},
	{
		id: 462,
		time: 461,
		velocity: 15.025,
		power: 3869.05377247345,
		road: 4695.66731481482,
		acceleration: 0.0125000000000028
	},
	{
		id: 463,
		time: 462,
		velocity: 15.1780555555556,
		power: 3370.14457064293,
		road: 4710.75319444445,
		acceleration: -0.0219444444444452
	},
	{
		id: 464,
		time: 463,
		velocity: 15.0216666666667,
		power: 3622.77057810059,
		road: 4725.82611111111,
		acceleration: -0.00398148148148181
	},
	{
		id: 465,
		time: 464,
		velocity: 15.0130555555556,
		power: 415.185708943045,
		road: 4740.78476851852,
		acceleration: -0.224537037037036
	},
	{
		id: 466,
		time: 465,
		velocity: 14.5044444444444,
		power: 498.123645987906,
		road: 4755.52388888889,
		acceleration: -0.214537037037038
	},
	{
		id: 467,
		time: 466,
		velocity: 14.3780555555556,
		power: -623.772227311685,
		road: 4770.01032407408,
		acceleration: -0.290833333333332
	},
	{
		id: 468,
		time: 467,
		velocity: 14.1405555555556,
		power: 683.956664630617,
		road: 4784.25555555556,
		acceleration: -0.191574074074076
	},
	{
		id: 469,
		time: 468,
		velocity: 13.9297222222222,
		power: 231.387759032977,
		road: 4798.29435185185,
		acceleration: -0.221296296296295
	},
	{
		id: 470,
		time: 469,
		velocity: 13.7141666666667,
		power: 252.088246081595,
		road: 4812.11449074074,
		acceleration: -0.216018518518519
	},
	{
		id: 471,
		time: 470,
		velocity: 13.4925,
		power: 42.3414720018998,
		road: 4825.71240740741,
		acceleration: -0.228425925925926
	},
	{
		id: 472,
		time: 471,
		velocity: 13.2444444444444,
		power: 140.22834602208,
		road: 4839.08745370371,
		acceleration: -0.217314814814815
	},
	{
		id: 473,
		time: 472,
		velocity: 13.0622222222222,
		power: -38.7699937137927,
		road: 4852.23981481482,
		acceleration: -0.228055555555557
	},
	{
		id: 474,
		time: 473,
		velocity: 12.8083333333333,
		power: 159.93778816428,
		road: 4865.17375,
		acceleration: -0.208796296296295
	},
	{
		id: 475,
		time: 474,
		velocity: 12.6180555555556,
		power: 617.625663450178,
		road: 4877.91916666667,
		acceleration: -0.168240740740742
	},
	{
		id: 476,
		time: 475,
		velocity: 12.5575,
		power: 2896.58529955608,
		road: 4890.59125,
		acceleration: 0.0215740740740777
	},
	{
		id: 477,
		time: 476,
		velocity: 12.8730555555556,
		power: 2964.59719917173,
		road: 4903.28731481482,
		acceleration: 0.0263888888888886
	},
	{
		id: 478,
		time: 477,
		velocity: 12.6972222222222,
		power: 2273.95804096173,
		road: 4915.9812962963,
		acceleration: -0.0305555555555568
	},
	{
		id: 479,
		time: 478,
		velocity: 12.4658333333333,
		power: -93.3931255594067,
		road: 4928.54787037037,
		acceleration: -0.224259259259258
	},
	{
		id: 480,
		time: 479,
		velocity: 12.2002777777778,
		power: 380.15110008923,
		road: 4940.91162037037,
		acceleration: -0.18138888888889
	},
	{
		id: 481,
		time: 480,
		velocity: 12.1530555555556,
		power: 1519.55655201109,
		road: 4953.14388888889,
		acceleration: -0.0815740740740747
	},
	{
		id: 482,
		time: 481,
		velocity: 12.2211111111111,
		power: 3166.02710346952,
		road: 4965.36532407408,
		acceleration: 0.0599074074074064
	},
	{
		id: 483,
		time: 482,
		velocity: 12.38,
		power: 4355.72928063539,
		road: 4977.69527777778,
		acceleration: 0.157129629629631
	},
	{
		id: 484,
		time: 483,
		velocity: 12.6244444444444,
		power: 4419.79873895821,
		road: 4990.18162037037,
		acceleration: 0.155648148148146
	},
	{
		id: 485,
		time: 484,
		velocity: 12.6880555555556,
		power: 3583.3534169156,
		road: 5002.78625,
		acceleration: 0.0809259259259267
	},
	{
		id: 486,
		time: 485,
		velocity: 12.6227777777778,
		power: 1764.15623549535,
		road: 5015.39611111111,
		acceleration: -0.0704629629629636
	},
	{
		id: 487,
		time: 486,
		velocity: 12.4130555555556,
		power: 1566.1955423041,
		road: 5027.92824074074,
		acceleration: -0.0850000000000009
	},
	{
		id: 488,
		time: 487,
		velocity: 12.4330555555556,
		power: 1557.97200956942,
		road: 5040.37606481482,
		acceleration: -0.0836111111111109
	},
	{
		id: 489,
		time: 488,
		velocity: 12.3719444444444,
		power: 1754.92423539837,
		road: 5052.74953703704,
		acceleration: -0.0650925925925918
	},
	{
		id: 490,
		time: 489,
		velocity: 12.2177777777778,
		power: 1122.80198426807,
		road: 5065.03212962963,
		acceleration: -0.116666666666664
	},
	{
		id: 491,
		time: 490,
		velocity: 12.0830555555556,
		power: 1933.29592433882,
		road: 5077.23375,
		acceleration: -0.0452777777777804
	},
	{
		id: 492,
		time: 491,
		velocity: 12.2361111111111,
		power: 3866.2719799548,
		road: 5089.47236111111,
		acceleration: 0.119259259259259
	},
	{
		id: 493,
		time: 492,
		velocity: 12.5755555555556,
		power: 5871.60215904992,
		road: 5101.91064814815,
		acceleration: 0.280092592592593
	},
	{
		id: 494,
		time: 493,
		velocity: 12.9233333333333,
		power: 7885.79509098777,
		road: 5114.70212962963,
		acceleration: 0.426296296296295
	},
	{
		id: 495,
		time: 494,
		velocity: 13.515,
		power: 8093.22680180018,
		road: 5127.91467592593,
		acceleration: 0.415833333333335
	},
	{
		id: 496,
		time: 495,
		velocity: 13.8230555555556,
		power: 11632.6666694707,
		road: 5141.66125,
		acceleration: 0.652222222222221
	},
	{
		id: 497,
		time: 496,
		velocity: 14.88,
		power: 17341.9408034125,
		road: 5156.23319444445,
		acceleration: 0.998518518518518
	},
	{
		id: 498,
		time: 497,
		velocity: 16.5105555555556,
		power: 18613.7588509582,
		road: 5171.79791666667,
		acceleration: 0.987037037037037
	},
	{
		id: 499,
		time: 498,
		velocity: 16.7841666666667,
		power: 17789.4986658561,
		road: 5188.28013888889,
		acceleration: 0.847962962962967
	},
	{
		id: 500,
		time: 499,
		velocity: 17.4238888888889,
		power: 9877.71163335661,
		road: 5205.34314814815,
		acceleration: 0.313611111111108
	},
	{
		id: 501,
		time: 500,
		velocity: 17.4513888888889,
		power: 9181.2991262324,
		road: 5222.69101851852,
		acceleration: 0.25611111111111
	},
	{
		id: 502,
		time: 501,
		velocity: 17.5525,
		power: 6279.45743321779,
		road: 5240.20416666667,
		acceleration: 0.0744444444444454
	},
	{
		id: 503,
		time: 502,
		velocity: 17.6472222222222,
		power: 13535.3399926096,
		road: 5257.99898148149,
		acceleration: 0.488888888888887
	},
	{
		id: 504,
		time: 503,
		velocity: 18.9180555555556,
		power: 16230.365944134,
		road: 5276.34185185186,
		acceleration: 0.607222222222227
	},
	{
		id: 505,
		time: 504,
		velocity: 19.3741666666667,
		power: 19379.3459878285,
		road: 5295.35490740741,
		acceleration: 0.733148148148146
	},
	{
		id: 506,
		time: 505,
		velocity: 19.8466666666667,
		power: 12413.3103017678,
		road: 5314.89486111112,
		acceleration: 0.320648148148145
	},
	{
		id: 507,
		time: 506,
		velocity: 19.88,
		power: 8813.89079082891,
		road: 5334.65425925926,
		acceleration: 0.118240740740742
	},
	{
		id: 508,
		time: 507,
		velocity: 19.7288888888889,
		power: 3980.62353573675,
		road: 5354.40439814815,
		acceleration: -0.136759259259261
	},
	{
		id: 509,
		time: 508,
		velocity: 19.4363888888889,
		power: 2373.13810154923,
		road: 5373.97782407408,
		acceleration: -0.216666666666661
	},
	{
		id: 510,
		time: 509,
		velocity: 19.23,
		power: 1241.79798045458,
		road: 5393.3075,
		acceleration: -0.270833333333336
	},
	{
		id: 511,
		time: 510,
		velocity: 18.9163888888889,
		power: -571.952836720074,
		road: 5412.32050925926,
		acceleration: -0.362500000000001
	},
	{
		id: 512,
		time: 511,
		velocity: 18.3488888888889,
		power: -3565.87228535605,
		road: 5430.89111111111,
		acceleration: -0.522314814814816
	},
	{
		id: 513,
		time: 512,
		velocity: 17.6630555555556,
		power: -5927.21573650214,
		road: 5448.87351851852,
		acceleration: -0.654074074074071
	},
	{
		id: 514,
		time: 513,
		velocity: 16.9541666666667,
		power: -6611.16697273332,
		road: 5466.18111111111,
		acceleration: -0.695555555555554
	},
	{
		id: 515,
		time: 514,
		velocity: 16.2622222222222,
		power: -5158.76343497814,
		road: 5482.83708333334,
		acceleration: -0.607685185185186
	},
	{
		id: 516,
		time: 515,
		velocity: 15.84,
		power: -3152.30211888996,
		road: 5498.95,
		acceleration: -0.478425925925928
	},
	{
		id: 517,
		time: 516,
		velocity: 15.5188888888889,
		power: -913.577607029445,
		road: 5514.66,
		acceleration: -0.327407407407406
	},
	{
		id: 518,
		time: 517,
		velocity: 15.28,
		power: -355.833957324992,
		road: 5530.06361111111,
		acceleration: -0.285370370370369
	},
	{
		id: 519,
		time: 518,
		velocity: 14.9838888888889,
		power: 349.458719991012,
		road: 5545.20824074074,
		acceleration: -0.232592592592592
	},
	{
		id: 520,
		time: 519,
		velocity: 14.8211111111111,
		power: 305.241346653715,
		road: 5560.12087962963,
		acceleration: -0.23138888888889
	},
	{
		id: 521,
		time: 520,
		velocity: 14.5858333333333,
		power: 416.739978763989,
		road: 5574.80814814815,
		acceleration: -0.219351851851849
	},
	{
		id: 522,
		time: 521,
		velocity: 14.3258333333333,
		power: -1072.47452900499,
		road: 5589.22449074074,
		acceleration: -0.322500000000003
	},
	{
		id: 523,
		time: 522,
		velocity: 13.8536111111111,
		power: -1973.94248407199,
		road: 5603.28662037037,
		acceleration: -0.385925925925925
	},
	{
		id: 524,
		time: 523,
		velocity: 13.4280555555556,
		power: -1994.50663988197,
		road: 5616.96296296297,
		acceleration: -0.38564814814815
	},
	{
		id: 525,
		time: 524,
		velocity: 13.1688888888889,
		power: -153.868372154216,
		road: 5630.32638888889,
		acceleration: -0.240185185185183
	},
	{
		id: 526,
		time: 525,
		velocity: 13.1330555555556,
		power: 1067.54662006348,
		road: 5643.49953703704,
		acceleration: -0.140370370370372
	},
	{
		id: 527,
		time: 526,
		velocity: 13.0069444444444,
		power: 1815.46294820964,
		road: 5656.56347222223,
		acceleration: -0.0780555555555562
	},
	{
		id: 528,
		time: 527,
		velocity: 12.9347222222222,
		power: 2836.35936353343,
		road: 5669.59087962963,
		acceleration: 0.00500000000000078
	},
	{
		id: 529,
		time: 528,
		velocity: 13.1480555555556,
		power: 3760.85268029784,
		road: 5682.65967592593,
		acceleration: 0.0777777777777775
	},
	{
		id: 530,
		time: 529,
		velocity: 13.2402777777778,
		power: 4726.28369618143,
		road: 5695.84245370371,
		acceleration: 0.150185185185185
	},
	{
		id: 531,
		time: 530,
		velocity: 13.3852777777778,
		power: 3161.21326916638,
		road: 5709.11175925926,
		acceleration: 0.0228703703703701
	},
	{
		id: 532,
		time: 531,
		velocity: 13.2166666666667,
		power: 3684.3894755833,
		road: 5722.4237962963,
		acceleration: 0.0625925925925941
	},
	{
		id: 533,
		time: 532,
		velocity: 13.4280555555556,
		power: 3757.22273037688,
		road: 5735.8000925926,
		acceleration: 0.0659259259259244
	},
	{
		id: 534,
		time: 533,
		velocity: 13.5830555555556,
		power: 4605.17011538266,
		road: 5749.27347222223,
		acceleration: 0.128240740740742
	},
	{
		id: 535,
		time: 534,
		velocity: 13.6013888888889,
		power: 3151.5208321207,
		road: 5762.81740740741,
		acceleration: 0.0128703703703685
	},
	{
		id: 536,
		time: 535,
		velocity: 13.4666666666667,
		power: 150.274342243018,
		road: 5776.25898148149,
		acceleration: -0.21759259259259
	},
	{
		id: 537,
		time: 536,
		velocity: 12.9302777777778,
		power: -766.273183766901,
		road: 5789.44856481482,
		acceleration: -0.286388888888888
	},
	{
		id: 538,
		time: 537,
		velocity: 12.7422222222222,
		power: -723.48898160505,
		road: 5802.35490740741,
		acceleration: -0.280092592592595
	},
	{
		id: 539,
		time: 538,
		velocity: 12.6263888888889,
		power: 568.514389561943,
		road: 5815.03564814815,
		acceleration: -0.171111111111109
	},
	{
		id: 540,
		time: 539,
		velocity: 12.4169444444444,
		power: 1576.75718318469,
		road: 5827.58851851852,
		acceleration: -0.0846296296296298
	},
	{
		id: 541,
		time: 540,
		velocity: 12.4883333333333,
		power: 1168.76215083339,
		road: 5840.04083333334,
		acceleration: -0.116481481481481
	},
	{
		id: 542,
		time: 541,
		velocity: 12.2769444444444,
		power: 3611.41582134583,
		road: 5852.47972222223,
		acceleration: 0.0896296296296271
	},
	{
		id: 543,
		time: 542,
		velocity: 12.6858333333333,
		power: 4137.53425060419,
		road: 5865.02810185186,
		acceleration: 0.129351851851853
	},
	{
		id: 544,
		time: 543,
		velocity: 12.8763888888889,
		power: 6707.68868142081,
		road: 5877.80643518519,
		acceleration: 0.330555555555556
	},
	{
		id: 545,
		time: 544,
		velocity: 13.2686111111111,
		power: 5796.57720309213,
		road: 5890.87064814815,
		acceleration: 0.241203703703704
	},
	{
		id: 546,
		time: 545,
		velocity: 13.4094444444444,
		power: 5933.08113312862,
		road: 5904.17541666667,
		acceleration: 0.239907407407408
	},
	{
		id: 547,
		time: 546,
		velocity: 13.5961111111111,
		power: 5066.99169316478,
		road: 5917.68148148149,
		acceleration: 0.162685185185184
	},
	{
		id: 548,
		time: 547,
		velocity: 13.7566666666667,
		power: 6353.34676129524,
		road: 5931.39476851852,
		acceleration: 0.251759259259259
	},
	{
		id: 549,
		time: 548,
		velocity: 14.1647222222222,
		power: 8374.79959050023,
		road: 5945.42717592593,
		acceleration: 0.386481481481482
	},
	{
		id: 550,
		time: 549,
		velocity: 14.7555555555556,
		power: 10161.59902285,
		road: 5959.89763888889,
		acceleration: 0.489629629629631
	},
	{
		id: 551,
		time: 550,
		velocity: 15.2255555555556,
		power: 11872.8002063323,
		road: 5974.89986111112,
		acceleration: 0.573888888888888
	},
	{
		id: 552,
		time: 551,
		velocity: 15.8863888888889,
		power: 11496.0802147097,
		road: 5990.44407407408,
		acceleration: 0.510092592592592
	},
	{
		id: 553,
		time: 552,
		velocity: 16.2858333333333,
		power: 10253.2075583727,
		road: 6006.44263888889,
		acceleration: 0.398611111111112
	},
	{
		id: 554,
		time: 553,
		velocity: 16.4213888888889,
		power: 5814.37182627473,
		road: 6022.68953703704,
		acceleration: 0.098055555555554
	},
	{
		id: 555,
		time: 554,
		velocity: 16.1805555555556,
		power: 3542.49042855599,
		road: 6038.96097222223,
		acceleration: -0.0489814814814835
	},
	{
		id: 556,
		time: 555,
		velocity: 16.1388888888889,
		power: 3538.26449164623,
		road: 6055.18407407408,
		acceleration: -0.0476851851851805
	},
	{
		id: 557,
		time: 556,
		velocity: 16.2783333333333,
		power: 4565.72062560985,
		road: 6071.39287037038,
		acceleration: 0.0190740740740729
	},
	{
		id: 558,
		time: 557,
		velocity: 16.2377777777778,
		power: 5495.53154595586,
		road: 6087.64976851852,
		acceleration: 0.0771296296296278
	},
	{
		id: 559,
		time: 558,
		velocity: 16.3702777777778,
		power: 4300.23571244644,
		road: 6103.94467592593,
		acceleration: -0.00111111111110773
	},
	{
		id: 560,
		time: 559,
		velocity: 16.275,
		power: 4836.07138508568,
		road: 6120.25537037038,
		acceleration: 0.0326851851851799
	},
	{
		id: 561,
		time: 560,
		velocity: 16.3358333333333,
		power: 4911.26947923696,
		road: 6136.60050925926,
		acceleration: 0.0362037037037055
	},
	{
		id: 562,
		time: 561,
		velocity: 16.4788888888889,
		power: 6719.69345789184,
		road: 6153.03763888889,
		acceleration: 0.14777777777778
	},
	{
		id: 563,
		time: 562,
		velocity: 16.7183333333333,
		power: 8247.05572261171,
		road: 6169.6662962963,
		acceleration: 0.235277777777778
	},
	{
		id: 564,
		time: 563,
		velocity: 17.0416666666667,
		power: 8949.83074246615,
		road: 6186.54574074075,
		acceleration: 0.266296296296296
	},
	{
		id: 565,
		time: 564,
		velocity: 17.2777777777778,
		power: 8673.04507747987,
		road: 6203.67652777778,
		acceleration: 0.236388888888889
	},
	{
		id: 566,
		time: 565,
		velocity: 17.4275,
		power: 7080.28590587375,
		road: 6220.99101851852,
		acceleration: 0.131018518518516
	},
	{
		id: 567,
		time: 566,
		velocity: 17.4347222222222,
		power: 6407.09002816816,
		road: 6238.41393518519,
		acceleration: 0.0858333333333334
	},
	{
		id: 568,
		time: 567,
		velocity: 17.5352777777778,
		power: 5660.55839796443,
		road: 6255.89907407408,
		acceleration: 0.0386111111111127
	},
	{
		id: 569,
		time: 568,
		velocity: 17.5433333333333,
		power: 5628.94550129536,
		road: 6273.42115740741,
		acceleration: 0.0352777777777789
	},
	{
		id: 570,
		time: 569,
		velocity: 17.5405555555556,
		power: 3885.74487123776,
		road: 6290.92680555556,
		acceleration: -0.0681481481481505
	},
	{
		id: 571,
		time: 570,
		velocity: 17.3308333333333,
		power: 1418.02406015696,
		road: 6308.29236111112,
		acceleration: -0.212037037037039
	},
	{
		id: 572,
		time: 571,
		velocity: 16.9072222222222,
		power: 1067.07572951825,
		road: 6325.43787037038,
		acceleration: -0.228055555555557
	},
	{
		id: 573,
		time: 572,
		velocity: 16.8563888888889,
		power: 968.379591602517,
		road: 6342.35490740741,
		acceleration: -0.228888888888889
	},
	{
		id: 574,
		time: 573,
		velocity: 16.6441666666667,
		power: 2591.85471225343,
		road: 6359.09574074075,
		acceleration: -0.123518518518516
	},
	{
		id: 575,
		time: 574,
		velocity: 16.5366666666667,
		power: 2592.41560381381,
		road: 6375.71481481482,
		acceleration: -0.120000000000001
	},
	{
		id: 576,
		time: 575,
		velocity: 16.4963888888889,
		power: 4343.19647709836,
		road: 6392.2700925926,
		acceleration: -0.00759259259259437
	},
	{
		id: 577,
		time: 576,
		velocity: 16.6213888888889,
		power: 6473.98817264715,
		road: 6408.8837962963,
		acceleration: 0.12444444444445
	},
	{
		id: 578,
		time: 577,
		velocity: 16.91,
		power: 7543.61551013083,
		road: 6425.65194444445,
		acceleration: 0.184444444444445
	},
	{
		id: 579,
		time: 578,
		velocity: 17.0497222222222,
		power: 8433.71547978288,
		road: 6442.62708333334,
		acceleration: 0.229537037037034
	},
	{
		id: 580,
		time: 579,
		velocity: 17.31,
		power: 7610.90224046878,
		road: 6459.80175925926,
		acceleration: 0.169537037037038
	},
	{
		id: 581,
		time: 580,
		velocity: 17.4186111111111,
		power: 7856.21853280745,
		road: 6477.14935185185,
		acceleration: 0.176296296296297
	},
	{
		id: 582,
		time: 581,
		velocity: 17.5786111111111,
		power: 7704.13182269039,
		road: 6494.66476851852,
		acceleration: 0.159351851851852
	},
	{
		id: 583,
		time: 582,
		velocity: 17.7880555555556,
		power: 8009.40505096386,
		road: 6512.34476851852,
		acceleration: 0.169814814814814
	},
	{
		id: 584,
		time: 583,
		velocity: 17.9280555555556,
		power: 7609.62644848407,
		road: 6530.17925925926,
		acceleration: 0.139166666666668
	},
	{
		id: 585,
		time: 584,
		velocity: 17.9961111111111,
		power: 8017.72002541848,
		road: 6548.16143518519,
		acceleration: 0.156203703703707
	},
	{
		id: 586,
		time: 585,
		velocity: 18.2566666666667,
		power: 8182.64865651092,
		road: 6566.30092592593,
		acceleration: 0.158425925925922
	},
	{
		id: 587,
		time: 586,
		velocity: 18.4033333333333,
		power: 8663.7973179756,
		road: 6584.60865740741,
		acceleration: 0.178055555555556
	},
	{
		id: 588,
		time: 587,
		velocity: 18.5302777777778,
		power: 8016.27958194795,
		road: 6603.07240740741,
		acceleration: 0.133981481481484
	},
	{
		id: 589,
		time: 588,
		velocity: 18.6586111111111,
		power: 7472.72155253149,
		road: 6621.65222222223,
		acceleration: 0.0981481481481481
	},
	{
		id: 590,
		time: 589,
		velocity: 18.6977777777778,
		power: 7358.19753713563,
		road: 6640.32495370371,
		acceleration: 0.0876851851851832
	},
	{
		id: 591,
		time: 590,
		velocity: 18.7933333333333,
		power: 5864.66488184721,
		road: 6659.04273148149,
		acceleration: 0.00240740740740719
	},
	{
		id: 592,
		time: 591,
		velocity: 18.6658333333333,
		power: 5399.70376602825,
		road: 6677.75013888889,
		acceleration: -0.0231481481481453
	},
	{
		id: 593,
		time: 592,
		velocity: 18.6283333333333,
		power: 3279.95659804397,
		road: 6696.37648148149,
		acceleration: -0.13898148148148
	},
	{
		id: 594,
		time: 593,
		velocity: 18.3763888888889,
		power: 3866.8323238564,
		road: 6714.88226851852,
		acceleration: -0.102129629629637
	},
	{
		id: 595,
		time: 594,
		velocity: 18.3594444444444,
		power: 3721.73662345889,
		road: 6733.28351851852,
		acceleration: -0.106944444444444
	},
	{
		id: 596,
		time: 595,
		velocity: 18.3075,
		power: 4421.67747647386,
		road: 6751.59916666667,
		acceleration: -0.0642592592592592
	},
	{
		id: 597,
		time: 596,
		velocity: 18.1836111111111,
		power: 5877.26114450532,
		road: 6769.8925,
		acceleration: 0.0196296296296303
	},
	{
		id: 598,
		time: 597,
		velocity: 18.4183333333333,
		power: 7099.47751398196,
		road: 6788.23921296297,
		acceleration: 0.0871296296296329
	},
	{
		id: 599,
		time: 598,
		velocity: 18.5688888888889,
		power: 8206.9194674168,
		road: 6806.70189814815,
		acceleration: 0.144814814814815
	},
	{
		id: 600,
		time: 599,
		velocity: 18.6180555555556,
		power: 8155.29710553204,
		road: 6825.30472222223,
		acceleration: 0.135462962962961
	},
	{
		id: 601,
		time: 600,
		velocity: 18.8247222222222,
		power: 7580.39451100893,
		road: 6844.02430555556,
		acceleration: 0.0980555555555576
	},
	{
		id: 602,
		time: 601,
		velocity: 18.8630555555556,
		power: 7141.513339866,
		road: 6862.82791666667,
		acceleration: 0.0699999999999967
	},
	{
		id: 603,
		time: 602,
		velocity: 18.8280555555556,
		power: 5917.42790082485,
		road: 6881.66685185186,
		acceleration: 0.000648148148147953
	},
	{
		id: 604,
		time: 603,
		velocity: 18.8266666666667,
		power: 4727.98313115015,
		road: 6900.47402777778,
		acceleration: -0.0641666666666652
	},
	{
		id: 605,
		time: 604,
		velocity: 18.6705555555556,
		power: 4352.73393228416,
		road: 6919.20787037038,
		acceleration: -0.0824999999999996
	},
	{
		id: 606,
		time: 605,
		velocity: 18.5805555555556,
		power: 2748.82166627512,
		road: 6937.8163425926,
		acceleration: -0.168240740740742
	},
	{
		id: 607,
		time: 606,
		velocity: 18.3219444444444,
		power: 3420.57135597237,
		road: 6956.27773148149,
		acceleration: -0.125925925925927
	},
	{
		id: 608,
		time: 607,
		velocity: 18.2927777777778,
		power: 3415.65132601705,
		road: 6974.61500000001,
		acceleration: -0.122314814814814
	},
	{
		id: 609,
		time: 608,
		velocity: 18.2136111111111,
		power: 5850.70792946872,
		road: 6992.90032407408,
		acceleration: 0.018425925925925
	},
	{
		id: 610,
		time: 609,
		velocity: 18.3772222222222,
		power: 5496.29998108279,
		road: 7011.1937962963,
		acceleration: -0.00212962962962848
	},
	{
		id: 611,
		time: 610,
		velocity: 18.2863888888889,
		power: 4543.06537629107,
		road: 7029.45842592593,
		acceleration: -0.0555555555555571
	},
	{
		id: 612,
		time: 611,
		velocity: 18.0469444444444,
		power: 425.735225410364,
		road: 7047.55162037038,
		acceleration: -0.287314814814813
	},
	{
		id: 613,
		time: 612,
		velocity: 17.5152777777778,
		power: -1228.83388307432,
		road: 7065.31240740741,
		acceleration: -0.377500000000001
	},
	{
		id: 614,
		time: 613,
		velocity: 17.1538888888889,
		power: -2796.55547181795,
		road: 7082.65171296297,
		acceleration: -0.465462962962963
	},
	{
		id: 615,
		time: 614,
		velocity: 16.6505555555556,
		power: -1901.80458747756,
		road: 7099.55523148149,
		acceleration: -0.406111111111109
	},
	{
		id: 616,
		time: 615,
		velocity: 16.2969444444444,
		power: -2037.79375378362,
		road: 7116.05069444445,
		acceleration: -0.41
	},
	{
		id: 617,
		time: 616,
		velocity: 15.9238888888889,
		power: -1461.98876284783,
		road: 7132.15685185186,
		acceleration: -0.368611111111113
	},
	{
		id: 618,
		time: 617,
		velocity: 15.5447222222222,
		power: -3541.34579700832,
		road: 7147.8275462963,
		acceleration: -0.502314814814813
	},
	{
		id: 619,
		time: 618,
		velocity: 14.79,
		power: -7867.99985287482,
		road: 7162.84560185186,
		acceleration: -0.802962962962964
	},
	{
		id: 620,
		time: 619,
		velocity: 13.515,
		power: -10666.5128052236,
		road: 7176.9462962963,
		acceleration: -1.03175925925926
	},
	{
		id: 621,
		time: 620,
		velocity: 12.4494444444444,
		power: -11845.6663795268,
		road: 7189.94217592593,
		acceleration: -1.17787037037037
	},
	{
		id: 622,
		time: 621,
		velocity: 11.2563888888889,
		power: -11538.3902389336,
		road: 7201.73310185186,
		acceleration: -1.23203703703703
	},
	{
		id: 623,
		time: 622,
		velocity: 9.81888888888889,
		power: -10354.4066802752,
		road: 7212.29861111112,
		acceleration: -1.2187962962963
	},
	{
		id: 624,
		time: 623,
		velocity: 8.79305555555556,
		power: -7028.06423864952,
		road: 7221.77638888889,
		acceleration: -0.956666666666667
	},
	{
		id: 625,
		time: 624,
		velocity: 8.38638888888889,
		power: -5139.69933502635,
		road: 7230.37740740741,
		acceleration: -0.796851851851851
	},
	{
		id: 626,
		time: 625,
		velocity: 7.42833333333333,
		power: -5299.21459539909,
		road: 7238.14060185186,
		acceleration: -0.878796296296296
	},
	{
		id: 627,
		time: 626,
		velocity: 6.15666666666667,
		power: -8525.8295795861,
		road: 7244.70555555556,
		acceleration: -1.51768518518519
	},
	{
		id: 628,
		time: 627,
		velocity: 3.83333333333333,
		power: -6872.21125203214,
		road: 7249.71939814815,
		acceleration: -1.58453703703704
	},
	{
		id: 629,
		time: 628,
		velocity: 2.67472222222222,
		power: -4295.1147333885,
		road: 7253.22907407408,
		acceleration: -1.4237962962963
	},
	{
		id: 630,
		time: 629,
		velocity: 1.88527777777778,
		power: -1532.88209690324,
		road: 7255.62416666667,
		acceleration: -0.80537037037037
	},
	{
		id: 631,
		time: 630,
		velocity: 1.41722222222222,
		power: -1118.4746484835,
		road: 7257.17078703704,
		acceleration: -0.891574074074074
	},
	{
		id: 632,
		time: 631,
		velocity: 0,
		power: -373.182009984896,
		road: 7257.95740740741,
		acceleration: -0.628425925925926
	},
	{
		id: 633,
		time: 632,
		velocity: 0,
		power: -77.173883008447,
		road: 7258.19361111112,
		acceleration: -0.472407407407407
	},
	{
		id: 634,
		time: 633,
		velocity: 0,
		power: 0,
		road: 7258.19361111112,
		acceleration: 0
	},
	{
		id: 635,
		time: 634,
		velocity: 0,
		power: 0,
		road: 7258.19361111112,
		acceleration: 0
	},
	{
		id: 636,
		time: 635,
		velocity: 0,
		power: 0,
		road: 7258.19361111112,
		acceleration: 0
	},
	{
		id: 637,
		time: 636,
		velocity: 0,
		power: 0,
		road: 7258.19361111112,
		acceleration: 0
	},
	{
		id: 638,
		time: 637,
		velocity: 0,
		power: 0,
		road: 7258.19361111112,
		acceleration: 0
	},
	{
		id: 639,
		time: 638,
		velocity: 0,
		power: 0,
		road: 7258.19361111112,
		acceleration: 0
	},
	{
		id: 640,
		time: 639,
		velocity: 0,
		power: 77.8073852218738,
		road: 7258.36685185186,
		acceleration: 0.346481481481481
	},
	{
		id: 641,
		time: 640,
		velocity: 1.03944444444444,
		power: 664.362450286805,
		road: 7259.11675925926,
		acceleration: 0.806851851851852
	},
	{
		id: 642,
		time: 641,
		velocity: 2.42055555555556,
		power: 1370.81209274771,
		road: 7260.67078703704,
		acceleration: 0.801388888888889
	},
	{
		id: 643,
		time: 642,
		velocity: 2.40416666666667,
		power: 966.704206816535,
		road: 7262.79986111112,
		acceleration: 0.348703703703703
	},
	{
		id: 644,
		time: 643,
		velocity: 2.08555555555556,
		power: 38.6469006468601,
		road: 7265.04722222223,
		acceleration: -0.11212962962963
	},
	{
		id: 645,
		time: 644,
		velocity: 2.08416666666667,
		power: 79.7416979153422,
		road: 7267.19310185186,
		acceleration: -0.0908333333333329
	},
	{
		id: 646,
		time: 645,
		velocity: 2.13166666666667,
		power: 146.776028844575,
		road: 7269.26597222223,
		acceleration: -0.0551851851851852
	},
	{
		id: 647,
		time: 646,
		velocity: 1.92,
		power: 32.1797257408543,
		road: 7271.25495370371,
		acceleration: -0.112592592592593
	},
	{
		id: 648,
		time: 647,
		velocity: 1.74638888888889,
		power: 232.508487721889,
		road: 7273.1863425926,
		acceleration: -0.00259259259259226
	},
	{
		id: 649,
		time: 648,
		velocity: 2.12388888888889,
		power: 377.997699421207,
		road: 7275.15296296297,
		acceleration: 0.0730555555555552
	},
	{
		id: 650,
		time: 649,
		velocity: 2.13916666666667,
		power: 516.843449383131,
		road: 7277.22282407408,
		acceleration: 0.133425925925926
	},
	{
		id: 651,
		time: 650,
		velocity: 2.14666666666667,
		power: 247.584588968777,
		road: 7279.35560185186,
		acceleration: -0.0075925925925926
	},
	{
		id: 652,
		time: 651,
		velocity: 2.10111111111111,
		power: 351.472436524947,
		road: 7281.50574074075,
		acceleration: 0.0423148148148149
	},
	{
		id: 653,
		time: 652,
		velocity: 2.26611111111111,
		power: 342.006772272528,
		road: 7283.69435185186,
		acceleration: 0.0346296296296296
	},
	{
		id: 654,
		time: 653,
		velocity: 2.25055555555556,
		power: 391.795273413593,
		road: 7285.92763888889,
		acceleration: 0.0547222222222223
	},
	{
		id: 655,
		time: 654,
		velocity: 2.26527777777778,
		power: 380.393871316206,
		road: 7288.21092592593,
		acceleration: 0.0452777777777778
	},
	{
		id: 656,
		time: 655,
		velocity: 2.40194444444444,
		power: 388.416202926017,
		road: 7290.53953703704,
		acceleration: 0.0453703703703701
	},
	{
		id: 657,
		time: 656,
		velocity: 2.38666666666667,
		power: 377.237453625696,
		road: 7292.90944444445,
		acceleration: 0.0372222222222223
	},
	{
		id: 658,
		time: 657,
		velocity: 2.37694444444444,
		power: 248.260318898147,
		road: 7295.28768518519,
		acceleration: -0.0205555555555552
	},
	{
		id: 659,
		time: 658,
		velocity: 2.34027777777778,
		power: 100.689286511116,
		road: 7297.61324074075,
		acceleration: -0.0848148148148149
	},
	{
		id: 660,
		time: 659,
		velocity: 2.13222222222222,
		power: -22.3743589474093,
		road: 7299.82597222223,
		acceleration: -0.140833333333333
	},
	{
		id: 661,
		time: 660,
		velocity: 1.95444444444444,
		power: 53.0127408730629,
		road: 7301.91671296297,
		acceleration: -0.103148148148148
	},
	{
		id: 662,
		time: 661,
		velocity: 2.03083333333333,
		power: 90.6278432806705,
		road: 7303.91495370371,
		acceleration: -0.0818518518518518
	},
	{
		id: 663,
		time: 662,
		velocity: 1.88666666666667,
		power: 168.000979826878,
		road: 7305.85319444445,
		acceleration: -0.038148148148148
	},
	{
		id: 664,
		time: 663,
		velocity: 1.84,
		power: -2.09415763817448,
		road: 7307.70708333334,
		acceleration: -0.130555555555556
	},
	{
		id: 665,
		time: 664,
		velocity: 1.63916666666667,
		power: -434.405855363811,
		road: 7309.28620370371,
		acceleration: -0.418981481481482
	},
	{
		id: 666,
		time: 665,
		velocity: 0.629722222222222,
		power: -488.882415391797,
		road: 7310.34916666667,
		acceleration: -0.613333333333333
	},
	{
		id: 667,
		time: 666,
		velocity: 0,
		power: -191.689870135616,
		road: 7310.83226851852,
		acceleration: -0.546388888888889
	},
	{
		id: 668,
		time: 667,
		velocity: 0,
		power: -8.19077116634178,
		road: 7310.93722222223,
		acceleration: -0.209907407407407
	},
	{
		id: 669,
		time: 668,
		velocity: 0,
		power: 0,
		road: 7310.93722222223,
		acceleration: 0
	},
	{
		id: 670,
		time: 669,
		velocity: 0,
		power: 0,
		road: 7310.93722222223,
		acceleration: 0
	},
	{
		id: 671,
		time: 670,
		velocity: 0,
		power: 0,
		road: 7310.93722222223,
		acceleration: 0
	},
	{
		id: 672,
		time: 671,
		velocity: 0,
		power: 148.017007677418,
		road: 7311.18662037037,
		acceleration: 0.498796296296296
	},
	{
		id: 673,
		time: 672,
		velocity: 1.49638888888889,
		power: 483.347430591047,
		road: 7311.9538425926,
		acceleration: 0.536851851851852
	},
	{
		id: 674,
		time: 673,
		velocity: 1.61055555555556,
		power: 807.642389829276,
		road: 7313.25310185186,
		acceleration: 0.527222222222222
	},
	{
		id: 675,
		time: 674,
		velocity: 1.58166666666667,
		power: 256.36115831657,
		road: 7314.83689814815,
		acceleration: 0.041851851851852
	},
	{
		id: 676,
		time: 675,
		velocity: 1.62194444444444,
		power: -10.8277453051045,
		road: 7316.37351851852,
		acceleration: -0.136203703703704
	},
	{
		id: 677,
		time: 676,
		velocity: 1.20194444444444,
		power: 41.2100366371001,
		road: 7317.79305555556,
		acceleration: -0.0979629629629628
	},
	{
		id: 678,
		time: 677,
		velocity: 1.28777777777778,
		power: -62.4485864866612,
		road: 7319.07370370371,
		acceleration: -0.179814814814815
	},
	{
		id: 679,
		time: 678,
		velocity: 1.0825,
		power: -2.73665885655568,
		road: 7320.19907407408,
		acceleration: -0.130740740740741
	},
	{
		id: 680,
		time: 679,
		velocity: 0.809722222222222,
		power: -147.460791948404,
		road: 7321.10967592593,
		acceleration: -0.298796296296296
	},
	{
		id: 681,
		time: 680,
		velocity: 0.391388888888889,
		power: -128.317456135563,
		road: 7321.69046296297,
		acceleration: -0.360833333333333
	},
	{
		id: 682,
		time: 681,
		velocity: 0,
		power: -35.7979719721647,
		road: 7321.95587962963,
		acceleration: -0.269907407407407
	},
	{
		id: 683,
		time: 682,
		velocity: 0,
		power: -0.181251965562054,
		road: 7322.02111111112,
		acceleration: -0.130462962962963
	},
	{
		id: 684,
		time: 683,
		velocity: 0,
		power: 0,
		road: 7322.02111111112,
		acceleration: 0
	},
	{
		id: 685,
		time: 684,
		velocity: 0,
		power: 0,
		road: 7322.02111111112,
		acceleration: 0
	},
	{
		id: 686,
		time: 685,
		velocity: 0,
		power: 21.499316575877,
		road: 7322.10041666667,
		acceleration: 0.158611111111111
	},
	{
		id: 687,
		time: 686,
		velocity: 0.475833333333333,
		power: 253.718299662575,
		road: 7322.51675925926,
		acceleration: 0.515462962962963
	},
	{
		id: 688,
		time: 687,
		velocity: 1.54638888888889,
		power: 730.42600875789,
		road: 7323.51337962964,
		acceleration: 0.645092592592593
	},
	{
		id: 689,
		time: 688,
		velocity: 1.93527777777778,
		power: 937.34778939153,
		road: 7325.08300925927,
		acceleration: 0.500925925925926
	},
	{
		id: 690,
		time: 689,
		velocity: 1.97861111111111,
		power: 512.645580804898,
		road: 7326.98078703704,
		acceleration: 0.15537037037037
	},
	{
		id: 691,
		time: 690,
		velocity: 2.0125,
		power: 463.40775989576,
		road: 7329.01166666667,
		acceleration: 0.110833333333333
	},
	{
		id: 692,
		time: 691,
		velocity: 2.26777777777778,
		power: 122.36997151835,
		road: 7331.06449074075,
		acceleration: -0.0669444444444443
	},
	{
		id: 693,
		time: 692,
		velocity: 1.77777777777778,
		power: 288.214714719031,
		road: 7333.0938425926,
		acceleration: 0.0199999999999996
	},
	{
		id: 694,
		time: 693,
		velocity: 2.0725,
		power: 74.1375143304625,
		road: 7335.08796296297,
		acceleration: -0.0904629629629627
	},
	{
		id: 695,
		time: 694,
		velocity: 1.99638888888889,
		power: 369.41829233451,
		road: 7337.07027777778,
		acceleration: 0.0668518518518519
	},
	{
		id: 696,
		time: 695,
		velocity: 1.97833333333333,
		power: 90.5220748936938,
		road: 7339.04537037038,
		acceleration: -0.0812962962962962
	},
	{
		id: 697,
		time: 696,
		velocity: 1.82861111111111,
		power: 65.9723501484351,
		road: 7340.93351851853,
		acceleration: -0.0925925925925926
	},
	{
		id: 698,
		time: 697,
		velocity: 1.71861111111111,
		power: 43.6570770267334,
		road: 7342.72361111112,
		acceleration: -0.103518518518519
	},
	{
		id: 699,
		time: 698,
		velocity: 1.66777777777778,
		power: 6.97454268539315,
		road: 7344.39962962964,
		acceleration: -0.12462962962963
	},
	{
		id: 700,
		time: 699,
		velocity: 1.45472222222222,
		power: -64.5422912146404,
		road: 7345.92666666667,
		acceleration: -0.173333333333333
	},
	{
		id: 701,
		time: 700,
		velocity: 1.19861111111111,
		power: -51.2338260584209,
		road: 7347.28287037038,
		acceleration: -0.168333333333333
	},
	{
		id: 702,
		time: 701,
		velocity: 1.16277777777778,
		power: -38.9635477623552,
		road: 7348.47351851853,
		acceleration: -0.162777777777778
	},
	{
		id: 703,
		time: 702,
		velocity: 0.966388888888889,
		power: 107.333085608374,
		road: 7349.57032407408,
		acceleration: -0.0249074074074074
	},
	{
		id: 704,
		time: 703,
		velocity: 1.12388888888889,
		power: 319.409247614743,
		road: 7350.73518518519,
		acceleration: 0.161018518518519
	},
	{
		id: 705,
		time: 704,
		velocity: 1.64583333333333,
		power: 746.187952855233,
		road: 7352.18722222223,
		acceleration: 0.413333333333333
	},
	{
		id: 706,
		time: 705,
		velocity: 2.20638888888889,
		power: 708.343057288921,
		road: 7353.98861111112,
		acceleration: 0.28537037037037
	},
	{
		id: 707,
		time: 706,
		velocity: 1.98,
		power: 510.494066660024,
		road: 7356.00152777779,
		acceleration: 0.137685185185185
	},
	{
		id: 708,
		time: 707,
		velocity: 2.05888888888889,
		power: 98.9232133587435,
		road: 7358.04393518519,
		acceleration: -0.0787037037037037
	},
	{
		id: 709,
		time: 708,
		velocity: 1.97027777777778,
		power: 260.208542254361,
		road: 7360.05050925927,
		acceleration: 0.00703703703703695
	},
	{
		id: 710,
		time: 709,
		velocity: 2.00111111111111,
		power: 244.7110480337,
		road: 7362.05995370371,
		acceleration: -0.00129629629629635
	},
	{
		id: 711,
		time: 710,
		velocity: 2.055,
		power: 354.211751004656,
		road: 7364.09560185186,
		acceleration: 0.0537037037037038
	},
	{
		id: 712,
		time: 711,
		velocity: 2.13138888888889,
		power: 219.29991977896,
		road: 7366.14949074075,
		acceleration: -0.0172222222222223
	},
	{
		id: 713,
		time: 712,
		velocity: 1.94944444444444,
		power: 186.402047536039,
		road: 7368.17833333334,
		acceleration: -0.0328703703703703
	},
	{
		id: 714,
		time: 713,
		velocity: 1.95638888888889,
		power: 141.939297712067,
		road: 7370.16361111112,
		acceleration: -0.0542592592592592
	},
	{
		id: 715,
		time: 714,
		velocity: 1.96861111111111,
		power: 56.2178255617941,
		road: 7372.0725462963,
		acceleration: -0.0984259259259259
	},
	{
		id: 716,
		time: 715,
		velocity: 1.65416666666667,
		power: 69.865479962214,
		road: 7373.88791666667,
		acceleration: -0.0887037037037037
	},
	{
		id: 717,
		time: 716,
		velocity: 1.69027777777778,
		power: 282.50394511959,
		road: 7375.6775462963,
		acceleration: 0.037222222222222
	},
	{
		id: 718,
		time: 717,
		velocity: 2.08027777777778,
		power: 516.177890018833,
		road: 7377.56523148149,
		acceleration: 0.158888888888889
	},
	{
		id: 719,
		time: 718,
		velocity: 2.13083333333333,
		power: 455.819765745027,
		road: 7379.5863888889,
		acceleration: 0.108055555555556
	},
	{
		id: 720,
		time: 719,
		velocity: 2.01444444444444,
		power: 223.88698714089,
		road: 7381.65375000001,
		acceleration: -0.0156481481481481
	},
	{
		id: 721,
		time: 720,
		velocity: 2.03333333333333,
		power: 209.716782700085,
		road: 7383.70236111112,
		acceleration: -0.0218518518518516
	},
	{
		id: 722,
		time: 721,
		velocity: 2.06527777777778,
		power: 13.5278069598829,
		road: 7385.6788425926,
		acceleration: -0.122407407407408
	},
	{
		id: 723,
		time: 722,
		velocity: 1.64722222222222,
		power: 394.991692085918,
		road: 7387.63574074075,
		acceleration: 0.0832407407407405
	},
	{
		id: 724,
		time: 723,
		velocity: 2.28305555555556,
		power: 500.623392294899,
		road: 7389.69736111112,
		acceleration: 0.126203703703704
	},
	{
		id: 725,
		time: 724,
		velocity: 2.44388888888889,
		power: 906.729119372845,
		road: 7391.96745370371,
		acceleration: 0.290740740740741
	},
	{
		id: 726,
		time: 725,
		velocity: 2.51944444444444,
		power: 460.726635434545,
		road: 7394.41666666668,
		acceleration: 0.0674999999999999
	},
	{
		id: 727,
		time: 726,
		velocity: 2.48555555555556,
		power: 395.890041759892,
		road: 7396.9175925926,
		acceleration: 0.0359259259259259
	},
	{
		id: 728,
		time: 727,
		velocity: 2.55166666666667,
		power: 450.416466284067,
		road: 7399.46416666667,
		acceleration: 0.0553703703703703
	},
	{
		id: 729,
		time: 728,
		velocity: 2.68555555555556,
		power: 552.755630501488,
		road: 7402.08398148149,
		acceleration: 0.0911111111111116
	},
	{
		id: 730,
		time: 729,
		velocity: 2.75888888888889,
		power: 482.363996605832,
		road: 7404.77796296297,
		acceleration: 0.0572222222222218
	},
	{
		id: 731,
		time: 730,
		velocity: 2.72333333333333,
		power: 383.695274591228,
		road: 7407.5087962963,
		acceleration: 0.0164814814814815
	},
	{
		id: 732,
		time: 731,
		velocity: 2.735,
		power: 626.706341535381,
		road: 7410.30027777779,
		acceleration: 0.104814814814815
	},
	{
		id: 733,
		time: 732,
		velocity: 3.07333333333333,
		power: 614.919296209572,
		road: 7413.19023148149,
		acceleration: 0.0921296296296292
	},
	{
		id: 734,
		time: 733,
		velocity: 2.99972222222222,
		power: 711.169139061375,
		road: 7416.1851388889,
		acceleration: 0.117777777777778
	},
	{
		id: 735,
		time: 734,
		velocity: 3.08833333333333,
		power: 117.59187706875,
		road: 7419.19328703705,
		acceleration: -0.0912962962962967
	},
	{
		id: 736,
		time: 735,
		velocity: 2.79944444444444,
		power: 452.913157485019,
		road: 7422.16976851853,
		acceleration: 0.0279629629629632
	},
	{
		id: 737,
		time: 736,
		velocity: 3.08361111111111,
		power: 355.127874369194,
		road: 7425.15666666667,
		acceleration: -0.00712962962963015
	},
	{
		id: 738,
		time: 737,
		velocity: 3.06694444444444,
		power: 806.053021713349,
		road: 7428.2126388889,
		acceleration: 0.145277777777778
	},
	{
		id: 739,
		time: 738,
		velocity: 3.23527777777778,
		power: 574.295246889118,
		road: 7431.37055555556,
		acceleration: 0.0586111111111118
	},
	{
		id: 740,
		time: 739,
		velocity: 3.25944444444444,
		power: 577.158755719612,
		road: 7434.58574074075,
		acceleration: 0.0559259259259255
	},
	{
		id: 741,
		time: 740,
		velocity: 3.23472222222222,
		power: 501.076188085577,
		road: 7437.84324074075,
		acceleration: 0.0287037037037035
	},
	{
		id: 742,
		time: 741,
		velocity: 3.32138888888889,
		power: 517.100160326505,
		road: 7441.13120370371,
		acceleration: 0.0322222222222228
	},
	{
		id: 743,
		time: 742,
		velocity: 3.35611111111111,
		power: 615.232085500151,
		road: 7444.46564814816,
		acceleration: 0.0607407407407408
	},
	{
		id: 744,
		time: 743,
		velocity: 3.41694444444444,
		power: 422.377475561418,
		road: 7447.82972222223,
		acceleration: -0.00148148148148186
	},
	{
		id: 745,
		time: 744,
		velocity: 3.31694444444444,
		power: 607.556265859985,
		road: 7451.22050925927,
		acceleration: 0.0549074074074074
	},
	{
		id: 746,
		time: 745,
		velocity: 3.52083333333333,
		power: -154.930542804844,
		road: 7454.54740740742,
		acceleration: -0.182685185185185
	},
	{
		id: 747,
		time: 746,
		velocity: 2.86888888888889,
		power: 68.5642994400911,
		road: 7457.72777777779,
		acceleration: -0.11037037037037
	},
	{
		id: 748,
		time: 747,
		velocity: 2.98583333333333,
		power: -437.782571672406,
		road: 7460.70939814816,
		acceleration: -0.287129629629629
	},
	{
		id: 749,
		time: 748,
		velocity: 2.65944444444444,
		power: 39.7835372871587,
		road: 7463.48912037038,
		acceleration: -0.116666666666667
	},
	{
		id: 750,
		time: 749,
		velocity: 2.51888888888889,
		power: -330.014101058528,
		road: 7466.07773148149,
		acceleration: -0.265555555555556
	},
	{
		id: 751,
		time: 750,
		velocity: 2.18916666666667,
		power: -31.4913396640505,
		road: 7468.4612962963,
		acceleration: -0.144537037037037
	},
	{
		id: 752,
		time: 751,
		velocity: 2.22583333333333,
		power: 214.532520395353,
		road: 7470.75666666668,
		acceleration: -0.0318518518518518
	},
	{
		id: 753,
		time: 752,
		velocity: 2.42333333333333,
		power: 211.785154965887,
		road: 7473.02027777779,
		acceleration: -0.0316666666666663
	},
	{
		id: 754,
		time: 753,
		velocity: 2.09416666666667,
		power: 185.435307652511,
		road: 7475.24685185186,
		acceleration: -0.0424074074074072
	},
	{
		id: 755,
		time: 754,
		velocity: 2.09861111111111,
		power: -6.02195155485916,
		road: 7477.38574074075,
		acceleration: -0.132962962962963
	},
	{
		id: 756,
		time: 755,
		velocity: 2.02444444444444,
		power: 235.341823031267,
		road: 7479.45324074075,
		acceleration: -0.00981481481481516
	},
	{
		id: 757,
		time: 756,
		velocity: 2.06472222222222,
		power: 285.69091555514,
		road: 7481.52365740742,
		acceleration: 0.0156481481481485
	},
	{
		id: 758,
		time: 757,
		velocity: 2.14555555555556,
		power: 296.283949804162,
		road: 7483.61175925927,
		acceleration: 0.0197222222222222
	},
	{
		id: 759,
		time: 758,
		velocity: 2.08361111111111,
		power: 300.905084377522,
		road: 7485.72000000001,
		acceleration: 0.0205555555555557
	},
	{
		id: 760,
		time: 759,
		velocity: 2.12638888888889,
		power: 211.284624998436,
		road: 7487.82643518519,
		acceleration: -0.0241666666666664
	},
	{
		id: 761,
		time: 760,
		velocity: 2.07305555555556,
		power: 242.481190867086,
		road: 7489.91699074075,
		acceleration: -0.00759259259259304
	},
	{
		id: 762,
		time: 761,
		velocity: 2.06083333333333,
		power: 219.001066575121,
		road: 7491.99439814816,
		acceleration: -0.0187037037037041
	},
	{
		id: 763,
		time: 762,
		velocity: 2.07027777777778,
		power: 304.454433057634,
		road: 7494.07467592594,
		acceleration: 0.0244444444444447
	},
	{
		id: 764,
		time: 763,
		velocity: 2.14638888888889,
		power: 290.151591520322,
		road: 7496.17504629631,
		acceleration: 0.0157407407407408
	},
	{
		id: 765,
		time: 764,
		velocity: 2.10805555555556,
		power: 286.632083297757,
		road: 7498.28976851853,
		acceleration: 0.0129629629629635
	},
	{
		id: 766,
		time: 765,
		velocity: 2.10916666666667,
		power: 356.632933637674,
		road: 7500.43365740742,
		acceleration: 0.0453703703703701
	},
	{
		id: 767,
		time: 766,
		velocity: 2.2825,
		power: 518.744907871913,
		road: 7502.65805555556,
		acceleration: 0.115648148148148
	},
	{
		id: 768,
		time: 767,
		velocity: 2.455,
		power: 720.320709385296,
		road: 7505.03472222223,
		acceleration: 0.188888888888889
	},
	{
		id: 769,
		time: 768,
		velocity: 2.67583333333333,
		power: 846.622845433471,
		road: 7507.61328703705,
		acceleration: 0.214907407407408
	},
	{
		id: 770,
		time: 769,
		velocity: 2.92722222222222,
		power: 1008.77422191847,
		road: 7510.4225925926,
		acceleration: 0.246574074074075
	},
	{
		id: 771,
		time: 770,
		velocity: 3.19472222222222,
		power: 1216.77162670313,
		road: 7513.49731481482,
		acceleration: 0.284259259259259
	},
	{
		id: 772,
		time: 771,
		velocity: 3.52861111111111,
		power: 1041.01440960177,
		road: 7516.81277777779,
		acceleration: 0.197222222222222
	},
	{
		id: 773,
		time: 772,
		velocity: 3.51888888888889,
		power: 699.793777846399,
		road: 7520.26652777779,
		acceleration: 0.0793518518518521
	},
	{
		id: 774,
		time: 773,
		velocity: 3.43277777777778,
		power: 34.8030305545787,
		road: 7523.69828703705,
		acceleration: -0.123333333333333
	},
	{
		id: 775,
		time: 774,
		velocity: 3.15861111111111,
		power: -17.3913033869286,
		road: 7526.9988425926,
		acceleration: -0.139074074074074
	},
	{
		id: 776,
		time: 775,
		velocity: 3.10166666666667,
		power: 103.835093417471,
		road: 7530.18050925927,
		acceleration: -0.0987037037037046
	},
	{
		id: 777,
		time: 776,
		velocity: 3.13666666666667,
		power: 629.066556636012,
		road: 7533.35083333334,
		acceleration: 0.0760185185185192
	},
	{
		id: 778,
		time: 777,
		velocity: 3.38666666666667,
		power: 913.868196708446,
		road: 7536.6388425926,
		acceleration: 0.159351851851852
	},
	{
		id: 779,
		time: 778,
		velocity: 3.57972222222222,
		power: 917.874695570518,
		road: 7540.08000000001,
		acceleration: 0.146944444444444
	},
	{
		id: 780,
		time: 779,
		velocity: 3.5775,
		power: 547.016653352067,
		road: 7543.60907407408,
		acceleration: 0.0288888888888894
	},
	{
		id: 781,
		time: 780,
		velocity: 3.47333333333333,
		power: 585.595627249978,
		road: 7547.17189814816,
		acceleration: 0.03861111111111
	},
	{
		id: 782,
		time: 781,
		velocity: 3.69555555555556,
		power: 539.321527732529,
		road: 7550.76574074075,
		acceleration: 0.0234259259259266
	},
	{
		id: 783,
		time: 782,
		velocity: 3.64777777777778,
		power: 794.631777655684,
		road: 7554.41842592594,
		acceleration: 0.094259259259259
	},
	{
		id: 784,
		time: 783,
		velocity: 3.75611111111111,
		power: 508.05724181378,
		road: 7558.12291666668,
		acceleration: 0.00935185185185183
	},
	{
		id: 785,
		time: 784,
		velocity: 3.72361111111111,
		power: 619.985875211689,
		road: 7561.85203703705,
		acceleration: 0.0399074074074082
	},
	{
		id: 786,
		time: 785,
		velocity: 3.7675,
		power: 596.352267176286,
		road: 7565.61685185186,
		acceleration: 0.0314814814814808
	},
	{
		id: 787,
		time: 786,
		velocity: 3.85055555555556,
		power: 604.566272727893,
		road: 7569.41351851853,
		acceleration: 0.0322222222222219
	},
	{
		id: 788,
		time: 787,
		velocity: 3.82027777777778,
		power: 650.25341684523,
		road: 7573.24777777779,
		acceleration: 0.0429629629629633
	},
	{
		id: 789,
		time: 788,
		velocity: 3.89638888888889,
		power: 765.919322041566,
		road: 7577.13921296297,
		acceleration: 0.0713888888888885
	},
	{
		id: 790,
		time: 789,
		velocity: 4.06472222222222,
		power: 852.298509831226,
		road: 7581.11120370371,
		acceleration: 0.089722222222222
	},
	{
		id: 791,
		time: 790,
		velocity: 4.08944444444444,
		power: 1023.95164074604,
		road: 7585.19180555557,
		acceleration: 0.1275
	},
	{
		id: 792,
		time: 791,
		velocity: 4.27888888888889,
		power: 735.412444275137,
		road: 7589.36046296297,
		acceleration: 0.0486111111111116
	},
	{
		id: 793,
		time: 792,
		velocity: 4.21055555555556,
		power: 785.243575153901,
		road: 7593.5826388889,
		acceleration: 0.0584259259259259
	},
	{
		id: 794,
		time: 793,
		velocity: 4.26472222222222,
		power: 598.440555399035,
		road: 7597.83925925927,
		acceleration: 0.0104629629629622
	},
	{
		id: 795,
		time: 794,
		velocity: 4.31027777777778,
		power: 743.417153118257,
		road: 7602.12361111112,
		acceleration: 0.0450000000000008
	},
	{
		id: 796,
		time: 795,
		velocity: 4.34555555555556,
		power: 896.108126966004,
		road: 7606.47000000001,
		acceleration: 0.0790740740740734
	},
	{
		id: 797,
		time: 796,
		velocity: 4.50194444444444,
		power: 1072.63290120388,
		road: 7610.91375000001,
		acceleration: 0.115648148148149
	},
	{
		id: 798,
		time: 797,
		velocity: 4.65722222222222,
		power: -135.041678345448,
		road: 7615.33004629631,
		acceleration: -0.170555555555556
	},
	{
		id: 799,
		time: 798,
		velocity: 3.83388888888889,
		power: -734.896490110217,
		road: 7619.49967592594,
		acceleration: -0.322777777777778
	},
	{
		id: 800,
		time: 799,
		velocity: 3.53361111111111,
		power: -1062.19473772676,
		road: 7623.2926851852,
		acceleration: -0.430462962962963
	},
	{
		id: 801,
		time: 800,
		velocity: 3.36583333333333,
		power: 39.8590692374783,
		road: 7626.80925925927,
		acceleration: -0.122407407407407
	},
	{
		id: 802,
		time: 801,
		velocity: 3.46666666666667,
		power: 660.274134144263,
		road: 7630.29722222223,
		acceleration: 0.065185185185185
	},
	{
		id: 803,
		time: 802,
		velocity: 3.72916666666667,
		power: 1138.2954859429,
		road: 7633.91606481483,
		acceleration: 0.196574074074074
	},
	{
		id: 804,
		time: 803,
		velocity: 3.95555555555556,
		power: 819.933536655991,
		road: 7637.68023148149,
		acceleration: 0.0940740740740744
	},
	{
		id: 805,
		time: 804,
		velocity: 3.74888888888889,
		power: -138.448787495296,
		road: 7641.40425925927,
		acceleration: -0.174351851851852
	},
	{
		id: 806,
		time: 805,
		velocity: 3.20611111111111,
		power: -654.247502569442,
		road: 7644.87472222223,
		acceleration: -0.332777777777777
	},
	{
		id: 807,
		time: 806,
		velocity: 2.95722222222222,
		power: -444.319372457116,
		road: 7648.03828703705,
		acceleration: -0.281018518518519
	},
	{
		id: 808,
		time: 807,
		velocity: 2.90583333333333,
		power: 400.926956623158,
		road: 7651.06486111112,
		acceleration: 0.00703703703703784
	},
	{
		id: 809,
		time: 808,
		velocity: 3.22722222222222,
		power: 844.600100464477,
		road: 7654.17175925927,
		acceleration: 0.15361111111111
	},
	{
		id: 810,
		time: 809,
		velocity: 3.41805555555556,
		power: 1077.35642324442,
		road: 7657.46125000001,
		acceleration: 0.211574074074075
	},
	{
		id: 811,
		time: 810,
		velocity: 3.54055555555556,
		power: 763.300023136382,
		road: 7660.90620370371,
		acceleration: 0.0993518518518517
	},
	{
		id: 812,
		time: 811,
		velocity: 3.52527777777778,
		power: 410.302191748719,
		road: 7664.39564814816,
		acceleration: -0.0103703703703704
	},
	{
		id: 813,
		time: 812,
		velocity: 3.38694444444444,
		power: 549.78741663363,
		road: 7667.89550925927,
		acceleration: 0.0312037037037034
	},
	{
		id: 814,
		time: 813,
		velocity: 3.63416666666667,
		power: 752.402090385788,
		road: 7671.45504629631,
		acceleration: 0.0881481481481483
	},
	{
		id: 815,
		time: 814,
		velocity: 3.78972222222222,
		power: 1063.82765093739,
		road: 7675.14305555557,
		acceleration: 0.168796296296296
	},
	{
		id: 816,
		time: 815,
		velocity: 3.89333333333333,
		power: 1216.60341951466,
		road: 7679.01310185186,
		acceleration: 0.195277777777778
	},
	{
		id: 817,
		time: 816,
		velocity: 4.22,
		power: 1347.62795638808,
		road: 7683.08662037038,
		acceleration: 0.211666666666666
	},
	{
		id: 818,
		time: 817,
		velocity: 4.42472222222222,
		power: 1090.27525655925,
		road: 7687.33240740742,
		acceleration: 0.132870370370371
	},
	{
		id: 819,
		time: 818,
		velocity: 4.29194444444444,
		power: 1002.99909335144,
		road: 7691.69657407409,
		acceleration: 0.103888888888889
	},
	{
		id: 820,
		time: 819,
		velocity: 4.53166666666667,
		power: 1113.06116865006,
		road: 7696.17421296297,
		acceleration: 0.123055555555555
	},
	{
		id: 821,
		time: 820,
		velocity: 4.79388888888889,
		power: 1799.6575717838,
		road: 7700.84629629631,
		acceleration: 0.265833333333333
	},
	{
		id: 822,
		time: 821,
		velocity: 5.08944444444444,
		power: 2041.8842675246,
		road: 7705.79773148149,
		acceleration: 0.292870370370371
	},
	{
		id: 823,
		time: 822,
		velocity: 5.41027777777778,
		power: 2348.38069628522,
		road: 7711.05898148149,
		acceleration: 0.32675925925926
	},
	{
		id: 824,
		time: 823,
		velocity: 5.77416666666667,
		power: 1843.84855941488,
		road: 7716.58680555557,
		acceleration: 0.206388888888888
	},
	{
		id: 825,
		time: 824,
		velocity: 5.70861111111111,
		power: 2219.91434500233,
		road: 7722.34750000001,
		acceleration: 0.259351851851851
	},
	{
		id: 826,
		time: 825,
		velocity: 6.18833333333333,
		power: 2565.5411552521,
		road: 7728.38731481483,
		acceleration: 0.298888888888889
	},
	{
		id: 827,
		time: 826,
		velocity: 6.67083333333333,
		power: 3696.78367759901,
		road: 7734.80421296297,
		acceleration: 0.455277777777778
	},
	{
		id: 828,
		time: 827,
		velocity: 7.07444444444444,
		power: 3982.41467810287,
		road: 7741.6763888889,
		acceleration: 0.455277777777777
	},
	{
		id: 829,
		time: 828,
		velocity: 7.55416666666667,
		power: 3956.29948514928,
		road: 7748.98208333334,
		acceleration: 0.411759259259259
	},
	{
		id: 830,
		time: 829,
		velocity: 7.90611111111111,
		power: 4755.93188188391,
		road: 7756.73532407408,
		acceleration: 0.483333333333334
	},
	{
		id: 831,
		time: 830,
		velocity: 8.52444444444444,
		power: 5506.23686580305,
		road: 7764.99736111112,
		acceleration: 0.534259259259259
	},
	{
		id: 832,
		time: 831,
		velocity: 9.15694444444445,
		power: 6195.50109423012,
		road: 7773.8100925926,
		acceleration: 0.56712962962963
	},
	{
		id: 833,
		time: 832,
		velocity: 9.6075,
		power: 5121.47047103248,
		road: 7783.10750000001,
		acceleration: 0.402222222222221
	},
	{
		id: 834,
		time: 833,
		velocity: 9.73111111111111,
		power: 4180.44797074668,
		road: 7792.7438425926,
		acceleration: 0.275648148148148
	},
	{
		id: 835,
		time: 834,
		velocity: 9.98388888888889,
		power: 3374.17338584621,
		road: 7802.6064351852,
		acceleration: 0.176851851851852
	},
	{
		id: 836,
		time: 835,
		velocity: 10.1380555555556,
		power: 3186.88565822502,
		road: 7812.63222222223,
		acceleration: 0.149537037037037
	},
	{
		id: 837,
		time: 836,
		velocity: 10.1797222222222,
		power: 1563.81937186291,
		road: 7822.72171296297,
		acceleration: -0.0221296296296298
	},
	{
		id: 838,
		time: 837,
		velocity: 9.9175,
		power: -133.469236960315,
		road: 7832.7013425926,
		acceleration: -0.197592592592592
	},
	{
		id: 839,
		time: 838,
		velocity: 9.54527777777778,
		power: -1801.31163196864,
		road: 7842.39444444446,
		acceleration: -0.375462962962963
	},
	{
		id: 840,
		time: 839,
		velocity: 9.05333333333333,
		power: -1464.03142882326,
		road: 7851.72921296297,
		acceleration: -0.341203703703703
	},
	{
		id: 841,
		time: 840,
		velocity: 8.89388888888889,
		power: -2100.71444800523,
		road: 7860.6838888889,
		acceleration: -0.418981481481481
	},
	{
		id: 842,
		time: 841,
		velocity: 8.28833333333333,
		power: -1864.03306062411,
		road: 7869.23023148149,
		acceleration: -0.397685185185185
	},
	{
		id: 843,
		time: 842,
		velocity: 7.86027777777778,
		power: -1408.62896087804,
		road: 7877.40467592594,
		acceleration: -0.34611111111111
	},
	{
		id: 844,
		time: 843,
		velocity: 7.85555555555556,
		power: -424.955105569828,
		road: 7885.29652777779,
		acceleration: -0.219074074074075
	},
	{
		id: 845,
		time: 844,
		velocity: 7.63111111111111,
		power: 256.016174925872,
		road: 7893.01578703705,
		acceleration: -0.126111111111112
	},
	{
		id: 846,
		time: 845,
		velocity: 7.48194444444444,
		power: 16.8353392844729,
		road: 7900.59328703705,
		acceleration: -0.157407407407407
	},
	{
		id: 847,
		time: 846,
		velocity: 7.38333333333333,
		power: 539.485153011966,
		road: 7908.05074074075,
		acceleration: -0.0826851851851842
	},
	{
		id: 848,
		time: 847,
		velocity: 7.38305555555556,
		power: 899.069337159484,
		road: 7915.45157407408,
		acceleration: -0.0305555555555559
	},
	{
		id: 849,
		time: 848,
		velocity: 7.39027777777778,
		power: 1494.70614017973,
		road: 7922.8639351852,
		acceleration: 0.0536111111111124
	},
	{
		id: 850,
		time: 849,
		velocity: 7.54416666666667,
		power: 2817.03603877025,
		road: 7930.41925925927,
		acceleration: 0.232314814814814
	},
	{
		id: 851,
		time: 850,
		velocity: 8.08,
		power: 3962.37451010926,
		road: 7938.27467592594,
		acceleration: 0.36787037037037
	},
	{
		id: 852,
		time: 851,
		velocity: 8.49388888888889,
		power: 3326.98515544655,
		road: 7946.44541666668,
		acceleration: 0.262777777777776
	},
	{
		id: 853,
		time: 852,
		velocity: 8.3325,
		power: 871.148875028042,
		road: 7954.71986111112,
		acceleration: -0.055370370370369
	},
	{
		id: 854,
		time: 853,
		velocity: 7.91388888888889,
		power: -1082.31811103678,
		road: 7962.81421296297,
		acceleration: -0.304814814814814
	},
	{
		id: 855,
		time: 854,
		velocity: 7.57944444444444,
		power: -2055.99706012846,
		road: 7970.53574074075,
		acceleration: -0.440833333333334
	},
	{
		id: 856,
		time: 855,
		velocity: 7.01,
		power: -1327.24489868915,
		road: 7977.86282407408,
		acceleration: -0.348055555555554
	},
	{
		id: 857,
		time: 856,
		velocity: 6.86972222222222,
		power: -639.170557887984,
		road: 7984.89046296297,
		acceleration: -0.250833333333334
	},
	{
		id: 858,
		time: 857,
		velocity: 6.82694444444444,
		power: 1190.47136675191,
		road: 7991.80601851853,
		acceleration: 0.0266666666666673
	},
	{
		id: 859,
		time: 858,
		velocity: 7.09,
		power: 2002.28348415045,
		road: 7998.80773148149,
		acceleration: 0.145648148148148
	},
	{
		id: 860,
		time: 859,
		velocity: 7.30666666666667,
		power: 3404.44082536558,
		road: 8006.05083333334,
		acceleration: 0.337129629629629
	},
	{
		id: 861,
		time: 860,
		velocity: 7.83833333333333,
		power: 3176.66247293226,
		road: 8013.60375000001,
		acceleration: 0.282500000000001
	},
	{
		id: 862,
		time: 861,
		velocity: 7.9375,
		power: 2498.20874657211,
		road: 8021.38583333334,
		acceleration: 0.175833333333333
	},
	{
		id: 863,
		time: 862,
		velocity: 7.83416666666667,
		power: 511.689434584728,
		road: 8029.20925925927,
		acceleration: -0.0931481481481482
	},
	{
		id: 864,
		time: 863,
		velocity: 7.55888888888889,
		power: 70.3931100243835,
		road: 8036.91050925927,
		acceleration: -0.151203703703704
	},
	{
		id: 865,
		time: 864,
		velocity: 7.48388888888889,
		power: 155.977217453207,
		road: 8044.46722222223,
		acceleration: -0.137870370370371
	},
	{
		id: 866,
		time: 865,
		velocity: 7.42055555555555,
		power: 630.137448517977,
		road: 8051.9200925926,
		acceleration: -0.0698148148148139
	},
	{
		id: 867,
		time: 866,
		velocity: 7.34944444444444,
		power: 374.97028613783,
		road: 8059.28583333334,
		acceleration: -0.104444444444445
	},
	{
		id: 868,
		time: 867,
		velocity: 7.17055555555556,
		power: 472.072889080057,
		road: 8066.55490740742,
		acceleration: -0.0888888888888886
	},
	{
		id: 869,
		time: 868,
		velocity: 7.15388888888889,
		power: 964.027306973591,
		road: 8073.7713888889,
		acceleration: -0.0162962962962965
	},
	{
		id: 870,
		time: 869,
		velocity: 7.30055555555556,
		power: 1736.4949696722,
		road: 8081.02699074075,
		acceleration: 0.0945370370370373
	},
	{
		id: 871,
		time: 870,
		velocity: 7.45416666666667,
		power: 2973.85169632417,
		road: 8088.46083333334,
		acceleration: 0.261944444444444
	},
	{
		id: 872,
		time: 871,
		velocity: 7.93972222222222,
		power: 3590.15433981591,
		road: 8096.18921296297,
		acceleration: 0.32712962962963
	},
	{
		id: 873,
		time: 872,
		velocity: 8.28194444444445,
		power: 3622.01577565786,
		road: 8104.23569444445,
		acceleration: 0.309074074074074
	},
	{
		id: 874,
		time: 873,
		velocity: 8.38138888888889,
		power: 4689.15713464227,
		road: 8112.64592592594,
		acceleration: 0.418425925925925
	},
	{
		id: 875,
		time: 874,
		velocity: 9.195,
		power: 5261.61946272776,
		road: 8121.49194444445,
		acceleration: 0.453148148148149
	},
	{
		id: 876,
		time: 875,
		velocity: 9.64138888888889,
		power: 6526.66429584459,
		road: 8130.8425925926,
		acceleration: 0.556111111111113
	},
	{
		id: 877,
		time: 876,
		velocity: 10.0497222222222,
		power: 5144.82654642491,
		road: 8140.65560185186,
		acceleration: 0.368611111111109
	},
	{
		id: 878,
		time: 877,
		velocity: 10.3008333333333,
		power: 3975.57114211041,
		road: 8150.76671296297,
		acceleration: 0.227592592592593
	},
	{
		id: 879,
		time: 878,
		velocity: 10.3241666666667,
		power: 2288.34473895784,
		road: 8161.01546296297,
		acceleration: 0.0476851851851858
	},
	{
		id: 880,
		time: 879,
		velocity: 10.1927777777778,
		power: 1587.01999169213,
		road: 8171.27583333334,
		acceleration: -0.0244444444444447
	},
	{
		id: 881,
		time: 880,
		velocity: 10.2275,
		power: 867.034262305525,
		road: 8181.47555555557,
		acceleration: -0.0968518518518522
	},
	{
		id: 882,
		time: 881,
		velocity: 10.0336111111111,
		power: 971.489356777061,
		road: 8191.58476851853,
		acceleration: -0.0841666666666683
	},
	{
		id: 883,
		time: 882,
		velocity: 9.94027777777778,
		power: -161.91561964601,
		road: 8201.55166666668,
		acceleration: -0.200462962962961
	},
	{
		id: 884,
		time: 883,
		velocity: 9.62611111111111,
		power: -43.4577909871868,
		road: 8211.32537037038,
		acceleration: -0.185925925925927
	},
	{
		id: 885,
		time: 884,
		velocity: 9.47583333333333,
		power: -382.699883255317,
		road: 8220.89560185186,
		acceleration: -0.221018518518518
	},
	{
		id: 886,
		time: 885,
		velocity: 9.27722222222222,
		power: -76.7207355157305,
		road: 8230.2625925926,
		acceleration: -0.185462962962962
	},
	{
		id: 887,
		time: 886,
		velocity: 9.06972222222222,
		power: -124.703065234557,
		road: 8239.44226851853,
		acceleration: -0.189166666666667
	},
	{
		id: 888,
		time: 887,
		velocity: 8.90833333333333,
		power: -82.9964849268279,
		road: 8248.43601851853,
		acceleration: -0.182685185185186
	},
	{
		id: 889,
		time: 888,
		velocity: 8.72916666666667,
		power: 115.0063111799,
		road: 8257.25962962964,
		acceleration: -0.157592592592593
	},
	{
		id: 890,
		time: 889,
		velocity: 8.59694444444444,
		power: 1459.47247418265,
		road: 8266.00680555556,
		acceleration: 0.00472222222222207
	},
	{
		id: 891,
		time: 890,
		velocity: 8.9225,
		power: 2375.46208798984,
		road: 8274.81245370371,
		acceleration: 0.112222222222224
	},
	{
		id: 892,
		time: 891,
		velocity: 9.06583333333333,
		power: 2897.8225958955,
		road: 8283.75805555557,
		acceleration: 0.167685185185183
	},
	{
		id: 893,
		time: 892,
		velocity: 9.1,
		power: 1526.12346859081,
		road: 8292.78953703705,
		acceleration: 0.00407407407407412
	},
	{
		id: 894,
		time: 893,
		velocity: 8.93472222222222,
		power: 256.707717324676,
		road: 8301.75175925927,
		acceleration: -0.142592592592592
	},
	{
		id: 895,
		time: 894,
		velocity: 8.63805555555555,
		power: 582.612937810643,
		road: 8310.59157407408,
		acceleration: -0.102222222222222
	},
	{
		id: 896,
		time: 895,
		velocity: 8.79333333333333,
		power: 1177.62152533908,
		road: 8319.36537037038,
		acceleration: -0.0298148148148147
	},
	{
		id: 897,
		time: 896,
		velocity: 8.84527777777778,
		power: 4532.12800154681,
		road: 8328.30425925927,
		acceleration: 0.359999999999999
	},
	{
		id: 898,
		time: 897,
		velocity: 9.71805555555556,
		power: 4925.0543601632,
		road: 8337.61277777779,
		acceleration: 0.379259259259261
	},
	{
		id: 899,
		time: 898,
		velocity: 9.93111111111111,
		power: 7579.90630919589,
		road: 8347.42546296297,
		acceleration: 0.629074074074072
	},
	{
		id: 900,
		time: 899,
		velocity: 10.7325,
		power: 5612.93548334582,
		road: 8357.74435185186,
		acceleration: 0.383333333333335
	},
	{
		id: 901,
		time: 900,
		velocity: 10.8680555555556,
		power: 4366.46984103012,
		road: 8368.37481481483,
		acceleration: 0.239814814814814
	},
	{
		id: 902,
		time: 901,
		velocity: 10.6505555555556,
		power: 604.54368640652,
		road: 8379.05898148149,
		acceleration: -0.132407407407406
	},
	{
		id: 903,
		time: 902,
		velocity: 10.3352777777778,
		power: -338.233650465003,
		road: 8389.56523148149,
		acceleration: -0.223425925925925
	},
	{
		id: 904,
		time: 903,
		velocity: 10.1977777777778,
		power: -923.480497606194,
		road: 8399.81916666668,
		acceleration: -0.281203703703707
	},
	{
		id: 905,
		time: 904,
		velocity: 9.80694444444444,
		power: -399.701461069172,
		road: 8409.81962962964,
		acceleration: -0.22574074074074
	},
	{
		id: 906,
		time: 905,
		velocity: 9.65805555555555,
		power: -701.669682077194,
		road: 8419.5789351852,
		acceleration: -0.256574074074074
	},
	{
		id: 907,
		time: 906,
		velocity: 9.42805555555556,
		power: -29.3864994727071,
		road: 8429.11898148149,
		acceleration: -0.181944444444444
	},
	{
		id: 908,
		time: 907,
		velocity: 9.26111111111111,
		power: 101.634570991115,
		road: 8438.48532407409,
		acceleration: -0.165462962962962
	},
	{
		id: 909,
		time: 908,
		velocity: 9.16166666666667,
		power: 1741.45284630594,
		road: 8447.77925925927,
		acceleration: 0.0206481481481475
	},
	{
		id: 910,
		time: 909,
		velocity: 9.49,
		power: 5954.23856271757,
		road: 8457.32166666668,
		acceleration: 0.476296296296297
	},
	{
		id: 911,
		time: 910,
		velocity: 10.69,
		power: 8463.65060774641,
		road: 8467.44814814816,
		acceleration: 0.691851851851851
	},
	{
		id: 912,
		time: 911,
		velocity: 11.2372222222222,
		power: 12737.0795577916,
		road: 8478.43101851853,
		acceleration: 1.02092592592592
	},
	{
		id: 913,
		time: 912,
		velocity: 12.5527777777778,
		power: 15420.0618308246,
		road: 8490.48981481483,
		acceleration: 1.13092592592593
	},
	{
		id: 914,
		time: 913,
		velocity: 14.0827777777778,
		power: 17736.8993719294,
		road: 8503.70412037038,
		acceleration: 1.18009259259259
	},
	{
		id: 915,
		time: 914,
		velocity: 14.7775,
		power: 14486.2740463638,
		road: 8517.92115740742,
		acceleration: 0.825370370370369
	},
	{
		id: 916,
		time: 915,
		velocity: 15.0288888888889,
		power: 6649.45325427116,
		road: 8532.66199074075,
		acceleration: 0.222222222222221
	},
	{
		id: 917,
		time: 916,
		velocity: 14.7494444444444,
		power: 3353.83943899335,
		road: 8547.50634259261,
		acceleration: -0.0151851851851852
	},
	{
		id: 918,
		time: 917,
		velocity: 14.7319444444444,
		power: 1956.69432577999,
		road: 8562.28708333335,
		acceleration: -0.112037037037037
	},
	{
		id: 919,
		time: 918,
		velocity: 14.6927777777778,
		power: 4244.55897772587,
		road: 8577.03736111112,
		acceleration: 0.051111111111112
	},
	{
		id: 920,
		time: 919,
		velocity: 14.9027777777778,
		power: 5463.16518337194,
		road: 8591.88000000001,
		acceleration: 0.133611111111112
	},
	{
		id: 921,
		time: 920,
		velocity: 15.1327777777778,
		power: 7347.00254318442,
		road: 8606.9176388889,
		acceleration: 0.256388888888887
	},
	{
		id: 922,
		time: 921,
		velocity: 15.4619444444444,
		power: 7438.84616162347,
		road: 8622.20837962964,
		acceleration: 0.249814814814814
	},
	{
		id: 923,
		time: 922,
		velocity: 15.6522222222222,
		power: 7342.56464462618,
		road: 8637.73962962964,
		acceleration: 0.231203703703706
	},
	{
		id: 924,
		time: 923,
		velocity: 15.8263888888889,
		power: 6898.73145728197,
		road: 8653.48212962964,
		acceleration: 0.191296296296297
	},
	{
		id: 925,
		time: 924,
		velocity: 16.0358333333333,
		power: 6935.6901091957,
		road: 8669.41273148149,
		acceleration: 0.184907407407406
	},
	{
		id: 926,
		time: 925,
		velocity: 16.2069444444444,
		power: 7218.77864436864,
		road: 8685.53300925927,
		acceleration: 0.194444444444443
	},
	{
		id: 927,
		time: 926,
		velocity: 16.4097222222222,
		power: 6437.8890394323,
		road: 8701.81875000001,
		acceleration: 0.136481481481486
	},
	{
		id: 928,
		time: 927,
		velocity: 16.4452777777778,
		power: 7103.12120826189,
		road: 8718.25875000001,
		acceleration: 0.172037037037036
	},
	{
		id: 929,
		time: 928,
		velocity: 16.7230555555556,
		power: 7711.10305102468,
		road: 8734.88560185186,
		acceleration: 0.201666666666664
	},
	{
		id: 930,
		time: 929,
		velocity: 17.0147222222222,
		power: 8693.69352637341,
		road: 8751.73916666668,
		acceleration: 0.251759259259259
	},
	{
		id: 931,
		time: 930,
		velocity: 17.2005555555556,
		power: 7856.37972609437,
		road: 8768.81324074075,
		acceleration: 0.189259259259259
	},
	{
		id: 932,
		time: 931,
		velocity: 17.2908333333333,
		power: 8086.14340820205,
		road: 8786.07898148149,
		acceleration: 0.194074074074074
	},
	{
		id: 933,
		time: 932,
		velocity: 17.5969444444444,
		power: 7221.19777040616,
		road: 8803.5089351852,
		acceleration: 0.134351851851854
	},
	{
		id: 934,
		time: 933,
		velocity: 17.6036111111111,
		power: 7675.30740860398,
		road: 8821.08351851853,
		acceleration: 0.154907407407407
	},
	{
		id: 935,
		time: 934,
		velocity: 17.7555555555556,
		power: 6360.66682077041,
		road: 8838.77157407409,
		acceleration: 0.0720370370370347
	},
	{
		id: 936,
		time: 935,
		velocity: 17.8130555555556,
		power: 8921.01766130708,
		road: 8856.60370370372,
		acceleration: 0.216111111111115
	},
	{
		id: 937,
		time: 936,
		velocity: 18.2519444444444,
		power: 8303.23556286429,
		road: 8874.62925925927,
		acceleration: 0.17074074074074
	},
	{
		id: 938,
		time: 937,
		velocity: 18.2677777777778,
		power: 5306.74963493765,
		road: 8892.73717592594,
		acceleration: -0.00601851851851976
	},
	{
		id: 939,
		time: 938,
		velocity: 17.795,
		power: 182.024840751098,
		road: 8910.69282407409,
		acceleration: -0.298518518518517
	},
	{
		id: 940,
		time: 939,
		velocity: 17.3563888888889,
		power: -2799.58587957882,
		road: 8928.2652314815,
		acceleration: -0.467962962962968
	},
	{
		id: 941,
		time: 940,
		velocity: 16.8638888888889,
		power: -3002.985506449,
		road: 8945.36574074075,
		acceleration: -0.47583333333333
	},
	{
		id: 942,
		time: 941,
		velocity: 16.3675,
		power: -1164.53552819928,
		road: 8962.04967592594,
		acceleration: -0.357314814814814
	},
	{
		id: 943,
		time: 942,
		velocity: 16.2844444444444,
		power: 463.807658934503,
		road: 8978.43041666668,
		acceleration: -0.249074074074073
	},
	{
		id: 944,
		time: 943,
		velocity: 16.1166666666667,
		power: 1584.79812708032,
		road: 8994.60046296298,
		acceleration: -0.172314814814815
	},
	{
		id: 945,
		time: 944,
		velocity: 15.8505555555556,
		power: 397.482929406116,
		road: 9010.56185185186,
		acceleration: -0.245000000000001
	},
	{
		id: 946,
		time: 945,
		velocity: 15.5494444444444,
		power: 796.565395541924,
		road: 9026.29375000001,
		acceleration: -0.213981481481483
	},
	{
		id: 947,
		time: 946,
		velocity: 15.4747222222222,
		power: 509.28807225185,
		road: 9041.80430555557,
		acceleration: -0.228703703703701
	},
	{
		id: 948,
		time: 947,
		velocity: 15.1644444444444,
		power: 1053.48609183884,
		road: 9057.10680555557,
		acceleration: -0.187407407407411
	},
	{
		id: 949,
		time: 948,
		velocity: 14.9872222222222,
		power: 192.636257708589,
		road: 9072.19439814816,
		acceleration: -0.242407407407406
	},
	{
		id: 950,
		time: 949,
		velocity: 14.7475,
		power: 668.688317634472,
		road: 9087.05833333334,
		acceleration: -0.204907407407406
	},
	{
		id: 951,
		time: 950,
		velocity: 14.5497222222222,
		power: -60.1897598412097,
		road: 9101.69356481482,
		acceleration: -0.2525
	},
	{
		id: 952,
		time: 951,
		velocity: 14.2297222222222,
		power: -1036.64482454674,
		road: 9116.04296296297,
		acceleration: -0.319166666666668
	},
	{
		id: 953,
		time: 952,
		velocity: 13.79,
		power: -2153.35709069776,
		road: 9130.03328703705,
		acceleration: -0.398981481481481
	},
	{
		id: 954,
		time: 953,
		velocity: 13.3527777777778,
		power: -1633.33536201302,
		road: 9143.64532407408,
		acceleration: -0.357592592592592
	},
	{
		id: 955,
		time: 954,
		velocity: 13.1569444444444,
		power: -1064.54689847227,
		road: 9156.92314814816,
		acceleration: -0.310833333333333
	},
	{
		id: 956,
		time: 955,
		velocity: 12.8575,
		power: 859.480032193629,
		road: 9169.96837962964,
		acceleration: -0.154351851851851
	},
	{
		id: 957,
		time: 956,
		velocity: 12.8897222222222,
		power: 2340.22099529476,
		road: 9182.9200925926,
		acceleration: -0.0326851851851853
	},
	{
		id: 958,
		time: 957,
		velocity: 13.0588888888889,
		power: 4959.38055041019,
		road: 9195.94342592593,
		acceleration: 0.175925925925926
	},
	{
		id: 959,
		time: 958,
		velocity: 13.3852777777778,
		power: 5488.55318015718,
		road: 9209.15930555556,
		acceleration: 0.209166666666665
	},
	{
		id: 960,
		time: 959,
		velocity: 13.5172222222222,
		power: 6506.94590621065,
		road: 9222.61819444445,
		acceleration: 0.276851851851852
	},
	{
		id: 961,
		time: 960,
		velocity: 13.8894444444444,
		power: 6872.11656761573,
		road: 9236.36041666667,
		acceleration: 0.289814814814815
	},
	{
		id: 962,
		time: 961,
		velocity: 14.2547222222222,
		power: 7314.87889290409,
		road: 9250.40101851853,
		acceleration: 0.306944444444445
	},
	{
		id: 963,
		time: 962,
		velocity: 14.4380555555556,
		power: 5780.55126218669,
		road: 9264.68569444445,
		acceleration: 0.181203703703703
	},
	{
		id: 964,
		time: 963,
		velocity: 14.4330555555556,
		power: 4918.28515299843,
		road: 9279.11694444445,
		acceleration: 0.111944444444445
	},
	{
		id: 965,
		time: 964,
		velocity: 14.5905555555556,
		power: 4241.68141203038,
		road: 9293.63398148149,
		acceleration: 0.0596296296296313
	},
	{
		id: 966,
		time: 965,
		velocity: 14.6169444444444,
		power: 3641.97554825568,
		road: 9308.18837962964,
		acceleration: 0.0150925925925911
	},
	{
		id: 967,
		time: 966,
		velocity: 14.4783333333333,
		power: 3612.52665864446,
		road: 9322.75657407408,
		acceleration: 0.0124999999999993
	},
	{
		id: 968,
		time: 967,
		velocity: 14.6280555555556,
		power: 3112.75143533171,
		road: 9337.31939814815,
		acceleration: -0.0232407407407393
	},
	{
		id: 969,
		time: 968,
		velocity: 14.5472222222222,
		power: 3502.6363307226,
		road: 9351.87314814815,
		acceleration: 0.00509259259259132
	},
	{
		id: 970,
		time: 969,
		velocity: 14.4936111111111,
		power: 2552.60240540492,
		road: 9366.39824074075,
		acceleration: -0.0624074074074059
	},
	{
		id: 971,
		time: 970,
		velocity: 14.4408333333333,
		power: 1976.68470866084,
		road: 9380.84125,
		acceleration: -0.101759259259259
	},
	{
		id: 972,
		time: 971,
		velocity: 14.2419444444444,
		power: -45.254067778371,
		road: 9395.11060185186,
		acceleration: -0.245555555555558
	},
	{
		id: 973,
		time: 972,
		velocity: 13.7569444444444,
		power: -1488.53962645342,
		road: 9409.08263888889,
		acceleration: -0.349074074074073
	},
	{
		id: 974,
		time: 973,
		velocity: 13.3936111111111,
		power: -2187.36248759478,
		road: 9422.6800462963,
		acceleration: -0.400185185185187
	},
	{
		id: 975,
		time: 974,
		velocity: 13.0413888888889,
		power: -1713.56031233591,
		road: 9435.89648148149,
		acceleration: -0.361759259259257
	},
	{
		id: 976,
		time: 975,
		velocity: 12.6716666666667,
		power: -1463.95940303957,
		road: 9448.76203703704,
		acceleration: -0.34
	},
	{
		id: 977,
		time: 976,
		velocity: 12.3736111111111,
		power: -2204.35522419282,
		road: 9461.25740740741,
		acceleration: -0.400370370370371
	},
	{
		id: 978,
		time: 977,
		velocity: 11.8402777777778,
		power: -1963.43905690308,
		road: 9473.3625462963,
		acceleration: -0.380092592592593
	},
	{
		id: 979,
		time: 978,
		velocity: 11.5313888888889,
		power: -2525.13868466745,
		road: 9485.06217592593,
		acceleration: -0.430925925925925
	},
	{
		id: 980,
		time: 979,
		velocity: 11.0808333333333,
		power: -2004.07265900784,
		road: 9496.35361111112,
		acceleration: -0.385462962962963
	},
	{
		id: 981,
		time: 980,
		velocity: 10.6838888888889,
		power: -1838.461719383,
		road: 9507.26666666667,
		acceleration: -0.371296296296297
	},
	{
		id: 982,
		time: 981,
		velocity: 10.4175,
		power: -1072.15616177786,
		road: 9517.84564814815,
		acceleration: -0.296851851851851
	},
	{
		id: 983,
		time: 982,
		velocity: 10.1902777777778,
		power: -1237.56151020542,
		road: 9528.11953703704,
		acceleration: -0.313333333333333
	},
	{
		id: 984,
		time: 983,
		velocity: 9.74388888888889,
		power: -1927.93851448914,
		road: 9538.04333333334,
		acceleration: -0.386851851851851
	},
	{
		id: 985,
		time: 984,
		velocity: 9.25694444444444,
		power: -2449.47932990462,
		road: 9547.54925925926,
		acceleration: -0.44888888888889
	},
	{
		id: 986,
		time: 985,
		velocity: 8.84361111111111,
		power: -2783.66921996307,
		road: 9556.58222222223,
		acceleration: -0.497037037037037
	},
	{
		id: 987,
		time: 986,
		velocity: 8.25277777777778,
		power: -1823.00046398305,
		road: 9565.17069444445,
		acceleration: -0.391944444444444
	},
	{
		id: 988,
		time: 987,
		velocity: 8.08111111111111,
		power: -2838.81435017457,
		road: 9573.29736111111,
		acceleration: -0.531666666666665
	},
	{
		id: 989,
		time: 988,
		velocity: 7.24861111111111,
		power: -1923.17124038809,
		road: 9580.94587962963,
		acceleration: -0.424629629629631
	},
	{
		id: 990,
		time: 989,
		velocity: 6.97888888888889,
		power: -539.753243987482,
		road: 9588.26453703704,
		acceleration: -0.235092592592594
	},
	{
		id: 991,
		time: 990,
		velocity: 7.37583333333333,
		power: 2581.91924196325,
		road: 9595.5725925926,
		acceleration: 0.213888888888889
	},
	{
		id: 992,
		time: 991,
		velocity: 7.89027777777778,
		power: 5299.16963310162,
		road: 9603.26898148149,
		acceleration: 0.562777777777777
	},
	{
		id: 993,
		time: 992,
		velocity: 8.66722222222222,
		power: 10894.24565868,
		road: 9611.83060185186,
		acceleration: 1.16768518518518
	},
	{
		id: 994,
		time: 993,
		velocity: 10.8788888888889,
		power: 14478.367112052,
		road: 9621.65819444445,
		acceleration: 1.36425925925926
	},
	{
		id: 995,
		time: 994,
		velocity: 11.9830555555556,
		power: 13100.9985546645,
		road: 9632.6925,
		acceleration: 1.04916666666667
	},
	{
		id: 996,
		time: 995,
		velocity: 11.8147222222222,
		power: 5671.45022483695,
		road: 9644.40291666667,
		acceleration: 0.303055555555556
	},
	{
		id: 997,
		time: 996,
		velocity: 11.7880555555556,
		power: 4829.67308105655,
		road: 9656.37226851852,
		acceleration: 0.214814814814815
	},
	{
		id: 998,
		time: 997,
		velocity: 12.6275,
		power: 7878.71512155888,
		road: 9668.67814814815,
		acceleration: 0.458240740740742
	},
	{
		id: 999,
		time: 998,
		velocity: 13.1894444444444,
		power: 13349.0374882846,
		road: 9681.64148148149,
		acceleration: 0.856666666666664
	},
	{
		id: 1000,
		time: 999,
		velocity: 14.3580555555556,
		power: 14333.3126724474,
		road: 9695.45875,
		acceleration: 0.851203703703703
	},
	{
		id: 1001,
		time: 1000,
		velocity: 15.1811111111111,
		power: 17894.4466389243,
		road: 9710.2112962963,
		acceleration: 1.01935185185185
	},
	{
		id: 1002,
		time: 1001,
		velocity: 16.2475,
		power: 15385.5642784949,
		road: 9725.85523148149,
		acceleration: 0.763425925925926
	},
	{
		id: 1003,
		time: 1002,
		velocity: 16.6483333333333,
		power: 11646.9796208687,
		road: 9742.11712962963,
		acceleration: 0.4725
	},
	{
		id: 1004,
		time: 1003,
		velocity: 16.5986111111111,
		power: 3516.69762849864,
		road: 9758.58675925926,
		acceleration: -0.0570370370370377
	},
	{
		id: 1005,
		time: 1004,
		velocity: 16.0763888888889,
		power: -815.419371237969,
		road: 9774.86328703704,
		acceleration: -0.329166666666666
	},
	{
		id: 1006,
		time: 1005,
		velocity: 15.6608333333333,
		power: -2885.92466275784,
		road: 9790.74537037038,
		acceleration: -0.459722222222222
	},
	{
		id: 1007,
		time: 1006,
		velocity: 15.2194444444444,
		power: -2115.49189216912,
		road: 9806.19495370371,
		acceleration: -0.405277777777778
	},
	{
		id: 1008,
		time: 1007,
		velocity: 14.8605555555556,
		power: -2614.4987646764,
		road: 9821.22347222223,
		acceleration: -0.436851851851852
	},
	{
		id: 1009,
		time: 1008,
		velocity: 14.3502777777778,
		power: -3381.39227821077,
		road: 9835.78851851852,
		acceleration: -0.490092592592591
	},
	{
		id: 1010,
		time: 1009,
		velocity: 13.7491666666667,
		power: -4002.21158674936,
		road: 9849.84000000001,
		acceleration: -0.537037037037038
	},
	{
		id: 1011,
		time: 1010,
		velocity: 13.2494444444444,
		power: -3533.9873257364,
		road: 9863.37083333334,
		acceleration: -0.504259259259259
	},
	{
		id: 1012,
		time: 1011,
		velocity: 12.8375,
		power: -1593.59515819491,
		road: 9876.47370370371,
		acceleration: -0.351666666666667
	},
	{
		id: 1013,
		time: 1012,
		velocity: 12.6941666666667,
		power: 1027.23634509626,
		road: 9889.33226851852,
		acceleration: -0.136944444444444
	},
	{
		id: 1014,
		time: 1013,
		velocity: 12.8386111111111,
		power: 3850.72891133175,
		road: 9902.16935185186,
		acceleration: 0.0939814814814799
	},
	{
		id: 1015,
		time: 1014,
		velocity: 13.1194444444444,
		power: 5545.94441042501,
		road: 9915.16564814816,
		acceleration: 0.224444444444444
	},
	{
		id: 1016,
		time: 1015,
		velocity: 13.3675,
		power: 6658.11143331671,
		road: 9928.42393518519,
		acceleration: 0.299537037037037
	},
	{
		id: 1017,
		time: 1016,
		velocity: 13.7372222222222,
		power: 7038.33018141989,
		road: 9941.98814814816,
		acceleration: 0.312314814814815
	},
	{
		id: 1018,
		time: 1017,
		velocity: 14.0563888888889,
		power: 7035.04933673923,
		road: 9955.85620370371,
		acceleration: 0.295370370370371
	},
	{
		id: 1019,
		time: 1018,
		velocity: 14.2536111111111,
		power: 5784.25187052169,
		road: 9969.96671296297,
		acceleration: 0.189537037037038
	},
	{
		id: 1020,
		time: 1019,
		velocity: 14.3058333333333,
		power: 5370.24128665961,
		road: 9984.2476388889,
		acceleration: 0.151296296296296
	},
	{
		id: 1021,
		time: 1020,
		velocity: 14.5102777777778,
		power: 3825.56859505239,
		road: 9998.62157407408,
		acceleration: 0.0347222222222214
	},
	{
		id: 1022,
		time: 1021,
		velocity: 14.3577777777778,
		power: 2409.24493249077,
		road: 10012.9788888889,
		acceleration: -0.0679629629629606
	},
	{
		id: 1023,
		time: 1022,
		velocity: 14.1019444444444,
		power: -937.547131782676,
		road: 10027.1472685185,
		acceleration: -0.309907407407408
	},
	{
		id: 1024,
		time: 1023,
		velocity: 13.5805555555556,
		power: -1750.92058353439,
		road: 10040.9767592593,
		acceleration: -0.367870370370373
	},
	{
		id: 1025,
		time: 1024,
		velocity: 13.2541666666667,
		power: -2719.90594277751,
		road: 10054.4016666667,
		acceleration: -0.441296296296295
	},
	{
		id: 1026,
		time: 1025,
		velocity: 12.7780555555556,
		power: -875.231650933103,
		road: 10067.4590277778,
		acceleration: -0.293796296296298
	},
	{
		id: 1027,
		time: 1026,
		velocity: 12.6991666666667,
		power: 1282.31924670009,
		road: 10080.3114814815,
		acceleration: -0.116018518518517
	},
	{
		id: 1028,
		time: 1027,
		velocity: 12.9061111111111,
		power: 4326.71654858172,
		road: 10093.1718518519,
		acceleration: 0.131851851851851
	},
	{
		id: 1029,
		time: 1028,
		velocity: 13.1736111111111,
		power: 6069.64759324663,
		road: 10106.2298611111,
		acceleration: 0.263425925925926
	},
	{
		id: 1030,
		time: 1029,
		velocity: 13.4894444444444,
		power: 6801.95903780535,
		road: 10119.5726851852,
		acceleration: 0.306203703703705
	},
	{
		id: 1031,
		time: 1030,
		velocity: 13.8247222222222,
		power: 5806.31912773923,
		road: 10133.17625,
		acceleration: 0.215277777777777
	},
	{
		id: 1032,
		time: 1031,
		velocity: 13.8194444444444,
		power: 4557.6652407111,
		road: 10146.9436574074,
		acceleration: 0.112407407407408
	},
	{
		id: 1033,
		time: 1032,
		velocity: 13.8266666666667,
		power: 2288.62147321963,
		road: 10160.7368055556,
		acceleration: -0.0609259259259272
	},
	{
		id: 1034,
		time: 1033,
		velocity: 13.6419444444444,
		power: 1860.21947609649,
		road: 10174.45375,
		acceleration: -0.0914814814814804
	},
	{
		id: 1035,
		time: 1034,
		velocity: 13.545,
		power: 2705.06390299525,
		road: 10188.1123611111,
		acceleration: -0.0251851851851868
	},
	{
		id: 1036,
		time: 1035,
		velocity: 13.7511111111111,
		power: 5188.99059722116,
		road: 10201.8394907407,
		acceleration: 0.162222222222224
	},
	{
		id: 1037,
		time: 1036,
		velocity: 14.1286111111111,
		power: 7089.47190754774,
		road: 10215.7950925926,
		acceleration: 0.294722222222221
	},
	{
		id: 1038,
		time: 1037,
		velocity: 14.4291666666667,
		power: 6682.54419787601,
		road: 10230.0231481482,
		acceleration: 0.250185185185186
	},
	{
		id: 1039,
		time: 1038,
		velocity: 14.5016666666667,
		power: 5709.84431600423,
		road: 10244.4608333333,
		acceleration: 0.169074074074071
	},
	{
		id: 1040,
		time: 1039,
		velocity: 14.6358333333333,
		power: 4259.96189361166,
		road: 10259.0128703704,
		acceleration: 0.0596296296296313
	},
	{
		id: 1041,
		time: 1040,
		velocity: 14.6080555555556,
		power: 4183.2886435726,
		road: 10273.6207407407,
		acceleration: 0.0520370370370369
	},
	{
		id: 1042,
		time: 1041,
		velocity: 14.6577777777778,
		power: 3444.30869025246,
		road: 10288.25375,
		acceleration: -0.00175925925925746
	},
	{
		id: 1043,
		time: 1042,
		velocity: 14.6305555555556,
		power: 3367.18000820227,
		road: 10302.8823148148,
		acceleration: -0.00712962962963104
	},
	{
		id: 1044,
		time: 1043,
		velocity: 14.5866666666667,
		power: 2889.81697642297,
		road: 10317.487037037,
		acceleration: -0.0405555555555566
	},
	{
		id: 1045,
		time: 1044,
		velocity: 14.5361111111111,
		power: 4527.1749225779,
		road: 10332.109537037,
		acceleration: 0.0761111111111106
	},
	{
		id: 1046,
		time: 1045,
		velocity: 14.8588888888889,
		power: 4237.47580685074,
		road: 10346.7965740741,
		acceleration: 0.0529629629629653
	},
	{
		id: 1047,
		time: 1046,
		velocity: 14.7455555555556,
		power: 4827.85577938148,
		road: 10361.5561574074,
		acceleration: 0.0921296296296266
	},
	{
		id: 1048,
		time: 1047,
		velocity: 14.8125,
		power: 3610.61935761264,
		road: 10376.3638888889,
		acceleration: 0.00416666666666821
	},
	{
		id: 1049,
		time: 1048,
		velocity: 14.8713888888889,
		power: 4506.04333118661,
		road: 10391.2067592593,
		acceleration: 0.0661111111111126
	},
	{
		id: 1050,
		time: 1049,
		velocity: 14.9438888888889,
		power: 5097.98298371374,
		road: 10406.1348611111,
		acceleration: 0.104351851851851
	},
	{
		id: 1051,
		time: 1050,
		velocity: 15.1255555555556,
		power: 4665.71359305864,
		road: 10421.150462963,
		acceleration: 0.0706481481481465
	},
	{
		id: 1052,
		time: 1051,
		velocity: 15.0833333333333,
		power: 4361.12818742891,
		road: 10436.225,
		acceleration: 0.0472222222222225
	},
	{
		id: 1053,
		time: 1052,
		velocity: 15.0855555555556,
		power: 2554.22403536419,
		road: 10451.2842592593,
		acceleration: -0.0777777777777757
	},
	{
		id: 1054,
		time: 1053,
		velocity: 14.8922222222222,
		power: 1074.95626353273,
		road: 10466.2157407407,
		acceleration: -0.177777777777777
	},
	{
		id: 1055,
		time: 1054,
		velocity: 14.55,
		power: -146.629140121742,
		road: 10480.9283796296,
		acceleration: -0.259907407407409
	},
	{
		id: 1056,
		time: 1055,
		velocity: 14.3058333333333,
		power: -641.684534194832,
		road: 10495.3653240741,
		acceleration: -0.291481481481481
	},
	{
		id: 1057,
		time: 1056,
		velocity: 14.0177777777778,
		power: -275.039352305873,
		road: 10509.5261111111,
		acceleration: -0.260833333333332
	},
	{
		id: 1058,
		time: 1057,
		velocity: 13.7675,
		power: 695.328889989982,
		road: 10523.4641203704,
		acceleration: -0.184722222222222
	},
	{
		id: 1059,
		time: 1058,
		velocity: 13.7516666666667,
		power: 1761.25905525916,
		road: 10537.2592592593,
		acceleration: -0.10101851851852
	},
	{
		id: 1060,
		time: 1059,
		velocity: 13.7147222222222,
		power: 2393.44171141686,
		road: 10550.9784722222,
		acceleration: -0.0508333333333315
	},
	{
		id: 1061,
		time: 1060,
		velocity: 13.615,
		power: 2688.33362815621,
		road: 10564.6587037037,
		acceleration: -0.0271296296296306
	},
	{
		id: 1062,
		time: 1061,
		velocity: 13.6702777777778,
		power: 2910.49651700454,
		road: 10578.3206018519,
		acceleration: -0.00953703703703646
	},
	{
		id: 1063,
		time: 1062,
		velocity: 13.6861111111111,
		power: 3635.12227954283,
		road: 10592.0004166667,
		acceleration: 0.0453703703703709
	},
	{
		id: 1064,
		time: 1063,
		velocity: 13.7511111111111,
		power: 3768.90217912381,
		road: 10605.7298148148,
		acceleration: 0.0537962962962961
	},
	{
		id: 1065,
		time: 1064,
		velocity: 13.8316666666667,
		power: 3993.4230175462,
		road: 10619.5204166667,
		acceleration: 0.0686111111111103
	},
	{
		id: 1066,
		time: 1065,
		velocity: 13.8919444444444,
		power: 3716.97503002074,
		road: 10633.3681018519,
		acceleration: 0.0455555555555556
	},
	{
		id: 1067,
		time: 1066,
		velocity: 13.8877777777778,
		power: 3633.72927712019,
		road: 10647.2574537037,
		acceleration: 0.0377777777777801
	},
	{
		id: 1068,
		time: 1067,
		velocity: 13.945,
		power: 4554.88839311949,
		road: 10661.2178703704,
		acceleration: 0.104351851851849
	},
	{
		id: 1069,
		time: 1068,
		velocity: 14.205,
		power: 4152.55873080487,
		road: 10675.2658796296,
		acceleration: 0.0708333333333329
	},
	{
		id: 1070,
		time: 1069,
		velocity: 14.1002777777778,
		power: 2852.45901629765,
		road: 10689.3359259259,
		acceleration: -0.0267592592592578
	},
	{
		id: 1071,
		time: 1070,
		velocity: 13.8647222222222,
		power: 1082.81206398911,
		road: 10703.3143518519,
		acceleration: -0.156481481481482
	},
	{
		id: 1072,
		time: 1071,
		velocity: 13.7355555555556,
		power: 1676.6211194434,
		road: 10717.1601851852,
		acceleration: -0.108703703703704
	},
	{
		id: 1073,
		time: 1072,
		velocity: 13.7741666666667,
		power: 3009.78503359297,
		road: 10730.9486574074,
		acceleration: -0.00601851851851976
	},
	{
		id: 1074,
		time: 1073,
		velocity: 13.8466666666667,
		power: 3439.86549444416,
		road: 10744.7472685185,
		acceleration: 0.026296296296298
	},
	{
		id: 1075,
		time: 1074,
		velocity: 13.8144444444444,
		power: -6035.05063065608,
		road: 10758.2093981482,
		acceleration: -0.699259259259261
	},
	{
		id: 1076,
		time: 1075,
		velocity: 11.6763888888889,
		power: -14191.170944554,
		road: 10770.6151851852,
		acceleration: -1.41342592592593
	},
	{
		id: 1077,
		time: 1076,
		velocity: 9.60638888888889,
		power: -21671.6173575231,
		road: 10781.1385648148,
		acceleration: -2.35138888888889
	},
	{
		id: 1078,
		time: 1077,
		velocity: 6.76027777777778,
		power: -15715.8331347327,
		road: 10789.404212963,
		acceleration: -2.16407407407407
	},
	{
		id: 1079,
		time: 1078,
		velocity: 5.18416666666667,
		power: -11827.646145965,
		road: 10795.4914351852,
		acceleration: -2.19277777777778
	},
	{
		id: 1080,
		time: 1079,
		velocity: 3.02805555555556,
		power: -6523.18647838729,
		road: 10799.5718981482,
		acceleration: -1.82074074074074
	},
	{
		id: 1081,
		time: 1080,
		velocity: 1.29805555555556,
		power: -3494.05367113725,
		road: 10801.877962963,
		acceleration: -1.72805555555556
	},
	{
		id: 1082,
		time: 1081,
		velocity: 0,
		power: -782.985742239163,
		road: 10802.8153240741,
		acceleration: -1.00935185185185
	},
	{
		id: 1083,
		time: 1082,
		velocity: 0,
		power: -62.5434289636127,
		road: 10803.0316666667,
		acceleration: -0.432685185185185
	},
	{
		id: 1084,
		time: 1083,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1085,
		time: 1084,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1086,
		time: 1085,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1087,
		time: 1086,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1088,
		time: 1087,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1089,
		time: 1088,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1090,
		time: 1089,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1091,
		time: 1090,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1092,
		time: 1091,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1093,
		time: 1092,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1094,
		time: 1093,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1095,
		time: 1094,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1096,
		time: 1095,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1097,
		time: 1096,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1098,
		time: 1097,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1099,
		time: 1098,
		velocity: 0,
		power: 0,
		road: 10803.0316666667,
		acceleration: 0
	},
	{
		id: 1100,
		time: 1099,
		velocity: 0,
		power: 52.5594266809793,
		road: 10803.1693518519,
		acceleration: 0.27537037037037
	},
	{
		id: 1101,
		time: 1100,
		velocity: 0.826111111111111,
		power: 389.256027750798,
		road: 10803.740462963,
		acceleration: 0.591481481481481
	},
	{
		id: 1102,
		time: 1101,
		velocity: 1.77444444444444,
		power: 1113.4370246693,
		road: 10805.0068055556,
		acceleration: 0.798981481481482
	},
	{
		id: 1103,
		time: 1102,
		velocity: 2.39694444444444,
		power: 2169.34555786582,
		road: 10807.1429166667,
		acceleration: 0.940555555555555
	},
	{
		id: 1104,
		time: 1103,
		velocity: 3.64777777777778,
		power: 3829.06842529719,
		road: 10810.3180092593,
		acceleration: 1.13740740740741
	},
	{
		id: 1105,
		time: 1104,
		velocity: 5.18666666666667,
		power: 5865.69469965631,
		road: 10814.6976851852,
		acceleration: 1.27175925925926
	},
	{
		id: 1106,
		time: 1105,
		velocity: 6.21222222222222,
		power: 5848.73991262323,
		road: 10820.2001851852,
		acceleration: 0.973888888888889
	},
	{
		id: 1107,
		time: 1106,
		velocity: 6.56944444444444,
		power: 7454.41771625772,
		road: 10826.7155092593,
		acceleration: 1.05175925925926
	},
	{
		id: 1108,
		time: 1107,
		velocity: 8.34194444444444,
		power: 9035.67418562036,
		road: 10834.3025462963,
		acceleration: 1.09166666666667
	},
	{
		id: 1109,
		time: 1108,
		velocity: 9.48722222222222,
		power: 16513.2947824731,
		road: 10843.3111111111,
		acceleration: 1.75138888888889
	},
	{
		id: 1110,
		time: 1109,
		velocity: 11.8236111111111,
		power: 13760.8890593666,
		road: 10853.7895833333,
		acceleration: 1.18842592592593
	},
	{
		id: 1111,
		time: 1110,
		velocity: 11.9072222222222,
		power: 12400.2913548886,
		road: 10865.3246296296,
		acceleration: 0.924722222222222
	},
	{
		id: 1112,
		time: 1111,
		velocity: 12.2613888888889,
		power: 7655.04526633019,
		road: 10877.544537037,
		acceleration: 0.445
	},
	{
		id: 1113,
		time: 1112,
		velocity: 13.1586111111111,
		power: 12539.461946091,
		road: 10890.3881944444,
		acceleration: 0.8025
	},
	{
		id: 1114,
		time: 1113,
		velocity: 14.3147222222222,
		power: 18610.8374277576,
		road: 10904.2199074074,
		acceleration: 1.17361111111111
	},
	{
		id: 1115,
		time: 1114,
		velocity: 15.7822222222222,
		power: 21086.5352385977,
		road: 10919.2453240741,
		acceleration: 1.21379629629629
	},
	{
		id: 1116,
		time: 1115,
		velocity: 16.8,
		power: 22928.1060584753,
		road: 10935.4781481482,
		acceleration: 1.20101851851852
	},
	{
		id: 1117,
		time: 1116,
		velocity: 17.9177777777778,
		power: 19221.9647732344,
		road: 10952.745462963,
		acceleration: 0.867962962962963
	},
	{
		id: 1118,
		time: 1117,
		velocity: 18.3861111111111,
		power: 13846.3264943509,
		road: 10970.6952314815,
		acceleration: 0.496944444444445
	},
	{
		id: 1119,
		time: 1118,
		velocity: 18.2908333333333,
		power: 5534.56083920029,
		road: 10988.8952777778,
		acceleration: 0.00361111111111256
	},
	{
		id: 1120,
		time: 1119,
		velocity: 17.9286111111111,
		power: 423.929833023568,
		road: 11006.9537962963,
		acceleration: -0.286666666666669
	},
	{
		id: 1121,
		time: 1120,
		velocity: 17.5261111111111,
		power: -1168.4046103429,
		road: 11024.6822685185,
		acceleration: -0.373425925925922
	},
	{
		id: 1122,
		time: 1121,
		velocity: 17.1705555555556,
		power: -547.843779878447,
		road: 11042.05875,
		acceleration: -0.330555555555559
	},
	{
		id: 1123,
		time: 1122,
		velocity: 16.9369444444444,
		power: -1240.89166095119,
		road: 11059.0864351852,
		acceleration: -0.367037037037036
	},
	{
		id: 1124,
		time: 1123,
		velocity: 16.425,
		power: -546.75512988867,
		road: 11075.7712962963,
		acceleration: -0.318611111111114
	},
	{
		id: 1125,
		time: 1124,
		velocity: 16.2147222222222,
		power: -1759.80168648908,
		road: 11092.1016203704,
		acceleration: -0.39046296296296
	},
	{
		id: 1126,
		time: 1125,
		velocity: 15.7655555555556,
		power: -183.275536481496,
		road: 11108.0949074074,
		acceleration: -0.283611111111112
	},
	{
		id: 1127,
		time: 1126,
		velocity: 15.5741666666667,
		power: -1404.12342234071,
		road: 11123.7665740741,
		acceleration: -0.359629629629627
	},
	{
		id: 1128,
		time: 1127,
		velocity: 15.1358333333333,
		power: -926.157408469095,
		road: 11139.0968518519,
		acceleration: -0.32314814814815
	},
	{
		id: 1129,
		time: 1128,
		velocity: 14.7961111111111,
		power: -1100.49763568616,
		road: 11154.1,
		acceleration: -0.331111111111113
	},
	{
		id: 1130,
		time: 1129,
		velocity: 14.5808333333333,
		power: -979.98986441388,
		road: 11168.7781944445,
		acceleration: -0.318796296296295
	},
	{
		id: 1131,
		time: 1130,
		velocity: 14.1794444444444,
		power: -532.775082907249,
		road: 11183.1556018519,
		acceleration: -0.282777777777778
	},
	{
		id: 1132,
		time: 1131,
		velocity: 13.9477777777778,
		power: 140.535828357512,
		road: 11197.2768981482,
		acceleration: -0.229444444444445
	},
	{
		id: 1133,
		time: 1132,
		velocity: 13.8925,
		power: 2078.14879743392,
		road: 11211.2426851852,
		acceleration: -0.0815740740740747
	},
	{
		id: 1134,
		time: 1133,
		velocity: 13.9347222222222,
		power: 3437.52230701189,
		road: 11225.1783796296,
		acceleration: 0.0213888888888896
	},
	{
		id: 1135,
		time: 1134,
		velocity: 14.0119444444444,
		power: 2679.37408890881,
		road: 11239.1070833333,
		acceleration: -0.0353703703703712
	},
	{
		id: 1136,
		time: 1135,
		velocity: 13.7863888888889,
		power: 3090.86306597946,
		road: 11253.0162037037,
		acceleration: -0.00379629629629541
	},
	{
		id: 1137,
		time: 1136,
		velocity: 13.9233333333333,
		power: 2755.90832963304,
		road: 11266.9091666667,
		acceleration: -0.0285185185185206
	},
	{
		id: 1138,
		time: 1137,
		velocity: 13.9263888888889,
		power: 3776.48875072951,
		road: 11280.8118981482,
		acceleration: 0.0480555555555569
	},
	{
		id: 1139,
		time: 1138,
		velocity: 13.9305555555556,
		power: 3542.40094890373,
		road: 11294.7531944445,
		acceleration: 0.0290740740740745
	},
	{
		id: 1140,
		time: 1139,
		velocity: 14.0105555555556,
		power: 2262.0475990046,
		road: 11308.6757407407,
		acceleration: -0.0665740740740723
	},
	{
		id: 1141,
		time: 1140,
		velocity: 13.7266666666667,
		power: 3010.35481433511,
		road: 11322.560462963,
		acceleration: -0.00907407407407668
	},
	{
		id: 1142,
		time: 1141,
		velocity: 13.9033333333333,
		power: 2474.98085013154,
		road: 11336.4163425926,
		acceleration: -0.0486111111111089
	},
	{
		id: 1143,
		time: 1142,
		velocity: 13.8647222222222,
		power: 3351.39505003146,
		road: 11350.2569907407,
		acceleration: 0.0181481481481462
	},
	{
		id: 1144,
		time: 1143,
		velocity: 13.7811111111111,
		power: 2278.52685573272,
		road: 11364.0755092593,
		acceleration: -0.0624074074074077
	},
	{
		id: 1145,
		time: 1144,
		velocity: 13.7161111111111,
		power: 2013.85900433423,
		road: 11377.8225462963,
		acceleration: -0.0805555555555557
	},
	{
		id: 1146,
		time: 1145,
		velocity: 13.6230555555556,
		power: 2179.26046829312,
		road: 11391.4963425926,
		acceleration: -0.0659259259259244
	},
	{
		id: 1147,
		time: 1146,
		velocity: 13.5833333333333,
		power: 2332.42574916516,
		road: 11405.1109259259,
		acceleration: -0.052500000000002
	},
	{
		id: 1148,
		time: 1147,
		velocity: 13.5586111111111,
		power: 3860.35028533408,
		road: 11418.7316666667,
		acceleration: 0.0648148148148167
	},
	{
		id: 1149,
		time: 1148,
		velocity: 13.8175,
		power: 3670.18112266044,
		road: 11432.4088888889,
		acceleration: 0.0481481481481492
	},
	{
		id: 1150,
		time: 1149,
		velocity: 13.7277777777778,
		power: 4999.72361270187,
		road: 11446.1830555556,
		acceleration: 0.145740740740742
	},
	{
		id: 1151,
		time: 1150,
		velocity: 13.9958333333333,
		power: 4093.70070508132,
		road: 11460.0664351852,
		acceleration: 0.0726851851851809
	},
	{
		id: 1152,
		time: 1151,
		velocity: 14.0355555555556,
		power: 4954.73414130827,
		road: 11474.0527777778,
		acceleration: 0.133240740740741
	},
	{
		id: 1153,
		time: 1152,
		velocity: 14.1275,
		power: 3657.19287280955,
		road: 11488.1223148148,
		acceleration: 0.0331481481481504
	},
	{
		id: 1154,
		time: 1153,
		velocity: 14.0952777777778,
		power: 3527.07010292625,
		road: 11502.2196759259,
		acceleration: 0.0224999999999991
	},
	{
		id: 1155,
		time: 1154,
		velocity: 14.1030555555556,
		power: 2759.22166533911,
		road: 11516.3111111111,
		acceleration: -0.0343518518518504
	},
	{
		id: 1156,
		time: 1155,
		velocity: 14.0244444444444,
		power: 2504.24398077604,
		road: 11530.3593518519,
		acceleration: -0.0520370370370387
	},
	{
		id: 1157,
		time: 1156,
		velocity: 13.9391666666667,
		power: 2083.78692145793,
		road: 11544.340787037,
		acceleration: -0.0815740740740729
	},
	{
		id: 1158,
		time: 1157,
		velocity: 13.8583333333333,
		power: 1811.80495053078,
		road: 11558.2316203704,
		acceleration: -0.0996296296296304
	},
	{
		id: 1159,
		time: 1158,
		velocity: 13.7255555555556,
		power: 2516.89041625508,
		road: 11572.050462963,
		acceleration: -0.0443518518518502
	},
	{
		id: 1160,
		time: 1159,
		velocity: 13.8061111111111,
		power: 2210.74335367249,
		road: 11585.8141203704,
		acceleration: -0.0660185185185185
	},
	{
		id: 1161,
		time: 1160,
		velocity: 13.6602777777778,
		power: 3988.1413115604,
		road: 11599.5793518519,
		acceleration: 0.0691666666666659
	},
	{
		id: 1162,
		time: 1161,
		velocity: 13.9330555555556,
		power: 3636.60508724644,
		road: 11613.3993981482,
		acceleration: 0.0404629629629643
	},
	{
		id: 1163,
		time: 1162,
		velocity: 13.9275,
		power: 4406.88529259613,
		road: 11627.2877314815,
		acceleration: 0.0961111111111119
	},
	{
		id: 1164,
		time: 1163,
		velocity: 13.9486111111111,
		power: 3285.99993407339,
		road: 11641.2290277778,
		acceleration: 0.00981481481481339
	},
	{
		id: 1165,
		time: 1164,
		velocity: 13.9625,
		power: 3439.70539752154,
		road: 11655.1856481482,
		acceleration: 0.0208333333333339
	},
	{
		id: 1166,
		time: 1165,
		velocity: 13.99,
		power: 3568.72830638802,
		road: 11669.1675,
		acceleration: 0.0296296296296266
	},
	{
		id: 1167,
		time: 1166,
		velocity: 14.0375,
		power: 3081.52305421482,
		road: 11683.1605555556,
		acceleration: -0.0072222222222198
	},
	{
		id: 1168,
		time: 1167,
		velocity: 13.9408333333333,
		power: 2947.74970314778,
		road: 11697.1415740741,
		acceleration: -0.0168518518518539
	},
	{
		id: 1169,
		time: 1168,
		velocity: 13.9394444444444,
		power: 2608.54218965581,
		road: 11711.0934722222,
		acceleration: -0.0413888888888874
	},
	{
		id: 1170,
		time: 1169,
		velocity: 13.9133333333333,
		power: 3633.57080695541,
		road: 11725.0425,
		acceleration: 0.0356481481481481
	},
	{
		id: 1171,
		time: 1170,
		velocity: 14.0477777777778,
		power: 3481.86296154667,
		road: 11739.0209722222,
		acceleration: 0.0232407407407429
	},
	{
		id: 1172,
		time: 1171,
		velocity: 14.0091666666667,
		power: 4628.27170400727,
		road: 11753.0643055556,
		acceleration: 0.106481481481481
	},
	{
		id: 1173,
		time: 1172,
		velocity: 14.2327777777778,
		power: 4124.28124632392,
		road: 11767.1937037037,
		acceleration: 0.0656481481481457
	},
	{
		id: 1174,
		time: 1173,
		velocity: 14.2447222222222,
		power: 3983.93336837867,
		road: 11781.3824537037,
		acceleration: 0.0530555555555559
	},
	{
		id: 1175,
		time: 1174,
		velocity: 14.1683333333333,
		power: 3405.99194983275,
		road: 11795.6024074074,
		acceleration: 0.00935185185185361
	},
	{
		id: 1176,
		time: 1175,
		velocity: 14.2608333333333,
		power: 3314.33821208591,
		road: 11809.8282407407,
		acceleration: 0.00240740740740542
	},
	{
		id: 1177,
		time: 1176,
		velocity: 14.2519444444444,
		power: 3989.46494981148,
		road: 11824.0808333333,
		acceleration: 0.051111111111112
	},
	{
		id: 1178,
		time: 1177,
		velocity: 14.3216666666667,
		power: 3605.86633310935,
		road: 11838.3698148148,
		acceleration: 0.0216666666666665
	},
	{
		id: 1179,
		time: 1178,
		velocity: 14.3258333333333,
		power: 3714.03518713069,
		road: 11852.6839814815,
		acceleration: 0.0287037037037017
	},
	{
		id: 1180,
		time: 1179,
		velocity: 14.3380555555556,
		power: 3270.15970824434,
		road: 11867.0104166667,
		acceleration: -0.00416666666666465
	},
	{
		id: 1181,
		time: 1180,
		velocity: 14.3091666666667,
		power: 2642.25182179795,
		road: 11881.3101388889,
		acceleration: -0.0492592592592587
	},
	{
		id: 1182,
		time: 1181,
		velocity: 14.1780555555556,
		power: 905.897364264985,
		road: 11895.4981944445,
		acceleration: -0.174074074074072
	},
	{
		id: 1183,
		time: 1182,
		velocity: 13.8158333333333,
		power: 255.29468078921,
		road: 11909.4898611111,
		acceleration: -0.218703703703707
	},
	{
		id: 1184,
		time: 1183,
		velocity: 13.6530555555556,
		power: 461.81687770158,
		road: 11923.2724537037,
		acceleration: -0.199444444444442
	},
	{
		id: 1185,
		time: 1184,
		velocity: 13.5797222222222,
		power: 2544.15710850758,
		road: 11936.9364814815,
		acceleration: -0.0376851851851878
	},
	{
		id: 1186,
		time: 1185,
		velocity: 13.7027777777778,
		power: 3357.88592197347,
		road: 11950.5941203704,
		acceleration: 0.0249074074074098
	},
	{
		id: 1187,
		time: 1186,
		velocity: 13.7277777777778,
		power: 3945.84553298316,
		road: 11964.2983333333,
		acceleration: 0.0682407407407375
	},
	{
		id: 1188,
		time: 1187,
		velocity: 13.7844444444444,
		power: 3372.65800312133,
		road: 11978.0481018519,
		acceleration: 0.0228703703703736
	},
	{
		id: 1189,
		time: 1188,
		velocity: 13.7713888888889,
		power: 2958.63679679262,
		road: 11991.8048611111,
		acceleration: -0.00888888888889028
	},
	{
		id: 1190,
		time: 1189,
		velocity: 13.7011111111111,
		power: 3131.57721369906,
		road: 12005.5593518519,
		acceleration: 0.00435185185185105
	},
	{
		id: 1191,
		time: 1190,
		velocity: 13.7975,
		power: 3644.84900425273,
		road: 12019.3373148148,
		acceleration: 0.0425925925925927
	},
	{
		id: 1192,
		time: 1191,
		velocity: 13.8991666666667,
		power: 5009.42354464006,
		road: 12033.2077314815,
		acceleration: 0.142314814814815
	},
	{
		id: 1193,
		time: 1192,
		velocity: 14.1280555555556,
		power: 5335.627016059,
		road: 12047.2293981482,
		acceleration: 0.160185185185185
	},
	{
		id: 1194,
		time: 1193,
		velocity: 14.2780555555556,
		power: 5071.62612620734,
		road: 12061.3982407407,
		acceleration: 0.134166666666667
	},
	{
		id: 1195,
		time: 1194,
		velocity: 14.3016666666667,
		power: 3848.8288884042,
		road: 12075.6544907407,
		acceleration: 0.0406481481481453
	},
	{
		id: 1196,
		time: 1195,
		velocity: 14.25,
		power: 2465.62474806469,
		road: 12089.9007407407,
		acceleration: -0.0606481481481467
	},
	{
		id: 1197,
		time: 1196,
		velocity: 14.0961111111111,
		power: 1851.06828760679,
		road: 12104.0648148148,
		acceleration: -0.103703703703703
	},
	{
		id: 1198,
		time: 1197,
		velocity: 13.9905555555556,
		power: 878.267260240426,
		road: 12118.0906481482,
		acceleration: -0.172777777777776
	},
	{
		id: 1199,
		time: 1198,
		velocity: 13.7316666666667,
		power: 1418.78967035621,
		road: 12131.9656481482,
		acceleration: -0.128888888888891
	},
	{
		id: 1200,
		time: 1199,
		velocity: 13.7094444444444,
		power: 2372.74597311997,
		road: 12145.7490740741,
		acceleration: -0.0542592592592612
	},
	{
		id: 1201,
		time: 1200,
		velocity: 13.8277777777778,
		power: 3988.45398487762,
		road: 12159.5394907407,
		acceleration: 0.0682407407407446
	},
	{
		id: 1202,
		time: 1201,
		velocity: 13.9363888888889,
		power: 4163.55080196081,
		road: 12173.4033796296,
		acceleration: 0.0787037037037024
	},
	{
		id: 1203,
		time: 1202,
		velocity: 13.9455555555556,
		power: 4573.59836630317,
		road: 12187.3595833333,
		acceleration: 0.105925925925927
	},
	{
		id: 1204,
		time: 1203,
		velocity: 14.1455555555556,
		power: 4890.04712196695,
		road: 12201.4311574074,
		acceleration: 0.124814814814815
	},
	{
		id: 1205,
		time: 1204,
		velocity: 14.3108333333333,
		power: 6678.15090966093,
		road: 12215.6893055556,
		acceleration: 0.248333333333331
	},
	{
		id: 1206,
		time: 1205,
		velocity: 14.6905555555556,
		power: 7426.9658091789,
		road: 12230.2160185185,
		acceleration: 0.288796296296297
	},
	{
		id: 1207,
		time: 1206,
		velocity: 15.0119444444444,
		power: 6362.05970412354,
		road: 12244.9873148148,
		acceleration: 0.20037037037037
	},
	{
		id: 1208,
		time: 1207,
		velocity: 14.9119444444444,
		power: 2667.74541231366,
		road: 12259.8270833333,
		acceleration: -0.0634259259259267
	},
	{
		id: 1209,
		time: 1208,
		velocity: 14.5002777777778,
		power: -600.023697250936,
		road: 12274.4893981482,
		acceleration: -0.291481481481481
	},
	{
		id: 1210,
		time: 1209,
		velocity: 14.1375,
		power: -2283.76088789013,
		road: 12288.8009722222,
		acceleration: -0.409999999999998
	},
	{
		id: 1211,
		time: 1210,
		velocity: 13.6819444444444,
		power: -1393.67419515887,
		road: 12302.736712963,
		acceleration: -0.341666666666667
	},
	{
		id: 1212,
		time: 1211,
		velocity: 13.4752777777778,
		power: -2144.11274073307,
		road: 12316.3032407408,
		acceleration: -0.396759259259259
	},
	{
		id: 1213,
		time: 1212,
		velocity: 12.9472222222222,
		power: -524.210850141198,
		road: 12329.5375462963,
		acceleration: -0.267685185185186
	},
	{
		id: 1214,
		time: 1213,
		velocity: 12.8788888888889,
		power: 517.275460966732,
		road: 12342.5474074074,
		acceleration: -0.181203703703705
	},
	{
		id: 1215,
		time: 1214,
		velocity: 12.9316666666667,
		power: 3699.35745368457,
		road: 12355.5051851852,
		acceleration: 0.0770370370370372
	},
	{
		id: 1216,
		time: 1215,
		velocity: 13.1783333333333,
		power: 5472.46530262379,
		road: 12368.6081481482,
		acceleration: 0.213333333333335
	},
	{
		id: 1217,
		time: 1216,
		velocity: 13.5188888888889,
		power: 5795.63095240093,
		road: 12381.9318518519,
		acceleration: 0.228148148148147
	},
	{
		id: 1218,
		time: 1217,
		velocity: 13.6161111111111,
		power: 5761.21419604147,
		road: 12395.4769444445,
		acceleration: 0.214629629629631
	},
	{
		id: 1219,
		time: 1218,
		velocity: 13.8222222222222,
		power: 4924.80195720518,
		road: 12409.200462963,
		acceleration: 0.142222222222223
	},
	{
		id: 1220,
		time: 1219,
		velocity: 13.9455555555556,
		power: 3901.65112453698,
		road: 12423.0252777778,
		acceleration: 0.060370370370368
	},
	{
		id: 1221,
		time: 1220,
		velocity: 13.7972222222222,
		power: 2260.65983660578,
		road: 12436.8483333333,
		acceleration: -0.0638888888888864
	},
	{
		id: 1222,
		time: 1221,
		velocity: 13.6305555555556,
		power: 52.3551985134133,
		road: 12450.525,
		acceleration: -0.228888888888889
	},
	{
		id: 1223,
		time: 1222,
		velocity: 13.2588888888889,
		power: -384.656043457088,
		road: 12463.9576388889,
		acceleration: -0.259166666666669
	},
	{
		id: 1224,
		time: 1223,
		velocity: 13.0197222222222,
		power: -28.500014351942,
		road: 12477.1468055556,
		acceleration: -0.227777777777776
	},
	{
		id: 1225,
		time: 1224,
		velocity: 12.9472222222222,
		power: 2545.71895208504,
		road: 12490.2123148148,
		acceleration: -0.019537037037038
	},
	{
		id: 1226,
		time: 1225,
		velocity: 13.2002777777778,
		power: 2800.70135964916,
		road: 12503.2686574074,
		acceleration: 0.00120370370370537
	},
	{
		id: 1227,
		time: 1226,
		velocity: 13.0233333333333,
		power: 3219.11249862054,
		road: 12516.3426851852,
		acceleration: 0.0341666666666658
	},
	{
		id: 1228,
		time: 1227,
		velocity: 13.0497222222222,
		power: 2072.39509750819,
		road: 12529.4050925926,
		acceleration: -0.0574074074074087
	},
	{
		id: 1229,
		time: 1228,
		velocity: 13.0280555555556,
		power: 2401.4729507645,
		road: 12542.4239351852,
		acceleration: -0.0297222222222207
	},
	{
		id: 1230,
		time: 1229,
		velocity: 12.9341666666667,
		power: 546.68826243162,
		road: 12555.3393518519,
		acceleration: -0.177129629629631
	},
	{
		id: 1231,
		time: 1230,
		velocity: 12.5183333333333,
		power: 1497.83724322552,
		road: 12568.1178703704,
		acceleration: -0.0966666666666658
	},
	{
		id: 1232,
		time: 1231,
		velocity: 12.7380555555556,
		power: 2206.15828785332,
		road: 12580.8297222222,
		acceleration: -0.0366666666666671
	},
	{
		id: 1233,
		time: 1232,
		velocity: 12.8241666666667,
		power: 5207.66665852737,
		road: 12593.6265740741,
		acceleration: 0.206666666666667
	},
	{
		id: 1234,
		time: 1233,
		velocity: 13.1383333333333,
		power: 5966.38622413623,
		road: 12606.6550925926,
		acceleration: 0.256666666666664
	},
	{
		id: 1235,
		time: 1234,
		velocity: 13.5080555555556,
		power: 7457.22652457934,
		road: 12619.9909722222,
		acceleration: 0.358055555555557
	},
	{
		id: 1236,
		time: 1235,
		velocity: 13.8983333333333,
		power: 7658.06651539477,
		road: 12633.6822222222,
		acceleration: 0.352685185185186
	},
	{
		id: 1237,
		time: 1236,
		velocity: 14.1963888888889,
		power: 7864.41035270957,
		road: 12647.72375,
		acceleration: 0.347870370370373
	},
	{
		id: 1238,
		time: 1237,
		velocity: 14.5516666666667,
		power: 7308.92880838589,
		road: 12662.0837962963,
		acceleration: 0.289166666666663
	},
	{
		id: 1239,
		time: 1238,
		velocity: 14.7658333333333,
		power: 5634.60935204088,
		road: 12676.6669907407,
		acceleration: 0.15712962962963
	},
	{
		id: 1240,
		time: 1239,
		velocity: 14.6677777777778,
		power: 3993.39643251181,
		road: 12691.3466666667,
		acceleration: 0.0358333333333345
	},
	{
		id: 1241,
		time: 1240,
		velocity: 14.6591666666667,
		power: 2905.27502642528,
		road: 12706.0234259259,
		acceleration: -0.0416666666666661
	},
	{
		id: 1242,
		time: 1241,
		velocity: 14.6408333333333,
		power: 3350.53182344247,
		road: 12720.6748148148,
		acceleration: -0.00907407407407135
	},
	{
		id: 1243,
		time: 1242,
		velocity: 14.6405555555556,
		power: 3051.64985889915,
		road: 12735.3067592593,
		acceleration: -0.0298148148148183
	},
	{
		id: 1244,
		time: 1243,
		velocity: 14.5697222222222,
		power: 2731.76619794472,
		road: 12749.8980555556,
		acceleration: -0.0514814814814812
	},
	{
		id: 1245,
		time: 1244,
		velocity: 14.4863888888889,
		power: 2555.04379815458,
		road: 12764.4323611111,
		acceleration: -0.0625
	},
	{
		id: 1246,
		time: 1245,
		velocity: 14.4530555555556,
		power: 3026.50656488265,
		road: 12778.9218518519,
		acceleration: -0.0271296296296288
	},
	{
		id: 1247,
		time: 1246,
		velocity: 14.4883333333333,
		power: 2877.00169229609,
		road: 12793.3793055556,
		acceleration: -0.036944444444444
	},
	{
		id: 1248,
		time: 1247,
		velocity: 14.3755555555556,
		power: 1452.14692683842,
		road: 12807.7492592593,
		acceleration: -0.138055555555558
	},
	{
		id: 1249,
		time: 1248,
		velocity: 14.0388888888889,
		power: -1021.35842956736,
		road: 12821.8922685185,
		acceleration: -0.31583333333333
	},
	{
		id: 1250,
		time: 1249,
		velocity: 13.5408333333333,
		power: -1783.70909869243,
		road: 12835.6922685185,
		acceleration: -0.370185185185187
	},
	{
		id: 1251,
		time: 1250,
		velocity: 13.265,
		power: -2036.62079019714,
		road: 12849.1131944445,
		acceleration: -0.387962962962963
	},
	{
		id: 1252,
		time: 1251,
		velocity: 12.875,
		power: -1237.98083435934,
		road: 12862.1786574074,
		acceleration: -0.322962962962963
	},
	{
		id: 1253,
		time: 1252,
		velocity: 12.5719444444444,
		power: -2837.12612241788,
		road: 12874.8563425926,
		acceleration: -0.452592592592593
	},
	{
		id: 1254,
		time: 1253,
		velocity: 11.9072222222222,
		power: -5754.90941384114,
		road: 12886.9533796296,
		acceleration: -0.708703703703701
	},
	{
		id: 1255,
		time: 1254,
		velocity: 10.7488888888889,
		power: -5369.08361361713,
		road: 12898.3486574074,
		acceleration: -0.694814814814816
	},
	{
		id: 1256,
		time: 1255,
		velocity: 10.4875,
		power: -3669.98802930887,
		road: 12909.1213888889,
		acceleration: -0.550277777777778
	},
	{
		id: 1257,
		time: 1256,
		velocity: 10.2563888888889,
		power: 1421.58692727254,
		road: 12919.5955555556,
		acceleration: -0.0468518518518533
	},
	{
		id: 1258,
		time: 1257,
		velocity: 10.6083333333333,
		power: 4032.35686237855,
		road: 12930.1515740741,
		acceleration: 0.210555555555556
	},
	{
		id: 1259,
		time: 1258,
		velocity: 11.1191666666667,
		power: 5201.96403995772,
		road: 12940.9684259259,
		acceleration: 0.311111111111112
	},
	{
		id: 1260,
		time: 1259,
		velocity: 11.1897222222222,
		power: 5462.19074644096,
		road: 12952.099537037,
		acceleration: 0.317407407407408
	},
	{
		id: 1261,
		time: 1260,
		velocity: 11.5605555555556,
		power: 3616.16232017545,
		road: 12963.45625,
		acceleration: 0.133796296296296
	},
	{
		id: 1262,
		time: 1261,
		velocity: 11.5205555555556,
		power: 1445.14322203286,
		road: 12974.8461111111,
		acceleration: -0.0675000000000026
	},
	{
		id: 1263,
		time: 1262,
		velocity: 10.9872222222222,
		power: -4729.53886765816,
		road: 12985.8793518519,
		acceleration: -0.64574074074074
	},
	{
		id: 1264,
		time: 1263,
		velocity: 9.62333333333333,
		power: -8783.16422357828,
		road: 12996.0433796296,
		acceleration: -1.09268518518518
	},
	{
		id: 1265,
		time: 1264,
		velocity: 8.2425,
		power: -10860.9484387364,
		road: 13004.9334259259,
		acceleration: -1.45527777777778
	},
	{
		id: 1266,
		time: 1265,
		velocity: 6.62138888888889,
		power: -8922.44355056008,
		road: 13012.3872685185,
		acceleration: -1.41712962962963
	},
	{
		id: 1267,
		time: 1266,
		velocity: 5.37194444444444,
		power: -7468.02084698485,
		road: 13018.4058796296,
		acceleration: -1.45333333333333
	},
	{
		id: 1268,
		time: 1267,
		velocity: 3.8825,
		power: -4357.61664441576,
		road: 13023.1436111111,
		acceleration: -1.10842592592593
	},
	{
		id: 1269,
		time: 1268,
		velocity: 3.29611111111111,
		power: -2150.39484583169,
		road: 13026.9627777778,
		acceleration: -0.728703703703703
	},
	{
		id: 1270,
		time: 1269,
		velocity: 3.18583333333333,
		power: -980.058655634152,
		road: 13030.1910185185,
		acceleration: -0.453148148148149
	},
	{
		id: 1271,
		time: 1270,
		velocity: 2.52305555555556,
		power: -1050.87773554214,
		road: 13032.9242592593,
		acceleration: -0.536851851851852
	},
	{
		id: 1272,
		time: 1271,
		velocity: 1.68555555555556,
		power: -827.074914681634,
		road: 13035.1259722222,
		acceleration: -0.526203703703704
	},
	{
		id: 1273,
		time: 1272,
		velocity: 1.60722222222222,
		power: -333.14889029272,
		road: 13036.9010185185,
		acceleration: -0.32712962962963
	},
	{
		id: 1274,
		time: 1273,
		velocity: 1.54166666666667,
		power: 85.5573628170142,
		road: 13038.476712963,
		acceleration: -0.071574074074074
	},
	{
		id: 1275,
		time: 1274,
		velocity: 1.47083333333333,
		power: 435.128411714414,
		road: 13040.0940277778,
		acceleration: 0.154814814814815
	},
	{
		id: 1276,
		time: 1275,
		velocity: 2.07166666666667,
		power: 448.323212473706,
		road: 13041.8581481482,
		acceleration: 0.138796296296296
	},
	{
		id: 1277,
		time: 1276,
		velocity: 1.95805555555556,
		power: 573.052777518007,
		road: 13043.7837962963,
		acceleration: 0.184259259259259
	},
	{
		id: 1278,
		time: 1277,
		velocity: 2.02361111111111,
		power: 262.734080093494,
		road: 13045.8052314815,
		acceleration: 0.00731481481481477
	},
	{
		id: 1279,
		time: 1278,
		velocity: 2.09361111111111,
		power: 322.767102177293,
		road: 13047.8487037037,
		acceleration: 0.0367592592592589
	},
	{
		id: 1280,
		time: 1279,
		velocity: 2.06833333333333,
		power: 217.188039871428,
		road: 13049.9014351852,
		acceleration: -0.0182407407407403
	},
	{
		id: 1281,
		time: 1280,
		velocity: 1.96888888888889,
		power: 179.859544185041,
		road: 13051.9269907407,
		acceleration: -0.0361111111111114
	},
	{
		id: 1282,
		time: 1281,
		velocity: 1.98527777777778,
		power: 211.922000316673,
		road: 13053.9255555556,
		acceleration: -0.01787037037037
	},
	{
		id: 1283,
		time: 1282,
		velocity: 2.01472222222222,
		power: 303.057946995701,
		road: 13055.9300462963,
		acceleration: 0.0297222222222218
	},
	{
		id: 1284,
		time: 1283,
		velocity: 2.05805555555556,
		power: 294.694870848862,
		road: 13057.9610185185,
		acceleration: 0.0232407407407407
	},
	{
		id: 1285,
		time: 1284,
		velocity: 2.055,
		power: 241.229373634196,
		road: 13060.0010648148,
		acceleration: -0.00509259259259265
	},
	{
		id: 1286,
		time: 1285,
		velocity: 1.99944444444444,
		power: 185.653176252697,
		road: 13062.0221296296,
		acceleration: -0.0328703703703699
	},
	{
		id: 1287,
		time: 1286,
		velocity: 1.95944444444444,
		power: 310.74819194688,
		road: 13064.042962963,
		acceleration: 0.032407407407407
	},
	{
		id: 1288,
		time: 1287,
		velocity: 2.15222222222222,
		power: 279.441646527822,
		road: 13066.0871759259,
		acceleration: 0.0143518518518517
	},
	{
		id: 1289,
		time: 1288,
		velocity: 2.0425,
		power: 329.50492929986,
		road: 13068.1575462963,
		acceleration: 0.037962962962963
	},
	{
		id: 1290,
		time: 1289,
		velocity: 2.07333333333333,
		power: 187.297144037817,
		road: 13070.2296296296,
		acceleration: -0.0345370370370364
	},
	{
		id: 1291,
		time: 1290,
		velocity: 2.04861111111111,
		power: 252.968328650687,
		road: 13072.2844444445,
		acceleration: -4.44089209850063E-16
	},
	{
		id: 1292,
		time: 1291,
		velocity: 2.0425,
		power: 204.787706743527,
		road: 13074.3272222222,
		acceleration: -0.0240740740740737
	},
	{
		id: 1293,
		time: 1292,
		velocity: 2.00111111111111,
		power: 237.018853655772,
		road: 13076.3547222222,
		acceleration: -0.0064814814814822
	},
	{
		id: 1294,
		time: 1293,
		velocity: 2.02916666666667,
		power: 188.720251056722,
		road: 13078.3636574074,
		acceleration: -0.0306481481481478
	},
	{
		id: 1295,
		time: 1294,
		velocity: 1.95055555555556,
		power: 107.188405315668,
		road: 13080.3213425926,
		acceleration: -0.0718518518518518
	},
	{
		id: 1296,
		time: 1295,
		velocity: 1.78555555555556,
		power: 39.6107695035412,
		road: 13082.1895833333,
		acceleration: -0.107037037037037
	},
	{
		id: 1297,
		time: 1296,
		velocity: 1.70805555555556,
		power: 131.426389538525,
		road: 13083.9784259259,
		acceleration: -0.0517592592592595
	},
	{
		id: 1298,
		time: 1297,
		velocity: 1.79527777777778,
		power: 512.012226110995,
		road: 13085.8230555556,
		acceleration: 0.163333333333333
	},
	{
		id: 1299,
		time: 1298,
		velocity: 2.27555555555556,
		power: 1033.47366487064,
		road: 13087.9415277778,
		acceleration: 0.384351851851852
	},
	{
		id: 1300,
		time: 1299,
		velocity: 2.86111111111111,
		power: 1355.18221574738,
		road: 13090.469212963,
		acceleration: 0.434074074074074
	},
	{
		id: 1301,
		time: 1300,
		velocity: 3.0975,
		power: 1268.41379977297,
		road: 13093.3776388889,
		acceleration: 0.327407407407407
	},
	{
		id: 1302,
		time: 1301,
		velocity: 3.25777777777778,
		power: 807.91135824933,
		road: 13096.5187962963,
		acceleration: 0.138055555555556
	},
	{
		id: 1303,
		time: 1302,
		velocity: 3.27527777777778,
		power: 624.470968866096,
		road: 13099.7637037037,
		acceleration: 0.0694444444444446
	},
	{
		id: 1304,
		time: 1303,
		velocity: 3.30583333333333,
		power: 245.008297617777,
		road: 13103.0163425926,
		acceleration: -0.0539814814814816
	},
	{
		id: 1305,
		time: 1304,
		velocity: 3.09583333333333,
		power: -135.145682359541,
		road: 13106.1528240741,
		acceleration: -0.178333333333333
	},
	{
		id: 1306,
		time: 1305,
		velocity: 2.74027777777778,
		power: -158.293725311122,
		road: 13109.1057407407,
		acceleration: -0.188796296296296
	},
	{
		id: 1307,
		time: 1306,
		velocity: 2.73944444444444,
		power: 261.085063442705,
		road: 13111.946712963,
		acceleration: -0.0350925925925925
	},
	{
		id: 1308,
		time: 1307,
		velocity: 2.99055555555556,
		power: 568.238483032102,
		road: 13114.80875,
		acceleration: 0.0772222222222223
	},
	{
		id: 1309,
		time: 1308,
		velocity: 2.97194444444444,
		power: 666.65898733823,
		road: 13117.7621759259,
		acceleration: 0.105555555555556
	},
	{
		id: 1310,
		time: 1309,
		velocity: 3.05611111111111,
		power: 512.268257350118,
		road: 13120.7912037037,
		acceleration: 0.0456481481481479
	},
	{
		id: 1311,
		time: 1310,
		velocity: 3.1275,
		power: 427.893189600886,
		road: 13123.8504166667,
		acceleration: 0.0147222222222223
	},
	{
		id: 1312,
		time: 1311,
		velocity: 3.01611111111111,
		power: -81.5735879602779,
		road: 13126.8363888889,
		acceleration: -0.161203703703704
	},
	{
		id: 1313,
		time: 1312,
		velocity: 2.5725,
		power: -285.55978961385,
		road: 13129.6218518519,
		acceleration: -0.239814814814815
	},
	{
		id: 1314,
		time: 1313,
		velocity: 2.40805555555556,
		power: -224.84780152857,
		road: 13132.175462963,
		acceleration: -0.223888888888889
	},
	{
		id: 1315,
		time: 1314,
		velocity: 2.34444444444444,
		power: 26.8991190355969,
		road: 13134.5577777778,
		acceleration: -0.118703703703703
	},
	{
		id: 1316,
		time: 1315,
		velocity: 2.21638888888889,
		power: 104.163951405317,
		road: 13136.8396296296,
		acceleration: -0.0822222222222222
	},
	{
		id: 1317,
		time: 1316,
		velocity: 2.16138888888889,
		power: 192.979473501204,
		road: 13139.0610648148,
		acceleration: -0.0386111111111114
	},
	{
		id: 1318,
		time: 1317,
		velocity: 2.22861111111111,
		power: 208.079331150612,
		road: 13141.248287037,
		acceleration: -0.0298148148148147
	},
	{
		id: 1319,
		time: 1318,
		velocity: 2.12694444444444,
		power: 213.057713241734,
		road: 13143.4075925926,
		acceleration: -0.0260185185185189
	},
	{
		id: 1320,
		time: 1319,
		velocity: 2.08333333333333,
		power: 151.946003297817,
		road: 13145.526712963,
		acceleration: -0.0543518518518518
	},
	{
		id: 1321,
		time: 1320,
		velocity: 2.06555555555556,
		power: 419.307800758431,
		road: 13147.6574074074,
		acceleration: 0.0775000000000001
	},
	{
		id: 1322,
		time: 1321,
		velocity: 2.35944444444444,
		power: 682.913506615392,
		road: 13149.9207407408,
		acceleration: 0.187777777777778
	},
	{
		id: 1323,
		time: 1322,
		velocity: 2.64666666666667,
		power: 1019.83904294058,
		road: 13152.4269444445,
		acceleration: 0.297962962962963
	},
	{
		id: 1324,
		time: 1323,
		velocity: 2.95944444444444,
		power: 1081.94715616162,
		road: 13155.2203240741,
		acceleration: 0.276388888888889
	},
	{
		id: 1325,
		time: 1324,
		velocity: 3.18861111111111,
		power: 1094.01246233525,
		road: 13158.2743055556,
		acceleration: 0.244814814814815
	},
	{
		id: 1326,
		time: 1325,
		velocity: 3.38111111111111,
		power: 942.422318461551,
		road: 13161.5362037037,
		acceleration: 0.171018518518519
	},
	{
		id: 1327,
		time: 1326,
		velocity: 3.4725,
		power: 768.181288294385,
		road: 13164.9356944445,
		acceleration: 0.104166666666667
	},
	{
		id: 1328,
		time: 1327,
		velocity: 3.50111111111111,
		power: 512.689648157285,
		road: 13168.3981944445,
		acceleration: 0.0218518518518516
	},
	{
		id: 1329,
		time: 1328,
		velocity: 3.44666666666667,
		power: 134.091414767621,
		road: 13171.8252314815,
		acceleration: -0.0927777777777772
	},
	{
		id: 1330,
		time: 1329,
		velocity: 3.19416666666667,
		power: 67.1990298706601,
		road: 13175.1497222222,
		acceleration: -0.112314814814815
	},
	{
		id: 1331,
		time: 1330,
		velocity: 3.16416666666667,
		power: 266.295952064279,
		road: 13178.3946296296,
		acceleration: -0.0468518518518515
	},
	{
		id: 1332,
		time: 1331,
		velocity: 3.30611111111111,
		power: 531.131567015319,
		road: 13181.635787037,
		acceleration: 0.0393518518518512
	},
	{
		id: 1333,
		time: 1332,
		velocity: 3.31222222222222,
		power: 737.589539932545,
		road: 13184.9471759259,
		acceleration: 0.101111111111112
	},
	{
		id: 1334,
		time: 1333,
		velocity: 3.4675,
		power: 937.070268972273,
		road: 13188.3856481482,
		acceleration: 0.153055555555556
	},
	{
		id: 1335,
		time: 1334,
		velocity: 3.76527777777778,
		power: 815.167145027143,
		road: 13191.9537037037,
		acceleration: 0.106111111111111
	},
	{
		id: 1336,
		time: 1335,
		velocity: 3.63055555555556,
		power: 984.444276510097,
		road: 13195.6476388889,
		acceleration: 0.145648148148148
	},
	{
		id: 1337,
		time: 1336,
		velocity: 3.90444444444444,
		power: 704.715889665894,
		road: 13199.4443981482,
		acceleration: 0.0599999999999996
	},
	{
		id: 1338,
		time: 1337,
		velocity: 3.94527777777778,
		power: 836.461673250776,
		road: 13203.3169907408,
		acceleration: 0.0916666666666668
	},
	{
		id: 1339,
		time: 1338,
		velocity: 3.90555555555556,
		power: 612.880367317194,
		road: 13207.2494444445,
		acceleration: 0.0280555555555555
	},
	{
		id: 1340,
		time: 1339,
		velocity: 3.98861111111111,
		power: 592.459964340822,
		road: 13211.2066666667,
		acceleration: 0.0214814814814814
	},
	{
		id: 1341,
		time: 1340,
		velocity: 4.00972222222222,
		power: 214.681478603376,
		road: 13215.1353703704,
		acceleration: -0.0785185185185182
	},
	{
		id: 1342,
		time: 1341,
		velocity: 3.67,
		power: -505.998905725612,
		road: 13218.8861111111,
		acceleration: -0.277407407407408
	},
	{
		id: 1343,
		time: 1342,
		velocity: 3.15638888888889,
		power: -564.081208001096,
		road: 13222.3451851852,
		acceleration: -0.305925925925926
	},
	{
		id: 1344,
		time: 1343,
		velocity: 3.09194444444444,
		power: -66.3351584698963,
		road: 13225.5738425926,
		acceleration: -0.154907407407407
	},
	{
		id: 1345,
		time: 1344,
		velocity: 3.20527777777778,
		power: 517.97444432832,
		road: 13228.7445833333,
		acceleration: 0.0390740740740738
	},
	{
		id: 1346,
		time: 1345,
		velocity: 3.27361111111111,
		power: 758.662224362566,
		road: 13231.9912962963,
		acceleration: 0.11287037037037
	},
	{
		id: 1347,
		time: 1346,
		velocity: 3.43055555555556,
		power: 731.042887709348,
		road: 13235.3425,
		acceleration: 0.0961111111111106
	},
	{
		id: 1348,
		time: 1347,
		velocity: 3.49361111111111,
		power: 779.316236916367,
		road: 13238.7936574074,
		acceleration: 0.103796296296296
	},
	{
		id: 1349,
		time: 1348,
		velocity: 3.585,
		power: 949.319040482728,
		road: 13242.3692592593,
		acceleration: 0.145092592592593
	},
	{
		id: 1350,
		time: 1349,
		velocity: 3.86583333333333,
		power: 938.757941709355,
		road: 13246.082962963,
		acceleration: 0.131111111111111
	},
	{
		id: 1351,
		time: 1350,
		velocity: 3.88694444444444,
		power: 1866.60356576214,
		road: 13250.0423611111,
		acceleration: 0.360277777777777
	},
	{
		id: 1352,
		time: 1351,
		velocity: 4.66583333333333,
		power: 2252.2687768204,
		road: 13254.3858796296,
		acceleration: 0.407962962962964
	},
	{
		id: 1353,
		time: 1352,
		velocity: 5.08972222222222,
		power: 2454.31637716837,
		road: 13259.1353240741,
		acceleration: 0.403888888888888
	},
	{
		id: 1354,
		time: 1353,
		velocity: 5.09861111111111,
		power: 1220.21213856568,
		road: 13264.1441666667,
		acceleration: 0.114907407407408
	},
	{
		id: 1355,
		time: 1354,
		velocity: 5.01055555555556,
		power: 1702.98670401606,
		road: 13269.3126388889,
		acceleration: 0.204351851851852
	},
	{
		id: 1356,
		time: 1355,
		velocity: 5.70277777777778,
		power: 2998.80823373982,
		road: 13274.7987037037,
		acceleration: 0.430833333333334
	},
	{
		id: 1357,
		time: 1356,
		velocity: 6.39111111111111,
		power: 3708.45475408071,
		road: 13280.7540277778,
		acceleration: 0.507685185185185
	},
	{
		id: 1358,
		time: 1357,
		velocity: 6.53361111111111,
		power: 1969.68034145178,
		road: 13287.0527777778,
		acceleration: 0.179166666666665
	},
	{
		id: 1359,
		time: 1358,
		velocity: 6.24027777777778,
		power: -345.789919321931,
		road: 13293.3373611111,
		acceleration: -0.2075
	},
	{
		id: 1360,
		time: 1359,
		velocity: 5.76861111111111,
		power: -1573.58740533997,
		road: 13299.305787037,
		acceleration: -0.424814814814813
	},
	{
		id: 1361,
		time: 1360,
		velocity: 5.25916666666667,
		power: -1408.43011464137,
		road: 13304.8559259259,
		acceleration: -0.411759259259259
	},
	{
		id: 1362,
		time: 1361,
		velocity: 5.005,
		power: -857.164262933016,
		road: 13310.0419444445,
		acceleration: -0.316481481481482
	},
	{
		id: 1363,
		time: 1362,
		velocity: 4.81916666666667,
		power: -1386.76072280997,
		road: 13314.8476388889,
		acceleration: -0.444166666666667
	},
	{
		id: 1364,
		time: 1363,
		velocity: 3.92666666666667,
		power: -2040.69459782663,
		road: 13319.1103703704,
		acceleration: -0.641759259259258
	},
	{
		id: 1365,
		time: 1364,
		velocity: 3.07972222222222,
		power: -2619.0511136945,
		road: 13322.5884259259,
		acceleration: -0.927592592592593
	},
	{
		id: 1366,
		time: 1365,
		velocity: 2.03638888888889,
		power: -1451.1963452527,
		road: 13325.2495833333,
		acceleration: -0.706203703703704
	},
	{
		id: 1367,
		time: 1366,
		velocity: 1.80805555555556,
		power: -747.498808464995,
		road: 13327.3006018519,
		acceleration: -0.514074074074074
	},
	{
		id: 1368,
		time: 1367,
		velocity: 1.5375,
		power: -118.18847554839,
		road: 13328.9932407408,
		acceleration: -0.202685185185185
	},
	{
		id: 1369,
		time: 1368,
		velocity: 1.42833333333333,
		power: -147.956679085369,
		road: 13330.4672685185,
		acceleration: -0.234537037037037
	},
	{
		id: 1370,
		time: 1369,
		velocity: 1.10444444444444,
		power: 101.823838047996,
		road: 13331.8000925926,
		acceleration: -0.04787037037037
	},
	{
		id: 1371,
		time: 1370,
		velocity: 1.39388888888889,
		power: 611.087279621545,
		road: 13333.2646759259,
		acceleration: 0.311388888888889
	},
	{
		id: 1372,
		time: 1371,
		velocity: 2.3625,
		power: 730.164951741896,
		road: 13335.0375,
		acceleration: 0.305092592592593
	},
	{
		id: 1373,
		time: 1372,
		velocity: 2.01972222222222,
		power: 640.360106923272,
		road: 13337.064537037,
		acceleration: 0.203333333333333
	},
	{
		id: 1374,
		time: 1373,
		velocity: 2.00388888888889,
		power: -66.2116652251806,
		road: 13339.1112962963,
		acceleration: -0.163888888888889
	},
	{
		id: 1375,
		time: 1374,
		velocity: 1.87083333333333,
		power: -464.206758365468,
		road: 13340.8725462963,
		acceleration: -0.40712962962963
	},
	{
		id: 1376,
		time: 1375,
		velocity: 0.798333333333333,
		power: -625.997112180584,
		road: 13342.09625,
		acceleration: -0.667962962962963
	},
	{
		id: 1377,
		time: 1376,
		velocity: 0,
		power: -271.582191226702,
		road: 13342.6741666667,
		acceleration: -0.623611111111111
	},
	{
		id: 1378,
		time: 1377,
		velocity: 0,
		power: -17.4685137426901,
		road: 13342.8072222222,
		acceleration: -0.266111111111111
	},
	{
		id: 1379,
		time: 1378,
		velocity: 0,
		power: 0,
		road: 13342.8072222222,
		acceleration: 0
	},
	{
		id: 1380,
		time: 1379,
		velocity: 0,
		power: 0,
		road: 13342.8072222222,
		acceleration: 0
	},
	{
		id: 1381,
		time: 1380,
		velocity: 0,
		power: 0,
		road: 13342.8072222222,
		acceleration: 0
	},
	{
		id: 1382,
		time: 1381,
		velocity: 0,
		power: 0,
		road: 13342.8072222222,
		acceleration: 0
	},
	{
		id: 1383,
		time: 1382,
		velocity: 0,
		power: 0,
		road: 13342.8072222222,
		acceleration: 0
	},
	{
		id: 1384,
		time: 1383,
		velocity: 0,
		power: 0,
		road: 13342.8072222222,
		acceleration: 0
	},
	{
		id: 1385,
		time: 1384,
		velocity: 0,
		power: 0,
		road: 13342.8072222222,
		acceleration: 0
	},
	{
		id: 1386,
		time: 1385,
		velocity: 0,
		power: 0,
		road: 13342.8072222222,
		acceleration: 0
	},
	{
		id: 1387,
		time: 1386,
		velocity: 0,
		power: 0,
		road: 13342.8072222222,
		acceleration: 0
	},
	{
		id: 1388,
		time: 1387,
		velocity: 0,
		power: 149.253704060194,
		road: 13343.0577777778,
		acceleration: 0.501111111111111
	},
	{
		id: 1389,
		time: 1388,
		velocity: 1.50333333333333,
		power: 1106.61729895957,
		road: 13344.0709259259,
		acceleration: 1.02407407407407
	},
	{
		id: 1390,
		time: 1389,
		velocity: 3.07222222222222,
		power: 2949.86761024881,
		road: 13346.2458796296,
		acceleration: 1.29953703703704
	},
	{
		id: 1391,
		time: 1390,
		velocity: 3.89861111111111,
		power: 3549.40633889018,
		road: 13349.5667592593,
		acceleration: 0.992314814814815
	},
	{
		id: 1392,
		time: 1391,
		velocity: 4.48027777777778,
		power: 4020.00435111539,
		road: 13353.8133796296,
		acceleration: 0.859166666666666
	},
	{
		id: 1393,
		time: 1392,
		velocity: 5.64972222222222,
		power: 3702.90935345145,
		road: 13358.8089351852,
		acceleration: 0.638703703703704
	},
	{
		id: 1394,
		time: 1393,
		velocity: 5.81472222222222,
		power: 4393.65756464843,
		road: 13364.4601388889,
		acceleration: 0.672592592592593
	},
	{
		id: 1395,
		time: 1394,
		velocity: 6.49805555555556,
		power: 4831.07095426439,
		road: 13370.775,
		acceleration: 0.654722222222222
	},
	{
		id: 1396,
		time: 1395,
		velocity: 7.61388888888889,
		power: 6958.72675051689,
		road: 13377.8559259259,
		acceleration: 0.877407407407408
	},
	{
		id: 1397,
		time: 1396,
		velocity: 8.44694444444444,
		power: 8215.43890714698,
		road: 13385.8348148148,
		acceleration: 0.918518518518516
	},
	{
		id: 1398,
		time: 1397,
		velocity: 9.25361111111111,
		power: 6695.24700071671,
		road: 13394.5892592593,
		acceleration: 0.632592592592594
	},
	{
		id: 1399,
		time: 1398,
		velocity: 9.51166666666666,
		power: 3711.80172697985,
		road: 13403.7843981482,
		acceleration: 0.248796296296296
	},
	{
		id: 1400,
		time: 1399,
		velocity: 9.19333333333333,
		power: 1054.03823201903,
		road: 13413.075462963,
		acceleration: -0.0569444444444454
	},
	{
		id: 1401,
		time: 1400,
		velocity: 9.08277777777778,
		power: -329.944516857792,
		road: 13422.2318055556,
		acceleration: -0.212499999999999
	},
	{
		id: 1402,
		time: 1401,
		velocity: 8.87416666666667,
		power: 135.186833863538,
		road: 13431.2034259259,
		acceleration: -0.156944444444445
	},
	{
		id: 1403,
		time: 1402,
		velocity: 8.7225,
		power: 276.065131946556,
		road: 13440.0273611111,
		acceleration: -0.138425925925926
	},
	{
		id: 1404,
		time: 1403,
		velocity: 8.6675,
		power: 14.9332117186857,
		road: 13448.6981018519,
		acceleration: -0.167962962962964
	},
	{
		id: 1405,
		time: 1404,
		velocity: 8.37027777777778,
		power: -491.515344059339,
		road: 13457.170462963,
		acceleration: -0.228796296296295
	},
	{
		id: 1406,
		time: 1405,
		velocity: 8.03611111111111,
		power: 155.986168703831,
		road: 13465.4552777778,
		acceleration: -0.146296296296297
	},
	{
		id: 1407,
		time: 1406,
		velocity: 8.22861111111111,
		power: 2900.55993489935,
		road: 13473.7670833333,
		acceleration: 0.200277777777778
	},
	{
		id: 1408,
		time: 1407,
		velocity: 8.97111111111111,
		power: 5931.41466135484,
		road: 13482.4526851852,
		acceleration: 0.547314814814815
	},
	{
		id: 1409,
		time: 1408,
		velocity: 9.67805555555556,
		power: 6800.12227143043,
		road: 13491.7097222222,
		acceleration: 0.595555555555556
	},
	{
		id: 1410,
		time: 1409,
		velocity: 10.0152777777778,
		power: 5312.61735292959,
		road: 13501.46,
		acceleration: 0.390925925925925
	},
	{
		id: 1411,
		time: 1410,
		velocity: 10.1438888888889,
		power: 3628.52464091578,
		road: 13511.5031944445,
		acceleration: 0.194907407407406
	},
	{
		id: 1412,
		time: 1411,
		velocity: 10.2627777777778,
		power: 2340.33507138573,
		road: 13521.6717592593,
		acceleration: 0.0558333333333341
	},
	{
		id: 1413,
		time: 1412,
		velocity: 10.1827777777778,
		power: 2085.38245060628,
		road: 13531.8823148148,
		acceleration: 0.0281481481481496
	},
	{
		id: 1414,
		time: 1413,
		velocity: 10.2283333333333,
		power: 2427.25203265175,
		road: 13542.1377777778,
		acceleration: 0.0616666666666674
	},
	{
		id: 1415,
		time: 1414,
		velocity: 10.4477777777778,
		power: 4294.54728537615,
		road: 13552.5462962963,
		acceleration: 0.244444444444444
	},
	{
		id: 1416,
		time: 1415,
		velocity: 10.9161111111111,
		power: 4675.80138846208,
		road: 13563.21125,
		acceleration: 0.268425925925925
	},
	{
		id: 1417,
		time: 1416,
		velocity: 11.0336111111111,
		power: 5766.18075630066,
		road: 13574.1882407407,
		acceleration: 0.355648148148148
	},
	{
		id: 1418,
		time: 1417,
		velocity: 11.5147222222222,
		power: 5252.67919700687,
		road: 13585.4871296296,
		acceleration: 0.288148148148146
	},
	{
		id: 1419,
		time: 1418,
		velocity: 11.7805555555556,
		power: 6883.75838622134,
		road: 13597.1378703704,
		acceleration: 0.41555555555556
	},
	{
		id: 1420,
		time: 1419,
		velocity: 12.2802777777778,
		power: 5972.6162201384,
		road: 13609.1525462963,
		acceleration: 0.312314814814814
	},
	{
		id: 1421,
		time: 1420,
		velocity: 12.4516666666667,
		power: 6176.03789263999,
		road: 13621.4793981482,
		acceleration: 0.312037037037037
	},
	{
		id: 1422,
		time: 1421,
		velocity: 12.7166666666667,
		power: 5248.04837611952,
		road: 13634.0722222222,
		acceleration: 0.219907407407407
	},
	{
		id: 1423,
		time: 1422,
		velocity: 12.94,
		power: 5520.47412555036,
		road: 13646.8906018519,
		acceleration: 0.231203703703704
	},
	{
		id: 1424,
		time: 1423,
		velocity: 13.1452777777778,
		power: 5921.86629620452,
		road: 13659.9503240741,
		acceleration: 0.251481481481482
	},
	{
		id: 1425,
		time: 1424,
		velocity: 13.4711111111111,
		power: 5939.52470053828,
		road: 13673.2559722222,
		acceleration: 0.240370370370371
	},
	{
		id: 1426,
		time: 1425,
		velocity: 13.6611111111111,
		power: 6315.83184326097,
		road: 13686.8103240741,
		acceleration: 0.257037037037037
	},
	{
		id: 1427,
		time: 1426,
		velocity: 13.9163888888889,
		power: 6965.44165952929,
		road: 13700.6393055556,
		acceleration: 0.292222222222222
	},
	{
		id: 1428,
		time: 1427,
		velocity: 14.3477777777778,
		power: 7432.56147850834,
		road: 13714.7697685185,
		acceleration: 0.310740740740741
	},
	{
		id: 1429,
		time: 1428,
		velocity: 14.5933333333333,
		power: 6325.42479974594,
		road: 13729.1635185185,
		acceleration: 0.215833333333332
	},
	{
		id: 1430,
		time: 1429,
		velocity: 14.5638888888889,
		power: 5250.81133271929,
		road: 13743.7303240741,
		acceleration: 0.130277777777776
	},
	{
		id: 1431,
		time: 1430,
		velocity: 14.7386111111111,
		power: 5792.24350235405,
		road: 13758.4435648148,
		acceleration: 0.162592592592594
	},
	{
		id: 1432,
		time: 1431,
		velocity: 15.0811111111111,
		power: 7140.89660160807,
		road: 13773.3621296296,
		acceleration: 0.248055555555556
	},
	{
		id: 1433,
		time: 1432,
		velocity: 15.3080555555556,
		power: 5948.34854780704,
		road: 13788.4824074074,
		acceleration: 0.15537037037037
	},
	{
		id: 1434,
		time: 1433,
		velocity: 15.2047222222222,
		power: 1701.02603503104,
		road: 13803.6109722222,
		acceleration: -0.138796296296295
	},
	{
		id: 1435,
		time: 1434,
		velocity: 14.6647222222222,
		power: -1208.73024375996,
		road: 13818.5014351852,
		acceleration: -0.337407407407408
	},
	{
		id: 1436,
		time: 1435,
		velocity: 14.2958333333333,
		power: -2774.42763836827,
		road: 13833.0000462963,
		acceleration: -0.446296296296296
	},
	{
		id: 1437,
		time: 1436,
		velocity: 13.8658333333333,
		power: -2002.4403028082,
		road: 13847.0814351852,
		acceleration: -0.388148148148149
	},
	{
		id: 1438,
		time: 1437,
		velocity: 13.5002777777778,
		power: -2654.6275548487,
		road: 13860.7506481482,
		acceleration: -0.436203703703704
	},
	{
		id: 1439,
		time: 1438,
		velocity: 12.9872222222222,
		power: -1750.37438813373,
		road: 13874.0193055556,
		acceleration: -0.364907407407408
	},
	{
		id: 1440,
		time: 1439,
		velocity: 12.7711111111111,
		power: -2617.86087683124,
		road: 13886.8885185185,
		acceleration: -0.433981481481482
	},
	{
		id: 1441,
		time: 1440,
		velocity: 12.1983333333333,
		power: -3870.81620044446,
		road: 13899.2699537037,
		acceleration: -0.541574074074072
	},
	{
		id: 1442,
		time: 1441,
		velocity: 11.3625,
		power: -6056.57244480664,
		road: 13911.0075,
		acceleration: -0.746203703703705
	},
	{
		id: 1443,
		time: 1442,
		velocity: 10.5325,
		power: -6778.12706603681,
		road: 13921.949537037,
		acceleration: -0.844814814814814
	},
	{
		id: 1444,
		time: 1443,
		velocity: 9.66388888888889,
		power: -5865.29581133671,
		road: 13932.0725,
		acceleration: -0.793333333333335
	},
	{
		id: 1445,
		time: 1444,
		velocity: 8.9825,
		power: -7024.79077698622,
		road: 13941.3118055556,
		acceleration: -0.973981481481481
	},
	{
		id: 1446,
		time: 1445,
		velocity: 7.61055555555556,
		power: -6551.718085604,
		road: 13949.5641203704,
		acceleration: -1
	},
	{
		id: 1447,
		time: 1446,
		velocity: 6.66388888888889,
		power: -8316.36170394655,
		road: 13956.6189351852,
		acceleration: -1.395
	},
	{
		id: 1448,
		time: 1447,
		velocity: 4.7975,
		power: -5181.54541172652,
		road: 13962.4342592593,
		acceleration: -1.08398148148148
	},
	{
		id: 1449,
		time: 1448,
		velocity: 4.35861111111111,
		power: -5057.97248354399,
		road: 13967.0625,
		acceleration: -1.29018518518519
	},
	{
		id: 1450,
		time: 1449,
		velocity: 2.79333333333333,
		power: -3236.27801275429,
		road: 13970.4797222222,
		acceleration: -1.13185185185185
	},
	{
		id: 1451,
		time: 1450,
		velocity: 1.40194444444444,
		power: -2266.63310918923,
		road: 13972.7363425926,
		acceleration: -1.18935185185185
	},
	{
		id: 1452,
		time: 1451,
		velocity: 0.790555555555556,
		power: -910.448848008282,
		road: 13973.9327314815,
		acceleration: -0.931111111111111
	},
	{
		id: 1453,
		time: 1452,
		velocity: 0,
		power: -160.02288579818,
		road: 13974.4299074074,
		acceleration: -0.467314814814815
	},
	{
		id: 1454,
		time: 1453,
		velocity: 0,
		power: -16.974707179987,
		road: 13974.5616666667,
		acceleration: -0.263518518518519
	},
	{
		id: 1455,
		time: 1454,
		velocity: 0,
		power: 0,
		road: 13974.5616666667,
		acceleration: 0
	},
	{
		id: 1456,
		time: 1455,
		velocity: 0,
		power: 0,
		road: 13974.5616666667,
		acceleration: 0
	},
	{
		id: 1457,
		time: 1456,
		velocity: 0,
		power: 0,
		road: 13974.5616666667,
		acceleration: 0
	},
	{
		id: 1458,
		time: 1457,
		velocity: 0,
		power: 0,
		road: 13974.5616666667,
		acceleration: 0
	},
	{
		id: 1459,
		time: 1458,
		velocity: 0,
		power: 0,
		road: 13974.5616666667,
		acceleration: 0
	},
	{
		id: 1460,
		time: 1459,
		velocity: 0,
		power: 0,
		road: 13974.5616666667,
		acceleration: 0
	},
	{
		id: 1461,
		time: 1460,
		velocity: 0,
		power: 0,
		road: 13974.5616666667,
		acceleration: 0
	},
	{
		id: 1462,
		time: 1461,
		velocity: 0,
		power: 0,
		road: 13974.5616666667,
		acceleration: 0
	},
	{
		id: 1463,
		time: 1462,
		velocity: 0,
		power: 361.472109109778,
		road: 13974.9676388889,
		acceleration: 0.811944444444444
	},
	{
		id: 1464,
		time: 1463,
		velocity: 2.43583333333333,
		power: 1837.47068532435,
		road: 13976.3943518519,
		acceleration: 1.22953703703704
	},
	{
		id: 1465,
		time: 1464,
		velocity: 3.68861111111111,
		power: 5468.07546666454,
		road: 13979.3456481482,
		acceleration: 1.81962962962963
	},
	{
		id: 1466,
		time: 1465,
		velocity: 5.45888888888889,
		power: 4681.40871244233,
		road: 13983.7032407407,
		acceleration: 0.992962962962963
	},
	{
		id: 1467,
		time: 1466,
		velocity: 5.41472222222222,
		power: 4977.50738462233,
		road: 13988.9818981482,
		acceleration: 0.849166666666666
	},
	{
		id: 1468,
		time: 1467,
		velocity: 6.23611111111111,
		power: 4704.48083446031,
		road: 13995.0208796296,
		acceleration: 0.671481481481481
	},
	{
		id: 1469,
		time: 1468,
		velocity: 7.47333333333333,
		power: 7488.50254726658,
		road: 14001.8915277778,
		acceleration: 0.991851851851852
	},
	{
		id: 1470,
		time: 1469,
		velocity: 8.39027777777778,
		power: 10397.1437712399,
		road: 14009.8617592593,
		acceleration: 1.20731481481481
	},
	{
		id: 1471,
		time: 1470,
		velocity: 9.85805555555555,
		power: 10216.0836941279,
		road: 14018.939537037,
		acceleration: 1.00777777777778
	},
	{
		id: 1472,
		time: 1471,
		velocity: 10.4966666666667,
		power: 11844.2686068515,
		road: 14029.04375,
		acceleration: 1.04509259259259
	},
	{
		id: 1473,
		time: 1472,
		velocity: 11.5255555555556,
		power: 14798.211917778,
		road: 14040.2629166667,
		acceleration: 1.18481481481482
	},
	{
		id: 1474,
		time: 1473,
		velocity: 13.4125,
		power: 19155.1852568677,
		road: 14052.7691666667,
		acceleration: 1.38935185185185
	},
	{
		id: 1475,
		time: 1474,
		velocity: 14.6647222222222,
		power: 17879.3877406195,
		road: 14066.5331018519,
		acceleration: 1.12601851851852
	},
	{
		id: 1476,
		time: 1475,
		velocity: 14.9036111111111,
		power: 14257.7894773519,
		road: 14081.2425925926,
		acceleration: 0.765092592592593
	},
	{
		id: 1477,
		time: 1476,
		velocity: 15.7077777777778,
		power: 9788.27270186316,
		road: 14096.5397222222,
		acceleration: 0.410185185185185
	},
	{
		id: 1478,
		time: 1477,
		velocity: 15.8952777777778,
		power: 6945.1486106704,
		road: 14112.1424537037,
		acceleration: 0.201018518518516
	},
	{
		id: 1479,
		time: 1478,
		velocity: 15.5066666666667,
		power: 661.936114840339,
		road: 14127.7356481482,
		acceleration: -0.220092592592591
	},
	{
		id: 1480,
		time: 1479,
		velocity: 15.0475,
		power: -1400.8183096929,
		road: 14143.0411574074,
		acceleration: -0.355277777777779
	},
	{
		id: 1481,
		time: 1480,
		velocity: 14.8294444444444,
		power: 5356.44383718133,
		road: 14158.2249074074,
		acceleration: 0.111759259259259
	},
	{
		id: 1482,
		time: 1481,
		velocity: 15.8419444444444,
		power: 9710.03279117828,
		road: 14173.6626851852,
		acceleration: 0.396296296296299
	},
	{
		id: 1483,
		time: 1482,
		velocity: 16.2363888888889,
		power: 11582.107169522,
		road: 14189.5451851852,
		acceleration: 0.493148148148149
	},
	{
		id: 1484,
		time: 1483,
		velocity: 16.3088888888889,
		power: 5590.33381445269,
		road: 14205.7175925926,
		acceleration: 0.086666666666666
	},
	{
		id: 1485,
		time: 1484,
		velocity: 16.1019444444444,
		power: 2348.29112224084,
		road: 14221.8720833333,
		acceleration: -0.122500000000002
	},
	{
		id: 1486,
		time: 1485,
		velocity: 15.8688888888889,
		power: 1282.68452124602,
		road: 14237.8713888889,
		acceleration: -0.187870370370369
	},
	{
		id: 1487,
		time: 1486,
		velocity: 15.7452777777778,
		power: 953.487730550549,
		road: 14253.674212963,
		acceleration: -0.205092592592592
	},
	{
		id: 1488,
		time: 1487,
		velocity: 15.4866666666667,
		power: -5198.84456267111,
		road: 14269.0672685185,
		acceleration: -0.614444444444445
	},
	{
		id: 1489,
		time: 1488,
		velocity: 14.0255555555556,
		power: -12179.9175997539,
		road: 14283.5908796296,
		acceleration: -1.12444444444444
	},
	{
		id: 1490,
		time: 1489,
		velocity: 12.3719444444444,
		power: -18539.1027234693,
		road: 14296.6990277778,
		acceleration: -1.70648148148148
	},
	{
		id: 1491,
		time: 1490,
		velocity: 10.3672222222222,
		power: -13704.0325640049,
		road: 14308.2294444445,
		acceleration: -1.44898148148148
	},
	{
		id: 1492,
		time: 1491,
		velocity: 9.67861111111111,
		power: -8269.79480455638,
		road: 14318.5200925926,
		acceleration: -1.03055555555556
	},
	{
		id: 1493,
		time: 1492,
		velocity: 9.28027777777778,
		power: -3905.0105015267,
		road: 14327.99,
		acceleration: -0.610925925925926
	},
	{
		id: 1494,
		time: 1493,
		velocity: 8.53444444444444,
		power: -4470.90558766893,
		road: 14336.8024074074,
		acceleration: -0.704074074074073
	},
	{
		id: 1495,
		time: 1494,
		velocity: 7.56638888888889,
		power: -5106.02724527176,
		road: 14344.8473611111,
		acceleration: -0.830833333333334
	},
	{
		id: 1496,
		time: 1495,
		velocity: 6.78777777777778,
		power: -3342.4040422146,
		road: 14352.1578240741,
		acceleration: -0.638148148148148
	},
	{
		id: 1497,
		time: 1496,
		velocity: 6.62,
		power: -1370.09604849941,
		road: 14358.9666666667,
		acceleration: -0.365092592592593
	},
	{
		id: 1498,
		time: 1497,
		velocity: 6.47111111111111,
		power: -44.6818608729231,
		road: 14365.5136111111,
		acceleration: -0.158703703703703
	},
	{
		id: 1499,
		time: 1498,
		velocity: 6.31166666666667,
		power: -83.3321167437124,
		road: 14371.8991666667,
		acceleration: -0.164074074074074
	},
	{
		id: 1500,
		time: 1499,
		velocity: 6.12777777777778,
		power: -21.6056008726506,
		road: 14378.12625,
		acceleration: -0.152870370370371
	},
	{
		id: 1501,
		time: 1500,
		velocity: 6.0125,
		power: -662.175651366073,
		road: 14384.1451388889,
		acceleration: -0.263518518518518
	},
	{
		id: 1502,
		time: 1501,
		velocity: 5.52111111111111,
		power: -1490.66240300373,
		road: 14389.8213425926,
		acceleration: -0.421851851851851
	},
	{
		id: 1503,
		time: 1502,
		velocity: 4.86222222222222,
		power: -1999.13462490709,
		road: 14395.0126851852,
		acceleration: -0.547870370370371
	},
	{
		id: 1504,
		time: 1503,
		velocity: 4.36888888888889,
		power: -1476.47311470853,
		road: 14399.694212963,
		acceleration: -0.471759259259259
	},
	{
		id: 1505,
		time: 1504,
		velocity: 4.10583333333333,
		power: -6.49876544819621,
		road: 14404.07,
		acceleration: -0.139722222222222
	},
	{
		id: 1506,
		time: 1505,
		velocity: 4.44305555555556,
		power: 1429.80676227299,
		road: 14408.4775462963,
		acceleration: 0.20324074074074
	},
	{
		id: 1507,
		time: 1506,
		velocity: 4.97861111111111,
		power: 1891.4427308561,
		road: 14413.1308796296,
		acceleration: 0.288333333333333
	},
	{
		id: 1508,
		time: 1507,
		velocity: 4.97083333333333,
		power: 2656.52893814807,
		road: 14418.1368981482,
		acceleration: 0.417037037037037
	},
	{
		id: 1509,
		time: 1508,
		velocity: 5.69416666666667,
		power: 3230.82555193724,
		road: 14423.5910185185,
		acceleration: 0.479166666666667
	},
	{
		id: 1510,
		time: 1509,
		velocity: 6.41611111111111,
		power: 3939.04176741619,
		road: 14429.5581944445,
		acceleration: 0.546944444444444
	},
	{
		id: 1511,
		time: 1510,
		velocity: 6.61166666666667,
		power: 2189.18654981728,
		road: 14435.9051851852,
		acceleration: 0.212685185185186
	},
	{
		id: 1512,
		time: 1511,
		velocity: 6.33222222222222,
		power: -303.557711027666,
		road: 14442.2583333333,
		acceleration: -0.200370370370369
	},
	{
		id: 1513,
		time: 1512,
		velocity: 5.815,
		power: -2267.1402083092,
		road: 14448.2381018519,
		acceleration: -0.54638888888889
	},
	{
		id: 1514,
		time: 1513,
		velocity: 4.9725,
		power: -3797.68191929785,
		road: 14453.4928703704,
		acceleration: -0.903611111111111
	},
	{
		id: 1515,
		time: 1514,
		velocity: 3.62138888888889,
		power: -3811.22408726973,
		road: 14457.7562962963,
		acceleration: -1.07907407407407
	},
	{
		id: 1516,
		time: 1515,
		velocity: 2.57777777777778,
		power: -2564.76372254797,
		road: 14460.9964814815,
		acceleration: -0.967407407407407
	},
	{
		id: 1517,
		time: 1516,
		velocity: 2.07027777777778,
		power: -700.981436709701,
		road: 14463.5423148148,
		acceleration: -0.421296296296296
	},
	{
		id: 1518,
		time: 1517,
		velocity: 2.3575,
		power: 1277.78657987497,
		road: 14466.0775925926,
		acceleration: 0.400185185185185
	},
	{
		id: 1519,
		time: 1518,
		velocity: 3.77833333333333,
		power: 1994.84946399793,
		road: 14469.095,
		acceleration: 0.564074074074074
	},
	{
		id: 1520,
		time: 1519,
		velocity: 3.7625,
		power: 1953.22342672655,
		road: 14472.6191666667,
		acceleration: 0.449444444444444
	},
	{
		id: 1521,
		time: 1520,
		velocity: 3.70583333333333,
		power: 193.778773329791,
		road: 14476.3280092593,
		acceleration: -0.0800925925925928
	},
	{
		id: 1522,
		time: 1521,
		velocity: 3.53805555555556,
		power: 247.193196983736,
		road: 14479.9651851852,
		acceleration: -0.0632407407407403
	},
	{
		id: 1523,
		time: 1522,
		velocity: 3.57277777777778,
		power: 225.497840078184,
		road: 14483.536712963,
		acceleration: -0.068055555555556
	},
	{
		id: 1524,
		time: 1523,
		velocity: 3.50166666666667,
		power: 2548.48298848025,
		road: 14487.3576388889,
		acceleration: 0.566851851851852
	},
	{
		id: 1525,
		time: 1524,
		velocity: 5.23861111111111,
		power: 3208.4308900826,
		road: 14491.7751388889,
		acceleration: 0.626296296296297
	},
	{
		id: 1526,
		time: 1525,
		velocity: 5.45166666666667,
		power: 3606.94994037115,
		road: 14496.8118055556,
		acceleration: 0.612037037037037
	},
	{
		id: 1527,
		time: 1526,
		velocity: 5.33777777777778,
		power: 909.010263037247,
		road: 14502.1719444444,
		acceleration: 0.0349074074074069
	},
	{
		id: 1528,
		time: 1527,
		velocity: 5.34333333333333,
		power: 319.294524076036,
		road: 14507.5093055556,
		acceleration: -0.0804629629629625
	},
	{
		id: 1529,
		time: 1528,
		velocity: 5.21027777777778,
		power: -573.234803657361,
		road: 14512.6768518519,
		acceleration: -0.259166666666666
	},
	{
		id: 1530,
		time: 1529,
		velocity: 4.56027777777778,
		power: -2000.38063508599,
		road: 14517.4229166667,
		acceleration: -0.583796296296297
	},
	{
		id: 1531,
		time: 1530,
		velocity: 3.59194444444444,
		power: -2335.52292715925,
		road: 14521.5075925926,
		acceleration: -0.738981481481481
	},
	{
		id: 1532,
		time: 1531,
		velocity: 2.99333333333333,
		power: -2196.41003717068,
		road: 14524.8050925926,
		acceleration: -0.835370370370371
	},
	{
		id: 1533,
		time: 1532,
		velocity: 2.05416666666667,
		power: -1873.1348378531,
		road: 14527.2087962963,
		acceleration: -0.952222222222222
	},
	{
		id: 1534,
		time: 1533,
		velocity: 0.735277777777778,
		power: -1177.21745068086,
		road: 14528.6375,
		acceleration: -0.997777777777778
	},
	{
		id: 1535,
		time: 1534,
		velocity: 0,
		power: -310.077865148236,
		road: 14529.2249537037,
		acceleration: -0.684722222222222
	},
	{
		id: 1536,
		time: 1535,
		velocity: 0,
		power: -13.6486044996751,
		road: 14529.3475,
		acceleration: -0.245092592592593
	},
	{
		id: 1537,
		time: 1536,
		velocity: 0,
		power: 0,
		road: 14529.3475,
		acceleration: 0
	},
	{
		id: 1538,
		time: 1537,
		velocity: 0,
		power: 0,
		road: 14529.3475,
		acceleration: 0
	},
	{
		id: 1539,
		time: 1538,
		velocity: 0,
		power: 38.0836747989047,
		road: 14529.4609259259,
		acceleration: 0.226851851851852
	},
	{
		id: 1540,
		time: 1539,
		velocity: 0.680555555555556,
		power: 902.086415275526,
		road: 14530.2369907407,
		acceleration: 1.09842592592593
	},
	{
		id: 1541,
		time: 1540,
		velocity: 3.29527777777778,
		power: 4066.35918409618,
		road: 14532.460787037,
		acceleration: 1.79703703703704
	},
	{
		id: 1542,
		time: 1541,
		velocity: 5.39111111111111,
		power: 6965.12990053979,
		road: 14536.4371296296,
		acceleration: 1.70805555555556
	},
	{
		id: 1543,
		time: 1542,
		velocity: 5.80472222222222,
		power: 4648.28488745169,
		road: 14541.6640277778,
		acceleration: 0.793055555555555
	},
	{
		id: 1544,
		time: 1543,
		velocity: 5.67444444444444,
		power: 2321.52614104776,
		road: 14547.4263425926,
		acceleration: 0.277777777777779
	},
	{
		id: 1545,
		time: 1544,
		velocity: 6.22444444444444,
		power: 5036.87509681771,
		road: 14553.6766203704,
		acceleration: 0.698148148148149
	},
	{
		id: 1546,
		time: 1545,
		velocity: 7.89916666666667,
		power: 8707.51052233258,
		road: 14560.8369444444,
		acceleration: 1.12194444444444
	},
	{
		id: 1547,
		time: 1546,
		velocity: 9.04027777777778,
		power: 9651.9103524759,
		road: 14569.0896296296,
		acceleration: 1.06277777777778
	},
	{
		id: 1548,
		time: 1547,
		velocity: 9.41277777777778,
		power: 5521.51474475582,
		road: 14578.1085185185,
		acceleration: 0.469629629629628
	},
	{
		id: 1549,
		time: 1548,
		velocity: 9.30805555555555,
		power: 3918.06693188388,
		road: 14587.4928703704,
		acceleration: 0.261296296296296
	},
	{
		id: 1550,
		time: 1549,
		velocity: 9.82416666666666,
		power: 7986.88127693501,
		road: 14597.3423611111,
		acceleration: 0.668981481481481
	},
	{
		id: 1551,
		time: 1550,
		velocity: 11.4197222222222,
		power: 9131.92574898551,
		road: 14607.8856481481,
		acceleration: 0.718611111111112
	},
	{
		id: 1552,
		time: 1551,
		velocity: 11.4638888888889,
		power: 6861.74589624632,
		road: 14619.0130092593,
		acceleration: 0.449537037037038
	},
	{
		id: 1553,
		time: 1552,
		velocity: 11.1727777777778,
		power: 465.940430962765,
		road: 14630.2872685185,
		acceleration: -0.15574074074074
	},
	{
		id: 1554,
		time: 1553,
		velocity: 10.9525,
		power: -167.36791902227,
		road: 14641.3773611111,
		acceleration: -0.212592592592594
	},
	{
		id: 1555,
		time: 1554,
		velocity: 10.8261111111111,
		power: 47.170893981254,
		road: 14652.2662962963,
		acceleration: -0.189722222222221
	},
	{
		id: 1556,
		time: 1555,
		velocity: 10.6036111111111,
		power: 399.633901255674,
		road: 14662.9838425926,
		acceleration: -0.153055555555556
	},
	{
		id: 1557,
		time: 1556,
		velocity: 10.4933333333333,
		power: 96.7170994650979,
		road: 14673.5345833333,
		acceleration: -0.180555555555555
	},
	{
		id: 1558,
		time: 1557,
		velocity: 10.2844444444444,
		power: 269.358002908328,
		road: 14683.9145833333,
		acceleration: -0.160925925925929
	},
	{
		id: 1559,
		time: 1558,
		velocity: 10.1208333333333,
		power: -86.6250958035519,
		road: 14694.1166203704,
		acceleration: -0.194999999999997
	},
	{
		id: 1560,
		time: 1559,
		velocity: 9.90833333333333,
		power: 162.168797498034,
		road: 14704.1376388889,
		acceleration: -0.167037037037039
	},
	{
		id: 1561,
		time: 1560,
		velocity: 9.78333333333333,
		power: -145.48111449017,
		road: 14713.9763888889,
		acceleration: -0.1975
	},
	{
		id: 1562,
		time: 1561,
		velocity: 9.52833333333333,
		power: 1733.33055325595,
		road: 14723.7193055556,
		acceleration: 0.00583333333333158
	},
	{
		id: 1563,
		time: 1562,
		velocity: 9.92583333333333,
		power: 3108.648796067,
		road: 14733.5403703704,
		acceleration: 0.150462962962964
	},
	{
		id: 1564,
		time: 1563,
		velocity: 10.2347222222222,
		power: 6555.67672174684,
		road: 14743.6831018518,
		acceleration: 0.492870370370371
	},
	{
		id: 1565,
		time: 1564,
		velocity: 11.0069444444444,
		power: 7329.2575124758,
		road: 14754.3374074074,
		acceleration: 0.530277777777778
	},
	{
		id: 1566,
		time: 1565,
		velocity: 11.5166666666667,
		power: 8424.49223568333,
		road: 14765.5516203704,
		acceleration: 0.589537037037038
	},
	{
		id: 1567,
		time: 1566,
		velocity: 12.0033333333333,
		power: 6633.69597743969,
		road: 14777.2554166667,
		acceleration: 0.389629629629628
	},
	{
		id: 1568,
		time: 1567,
		velocity: 12.1758333333333,
		power: 5909.15944887223,
		road: 14789.3063888889,
		acceleration: 0.304722222222223
	},
	{
		id: 1569,
		time: 1568,
		velocity: 12.4308333333333,
		power: 3551.08516335046,
		road: 14801.5556481481,
		acceleration: 0.0918518518518514
	},
	{
		id: 1570,
		time: 1569,
		velocity: 12.2788888888889,
		power: 2695.40766884621,
		road: 14813.8592592593,
		acceleration: 0.0168518518518521
	},
	{
		id: 1571,
		time: 1570,
		velocity: 12.2263888888889,
		power: 1804.22472974961,
		road: 14826.142037037,
		acceleration: -0.0585185185185182
	},
	{
		id: 1572,
		time: 1571,
		velocity: 12.2552777777778,
		power: 2872.23827713833,
		road: 14838.4120833333,
		acceleration: 0.0330555555555545
	},
	{
		id: 1573,
		time: 1572,
		velocity: 12.3780555555556,
		power: 4017.17422664923,
		road: 14850.7624074074,
		acceleration: 0.127500000000001
	},
	{
		id: 1574,
		time: 1573,
		velocity: 12.6088888888889,
		power: 3976.69630136957,
		road: 14863.2359722222,
		acceleration: 0.118981481481482
	},
	{
		id: 1575,
		time: 1574,
		velocity: 12.6122222222222,
		power: 4593.63246794942,
		road: 14875.85125,
		acceleration: 0.164444444444444
	},
	{
		id: 1576,
		time: 1575,
		velocity: 12.8713888888889,
		power: 4104.15827134646,
		road: 14888.6077314815,
		acceleration: 0.117962962962963
	},
	{
		id: 1577,
		time: 1576,
		velocity: 12.9627777777778,
		power: 3791.07399103634,
		road: 14901.4673148148,
		acceleration: 0.0882407407407406
	},
	{
		id: 1578,
		time: 1577,
		velocity: 12.8769444444444,
		power: 2399.03431197419,
		road: 14914.357962963,
		acceleration: -0.0261111111111116
	},
	{
		id: 1579,
		time: 1578,
		velocity: 12.7930555555556,
		power: 2149.69681979179,
		road: 14927.2128703704,
		acceleration: -0.0453703703703692
	},
	{
		id: 1580,
		time: 1579,
		velocity: 12.8266666666667,
		power: 1790.05612858989,
		road: 14940.0085185185,
		acceleration: -0.0731481481481495
	},
	{
		id: 1581,
		time: 1580,
		velocity: 12.6575,
		power: 1889.99241727956,
		road: 14952.7360185185,
		acceleration: -0.063148148148148
	},
	{
		id: 1582,
		time: 1581,
		velocity: 12.6036111111111,
		power: 1791.4045418201,
		road: 14965.3971759259,
		acceleration: -0.069537037037037
	},
	{
		id: 1583,
		time: 1582,
		velocity: 12.6180555555556,
		power: 2165.66261174635,
		road: 14978.0050462963,
		acceleration: -0.0370370370370381
	},
	{
		id: 1584,
		time: 1583,
		velocity: 12.5463888888889,
		power: 2719.50039818413,
		road: 14990.5991203704,
		acceleration: 0.00944444444444414
	},
	{
		id: 1585,
		time: 1584,
		velocity: 12.6319444444444,
		power: 2487.98701333959,
		road: 15003.1930092593,
		acceleration: -0.00981481481481161
	},
	{
		id: 1586,
		time: 1585,
		velocity: 12.5886111111111,
		power: 2680.9190768076,
		road: 15015.7851388889,
		acceleration: 0.00629629629629314
	},
	{
		id: 1587,
		time: 1586,
		velocity: 12.5652777777778,
		power: 2097.50903096536,
		road: 15028.359537037,
		acceleration: -0.0417592592592584
	},
	{
		id: 1588,
		time: 1587,
		velocity: 12.5066666666667,
		power: 3264.7200000674,
		road: 15040.9406944444,
		acceleration: 0.0552777777777784
	},
	{
		id: 1589,
		time: 1588,
		velocity: 12.7544444444444,
		power: 4573.75319010074,
		road: 15053.6292592593,
		acceleration: 0.159537037037037
	},
	{
		id: 1590,
		time: 1589,
		velocity: 13.0438888888889,
		power: 5192.24698816273,
		road: 15066.4985648148,
		acceleration: 0.201944444444443
	},
	{
		id: 1591,
		time: 1590,
		velocity: 13.1125,
		power: 4766.83995126525,
		road: 15079.5484722222,
		acceleration: 0.159259259259262
	},
	{
		id: 1592,
		time: 1591,
		velocity: 13.2322222222222,
		power: 4128.79023262539,
		road: 15092.7293981481,
		acceleration: 0.102777777777776
	},
	{
		id: 1593,
		time: 1592,
		velocity: 13.3522222222222,
		power: 4056.16836954248,
		road: 15106.008287037,
		acceleration: 0.0931481481481509
	},
	{
		id: 1594,
		time: 1593,
		velocity: 13.3919444444444,
		power: 5064.71982865083,
		road: 15119.4171759259,
		acceleration: 0.166851851851849
	},
	{
		id: 1595,
		time: 1594,
		velocity: 13.7327777777778,
		power: 4858.29212415869,
		road: 15132.9814814815,
		acceleration: 0.143981481481482
	},
	{
		id: 1596,
		time: 1595,
		velocity: 13.7841666666667,
		power: 5443.86030759448,
		road: 15146.7086111111,
		acceleration: 0.181666666666668
	},
	{
		id: 1597,
		time: 1596,
		velocity: 13.9369444444444,
		power: 4147.71395346745,
		road: 15160.565462963,
		acceleration: 0.0777777777777757
	},
	{
		id: 1598,
		time: 1597,
		velocity: 13.9661111111111,
		power: 3698.25855946608,
		road: 15174.482037037,
		acceleration: 0.0416666666666661
	},
	{
		id: 1599,
		time: 1598,
		velocity: 13.9091666666667,
		power: 3260.13396980837,
		road: 15188.4233796296,
		acceleration: 0.0078703703703713
	},
	{
		id: 1600,
		time: 1599,
		velocity: 13.9605555555556,
		power: 2855.27400465566,
		road: 15202.3575,
		acceleration: -0.0223148148148145
	},
	{
		id: 1601,
		time: 1600,
		velocity: 13.8991666666667,
		power: 3003.4838809088,
		road: 15216.2751388889,
		acceleration: -0.0106481481481477
	},
	{
		id: 1602,
		time: 1601,
		velocity: 13.8772222222222,
		power: 3066.42607369563,
		road: 15230.1846296296,
		acceleration: -0.00564814814814874
	},
	{
		id: 1603,
		time: 1602,
		velocity: 13.9436111111111,
		power: 3523.04124000776,
		road: 15244.105462963,
		acceleration: 0.0283333333333342
	},
	{
		id: 1604,
		time: 1603,
		velocity: 13.9841666666667,
		power: 4329.72629691821,
		road: 15258.0838425926,
		acceleration: 0.0867592592592601
	},
	{
		id: 1605,
		time: 1604,
		velocity: 14.1375,
		power: 2521.39046280216,
		road: 15272.0809722222,
		acceleration: -0.0492592592592587
	},
	{
		id: 1606,
		time: 1605,
		velocity: 13.7958333333333,
		power: 2127.09183301017,
		road: 15286.0149537037,
		acceleration: -0.0770370370370372
	},
	{
		id: 1607,
		time: 1606,
		velocity: 13.7530555555556,
		power: 2027.26174308319,
		road: 15299.869212963,
		acceleration: -0.082407407407409
	},
	{
		id: 1608,
		time: 1607,
		velocity: 13.8902777777778,
		power: 4884.41724310594,
		road: 15313.7485185185,
		acceleration: 0.1325
	},
	{
		id: 1609,
		time: 1608,
		velocity: 14.1933333333333,
		power: 6140.93971471649,
		road: 15327.8034259259,
		acceleration: 0.218703703703703
	},
	{
		id: 1610,
		time: 1609,
		velocity: 14.4091666666667,
		power: 5875.38547640047,
		road: 15342.0623611111,
		acceleration: 0.189351851851853
	},
	{
		id: 1611,
		time: 1610,
		velocity: 14.4583333333333,
		power: 3917.28422065215,
		road: 15356.4366666667,
		acceleration: 0.0413888888888856
	},
	{
		id: 1612,
		time: 1611,
		velocity: 14.3175,
		power: 2113.34462886281,
		road: 15370.7869907407,
		acceleration: -0.0893518518518519
	},
	{
		id: 1613,
		time: 1612,
		velocity: 14.1411111111111,
		power: 1428.44285175172,
		road: 15385.0243055555,
		acceleration: -0.136666666666665
	},
	{
		id: 1614,
		time: 1613,
		velocity: 14.0483333333333,
		power: 1378.92131444412,
		road: 15399.1247222222,
		acceleration: -0.137129629629632
	},
	{
		id: 1615,
		time: 1614,
		velocity: 13.9061111111111,
		power: 1700.23346229395,
		road: 15413.1014814815,
		acceleration: -0.110185185185182
	},
	{
		id: 1616,
		time: 1615,
		velocity: 13.8105555555556,
		power: 761.236767789863,
		road: 15426.9343055555,
		acceleration: -0.177685185185185
	},
	{
		id: 1617,
		time: 1616,
		velocity: 13.5152777777778,
		power: 1079.08080139724,
		road: 15440.6032407407,
		acceleration: -0.150092592592593
	},
	{
		id: 1618,
		time: 1617,
		velocity: 13.4558333333333,
		power: 841.913687677564,
		road: 15454.1145833333,
		acceleration: -0.165092592592593
	},
	{
		id: 1619,
		time: 1618,
		velocity: 13.3152777777778,
		power: 2077.97426601479,
		road: 15467.5103240741,
		acceleration: -0.0661111111111108
	},
	{
		id: 1620,
		time: 1619,
		velocity: 13.3169444444444,
		power: 1588.66460474717,
		road: 15480.8218518518,
		acceleration: -0.102314814814816
	},
	{
		id: 1621,
		time: 1620,
		velocity: 13.1488888888889,
		power: 2399.32732345429,
		road: 15494.0639814815,
		acceleration: -0.0364814814814807
	},
	{
		id: 1622,
		time: 1621,
		velocity: 13.2058333333333,
		power: 2395.28418788651,
		road: 15507.27,
		acceleration: -0.0357407407407386
	},
	{
		id: 1623,
		time: 1622,
		velocity: 13.2097222222222,
		power: 3943.61138586655,
		road: 15520.5012037037,
		acceleration: 0.0861111111111086
	},
	{
		id: 1624,
		time: 1623,
		velocity: 13.4072222222222,
		power: 4588.07337435924,
		road: 15533.841712963,
		acceleration: 0.132500000000002
	},
	{
		id: 1625,
		time: 1624,
		velocity: 13.6033333333333,
		power: 5257.91700690701,
		road: 15547.3374537037,
		acceleration: 0.17796296296296
	},
	{
		id: 1626,
		time: 1625,
		velocity: 13.7436111111111,
		power: 5515.53379230513,
		road: 15561.0168518518,
		acceleration: 0.189351851851852
	},
	{
		id: 1627,
		time: 1626,
		velocity: 13.9752777777778,
		power: 6448.82983830787,
		road: 15574.9156944444,
		acceleration: 0.249537037037038
	},
	{
		id: 1628,
		time: 1627,
		velocity: 14.3519444444444,
		power: 7010.56423271896,
		road: 15589.0781944444,
		acceleration: 0.277777777777779
	},
	{
		id: 1629,
		time: 1628,
		velocity: 14.5769444444444,
		power: 7141.48588736471,
		road: 15603.5160185185,
		acceleration: 0.27287037037037
	},
	{
		id: 1630,
		time: 1629,
		velocity: 14.7938888888889,
		power: 6990.96605351362,
		road: 15618.2145833333,
		acceleration: 0.24861111111111
	},
	{
		id: 1631,
		time: 1630,
		velocity: 15.0977777777778,
		power: 5885.09970836255,
		road: 15633.1177777778,
		acceleration: 0.160648148148148
	},
	{
		id: 1632,
		time: 1631,
		velocity: 15.0588888888889,
		power: 4326.43369336532,
		road: 15648.1249537037,
		acceleration: 0.0473148148148166
	},
	{
		id: 1633,
		time: 1632,
		velocity: 14.9358333333333,
		power: 2066.74283663575,
		road: 15663.1010648148,
		acceleration: -0.109444444444444
	},
	{
		id: 1634,
		time: 1633,
		velocity: 14.7694444444444,
		power: 1273.83237220139,
		road: 15677.9415740741,
		acceleration: -0.161759259259261
	},
	{
		id: 1635,
		time: 1634,
		velocity: 14.5736111111111,
		power: 1487.50430072054,
		road: 15692.6296759259,
		acceleration: -0.143055555555556
	},
	{
		id: 1636,
		time: 1635,
		velocity: 14.5066666666667,
		power: 1548.22126164027,
		road: 15707.1785648148,
		acceleration: -0.135370370370369
	},
	{
		id: 1637,
		time: 1636,
		velocity: 14.3633333333333,
		power: 2269.70260016335,
		road: 15721.619537037,
		acceleration: -0.0804629629629652
	},
	{
		id: 1638,
		time: 1637,
		velocity: 14.3322222222222,
		power: 2372.99432849618,
		road: 15735.9848611111,
		acceleration: -0.0708333333333293
	},
	{
		id: 1639,
		time: 1638,
		velocity: 14.2941666666667,
		power: 2581.60713840814,
		road: 15750.2878703704,
		acceleration: -0.0537962962962979
	},
	{
		id: 1640,
		time: 1639,
		velocity: 14.2019444444444,
		power: 3076.00366756114,
		road: 15764.5557407407,
		acceleration: -0.0164814814814811
	},
	{
		id: 1641,
		time: 1640,
		velocity: 14.2827777777778,
		power: 3200.75171997334,
		road: 15778.8118981481,
		acceleration: -0.00694444444444464
	},
	{
		id: 1642,
		time: 1641,
		velocity: 14.2733333333333,
		power: 2614.52204598869,
		road: 15793.04,
		acceleration: -0.0491666666666699
	},
	{
		id: 1643,
		time: 1642,
		velocity: 14.0544444444444,
		power: 793.859595570812,
		road: 15807.1531018518,
		acceleration: -0.180833333333332
	},
	{
		id: 1644,
		time: 1643,
		velocity: 13.7402777777778,
		power: -547.35027884224,
		road: 15821.0370833333,
		acceleration: -0.277407407407406
	},
	{
		id: 1645,
		time: 1644,
		velocity: 13.4411111111111,
		power: 0.498561258729353,
		road: 15834.6662962963,
		acceleration: -0.232129629629631
	},
	{
		id: 1646,
		time: 1645,
		velocity: 13.3580555555556,
		power: 1307.39125913808,
		road: 15848.1156481481,
		acceleration: -0.127592592592592
	},
	{
		id: 1647,
		time: 1646,
		velocity: 13.3575,
		power: 1016.96383758327,
		road: 15861.4275462963,
		acceleration: -0.147314814814814
	},
	{
		id: 1648,
		time: 1647,
		velocity: 12.9991666666667,
		power: -431.311460567983,
		road: 15874.5363888889,
		acceleration: -0.258796296296296
	},
	{
		id: 1649,
		time: 1648,
		velocity: 12.5816666666667,
		power: -1118.7413719437,
		road: 15887.3600462963,
		acceleration: -0.311574074074073
	},
	{
		id: 1650,
		time: 1649,
		velocity: 12.4227777777778,
		power: -442.052245237972,
		road: 15899.9013888889,
		acceleration: -0.253055555555559
	},
	{
		id: 1651,
		time: 1650,
		velocity: 12.24,
		power: -164.3958468867,
		road: 15912.2028240741,
		acceleration: -0.226759259259257
	},
	{
		id: 1652,
		time: 1651,
		velocity: 11.9013888888889,
		power: 49.7370663473252,
		road: 15924.2881481481,
		acceleration: -0.205462962962962
	},
	{
		id: 1653,
		time: 1652,
		velocity: 11.8063888888889,
		power: -33.2812355396481,
		road: 15936.165787037,
		acceleration: -0.20990740740741
	},
	{
		id: 1654,
		time: 1653,
		velocity: 11.6102777777778,
		power: 732.912403433189,
		road: 15947.8689814815,
		acceleration: -0.138981481481482
	},
	{
		id: 1655,
		time: 1654,
		velocity: 11.4844444444444,
		power: -185.333029629691,
		road: 15959.3931018518,
		acceleration: -0.219166666666666
	},
	{
		id: 1656,
		time: 1655,
		velocity: 11.1488888888889,
		power: -2149.29023427562,
		road: 15970.607962963,
		acceleration: -0.399351851851852
	},
	{
		id: 1657,
		time: 1656,
		velocity: 10.4122222222222,
		power: -4438.20630559229,
		road: 15981.3095833333,
		acceleration: -0.627129629629628
	},
	{
		id: 1658,
		time: 1657,
		velocity: 9.60305555555555,
		power: -5710.86927044687,
		road: 15991.3059259259,
		acceleration: -0.783425925925926
	},
	{
		id: 1659,
		time: 1658,
		velocity: 8.79861111111111,
		power: -5948.01041265456,
		road: 16000.4827777778,
		acceleration: -0.855555555555556
	},
	{
		id: 1660,
		time: 1659,
		velocity: 7.84555555555556,
		power: -5548.12329999747,
		road: 16008.7981481481,
		acceleration: -0.867407407407407
	},
	{
		id: 1661,
		time: 1660,
		velocity: 7.00083333333333,
		power: -7223.06456819875,
		road: 16016.0797222222,
		acceleration: -1.20018518518519
	},
	{
		id: 1662,
		time: 1661,
		velocity: 5.19805555555556,
		power: -8324.16263911975,
		road: 16021.9405555555,
		acceleration: -1.6412962962963
	},
	{
		id: 1663,
		time: 1662,
		velocity: 2.92166666666667,
		power: -6954.73914456258,
		road: 16026.0128703704,
		acceleration: -1.93574074074074
	},
	{
		id: 1664,
		time: 1663,
		velocity: 1.19361111111111,
		power: -3242.77537904656,
		road: 16028.3068518518,
		acceleration: -1.62092592592593
	},
	{
		id: 1665,
		time: 1664,
		velocity: 0.335277777777778,
		power: -798.926217348615,
		road: 16029.3034259259,
		acceleration: -0.973888888888889
	},
	{
		id: 1666,
		time: 1665,
		velocity: 0,
		power: -79.5704470811288,
		road: 16029.6141203704,
		acceleration: -0.39787037037037
	},
	{
		id: 1667,
		time: 1666,
		velocity: 0,
		power: 0.834880880441845,
		road: 16029.67,
		acceleration: -0.111759259259259
	},
	{
		id: 1668,
		time: 1667,
		velocity: 0,
		power: 0,
		road: 16029.67,
		acceleration: 0
	},
	{
		id: 1669,
		time: 1668,
		velocity: 0,
		power: 0,
		road: 16029.67,
		acceleration: 0
	},
	{
		id: 1670,
		time: 1669,
		velocity: 0,
		power: 0,
		road: 16029.67,
		acceleration: 0
	},
	{
		id: 1671,
		time: 1670,
		velocity: 0,
		power: 0,
		road: 16029.67,
		acceleration: 0
	},
	{
		id: 1672,
		time: 1671,
		velocity: 0,
		power: 0,
		road: 16029.67,
		acceleration: 0
	},
	{
		id: 1673,
		time: 1672,
		velocity: 0,
		power: 0,
		road: 16029.67,
		acceleration: 0
	},
	{
		id: 1674,
		time: 1673,
		velocity: 0,
		power: 0,
		road: 16029.67,
		acceleration: 0
	},
	{
		id: 1675,
		time: 1674,
		velocity: 0,
		power: 0,
		road: 16029.67,
		acceleration: 0
	},
	{
		id: 1676,
		time: 1675,
		velocity: 0,
		power: 0,
		road: 16029.67,
		acceleration: 0
	},
	{
		id: 1677,
		time: 1676,
		velocity: 0,
		power: 0,
		road: 16029.67,
		acceleration: 0
	},
	{
		id: 1678,
		time: 1677,
		velocity: 0,
		power: 89.7806819934321,
		road: 16029.8581018518,
		acceleration: 0.376203703703704
	},
	{
		id: 1679,
		time: 1678,
		velocity: 1.12861111111111,
		power: 719.021798980837,
		road: 16030.6495833333,
		acceleration: 0.830555555555556
	},
	{
		id: 1680,
		time: 1679,
		velocity: 2.49166666666667,
		power: 2963.68769464373,
		road: 16032.5946759259,
		acceleration: 1.47666666666667
	},
	{
		id: 1681,
		time: 1680,
		velocity: 4.43,
		power: 4701.80090905244,
		road: 16035.9493981481,
		acceleration: 1.34259259259259
	},
	{
		id: 1682,
		time: 1681,
		velocity: 5.15638888888889,
		power: 4488.93963445017,
		road: 16040.4330555555,
		acceleration: 0.915277777777778
	},
	{
		id: 1683,
		time: 1682,
		velocity: 5.2375,
		power: 3010.55253752066,
		road: 16045.6091666667,
		acceleration: 0.46962962962963
	},
	{
		id: 1684,
		time: 1683,
		velocity: 5.83888888888889,
		power: 3249.00500932856,
		road: 16051.2504166667,
		acceleration: 0.460648148148149
	},
	{
		id: 1685,
		time: 1684,
		velocity: 6.53833333333333,
		power: 5231.23339801093,
		road: 16057.4883333333,
		acceleration: 0.732685185185184
	},
	{
		id: 1686,
		time: 1685,
		velocity: 7.43555555555556,
		power: 5947.31323339534,
		road: 16064.4633796296,
		acceleration: 0.741574074074076
	},
	{
		id: 1687,
		time: 1686,
		velocity: 8.06361111111111,
		power: 7548.69159637328,
		road: 16072.2385648148,
		acceleration: 0.858703703703703
	},
	{
		id: 1688,
		time: 1687,
		velocity: 9.11444444444444,
		power: 7859.97675977567,
		road: 16080.8385185185,
		acceleration: 0.790833333333334
	},
	{
		id: 1689,
		time: 1688,
		velocity: 9.80805555555555,
		power: 6856.74449068466,
		road: 16090.1331018518,
		acceleration: 0.598425925925925
	},
	{
		id: 1690,
		time: 1689,
		velocity: 9.85888888888889,
		power: 3134.73242407268,
		road: 16099.8068981481,
		acceleration: 0.16
	},
	{
		id: 1691,
		time: 1690,
		velocity: 9.59444444444444,
		power: 1181.50011281118,
		road: 16109.5340740741,
		acceleration: -0.0532407407407405
	},
	{
		id: 1692,
		time: 1691,
		velocity: 9.64833333333333,
		power: 2897.52864722156,
		road: 16119.2997685185,
		acceleration: 0.130277777777778
	},
	{
		id: 1693,
		time: 1692,
		velocity: 10.2497222222222,
		power: 5528.09239899268,
		road: 16129.3278240741,
		acceleration: 0.394444444444446
	},
	{
		id: 1694,
		time: 1693,
		velocity: 10.7777777777778,
		power: 6802.02647771875,
		road: 16139.7992592592,
		acceleration: 0.492314814814815
	},
	{
		id: 1695,
		time: 1694,
		velocity: 11.1252777777778,
		power: 5537.12671158348,
		road: 16150.6864814815,
		acceleration: 0.33925925925926
	},
	{
		id: 1696,
		time: 1695,
		velocity: 11.2675,
		power: 4935.57040472316,
		road: 16161.8756481481,
		acceleration: 0.26462962962963
	},
	{
		id: 1697,
		time: 1696,
		velocity: 11.5716666666667,
		power: 5137.77844881267,
		road: 16173.3315740741,
		acceleration: 0.268888888888888
	},
	{
		id: 1698,
		time: 1697,
		velocity: 11.9319444444444,
		power: 6924.49823433502,
		road: 16185.1267592592,
		acceleration: 0.409629629629629
	},
	{
		id: 1699,
		time: 1698,
		velocity: 12.4963888888889,
		power: 7367.10424344018,
		road: 16197.3372222222,
		acceleration: 0.420925925925923
	},
	{
		id: 1700,
		time: 1699,
		velocity: 12.8344444444444,
		power: 6041.87677656434,
		road: 16209.9018981481,
		acceleration: 0.287500000000001
	},
	{
		id: 1701,
		time: 1700,
		velocity: 12.7944444444444,
		power: 2456.97413092946,
		road: 16222.6025,
		acceleration: -0.0156481481481467
	},
	{
		id: 1702,
		time: 1701,
		velocity: 12.4494444444444,
		power: -243.208823078055,
		road: 16235.1768518518,
		acceleration: -0.236851851851853
	},
	{
		id: 1703,
		time: 1702,
		velocity: 12.1238888888889,
		power: -1479.79118163622,
		road: 16247.4633796296,
		acceleration: -0.338796296296294
	},
	{
		id: 1704,
		time: 1703,
		velocity: 11.7780555555556,
		power: -2011.29423310949,
		road: 16259.388287037,
		acceleration: -0.384444444444446
	},
	{
		id: 1705,
		time: 1704,
		velocity: 11.2961111111111,
		power: -1634.19910768892,
		road: 16270.945462963,
		acceleration: -0.351018518518519
	},
	{
		id: 1706,
		time: 1705,
		velocity: 11.0708333333333,
		power: -981.064516887481,
		road: 16282.182037037,
		acceleration: -0.290185185185186
	},
	{
		id: 1707,
		time: 1706,
		velocity: 10.9075,
		power: 711.117249039188,
		road: 16293.2093518518,
		acceleration: -0.128333333333332
	},
	{
		id: 1708,
		time: 1707,
		velocity: 10.9111111111111,
		power: 1807.27789240979,
		road: 16304.1615277778,
		acceleration: -0.0219444444444434
	},
	{
		id: 1709,
		time: 1708,
		velocity: 11.005,
		power: 1663.91326909892,
		road: 16315.0852777778,
		acceleration: -0.0349074074074078
	},
	{
		id: 1710,
		time: 1709,
		velocity: 10.8027777777778,
		power: 1397.23142529801,
		road: 16325.9618981481,
		acceleration: -0.0593518518518525
	},
	{
		id: 1711,
		time: 1710,
		velocity: 10.7330555555556,
		power: -22.1049986736706,
		road: 16336.7114814815,
		acceleration: -0.194722222222223
	},
	{
		id: 1712,
		time: 1711,
		velocity: 10.4208333333333,
		power: -2234.69191776902,
		road: 16347.1569907407,
		acceleration: -0.413425925925926
	},
	{
		id: 1713,
		time: 1712,
		velocity: 9.5625,
		power: -3109.76391040016,
		road: 16357.1405092592,
		acceleration: -0.510555555555555
	},
	{
		id: 1714,
		time: 1713,
		velocity: 9.20138888888889,
		power: -3577.63436458647,
		road: 16366.5809722222,
		acceleration: -0.575555555555555
	},
	{
		id: 1715,
		time: 1714,
		velocity: 8.69416666666667,
		power: -1590.85824945637,
		road: 16375.5541666667,
		acceleration: -0.35898148148148
	},
	{
		id: 1716,
		time: 1715,
		velocity: 8.48555555555556,
		power: -616.392629256054,
		road: 16384.2256481481,
		acceleration: -0.244444444444445
	},
	{
		id: 1717,
		time: 1716,
		velocity: 8.46805555555556,
		power: 378.962426356211,
		road: 16392.7143518518,
		acceleration: -0.121111111111111
	},
	{
		id: 1718,
		time: 1717,
		velocity: 8.33083333333333,
		power: 910.444797764111,
		road: 16401.1158333333,
		acceleration: -0.0533333333333346
	},
	{
		id: 1719,
		time: 1718,
		velocity: 8.32555555555555,
		power: 720.758157202673,
		road: 16409.4527777778,
		acceleration: -0.0757407407407413
	},
	{
		id: 1720,
		time: 1719,
		velocity: 8.24083333333333,
		power: 1056.66034592994,
		road: 16417.7358333333,
		acceleration: -0.0320370370370355
	},
	{
		id: 1721,
		time: 1720,
		velocity: 8.23472222222222,
		power: 667.613518499378,
		road: 16425.9627314815,
		acceleration: -0.080277777777777
	},
	{
		id: 1722,
		time: 1721,
		velocity: 8.08472222222222,
		power: 73.6684683417755,
		road: 16434.072037037,
		acceleration: -0.154907407407407
	},
	{
		id: 1723,
		time: 1722,
		velocity: 7.77611111111111,
		power: -1034.11612176541,
		road: 16441.95375,
		acceleration: -0.300277777777779
	},
	{
		id: 1724,
		time: 1723,
		velocity: 7.33388888888889,
		power: -867.064695664467,
		road: 16449.5453703704,
		acceleration: -0.279907407407407
	},
	{
		id: 1725,
		time: 1724,
		velocity: 7.245,
		power: -300.696052349697,
		road: 16456.8966203704,
		acceleration: -0.200833333333334
	},
	{
		id: 1726,
		time: 1725,
		velocity: 7.17361111111111,
		power: 1078.44038045599,
		road: 16464.1471296296,
		acceleration: -0.000648148148148842
	},
	{
		id: 1727,
		time: 1726,
		velocity: 7.33194444444444,
		power: 3182.44146507378,
		road: 16471.5443055555,
		acceleration: 0.293981481481482
	},
	{
		id: 1728,
		time: 1727,
		velocity: 8.12694444444444,
		power: 9275.2187275412,
		road: 16479.6104629629,
		acceleration: 1.04398148148148
	},
	{
		id: 1729,
		time: 1728,
		velocity: 10.3055555555556,
		power: 13855.7999484119,
		road: 16488.8940277778,
		acceleration: 1.39083333333333
	},
	{
		id: 1730,
		time: 1729,
		velocity: 11.5044444444444,
		power: 16622.1134032581,
		road: 16499.5919444444,
		acceleration: 1.43787037037037
	},
	{
		id: 1731,
		time: 1730,
		velocity: 12.4405555555556,
		power: 11897.5652060029,
		road: 16511.4322685185,
		acceleration: 0.846944444444445
	},
	{
		id: 1732,
		time: 1731,
		velocity: 12.8463888888889,
		power: 9669.23859114832,
		road: 16523.9912962963,
		acceleration: 0.590462962962961
	},
	{
		id: 1733,
		time: 1732,
		velocity: 13.2758333333333,
		power: 10978.7429607472,
		road: 16537.1692592592,
		acceleration: 0.647407407407407
	},
	{
		id: 1734,
		time: 1733,
		velocity: 14.3827777777778,
		power: 11480.8954780927,
		road: 16550.9883796296,
		acceleration: 0.634907407407409
	},
	{
		id: 1735,
		time: 1734,
		velocity: 14.7511111111111,
		power: 14763.8890189435,
		road: 16565.5328703704,
		acceleration: 0.815833333333332
	},
	{
		id: 1736,
		time: 1735,
		velocity: 15.7233333333333,
		power: 10906.976718587,
		road: 16580.7318981481,
		acceleration: 0.49324074074074
	},
	{
		id: 1737,
		time: 1736,
		velocity: 15.8625,
		power: 12203.6361720904,
		road: 16596.4502777778,
		acceleration: 0.545462962962963
	},
	{
		id: 1738,
		time: 1737,
		velocity: 16.3875,
		power: 8709.1210932121,
		road: 16612.5865277778,
		acceleration: 0.290277777777778
	},
	{
		id: 1739,
		time: 1738,
		velocity: 16.5941666666667,
		power: 10308.9194973797,
		road: 16629.0550462963,
		acceleration: 0.374259259259262
	},
	{
		id: 1740,
		time: 1739,
		velocity: 16.9852777777778,
		power: 7293.04846875939,
		road: 16645.7957407407,
		acceleration: 0.170092592592592
	},
	{
		id: 1741,
		time: 1740,
		velocity: 16.8977777777778,
		power: 5311.47300215323,
		road: 16662.6425925926,
		acceleration: 0.0422222222222217
	},
	{
		id: 1742,
		time: 1741,
		velocity: 16.7208333333333,
		power: 2145.25245967773,
		road: 16679.4341666667,
		acceleration: -0.152777777777782
	},
	{
		id: 1743,
		time: 1742,
		velocity: 16.5269444444444,
		power: 2350.18833441311,
		road: 16696.0813425926,
		acceleration: -0.136018518518515
	},
	{
		id: 1744,
		time: 1743,
		velocity: 16.4897222222222,
		power: 2769.87160244455,
		road: 16712.6074537037,
		acceleration: -0.106111111111108
	},
	{
		id: 1745,
		time: 1744,
		velocity: 16.4025,
		power: 2914.32705919401,
		road: 16729.0335185185,
		acceleration: -0.0939814814814852
	},
	{
		id: 1746,
		time: 1745,
		velocity: 16.245,
		power: 2300.20960263625,
		road: 16745.3475925926,
		acceleration: -0.129999999999999
	},
	{
		id: 1747,
		time: 1746,
		velocity: 16.0997222222222,
		power: 4015.67649776596,
		road: 16761.5879166667,
		acceleration: -0.0174999999999983
	},
	{
		id: 1748,
		time: 1747,
		velocity: 16.35,
		power: 5524.3949494724,
		road: 16777.8587037037,
		acceleration: 0.0784259259259272
	},
	{
		id: 1749,
		time: 1748,
		velocity: 16.4802777777778,
		power: 7658.57971809396,
		road: 16794.2730092592,
		acceleration: 0.208611111111107
	},
	{
		id: 1750,
		time: 1749,
		velocity: 16.7255555555556,
		power: 6871.71165377517,
		road: 16810.8668055555,
		acceleration: 0.150370370370375
	},
	{
		id: 1751,
		time: 1750,
		velocity: 16.8011111111111,
		power: 6582.74906661852,
		road: 16827.5988425926,
		acceleration: 0.126111111111111
	},
	{
		id: 1752,
		time: 1751,
		velocity: 16.8586111111111,
		power: 6210.87547568726,
		road: 16844.4430092592,
		acceleration: 0.0981481481481445
	},
	{
		id: 1753,
		time: 1752,
		velocity: 17.02,
		power: 5790.82055098649,
		road: 16861.3706018518,
		acceleration: 0.0687037037037044
	},
	{
		id: 1754,
		time: 1753,
		velocity: 17.0072222222222,
		power: 5940.33270150757,
		road: 16878.3700462963,
		acceleration: 0.0749999999999993
	},
	{
		id: 1755,
		time: 1754,
		velocity: 17.0836111111111,
		power: 5090.3900205559,
		road: 16895.4174537037,
		acceleration: 0.0209259259259262
	},
	{
		id: 1756,
		time: 1755,
		velocity: 17.0827777777778,
		power: 3599.27979117403,
		road: 16912.440462963,
		acceleration: -0.0697222222222251
	},
	{
		id: 1757,
		time: 1756,
		velocity: 16.7980555555556,
		power: 2146.01554981003,
		road: 16929.3506481481,
		acceleration: -0.155925925925921
	},
	{
		id: 1758,
		time: 1757,
		velocity: 16.6158333333333,
		power: -1202.65766367597,
		road: 16946.0032407407,
		acceleration: -0.359259259259261
	},
	{
		id: 1759,
		time: 1758,
		velocity: 16.005,
		power: -1328.17404240471,
		road: 16962.2950462963,
		acceleration: -0.362314814814816
	},
	{
		id: 1760,
		time: 1759,
		velocity: 15.7111111111111,
		power: -3150.89973084032,
		road: 16978.1671296296,
		acceleration: -0.477129629629628
	},
	{
		id: 1761,
		time: 1760,
		velocity: 15.1844444444444,
		power: -2421.82847889708,
		road: 16993.5877314815,
		acceleration: -0.425833333333335
	},
	{
		id: 1762,
		time: 1761,
		velocity: 14.7275,
		power: -2458.90573474177,
		road: 17008.5825,
		acceleration: -0.425833333333332
	},
	{
		id: 1763,
		time: 1762,
		velocity: 14.4336111111111,
		power: -2599.50413197945,
		road: 17023.1474074074,
		acceleration: -0.433888888888891
	},
	{
		id: 1764,
		time: 1763,
		velocity: 13.8827777777778,
		power: -2197.91402188513,
		road: 17037.2938888889,
		acceleration: -0.402962962962961
	},
	{
		id: 1765,
		time: 1764,
		velocity: 13.5186111111111,
		power: -2752.53179783926,
		road: 17051.017037037,
		acceleration: -0.443703703703704
	},
	{
		id: 1766,
		time: 1765,
		velocity: 13.1025,
		power: -1500.71143321802,
		road: 17064.3455555555,
		acceleration: -0.345555555555555
	},
	{
		id: 1767,
		time: 1766,
		velocity: 12.8461111111111,
		power: -1048.17014466182,
		road: 17077.3476851852,
		acceleration: -0.307222222222224
	},
	{
		id: 1768,
		time: 1767,
		velocity: 12.5969444444444,
		power: -378.772456415052,
		road: 17090.07125,
		acceleration: -0.249907407407406
	},
	{
		id: 1769,
		time: 1768,
		velocity: 12.3527777777778,
		power: -663.774659352001,
		road: 17102.5344444444,
		acceleration: -0.270833333333334
	},
	{
		id: 1770,
		time: 1769,
		velocity: 12.0336111111111,
		power: -21.9207472485367,
		road: 17114.755462963,
		acceleration: -0.213518518518519
	},
	{
		id: 1771,
		time: 1770,
		velocity: 11.9563888888889,
		power: 8614.54303772493,
		road: 17127.1277314815,
		acceleration: 0.51601851851852
	},
	{
		id: 1772,
		time: 1771,
		velocity: 13.9008333333333,
		power: 14447.6910472408,
		road: 17140.2237962963,
		acceleration: 0.931574074074073
	},
	{
		id: 1773,
		time: 1772,
		velocity: 14.8283333333333,
		power: 21758.1422763598,
		road: 17154.4644907407,
		acceleration: 1.35768518518519
	},
	{
		id: 1774,
		time: 1773,
		velocity: 16.0294444444444,
		power: 14223.9776287626,
		road: 17169.7416203704,
		acceleration: 0.715185185185186
	},
	{
		id: 1775,
		time: 1774,
		velocity: 16.0463888888889,
		power: 9529.56472660575,
		road: 17185.5572685185,
		acceleration: 0.361851851851851
	},
	{
		id: 1776,
		time: 1775,
		velocity: 15.9138888888889,
		power: 2177.19560735079,
		road: 17201.4900462963,
		acceleration: -0.127592592592595
	},
	{
		id: 1777,
		time: 1776,
		velocity: 15.6466666666667,
		power: 1854.53537714961,
		road: 17217.2863888889,
		acceleration: -0.145277777777775
	},
	{
		id: 1778,
		time: 1777,
		velocity: 15.6105555555556,
		power: 2775.77276331933,
		road: 17232.9696296296,
		acceleration: -0.080925925925925
	},
	{
		id: 1779,
		time: 1778,
		velocity: 15.6711111111111,
		power: 3775.0030486373,
		road: 17248.6060648148,
		acceleration: -0.0126851851851875
	},
	{
		id: 1780,
		time: 1779,
		velocity: 15.6086111111111,
		power: 3188.53488389009,
		road: 17264.2106944444,
		acceleration: -0.0509259259259256
	},
	{
		id: 1781,
		time: 1780,
		velocity: 15.4577777777778,
		power: 1394.61157131072,
		road: 17279.7055555555,
		acceleration: -0.16861111111111
	},
	{
		id: 1782,
		time: 1781,
		velocity: 15.1652777777778,
		power: 2147.33568841556,
		road: 17295.0591203704,
		acceleration: -0.113981481481483
	},
	{
		id: 1783,
		time: 1782,
		velocity: 15.2666666666667,
		power: 3771.85225284965,
		road: 17310.3550462963,
		acceleration: -0.00129629629629413
	},
	{
		id: 1784,
		time: 1783,
		velocity: 15.4538888888889,
		power: 5438.84195981187,
		road: 17325.7055555555,
		acceleration: 0.110462962962961
	},
	{
		id: 1785,
		time: 1784,
		velocity: 15.4966666666667,
		power: 6490.70049220573,
		road: 17341.1990740741,
		acceleration: 0.175555555555556
	},
	{
		id: 1786,
		time: 1785,
		velocity: 15.7933333333333,
		power: 7196.31786232852,
		road: 17356.8872222222,
		acceleration: 0.213703703703704
	},
	{
		id: 1787,
		time: 1786,
		velocity: 16.095,
		power: 6966.89183147402,
		road: 17372.7766666667,
		acceleration: 0.18888888888889
	},
	{
		id: 1788,
		time: 1787,
		velocity: 16.0633333333333,
		power: 5968.85193969189,
		road: 17388.8188888889,
		acceleration: 0.116666666666665
	},
	{
		id: 1789,
		time: 1788,
		velocity: 16.1433333333333,
		power: 4345.95380619717,
		road: 17404.92375,
		acceleration: 0.00861111111111157
	},
	{
		id: 1790,
		time: 1789,
		velocity: 16.1208333333333,
		power: 4003.90038546226,
		road: 17421.0261574074,
		acceleration: -0.01351851851852
	},
	{
		id: 1791,
		time: 1790,
		velocity: 16.0227777777778,
		power: 3277.71683252811,
		road: 17437.092037037,
		acceleration: -0.0595370370370354
	},
	{
		id: 1792,
		time: 1791,
		velocity: 15.9647222222222,
		power: 3722.95180240476,
		road: 17453.1136111111,
		acceleration: -0.0290740740740745
	},
	{
		id: 1793,
		time: 1792,
		velocity: 16.0336111111111,
		power: 6023.19162028517,
		road: 17469.1802314815,
		acceleration: 0.119166666666668
	},
	{
		id: 1794,
		time: 1793,
		velocity: 16.3802777777778,
		power: 8103.1257087124,
		road: 17485.4290740741,
		acceleration: 0.245277777777776
	},
	{
		id: 1795,
		time: 1794,
		velocity: 16.7005555555556,
		power: 7884.85057100897,
		road: 17501.9104166667,
		acceleration: 0.21972222222222
	},
	{
		id: 1796,
		time: 1795,
		velocity: 16.6927777777778,
		power: 6435.16361996051,
		road: 17518.5618055555,
		acceleration: 0.120370370370374
	},
	{
		id: 1797,
		time: 1796,
		velocity: 16.7413888888889,
		power: 5243.63192444822,
		road: 17535.2945833333,
		acceleration: 0.0424074074074028
	},
	{
		id: 1798,
		time: 1797,
		velocity: 16.8277777777778,
		power: 6754.97335139164,
		road: 17552.1150462963,
		acceleration: 0.132962962962964
	},
	{
		id: 1799,
		time: 1798,
		velocity: 17.0916666666667,
		power: 6302.457091368,
		road: 17569.0519444444,
		acceleration: 0.0999074074074073
	},
	{
		id: 1800,
		time: 1799,
		velocity: 17.0411111111111,
		power: 6415.81234818232,
		road: 17586.0900925926,
		acceleration: 0.102592592592593
	},
	{
		id: 1801,
		time: 1800,
		velocity: 17.1355555555556,
		power: 5629.36716027554,
		road: 17603.2051851852,
		acceleration: 0.0512962962962966
	},
	{
		id: 1802,
		time: 1801,
		velocity: 17.2455555555556,
		power: 5595.97640153514,
		road: 17620.3695833333,
		acceleration: 0.0473148148148148
	},
	{
		id: 1803,
		time: 1802,
		velocity: 17.1830555555556,
		power: 5013.15032742652,
		road: 17637.5630092592,
		acceleration: 0.0107407407407436
	},
	{
		id: 1804,
		time: 1803,
		velocity: 17.1677777777778,
		power: 5420.53658790518,
		road: 17654.7791203704,
		acceleration: 0.0346296296296273
	},
	{
		id: 1805,
		time: 1804,
		velocity: 17.3494444444444,
		power: 5777.74585720662,
		road: 17672.0398148148,
		acceleration: 0.0545370370370364
	},
	{
		id: 1806,
		time: 1805,
		velocity: 17.3466666666667,
		power: 6153.47421978863,
		road: 17689.3650925926,
		acceleration: 0.07462962962963
	},
	{
		id: 1807,
		time: 1806,
		velocity: 17.3916666666667,
		power: 5114.66384438772,
		road: 17706.7328703704,
		acceleration: 0.0103703703703708
	},
	{
		id: 1808,
		time: 1807,
		velocity: 17.3805555555556,
		power: 5845.05337554449,
		road: 17724.1323611111,
		acceleration: 0.0530555555555559
	},
	{
		id: 1809,
		time: 1808,
		velocity: 17.5058333333333,
		power: 4191.39007729048,
		road: 17741.5351851852,
		acceleration: -0.0463888888888881
	},
	{
		id: 1810,
		time: 1809,
		velocity: 17.2525,
		power: 1664.39446068328,
		road: 17758.8172685185,
		acceleration: -0.195092592592594
	},
	{
		id: 1811,
		time: 1810,
		velocity: 16.7952777777778,
		power: -236.7855580821,
		road: 17775.8490740741,
		acceleration: -0.305462962962963
	},
	{
		id: 1812,
		time: 1811,
		velocity: 16.5894444444444,
		power: -1677.08930626616,
		road: 17792.5334259259,
		acceleration: -0.389444444444443
	},
	{
		id: 1813,
		time: 1812,
		velocity: 16.0841666666667,
		power: 520.323489206235,
		road: 17808.9004629629,
		acceleration: -0.245185185185186
	},
	{
		id: 1814,
		time: 1813,
		velocity: 16.0597222222222,
		power: 1369.00618575724,
		road: 17825.0519907407,
		acceleration: -0.185833333333335
	},
	{
		id: 1815,
		time: 1814,
		velocity: 16.0319444444444,
		power: 5499.50376584661,
		road: 17841.1524537037,
		acceleration: 0.0837037037037049
	},
	{
		id: 1816,
		time: 1815,
		velocity: 16.3352777777778,
		power: 6407.11161326243,
		road: 17857.3636574074,
		acceleration: 0.137777777777782
	},
	{
		id: 1817,
		time: 1816,
		velocity: 16.4730555555556,
		power: 7375.93057054137,
		road: 17873.7399537037,
		acceleration: 0.192407407407405
	},
	{
		id: 1818,
		time: 1817,
		velocity: 16.6091666666667,
		power: 6701.01461760077,
		road: 17890.2833796296,
		acceleration: 0.141851851851854
	},
	{
		id: 1819,
		time: 1818,
		velocity: 16.7608333333333,
		power: 5646.70501574751,
		road: 17906.9331944444,
		acceleration: 0.0709259259259269
	},
	{
		id: 1820,
		time: 1819,
		velocity: 16.6858333333333,
		power: 6801.86791789451,
		road: 17923.6878703704,
		acceleration: 0.138796296296295
	},
	{
		id: 1821,
		time: 1820,
		velocity: 17.0255555555556,
		power: 8133.75141837493,
		road: 17940.6185648148,
		acceleration: 0.213240740740741
	},
	{
		id: 1822,
		time: 1821,
		velocity: 17.4005555555556,
		power: 10464.2806744827,
		road: 17957.8265277778,
		acceleration: 0.341296296296296
	},
	{
		id: 1823,
		time: 1822,
		velocity: 17.7097222222222,
		power: 11535.2649773205,
		road: 17975.3974537037,
		acceleration: 0.384629629629629
	},
	{
		id: 1824,
		time: 1823,
		velocity: 18.1794444444444,
		power: 10369.6069719999,
		road: 17993.3090277778,
		acceleration: 0.296666666666667
	},
	{
		id: 1825,
		time: 1824,
		velocity: 18.2905555555556,
		power: 7892.91454101674,
		road: 18011.44,
		acceleration: 0.142129629629629
	},
	{
		id: 1826,
		time: 1825,
		velocity: 18.1361111111111,
		power: 4540.59360373505,
		road: 18029.6157407407,
		acceleration: -0.0525925925925925
	},
	{
		id: 1827,
		time: 1826,
		velocity: 18.0216666666667,
		power: 3065.71116414504,
		road: 18047.6979166667,
		acceleration: -0.134537037037035
	},
	{
		id: 1828,
		time: 1827,
		velocity: 17.8869444444444,
		power: 3833.21334187329,
		road: 18065.669537037,
		acceleration: -0.086574074074079
	},
	{
		id: 1829,
		time: 1828,
		velocity: 17.8763888888889,
		power: 4526.51548356434,
		road: 18083.5758796296,
		acceleration: -0.0439814814814774
	},
	{
		id: 1830,
		time: 1829,
		velocity: 17.8897222222222,
		power: 4305.34381500071,
		road: 18101.4326388889,
		acceleration: -0.0551851851851843
	},
	{
		id: 1831,
		time: 1830,
		velocity: 17.7213888888889,
		power: 3687.98506353463,
		road: 18119.2173148148,
		acceleration: -0.0889814814814862
	},
	{
		id: 1832,
		time: 1831,
		velocity: 17.6094444444444,
		power: 3242.84170873489,
		road: 18136.9014814815,
		acceleration: -0.112037037037037
	},
	{
		id: 1833,
		time: 1832,
		velocity: 17.5536111111111,
		power: 4580.65444778815,
		road: 18154.5143981481,
		acceleration: -0.0304629629629645
	},
	{
		id: 1834,
		time: 1833,
		velocity: 17.63,
		power: 5613.88380938016,
		road: 18172.1275,
		acceleration: 0.0308333333333373
	},
	{
		id: 1835,
		time: 1834,
		velocity: 17.7019444444444,
		power: 7140.86372726641,
		road: 18189.8150925926,
		acceleration: 0.118148148148148
	},
	{
		id: 1836,
		time: 1835,
		velocity: 17.9080555555556,
		power: 6708.7772041088,
		road: 18207.6058796296,
		acceleration: 0.0882407407407406
	},
	{
		id: 1837,
		time: 1836,
		velocity: 17.8947222222222,
		power: 6137.421916718,
		road: 18225.466712963,
		acceleration: 0.051851851851854
	},
	{
		id: 1838,
		time: 1837,
		velocity: 17.8575,
		power: 3339.20516894097,
		road: 18243.2980092593,
		acceleration: -0.110925925925926
	},
	{
		id: 1839,
		time: 1838,
		velocity: 17.5752777777778,
		power: 2641.9154705582,
		road: 18260.9998148148,
		acceleration: -0.148055555555555
	},
	{
		id: 1840,
		time: 1839,
		velocity: 17.4505555555556,
		power: 2524.54441652697,
		road: 18278.5522222222,
		acceleration: -0.150740740740741
	},
	{
		id: 1841,
		time: 1840,
		velocity: 17.4052777777778,
		power: 3141.9721476359,
		road: 18295.9742592593,
		acceleration: -0.110000000000003
	},
	{
		id: 1842,
		time: 1841,
		velocity: 17.2452777777778,
		power: 2782.90727645705,
		road: 18313.2772685185,
		acceleration: -0.128055555555555
	},
	{
		id: 1843,
		time: 1842,
		velocity: 17.0663888888889,
		power: 1872.13709560763,
		road: 18330.426712963,
		acceleration: -0.179074074074073
	},
	{
		id: 1844,
		time: 1843,
		velocity: 16.8680555555556,
		power: 1956.48541994503,
		road: 18347.4019444444,
		acceleration: -0.169351851851854
	},
	{
		id: 1845,
		time: 1844,
		velocity: 16.7372222222222,
		power: 1948.36275547645,
		road: 18364.2097685185,
		acceleration: -0.165462962962962
	},
	{
		id: 1846,
		time: 1845,
		velocity: 16.57,
		power: 2650.82713803512,
		road: 18380.8760185185,
		acceleration: -0.117685185185184
	},
	{
		id: 1847,
		time: 1846,
		velocity: 16.515,
		power: 2442.5844291409,
		road: 18397.4197685185,
		acceleration: -0.127314814814817
	},
	{
		id: 1848,
		time: 1847,
		velocity: 16.3552777777778,
		power: 2861.52054679034,
		road: 18413.8511111111,
		acceleration: -0.0975000000000001
	},
	{
		id: 1849,
		time: 1848,
		velocity: 16.2775,
		power: 2515.63420287948,
		road: 18430.175462963,
		acceleration: -0.116481481481479
	},
	{
		id: 1850,
		time: 1849,
		velocity: 16.1655555555556,
		power: 3248.52247189516,
		road: 18446.4082407407,
		acceleration: -0.06666666666667
	},
	{
		id: 1851,
		time: 1850,
		velocity: 16.1552777777778,
		power: 2734.16330344598,
		road: 18462.5589814815,
		acceleration: -0.0974074074074061
	},
	{
		id: 1852,
		time: 1851,
		velocity: 15.9852777777778,
		power: 3108.88697257224,
		road: 18478.6257407407,
		acceleration: -0.0705555555555542
	},
	{
		id: 1853,
		time: 1852,
		velocity: 15.9538888888889,
		power: 3595.20738978081,
		road: 18494.6386574074,
		acceleration: -0.0371296296296304
	},
	{
		id: 1854,
		time: 1853,
		velocity: 16.0438888888889,
		power: 3441.50579513149,
		road: 18510.6100925926,
		acceleration: -0.0458333333333343
	},
	{
		id: 1855,
		time: 1854,
		velocity: 15.8477777777778,
		power: 2378.46485822429,
		road: 18526.5019907407,
		acceleration: -0.113240740740741
	},
	{
		id: 1856,
		time: 1855,
		velocity: 15.6141666666667,
		power: 1683.31992490005,
		road: 18542.2594444444,
		acceleration: -0.155648148148147
	},
	{
		id: 1857,
		time: 1856,
		velocity: 15.5769444444444,
		power: 1765.37397860336,
		road: 18557.8658796296,
		acceleration: -0.14638888888889
	},
	{
		id: 1858,
		time: 1857,
		velocity: 15.4086111111111,
		power: 1846.61411603281,
		road: 18573.330462963,
		acceleration: -0.137314814814815
	},
	{
		id: 1859,
		time: 1858,
		velocity: 15.2022222222222,
		power: 1135.54491050773,
		road: 18588.635462963,
		acceleration: -0.181851851851853
	},
	{
		id: 1860,
		time: 1859,
		velocity: 15.0313888888889,
		power: 1309.69126051729,
		road: 18603.7665740741,
		acceleration: -0.165925925925922
	},
	{
		id: 1861,
		time: 1860,
		velocity: 14.9108333333333,
		power: 1620.84512794187,
		road: 18618.7443981481,
		acceleration: -0.14064814814815
	},
	{
		id: 1862,
		time: 1861,
		velocity: 14.7802777777778,
		power: 1657.0446424768,
		road: 18633.584537037,
		acceleration: -0.134722222222223
	},
	{
		id: 1863,
		time: 1862,
		velocity: 14.6272222222222,
		power: 1560.34309982154,
		road: 18648.2881944444,
		acceleration: -0.13824074074074
	},
	{
		id: 1864,
		time: 1863,
		velocity: 14.4961111111111,
		power: 1111.38584547464,
		road: 18662.8393055556,
		acceleration: -0.166851851851852
	},
	{
		id: 1865,
		time: 1864,
		velocity: 14.2797222222222,
		power: 506.445051029103,
		road: 18677.2035648148,
		acceleration: -0.206851851851852
	},
	{
		id: 1866,
		time: 1865,
		velocity: 14.0066666666667,
		power: 577.987521002031,
		road: 18691.3655092593,
		acceleration: -0.197777777777777
	},
	{
		id: 1867,
		time: 1866,
		velocity: 13.9027777777778,
		power: -463.710279653112,
		road: 18705.2927314815,
		acceleration: -0.271666666666667
	},
	{
		id: 1868,
		time: 1867,
		velocity: 13.4647222222222,
		power: -1103.85910008566,
		road: 18718.9256018518,
		acceleration: -0.317037037037037
	},
	{
		id: 1869,
		time: 1868,
		velocity: 13.0555555555556,
		power: -2669.54277897627,
		road: 18732.1812037037,
		acceleration: -0.4375
	},
	{
		id: 1870,
		time: 1869,
		velocity: 12.5902777777778,
		power: -1945.27619773134,
		road: 18745.0284722222,
		acceleration: -0.379166666666666
	},
	{
		id: 1871,
		time: 1870,
		velocity: 12.3272222222222,
		power: -2032.27286265418,
		road: 18757.4931944444,
		acceleration: -0.385925925925925
	},
	{
		id: 1872,
		time: 1871,
		velocity: 11.8977777777778,
		power: -6958.6873985621,
		road: 18769.3539814815,
		acceleration: -0.821944444444446
	},
	{
		id: 1873,
		time: 1872,
		velocity: 10.1244444444444,
		power: -9045.66062599996,
		road: 18780.2718518518,
		acceleration: -1.06388888888889
	},
	{
		id: 1874,
		time: 1873,
		velocity: 9.13555555555556,
		power: -9311.65603744169,
		road: 18790.0680555555,
		acceleration: -1.17944444444444
	},
	{
		id: 1875,
		time: 1874,
		velocity: 8.35944444444444,
		power: -5205.5722320901,
		road: 18798.8786574074,
		acceleration: -0.791759259259258
	},
	{
		id: 1876,
		time: 1875,
		velocity: 7.74916666666667,
		power: -4605.90800600843,
		road: 18806.9101851852,
		acceleration: -0.766388888888889
	},
	{
		id: 1877,
		time: 1876,
		velocity: 6.83638888888889,
		power: -4613.52593637973,
		road: 18814.1448611111,
		acceleration: -0.827314814814815
	},
	{
		id: 1878,
		time: 1877,
		velocity: 5.8775,
		power: -6584.44394081439,
		road: 18820.3315740741,
		acceleration: -1.26861111111111
	},
	{
		id: 1879,
		time: 1878,
		velocity: 3.94333333333333,
		power: -6730.69639774771,
		road: 18825.0654166667,
		acceleration: -1.63712962962963
	},
	{
		id: 1880,
		time: 1879,
		velocity: 1.925,
		power: -4397.70500442985,
		road: 18828.1671296296,
		acceleration: -1.62712962962963
	},
	{
		id: 1881,
		time: 1880,
		velocity: 0.996111111111111,
		power: -1833.04750263049,
		road: 18829.7980555555,
		acceleration: -1.31444444444444
	},
	{
		id: 1882,
		time: 1881,
		velocity: 0,
		power: -317.958963547432,
		road: 18830.4509259259,
		acceleration: -0.641666666666667
	},
	{
		id: 1883,
		time: 1882,
		velocity: 0,
		power: -32.1650102988954,
		road: 18830.6169444444,
		acceleration: -0.332037037037037
	},
	{
		id: 1884,
		time: 1883,
		velocity: 0,
		power: 0,
		road: 18830.6169444444,
		acceleration: 0
	},
	{
		id: 1885,
		time: 1884,
		velocity: 0,
		power: 93.9578802413861,
		road: 18830.81,
		acceleration: 0.386111111111111
	},
	{
		id: 1886,
		time: 1885,
		velocity: 1.15833333333333,
		power: 1190.73862551829,
		road: 18831.7797222222,
		acceleration: 1.16722222222222
	},
	{
		id: 1887,
		time: 1886,
		velocity: 3.50166666666667,
		power: 3091.30204825712,
		road: 18834.0012962963,
		acceleration: 1.33648148148148
	},
	{
		id: 1888,
		time: 1887,
		velocity: 4.00944444444444,
		power: 3217.52726681964,
		road: 18837.332962963,
		acceleration: 0.883703703703704
	},
	{
		id: 1889,
		time: 1888,
		velocity: 3.80944444444444,
		power: 343.34097367015,
		road: 18841.0869907407,
		acceleration: -0.0389814814814811
	},
	{
		id: 1890,
		time: 1889,
		velocity: 3.38472222222222,
		power: -1007.83179823286,
		road: 18844.6033796296,
		acceleration: -0.436296296296296
	},
	{
		id: 1891,
		time: 1890,
		velocity: 2.70055555555556,
		power: -970.355346415181,
		road: 18847.6684722222,
		acceleration: -0.466296296296297
	},
	{
		id: 1892,
		time: 1891,
		velocity: 2.41055555555556,
		power: -560.053785494591,
		road: 18850.3235648148,
		acceleration: -0.353703703703704
	},
	{
		id: 1893,
		time: 1892,
		velocity: 2.32361111111111,
		power: -138.409144296533,
		road: 18852.7058796296,
		acceleration: -0.191851851851852
	},
	{
		id: 1894,
		time: 1893,
		velocity: 2.125,
		power: 202.689939820449,
		road: 18854.9742129629,
		acceleration: -0.036111111111111
	},
	{
		id: 1895,
		time: 1894,
		velocity: 2.30222222222222,
		power: 1658.9301134649,
		road: 18857.5044907407,
		acceleration: 0.56
	},
	{
		id: 1896,
		time: 1895,
		velocity: 4.00361111111111,
		power: 3474.2589490046,
		road: 18860.8028240741,
		acceleration: 0.976111111111111
	},
	{
		id: 1897,
		time: 1896,
		velocity: 5.05333333333333,
		power: 5478.79958123019,
		road: 18865.1791203704,
		acceleration: 1.17981481481481
	},
	{
		id: 1898,
		time: 1897,
		velocity: 5.84166666666667,
		power: 3690.89329357855,
		road: 18870.4427777778,
		acceleration: 0.594907407407407
	},
	{
		id: 1899,
		time: 1898,
		velocity: 5.78833333333333,
		power: 3363.19261029437,
		road: 18876.2361111111,
		acceleration: 0.464444444444445
	},
	{
		id: 1900,
		time: 1899,
		velocity: 6.44666666666667,
		power: 3193.4668679642,
		road: 18882.457037037,
		acceleration: 0.39074074074074
	},
	{
		id: 1901,
		time: 1900,
		velocity: 7.01388888888889,
		power: 5785.95686748907,
		road: 18889.2447685185,
		acceleration: 0.742870370370371
	},
	{
		id: 1902,
		time: 1901,
		velocity: 8.01694444444444,
		power: 7523.62825883765,
		road: 18896.8441666667,
		acceleration: 0.880462962962961
	},
	{
		id: 1903,
		time: 1902,
		velocity: 9.08805555555555,
		power: 10478.3706772786,
		road: 18905.4394444444,
		acceleration: 1.1112962962963
	},
	{
		id: 1904,
		time: 1903,
		velocity: 10.3477777777778,
		power: 13747.9814039447,
		road: 18915.2360185185,
		acceleration: 1.2912962962963
	},
	{
		id: 1905,
		time: 1904,
		velocity: 11.8908333333333,
		power: 12013.73734098,
		road: 18926.1577777778,
		acceleration: 0.959074074074074
	},
	{
		id: 1906,
		time: 1905,
		velocity: 11.9652777777778,
		power: 7102.12636435534,
		road: 18937.7777314815,
		acceleration: 0.437314814814815
	},
	{
		id: 1907,
		time: 1906,
		velocity: 11.6597222222222,
		power: 1624.769092334,
		road: 18949.5854629629,
		acceleration: -0.0617592592592597
	},
	{
		id: 1908,
		time: 1907,
		velocity: 11.7055555555556,
		power: 4226.1688437712,
		road: 18961.4457407407,
		acceleration: 0.166851851851852
	},
	{
		id: 1909,
		time: 1908,
		velocity: 12.4658333333333,
		power: 8496.50741322387,
		road: 18973.6486574074,
		acceleration: 0.518425925925925
	},
	{
		id: 1910,
		time: 1909,
		velocity: 13.215,
		power: 11653.9049567327,
		road: 18986.4766203703,
		acceleration: 0.731666666666667
	},
	{
		id: 1911,
		time: 1910,
		velocity: 13.9005555555556,
		power: 10911.2075899865,
		road: 18999.9785185185,
		acceleration: 0.616203703703704
	},
	{
		id: 1912,
		time: 1911,
		velocity: 14.3144444444444,
		power: 9288.61405681466,
		road: 19014.0156944444,
		acceleration: 0.45435185185185
	},
	{
		id: 1913,
		time: 1912,
		velocity: 14.5780555555556,
		power: 7152.6476080038,
		road: 19028.4178240741,
		acceleration: 0.275555555555556
	},
	{
		id: 1914,
		time: 1913,
		velocity: 14.7272222222222,
		power: 7254.03374012851,
		road: 19043.092037037,
		acceleration: 0.268611111111111
	},
	{
		id: 1915,
		time: 1914,
		velocity: 15.1202777777778,
		power: 10091.1453580271,
		road: 19058.1243981481,
		acceleration: 0.447685185185186
	},
	{
		id: 1916,
		time: 1915,
		velocity: 15.9211111111111,
		power: 12138.1207907317,
		road: 19073.6576388889,
		acceleration: 0.554074074074073
	},
	{
		id: 1917,
		time: 1916,
		velocity: 16.3894444444444,
		power: 12074.5102151456,
		road: 19089.7244444444,
		acceleration: 0.513055555555553
	},
	{
		id: 1918,
		time: 1917,
		velocity: 16.6594444444444,
		power: 8222.0768702546,
		road: 19106.1692592592,
		acceleration: 0.242962962962963
	},
	{
		id: 1919,
		time: 1918,
		velocity: 16.65,
		power: 7159.12905710068,
		road: 19122.8185185185,
		acceleration: 0.165925925925926
	},
	{
		id: 1920,
		time: 1919,
		velocity: 16.8872222222222,
		power: 9643.95482727277,
		road: 19139.7051851852,
		acceleration: 0.308888888888891
	},
	{
		id: 1921,
		time: 1920,
		velocity: 17.5861111111111,
		power: 12148.8789955863,
		road: 19156.9664351852,
		acceleration: 0.440277777777776
	},
	{
		id: 1922,
		time: 1921,
		velocity: 17.9708333333333,
		power: 15749.5928930429,
		road: 19174.7574074074,
		acceleration: 0.619166666666668
	},
	{
		id: 1923,
		time: 1922,
		velocity: 18.7447222222222,
		power: 13508.6577312078,
		road: 19193.0845833333,
		acceleration: 0.453240740740739
	},
	{
		id: 1924,
		time: 1923,
		velocity: 18.9458333333333,
		power: 12992.2582410383,
		road: 19211.8373611111,
		acceleration: 0.397962962962964
	},
	{
		id: 1925,
		time: 1924,
		velocity: 19.1647222222222,
		power: 8890.59257101855,
		road: 19230.8673148148,
		acceleration: 0.156388888888891
	},
	{
		id: 1926,
		time: 1925,
		velocity: 19.2138888888889,
		power: 6151.38967049561,
		road: 19249.9769444444,
		acceleration: 0.00296296296296106
	},
	{
		id: 1927,
		time: 1926,
		velocity: 18.9547222222222,
		power: 3111.35438773439,
		road: 19269.0076851852,
		acceleration: -0.160740740740742
	},
	{
		id: 1928,
		time: 1927,
		velocity: 18.6825,
		power: 2227.34893008272,
		road: 19287.8559722222,
		acceleration: -0.204166666666666
	},
	{
		id: 1929,
		time: 1928,
		velocity: 18.6013888888889,
		power: 2713.1056784014,
		road: 19306.5162962963,
		acceleration: -0.171759259259261
	},
	{
		id: 1930,
		time: 1929,
		velocity: 18.4394444444444,
		power: 2950.41522028968,
		road: 19325.0139351852,
		acceleration: -0.153611111111108
	},
	{
		id: 1931,
		time: 1930,
		velocity: 18.2216666666667,
		power: -140.544603743978,
		road: 19343.2731018518,
		acceleration: -0.323333333333334
	},
	{
		id: 1932,
		time: 1931,
		velocity: 17.6313888888889,
		power: 81.3695513108378,
		road: 19361.2185185185,
		acceleration: -0.304166666666667
	},
	{
		id: 1933,
		time: 1932,
		velocity: 17.5269444444444,
		power: 710.516782823118,
		road: 19378.88125,
		acceleration: -0.2612037037037
	},
	{
		id: 1934,
		time: 1933,
		velocity: 17.4380555555556,
		power: 2607.82556013132,
		road: 19396.3418055555,
		acceleration: -0.14314814814815
	},
	{
		id: 1935,
		time: 1934,
		velocity: 17.2019444444444,
		power: 1725.71131419394,
		road: 19413.6349537037,
		acceleration: -0.19166666666667
	},
	{
		id: 1936,
		time: 1935,
		velocity: 16.9519444444444,
		power: 1477.05405873756,
		road: 19430.7313425926,
		acceleration: -0.201851851851849
	},
	{
		id: 1937,
		time: 1936,
		velocity: 16.8325,
		power: 1509.14481693963,
		road: 19447.6293055555,
		acceleration: -0.195000000000004
	},
	{
		id: 1938,
		time: 1937,
		velocity: 16.6169444444444,
		power: 1217.17768947178,
		road: 19464.3256018518,
		acceleration: -0.208333333333332
	},
	{
		id: 1939,
		time: 1938,
		velocity: 16.3269444444444,
		power: 432.666216375545,
		road: 19480.7913425926,
		acceleration: -0.25277777777778
	},
	{
		id: 1940,
		time: 1939,
		velocity: 16.0741666666667,
		power: -296.058825917404,
		road: 19496.9835185185,
		acceleration: -0.29435185185185
	},
	{
		id: 1941,
		time: 1940,
		velocity: 15.7338888888889,
		power: -558.637105254515,
		road: 19512.8752314815,
		acceleration: -0.306574074074073
	},
	{
		id: 1942,
		time: 1941,
		velocity: 15.4072222222222,
		power: -913.36684885634,
		road: 19528.4508796296,
		acceleration: -0.325555555555555
	},
	{
		id: 1943,
		time: 1942,
		velocity: 15.0975,
		power: -971.049042648311,
		road: 19543.7011574074,
		acceleration: -0.325185185185186
	},
	{
		id: 1944,
		time: 1943,
		velocity: 14.7583333333333,
		power: 197.821499817856,
		road: 19558.6688888889,
		acceleration: -0.239907407407406
	},
	{
		id: 1945,
		time: 1944,
		velocity: 14.6875,
		power: 424.797461033564,
		road: 19573.4068055555,
		acceleration: -0.219722222222224
	},
	{
		id: 1946,
		time: 1945,
		velocity: 14.4383333333333,
		power: 1430.89502951733,
		road: 19587.9628703703,
		acceleration: -0.143981481481482
	},
	{
		id: 1947,
		time: 1946,
		velocity: 14.3263888888889,
		power: 1898.13321983249,
		road: 19602.3933796296,
		acceleration: -0.107129629629627
	},
	{
		id: 1948,
		time: 1947,
		velocity: 14.3661111111111,
		power: 2262.16771929491,
		road: 19616.73125,
		acceleration: -0.0781481481481485
	},
	{
		id: 1949,
		time: 1948,
		velocity: 14.2038888888889,
		power: 2224.913969064,
		road: 19630.9906944444,
		acceleration: -0.0787037037037042
	},
	{
		id: 1950,
		time: 1949,
		velocity: 14.0902777777778,
		power: 1381.03407419324,
		road: 19645.1417129629,
		acceleration: -0.138148148148147
	},
	{
		id: 1951,
		time: 1950,
		velocity: 13.9516666666667,
		power: 1304.26406496181,
		road: 19659.1533333333,
		acceleration: -0.14064814814815
	},
	{
		id: 1952,
		time: 1951,
		velocity: 13.7819444444444,
		power: 1034.97265893391,
		road: 19673.0158333333,
		acceleration: -0.157592592592593
	},
	{
		id: 1953,
		time: 1952,
		velocity: 13.6175,
		power: -897.755695467492,
		road: 19686.6489351851,
		acceleration: -0.301203703703701
	},
	{
		id: 1954,
		time: 1953,
		velocity: 13.0480555555556,
		power: -3629.85175500135,
		road: 19699.8746296296,
		acceleration: -0.513611111111112
	},
	{
		id: 1955,
		time: 1954,
		velocity: 12.2411111111111,
		power: -6945.60446882386,
		road: 19712.4456944444,
		acceleration: -0.79564814814815
	},
	{
		id: 1956,
		time: 1955,
		velocity: 11.2305555555556,
		power: -11085.7774564775,
		road: 19724.015,
		acceleration: -1.20787037037037
	},
	{
		id: 1957,
		time: 1956,
		velocity: 9.42444444444444,
		power: -11262.3144929737,
		road: 19734.312824074,
		acceleration: -1.33509259259259
	},
	{
		id: 1958,
		time: 1957,
		velocity: 8.23583333333333,
		power: -11162.2551021169,
		road: 19743.1972685185,
		acceleration: -1.49166666666667
	},
	{
		id: 1959,
		time: 1958,
		velocity: 6.75555555555556,
		power: -8506.26826789377,
		road: 19750.6571296296,
		acceleration: -1.3575
	},
	{
		id: 1960,
		time: 1959,
		velocity: 5.35194444444444,
		power: -7045.25501012985,
		road: 19756.7564351852,
		acceleration: -1.36361111111111
	},
	{
		id: 1961,
		time: 1960,
		velocity: 4.145,
		power: -5994.42009506298,
		road: 19761.4286111111,
		acceleration: -1.49064814814815
	},
	{
		id: 1962,
		time: 1961,
		velocity: 2.28361111111111,
		power: -3628.47250090632,
		road: 19764.7052777777,
		acceleration: -1.30037037037037
	},
	{
		id: 1963,
		time: 1962,
		velocity: 1.45083333333333,
		power: -2298.17232391949,
		road: 19766.6409259259,
		acceleration: -1.38166666666667
	},
	{
		id: 1964,
		time: 1963,
		velocity: 0,
		power: -518.69672254593,
		road: 19767.5051388889,
		acceleration: -0.761203703703704
	},
	{
		id: 1965,
		time: 1964,
		velocity: 0,
		power: -81.5706861111111,
		road: 19767.7469444444,
		acceleration: -0.483611111111111
	},
	{
		id: 1966,
		time: 1965,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1967,
		time: 1966,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1968,
		time: 1967,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1969,
		time: 1968,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1970,
		time: 1969,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1971,
		time: 1970,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1972,
		time: 1971,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1973,
		time: 1972,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1974,
		time: 1973,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1975,
		time: 1974,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1976,
		time: 1975,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1977,
		time: 1976,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1978,
		time: 1977,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1979,
		time: 1978,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1980,
		time: 1979,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1981,
		time: 1980,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1982,
		time: 1981,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1983,
		time: 1982,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1984,
		time: 1983,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1985,
		time: 1984,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1986,
		time: 1985,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1987,
		time: 1986,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1988,
		time: 1987,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1989,
		time: 1988,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1990,
		time: 1989,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1991,
		time: 1990,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1992,
		time: 1991,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1993,
		time: 1992,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1994,
		time: 1993,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1995,
		time: 1994,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1996,
		time: 1995,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1997,
		time: 1996,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1998,
		time: 1997,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 1999,
		time: 1998,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2000,
		time: 1999,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2001,
		time: 2000,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2002,
		time: 2001,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2003,
		time: 2002,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2004,
		time: 2003,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2005,
		time: 2004,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2006,
		time: 2005,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2007,
		time: 2006,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2008,
		time: 2007,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2009,
		time: 2008,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2010,
		time: 2009,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2011,
		time: 2010,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2012,
		time: 2011,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2013,
		time: 2012,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2014,
		time: 2013,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2015,
		time: 2014,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2016,
		time: 2015,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2017,
		time: 2016,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2018,
		time: 2017,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2019,
		time: 2018,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2020,
		time: 2019,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2021,
		time: 2020,
		velocity: 0,
		power: 0,
		road: 19767.7469444444,
		acceleration: 0
	},
	{
		id: 2022,
		time: 2021,
		velocity: 0,
		power: 120.930535351817,
		road: 19767.9696759259,
		acceleration: 0.445462962962963
	},
	{
		id: 2023,
		time: 2022,
		velocity: 1.33638888888889,
		power: 1449.6360676511,
		road: 19769.0552777777,
		acceleration: 1.28027777777778
	},
	{
		id: 2024,
		time: 2023,
		velocity: 3.84083333333333,
		power: 5026.80901086918,
		road: 19771.7121296296,
		acceleration: 1.86222222222222
	},
	{
		id: 2025,
		time: 2024,
		velocity: 5.58666666666667,
		power: 7620.9094906668,
		road: 19776.1373148148,
		acceleration: 1.67444444444444
	},
	{
		id: 2026,
		time: 2025,
		velocity: 6.35972222222222,
		power: 6277.44777003243,
		road: 19781.8996759259,
		acceleration: 0.999907407407409
	},
	{
		id: 2027,
		time: 2026,
		velocity: 6.84055555555556,
		power: 5438.8495760709,
		road: 19788.5180092592,
		acceleration: 0.712037037037036
	},
	{
		id: 2028,
		time: 2027,
		velocity: 7.72277777777778,
		power: 5649.37131352966,
		road: 19795.8202314814,
		acceleration: 0.655740740740741
	},
	{
		id: 2029,
		time: 2028,
		velocity: 8.32694444444444,
		power: 6491.3054737674,
		road: 19803.79625,
		acceleration: 0.691851851851854
	},
	{
		id: 2030,
		time: 2029,
		velocity: 8.91611111111111,
		power: 4361.7108160518,
		road: 19812.3033796296,
		acceleration: 0.370370370370368
	},
	{
		id: 2031,
		time: 2030,
		velocity: 8.83388888888889,
		power: 2240.32124152733,
		road: 19821.0450462963,
		acceleration: 0.0987037037037037
	},
	{
		id: 2032,
		time: 2031,
		velocity: 8.62305555555555,
		power: 2703.05264733195,
		road: 19829.910324074,
		acceleration: 0.148518518518518
	},
	{
		id: 2033,
		time: 2032,
		velocity: 9.36166666666667,
		power: 544.897242924362,
		road: 19838.7961111111,
		acceleration: -0.107500000000002
	},
	{
		id: 2034,
		time: 2033,
		velocity: 8.51138888888889,
		power: -2774.90699213681,
		road: 19847.3737962963,
		acceleration: -0.508703703703702
	},
	{
		id: 2035,
		time: 2034,
		velocity: 7.09694444444444,
		power: -6223.53601266341,
		road: 19855.1981944444,
		acceleration: -0.99787037037037
	},
	{
		id: 2036,
		time: 2035,
		velocity: 6.36805555555556,
		power: -5722.93337562344,
		road: 19862.0048148148,
		acceleration: -1.03768518518519
	},
	{
		id: 2037,
		time: 2036,
		velocity: 5.39833333333333,
		power: -4053.57909920288,
		road: 19867.8547222222,
		acceleration: -0.875740740740739
	},
	{
		id: 2038,
		time: 2037,
		velocity: 4.46972222222222,
		power: -4763.7124300751,
		road: 19872.6764351851,
		acceleration: -1.18064814814815
	},
	{
		id: 2039,
		time: 2038,
		velocity: 2.82611111111111,
		power: -5266.39288303026,
		road: 19876.0081018518,
		acceleration: -1.79944444444444
	},
	{
		id: 2040,
		time: 2039,
		velocity: 0,
		power: -2176.54123522375,
		road: 19877.6950925926,
		acceleration: -1.48990740740741
	},
	{
		id: 2041,
		time: 2040,
		velocity: 0,
		power: -363.455903281351,
		road: 19878.1661111111,
		acceleration: -0.942037037037037
	},
	{
		id: 2042,
		time: 2041,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2043,
		time: 2042,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2044,
		time: 2043,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2045,
		time: 2044,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2046,
		time: 2045,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2047,
		time: 2046,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2048,
		time: 2047,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2049,
		time: 2048,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2050,
		time: 2049,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2051,
		time: 2050,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2052,
		time: 2051,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2053,
		time: 2052,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2054,
		time: 2053,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2055,
		time: 2054,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2056,
		time: 2055,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2057,
		time: 2056,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2058,
		time: 2057,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2059,
		time: 2058,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2060,
		time: 2059,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2061,
		time: 2060,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2062,
		time: 2061,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2063,
		time: 2062,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2064,
		time: 2063,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2065,
		time: 2064,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2066,
		time: 2065,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2067,
		time: 2066,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2068,
		time: 2067,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2069,
		time: 2068,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2070,
		time: 2069,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2071,
		time: 2070,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2072,
		time: 2071,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2073,
		time: 2072,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2074,
		time: 2073,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2075,
		time: 2074,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2076,
		time: 2075,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2077,
		time: 2076,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2078,
		time: 2077,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2079,
		time: 2078,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2080,
		time: 2079,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2081,
		time: 2080,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2082,
		time: 2081,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2083,
		time: 2082,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2084,
		time: 2083,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2085,
		time: 2084,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2086,
		time: 2085,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2087,
		time: 2086,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2088,
		time: 2087,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2089,
		time: 2088,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2090,
		time: 2089,
		velocity: 0,
		power: 0,
		road: 19878.1661111111,
		acceleration: 0
	},
	{
		id: 2091,
		time: 2090,
		velocity: 0,
		power: 65.806189887677,
		road: 19878.323287037,
		acceleration: 0.314351851851852
	},
	{
		id: 2092,
		time: 2091,
		velocity: 0.943055555555556,
		power: 237.335522610009,
		road: 19878.8239351851,
		acceleration: 0.372592592592593
	},
	{
		id: 2093,
		time: 2092,
		velocity: 1.11777777777778,
		power: 317.788254764042,
		road: 19879.6499074074,
		acceleration: 0.278055555555556
	},
	{
		id: 2094,
		time: 2093,
		velocity: 0.834166666666667,
		power: 33.0102984138321,
		road: 19880.5698611111,
		acceleration: -0.0900925925925926
	},
	{
		id: 2095,
		time: 2094,
		velocity: 0.672777777777778,
		power: -159.776772576986,
		road: 19881.2584722222,
		acceleration: -0.372592592592592
	},
	{
		id: 2096,
		time: 2095,
		velocity: 0,
		power: -51.7959601823753,
		road: 19881.6217592592,
		acceleration: -0.278055555555556
	},
	{
		id: 2097,
		time: 2096,
		velocity: 0,
		power: -10.2753625406108,
		road: 19881.7338888889,
		acceleration: -0.224259259259259
	},
	{
		id: 2098,
		time: 2097,
		velocity: 0,
		power: 0,
		road: 19881.7338888889,
		acceleration: 0
	},
	{
		id: 2099,
		time: 2098,
		velocity: 0,
		power: 0,
		road: 19881.7338888889,
		acceleration: 0
	},
	{
		id: 2100,
		time: 2099,
		velocity: 0,
		power: 171.267072358843,
		road: 19882.0043055555,
		acceleration: 0.540833333333333
	},
	{
		id: 2101,
		time: 2100,
		velocity: 1.6225,
		power: 797.170611197863,
		road: 19882.9336111111,
		acceleration: 0.776944444444444
	},
	{
		id: 2102,
		time: 2101,
		velocity: 2.33083333333333,
		power: 2000.28266416777,
		road: 19884.7631018518,
		acceleration: 1.02342592592593
	},
	{
		id: 2103,
		time: 2102,
		velocity: 3.07027777777778,
		power: 1471.67100233171,
		road: 19887.3397222222,
		acceleration: 0.470833333333333
	},
	{
		id: 2104,
		time: 2103,
		velocity: 3.035,
		power: 1047.53079923919,
		road: 19890.27375,
		acceleration: 0.243981481481482
	},
	{
		id: 2105,
		time: 2104,
		velocity: 3.06277777777778,
		power: -560.543050945433,
		road: 19893.1614351851,
		acceleration: -0.336666666666666
	},
	{
		id: 2106,
		time: 2105,
		velocity: 2.06027777777778,
		power: -640.024117304116,
		road: 19895.6814351852,
		acceleration: -0.398703703703704
	},
	{
		id: 2107,
		time: 2106,
		velocity: 1.83888888888889,
		power: -638.956670548552,
		road: 19897.7763425926,
		acceleration: -0.451481481481482
	},
	{
		id: 2108,
		time: 2107,
		velocity: 1.70833333333333,
		power: 80.333059850086,
		road: 19899.6040277777,
		acceleration: -0.0829629629629629
	},
	{
		id: 2109,
		time: 2108,
		velocity: 1.81138888888889,
		power: 374.739230572855,
		road: 19901.4335648148,
		acceleration: 0.0866666666666667
	},
	{
		id: 2110,
		time: 2109,
		velocity: 2.09888888888889,
		power: 426.795461433072,
		road: 19903.3585648148,
		acceleration: 0.104259259259259
	},
	{
		id: 2111,
		time: 2110,
		velocity: 2.02111111111111,
		power: 250.075683821089,
		road: 19905.3375,
		acceleration: 0.00361111111111145
	},
	{
		id: 2112,
		time: 2111,
		velocity: 1.82222222222222,
		power: -378.270927664921,
		road: 19907.1431481481,
		acceleration: -0.350185185185185
	},
	{
		id: 2113,
		time: 2112,
		velocity: 1.04833333333333,
		power: -399.735213297239,
		road: 19908.5606944444,
		acceleration: -0.426018518518518
	},
	{
		id: 2114,
		time: 2113,
		velocity: 0.743055555555555,
		power: -227.631698612062,
		road: 19909.5838425926,
		acceleration: -0.362777777777778
	},
	{
		id: 2115,
		time: 2114,
		velocity: 0.733888888888889,
		power: 3.32006189922773,
		road: 19910.3639351851,
		acceleration: -0.123333333333333
	},
	{
		id: 2116,
		time: 2115,
		velocity: 0.678333333333333,
		power: 60.1141764282288,
		road: 19911.0637962963,
		acceleration: -0.0371296296296297
	},
	{
		id: 2117,
		time: 2116,
		velocity: 0.631666666666667,
		power: 121.512771652897,
		road: 19911.7717592592,
		acceleration: 0.0533333333333335
	},
	{
		id: 2118,
		time: 2117,
		velocity: 0.893888888888889,
		power: 160.408824300406,
		road: 19912.5510648148,
		acceleration: 0.0893518518518519
	},
	{
		id: 2119,
		time: 2118,
		velocity: 0.946388888888889,
		power: 257.632037349746,
		road: 19913.4605092592,
		acceleration: 0.170925925925926
	},
	{
		id: 2120,
		time: 2119,
		velocity: 1.14444444444444,
		power: 361.970162927444,
		road: 19914.5643055555,
		acceleration: 0.217777777777778
	},
	{
		id: 2121,
		time: 2120,
		velocity: 1.54722222222222,
		power: 459.693515418275,
		road: 19915.8949537037,
		acceleration: 0.235925925925926
	},
	{
		id: 2122,
		time: 2121,
		velocity: 1.65416666666667,
		power: 450.778440616253,
		road: 19917.4336574074,
		acceleration: 0.180185185185185
	},
	{
		id: 2123,
		time: 2122,
		velocity: 1.685,
		power: 233.87542437552,
		road: 19919.0731944444,
		acceleration: 0.0214814814814812
	},
	{
		id: 2124,
		time: 2123,
		velocity: 1.61166666666667,
		power: -309.936651516194,
		road: 19920.5483333333,
		acceleration: -0.350277777777778
	},
	{
		id: 2125,
		time: 2124,
		velocity: 0.603333333333333,
		power: -418.868765836959,
		road: 19921.5675,
		acceleration: -0.561666666666667
	},
	{
		id: 2126,
		time: 2125,
		velocity: 0,
		power: -182.302725307476,
		road: 19922.0372222222,
		acceleration: -0.537222222222222
	},
	{
		id: 2127,
		time: 2126,
		velocity: 0,
		power: -7.00956900584796,
		road: 19922.1377777777,
		acceleration: -0.201111111111111
	},
	{
		id: 2128,
		time: 2127,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2129,
		time: 2128,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2130,
		time: 2129,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2131,
		time: 2130,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2132,
		time: 2131,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2133,
		time: 2132,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2134,
		time: 2133,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2135,
		time: 2134,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2136,
		time: 2135,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2137,
		time: 2136,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2138,
		time: 2137,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2139,
		time: 2138,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2140,
		time: 2139,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2141,
		time: 2140,
		velocity: 0,
		power: 0,
		road: 19922.1377777777,
		acceleration: 0
	},
	{
		id: 2142,
		time: 2141,
		velocity: 0,
		power: 2.29636601747914,
		road: 19922.1531018518,
		acceleration: 0.0306481481481482
	},
	{
		id: 2143,
		time: 2142,
		velocity: 0.0919444444444445,
		power: 3.70286036504275,
		road: 19922.18375,
		acceleration: 0
	},
	{
		id: 2144,
		time: 2143,
		velocity: 0,
		power: 3.70286036504275,
		road: 19922.2143981481,
		acceleration: 0
	},
	{
		id: 2145,
		time: 2144,
		velocity: 0,
		power: 1.40648653346329,
		road: 19922.2297222222,
		acceleration: -0.0306481481481482
	},
	{
		id: 2146,
		time: 2145,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2147,
		time: 2146,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2148,
		time: 2147,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2149,
		time: 2148,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2150,
		time: 2149,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2151,
		time: 2150,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2152,
		time: 2151,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2153,
		time: 2152,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2154,
		time: 2153,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2155,
		time: 2154,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2156,
		time: 2155,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2157,
		time: 2156,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2158,
		time: 2157,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2159,
		time: 2158,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2160,
		time: 2159,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2161,
		time: 2160,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2162,
		time: 2161,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2163,
		time: 2162,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2164,
		time: 2163,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2165,
		time: 2164,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2166,
		time: 2165,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2167,
		time: 2166,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2168,
		time: 2167,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2169,
		time: 2168,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2170,
		time: 2169,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2171,
		time: 2170,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2172,
		time: 2171,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2173,
		time: 2172,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2174,
		time: 2173,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2175,
		time: 2174,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2176,
		time: 2175,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2177,
		time: 2176,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2178,
		time: 2177,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2179,
		time: 2178,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2180,
		time: 2179,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2181,
		time: 2180,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2182,
		time: 2181,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2183,
		time: 2182,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2184,
		time: 2183,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2185,
		time: 2184,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2186,
		time: 2185,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2187,
		time: 2186,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2188,
		time: 2187,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2189,
		time: 2188,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2190,
		time: 2189,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2191,
		time: 2190,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2192,
		time: 2191,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2193,
		time: 2192,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2194,
		time: 2193,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2195,
		time: 2194,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2196,
		time: 2195,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2197,
		time: 2196,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2198,
		time: 2197,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2199,
		time: 2198,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2200,
		time: 2199,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2201,
		time: 2200,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2202,
		time: 2201,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2203,
		time: 2202,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2204,
		time: 2203,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2205,
		time: 2204,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2206,
		time: 2205,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2207,
		time: 2206,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2208,
		time: 2207,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2209,
		time: 2208,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2210,
		time: 2209,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2211,
		time: 2210,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2212,
		time: 2211,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2213,
		time: 2212,
		velocity: 0,
		power: 0,
		road: 19922.2297222222,
		acceleration: 0
	},
	{
		id: 2214,
		time: 2213,
		velocity: 0,
		power: 139.023361296412,
		road: 19922.4705555555,
		acceleration: 0.481666666666667
	},
	{
		id: 2215,
		time: 2214,
		velocity: 1.445,
		power: 1723.77385837945,
		road: 19923.655324074,
		acceleration: 1.4062037037037
	},
	{
		id: 2216,
		time: 2215,
		velocity: 4.21861111111111,
		power: 5038.9562446601,
		road: 19926.4330092592,
		acceleration: 1.77962962962963
	},
	{
		id: 2217,
		time: 2216,
		velocity: 5.33888888888889,
		power: 6189.91612536872,
		road: 19930.7808796296,
		acceleration: 1.36074074074074
	},
	{
		id: 2218,
		time: 2217,
		velocity: 5.52722222222222,
		power: 3247.25004731639,
		road: 19936.0611574074,
		acceleration: 0.504074074074074
	},
	{
		id: 2219,
		time: 2218,
		velocity: 5.73083333333333,
		power: 1185.32155559342,
		road: 19941.6329629629,
		acceleration: 0.078981481481482
	},
	{
		id: 2220,
		time: 2219,
		velocity: 5.57583333333333,
		power: -2046.15737819894,
		road: 19946.9708333333,
		acceleration: -0.546851851851851
	},
	{
		id: 2221,
		time: 2220,
		velocity: 3.88666666666667,
		power: -4324.01655735729,
		road: 19951.4586111111,
		acceleration: -1.15333333333333
	},
	{
		id: 2222,
		time: 2221,
		velocity: 2.27083333333333,
		power: -2604.03261473423,
		road: 19954.9045833333,
		acceleration: -0.930277777777778
	},
	{
		id: 2223,
		time: 2222,
		velocity: 2.785,
		power: -207.207040412487,
		road: 19957.7814351851,
		acceleration: -0.207962962962962
	},
	{
		id: 2224,
		time: 2223,
		velocity: 3.26277777777778,
		power: 4325.00608854571,
		road: 19961.1613425926,
		acceleration: 1.21407407407407
	},
	{
		id: 2225,
		time: 2224,
		velocity: 5.91305555555556,
		power: 6915.26361766622,
		road: 19965.8539351852,
		acceleration: 1.4112962962963
	},
	{
		id: 2226,
		time: 2225,
		velocity: 7.01888888888889,
		power: 13852.7154002399,
		road: 19972.3055092592,
		acceleration: 2.10666666666667
	},
	{
		id: 2227,
		time: 2226,
		velocity: 9.58277777777778,
		power: 14732.7575913986,
		road: 19980.6538425926,
		acceleration: 1.68685185185185
	},
	{
		id: 2228,
		time: 2227,
		velocity: 10.9736111111111,
		power: 19479.595740666,
		road: 19990.7638888889,
		acceleration: 1.83657407407408
	},
	{
		id: 2229,
		time: 2228,
		velocity: 12.5286111111111,
		power: 12592.6533599343,
		road: 20002.2652314814,
		acceleration: 0.946018518518517
	},
	{
		id: 2230,
		time: 2229,
		velocity: 12.4208333333333,
		power: 6535.71382297962,
		road: 20014.4161574074,
		acceleration: 0.353148148148149
	},
	{
		id: 2231,
		time: 2230,
		velocity: 12.0330555555556,
		power: -1630.94756229044,
		road: 20026.5679629629,
		acceleration: -0.35138888888889
	},
	{
		id: 2232,
		time: 2231,
		velocity: 11.4744444444444,
		power: -3179.93917498424,
		road: 20038.2994444444,
		acceleration: -0.489259259259258
	},
	{
		id: 2233,
		time: 2232,
		velocity: 10.9530555555556,
		power: -5745.66507522559,
		road: 20049.4166666666,
		acceleration: -0.739259259259262
	},
	{
		id: 2234,
		time: 2233,
		velocity: 9.81527777777778,
		power: -3659.63779254184,
		road: 20059.88625,
		acceleration: -0.556018518518517
	},
	{
		id: 2235,
		time: 2234,
		velocity: 9.80638888888889,
		power: -1532.41913171195,
		road: 20069.9055555555,
		acceleration: -0.344537037037036
	},
	{
		id: 2236,
		time: 2235,
		velocity: 9.91944444444444,
		power: 1048.33154355664,
		road: 20079.717824074,
		acceleration: -0.069537037037037
	},
	{
		id: 2237,
		time: 2236,
		velocity: 9.60666666666667,
		power: 822.802746869813,
		road: 20089.4493055555,
		acceleration: -0.0920370370370378
	},
	{
		id: 2238,
		time: 2237,
		velocity: 9.53027777777778,
		power: -479.793817553108,
		road: 20099.0189351852,
		acceleration: -0.231666666666667
	},
	{
		id: 2239,
		time: 2238,
		velocity: 9.22444444444444,
		power: -398.446895054956,
		road: 20108.362037037,
		acceleration: -0.221388888888891
	},
	{
		id: 2240,
		time: 2239,
		velocity: 8.9425,
		power: -1785.22782831987,
		road: 20117.4040277777,
		acceleration: -0.380833333333332
	},
	{
		id: 2241,
		time: 2240,
		velocity: 8.38777777777778,
		power: -1665.99025388478,
		road: 20126.0697685185,
		acceleration: -0.371666666666668
	},
	{
		id: 2242,
		time: 2241,
		velocity: 8.10944444444444,
		power: -2055.96103474761,
		road: 20134.3360648148,
		acceleration: -0.427222222222222
	},
	{
		id: 2243,
		time: 2242,
		velocity: 7.66083333333333,
		power: -1487.11482988684,
		road: 20142.2083333333,
		acceleration: -0.360833333333334
	},
	{
		id: 2244,
		time: 2243,
		velocity: 7.30527777777778,
		power: -1532.75214200264,
		road: 20149.713287037,
		acceleration: -0.373796296296296
	},
	{
		id: 2245,
		time: 2244,
		velocity: 6.98805555555556,
		power: -1251.10849139469,
		road: 20156.86125,
		acceleration: -0.340185185185184
	},
	{
		id: 2246,
		time: 2245,
		velocity: 6.64027777777778,
		power: -200.57718537899,
		road: 20163.7467592592,
		acceleration: -0.184722222222224
	},
	{
		id: 2247,
		time: 2246,
		velocity: 6.75111111111111,
		power: 675.639477078505,
		road: 20170.515787037,
		acceleration: -0.0482407407407406
	},
	{
		id: 2248,
		time: 2247,
		velocity: 6.84333333333333,
		power: 2615.19114273771,
		road: 20177.3838888889,
		acceleration: 0.246388888888888
	},
	{
		id: 2249,
		time: 2248,
		velocity: 7.37944444444444,
		power: 3407.73237246026,
		road: 20184.5470833333,
		acceleration: 0.343796296296297
	},
	{
		id: 2250,
		time: 2249,
		velocity: 7.7825,
		power: 4224.5835556836,
		road: 20192.0964814815,
		acceleration: 0.428611111111112
	},
	{
		id: 2251,
		time: 2250,
		velocity: 8.12916666666667,
		power: 2383.40376693322,
		road: 20199.9388425926,
		acceleration: 0.157314814814814
	},
	{
		id: 2252,
		time: 2251,
		velocity: 7.85138888888889,
		power: 1310.43662782021,
		road: 20207.865324074,
		acceleration: 0.0109259259259264
	},
	{
		id: 2253,
		time: 2252,
		velocity: 7.81527777777778,
		power: 2014.45345930453,
		road: 20215.8481944444,
		acceleration: 0.101851851851852
	},
	{
		id: 2254,
		time: 2253,
		velocity: 8.43472222222222,
		power: 6011.76877984099,
		road: 20224.177824074,
		acceleration: 0.591666666666667
	},
	{
		id: 2255,
		time: 2254,
		velocity: 9.62638888888889,
		power: 8118.53621529083,
		road: 20233.1896759259,
		acceleration: 0.772777777777776
	},
	{
		id: 2256,
		time: 2255,
		velocity: 10.1336111111111,
		power: 5703.15350904833,
		road: 20242.8093055555,
		acceleration: 0.442777777777779
	},
	{
		id: 2257,
		time: 2256,
		velocity: 9.76305555555555,
		power: 1262.50780811376,
		road: 20252.6269444444,
		acceleration: -0.0467592592592592
	},
	{
		id: 2258,
		time: 2257,
		velocity: 9.48611111111111,
		power: 121.698727542064,
		road: 20262.3375,
		acceleration: -0.167407407407408
	},
	{
		id: 2259,
		time: 2258,
		velocity: 9.63138888888889,
		power: 1403.04653501462,
		road: 20271.9512037037,
		acceleration: -0.0262962962962963
	},
	{
		id: 2260,
		time: 2259,
		velocity: 9.68416666666667,
		power: 2224.83309559609,
		road: 20281.5831481481,
		acceleration: 0.0627777777777769
	},
	{
		id: 2261,
		time: 2260,
		velocity: 9.67444444444445,
		power: -22.063637271118,
		road: 20291.1557407407,
		acceleration: -0.18148148148148
	},
	{
		id: 2262,
		time: 2261,
		velocity: 9.08694444444444,
		power: -1105.05609056816,
		road: 20300.4871759259,
		acceleration: -0.300833333333333
	},
	{
		id: 2263,
		time: 2262,
		velocity: 8.78166666666667,
		power: -1395.83452534882,
		road: 20309.5002777777,
		acceleration: -0.335833333333333
	},
	{
		id: 2264,
		time: 2263,
		velocity: 8.66694444444444,
		power: 400.771543992325,
		road: 20318.2839814815,
		acceleration: -0.122962962962964
	},
	{
		id: 2265,
		time: 2264,
		velocity: 8.71805555555556,
		power: 2153.49837617264,
		road: 20327.0498611111,
		acceleration: 0.0873148148148157
	},
	{
		id: 2266,
		time: 2265,
		velocity: 9.04361111111111,
		power: 3083.04162412362,
		road: 20335.9551388889,
		acceleration: 0.191481481481482
	},
	{
		id: 2267,
		time: 2266,
		velocity: 9.24138888888889,
		power: 2538.94748666083,
		road: 20345.0164351852,
		acceleration: 0.120555555555555
	},
	{
		id: 2268,
		time: 2267,
		velocity: 9.07972222222222,
		power: 412.515594350407,
		road: 20354.0750925926,
		acceleration: -0.125833333333334
	},
	{
		id: 2269,
		time: 2268,
		velocity: 8.66611111111111,
		power: -1423.73717448488,
		road: 20362.9004629629,
		acceleration: -0.34074074074074
	},
	{
		id: 2270,
		time: 2269,
		velocity: 8.21916666666667,
		power: -2598.16642475383,
		road: 20371.309537037,
		acceleration: -0.491851851851852
	},
	{
		id: 2271,
		time: 2270,
		velocity: 7.60416666666667,
		power: -2116.25928665155,
		road: 20379.2512037037,
		acceleration: -0.442962962962963
	},
	{
		id: 2272,
		time: 2271,
		velocity: 7.33722222222222,
		power: -1796.99118253764,
		road: 20386.7661111111,
		acceleration: -0.410555555555555
	},
	{
		id: 2273,
		time: 2272,
		velocity: 6.9875,
		power: -356.864165534727,
		road: 20393.9713888889,
		acceleration: -0.208703703703704
	},
	{
		id: 2274,
		time: 2273,
		velocity: 6.97805555555555,
		power: 1187.06120321427,
		road: 20401.0821296296,
		acceleration: 0.0196296296296294
	},
	{
		id: 2275,
		time: 2274,
		velocity: 7.39611111111111,
		power: 2323.09057980226,
		road: 20408.2936574074,
		acceleration: 0.181944444444444
	},
	{
		id: 2276,
		time: 2275,
		velocity: 7.53333333333333,
		power: 3412.93243908488,
		road: 20415.7570833333,
		acceleration: 0.321851851851853
	},
	{
		id: 2277,
		time: 2276,
		velocity: 7.94361111111111,
		power: 3817.61166284188,
		road: 20423.5577314815,
		acceleration: 0.352592592592592
	},
	{
		id: 2278,
		time: 2277,
		velocity: 8.45388888888889,
		power: 4056.59562316509,
		road: 20431.7135185185,
		acceleration: 0.357685185185185
	},
	{
		id: 2279,
		time: 2278,
		velocity: 8.60638888888889,
		power: 3151.75965432644,
		road: 20440.160324074,
		acceleration: 0.224351851851852
	},
	{
		id: 2280,
		time: 2279,
		velocity: 8.61666666666667,
		power: 1552.29599084864,
		road: 20448.7300462963,
		acceleration: 0.0214814814814801
	},
	{
		id: 2281,
		time: 2280,
		velocity: 8.51833333333333,
		power: 135.045129725394,
		road: 20457.2347685185,
		acceleration: -0.151481481481481
	},
	{
		id: 2282,
		time: 2281,
		velocity: 8.15194444444444,
		power: -617.989467342622,
		road: 20465.5415277778,
		acceleration: -0.244444444444444
	},
	{
		id: 2283,
		time: 2282,
		velocity: 7.88333333333333,
		power: -93.6042676238656,
		road: 20473.637824074,
		acceleration: -0.176481481481481
	},
	{
		id: 2284,
		time: 2283,
		velocity: 7.98888888888889,
		power: 1190.47494220994,
		road: 20481.6422685185,
		acceleration: -0.00722222222222335
	},
	{
		id: 2285,
		time: 2284,
		velocity: 8.13027777777778,
		power: 1702.83056490234,
		road: 20489.6726388889,
		acceleration: 0.0590740740740756
	},
	{
		id: 2286,
		time: 2285,
		velocity: 8.06055555555556,
		power: 2150.82227489501,
		road: 20497.7894907407,
		acceleration: 0.113888888888889
	},
	{
		id: 2287,
		time: 2286,
		velocity: 8.33055555555556,
		power: 3117.79388644781,
		road: 20506.077824074,
		acceleration: 0.229074074074074
	},
	{
		id: 2288,
		time: 2287,
		velocity: 8.8175,
		power: 4890.7809941352,
		road: 20514.6941666666,
		acceleration: 0.426944444444445
	},
	{
		id: 2289,
		time: 2288,
		velocity: 9.34138888888889,
		power: 5506.28455055768,
		road: 20523.7561574074,
		acceleration: 0.46435185185185
	},
	{
		id: 2290,
		time: 2289,
		velocity: 9.72361111111111,
		power: 5516.84275644949,
		road: 20533.2656481481,
		acceleration: 0.430648148148148
	},
	{
		id: 2291,
		time: 2290,
		velocity: 10.1094444444444,
		power: 3701.06361550679,
		road: 20543.0970833333,
		acceleration: 0.213240740740742
	},
	{
		id: 2292,
		time: 2291,
		velocity: 9.98111111111111,
		power: 1784.26953382052,
		road: 20553.0377777778,
		acceleration: 0.00527777777777771
	},
	{
		id: 2293,
		time: 2292,
		velocity: 9.73944444444444,
		power: 358.114060609383,
		road: 20562.9089814815,
		acceleration: -0.144259259259261
	},
	{
		id: 2294,
		time: 2293,
		velocity: 9.67666666666667,
		power: 2539.78457510778,
		road: 20572.7524537037,
		acceleration: 0.088796296296298
	},
	{
		id: 2295,
		time: 2294,
		velocity: 10.2475,
		power: 4520.90662019411,
		road: 20582.7847222222,
		acceleration: 0.288796296296296
	},
	{
		id: 2296,
		time: 2295,
		velocity: 10.6058333333333,
		power: 4860.79618509147,
		road: 20593.1144907407,
		acceleration: 0.306203703703703
	},
	{
		id: 2297,
		time: 2296,
		velocity: 10.5952777777778,
		power: 2254.00139132489,
		road: 20603.6151851852,
		acceleration: 0.0356481481481481
	},
	{
		id: 2298,
		time: 2297,
		velocity: 10.3544444444444,
		power: 432.705223797119,
		road: 20614.0609722222,
		acceleration: -0.145462962962961
	},
	{
		id: 2299,
		time: 2298,
		velocity: 10.1694444444444,
		power: 948.109261487085,
		road: 20624.3884259259,
		acceleration: -0.0912037037037035
	},
	{
		id: 2300,
		time: 2299,
		velocity: 10.3216666666667,
		power: 2881.83822431468,
		road: 20634.7227777778,
		acceleration: 0.104999999999999
	},
	{
		id: 2301,
		time: 2300,
		velocity: 10.6694444444444,
		power: 4959.31128048239,
		road: 20645.2614814815,
		acceleration: 0.303703703703704
	},
	{
		id: 2302,
		time: 2301,
		velocity: 11.0805555555556,
		power: 5523.80825975157,
		road: 20656.1218518518,
		acceleration: 0.339629629629629
	},
	{
		id: 2303,
		time: 2302,
		velocity: 11.3405555555556,
		power: 5454.76452191213,
		road: 20667.30875,
		acceleration: 0.313425925925927
	},
	{
		id: 2304,
		time: 2303,
		velocity: 11.6097222222222,
		power: 4752.80245751591,
		road: 20678.769074074,
		acceleration: 0.233425925925927
	},
	{
		id: 2305,
		time: 2304,
		velocity: 11.7808333333333,
		power: 4210.32731800097,
		road: 20690.433287037,
		acceleration: 0.174351851851853
	},
	{
		id: 2306,
		time: 2305,
		velocity: 11.8636111111111,
		power: 3716.03558890831,
		road: 20702.2465277777,
		acceleration: 0.123703703703704
	},
	{
		id: 2307,
		time: 2306,
		velocity: 11.9808333333333,
		power: 3400.09609543367,
		road: 20714.1673611111,
		acceleration: 0.0914814814814804
	},
	{
		id: 2308,
		time: 2307,
		velocity: 12.0552777777778,
		power: 3688.41090051377,
		road: 20726.1902777777,
		acceleration: 0.112685185185185
	},
	{
		id: 2309,
		time: 2308,
		velocity: 12.2016666666667,
		power: 3838.36894001111,
		road: 20738.33,
		acceleration: 0.120925925925926
	},
	{
		id: 2310,
		time: 2309,
		velocity: 12.3436111111111,
		power: 3641.21681935728,
		road: 20750.5799537037,
		acceleration: 0.0995370370370363
	},
	{
		id: 2311,
		time: 2310,
		velocity: 12.3538888888889,
		power: 3030.14579692284,
		road: 20762.902037037,
		acceleration: 0.0447222222222212
	},
	{
		id: 2312,
		time: 2311,
		velocity: 12.3358333333333,
		power: 950.465198909911,
		road: 20775.1808333333,
		acceleration: -0.131296296296295
	},
	{
		id: 2313,
		time: 2312,
		velocity: 11.9497222222222,
		power: -87.2360131635902,
		road: 20787.2851851852,
		acceleration: -0.217592592592593
	},
	{
		id: 2314,
		time: 2313,
		velocity: 11.7011111111111,
		power: -1149.17762529952,
		road: 20799.1266203703,
		acceleration: -0.308240740740741
	},
	{
		id: 2315,
		time: 2314,
		velocity: 11.4111111111111,
		power: -849.160985742103,
		road: 20810.674074074,
		acceleration: -0.279722222222221
	},
	{
		id: 2316,
		time: 2315,
		velocity: 11.1105555555556,
		power: -906.044009923458,
		road: 20821.94,
		acceleration: -0.283333333333333
	},
	{
		id: 2317,
		time: 2316,
		velocity: 10.8511111111111,
		power: -1697.77293664925,
		road: 20832.8854166666,
		acceleration: -0.357685185185186
	},
	{
		id: 2318,
		time: 2317,
		velocity: 10.3380555555556,
		power: -1916.81921200881,
		road: 20843.4616666666,
		acceleration: -0.380648148148149
	},
	{
		id: 2319,
		time: 2318,
		velocity: 9.96861111111111,
		power: -2863.40533662781,
		road: 20853.6067592592,
		acceleration: -0.481666666666667
	},
	{
		id: 2320,
		time: 2319,
		velocity: 9.40611111111111,
		power: -4026.42117937768,
		road: 20863.2010648148,
		acceleration: -0.619907407407405
	},
	{
		id: 2321,
		time: 2320,
		velocity: 8.47833333333333,
		power: -5632.48512758254,
		road: 20872.0658796296,
		acceleration: -0.839074074074075
	},
	{
		id: 2322,
		time: 2321,
		velocity: 7.45138888888889,
		power: -7168.58184056353,
		road: 20879.9522685185,
		acceleration: -1.11777777777778
	},
	{
		id: 2323,
		time: 2322,
		velocity: 6.05277777777778,
		power: -6549.31811773987,
		road: 20886.6923148148,
		acceleration: -1.17490740740741
	},
	{
		id: 2324,
		time: 2323,
		velocity: 4.95361111111111,
		power: -6558.94850879851,
		road: 20892.1391666666,
		acceleration: -1.41148148148148
	},
	{
		id: 2325,
		time: 2324,
		velocity: 3.21694444444444,
		power: -4737.20555676,
		road: 20896.1971296296,
		acceleration: -1.3662962962963
	},
	{
		id: 2326,
		time: 2325,
		velocity: 1.95388888888889,
		power: -3108.20467608198,
		road: 20898.9000925926,
		acceleration: -1.3437037037037
	},
	{
		id: 2327,
		time: 2326,
		velocity: 0.9225,
		power: -1337.32614788502,
		road: 20900.3950462963,
		acceleration: -1.07231481481481
	},
	{
		id: 2328,
		time: 2327,
		velocity: 0,
		power: -314.135388318993,
		road: 20901.0281944444,
		acceleration: -0.651296296296296
	},
	{
		id: 2329,
		time: 2328,
		velocity: 0,
		power: -26.2140513157895,
		road: 20901.1819444444,
		acceleration: -0.3075
	},
	{
		id: 2330,
		time: 2329,
		velocity: 0,
		power: 83.1865450241627,
		road: 20901.3619907407,
		acceleration: 0.360092592592593
	},
	{
		id: 2331,
		time: 2330,
		velocity: 1.08027777777778,
		power: 932.401743849597,
		road: 20902.2268055555,
		acceleration: 1.00944444444444
	},
	{
		id: 2332,
		time: 2331,
		velocity: 3.02833333333333,
		power: 4056.21832488823,
		road: 20904.4799074074,
		acceleration: 1.76712962962963
	},
	{
		id: 2333,
		time: 2332,
		velocity: 5.30138888888889,
		power: 7022.20838377071,
		road: 20908.4739814814,
		acceleration: 1.71481481481481
	},
	{
		id: 2334,
		time: 2333,
		velocity: 6.22472222222222,
		power: 7461.54085995665,
		road: 20913.9677314815,
		acceleration: 1.28453703703704
	},
	{
		id: 2335,
		time: 2334,
		velocity: 6.88194444444444,
		power: 6462.58303130847,
		road: 20920.5444907407,
		acceleration: 0.88148148148148
	},
	{
		id: 2336,
		time: 2335,
		velocity: 7.94583333333333,
		power: 5817.21775351481,
		road: 20927.89875,
		acceleration: 0.673518518518519
	},
	{
		id: 2337,
		time: 2336,
		velocity: 8.24527777777778,
		power: 5838.54930180403,
		road: 20935.8918055555,
		acceleration: 0.604074074074074
	},
	{
		id: 2338,
		time: 2337,
		velocity: 8.69416666666667,
		power: 2563.28912552844,
		road: 20944.2642592592,
		acceleration: 0.154722222222222
	},
	{
		id: 2339,
		time: 2338,
		velocity: 8.41,
		power: 99.2884560453751,
		road: 20952.6368518518,
		acceleration: -0.154444444444444
	},
	{
		id: 2340,
		time: 2339,
		velocity: 7.78194444444444,
		power: -1870.75494812253,
		road: 20960.7286111111,
		acceleration: -0.407222222222223
	},
	{
		id: 2341,
		time: 2340,
		velocity: 7.4725,
		power: -6636.4471330192,
		road: 20968.0622222222,
		acceleration: -1.10907407407407
	},
	{
		id: 2342,
		time: 2341,
		velocity: 5.08277777777778,
		power: -5361.2923365996,
		road: 20974.3156481481,
		acceleration: -1.0512962962963
	},
	{
		id: 2343,
		time: 2342,
		velocity: 4.62805555555556,
		power: -4157.8718966289,
		road: 20979.5543055555,
		acceleration: -0.978240740740741
	},
	{
		id: 2344,
		time: 2343,
		velocity: 4.53777777777778,
		power: 59.8558675605453,
		road: 20984.2406944444,
		acceleration: -0.126296296296296
	},
	{
		id: 2345,
		time: 2344,
		velocity: 4.70388888888889,
		power: 556.836915782391,
		road: 20988.8577314815,
		acceleration: -0.0124074074074079
	},
	{
		id: 2346,
		time: 2345,
		velocity: 4.59083333333333,
		power: 400.864371934915,
		road: 20993.4449537037,
		acceleration: -0.0472222222222225
	},
	{
		id: 2347,
		time: 2346,
		velocity: 4.39611111111111,
		power: 146.166990220924,
		road: 20997.9562037037,
		acceleration: -0.104722222222222
	},
	{
		id: 2348,
		time: 2347,
		velocity: 4.38972222222222,
		power: 35.5804895095431,
		road: 21002.3502314815,
		acceleration: -0.129722222222222
	},
	{
		id: 2349,
		time: 2348,
		velocity: 4.20166666666667,
		power: 192.598503923178,
		road: 21006.6342129629,
		acceleration: -0.09037037037037
	},
	{
		id: 2350,
		time: 2349,
		velocity: 4.125,
		power: 194.245171821199,
		road: 21010.82875,
		acceleration: -0.0885185185185184
	},
	{
		id: 2351,
		time: 2350,
		velocity: 4.12416666666667,
		power: 298.454811362072,
		road: 21014.9487037037,
		acceleration: -0.0606481481481485
	},
	{
		id: 2352,
		time: 2351,
		velocity: 4.01972222222222,
		power: 491.339759187577,
		road: 21019.033287037,
		acceleration: -0.0100925925925921
	},
	{
		id: 2353,
		time: 2352,
		velocity: 4.09472222222222,
		power: 803.597227514715,
		road: 21023.1472222222,
		acceleration: 0.0687962962962958
	},
	{
		id: 2354,
		time: 2353,
		velocity: 4.33055555555556,
		power: 1031.16114371217,
		road: 21027.3558796296,
		acceleration: 0.120648148148148
	},
	{
		id: 2355,
		time: 2354,
		velocity: 4.38166666666667,
		power: 782.3112114485,
		road: 21031.6518518518,
		acceleration: 0.0539814814814807
	},
	{
		id: 2356,
		time: 2355,
		velocity: 4.25666666666667,
		power: -286.757241234913,
		road: 21035.870324074,
		acceleration: -0.208981481481481
	},
	{
		id: 2357,
		time: 2356,
		velocity: 3.70361111111111,
		power: -879.001586254806,
		road: 21039.7984259259,
		acceleration: -0.371759259259259
	},
	{
		id: 2358,
		time: 2357,
		velocity: 3.26638888888889,
		power: -1016.76693945692,
		road: 21043.3214351852,
		acceleration: -0.438425925925926
	},
	{
		id: 2359,
		time: 2358,
		velocity: 2.94138888888889,
		power: -508.233363428447,
		road: 21046.4737962963,
		acceleration: -0.30287037037037
	},
	{
		id: 2360,
		time: 2359,
		velocity: 2.795,
		power: -469.852589511483,
		road: 21049.3218055555,
		acceleration: -0.305833333333334
	},
	{
		id: 2361,
		time: 2360,
		velocity: 2.34888888888889,
		power: -190.312319681203,
		road: 21051.9125925926,
		acceleration: -0.208611111111111
	},
	{
		id: 2362,
		time: 2361,
		velocity: 2.31555555555556,
		power: -314.722466707113,
		road: 21054.2632407407,
		acceleration: -0.271666666666667
	},
	{
		id: 2363,
		time: 2362,
		velocity: 1.98,
		power: -221.995349085259,
		road: 21056.3572222222,
		acceleration: -0.241666666666667
	},
	{
		id: 2364,
		time: 2363,
		velocity: 1.62388888888889,
		power: -519.824241788553,
		road: 21058.1093518518,
		acceleration: -0.442037037037037
	},
	{
		id: 2365,
		time: 2364,
		velocity: 0.989444444444444,
		power: -336.57094188936,
		road: 21059.4431481481,
		acceleration: -0.39462962962963
	},
	{
		id: 2366,
		time: 2365,
		velocity: 0.796111111111111,
		power: -339.230740950862,
		road: 21060.3089814815,
		acceleration: -0.541296296296296
	},
	{
		id: 2367,
		time: 2366,
		velocity: 0,
		power: -82.4412290607918,
		road: 21060.7392592592,
		acceleration: -0.329814814814815
	},
	{
		id: 2368,
		time: 2367,
		velocity: 0,
		power: -17.3267763807667,
		road: 21060.8719444444,
		acceleration: -0.26537037037037
	},
	{
		id: 2369,
		time: 2368,
		velocity: 0,
		power: 0,
		road: 21060.8719444444,
		acceleration: 0
	},
	{
		id: 2370,
		time: 2369,
		velocity: 0,
		power: 43.7532395949894,
		road: 21060.995324074,
		acceleration: 0.246759259259259
	},
	{
		id: 2371,
		time: 2370,
		velocity: 0.740277777777778,
		power: 338.261655695541,
		road: 21061.5190277777,
		acceleration: 0.553888888888889
	},
	{
		id: 2372,
		time: 2371,
		velocity: 1.66166666666667,
		power: 731.419620698463,
		road: 21062.6093981481,
		acceleration: 0.579444444444444
	},
	{
		id: 2373,
		time: 2372,
		velocity: 1.73833333333333,
		power: 1025.42222819281,
		road: 21064.2537962963,
		acceleration: 0.528611111111111
	},
	{
		id: 2374,
		time: 2373,
		velocity: 2.32611111111111,
		power: 796.095348147251,
		road: 21066.3024537037,
		acceleration: 0.279907407407408
	},
	{
		id: 2375,
		time: 2374,
		velocity: 2.50138888888889,
		power: 1199.39975186381,
		road: 21068.6904629629,
		acceleration: 0.398796296296296
	},
	{
		id: 2376,
		time: 2375,
		velocity: 2.93472222222222,
		power: 1739.34828660818,
		road: 21071.5341666666,
		acceleration: 0.512592592592592
	},
	{
		id: 2377,
		time: 2376,
		velocity: 3.86388888888889,
		power: 2323.96740398954,
		road: 21074.9279166666,
		acceleration: 0.587499999999999
	},
	{
		id: 2378,
		time: 2377,
		velocity: 4.26388888888889,
		power: 2080.94451434466,
		road: 21078.8283796296,
		acceleration: 0.425925925925926
	},
	{
		id: 2379,
		time: 2378,
		velocity: 4.2125,
		power: 1255.17659233884,
		road: 21083.0304166666,
		acceleration: 0.177222222222222
	},
	{
		id: 2380,
		time: 2379,
		velocity: 4.39555555555556,
		power: 573.953772411092,
		road: 21087.3225925926,
		acceleration: 0.00305555555555515
	},
	{
		id: 2381,
		time: 2380,
		velocity: 4.27305555555556,
		power: 581.736658579518,
		road: 21091.6187037037,
		acceleration: 0.00481481481481527
	},
	{
		id: 2382,
		time: 2381,
		velocity: 4.22694444444444,
		power: 281.332736282283,
		road: 21095.8831481481,
		acceleration: -0.0681481481481487
	},
	{
		id: 2383,
		time: 2382,
		velocity: 4.19111111111111,
		power: 62.844826893754,
		road: 21100.0528703703,
		acceleration: -0.121296296296296
	},
	{
		id: 2384,
		time: 2383,
		velocity: 3.90916666666667,
		power: -409.95566517335,
		road: 21104.0396296296,
		acceleration: -0.244629629629629
	},
	{
		id: 2385,
		time: 2384,
		velocity: 3.49305555555556,
		power: -21.5966501819477,
		road: 21107.8333333333,
		acceleration: -0.141481481481482
	},
	{
		id: 2386,
		time: 2385,
		velocity: 3.76666666666667,
		power: 730.550833020004,
		road: 21111.5910185185,
		acceleration: 0.0694444444444446
	},
	{
		id: 2387,
		time: 2386,
		velocity: 4.1175,
		power: 1548.16057710952,
		road: 21115.5227314815,
		acceleration: 0.278611111111111
	},
	{
		id: 2388,
		time: 2387,
		velocity: 4.32888888888889,
		power: 1202.54596519364,
		road: 21119.6775925926,
		acceleration: 0.167685185185185
	},
	{
		id: 2389,
		time: 2388,
		velocity: 4.26972222222222,
		power: 667.340755059494,
		road: 21123.9301388889,
		acceleration: 0.0276851851851854
	},
	{
		id: 2390,
		time: 2389,
		velocity: 4.20055555555556,
		power: 318.879391236016,
		road: 21128.1674074074,
		acceleration: -0.0582407407407404
	},
	{
		id: 2391,
		time: 2390,
		velocity: 4.15416666666667,
		power: 55.5093145486629,
		road: 21132.314074074,
		acceleration: -0.122962962962963
	},
	{
		id: 2392,
		time: 2391,
		velocity: 3.90083333333333,
		power: -1304.0011771009,
		road: 21136.1525,
		acceleration: -0.493518518518519
	},
	{
		id: 2393,
		time: 2392,
		velocity: 2.72,
		power: -2767.86736811041,
		road: 21139.1991203703,
		acceleration: -1.09009259259259
	},
	{
		id: 2394,
		time: 2393,
		velocity: 0.883888888888889,
		power: -1689.90907341667,
		road: 21141.187824074,
		acceleration: -1.02574074074074
	},
	{
		id: 2395,
		time: 2394,
		velocity: 0.823611111111111,
		power: -573.256380389283,
		road: 21142.33625,
		acceleration: -0.654814814814815
	},
	{
		id: 2396,
		time: 2395,
		velocity: 0.755555555555556,
		power: -106.549266248743,
		road: 21143.0099537037,
		acceleration: -0.29462962962963
	},
	{
		id: 2397,
		time: 2396,
		velocity: 0,
		power: -54.1793267721334,
		road: 21143.399074074,
		acceleration: -0.274537037037037
	},
	{
		id: 2398,
		time: 2397,
		velocity: 0,
		power: -14.8313788174139,
		road: 21143.525,
		acceleration: -0.251851851851852
	},
	{
		id: 2399,
		time: 2398,
		velocity: 0,
		power: 0,
		road: 21143.525,
		acceleration: 0
	},
	{
		id: 2400,
		time: 2399,
		velocity: 0,
		power: 100.826773944548,
		road: 21143.7259722222,
		acceleration: 0.401944444444444
	},
	{
		id: 2401,
		time: 2400,
		velocity: 1.20583333333333,
		power: 1239.2065059702,
		road: 21144.7208333333,
		acceleration: 1.18583333333333
	},
	{
		id: 2402,
		time: 2401,
		velocity: 3.5575,
		power: 4429.36306856454,
		road: 21147.1888425926,
		acceleration: 1.76046296296296
	},
	{
		id: 2403,
		time: 2402,
		velocity: 5.28138888888889,
		power: 5851.49387543919,
		road: 21151.2309259259,
		acceleration: 1.38768518518519
	},
	{
		id: 2404,
		time: 2403,
		velocity: 5.36888888888889,
		power: 4526.78588507296,
		road: 21156.3601388889,
		acceleration: 0.786574074074075
	},
	{
		id: 2405,
		time: 2404,
		velocity: 5.91722222222222,
		power: 1054.44882195275,
		road: 21161.9102314814,
		acceleration: 0.0551851851851843
	},
	{
		id: 2406,
		time: 2405,
		velocity: 5.44694444444445,
		power: 360.380882446553,
		road: 21167.4498148148,
		acceleration: -0.0762037037037038
	},
	{
		id: 2407,
		time: 2406,
		velocity: 5.14027777777778,
		power: -2190.06121825615,
		road: 21172.6587037037,
		acceleration: -0.585185185185185
	},
	{
		id: 2408,
		time: 2407,
		velocity: 4.16166666666667,
		power: -1563.52080566316,
		road: 21177.3289351852,
		acceleration: -0.49212962962963
	},
	{
		id: 2409,
		time: 2408,
		velocity: 3.97055555555556,
		power: -1696.50008818988,
		road: 21181.4687962963,
		acceleration: -0.568611111111111
	},
	{
		id: 2410,
		time: 2409,
		velocity: 3.43444444444444,
		power: -1592.65020818701,
		road: 21185.0209259259,
		acceleration: -0.606851851851852
	},
	{
		id: 2411,
		time: 2410,
		velocity: 2.34111111111111,
		power: -1283.24967472169,
		road: 21187.974537037,
		acceleration: -0.590185185185186
	},
	{
		id: 2412,
		time: 2411,
		velocity: 2.2,
		power: -755.156036457985,
		road: 21190.4038425926,
		acceleration: -0.458425925925926
	},
	{
		id: 2413,
		time: 2412,
		velocity: 2.05916666666667,
		power: 67.5114608467021,
		road: 21192.5554629629,
		acceleration: -0.0969444444444449
	},
	{
		id: 2414,
		time: 2413,
		velocity: 2.05027777777778,
		power: 466.804022228255,
		road: 21194.7079166666,
		acceleration: 0.0986111111111114
	},
	{
		id: 2415,
		time: 2414,
		velocity: 2.49583333333333,
		power: 1035.24846106843,
		road: 21197.0749074074,
		acceleration: 0.330462962962963
	},
	{
		id: 2416,
		time: 2415,
		velocity: 3.05055555555556,
		power: 1552.51401319023,
		road: 21199.8374074074,
		acceleration: 0.460555555555556
	},
	{
		id: 2417,
		time: 2416,
		velocity: 3.43194444444444,
		power: 1520.05303294451,
		road: 21203.0156018518,
		acceleration: 0.370833333333333
	},
	{
		id: 2418,
		time: 2417,
		velocity: 3.60833333333333,
		power: 1230.07075458625,
		road: 21206.4981481481,
		acceleration: 0.237870370370371
	},
	{
		id: 2419,
		time: 2418,
		velocity: 3.76416666666667,
		power: 736.462306176004,
		road: 21210.13875,
		acceleration: 0.0782407407407404
	},
	{
		id: 2420,
		time: 2419,
		velocity: 3.66666666666667,
		power: 503.680740720006,
		road: 21213.8229629629,
		acceleration: 0.00898148148148126
	},
	{
		id: 2421,
		time: 2420,
		velocity: 3.63527777777778,
		power: 359.244339963226,
		road: 21217.4956944444,
		acceleration: -0.0319444444444446
	},
	{
		id: 2422,
		time: 2421,
		velocity: 3.66833333333333,
		power: 43.1007987198656,
		road: 21221.0914351852,
		acceleration: -0.122037037037037
	},
	{
		id: 2423,
		time: 2422,
		velocity: 3.30055555555556,
		power: 1058.23223370832,
		road: 21224.7126851852,
		acceleration: 0.173055555555556
	},
	{
		id: 2424,
		time: 2423,
		velocity: 4.15444444444444,
		power: 1312.53985410329,
		road: 21228.5335648148,
		acceleration: 0.226203703703704
	},
	{
		id: 2425,
		time: 2424,
		velocity: 4.34694444444444,
		power: 2112.05516790742,
		road: 21232.6680092592,
		acceleration: 0.400925925925925
	},
	{
		id: 2426,
		time: 2425,
		velocity: 4.50333333333333,
		power: 857.477452905747,
		road: 21237.0371759259,
		acceleration: 0.0685185185185189
	},
	{
		id: 2427,
		time: 2426,
		velocity: 4.36,
		power: 361.183878729806,
		road: 21241.4149537037,
		acceleration: -0.0512962962962957
	},
	{
		id: 2428,
		time: 2427,
		velocity: 4.19305555555556,
		power: -416.13863259573,
		road: 21245.6465740741,
		acceleration: -0.241018518518519
	},
	{
		id: 2429,
		time: 2428,
		velocity: 3.78027777777778,
		power: -358.036135894357,
		road: 21249.6423148148,
		acceleration: -0.23074074074074
	},
	{
		id: 2430,
		time: 2429,
		velocity: 3.66777777777778,
		power: -213.574451369668,
		road: 21253.4252314815,
		acceleration: -0.194907407407407
	},
	{
		id: 2431,
		time: 2430,
		velocity: 3.60833333333333,
		power: 84.8462306696495,
		road: 21257.0556018518,
		acceleration: -0.110185185185185
	},
	{
		id: 2432,
		time: 2431,
		velocity: 3.44972222222222,
		power: -555.891553792014,
		road: 21260.4783333333,
		acceleration: -0.305092592592592
	},
	{
		id: 2433,
		time: 2432,
		velocity: 2.7525,
		power: -563.295338286702,
		road: 21263.5866203703,
		acceleration: -0.323796296296297
	},
	{
		id: 2434,
		time: 2433,
		velocity: 2.63694444444444,
		power: -1389.40813881325,
		road: 21266.1856481481,
		acceleration: -0.694722222222222
	},
	{
		id: 2435,
		time: 2434,
		velocity: 1.36555555555556,
		power: -1105.90252006806,
		road: 21268.0618055555,
		acceleration: -0.751018518518519
	},
	{
		id: 2436,
		time: 2435,
		velocity: 0.499444444444444,
		power: -755.216813806713,
		road: 21269.1229629629,
		acceleration: -0.878981481481481
	},
	{
		id: 2437,
		time: 2436,
		velocity: 0,
		power: -122.318673134096,
		road: 21269.517037037,
		acceleration: -0.455185185185185
	},
	{
		id: 2438,
		time: 2437,
		velocity: 0,
		power: -3.07170016244315,
		road: 21269.6002777778,
		acceleration: -0.166481481481481
	},
	{
		id: 2439,
		time: 2438,
		velocity: 0,
		power: 33.4339058525169,
		road: 21269.705,
		acceleration: 0.209444444444444
	},
	{
		id: 2440,
		time: 2439,
		velocity: 0.628333333333333,
		power: 83.4075598750752,
		road: 21269.9999074074,
		acceleration: 0.170925925925926
	},
	{
		id: 2441,
		time: 2440,
		velocity: 0.512777777777778,
		power: 45.985422945422,
		road: 21270.3802777778,
		acceleration: 0
	},
	{
		id: 2442,
		time: 2441,
		velocity: 0,
		power: -21.3867948942781,
		road: 21270.6559259259,
		acceleration: -0.209444444444444
	},
	{
		id: 2443,
		time: 2442,
		velocity: 0,
		power: 49.8557932706897,
		road: 21270.880324074,
		acceleration: 0.106944444444444
	},
	{
		id: 2444,
		time: 2443,
		velocity: 0.833611111111111,
		power: 196.733328948623,
		road: 21271.3268518518,
		acceleration: 0.337314814814815
	},
	{
		id: 2445,
		time: 2444,
		velocity: 1.01194444444444,
		power: 287.858682051665,
		road: 21272.0798148148,
		acceleration: 0.275555555555556
	},
	{
		id: 2446,
		time: 2445,
		velocity: 0.826666666666667,
		power: -106.924651731149,
		road: 21272.8316203703,
		acceleration: -0.27787037037037
	},
	{
		id: 2447,
		time: 2446,
		velocity: 0,
		power: -88.2661321638419,
		road: 21273.2758333333,
		acceleration: -0.337314814814815
	},
	{
		id: 2448,
		time: 2447,
		velocity: 0,
		power: -19.3212304093567,
		road: 21273.4136111111,
		acceleration: -0.275555555555556
	},
	{
		id: 2449,
		time: 2448,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2450,
		time: 2449,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2451,
		time: 2450,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2452,
		time: 2451,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2453,
		time: 2452,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2454,
		time: 2453,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2455,
		time: 2454,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2456,
		time: 2455,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2457,
		time: 2456,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2458,
		time: 2457,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2459,
		time: 2458,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2460,
		time: 2459,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2461,
		time: 2460,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2462,
		time: 2461,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2463,
		time: 2462,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	},
	{
		id: 2464,
		time: 2463,
		velocity: 0,
		power: 0,
		road: 21273.4136111111,
		acceleration: 0
	}
];
export default train2;
