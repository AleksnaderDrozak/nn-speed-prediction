export const train6 = 
[
	{
		id: 1,
		time: 0,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 3,
		time: 2,
		velocity: 0,
		power: 34.2535497987854,
		road: 0.106296296296296,
		acceleration: 0.212592592592593
	},
	{
		id: 4,
		time: 3,
		velocity: 0.637777777777778,
		power: 25.6902055105012,
		road: 0.318888888888889,
		acceleration: 0
	},
	{
		id: 5,
		time: 4,
		velocity: 0,
		power: 25.6902055105012,
		road: 0.531481481481481,
		acceleration: 0
	},
	{
		id: 6,
		time: 5,
		velocity: 0,
		power: -8.56595230669265,
		road: 0.637777777777778,
		acceleration: -0.212592592592593
	},
	{
		id: 7,
		time: 6,
		velocity: 0,
		power: 0,
		road: 0.637777777777778,
		acceleration: 0
	},
	{
		id: 8,
		time: 7,
		velocity: 0,
		power: 0,
		road: 0.637777777777778,
		acceleration: 0
	},
	{
		id: 9,
		time: 8,
		velocity: 0,
		power: 0,
		road: 0.637777777777778,
		acceleration: 0
	},
	{
		id: 10,
		time: 9,
		velocity: 0,
		power: 0,
		road: 0.637777777777778,
		acceleration: 0
	},
	{
		id: 11,
		time: 10,
		velocity: 0,
		power: 0,
		road: 0.637777777777778,
		acceleration: 0
	},
	{
		id: 12,
		time: 11,
		velocity: 0,
		power: 12.1038582639556,
		road: 0.691944444444444,
		acceleration: 0.108333333333333
	},
	{
		id: 13,
		time: 12,
		velocity: 0.325,
		power: 13.0892954752796,
		road: 0.800277777777778,
		acceleration: 0
	},
	{
		id: 14,
		time: 13,
		velocity: 0,
		power: 13.0892954752796,
		road: 0.908611111111111,
		acceleration: 0
	},
	{
		id: 15,
		time: 14,
		velocity: 0,
		power: 0.985092105263159,
		road: 0.962777777777778,
		acceleration: -0.108333333333333
	},
	{
		id: 16,
		time: 15,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 17,
		time: 16,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 18,
		time: 17,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 19,
		time: 18,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 20,
		time: 19,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 21,
		time: 20,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 22,
		time: 21,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 23,
		time: 22,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 24,
		time: 23,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 25,
		time: 24,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 26,
		time: 25,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 27,
		time: 26,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 28,
		time: 27,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 29,
		time: 28,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 30,
		time: 29,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 31,
		time: 30,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 32,
		time: 31,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 33,
		time: 32,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 34,
		time: 33,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 35,
		time: 34,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 36,
		time: 35,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 37,
		time: 36,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 38,
		time: 37,
		velocity: 0,
		power: 0,
		road: 0.962777777777778,
		acceleration: 0
	},
	{
		id: 39,
		time: 38,
		velocity: 0,
		power: 7.19525924859375,
		road: 1.00027777777778,
		acceleration: 0.075
	},
	{
		id: 40,
		time: 39,
		velocity: 0.225,
		power: 9.06157112876645,
		road: 1.07527777777778,
		acceleration: 0
	},
	{
		id: 41,
		time: 40,
		velocity: 0,
		power: 9.06157112876645,
		road: 1.15027777777778,
		acceleration: 0
	},
	{
		id: 42,
		time: 41,
		velocity: 0,
		power: 1.86619736842105,
		road: 1.18777777777778,
		acceleration: -0.075
	},
	{
		id: 43,
		time: 42,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 44,
		time: 43,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 45,
		time: 44,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 46,
		time: 45,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 47,
		time: 46,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 48,
		time: 47,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 49,
		time: 48,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 50,
		time: 49,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 51,
		time: 50,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 52,
		time: 51,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 53,
		time: 52,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 54,
		time: 53,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 55,
		time: 54,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 56,
		time: 55,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 57,
		time: 56,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 58,
		time: 57,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 59,
		time: 58,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 60,
		time: 59,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 61,
		time: 60,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 62,
		time: 61,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 63,
		time: 62,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 64,
		time: 63,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 65,
		time: 64,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 66,
		time: 65,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 67,
		time: 66,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 68,
		time: 67,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 69,
		time: 68,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 70,
		time: 69,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 71,
		time: 70,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 72,
		time: 71,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 73,
		time: 72,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 74,
		time: 73,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 75,
		time: 74,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 76,
		time: 75,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 77,
		time: 76,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 78,
		time: 77,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 79,
		time: 78,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 80,
		time: 79,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 81,
		time: 80,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 82,
		time: 81,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 83,
		time: 82,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 84,
		time: 83,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 85,
		time: 84,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 86,
		time: 85,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 87,
		time: 86,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 88,
		time: 87,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 89,
		time: 88,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 90,
		time: 89,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 91,
		time: 90,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 92,
		time: 91,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 93,
		time: 92,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 94,
		time: 93,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 95,
		time: 94,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 96,
		time: 95,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 97,
		time: 96,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 98,
		time: 97,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 99,
		time: 98,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 100,
		time: 99,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 101,
		time: 100,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 102,
		time: 101,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 103,
		time: 102,
		velocity: 0,
		power: 0,
		road: 1.18777777777778,
		acceleration: 0
	},
	{
		id: 104,
		time: 103,
		velocity: 0,
		power: 10.0698574491285,
		road: 1.23546296296296,
		acceleration: 0.0953703703703704
	},
	{
		id: 105,
		time: 104,
		velocity: 0.286111111111111,
		power: 11.5229182770745,
		road: 1.33083333333333,
		acceleration: 0
	},
	{
		id: 106,
		time: 105,
		velocity: 0,
		power: 11.5229182770745,
		road: 1.4262037037037,
		acceleration: 0
	},
	{
		id: 107,
		time: 106,
		velocity: 0,
		power: 5.04497425670262,
		road: 1.49597222222222,
		acceleration: -0.0512037037037037
	},
	{
		id: 108,
		time: 107,
		velocity: 0.1325,
		power: 5.33617045558515,
		road: 1.54013888888889,
		acceleration: 0
	},
	{
		id: 109,
		time: 108,
		velocity: 0,
		power: 5.33617045558515,
		road: 1.58430555555556,
		acceleration: 0
	},
	{
		id: 110,
		time: 109,
		velocity: 0,
		power: 1.74404868421053,
		road: 1.60638888888889,
		acceleration: -0.0441666666666667
	},
	{
		id: 111,
		time: 110,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 112,
		time: 111,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 113,
		time: 112,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 114,
		time: 113,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 115,
		time: 114,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 116,
		time: 115,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 117,
		time: 116,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 118,
		time: 117,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 119,
		time: 118,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 120,
		time: 119,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 121,
		time: 120,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 122,
		time: 121,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 123,
		time: 122,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 124,
		time: 123,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 125,
		time: 124,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 126,
		time: 125,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 127,
		time: 126,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 128,
		time: 127,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 129,
		time: 128,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 130,
		time: 129,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 131,
		time: 130,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 132,
		time: 131,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 133,
		time: 132,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 134,
		time: 133,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 135,
		time: 134,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 136,
		time: 135,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 137,
		time: 136,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 138,
		time: 137,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 139,
		time: 138,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 140,
		time: 139,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 141,
		time: 140,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 142,
		time: 141,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 143,
		time: 142,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 144,
		time: 143,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 145,
		time: 144,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 146,
		time: 145,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 147,
		time: 146,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 148,
		time: 147,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 149,
		time: 148,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 150,
		time: 149,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 151,
		time: 150,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 152,
		time: 151,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 153,
		time: 152,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 154,
		time: 153,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 155,
		time: 154,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 156,
		time: 155,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 157,
		time: 156,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 158,
		time: 157,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 159,
		time: 158,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 160,
		time: 159,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 161,
		time: 160,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 162,
		time: 161,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 163,
		time: 162,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 164,
		time: 163,
		velocity: 0,
		power: 0,
		road: 1.60638888888889,
		acceleration: 0
	},
	{
		id: 165,
		time: 164,
		velocity: 0,
		power: 4.35529926302666,
		road: 1.63208333333333,
		acceleration: 0.0513888888888889
	},
	{
		id: 166,
		time: 165,
		velocity: 0.154166666666667,
		power: 6.2087710406732,
		road: 1.68347222222222,
		acceleration: 0
	},
	{
		id: 167,
		time: 166,
		velocity: 0,
		power: 6.2087710406732,
		road: 1.73486111111111,
		acceleration: 0
	},
	{
		id: 168,
		time: 167,
		velocity: 0,
		power: 1.85343494152047,
		road: 1.76055555555556,
		acceleration: -0.0513888888888889
	},
	{
		id: 169,
		time: 168,
		velocity: 0,
		power: 0,
		road: 1.76055555555556,
		acceleration: 0
	},
	{
		id: 170,
		time: 169,
		velocity: 0,
		power: 0,
		road: 1.76055555555556,
		acceleration: 0
	},
	{
		id: 171,
		time: 170,
		velocity: 0,
		power: 0,
		road: 1.76055555555556,
		acceleration: 0
	},
	{
		id: 172,
		time: 171,
		velocity: 0,
		power: 0,
		road: 1.76055555555556,
		acceleration: 0
	},
	{
		id: 173,
		time: 172,
		velocity: 0,
		power: 0,
		road: 1.76055555555556,
		acceleration: 0
	},
	{
		id: 174,
		time: 173,
		velocity: 0,
		power: 0,
		road: 1.76055555555556,
		acceleration: 0
	},
	{
		id: 175,
		time: 174,
		velocity: 0,
		power: 49.8274050332797,
		road: 1.89393518518519,
		acceleration: 0.266759259259259
	},
	{
		id: 176,
		time: 175,
		velocity: 0.800277777777778,
		power: 298.617122580894,
		road: 2.40509259259259,
		acceleration: 0.488796296296296
	},
	{
		id: 177,
		time: 176,
		velocity: 1.46638888888889,
		power: 893.645283206173,
		road: 3.51949074074074,
		acceleration: 0.717685185185185
	},
	{
		id: 178,
		time: 177,
		velocity: 2.15305555555556,
		power: 1270.90399567453,
		road: 5.30365740740741,
		acceleration: 0.621851851851852
	},
	{
		id: 179,
		time: 178,
		velocity: 2.66583333333333,
		power: 512.919058129891,
		road: 7.45916666666667,
		acceleration: 0.120833333333334
	},
	{
		id: 180,
		time: 179,
		velocity: 1.82888888888889,
		power: -192.233944200374,
		road: 9.56194444444444,
		acceleration: -0.226296296296296
	},
	{
		id: 181,
		time: 180,
		velocity: 1.47416666666667,
		power: -665.473865677233,
		road: 11.2831481481481,
		acceleration: -0.536851851851852
	},
	{
		id: 182,
		time: 181,
		velocity: 1.05527777777778,
		power: -176.313962855629,
		road: 12.6011574074074,
		acceleration: -0.269537037037037
	},
	{
		id: 183,
		time: 182,
		velocity: 1.02027777777778,
		power: -205.47107242901,
		road: 13.613287037037,
		acceleration: -0.342222222222223
	},
	{
		id: 184,
		time: 183,
		velocity: 0.4475,
		power: -57.2236183151139,
		road: 14.3493981481481,
		acceleration: -0.209814814814815
	},
	{
		id: 185,
		time: 184,
		velocity: 0.425833333333333,
		power: 83.59132959288,
		road: 14.9860185185185,
		acceleration: 0.0108333333333334
	},
	{
		id: 186,
		time: 185,
		velocity: 1.05277777777778,
		power: 394.349066668931,
		road: 15.8150462962963,
		acceleration: 0.373981481481481
	},
	{
		id: 187,
		time: 186,
		velocity: 1.56944444444444,
		power: 502.081231331881,
		road: 16.9919444444444,
		acceleration: 0.32175925925926
	},
	{
		id: 188,
		time: 187,
		velocity: 1.39111111111111,
		power: -245.375765329107,
		road: 18.1542592592593,
		acceleration: -0.350925925925926
	},
	{
		id: 189,
		time: 188,
		velocity: 0,
		power: -107.981228955888,
		road: 19.0106481481482,
		acceleration: -0.260925925925926
	},
	{
		id: 190,
		time: 189,
		velocity: 0.786666666666667,
		power: 214.968764202042,
		road: 19.8138425925926,
		acceleration: 0.154537037037037
	},
	{
		id: 191,
		time: 190,
		velocity: 1.85472222222222,
		power: 1404.51001037371,
		road: 21.1743981481482,
		acceleration: 0.960185185185185
	},
	{
		id: 192,
		time: 191,
		velocity: 2.88055555555556,
		power: 2719.20502924518,
		road: 23.5523611111111,
		acceleration: 1.07462962962963
	},
	{
		id: 193,
		time: 192,
		velocity: 4.01055555555556,
		power: 2904.08757397022,
		road: 26.8629166666667,
		acceleration: 0.790555555555555
	},
	{
		id: 194,
		time: 193,
		velocity: 4.22638888888889,
		power: 2609.95205138775,
		road: 30.8456481481482,
		acceleration: 0.553796296296297
	},
	{
		id: 195,
		time: 194,
		velocity: 4.54194444444444,
		power: 896.292673153269,
		road: 35.1461111111111,
		acceleration: 0.0816666666666661
	},
	{
		id: 196,
		time: 195,
		velocity: 4.25555555555556,
		power: 719.21102632195,
		road: 39.5052314814815,
		acceleration: 0.0356481481481481
	},
	{
		id: 197,
		time: 196,
		velocity: 4.33333333333333,
		power: -229.161102633294,
		road: 43.7851388888889,
		acceleration: -0.194074074074074
	},
	{
		id: 198,
		time: 197,
		velocity: 3.95972222222222,
		power: -323.849410812384,
		road: 47.8577777777778,
		acceleration: -0.220462962962964
	},
	{
		id: 199,
		time: 198,
		velocity: 3.59416666666667,
		power: -521.347203334177,
		road: 51.6805555555556,
		acceleration: -0.279259259259259
	},
	{
		id: 200,
		time: 199,
		velocity: 3.49555555555556,
		power: 959.436003747249,
		road: 55.430787037037,
		acceleration: 0.134166666666667
	},
	{
		id: 201,
		time: 200,
		velocity: 4.36222222222222,
		power: 1941.07299385808,
		road: 59.4351388888889,
		acceleration: 0.374074074074074
	},
	{
		id: 202,
		time: 201,
		velocity: 4.71638888888889,
		power: 3560.33769448296,
		road: 63.9702777777778,
		acceleration: 0.6875
	},
	{
		id: 203,
		time: 202,
		velocity: 5.55805555555556,
		power: 3360.02954883396,
		road: 69.12125,
		acceleration: 0.544166666666667
	},
	{
		id: 204,
		time: 203,
		velocity: 5.99472222222222,
		power: 4634.68179298258,
		road: 74.8935648148148,
		acceleration: 0.698518518518519
	},
	{
		id: 205,
		time: 204,
		velocity: 6.81194444444444,
		power: 5176.12419921152,
		road: 81.3605092592593,
		acceleration: 0.69074074074074
	},
	{
		id: 206,
		time: 205,
		velocity: 7.63027777777778,
		power: 6345.80283483595,
		road: 88.5579166666667,
		acceleration: 0.770185185185186
	},
	{
		id: 207,
		time: 206,
		velocity: 8.30527777777778,
		power: 6338.30608814712,
		road: 96.4793981481482,
		acceleration: 0.677962962962963
	},
	{
		id: 208,
		time: 207,
		velocity: 8.84583333333333,
		power: 4551.82291045922,
		road: 104.938611111111,
		acceleration: 0.397500000000001
	},
	{
		id: 209,
		time: 208,
		velocity: 8.82277777777778,
		power: 2247.4700745544,
		road: 113.647037037037,
		acceleration: 0.100925925925925
	},
	{
		id: 210,
		time: 209,
		velocity: 8.60805555555556,
		power: 183.013919703962,
		road: 122.332037037037,
		acceleration: -0.147777777777776
	},
	{
		id: 211,
		time: 210,
		velocity: 8.4025,
		power: 339.511645472598,
		road: 130.879722222222,
		acceleration: -0.126851851851853
	},
	{
		id: 212,
		time: 211,
		velocity: 8.44222222222222,
		power: 1281.50417685124,
		road: 139.359398148148,
		acceleration: -0.00916666666666721
	},
	{
		id: 213,
		time: 212,
		velocity: 8.58055555555556,
		power: 954.561728756162,
		road: 147.81,
		acceleration: -0.0489814814814817
	},
	{
		id: 214,
		time: 213,
		velocity: 8.25555555555555,
		power: -518.386910689575,
		road: 156.120185185185,
		acceleration: -0.23185185185185
	},
	{
		id: 215,
		time: 214,
		velocity: 7.74666666666667,
		power: -3358.96581009033,
		road: 164.009490740741,
		acceleration: -0.609907407407408
	},
	{
		id: 216,
		time: 215,
		velocity: 6.75083333333333,
		power: -6306.75047235624,
		road: 171.044907407407,
		acceleration: -1.09787037037037
	},
	{
		id: 217,
		time: 216,
		velocity: 4.96194444444444,
		power: -6735.80459120132,
		road: 176.847453703704,
		acceleration: -1.36787037037037
	},
	{
		id: 218,
		time: 217,
		velocity: 3.64305555555556,
		power: -4076.06890611947,
		road: 181.427962962963,
		acceleration: -1.0762037037037
	},
	{
		id: 219,
		time: 218,
		velocity: 3.52222222222222,
		power: -381.690592606804,
		road: 185.351111111111,
		acceleration: -0.238518518518518
	},
	{
		id: 220,
		time: 219,
		velocity: 4.24638888888889,
		power: 2520.30605754951,
		road: 189.413333333333,
		acceleration: 0.516666666666666
	},
	{
		id: 221,
		time: 220,
		velocity: 5.19305555555556,
		power: 4990.80398842197,
		road: 194.211157407407,
		acceleration: 0.954537037037038
	},
	{
		id: 222,
		time: 221,
		velocity: 6.38583333333333,
		power: 7217.73701254346,
		road: 200.061759259259,
		acceleration: 1.15101851851852
	},
	{
		id: 223,
		time: 222,
		velocity: 7.69944444444444,
		power: 9275.38991649599,
		road: 207.102592592593,
		acceleration: 1.22944444444444
	},
	{
		id: 224,
		time: 223,
		velocity: 8.88138888888889,
		power: 11014.927374452,
		road: 215.374537037037,
		acceleration: 1.23277777777778
	},
	{
		id: 225,
		time: 224,
		velocity: 10.0841666666667,
		power: 10996.9353090588,
		road: 224.787407407407,
		acceleration: 1.04907407407407
	},
	{
		id: 226,
		time: 225,
		velocity: 10.8466666666667,
		power: 9027.09360444965,
		road: 235.090833333333,
		acceleration: 0.732037037037037
	},
	{
		id: 227,
		time: 226,
		velocity: 11.0775,
		power: 4042.12978654227,
		road: 245.860740740741,
		acceleration: 0.200925925925928
	},
	{
		id: 228,
		time: 227,
		velocity: 10.6869444444444,
		power: 585.002848047566,
		road: 256.662916666667,
		acceleration: -0.136388888888892
	},
	{
		id: 229,
		time: 228,
		velocity: 10.4375,
		power: -1574.77788999423,
		road: 267.223518518519,
		acceleration: -0.346759259259258
	},
	{
		id: 230,
		time: 229,
		velocity: 10.0372222222222,
		power: -728.863685747287,
		road: 277.480092592593,
		acceleration: -0.261296296296296
	},
	{
		id: 231,
		time: 230,
		velocity: 9.90305555555556,
		power: -342.099830912366,
		road: 287.496111111111,
		acceleration: -0.219814814814816
	},
	{
		id: 232,
		time: 231,
		velocity: 9.77805555555556,
		power: -21.2802991410566,
		road: 297.310231481481,
		acceleration: -0.183981481481482
	},
	{
		id: 233,
		time: 232,
		velocity: 9.48527777777778,
		power: 259.072871864677,
		road: 306.956527777778,
		acceleration: -0.151666666666667
	},
	{
		id: 234,
		time: 233,
		velocity: 9.44805555555556,
		power: -62.7959899285661,
		road: 316.434490740741,
		acceleration: -0.185
	},
	{
		id: 235,
		time: 234,
		velocity: 9.22305555555556,
		power: 499.477729591637,
		road: 325.759861111111,
		acceleration: -0.120185185185184
	},
	{
		id: 236,
		time: 235,
		velocity: 9.12472222222222,
		power: 394.466358105701,
		road: 334.960092592593,
		acceleration: -0.130092592592593
	},
	{
		id: 237,
		time: 236,
		velocity: 9.05777777777778,
		power: 915.745420690508,
		road: 344.061064814815,
		acceleration: -0.0684259259259257
	},
	{
		id: 238,
		time: 237,
		velocity: 9.01777777777778,
		power: 525.305000934037,
		road: 353.071851851852,
		acceleration: -0.111944444444445
	},
	{
		id: 239,
		time: 238,
		velocity: 8.78888888888889,
		power: 387.413664540727,
		road: 361.963564814815,
		acceleration: -0.126203703703702
	},
	{
		id: 240,
		time: 239,
		velocity: 8.67916666666667,
		power: -47.8139697192247,
		road: 370.704074074074,
		acceleration: -0.176203703703704
	},
	{
		id: 241,
		time: 240,
		velocity: 8.48916666666667,
		power: 242.308133948818,
		road: 379.286851851852,
		acceleration: -0.13925925925926
	},
	{
		id: 242,
		time: 241,
		velocity: 8.37111111111111,
		power: 845.329084676706,
		road: 387.76837962963,
		acceleration: -0.0632407407407403
	},
	{
		id: 243,
		time: 242,
		velocity: 8.48944444444444,
		power: 1022.70383393284,
		road: 396.198287037037,
		acceleration: -0.0399999999999991
	},
	{
		id: 244,
		time: 243,
		velocity: 8.36916666666667,
		power: 1824.33988433403,
		road: 404.637962962963,
		acceleration: 0.0595370370370354
	},
	{
		id: 245,
		time: 244,
		velocity: 8.54972222222222,
		power: 1005.47534521998,
		road: 413.086111111111,
		acceleration: -0.0425925925925927
	},
	{
		id: 246,
		time: 245,
		velocity: 8.36166666666667,
		power: 2061.97499365538,
		road: 421.556898148148,
		acceleration: 0.0878703703703714
	},
	{
		id: 247,
		time: 246,
		velocity: 8.63277777777778,
		power: 4890.940616604,
		road: 430.28087962963,
		acceleration: 0.418518518518519
	},
	{
		id: 248,
		time: 247,
		velocity: 9.80527777777778,
		power: 9865.39991808421,
		road: 439.676666666667,
		acceleration: 0.925092592592591
	},
	{
		id: 249,
		time: 248,
		velocity: 11.1369444444444,
		power: 10643.8149322279,
		road: 449.983148148148,
		acceleration: 0.896296296296295
	},
	{
		id: 250,
		time: 249,
		velocity: 11.3216666666667,
		power: 7250.5962946756,
		road: 460.985555555556,
		acceleration: 0.495555555555558
	},
	{
		id: 251,
		time: 250,
		velocity: 11.2919444444444,
		power: 3537.80163655141,
		road: 472.299907407407,
		acceleration: 0.128333333333332
	},
	{
		id: 252,
		time: 251,
		velocity: 11.5219444444444,
		power: 4803.56414750047,
		road: 483.796527777778,
		acceleration: 0.236203703703703
	},
	{
		id: 253,
		time: 252,
		velocity: 12.0302777777778,
		power: 8802.98249903009,
		road: 495.695416666667,
		acceleration: 0.568333333333333
	},
	{
		id: 254,
		time: 253,
		velocity: 12.9969444444444,
		power: 10481.5702204727,
		road: 508.209490740741,
		acceleration: 0.662037037037038
	},
	{
		id: 255,
		time: 254,
		velocity: 13.5080555555556,
		power: 10507.122761489,
		road: 521.360601851852,
		acceleration: 0.612037037037036
	},
	{
		id: 256,
		time: 255,
		velocity: 13.8663888888889,
		power: 8359.84241018422,
		road: 535.021851851852,
		acceleration: 0.408240740740743
	},
	{
		id: 257,
		time: 256,
		velocity: 14.2216666666667,
		power: 7007.36487518678,
		road: 549.030092592593,
		acceleration: 0.28574074074074
	},
	{
		id: 258,
		time: 257,
		velocity: 14.3652777777778,
		power: 6304.58277607216,
		road: 563.291574074074,
		acceleration: 0.220740740740739
	},
	{
		id: 259,
		time: 258,
		velocity: 14.5286111111111,
		power: 4734.08889142472,
		road: 577.712916666667,
		acceleration: 0.0989814814814824
	},
	{
		id: 260,
		time: 259,
		velocity: 14.5186111111111,
		power: 4205.70293887746,
		road: 592.212592592593,
		acceleration: 0.0576851851851856
	},
	{
		id: 261,
		time: 260,
		velocity: 14.5383333333333,
		power: 1647.06587022034,
		road: 606.678009259259,
		acceleration: -0.126203703703704
	},
	{
		id: 262,
		time: 261,
		velocity: 14.15,
		power: -649.210797587381,
		road: 620.935462962963,
		acceleration: -0.289722222222224
	},
	{
		id: 263,
		time: 262,
		velocity: 13.6494444444444,
		power: -2387.83506317847,
		road: 634.839907407408,
		acceleration: -0.416296296296295
	},
	{
		id: 264,
		time: 263,
		velocity: 13.2894444444444,
		power: -3154.40708996375,
		road: 648.298657407408,
		acceleration: -0.475092592592594
	},
	{
		id: 265,
		time: 264,
		velocity: 12.7247222222222,
		power: -4576.64968645826,
		road: 661.223518518519,
		acceleration: -0.592685185185184
	},
	{
		id: 266,
		time: 265,
		velocity: 11.8713888888889,
		power: -5711.75021450095,
		road: 673.501990740741,
		acceleration: -0.700092592592592
	},
	{
		id: 267,
		time: 266,
		velocity: 11.1891666666667,
		power: -5507.10618438933,
		road: 685.079490740741,
		acceleration: -0.701851851851853
	},
	{
		id: 268,
		time: 267,
		velocity: 10.6191666666667,
		power: -4351.31714578917,
		road: 695.999722222222,
		acceleration: -0.612685185185185
	},
	{
		id: 269,
		time: 268,
		velocity: 10.0333333333333,
		power: -3933.9038864745,
		road: 706.319861111111,
		acceleration: -0.5875
	},
	{
		id: 270,
		time: 269,
		velocity: 9.42666666666667,
		power: -3380.78157159563,
		road: 716.073796296296,
		acceleration: -0.544907407407408
	},
	{
		id: 271,
		time: 270,
		velocity: 8.98444444444444,
		power: -4809.40860423504,
		road: 725.191157407408,
		acceleration: -0.728240740740741
	},
	{
		id: 272,
		time: 271,
		velocity: 7.84861111111111,
		power: -4703.06747628364,
		road: 733.565925925926,
		acceleration: -0.756944444444444
	},
	{
		id: 273,
		time: 272,
		velocity: 7.15583333333333,
		power: -4177.83708580225,
		road: 741.194305555556,
		acceleration: -0.735833333333334
	},
	{
		id: 274,
		time: 273,
		velocity: 6.77694444444444,
		power: -1614.50049502176,
		road: 748.256851851852,
		acceleration: -0.395833333333333
	},
	{
		id: 275,
		time: 274,
		velocity: 6.66111111111111,
		power: -1340.74383971572,
		road: 754.939722222222,
		acceleration: -0.363518518518518
	},
	{
		id: 276,
		time: 275,
		velocity: 6.06527777777778,
		power: 1864.94588241022,
		road: 761.514120370371,
		acceleration: 0.146574074074074
	},
	{
		id: 277,
		time: 276,
		velocity: 7.21666666666667,
		power: 8940.95874401288,
		road: 768.734212962963,
		acceleration: 1.14481481481482
	},
	{
		id: 278,
		time: 277,
		velocity: 10.0955555555556,
		power: 16116.3553560729,
		road: 777.416388888889,
		acceleration: 1.77935185185185
	},
	{
		id: 279,
		time: 278,
		velocity: 11.4033333333333,
		power: 19610.3983903453,
		road: 787.876990740741,
		acceleration: 1.7775
	},
	{
		id: 280,
		time: 279,
		velocity: 12.5491666666667,
		power: 11839.9670200801,
		road: 799.650694444445,
		acceleration: 0.848703703703704
	},
	{
		id: 281,
		time: 280,
		velocity: 12.6416666666667,
		power: 6129.15232681934,
		road: 812.002083333333,
		acceleration: 0.306666666666665
	},
	{
		id: 282,
		time: 281,
		velocity: 12.3233333333333,
		power: 511.717821051832,
		road: 824.421203703704,
		acceleration: -0.171203703703704
	},
	{
		id: 283,
		time: 282,
		velocity: 12.0355555555556,
		power: -1311.18696746669,
		road: 836.592777777778,
		acceleration: -0.323888888888888
	},
	{
		id: 284,
		time: 283,
		velocity: 11.67,
		power: -442.387106497656,
		road: 848.479351851852,
		acceleration: -0.246111111111111
	},
	{
		id: 285,
		time: 284,
		velocity: 11.585,
		power: -3530.03384511096,
		road: 859.980925925926,
		acceleration: -0.523888888888887
	},
	{
		id: 286,
		time: 285,
		velocity: 10.4638888888889,
		power: -2564.35746199875,
		road: 871.000555555556,
		acceleration: -0.440000000000001
	},
	{
		id: 287,
		time: 286,
		velocity: 10.35,
		power: -1403.29413828059,
		road: 881.63537037037,
		acceleration: -0.329629629629629
	},
	{
		id: 288,
		time: 287,
		velocity: 10.5961111111111,
		power: 4585.8634476439,
		road: 892.236898148148,
		acceleration: 0.263055555555557
	},
	{
		id: 289,
		time: 288,
		velocity: 11.2530555555556,
		power: 4164.32230692041,
		road: 903.074675925926,
		acceleration: 0.209444444444443
	},
	{
		id: 290,
		time: 289,
		velocity: 10.9783333333333,
		power: 5122.99140345017,
		road: 914.161157407407,
		acceleration: 0.287962962962961
	},
	{
		id: 291,
		time: 290,
		velocity: 11.46,
		power: 2968.15294163061,
		road: 925.430231481481,
		acceleration: 0.0772222222222236
	},
	{
		id: 292,
		time: 291,
		velocity: 11.4847222222222,
		power: 4999.94995977288,
		road: 936.866574074074,
		acceleration: 0.257314814814814
	},
	{
		id: 293,
		time: 292,
		velocity: 11.7502777777778,
		power: 3726.13480666293,
		road: 948.497685185185,
		acceleration: 0.132222222222223
	},
	{
		id: 294,
		time: 293,
		velocity: 11.8566666666667,
		power: 4890.72403665237,
		road: 960.308935185185,
		acceleration: 0.228055555555557
	},
	{
		id: 295,
		time: 294,
		velocity: 12.1688888888889,
		power: 3958.92239582142,
		road: 972.302962962963,
		acceleration: 0.137499999999998
	},
	{
		id: 296,
		time: 295,
		velocity: 12.1627777777778,
		power: 4976.07043358214,
		road: 984.474537037037,
		acceleration: 0.217592592592593
	},
	{
		id: 297,
		time: 296,
		velocity: 12.5094444444444,
		power: 6197.49476279905,
		road: 996.908796296296,
		acceleration: 0.307777777777778
	},
	{
		id: 298,
		time: 297,
		velocity: 13.0922222222222,
		power: 7195.35870658509,
		road: 1009.68236111111,
		acceleration: 0.370833333333334
	},
	{
		id: 299,
		time: 298,
		velocity: 13.2752777777778,
		power: 7015.99352234936,
		road: 1022.80902777778,
		acceleration: 0.335370370370368
	},
	{
		id: 300,
		time: 299,
		velocity: 13.5155555555556,
		power: 5445.11226310039,
		road: 1036.20203703704,
		acceleration: 0.197314814814815
	},
	{
		id: 301,
		time: 300,
		velocity: 13.6841666666667,
		power: 6333.92339773925,
		road: 1049.82125,
		acceleration: 0.255092592592593
	},
	{
		id: 302,
		time: 301,
		velocity: 14.0405555555556,
		power: 5619.61544327752,
		road: 1063.66287037037,
		acceleration: 0.189722222222221
	},
	{
		id: 303,
		time: 302,
		velocity: 14.0847222222222,
		power: 5990.0278716909,
		road: 1077.70342592593,
		acceleration: 0.208148148148149
	},
	{
		id: 304,
		time: 303,
		velocity: 14.3086111111111,
		power: 4572.09355885049,
		road: 1091.8962037037,
		acceleration: 0.0962962962962983
	},
	{
		id: 305,
		time: 304,
		velocity: 14.3294444444444,
		power: 4067.10871449278,
		road: 1106.16523148148,
		acceleration: 0.0562037037037033
	},
	{
		id: 306,
		time: 305,
		velocity: 14.2533333333333,
		power: 2656.82642120208,
		road: 1120.43865740741,
		acceleration: -0.0474074074074071
	},
	{
		id: 307,
		time: 306,
		velocity: 14.1663888888889,
		power: 2012.96410967978,
		road: 1134.64199074074,
		acceleration: -0.0927777777777798
	},
	{
		id: 308,
		time: 307,
		velocity: 14.0511111111111,
		power: 1483.56976970375,
		road: 1148.73435185185,
		acceleration: -0.129166666666665
	},
	{
		id: 309,
		time: 308,
		velocity: 13.8658333333333,
		power: 1708.88463020414,
		road: 1162.70740740741,
		acceleration: -0.109444444444444
	},
	{
		id: 310,
		time: 309,
		velocity: 13.8380555555556,
		power: 2246.86855566745,
		road: 1176.59240740741,
		acceleration: -0.0666666666666682
	},
	{
		id: 311,
		time: 310,
		velocity: 13.8511111111111,
		power: 2994.37013515208,
		road: 1190.43953703704,
		acceleration: -0.00907407407407312
	},
	{
		id: 312,
		time: 311,
		velocity: 13.8386111111111,
		power: 3720.42506983969,
		road: 1204.30472222222,
		acceleration: 0.0451851851851846
	},
	{
		id: 313,
		time: 312,
		velocity: 13.9736111111111,
		power: 3902.38586669258,
		road: 1218.22101851852,
		acceleration: 0.0570370370370359
	},
	{
		id: 314,
		time: 313,
		velocity: 14.0222222222222,
		power: 4139.48534402503,
		road: 1232.20203703704,
		acceleration: 0.0724074074074075
	},
	{
		id: 315,
		time: 314,
		velocity: 14.0558333333333,
		power: 4618.94175631247,
		road: 1246.27162037037,
		acceleration: 0.104722222222222
	},
	{
		id: 316,
		time: 315,
		velocity: 14.2877777777778,
		power: 4701.74491437721,
		road: 1260.44685185185,
		acceleration: 0.106574074074075
	},
	{
		id: 317,
		time: 316,
		velocity: 14.3419444444444,
		power: 5825.51859629805,
		road: 1274.76680555556,
		acceleration: 0.18287037037037
	},
	{
		id: 318,
		time: 317,
		velocity: 14.6044444444444,
		power: 5115.20559613252,
		road: 1289.24041666667,
		acceleration: 0.124444444444446
	},
	{
		id: 319,
		time: 318,
		velocity: 14.6611111111111,
		power: 2147.75282309731,
		road: 1303.73092592593,
		acceleration: -0.0906481481481478
	},
	{
		id: 320,
		time: 319,
		velocity: 14.07,
		power: -444.387680315304,
		road: 1318.03842592593,
		acceleration: -0.275370370370373
	},
	{
		id: 321,
		time: 320,
		velocity: 13.7783333333333,
		power: -3193.60515841535,
		road: 1331.96976851852,
		acceleration: -0.476944444444444
	},
	{
		id: 322,
		time: 321,
		velocity: 13.2302777777778,
		power: -5304.83902389689,
		road: 1345.34069444444,
		acceleration: -0.643888888888888
	},
	{
		id: 323,
		time: 322,
		velocity: 12.1383333333333,
		power: -7767.2080183987,
		road: 1357.95847222222,
		acceleration: -0.862407407407408
	},
	{
		id: 324,
		time: 323,
		velocity: 11.1911111111111,
		power: -14296.2124428387,
		road: 1369.38851851852,
		acceleration: -1.51305555555555
	},
	{
		id: 325,
		time: 324,
		velocity: 8.69111111111111,
		power: -14667.0651082947,
		road: 1379.18532407407,
		acceleration: -1.75342592592593
	},
	{
		id: 326,
		time: 325,
		velocity: 6.87805555555556,
		power: -15323.6989854911,
		road: 1386.99296296296,
		acceleration: -2.22490740740741
	},
	{
		id: 327,
		time: 326,
		velocity: 4.51638888888889,
		power: -9907.63882236943,
		road: 1392.70208333333,
		acceleration: -1.97212962962963
	},
	{
		id: 328,
		time: 327,
		velocity: 2.77472222222222,
		power: -5928.63136571863,
		road: 1396.54453703704,
		acceleration: -1.7612037037037
	},
	{
		id: 329,
		time: 328,
		velocity: 1.59444444444444,
		power: -2088.43614218121,
		road: 1398.99101851852,
		acceleration: -1.03074074074074
	},
	{
		id: 330,
		time: 329,
		velocity: 1.42416666666667,
		power: -684.101187301736,
		road: 1400.6387037037,
		acceleration: -0.566851851851852
	},
	{
		id: 331,
		time: 330,
		velocity: 1.07416666666667,
		power: 304.75119940291,
		road: 1402.05236111111,
		acceleration: 0.098796296296296
	},
	{
		id: 332,
		time: 331,
		velocity: 1.89083333333333,
		power: 623.619154727152,
		road: 1403.65601851852,
		acceleration: 0.281203703703704
	},
	{
		id: 333,
		time: 332,
		velocity: 2.26777777777778,
		power: 1830.51413375309,
		road: 1405.78787037037,
		acceleration: 0.775185185185185
	},
	{
		id: 334,
		time: 333,
		velocity: 3.39972222222222,
		power: 2570.9016364121,
		road: 1408.70546296296,
		acceleration: 0.796296296296297
	},
	{
		id: 335,
		time: 334,
		velocity: 4.27972222222222,
		power: 4167.89033276887,
		road: 1412.52759259259,
		acceleration: 1.01277777777778
	},
	{
		id: 336,
		time: 335,
		velocity: 5.30611111111111,
		power: 3544.81350721868,
		road: 1417.18675925926,
		acceleration: 0.661296296296296
	},
	{
		id: 337,
		time: 336,
		velocity: 5.38361111111111,
		power: 4451.4753193508,
		road: 1422.54212962963,
		acceleration: 0.731111111111111
	},
	{
		id: 338,
		time: 337,
		velocity: 6.47305555555556,
		power: 3740.27933494316,
		road: 1428.51847222222,
		acceleration: 0.510833333333334
	},
	{
		id: 339,
		time: 338,
		velocity: 6.83861111111111,
		power: 3399.35752792758,
		road: 1434.95268518518,
		acceleration: 0.404907407407407
	},
	{
		id: 340,
		time: 339,
		velocity: 6.59833333333333,
		power: -369.035866209012,
		road: 1441.48393518518,
		acceleration: -0.210833333333333
	},
	{
		id: 341,
		time: 340,
		velocity: 5.84055555555556,
		power: -753.781109180824,
		road: 1447.77189814815,
		acceleration: -0.275740740740741
	},
	{
		id: 342,
		time: 341,
		velocity: 6.01138888888889,
		power: -1335.87912510302,
		road: 1453.73037037037,
		acceleration: -0.383240740740741
	},
	{
		id: 343,
		time: 342,
		velocity: 5.44861111111111,
		power: -2367.28855417996,
		road: 1459.19726851852,
		acceleration: -0.599907407407407
	},
	{
		id: 344,
		time: 343,
		velocity: 4.04083333333333,
		power: -5322.05481967819,
		road: 1463.66810185185,
		acceleration: -1.39222222222222
	},
	{
		id: 345,
		time: 344,
		velocity: 1.83472222222222,
		power: -4580.03892629383,
		road: 1466.53472222222,
		acceleration: -1.8162037037037
	},
	{
		id: 346,
		time: 345,
		velocity: 0,
		power: -1484.26919644669,
		road: 1467.81976851852,
		acceleration: -1.34694444444444
	},
	{
		id: 347,
		time: 346,
		velocity: 0,
		power: -140.22417145874,
		road: 1468.12555555555,
		acceleration: -0.611574074074074
	},
	{
		id: 348,
		time: 347,
		velocity: 0,
		power: 0,
		road: 1468.12555555555,
		acceleration: 0
	},
	{
		id: 349,
		time: 348,
		velocity: 0,
		power: 0,
		road: 1468.12555555555,
		acceleration: 0
	},
	{
		id: 350,
		time: 349,
		velocity: 0,
		power: 0,
		road: 1468.12555555555,
		acceleration: 0
	},
	{
		id: 351,
		time: 350,
		velocity: 0,
		power: 0,
		road: 1468.12555555555,
		acceleration: 0
	},
	{
		id: 352,
		time: 351,
		velocity: 0,
		power: 0,
		road: 1468.12555555555,
		acceleration: 0
	},
	{
		id: 353,
		time: 352,
		velocity: 0,
		power: 0,
		road: 1468.12555555555,
		acceleration: 0
	},
	{
		id: 354,
		time: 353,
		velocity: 0,
		power: 61.722728663782,
		road: 1468.27694444444,
		acceleration: 0.302777777777778
	},
	{
		id: 355,
		time: 354,
		velocity: 0.908333333333333,
		power: 1030.32444148926,
		road: 1469.14314814815,
		acceleration: 1.12685185185185
	},
	{
		id: 356,
		time: 355,
		velocity: 3.38055555555556,
		power: 4274.82435189296,
		road: 1471.47398148148,
		acceleration: 1.80240740740741
	},
	{
		id: 357,
		time: 356,
		velocity: 5.40722222222222,
		power: 6083.0834582093,
		road: 1475.44449074074,
		acceleration: 1.47694444444444
	},
	{
		id: 358,
		time: 357,
		velocity: 5.33916666666667,
		power: 3646.04614900167,
		road: 1480.46486111111,
		acceleration: 0.622777777777779
	},
	{
		id: 359,
		time: 358,
		velocity: 5.24888888888889,
		power: 516.010475809562,
		road: 1485.77611111111,
		acceleration: -0.041018518518519
	},
	{
		id: 360,
		time: 359,
		velocity: 5.28416666666667,
		power: 3930.78158016155,
		road: 1491.36439814815,
		acceleration: 0.595092592592592
	},
	{
		id: 361,
		time: 360,
		velocity: 7.12444444444444,
		power: 7580.04828301201,
		road: 1497.79467592593,
		acceleration: 1.08888888888889
	},
	{
		id: 362,
		time: 361,
		velocity: 8.51555555555555,
		power: 13174.8695396894,
		road: 1505.57787037037,
		acceleration: 1.61694444444444
	},
	{
		id: 363,
		time: 362,
		velocity: 10.135,
		power: 14068.8224982583,
		road: 1514.87569444444,
		acceleration: 1.41231481481481
	},
	{
		id: 364,
		time: 363,
		velocity: 11.3613888888889,
		power: 11417.0058363021,
		road: 1525.35643518518,
		acceleration: 0.953518518518521
	},
	{
		id: 365,
		time: 364,
		velocity: 11.3761111111111,
		power: 7211.28196453527,
		road: 1536.55263888889,
		acceleration: 0.477407407407407
	},
	{
		id: 366,
		time: 365,
		velocity: 11.5672222222222,
		power: 4413.31351668945,
		road: 1548.08694444444,
		acceleration: 0.198796296296294
	},
	{
		id: 367,
		time: 366,
		velocity: 11.9577777777778,
		power: 6560.83366467852,
		road: 1559.90847222222,
		acceleration: 0.37564814814815
	},
	{
		id: 368,
		time: 367,
		velocity: 12.5030555555556,
		power: 8193.91669549147,
		road: 1572.16222222222,
		acceleration: 0.488796296296295
	},
	{
		id: 369,
		time: 368,
		velocity: 13.0336111111111,
		power: 5693.69742424581,
		road: 1584.78800925926,
		acceleration: 0.255277777777778
	},
	{
		id: 370,
		time: 369,
		velocity: 12.7236111111111,
		power: 7770.62191651032,
		road: 1597.74458333333,
		acceleration: 0.406296296296299
	},
	{
		id: 371,
		time: 370,
		velocity: 13.7219444444444,
		power: 5754.65615001086,
		road: 1611.01800925926,
		acceleration: 0.227407407407409
	},
	{
		id: 372,
		time: 371,
		velocity: 13.7158333333333,
		power: 8051.3058618571,
		road: 1624.59986111111,
		acceleration: 0.389444444444443
	},
	{
		id: 373,
		time: 372,
		velocity: 13.8919444444444,
		power: 5253.06837464517,
		road: 1638.45708333333,
		acceleration: 0.161296296296296
	},
	{
		id: 374,
		time: 373,
		velocity: 14.2058333333333,
		power: 6760.41782687985,
		road: 1652.52699074074,
		acceleration: 0.264074074074074
	},
	{
		id: 375,
		time: 374,
		velocity: 14.5080555555556,
		power: 8101.87885469526,
		road: 1666.90199074074,
		acceleration: 0.34611111111111
	},
	{
		id: 376,
		time: 375,
		velocity: 14.9302777777778,
		power: 8955.24214671031,
		road: 1681.64300925926,
		acceleration: 0.385925925925926
	},
	{
		id: 377,
		time: 376,
		velocity: 15.3636111111111,
		power: 10747.4946010496,
		road: 1696.81888888889,
		acceleration: 0.483796296296296
	},
	{
		id: 378,
		time: 377,
		velocity: 15.9594444444444,
		power: 12953.3108870932,
		road: 1712.53444444444,
		acceleration: 0.595555555555555
	},
	{
		id: 379,
		time: 378,
		velocity: 16.7169444444444,
		power: 13697.0562406505,
		road: 1728.84805555555,
		acceleration: 0.600555555555559
	},
	{
		id: 380,
		time: 379,
		velocity: 17.1652777777778,
		power: 11298.4707818417,
		road: 1745.66958333333,
		acceleration: 0.415277777777774
	},
	{
		id: 381,
		time: 380,
		velocity: 17.2052777777778,
		power: 4925.7354217741,
		road: 1762.70439814815,
		acceleration: 0.0112962962962975
	},
	{
		id: 382,
		time: 381,
		velocity: 16.7508333333333,
		power: 1555.9541120989,
		road: 1779.64824074074,
		acceleration: -0.193240740740737
	},
	{
		id: 383,
		time: 382,
		velocity: 16.5855555555556,
		power: 3942.50918017001,
		road: 1796.47444444444,
		acceleration: -0.0420370370370406
	},
	{
		id: 384,
		time: 383,
		velocity: 17.0791666666667,
		power: 8970.26273634243,
		road: 1813.41189814815,
		acceleration: 0.264537037037037
	},
	{
		id: 385,
		time: 384,
		velocity: 17.5444444444444,
		power: 13013.671774673,
		road: 1830.7262037037,
		acceleration: 0.489166666666666
	},
	{
		id: 386,
		time: 385,
		velocity: 18.0530555555556,
		power: 14906.5009337008,
		road: 1848.56814814815,
		acceleration: 0.566111111111113
	},
	{
		id: 387,
		time: 386,
		velocity: 18.7775,
		power: 15680.6330756134,
		road: 1866.97875,
		acceleration: 0.571203703703702
	},
	{
		id: 388,
		time: 387,
		velocity: 19.2580555555556,
		power: 13847.9326430812,
		road: 1885.89273148148,
		acceleration: 0.435555555555556
	},
	{
		id: 389,
		time: 388,
		velocity: 19.3597222222222,
		power: 7270.41885454933,
		road: 1905.05541666667,
		acceleration: 0.061851851851852
	},
	{
		id: 390,
		time: 389,
		velocity: 18.9630555555556,
		power: 2447.38148262971,
		road: 1924.14953703704,
		acceleration: -0.198981481481482
	},
	{
		id: 391,
		time: 390,
		velocity: 18.6611111111111,
		power: 82.552700775201,
		road: 1942.98282407407,
		acceleration: -0.322685185185183
	},
	{
		id: 392,
		time: 391,
		velocity: 18.3916666666667,
		power: 1561.46579027551,
		road: 1961.53800925926,
		acceleration: -0.233518518518519
	},
	{
		id: 393,
		time: 392,
		velocity: 18.2625,
		power: 1134.56998603389,
		road: 1979.85060185185,
		acceleration: -0.251666666666669
	},
	{
		id: 394,
		time: 393,
		velocity: 17.9061111111111,
		power: 1777.33258700927,
		road: 1997.93287037037,
		acceleration: -0.20898148148148
	},
	{
		id: 395,
		time: 394,
		velocity: 17.7647222222222,
		power: 1493.47664618383,
		road: 2015.80064814815,
		acceleration: -0.219999999999999
	},
	{
		id: 396,
		time: 395,
		velocity: 17.6025,
		power: 1174.43576572987,
		road: 2033.44180555555,
		acceleration: -0.233240740740744
	},
	{
		id: 397,
		time: 396,
		velocity: 17.2063888888889,
		power: 1611.54336436165,
		road: 2050.86541666667,
		acceleration: -0.201851851851849
	},
	{
		id: 398,
		time: 397,
		velocity: 17.1591666666667,
		power: 2651.35442838853,
		road: 2068.12078703704,
		acceleration: -0.134629629629632
	},
	{
		id: 399,
		time: 398,
		velocity: 17.1986111111111,
		power: 5340.68826996069,
		road: 2085.32398148148,
		acceleration: 0.0302777777777798
	},
	{
		id: 400,
		time: 399,
		velocity: 17.2972222222222,
		power: 5302.28535181588,
		road: 2102.55574074074,
		acceleration: 0.0268518518518484
	},
	{
		id: 401,
		time: 400,
		velocity: 17.2397222222222,
		power: 6277.52720017665,
		road: 2119.84277777778,
		acceleration: 0.0837037037037085
	},
	{
		id: 402,
		time: 401,
		velocity: 17.4497222222222,
		power: 7002.22641481973,
		road: 2137.23314814815,
		acceleration: 0.122962962962962
	},
	{
		id: 403,
		time: 402,
		velocity: 17.6661111111111,
		power: 10833.3650485566,
		road: 2154.855,
		acceleration: 0.339999999999996
	},
	{
		id: 404,
		time: 403,
		velocity: 18.2597222222222,
		power: 9652.40535575675,
		road: 2172.77407407407,
		acceleration: 0.254444444444445
	},
	{
		id: 405,
		time: 404,
		velocity: 18.2130555555556,
		power: 8818.07631237542,
		road: 2190.91777777778,
		acceleration: 0.194814814814816
	},
	{
		id: 406,
		time: 405,
		velocity: 18.2505555555556,
		power: 6794.71062425276,
		road: 2209.19523148148,
		acceleration: 0.0726851851851826
	},
	{
		id: 407,
		time: 406,
		velocity: 18.4777777777778,
		power: 8143.01443757943,
		road: 2227.58138888889,
		acceleration: 0.144722222222224
	},
	{
		id: 408,
		time: 407,
		velocity: 18.6472222222222,
		power: 9213.76355419454,
		road: 2246.13847222222,
		acceleration: 0.197129629629629
	},
	{
		id: 409,
		time: 408,
		velocity: 18.8419444444444,
		power: 9148.46730737265,
		road: 2264.8862037037,
		acceleration: 0.18416666666667
	},
	{
		id: 410,
		time: 409,
		velocity: 19.0302777777778,
		power: 10760.9687240645,
		road: 2283.85708333333,
		acceleration: 0.26212962962963
	},
	{
		id: 411,
		time: 410,
		velocity: 19.4336111111111,
		power: 10392.0955430116,
		road: 2303.07361111111,
		acceleration: 0.229166666666664
	},
	{
		id: 412,
		time: 411,
		velocity: 19.5294444444444,
		power: 9874.83369189256,
		road: 2322.50004629629,
		acceleration: 0.190648148148146
	},
	{
		id: 413,
		time: 412,
		velocity: 19.6022222222222,
		power: 8506.61334564657,
		road: 2342.07694444444,
		acceleration: 0.110277777777778
	},
	{
		id: 414,
		time: 413,
		velocity: 19.7644444444444,
		power: 10901.4750378732,
		road: 2361.82356481481,
		acceleration: 0.229166666666671
	},
	{
		id: 415,
		time: 414,
		velocity: 20.2169444444444,
		power: 11543.9115135037,
		road: 2381.8099537037,
		acceleration: 0.250370370370369
	},
	{
		id: 416,
		time: 415,
		velocity: 20.3533333333333,
		power: 12416.0751137362,
		road: 2402.06217592592,
		acceleration: 0.281296296296297
	},
	{
		id: 417,
		time: 416,
		velocity: 20.6083333333333,
		power: 13786.028151698,
		road: 2422.62208333333,
		acceleration: 0.334074074074074
	},
	{
		id: 418,
		time: 417,
		velocity: 21.2191666666667,
		power: 13325.6842102978,
		road: 2443.49560185185,
		acceleration: 0.293148148148145
	},
	{
		id: 419,
		time: 418,
		velocity: 21.2327777777778,
		power: 14357.2874296152,
		road: 2464.67907407407,
		acceleration: 0.326759259259262
	},
	{
		id: 420,
		time: 419,
		velocity: 21.5886111111111,
		power: 10480.3959209527,
		road: 2486.08842592592,
		acceleration: 0.125
	},
	{
		id: 421,
		time: 420,
		velocity: 21.5941666666667,
		power: 13187.3614044078,
		road: 2507.68365740741,
		acceleration: 0.24675925925926
	},
	{
		id: 422,
		time: 421,
		velocity: 21.9730555555556,
		power: 9934.27890281478,
		road: 2529.44328703704,
		acceleration: 0.0820370370370327
	},
	{
		id: 423,
		time: 422,
		velocity: 21.8347222222222,
		power: 13451.4056621334,
		road: 2551.36481481481,
		acceleration: 0.241759259259265
	},
	{
		id: 424,
		time: 423,
		velocity: 22.3194444444444,
		power: 11952.2152589442,
		road: 2573.48736111111,
		acceleration: 0.160277777777775
	},
	{
		id: 425,
		time: 424,
		velocity: 22.4538888888889,
		power: 16775.6296719876,
		road: 2595.87574074074,
		acceleration: 0.371388888888891
	},
	{
		id: 426,
		time: 425,
		velocity: 22.9488888888889,
		power: 12289.7221879718,
		road: 2618.52449074074,
		acceleration: 0.149351851851851
	},
	{
		id: 427,
		time: 426,
		velocity: 22.7675,
		power: 13276.3443589607,
		road: 2641.34087962963,
		acceleration: 0.185925925925925
	},
	{
		id: 428,
		time: 427,
		velocity: 23.0116666666667,
		power: 10933.5923496716,
		road: 2664.28662037037,
		acceleration: 0.0727777777777767
	},
	{
		id: 429,
		time: 428,
		velocity: 23.1672222222222,
		power: 18676.4931689972,
		road: 2687.47333333333,
		acceleration: 0.409166666666671
	},
	{
		id: 430,
		time: 429,
		velocity: 23.995,
		power: 16290.2454203877,
		road: 2711.00564814815,
		acceleration: 0.282037037037036
	},
	{
		id: 431,
		time: 430,
		velocity: 23.8577777777778,
		power: 15520.8528228846,
		road: 2734.79578703704,
		acceleration: 0.233611111111109
	},
	{
		id: 432,
		time: 431,
		velocity: 23.8680555555556,
		power: 7002.05866667268,
		road: 2758.63217592592,
		acceleration: -0.141111111111112
	},
	{
		id: 433,
		time: 432,
		velocity: 23.5716666666667,
		power: 2084.00204608533,
		road: 2782.22375,
		acceleration: -0.348518518518517
	},
	{
		id: 434,
		time: 433,
		velocity: 22.8122222222222,
		power: -2341.56443640177,
		road: 2805.37388888889,
		acceleration: -0.534351851851852
	},
	{
		id: 435,
		time: 434,
		velocity: 22.265,
		power: -1474.95194325097,
		road: 2828.0149537037,
		acceleration: -0.483796296296301
	},
	{
		id: 436,
		time: 435,
		velocity: 22.1202777777778,
		power: -8715.92686016427,
		road: 2850.00768518518,
		acceleration: -0.812870370370366
	},
	{
		id: 437,
		time: 436,
		velocity: 20.3736111111111,
		power: -560.744340442367,
		road: 2871.38791666667,
		acceleration: -0.412129629629632
	},
	{
		id: 438,
		time: 437,
		velocity: 21.0286111111111,
		power: 3559.39885785047,
		road: 2892.46143518518,
		acceleration: -0.201296296296295
	},
	{
		id: 439,
		time: 438,
		velocity: 21.5163888888889,
		power: 14176.7008064401,
		road: 2913.59467592592,
		acceleration: 0.320740740740742
	},
	{
		id: 440,
		time: 439,
		velocity: 21.3358333333333,
		power: 7308.53105749791,
		road: 2934.87615740741,
		acceleration: -0.0242592592592565
	},
	{
		id: 441,
		time: 440,
		velocity: 20.9558333333333,
		power: 698.270077523069,
		road: 2955.97375,
		acceleration: -0.343518518518522
	},
	{
		id: 442,
		time: 441,
		velocity: 20.4858333333333,
		power: -1834.99643403747,
		road: 2976.66902777778,
		acceleration: -0.461111111111112
	},
	{
		id: 443,
		time: 442,
		velocity: 19.9525,
		power: 601.217171612541,
		road: 2996.96944444444,
		acceleration: -0.328611111111112
	},
	{
		id: 444,
		time: 443,
		velocity: 19.97,
		power: -319.124401964904,
		road: 3016.92138888889,
		acceleration: -0.368333333333332
	},
	{
		id: 445,
		time: 444,
		velocity: 19.3808333333333,
		power: -871.774634689155,
		road: 3036.49430555555,
		acceleration: -0.389722222222222
	},
	{
		id: 446,
		time: 445,
		velocity: 18.7833333333333,
		power: -3352.64591513811,
		road: 3055.61412037037,
		acceleration: -0.516481481481485
	},
	{
		id: 447,
		time: 446,
		velocity: 18.4205555555556,
		power: -1787.67583266783,
		road: 3074.26393518518,
		acceleration: -0.423518518518517
	},
	{
		id: 448,
		time: 447,
		velocity: 18.1102777777778,
		power: 745.334744773818,
		road: 3092.56518518518,
		acceleration: -0.273611111111109
	},
	{
		id: 449,
		time: 448,
		velocity: 17.9625,
		power: 760.442318723494,
		road: 3110.59634259259,
		acceleration: -0.266574074074075
	},
	{
		id: 450,
		time: 449,
		velocity: 17.6208333333333,
		power: 1100.62462020198,
		road: 3128.37379629629,
		acceleration: -0.240833333333331
	},
	{
		id: 451,
		time: 450,
		velocity: 17.3877777777778,
		power: 79.4915053060021,
		road: 3145.88310185185,
		acceleration: -0.295462962962965
	},
	{
		id: 452,
		time: 451,
		velocity: 17.0761111111111,
		power: -685.779727441596,
		road: 3163.07680555555,
		acceleration: -0.335740740740743
	},
	{
		id: 453,
		time: 452,
		velocity: 16.6136111111111,
		power: -1579.91086083741,
		road: 3179.91,
		acceleration: -0.385277777777777
	},
	{
		id: 454,
		time: 453,
		velocity: 16.2319444444444,
		power: -1850.2259901689,
		road: 3196.35180555555,
		acceleration: -0.397500000000001
	},
	{
		id: 455,
		time: 454,
		velocity: 15.8836111111111,
		power: -1280.5554672046,
		road: 3212.41671296296,
		acceleration: -0.356296296296295
	},
	{
		id: 456,
		time: 455,
		velocity: 15.5447222222222,
		power: -136.166325610978,
		road: 3228.16532407407,
		acceleration: -0.276296296296296
	},
	{
		id: 457,
		time: 456,
		velocity: 15.4030555555556,
		power: 1924.71051243896,
		road: 3243.70875,
		acceleration: -0.134074074074077
	},
	{
		id: 458,
		time: 457,
		velocity: 15.4813888888889,
		power: 4212.62363837287,
		road: 3259.19611111111,
		acceleration: 0.021944444444447
	},
	{
		id: 459,
		time: 458,
		velocity: 15.6105555555556,
		power: 4779.70434248311,
		road: 3274.72379629629,
		acceleration: 0.0587037037037028
	},
	{
		id: 460,
		time: 459,
		velocity: 15.5791666666667,
		power: 3610.01312608016,
		road: 3290.27046296296,
		acceleration: -0.0207407407407416
	},
	{
		id: 461,
		time: 460,
		velocity: 15.4191666666667,
		power: 223.311220051527,
		road: 3305.68365740741,
		acceleration: -0.246203703703703
	},
	{
		id: 462,
		time: 461,
		velocity: 14.8719444444444,
		power: -1903.31759756215,
		road: 3320.77981481481,
		acceleration: -0.38787037037037
	},
	{
		id: 463,
		time: 462,
		velocity: 14.4155555555556,
		power: -4276.72907260817,
		road: 3335.4049537037,
		acceleration: -0.554166666666665
	},
	{
		id: 464,
		time: 463,
		velocity: 13.7566666666667,
		power: -2736.16735020995,
		road: 3349.53162037037,
		acceleration: -0.442777777777779
	},
	{
		id: 465,
		time: 464,
		velocity: 13.5436111111111,
		power: -1763.99180091022,
		road: 3363.25277777778,
		acceleration: -0.36824074074074
	},
	{
		id: 466,
		time: 465,
		velocity: 13.3108333333333,
		power: -3847.33759166917,
		road: 3376.52458333333,
		acceleration: -0.530462962962963
	},
	{
		id: 467,
		time: 466,
		velocity: 12.1652777777778,
		power: -4523.25035866427,
		road: 3389.23541666666,
		acceleration: -0.591481481481482
	},
	{
		id: 468,
		time: 467,
		velocity: 11.7691666666667,
		power: -3397.82531118285,
		road: 3401.39865740741,
		acceleration: -0.503703703703703
	},
	{
		id: 469,
		time: 468,
		velocity: 11.7997222222222,
		power: 1623.55820407254,
		road: 3413.27819444444,
		acceleration: -0.0637037037037036
	},
	{
		id: 470,
		time: 469,
		velocity: 11.9741666666667,
		power: 5749.72799780764,
		road: 3425.27287037037,
		acceleration: 0.293981481481481
	},
	{
		id: 471,
		time: 470,
		velocity: 12.6511111111111,
		power: 6986.79505493817,
		road: 3437.60486111111,
		acceleration: 0.380648148148149
	},
	{
		id: 472,
		time: 471,
		velocity: 12.9416666666667,
		power: 7950.49521199342,
		road: 3450.34467592592,
		acceleration: 0.434999999999999
	},
	{
		id: 473,
		time: 472,
		velocity: 13.2791666666667,
		power: 6315.58619539961,
		road: 3463.4425,
		acceleration: 0.281018518518522
	},
	{
		id: 474,
		time: 473,
		velocity: 13.4941666666667,
		power: 5754.45908788224,
		road: 3476.79263888889,
		acceleration: 0.223611111111111
	},
	{
		id: 475,
		time: 474,
		velocity: 13.6125,
		power: 5453.51278376339,
		road: 3490.34972222222,
		acceleration: 0.190277777777775
	},
	{
		id: 476,
		time: 475,
		velocity: 13.85,
		power: 5565.36702799396,
		road: 3504.09694444444,
		acceleration: 0.190000000000003
	},
	{
		id: 477,
		time: 476,
		velocity: 14.0641666666667,
		power: 5548.7275269963,
		road: 3518.02925925926,
		acceleration: 0.180185185185183
	},
	{
		id: 478,
		time: 477,
		velocity: 14.1530555555556,
		power: 3197.26033747674,
		road: 3532.05189814815,
		acceleration: 0.000462962962963331
	},
	{
		id: 479,
		time: 478,
		velocity: 13.8513888888889,
		power: 789.171109101141,
		road: 3545.98597222222,
		acceleration: -0.177592592592591
	},
	{
		id: 480,
		time: 479,
		velocity: 13.5313888888889,
		power: -74.662290728346,
		road: 3559.71157407407,
		acceleration: -0.239351851851854
	},
	{
		id: 481,
		time: 480,
		velocity: 13.435,
		power: 1583.1268137712,
		road: 3573.26319444444,
		acceleration: -0.108611111111111
	},
	{
		id: 482,
		time: 481,
		velocity: 13.5255555555556,
		power: 5568.35580086334,
		road: 3586.85916666667,
		acceleration: 0.197314814814817
	},
	{
		id: 483,
		time: 482,
		velocity: 14.1233333333333,
		power: 7321.05575221556,
		road: 3600.71268518518,
		acceleration: 0.317777777777778
	},
	{
		id: 484,
		time: 483,
		velocity: 14.3883333333333,
		power: 8962.21484921536,
		road: 3614.93425925926,
		acceleration: 0.418333333333331
	},
	{
		id: 485,
		time: 484,
		velocity: 14.7805555555556,
		power: 8048.92278213349,
		road: 3629.52986111111,
		acceleration: 0.329722222222223
	},
	{
		id: 486,
		time: 485,
		velocity: 15.1125,
		power: 8477.55618026477,
		road: 3644.46087962963,
		acceleration: 0.341111111111111
	},
	{
		id: 487,
		time: 486,
		velocity: 15.4116666666667,
		power: 7355.32249901646,
		road: 3659.68615740741,
		acceleration: 0.247407407407406
	},
	{
		id: 488,
		time: 487,
		velocity: 15.5227777777778,
		power: 6119.74939924724,
		road: 3675.11189814815,
		acceleration: 0.153518518518517
	},
	{
		id: 489,
		time: 488,
		velocity: 15.5730555555556,
		power: 5870.37762991603,
		road: 3690.67962962963,
		acceleration: 0.130462962962964
	},
	{
		id: 490,
		time: 489,
		velocity: 15.8030555555556,
		power: 5502.09687071414,
		road: 3706.36305555555,
		acceleration: 0.100925925925928
	},
	{
		id: 491,
		time: 490,
		velocity: 15.8255555555556,
		power: 6147.33054369204,
		road: 3722.1662962963,
		acceleration: 0.138703703703701
	},
	{
		id: 492,
		time: 491,
		velocity: 15.9891666666667,
		power: 6438.66188926106,
		road: 3738.11462962963,
		acceleration: 0.151481481481484
	},
	{
		id: 493,
		time: 492,
		velocity: 16.2575,
		power: 5714.63248799949,
		road: 3754.18810185185,
		acceleration: 0.0987962962962925
	},
	{
		id: 494,
		time: 493,
		velocity: 16.1219444444444,
		power: 7278.83985365699,
		road: 3770.40777777778,
		acceleration: 0.193611111111114
	},
	{
		id: 495,
		time: 494,
		velocity: 16.57,
		power: 6519.24609775427,
		road: 3786.79291666667,
		acceleration: 0.137314814814815
	},
	{
		id: 496,
		time: 495,
		velocity: 16.6694444444444,
		power: 10124.850124609,
		road: 3803.42333333333,
		acceleration: 0.353240740740741
	},
	{
		id: 497,
		time: 496,
		velocity: 17.1816666666667,
		power: 4457.93659624274,
		road: 3820.22578703704,
		acceleration: -0.00916666666666899
	},
	{
		id: 498,
		time: 497,
		velocity: 16.5425,
		power: 9084.13465489672,
		road: 3837.15953703704,
		acceleration: 0.271759259259259
	},
	{
		id: 499,
		time: 498,
		velocity: 17.4847222222222,
		power: 4577.66194473208,
		road: 3854.22361111111,
		acceleration: -0.0111111111111057
	},
	{
		id: 500,
		time: 499,
		velocity: 17.1483333333333,
		power: 7291.814403277,
		road: 3871.35810185185,
		acceleration: 0.151944444444442
	},
	{
		id: 501,
		time: 500,
		velocity: 16.9983333333333,
		power: 1509.95792305909,
		road: 3888.46847222222,
		acceleration: -0.200185185185187
	},
	{
		id: 502,
		time: 501,
		velocity: 16.8841666666667,
		power: 1918.67854287023,
		road: 3905.39356481481,
		acceleration: -0.170370370370371
	},
	{
		id: 503,
		time: 502,
		velocity: 16.6372222222222,
		power: 2795.9352134522,
		road: 3922.1774537037,
		acceleration: -0.112037037037034
	},
	{
		id: 504,
		time: 503,
		velocity: 16.6622222222222,
		power: 2980.23587109136,
		road: 3938.85662037037,
		acceleration: -0.0974074074074096
	},
	{
		id: 505,
		time: 504,
		velocity: 16.5919444444444,
		power: 4881.25809365671,
		road: 3955.49865740741,
		acceleration: 0.0231481481481524
	},
	{
		id: 506,
		time: 505,
		velocity: 16.7066666666667,
		power: 4950.65922163858,
		road: 3972.16555555555,
		acceleration: 0.0265740740740696
	},
	{
		id: 507,
		time: 506,
		velocity: 16.7419444444444,
		power: 4201.05447673618,
		road: 3988.83546296296,
		acceleration: -0.0205555555555534
	},
	{
		id: 508,
		time: 507,
		velocity: 16.5302777777778,
		power: 3973.77937782196,
		road: 4005.47814814815,
		acceleration: -0.0338888888888924
	},
	{
		id: 509,
		time: 508,
		velocity: 16.605,
		power: 5408.77286065718,
		road: 4022.13180555555,
		acceleration: 0.0558333333333358
	},
	{
		id: 510,
		time: 509,
		velocity: 16.9094444444444,
		power: 6694.61271169726,
		road: 4038.87958333333,
		acceleration: 0.132407407407406
	},
	{
		id: 511,
		time: 510,
		velocity: 16.9275,
		power: 8039.77220517591,
		road: 4055.79759259259,
		acceleration: 0.208055555555557
	},
	{
		id: 512,
		time: 511,
		velocity: 17.2291666666667,
		power: 8012.2522116884,
		road: 4072.91791666667,
		acceleration: 0.196574074074075
	},
	{
		id: 513,
		time: 512,
		velocity: 17.4991666666667,
		power: 10802.3161320411,
		road: 4090.31208333333,
		acceleration: 0.351111111111109
	},
	{
		id: 514,
		time: 513,
		velocity: 17.9808333333333,
		power: 11240.7196976288,
		road: 4108.06023148148,
		acceleration: 0.35685185185185
	},
	{
		id: 515,
		time: 514,
		velocity: 18.2997222222222,
		power: 8897.33482829148,
		road: 4126.08930555555,
		acceleration: 0.205000000000005
	},
	{
		id: 516,
		time: 515,
		velocity: 18.1141666666667,
		power: 3321.37475900288,
		road: 4144.16115740741,
		acceleration: -0.119444444444447
	},
	{
		id: 517,
		time: 516,
		velocity: 17.6225,
		power: -165.734150664735,
		road: 4162.01490740741,
		acceleration: -0.316759259259261
	},
	{
		id: 518,
		time: 517,
		velocity: 17.3494444444444,
		power: -1759.28284879712,
		road: 4179.50777777778,
		acceleration: -0.405000000000001
	},
	{
		id: 519,
		time: 518,
		velocity: 16.8991666666667,
		power: 6036.07338305482,
		road: 4196.83194444444,
		acceleration: 0.0675925925925931
	},
	{
		id: 520,
		time: 519,
		velocity: 17.8252777777778,
		power: 7987.26258866513,
		road: 4214.27962962963,
		acceleration: 0.179444444444446
	},
	{
		id: 521,
		time: 520,
		velocity: 17.8877777777778,
		power: 10805.5891854055,
		road: 4231.98388888889,
		acceleration: 0.333703703703705
	},
	{
		id: 522,
		time: 521,
		velocity: 17.9002777777778,
		power: 6225.57084788634,
		road: 4249.88273148148,
		acceleration: 0.0554629629629595
	},
	{
		id: 523,
		time: 522,
		velocity: 17.9916666666667,
		power: 5343.46207069419,
		road: 4267.81074074074,
		acceleration: 0.00287037037037052
	},
	{
		id: 524,
		time: 523,
		velocity: 17.8963888888889,
		power: 3217.19985321651,
		road: 4285.68055555555,
		acceleration: -0.119259259259255
	},
	{
		id: 525,
		time: 524,
		velocity: 17.5425,
		power: 4290.83460598189,
		road: 4303.46398148148,
		acceleration: -0.0535185185185227
	},
	{
		id: 526,
		time: 525,
		velocity: 17.8311111111111,
		power: 5640.21331423043,
		road: 4321.23379629629,
		acceleration: 0.0262962962963016
	},
	{
		id: 527,
		time: 526,
		velocity: 17.9752777777778,
		power: 9341.7483650732,
		road: 4339.13537037037,
		acceleration: 0.237222222222218
	},
	{
		id: 528,
		time: 527,
		velocity: 18.2541666666667,
		power: 7602.40535537583,
		road: 4357.21930555555,
		acceleration: 0.127500000000001
	},
	{
		id: 529,
		time: 528,
		velocity: 18.2136111111111,
		power: 7416.50214734231,
		road: 4375.42273148148,
		acceleration: 0.111481481481484
	},
	{
		id: 530,
		time: 529,
		velocity: 18.3097222222222,
		power: 5730.20253330339,
		road: 4393.68805555555,
		acceleration: 0.0123148148148147
	},
	{
		id: 531,
		time: 530,
		velocity: 18.2911111111111,
		power: 4943.52492745092,
		road: 4411.94337962963,
		acceleration: -0.0323148148148178
	},
	{
		id: 532,
		time: 531,
		velocity: 18.1166666666667,
		power: 4195.49654420249,
		road: 4430.14587962963,
		acceleration: -0.0733333333333306
	},
	{
		id: 533,
		time: 532,
		velocity: 18.0897222222222,
		power: 4332.20053837244,
		road: 4448.28013888889,
		acceleration: -0.063148148148148
	},
	{
		id: 534,
		time: 533,
		velocity: 18.1016666666667,
		power: 4335.45123194202,
		road: 4466.3524074074,
		acceleration: -0.0608333333333384
	},
	{
		id: 535,
		time: 534,
		velocity: 17.9341666666667,
		power: 4643.81526291657,
		road: 4484.3736574074,
		acceleration: -0.041203703703701
	},
	{
		id: 536,
		time: 535,
		velocity: 17.9661111111111,
		power: 4113.22235550724,
		road: 4502.33925925926,
		acceleration: -0.0700925925925908
	},
	{
		id: 537,
		time: 536,
		velocity: 17.8913888888889,
		power: 2610.57871651124,
		road: 4520.19268518518,
		acceleration: -0.154259259259259
	},
	{
		id: 538,
		time: 537,
		velocity: 17.4713888888889,
		power: 1308.5884304513,
		road: 4537.85606481481,
		acceleration: -0.225833333333334
	},
	{
		id: 539,
		time: 538,
		velocity: 17.2886111111111,
		power: 385.308975322987,
		road: 4555.26893518518,
		acceleration: -0.275185185185187
	},
	{
		id: 540,
		time: 539,
		velocity: 17.0658333333333,
		power: 1916.96799090194,
		road: 4572.45555555555,
		acceleration: -0.177314814814814
	},
	{
		id: 541,
		time: 540,
		velocity: 16.9394444444444,
		power: 2700.43114795777,
		road: 4589.49087962963,
		acceleration: -0.125277777777779
	},
	{
		id: 542,
		time: 541,
		velocity: 16.9127777777778,
		power: 5113.86929026652,
		road: 4606.47592592592,
		acceleration: 0.0247222222222234
	},
	{
		id: 543,
		time: 542,
		velocity: 17.14,
		power: 7138.09398917309,
		road: 4623.54606481481,
		acceleration: 0.145462962962963
	},
	{
		id: 544,
		time: 543,
		velocity: 17.3758333333333,
		power: 7979.64911620211,
		road: 4640.78342592592,
		acceleration: 0.188981481481481
	},
	{
		id: 545,
		time: 544,
		velocity: 17.4797222222222,
		power: 8653.19071136593,
		road: 4658.22509259259,
		acceleration: 0.219629629629626
	},
	{
		id: 546,
		time: 545,
		velocity: 17.7988888888889,
		power: 6228.54798368004,
		road: 4675.81078703703,
		acceleration: 0.0684259259259292
	},
	{
		id: 547,
		time: 546,
		velocity: 17.5811111111111,
		power: 1821.67814366854,
		road: 4693.33476851852,
		acceleration: -0.191851851851851
	},
	{
		id: 548,
		time: 547,
		velocity: 16.9041666666667,
		power: -198.044152234426,
		road: 4710.60902777777,
		acceleration: -0.307592592592595
	},
	{
		id: 549,
		time: 548,
		velocity: 16.8761111111111,
		power: -620.984336881681,
		road: 4727.56560185185,
		acceleration: -0.327777777777772
	},
	{
		id: 550,
		time: 549,
		velocity: 16.5977777777778,
		power: -279.582319033641,
		road: 4744.20773148148,
		acceleration: -0.301111111111112
	},
	{
		id: 551,
		time: 550,
		velocity: 16.0008333333333,
		power: -1321.47157767286,
		road: 4760.51824074074,
		acceleration: -0.362129629629631
	},
	{
		id: 552,
		time: 551,
		velocity: 15.7897222222222,
		power: -10022.0861807678,
		road: 4776.18004629629,
		acceleration: -0.935277777777779
	},
	{
		id: 553,
		time: 552,
		velocity: 13.7919444444444,
		power: -12993.0642082752,
		road: 4790.78462962963,
		acceleration: -1.17916666666667
	},
	{
		id: 554,
		time: 553,
		velocity: 12.4633333333333,
		power: -14543.5740312109,
		road: 4804.11421296296,
		acceleration: -1.37083333333334
	},
	{
		id: 555,
		time: 554,
		velocity: 11.6772222222222,
		power: -7738.69303759791,
		road: 4816.32032407407,
		acceleration: -0.87611111111111
	},
	{
		id: 556,
		time: 555,
		velocity: 11.1636111111111,
		power: -5767.05555825492,
		road: 4827.72277777778,
		acceleration: -0.731203703703706
	},
	{
		id: 557,
		time: 556,
		velocity: 10.2697222222222,
		power: -12324.9686025024,
		road: 4838.03893518518,
		acceleration: -1.44138888888889
	},
	{
		id: 558,
		time: 557,
		velocity: 7.35305555555556,
		power: -12357.2337612434,
		road: 4846.80888888889,
		acceleration: -1.65101851851852
	},
	{
		id: 559,
		time: 558,
		velocity: 6.21055555555556,
		power: -9740.85950634531,
		road: 4853.95898148148,
		acceleration: -1.5887037037037
	},
	{
		id: 560,
		time: 559,
		velocity: 5.50361111111111,
		power: -2328.68683697581,
		road: 4860.03916666666,
		acceleration: -0.551111111111112
	},
	{
		id: 561,
		time: 560,
		velocity: 5.69972222222222,
		power: -952.267675718315,
		road: 4865.68236111111,
		acceleration: -0.322870370370371
	},
	{
		id: 562,
		time: 561,
		velocity: 5.24194444444444,
		power: -538.640901404036,
		road: 4871.03944444444,
		acceleration: -0.249351851851852
	},
	{
		id: 563,
		time: 562,
		velocity: 4.75555555555556,
		power: -2729.80288464932,
		road: 4875.90625,
		acceleration: -0.731203703703703
	},
	{
		id: 564,
		time: 563,
		velocity: 3.50611111111111,
		power: -1503.99593484794,
		road: 4880.15217592592,
		acceleration: -0.510555555555556
	},
	{
		id: 565,
		time: 564,
		velocity: 3.71027777777778,
		power: -1218.48985151883,
		road: 4883.90412037037,
		acceleration: -0.477407407407408
	},
	{
		id: 566,
		time: 565,
		velocity: 3.32333333333333,
		power: 615.315557623159,
		road: 4887.44175925926,
		acceleration: 0.0487962962962967
	},
	{
		id: 567,
		time: 566,
		velocity: 3.6525,
		power: 704.752959555514,
		road: 4891.03962962963,
		acceleration: 0.0716666666666668
	},
	{
		id: 568,
		time: 567,
		velocity: 3.92527777777778,
		power: 2611.33894606019,
		road: 4894.95638888889,
		acceleration: 0.566111111111111
	},
	{
		id: 569,
		time: 568,
		velocity: 5.02166666666667,
		power: 3845.50868692853,
		road: 4899.52925925926,
		acceleration: 0.746111111111111
	},
	{
		id: 570,
		time: 569,
		velocity: 5.89083333333333,
		power: 4363.86398270519,
		road: 4904.8362037037,
		acceleration: 0.722037037037037
	},
	{
		id: 571,
		time: 570,
		velocity: 6.09138888888889,
		power: 5888.65116613754,
		road: 4910.93754629629,
		acceleration: 0.866759259259259
	},
	{
		id: 572,
		time: 571,
		velocity: 7.62194444444444,
		power: 6494.48362146788,
		road: 4917.88625,
		acceleration: 0.827962962962964
	},
	{
		id: 573,
		time: 572,
		velocity: 8.37472222222222,
		power: 7963.53976291759,
		road: 4925.70324074074,
		acceleration: 0.908611111111112
	},
	{
		id: 574,
		time: 573,
		velocity: 8.81722222222222,
		power: 6732.24253288338,
		road: 4934.3012037037,
		acceleration: 0.653333333333332
	},
	{
		id: 575,
		time: 574,
		velocity: 9.58194444444444,
		power: 6236.38780982568,
		road: 4943.49444444444,
		acceleration: 0.537222222222223
	},
	{
		id: 576,
		time: 575,
		velocity: 9.98638888888889,
		power: 6475.42750327318,
		road: 4953.21555555555,
		acceleration: 0.518518518518519
	},
	{
		id: 577,
		time: 576,
		velocity: 10.3727777777778,
		power: 6509.14994080798,
		road: 4963.43689814815,
		acceleration: 0.481944444444444
	},
	{
		id: 578,
		time: 577,
		velocity: 11.0277777777778,
		power: 4097.48338148658,
		road: 4974.00736111111,
		acceleration: 0.216296296296294
	},
	{
		id: 579,
		time: 578,
		velocity: 10.6352777777778,
		power: 5068.86331176492,
		road: 4984.83476851852,
		acceleration: 0.297592592592594
	},
	{
		id: 580,
		time: 579,
		velocity: 11.2655555555556,
		power: 4420.49767168957,
		road: 4995.92171296296,
		acceleration: 0.221481481481481
	},
	{
		id: 581,
		time: 580,
		velocity: 11.6922222222222,
		power: 10145.443644602,
		road: 5007.47828703703,
		acceleration: 0.717777777777778
	},
	{
		id: 582,
		time: 581,
		velocity: 12.7886111111111,
		power: 10198.6724197173,
		road: 5019.72421296296,
		acceleration: 0.660925925925925
	},
	{
		id: 583,
		time: 582,
		velocity: 13.2483333333333,
		power: 11114.8135154512,
		road: 5032.64064814814,
		acceleration: 0.680092592592592
	},
	{
		id: 584,
		time: 583,
		velocity: 13.7325,
		power: 10031.1524205613,
		road: 5046.17009259259,
		acceleration: 0.545925925925928
	},
	{
		id: 585,
		time: 584,
		velocity: 14.4263888888889,
		power: 7672.75834971176,
		road: 5060.14129629629,
		acceleration: 0.337592592592593
	},
	{
		id: 586,
		time: 585,
		velocity: 14.2611111111111,
		power: 6049.47143284845,
		road: 5074.38277777777,
		acceleration: 0.202962962962962
	},
	{
		id: 587,
		time: 586,
		velocity: 14.3413888888889,
		power: 2768.09170046627,
		road: 5088.70537037037,
		acceleration: -0.0407407407407412
	},
	{
		id: 588,
		time: 587,
		velocity: 14.3041666666667,
		power: 3828.61477215236,
		road: 5103.02601851851,
		acceleration: 0.0368518518518499
	},
	{
		id: 589,
		time: 588,
		velocity: 14.3716666666667,
		power: 4214.1470844996,
		road: 5117.39666666666,
		acceleration: 0.0631481481481497
	},
	{
		id: 590,
		time: 589,
		velocity: 14.5308333333333,
		power: 4399.19239519113,
		road: 5131.83587962963,
		acceleration: 0.0739814814814821
	},
	{
		id: 591,
		time: 590,
		velocity: 14.5261111111111,
		power: 4387.62718176269,
		road: 5146.34726851852,
		acceleration: 0.0703703703703695
	},
	{
		id: 592,
		time: 591,
		velocity: 14.5827777777778,
		power: 4115.14298157095,
		road: 5160.91810185185,
		acceleration: 0.0485185185185202
	},
	{
		id: 593,
		time: 592,
		velocity: 14.6763888888889,
		power: 4312.70769759974,
		road: 5175.54351851852,
		acceleration: 0.0606481481481467
	},
	{
		id: 594,
		time: 593,
		velocity: 14.7080555555556,
		power: 4351.4121283288,
		road: 5190.22981481481,
		acceleration: 0.0611111111111118
	},
	{
		id: 595,
		time: 594,
		velocity: 14.7661111111111,
		power: 3180.40229072646,
		road: 5204.93518518518,
		acceleration: -0.0229629629629642
	},
	{
		id: 596,
		time: 595,
		velocity: 14.6075,
		power: 438.282986026749,
		road: 5219.52111111111,
		acceleration: -0.215925925925925
	},
	{
		id: 597,
		time: 596,
		velocity: 14.0602777777778,
		power: -894.787134746026,
		road: 5233.84481481481,
		acceleration: -0.308518518518518
	},
	{
		id: 598,
		time: 597,
		velocity: 13.8405555555556,
		power: -2262.12203186067,
		road: 5247.81074074074,
		acceleration: -0.407037037037036
	},
	{
		id: 599,
		time: 598,
		velocity: 13.3863888888889,
		power: -311.84790489366,
		road: 5261.44504629629,
		acceleration: -0.256203703703704
	},
	{
		id: 600,
		time: 599,
		velocity: 13.2916666666667,
		power: -345.061883955157,
		road: 5274.82356481481,
		acceleration: -0.255370370370372
	},
	{
		id: 601,
		time: 600,
		velocity: 13.0744444444444,
		power: 1365.33363146905,
		road: 5288.01583333333,
		acceleration: -0.11712962962963
	},
	{
		id: 602,
		time: 601,
		velocity: 13.035,
		power: 1903.99686032253,
		road: 5301.11361111111,
		acceleration: -0.0718518518518518
	},
	{
		id: 603,
		time: 602,
		velocity: 13.0761111111111,
		power: 2374.4162716728,
		road: 5314.15912037037,
		acceleration: -0.0326851851851817
	},
	{
		id: 604,
		time: 603,
		velocity: 12.9763888888889,
		power: 2868.50236071605,
		road: 5327.19199074074,
		acceleration: 0.0074074074074062
	},
	{
		id: 605,
		time: 604,
		velocity: 13.0572222222222,
		power: 2982.80005901918,
		road: 5340.23666666666,
		acceleration: 0.0162037037037042
	},
	{
		id: 606,
		time: 605,
		velocity: 13.1247222222222,
		power: 4339.77119497474,
		road: 5353.35060185185,
		acceleration: 0.122314814814814
	},
	{
		id: 607,
		time: 606,
		velocity: 13.3433333333333,
		power: 4501.46550497156,
		road: 5366.59064814814,
		acceleration: 0.129907407407407
	},
	{
		id: 608,
		time: 607,
		velocity: 13.4469444444444,
		power: 5255.17189936003,
		road: 5379.98680555555,
		acceleration: 0.182314814814815
	},
	{
		id: 609,
		time: 608,
		velocity: 13.6716666666667,
		power: 4824.88601510294,
		road: 5393.5449537037,
		acceleration: 0.141666666666667
	},
	{
		id: 610,
		time: 609,
		velocity: 13.7683333333333,
		power: 4282.87840691957,
		road: 5407.22148148148,
		acceleration: 0.095092592592593
	},
	{
		id: 611,
		time: 610,
		velocity: 13.7322222222222,
		power: 4728.77770493408,
		road: 5421.00787037037,
		acceleration: 0.124629629629631
	},
	{
		id: 612,
		time: 611,
		velocity: 14.0455555555556,
		power: 3009.28140840647,
		road: 5434.85263888888,
		acceleration: -0.0078703703703713
	},
	{
		id: 613,
		time: 612,
		velocity: 13.7447222222222,
		power: 2328.47144946284,
		road: 5448.66425925926,
		acceleration: -0.0584259259259277
	},
	{
		id: 614,
		time: 613,
		velocity: 13.5569444444444,
		power: 240.639886172731,
		road: 5462.33944444444,
		acceleration: -0.214444444444446
	},
	{
		id: 615,
		time: 614,
		velocity: 13.4022222222222,
		power: 1155.7575269215,
		road: 5475.83717592592,
		acceleration: -0.14046296296296
	},
	{
		id: 616,
		time: 615,
		velocity: 13.3233333333333,
		power: 2430.71951447541,
		road: 5489.24523148148,
		acceleration: -0.0388888888888896
	},
	{
		id: 617,
		time: 616,
		velocity: 13.4402777777778,
		power: 5070.62490729169,
		road: 5502.71611111111,
		acceleration: 0.164537037037036
	},
	{
		id: 618,
		time: 617,
		velocity: 13.8958333333333,
		power: 5029.02493215809,
		road: 5516.34638888888,
		acceleration: 0.154259259259261
	},
	{
		id: 619,
		time: 618,
		velocity: 13.7861111111111,
		power: 6525.58633269968,
		road: 5530.18305555555,
		acceleration: 0.258518518518517
	},
	{
		id: 620,
		time: 619,
		velocity: 14.2158333333333,
		power: 5432.68255444968,
		road: 5544.23208333333,
		acceleration: 0.166203703703705
	},
	{
		id: 621,
		time: 620,
		velocity: 14.3944444444444,
		power: 5103.27751267941,
		road: 5558.43180555555,
		acceleration: 0.135185185185186
	},
	{
		id: 622,
		time: 621,
		velocity: 14.1916666666667,
		power: 5301.19299633608,
		road: 5572.77097222222,
		acceleration: 0.143703703703702
	},
	{
		id: 623,
		time: 622,
		velocity: 14.6469444444444,
		power: 4695.84546506346,
		road: 5587.22935185185,
		acceleration: 0.0947222222222219
	},
	{
		id: 624,
		time: 623,
		velocity: 14.6786111111111,
		power: 5743.45924912672,
		road: 5601.8174537037,
		acceleration: 0.164722222222224
	},
	{
		id: 625,
		time: 624,
		velocity: 14.6858333333333,
		power: 3848.38532686116,
		road: 5616.50060185185,
		acceleration: 0.0253703703703696
	},
	{
		id: 626,
		time: 625,
		velocity: 14.7230555555556,
		power: 2544.99760032219,
		road: 5631.16296296296,
		acceleration: -0.0669444444444451
	},
	{
		id: 627,
		time: 626,
		velocity: 14.4777777777778,
		power: 2452.42198596106,
		road: 5645.75606481481,
		acceleration: -0.0715740740740731
	},
	{
		id: 628,
		time: 627,
		velocity: 14.4711111111111,
		power: -87.4282623940657,
		road: 5660.18777777777,
		acceleration: -0.251203703703705
	},
	{
		id: 629,
		time: 628,
		velocity: 13.9694444444444,
		power: 1035.37002815746,
		road: 5674.41125,
		acceleration: -0.165277777777776
	},
	{
		id: 630,
		time: 629,
		velocity: 13.9819444444444,
		power: 1165.07864386286,
		road: 5688.47597222222,
		acceleration: -0.152222222222223
	},
	{
		id: 631,
		time: 630,
		velocity: 14.0144444444444,
		power: 4318.59213028896,
		road: 5702.50652777777,
		acceleration: 0.0838888888888896
	},
	{
		id: 632,
		time: 631,
		velocity: 14.2211111111111,
		power: 5218.29727616891,
		road: 5716.65203703703,
		acceleration: 0.146018518518517
	},
	{
		id: 633,
		time: 632,
		velocity: 14.42,
		power: 5456.99603446377,
		road: 5730.94902777777,
		acceleration: 0.156944444444445
	},
	{
		id: 634,
		time: 633,
		velocity: 14.4852777777778,
		power: 4672.32756945717,
		road: 5745.37171296296,
		acceleration: 0.094444444444445
	},
	{
		id: 635,
		time: 634,
		velocity: 14.5044444444444,
		power: 2949.73000815369,
		road: 5759.82583333333,
		acceleration: -0.031574074074074
	},
	{
		id: 636,
		time: 635,
		velocity: 14.3252777777778,
		power: 2876.57388294601,
		road: 5774.24625,
		acceleration: -0.0358333333333345
	},
	{
		id: 637,
		time: 636,
		velocity: 14.3777777777778,
		power: 1637.96037993164,
		road: 5788.58685185185,
		acceleration: -0.123796296296295
	},
	{
		id: 638,
		time: 637,
		velocity: 14.1330555555556,
		power: 3736.02774133128,
		road: 5802.88106481481,
		acceleration: 0.0310185185185166
	},
	{
		id: 639,
		time: 638,
		velocity: 14.4183333333333,
		power: 2483.80047147965,
		road: 5817.16064814814,
		acceleration: -0.0602777777777757
	},
	{
		id: 640,
		time: 639,
		velocity: 14.1969444444444,
		power: 3972.41644879844,
		road: 5831.43462962963,
		acceleration: 0.049074074074074
	},
	{
		id: 641,
		time: 640,
		velocity: 14.2802777777778,
		power: 5174.36643071912,
		road: 5845.79981481481,
		acceleration: 0.133333333333333
	},
	{
		id: 642,
		time: 641,
		velocity: 14.8183333333333,
		power: 6854.38949015615,
		road: 5860.35472222222,
		acceleration: 0.246111111111109
	},
	{
		id: 643,
		time: 642,
		velocity: 14.9352777777778,
		power: 6752.44739068569,
		road: 5875.1462037037,
		acceleration: 0.227037037037039
	},
	{
		id: 644,
		time: 643,
		velocity: 14.9613888888889,
		power: 1576.460875375,
		road: 5889.98106481481,
		acceleration: -0.140277777777776
	},
	{
		id: 645,
		time: 644,
		velocity: 14.3975,
		power: 1154.11305886026,
		road: 5904.6624537037,
		acceleration: -0.166666666666668
	},
	{
		id: 646,
		time: 645,
		velocity: 14.4352777777778,
		power: 2436.2319317073,
		road: 5919.22458333333,
		acceleration: -0.0718518518518501
	},
	{
		id: 647,
		time: 646,
		velocity: 14.7458333333333,
		power: 6077.59720407729,
		road: 5933.84439814814,
		acceleration: 0.18722222222222
	},
	{
		id: 648,
		time: 647,
		velocity: 14.9591666666667,
		power: 7058.24368550241,
		road: 5948.68101851851,
		acceleration: 0.246388888888889
	},
	{
		id: 649,
		time: 648,
		velocity: 15.1744444444444,
		power: 6296.88851270407,
		road: 5963.73217592592,
		acceleration: 0.182685185185186
	},
	{
		id: 650,
		time: 649,
		velocity: 15.2938888888889,
		power: 6145.03808709218,
		road: 5978.95680555555,
		acceleration: 0.164259259259259
	},
	{
		id: 651,
		time: 650,
		velocity: 15.4519444444444,
		power: 6595.02382147903,
		road: 5994.35703703703,
		acceleration: 0.186944444444444
	},
	{
		id: 652,
		time: 651,
		velocity: 15.7352777777778,
		power: 6600.1862863575,
		road: 6009.94013888888,
		acceleration: 0.178796296296296
	},
	{
		id: 653,
		time: 652,
		velocity: 15.8302777777778,
		power: 8145.79106082875,
		road: 6025.74800925926,
		acceleration: 0.270740740740742
	},
	{
		id: 654,
		time: 653,
		velocity: 16.2641666666667,
		power: 8181.69633969527,
		road: 6041.82092592592,
		acceleration: 0.259351851851854
	},
	{
		id: 655,
		time: 654,
		velocity: 16.5133333333333,
		power: 9561.9036704549,
		road: 6058.18972222222,
		acceleration: 0.332407407407405
	},
	{
		id: 656,
		time: 655,
		velocity: 16.8275,
		power: 8098.45634310812,
		road: 6074.83722222222,
		acceleration: 0.225000000000001
	},
	{
		id: 657,
		time: 656,
		velocity: 16.9391666666667,
		power: 7285.24679178024,
		road: 6091.67967592592,
		acceleration: 0.164907407407405
	},
	{
		id: 658,
		time: 657,
		velocity: 17.0080555555556,
		power: 7411.44978289805,
		road: 6108.68712962962,
		acceleration: 0.165092592592593
	},
	{
		id: 659,
		time: 658,
		velocity: 17.3227777777778,
		power: 7380.64800588661,
		road: 6125.85504629629,
		acceleration: 0.155833333333334
	},
	{
		id: 660,
		time: 659,
		velocity: 17.4066666666667,
		power: 7226.94073728618,
		road: 6143.17078703703,
		acceleration: 0.139814814814812
	},
	{
		id: 661,
		time: 660,
		velocity: 17.4275,
		power: 5524.82381789261,
		road: 6160.57328703703,
		acceleration: 0.0337037037037078
	},
	{
		id: 662,
		time: 661,
		velocity: 17.4238888888889,
		power: 4482.43684937342,
		road: 6177.97814814814,
		acceleration: -0.0289814814814875
	},
	{
		id: 663,
		time: 662,
		velocity: 17.3197222222222,
		power: 3961.52155957522,
		road: 6195.33912037037,
		acceleration: -0.0587962962962933
	},
	{
		id: 664,
		time: 663,
		velocity: 17.2511111111111,
		power: 3388.77529277554,
		road: 6212.62523148148,
		acceleration: -0.090925925925923
	},
	{
		id: 665,
		time: 664,
		velocity: 17.1511111111111,
		power: 1181.17253014318,
		road: 6229.75550925925,
		acceleration: -0.220740740740744
	},
	{
		id: 666,
		time: 665,
		velocity: 16.6575,
		power: 1320.19811504885,
		road: 6246.67185185185,
		acceleration: -0.207129629629627
	},
	{
		id: 667,
		time: 666,
		velocity: 16.6297222222222,
		power: 171.515430746898,
		road: 6263.34791666666,
		acceleration: -0.273425925925928
	},
	{
		id: 668,
		time: 667,
		velocity: 16.3308333333333,
		power: 1411.55505649218,
		road: 6279.79222222222,
		acceleration: -0.190092592592592
	},
	{
		id: 669,
		time: 668,
		velocity: 16.0872222222222,
		power: 4115.01645214662,
		road: 6296.13416666666,
		acceleration: -0.0146296296296313
	},
	{
		id: 670,
		time: 669,
		velocity: 16.5858333333333,
		power: 5894.23928302626,
		road: 6312.51754629629,
		acceleration: 0.0975000000000037
	},
	{
		id: 671,
		time: 670,
		velocity: 16.6233333333333,
		power: 7586.97656236985,
		road: 6329.04888888888,
		acceleration: 0.198425925925925
	},
	{
		id: 672,
		time: 671,
		velocity: 16.6825,
		power: 4905.33463800754,
		road: 6345.69175925925,
		acceleration: 0.0246296296296293
	},
	{
		id: 673,
		time: 672,
		velocity: 16.6597222222222,
		power: 5401.63108272189,
		road: 6362.37407407407,
		acceleration: 0.0542592592592577
	},
	{
		id: 674,
		time: 673,
		velocity: 16.7861111111111,
		power: 3638.64780706906,
		road: 6379.05541666666,
		acceleration: -0.0562037037037051
	},
	{
		id: 675,
		time: 674,
		velocity: 16.5138888888889,
		power: 3255.17433800206,
		road: 6395.66958333333,
		acceleration: -0.078148148148145
	},
	{
		id: 676,
		time: 675,
		velocity: 16.4252777777778,
		power: 3734.63380713061,
		road: 6412.22171296296,
		acceleration: -0.0459259259259284
	},
	{
		id: 677,
		time: 676,
		velocity: 16.6483333333333,
		power: 5385.90682602458,
		road: 6428.7799537037,
		acceleration: 0.0581481481481489
	},
	{
		id: 678,
		time: 677,
		velocity: 16.6883333333333,
		power: 6363.21183763055,
		road: 6445.42532407407,
		acceleration: 0.11611111111111
	},
	{
		id: 679,
		time: 678,
		velocity: 16.7736111111111,
		power: 5762.98415191919,
		road: 6462.16601851851,
		acceleration: 0.0745370370370395
	},
	{
		id: 680,
		time: 679,
		velocity: 16.8719444444444,
		power: 6275.36876505914,
		road: 6478.99537037037,
		acceleration: 0.102777777777774
	},
	{
		id: 681,
		time: 680,
		velocity: 16.9966666666667,
		power: 4039.64926060487,
		road: 6495.85749999999,
		acceleration: -0.0372222222222192
	},
	{
		id: 682,
		time: 681,
		velocity: 16.6619444444444,
		power: 3436.50501887732,
		road: 6512.66458333333,
		acceleration: -0.0728703703703673
	},
	{
		id: 683,
		time: 682,
		velocity: 16.6533333333333,
		power: 1805.46052408485,
		road: 6529.34962962962,
		acceleration: -0.171203703703707
	},
	{
		id: 684,
		time: 683,
		velocity: 16.4830555555556,
		power: 3775.72997833109,
		road: 6545.92699074074,
		acceleration: -0.0441666666666656
	},
	{
		id: 685,
		time: 684,
		velocity: 16.5294444444444,
		power: 3413.46479550099,
		road: 6562.44962962962,
		acceleration: -0.06527777777778
	},
	{
		id: 686,
		time: 685,
		velocity: 16.4575,
		power: 5170.6794823629,
		road: 6578.96277777777,
		acceleration: 0.0462962962962976
	},
	{
		id: 687,
		time: 686,
		velocity: 16.6219444444444,
		power: 4436.25650866865,
		road: 6595.49856481481,
		acceleration: -0.0010185185185172
	},
	{
		id: 688,
		time: 687,
		velocity: 16.5263888888889,
		power: 3541.51965599714,
		road: 6612.00550925925,
		acceleration: -0.0566666666666684
	},
	{
		id: 689,
		time: 688,
		velocity: 16.2875,
		power: 2254.76804929861,
		road: 6628.41634259259,
		acceleration: -0.135555555555555
	},
	{
		id: 690,
		time: 689,
		velocity: 16.2152777777778,
		power: 977.733829383441,
		road: 6644.65287037037,
		acceleration: -0.213055555555556
	},
	{
		id: 691,
		time: 690,
		velocity: 15.8872222222222,
		power: 1354.71077120829,
		road: 6660.69083333333,
		acceleration: -0.184074074074074
	},
	{
		id: 692,
		time: 691,
		velocity: 15.7352777777778,
		power: 83.9161417921353,
		road: 6676.50532407407,
		acceleration: -0.26287037037037
	},
	{
		id: 693,
		time: 692,
		velocity: 15.4266666666667,
		power: 1281.74918635229,
		road: 6692.09912037037,
		acceleration: -0.178518518518517
	},
	{
		id: 694,
		time: 693,
		velocity: 15.3516666666667,
		power: 1065.17177547157,
		road: 6707.50916666666,
		acceleration: -0.188981481481482
	},
	{
		id: 695,
		time: 694,
		velocity: 15.1683333333333,
		power: 1795.58759953485,
		road: 6722.75708333333,
		acceleration: -0.135277777777778
	},
	{
		id: 696,
		time: 695,
		velocity: 15.0208333333333,
		power: 1174.1737063736,
		road: 6737.85013888888,
		acceleration: -0.174444444444443
	},
	{
		id: 697,
		time: 696,
		velocity: 14.8283333333333,
		power: 897.059700342209,
		road: 6752.76106481481,
		acceleration: -0.189814814814815
	},
	{
		id: 698,
		time: 697,
		velocity: 14.5988888888889,
		power: 428.288658508082,
		road: 6767.46763888888,
		acceleration: -0.218888888888889
	},
	{
		id: 699,
		time: 698,
		velocity: 14.3641666666667,
		power: 213.058590828056,
		road: 6781.94962962962,
		acceleration: -0.230277777777779
	},
	{
		id: 700,
		time: 699,
		velocity: 14.1375,
		power: 667.927588076484,
		road: 6796.21986111111,
		acceleration: -0.193240740740741
	},
	{
		id: 701,
		time: 700,
		velocity: 14.0191666666667,
		power: 2590.40414306501,
		road: 6810.36916666666,
		acceleration: -0.0486111111111089
	},
	{
		id: 702,
		time: 701,
		velocity: 14.2183333333333,
		power: 4200.01373125853,
		road: 6824.52921296296,
		acceleration: 0.0700925925925926
	},
	{
		id: 703,
		time: 702,
		velocity: 14.3477777777778,
		power: 6307.93821493441,
		road: 6838.83374999999,
		acceleration: 0.218888888888888
	},
	{
		id: 704,
		time: 703,
		velocity: 14.6758333333333,
		power: 5480.4857689252,
		road: 6853.32282407407,
		acceleration: 0.150185185185187
	},
	{
		id: 705,
		time: 704,
		velocity: 14.6688888888889,
		power: 6155.35751576079,
		road: 6867.9824537037,
		acceleration: 0.190925925925928
	},
	{
		id: 706,
		time: 705,
		velocity: 14.9205555555556,
		power: 4879.4100058246,
		road: 6882.78458333333,
		acceleration: 0.0940740740740722
	},
	{
		id: 707,
		time: 706,
		velocity: 14.9580555555556,
		power: 4738.23651131678,
		road: 6897.67407407407,
		acceleration: 0.0806481481481462
	},
	{
		id: 708,
		time: 707,
		velocity: 14.9108333333333,
		power: 4617.46966571609,
		road: 6912.63851851851,
		acceleration: 0.06925925925926
	},
	{
		id: 709,
		time: 708,
		velocity: 15.1283333333333,
		power: 3947.87182362557,
		road: 6927.64800925925,
		acceleration: 0.0208333333333339
	},
	{
		id: 710,
		time: 709,
		velocity: 15.0205555555556,
		power: 2671.17054975166,
		road: 6942.63416666666,
		acceleration: -0.0675000000000008
	},
	{
		id: 711,
		time: 710,
		velocity: 14.7083333333333,
		power: 1216.04369350665,
		road: 6957.50333333333,
		acceleration: -0.166481481481481
	},
	{
		id: 712,
		time: 711,
		velocity: 14.6288888888889,
		power: 2077.97328636924,
		road: 6972.23814814814,
		acceleration: -0.10222222222222
	},
	{
		id: 713,
		time: 712,
		velocity: 14.7138888888889,
		power: 3660.56683958547,
		road: 6986.92773148148,
		acceleration: 0.011759259259259
	},
	{
		id: 714,
		time: 713,
		velocity: 14.7436111111111,
		power: 4517.27168757017,
		road: 7001.65879629629,
		acceleration: 0.0712037037037021
	},
	{
		id: 715,
		time: 714,
		velocity: 14.8425,
		power: 3990.85565366857,
		road: 7016.44143518518,
		acceleration: 0.0319444444444432
	},
	{
		id: 716,
		time: 715,
		velocity: 14.8097222222222,
		power: 3935.06065859118,
		road: 7031.25351851851,
		acceleration: 0.026944444444446
	},
	{
		id: 717,
		time: 716,
		velocity: 14.8244444444444,
		power: 2831.9196331956,
		road: 7046.05374999999,
		acceleration: -0.0506481481481487
	},
	{
		id: 718,
		time: 717,
		velocity: 14.6905555555556,
		power: 1885.18829264021,
		road: 7060.77092592592,
		acceleration: -0.115462962962962
	},
	{
		id: 719,
		time: 718,
		velocity: 14.4633333333333,
		power: 784.302186895817,
		road: 7075.33504629629,
		acceleration: -0.190648148148149
	},
	{
		id: 720,
		time: 719,
		velocity: 14.2525,
		power: 196.648307982094,
		road: 7089.68921296296,
		acceleration: -0.229259259259258
	},
	{
		id: 721,
		time: 720,
		velocity: 14.0027777777778,
		power: -1062.55996293288,
		road: 7103.76962962963,
		acceleration: -0.318240740740741
	},
	{
		id: 722,
		time: 721,
		velocity: 13.5086111111111,
		power: -1733.58559793274,
		road: 7117.50791666666,
		acceleration: -0.366018518518519
	},
	{
		id: 723,
		time: 722,
		velocity: 13.1544444444444,
		power: -921.203975571226,
		road: 7130.91282407407,
		acceleration: -0.300740740740739
	},
	{
		id: 724,
		time: 723,
		velocity: 13.1005555555556,
		power: -3885.23145148574,
		road: 7143.89935185185,
		acceleration: -0.536018518518521
	},
	{
		id: 725,
		time: 724,
		velocity: 11.9005555555556,
		power: -4132.8551426109,
		road: 7156.33638888889,
		acceleration: -0.56296296296296
	},
	{
		id: 726,
		time: 725,
		velocity: 11.4655555555556,
		power: -6287.89116027126,
		road: 7168.10912037037,
		acceleration: -0.76564814814815
	},
	{
		id: 727,
		time: 726,
		velocity: 10.8036111111111,
		power: -3449.67545270704,
		road: 7179.23782407407,
		acceleration: -0.522407407407407
	},
	{
		id: 728,
		time: 727,
		velocity: 10.3333333333333,
		power: -3470.10530066251,
		road: 7189.83819444444,
		acceleration: -0.534259259259258
	},
	{
		id: 729,
		time: 728,
		velocity: 9.86277777777778,
		power: -3757.62484698693,
		road: 7199.88296296296,
		acceleration: -0.576944444444445
	},
	{
		id: 730,
		time: 729,
		velocity: 9.07277777777778,
		power: -3202.59855794175,
		road: 7209.37300925925,
		acceleration: -0.532499999999999
	},
	{
		id: 731,
		time: 730,
		velocity: 8.73583333333333,
		power: -3960.48153027227,
		road: 7218.27717592592,
		acceleration: -0.639259259259262
	},
	{
		id: 732,
		time: 731,
		velocity: 7.945,
		power: -2357.49781956388,
		road: 7226.63009259259,
		acceleration: -0.463240740740741
	},
	{
		id: 733,
		time: 732,
		velocity: 7.68305555555555,
		power: -2704.21927033674,
		road: 7234.48949074074,
		acceleration: -0.523796296296295
	},
	{
		id: 734,
		time: 733,
		velocity: 7.16444444444444,
		power: -1216.44542562168,
		road: 7241.92171296296,
		acceleration: -0.330555555555556
	},
	{
		id: 735,
		time: 734,
		velocity: 6.95333333333333,
		power: -1940.71937593026,
		road: 7248.9661574074,
		acceleration: -0.445
	},
	{
		id: 736,
		time: 735,
		velocity: 6.34805555555556,
		power: -2219.36839967101,
		road: 7255.53458333333,
		acceleration: -0.507037037037037
	},
	{
		id: 737,
		time: 736,
		velocity: 5.64333333333333,
		power: -1630.61718367281,
		road: 7261.63472222222,
		acceleration: -0.429537037037037
	},
	{
		id: 738,
		time: 737,
		velocity: 5.66472222222222,
		power: -1052.04405504791,
		road: 7267.35037037037,
		acceleration: -0.339444444444444
	},
	{
		id: 739,
		time: 738,
		velocity: 5.32972222222222,
		power: -122.017301420496,
		road: 7272.8124537037,
		acceleration: -0.167685185185185
	},
	{
		id: 740,
		time: 739,
		velocity: 5.14027777777778,
		power: -914.743207728429,
		road: 7278.02703703703,
		acceleration: -0.327314814814814
	},
	{
		id: 741,
		time: 740,
		velocity: 4.68277777777778,
		power: -559.678801575924,
		road: 7282.94759259259,
		acceleration: -0.260740740740742
	},
	{
		id: 742,
		time: 741,
		velocity: 4.5475,
		power: -295.46505455866,
		road: 7287.63472222222,
		acceleration: -0.206111111111111
	},
	{
		id: 743,
		time: 742,
		velocity: 4.52194444444444,
		power: 189.035002309855,
		road: 7292.17124999999,
		acceleration: -0.0950925925925921
	},
	{
		id: 744,
		time: 743,
		velocity: 4.3975,
		power: 368.07804744043,
		road: 7296.63435185185,
		acceleration: -0.0517592592592591
	},
	{
		id: 745,
		time: 744,
		velocity: 4.39222222222222,
		power: 405.963684373005,
		road: 7301.05078703703,
		acceleration: -0.0415740740740738
	},
	{
		id: 746,
		time: 745,
		velocity: 4.39722222222222,
		power: 479.426527928085,
		road: 7305.4349074074,
		acceleration: -0.0230555555555556
	},
	{
		id: 747,
		time: 746,
		velocity: 4.32833333333333,
		power: 441.406141410442,
		road: 7309.79180555555,
		acceleration: -0.0313888888888885
	},
	{
		id: 748,
		time: 747,
		velocity: 4.29805555555556,
		power: 149.264597329803,
		road: 7314.0824537037,
		acceleration: -0.101111111111112
	},
	{
		id: 749,
		time: 748,
		velocity: 4.09388888888889,
		power: -726.084757181782,
		road: 7318.16041666666,
		acceleration: -0.324259259259259
	},
	{
		id: 750,
		time: 749,
		velocity: 3.35555555555556,
		power: -1185.34594029005,
		road: 7321.83902777777,
		acceleration: -0.474444444444444
	},
	{
		id: 751,
		time: 750,
		velocity: 2.87472222222222,
		power: -1042.82805449929,
		road: 7325.04231481481,
		acceleration: -0.476203703703704
	},
	{
		id: 752,
		time: 751,
		velocity: 2.66527777777778,
		power: -327.701529278313,
		road: 7327.88069444444,
		acceleration: -0.253611111111111
	},
	{
		id: 753,
		time: 752,
		velocity: 2.59472222222222,
		power: -154.238469698654,
		road: 7330.49555555555,
		acceleration: -0.193425925925926
	},
	{
		id: 754,
		time: 753,
		velocity: 2.29444444444444,
		power: 43.6587976311968,
		road: 7332.95763888888,
		acceleration: -0.11212962962963
	},
	{
		id: 755,
		time: 754,
		velocity: 2.32888888888889,
		power: 282.621493121165,
		road: 7335.36032407407,
		acceleration: -0.00666666666666638
	},
	{
		id: 756,
		time: 755,
		velocity: 2.57472222222222,
		power: 767.136144449564,
		road: 7337.8562037037,
		acceleration: 0.193055555555556
	},
	{
		id: 757,
		time: 756,
		velocity: 2.87361111111111,
		power: 1001.24706182104,
		road: 7340.57675925925,
		acceleration: 0.256296296296296
	},
	{
		id: 758,
		time: 757,
		velocity: 3.09777777777778,
		power: 798.100343435246,
		road: 7343.50305555555,
		acceleration: 0.155185185185185
	},
	{
		id: 759,
		time: 758,
		velocity: 3.04027777777778,
		power: 676.764174943546,
		road: 7346.5573611111,
		acceleration: 0.100833333333333
	},
	{
		id: 760,
		time: 759,
		velocity: 3.17611111111111,
		power: 347.904999587017,
		road: 7349.6548611111,
		acceleration: -0.0144444444444445
	},
	{
		id: 761,
		time: 760,
		velocity: 3.05444444444444,
		power: 449.313036675916,
		road: 7352.75509259259,
		acceleration: 0.0199074074074082
	},
	{
		id: 762,
		time: 761,
		velocity: 3.1,
		power: 536.910198377852,
		road: 7355.88907407407,
		acceleration: 0.0475925925925922
	},
	{
		id: 763,
		time: 762,
		velocity: 3.31888888888889,
		power: 1084.60077165346,
		road: 7359.15509259259,
		acceleration: 0.216481481481482
	},
	{
		id: 764,
		time: 763,
		velocity: 3.70388888888889,
		power: 1180.6217662264,
		road: 7362.64064814814,
		acceleration: 0.222592592592593
	},
	{
		id: 765,
		time: 764,
		velocity: 3.76777777777778,
		power: 1030.74571103866,
		road: 7366.31763888888,
		acceleration: 0.160277777777778
	},
	{
		id: 766,
		time: 765,
		velocity: 3.79972222222222,
		power: 966.297997083933,
		road: 7370.14009259259,
		acceleration: 0.130648148148148
	},
	{
		id: 767,
		time: 766,
		velocity: 4.09583333333333,
		power: 795.134502633274,
		road: 7374.06648148148,
		acceleration: 0.0772222222222219
	},
	{
		id: 768,
		time: 767,
		velocity: 3.99944444444444,
		power: 816.826168494808,
		road: 7378.07069444444,
		acceleration: 0.0784259259259263
	},
	{
		id: 769,
		time: 768,
		velocity: 4.035,
		power: -13.374414232425,
		road: 7382.04421296296,
		acceleration: -0.139814814814815
	},
	{
		id: 770,
		time: 769,
		velocity: 3.67638888888889,
		power: -141.811914228693,
		road: 7385.86046296296,
		acceleration: -0.174722222222222
	},
	{
		id: 771,
		time: 770,
		velocity: 3.47527777777778,
		power: -374.454671929742,
		road: 7389.46731481481,
		acceleration: -0.244074074074074
	},
	{
		id: 772,
		time: 771,
		velocity: 3.30277777777778,
		power: 46.0861291926067,
		road: 7392.89222222222,
		acceleration: -0.119814814814815
	},
	{
		id: 773,
		time: 772,
		velocity: 3.31694444444444,
		power: 286.707578492855,
		road: 7396.23555555555,
		acceleration: -0.043333333333333
	},
	{
		id: 774,
		time: 773,
		velocity: 3.34527777777778,
		power: 677.770386705265,
		road: 7399.59657407407,
		acceleration: 0.0787037037037033
	},
	{
		id: 775,
		time: 774,
		velocity: 3.53888888888889,
		power: 760.726394103845,
		road: 7403.04606481481,
		acceleration: 0.0982407407407413
	},
	{
		id: 776,
		time: 775,
		velocity: 3.61166666666667,
		power: 906.305430181253,
		road: 7406.61129629629,
		acceleration: 0.133240740740741
	},
	{
		id: 777,
		time: 776,
		velocity: 3.745,
		power: 862.704522527952,
		road: 7410.29884259259,
		acceleration: 0.111388888888889
	},
	{
		id: 778,
		time: 777,
		velocity: 3.87305555555556,
		power: 906.414063894408,
		road: 7414.0999074074,
		acceleration: 0.115648148148148
	},
	{
		id: 779,
		time: 778,
		velocity: 3.95861111111111,
		power: 1024.86575233061,
		road: 7418.02814814814,
		acceleration: 0.138703703703704
	},
	{
		id: 780,
		time: 779,
		velocity: 4.16111111111111,
		power: 1185.15137942326,
		road: 7422.11023148148,
		acceleration: 0.168981481481481
	},
	{
		id: 781,
		time: 780,
		velocity: 4.38,
		power: 880.780945930597,
		road: 7426.31833333333,
		acceleration: 0.0830555555555561
	},
	{
		id: 782,
		time: 781,
		velocity: 4.20777777777778,
		power: 649.74448266041,
		road: 7430.57944444444,
		acceleration: 0.0229629629629624
	},
	{
		id: 783,
		time: 782,
		velocity: 4.23,
		power: 494.373708650363,
		road: 7434.84425925925,
		acceleration: -0.0155555555555553
	},
	{
		id: 784,
		time: 783,
		velocity: 4.33333333333333,
		power: 772.176351515958,
		road: 7439.12736111111,
		acceleration: 0.0521296296296292
	},
	{
		id: 785,
		time: 784,
		velocity: 4.36416666666667,
		power: 1259.59784287622,
		road: 7443.51842592592,
		acceleration: 0.163796296296296
	},
	{
		id: 786,
		time: 785,
		velocity: 4.72138888888889,
		power: 1162.23265207286,
		road: 7448.05671296296,
		acceleration: 0.130648148148148
	},
	{
		id: 787,
		time: 786,
		velocity: 4.72527777777778,
		power: 1518.41100176362,
		road: 7452.76032407407,
		acceleration: 0.2
	},
	{
		id: 788,
		time: 787,
		velocity: 4.96416666666667,
		power: 750.416837514333,
		road: 7457.57574074074,
		acceleration: 0.0236111111111112
	},
	{
		id: 789,
		time: 788,
		velocity: 4.79222222222222,
		power: 656.809589972882,
		road: 7462.40430555555,
		acceleration: 0.0026851851851859
	},
	{
		id: 790,
		time: 789,
		velocity: 4.73333333333333,
		power: -624.728231042841,
		road: 7467.09421296296,
		acceleration: -0.28
	},
	{
		id: 791,
		time: 790,
		velocity: 4.12416666666667,
		power: -933.143118093808,
		road: 7471.46259259259,
		acceleration: -0.363055555555555
	},
	{
		id: 792,
		time: 791,
		velocity: 3.70305555555556,
		power: -1757.93435777141,
		road: 7475.34291666666,
		acceleration: -0.613055555555556
	},
	{
		id: 793,
		time: 792,
		velocity: 2.89416666666667,
		power: -2749.30994069387,
		road: 7478.37217592592,
		acceleration: -1.08907407407407
	},
	{
		id: 794,
		time: 793,
		velocity: 0.856944444444444,
		power: -1703.84924642441,
		road: 7480.33421296296,
		acceleration: -1.04537037037037
	},
	{
		id: 795,
		time: 794,
		velocity: 0.566944444444444,
		power: -726.187609463663,
		road: 7481.32152777777,
		acceleration: -0.904074074074074
	},
	{
		id: 796,
		time: 795,
		velocity: 0.181944444444444,
		power: -58.7747737841941,
		road: 7481.71398148148,
		acceleration: -0.285648148148148
	},
	{
		id: 797,
		time: 796,
		velocity: 0,
		power: -9.03144085448856,
		road: 7481.86912037037,
		acceleration: -0.188981481481481
	},
	{
		id: 798,
		time: 797,
		velocity: 0,
		power: 1.92138653346329,
		road: 7481.89944444444,
		acceleration: -0.0606481481481481
	},
	{
		id: 799,
		time: 798,
		velocity: 0,
		power: 0,
		road: 7481.89944444444,
		acceleration: 0
	},
	{
		id: 800,
		time: 799,
		velocity: 0,
		power: 123.265097184202,
		road: 7482.12458333333,
		acceleration: 0.450277777777778
	},
	{
		id: 801,
		time: 800,
		velocity: 1.35083333333333,
		power: 1114.17034389305,
		road: 7483.10824074074,
		acceleration: 1.06675925925926
	},
	{
		id: 802,
		time: 801,
		velocity: 3.20027777777778,
		power: 3035.67228379898,
		road: 7485.29263888888,
		acceleration: 1.33472222222222
	},
	{
		id: 803,
		time: 802,
		velocity: 4.00416666666667,
		power: 3228.90429861767,
		road: 7488.59296296296,
		acceleration: 0.897129629629629
	},
	{
		id: 804,
		time: 803,
		velocity: 4.04222222222222,
		power: 1552.30871160797,
		road: 7492.48398148148,
		acceleration: 0.284259259259259
	},
	{
		id: 805,
		time: 804,
		velocity: 4.05305555555556,
		power: 435.307490658233,
		road: 7496.50587962963,
		acceleration: -0.0225
	},
	{
		id: 806,
		time: 805,
		velocity: 3.93666666666667,
		power: 277.419191649717,
		road: 7500.48509259259,
		acceleration: -0.0628703703703697
	},
	{
		id: 807,
		time: 806,
		velocity: 3.85361111111111,
		power: 157.15610386811,
		road: 7504.38611111111,
		acceleration: -0.0935185185185183
	},
	{
		id: 808,
		time: 807,
		velocity: 3.7725,
		power: 265.024443210155,
		road: 7508.20907407407,
		acceleration: -0.0625925925925932
	},
	{
		id: 809,
		time: 808,
		velocity: 3.74888888888889,
		power: 289.52405673068,
		road: 7511.97356481481,
		acceleration: -0.0543518518518518
	},
	{
		id: 810,
		time: 809,
		velocity: 3.69055555555556,
		power: 628.485318126621,
		road: 7515.73129629629,
		acceleration: 0.0408333333333331
	},
	{
		id: 811,
		time: 810,
		velocity: 3.895,
		power: 106.809224800615,
		road: 7519.45694444444,
		acceleration: -0.105
	},
	{
		id: 812,
		time: 811,
		velocity: 3.43388888888889,
		power: -224.412678372081,
		road: 7523.02972222222,
		acceleration: -0.20074074074074
	},
	{
		id: 813,
		time: 812,
		velocity: 3.08833333333333,
		power: -1078.9847381696,
		road: 7526.25949074074,
		acceleration: -0.485277777777778
	},
	{
		id: 814,
		time: 813,
		velocity: 2.43916666666667,
		power: -615.998264694897,
		road: 7529.065,
		acceleration: -0.363240740740741
	},
	{
		id: 815,
		time: 814,
		velocity: 2.34416666666667,
		power: -324.824018368235,
		road: 7531.55467592592,
		acceleration: -0.268425925925925
	},
	{
		id: 816,
		time: 815,
		velocity: 2.28305555555556,
		power: 158.332702138814,
		road: 7533.88078703703,
		acceleration: -0.0587037037037041
	},
	{
		id: 817,
		time: 816,
		velocity: 2.26305555555556,
		power: 204.49270093382,
		road: 7536.15967592592,
		acceleration: -0.0357407407407409
	},
	{
		id: 818,
		time: 817,
		velocity: 2.23694444444444,
		power: 340.771037109179,
		road: 7538.43449074074,
		acceleration: 0.0275925925925926
	},
	{
		id: 819,
		time: 818,
		velocity: 2.36583333333333,
		power: 465.770611624673,
		road: 7540.76328703703,
		acceleration: 0.0803703703703706
	},
	{
		id: 820,
		time: 819,
		velocity: 2.50416666666667,
		power: 498.520390427508,
		road: 7543.17583333333,
		acceleration: 0.0871296296296293
	},
	{
		id: 821,
		time: 820,
		velocity: 2.49833333333333,
		power: 338.370478716404,
		road: 7545.63893518518,
		acceleration: 0.0139814814814816
	},
	{
		id: 822,
		time: 821,
		velocity: 2.40777777777778,
		power: 220.690209467495,
		road: 7548.09106481481,
		acceleration: -0.0359259259259259
	},
	{
		id: 823,
		time: 822,
		velocity: 2.39638888888889,
		power: 105.865664810146,
		road: 7550.48324074074,
		acceleration: -0.0839814814814814
	},
	{
		id: 824,
		time: 823,
		velocity: 2.24638888888889,
		power: 308.22615894176,
		road: 7552.83717592592,
		acceleration: 0.00749999999999984
	},
	{
		id: 825,
		time: 824,
		velocity: 2.43027777777778,
		power: 394.112027655273,
		road: 7555.21685185185,
		acceleration: 0.0439814814814818
	},
	{
		id: 826,
		time: 825,
		velocity: 2.52833333333333,
		power: 548.777312480373,
		road: 7557.67097222222,
		acceleration: 0.104907407407407
	},
	{
		id: 827,
		time: 826,
		velocity: 2.56111111111111,
		power: 678.116469654796,
		road: 7560.25050925926,
		acceleration: 0.145925925925926
	},
	{
		id: 828,
		time: 827,
		velocity: 2.86805555555556,
		power: 722.444917530042,
		road: 7562.97685185185,
		acceleration: 0.147685185185185
	},
	{
		id: 829,
		time: 828,
		velocity: 2.97138888888889,
		power: 990.502679465963,
		road: 7565.89009259259,
		acceleration: 0.226111111111111
	},
	{
		id: 830,
		time: 829,
		velocity: 3.23944444444444,
		power: 807.86895299607,
		road: 7568.98740740741,
		acceleration: 0.142037037037037
	},
	{
		id: 831,
		time: 830,
		velocity: 3.29416666666667,
		power: 595.017753103261,
		road: 7572.18712962963,
		acceleration: 0.0627777777777783
	},
	{
		id: 832,
		time: 831,
		velocity: 3.15972222222222,
		power: 382.012423567815,
		road: 7575.41398148148,
		acceleration: -0.00851851851851837
	},
	{
		id: 833,
		time: 832,
		velocity: 3.21388888888889,
		power: 168.849119246307,
		road: 7578.59796296296,
		acceleration: -0.0772222222222227
	},
	{
		id: 834,
		time: 833,
		velocity: 3.0625,
		power: 300.129499944319,
		road: 7581.72740740741,
		acceleration: -0.0318518518518518
	},
	{
		id: 835,
		time: 834,
		velocity: 3.06416666666667,
		power: -313.04209951695,
		road: 7584.71958333333,
		acceleration: -0.242685185185185
	},
	{
		id: 836,
		time: 835,
		velocity: 2.48583333333333,
		power: -373.27594620389,
		road: 7587.45263888889,
		acceleration: -0.275555555555556
	},
	{
		id: 837,
		time: 836,
		velocity: 2.23583333333333,
		power: -234.741087050407,
		road: 7589.93259259259,
		acceleration: -0.230648148148148
	},
	{
		id: 838,
		time: 837,
		velocity: 2.37222222222222,
		power: 163.808782994528,
		road: 7592.26893518518,
		acceleration: -0.0565740740740743
	},
	{
		id: 839,
		time: 838,
		velocity: 2.31611111111111,
		power: 310.634816700366,
		road: 7594.58254629629,
		acceleration: 0.0111111111111111
	},
	{
		id: 840,
		time: 839,
		velocity: 2.26916666666667,
		power: 74.4982629711582,
		road: 7596.85384259259,
		acceleration: -0.0957407407407409
	},
	{
		id: 841,
		time: 840,
		velocity: 2.085,
		power: -29.2352869485042,
		road: 7599.00509259259,
		acceleration: -0.144351851851852
	},
	{
		id: 842,
		time: 841,
		velocity: 1.88305555555556,
		power: -55.5778377058376,
		road: 7601.00467592592,
		acceleration: -0.158981481481481
	},
	{
		id: 843,
		time: 842,
		velocity: 1.79222222222222,
		power: -140.334178472631,
		road: 7602.81935185185,
		acceleration: -0.210833333333333
	},
	{
		id: 844,
		time: 843,
		velocity: 1.4525,
		power: -90.3095709181433,
		road: 7604.43467592592,
		acceleration: -0.187870370370371
	},
	{
		id: 845,
		time: 844,
		velocity: 1.31944444444444,
		power: -135.016324371848,
		road: 7605.84115740741,
		acceleration: -0.229814814814815
	},
	{
		id: 846,
		time: 845,
		velocity: 1.10277777777778,
		power: -105.662318566145,
		road: 7607.02138888889,
		acceleration: -0.222685185185185
	},
	{
		id: 847,
		time: 846,
		velocity: 0.784444444444444,
		power: -250.987739934151,
		road: 7607.87037037037,
		acceleration: -0.439814814814815
	},
	{
		id: 848,
		time: 847,
		velocity: 0,
		power: -101.251981878346,
		road: 7608.31564814815,
		acceleration: -0.367592592592593
	},
	{
		id: 849,
		time: 848,
		velocity: 0,
		power: -16.5911834957765,
		road: 7608.44638888889,
		acceleration: -0.261481481481481
	},
	{
		id: 850,
		time: 849,
		velocity: 0,
		power: 0,
		road: 7608.44638888889,
		acceleration: 0
	},
	{
		id: 851,
		time: 850,
		velocity: 0,
		power: 149.303277594374,
		road: 7608.69699074074,
		acceleration: 0.501203703703704
	},
	{
		id: 852,
		time: 851,
		velocity: 1.50361111111111,
		power: 814.07630548506,
		road: 7609.60638888889,
		acceleration: 0.816388888888889
	},
	{
		id: 853,
		time: 852,
		velocity: 2.44916666666667,
		power: 1386.01401104407,
		road: 7611.29277777778,
		acceleration: 0.737592592592592
	},
	{
		id: 854,
		time: 853,
		velocity: 2.21277777777778,
		power: 499.87276322582,
		road: 7613.40759259259,
		acceleration: 0.11925925925926
	},
	{
		id: 855,
		time: 854,
		velocity: 1.86138888888889,
		power: 100.244368492972,
		road: 7615.54180555555,
		acceleration: -0.080462962962963
	},
	{
		id: 856,
		time: 855,
		velocity: 2.20777777777778,
		power: 295.912028308107,
		road: 7617.645,
		acceleration: 0.0184259259259254
	},
	{
		id: 857,
		time: 856,
		velocity: 2.26805555555556,
		power: 543.879078298157,
		road: 7619.82393518518,
		acceleration: 0.133055555555556
	},
	{
		id: 858,
		time: 857,
		velocity: 2.26055555555556,
		power: 216.645797936653,
		road: 7622.05546296296,
		acceleration: -0.0278703703703709
	},
	{
		id: 859,
		time: 858,
		velocity: 2.12416666666667,
		power: 167.81543830663,
		road: 7624.24833333333,
		acceleration: -0.0494444444444442
	},
	{
		id: 860,
		time: 859,
		velocity: 2.11972222222222,
		power: 89.5201681481827,
		road: 7626.3737037037,
		acceleration: -0.0855555555555552
	},
	{
		id: 861,
		time: 860,
		velocity: 2.00388888888889,
		power: 335.389690817568,
		road: 7628.47546296296,
		acceleration: 0.0383333333333331
	},
	{
		id: 862,
		time: 861,
		velocity: 2.23916666666667,
		power: 560.755322049665,
		road: 7630.66625,
		acceleration: 0.139722222222222
	},
	{
		id: 863,
		time: 862,
		velocity: 2.53888888888889,
		power: 1425.30159087893,
		road: 7633.16236111111,
		acceleration: 0.470925925925926
	},
	{
		id: 864,
		time: 863,
		velocity: 3.41666666666667,
		power: 1599.3566392121,
		road: 7636.11333333333,
		acceleration: 0.438796296296297
	},
	{
		id: 865,
		time: 864,
		velocity: 3.55555555555556,
		power: 2954.188889178,
		road: 7639.65569444444,
		acceleration: 0.743981481481482
	},
	{
		id: 866,
		time: 865,
		velocity: 4.77083333333333,
		power: 3418.46733171839,
		road: 7643.92296296296,
		acceleration: 0.705833333333332
	},
	{
		id: 867,
		time: 866,
		velocity: 5.53416666666667,
		power: 3567.32666504569,
		road: 7648.85337962963,
		acceleration: 0.620462962962963
	},
	{
		id: 868,
		time: 867,
		velocity: 5.41694444444444,
		power: 2369.96064836871,
		road: 7654.25305555555,
		acceleration: 0.318055555555556
	},
	{
		id: 869,
		time: 868,
		velocity: 5.725,
		power: 674.005746210392,
		road: 7659.80328703704,
		acceleration: -0.0169444444444453
	},
	{
		id: 870,
		time: 869,
		velocity: 5.48333333333333,
		power: 476.870068840461,
		road: 7665.31828703704,
		acceleration: -0.0535185185185183
	},
	{
		id: 871,
		time: 870,
		velocity: 5.25638888888889,
		power: -822.056998748019,
		road: 7670.65375,
		acceleration: -0.305555555555555
	},
	{
		id: 872,
		time: 871,
		velocity: 4.80833333333333,
		power: -467.865078741268,
		road: 7675.71689814815,
		acceleration: -0.239074074074074
	},
	{
		id: 873,
		time: 872,
		velocity: 4.76611111111111,
		power: 1170.63537904307,
		road: 7680.71310185185,
		acceleration: 0.105185185185186
	},
	{
		id: 874,
		time: 873,
		velocity: 5.57194444444445,
		power: 3092.73260010943,
		road: 7685.99824074074,
		acceleration: 0.472685185185185
	},
	{
		id: 875,
		time: 874,
		velocity: 6.22638888888889,
		power: 5524.84138887556,
		road: 7691.93550925926,
		acceleration: 0.831574074074075
	},
	{
		id: 876,
		time: 875,
		velocity: 7.26083333333333,
		power: 5113.80113803839,
		road: 7698.61481481481,
		acceleration: 0.6525
	},
	{
		id: 877,
		time: 876,
		velocity: 7.52944444444444,
		power: 5368.10598082958,
		road: 7705.9274074074,
		acceleration: 0.614074074074074
	},
	{
		id: 878,
		time: 877,
		velocity: 8.06861111111111,
		power: 3752.2913071175,
		road: 7713.71925925926,
		acceleration: 0.344444444444445
	},
	{
		id: 879,
		time: 878,
		velocity: 8.29416666666667,
		power: 4118.485824105,
		road: 7721.86648148148,
		acceleration: 0.366296296296295
	},
	{
		id: 880,
		time: 879,
		velocity: 8.62833333333333,
		power: 3182.74360438508,
		road: 7730.31101851852,
		acceleration: 0.228333333333335
	},
	{
		id: 881,
		time: 880,
		velocity: 8.75361111111111,
		power: 1966.32813012611,
		road: 7738.90537037037,
		acceleration: 0.0712962962962962
	},
	{
		id: 882,
		time: 881,
		velocity: 8.50805555555556,
		power: -34.4966612194544,
		road: 7747.44898148148,
		acceleration: -0.172777777777778
	},
	{
		id: 883,
		time: 882,
		velocity: 8.11,
		power: -372.91797253625,
		road: 7755.79939814815,
		acceleration: -0.21361111111111
	},
	{
		id: 884,
		time: 883,
		velocity: 8.11277777777778,
		power: -723.929991674588,
		road: 7763.91388888889,
		acceleration: -0.258240740740743
	},
	{
		id: 885,
		time: 884,
		velocity: 7.73333333333333,
		power: 1182.67670967275,
		road: 7771.89546296296,
		acceleration: -0.0075925925925926
	},
	{
		id: 886,
		time: 885,
		velocity: 8.08722222222222,
		power: 1350.17399961137,
		road: 7779.88041666666,
		acceleration: 0.0143518518518535
	},
	{
		id: 887,
		time: 886,
		velocity: 8.15583333333333,
		power: 1298.49297760719,
		road: 7787.8761574074,
		acceleration: 0.00722222222222069
	},
	{
		id: 888,
		time: 887,
		velocity: 7.755,
		power: -109.650164007831,
		road: 7795.78689814815,
		acceleration: -0.177222222222222
	},
	{
		id: 889,
		time: 888,
		velocity: 7.55555555555556,
		power: -447.922113912536,
		road: 7803.49805555555,
		acceleration: -0.221944444444445
	},
	{
		id: 890,
		time: 889,
		velocity: 7.49,
		power: 227.340753072844,
		road: 7811.03439814814,
		acceleration: -0.127685185185186
	},
	{
		id: 891,
		time: 890,
		velocity: 7.37194444444444,
		power: 326.32213615978,
		road: 7818.45083333333,
		acceleration: -0.11212962962963
	},
	{
		id: 892,
		time: 891,
		velocity: 7.21916666666667,
		power: 244.95216158986,
		road: 7825.75013888889,
		acceleration: -0.122129629629629
	},
	{
		id: 893,
		time: 892,
		velocity: 7.12361111111111,
		power: 1141.24496381327,
		road: 7832.99273148148,
		acceleration: 0.00870370370370388
	},
	{
		id: 894,
		time: 893,
		velocity: 7.39805555555555,
		power: 1993.35712541803,
		road: 7840.30421296296,
		acceleration: 0.129074074074074
	},
	{
		id: 895,
		time: 894,
		velocity: 7.60638888888889,
		power: 3963.36135968972,
		road: 7847.87546296296,
		acceleration: 0.390462962962963
	},
	{
		id: 896,
		time: 895,
		velocity: 8.295,
		power: 4384.53395946614,
		road: 7855.84921296296,
		acceleration: 0.414537037037038
	},
	{
		id: 897,
		time: 896,
		velocity: 8.64166666666667,
		power: 6955.76177559011,
		road: 7864.37453703703,
		acceleration: 0.688611111111111
	},
	{
		id: 898,
		time: 897,
		velocity: 9.67222222222222,
		power: 6831.71797765477,
		road: 7873.54773148148,
		acceleration: 0.607129629629629
	},
	{
		id: 899,
		time: 898,
		velocity: 10.1163888888889,
		power: 6469.93438035918,
		road: 7883.28287037037,
		acceleration: 0.51675925925926
	},
	{
		id: 900,
		time: 899,
		velocity: 10.1919444444444,
		power: 3051.73257940595,
		road: 7893.34333333333,
		acceleration: 0.133888888888889
	},
	{
		id: 901,
		time: 900,
		velocity: 10.0738888888889,
		power: 1720.37354232082,
		road: 7903.46731481481,
		acceleration: -0.00685185185185233
	},
	{
		id: 902,
		time: 901,
		velocity: 10.0958333333333,
		power: 2465.67326861733,
		road: 7913.6225,
		acceleration: 0.06925925925926
	},
	{
		id: 903,
		time: 902,
		velocity: 10.3997222222222,
		power: 3758.63482686739,
		road: 7923.91046296296,
		acceleration: 0.196296296296296
	},
	{
		id: 904,
		time: 903,
		velocity: 10.6627777777778,
		power: 3806.39538341661,
		road: 7934.39240740741,
		acceleration: 0.191666666666666
	},
	{
		id: 905,
		time: 904,
		velocity: 10.6708333333333,
		power: 3544.27780817203,
		road: 7945.04893518518,
		acceleration: 0.157499999999999
	},
	{
		id: 906,
		time: 905,
		velocity: 10.8722222222222,
		power: 3685.97047163123,
		road: 7955.86625,
		acceleration: 0.164074074074076
	},
	{
		id: 907,
		time: 906,
		velocity: 11.155,
		power: 4311.85967383005,
		road: 7966.87319444444,
		acceleration: 0.215185185185184
	},
	{
		id: 908,
		time: 907,
		velocity: 11.3163888888889,
		power: 5835.84351960474,
		road: 7978.15925925926,
		acceleration: 0.343055555555555
	},
	{
		id: 909,
		time: 908,
		velocity: 11.9013888888889,
		power: 6340.03444505582,
		road: 7989.80046296296,
		acceleration: 0.367222222222223
	},
	{
		id: 910,
		time: 909,
		velocity: 12.2566666666667,
		power: 6446.69878195919,
		road: 8001.8025,
		acceleration: 0.354444444444445
	},
	{
		id: 911,
		time: 910,
		velocity: 12.3797222222222,
		power: 3917.1678007886,
		road: 8014.04351851852,
		acceleration: 0.123518518518518
	},
	{
		id: 912,
		time: 911,
		velocity: 12.2719444444444,
		power: 3243.15152608741,
		road: 8026.3775,
		acceleration: 0.0624074074074077
	},
	{
		id: 913,
		time: 912,
		velocity: 12.4438888888889,
		power: 3237.63919485201,
		road: 8038.77254629629,
		acceleration: 0.0597222222222236
	},
	{
		id: 914,
		time: 913,
		velocity: 12.5588888888889,
		power: 4556.02068923218,
		road: 8051.28050925926,
		acceleration: 0.16611111111111
	},
	{
		id: 915,
		time: 914,
		velocity: 12.7702777777778,
		power: 5307.54477446146,
		road: 8063.98129629629,
		acceleration: 0.219537037037036
	},
	{
		id: 916,
		time: 915,
		velocity: 13.1025,
		power: 8426.57318293969,
		road: 8077.01879629629,
		acceleration: 0.453888888888889
	},
	{
		id: 917,
		time: 916,
		velocity: 13.9205555555556,
		power: 5315.90440724382,
		road: 8090.37763888889,
		acceleration: 0.188796296296296
	},
	{
		id: 918,
		time: 917,
		velocity: 13.3366666666667,
		power: 3063.2759058837,
		road: 8103.83532407407,
		acceleration: 0.00888888888889028
	},
	{
		id: 919,
		time: 918,
		velocity: 13.1291666666667,
		power: -1547.30603965824,
		road: 8117.12296296296,
		acceleration: -0.348981481481482
	},
	{
		id: 920,
		time: 919,
		velocity: 12.8736111111111,
		power: 58.6396267488392,
		road: 8130.12708333333,
		acceleration: -0.218055555555557
	},
	{
		id: 921,
		time: 920,
		velocity: 12.6825,
		power: 400.633095719668,
		road: 8142.9286574074,
		acceleration: -0.187037037037035
	},
	{
		id: 922,
		time: 921,
		velocity: 12.5680555555556,
		power: 1379.4847019908,
		road: 8155.5849537037,
		acceleration: -0.10351851851852
	},
	{
		id: 923,
		time: 922,
		velocity: 12.5630555555556,
		power: 3217.5063979969,
		road: 8168.21430555555,
		acceleration: 0.0496296296296315
	},
	{
		id: 924,
		time: 923,
		velocity: 12.8313888888889,
		power: 5176.55109221514,
		road: 8180.97148148148,
		acceleration: 0.206018518518515
	},
	{
		id: 925,
		time: 924,
		velocity: 13.1861111111111,
		power: 5016.44100136852,
		road: 8193.92356481481,
		acceleration: 0.183796296296299
	},
	{
		id: 926,
		time: 925,
		velocity: 13.1144444444444,
		power: 4031.65374600189,
		road: 8207.01680555555,
		acceleration: 0.0985185185185173
	},
	{
		id: 927,
		time: 926,
		velocity: 13.1269444444444,
		power: 2964.22879380176,
		road: 8220.1649537037,
		acceleration: 0.0112962962962992
	},
	{
		id: 928,
		time: 927,
		velocity: 13.22,
		power: 2341.15373183387,
		road: 8233.29976851851,
		acceleration: -0.0379629629629648
	},
	{
		id: 929,
		time: 928,
		velocity: 13.0005555555556,
		power: 2166.51762804455,
		road: 8246.39027777777,
		acceleration: -0.0506481481481469
	},
	{
		id: 930,
		time: 929,
		velocity: 12.975,
		power: 1325.88182008787,
		road: 8259.3974537037,
		acceleration: -0.116018518518521
	},
	{
		id: 931,
		time: 930,
		velocity: 12.8719444444444,
		power: 1065.17455752532,
		road: 8272.27944444444,
		acceleration: -0.13435185185185
	},
	{
		id: 932,
		time: 931,
		velocity: 12.5975,
		power: 1624.50104095048,
		road: 8285.0512037037,
		acceleration: -0.0861111111111121
	},
	{
		id: 933,
		time: 932,
		velocity: 12.7166666666667,
		power: 2959.33187359795,
		road: 8297.79212962963,
		acceleration: 0.0244444444444447
	},
	{
		id: 934,
		time: 933,
		velocity: 12.9452777777778,
		power: 5971.44790825065,
		road: 8310.67754629629,
		acceleration: 0.264537037037037
	},
	{
		id: 935,
		time: 934,
		velocity: 13.3911111111111,
		power: 6733.91934750788,
		road: 8323.85037037037,
		acceleration: 0.310277777777777
	},
	{
		id: 936,
		time: 935,
		velocity: 13.6475,
		power: 6914.57705781449,
		road: 8337.33199074074,
		acceleration: 0.307314814814813
	},
	{
		id: 937,
		time: 936,
		velocity: 13.8672222222222,
		power: 5609.67889092014,
		road: 8351.06430555555,
		acceleration: 0.194074074074077
	},
	{
		id: 938,
		time: 937,
		velocity: 13.9733333333333,
		power: 4226.00744236249,
		road: 8364.93523148148,
		acceleration: 0.0831481481481475
	},
	{
		id: 939,
		time: 938,
		velocity: 13.8969444444444,
		power: 2305.68690405519,
		road: 8378.81666666666,
		acceleration: -0.062129629629629
	},
	{
		id: 940,
		time: 939,
		velocity: 13.6808333333333,
		power: 757.453559298898,
		road: 8392.57875,
		acceleration: -0.176574074074075
	},
	{
		id: 941,
		time: 940,
		velocity: 13.4436111111111,
		power: -3076.38916542059,
		road: 8406.01800925926,
		acceleration: -0.469074074074074
	},
	{
		id: 942,
		time: 941,
		velocity: 12.4897222222222,
		power: 52.5816790169322,
		road: 8419.11277777777,
		acceleration: -0.219907407407408
	},
	{
		id: 943,
		time: 942,
		velocity: 13.0211111111111,
		power: 685.076937331732,
		road: 8432.01476851851,
		acceleration: -0.165648148148147
	},
	{
		id: 944,
		time: 943,
		velocity: 12.9466666666667,
		power: 7004.97550943271,
		road: 8445.00513888889,
		acceleration: 0.342407407407407
	},
	{
		id: 945,
		time: 944,
		velocity: 13.5169444444444,
		power: 8436.09306499752,
		road: 8458.38282407407,
		acceleration: 0.432222222222222
	},
	{
		id: 946,
		time: 945,
		velocity: 14.3177777777778,
		power: 14349.674680525,
		road: 8472.39375,
		acceleration: 0.834259259259259
	},
	{
		id: 947,
		time: 946,
		velocity: 15.4494444444444,
		power: 15255.4839843692,
		road: 8487.23393518518,
		acceleration: 0.824259259259261
	},
	{
		id: 948,
		time: 947,
		velocity: 15.9897222222222,
		power: 16916.6403294228,
		road: 8502.91736111111,
		acceleration: 0.86222222222222
	},
	{
		id: 949,
		time: 948,
		velocity: 16.9044444444444,
		power: 16281.8150061403,
		road: 8519.40773148148,
		acceleration: 0.751666666666669
	},
	{
		id: 950,
		time: 949,
		velocity: 17.7044444444444,
		power: 19517.5921777905,
		road: 8536.71509259259,
		acceleration: 0.882314814814816
	},
	{
		id: 951,
		time: 950,
		velocity: 18.6366666666667,
		power: 18859.9179631351,
		road: 8554.85041666666,
		acceleration: 0.773611111111112
	},
	{
		id: 952,
		time: 951,
		velocity: 19.2252777777778,
		power: 14675.6600591963,
		road: 8573.61791666666,
		acceleration: 0.490740740740737
	},
	{
		id: 953,
		time: 952,
		velocity: 19.1766666666667,
		power: 4839.11206332107,
		road: 8592.5987037037,
		acceleration: -0.0641666666666652
	},
	{
		id: 954,
		time: 953,
		velocity: 18.4441666666667,
		power: -720.321507329864,
		road: 8611.36444444444,
		acceleration: -0.365925925925925
	},
	{
		id: 955,
		time: 954,
		velocity: 18.1275,
		power: -4404.82941989803,
		road: 8629.66342592592,
		acceleration: -0.567592592592593
	},
	{
		id: 956,
		time: 955,
		velocity: 17.4738888888889,
		power: -1823.37225547351,
		road: 8647.47203703703,
		acceleration: -0.413148148148149
	},
	{
		id: 957,
		time: 956,
		velocity: 17.2047222222222,
		power: -1847.01204510873,
		road: 8664.86958333333,
		acceleration: -0.408981481481483
	},
	{
		id: 958,
		time: 957,
		velocity: 16.9005555555556,
		power: 827.849669549006,
		road: 8681.94212962963,
		acceleration: -0.241018518518519
	},
	{
		id: 959,
		time: 958,
		velocity: 16.7508333333333,
		power: 1962.24910017243,
		road: 8698.81106481481,
		acceleration: -0.166203703703701
	},
	{
		id: 960,
		time: 959,
		velocity: 16.7061111111111,
		power: 2285.4176298488,
		road: 8715.52592592592,
		acceleration: -0.141944444444441
	},
	{
		id: 961,
		time: 960,
		velocity: 16.4747222222222,
		power: 3144.54672089745,
		road: 8732.1274537037,
		acceleration: -0.0847222222222257
	},
	{
		id: 962,
		time: 961,
		velocity: 16.4966666666667,
		power: 5124.67472446792,
		road: 8748.70703703703,
		acceleration: 0.0408333333333317
	},
	{
		id: 963,
		time: 962,
		velocity: 16.8286111111111,
		power: 4909.63808907747,
		road: 8765.32004629629,
		acceleration: 0.0260185185185229
	},
	{
		id: 964,
		time: 963,
		velocity: 16.5527777777778,
		power: 5082.81269042824,
		road: 8781.96393518518,
		acceleration: 0.0357407407407386
	},
	{
		id: 965,
		time: 964,
		velocity: 16.6038888888889,
		power: 3464.27559602781,
		road: 8798.59296296296,
		acceleration: -0.0654629629629611
	},
	{
		id: 966,
		time: 965,
		velocity: 16.6322222222222,
		power: 2526.73912613515,
		road: 8815.12837962963,
		acceleration: -0.12175925925926
	},
	{
		id: 967,
		time: 966,
		velocity: 16.1875,
		power: 688.128940954136,
		road: 8831.48578703704,
		acceleration: -0.234259259259261
	},
	{
		id: 968,
		time: 967,
		velocity: 15.9011111111111,
		power: -1275.37984764082,
		road: 8847.54810185185,
		acceleration: -0.355925925925925
	},
	{
		id: 969,
		time: 968,
		velocity: 15.5644444444444,
		power: 1774.04164044246,
		road: 8863.35699074074,
		acceleration: -0.150925925925929
	},
	{
		id: 970,
		time: 969,
		velocity: 15.7347222222222,
		power: -1143.1573948329,
		road: 8878.92,
		acceleration: -0.340833333333331
	},
	{
		id: 971,
		time: 970,
		velocity: 14.8786111111111,
		power: 1958.79195923424,
		road: 8894.24949074074,
		acceleration: -0.126203703703704
	},
	{
		id: 972,
		time: 971,
		velocity: 15.1858333333333,
		power: 3194.16321845498,
		road: 8909.49625,
		acceleration: -0.0392592592592607
	},
	{
		id: 973,
		time: 972,
		velocity: 15.6169444444444,
		power: 8654.84112330291,
		road: 8924.88708333333,
		acceleration: 0.327407407407408
	},
	{
		id: 974,
		time: 973,
		velocity: 15.8608333333333,
		power: 7266.3292740931,
		road: 8940.55138888889,
		acceleration: 0.219537037037036
	},
	{
		id: 975,
		time: 974,
		velocity: 15.8444444444444,
		power: 7281.41699520104,
		road: 8956.43050925926,
		acceleration: 0.210092592592595
	},
	{
		id: 976,
		time: 975,
		velocity: 16.2472222222222,
		power: 5463.34609748701,
		road: 8972.45685185185,
		acceleration: 0.0843518518518529
	},
	{
		id: 977,
		time: 976,
		velocity: 16.1138888888889,
		power: 5127.62606238842,
		road: 8988.55518518518,
		acceleration: 0.0596296296296259
	},
	{
		id: 978,
		time: 977,
		velocity: 16.0233333333333,
		power: 4050.26868966743,
		road: 9004.67773148148,
		acceleration: -0.0112037037036998
	},
	{
		id: 979,
		time: 978,
		velocity: 16.2136111111111,
		power: 4842.15574100967,
		road: 9020.81449074074,
		acceleration: 0.0396296296296299
	},
	{
		id: 980,
		time: 979,
		velocity: 16.2327777777778,
		power: 6155.63705203514,
		road: 9037.03171296296,
		acceleration: 0.121296296296293
	},
	{
		id: 981,
		time: 980,
		velocity: 16.3872222222222,
		power: 6764.94988828109,
		road: 9053.38675925926,
		acceleration: 0.15435185185185
	},
	{
		id: 982,
		time: 981,
		velocity: 16.6766666666667,
		power: 6577.13390008471,
		road: 9069.88694444444,
		acceleration: 0.135925925925928
	},
	{
		id: 983,
		time: 982,
		velocity: 16.6405555555556,
		power: 5306.85964188563,
		road: 9086.48097222222,
		acceleration: 0.0517592592592599
	},
	{
		id: 984,
		time: 983,
		velocity: 16.5425,
		power: 3073.2495459742,
		road: 9103.05666666666,
		acceleration: -0.0884259259259252
	},
	{
		id: 985,
		time: 984,
		velocity: 16.4113888888889,
		power: 3759.44298429227,
		road: 9119.56666666666,
		acceleration: -0.0429629629629638
	},
	{
		id: 986,
		time: 985,
		velocity: 16.5116666666667,
		power: 4311.25693666942,
		road: 9136.05162037037,
		acceleration: -0.00712962962962749
	},
	{
		id: 987,
		time: 986,
		velocity: 16.5211111111111,
		power: 4916.77326694812,
		road: 9152.54842592592,
		acceleration: 0.0308333333333302
	},
	{
		id: 988,
		time: 987,
		velocity: 16.5038888888889,
		power: 3874.09321948873,
		road: 9169.04305555555,
		acceleration: -0.0351851851851848
	},
	{
		id: 989,
		time: 988,
		velocity: 16.4061111111111,
		power: 580.661389708416,
		road: 9185.39953703703,
		acceleration: -0.241111111111113
	},
	{
		id: 990,
		time: 989,
		velocity: 15.7977777777778,
		power: -236.878243786326,
		road: 9201.49106481481,
		acceleration: -0.288796296296294
	},
	{
		id: 991,
		time: 990,
		velocity: 15.6375,
		power: -1064.16711617497,
		road: 9217.26902777777,
		acceleration: -0.338333333333333
	},
	{
		id: 992,
		time: 991,
		velocity: 15.3911111111111,
		power: 1538.46908517689,
		road: 9232.79796296296,
		acceleration: -0.159722222222221
	},
	{
		id: 993,
		time: 992,
		velocity: 15.3186111111111,
		power: 1000.50474625059,
		road: 9248.15097222222,
		acceleration: -0.192129629629632
	},
	{
		id: 994,
		time: 993,
		velocity: 15.0611111111111,
		power: 1667.91663238869,
		road: 9263.33666666666,
		acceleration: -0.142499999999998
	},
	{
		id: 995,
		time: 994,
		velocity: 14.9636111111111,
		power: 1420.65147423149,
		road: 9278.37310185185,
		acceleration: -0.15601851851852
	},
	{
		id: 996,
		time: 995,
		velocity: 14.8505555555556,
		power: 1062.33295305191,
		road: 9293.24287037037,
		acceleration: -0.177314814814814
	},
	{
		id: 997,
		time: 996,
		velocity: 14.5291666666667,
		power: 1257.83436042671,
		road: 9307.94412037037,
		acceleration: -0.159722222222223
	},
	{
		id: 998,
		time: 997,
		velocity: 14.4844444444444,
		power: 375.564718964157,
		road: 9322.45597222222,
		acceleration: -0.219074074074072
	},
	{
		id: 999,
		time: 998,
		velocity: 14.1933333333333,
		power: 443.759937789056,
		road: 9336.75319444444,
		acceleration: -0.210185185185187
	},
	{
		id: 1000,
		time: 999,
		velocity: 13.8986111111111,
		power: 452.285563970353,
		road: 9350.8424537037,
		acceleration: -0.20574074074074
	},
	{
		id: 1001,
		time: 1000,
		velocity: 13.8672222222222,
		power: 1254.15801754854,
		road: 9364.75773148148,
		acceleration: -0.142222222222221
	},
	{
		id: 1002,
		time: 1001,
		velocity: 13.7666666666667,
		power: 2226.93175318926,
		road: 9378.56884259259,
		acceleration: -0.0661111111111126
	},
	{
		id: 1003,
		time: 1002,
		velocity: 13.7002777777778,
		power: 1323.92349304565,
		road: 9392.28074074074,
		acceleration: -0.132314814814814
	},
	{
		id: 1004,
		time: 1003,
		velocity: 13.4702777777778,
		power: 3294.27792897299,
		road: 9405.93652777777,
		acceleration: 0.0200925925925937
	},
	{
		id: 1005,
		time: 1004,
		velocity: 13.8269444444444,
		power: 3926.47383064676,
		road: 9419.63583333333,
		acceleration: 0.0669444444444451
	},
	{
		id: 1006,
		time: 1005,
		velocity: 13.9011111111111,
		power: 4685.01261629967,
		road: 9433.42912037037,
		acceleration: 0.121018518518516
	},
	{
		id: 1007,
		time: 1006,
		velocity: 13.8333333333333,
		power: 2102.90618469192,
		road: 9447.24509259259,
		acceleration: -0.0756481481481472
	},
	{
		id: 1008,
		time: 1007,
		velocity: 13.6,
		power: 272.614094642453,
		road: 9460.91726851851,
		acceleration: -0.211944444444446
	},
	{
		id: 1009,
		time: 1008,
		velocity: 13.2652777777778,
		power: -3242.7207429978,
		road: 9474.24222222222,
		acceleration: -0.482499999999998
	},
	{
		id: 1010,
		time: 1009,
		velocity: 12.3858333333333,
		power: -9292.92046599813,
		road: 9486.83064814814,
		acceleration: -0.990555555555556
	},
	{
		id: 1011,
		time: 1010,
		velocity: 10.6283333333333,
		power: -10856.8407286632,
		road: 9498.32759259259,
		acceleration: -1.19240740740741
	},
	{
		id: 1012,
		time: 1011,
		velocity: 9.68805555555556,
		power: -14287.1457062449,
		road: 9508.39087962963,
		acceleration: -1.67490740740741
	},
	{
		id: 1013,
		time: 1012,
		velocity: 7.36111111111111,
		power: -11939.1183467089,
		road: 9516.78597222222,
		acceleration: -1.66148148148148
	},
	{
		id: 1014,
		time: 1013,
		velocity: 5.64388888888889,
		power: -11161.8974530344,
		road: 9523.38472222222,
		acceleration: -1.9312037037037
	},
	{
		id: 1015,
		time: 1014,
		velocity: 3.89444444444444,
		power: -5129.09114050862,
		road: 9528.40981481481,
		acceleration: -1.21611111111111
	},
	{
		id: 1016,
		time: 1015,
		velocity: 3.71277777777778,
		power: -2063.02427352245,
		road: 9532.49236111111,
		acceleration: -0.668981481481481
	},
	{
		id: 1017,
		time: 1016,
		velocity: 3.63694444444444,
		power: 453.880036401109,
		road: 9536.23662037037,
		acceleration: -0.0075925925925926
	},
	{
		id: 1018,
		time: 1017,
		velocity: 3.87166666666667,
		power: 926.463921679402,
		road: 9540.03768518518,
		acceleration: 0.121203703703704
	},
	{
		id: 1019,
		time: 1018,
		velocity: 4.07638888888889,
		power: 1038.54869156861,
		road: 9543.97037037037,
		acceleration: 0.142037037037037
	},
	{
		id: 1020,
		time: 1019,
		velocity: 4.06305555555556,
		power: 858.729095766528,
		road: 9548.0175,
		acceleration: 0.0868518518518515
	},
	{
		id: 1021,
		time: 1020,
		velocity: 4.13222222222222,
		power: 1768.21295208316,
		road: 9552.25879629629,
		acceleration: 0.301481481481481
	},
	{
		id: 1022,
		time: 1021,
		velocity: 4.98083333333333,
		power: 2819.57249830088,
		road: 9556.90078703703,
		acceleration: 0.499907407407408
	},
	{
		id: 1023,
		time: 1022,
		velocity: 5.56277777777778,
		power: 3778.1206051892,
		road: 9562.10351851852,
		acceleration: 0.621574074074075
	},
	{
		id: 1024,
		time: 1023,
		velocity: 5.99694444444444,
		power: 4320.99017780071,
		road: 9567.93361111111,
		acceleration: 0.633148148148146
	},
	{
		id: 1025,
		time: 1024,
		velocity: 6.88027777777778,
		power: 4550.16854759092,
		road: 9574.37625,
		acceleration: 0.591944444444445
	},
	{
		id: 1026,
		time: 1025,
		velocity: 7.33861111111111,
		power: 4127.60435991081,
		road: 9581.34865740741,
		acceleration: 0.467592592592593
	},
	{
		id: 1027,
		time: 1026,
		velocity: 7.39972222222222,
		power: 1162.12657910149,
		road: 9588.5612037037,
		acceleration: 0.0126851851851848
	},
	{
		id: 1028,
		time: 1027,
		velocity: 6.91833333333333,
		power: 697.699742707916,
		road: 9595.75282407407,
		acceleration: -0.0545370370370373
	},
	{
		id: 1029,
		time: 1028,
		velocity: 7.175,
		power: 1179.35142164602,
		road: 9602.92541666666,
		acceleration: 0.0164814814814811
	},
	{
		id: 1030,
		time: 1029,
		velocity: 7.44916666666667,
		power: 6006.64352365792,
		road: 9610.44629629629,
		acceleration: 0.680092592592594
	},
	{
		id: 1031,
		time: 1030,
		velocity: 8.95861111111111,
		power: 7828.07126808597,
		road: 9618.72111111111,
		acceleration: 0.827777777777778
	},
	{
		id: 1032,
		time: 1031,
		velocity: 9.65833333333333,
		power: 10397.9903818018,
		road: 9627.91592592592,
		acceleration: 1.01222222222222
	},
	{
		id: 1033,
		time: 1032,
		velocity: 10.4858333333333,
		power: 7820.15458023505,
		road: 9637.93444444444,
		acceleration: 0.635185185185184
	},
	{
		id: 1034,
		time: 1033,
		velocity: 10.8641666666667,
		power: 6870.2829394881,
		road: 9648.51587962963,
		acceleration: 0.49064814814815
	},
	{
		id: 1035,
		time: 1034,
		velocity: 11.1302777777778,
		power: 5743.28129668627,
		road: 9659.51856481481,
		acceleration: 0.351851851851849
	},
	{
		id: 1036,
		time: 1035,
		velocity: 11.5413888888889,
		power: 7172.366512302,
		road: 9670.92643518518,
		acceleration: 0.458518518518522
	},
	{
		id: 1037,
		time: 1036,
		velocity: 12.2397222222222,
		power: 6597.73970286484,
		road: 9682.75287037037,
		acceleration: 0.378611111111109
	},
	{
		id: 1038,
		time: 1037,
		velocity: 12.2661111111111,
		power: 5610.27535874195,
		road: 9694.90523148148,
		acceleration: 0.273240740740739
	},
	{
		id: 1039,
		time: 1038,
		velocity: 12.3611111111111,
		power: 4314.99160308548,
		road: 9707.27027777778,
		acceleration: 0.152129629629632
	},
	{
		id: 1040,
		time: 1039,
		velocity: 12.6961111111111,
		power: 4504.74869793998,
		road: 9719.79199074074,
		acceleration: 0.1612037037037
	},
	{
		id: 1041,
		time: 1040,
		velocity: 12.7497222222222,
		power: 5364.90842531901,
		road: 9732.50611111111,
		acceleration: 0.223611111111111
	},
	{
		id: 1042,
		time: 1041,
		velocity: 13.0319444444444,
		power: 5279.8005593063,
		road: 9745.43513888889,
		acceleration: 0.206203703703704
	},
	{
		id: 1043,
		time: 1042,
		velocity: 13.3147222222222,
		power: 6290.56620631979,
		road: 9758.60486111111,
		acceleration: 0.275185185185187
	},
	{
		id: 1044,
		time: 1043,
		velocity: 13.5752777777778,
		power: 6187.10515042558,
		road: 9772.0387962963,
		acceleration: 0.25324074074074
	},
	{
		id: 1045,
		time: 1044,
		velocity: 13.7916666666667,
		power: 5113.37108905533,
		road: 9785.67949074074,
		acceleration: 0.160277777777779
	},
	{
		id: 1046,
		time: 1045,
		velocity: 13.7955555555556,
		power: 5664.93500573005,
		road: 9799.4974537037,
		acceleration: 0.19425925925926
	},
	{
		id: 1047,
		time: 1046,
		velocity: 14.1580555555556,
		power: 5241.46406036951,
		road: 9813.48976851852,
		acceleration: 0.154444444444444
	},
	{
		id: 1048,
		time: 1047,
		velocity: 14.255,
		power: 6543.63670728821,
		road: 9827.68023148148,
		acceleration: 0.241851851851854
	},
	{
		id: 1049,
		time: 1048,
		velocity: 14.5211111111111,
		power: 5396.31905161296,
		road: 9842.06592592593,
		acceleration: 0.148611111111109
	},
	{
		id: 1050,
		time: 1049,
		velocity: 14.6038888888889,
		power: 5196.01491765488,
		road: 9856.59,
		acceleration: 0.128148148148149
	},
	{
		id: 1051,
		time: 1050,
		velocity: 14.6394444444444,
		power: 4299.82267106088,
		road: 9871.20814814815,
		acceleration: 0.0599999999999987
	},
	{
		id: 1052,
		time: 1051,
		velocity: 14.7011111111111,
		power: 4270.66093424762,
		road: 9885.88416666667,
		acceleration: 0.0557407407407418
	},
	{
		id: 1053,
		time: 1052,
		velocity: 14.7711111111111,
		power: 4774.5153095332,
		road: 9900.63245370371,
		acceleration: 0.0887962962962963
	},
	{
		id: 1054,
		time: 1053,
		velocity: 14.9058333333333,
		power: 4333.72971464109,
		road: 9915.4525462963,
		acceleration: 0.0548148148148133
	},
	{
		id: 1055,
		time: 1054,
		velocity: 14.8655555555556,
		power: 4440.70078718375,
		road: 9930.33013888889,
		acceleration: 0.0601851851851869
	},
	{
		id: 1056,
		time: 1055,
		velocity: 14.9516666666667,
		power: 3282.05180279568,
		road: 9945.22685185185,
		acceleration: -0.021944444444447
	},
	{
		id: 1057,
		time: 1056,
		velocity: 14.84,
		power: 3358.88199882769,
		road: 9960.10462962963,
		acceleration: -0.0159259259259237
	},
	{
		id: 1058,
		time: 1057,
		velocity: 14.8177777777778,
		power: 2906.94354121701,
		road: 9974.95106481482,
		acceleration: -0.0467592592592592
	},
	{
		id: 1059,
		time: 1058,
		velocity: 14.8113888888889,
		power: 1945.03003470638,
		road: 9989.71787037037,
		acceleration: -0.112499999999999
	},
	{
		id: 1060,
		time: 1059,
		velocity: 14.5025,
		power: 997.404336658522,
		road: 10004.3401388889,
		acceleration: -0.176574074074075
	},
	{
		id: 1061,
		time: 1060,
		velocity: 14.2880555555556,
		power: -268.372902379172,
		road: 10018.7421759259,
		acceleration: -0.263888888888889
	},
	{
		id: 1062,
		time: 1061,
		velocity: 14.0197222222222,
		power: 694.256435685318,
		road: 10032.9175462963,
		acceleration: -0.189444444444444
	},
	{
		id: 1063,
		time: 1062,
		velocity: 13.9341666666667,
		power: 1181.12274063468,
		road: 10046.9233333333,
		acceleration: -0.149722222222223
	},
	{
		id: 1064,
		time: 1063,
		velocity: 13.8388888888889,
		power: 2017.44747949738,
		road: 10060.8122222222,
		acceleration: -0.0840740740740724
	},
	{
		id: 1065,
		time: 1064,
		velocity: 13.7675,
		power: 1089.73059851633,
		road: 10074.5833333333,
		acceleration: -0.151481481481483
	},
	{
		id: 1066,
		time: 1065,
		velocity: 13.4797222222222,
		power: 2585.83527629225,
		road: 10088.26125,
		acceleration: -0.0349074074074078
	},
	{
		id: 1067,
		time: 1066,
		velocity: 13.7341666666667,
		power: 3453.29596775754,
		road: 10101.9375,
		acceleration: 0.0315740740740758
	},
	{
		id: 1068,
		time: 1067,
		velocity: 13.8622222222222,
		power: 5704.54352625655,
		road: 10115.7287962963,
		acceleration: 0.198518518518519
	},
	{
		id: 1069,
		time: 1068,
		velocity: 14.0752777777778,
		power: 5565.63665831947,
		road: 10129.7089814815,
		acceleration: 0.179259259259258
	},
	{
		id: 1070,
		time: 1069,
		velocity: 14.2719444444444,
		power: 5037.89843178501,
		road: 10143.8453240741,
		acceleration: 0.133055555555556
	},
	{
		id: 1071,
		time: 1070,
		velocity: 14.2613888888889,
		power: 2805.82190924708,
		road: 10158.0312962963,
		acceleration: -0.0337962962962965
	},
	{
		id: 1072,
		time: 1071,
		velocity: 13.9738888888889,
		power: 1013.52428751774,
		road: 10172.1183796296,
		acceleration: -0.163981481481482
	},
	{
		id: 1073,
		time: 1072,
		velocity: 13.78,
		power: 456.671473095329,
		road: 10186.0224537037,
		acceleration: -0.202037037037037
	},
	{
		id: 1074,
		time: 1073,
		velocity: 13.6552777777778,
		power: 2448.99618781019,
		road: 10199.8013425926,
		acceleration: -0.048333333333332
	},
	{
		id: 1075,
		time: 1074,
		velocity: 13.8288888888889,
		power: 3105.42309439847,
		road: 10213.5572222222,
		acceleration: 0.00231481481481488
	},
	{
		id: 1076,
		time: 1075,
		velocity: 13.7869444444444,
		power: 3759.13779237587,
		road: 10227.3398148148,
		acceleration: 0.051111111111112
	},
	{
		id: 1077,
		time: 1076,
		velocity: 13.8086111111111,
		power: 3923.23754249007,
		road: 10241.1787037037,
		acceleration: 0.0614814814814793
	},
	{
		id: 1078,
		time: 1077,
		velocity: 14.0133333333333,
		power: 4882.31496253846,
		road: 10255.1133333333,
		acceleration: 0.130000000000003
	},
	{
		id: 1079,
		time: 1078,
		velocity: 14.1769444444444,
		power: 5845.58404889844,
		road: 10269.2103240741,
		acceleration: 0.19472222222222
	},
	{
		id: 1080,
		time: 1079,
		velocity: 14.3927777777778,
		power: 6343.73312146282,
		road: 10283.5154166667,
		acceleration: 0.221481481481483
	},
	{
		id: 1081,
		time: 1080,
		velocity: 14.6777777777778,
		power: 8315.895224845,
		road: 10298.1058333333,
		acceleration: 0.349166666666667
	},
	{
		id: 1082,
		time: 1081,
		velocity: 15.2244444444444,
		power: 9069.16205247044,
		road: 10313.0613888889,
		acceleration: 0.38111111111111
	},
	{
		id: 1083,
		time: 1082,
		velocity: 15.5361111111111,
		power: 9534.60729425011,
		road: 10328.4025925926,
		acceleration: 0.390185185185183
	},
	{
		id: 1084,
		time: 1083,
		velocity: 15.8483333333333,
		power: 6932.57011536211,
		road: 10344.0381944444,
		acceleration: 0.198611111111113
	},
	{
		id: 1085,
		time: 1084,
		velocity: 15.8202777777778,
		power: 5942.23353682102,
		road: 10359.835787037,
		acceleration: 0.125370370370369
	},
	{
		id: 1086,
		time: 1085,
		velocity: 15.9122222222222,
		power: 5170.11467780903,
		road: 10375.73125,
		acceleration: 0.0703703703703695
	},
	{
		id: 1087,
		time: 1086,
		velocity: 16.0594444444444,
		power: 5577.39981408679,
		road: 10391.7087962963,
		acceleration: 0.0937962962962988
	},
	{
		id: 1088,
		time: 1087,
		velocity: 16.1016666666667,
		power: 6081.16587450165,
		road: 10407.7943055556,
		acceleration: 0.122129629629629
	},
	{
		id: 1089,
		time: 1088,
		velocity: 16.2786111111111,
		power: 9128.47846995061,
		road: 10424.0950925926,
		acceleration: 0.308425925925928
	},
	{
		id: 1090,
		time: 1089,
		velocity: 16.9847222222222,
		power: 9237.97621094313,
		road: 10440.699537037,
		acceleration: 0.298888888888889
	},
	{
		id: 1091,
		time: 1090,
		velocity: 16.9983333333333,
		power: 8816.59642303007,
		road: 10457.5823611111,
		acceleration: 0.25787037037037
	},
	{
		id: 1092,
		time: 1091,
		velocity: 17.0522222222222,
		power: 2621.84245834934,
		road: 10474.5303240741,
		acceleration: -0.127592592592592
	},
	{
		id: 1093,
		time: 1092,
		velocity: 16.6019444444444,
		power: 862.523751573506,
		road: 10491.2984259259,
		acceleration: -0.232129629629632
	},
	{
		id: 1094,
		time: 1093,
		velocity: 16.3019444444444,
		power: -1400.98159972878,
		road: 10507.7658333333,
		acceleration: -0.369259259259259
	},
	{
		id: 1095,
		time: 1094,
		velocity: 15.9444444444444,
		power: -951.064163463405,
		road: 10523.8808333333,
		acceleration: -0.335555555555555
	},
	{
		id: 1096,
		time: 1095,
		velocity: 15.5952777777778,
		power: -1955.05662621841,
		road: 10539.6294907407,
		acceleration: -0.397129629629632
	},
	{
		id: 1097,
		time: 1096,
		velocity: 15.1105555555556,
		power: -2016.91990816922,
		road: 10554.9806944444,
		acceleration: -0.397777777777776
	},
	{
		id: 1098,
		time: 1097,
		velocity: 14.7511111111111,
		power: -1972.26761008411,
		road: 10569.9372222222,
		acceleration: -0.391574074074073
	},
	{
		id: 1099,
		time: 1098,
		velocity: 14.4205555555556,
		power: -846.974772728265,
		road: 10584.54375,
		acceleration: -0.308425925925928
	},
	{
		id: 1100,
		time: 1099,
		velocity: 14.1852777777778,
		power: -676.156913589736,
		road: 10598.8499074074,
		acceleration: -0.292314814814812
	},
	{
		id: 1101,
		time: 1100,
		velocity: 13.8741666666667,
		power: -791.965210595667,
		road: 10612.86125,
		acceleration: -0.297314814814817
	},
	{
		id: 1102,
		time: 1101,
		velocity: 13.5286111111111,
		power: -1193.85630203993,
		road: 10626.5616666667,
		acceleration: -0.324537037037038
	},
	{
		id: 1103,
		time: 1102,
		velocity: 13.2116666666667,
		power: -1637.58009444514,
		road: 10639.9215740741,
		acceleration: -0.356481481481481
	},
	{
		id: 1104,
		time: 1103,
		velocity: 12.8047222222222,
		power: -1297.34377952166,
		road: 10652.939537037,
		acceleration: -0.327407407407408
	},
	{
		id: 1105,
		time: 1104,
		velocity: 12.5463888888889,
		power: -1046.24466764133,
		road: 10665.6414351852,
		acceleration: -0.304722222222223
	},
	{
		id: 1106,
		time: 1105,
		velocity: 12.2975,
		power: -1207.07369809941,
		road: 10678.0329166667,
		acceleration: -0.316111111111113
	},
	{
		id: 1107,
		time: 1106,
		velocity: 11.8563888888889,
		power: -1972.5878764059,
		road: 10690.0758796296,
		acceleration: -0.380925925925924
	},
	{
		id: 1108,
		time: 1107,
		velocity: 11.4036111111111,
		power: -959.566908212846,
		road: 10701.7830555556,
		acceleration: -0.290648148148149
	},
	{
		id: 1109,
		time: 1108,
		velocity: 11.4255555555556,
		power: 35.6794488583943,
		road: 10713.245787037,
		acceleration: -0.19824074074074
	},
	{
		id: 1110,
		time: 1109,
		velocity: 11.2616666666667,
		power: 492.10404674155,
		road: 10724.5326388889,
		acceleration: -0.153518518518517
	},
	{
		id: 1111,
		time: 1110,
		velocity: 10.9430555555556,
		power: -33.6134764401316,
		road: 10735.6426388889,
		acceleration: -0.200185185185187
	},
	{
		id: 1112,
		time: 1111,
		velocity: 10.825,
		power: -567.213530045675,
		road: 10746.5281018519,
		acceleration: -0.248888888888889
	},
	{
		id: 1113,
		time: 1112,
		velocity: 10.515,
		power: -641.306087444361,
		road: 10757.1618981482,
		acceleration: -0.254444444444445
	},
	{
		id: 1114,
		time: 1113,
		velocity: 10.1797222222222,
		power: -1396.77169186768,
		road: 10767.50375,
		acceleration: -0.329444444444444
	},
	{
		id: 1115,
		time: 1114,
		velocity: 9.83666666666667,
		power: -1435.73858170576,
		road: 10777.5136574074,
		acceleration: -0.334444444444443
	},
	{
		id: 1116,
		time: 1115,
		velocity: 9.51166666666666,
		power: -1727.64541909599,
		road: 10787.1724537037,
		acceleration: -0.367777777777778
	},
	{
		id: 1117,
		time: 1116,
		velocity: 9.07638888888889,
		power: -1907.96576002291,
		road: 10796.4514351852,
		acceleration: -0.391851851851852
	},
	{
		id: 1118,
		time: 1117,
		velocity: 8.66111111111111,
		power: -1062.70206267113,
		road: 10805.3858333333,
		acceleration: -0.297314814814813
	},
	{
		id: 1119,
		time: 1118,
		velocity: 8.61972222222222,
		power: 57.6078996388517,
		road: 10814.09,
		acceleration: -0.163148148148148
	},
	{
		id: 1120,
		time: 1119,
		velocity: 8.58694444444444,
		power: 2830.08933349689,
		road: 10822.7981944445,
		acceleration: 0.171203703703705
	},
	{
		id: 1121,
		time: 1120,
		velocity: 9.17472222222222,
		power: 6642.10629036214,
		road: 10831.8886111111,
		acceleration: 0.59324074074074
	},
	{
		id: 1122,
		time: 1121,
		velocity: 10.3994444444444,
		power: 14341.8965178735,
		road: 10841.9326851852,
		acceleration: 1.31407407407407
	},
	{
		id: 1123,
		time: 1122,
		velocity: 12.5291666666667,
		power: 20252.443839074,
		road: 10853.4541203704,
		acceleration: 1.64064814814815
	},
	{
		id: 1124,
		time: 1123,
		velocity: 14.0966666666667,
		power: 25601.6526772963,
		road: 10866.6952777778,
		acceleration: 1.7987962962963
	},
	{
		id: 1125,
		time: 1124,
		velocity: 15.7958333333333,
		power: 30886.2908813347,
		road: 10881.7791666667,
		acceleration: 1.88666666666666
	},
	{
		id: 1126,
		time: 1125,
		velocity: 18.1891666666667,
		power: 33212.5845289652,
		road: 10898.6885648148,
		acceleration: 1.76435185185185
	},
	{
		id: 1127,
		time: 1126,
		velocity: 19.3897222222222,
		power: 32737.6902351467,
		road: 10917.2408333333,
		acceleration: 1.52138888888889
	},
	{
		id: 1128,
		time: 1127,
		velocity: 20.36,
		power: 20938.401966285,
		road: 10936.9356481482,
		acceleration: 0.763703703703705
	},
	{
		id: 1129,
		time: 1128,
		velocity: 20.4802777777778,
		power: 12727.5733147254,
		road: 10957.161712963,
		acceleration: 0.298796296296292
	},
	{
		id: 1130,
		time: 1129,
		velocity: 20.2861111111111,
		power: 5102.88381648599,
		road: 10977.4881018519,
		acceleration: -0.0981481481481445
	},
	{
		id: 1131,
		time: 1130,
		velocity: 20.0655555555556,
		power: 3193.7391090613,
		road: 10997.6695833333,
		acceleration: -0.191666666666666
	},
	{
		id: 1132,
		time: 1131,
		velocity: 19.9052777777778,
		power: 2947.03073496884,
		road: 11017.6559722222,
		acceleration: -0.198518518518519
	},
	{
		id: 1133,
		time: 1132,
		velocity: 19.6905555555556,
		power: 3109.60803818924,
		road: 11037.4510185185,
		acceleration: -0.184166666666663
	},
	{
		id: 1134,
		time: 1133,
		velocity: 19.5130555555556,
		power: 2654.30929331857,
		road: 11057.0527314815,
		acceleration: -0.202500000000004
	},
	{
		id: 1135,
		time: 1134,
		velocity: 19.2977777777778,
		power: 2324.29545844073,
		road: 11076.4461111111,
		acceleration: -0.214166666666667
	},
	{
		id: 1136,
		time: 1135,
		velocity: 19.0480555555556,
		power: 1771.89857773813,
		road: 11095.6134722222,
		acceleration: -0.237870370370374
	},
	{
		id: 1137,
		time: 1136,
		velocity: 18.7994444444444,
		power: 1210.47159083696,
		road: 11114.530787037,
		acceleration: -0.262222222222221
	},
	{
		id: 1138,
		time: 1137,
		velocity: 18.5111111111111,
		power: 449.129225291095,
		road: 11133.1680092593,
		acceleration: -0.297962962962963
	},
	{
		id: 1139,
		time: 1138,
		velocity: 18.1541666666667,
		power: 1112.91583735582,
		road: 11151.529212963,
		acceleration: -0.254074074074072
	},
	{
		id: 1140,
		time: 1139,
		velocity: 18.0372222222222,
		power: 711.706428995176,
		road: 11169.6279166667,
		acceleration: -0.270925925925926
	},
	{
		id: 1141,
		time: 1140,
		velocity: 17.6983333333333,
		power: 2327.75109102046,
		road: 11187.5054166667,
		acceleration: -0.171481481481479
	},
	{
		id: 1142,
		time: 1141,
		velocity: 17.6397222222222,
		power: 1817.95405492943,
		road: 11205.1989351852,
		acceleration: -0.196481481481484
	},
	{
		id: 1143,
		time: 1142,
		velocity: 17.4477777777778,
		power: 2680.71222937169,
		road: 11222.7238888889,
		acceleration: -0.140648148148152
	},
	{
		id: 1144,
		time: 1143,
		velocity: 17.2763888888889,
		power: 1507.60074513128,
		road: 11240.0753703704,
		acceleration: -0.206296296296294
	},
	{
		id: 1145,
		time: 1144,
		velocity: 17.0208333333333,
		power: 2034.76125162353,
		road: 11257.2389351852,
		acceleration: -0.169537037037038
	},
	{
		id: 1146,
		time: 1145,
		velocity: 16.9391666666667,
		power: 3705.41309907286,
		road: 11274.2857407407,
		acceleration: -0.0639814814814805
	},
	{
		id: 1147,
		time: 1146,
		velocity: 17.0844444444444,
		power: 5202.82266043579,
		road: 11291.3148148148,
		acceleration: 0.0285185185185206
	},
	{
		id: 1148,
		time: 1147,
		velocity: 17.1063888888889,
		power: 6869.78442041389,
		road: 11308.4218518519,
		acceleration: 0.127407407407407
	},
	{
		id: 1149,
		time: 1148,
		velocity: 17.3213888888889,
		power: 6003.94636616671,
		road: 11325.6278240741,
		acceleration: 0.0704629629629636
	},
	{
		id: 1150,
		time: 1149,
		velocity: 17.2958333333333,
		power: 6601.63154267649,
		road: 11342.9205555556,
		acceleration: 0.103055555555557
	},
	{
		id: 1151,
		time: 1150,
		velocity: 17.4155555555556,
		power: 6613.44543728984,
		road: 11360.314537037,
		acceleration: 0.099444444444444
	},
	{
		id: 1152,
		time: 1151,
		velocity: 17.6197222222222,
		power: 7630.88520775642,
		road: 11377.8356018519,
		acceleration: 0.154722222222219
	},
	{
		id: 1153,
		time: 1152,
		velocity: 17.76,
		power: 7259.23118830906,
		road: 11395.4971759259,
		acceleration: 0.126296296296299
	},
	{
		id: 1154,
		time: 1153,
		velocity: 17.7944444444444,
		power: 6008.74562410736,
		road: 11413.2462962963,
		acceleration: 0.0487962962962953
	},
	{
		id: 1155,
		time: 1154,
		velocity: 17.7661111111111,
		power: 4820.17843176453,
		road: 11431.0089814815,
		acceleration: -0.0216666666666683
	},
	{
		id: 1156,
		time: 1155,
		velocity: 17.695,
		power: 3971.04356091077,
		road: 11448.725787037,
		acceleration: -0.0700925925925873
	},
	{
		id: 1157,
		time: 1156,
		velocity: 17.5841666666667,
		power: 4493.25986994693,
		road: 11466.3888425926,
		acceleration: -0.0374074074074109
	},
	{
		id: 1158,
		time: 1157,
		velocity: 17.6538888888889,
		power: 2117.54782468303,
		road: 11483.9456481482,
		acceleration: -0.175092592592591
	},
	{
		id: 1159,
		time: 1158,
		velocity: 17.1697222222222,
		power: 1392.17109366505,
		road: 11501.3081481482,
		acceleration: -0.213518518518523
	},
	{
		id: 1160,
		time: 1159,
		velocity: 16.9436111111111,
		power: 125.727155079324,
		road: 11518.4214814815,
		acceleration: -0.284814814814812
	},
	{
		id: 1161,
		time: 1160,
		velocity: 16.7994444444444,
		power: 3203.48402647999,
		road: 11535.3468981482,
		acceleration: -0.0910185185185206
	},
	{
		id: 1162,
		time: 1161,
		velocity: 16.8966666666667,
		power: 3714.3837144553,
		road: 11552.198287037,
		acceleration: -0.0570370370370341
	},
	{
		id: 1163,
		time: 1162,
		velocity: 16.7725,
		power: 4376.05073553076,
		road: 11569.0137962963,
		acceleration: -0.0147222222222219
	},
	{
		id: 1164,
		time: 1163,
		velocity: 16.7552777777778,
		power: 3527.14210434971,
		road: 11585.7888425926,
		acceleration: -0.0662037037037031
	},
	{
		id: 1165,
		time: 1164,
		velocity: 16.6980555555556,
		power: 4194.64396394424,
		road: 11602.5192592593,
		acceleration: -0.0230555555555583
	},
	{
		id: 1166,
		time: 1165,
		velocity: 16.7033333333333,
		power: 5169.58366997295,
		road: 11619.2569444444,
		acceleration: 0.0375925925925955
	},
	{
		id: 1167,
		time: 1166,
		velocity: 16.8680555555556,
		power: 5717.08768231084,
		road: 11636.0482407407,
		acceleration: 0.069629629629631
	},
	{
		id: 1168,
		time: 1167,
		velocity: 16.9069444444444,
		power: 5878.0970248723,
		road: 11652.9126851852,
		acceleration: 0.0766666666666644
	},
	{
		id: 1169,
		time: 1168,
		velocity: 16.9333333333333,
		power: 5271.0809161382,
		road: 11669.8338888889,
		acceleration: 0.0368518518518535
	},
	{
		id: 1170,
		time: 1169,
		velocity: 16.9786111111111,
		power: 5416.56938755927,
		road: 11686.7956481482,
		acceleration: 0.0442592592592561
	},
	{
		id: 1171,
		time: 1170,
		velocity: 17.0397222222222,
		power: 5578.23759067627,
		road: 11703.8056944444,
		acceleration: 0.0523148148148174
	},
	{
		id: 1172,
		time: 1171,
		velocity: 17.0902777777778,
		power: 6212.10955064109,
		road: 11720.8860648148,
		acceleration: 0.0883333333333347
	},
	{
		id: 1173,
		time: 1172,
		velocity: 17.2436111111111,
		power: 7478.34553498486,
		road: 11738.0906481482,
		acceleration: 0.160092592592591
	},
	{
		id: 1174,
		time: 1173,
		velocity: 17.52,
		power: 8468.86498384061,
		road: 11755.4808333333,
		acceleration: 0.211111111111109
	},
	{
		id: 1175,
		time: 1174,
		velocity: 17.7236111111111,
		power: 8610.72630838716,
		road: 11773.0812037037,
		acceleration: 0.209259259259259
	},
	{
		id: 1176,
		time: 1175,
		velocity: 17.8713888888889,
		power: 8021.19704240437,
		road: 11790.8689351852,
		acceleration: 0.165462962962966
	},
	{
		id: 1177,
		time: 1176,
		velocity: 18.0163888888889,
		power: 6691.56199988629,
		road: 11808.780462963,
		acceleration: 0.0821296296296268
	},
	{
		id: 1178,
		time: 1177,
		velocity: 17.97,
		power: 2976.71718046011,
		road: 11826.6661574074,
		acceleration: -0.133796296296296
	},
	{
		id: 1179,
		time: 1178,
		velocity: 17.47,
		power: 2331.3531323193,
		road: 11844.4012962963,
		acceleration: -0.167314814814816
	},
	{
		id: 1180,
		time: 1179,
		velocity: 17.5144444444444,
		power: 1533.09605784842,
		road: 11861.947962963,
		acceleration: -0.209629629629628
	},
	{
		id: 1181,
		time: 1180,
		velocity: 17.3411111111111,
		power: 5796.30466117174,
		road: 11879.4135648148,
		acceleration: 0.047500000000003
	},
	{
		id: 1182,
		time: 1181,
		velocity: 17.6125,
		power: 5268.20393668059,
		road: 11896.9102777778,
		acceleration: 0.0147222222222183
	},
	{
		id: 1183,
		time: 1182,
		velocity: 17.5586111111111,
		power: 7040.70550424763,
		road: 11914.4731944444,
		acceleration: 0.117685185185188
	},
	{
		id: 1184,
		time: 1183,
		velocity: 17.6941666666667,
		power: 3646.37629556053,
		road: 11932.0525462963,
		acceleration: -0.0848148148148162
	},
	{
		id: 1185,
		time: 1184,
		velocity: 17.3580555555556,
		power: 1107.29571708821,
		road: 11949.4734722222,
		acceleration: -0.232037037037035
	},
	{
		id: 1186,
		time: 1185,
		velocity: 16.8625,
		power: -1601.49385107632,
		road: 11966.5832407407,
		acceleration: -0.390277777777779
	},
	{
		id: 1187,
		time: 1186,
		velocity: 16.5233333333333,
		power: -96.5624662363953,
		road: 11983.3518981482,
		acceleration: -0.291944444444443
	},
	{
		id: 1188,
		time: 1187,
		velocity: 16.4822222222222,
		power: 2520.74760592519,
		road: 11999.9131481482,
		acceleration: -0.122870370370372
	},
	{
		id: 1189,
		time: 1188,
		velocity: 16.4938888888889,
		power: 3671.71348975377,
		road: 12016.3892592593,
		acceleration: -0.0474074074074089
	},
	{
		id: 1190,
		time: 1189,
		velocity: 16.3811111111111,
		power: 5749.2434569111,
		road: 12032.8835185185,
		acceleration: 0.0837037037037049
	},
	{
		id: 1191,
		time: 1190,
		velocity: 16.7333333333333,
		power: 5316.95444119457,
		road: 12049.4464351852,
		acceleration: 0.0536111111111097
	},
	{
		id: 1192,
		time: 1191,
		velocity: 16.6547222222222,
		power: 6530.56905665988,
		road: 12066.0993055556,
		acceleration: 0.126296296296299
	},
	{
		id: 1193,
		time: 1192,
		velocity: 16.76,
		power: 4672.84978205775,
		road: 12082.8189351852,
		acceleration: 0.00722222222221802
	},
	{
		id: 1194,
		time: 1193,
		velocity: 16.755,
		power: 5305.26405591581,
		road: 12099.5650462963,
		acceleration: 0.0457407407407402
	},
	{
		id: 1195,
		time: 1194,
		velocity: 16.7919444444444,
		power: 3454.30567435989,
		road: 12116.2993055556,
		acceleration: -0.0694444444444429
	},
	{
		id: 1196,
		time: 1195,
		velocity: 16.5516666666667,
		power: 3011.56496168986,
		road: 12132.9515277778,
		acceleration: -0.0946296296296296
	},
	{
		id: 1197,
		time: 1196,
		velocity: 16.4711111111111,
		power: 3106.21140241936,
		road: 12149.5134722222,
		acceleration: -0.0859259259259275
	},
	{
		id: 1198,
		time: 1197,
		velocity: 16.5341666666667,
		power: 4355.02522586584,
		road: 12166.0297222222,
		acceleration: -0.00546296296295878
	},
	{
		id: 1199,
		time: 1198,
		velocity: 16.5352777777778,
		power: 5734.89036745416,
		road: 12182.5834259259,
		acceleration: 0.0803703703703675
	},
	{
		id: 1200,
		time: 1199,
		velocity: 16.7122222222222,
		power: 6632.96311343144,
		road: 12199.2435185185,
		acceleration: 0.13240740740741
	},
	{
		id: 1201,
		time: 1200,
		velocity: 16.9313888888889,
		power: 7222.19602232254,
		road: 12216.0511111111,
		acceleration: 0.162592592592592
	},
	{
		id: 1202,
		time: 1201,
		velocity: 17.0230555555556,
		power: 6339.02689323396,
		road: 12232.9910185185,
		acceleration: 0.102037037037036
	},
	{
		id: 1203,
		time: 1202,
		velocity: 17.0183333333333,
		power: 5290.86945619678,
		road: 12249.9993055556,
		acceleration: 0.034722222222225
	},
	{
		id: 1204,
		time: 1203,
		velocity: 17.0355555555556,
		power: 6021.29882830284,
		road: 12267.0636111111,
		acceleration: 0.0773148148148124
	},
	{
		id: 1205,
		time: 1204,
		velocity: 17.255,
		power: 6180.19236929404,
		road: 12284.2084259259,
		acceleration: 0.0837037037037049
	},
	{
		id: 1206,
		time: 1205,
		velocity: 17.2694444444444,
		power: 5818.55813502154,
		road: 12301.4244907407,
		acceleration: 0.0587962962962933
	},
	{
		id: 1207,
		time: 1206,
		velocity: 17.2119444444444,
		power: 3443.29173622731,
		road: 12318.6274537037,
		acceleration: -0.0849999999999973
	},
	{
		id: 1208,
		time: 1207,
		velocity: 17,
		power: 2151.41301513756,
		road: 12335.7078240741,
		acceleration: -0.160185185185185
	},
	{
		id: 1209,
		time: 1208,
		velocity: 16.7888888888889,
		power: 1047.74358670318,
		road: 12352.5964351852,
		acceleration: -0.223333333333336
	},
	{
		id: 1210,
		time: 1209,
		velocity: 16.5419444444444,
		power: 2152.57672805622,
		road: 12369.2984259259,
		acceleration: -0.149907407407404
	},
	{
		id: 1211,
		time: 1210,
		velocity: 16.5502777777778,
		power: 2591.1428573703,
		road: 12385.8661574074,
		acceleration: -0.118611111111115
	},
	{
		id: 1212,
		time: 1211,
		velocity: 16.4330555555556,
		power: 3004.94820834194,
		road: 12402.3299074074,
		acceleration: -0.0893518518518484
	},
	{
		id: 1213,
		time: 1212,
		velocity: 16.2738888888889,
		power: 2574.41766458763,
		road: 12418.6920833333,
		acceleration: -0.1137962962963
	},
	{
		id: 1214,
		time: 1213,
		velocity: 16.2088888888889,
		power: 4349.73729360111,
		road: 12434.9981944444,
		acceleration: 0.0016666666666687
	},
	{
		id: 1215,
		time: 1214,
		velocity: 16.4380555555556,
		power: 3158.66681655555,
		road: 12451.2683333333,
		acceleration: -0.0736111111111128
	},
	{
		id: 1216,
		time: 1215,
		velocity: 16.0530555555556,
		power: 4525.04057755815,
		road: 12467.5093055556,
		acceleration: 0.0152777777777828
	},
	{
		id: 1217,
		time: 1216,
		velocity: 16.2547222222222,
		power: 4138.30887262922,
		road: 12483.7530555556,
		acceleration: -0.00972222222222285
	},
	{
		id: 1218,
		time: 1217,
		velocity: 16.4088888888889,
		power: 6163.48294040325,
		road: 12500.0511111111,
		acceleration: 0.118333333333332
	},
	{
		id: 1219,
		time: 1218,
		velocity: 16.4080555555556,
		power: 4867.08764203618,
		road: 12516.4244907407,
		acceleration: 0.0323148148148142
	},
	{
		id: 1220,
		time: 1219,
		velocity: 16.3516666666667,
		power: 5628.65871211156,
		road: 12532.8533796296,
		acceleration: 0.0787037037037059
	},
	{
		id: 1221,
		time: 1220,
		velocity: 16.645,
		power: 6684.54460342891,
		road: 12549.3921296296,
		acceleration: 0.141018518518514
	},
	{
		id: 1222,
		time: 1221,
		velocity: 16.8311111111111,
		power: 6997.13967647739,
		road: 12566.0784259259,
		acceleration: 0.154074074074074
	},
	{
		id: 1223,
		time: 1222,
		velocity: 16.8138888888889,
		power: 4265.38288594171,
		road: 12582.832037037,
		acceleration: -0.0194444444444422
	},
	{
		id: 1224,
		time: 1223,
		velocity: 16.5866666666667,
		power: 1227.42836253971,
		road: 12599.4727314815,
		acceleration: -0.206388888888888
	},
	{
		id: 1225,
		time: 1224,
		velocity: 16.2119444444444,
		power: -753.117306941127,
		road: 12615.8468981482,
		acceleration: -0.326666666666668
	},
	{
		id: 1226,
		time: 1225,
		velocity: 15.8338888888889,
		power: -462.840947853398,
		road: 12631.90625,
		acceleration: -0.302962962962964
	},
	{
		id: 1227,
		time: 1226,
		velocity: 15.6777777777778,
		power: -684.685867822749,
		road: 12647.6577314815,
		acceleration: -0.312777777777777
	},
	{
		id: 1228,
		time: 1227,
		velocity: 15.2736111111111,
		power: -559.348372484245,
		road: 12663.1029166667,
		acceleration: -0.299814814814816
	},
	{
		id: 1229,
		time: 1228,
		velocity: 14.9344444444444,
		power: -615.0623939545,
		road: 12678.2485648148,
		acceleration: -0.299259259259259
	},
	{
		id: 1230,
		time: 1229,
		velocity: 14.78,
		power: 1151.36388582066,
		road: 12693.1586111111,
		acceleration: -0.171944444444444
	},
	{
		id: 1231,
		time: 1230,
		velocity: 14.7577777777778,
		power: 3113.21533257322,
		road: 12707.9671759259,
		acceleration: -0.0310185185185183
	},
	{
		id: 1232,
		time: 1231,
		velocity: 14.8413888888889,
		power: 3361.10229816965,
		road: 12722.7538425926,
		acceleration: -0.012777777777778
	},
	{
		id: 1233,
		time: 1232,
		velocity: 14.7416666666667,
		power: 3397.68289892749,
		road: 12737.529212963,
		acceleration: -0.00981481481481694
	},
	{
		id: 1234,
		time: 1233,
		velocity: 14.7283333333333,
		power: 3313.45129920745,
		road: 12752.2919907407,
		acceleration: -0.0153703703703698
	},
	{
		id: 1235,
		time: 1234,
		velocity: 14.7952777777778,
		power: 4376.4138185158,
		road: 12767.0766666667,
		acceleration: 0.0591666666666679
	},
	{
		id: 1236,
		time: 1235,
		velocity: 14.9191666666667,
		power: 5817.64914314803,
		road: 12781.9691203704,
		acceleration: 0.156388888888889
	},
	{
		id: 1237,
		time: 1236,
		velocity: 15.1975,
		power: 5752.55604564608,
		road: 12797.0123611111,
		acceleration: 0.145185185185186
	},
	{
		id: 1238,
		time: 1237,
		velocity: 15.2308333333333,
		power: 4915.18224517049,
		road: 12812.1693981482,
		acceleration: 0.0824074074074037
	},
	{
		id: 1239,
		time: 1238,
		velocity: 15.1663888888889,
		power: 3274.71882050071,
		road: 12827.3518055556,
		acceleration: -0.0316666666666663
	},
	{
		id: 1240,
		time: 1239,
		velocity: 15.1025,
		power: 2291.00773144712,
		road: 12842.469537037,
		acceleration: -0.0976851851851848
	},
	{
		id: 1241,
		time: 1240,
		velocity: 14.9377777777778,
		power: -1162.1364413879,
		road: 12857.3712962963,
		acceleration: -0.334259259259259
	},
	{
		id: 1242,
		time: 1241,
		velocity: 14.1636111111111,
		power: -2745.72385043395,
		road: 12871.8837962963,
		acceleration: -0.44425925925926
	},
	{
		id: 1243,
		time: 1242,
		velocity: 13.7697222222222,
		power: -5690.56662557442,
		road: 12885.842037037,
		acceleration: -0.664259259259259
	},
	{
		id: 1244,
		time: 1243,
		velocity: 12.945,
		power: -9129.5262749735,
		road: 12898.9919444445,
		acceleration: -0.952407407407406
	},
	{
		id: 1245,
		time: 1244,
		velocity: 11.3063888888889,
		power: -16679.3800434435,
		road: 12910.8232407407,
		acceleration: -1.68481481481481
	},
	{
		id: 1246,
		time: 1245,
		velocity: 8.71527777777778,
		power: -13528.5997839728,
		road: 12921.022962963,
		acceleration: -1.57833333333333
	},
	{
		id: 1247,
		time: 1246,
		velocity: 8.21,
		power: -8538.12400676845,
		road: 12929.8392592593,
		acceleration: -1.18851851851852
	},
	{
		id: 1248,
		time: 1247,
		velocity: 7.74083333333333,
		power: -1232.07079013218,
		road: 12937.8989814815,
		acceleration: -0.324629629629631
	},
	{
		id: 1249,
		time: 1248,
		velocity: 7.74138888888889,
		power: -19.6053174969221,
		road: 12945.7141666667,
		acceleration: -0.164444444444445
	},
	{
		id: 1250,
		time: 1249,
		velocity: 7.71666666666667,
		power: 1048.57246776019,
		road: 12953.437962963,
		acceleration: -0.0183333333333326
	},
	{
		id: 1251,
		time: 1250,
		velocity: 7.68583333333333,
		power: 1422.56056005994,
		road: 12961.16875,
		acceleration: 0.0323148148148151
	},
	{
		id: 1252,
		time: 1251,
		velocity: 7.83833333333333,
		power: 3292.88667990491,
		road: 12969.0538888889,
		acceleration: 0.276388888888889
	},
	{
		id: 1253,
		time: 1252,
		velocity: 8.54583333333333,
		power: 4225.66591515781,
		road: 12977.2648611111,
		acceleration: 0.375277777777777
	},
	{
		id: 1254,
		time: 1253,
		velocity: 8.81166666666667,
		power: 5726.19918705689,
		road: 12985.9258333333,
		acceleration: 0.524722222222223
	},
	{
		id: 1255,
		time: 1254,
		velocity: 9.4125,
		power: 5060.14315852684,
		road: 12995.0530555556,
		acceleration: 0.407777777777778
	},
	{
		id: 1256,
		time: 1255,
		velocity: 9.76916666666667,
		power: 7092.49264791071,
		road: 13004.6809722222,
		acceleration: 0.593611111111111
	},
	{
		id: 1257,
		time: 1256,
		velocity: 10.5925,
		power: 6491.5332193317,
		road: 13014.8478703704,
		acceleration: 0.484351851851853
	},
	{
		id: 1258,
		time: 1257,
		velocity: 10.8655555555556,
		power: 6306.56581784985,
		road: 13025.4727777778,
		acceleration: 0.431666666666667
	},
	{
		id: 1259,
		time: 1258,
		velocity: 11.0641666666667,
		power: 2697.99445575053,
		road: 13036.3466203704,
		acceleration: 0.0662037037037013
	},
	{
		id: 1260,
		time: 1259,
		velocity: 10.7911111111111,
		power: 1044.15721071928,
		road: 13047.207037037,
		acceleration: -0.093055555555555
	},
	{
		id: 1261,
		time: 1260,
		velocity: 10.5863888888889,
		power: -1017.65581318798,
		road: 13057.8750925926,
		acceleration: -0.291666666666668
	},
	{
		id: 1262,
		time: 1261,
		velocity: 10.1891666666667,
		power: -1463.71395624646,
		road: 13068.229212963,
		acceleration: -0.336203703703703
	},
	{
		id: 1263,
		time: 1262,
		velocity: 9.7825,
		power: -2435.41743705533,
		road: 13078.1952777778,
		acceleration: -0.439907407407407
	},
	{
		id: 1264,
		time: 1263,
		velocity: 9.26666666666667,
		power: -3438.94753506847,
		road: 13087.661712963,
		acceleration: -0.559351851851851
	},
	{
		id: 1265,
		time: 1264,
		velocity: 8.51111111111111,
		power: -5737.19097242348,
		road: 13096.4190740741,
		acceleration: -0.858796296296298
	},
	{
		id: 1266,
		time: 1265,
		velocity: 7.20611111111111,
		power: -6980.69484699716,
		road: 13104.1944907407,
		acceleration: -1.10509259259259
	},
	{
		id: 1267,
		time: 1266,
		velocity: 5.95138888888889,
		power: -6608.81145014645,
		road: 13110.8164814815,
		acceleration: -1.20175925925926
	},
	{
		id: 1268,
		time: 1267,
		velocity: 4.90583333333333,
		power: -4591.9660378552,
		road: 13116.3268518519,
		acceleration: -1.02148148148148
	},
	{
		id: 1269,
		time: 1268,
		velocity: 4.14166666666667,
		power: -2229.91360404939,
		road: 13121.0057407407,
		acceleration: -0.641481481481481
	},
	{
		id: 1270,
		time: 1269,
		velocity: 4.02694444444444,
		power: -519.22754145495,
		road: 13125.230462963,
		acceleration: -0.266851851851853
	},
	{
		id: 1271,
		time: 1270,
		velocity: 4.10527777777778,
		power: 99.2664345624291,
		road: 13129.2664351852,
		acceleration: -0.110648148148148
	},
	{
		id: 1272,
		time: 1271,
		velocity: 3.80972222222222,
		power: -117.827216029102,
		road: 13133.1631944444,
		acceleration: -0.167777777777777
	},
	{
		id: 1273,
		time: 1272,
		velocity: 3.52361111111111,
		power: -1603.67906374715,
		road: 13136.6678703704,
		acceleration: -0.616388888888889
	},
	{
		id: 1274,
		time: 1273,
		velocity: 2.25611111111111,
		power: -2405.72629661798,
		road: 13139.3206944444,
		acceleration: -1.08731481481481
	},
	{
		id: 1275,
		time: 1274,
		velocity: 0.547777777777778,
		power: -1508.8512021053,
		road: 13140.8425925926,
		acceleration: -1.17453703703704
	},
	{
		id: 1276,
		time: 1275,
		velocity: 0,
		power: -330.485587269692,
		road: 13141.4012037037,
		acceleration: -0.752037037037037
	},
	{
		id: 1277,
		time: 1276,
		velocity: 0,
		power: -4.76243125406108,
		road: 13141.4925,
		acceleration: -0.182592592592593
	},
	{
		id: 1278,
		time: 1277,
		velocity: 0,
		power: 0,
		road: 13141.4925,
		acceleration: 0
	},
	{
		id: 1279,
		time: 1278,
		velocity: 0,
		power: 0,
		road: 13141.4925,
		acceleration: 0
	},
	{
		id: 1280,
		time: 1279,
		velocity: 0,
		power: 0,
		road: 13141.4925,
		acceleration: 0
	},
	{
		id: 1281,
		time: 1280,
		velocity: 0,
		power: 0,
		road: 13141.4925,
		acceleration: 0
	},
	{
		id: 1282,
		time: 1281,
		velocity: 0,
		power: 0,
		road: 13141.4925,
		acceleration: 0
	},
	{
		id: 1283,
		time: 1282,
		velocity: 0,
		power: 0,
		road: 13141.4925,
		acceleration: 0
	},
	{
		id: 1284,
		time: 1283,
		velocity: 0,
		power: 0,
		road: 13141.4925,
		acceleration: 0
	},
	{
		id: 1285,
		time: 1284,
		velocity: 0,
		power: 36.418596988352,
		road: 13141.6028703704,
		acceleration: 0.220740740740741
	},
	{
		id: 1286,
		time: 1285,
		velocity: 0.662222222222222,
		power: 480.247202344199,
		road: 13142.1907407407,
		acceleration: 0.734259259259259
	},
	{
		id: 1287,
		time: 1286,
		velocity: 2.20277777777778,
		power: 1623.63131947654,
		road: 13143.6629166667,
		acceleration: 1.03435185185185
	},
	{
		id: 1288,
		time: 1287,
		velocity: 3.10305555555556,
		power: 2705.10037448828,
		road: 13146.1581018519,
		acceleration: 1.01166666666667
	},
	{
		id: 1289,
		time: 1288,
		velocity: 3.69722222222222,
		power: 1621.69018643653,
		road: 13149.3593981482,
		acceleration: 0.400555555555555
	},
	{
		id: 1290,
		time: 1289,
		velocity: 3.40444444444444,
		power: 718.676483445787,
		road: 13152.8038425926,
		acceleration: 0.0857407407407411
	},
	{
		id: 1291,
		time: 1290,
		velocity: 3.36027777777778,
		power: 717.224093474488,
		road: 13156.3310648148,
		acceleration: 0.079814814814815
	},
	{
		id: 1292,
		time: 1291,
		velocity: 3.93666666666667,
		power: 1390.85586662165,
		road: 13160.02875,
		acceleration: 0.261111111111111
	},
	{
		id: 1293,
		time: 1292,
		velocity: 4.18777777777778,
		power: 2095.11085831781,
		road: 13164.0622222222,
		acceleration: 0.410462962962963
	},
	{
		id: 1294,
		time: 1293,
		velocity: 4.59166666666667,
		power: 1330.38034933909,
		road: 13168.3936574074,
		acceleration: 0.185462962962964
	},
	{
		id: 1295,
		time: 1294,
		velocity: 4.49305555555556,
		power: 930.080217928705,
		road: 13172.8581944445,
		acceleration: 0.0807407407407403
	},
	{
		id: 1296,
		time: 1295,
		velocity: 4.43,
		power: 127.646928484348,
		road: 13177.3089351852,
		acceleration: -0.108333333333334
	},
	{
		id: 1297,
		time: 1296,
		velocity: 4.26666666666667,
		power: 200.846850593051,
		road: 13181.660787037,
		acceleration: -0.0894444444444442
	},
	{
		id: 1298,
		time: 1297,
		velocity: 4.22472222222222,
		power: 213.477472396815,
		road: 13185.925462963,
		acceleration: -0.0849074074074077
	},
	{
		id: 1299,
		time: 1298,
		velocity: 4.17527777777778,
		power: 622.988782457569,
		road: 13190.1564814815,
		acceleration: 0.0175925925925933
	},
	{
		id: 1300,
		time: 1299,
		velocity: 4.31944444444444,
		power: 305.135363446149,
		road: 13194.365787037,
		acceleration: -0.0610185185185186
	},
	{
		id: 1301,
		time: 1300,
		velocity: 4.04166666666667,
		power: -1257.08239800109,
		road: 13198.3086111111,
		acceleration: -0.471944444444445
	},
	{
		id: 1302,
		time: 1301,
		velocity: 2.75944444444444,
		power: -2084.21727747451,
		road: 13201.6167592593,
		acceleration: -0.797407407407407
	},
	{
		id: 1303,
		time: 1302,
		velocity: 1.92722222222222,
		power: -1488.16724997074,
		road: 13204.1512037037,
		acceleration: -0.75
	},
	{
		id: 1304,
		time: 1303,
		velocity: 1.79166666666667,
		power: -366.927275832905,
		road: 13206.1489814815,
		acceleration: -0.323333333333333
	},
	{
		id: 1305,
		time: 1304,
		velocity: 1.78944444444444,
		power: 72.5412697890477,
		road: 13207.9418055556,
		acceleration: -0.0865740740740744
	},
	{
		id: 1306,
		time: 1305,
		velocity: 1.6675,
		power: -644.574915529856,
		road: 13209.3927314815,
		acceleration: -0.597222222222222
	},
	{
		id: 1307,
		time: 1306,
		velocity: 0,
		power: 221.004118295434,
		road: 13210.5791666667,
		acceleration: 0.0682407407407408
	},
	{
		id: 1308,
		time: 1307,
		velocity: 1.99416666666667,
		power: 394.703400785972,
		road: 13211.8938425926,
		acceleration: 0.188240740740741
	},
	{
		id: 1309,
		time: 1308,
		velocity: 2.23222222222222,
		power: 1632.527174649,
		road: 13213.7114814815,
		acceleration: 0.817685185185185
	},
	{
		id: 1310,
		time: 1309,
		velocity: 2.45305555555556,
		power: 528.191889905435,
		road: 13215.9947222222,
		acceleration: 0.113518518518518
	},
	{
		id: 1311,
		time: 1310,
		velocity: 2.33472222222222,
		power: 265.185378845024,
		road: 13218.3293518519,
		acceleration: -0.0107407407407409
	},
	{
		id: 1312,
		time: 1311,
		velocity: 2.2,
		power: 108.555016687158,
		road: 13220.6184259259,
		acceleration: -0.0803703703703702
	},
	{
		id: 1313,
		time: 1312,
		velocity: 2.21194444444444,
		power: 342.800179478704,
		road: 13222.8819907407,
		acceleration: 0.0293518518518519
	},
	{
		id: 1314,
		time: 1313,
		velocity: 2.42277777777778,
		power: 217.56499922188,
		road: 13225.1457407407,
		acceleration: -0.0289814814814813
	},
	{
		id: 1315,
		time: 1314,
		velocity: 2.11305555555556,
		power: 153.50562259394,
		road: 13227.3663425926,
		acceleration: -0.0573148148148146
	},
	{
		id: 1316,
		time: 1315,
		velocity: 2.04,
		power: 159.902814459348,
		road: 13229.5321759259,
		acceleration: -0.0522222222222224
	},
	{
		id: 1317,
		time: 1316,
		velocity: 2.26611111111111,
		power: 327.468265071127,
		road: 13231.6869907407,
		acceleration: 0.0301851851851849
	},
	{
		id: 1318,
		time: 1317,
		velocity: 2.20361111111111,
		power: 398.186939023872,
		road: 13233.8872222222,
		acceleration: 0.0606481481481485
	},
	{
		id: 1319,
		time: 1318,
		velocity: 2.22194444444444,
		power: 261.001424049703,
		road: 13236.1144444445,
		acceleration: -0.00666666666666682
	},
	{
		id: 1320,
		time: 1319,
		velocity: 2.24611111111111,
		power: 621.340574383945,
		road: 13238.415462963,
		acceleration: 0.154259259259259
	},
	{
		id: 1321,
		time: 1320,
		velocity: 2.66638888888889,
		power: 1183.56306296179,
		road: 13240.972037037,
		acceleration: 0.356851851851852
	},
	{
		id: 1322,
		time: 1321,
		velocity: 3.2925,
		power: 1665.6236013033,
		road: 13243.9368518519,
		acceleration: 0.45962962962963
	},
	{
		id: 1323,
		time: 1322,
		velocity: 3.625,
		power: 1473.19572507777,
		road: 13247.2956481482,
		acceleration: 0.328333333333333
	},
	{
		id: 1324,
		time: 1323,
		velocity: 3.65138888888889,
		power: 802.130955776305,
		road: 13250.869537037,
		acceleration: 0.101851851851852
	},
	{
		id: 1325,
		time: 1324,
		velocity: 3.59805555555556,
		power: 479.417593717931,
		road: 13254.4965740741,
		acceleration: 0.00444444444444425
	},
	{
		id: 1326,
		time: 1325,
		velocity: 3.63833333333333,
		power: 563.792495084676,
		road: 13258.1399074074,
		acceleration: 0.0281481481481487
	},
	{
		id: 1327,
		time: 1326,
		velocity: 3.73583333333333,
		power: 773.421822908555,
		road: 13261.8398611111,
		acceleration: 0.0850925925925918
	},
	{
		id: 1328,
		time: 1327,
		velocity: 3.85333333333333,
		power: 434.376754095201,
		road: 13265.5759722222,
		acceleration: -0.0127777777777776
	},
	{
		id: 1329,
		time: 1328,
		velocity: 3.6,
		power: 209.861417573085,
		road: 13269.2681018519,
		acceleration: -0.0751851851851848
	},
	{
		id: 1330,
		time: 1329,
		velocity: 3.51027777777778,
		power: 286.690967317244,
		road: 13272.8968518519,
		acceleration: -0.0515740740740744
	},
	{
		id: 1331,
		time: 1330,
		velocity: 3.69861111111111,
		power: 530.711403071605,
		road: 13276.5098148148,
		acceleration: 0.0200000000000005
	},
	{
		id: 1332,
		time: 1331,
		velocity: 3.66,
		power: 836.734990490387,
		road: 13280.1851851852,
		acceleration: 0.104814814814814
	},
	{
		id: 1333,
		time: 1332,
		velocity: 3.82472222222222,
		power: 1066.03167467432,
		road: 13283.9926388889,
		acceleration: 0.159351851851852
	},
	{
		id: 1334,
		time: 1333,
		velocity: 4.17666666666667,
		power: 1285.05152598383,
		road: 13287.98125,
		acceleration: 0.202962962962963
	},
	{
		id: 1335,
		time: 1334,
		velocity: 4.26888888888889,
		power: 1200.42779813227,
		road: 13292.154212963,
		acceleration: 0.165740740740739
	},
	{
		id: 1336,
		time: 1335,
		velocity: 4.32194444444444,
		power: 875.237736704716,
		road: 13296.4484722222,
		acceleration: 0.0768518518518526
	},
	{
		id: 1337,
		time: 1336,
		velocity: 4.40722222222222,
		power: 631.364025438162,
		road: 13300.78875,
		acceleration: 0.0151851851851852
	},
	{
		id: 1338,
		time: 1337,
		velocity: 4.31444444444444,
		power: 563.691453104621,
		road: 13305.1358796296,
		acceleration: -0.00148148148148142
	},
	{
		id: 1339,
		time: 1338,
		velocity: 4.3175,
		power: 250.990251761828,
		road: 13309.4440277778,
		acceleration: -0.0764814814814825
	},
	{
		id: 1340,
		time: 1339,
		velocity: 4.17777777777778,
		power: 91.3423290964209,
		road: 13313.6566666667,
		acceleration: -0.114537037037036
	},
	{
		id: 1341,
		time: 1340,
		velocity: 3.97083333333333,
		power: 97.6378340421967,
		road: 13317.7561574074,
		acceleration: -0.11175925925926
	},
	{
		id: 1342,
		time: 1341,
		velocity: 3.98222222222222,
		power: -3.56942817087765,
		road: 13321.7311574074,
		acceleration: -0.137222222222222
	},
	{
		id: 1343,
		time: 1342,
		velocity: 3.76611111111111,
		power: 5.18068239075356,
		road: 13325.5704166667,
		acceleration: -0.13425925925926
	},
	{
		id: 1344,
		time: 1343,
		velocity: 3.56805555555556,
		power: -75.4242162373547,
		road: 13329.2642592593,
		acceleration: -0.156574074074074
	},
	{
		id: 1345,
		time: 1344,
		velocity: 3.5125,
		power: -70.3164586693348,
		road: 13332.8021296296,
		acceleration: -0.15537037037037
	},
	{
		id: 1346,
		time: 1345,
		velocity: 3.3,
		power: 210.791158059269,
		road: 13336.2277314815,
		acceleration: -0.0691666666666673
	},
	{
		id: 1347,
		time: 1346,
		velocity: 3.36055555555556,
		power: 283.54502853477,
		road: 13339.5962037037,
		acceleration: -0.0450925925925922
	},
	{
		id: 1348,
		time: 1347,
		velocity: 3.37722222222222,
		power: 479.107801395036,
		road: 13342.9505092593,
		acceleration: 0.0167592592592589
	},
	{
		id: 1349,
		time: 1348,
		velocity: 3.35027777777778,
		power: 426.008769599257,
		road: 13346.3130555556,
		acceleration: -0.000277777777777821
	},
	{
		id: 1350,
		time: 1349,
		velocity: 3.35972222222222,
		power: 660.580367145862,
		road: 13349.7109259259,
		acceleration: 0.0709259259259265
	},
	{
		id: 1351,
		time: 1350,
		velocity: 3.59,
		power: 746.585202880545,
		road: 13353.1901851852,
		acceleration: 0.0918518518518519
	},
	{
		id: 1352,
		time: 1351,
		velocity: 3.62583333333333,
		power: 1015.56327740745,
		road: 13356.7963425926,
		acceleration: 0.161944444444444
	},
	{
		id: 1353,
		time: 1352,
		velocity: 3.84555555555556,
		power: 822.433823233604,
		road: 13360.5318055556,
		acceleration: 0.0966666666666667
	},
	{
		id: 1354,
		time: 1353,
		velocity: 3.88,
		power: 1001.92095148345,
		road: 13364.3846759259,
		acceleration: 0.138148148148148
	},
	{
		id: 1355,
		time: 1354,
		velocity: 4.04027777777778,
		power: 1100.37099871326,
		road: 13368.3833333333,
		acceleration: 0.153425925925925
	},
	{
		id: 1356,
		time: 1355,
		velocity: 4.30583333333333,
		power: 1095.16145029436,
		road: 13372.5292592593,
		acceleration: 0.141111111111112
	},
	{
		id: 1357,
		time: 1356,
		velocity: 4.30333333333333,
		power: 473.618970857898,
		road: 13376.7363425926,
		acceleration: -0.018796296296296
	},
	{
		id: 1358,
		time: 1357,
		velocity: 3.98388888888889,
		power: -237.1768477524,
		road: 13380.8351388889,
		acceleration: -0.197777777777778
	},
	{
		id: 1359,
		time: 1358,
		velocity: 3.7125,
		power: -656.083229885267,
		road: 13384.6772685185,
		acceleration: -0.315555555555555
	},
	{
		id: 1360,
		time: 1359,
		velocity: 3.35666666666667,
		power: -1193.82059723089,
		road: 13388.1114814815,
		acceleration: -0.500277777777778
	},
	{
		id: 1361,
		time: 1360,
		velocity: 2.48305555555556,
		power: -1711.37525521529,
		road: 13390.907037037,
		acceleration: -0.777037037037037
	},
	{
		id: 1362,
		time: 1361,
		velocity: 1.38138888888889,
		power: -1640.9913715761,
		road: 13392.7897685185,
		acceleration: -1.04861111111111
	},
	{
		id: 1363,
		time: 1362,
		velocity: 0.210833333333333,
		power: -626.402320430616,
		road: 13393.7343518519,
		acceleration: -0.827685185185185
	},
	{
		id: 1364,
		time: 1363,
		velocity: 0,
		power: -84.2091565672005,
		road: 13394.0681944444,
		acceleration: -0.393796296296296
	},
	{
		id: 1365,
		time: 1364,
		velocity: 0.2,
		power: 5.52207180174854,
		road: 13394.17,
		acceleration: -0.0702777777777778
	},
	{
		id: 1366,
		time: 1365,
		velocity: 0,
		power: 32.5388320916118,
		road: 13394.3025,
		acceleration: 0.131666666666667
	},
	{
		id: 1367,
		time: 1366,
		velocity: 0.395,
		power: 9.51545285852092,
		road: 13394.4675,
		acceleration: -0.0666666666666667
	},
	{
		id: 1368,
		time: 1367,
		velocity: 0,
		power: 23.396642775841,
		road: 13394.6176388889,
		acceleration: 0.0369444444444445
	},
	{
		id: 1369,
		time: 1368,
		velocity: 0.110833333333333,
		power: -0.402704547570706,
		road: 13394.7204166667,
		acceleration: -0.131666666666667
	},
	{
		id: 1370,
		time: 1369,
		velocity: 0,
		power: 406.652873749962,
		road: 13395.1703703704,
		acceleration: 0.826018518518518
	},
	{
		id: 1371,
		time: 1370,
		velocity: 2.47805555555556,
		power: 2614.61146103324,
		road: 13396.8096759259,
		acceleration: 1.55268518518518
	},
	{
		id: 1372,
		time: 1371,
		velocity: 4.76888888888889,
		power: 6670.66752321654,
		road: 13400.1958333333,
		acceleration: 1.94101851851852
	},
	{
		id: 1373,
		time: 1372,
		velocity: 5.82305555555556,
		power: 6558.68581805949,
		road: 13405.1749537037,
		acceleration: 1.24490740740741
	},
	{
		id: 1374,
		time: 1373,
		velocity: 6.21277777777778,
		power: 5005.37705147792,
		road: 13411.1438425926,
		acceleration: 0.73462962962963
	},
	{
		id: 1375,
		time: 1374,
		velocity: 6.97277777777778,
		power: 5015.18410691209,
		road: 13417.8,
		acceleration: 0.639907407407408
	},
	{
		id: 1376,
		time: 1375,
		velocity: 7.74277777777778,
		power: 3870.23820699955,
		road: 13424.9811574074,
		acceleration: 0.410092592592592
	},
	{
		id: 1377,
		time: 1376,
		velocity: 7.44305555555556,
		power: 2751.77872788299,
		road: 13432.4806481482,
		acceleration: 0.226574074074075
	},
	{
		id: 1378,
		time: 1377,
		velocity: 7.6525,
		power: 2892.36919694906,
		road: 13440.209537037,
		acceleration: 0.232222222222221
	},
	{
		id: 1379,
		time: 1378,
		velocity: 8.43944444444444,
		power: 4952.33958265984,
		road: 13448.294212963,
		acceleration: 0.479351851851853
	},
	{
		id: 1380,
		time: 1379,
		velocity: 8.88111111111111,
		power: 7807.1072227535,
		road: 13457.0041666667,
		acceleration: 0.771203703703701
	},
	{
		id: 1381,
		time: 1380,
		velocity: 9.96611111111111,
		power: 6266.41353781291,
		road: 13466.3628240741,
		acceleration: 0.526203703703704
	},
	{
		id: 1382,
		time: 1381,
		velocity: 10.0180555555556,
		power: 4808.34231567133,
		road: 13476.1516666667,
		acceleration: 0.334166666666667
	},
	{
		id: 1383,
		time: 1382,
		velocity: 9.88361111111111,
		power: 1288.60530854802,
		road: 13486.0841666667,
		acceleration: -0.0468518518518497
	},
	{
		id: 1384,
		time: 1383,
		velocity: 9.82555555555555,
		power: 2983.6295512408,
		road: 13496.0584722222,
		acceleration: 0.130462962962962
	},
	{
		id: 1385,
		time: 1384,
		velocity: 10.4094444444444,
		power: 6515.1347819622,
		road: 13506.3370833333,
		acceleration: 0.478148148148147
	},
	{
		id: 1386,
		time: 1385,
		velocity: 11.3180555555556,
		power: 9397.15258795307,
		road: 13517.2109722222,
		acceleration: 0.712407407407408
	},
	{
		id: 1387,
		time: 1386,
		velocity: 11.9627777777778,
		power: 10431.785324941,
		road: 13528.8108796296,
		acceleration: 0.739629629629629
	},
	{
		id: 1388,
		time: 1387,
		velocity: 12.6283333333333,
		power: 8150.80052996757,
		road: 13541.0245833333,
		acceleration: 0.487962962962964
	},
	{
		id: 1389,
		time: 1388,
		velocity: 12.7819444444444,
		power: 9276.91054807718,
		road: 13553.7546759259,
		acceleration: 0.544814814814817
	},
	{
		id: 1390,
		time: 1389,
		velocity: 13.5972222222222,
		power: 8420.78545547463,
		road: 13566.9777314815,
		acceleration: 0.441111111111107
	},
	{
		id: 1391,
		time: 1390,
		velocity: 13.9516666666667,
		power: 7692.86793537376,
		road: 13580.6010185185,
		acceleration: 0.359351851851853
	},
	{
		id: 1392,
		time: 1391,
		velocity: 13.86,
		power: 5170.76330159989,
		road: 13594.4810185185,
		acceleration: 0.154074074074074
	},
	{
		id: 1393,
		time: 1392,
		velocity: 14.0594444444444,
		power: 2687.41864250117,
		road: 13608.4205092593,
		acceleration: -0.0350925925925942
	},
	{
		id: 1394,
		time: 1393,
		velocity: 13.8463888888889,
		power: 2995.54887857483,
		road: 13622.3368518519,
		acceleration: -0.0112037037037034
	},
	{
		id: 1395,
		time: 1394,
		velocity: 13.8263888888889,
		power: 2361.03974696633,
		road: 13636.2186111111,
		acceleration: -0.0579629629629643
	},
	{
		id: 1396,
		time: 1395,
		velocity: 13.8855555555556,
		power: 3020.96842621172,
		road: 13650.0678240741,
		acceleration: -0.00712962962962749
	},
	{
		id: 1397,
		time: 1396,
		velocity: 13.825,
		power: 3612.06571711253,
		road: 13663.9319907407,
		acceleration: 0.0370370370370381
	},
	{
		id: 1398,
		time: 1397,
		velocity: 13.9375,
		power: 4776.34997968486,
		road: 13677.8755092593,
		acceleration: 0.121666666666664
	},
	{
		id: 1399,
		time: 1398,
		velocity: 14.2505555555556,
		power: 6115.50328602327,
		road: 13691.9868981482,
		acceleration: 0.214074074074075
	},
	{
		id: 1400,
		time: 1399,
		velocity: 14.4672222222222,
		power: 6825.1395736063,
		road: 13706.3326388889,
		acceleration: 0.25462962962963
	},
	{
		id: 1401,
		time: 1400,
		velocity: 14.7013888888889,
		power: 5888.95611386616,
		road: 13720.8938888889,
		acceleration: 0.176388888888891
	},
	{
		id: 1402,
		time: 1401,
		velocity: 14.7797222222222,
		power: 3163.2737899431,
		road: 13735.5323148148,
		acceleration: -0.0220370370370411
	},
	{
		id: 1403,
		time: 1402,
		velocity: 14.4011111111111,
		power: 2251.63759721458,
		road: 13750.1168518519,
		acceleration: -0.0857407407407393
	},
	{
		id: 1404,
		time: 1403,
		velocity: 14.4441666666667,
		power: 762.954213462161,
		road: 13764.5636111111,
		acceleration: -0.189814814814813
	},
	{
		id: 1405,
		time: 1404,
		velocity: 14.2102777777778,
		power: 2359.18336916388,
		road: 13778.8802314815,
		acceleration: -0.0704629629629636
	},
	{
		id: 1406,
		time: 1405,
		velocity: 14.1897222222222,
		power: 1031.99280952432,
		road: 13793.0791203704,
		acceleration: -0.165000000000003
	},
	{
		id: 1407,
		time: 1406,
		velocity: 13.9491666666667,
		power: 673.989406391261,
		road: 13807.1015277778,
		acceleration: -0.18796296296296
	},
	{
		id: 1408,
		time: 1407,
		velocity: 13.6463888888889,
		power: -603.441066014945,
		road: 13820.8897222222,
		acceleration: -0.280462962962963
	},
	{
		id: 1409,
		time: 1408,
		velocity: 13.3483333333333,
		power: -1625.00673404888,
		road: 13834.3596296296,
		acceleration: -0.356111111111113
	},
	{
		id: 1410,
		time: 1409,
		velocity: 12.8808333333333,
		power: -3769.36992226516,
		road: 13847.3883333333,
		acceleration: -0.526296296296294
	},
	{
		id: 1411,
		time: 1410,
		velocity: 12.0675,
		power: -7200.92465406141,
		road: 13859.7416203704,
		acceleration: -0.824537037037038
	},
	{
		id: 1412,
		time: 1411,
		velocity: 10.8747222222222,
		power: -8631.64216864693,
		road: 13871.1864814815,
		acceleration: -0.992314814814813
	},
	{
		id: 1413,
		time: 1412,
		velocity: 9.90388888888889,
		power: -8899.3177203214,
		road: 13881.5921759259,
		acceleration: -1.08601851851852
	},
	{
		id: 1414,
		time: 1413,
		velocity: 8.80944444444444,
		power: -7526.49540933332,
		road: 13890.9439351852,
		acceleration: -1.02185185185185
	},
	{
		id: 1415,
		time: 1414,
		velocity: 7.80916666666667,
		power: -5154.52564400707,
		road: 13899.3800462963,
		acceleration: -0.809444444444443
	},
	{
		id: 1416,
		time: 1415,
		velocity: 7.47555555555556,
		power: -2132.43905938784,
		road: 13907.187037037,
		acceleration: -0.448796296296297
	},
	{
		id: 1417,
		time: 1416,
		velocity: 7.46305555555556,
		power: 1240.31163792868,
		road: 13914.7756018519,
		acceleration: 0.0119444444444436
	},
	{
		id: 1418,
		time: 1417,
		velocity: 7.845,
		power: 2813.01410083156,
		road: 13922.4815277778,
		acceleration: 0.222777777777778
	},
	{
		id: 1419,
		time: 1418,
		velocity: 8.14388888888889,
		power: 3731.54887472228,
		road: 13930.4628240741,
		acceleration: 0.327962962962962
	},
	{
		id: 1420,
		time: 1419,
		velocity: 8.44694444444444,
		power: 3699.89178013117,
		road: 13938.7592592593,
		acceleration: 0.302314814814816
	},
	{
		id: 1421,
		time: 1420,
		velocity: 8.75194444444444,
		power: 1844.56169029287,
		road: 13947.2371759259,
		acceleration: 0.0606481481481485
	},
	{
		id: 1422,
		time: 1421,
		velocity: 8.32583333333333,
		power: 88.743779406081,
		road: 13955.6672222222,
		acceleration: -0.156388888888889
	},
	{
		id: 1423,
		time: 1422,
		velocity: 7.97777777777778,
		power: -1695.53839550263,
		road: 13963.8274537037,
		acceleration: -0.38324074074074
	},
	{
		id: 1424,
		time: 1423,
		velocity: 7.60222222222222,
		power: -1092.32218367223,
		road: 13971.641712963,
		acceleration: -0.308703703703705
	},
	{
		id: 1425,
		time: 1424,
		velocity: 7.39972222222222,
		power: -1431.60318189583,
		road: 13979.1215740741,
		acceleration: -0.360092592592594
	},
	{
		id: 1426,
		time: 1425,
		velocity: 6.8975,
		power: -1946.20891605458,
		road: 13986.1990277778,
		acceleration: -0.44472222222222
	},
	{
		id: 1427,
		time: 1426,
		velocity: 6.26805555555556,
		power: -1939.6263007604,
		road: 13992.8241203704,
		acceleration: -0.460000000000001
	},
	{
		id: 1428,
		time: 1427,
		velocity: 6.01972222222222,
		power: -2337.00841934622,
		road: 13998.9441203704,
		acceleration: -0.550185185185185
	},
	{
		id: 1429,
		time: 1428,
		velocity: 5.24694444444444,
		power: -2738.57413300618,
		road: 14004.4553240741,
		acceleration: -0.667407407407408
	},
	{
		id: 1430,
		time: 1429,
		velocity: 4.26583333333333,
		power: -4445.13373377763,
		road: 14009.0543055556,
		acceleration: -1.15703703703704
	},
	{
		id: 1431,
		time: 1430,
		velocity: 2.54861111111111,
		power: -3474.46887214865,
		road: 14012.4722222222,
		acceleration: -1.20509259259259
	},
	{
		id: 1432,
		time: 1431,
		velocity: 1.63166666666667,
		power: -2216.38160534972,
		road: 14014.6973611111,
		acceleration: -1.18046296296296
	},
	{
		id: 1433,
		time: 1432,
		velocity: 0.724444444444444,
		power: -827.337857264403,
		road: 14015.9075,
		acceleration: -0.849537037037037
	},
	{
		id: 1434,
		time: 1433,
		velocity: 0,
		power: -202.502171157573,
		road: 14016.4209259259,
		acceleration: -0.543888888888889
	},
	{
		id: 1435,
		time: 1434,
		velocity: 0,
		power: -13.0344501624431,
		road: 14016.5416666667,
		acceleration: -0.241481481481481
	},
	{
		id: 1436,
		time: 1435,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1437,
		time: 1436,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1438,
		time: 1437,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1439,
		time: 1438,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1440,
		time: 1439,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1441,
		time: 1440,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1442,
		time: 1441,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1443,
		time: 1442,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1444,
		time: 1443,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1445,
		time: 1444,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1446,
		time: 1445,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1447,
		time: 1446,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1448,
		time: 1447,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1449,
		time: 1448,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1450,
		time: 1449,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1451,
		time: 1450,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1452,
		time: 1451,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1453,
		time: 1452,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1454,
		time: 1453,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1455,
		time: 1454,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1456,
		time: 1455,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1457,
		time: 1456,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1458,
		time: 1457,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1459,
		time: 1458,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1460,
		time: 1459,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1461,
		time: 1460,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1462,
		time: 1461,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1463,
		time: 1462,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1464,
		time: 1463,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1465,
		time: 1464,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1466,
		time: 1465,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1467,
		time: 1466,
		velocity: 0,
		power: 0,
		road: 14016.5416666667,
		acceleration: 0
	},
	{
		id: 1468,
		time: 1467,
		velocity: 0,
		power: 45.4024793951251,
		road: 14016.6678240741,
		acceleration: 0.252314814814815
	},
	{
		id: 1469,
		time: 1468,
		velocity: 0.756944444444444,
		power: 802.866525330281,
		road: 14017.4195833333,
		acceleration: 0.998888888888889
	},
	{
		id: 1470,
		time: 1469,
		velocity: 2.99666666666667,
		power: 2812.73056544164,
		road: 14019.3672222222,
		acceleration: 1.39287037037037
	},
	{
		id: 1471,
		time: 1470,
		velocity: 4.17861111111111,
		power: 5078.94490047131,
		road: 14022.737962963,
		acceleration: 1.45333333333333
	},
	{
		id: 1472,
		time: 1471,
		velocity: 5.11694444444444,
		power: 3989.03481697297,
		road: 14027.2331018519,
		acceleration: 0.795462962962963
	},
	{
		id: 1473,
		time: 1472,
		velocity: 5.38305555555556,
		power: 3781.28677077809,
		road: 14032.4369907407,
		acceleration: 0.622037037037036
	},
	{
		id: 1474,
		time: 1473,
		velocity: 6.04472222222222,
		power: 1783.1075526383,
		road: 14038.0465740741,
		acceleration: 0.189351851851853
	},
	{
		id: 1475,
		time: 1474,
		velocity: 5.685,
		power: 356.344505953767,
		road: 14043.7112037037,
		acceleration: -0.0792592592592598
	},
	{
		id: 1476,
		time: 1475,
		velocity: 5.14527777777778,
		power: -398.083775114452,
		road: 14049.2259722222,
		acceleration: -0.220462962962962
	},
	{
		id: 1477,
		time: 1476,
		velocity: 5.38333333333333,
		power: 1896.73171849083,
		road: 14054.7392592593,
		acceleration: 0.217499999999998
	},
	{
		id: 1478,
		time: 1477,
		velocity: 6.3375,
		power: 4697.55739915338,
		road: 14060.7019444444,
		acceleration: 0.681296296296297
	},
	{
		id: 1479,
		time: 1478,
		velocity: 7.18916666666667,
		power: 7463.57229350158,
		road: 14067.5052314815,
		acceleration: 0.999907407407407
	},
	{
		id: 1480,
		time: 1479,
		velocity: 8.38305555555555,
		power: 5967.75821355479,
		road: 14075.1391203704,
		acceleration: 0.661296296296295
	},
	{
		id: 1481,
		time: 1480,
		velocity: 8.32138888888889,
		power: 2698.62499909926,
		road: 14083.1975925926,
		acceleration: 0.187870370370372
	},
	{
		id: 1482,
		time: 1481,
		velocity: 7.75277777777778,
		power: -3017.72830216051,
		road: 14091.0673611111,
		acceleration: -0.565277777777778
	},
	{
		id: 1483,
		time: 1482,
		velocity: 6.68722222222222,
		power: -5683.27100499408,
		road: 14098.1550925926,
		acceleration: -0.998796296296296
	},
	{
		id: 1484,
		time: 1483,
		velocity: 5.325,
		power: -6189.32089550473,
		road: 14104.124212963,
		acceleration: -1.23842592592593
	},
	{
		id: 1485,
		time: 1484,
		velocity: 4.0375,
		power: -4868.4447543005,
		road: 14108.863287037,
		acceleration: -1.22166666666667
	},
	{
		id: 1486,
		time: 1485,
		velocity: 3.02222222222222,
		power: -2423.70296060503,
		road: 14112.5805092593,
		acceleration: -0.822037037037037
	},
	{
		id: 1487,
		time: 1486,
		velocity: 2.85888888888889,
		power: -1241.07234264242,
		road: 14115.6041666667,
		acceleration: -0.565092592592592
	},
	{
		id: 1488,
		time: 1487,
		velocity: 2.34222222222222,
		power: -473.13502544328,
		road: 14118.1830092593,
		acceleration: -0.324537037037037
	},
	{
		id: 1489,
		time: 1488,
		velocity: 2.04861111111111,
		power: -355.034095771095,
		road: 14120.4519444444,
		acceleration: -0.295277777777777
	},
	{
		id: 1490,
		time: 1489,
		velocity: 1.97305555555556,
		power: 190.146552070845,
		road: 14122.5559259259,
		acceleration: -0.03462962962963
	},
	{
		id: 1491,
		time: 1490,
		velocity: 2.23833333333333,
		power: 285.888910049942,
		road: 14124.6496296296,
		acceleration: 0.0140740740740739
	},
	{
		id: 1492,
		time: 1491,
		velocity: 2.09083333333333,
		power: 361.689716377211,
		road: 14126.7750925926,
		acceleration: 0.0494444444444446
	},
	{
		id: 1493,
		time: 1492,
		velocity: 2.12138888888889,
		power: 111.394832798808,
		road: 14128.8881018519,
		acceleration: -0.0743518518518518
	},
	{
		id: 1494,
		time: 1493,
		velocity: 2.01527777777778,
		power: 161.456324411045,
		road: 14130.9405092593,
		acceleration: -0.0468518518518515
	},
	{
		id: 1495,
		time: 1494,
		velocity: 1.95027777777778,
		power: 1.32131852339278,
		road: 14132.9050462963,
		acceleration: -0.128888888888889
	},
	{
		id: 1496,
		time: 1495,
		velocity: 1.73472222222222,
		power: -455.118089121007,
		road: 14134.5989351852,
		acceleration: -0.412407407407407
	},
	{
		id: 1497,
		time: 1496,
		velocity: 0.778055555555556,
		power: -575.132393310399,
		road: 14135.7615740741,
		acceleration: -0.650092592592593
	},
	{
		id: 1498,
		time: 1497,
		velocity: 0,
		power: -234.171645418469,
		road: 14136.3100462963,
		acceleration: -0.578240740740741
	},
	{
		id: 1499,
		time: 1498,
		velocity: 0,
		power: -16.1944301332034,
		road: 14136.4397222222,
		acceleration: -0.259351851851852
	},
	{
		id: 1500,
		time: 1499,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1501,
		time: 1500,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1502,
		time: 1501,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1503,
		time: 1502,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1504,
		time: 1503,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1505,
		time: 1504,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1506,
		time: 1505,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1507,
		time: 1506,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1508,
		time: 1507,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1509,
		time: 1508,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1510,
		time: 1509,
		velocity: 0,
		power: 0,
		road: 14136.4397222222,
		acceleration: 0
	},
	{
		id: 1511,
		time: 1510,
		velocity: 0,
		power: 8.74079321708616,
		road: 14136.4828703704,
		acceleration: 0.0862962962962963
	},
	{
		id: 1512,
		time: 1511,
		velocity: 0.258888888888889,
		power: 10.4264857194225,
		road: 14136.5691666667,
		acceleration: 0
	},
	{
		id: 1513,
		time: 1512,
		velocity: 0,
		power: 10.4264857194225,
		road: 14136.655462963,
		acceleration: 0
	},
	{
		id: 1514,
		time: 1513,
		velocity: 0,
		power: 1.68551806367771,
		road: 14136.6986111111,
		acceleration: -0.0862962962962963
	},
	{
		id: 1515,
		time: 1514,
		velocity: 0,
		power: 0,
		road: 14136.6986111111,
		acceleration: 0
	},
	{
		id: 1516,
		time: 1515,
		velocity: 0,
		power: 0,
		road: 14136.6986111111,
		acceleration: 0
	},
	{
		id: 1517,
		time: 1516,
		velocity: 0,
		power: 0,
		road: 14136.6986111111,
		acceleration: 0
	},
	{
		id: 1518,
		time: 1517,
		velocity: 0,
		power: 0,
		road: 14136.6986111111,
		acceleration: 0
	},
	{
		id: 1519,
		time: 1518,
		velocity: 0,
		power: 0,
		road: 14136.6986111111,
		acceleration: 0
	},
	{
		id: 1520,
		time: 1519,
		velocity: 0,
		power: 0,
		road: 14136.6986111111,
		acceleration: 0
	},
	{
		id: 1521,
		time: 1520,
		velocity: 0,
		power: 0,
		road: 14136.6986111111,
		acceleration: 0
	},
	{
		id: 1522,
		time: 1521,
		velocity: 0,
		power: 0,
		road: 14136.6986111111,
		acceleration: 0
	},
	{
		id: 1523,
		time: 1522,
		velocity: 0,
		power: 316.960285829512,
		road: 14137.0768981481,
		acceleration: 0.756574074074074
	},
	{
		id: 1524,
		time: 1523,
		velocity: 2.26972222222222,
		power: 2000.83409771093,
		road: 14138.5068981481,
		acceleration: 1.34685185185185
	},
	{
		id: 1525,
		time: 1524,
		velocity: 4.04055555555556,
		power: 4137.66944025981,
		road: 14141.3194444444,
		acceleration: 1.41824074074074
	},
	{
		id: 1526,
		time: 1525,
		velocity: 4.25472222222222,
		power: 2516.4275579601,
		road: 14145.1218518519,
		acceleration: 0.561481481481482
	},
	{
		id: 1527,
		time: 1526,
		velocity: 3.95416666666667,
		power: 322.342621265222,
		road: 14149.1785185185,
		acceleration: -0.0529629629629635
	},
	{
		id: 1528,
		time: 1527,
		velocity: 3.88166666666667,
		power: -8.66794619068578,
		road: 14153.1394444444,
		acceleration: -0.138518518518518
	},
	{
		id: 1529,
		time: 1528,
		velocity: 3.83916666666667,
		power: 643.79212292763,
		road: 14157.0498148148,
		acceleration: 0.0374074074074073
	},
	{
		id: 1530,
		time: 1529,
		velocity: 4.06638888888889,
		power: 978.571317392344,
		road: 14161.0398611111,
		acceleration: 0.121944444444444
	},
	{
		id: 1531,
		time: 1530,
		velocity: 4.2475,
		power: 976.361849272875,
		road: 14165.1475925926,
		acceleration: 0.113425925925926
	},
	{
		id: 1532,
		time: 1531,
		velocity: 4.17944444444444,
		power: 483.131495814675,
		road: 14169.3046759259,
		acceleration: -0.0147222222222219
	},
	{
		id: 1533,
		time: 1532,
		velocity: 4.02222222222222,
		power: 81.4874186841165,
		road: 14173.3964814815,
		acceleration: -0.115833333333334
	},
	{
		id: 1534,
		time: 1533,
		velocity: 3.9,
		power: 58.2377760610117,
		road: 14177.3699537037,
		acceleration: -0.120833333333334
	},
	{
		id: 1535,
		time: 1534,
		velocity: 3.81694444444444,
		power: -34.2988851779603,
		road: 14181.210462963,
		acceleration: -0.145092592592592
	},
	{
		id: 1536,
		time: 1535,
		velocity: 3.58694444444444,
		power: -15.3101821431498,
		road: 14184.9087037037,
		acceleration: -0.139444444444444
	},
	{
		id: 1537,
		time: 1536,
		velocity: 3.48166666666667,
		power: 26.6501077790457,
		road: 14188.4738888889,
		acceleration: -0.126666666666666
	},
	{
		id: 1538,
		time: 1537,
		velocity: 3.43694444444444,
		power: 261.449943746664,
		road: 14191.948287037,
		acceleration: -0.0549074074074074
	},
	{
		id: 1539,
		time: 1538,
		velocity: 3.42222222222222,
		power: 257.230269936003,
		road: 14195.3678703704,
		acceleration: -0.0547222222222228
	},
	{
		id: 1540,
		time: 1539,
		velocity: 3.3175,
		power: 267.439719479917,
		road: 14198.7350462963,
		acceleration: -0.0500925925925921
	},
	{
		id: 1541,
		time: 1540,
		velocity: 3.28666666666667,
		power: 265.228832200313,
		road: 14202.0525,
		acceleration: -0.0493518518518519
	},
	{
		id: 1542,
		time: 1541,
		velocity: 3.27416666666667,
		power: 412.771786322109,
		road: 14205.3445833333,
		acceleration: -0.00138888888888866
	},
	{
		id: 1543,
		time: 1542,
		velocity: 3.31333333333333,
		power: 366.575034562748,
		road: 14208.6280555556,
		acceleration: -0.0158333333333331
	},
	{
		id: 1544,
		time: 1543,
		velocity: 3.23916666666667,
		power: 558.210263757291,
		road: 14211.9260185185,
		acceleration: 0.0448148148148149
	},
	{
		id: 1545,
		time: 1544,
		velocity: 3.40861111111111,
		power: 363.031721689796,
		road: 14215.2373611111,
		acceleration: -0.0180555555555566
	},
	{
		id: 1546,
		time: 1545,
		velocity: 3.25916666666667,
		power: 475.038368803447,
		road: 14218.5484722222,
		acceleration: 0.0175925925925933
	},
	{
		id: 1547,
		time: 1546,
		velocity: 3.29194444444444,
		power: 385.540900097919,
		road: 14221.8628703704,
		acceleration: -0.0110185185185183
	},
	{
		id: 1548,
		time: 1547,
		velocity: 3.37555555555556,
		power: 547.701831394457,
		road: 14225.1916203704,
		acceleration: 0.0397222222222222
	},
	{
		id: 1549,
		time: 1548,
		velocity: 3.37833333333333,
		power: 177.57285895086,
		road: 14228.501712963,
		acceleration: -0.0770370370370372
	},
	{
		id: 1550,
		time: 1549,
		velocity: 3.06083333333333,
		power: -140.808827381324,
		road: 14231.6834259259,
		acceleration: -0.179722222222222
	},
	{
		id: 1551,
		time: 1550,
		velocity: 2.83638888888889,
		power: -227.93408733167,
		road: 14234.6688425926,
		acceleration: -0.21287037037037
	},
	{
		id: 1552,
		time: 1551,
		velocity: 2.73972222222222,
		power: 94.4103622266705,
		road: 14237.4994444444,
		acceleration: -0.096759259259259
	},
	{
		id: 1553,
		time: 1552,
		velocity: 2.77055555555556,
		power: 399.887389356322,
		road: 14240.29125,
		acceleration: 0.0191666666666666
	},
	{
		id: 1554,
		time: 1553,
		velocity: 2.89388888888889,
		power: 303.854262795527,
		road: 14243.0840740741,
		acceleration: -0.0171296296296295
	},
	{
		id: 1555,
		time: 1554,
		velocity: 2.68833333333333,
		power: -6.00753889974005,
		road: 14245.8013888889,
		acceleration: -0.133888888888889
	},
	{
		id: 1556,
		time: 1555,
		velocity: 2.36888888888889,
		power: -268.378988950098,
		road: 14248.3303240741,
		acceleration: -0.24287037037037
	},
	{
		id: 1557,
		time: 1556,
		velocity: 2.16527777777778,
		power: 28.3832567061605,
		road: 14250.6789351852,
		acceleration: -0.117777777777778
	},
	{
		id: 1558,
		time: 1557,
		velocity: 2.335,
		power: 378.866860993237,
		road: 14252.9898611111,
		acceleration: 0.0424074074074077
	},
	{
		id: 1559,
		time: 1558,
		velocity: 2.49611111111111,
		power: 567.439484984333,
		road: 14255.381712963,
		acceleration: 0.119444444444444
	},
	{
		id: 1560,
		time: 1559,
		velocity: 2.52361111111111,
		power: -303.12851631548,
		road: 14257.6991203704,
		acceleration: -0.268333333333334
	},
	{
		id: 1561,
		time: 1560,
		velocity: 1.53,
		power: -983.32763737631,
		road: 14259.5353240741,
		acceleration: -0.694074074074074
	},
	{
		id: 1562,
		time: 1561,
		velocity: 0.413888888888889,
		power: -722.225863835335,
		road: 14260.6038888889,
		acceleration: -0.841203703703704
	},
	{
		id: 1563,
		time: 1562,
		velocity: 0,
		power: -142.382139561761,
		road: 14260.9968518518,
		acceleration: -0.51
	},
	{
		id: 1564,
		time: 1563,
		velocity: 0,
		power: -0.681803281351526,
		road: 14261.0658333333,
		acceleration: -0.137962962962963
	},
	{
		id: 1565,
		time: 1564,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1566,
		time: 1565,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1567,
		time: 1566,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1568,
		time: 1567,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1569,
		time: 1568,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1570,
		time: 1569,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1571,
		time: 1570,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1572,
		time: 1571,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1573,
		time: 1572,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1574,
		time: 1573,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1575,
		time: 1574,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1576,
		time: 1575,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1577,
		time: 1576,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1578,
		time: 1577,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1579,
		time: 1578,
		velocity: 0,
		power: 0,
		road: 14261.0658333333,
		acceleration: 0
	},
	{
		id: 1580,
		time: 1579,
		velocity: 0,
		power: 210.751456893969,
		road: 14261.3689351852,
		acceleration: 0.606203703703704
	},
	{
		id: 1581,
		time: 1580,
		velocity: 1.81861111111111,
		power: 1605.6856750885,
		road: 14262.599212963,
		acceleration: 1.24814814814815
	},
	{
		id: 1582,
		time: 1581,
		velocity: 3.74444444444444,
		power: 3906.37941080118,
		road: 14265.184212963,
		acceleration: 1.4612962962963
	},
	{
		id: 1583,
		time: 1582,
		velocity: 4.38388888888889,
		power: 4249.32539080005,
		road: 14269.0159722222,
		acceleration: 1.03222222222222
	},
	{
		id: 1584,
		time: 1583,
		velocity: 4.91527777777778,
		power: 2342.12049956257,
		road: 14273.5653240741,
		acceleration: 0.402962962962963
	},
	{
		id: 1585,
		time: 1584,
		velocity: 4.95333333333333,
		power: 1783.26048055601,
		road: 14278.4383796296,
		acceleration: 0.244444444444444
	},
	{
		id: 1586,
		time: 1585,
		velocity: 5.11722222222222,
		power: 654.655578249922,
		road: 14283.4319444444,
		acceleration: -0.00342592592592528
	},
	{
		id: 1587,
		time: 1586,
		velocity: 4.905,
		power: 999.297012768036,
		road: 14288.4576388889,
		acceleration: 0.0676851851851845
	},
	{
		id: 1588,
		time: 1587,
		velocity: 5.15638888888889,
		power: 3051.91633642396,
		road: 14293.7490740741,
		acceleration: 0.463796296296297
	},
	{
		id: 1589,
		time: 1588,
		velocity: 6.50861111111111,
		power: 4942.76701853301,
		road: 14299.6402314815,
		acceleration: 0.735648148148148
	},
	{
		id: 1590,
		time: 1589,
		velocity: 7.11194444444445,
		power: 6379.00365100958,
		road: 14306.3246296296,
		acceleration: 0.850833333333333
	},
	{
		id: 1591,
		time: 1590,
		velocity: 7.70888888888889,
		power: 4451.24691580249,
		road: 14313.6738425926,
		acceleration: 0.478796296296296
	},
	{
		id: 1592,
		time: 1591,
		velocity: 7.945,
		power: 3113.63864569628,
		road: 14321.3938888889,
		acceleration: 0.262870370370371
	},
	{
		id: 1593,
		time: 1592,
		velocity: 7.90055555555556,
		power: 1585.3175703749,
		road: 14329.2699537037,
		acceleration: 0.0491666666666655
	},
	{
		id: 1594,
		time: 1593,
		velocity: 7.85638888888889,
		power: 563.923089873952,
		road: 14337.1272222222,
		acceleration: -0.0867592592592583
	},
	{
		id: 1595,
		time: 1594,
		velocity: 7.68472222222222,
		power: 419.498749243515,
		road: 14344.8888425926,
		acceleration: -0.104537037037037
	},
	{
		id: 1596,
		time: 1595,
		velocity: 7.58694444444444,
		power: 546.76572988702,
		road: 14352.5554166667,
		acceleration: -0.0855555555555565
	},
	{
		id: 1597,
		time: 1596,
		velocity: 7.59972222222222,
		power: 1417.84400129261,
		road: 14360.1965740741,
		acceleration: 0.0347222222222232
	},
	{
		id: 1598,
		time: 1597,
		velocity: 7.78888888888889,
		power: 1489.13782334035,
		road: 14367.8766666667,
		acceleration: 0.0431481481481484
	},
	{
		id: 1599,
		time: 1598,
		velocity: 7.71638888888889,
		power: 560.714566976607,
		road: 14375.5365740741,
		acceleration: -0.0835185185185194
	},
	{
		id: 1600,
		time: 1599,
		velocity: 7.34916666666667,
		power: -184.007729205887,
		road: 14383.0622222222,
		acceleration: -0.184999999999999
	},
	{
		id: 1601,
		time: 1600,
		velocity: 7.23388888888889,
		power: -1058.30816079621,
		road: 14390.3403240741,
		acceleration: -0.310092592592594
	},
	{
		id: 1602,
		time: 1601,
		velocity: 6.78611111111111,
		power: -752.768096709599,
		road: 14397.3293055556,
		acceleration: -0.268148148148148
	},
	{
		id: 1603,
		time: 1602,
		velocity: 6.54472222222222,
		power: -1239.29546245929,
		road: 14404.0104166667,
		acceleration: -0.347592592592592
	},
	{
		id: 1604,
		time: 1603,
		velocity: 6.19111111111111,
		power: -913.530709184168,
		road: 14410.3670833333,
		acceleration: -0.301296296296297
	},
	{
		id: 1605,
		time: 1604,
		velocity: 5.88222222222222,
		power: -781.752924669687,
		road: 14416.43125,
		acceleration: -0.283703703703704
	},
	{
		id: 1606,
		time: 1605,
		velocity: 5.69361111111111,
		power: -1071.53825424438,
		road: 14422.1825462963,
		acceleration: -0.342037037037036
	},
	{
		id: 1607,
		time: 1606,
		velocity: 5.165,
		power: -1129.97886518906,
		road: 14427.580787037,
		acceleration: -0.364074074074074
	},
	{
		id: 1608,
		time: 1607,
		velocity: 4.79,
		power: -1713.54008218279,
		road: 14432.5446759259,
		acceleration: -0.50462962962963
	},
	{
		id: 1609,
		time: 1608,
		velocity: 4.17972222222222,
		power: -961.946647764048,
		road: 14437.075,
		acceleration: -0.362500000000001
	},
	{
		id: 1610,
		time: 1609,
		velocity: 4.0775,
		power: -568.688793377631,
		road: 14441.2842592593,
		acceleration: -0.27962962962963
	},
	{
		id: 1611,
		time: 1610,
		velocity: 3.95111111111111,
		power: 221.976532822138,
		road: 14445.3144444444,
		acceleration: -0.0785185185185182
	},
	{
		id: 1612,
		time: 1611,
		velocity: 3.94416666666667,
		power: 338.515942783793,
		road: 14449.2821759259,
		acceleration: -0.0463888888888886
	},
	{
		id: 1613,
		time: 1612,
		velocity: 3.93833333333333,
		power: -133.558477704821,
		road: 14453.1406018519,
		acceleration: -0.172222222222222
	},
	{
		id: 1614,
		time: 1613,
		velocity: 3.43444444444444,
		power: -392.918889986316,
		road: 14456.78875,
		acceleration: -0.248333333333333
	},
	{
		id: 1615,
		time: 1614,
		velocity: 3.19916666666667,
		power: -846.023772688795,
		road: 14460.1118055556,
		acceleration: -0.401851851851852
	},
	{
		id: 1616,
		time: 1615,
		velocity: 2.73277777777778,
		power: -244.611667371944,
		road: 14463.1249074074,
		acceleration: -0.218055555555555
	},
	{
		id: 1617,
		time: 1616,
		velocity: 2.78027777777778,
		power: 88.8933038128159,
		road: 14465.9793981481,
		acceleration: -0.0991666666666671
	},
	{
		id: 1618,
		time: 1617,
		velocity: 2.90166666666667,
		power: 652.55568488262,
		road: 14468.8385648148,
		acceleration: 0.108518518518519
	},
	{
		id: 1619,
		time: 1618,
		velocity: 3.05833333333333,
		power: 464.592911363772,
		road: 14471.7693981481,
		acceleration: 0.0348148148148142
	},
	{
		id: 1620,
		time: 1619,
		velocity: 2.88472222222222,
		power: 312.206756152228,
		road: 14474.7075,
		acceleration: -0.020277777777777
	},
	{
		id: 1621,
		time: 1620,
		velocity: 2.84083333333333,
		power: -4.354971067473,
		road: 14477.5686574074,
		acceleration: -0.133611111111112
	},
	{
		id: 1622,
		time: 1621,
		velocity: 2.6575,
		power: -50.664177535594,
		road: 14480.2874074074,
		acceleration: -0.151203703703703
	},
	{
		id: 1623,
		time: 1622,
		velocity: 2.43111111111111,
		power: -107.371390267136,
		road: 14482.8428703704,
		acceleration: -0.17537037037037
	},
	{
		id: 1624,
		time: 1623,
		velocity: 2.31472222222222,
		power: -20.6418744740663,
		road: 14485.240787037,
		acceleration: -0.139722222222222
	},
	{
		id: 1625,
		time: 1624,
		velocity: 2.23833333333333,
		power: 264.596890332227,
		road: 14487.5636574074,
		acceleration: -0.0103703703703704
	},
	{
		id: 1626,
		time: 1625,
		velocity: 2.4,
		power: 555.783145161531,
		road: 14489.9393518519,
		acceleration: 0.116018518518518
	},
	{
		id: 1627,
		time: 1626,
		velocity: 2.66277777777778,
		power: 886.795018519602,
		road: 14492.4906944444,
		acceleration: 0.235277777777778
	},
	{
		id: 1628,
		time: 1627,
		velocity: 2.94416666666667,
		power: 1367.71838166031,
		road: 14495.3460648148,
		acceleration: 0.372777777777778
	},
	{
		id: 1629,
		time: 1628,
		velocity: 3.51833333333333,
		power: 3373.51407860482,
		road: 14498.8306018519,
		acceleration: 0.885555555555555
	},
	{
		id: 1630,
		time: 1629,
		velocity: 5.31944444444444,
		power: 5122.10837026737,
		road: 14503.2928240741,
		acceleration: 1.06981481481481
	},
	{
		id: 1631,
		time: 1630,
		velocity: 6.15361111111111,
		power: 6261.7569322233,
		road: 14508.8142592593,
		acceleration: 1.04861111111111
	},
	{
		id: 1632,
		time: 1631,
		velocity: 6.66416666666667,
		power: 3028.48614199773,
		road: 14515.0411574074,
		acceleration: 0.362314814814815
	},
	{
		id: 1633,
		time: 1632,
		velocity: 6.40638888888889,
		power: -1236.05937233657,
		road: 14521.2702314815,
		acceleration: -0.357962962962963
	},
	{
		id: 1634,
		time: 1633,
		velocity: 5.07972222222222,
		power: -2205.23342817778,
		road: 14527.0463888889,
		acceleration: -0.54787037037037
	},
	{
		id: 1635,
		time: 1634,
		velocity: 5.02055555555556,
		power: -3857.71223259782,
		road: 14532.0739351852,
		acceleration: -0.949351851851851
	},
	{
		id: 1636,
		time: 1635,
		velocity: 3.55833333333333,
		power: -2823.12224441597,
		road: 14536.1978240741,
		acceleration: -0.857962962962964
	},
	{
		id: 1637,
		time: 1636,
		velocity: 2.50583333333333,
		power: -2292.35040502767,
		road: 14539.4552777778,
		acceleration: -0.874907407407407
	},
	{
		id: 1638,
		time: 1637,
		velocity: 2.39583333333333,
		power: -524.339561833851,
		road: 14542.1053240741,
		acceleration: -0.339907407407408
	},
	{
		id: 1639,
		time: 1638,
		velocity: 2.53861111111111,
		power: 1258.95344273936,
		road: 14544.7687962963,
		acceleration: 0.366759259259259
	},
	{
		id: 1640,
		time: 1639,
		velocity: 3.60611111111111,
		power: 2723.69776219418,
		road: 14547.9938888889,
		acceleration: 0.756481481481482
	},
	{
		id: 1641,
		time: 1640,
		velocity: 4.66527777777778,
		power: 3099.80813667785,
		road: 14551.9425,
		acceleration: 0.690555555555556
	},
	{
		id: 1642,
		time: 1641,
		velocity: 4.61027777777778,
		power: 1642.19461410552,
		road: 14556.3627777778,
		acceleration: 0.252777777777777
	},
	{
		id: 1643,
		time: 1642,
		velocity: 4.36444444444444,
		power: -218.75964412641,
		road: 14560.8143055556,
		acceleration: -0.190277777777777
	},
	{
		id: 1644,
		time: 1643,
		velocity: 4.09444444444444,
		power: -130.468933569462,
		road: 14565.085787037,
		acceleration: -0.169814814814815
	},
	{
		id: 1645,
		time: 1644,
		velocity: 4.10083333333333,
		power: 289.192988281811,
		road: 14569.240462963,
		acceleration: -0.0637962962962968
	},
	{
		id: 1646,
		time: 1645,
		velocity: 4.17305555555556,
		power: 1317.16567995786,
		road: 14573.4589351852,
		acceleration: 0.191388888888889
	},
	{
		id: 1647,
		time: 1646,
		velocity: 4.66861111111111,
		power: 2861.02301644385,
		road: 14578.0327777778,
		acceleration: 0.519351851851853
	},
	{
		id: 1648,
		time: 1647,
		velocity: 5.65888888888889,
		power: 3677.8743826511,
		road: 14583.1717592593,
		acceleration: 0.610925925925925
	},
	{
		id: 1649,
		time: 1648,
		velocity: 6.00583333333333,
		power: 3742.01808049578,
		road: 14588.8876851852,
		acceleration: 0.542962962962963
	},
	{
		id: 1650,
		time: 1649,
		velocity: 6.2975,
		power: 1445.16489897554,
		road: 14594.9269907407,
		acceleration: 0.103796296296296
	},
	{
		id: 1651,
		time: 1650,
		velocity: 5.97027777777778,
		power: -83.8966900059433,
		road: 14600.9369907407,
		acceleration: -0.162407407407407
	},
	{
		id: 1652,
		time: 1651,
		velocity: 5.51861111111111,
		power: -1341.14630650696,
		road: 14606.6697685185,
		acceleration: -0.392037037037036
	},
	{
		id: 1653,
		time: 1652,
		velocity: 5.12138888888889,
		power: -893.026867160839,
		road: 14612.0473148148,
		acceleration: -0.318425925925927
	},
	{
		id: 1654,
		time: 1653,
		velocity: 5.015,
		power: -720.161297641025,
		road: 14617.12,
		acceleration: -0.291296296296296
	},
	{
		id: 1655,
		time: 1654,
		velocity: 4.64472222222222,
		power: -262.693467580508,
		road: 14621.9481481481,
		acceleration: -0.197777777777778
	},
	{
		id: 1656,
		time: 1655,
		velocity: 4.52805555555555,
		power: -318.919764655324,
		road: 14626.5713888889,
		acceleration: -0.212037037037036
	},
	{
		id: 1657,
		time: 1656,
		velocity: 4.37888888888889,
		power: -229.161776806344,
		road: 14630.9921296296,
		acceleration: -0.192962962962964
	},
	{
		id: 1658,
		time: 1657,
		velocity: 4.06583333333333,
		power: -770.369992245362,
		road: 14635.1502777778,
		acceleration: -0.332222222222222
	},
	{
		id: 1659,
		time: 1658,
		velocity: 3.53138888888889,
		power: -1762.77659843645,
		road: 14638.8219444444,
		acceleration: -0.64074074074074
	},
	{
		id: 1660,
		time: 1659,
		velocity: 2.45666666666667,
		power: -1468.1412465976,
		road: 14641.8516203704,
		acceleration: -0.64324074074074
	},
	{
		id: 1661,
		time: 1660,
		velocity: 2.13611111111111,
		power: -987.934111191547,
		road: 14644.2798611111,
		acceleration: -0.55962962962963
	},
	{
		id: 1662,
		time: 1661,
		velocity: 1.8525,
		power: 511.137622514824,
		road: 14646.4853703704,
		acceleration: 0.114166666666667
	},
	{
		id: 1663,
		time: 1662,
		velocity: 2.79916666666667,
		power: 965.227704678012,
		road: 14648.8938425926,
		acceleration: 0.291759259259259
	},
	{
		id: 1664,
		time: 1663,
		velocity: 3.01138888888889,
		power: 1260.58905709118,
		road: 14651.6255555556,
		acceleration: 0.354722222222222
	},
	{
		id: 1665,
		time: 1664,
		velocity: 2.91666666666667,
		power: 325.909724346675,
		road: 14654.5277314815,
		acceleration: -0.0137962962962961
	},
	{
		id: 1666,
		time: 1665,
		velocity: 2.75777777777778,
		power: 1049.63766020839,
		road: 14657.5403240741,
		acceleration: 0.234629629629629
	},
	{
		id: 1667,
		time: 1666,
		velocity: 3.71527777777778,
		power: 1944.31608165446,
		road: 14660.9075,
		acceleration: 0.474537037037037
	},
	{
		id: 1668,
		time: 1667,
		velocity: 4.34027777777778,
		power: 3685.90702572495,
		road: 14664.9265740741,
		acceleration: 0.829259259259259
	},
	{
		id: 1669,
		time: 1668,
		velocity: 5.24555555555556,
		power: 2865.78888406213,
		road: 14669.6123148148,
		acceleration: 0.504074074074074
	},
	{
		id: 1670,
		time: 1669,
		velocity: 5.2275,
		power: 2335.34359711023,
		road: 14674.7196759259,
		acceleration: 0.339166666666667
	},
	{
		id: 1671,
		time: 1670,
		velocity: 5.35777777777778,
		power: 1859.99115156419,
		road: 14680.1064351852,
		acceleration: 0.21962962962963
	},
	{
		id: 1672,
		time: 1671,
		velocity: 5.90444444444444,
		power: 1591.1346786819,
		road: 14685.6807407407,
		acceleration: 0.155462962962962
	},
	{
		id: 1673,
		time: 1672,
		velocity: 5.69388888888889,
		power: 1411.33084210402,
		road: 14691.3899537037,
		acceleration: 0.114351851851851
	},
	{
		id: 1674,
		time: 1673,
		velocity: 5.70083333333333,
		power: -403.039859259806,
		road: 14697.0461574074,
		acceleration: -0.22037037037037
	},
	{
		id: 1675,
		time: 1674,
		velocity: 5.24333333333333,
		power: -1269.47714931867,
		road: 14702.3955555556,
		acceleration: -0.39324074074074
	},
	{
		id: 1676,
		time: 1675,
		velocity: 4.51416666666667,
		power: -2946.60358479259,
		road: 14707.1521759259,
		acceleration: -0.792314814814815
	},
	{
		id: 1677,
		time: 1676,
		velocity: 3.32388888888889,
		power: -3542.91880186474,
		road: 14710.9540277778,
		acceleration: -1.11722222222222
	},
	{
		id: 1678,
		time: 1677,
		velocity: 1.89166666666667,
		power: -2564.56725243244,
		road: 14713.6256018519,
		acceleration: -1.14333333333333
	},
	{
		id: 1679,
		time: 1678,
		velocity: 1.08416666666667,
		power: -1435.07850525302,
		road: 14715.1715277778,
		acceleration: -1.10796296296296
	},
	{
		id: 1680,
		time: 1679,
		velocity: 0,
		power: -322.417880515678,
		road: 14715.8481944444,
		acceleration: -0.630555555555556
	},
	{
		id: 1681,
		time: 1680,
		velocity: 0,
		power: -40.0329492690059,
		road: 14716.0288888889,
		acceleration: -0.361388888888889
	},
	{
		id: 1682,
		time: 1681,
		velocity: 0,
		power: 0,
		road: 14716.0288888889,
		acceleration: 0
	},
	{
		id: 1683,
		time: 1682,
		velocity: 0,
		power: 0,
		road: 14716.0288888889,
		acceleration: 0
	},
	{
		id: 1684,
		time: 1683,
		velocity: 0,
		power: 0,
		road: 14716.0288888889,
		acceleration: 0
	},
	{
		id: 1685,
		time: 1684,
		velocity: 0,
		power: 44.7392731968428,
		road: 14716.1539351852,
		acceleration: 0.250092592592593
	},
	{
		id: 1686,
		time: 1685,
		velocity: 0.750277777777778,
		power: 572.807955163541,
		road: 14716.8045833333,
		acceleration: 0.801111111111111
	},
	{
		id: 1687,
		time: 1686,
		velocity: 2.40333333333333,
		power: 1874.48159807311,
		road: 14718.4077777778,
		acceleration: 1.10398148148148
	},
	{
		id: 1688,
		time: 1687,
		velocity: 3.31194444444444,
		power: 3523.25401438622,
		road: 14721.1693055556,
		acceleration: 1.21268518518519
	},
	{
		id: 1689,
		time: 1688,
		velocity: 4.38833333333333,
		power: 2836.28434441999,
		road: 14724.8729166667,
		acceleration: 0.671481481481482
	},
	{
		id: 1690,
		time: 1689,
		velocity: 4.41777777777778,
		power: 2283.22535981888,
		road: 14729.1261111111,
		acceleration: 0.427685185185185
	},
	{
		id: 1691,
		time: 1690,
		velocity: 4.595,
		power: 2551.03209623915,
		road: 14733.8099537037,
		acceleration: 0.433611111111111
	},
	{
		id: 1692,
		time: 1691,
		velocity: 5.68916666666667,
		power: 3559.479551661,
		road: 14739.0001851852,
		acceleration: 0.579166666666668
	},
	{
		id: 1693,
		time: 1692,
		velocity: 6.15527777777778,
		power: 3505.85789864136,
		road: 14744.7289814815,
		acceleration: 0.497962962962962
	},
	{
		id: 1694,
		time: 1693,
		velocity: 6.08888888888889,
		power: 1261.34680326074,
		road: 14750.7431944444,
		acceleration: 0.0728703703703699
	},
	{
		id: 1695,
		time: 1694,
		velocity: 5.90777777777778,
		power: -412.345000669982,
		road: 14756.6837037037,
		acceleration: -0.220277777777778
	},
	{
		id: 1696,
		time: 1695,
		velocity: 5.49444444444444,
		power: -2131.33085859382,
		road: 14762.2398611111,
		acceleration: -0.548425925925926
	},
	{
		id: 1697,
		time: 1696,
		velocity: 4.44361111111111,
		power: -1172.96579176206,
		road: 14767.329537037,
		acceleration: -0.384537037037037
	},
	{
		id: 1698,
		time: 1697,
		velocity: 4.75416666666667,
		power: -895.815513977958,
		road: 14772.0572222222,
		acceleration: -0.339444444444443
	},
	{
		id: 1699,
		time: 1698,
		velocity: 4.47611111111111,
		power: 645.385678575032,
		road: 14776.6200925926,
		acceleration: 0.00981481481481428
	},
	{
		id: 1700,
		time: 1699,
		velocity: 4.47305555555556,
		power: 112.056477792087,
		road: 14781.1315277778,
		acceleration: -0.112685185185184
	},
	{
		id: 1701,
		time: 1700,
		velocity: 4.41611111111111,
		power: 453.883848961263,
		road: 14785.5712037037,
		acceleration: -0.0308333333333337
	},
	{
		id: 1702,
		time: 1701,
		velocity: 4.38361111111111,
		power: -89.6036524876389,
		road: 14789.9156018518,
		acceleration: -0.159722222222222
	},
	{
		id: 1703,
		time: 1702,
		velocity: 3.99388888888889,
		power: -890.587396212025,
		road: 14793.9968518518,
		acceleration: -0.366574074074074
	},
	{
		id: 1704,
		time: 1703,
		velocity: 3.31638888888889,
		power: -1599.8786464645,
		road: 14797.5931481481,
		acceleration: -0.603333333333334
	},
	{
		id: 1705,
		time: 1704,
		velocity: 2.57361111111111,
		power: -1058.66847399797,
		road: 14800.638287037,
		acceleration: -0.498981481481481
	},
	{
		id: 1706,
		time: 1705,
		velocity: 2.49694444444444,
		power: -464.656571699539,
		road: 14803.2754166667,
		acceleration: -0.317037037037037
	},
	{
		id: 1707,
		time: 1706,
		velocity: 2.36527777777778,
		power: 11.5676924169942,
		road: 14805.6912037037,
		acceleration: -0.125648148148148
	},
	{
		id: 1708,
		time: 1707,
		velocity: 2.19666666666667,
		power: 73.1417567126282,
		road: 14807.9956944444,
		acceleration: -0.0969444444444445
	},
	{
		id: 1709,
		time: 1708,
		velocity: 2.20611111111111,
		power: 143.270559466026,
		road: 14810.2205555556,
		acceleration: -0.0623148148148145
	},
	{
		id: 1710,
		time: 1709,
		velocity: 2.17833333333333,
		power: 342.603260944816,
		road: 14812.4308796296,
		acceleration: 0.03324074074074
	},
	{
		id: 1711,
		time: 1710,
		velocity: 2.29638888888889,
		power: 629.504181513401,
		road: 14814.7365277778,
		acceleration: 0.157407407407408
	},
	{
		id: 1712,
		time: 1711,
		velocity: 2.67833333333333,
		power: 1455.90290878039,
		road: 14817.3489351852,
		acceleration: 0.456111111111111
	},
	{
		id: 1713,
		time: 1712,
		velocity: 3.54666666666667,
		power: 2084.79074607539,
		road: 14820.4743518518,
		acceleration: 0.569907407407408
	},
	{
		id: 1714,
		time: 1713,
		velocity: 4.00611111111111,
		power: 2680.52594957763,
		road: 14824.1963888889,
		acceleration: 0.623333333333333
	},
	{
		id: 1715,
		time: 1714,
		velocity: 4.54833333333333,
		power: 2258.92304788379,
		road: 14828.4414814815,
		acceleration: 0.422777777777778
	},
	{
		id: 1716,
		time: 1715,
		velocity: 4.815,
		power: 1696.23262502152,
		road: 14833.0232407407,
		acceleration: 0.250555555555555
	},
	{
		id: 1717,
		time: 1716,
		velocity: 4.75777777777778,
		power: 1159.18227481658,
		road: 14837.7882407407,
		acceleration: 0.115925925925927
	},
	{
		id: 1718,
		time: 1717,
		velocity: 4.89611111111111,
		power: 1270.9377513445,
		road: 14842.6775925926,
		acceleration: 0.132777777777778
	},
	{
		id: 1719,
		time: 1718,
		velocity: 5.21333333333333,
		power: 1417.65227796922,
		road: 14847.7107407407,
		acceleration: 0.154814814814815
	},
	{
		id: 1720,
		time: 1719,
		velocity: 5.22222222222222,
		power: 1660.02065385895,
		road: 14852.9177314815,
		acceleration: 0.19287037037037
	},
	{
		id: 1721,
		time: 1720,
		velocity: 5.47472222222222,
		power: 2282.71611217286,
		road: 14858.3693981481,
		acceleration: 0.296481481481481
	},
	{
		id: 1722,
		time: 1721,
		velocity: 6.10277777777778,
		power: 916.962517474017,
		road: 14863.9826851852,
		acceleration: 0.0267592592592587
	},
	{
		id: 1723,
		time: 1722,
		velocity: 5.3025,
		power: 8.97538617884228,
		road: 14869.5378240741,
		acceleration: -0.143055555555556
	},
	{
		id: 1724,
		time: 1723,
		velocity: 5.04555555555556,
		power: -1711.4444334691,
		road: 14874.7781481481,
		acceleration: -0.486574074074074
	},
	{
		id: 1725,
		time: 1724,
		velocity: 4.64305555555556,
		power: -1626.98021866265,
		road: 14879.5247222222,
		acceleration: -0.500925925925925
	},
	{
		id: 1726,
		time: 1725,
		velocity: 3.79972222222222,
		power: -546.342133126514,
		road: 14883.8858333333,
		acceleration: -0.27
	},
	{
		id: 1727,
		time: 1726,
		velocity: 4.23555555555556,
		power: 601.60480327406,
		road: 14888.1180555556,
		acceleration: 0.0122222222222224
	},
	{
		id: 1728,
		time: 1727,
		velocity: 4.67972222222222,
		power: 1816.31387288406,
		road: 14892.5052314815,
		acceleration: 0.297685185185185
	},
	{
		id: 1729,
		time: 1728,
		velocity: 4.69277777777778,
		power: 1298.78640635692,
		road: 14897.1197222222,
		acceleration: 0.156944444444445
	},
	{
		id: 1730,
		time: 1729,
		velocity: 4.70638888888889,
		power: 108.174465871611,
		road: 14901.7552314815,
		acceleration: -0.114907407407408
	},
	{
		id: 1731,
		time: 1730,
		velocity: 4.335,
		power: 231.646330353569,
		road: 14906.2906944444,
		acceleration: -0.0851851851851855
	},
	{
		id: 1732,
		time: 1731,
		velocity: 4.43722222222222,
		power: 289.511681539533,
		road: 14910.7484722222,
		acceleration: -0.0701851851851849
	},
	{
		id: 1733,
		time: 1732,
		velocity: 4.49583333333333,
		power: 879.006090492108,
		road: 14915.2056944444,
		acceleration: 0.0690740740740736
	},
	{
		id: 1734,
		time: 1733,
		velocity: 4.54222222222222,
		power: 1083.65876101532,
		road: 14919.7533796296,
		acceleration: 0.111851851851852
	},
	{
		id: 1735,
		time: 1734,
		velocity: 4.77277777777778,
		power: 297.946018186642,
		road: 14924.3217592593,
		acceleration: -0.0704629629629627
	},
	{
		id: 1736,
		time: 1735,
		velocity: 4.28444444444444,
		power: 314.59183272125,
		road: 14928.8223148148,
		acceleration: -0.0651851851851859
	},
	{
		id: 1737,
		time: 1736,
		velocity: 4.34666666666667,
		power: 42.8369059964375,
		road: 14933.22625,
		acceleration: -0.128055555555554
	},
	{
		id: 1738,
		time: 1737,
		velocity: 4.38861111111111,
		power: 528.337757519764,
		road: 14937.5613425926,
		acceleration: -0.00962962962962965
	},
	{
		id: 1739,
		time: 1738,
		velocity: 4.25555555555556,
		power: 468.6332573305,
		road: 14941.8798148148,
		acceleration: -0.0236111111111112
	},
	{
		id: 1740,
		time: 1739,
		velocity: 4.27583333333333,
		power: 443.745682400296,
		road: 14946.172037037,
		acceleration: -0.0288888888888881
	},
	{
		id: 1741,
		time: 1740,
		velocity: 4.30194444444444,
		power: 1285.35467070733,
		road: 14950.5358333333,
		acceleration: 0.172037037037036
	},
	{
		id: 1742,
		time: 1741,
		velocity: 4.77166666666667,
		power: 2087.05407795927,
		road: 14955.1538425926,
		acceleration: 0.336388888888889
	},
	{
		id: 1743,
		time: 1742,
		velocity: 5.285,
		power: 3344.05633662264,
		road: 14960.216712963,
		acceleration: 0.553333333333334
	},
	{
		id: 1744,
		time: 1743,
		velocity: 5.96194444444444,
		power: 3203.29620721425,
		road: 14965.7863888889,
		acceleration: 0.460277777777778
	},
	{
		id: 1745,
		time: 1744,
		velocity: 6.1525,
		power: 4322.13141972606,
		road: 14971.8847685185,
		acceleration: 0.597129629629629
	},
	{
		id: 1746,
		time: 1745,
		velocity: 7.07638888888889,
		power: 2790.15333213101,
		road: 14978.4300925926,
		acceleration: 0.29675925925926
	},
	{
		id: 1747,
		time: 1746,
		velocity: 6.85222222222222,
		power: 2241.41625393113,
		road: 14985.2206481481,
		acceleration: 0.193703703703703
	},
	{
		id: 1748,
		time: 1747,
		velocity: 6.73361111111111,
		power: -438.508303297722,
		road: 14991.9974074074,
		acceleration: -0.221296296296297
	},
	{
		id: 1749,
		time: 1748,
		velocity: 6.4125,
		power: -528.328381693878,
		road: 14998.5453240741,
		acceleration: -0.236388888888889
	},
	{
		id: 1750,
		time: 1749,
		velocity: 6.14305555555555,
		power: -543.28317834357,
		road: 15004.8548611111,
		acceleration: -0.240370370370371
	},
	{
		id: 1751,
		time: 1750,
		velocity: 6.0125,
		power: 127.790048512175,
		road: 15010.9809259259,
		acceleration: -0.126574074074073
	},
	{
		id: 1752,
		time: 1751,
		velocity: 6.03277777777778,
		power: -89.8672889740879,
		road: 15016.962037037,
		acceleration: -0.163333333333334
	},
	{
		id: 1753,
		time: 1752,
		velocity: 5.65305555555556,
		power: 138.758785590095,
		road: 15022.8006944444,
		acceleration: -0.121574074074074
	},
	{
		id: 1754,
		time: 1753,
		velocity: 5.64777777777778,
		power: 490.199430162346,
		road: 15028.5504166667,
		acceleration: -0.0562962962962956
	},
	{
		id: 1755,
		time: 1754,
		velocity: 5.86388888888889,
		power: 2063.09342767503,
		road: 15034.3847222222,
		acceleration: 0.225462962962963
	},
	{
		id: 1756,
		time: 1755,
		velocity: 6.32944444444444,
		power: 2078.49832914511,
		road: 15040.4383333333,
		acceleration: 0.213148148148147
	},
	{
		id: 1757,
		time: 1756,
		velocity: 6.28722222222222,
		power: 952.211458888754,
		road: 15046.6053240741,
		acceleration: 0.0136111111111115
	},
	{
		id: 1758,
		time: 1757,
		velocity: 5.90472222222222,
		power: -144.082306741215,
		road: 15052.6925462963,
		acceleration: -0.173148148148148
	},
	{
		id: 1759,
		time: 1758,
		velocity: 5.81,
		power: -125.187435165773,
		road: 15058.6085185185,
		acceleration: -0.169351851851851
	},
	{
		id: 1760,
		time: 1759,
		velocity: 5.77916666666667,
		power: 510.634570486025,
		road: 15064.4129166667,
		acceleration: -0.053796296296297
	},
	{
		id: 1761,
		time: 1760,
		velocity: 5.74333333333333,
		power: 946.128155118792,
		road: 15070.2032407407,
		acceleration: 0.0256481481481492
	},
	{
		id: 1762,
		time: 1761,
		velocity: 5.88694444444444,
		power: 1222.84803007905,
		road: 15076.0432407407,
		acceleration: 0.0737037037037016
	},
	{
		id: 1763,
		time: 1762,
		velocity: 6.00027777777778,
		power: 1889.74220294554,
		road: 15082.0128703704,
		acceleration: 0.185555555555556
	},
	{
		id: 1764,
		time: 1763,
		velocity: 6.3,
		power: 2839.12470954636,
		road: 15088.2404166667,
		acceleration: 0.330277777777777
	},
	{
		id: 1765,
		time: 1764,
		velocity: 6.87777777777778,
		power: 3178.5188678036,
		road: 15094.8115740741,
		acceleration: 0.356944444444446
	},
	{
		id: 1766,
		time: 1765,
		velocity: 7.07111111111111,
		power: 2135.07258042874,
		road: 15101.6485185185,
		acceleration: 0.174629629629629
	},
	{
		id: 1767,
		time: 1766,
		velocity: 6.82388888888889,
		power: 573.864667916203,
		road: 15108.5394907407,
		acceleration: -0.0665740740740732
	},
	{
		id: 1768,
		time: 1767,
		velocity: 6.67805555555556,
		power: 396.492674252883,
		road: 15115.3510185185,
		acceleration: -0.0923148148148156
	},
	{
		id: 1769,
		time: 1768,
		velocity: 6.79416666666667,
		power: 2087.47984560888,
		road: 15122.1997222222,
		acceleration: 0.166666666666667
	},
	{
		id: 1770,
		time: 1769,
		velocity: 7.32388888888889,
		power: 3013.29988636738,
		road: 15129.2777314815,
		acceleration: 0.291944444444444
	},
	{
		id: 1771,
		time: 1770,
		velocity: 7.55388888888889,
		power: 3646.79641625012,
		road: 15136.6814351852,
		acceleration: 0.359444444444446
	},
	{
		id: 1772,
		time: 1771,
		velocity: 7.8725,
		power: 3031.98999256231,
		road: 15144.3910648148,
		acceleration: 0.252407407407407
	},
	{
		id: 1773,
		time: 1772,
		velocity: 8.08111111111111,
		power: 3270.96814092848,
		road: 15152.3609259259,
		acceleration: 0.268055555555556
	},
	{
		id: 1774,
		time: 1773,
		velocity: 8.35805555555556,
		power: 3369.29678929226,
		road: 15160.5968981481,
		acceleration: 0.264166666666666
	},
	{
		id: 1775,
		time: 1774,
		velocity: 8.665,
		power: 3167.63966823914,
		road: 15169.0771759259,
		acceleration: 0.224444444444444
	},
	{
		id: 1776,
		time: 1775,
		velocity: 8.75444444444444,
		power: 2374.67207302072,
		road: 15177.7290277778,
		acceleration: 0.118703703703703
	},
	{
		id: 1777,
		time: 1776,
		velocity: 8.71416666666667,
		power: 1211.3351704569,
		road: 15186.4283333333,
		acceleration: -0.0237962962962968
	},
	{
		id: 1778,
		time: 1777,
		velocity: 8.59361111111111,
		power: 953.866647852822,
		road: 15195.08875,
		acceleration: -0.0539814814814807
	},
	{
		id: 1779,
		time: 1778,
		velocity: 8.5925,
		power: 734.37475888038,
		road: 15203.6825462963,
		acceleration: -0.079259259259258
	},
	{
		id: 1780,
		time: 1779,
		velocity: 8.47638888888889,
		power: 308.693230027463,
		road: 15212.1718055555,
		acceleration: -0.129814814814814
	},
	{
		id: 1781,
		time: 1780,
		velocity: 8.20416666666667,
		power: -190.035117485918,
		road: 15220.5009259259,
		acceleration: -0.190462962962963
	},
	{
		id: 1782,
		time: 1781,
		velocity: 8.02111111111111,
		power: 1282.38677470969,
		road: 15228.7338425926,
		acceleration: -0.00194444444444564
	},
	{
		id: 1783,
		time: 1782,
		velocity: 8.47055555555555,
		power: 2806.69851185004,
		road: 15237.0596296296,
		acceleration: 0.187685185185186
	},
	{
		id: 1784,
		time: 1783,
		velocity: 8.76722222222222,
		power: 4347.47811198946,
		road: 15245.6601851852,
		acceleration: 0.361851851851849
	},
	{
		id: 1785,
		time: 1784,
		velocity: 9.10666666666667,
		power: 3858.3077229678,
		road: 15254.5825925926,
		acceleration: 0.281851851851853
	},
	{
		id: 1786,
		time: 1785,
		velocity: 9.31611111111111,
		power: 4373.16032528103,
		road: 15263.8071296296,
		acceleration: 0.322407407407407
	},
	{
		id: 1787,
		time: 1786,
		velocity: 9.73444444444444,
		power: 2326.90060222796,
		road: 15273.2337037037,
		acceleration: 0.081666666666667
	},
	{
		id: 1788,
		time: 1787,
		velocity: 9.35166666666666,
		power: 1953.45579731096,
		road: 15282.7201388889,
		acceleration: 0.0380555555555535
	},
	{
		id: 1789,
		time: 1788,
		velocity: 9.43027777777778,
		power: 1054.98279743882,
		road: 15292.1950462963,
		acceleration: -0.06111111111111
	},
	{
		id: 1790,
		time: 1789,
		velocity: 9.55111111111111,
		power: 1776.89218659071,
		road: 15301.6491666667,
		acceleration: 0.019537037037038
	},
	{
		id: 1791,
		time: 1790,
		velocity: 9.41027777777778,
		power: 702.84619755387,
		road: 15311.0635648148,
		acceleration: -0.0989814814814807
	},
	{
		id: 1792,
		time: 1791,
		velocity: 9.13333333333333,
		power: -1217.78986957586,
		road: 15320.2714351852,
		acceleration: -0.314074074074073
	},
	{
		id: 1793,
		time: 1792,
		velocity: 8.60888888888889,
		power: -2165.39883630089,
		road: 15329.1078703704,
		acceleration: -0.428796296296298
	},
	{
		id: 1794,
		time: 1793,
		velocity: 8.12388888888889,
		power: -3589.04713120594,
		road: 15337.4199074074,
		acceleration: -0.620000000000001
	},
	{
		id: 1795,
		time: 1794,
		velocity: 7.27333333333333,
		power: -3900.42196957328,
		road: 15345.0739351852,
		acceleration: -0.696018518518518
	},
	{
		id: 1796,
		time: 1795,
		velocity: 6.52083333333333,
		power: -5781.98602464602,
		road: 15351.8549537037,
		acceleration: -1.05
	},
	{
		id: 1797,
		time: 1796,
		velocity: 4.97388888888889,
		power: -4212.13127801676,
		road: 15357.655787037,
		acceleration: -0.910370370370371
	},
	{
		id: 1798,
		time: 1797,
		velocity: 4.54222222222222,
		power: -2696.10277153331,
		road: 15362.6463888889,
		acceleration: -0.710092592592592
	},
	{
		id: 1799,
		time: 1798,
		velocity: 4.39055555555556,
		power: -216.820332973857,
		road: 15367.1873148148,
		acceleration: -0.189259259259259
	},
	{
		id: 1800,
		time: 1799,
		velocity: 4.40611111111111,
		power: 1920.38899667075,
		road: 15371.7838888889,
		acceleration: 0.300555555555555
	},
	{
		id: 1801,
		time: 1800,
		velocity: 5.44388888888889,
		power: 3952.5454778393,
		road: 15376.8687962963,
		acceleration: 0.676111111111112
	},
	{
		id: 1802,
		time: 1801,
		velocity: 6.41888888888889,
		power: 5169.12497381039,
		road: 15382.6859259259,
		acceleration: 0.788333333333333
	},
	{
		id: 1803,
		time: 1802,
		velocity: 6.77111111111111,
		power: 5047.3862653912,
		road: 15389.2271759259,
		acceleration: 0.659907407407408
	},
	{
		id: 1804,
		time: 1803,
		velocity: 7.42361111111111,
		power: 4218.12845790359,
		road: 15396.3325,
		acceleration: 0.468240740740741
	},
	{
		id: 1805,
		time: 1804,
		velocity: 7.82361111111111,
		power: 4701.41069318417,
		road: 15403.9177314815,
		acceleration: 0.491574074074073
	},
	{
		id: 1806,
		time: 1805,
		velocity: 8.24583333333333,
		power: 4383.37353889331,
		road: 15411.9534259259,
		acceleration: 0.409351851851853
	},
	{
		id: 1807,
		time: 1806,
		velocity: 8.65166666666667,
		power: 5110.22581698627,
		road: 15420.4266203704,
		acceleration: 0.465648148148148
	},
	{
		id: 1808,
		time: 1807,
		velocity: 9.22055555555556,
		power: 5636.83683798392,
		road: 15429.377037037,
		acceleration: 0.488796296296297
	},
	{
		id: 1809,
		time: 1808,
		velocity: 9.71222222222222,
		power: 4900.66474759617,
		road: 15438.7575925926,
		acceleration: 0.37148148148148
	},
	{
		id: 1810,
		time: 1809,
		velocity: 9.76611111111111,
		power: 2665.36943022072,
		road: 15448.3794907407,
		acceleration: 0.111203703703705
	},
	{
		id: 1811,
		time: 1810,
		velocity: 9.55416666666667,
		power: 1358.55406717635,
		road: 15458.040787037,
		acceleration: -0.0324074074074083
	},
	{
		id: 1812,
		time: 1811,
		velocity: 9.615,
		power: 2627.16770517083,
		road: 15467.7378703704,
		acceleration: 0.103981481481481
	},
	{
		id: 1813,
		time: 1812,
		velocity: 10.0780555555556,
		power: 5182.05064688457,
		road: 15477.6692592592,
		acceleration: 0.364629629629629
	},
	{
		id: 1814,
		time: 1813,
		velocity: 10.6480555555556,
		power: 6800.73656756312,
		road: 15488.0332407407,
		acceleration: 0.500555555555556
	},
	{
		id: 1815,
		time: 1814,
		velocity: 11.1166666666667,
		power: 6199.71273251544,
		road: 15498.8513888889,
		acceleration: 0.407777777777779
	},
	{
		id: 1816,
		time: 1815,
		velocity: 11.3013888888889,
		power: 6294.71475156872,
		road: 15510.0685185185,
		acceleration: 0.390185185185183
	},
	{
		id: 1817,
		time: 1816,
		velocity: 11.8186111111111,
		power: 7544.59629336648,
		road: 15521.718287037,
		acceleration: 0.475092592592594
	},
	{
		id: 1818,
		time: 1817,
		velocity: 12.5419444444444,
		power: 8830.19201372232,
		road: 15533.8806944444,
		acceleration: 0.550185185185185
	},
	{
		id: 1819,
		time: 1818,
		velocity: 12.9519444444444,
		power: 7814.3663549364,
		road: 15546.532962963,
		acceleration: 0.429537037037036
	},
	{
		id: 1820,
		time: 1819,
		velocity: 13.1072222222222,
		power: 5007.4645774389,
		road: 15559.4913888889,
		acceleration: 0.182777777777778
	},
	{
		id: 1821,
		time: 1820,
		velocity: 13.0902777777778,
		power: 3816.26980569882,
		road: 15572.5818981481,
		acceleration: 0.0813888888888883
	},
	{
		id: 1822,
		time: 1821,
		velocity: 13.1961111111111,
		power: 4145.07800077464,
		road: 15585.7650925926,
		acceleration: 0.103981481481483
	},
	{
		id: 1823,
		time: 1822,
		velocity: 13.4191666666667,
		power: 3213.40483062158,
		road: 15599.0141203704,
		acceleration: 0.0276851851851845
	},
	{
		id: 1824,
		time: 1823,
		velocity: 13.1733333333333,
		power: 920.852620835763,
		road: 15612.2008333333,
		acceleration: -0.152314814814815
	},
	{
		id: 1825,
		time: 1824,
		velocity: 12.7391666666667,
		power: -1008.99379478478,
		road: 15625.159537037,
		acceleration: -0.303703703703704
	},
	{
		id: 1826,
		time: 1825,
		velocity: 12.5080555555556,
		power: -1166.03097227424,
		road: 15637.8092592592,
		acceleration: -0.314259259259259
	},
	{
		id: 1827,
		time: 1826,
		velocity: 12.2305555555556,
		power: 171.280908514542,
		road: 15650.2020833333,
		acceleration: -0.199537037037036
	},
	{
		id: 1828,
		time: 1827,
		velocity: 12.1405555555556,
		power: 303.491048054955,
		road: 15662.4025,
		acceleration: -0.185277777777779
	},
	{
		id: 1829,
		time: 1828,
		velocity: 11.9522222222222,
		power: 1258.29877001408,
		road: 15674.4602777778,
		acceleration: -0.0999999999999996
	},
	{
		id: 1830,
		time: 1829,
		velocity: 11.9305555555556,
		power: 883.54395276117,
		road: 15686.4029166667,
		acceleration: -0.130277777777779
	},
	{
		id: 1831,
		time: 1830,
		velocity: 11.7497222222222,
		power: 1018.61199382717,
		road: 15698.2225,
		acceleration: -0.115833333333333
	},
	{
		id: 1832,
		time: 1831,
		velocity: 11.6047222222222,
		power: 41.4606136879177,
		road: 15709.8839814815,
		acceleration: -0.200370370370369
	},
	{
		id: 1833,
		time: 1832,
		velocity: 11.3294444444444,
		power: 410.020268674596,
		road: 15721.3631481481,
		acceleration: -0.164259259259259
	},
	{
		id: 1834,
		time: 1833,
		velocity: 11.2569444444444,
		power: 714.042282696226,
		road: 15732.6933333333,
		acceleration: -0.133703703703704
	},
	{
		id: 1835,
		time: 1834,
		velocity: 11.2036111111111,
		power: 4620.4992853345,
		road: 15744.0694444444,
		acceleration: 0.225555555555554
	},
	{
		id: 1836,
		time: 1835,
		velocity: 12.0061111111111,
		power: 6638.44987842186,
		road: 15755.7539814815,
		acceleration: 0.391296296296296
	},
	{
		id: 1837,
		time: 1836,
		velocity: 12.4308333333333,
		power: 8949.89974959123,
		road: 15767.9144907407,
		acceleration: 0.560648148148148
	},
	{
		id: 1838,
		time: 1837,
		velocity: 12.8855555555556,
		power: 6771.61949653332,
		road: 15780.5280555555,
		acceleration: 0.345462962962962
	},
	{
		id: 1839,
		time: 1838,
		velocity: 13.0425,
		power: 6340.03977467363,
		road: 15793.4603240741,
		acceleration: 0.291944444444447
	},
	{
		id: 1840,
		time: 1839,
		velocity: 13.3066666666667,
		power: 5400.53179305427,
		road: 15806.6405092593,
		acceleration: 0.203888888888891
	},
	{
		id: 1841,
		time: 1840,
		velocity: 13.4972222222222,
		power: 6725.79411826221,
		road: 15820.0703703704,
		acceleration: 0.295462962962961
	},
	{
		id: 1842,
		time: 1841,
		velocity: 13.9288888888889,
		power: 7407.22586299669,
		road: 15833.8132407407,
		acceleration: 0.330555555555556
	},
	{
		id: 1843,
		time: 1842,
		velocity: 14.2983333333333,
		power: 6525.82482133342,
		road: 15847.8456481481,
		acceleration: 0.248518518518518
	},
	{
		id: 1844,
		time: 1843,
		velocity: 14.2427777777778,
		power: 6652.40044184845,
		road: 15862.125,
		acceleration: 0.245370370370372
	},
	{
		id: 1845,
		time: 1844,
		velocity: 14.665,
		power: 7729.83665102216,
		road: 15876.6815277778,
		acceleration: 0.308981481481482
	},
	{
		id: 1846,
		time: 1845,
		velocity: 15.2252777777778,
		power: 7714.15947088772,
		road: 15891.5383333333,
		acceleration: 0.291574074074072
	},
	{
		id: 1847,
		time: 1846,
		velocity: 15.1175,
		power: 4560.16831312336,
		road: 15906.5722222222,
		acceleration: 0.0625925925925941
	},
	{
		id: 1848,
		time: 1847,
		velocity: 14.8527777777778,
		power: -1152.12758667753,
		road: 15921.4706481481,
		acceleration: -0.333518518518519
	},
	{
		id: 1849,
		time: 1848,
		velocity: 14.2247222222222,
		power: -5825.53344945244,
		road: 15935.8683333333,
		acceleration: -0.667962962962964
	},
	{
		id: 1850,
		time: 1849,
		velocity: 13.1136111111111,
		power: -13701.6434277825,
		road: 15949.2825,
		acceleration: -1.29907407407407
	},
	{
		id: 1851,
		time: 1850,
		velocity: 10.9555555555556,
		power: -17709.356630387,
		road: 15961.1619444444,
		acceleration: -1.77037037037037
	},
	{
		id: 1852,
		time: 1851,
		velocity: 8.91361111111111,
		power: -16547.024999887,
		road: 15971.1987037037,
		acceleration: -1.915
	},
	{
		id: 1853,
		time: 1852,
		velocity: 7.36861111111111,
		power: -13982.7186900157,
		road: 15979.2874074074,
		acceleration: -1.98111111111111
	},
	{
		id: 1854,
		time: 1853,
		velocity: 5.01222222222222,
		power: -7815.64033885376,
		road: 15985.6659259259,
		acceleration: -1.43925925925926
	},
	{
		id: 1855,
		time: 1854,
		velocity: 4.59583333333333,
		power: -4339.70393429229,
		road: 15990.8096296296,
		acceleration: -1.03037037037037
	},
	{
		id: 1856,
		time: 1855,
		velocity: 4.2775,
		power: 197.973131705463,
		road: 15995.3912962963,
		acceleration: -0.0937037037037038
	},
	{
		id: 1857,
		time: 1856,
		velocity: 4.73111111111111,
		power: 2619.91152223931,
		road: 16000.1460648148,
		acceleration: 0.439907407407406
	},
	{
		id: 1858,
		time: 1857,
		velocity: 5.91555555555556,
		power: 5389.67987947381,
		road: 16005.5714351852,
		acceleration: 0.901296296296296
	},
	{
		id: 1859,
		time: 1858,
		velocity: 6.98138888888889,
		power: 5841.20850454506,
		road: 16011.8609722222,
		acceleration: 0.827037037037037
	},
	{
		id: 1860,
		time: 1859,
		velocity: 7.21222222222222,
		power: 4053.38595195479,
		road: 16018.7941203704,
		acceleration: 0.460185185185185
	},
	{
		id: 1861,
		time: 1860,
		velocity: 7.29611111111111,
		power: 2911.69715964,
		road: 16026.0884722222,
		acceleration: 0.262222222222223
	},
	{
		id: 1862,
		time: 1861,
		velocity: 7.76805555555556,
		power: 4621.10066518141,
		road: 16033.7506018518,
		acceleration: 0.473333333333333
	},
	{
		id: 1863,
		time: 1862,
		velocity: 8.63222222222222,
		power: 6059.10001123567,
		road: 16041.9546759259,
		acceleration: 0.610555555555556
	},
	{
		id: 1864,
		time: 1863,
		velocity: 9.12777777777778,
		power: 5756.3777064358,
		road: 16050.7233796296,
		acceleration: 0.518703703703704
	},
	{
		id: 1865,
		time: 1864,
		velocity: 9.32416666666667,
		power: 4338.08029424952,
		road: 16059.9118055555,
		acceleration: 0.320740740740742
	},
	{
		id: 1866,
		time: 1865,
		velocity: 9.59444444444444,
		power: 1887.39881838652,
		road: 16069.277962963,
		acceleration: 0.0347222222222197
	},
	{
		id: 1867,
		time: 1866,
		velocity: 9.23194444444444,
		power: 1452.16364098755,
		road: 16078.6543055555,
		acceleration: -0.0143518518518526
	},
	{
		id: 1868,
		time: 1867,
		velocity: 9.28111111111111,
		power: 2109.17339805678,
		road: 16088.0526851852,
		acceleration: 0.0584259259259277
	},
	{
		id: 1869,
		time: 1868,
		velocity: 9.76972222222222,
		power: 4791.80591855139,
		road: 16097.6525925926,
		acceleration: 0.344629629629628
	},
	{
		id: 1870,
		time: 1869,
		velocity: 10.2658333333333,
		power: 6445.92225964285,
		road: 16107.670462963,
		acceleration: 0.491296296296298
	},
	{
		id: 1871,
		time: 1870,
		velocity: 10.755,
		power: 4318.21478461759,
		road: 16118.057962963,
		acceleration: 0.247962962962964
	},
	{
		id: 1872,
		time: 1871,
		velocity: 10.5136111111111,
		power: 3401.35671209223,
		road: 16128.6427314815,
		acceleration: 0.146574074074074
	},
	{
		id: 1873,
		time: 1872,
		velocity: 10.7055555555556,
		power: 1791.3206413789,
		road: 16139.2933333333,
		acceleration: -0.0149074074074083
	},
	{
		id: 1874,
		time: 1873,
		velocity: 10.7102777777778,
		power: 896.630100447858,
		road: 16149.8855555555,
		acceleration: -0.101851851851851
	},
	{
		id: 1875,
		time: 1874,
		velocity: 10.2080555555556,
		power: -1426.73763271702,
		road: 16160.2606481481,
		acceleration: -0.332407407407409
	},
	{
		id: 1876,
		time: 1875,
		velocity: 9.70833333333333,
		power: -3376.71926747692,
		road: 16170.1996759259,
		acceleration: -0.53972222222222
	},
	{
		id: 1877,
		time: 1876,
		velocity: 9.09111111111111,
		power: -2100.45192647807,
		road: 16179.6633796296,
		acceleration: -0.410925925925927
	},
	{
		id: 1878,
		time: 1877,
		velocity: 8.97527777777778,
		power: -647.655899720409,
		road: 16188.7971759259,
		acceleration: -0.248888888888889
	},
	{
		id: 1879,
		time: 1878,
		velocity: 8.96166666666667,
		power: 1836.10117992456,
		road: 16197.8266203704,
		acceleration: 0.0401851851851838
	},
	{
		id: 1880,
		time: 1879,
		velocity: 9.21166666666667,
		power: 3064.75765403969,
		road: 16206.965,
		acceleration: 0.177685185185187
	},
	{
		id: 1881,
		time: 1880,
		velocity: 9.50833333333333,
		power: 1439.80577342518,
		road: 16216.1865277778,
		acceleration: -0.011388888888888
	},
	{
		id: 1882,
		time: 1881,
		velocity: 8.9275,
		power: -217.142657073997,
		road: 16225.3027314815,
		acceleration: -0.199259259259261
	},
	{
		id: 1883,
		time: 1882,
		velocity: 8.61388888888889,
		power: -1820.94047557784,
		road: 16234.1252777778,
		acceleration: -0.388055555555555
	},
	{
		id: 1884,
		time: 1883,
		velocity: 8.34416666666667,
		power: -1568.53506653364,
		road: 16242.5724537037,
		acceleration: -0.362685185185185
	},
	{
		id: 1885,
		time: 1884,
		velocity: 7.83944444444444,
		power: -933.113273258952,
		road: 16250.6956481481,
		acceleration: -0.285277777777778
	},
	{
		id: 1886,
		time: 1885,
		velocity: 7.75805555555556,
		power: 6.70214297916927,
		road: 16258.5953703704,
		acceleration: -0.161666666666667
	},
	{
		id: 1887,
		time: 1886,
		velocity: 7.85916666666667,
		power: 1674.43993657237,
		road: 16266.4452777778,
		acceleration: 0.0620370370370376
	},
	{
		id: 1888,
		time: 1887,
		velocity: 8.02555555555556,
		power: 1873.27141256085,
		road: 16274.3690277778,
		acceleration: 0.0856481481481488
	},
	{
		id: 1889,
		time: 1888,
		velocity: 8.015,
		power: 2441.11682805764,
		road: 16282.4131018518,
		acceleration: 0.154999999999999
	},
	{
		id: 1890,
		time: 1889,
		velocity: 8.32416666666667,
		power: 3496.44843056763,
		road: 16290.6740740741,
		acceleration: 0.278796296296296
	},
	{
		id: 1891,
		time: 1890,
		velocity: 8.86194444444444,
		power: 4928.64197722811,
		road: 16299.2902314815,
		acceleration: 0.431574074074074
	},
	{
		id: 1892,
		time: 1891,
		velocity: 9.30972222222222,
		power: 6808.56204148486,
		road: 16308.4262037037,
		acceleration: 0.608055555555556
	},
	{
		id: 1893,
		time: 1892,
		velocity: 10.1483333333333,
		power: 7142.89082342898,
		road: 16318.1608796296,
		acceleration: 0.589351851851852
	},
	{
		id: 1894,
		time: 1893,
		velocity: 10.63,
		power: 8118.12080673425,
		road: 16328.507962963,
		acceleration: 0.635462962962965
	},
	{
		id: 1895,
		time: 1894,
		velocity: 11.2161111111111,
		power: 5663.26003406061,
		road: 16339.3499074074,
		acceleration: 0.354259259259258
	},
	{
		id: 1896,
		time: 1895,
		velocity: 11.2111111111111,
		power: 3061.17798901122,
		road: 16350.4158333333,
		acceleration: 0.093703703703703
	},
	{
		id: 1897,
		time: 1896,
		velocity: 10.9111111111111,
		power: 1965.20111354187,
		road: 16361.5229166667,
		acceleration: -0.011388888888888
	},
	{
		id: 1898,
		time: 1897,
		velocity: 11.1819444444444,
		power: 2268.16824935473,
		road: 16372.6328703704,
		acceleration: 0.0171296296296291
	},
	{
		id: 1899,
		time: 1898,
		velocity: 11.2625,
		power: 3757.06738211674,
		road: 16383.8283333333,
		acceleration: 0.15388888888889
	},
	{
		id: 1900,
		time: 1899,
		velocity: 11.3727777777778,
		power: 2039.50500922925,
		road: 16395.0961574074,
		acceleration: -0.00916666666666721
	},
	{
		id: 1901,
		time: 1900,
		velocity: 11.1544444444444,
		power: -106.951377204593,
		road: 16406.2555555556,
		acceleration: -0.207685185185186
	},
	{
		id: 1902,
		time: 1901,
		velocity: 10.6394444444444,
		power: -314.607013405868,
		road: 16417.1985648148,
		acceleration: -0.225092592592592
	},
	{
		id: 1903,
		time: 1902,
		velocity: 10.6975,
		power: -1632.91414455074,
		road: 16427.8529166667,
		acceleration: -0.352222222222224
	},
	{
		id: 1904,
		time: 1903,
		velocity: 10.0977777777778,
		power: -688.321294526814,
		road: 16438.2023611111,
		acceleration: -0.257592592592591
	},
	{
		id: 1905,
		time: 1904,
		velocity: 9.86666666666667,
		power: -2491.86593036498,
		road: 16448.2003240741,
		acceleration: -0.44537037037037
	},
	{
		id: 1906,
		time: 1905,
		velocity: 9.36138888888889,
		power: -2245.23865086888,
		road: 16457.7628703704,
		acceleration: -0.425462962962964
	},
	{
		id: 1907,
		time: 1906,
		velocity: 8.82138888888889,
		power: -2300.41963214615,
		road: 16466.8931944444,
		acceleration: -0.438981481481481
	},
	{
		id: 1908,
		time: 1907,
		velocity: 8.54972222222222,
		power: -2695.6596614312,
		road: 16475.5557407407,
		acceleration: -0.496574074074076
	},
	{
		id: 1909,
		time: 1908,
		velocity: 7.87166666666667,
		power: -1877.64156188283,
		road: 16483.7671759259,
		acceleration: -0.405648148148147
	},
	{
		id: 1910,
		time: 1909,
		velocity: 7.60444444444444,
		power: -2791.61564578936,
		road: 16491.5056481481,
		acceleration: -0.540277777777778
	},
	{
		id: 1911,
		time: 1910,
		velocity: 6.92888888888889,
		power: -577.123372395728,
		road: 16498.8537962963,
		acceleration: -0.240370370370371
	},
	{
		id: 1912,
		time: 1911,
		velocity: 7.15055555555556,
		power: 690.752085841807,
		road: 16506.0538888889,
		acceleration: -0.0557407407407409
	},
	{
		id: 1913,
		time: 1912,
		velocity: 7.43722222222222,
		power: 3934.1687673421,
		road: 16513.4275,
		acceleration: 0.402777777777779
	},
	{
		id: 1914,
		time: 1913,
		velocity: 8.13722222222222,
		power: 8498.61184571549,
		road: 16521.4752777778,
		acceleration: 0.945555555555555
	},
	{
		id: 1915,
		time: 1914,
		velocity: 9.98722222222222,
		power: 13265.3089942856,
		road: 16530.6659722222,
		acceleration: 1.34027777777778
	},
	{
		id: 1916,
		time: 1915,
		velocity: 11.4580555555556,
		power: 13894.5293520742,
		road: 16541.1288425926,
		acceleration: 1.20407407407408
	},
	{
		id: 1917,
		time: 1916,
		velocity: 11.7494444444444,
		power: 11591.2660095803,
		road: 16552.6215740741,
		acceleration: 0.855648148148145
	},
	{
		id: 1918,
		time: 1917,
		velocity: 12.5541666666667,
		power: 10105.5999088778,
		road: 16564.8685648148,
		acceleration: 0.652870370370371
	},
	{
		id: 1919,
		time: 1918,
		velocity: 13.4166666666667,
		power: 13328.3719407196,
		road: 16577.8677314815,
		acceleration: 0.851481481481484
	},
	{
		id: 1920,
		time: 1919,
		velocity: 14.3038888888889,
		power: 21739.7843596436,
		road: 16591.979212963,
		acceleration: 1.37314814814815
	},
	{
		id: 1921,
		time: 1920,
		velocity: 16.6736111111111,
		power: 24097.7085066599,
		road: 16607.4601851852,
		acceleration: 1.36583333333334
	},
	{
		id: 1922,
		time: 1921,
		velocity: 17.5141666666667,
		power: 26458.1839701993,
		road: 16624.3015277778,
		acceleration: 1.35490740740741
	},
	{
		id: 1923,
		time: 1922,
		velocity: 18.3686111111111,
		power: 14193.5881397461,
		road: 16642.0844907407,
		acceleration: 0.528333333333329
	},
	{
		id: 1924,
		time: 1923,
		velocity: 18.2586111111111,
		power: 8571.76559612154,
		road: 16660.2220833333,
		acceleration: 0.180925925925926
	},
	{
		id: 1925,
		time: 1924,
		velocity: 18.0569444444444,
		power: 1339.10669994535,
		road: 16678.3326388889,
		acceleration: -0.234999999999999
	},
	{
		id: 1926,
		time: 1925,
		velocity: 17.6636111111111,
		power: 1362.06566473925,
		road: 16696.211712963,
		acceleration: -0.227962962962962
	},
	{
		id: 1927,
		time: 1926,
		velocity: 17.5747222222222,
		power: 4935.15431653597,
		road: 16713.9694444444,
		acceleration: -0.0147222222222219
	},
	{
		id: 1928,
		time: 1927,
		velocity: 18.0127777777778,
		power: 8541.33873956247,
		road: 16731.8163888889,
		acceleration: 0.193148148148147
	},
	{
		id: 1929,
		time: 1928,
		velocity: 18.2430555555556,
		power: 9569.54691734851,
		road: 16749.8809722222,
		acceleration: 0.242129629629634
	},
	{
		id: 1930,
		time: 1929,
		velocity: 18.3011111111111,
		power: 7199.57256898196,
		road: 16768.115462963,
		acceleration: 0.0976851851851812
	},
	{
		id: 1931,
		time: 1930,
		velocity: 18.3058333333333,
		power: 6216.21730075304,
		road: 16786.4181018519,
		acceleration: 0.0386111111111127
	},
	{
		id: 1932,
		time: 1931,
		velocity: 18.3588888888889,
		power: 5086.21271789703,
		road: 16804.7269907407,
		acceleration: -0.0261111111111134
	},
	{
		id: 1933,
		time: 1932,
		velocity: 18.2227777777778,
		power: 5743.45621807315,
		road: 16823.0286574074,
		acceleration: 0.0116666666666703
	},
	{
		id: 1934,
		time: 1933,
		velocity: 18.3408333333333,
		power: 6978.16032731438,
		road: 16841.37625,
		acceleration: 0.0801851851851865
	},
	{
		id: 1935,
		time: 1934,
		velocity: 18.5994444444444,
		power: 8347.14802488424,
		road: 16859.8402777778,
		acceleration: 0.152685185185184
	},
	{
		id: 1936,
		time: 1935,
		velocity: 18.6808333333333,
		power: 7584.64752354497,
		road: 16878.4325925926,
		acceleration: 0.103888888888889
	},
	{
		id: 1937,
		time: 1936,
		velocity: 18.6525,
		power: 4850.56637525605,
		road: 16897.0515277778,
		acceleration: -0.0506481481481487
	},
	{
		id: 1938,
		time: 1937,
		velocity: 18.4475,
		power: 651.344533094708,
		road: 16915.5039351852,
		acceleration: -0.282407407407412
	},
	{
		id: 1939,
		time: 1938,
		velocity: 17.8336111111111,
		power: 16.3135650791417,
		road: 16933.6590277778,
		acceleration: -0.312222222222221
	},
	{
		id: 1940,
		time: 1939,
		velocity: 17.7158333333333,
		power: -1304.49573566052,
		road: 16951.4666666667,
		acceleration: -0.382685185185185
	},
	{
		id: 1941,
		time: 1940,
		velocity: 17.2994444444444,
		power: -762.814351536167,
		road: 16968.9106481481,
		acceleration: -0.34462962962963
	},
	{
		id: 1942,
		time: 1941,
		velocity: 16.7997222222222,
		power: -2428.23106626847,
		road: 16985.9622222222,
		acceleration: -0.440185185185186
	},
	{
		id: 1943,
		time: 1942,
		velocity: 16.3952777777778,
		power: -2357.01934880186,
		road: 17002.5780092593,
		acceleration: -0.431388888888886
	},
	{
		id: 1944,
		time: 1943,
		velocity: 16.0052777777778,
		power: -2242.91925629591,
		road: 17018.7680555556,
		acceleration: -0.420092592592594
	},
	{
		id: 1945,
		time: 1944,
		velocity: 15.5394444444444,
		power: -2760.10667497876,
		road: 17034.5227314815,
		acceleration: -0.450648148148147
	},
	{
		id: 1946,
		time: 1945,
		velocity: 15.0433333333333,
		power: -1982.6452469724,
		road: 17049.8544444444,
		acceleration: -0.395277777777778
	},
	{
		id: 1947,
		time: 1946,
		velocity: 14.8194444444444,
		power: -1290.01806669977,
		road: 17064.8165740741,
		acceleration: -0.343888888888888
	},
	{
		id: 1948,
		time: 1947,
		velocity: 14.5077777777778,
		power: -290.203693627136,
		road: 17079.4721296296,
		acceleration: -0.269259259259261
	},
	{
		id: 1949,
		time: 1948,
		velocity: 14.2355555555556,
		power: -1960.97889928505,
		road: 17093.7998148148,
		acceleration: -0.386481481481482
	},
	{
		id: 1950,
		time: 1949,
		velocity: 13.66,
		power: -2285.93480239618,
		road: 17107.7299074074,
		acceleration: -0.408703703703702
	},
	{
		id: 1951,
		time: 1950,
		velocity: 13.2816666666667,
		power: -1404.01316659923,
		road: 17121.2858796296,
		acceleration: -0.339537037037035
	},
	{
		id: 1952,
		time: 1951,
		velocity: 13.2169444444444,
		power: 1535.15401397384,
		road: 17134.6185648148,
		acceleration: -0.107037037037038
	},
	{
		id: 1953,
		time: 1952,
		velocity: 13.3388888888889,
		power: 1063.03516255562,
		road: 17147.8269907407,
		acceleration: -0.141481481481481
	},
	{
		id: 1954,
		time: 1953,
		velocity: 12.8572222222222,
		power: 5390.74073632434,
		road: 17161.0648611111,
		acceleration: 0.20037037037037
	},
	{
		id: 1955,
		time: 1954,
		velocity: 13.8180555555556,
		power: 7760.90607688187,
		road: 17174.5881944444,
		acceleration: 0.370555555555555
	},
	{
		id: 1956,
		time: 1955,
		velocity: 14.4505555555556,
		power: 12512.8520883305,
		road: 17188.6434722222,
		acceleration: 0.693333333333333
	},
	{
		id: 1957,
		time: 1956,
		velocity: 14.9372222222222,
		power: 12578.0714033998,
		road: 17203.3676388889,
		acceleration: 0.644444444444442
	},
	{
		id: 1958,
		time: 1957,
		velocity: 15.7513888888889,
		power: 10322.6191992036,
		road: 17218.6382407407,
		acceleration: 0.44842592592593
	},
	{
		id: 1959,
		time: 1958,
		velocity: 15.7958333333333,
		power: 9645.90661563676,
		road: 17234.321712963,
		acceleration: 0.377314814814811
	},
	{
		id: 1960,
		time: 1959,
		velocity: 16.0691666666667,
		power: 5746.52456366795,
		road: 17250.2473611111,
		acceleration: 0.107037037037038
	},
	{
		id: 1961,
		time: 1960,
		velocity: 16.0725,
		power: 6965.34434901774,
		road: 17266.316712963,
		acceleration: 0.180370370370371
	},
	{
		id: 1962,
		time: 1961,
		velocity: 16.3369444444444,
		power: 5572.97002291328,
		road: 17282.5184259259,
		acceleration: 0.0843518518518493
	},
	{
		id: 1963,
		time: 1962,
		velocity: 16.3222222222222,
		power: 7317.95318855525,
		road: 17298.8575462963,
		acceleration: 0.190462962962968
	},
	{
		id: 1964,
		time: 1963,
		velocity: 16.6438888888889,
		power: 4284.15193920322,
		road: 17315.2884259259,
		acceleration: -0.00694444444444997
	},
	{
		id: 1965,
		time: 1964,
		velocity: 16.3161111111111,
		power: 4594.23004264395,
		road: 17331.7221759259,
		acceleration: 0.0126851851851875
	},
	{
		id: 1966,
		time: 1965,
		velocity: 16.3602777777778,
		power: 3552.29659779261,
		road: 17348.135787037,
		acceleration: -0.0529629629629618
	},
	{
		id: 1967,
		time: 1966,
		velocity: 16.485,
		power: 6078.64330481623,
		road: 17364.5763425926,
		acceleration: 0.106851851851854
	},
	{
		id: 1968,
		time: 1967,
		velocity: 16.6366666666667,
		power: 6004.81033664334,
		road: 17381.1192592593,
		acceleration: 0.0978703703703694
	},
	{
		id: 1969,
		time: 1968,
		velocity: 16.6538888888889,
		power: 4936.24780087084,
		road: 17397.7250925926,
		acceleration: 0.0279629629629632
	},
	{
		id: 1970,
		time: 1969,
		velocity: 16.5688888888889,
		power: 3019.56557164705,
		road: 17414.2990277778,
		acceleration: -0.0917592592592626
	},
	{
		id: 1971,
		time: 1970,
		velocity: 16.3613888888889,
		power: 758.761392113883,
		road: 17430.7116203704,
		acceleration: -0.230925925925927
	},
	{
		id: 1972,
		time: 1971,
		velocity: 15.9611111111111,
		power: -403.729440377699,
		road: 17446.8584722222,
		acceleration: -0.300555555555553
	},
	{
		id: 1973,
		time: 1972,
		velocity: 15.6672222222222,
		power: 124.815094015014,
		road: 17462.7244907407,
		acceleration: -0.261111111111111
	},
	{
		id: 1974,
		time: 1973,
		velocity: 15.5780555555556,
		power: 1713.48284679203,
		road: 17478.3843518518,
		acceleration: -0.151203703703702
	},
	{
		id: 1975,
		time: 1974,
		velocity: 15.5075,
		power: 4054.0781072914,
		road: 17493.9724537037,
		acceleration: 0.00768518518518313
	},
	{
		id: 1976,
		time: 1975,
		velocity: 15.6902777777778,
		power: 4876.98288468486,
		road: 17509.5951851852,
		acceleration: 0.0615740740740733
	},
	{
		id: 1977,
		time: 1976,
		velocity: 15.7627777777778,
		power: 6564.09827313277,
		road: 17525.3333333333,
		acceleration: 0.169259259259261
	},
	{
		id: 1978,
		time: 1977,
		velocity: 16.0152777777778,
		power: 7289.79677471621,
		road: 17541.2602777778,
		acceleration: 0.208333333333334
	},
	{
		id: 1979,
		time: 1978,
		velocity: 16.3152777777778,
		power: 6684.38009612947,
		road: 17557.3714814815,
		acceleration: 0.160185185185185
	},
	{
		id: 1980,
		time: 1979,
		velocity: 16.2433333333333,
		power: 3772.27527255212,
		road: 17573.5472685185,
		acceleration: -0.0310185185185183
	},
	{
		id: 1981,
		time: 1980,
		velocity: 15.9222222222222,
		power: 625.608848365584,
		road: 17589.5916666667,
		acceleration: -0.231759259259261
	},
	{
		id: 1982,
		time: 1981,
		velocity: 15.62,
		power: -654.751032903534,
		road: 17605.3646296296,
		acceleration: -0.31111111111111
	},
	{
		id: 1983,
		time: 1982,
		velocity: 15.31,
		power: -17.3698645256425,
		road: 17620.8501388889,
		acceleration: -0.263796296296299
	},
	{
		id: 1984,
		time: 1983,
		velocity: 15.1308333333333,
		power: 45.6187492707391,
		road: 17636.07625,
		acceleration: -0.254999999999997
	},
	{
		id: 1985,
		time: 1984,
		velocity: 14.855,
		power: 1353.45588335737,
		road: 17651.0947222222,
		acceleration: -0.160277777777779
	},
	{
		id: 1986,
		time: 1985,
		velocity: 14.8291666666667,
		power: 2277.35691906169,
		road: 17665.9868518518,
		acceleration: -0.0924074074074071
	},
	{
		id: 1987,
		time: 1986,
		velocity: 14.8536111111111,
		power: 2482.93649391066,
		road: 17680.795,
		acceleration: -0.0755555555555549
	},
	{
		id: 1988,
		time: 1987,
		velocity: 14.6283333333333,
		power: 755.632076250425,
		road: 17695.4679166667,
		acceleration: -0.194907407407406
	},
	{
		id: 1989,
		time: 1988,
		velocity: 14.2444444444444,
		power: -1143.26718305562,
		road: 17709.8795833333,
		acceleration: -0.327592592592595
	},
	{
		id: 1990,
		time: 1989,
		velocity: 13.8708333333333,
		power: -3047.84580236849,
		road: 17723.8944907407,
		acceleration: -0.465925925925927
	},
	{
		id: 1991,
		time: 1990,
		velocity: 13.2305555555556,
		power: -243.961430264334,
		road: 17737.550787037,
		acceleration: -0.251296296296292
	},
	{
		id: 1992,
		time: 1991,
		velocity: 13.4905555555556,
		power: 1458.26021970955,
		road: 17751.0232407407,
		acceleration: -0.116388888888892
	},
	{
		id: 1993,
		time: 1992,
		velocity: 13.5216666666667,
		power: 3309.80964584593,
		road: 17764.452037037,
		acceleration: 0.0290740740740745
	},
	{
		id: 1994,
		time: 1993,
		velocity: 13.3177777777778,
		power: 3906.96864485654,
		road: 17777.9322222222,
		acceleration: 0.0737037037037034
	},
	{
		id: 1995,
		time: 1994,
		velocity: 13.7116666666667,
		power: 3662.43996769539,
		road: 17791.475462963,
		acceleration: 0.0524074074074061
	},
	{
		id: 1996,
		time: 1995,
		velocity: 13.6788888888889,
		power: 5736.32482222993,
		road: 17805.1481944444,
		acceleration: 0.206574074074075
	},
	{
		id: 1997,
		time: 1996,
		velocity: 13.9375,
		power: 4768.66604556945,
		road: 17818.9869444444,
		acceleration: 0.125462962962963
	},
	{
		id: 1998,
		time: 1997,
		velocity: 14.0880555555556,
		power: 5249.88377295329,
		road: 17832.96625,
		acceleration: 0.155648148148147
	},
	{
		id: 1999,
		time: 1998,
		velocity: 14.1458333333333,
		power: 4945.1727474973,
		road: 17847.0868055555,
		acceleration: 0.126851851851853
	},
	{
		id: 2000,
		time: 1999,
		velocity: 14.3180555555556,
		power: 5183.59282499984,
		road: 17861.3401851852,
		acceleration: 0.138796296296297
	},
	{
		id: 2001,
		time: 2000,
		velocity: 14.5044444444444,
		power: 5339.41666582988,
		road: 17875.735,
		acceleration: 0.144074074074073
	},
	{
		id: 2002,
		time: 2001,
		velocity: 14.5780555555556,
		power: 6338.52289984548,
		road: 17890.3059722222,
		acceleration: 0.20824074074074
	},
	{
		id: 2003,
		time: 2002,
		velocity: 14.9427777777778,
		power: 8425.92493774036,
		road: 17905.1522222222,
		acceleration: 0.342314814814815
	},
	{
		id: 2004,
		time: 2003,
		velocity: 15.5313888888889,
		power: 9165.33818988071,
		road: 17920.3561574074,
		acceleration: 0.373055555555556
	},
	{
		id: 2005,
		time: 2004,
		velocity: 15.6972222222222,
		power: 10118.0769969082,
		road: 17935.9536574074,
		acceleration: 0.414074074074074
	},
	{
		id: 2006,
		time: 2005,
		velocity: 16.185,
		power: 7952.31152870569,
		road: 17951.8840277778,
		acceleration: 0.251666666666669
	},
	{
		id: 2007,
		time: 2006,
		velocity: 16.2863888888889,
		power: 8145.0849899349,
		road: 17968.0659259259,
		acceleration: 0.25138888888889
	},
	{
		id: 2008,
		time: 2007,
		velocity: 16.4513888888889,
		power: 6413.83457842261,
		road: 17984.4390740741,
		acceleration: 0.13111111111111
	},
	{
		id: 2009,
		time: 2008,
		velocity: 16.5783333333333,
		power: 6114.45922221918,
		road: 18000.93125,
		acceleration: 0.106944444444441
	},
	{
		id: 2010,
		time: 2009,
		velocity: 16.6072222222222,
		power: 2607.72879377924,
		road: 18017.4192592593,
		acceleration: -0.115277777777777
	},
	{
		id: 2011,
		time: 2010,
		velocity: 16.1055555555556,
		power: -949.926627048807,
		road: 18033.6808333333,
		acceleration: -0.337592592592589
	},
	{
		id: 2012,
		time: 2011,
		velocity: 15.5655555555556,
		power: 2680.52190349979,
		road: 18049.7247222222,
		acceleration: -0.0977777777777806
	},
	{
		id: 2013,
		time: 2012,
		velocity: 16.3138888888889,
		power: 5326.71250504807,
		road: 18065.7573148148,
		acceleration: 0.0751851851851857
	},
	{
		id: 2014,
		time: 2013,
		velocity: 16.3311111111111,
		power: 11638.2442785536,
		road: 18082.0620833333,
		acceleration: 0.469166666666663
	},
	{
		id: 2015,
		time: 2014,
		velocity: 16.9730555555556,
		power: 9996.05699660573,
		road: 18098.7717592593,
		acceleration: 0.340648148148151
	},
	{
		id: 2016,
		time: 2015,
		velocity: 17.3358333333333,
		power: 13796.5875536939,
		road: 18115.925462963,
		acceleration: 0.547407407407409
	},
	{
		id: 2017,
		time: 2016,
		velocity: 17.9733333333333,
		power: 12137.4111627538,
		road: 18133.5611111111,
		acceleration: 0.41648148148148
	},
	{
		id: 2018,
		time: 2017,
		velocity: 18.2225,
		power: 11005.4876586039,
		road: 18151.5691666667,
		acceleration: 0.328333333333333
	},
	{
		id: 2019,
		time: 2018,
		velocity: 18.3208333333333,
		power: 5697.92402864576,
		road: 18169.748287037,
		acceleration: 0.0137962962962987
	},
	{
		id: 2020,
		time: 2019,
		velocity: 18.0147222222222,
		power: 2526.25076013527,
		road: 18187.8511574074,
		acceleration: -0.166296296296299
	},
	{
		id: 2021,
		time: 2020,
		velocity: 17.7236111111111,
		power: -937.306851219119,
		road: 18205.6900462963,
		acceleration: -0.361666666666668
	},
	{
		id: 2022,
		time: 2021,
		velocity: 17.2358333333333,
		power: 139.375625404247,
		road: 18223.2021296296,
		acceleration: -0.291944444444443
	},
	{
		id: 2023,
		time: 2022,
		velocity: 17.1388888888889,
		power: 656.731333892013,
		road: 18240.4406944444,
		acceleration: -0.255092592592593
	},
	{
		id: 2024,
		time: 2023,
		velocity: 16.9583333333333,
		power: 3399.9000478663,
		road: 18257.51,
		acceleration: -0.0834259259259227
	},
	{
		id: 2025,
		time: 2024,
		velocity: 16.9855555555556,
		power: 4137.56425378378,
		road: 18274.5194907407,
		acceleration: -0.0362037037037055
	},
	{
		id: 2026,
		time: 2025,
		velocity: 17.0302777777778,
		power: 5386.37693280807,
		road: 18291.5311111111,
		acceleration: 0.0404629629629625
	},
	{
		id: 2027,
		time: 2026,
		velocity: 17.0797222222222,
		power: 2849.28569746797,
		road: 18308.505787037,
		acceleration: -0.11435185185185
	},
	{
		id: 2028,
		time: 2027,
		velocity: 16.6425,
		power: -757.424133428296,
		road: 18325.2568518518,
		acceleration: -0.332870370370372
	},
	{
		id: 2029,
		time: 2028,
		velocity: 16.0316666666667,
		power: -2208.47085072635,
		road: 18341.6316666667,
		acceleration: -0.419629629629629
	},
	{
		id: 2030,
		time: 2029,
		velocity: 15.8208333333333,
		power: -2701.4501382458,
		road: 18357.5726851852,
		acceleration: -0.447962962962963
	},
	{
		id: 2031,
		time: 2030,
		velocity: 15.2986111111111,
		power: -1516.91519687123,
		road: 18373.1068981481,
		acceleration: -0.365648148148146
	},
	{
		id: 2032,
		time: 2031,
		velocity: 14.9347222222222,
		power: -2825.82565914198,
		road: 18388.2323148148,
		acceleration: -0.451944444444443
	},
	{
		id: 2033,
		time: 2032,
		velocity: 14.465,
		power: -2031.92571117588,
		road: 18402.9347685185,
		acceleration: -0.393981481481482
	},
	{
		id: 2034,
		time: 2033,
		velocity: 14.1166666666667,
		power: -2002.87429400088,
		road: 18417.2455092593,
		acceleration: -0.389444444444447
	},
	{
		id: 2035,
		time: 2034,
		velocity: 13.7663888888889,
		power: -6427.9079449965,
		road: 18430.9997222222,
		acceleration: -0.723611111111111
	},
	{
		id: 2036,
		time: 2035,
		velocity: 12.2941666666667,
		power: -9070.95872157744,
		road: 18443.9133333333,
		acceleration: -0.957592592592592
	},
	{
		id: 2037,
		time: 2036,
		velocity: 11.2438888888889,
		power: -11106.6598432398,
		road: 18455.7530555556,
		acceleration: -1.19018518518519
	},
	{
		id: 2038,
		time: 2037,
		velocity: 10.1958333333333,
		power: -6171.11590745915,
		road: 18466.6024074074,
		acceleration: -0.790555555555555
	},
	{
		id: 2039,
		time: 2038,
		velocity: 9.9225,
		power: -3665.81590442277,
		road: 18476.7744907407,
		acceleration: -0.563981481481481
	},
	{
		id: 2040,
		time: 2039,
		velocity: 9.55194444444444,
		power: -1100.86583748454,
		road: 18486.5148148148,
		acceleration: -0.299537037037037
	},
	{
		id: 2041,
		time: 2040,
		velocity: 9.29722222222222,
		power: -958.673967228548,
		road: 18495.9632407407,
		acceleration: -0.284259259259258
	},
	{
		id: 2042,
		time: 2041,
		velocity: 9.06972222222222,
		power: -203.768022421198,
		road: 18505.1703240741,
		acceleration: -0.198425925925926
	},
	{
		id: 2043,
		time: 2042,
		velocity: 8.95666666666667,
		power: 1008.48953204473,
		road: 18514.2495833333,
		acceleration: -0.0572222222222223
	},
	{
		id: 2044,
		time: 2043,
		velocity: 9.12555555555555,
		power: 1779.89100208589,
		road: 18523.3164351852,
		acceleration: 0.0324074074074066
	},
	{
		id: 2045,
		time: 2044,
		velocity: 9.16694444444444,
		power: 2277.57025474968,
		road: 18532.4433333333,
		acceleration: 0.0876851851851832
	},
	{
		id: 2046,
		time: 2045,
		velocity: 9.21972222222222,
		power: 2247.90709621635,
		road: 18541.6545833333,
		acceleration: 0.081018518518519
	},
	{
		id: 2047,
		time: 2046,
		velocity: 9.36861111111111,
		power: 2001.78616790727,
		road: 18550.9316666667,
		acceleration: 0.0506481481481504
	},
	{
		id: 2048,
		time: 2047,
		velocity: 9.31888888888889,
		power: 1568.39073047696,
		road: 18560.2344907407,
		acceleration: 0.000833333333332575
	},
	{
		id: 2049,
		time: 2048,
		velocity: 9.22222222222222,
		power: 2758.4468895501,
		road: 18569.6038425926,
		acceleration: 0.132222222222223
	},
	{
		id: 2050,
		time: 2049,
		velocity: 9.76527777777778,
		power: 2321.03551787297,
		road: 18579.0788888889,
		acceleration: 0.0791666666666657
	},
	{
		id: 2051,
		time: 2050,
		velocity: 9.55638888888889,
		power: 1819.1937677306,
		road: 18588.6044907407,
		acceleration: 0.0219444444444434
	},
	{
		id: 2052,
		time: 2051,
		velocity: 9.28805555555556,
		power: -2248.99875911945,
		road: 18597.9262037037,
		acceleration: -0.429722222222221
	},
	{
		id: 2053,
		time: 2052,
		velocity: 8.47611111111111,
		power: -6494.96970774339,
		road: 18606.5529166667,
		acceleration: -0.960277777777778
	},
	{
		id: 2054,
		time: 2053,
		velocity: 6.67555555555556,
		power: -7400.78167845399,
		road: 18614.1046296296,
		acceleration: -1.18972222222222
	},
	{
		id: 2055,
		time: 2054,
		velocity: 5.71888888888889,
		power: -5223.19026732205,
		road: 18620.5605555556,
		acceleration: -1.00185185185185
	},
	{
		id: 2056,
		time: 2055,
		velocity: 5.47055555555556,
		power: -1901.41682452875,
		road: 18626.2674074074,
		acceleration: -0.496296296296295
	},
	{
		id: 2057,
		time: 2056,
		velocity: 5.18666666666667,
		power: -492.709927363616,
		road: 18631.6058333333,
		acceleration: -0.240555555555556
	},
	{
		id: 2058,
		time: 2057,
		velocity: 4.99722222222222,
		power: -157.638246427508,
		road: 18636.736712963,
		acceleration: -0.174537037037036
	},
	{
		id: 2059,
		time: 2058,
		velocity: 4.94694444444445,
		power: 219.946255802903,
		road: 18641.7327777778,
		acceleration: -0.0950925925925938
	},
	{
		id: 2060,
		time: 2059,
		velocity: 4.90138888888889,
		power: -24.3515568315712,
		road: 18646.608287037,
		acceleration: -0.146018518518519
	},
	{
		id: 2061,
		time: 2060,
		velocity: 4.55916666666667,
		power: 380.580329535254,
		road: 18651.3826388889,
		acceleration: -0.0562962962962956
	},
	{
		id: 2062,
		time: 2061,
		velocity: 4.77805555555556,
		power: 1508.43797502101,
		road: 18656.2225925926,
		acceleration: 0.1875
	},
	{
		id: 2063,
		time: 2062,
		velocity: 5.46388888888889,
		power: 2090.67905928076,
		road: 18661.3019444444,
		acceleration: 0.291296296296297
	},
	{
		id: 2064,
		time: 2063,
		velocity: 5.43305555555556,
		power: 788.281224115886,
		road: 18666.5348148148,
		acceleration: 0.0157407407407399
	},
	{
		id: 2065,
		time: 2064,
		velocity: 4.82527777777778,
		power: -592.160855186922,
		road: 18671.6435185185,
		acceleration: -0.264074074074074
	},
	{
		id: 2066,
		time: 2065,
		velocity: 4.67166666666667,
		power: -1019.89695340182,
		road: 18676.4380555556,
		acceleration: -0.364259259259259
	},
	{
		id: 2067,
		time: 2066,
		velocity: 4.34027777777778,
		power: -692.955621661374,
		road: 18680.8993981481,
		acceleration: -0.302129629629629
	},
	{
		id: 2068,
		time: 2067,
		velocity: 3.91888888888889,
		power: -561.991034781315,
		road: 18685.0701388889,
		acceleration: -0.279074074074074
	},
	{
		id: 2069,
		time: 2068,
		velocity: 3.83444444444444,
		power: -195.56741715772,
		road: 18689.0071296296,
		acceleration: -0.188425925925926
	},
	{
		id: 2070,
		time: 2069,
		velocity: 3.775,
		power: -109.499423017237,
		road: 18692.7668981481,
		acceleration: -0.166018518518519
	},
	{
		id: 2071,
		time: 2070,
		velocity: 3.42083333333333,
		power: -151.972332312587,
		road: 18696.3540277778,
		acceleration: -0.179259259259259
	},
	{
		id: 2072,
		time: 2071,
		velocity: 3.29666666666667,
		power: -91.3531782262404,
		road: 18699.770462963,
		acceleration: -0.16212962962963
	},
	{
		id: 2073,
		time: 2072,
		velocity: 3.28861111111111,
		power: 127.057036333773,
		road: 18703.0594444444,
		acceleration: -0.0927777777777776
	},
	{
		id: 2074,
		time: 2073,
		velocity: 3.1425,
		power: -323.102334532275,
		road: 18706.1810648148,
		acceleration: -0.241944444444445
	},
	{
		id: 2075,
		time: 2074,
		velocity: 2.57083333333333,
		power: -464.705646655512,
		road: 18709.0297685185,
		acceleration: -0.303888888888888
	},
	{
		id: 2076,
		time: 2075,
		velocity: 2.37694444444444,
		power: -350.495560020263,
		road: 18711.5887962963,
		acceleration: -0.275462962962964
	},
	{
		id: 2077,
		time: 2076,
		velocity: 2.31611111111111,
		power: 46.3693908336785,
		road: 18713.9551388889,
		acceleration: -0.109907407407407
	},
	{
		id: 2078,
		time: 2077,
		velocity: 2.24111111111111,
		power: 101.554574320396,
		road: 18716.2249537037,
		acceleration: -0.083148148148148
	},
	{
		id: 2079,
		time: 2078,
		velocity: 2.1275,
		power: -357.563264226643,
		road: 18718.2973148148,
		acceleration: -0.311759259259259
	},
	{
		id: 2080,
		time: 2079,
		velocity: 1.38083333333333,
		power: -904.421586687735,
		road: 18719.8402777778,
		acceleration: -0.747037037037037
	},
	{
		id: 2081,
		time: 2080,
		velocity: 0,
		power: -448.914483116482,
		road: 18720.6551388889,
		acceleration: -0.709166666666667
	},
	{
		id: 2082,
		time: 2081,
		velocity: 0,
		power: -72.5477720760234,
		road: 18720.8852777778,
		acceleration: -0.460277777777778
	},
	{
		id: 2083,
		time: 2082,
		velocity: 0,
		power: 0,
		road: 18720.8852777778,
		acceleration: 0
	},
	{
		id: 2084,
		time: 2083,
		velocity: 0,
		power: 0,
		road: 18720.8852777778,
		acceleration: 0
	},
	{
		id: 2085,
		time: 2084,
		velocity: 0,
		power: 0,
		road: 18720.8852777778,
		acceleration: 0
	},
	{
		id: 2086,
		time: 2085,
		velocity: 0,
		power: 0,
		road: 18720.8852777778,
		acceleration: 0
	},
	{
		id: 2087,
		time: 2086,
		velocity: 0,
		power: 66.2714446279069,
		road: 18721.0431018518,
		acceleration: 0.315648148148148
	},
	{
		id: 2088,
		time: 2087,
		velocity: 0.946944444444444,
		power: 529.928606826079,
		road: 18721.7125,
		acceleration: 0.7075
	},
	{
		id: 2089,
		time: 2088,
		velocity: 2.1225,
		power: 2072.33893241536,
		road: 18723.3417592592,
		acceleration: 1.21222222222222
	},
	{
		id: 2090,
		time: 2089,
		velocity: 3.63666666666667,
		power: 3610.32568763546,
		road: 18726.1810648148,
		acceleration: 1.20787037037037
	},
	{
		id: 2091,
		time: 2090,
		velocity: 4.57055555555556,
		power: 4150.09016280238,
		road: 18730.1121296296,
		acceleration: 0.975648148148148
	},
	{
		id: 2092,
		time: 2091,
		velocity: 5.04944444444444,
		power: 2867.80970222348,
		road: 18734.7842592593,
		acceleration: 0.506481481481481
	},
	{
		id: 2093,
		time: 2092,
		velocity: 5.15611111111111,
		power: 1509.34284807943,
		road: 18739.7973148148,
		acceleration: 0.17537037037037
	},
	{
		id: 2094,
		time: 2093,
		velocity: 5.09666666666667,
		power: 1105.08152122232,
		road: 18744.94,
		acceleration: 0.0838888888888887
	},
	{
		id: 2095,
		time: 2094,
		velocity: 5.30111111111111,
		power: 1201.0948210381,
		road: 18750.1739814815,
		acceleration: 0.0987037037037046
	},
	{
		id: 2096,
		time: 2095,
		velocity: 5.45222222222222,
		power: 2817.22810206989,
		road: 18755.6555555555,
		acceleration: 0.396481481481481
	},
	{
		id: 2097,
		time: 2096,
		velocity: 6.28611111111111,
		power: 3327.6192245113,
		road: 18761.5583796296,
		acceleration: 0.446018518518517
	},
	{
		id: 2098,
		time: 2097,
		velocity: 6.63916666666667,
		power: 2896.79922072707,
		road: 18767.8514351852,
		acceleration: 0.334444444444444
	},
	{
		id: 2099,
		time: 2098,
		velocity: 6.45555555555555,
		power: 565.251081216809,
		road: 18774.2825925926,
		acceleration: -0.0582407407407395
	},
	{
		id: 2100,
		time: 2099,
		velocity: 6.11138888888889,
		power: -578.40926864442,
		road: 18780.5613888889,
		acceleration: -0.246481481481482
	},
	{
		id: 2101,
		time: 2100,
		velocity: 5.89972222222222,
		power: -863.193756584511,
		road: 18786.5675,
		acceleration: -0.298888888888889
	},
	{
		id: 2102,
		time: 2101,
		velocity: 5.55888888888889,
		power: -754.15960223204,
		road: 18792.2818518518,
		acceleration: -0.284629629629629
	},
	{
		id: 2103,
		time: 2102,
		velocity: 5.2575,
		power: -675.049128466564,
		road: 18797.7165277778,
		acceleration: -0.274722222222223
	},
	{
		id: 2104,
		time: 2103,
		velocity: 5.07555555555555,
		power: -781.97985307646,
		road: 18802.8627314815,
		acceleration: -0.302222222222222
	},
	{
		id: 2105,
		time: 2104,
		velocity: 4.65222222222222,
		power: -506.794551759109,
		road: 18807.7326851852,
		acceleration: -0.250277777777778
	},
	{
		id: 2106,
		time: 2105,
		velocity: 4.50666666666667,
		power: -229.34051568388,
		road: 18812.3817592593,
		acceleration: -0.191481481481481
	},
	{
		id: 2107,
		time: 2106,
		velocity: 4.50111111111111,
		power: 434.941087323012,
		road: 18816.9161111111,
		acceleration: -0.037962962962963
	},
	{
		id: 2108,
		time: 2107,
		velocity: 4.53833333333333,
		power: 878.444493306793,
		road: 18821.4636574074,
		acceleration: 0.0643518518518515
	},
	{
		id: 2109,
		time: 2108,
		velocity: 4.69972222222222,
		power: 773.817280695768,
		road: 18826.0623148148,
		acceleration: 0.0378703703703707
	},
	{
		id: 2110,
		time: 2109,
		velocity: 4.61472222222222,
		power: 1582.40090562253,
		road: 18830.78625,
		acceleration: 0.212685185185185
	},
	{
		id: 2111,
		time: 2110,
		velocity: 5.17638888888889,
		power: 1872.19737039601,
		road: 18835.7446296296,
		acceleration: 0.256203703703703
	},
	{
		id: 2112,
		time: 2111,
		velocity: 5.46833333333333,
		power: 2610.89542967771,
		road: 18841.02,
		acceleration: 0.377777777777779
	},
	{
		id: 2113,
		time: 2112,
		velocity: 5.74805555555556,
		power: 1238.05165273873,
		road: 18846.5302314815,
		acceleration: 0.0919444444444437
	},
	{
		id: 2114,
		time: 2113,
		velocity: 5.45222222222222,
		power: 433.356753568241,
		road: 18852.0554166667,
		acceleration: -0.0620370370370367
	},
	{
		id: 2115,
		time: 2114,
		velocity: 5.28222222222222,
		power: -598.561864003175,
		road: 18857.4190740741,
		acceleration: -0.26101851851852
	},
	{
		id: 2116,
		time: 2115,
		velocity: 4.965,
		power: 424.20537827172,
		road: 18862.6237962963,
		acceleration: -0.0568518518518522
	},
	{
		id: 2117,
		time: 2116,
		velocity: 5.28166666666667,
		power: 279.546553018757,
		road: 18867.7576388889,
		acceleration: -0.0849074074074068
	},
	{
		id: 2118,
		time: 2117,
		velocity: 5.0275,
		power: 425.344120228439,
		road: 18872.8223148148,
		acceleration: -0.0534259259259269
	},
	{
		id: 2119,
		time: 2118,
		velocity: 4.80472222222222,
		power: 843.602306923136,
		road: 18877.8772222222,
		acceleration: 0.0338888888888889
	},
	{
		id: 2120,
		time: 2119,
		velocity: 5.38333333333333,
		power: 2570.57097366995,
		road: 18883.1348611111,
		acceleration: 0.371574074074075
	},
	{
		id: 2121,
		time: 2120,
		velocity: 6.14222222222222,
		power: 5238.2672033209,
		road: 18888.9766203704,
		acceleration: 0.796666666666666
	},
	{
		id: 2122,
		time: 2121,
		velocity: 7.19472222222222,
		power: 7594.36331718771,
		road: 18895.7312037037,
		acceleration: 1.02898148148148
	},
	{
		id: 2123,
		time: 2122,
		velocity: 8.47027777777778,
		power: 7995.62256306066,
		road: 18903.4630555555,
		acceleration: 0.925555555555555
	},
	{
		id: 2124,
		time: 2123,
		velocity: 8.91888888888889,
		power: 7720.01974566154,
		road: 18912.0456018518,
		acceleration: 0.775833333333335
	},
	{
		id: 2125,
		time: 2124,
		velocity: 9.52222222222222,
		power: 5538.24188122229,
		road: 18921.2445833333,
		acceleration: 0.457037037037036
	},
	{
		id: 2126,
		time: 2125,
		velocity: 9.84138888888889,
		power: 4466.96282550545,
		road: 18930.8271759259,
		acceleration: 0.310185185185185
	},
	{
		id: 2127,
		time: 2126,
		velocity: 9.84944444444444,
		power: 2574.7134811944,
		road: 18940.6122685185,
		acceleration: 0.0948148148148142
	},
	{
		id: 2128,
		time: 2127,
		velocity: 9.80666666666667,
		power: 1688.14459044348,
		road: 18950.4439351852,
		acceleration: -0.00166666666666693
	},
	{
		id: 2129,
		time: 2128,
		velocity: 9.83638888888889,
		power: 1636.6608570955,
		road: 18960.27125,
		acceleration: -0.00703703703703695
	},
	{
		id: 2130,
		time: 2129,
		velocity: 9.82833333333333,
		power: 1973.71569247595,
		road: 18970.1093518518,
		acceleration: 0.0286111111111111
	},
	{
		id: 2131,
		time: 2130,
		velocity: 9.8925,
		power: 3541.458948636,
		road: 18980.0569907407,
		acceleration: 0.190462962962965
	},
	{
		id: 2132,
		time: 2131,
		velocity: 10.4077777777778,
		power: 4921.50865756827,
		road: 18990.2599074074,
		acceleration: 0.320092592592591
	},
	{
		id: 2133,
		time: 2132,
		velocity: 10.7886111111111,
		power: 4803.84908785986,
		road: 19000.7678703704,
		acceleration: 0.290000000000003
	},
	{
		id: 2134,
		time: 2133,
		velocity: 10.7625,
		power: 2097.13043915362,
		road: 19011.428287037,
		acceleration: 0.0149074074074029
	},
	{
		id: 2135,
		time: 2134,
		velocity: 10.4525,
		power: 549.256972010862,
		road: 19022.027962963,
		acceleration: -0.136388888888888
	},
	{
		id: 2136,
		time: 2135,
		velocity: 10.3794444444444,
		power: 873.853928728082,
		road: 19032.5085185185,
		acceleration: -0.101851851851849
	},
	{
		id: 2137,
		time: 2136,
		velocity: 10.4569444444444,
		power: 2709.7148093057,
		road: 19042.9793055555,
		acceleration: 0.082314814814815
	},
	{
		id: 2138,
		time: 2137,
		velocity: 10.6994444444444,
		power: 3357.65255745709,
		road: 19053.5624074074,
		acceleration: 0.142314814814814
	},
	{
		id: 2139,
		time: 2138,
		velocity: 10.8063888888889,
		power: 3608.37861025149,
		road: 19064.2968055555,
		acceleration: 0.160277777777777
	},
	{
		id: 2140,
		time: 2139,
		velocity: 10.9377777777778,
		power: 2026.16387361328,
		road: 19075.1129166667,
		acceleration: 0.00314814814814746
	},
	{
		id: 2141,
		time: 2140,
		velocity: 10.7088888888889,
		power: 1253.25294039971,
		road: 19085.8950925926,
		acceleration: -0.0710185185185193
	},
	{
		id: 2142,
		time: 2141,
		velocity: 10.5933333333333,
		power: 778.984085745988,
		road: 19096.5840740741,
		acceleration: -0.115370370370368
	},
	{
		id: 2143,
		time: 2142,
		velocity: 10.5916666666667,
		power: 1629.58182743957,
		road: 19107.2004166667,
		acceleration: -0.0299074074074071
	},
	{
		id: 2144,
		time: 2143,
		velocity: 10.6191666666667,
		power: 2985.9519238094,
		road: 19117.8531481481,
		acceleration: 0.102685185185184
	},
	{
		id: 2145,
		time: 2144,
		velocity: 10.9013888888889,
		power: 5429.58497271761,
		road: 19128.7222222222,
		acceleration: 0.330000000000002
	},
	{
		id: 2146,
		time: 2145,
		velocity: 11.5816666666667,
		power: 7513.01219590361,
		road: 19140.0058333333,
		acceleration: 0.499074074074073
	},
	{
		id: 2147,
		time: 2146,
		velocity: 12.1163888888889,
		power: 10072.1090184836,
		road: 19151.8801388889,
		acceleration: 0.682314814814816
	},
	{
		id: 2148,
		time: 2147,
		velocity: 12.9483333333333,
		power: 10410.4208975227,
		road: 19164.4224537037,
		acceleration: 0.653703703703702
	},
	{
		id: 2149,
		time: 2148,
		velocity: 13.5427777777778,
		power: 8716.15661157348,
		road: 19177.5278703704,
		acceleration: 0.4725
	},
	{
		id: 2150,
		time: 2149,
		velocity: 13.5338888888889,
		power: 3962.4781047317,
		road: 19190.9104166667,
		acceleration: 0.0817592592592593
	},
	{
		id: 2151,
		time: 2150,
		velocity: 13.1936111111111,
		power: 566.297288915114,
		road: 19204.2422685185,
		acceleration: -0.183148148148149
	},
	{
		id: 2152,
		time: 2151,
		velocity: 12.9933333333333,
		power: 293.318380096624,
		road: 19217.3818518518,
		acceleration: -0.201388888888888
	},
	{
		id: 2153,
		time: 2152,
		velocity: 12.9297222222222,
		power: 1502.99088030428,
		road: 19230.3700925926,
		acceleration: -0.101296296296297
	},
	{
		id: 2154,
		time: 2153,
		velocity: 12.8897222222222,
		power: 817.51961297656,
		road: 19243.2306481481,
		acceleration: -0.154074074074073
	},
	{
		id: 2155,
		time: 2154,
		velocity: 12.5311111111111,
		power: -248.507725271923,
		road: 19255.8949537037,
		acceleration: -0.238425925925929
	},
	{
		id: 2156,
		time: 2155,
		velocity: 12.2144444444444,
		power: -2173.03068470361,
		road: 19268.2411111111,
		acceleration: -0.39787037037037
	},
	{
		id: 2157,
		time: 2156,
		velocity: 11.6961111111111,
		power: -1773.13106051526,
		road: 19280.2065740741,
		acceleration: -0.363518518518518
	},
	{
		id: 2158,
		time: 2157,
		velocity: 11.4405555555556,
		power: -2109.84261273321,
		road: 19291.7932407407,
		acceleration: -0.394074074074073
	},
	{
		id: 2159,
		time: 2158,
		velocity: 11.0322222222222,
		power: -704.781861443128,
		road: 19303.0506018518,
		acceleration: -0.264537037037037
	},
	{
		id: 2160,
		time: 2159,
		velocity: 10.9025,
		power: 253.493614201254,
		road: 19314.0896759259,
		acceleration: -0.172037037037036
	},
	{
		id: 2161,
		time: 2160,
		velocity: 10.9244444444444,
		power: 1306.81905512225,
		road: 19325.0081944444,
		acceleration: -0.0690740740740772
	},
	{
		id: 2162,
		time: 2161,
		velocity: 10.825,
		power: 810.121180112453,
		road: 19335.8346759259,
		acceleration: -0.114999999999997
	},
	{
		id: 2163,
		time: 2162,
		velocity: 10.5575,
		power: -883.565158504847,
		road: 19346.4644907407,
		acceleration: -0.278333333333334
	},
	{
		id: 2164,
		time: 2163,
		velocity: 10.0894444444444,
		power: -899.213575853882,
		road: 19356.8156481481,
		acceleration: -0.278981481481482
	},
	{
		id: 2165,
		time: 2164,
		velocity: 9.98805555555556,
		power: -227.349432795958,
		road: 19366.9230092592,
		acceleration: -0.208611111111111
	},
	{
		id: 2166,
		time: 2165,
		velocity: 9.93166666666667,
		power: 2075.77972258096,
		road: 19376.9427777778,
		acceleration: 0.0334259259259255
	},
	{
		id: 2167,
		time: 2166,
		velocity: 10.1897222222222,
		power: 4887.09524006489,
		road: 19387.1377777778,
		acceleration: 0.317037037037037
	},
	{
		id: 2168,
		time: 2167,
		velocity: 10.9391666666667,
		power: 5150.72511596506,
		road: 19397.6533796296,
		acceleration: 0.324166666666667
	},
	{
		id: 2169,
		time: 2168,
		velocity: 10.9041666666667,
		power: 3699.8343271137,
		road: 19408.4150462963,
		acceleration: 0.167962962962964
	},
	{
		id: 2170,
		time: 2169,
		velocity: 10.6936111111111,
		power: 619.908352213367,
		road: 19419.1943981481,
		acceleration: -0.132592592592593
	},
	{
		id: 2171,
		time: 2170,
		velocity: 10.5413888888889,
		power: 1138.53892773654,
		road: 19429.8675925926,
		acceleration: -0.0797222222222231
	},
	{
		id: 2172,
		time: 2171,
		velocity: 10.665,
		power: 3464.3882080141,
		road: 19440.5746296296,
		acceleration: 0.147407407407407
	},
	{
		id: 2173,
		time: 2172,
		velocity: 11.1358333333333,
		power: 4091.38012785744,
		road: 19451.4555092592,
		acceleration: 0.200277777777778
	},
	{
		id: 2174,
		time: 2173,
		velocity: 11.1422222222222,
		power: 2753.19394826749,
		road: 19462.4697222222,
		acceleration: 0.0663888888888895
	},
	{
		id: 2175,
		time: 2174,
		velocity: 10.8641666666667,
		power: 241.618978270893,
		road: 19473.4311111111,
		acceleration: -0.172037037037036
	},
	{
		id: 2176,
		time: 2175,
		velocity: 10.6197222222222,
		power: -1803.88421246037,
		road: 19484.122037037,
		acceleration: -0.36888888888889
	},
	{
		id: 2177,
		time: 2176,
		velocity: 10.0355555555556,
		power: -1251.7362110202,
		road: 19494.4711574074,
		acceleration: -0.314722222222223
	},
	{
		id: 2178,
		time: 2177,
		velocity: 9.92,
		power: -1176.41149311141,
		road: 19504.5093055555,
		acceleration: -0.307222222222222
	},
	{
		id: 2179,
		time: 2178,
		velocity: 9.69805555555555,
		power: 432.738428828556,
		road: 19514.3261111111,
		acceleration: -0.135462962962963
	},
	{
		id: 2180,
		time: 2179,
		velocity: 9.62916666666667,
		power: 390.609605235055,
		road: 19524.00625,
		acceleration: -0.13787037037037
	},
	{
		id: 2181,
		time: 2180,
		velocity: 9.50638888888889,
		power: 869.728641978836,
		road: 19533.5756481481,
		acceleration: -0.0836111111111109
	},
	{
		id: 2182,
		time: 2181,
		velocity: 9.44722222222222,
		power: 261.793550519282,
		road: 19543.0288888889,
		acceleration: -0.148703703703703
	},
	{
		id: 2183,
		time: 2182,
		velocity: 9.18305555555555,
		power: 893.85755672976,
		road: 19552.3697222222,
		acceleration: -0.0761111111111124
	},
	{
		id: 2184,
		time: 2183,
		velocity: 9.27805555555556,
		power: -7.42009092333045,
		road: 19561.5844444444,
		acceleration: -0.17611111111111
	},
	{
		id: 2185,
		time: 2184,
		velocity: 8.91888888888889,
		power: 328.679483484204,
		road: 19570.6433333333,
		acceleration: -0.135555555555557
	},
	{
		id: 2186,
		time: 2185,
		velocity: 8.77638888888889,
		power: -2948.64544809265,
		road: 19579.3718518518,
		acceleration: -0.525185185185185
	},
	{
		id: 2187,
		time: 2186,
		velocity: 7.7025,
		power: -2392.67003877214,
		road: 19587.6022685185,
		acceleration: -0.471018518518518
	},
	{
		id: 2188,
		time: 2187,
		velocity: 7.50583333333333,
		power: -2145.04458188833,
		road: 19595.3713888889,
		acceleration: -0.451574074074074
	},
	{
		id: 2189,
		time: 2188,
		velocity: 7.42166666666667,
		power: 196.336419416385,
		road: 19602.8490740741,
		acceleration: -0.131296296296296
	},
	{
		id: 2190,
		time: 2189,
		velocity: 7.30861111111111,
		power: 465.219157923077,
		road: 19610.2153240741,
		acceleration: -0.0915740740740745
	},
	{
		id: 2191,
		time: 2190,
		velocity: 7.23111111111111,
		power: 451.588891580249,
		road: 19617.4898148148,
		acceleration: -0.0919444444444455
	},
	{
		id: 2192,
		time: 2191,
		velocity: 7.14583333333333,
		power: 549.317516314246,
		road: 19624.6802314815,
		acceleration: -0.076203703703702
	},
	{
		id: 2193,
		time: 2192,
		velocity: 7.08,
		power: 682.492235018523,
		road: 19631.8049074074,
		acceleration: -0.0552777777777775
	},
	{
		id: 2194,
		time: 2193,
		velocity: 7.06527777777778,
		power: 318.573693360881,
		road: 19638.8480555555,
		acceleration: -0.107777777777779
	},
	{
		id: 2195,
		time: 2194,
		velocity: 6.8225,
		power: -551.189275391098,
		road: 19645.7181481481,
		acceleration: -0.238333333333333
	},
	{
		id: 2196,
		time: 2195,
		velocity: 6.365,
		power: -1790.41185805116,
		road: 19652.2492129629,
		acceleration: -0.439722222222222
	},
	{
		id: 2197,
		time: 2196,
		velocity: 5.74611111111111,
		power: -3653.62647417417,
		road: 19658.1618055555,
		acceleration: -0.797222222222222
	},
	{
		id: 2198,
		time: 2197,
		velocity: 4.43083333333333,
		power: -3785.74201137623,
		road: 19663.2102314815,
		acceleration: -0.931111111111113
	},
	{
		id: 2199,
		time: 2198,
		velocity: 3.57166666666667,
		power: -3745.98501403039,
		road: 19667.2346296296,
		acceleration: -1.11694444444444
	},
	{
		id: 2200,
		time: 2199,
		velocity: 2.39527777777778,
		power: -2290.36432910111,
		road: 19670.2316203703,
		acceleration: -0.93787037037037
	},
	{
		id: 2201,
		time: 2200,
		velocity: 1.61722222222222,
		power: -1462.74781403637,
		road: 19672.3266203704,
		acceleration: -0.866111111111111
	},
	{
		id: 2202,
		time: 2201,
		velocity: 0.973333333333333,
		power: -802.062811408683,
		road: 19673.5893518518,
		acceleration: -0.798425925925926
	},
	{
		id: 2203,
		time: 2202,
		velocity: 0,
		power: -231.549849589134,
		road: 19674.1833333333,
		acceleration: -0.539074074074074
	},
	{
		id: 2204,
		time: 2203,
		velocity: 0,
		power: -30.2626409356725,
		road: 19674.3455555555,
		acceleration: -0.324444444444444
	},
	{
		id: 2205,
		time: 2204,
		velocity: 0,
		power: 0,
		road: 19674.3455555555,
		acceleration: 0
	},
	{
		id: 2206,
		time: 2205,
		velocity: 0,
		power: 0,
		road: 19674.3455555555,
		acceleration: 0
	},
	{
		id: 2207,
		time: 2206,
		velocity: 0,
		power: 0,
		road: 19674.3455555555,
		acceleration: 0
	},
	{
		id: 2208,
		time: 2207,
		velocity: 0,
		power: 256.81157982664,
		road: 19674.6831481481,
		acceleration: 0.675185185185185
	},
	{
		id: 2209,
		time: 2208,
		velocity: 2.02555555555556,
		power: 1408.68238227853,
		road: 19675.9031018518,
		acceleration: 1.08953703703704
	},
	{
		id: 2210,
		time: 2209,
		velocity: 3.26861111111111,
		power: 3705.75104771837,
		road: 19678.3881481481,
		acceleration: 1.44064814814815
	},
	{
		id: 2211,
		time: 2210,
		velocity: 4.32194444444444,
		power: 4592.46553048756,
		road: 19682.1659259259,
		acceleration: 1.14481481481481
	},
	{
		id: 2212,
		time: 2211,
		velocity: 5.46,
		power: 3723.2768876882,
		road: 19686.8633796296,
		acceleration: 0.694537037037037
	},
	{
		id: 2213,
		time: 2212,
		velocity: 5.35222222222222,
		power: 2410.10691329143,
		road: 19692.0798611111,
		acceleration: 0.343518518518518
	},
	{
		id: 2214,
		time: 2213,
		velocity: 5.3525,
		power: 2132.1090008242,
		road: 19697.5990740741,
		acceleration: 0.261944444444444
	},
	{
		id: 2215,
		time: 2214,
		velocity: 6.24583333333333,
		power: 2138.32038931774,
		road: 19703.3710648148,
		acceleration: 0.243611111111111
	},
	{
		id: 2216,
		time: 2215,
		velocity: 6.08305555555556,
		power: 1917.5196424815,
		road: 19709.3594907407,
		acceleration: 0.189259259259261
	},
	{
		id: 2217,
		time: 2216,
		velocity: 5.92027777777778,
		power: -3123.3974694537,
		road: 19715.0825,
		acceleration: -0.720092592592593
	},
	{
		id: 2218,
		time: 2217,
		velocity: 4.08555555555556,
		power: -3016.00090359992,
		road: 19720.0556018518,
		acceleration: -0.779722222222222
	},
	{
		id: 2219,
		time: 2218,
		velocity: 3.74388888888889,
		power: -2683.69885722992,
		road: 19724.2318518518,
		acceleration: -0.813981481481481
	},
	{
		id: 2220,
		time: 2219,
		velocity: 3.47833333333333,
		power: -590.428727879128,
		road: 19727.8477314815,
		acceleration: -0.306759259259259
	},
	{
		id: 2221,
		time: 2220,
		velocity: 3.16527777777778,
		power: -333.246603334292,
		road: 19731.1908796296,
		acceleration: -0.238703703703704
	},
	{
		id: 2222,
		time: 2221,
		velocity: 3.02777777777778,
		power: -1294.45885396465,
		road: 19734.1153240741,
		acceleration: -0.598703703703704
	},
	{
		id: 2223,
		time: 2222,
		velocity: 1.68222222222222,
		power: -1271.23323449562,
		road: 19736.3792592592,
		acceleration: -0.722314814814815
	},
	{
		id: 2224,
		time: 2223,
		velocity: 0.998333333333333,
		power: -1167.29856755887,
		road: 19737.7774074074,
		acceleration: -1.00925925925926
	},
	{
		id: 2225,
		time: 2224,
		velocity: 0,
		power: -251.605377779223,
		road: 19738.3905555555,
		acceleration: -0.560740740740741
	},
	{
		id: 2226,
		time: 2225,
		velocity: 0,
		power: -32.3535312865497,
		road: 19738.5569444444,
		acceleration: -0.332777777777778
	},
	{
		id: 2227,
		time: 2226,
		velocity: 0,
		power: 0,
		road: 19738.5569444444,
		acceleration: 0
	},
	{
		id: 2228,
		time: 2227,
		velocity: 0,
		power: 0,
		road: 19738.5569444444,
		acceleration: 0
	},
	{
		id: 2229,
		time: 2228,
		velocity: 0,
		power: 0,
		road: 19738.5569444444,
		acceleration: 0
	},
	{
		id: 2230,
		time: 2229,
		velocity: 0,
		power: 0,
		road: 19738.5569444444,
		acceleration: 0
	},
	{
		id: 2231,
		time: 2230,
		velocity: 0,
		power: 0,
		road: 19738.5569444444,
		acceleration: 0
	},
	{
		id: 2232,
		time: 2231,
		velocity: 0,
		power: 0,
		road: 19738.5569444444,
		acceleration: 0
	},
	{
		id: 2233,
		time: 2232,
		velocity: 0,
		power: 0,
		road: 19738.5569444444,
		acceleration: 0
	},
	{
		id: 2234,
		time: 2233,
		velocity: 0,
		power: 0,
		road: 19738.5569444444,
		acceleration: 0
	},
	{
		id: 2235,
		time: 2234,
		velocity: 0,
		power: 90.0897774071158,
		road: 19738.7454166667,
		acceleration: 0.376944444444444
	},
	{
		id: 2236,
		time: 2235,
		velocity: 1.13083333333333,
		power: 1034.44538686158,
		road: 19739.6569444444,
		acceleration: 1.06916666666667
	},
	{
		id: 2237,
		time: 2236,
		velocity: 3.2075,
		power: 3185.87518606549,
		road: 19741.815787037,
		acceleration: 1.42546296296296
	},
	{
		id: 2238,
		time: 2237,
		velocity: 4.27638888888889,
		power: 3876.20526578985,
		road: 19745.2200925926,
		acceleration: 1.06546296296296
	},
	{
		id: 2239,
		time: 2238,
		velocity: 4.32722222222222,
		power: 1311.30534843824,
		road: 19749.2597685185,
		acceleration: 0.205277777777778
	},
	{
		id: 2240,
		time: 2239,
		velocity: 3.82333333333333,
		power: -2472.59848289677,
		road: 19752.9848611111,
		acceleration: -0.834444444444444
	},
	{
		id: 2241,
		time: 2240,
		velocity: 1.77305555555556,
		power: -3217.25505286907,
		road: 19755.5715277778,
		acceleration: -1.44240740740741
	},
	{
		id: 2242,
		time: 2241,
		velocity: 0,
		power: -1334.31271661679,
		road: 19756.7997685185,
		acceleration: -1.27444444444444
	},
	{
		id: 2243,
		time: 2242,
		velocity: 0,
		power: -129.756456741391,
		road: 19757.0952777778,
		acceleration: -0.591018518518519
	},
	{
		id: 2244,
		time: 2243,
		velocity: 0,
		power: 0,
		road: 19757.0952777778,
		acceleration: 0
	},
	{
		id: 2245,
		time: 2244,
		velocity: 0,
		power: 0,
		road: 19757.0952777778,
		acceleration: 0
	},
	{
		id: 2246,
		time: 2245,
		velocity: 0,
		power: 0,
		road: 19757.0952777778,
		acceleration: 0
	},
	{
		id: 2247,
		time: 2246,
		velocity: 0,
		power: 0,
		road: 19757.0952777778,
		acceleration: 0
	},
	{
		id: 2248,
		time: 2247,
		velocity: 0,
		power: 0,
		road: 19757.0952777778,
		acceleration: 0
	},
	{
		id: 2249,
		time: 2248,
		velocity: 0,
		power: 0,
		road: 19757.0952777778,
		acceleration: 0
	},
	{
		id: 2250,
		time: 2249,
		velocity: 0,
		power: 0,
		road: 19757.0952777778,
		acceleration: 0
	},
	{
		id: 2251,
		time: 2250,
		velocity: 0,
		power: 0,
		road: 19757.0952777778,
		acceleration: 0
	},
	{
		id: 2252,
		time: 2251,
		velocity: 0,
		power: 3.63953910607976,
		road: 19757.1175925926,
		acceleration: 0.0446296296296296
	},
	{
		id: 2253,
		time: 2252,
		velocity: 0.133888888888889,
		power: 5.39210615238045,
		road: 19757.1622222222,
		acceleration: 0
	},
	{
		id: 2254,
		time: 2253,
		velocity: 0,
		power: 5.39210615238045,
		road: 19757.2068518518,
		acceleration: 0
	},
	{
		id: 2255,
		time: 2254,
		velocity: 0,
		power: 1.75254291747888,
		road: 19757.2291666667,
		acceleration: -0.0446296296296296
	},
	{
		id: 2256,
		time: 2255,
		velocity: 0,
		power: 0,
		road: 19757.2291666667,
		acceleration: 0
	},
	{
		id: 2257,
		time: 2256,
		velocity: 0,
		power: 0,
		road: 19757.2291666667,
		acceleration: 0
	},
	{
		id: 2258,
		time: 2257,
		velocity: 0,
		power: 0,
		road: 19757.2291666667,
		acceleration: 0
	},
	{
		id: 2259,
		time: 2258,
		velocity: 0,
		power: 0,
		road: 19757.2291666667,
		acceleration: 0
	},
	{
		id: 2260,
		time: 2259,
		velocity: 0,
		power: 0,
		road: 19757.2291666667,
		acceleration: 0
	},
	{
		id: 2261,
		time: 2260,
		velocity: 0,
		power: 3.33942752981086,
		road: 19757.25,
		acceleration: 0.0416666666666667
	},
	{
		id: 2262,
		time: 2261,
		velocity: 0.125,
		power: 5.03411821751645,
		road: 19757.2916666667,
		acceleration: 0
	},
	{
		id: 2263,
		time: 2262,
		velocity: 0,
		power: 5.03411821751645,
		road: 19757.3333333333,
		acceleration: 0
	},
	{
		id: 2264,
		time: 2263,
		velocity: 0,
		power: 1.69467105263158,
		road: 19757.3541666667,
		acceleration: -0.0416666666666667
	},
	{
		id: 2265,
		time: 2264,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2266,
		time: 2265,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2267,
		time: 2266,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2268,
		time: 2267,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2269,
		time: 2268,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2270,
		time: 2269,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2271,
		time: 2270,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2272,
		time: 2271,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2273,
		time: 2272,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2274,
		time: 2273,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2275,
		time: 2274,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2276,
		time: 2275,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2277,
		time: 2276,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2278,
		time: 2277,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2279,
		time: 2278,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2280,
		time: 2279,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2281,
		time: 2280,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2282,
		time: 2281,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2283,
		time: 2282,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2284,
		time: 2283,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2285,
		time: 2284,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2286,
		time: 2285,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2287,
		time: 2286,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2288,
		time: 2287,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2289,
		time: 2288,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2290,
		time: 2289,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2291,
		time: 2290,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2292,
		time: 2291,
		velocity: 0,
		power: 0,
		road: 19757.3541666667,
		acceleration: 0
	},
	{
		id: 2293,
		time: 2292,
		velocity: 0,
		power: 1.77519973447038,
		road: 19757.3664814815,
		acceleration: 0.0246296296296296
	},
	{
		id: 2294,
		time: 2293,
		velocity: 0.0738888888888889,
		power: 2.97570811091606,
		road: 19757.3911111111,
		acceleration: 0
	},
	{
		id: 2295,
		time: 2294,
		velocity: 0,
		power: 2.97570811091606,
		road: 19757.4157407407,
		acceleration: 0
	},
	{
		id: 2296,
		time: 2295,
		velocity: 0,
		power: 1.20050432098765,
		road: 19757.4280555555,
		acceleration: -0.0246296296296296
	},
	{
		id: 2297,
		time: 2296,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2298,
		time: 2297,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2299,
		time: 2298,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2300,
		time: 2299,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2301,
		time: 2300,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2302,
		time: 2301,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2303,
		time: 2302,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2304,
		time: 2303,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2305,
		time: 2304,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2306,
		time: 2305,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2307,
		time: 2306,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2308,
		time: 2307,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2309,
		time: 2308,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2310,
		time: 2309,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2311,
		time: 2310,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2312,
		time: 2311,
		velocity: 0,
		power: 0,
		road: 19757.4280555555,
		acceleration: 0
	},
	{
		id: 2313,
		time: 2312,
		velocity: 0,
		power: 204.622603007726,
		road: 19757.7262962963,
		acceleration: 0.596481481481482
	},
	{
		id: 2314,
		time: 2313,
		velocity: 1.78944444444444,
		power: 848.145271457291,
		road: 19758.7124074074,
		acceleration: 0.779259259259259
	},
	{
		id: 2315,
		time: 2314,
		velocity: 2.33777777777778,
		power: 1551.7515463259,
		road: 19760.4850462963,
		acceleration: 0.793796296296296
	},
	{
		id: 2316,
		time: 2315,
		velocity: 2.38138888888889,
		power: 562.203872547061,
		road: 19762.7219444444,
		acceleration: 0.134722222222222
	},
	{
		id: 2317,
		time: 2316,
		velocity: 2.19361111111111,
		power: 33.3091184616448,
		road: 19764.9688888889,
		acceleration: -0.11462962962963
	},
	{
		id: 2318,
		time: 2317,
		velocity: 1.99388888888889,
		power: -127.034513705709,
		road: 19767.0615740741,
		acceleration: -0.193888888888889
	},
	{
		id: 2319,
		time: 2318,
		velocity: 1.79972222222222,
		power: -544.173838079029,
		road: 19768.8305092592,
		acceleration: -0.453611111111111
	},
	{
		id: 2320,
		time: 2319,
		velocity: 0.832777777777778,
		power: -615.085838794831,
		road: 19770.040324074,
		acceleration: -0.66462962962963
	},
	{
		id: 2321,
		time: 2320,
		velocity: 0,
		power: -258.436723992081,
		road: 19770.6178703703,
		acceleration: -0.599907407407407
	},
	{
		id: 2322,
		time: 2321,
		velocity: 0,
		power: -19.7319145873944,
		road: 19770.7566666666,
		acceleration: -0.277592592592593
	},
	{
		id: 2323,
		time: 2322,
		velocity: 0,
		power: 0.307858917719158,
		road: 19770.7591203703,
		acceleration: 0.00490740740740741
	},
	{
		id: 2324,
		time: 2323,
		velocity: 0.0147222222222222,
		power: 0.592902695737212,
		road: 19770.7640277778,
		acceleration: 0
	},
	{
		id: 2325,
		time: 2324,
		velocity: 0,
		power: 0.592902695737212,
		road: 19770.7689351852,
		acceleration: 0
	},
	{
		id: 2326,
		time: 2325,
		velocity: 0,
		power: 0.285043745938921,
		road: 19770.7713888889,
		acceleration: -0.00490740740740741
	},
	{
		id: 2327,
		time: 2326,
		velocity: 0,
		power: 0,
		road: 19770.7713888889,
		acceleration: 0
	},
	{
		id: 2328,
		time: 2327,
		velocity: 0,
		power: 0,
		road: 19770.7713888889,
		acceleration: 0
	},
	{
		id: 2329,
		time: 2328,
		velocity: 0,
		power: 0,
		road: 19770.7713888889,
		acceleration: 0
	},
	{
		id: 2330,
		time: 2329,
		velocity: 0,
		power: 0,
		road: 19770.7713888889,
		acceleration: 0
	},
	{
		id: 2331,
		time: 2330,
		velocity: 0,
		power: 0,
		road: 19770.7713888889,
		acceleration: 0
	},
	{
		id: 2332,
		time: 2331,
		velocity: 0,
		power: 434.788300326426,
		road: 19771.2194907407,
		acceleration: 0.896203703703704
	},
	{
		id: 2333,
		time: 2332,
		velocity: 2.68861111111111,
		power: 2089.17214366461,
		road: 19772.7643055555,
		acceleration: 1.29722222222222
	},
	{
		id: 2334,
		time: 2333,
		velocity: 3.89166666666667,
		power: 5034.35962277243,
		road: 19775.7729166666,
		acceleration: 1.63037037037037
	},
	{
		id: 2335,
		time: 2334,
		velocity: 4.89111111111111,
		power: 3286.16470864502,
		road: 19779.9430092592,
		acceleration: 0.692592592592592
	},
	{
		id: 2336,
		time: 2335,
		velocity: 4.76638888888889,
		power: 1658.92979459127,
		road: 19784.5780555555,
		acceleration: 0.237314814814816
	},
	{
		id: 2337,
		time: 2336,
		velocity: 4.60361111111111,
		power: 271.493568516245,
		road: 19789.2921296296,
		acceleration: -0.0792592592592598
	},
	{
		id: 2338,
		time: 2337,
		velocity: 4.65333333333333,
		power: 284.011126872758,
		road: 19793.929074074,
		acceleration: -0.0750000000000002
	},
	{
		id: 2339,
		time: 2338,
		velocity: 4.54138888888889,
		power: -1474.13605197094,
		road: 19798.2811574074,
		acceleration: -0.494722222222221
	},
	{
		id: 2340,
		time: 2339,
		velocity: 3.11944444444444,
		power: -2033.17969532903,
		road: 19802.0327777778,
		acceleration: -0.706203703703704
	},
	{
		id: 2341,
		time: 2340,
		velocity: 2.53472222222222,
		power: -2258.0307207562,
		road: 19804.9584722222,
		acceleration: -0.945648148148148
	},
	{
		id: 2342,
		time: 2341,
		velocity: 1.70444444444444,
		power: -925.392836451586,
		road: 19807.1206944444,
		acceleration: -0.581296296296296
	},
	{
		id: 2343,
		time: 2342,
		velocity: 1.37555555555556,
		power: -620.239040626952,
		road: 19808.7237962963,
		acceleration: -0.536944444444444
	},
	{
		id: 2344,
		time: 2343,
		velocity: 0.923888888888889,
		power: -438.195952609737,
		road: 19809.7743518518,
		acceleration: -0.568148148148148
	},
	{
		id: 2345,
		time: 2344,
		velocity: 0,
		power: -168.428075793771,
		road: 19810.3115740741,
		acceleration: -0.458518518518518
	},
	{
		id: 2346,
		time: 2345,
		velocity: 0,
		power: -26.3210541585445,
		road: 19810.4655555555,
		acceleration: -0.307962962962963
	},
	{
		id: 2347,
		time: 2346,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2348,
		time: 2347,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2349,
		time: 2348,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2350,
		time: 2349,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2351,
		time: 2350,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2352,
		time: 2351,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2353,
		time: 2352,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2354,
		time: 2353,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2355,
		time: 2354,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2356,
		time: 2355,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2357,
		time: 2356,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2358,
		time: 2357,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2359,
		time: 2358,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2360,
		time: 2359,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2361,
		time: 2360,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2362,
		time: 2361,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2363,
		time: 2362,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2364,
		time: 2363,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2365,
		time: 2364,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2366,
		time: 2365,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2367,
		time: 2366,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2368,
		time: 2367,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2369,
		time: 2368,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2370,
		time: 2369,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2371,
		time: 2370,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2372,
		time: 2371,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2373,
		time: 2372,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2374,
		time: 2373,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2375,
		time: 2374,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2376,
		time: 2375,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2377,
		time: 2376,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2378,
		time: 2377,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2379,
		time: 2378,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2380,
		time: 2379,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2381,
		time: 2380,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2382,
		time: 2381,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2383,
		time: 2382,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2384,
		time: 2383,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2385,
		time: 2384,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2386,
		time: 2385,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2387,
		time: 2386,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2388,
		time: 2387,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2389,
		time: 2388,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2390,
		time: 2389,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2391,
		time: 2390,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2392,
		time: 2391,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2393,
		time: 2392,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2394,
		time: 2393,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2395,
		time: 2394,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2396,
		time: 2395,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2397,
		time: 2396,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2398,
		time: 2397,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2399,
		time: 2398,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2400,
		time: 2399,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2401,
		time: 2400,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2402,
		time: 2401,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2403,
		time: 2402,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2404,
		time: 2403,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2405,
		time: 2404,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2406,
		time: 2405,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2407,
		time: 2406,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2408,
		time: 2407,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2409,
		time: 2408,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2410,
		time: 2409,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2411,
		time: 2410,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2412,
		time: 2411,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2413,
		time: 2412,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2414,
		time: 2413,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2415,
		time: 2414,
		velocity: 0,
		power: 0,
		road: 19810.4655555555,
		acceleration: 0
	},
	{
		id: 2416,
		time: 2415,
		velocity: 0,
		power: 1.0962096146547,
		road: 19810.4736111111,
		acceleration: 0.0161111111111111
	},
	{
		id: 2417,
		time: 2416,
		velocity: 0.0483333333333333,
		power: 1.94651279656086,
		road: 19810.4897222222,
		acceleration: 0
	},
	{
		id: 2418,
		time: 2417,
		velocity: 0,
		power: 1.94651279656086,
		road: 19810.5058333333,
		acceleration: 0
	},
	{
		id: 2419,
		time: 2418,
		velocity: 0,
		power: 0.850302046783626,
		road: 19810.5138888889,
		acceleration: -0.0161111111111111
	},
	{
		id: 2420,
		time: 2419,
		velocity: 0,
		power: 0,
		road: 19810.5138888889,
		acceleration: 0
	},
	{
		id: 2421,
		time: 2420,
		velocity: 0,
		power: 0,
		road: 19810.5138888889,
		acceleration: 0
	},
	{
		id: 2422,
		time: 2421,
		velocity: 0,
		power: 0,
		road: 19810.5138888889,
		acceleration: 0
	},
	{
		id: 2423,
		time: 2422,
		velocity: 0,
		power: 0,
		road: 19810.5138888889,
		acceleration: 0
	},
	{
		id: 2424,
		time: 2423,
		velocity: 0,
		power: 0,
		road: 19810.5138888889,
		acceleration: 0
	},
	{
		id: 2425,
		time: 2424,
		velocity: 0,
		power: 0,
		road: 19810.5138888889,
		acceleration: 0
	},
	{
		id: 2426,
		time: 2425,
		velocity: 0,
		power: 0,
		road: 19810.5138888889,
		acceleration: 0
	},
	{
		id: 2427,
		time: 2426,
		velocity: 0,
		power: 188.884258443311,
		road: 19810.7993055555,
		acceleration: 0.570833333333333
	},
	{
		id: 2428,
		time: 2427,
		velocity: 1.7125,
		power: 1316.28978007504,
		road: 19811.9235185185,
		acceleration: 1.10675925925926
	},
	{
		id: 2429,
		time: 2428,
		velocity: 3.32027777777778,
		power: 3371.97252400008,
		road: 19814.2875,
		acceleration: 1.37277777777778
	},
	{
		id: 2430,
		time: 2429,
		velocity: 4.11833333333333,
		power: 4403.31232848448,
		road: 19817.9105092592,
		acceleration: 1.14527777777778
	},
	{
		id: 2431,
		time: 2430,
		velocity: 5.14833333333333,
		power: 3161.14033653681,
		road: 19822.4068518518,
		acceleration: 0.601388888888889
	},
	{
		id: 2432,
		time: 2431,
		velocity: 5.12444444444444,
		power: 2093.52903465615,
		road: 19827.3559259259,
		acceleration: 0.304074074074074
	},
	{
		id: 2433,
		time: 2432,
		velocity: 5.03055555555556,
		power: 280.547409845609,
		road: 19832.415324074,
		acceleration: -0.0834259259259253
	},
	{
		id: 2434,
		time: 2433,
		velocity: 4.89805555555556,
		power: -101.673412669595,
		road: 19837.3516203703,
		acceleration: -0.162777777777777
	},
	{
		id: 2435,
		time: 2434,
		velocity: 4.63611111111111,
		power: -551.558852447672,
		road: 19842.0750925926,
		acceleration: -0.26287037037037
	},
	{
		id: 2436,
		time: 2435,
		velocity: 4.24194444444445,
		power: -989.345409394553,
		road: 19846.4797222222,
		acceleration: -0.374814814814814
	},
	{
		id: 2437,
		time: 2436,
		velocity: 3.77361111111111,
		power: -2906.80747444104,
		road: 19850.2199537037,
		acceleration: -0.953981481481481
	},
	{
		id: 2438,
		time: 2437,
		velocity: 1.77416666666667,
		power: -2676.48197708464,
		road: 19852.8889351852,
		acceleration: -1.18851851851852
	},
	{
		id: 2439,
		time: 2438,
		velocity: 0.676388888888889,
		power: -1547.6955675217,
		road: 19854.3347222222,
		acceleration: -1.25787037037037
	},
	{
		id: 2440,
		time: 2439,
		velocity: 0,
		power: -224.7601939503,
		road: 19854.8634259259,
		acceleration: -0.576296296296296
	},
	{
		id: 2441,
		time: 2440,
		velocity: 0.0452777777777778,
		power: -11.8593223838506,
		road: 19854.99125,
		acceleration: -0.225462962962963
	},
	{
		id: 2442,
		time: 2441,
		velocity: 0,
		power: 1.8234571294852,
		road: 19855.0063425926,
		acceleration: 0
	},
	{
		id: 2443,
		time: 2442,
		velocity: 0,
		power: 0.80382883365822,
		road: 19855.0138888889,
		acceleration: -0.0150925925925926
	},
	{
		id: 2444,
		time: 2443,
		velocity: 0,
		power: 0,
		road: 19855.0138888889,
		acceleration: 0
	},
	{
		id: 2445,
		time: 2444,
		velocity: 0,
		power: 0,
		road: 19855.0138888889,
		acceleration: 0
	},
	{
		id: 2446,
		time: 2445,
		velocity: 0,
		power: 0,
		road: 19855.0138888889,
		acceleration: 0
	},
	{
		id: 2447,
		time: 2446,
		velocity: 0,
		power: 0,
		road: 19855.0138888889,
		acceleration: 0
	},
	{
		id: 2448,
		time: 2447,
		velocity: 0,
		power: 0,
		road: 19855.0138888889,
		acceleration: 0
	},
	{
		id: 2449,
		time: 2448,
		velocity: 0,
		power: 2.36288134770212,
		road: 19855.0295833333,
		acceleration: 0.0313888888888889
	},
	{
		id: 2450,
		time: 2449,
		velocity: 0.0941666666666667,
		power: 3.7923562626557,
		road: 19855.0609722222,
		acceleration: 0
	},
	{
		id: 2451,
		time: 2450,
		velocity: 0,
		power: 3.7923562626557,
		road: 19855.0923611111,
		acceleration: 0
	},
	{
		id: 2452,
		time: 2451,
		velocity: 0,
		power: 1.42946652046784,
		road: 19855.1080555555,
		acceleration: -0.0313888888888889
	},
	{
		id: 2453,
		time: 2452,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2454,
		time: 2453,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2455,
		time: 2454,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2456,
		time: 2455,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2457,
		time: 2456,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2458,
		time: 2457,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2459,
		time: 2458,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2460,
		time: 2459,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2461,
		time: 2460,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2462,
		time: 2461,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2463,
		time: 2462,
		velocity: 0,
		power: 0,
		road: 19855.1080555555,
		acceleration: 0
	},
	{
		id: 2464,
		time: 2463,
		velocity: 0,
		power: 0.802430812540455,
		road: 19855.1141203704,
		acceleration: 0.0121296296296296
	},
	{
		id: 2465,
		time: 2464,
		velocity: 0.0363888888888889,
		power: 5.24255156992269,
		road: 19855.1468981481,
		acceleration: 0.0412962962962963
	},
	{
		id: 2466,
		time: 2465,
		velocity: 0.123888888888889,
		power: 6.45489067988616,
		road: 19855.2003240741,
		acceleration: 0
	},
	{
		id: 2467,
		time: 2466,
		velocity: 0,
		power: 5.17787625445466,
		road: 19855.2476851852,
		acceleration: -0.0121296296296296
	},
	{
		id: 2468,
		time: 2467,
		velocity: 0,
		power: 1.68685227420403,
		road: 19855.2683333333,
		acceleration: -0.0412962962962963
	},
	{
		id: 2469,
		time: 2468,
		velocity: 0,
		power: 0,
		road: 19855.2683333333,
		acceleration: 0
	},
	{
		id: 2470,
		time: 2469,
		velocity: 0,
		power: 0.236156951172975,
		road: 19855.2702314815,
		acceleration: 0.0037962962962963
	},
	{
		id: 2471,
		time: 2470,
		velocity: 0.0113888888888889,
		power: 0.458660556017165,
		road: 19855.2740277778,
		acceleration: 0
	},
	{
		id: 2472,
		time: 2471,
		velocity: 0,
		power: 0.458660556017165,
		road: 19855.2778240741,
		acceleration: 0
	},
	{
		id: 2473,
		time: 2472,
		velocity: 0,
		power: 0.222503589993502,
		road: 19855.2797222222,
		acceleration: -0.0037962962962963
	},
	{
		id: 2474,
		time: 2473,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2475,
		time: 2474,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2476,
		time: 2475,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2477,
		time: 2476,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2478,
		time: 2477,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2479,
		time: 2478,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2480,
		time: 2479,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2481,
		time: 2480,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2482,
		time: 2481,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2483,
		time: 2482,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2484,
		time: 2483,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2485,
		time: 2484,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2486,
		time: 2485,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2487,
		time: 2486,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2488,
		time: 2487,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2489,
		time: 2488,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2490,
		time: 2489,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2491,
		time: 2490,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2492,
		time: 2491,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2493,
		time: 2492,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2494,
		time: 2493,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2495,
		time: 2494,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2496,
		time: 2495,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2497,
		time: 2496,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2498,
		time: 2497,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2499,
		time: 2498,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2500,
		time: 2499,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2501,
		time: 2500,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2502,
		time: 2501,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2503,
		time: 2502,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2504,
		time: 2503,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2505,
		time: 2504,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2506,
		time: 2505,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2507,
		time: 2506,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2508,
		time: 2507,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2509,
		time: 2508,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2510,
		time: 2509,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2511,
		time: 2510,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2512,
		time: 2511,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2513,
		time: 2512,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2514,
		time: 2513,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2515,
		time: 2514,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2516,
		time: 2515,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2517,
		time: 2516,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2518,
		time: 2517,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2519,
		time: 2518,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2520,
		time: 2519,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2521,
		time: 2520,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2522,
		time: 2521,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2523,
		time: 2522,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2524,
		time: 2523,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2525,
		time: 2524,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2526,
		time: 2525,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2527,
		time: 2526,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2528,
		time: 2527,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2529,
		time: 2528,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2530,
		time: 2529,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2531,
		time: 2530,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2532,
		time: 2531,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2533,
		time: 2532,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2534,
		time: 2533,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2535,
		time: 2534,
		velocity: 0,
		power: 0,
		road: 19855.2797222222,
		acceleration: 0
	},
	{
		id: 2536,
		time: 2535,
		velocity: 0,
		power: 230.228417377667,
		road: 19855.5978240741,
		acceleration: 0.636203703703704
	},
	{
		id: 2537,
		time: 2536,
		velocity: 1.90861111111111,
		power: 1613.89042975835,
		road: 19856.8496759259,
		acceleration: 1.2312962962963
	},
	{
		id: 2538,
		time: 2537,
		velocity: 3.69388888888889,
		power: 3577.23751586774,
		road: 19859.3928240741,
		acceleration: 1.3512962962963
	},
	{
		id: 2539,
		time: 2538,
		velocity: 4.05388888888889,
		power: 2805.8474901943,
		road: 19862.95875,
		acceleration: 0.694259259259259
	},
	{
		id: 2540,
		time: 2539,
		velocity: 3.99138888888889,
		power: 994.261597574431,
		road: 19866.9353240741,
		acceleration: 0.127037037037037
	},
	{
		id: 2541,
		time: 2540,
		velocity: 4.075,
		power: 773.098882490593,
		road: 19871.007037037,
		acceleration: 0.0632407407407403
	},
	{
		id: 2542,
		time: 2541,
		velocity: 4.24361111111111,
		power: 2018.27689769395,
		road: 19875.2896296296,
		acceleration: 0.358518518518519
	},
	{
		id: 2543,
		time: 2542,
		velocity: 5.06694444444444,
		power: 3302.1583807487,
		road: 19880.0467592592,
		acceleration: 0.590555555555556
	},
	{
		id: 2544,
		time: 2543,
		velocity: 5.84666666666667,
		power: 6284.52885348744,
		road: 19885.6199074074,
		acceleration: 1.04148148148148
	},
	{
		id: 2545,
		time: 2544,
		velocity: 7.36805555555556,
		power: 8012.07011673469,
		road: 19892.2709259259,
		acceleration: 1.11425925925926
	},
	{
		id: 2546,
		time: 2545,
		velocity: 8.40972222222222,
		power: 11452.1049506523,
		road: 19900.1603703704,
		acceleration: 1.36259259259259
	},
	{
		id: 2547,
		time: 2546,
		velocity: 9.93444444444444,
		power: 8343.0352915991,
		road: 19909.1329166666,
		acceleration: 0.80361111111111
	},
	{
		id: 2548,
		time: 2547,
		velocity: 9.77888888888889,
		power: 5348.0086588462,
		road: 19918.710787037,
		acceleration: 0.407037037037039
	},
	{
		id: 2549,
		time: 2548,
		velocity: 9.63083333333333,
		power: -2841.09985174907,
		road: 19928.2464351852,
		acceleration: -0.491481481481483
	},
	{
		id: 2550,
		time: 2549,
		velocity: 8.46,
		power: -5912.53992181476,
		road: 19937.0998148148,
		acceleration: -0.873055555555556
	},
	{
		id: 2551,
		time: 2550,
		velocity: 7.15972222222222,
		power: -10209.3472869817,
		road: 19944.7336111111,
		acceleration: -1.56611111111111
	},
	{
		id: 2552,
		time: 2551,
		velocity: 4.9325,
		power: -9580.74737808386,
		road: 19950.6602314815,
		acceleration: -1.84824074074074
	},
	{
		id: 2553,
		time: 2552,
		velocity: 2.91527777777778,
		power: -7342.63774158879,
		road: 19954.6171759259,
		acceleration: -2.09111111111111
	},
	{
		id: 2554,
		time: 2553,
		velocity: 0.886388888888889,
		power: -2934.62683516289,
		road: 19956.7318981481,
		acceleration: -1.59333333333333
	},
	{
		id: 2555,
		time: 2554,
		velocity: 0.1525,
		power: -665.516917627471,
		road: 19957.5640740741,
		acceleration: -0.971759259259259
	},
	{
		id: 2556,
		time: 2555,
		velocity: 0,
		power: -31.5902687419626,
		road: 19957.7626388889,
		acceleration: -0.295462962962963
	},
	{
		id: 2557,
		time: 2556,
		velocity: 0,
		power: 1.846775,
		road: 19957.7880555555,
		acceleration: -0.0508333333333333
	},
	{
		id: 2558,
		time: 2557,
		velocity: 0,
		power: 9.31434810596845,
		road: 19957.8331944444,
		acceleration: 0.0902777777777778
	},
	{
		id: 2559,
		time: 2558,
		velocity: 0.270833333333333,
		power: 10.9075704809428,
		road: 19957.9234722222,
		acceleration: 0
	},
	{
		id: 2560,
		time: 2559,
		velocity: 0,
		power: 14.5398309188402,
		road: 19958.0253240741,
		acceleration: 0.0231481481481482
	},
	{
		id: 2561,
		time: 2560,
		velocity: 0.0694444444444444,
		power: 5.54460194473615,
		road: 19958.1096759259,
		acceleration: -0.0581481481481482
	},
	{
		id: 2562,
		time: 2561,
		velocity: 0.0963888888888889,
		power: 7.75227487443608,
		road: 19958.169537037,
		acceleration: 0.00916666666666666
	},
	{
		id: 2563,
		time: 2562,
		velocity: 0.0275,
		power: 5.22829784292128,
		road: 19958.2224074074,
		acceleration: -0.0231481481481481
	},
	{
		id: 2564,
		time: 2563,
		velocity: 0,
		power: 2.28040474095791,
		road: 19958.2476388889,
		acceleration: -0.0321296296296296
	},
	{
		id: 2565,
		time: 2564,
		velocity: 0,
		power: 0.513946052631579,
		road: 19958.2522222222,
		acceleration: -0.00916666666666667
	},
	{
		id: 2566,
		time: 2565,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2567,
		time: 2566,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2568,
		time: 2567,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2569,
		time: 2568,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2570,
		time: 2569,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2571,
		time: 2570,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2572,
		time: 2571,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2573,
		time: 2572,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2574,
		time: 2573,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2575,
		time: 2574,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2576,
		time: 2575,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2577,
		time: 2576,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2578,
		time: 2577,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2579,
		time: 2578,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2580,
		time: 2579,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2581,
		time: 2580,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2582,
		time: 2581,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2583,
		time: 2582,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2584,
		time: 2583,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2585,
		time: 2584,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2586,
		time: 2585,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2587,
		time: 2586,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2588,
		time: 2587,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2589,
		time: 2588,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2590,
		time: 2589,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2591,
		time: 2590,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2592,
		time: 2591,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2593,
		time: 2592,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2594,
		time: 2593,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2595,
		time: 2594,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2596,
		time: 2595,
		velocity: 0,
		power: 0,
		road: 19958.2522222222,
		acceleration: 0
	},
	{
		id: 2597,
		time: 2596,
		velocity: 0,
		power: 25.8177070439467,
		road: 19958.3413425926,
		acceleration: 0.178240740740741
	},
	{
		id: 2598,
		time: 2597,
		velocity: 0.534722222222222,
		power: 1042.99140521908,
		road: 19959.1424074074,
		acceleration: 1.24564814814815
	},
	{
		id: 2599,
		time: 2598,
		velocity: 3.73694444444444,
		power: 3842.0687071009,
		road: 19961.3985648148,
		acceleration: 1.66453703703704
	},
	{
		id: 2600,
		time: 2599,
		velocity: 4.99361111111111,
		power: 5821.13136850328,
		road: 19965.2210185185,
		acceleration: 1.46805555555556
	},
	{
		id: 2601,
		time: 2600,
		velocity: 4.93888888888889,
		power: 2379.89598439632,
		road: 19969.9711574074,
		acceleration: 0.387314814814815
	},
	{
		id: 2602,
		time: 2601,
		velocity: 4.89888888888889,
		power: 1571.32880057217,
		road: 19975.008287037,
		acceleration: 0.186666666666667
	},
	{
		id: 2603,
		time: 2602,
		velocity: 5.55361111111111,
		power: 2195.12418180605,
		road: 19980.2860648148,
		acceleration: 0.29462962962963
	},
	{
		id: 2604,
		time: 2603,
		velocity: 5.82277777777778,
		power: 2839.94561425591,
		road: 19985.9044907407,
		acceleration: 0.386666666666667
	},
	{
		id: 2605,
		time: 2604,
		velocity: 6.05888888888889,
		power: 974.392736103058,
		road: 19991.7309722222,
		acceleration: 0.0294444444444455
	},
	{
		id: 2606,
		time: 2605,
		velocity: 5.64194444444444,
		power: 71.4050052242858,
		road: 19997.5056018518,
		acceleration: -0.133148148148148
	},
	{
		id: 2607,
		time: 2606,
		velocity: 5.42333333333333,
		power: -379.726566681139,
		road: 20003.1054629629,
		acceleration: -0.216388888888889
	},
	{
		id: 2608,
		time: 2607,
		velocity: 5.40972222222222,
		power: 35.0890482039891,
		road: 20008.5285648148,
		acceleration: -0.13712962962963
	},
	{
		id: 2609,
		time: 2608,
		velocity: 5.23055555555556,
		power: 58.9939266392104,
		road: 20013.8174074074,
		acceleration: -0.131388888888889
	},
	{
		id: 2610,
		time: 2609,
		velocity: 5.02916666666667,
		power: -960.898394670473,
		road: 20018.8695833333,
		acceleration: -0.341944444444445
	},
	{
		id: 2611,
		time: 2610,
		velocity: 4.38388888888889,
		power: -2273.73348916867,
		road: 20023.4181018518,
		acceleration: -0.66537037037037
	},
	{
		id: 2612,
		time: 2611,
		velocity: 3.23444444444444,
		power: -2919.51848725565,
		road: 20027.1547685185,
		acceleration: -0.958333333333333
	},
	{
		id: 2613,
		time: 2612,
		velocity: 2.15416666666667,
		power: -2315.33561349511,
		road: 20029.9023148148,
		acceleration: -1.01990740740741
	},
	{
		id: 2614,
		time: 2613,
		velocity: 1.32416666666667,
		power: -1167.30546233335,
		road: 20031.7403703703,
		acceleration: -0.799074074074074
	},
	{
		id: 2615,
		time: 2614,
		velocity: 0.837222222222222,
		power: -413.787539538716,
		road: 20032.9315277778,
		acceleration: -0.494722222222222
	},
	{
		id: 2616,
		time: 2615,
		velocity: 0.67,
		power: -214.908019654413,
		road: 20033.6546296296,
		acceleration: -0.441388888888889
	},
	{
		id: 2617,
		time: 2616,
		velocity: 0,
		power: -52.086773293228,
		road: 20034.0175,
		acceleration: -0.279074074074074
	},
	{
		id: 2618,
		time: 2617,
		velocity: 0,
		power: -7.03317935545829,
		road: 20034.1487962963,
		acceleration: -0.184074074074074
	},
	{
		id: 2619,
		time: 2618,
		velocity: 0.117777777777778,
		power: 6.25192989042652,
		road: 20034.1952777777,
		acceleration: 0.0144444444444444
	},
	{
		id: 2620,
		time: 2619,
		velocity: 0.0433333333333333,
		power: 6.48845250420349,
		road: 20034.2489814814,
		acceleration: 0
	},
	{
		id: 2621,
		time: 2620,
		velocity: 0,
		power: 2.84944531491031,
		road: 20034.2830555555,
		acceleration: -0.0392592592592593
	},
	{
		id: 2622,
		time: 2621,
		velocity: 0,
		power: 0.773743274853801,
		road: 20034.2902777777,
		acceleration: -0.0144444444444444
	},
	{
		id: 2623,
		time: 2622,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2624,
		time: 2623,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2625,
		time: 2624,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2626,
		time: 2625,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2627,
		time: 2626,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2628,
		time: 2627,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2629,
		time: 2628,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2630,
		time: 2629,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2631,
		time: 2630,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2632,
		time: 2631,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2633,
		time: 2632,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2634,
		time: 2633,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2635,
		time: 2634,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2636,
		time: 2635,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2637,
		time: 2636,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2638,
		time: 2637,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2639,
		time: 2638,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2640,
		time: 2639,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2641,
		time: 2640,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2642,
		time: 2641,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2643,
		time: 2642,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2644,
		time: 2643,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2645,
		time: 2644,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2646,
		time: 2645,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2647,
		time: 2646,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2648,
		time: 2647,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2649,
		time: 2648,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2650,
		time: 2649,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2651,
		time: 2650,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2652,
		time: 2651,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2653,
		time: 2652,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2654,
		time: 2653,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2655,
		time: 2654,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2656,
		time: 2655,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2657,
		time: 2656,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2658,
		time: 2657,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2659,
		time: 2658,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2660,
		time: 2659,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2661,
		time: 2660,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2662,
		time: 2661,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2663,
		time: 2662,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2664,
		time: 2663,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2665,
		time: 2664,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2666,
		time: 2665,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2667,
		time: 2666,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2668,
		time: 2667,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2669,
		time: 2668,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2670,
		time: 2669,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2671,
		time: 2670,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2672,
		time: 2671,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2673,
		time: 2672,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2674,
		time: 2673,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2675,
		time: 2674,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2676,
		time: 2675,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2677,
		time: 2676,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2678,
		time: 2677,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2679,
		time: 2678,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2680,
		time: 2679,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2681,
		time: 2680,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2682,
		time: 2681,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2683,
		time: 2682,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2684,
		time: 2683,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2685,
		time: 2684,
		velocity: 0,
		power: 0,
		road: 20034.2902777777,
		acceleration: 0
	},
	{
		id: 2686,
		time: 2685,
		velocity: 0,
		power: 0.580772727521557,
		road: 20034.2947685185,
		acceleration: 0.00898148148148148
	},
	{
		id: 2687,
		time: 2686,
		velocity: 0.0269444444444444,
		power: 1.08512407752525,
		road: 20034.30375,
		acceleration: 0
	},
	{
		id: 2688,
		time: 2687,
		velocity: 0,
		power: 1.08512407752525,
		road: 20034.3127314814,
		acceleration: 0
	},
	{
		id: 2689,
		time: 2688,
		velocity: 0,
		power: 0.504351153346329,
		road: 20034.3172222222,
		acceleration: -0.00898148148148148
	},
	{
		id: 2690,
		time: 2689,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2691,
		time: 2690,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2692,
		time: 2691,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2693,
		time: 2692,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2694,
		time: 2693,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2695,
		time: 2694,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2696,
		time: 2695,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2697,
		time: 2696,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2698,
		time: 2697,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2699,
		time: 2698,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2700,
		time: 2699,
		velocity: 0,
		power: 0,
		road: 20034.3172222222,
		acceleration: 0
	},
	{
		id: 2701,
		time: 2700,
		velocity: 0,
		power: 68.791003233208,
		road: 20034.4785185185,
		acceleration: 0.322592592592593
	},
	{
		id: 2702,
		time: 2701,
		velocity: 0.967777777777778,
		power: 944.763427427743,
		road: 20035.3255092592,
		acceleration: 1.0487962962963
	},
	{
		id: 2703,
		time: 2702,
		velocity: 3.14638888888889,
		power: 3932.16552052165,
		road: 20037.5593981481,
		acceleration: 1.725
	},
	{
		id: 2704,
		time: 2703,
		velocity: 5.175,
		power: 5440.04912121,
		road: 20041.3447685185,
		acceleration: 1.37796296296296
	},
	{
		id: 2705,
		time: 2704,
		velocity: 5.10166666666667,
		power: 3196.37660715828,
		road: 20046.1026388889,
		acceleration: 0.567037037037037
	},
	{
		id: 2706,
		time: 2705,
		velocity: 4.8475,
		power: 832.013927227037,
		road: 20051.1597222222,
		acceleration: 0.0313888888888885
	},
	{
		id: 2707,
		time: 2706,
		velocity: 5.26916666666667,
		power: 2339.78834351459,
		road: 20056.3962037037,
		acceleration: 0.327407407407408
	},
	{
		id: 2708,
		time: 2707,
		velocity: 6.08388888888889,
		power: 3820.48591727845,
		road: 20062.0773611111,
		acceleration: 0.561944444444443
	},
	{
		id: 2709,
		time: 2708,
		velocity: 6.53333333333333,
		power: 4751.42414553267,
		road: 20068.3622222222,
		acceleration: 0.645462962962963
	},
	{
		id: 2710,
		time: 2709,
		velocity: 7.20555555555556,
		power: 3530.46999557829,
		road: 20075.1658796296,
		acceleration: 0.39212962962963
	},
	{
		id: 2711,
		time: 2710,
		velocity: 7.26027777777778,
		power: 3573.29099755952,
		road: 20082.3488425926,
		acceleration: 0.366481481481482
	},
	{
		id: 2712,
		time: 2711,
		velocity: 7.63277777777778,
		power: 2440.61402838562,
		road: 20089.8076388889,
		acceleration: 0.185185185185184
	},
	{
		id: 2713,
		time: 2712,
		velocity: 7.76111111111111,
		power: 3761.49886216841,
		road: 20097.5343055555,
		acceleration: 0.350555555555557
	},
	{
		id: 2714,
		time: 2713,
		velocity: 8.31194444444444,
		power: 2823.89173013578,
		road: 20105.5398148148,
		acceleration: 0.207129629629629
	},
	{
		id: 2715,
		time: 2714,
		velocity: 8.25416666666667,
		power: 5726.62885140657,
		road: 20113.9241203703,
		acceleration: 0.55046296296296
	},
	{
		id: 2716,
		time: 2715,
		velocity: 9.4125,
		power: 11101.5334181289,
		road: 20123.1291666666,
		acceleration: 1.09101851851852
	},
	{
		id: 2717,
		time: 2716,
		velocity: 11.585,
		power: 14686.8973716171,
		road: 20133.5265277777,
		acceleration: 1.29361111111111
	},
	{
		id: 2718,
		time: 2717,
		velocity: 12.135,
		power: 11644.0960274711,
		road: 20145.0018518518,
		acceleration: 0.862314814814816
	},
	{
		id: 2719,
		time: 2718,
		velocity: 11.9994444444444,
		power: 7432.63730292196,
		road: 20157.124537037,
		acceleration: 0.432407407407405
	},
	{
		id: 2720,
		time: 2719,
		velocity: 12.8822222222222,
		power: 10609.4614690468,
		road: 20169.7932407407,
		acceleration: 0.659629629629629
	},
	{
		id: 2721,
		time: 2720,
		velocity: 14.1138888888889,
		power: 14830.3618664126,
		road: 20183.2539351852,
		acceleration: 0.924351851851853
	},
	{
		id: 2722,
		time: 2721,
		velocity: 14.7725,
		power: 14574.4714546356,
		road: 20197.5874074074,
		acceleration: 0.821203703703702
	},
	{
		id: 2723,
		time: 2722,
		velocity: 15.3458333333333,
		power: 12897.3494076567,
		road: 20212.6517592592,
		acceleration: 0.640555555555556
	},
	{
		id: 2724,
		time: 2723,
		velocity: 16.0355555555556,
		power: 8224.03982077518,
		road: 20228.1817129629,
		acceleration: 0.290648148148149
	},
	{
		id: 2725,
		time: 2724,
		velocity: 15.6444444444444,
		power: 3363.14484019116,
		road: 20243.836574074,
		acceleration: -0.0408333333333353
	},
	{
		id: 2726,
		time: 2725,
		velocity: 15.2233333333333,
		power: 398.37249401334,
		road: 20259.3528703703,
		acceleration: -0.236296296296295
	},
	{
		id: 2727,
		time: 2726,
		velocity: 15.3266666666667,
		power: 3840.9770465725,
		road: 20274.7509722222,
		acceleration: -9.25925925940874E-05
	},
	{
		id: 2728,
		time: 2727,
		velocity: 15.6441666666667,
		power: 5841.41931999996,
		road: 20290.2155092592,
		acceleration: 0.132962962962967
	},
	{
		id: 2729,
		time: 2728,
		velocity: 15.6222222222222,
		power: 5336.56405978869,
		road: 20305.7936111111,
		acceleration: 0.0941666666666627
	},
	{
		id: 2730,
		time: 2729,
		velocity: 15.6091666666667,
		power: 1316.12901192416,
		road: 20321.3313425926,
		acceleration: -0.174907407407407
	},
	{
		id: 2731,
		time: 2730,
		velocity: 15.1194444444444,
		power: 226.242732758987,
		road: 20336.6593981481,
		acceleration: -0.244444444444444
	},
	{
		id: 2732,
		time: 2731,
		velocity: 14.8888888888889,
		power: -829.036677903492,
		road: 20351.7088425926,
		acceleration: -0.312777777777779
	},
	{
		id: 2733,
		time: 2732,
		velocity: 14.6708333333333,
		power: 1439.68135962006,
		road: 20366.5271296296,
		acceleration: -0.149537037037035
	},
	{
		id: 2734,
		time: 2733,
		velocity: 14.6708333333333,
		power: 2536.71304712485,
		road: 20381.2362037037,
		acceleration: -0.068888888888889
	},
	{
		id: 2735,
		time: 2734,
		velocity: 14.6822222222222,
		power: 3615.23481792519,
		road: 20395.9152777777,
		acceleration: 0.0088888888888885
	},
	{
		id: 2736,
		time: 2735,
		velocity: 14.6975,
		power: 6686.57762711258,
		road: 20410.7099074074,
		acceleration: 0.222222222222221
	},
	{
		id: 2737,
		time: 2736,
		velocity: 15.3375,
		power: 14942.6122619423,
		road: 20425.9974074074,
		acceleration: 0.763518518518518
	},
	{
		id: 2738,
		time: 2737,
		velocity: 16.9727777777778,
		power: 24066.6405208891,
		road: 20442.2998611111,
		acceleration: 1.26638888888889
	},
	{
		id: 2739,
		time: 2738,
		velocity: 18.4966666666667,
		power: 22372.5788586381,
		road: 20459.7556018518,
		acceleration: 1.04018518518518
	},
	{
		id: 2740,
		time: 2739,
		velocity: 18.4580555555556,
		power: 10097.0900140981,
		road: 20477.8665277777,
		acceleration: 0.270185185185184
	},
	{
		id: 2741,
		time: 2740,
		velocity: 17.7833333333333,
		power: -4440.97705478281,
		road: 20495.82875,
		acceleration: -0.56759259259259
	},
	{
		id: 2742,
		time: 2741,
		velocity: 16.7938888888889,
		power: -6785.69091704416,
		road: 20513.1541666666,
		acceleration: -0.706018518518519
	},
	{
		id: 2743,
		time: 2742,
		velocity: 16.34,
		power: -6602.09863033397,
		road: 20529.7773148148,
		acceleration: -0.698518518518519
	},
	{
		id: 2744,
		time: 2743,
		velocity: 15.6877777777778,
		power: -9376.62223826239,
		road: 20545.6069907407,
		acceleration: -0.888425925925926
	},
	{
		id: 2745,
		time: 2744,
		velocity: 14.1286111111111,
		power: -6680.57442157287,
		road: 20560.6324074074,
		acceleration: -0.720092592592593
	},
	{
		id: 2746,
		time: 2745,
		velocity: 14.1797222222222,
		power: -6410.03702326826,
		road: 20574.9418055555,
		acceleration: -0.711944444444443
	},
	{
		id: 2747,
		time: 2746,
		velocity: 13.5519444444444,
		power: -1145.3383722122,
		road: 20588.7343981481,
		acceleration: -0.321666666666665
	},
	{
		id: 2748,
		time: 2747,
		velocity: 13.1636111111111,
		power: -4324.5348879679,
		road: 20602.0825,
		acceleration: -0.567314814814814
	},
	{
		id: 2749,
		time: 2748,
		velocity: 12.4777777777778,
		power: -2536.74698305677,
		road: 20614.9332407407,
		acceleration: -0.427407407407408
	},
	{
		id: 2750,
		time: 2749,
		velocity: 12.2697222222222,
		power: -1647.04263436902,
		road: 20627.3935185185,
		acceleration: -0.353518518518518
	},
	{
		id: 2751,
		time: 2750,
		velocity: 12.1030555555556,
		power: 144.438236660504,
		road: 20639.5776851851,
		acceleration: -0.198703703703705
	},
	{
		id: 2752,
		time: 2751,
		velocity: 11.8816666666667,
		power: -84.9312503763107,
		road: 20651.5546296296,
		acceleration: -0.215740740740742
	},
	{
		id: 2753,
		time: 2752,
		velocity: 11.6225,
		power: -107.442323587903,
		road: 20663.3162037037,
		acceleration: -0.214999999999998
	},
	{
		id: 2754,
		time: 2753,
		velocity: 11.4580555555556,
		power: 239.323045907383,
		road: 20674.8797222222,
		acceleration: -0.181111111111113
	},
	{
		id: 2755,
		time: 2754,
		velocity: 11.3383333333333,
		power: 578.087648581267,
		road: 20686.2789351851,
		acceleration: -0.147500000000001
	},
	{
		id: 2756,
		time: 2755,
		velocity: 11.18,
		power: 560.352160499283,
		road: 20697.5311111111,
		acceleration: -0.146574074074074
	},
	{
		id: 2757,
		time: 2756,
		velocity: 11.0183333333333,
		power: 270.721279717555,
		road: 20708.6243981481,
		acceleration: -0.1712037037037
	},
	{
		id: 2758,
		time: 2757,
		velocity: 10.8247222222222,
		power: -474.795236833507,
		road: 20719.5120833333,
		acceleration: -0.240000000000002
	},
	{
		id: 2759,
		time: 2758,
		velocity: 10.46,
		power: -1755.78816539906,
		road: 20730.0974537037,
		acceleration: -0.364629629629629
	},
	{
		id: 2760,
		time: 2759,
		velocity: 9.92444444444445,
		power: -2302.28097790946,
		road: 20740.2889814814,
		acceleration: -0.423055555555557
	},
	{
		id: 2761,
		time: 2760,
		velocity: 9.55555555555556,
		power: -2047.5316631104,
		road: 20750.0684259259,
		acceleration: -0.40111111111111
	},
	{
		id: 2762,
		time: 2761,
		velocity: 9.25666666666667,
		power: -1728.68203146618,
		road: 20759.4621296296,
		acceleration: -0.37037037037037
	},
	{
		id: 2763,
		time: 2762,
		velocity: 8.81333333333333,
		power: -1035.60051203513,
		road: 20768.5237962963,
		acceleration: -0.293703703703702
	},
	{
		id: 2764,
		time: 2763,
		velocity: 8.67444444444445,
		power: -1378.16835169649,
		road: 20777.2706018518,
		acceleration: -0.33601851851852
	},
	{
		id: 2765,
		time: 2764,
		velocity: 8.24861111111111,
		power: -872.253488796226,
		road: 20785.7113425926,
		acceleration: -0.276111111111112
	},
	{
		id: 2766,
		time: 2765,
		velocity: 7.985,
		power: -1273.69539828987,
		road: 20793.8494444444,
		acceleration: -0.329166666666668
	},
	{
		id: 2767,
		time: 2766,
		velocity: 7.68694444444444,
		power: -589.418329199935,
		road: 20801.7024537037,
		acceleration: -0.241018518518518
	},
	{
		id: 2768,
		time: 2767,
		velocity: 7.52555555555556,
		power: -726.98444015441,
		road: 20809.3047222222,
		acceleration: -0.260462962962962
	},
	{
		id: 2769,
		time: 2768,
		velocity: 7.20361111111111,
		power: -772.744099196913,
		road: 20816.6425462963,
		acceleration: -0.268425925925926
	},
	{
		id: 2770,
		time: 2769,
		velocity: 6.88166666666667,
		power: -836.487021715106,
		road: 20823.7061574074,
		acceleration: -0.279999999999999
	},
	{
		id: 2771,
		time: 2770,
		velocity: 6.68555555555556,
		power: -2297.77529626483,
		road: 20830.3723148148,
		acceleration: -0.514907407407407
	},
	{
		id: 2772,
		time: 2771,
		velocity: 5.65888888888889,
		power: -3153.14523418952,
		road: 20836.4333333333,
		acceleration: -0.69537037037037
	},
	{
		id: 2773,
		time: 2772,
		velocity: 4.79555555555556,
		power: -3570.38642045918,
		road: 20841.7196759259,
		acceleration: -0.853981481481482
	},
	{
		id: 2774,
		time: 2773,
		velocity: 4.12361111111111,
		power: -1817.53163250169,
		road: 20846.3005555555,
		acceleration: -0.556944444444444
	},
	{
		id: 2775,
		time: 2774,
		velocity: 3.98805555555556,
		power: -820.005371062286,
		road: 20850.4299074074,
		acceleration: -0.346111111111111
	},
	{
		id: 2776,
		time: 2775,
		velocity: 3.75722222222222,
		power: -1004.00516737168,
		road: 20854.1774537037,
		acceleration: -0.4175
	},
	{
		id: 2777,
		time: 2776,
		velocity: 2.87111111111111,
		power: -1494.0911885235,
		road: 20857.405787037,
		acceleration: -0.620925925925926
	},
	{
		id: 2778,
		time: 2777,
		velocity: 2.12527777777778,
		power: -1348.50661237339,
		road: 20859.9822222222,
		acceleration: -0.68287037037037
	},
	{
		id: 2779,
		time: 2778,
		velocity: 1.70861111111111,
		power: -538.400810365646,
		road: 20862.0125462963,
		acceleration: -0.409351851851852
	},
	{
		id: 2780,
		time: 2779,
		velocity: 1.64305555555556,
		power: -62.6021869124777,
		road: 20863.7546759259,
		acceleration: -0.167037037037037
	},
	{
		id: 2781,
		time: 2780,
		velocity: 1.62416666666667,
		power: 130.67657747622,
		road: 20865.3909259259,
		acceleration: -0.0447222222222221
	},
	{
		id: 2782,
		time: 2781,
		velocity: 1.57444444444444,
		power: 115.095128733261,
		road: 20866.9786111111,
		acceleration: -0.0524074074074072
	},
	{
		id: 2783,
		time: 2782,
		velocity: 1.48583333333333,
		power: 462.358002331713,
		road: 20868.6237962963,
		acceleration: 0.167407407407407
	},
	{
		id: 2784,
		time: 2783,
		velocity: 2.12638888888889,
		power: 840.225245578522,
		road: 20870.5213888889,
		acceleration: 0.337407407407408
	},
	{
		id: 2785,
		time: 2784,
		velocity: 2.58666666666667,
		power: 1124.53415074258,
		road: 20872.7844444444,
		acceleration: 0.393518518518518
	},
	{
		id: 2786,
		time: 2785,
		velocity: 2.66638888888889,
		power: 1524.60799231785,
		road: 20875.4768981481,
		acceleration: 0.465277777777778
	},
	{
		id: 2787,
		time: 2786,
		velocity: 3.52222222222222,
		power: 2089.69032077397,
		road: 20878.6791666666,
		acceleration: 0.554351851851852
	},
	{
		id: 2788,
		time: 2787,
		velocity: 4.24972222222222,
		power: 2963.81737644312,
		road: 20882.4993518518,
		acceleration: 0.681481481481482
	},
	{
		id: 2789,
		time: 2788,
		velocity: 4.71083333333333,
		power: 2648.06630308569,
		road: 20886.9073611111,
		acceleration: 0.494166666666667
	},
	{
		id: 2790,
		time: 2789,
		velocity: 5.00472222222222,
		power: 1692.3523357886,
		road: 20891.6790277777,
		acceleration: 0.233148148148148
	},
	{
		id: 2791,
		time: 2790,
		velocity: 4.94916666666667,
		power: 731.104117805987,
		road: 20896.5754166666,
		acceleration: 0.0162962962962965
	},
	{
		id: 2792,
		time: 2791,
		velocity: 4.75972222222222,
		power: 0.333213597949583,
		road: 20901.4097222222,
		acceleration: -0.140462962962963
	},
	{
		id: 2793,
		time: 2792,
		velocity: 4.58333333333333,
		power: -186.816110097345,
		road: 20906.0829166666,
		acceleration: -0.181759259259259
	},
	{
		id: 2794,
		time: 2793,
		velocity: 4.40388888888889,
		power: -340.897805316393,
		road: 20910.555787037,
		acceleration: -0.218888888888888
	},
	{
		id: 2795,
		time: 2794,
		velocity: 4.10305555555556,
		power: -348.928044559392,
		road: 20914.8072222222,
		acceleration: -0.223981481481482
	},
	{
		id: 2796,
		time: 2795,
		velocity: 3.91138888888889,
		power: -466.591965357988,
		road: 20918.8171759259,
		acceleration: -0.258981481481481
	},
	{
		id: 2797,
		time: 2796,
		velocity: 3.62694444444444,
		power: -254.183902199822,
		road: 20922.5944907407,
		acceleration: -0.206296296296296
	},
	{
		id: 2798,
		time: 2797,
		velocity: 3.48416666666667,
		power: -332.175374043636,
		road: 20926.1522222222,
		acceleration: -0.232870370370371
	},
	{
		id: 2799,
		time: 2798,
		velocity: 3.21277777777778,
		power: -294.466919698867,
		road: 20929.4800925926,
		acceleration: -0.226851851851852
	},
	{
		id: 2800,
		time: 2799,
		velocity: 2.94638888888889,
		power: -282.137171232251,
		road: 20932.5801851851,
		acceleration: -0.228703703703704
	},
	{
		id: 2801,
		time: 2800,
		velocity: 2.79805555555556,
		power: -119.174162293165,
		road: 20935.4781944444,
		acceleration: -0.175462962962963
	},
	{
		id: 2802,
		time: 2801,
		velocity: 2.68638888888889,
		power: -241.889443885416,
		road: 20938.1754629629,
		acceleration: -0.226018518518518
	},
	{
		id: 2803,
		time: 2802,
		velocity: 2.26833333333333,
		power: -356.843118471599,
		road: 20940.6173148148,
		acceleration: -0.284814814814815
	},
	{
		id: 2804,
		time: 2803,
		velocity: 1.94361111111111,
		power: -330.169022149433,
		road: 20942.7709259259,
		acceleration: -0.291666666666667
	},
	{
		id: 2805,
		time: 2804,
		velocity: 1.81138888888889,
		power: -307.297271463667,
		road: 20944.6267129629,
		acceleration: -0.303981481481481
	},
	{
		id: 2806,
		time: 2805,
		velocity: 1.35638888888889,
		power: -228.733701860839,
		road: 20946.1888888889,
		acceleration: -0.283240740740741
	},
	{
		id: 2807,
		time: 2806,
		velocity: 1.09388888888889,
		power: -504.332711748711,
		road: 20947.3075462963,
		acceleration: -0.603796296296296
	},
	{
		id: 2808,
		time: 2807,
		velocity: 0,
		power: -181.605026474475,
		road: 20947.8982407407,
		acceleration: -0.45212962962963
	},
	{
		id: 2809,
		time: 2808,
		velocity: 0,
		power: -40.9516816439246,
		road: 20948.0805555555,
		acceleration: -0.36462962962963
	},
	{
		id: 2810,
		time: 2809,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2811,
		time: 2810,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2812,
		time: 2811,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2813,
		time: 2812,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2814,
		time: 2813,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2815,
		time: 2814,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2816,
		time: 2815,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2817,
		time: 2816,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2818,
		time: 2817,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2819,
		time: 2818,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2820,
		time: 2819,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2821,
		time: 2820,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2822,
		time: 2821,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2823,
		time: 2822,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2824,
		time: 2823,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2825,
		time: 2824,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2826,
		time: 2825,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2827,
		time: 2826,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2828,
		time: 2827,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2829,
		time: 2828,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2830,
		time: 2829,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2831,
		time: 2830,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2832,
		time: 2831,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2833,
		time: 2832,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2834,
		time: 2833,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2835,
		time: 2834,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2836,
		time: 2835,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2837,
		time: 2836,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2838,
		time: 2837,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2839,
		time: 2838,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2840,
		time: 2839,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2841,
		time: 2840,
		velocity: 0,
		power: 0,
		road: 20948.0805555555,
		acceleration: 0
	},
	{
		id: 2842,
		time: 2841,
		velocity: 0,
		power: 1.53872981069046,
		road: 20948.0914351851,
		acceleration: 0.0217592592592593
	},
	{
		id: 2843,
		time: 2842,
		velocity: 0.0652777777777778,
		power: 2.62891348752776,
		road: 20948.1131944444,
		acceleration: 0
	},
	{
		id: 2844,
		time: 2843,
		velocity: 0,
		power: 2.62891348752776,
		road: 20948.1349537037,
		acceleration: 0
	},
	{
		id: 2845,
		time: 2844,
		velocity: 0,
		power: 1.09018088044185,
		road: 20948.1458333333,
		acceleration: -0.0217592592592593
	},
	{
		id: 2846,
		time: 2845,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2847,
		time: 2846,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2848,
		time: 2847,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2849,
		time: 2848,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2850,
		time: 2849,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2851,
		time: 2850,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2852,
		time: 2851,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2853,
		time: 2852,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2854,
		time: 2853,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2855,
		time: 2854,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2856,
		time: 2855,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2857,
		time: 2856,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2858,
		time: 2857,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2859,
		time: 2858,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2860,
		time: 2859,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2861,
		time: 2860,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2862,
		time: 2861,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2863,
		time: 2862,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2864,
		time: 2863,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2865,
		time: 2864,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2866,
		time: 2865,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2867,
		time: 2866,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2868,
		time: 2867,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2869,
		time: 2868,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2870,
		time: 2869,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2871,
		time: 2870,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2872,
		time: 2871,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2873,
		time: 2872,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2874,
		time: 2873,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2875,
		time: 2874,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2876,
		time: 2875,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2877,
		time: 2876,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2878,
		time: 2877,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2879,
		time: 2878,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2880,
		time: 2879,
		velocity: 0,
		power: 0,
		road: 20948.1458333333,
		acceleration: 0
	},
	{
		id: 2881,
		time: 2880,
		velocity: 0,
		power: 128.920827169339,
		road: 20948.3767129629,
		acceleration: 0.461759259259259
	},
	{
		id: 2882,
		time: 2881,
		velocity: 1.38527777777778,
		power: 1172.94459396708,
		road: 20949.3868518518,
		acceleration: 1.09675925925926
	},
	{
		id: 2883,
		time: 2882,
		velocity: 3.29027777777778,
		power: 3616.06577923386,
		road: 20951.7029166666,
		acceleration: 1.51509259259259
	},
	{
		id: 2884,
		time: 2883,
		velocity: 4.54527777777778,
		power: 5327.4104446928,
		road: 20955.45625,
		acceleration: 1.35944444444444
	},
	{
		id: 2885,
		time: 2884,
		velocity: 5.46361111111111,
		power: 4582.9545043184,
		road: 20960.315324074,
		acceleration: 0.852037037037036
	},
	{
		id: 2886,
		time: 2885,
		velocity: 5.84638888888889,
		power: 3515.01419629524,
		road: 20965.8614814814,
		acceleration: 0.522129629629631
	},
	{
		id: 2887,
		time: 2886,
		velocity: 6.11166666666667,
		power: 3703.10495608174,
		road: 20971.9163425926,
		acceleration: 0.495277777777777
	},
	{
		id: 2888,
		time: 2887,
		velocity: 6.94944444444444,
		power: 6564.26638716641,
		road: 20978.6544907407,
		acceleration: 0.871296296296296
	},
	{
		id: 2889,
		time: 2888,
		velocity: 8.46027777777778,
		power: 8032.70746275251,
		road: 20986.3001388889,
		acceleration: 0.943703703703704
	},
	{
		id: 2890,
		time: 2889,
		velocity: 8.94277777777778,
		power: 1385.19070236582,
		road: 20994.4249074074,
		acceleration: 0.0145370370370372
	},
	{
		id: 2891,
		time: 2890,
		velocity: 6.99305555555556,
		power: -7712.88678118056,
		road: 21001.9377314814,
		acceleration: -1.23842592592593
	},
	{
		id: 2892,
		time: 2891,
		velocity: 4.745,
		power: -11230.4257939769,
		road: 21007.7396296296,
		acceleration: -2.18342592592593
	},
	{
		id: 2893,
		time: 2892,
		velocity: 2.3925,
		power: -7388.67334650791,
		road: 21011.2843055555,
		acceleration: -2.33101851851852
	},
	{
		id: 2894,
		time: 2893,
		velocity: 0,
		power: -2187.54461345208,
		road: 21012.8726388889,
		acceleration: -1.58166666666667
	},
	{
		id: 2895,
		time: 2894,
		velocity: 0,
		power: -253.089982894737,
		road: 21013.2713888889,
		acceleration: -0.7975
	},
	{
		id: 2896,
		time: 2895,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2897,
		time: 2896,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2898,
		time: 2897,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2899,
		time: 2898,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2900,
		time: 2899,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2901,
		time: 2900,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2902,
		time: 2901,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2903,
		time: 2902,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2904,
		time: 2903,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2905,
		time: 2904,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2906,
		time: 2905,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2907,
		time: 2906,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2908,
		time: 2907,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2909,
		time: 2908,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2910,
		time: 2909,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2911,
		time: 2910,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2912,
		time: 2911,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2913,
		time: 2912,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2914,
		time: 2913,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2915,
		time: 2914,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2916,
		time: 2915,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2917,
		time: 2916,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2918,
		time: 2917,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2919,
		time: 2918,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2920,
		time: 2919,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2921,
		time: 2920,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2922,
		time: 2921,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2923,
		time: 2922,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2924,
		time: 2923,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2925,
		time: 2924,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2926,
		time: 2925,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2927,
		time: 2926,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2928,
		time: 2927,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2929,
		time: 2928,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2930,
		time: 2929,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2931,
		time: 2930,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2932,
		time: 2931,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2933,
		time: 2932,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2934,
		time: 2933,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2935,
		time: 2934,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2936,
		time: 2935,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2937,
		time: 2936,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2938,
		time: 2937,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2939,
		time: 2938,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2940,
		time: 2939,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2941,
		time: 2940,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2942,
		time: 2941,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2943,
		time: 2942,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2944,
		time: 2943,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2945,
		time: 2944,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2946,
		time: 2945,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2947,
		time: 2946,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2948,
		time: 2947,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2949,
		time: 2948,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2950,
		time: 2949,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2951,
		time: 2950,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2952,
		time: 2951,
		velocity: 0,
		power: 0,
		road: 21013.2713888889,
		acceleration: 0
	},
	{
		id: 2953,
		time: 2952,
		velocity: 0,
		power: 306.039057347132,
		road: 21013.6425925926,
		acceleration: 0.742407407407407
	},
	{
		id: 2954,
		time: 2953,
		velocity: 2.22722222222222,
		power: 2734.23968753446,
		road: 21015.2291203703,
		acceleration: 1.68824074074074
	},
	{
		id: 2955,
		time: 2954,
		velocity: 5.06472222222222,
		power: 6813.42067251221,
		road: 21018.6436111111,
		acceleration: 1.96768518518519
	},
	{
		id: 2956,
		time: 2955,
		velocity: 5.90305555555556,
		power: 6441.36619930371,
		road: 21023.6484259259,
		acceleration: 1.21296296296296
	},
	{
		id: 2957,
		time: 2956,
		velocity: 5.86611111111111,
		power: 2430.62740357339,
		road: 21029.4086574074,
		acceleration: 0.29787037037037
	},
	{
		id: 2958,
		time: 2957,
		velocity: 5.95833333333333,
		power: 3638.92367092438,
		road: 21035.5548611111,
		acceleration: 0.474074074074073
	},
	{
		id: 2959,
		time: 2958,
		velocity: 7.32527777777778,
		power: 4886.98841065362,
		road: 21042.245787037,
		acceleration: 0.61537037037037
	},
	{
		id: 2960,
		time: 2959,
		velocity: 7.71222222222222,
		power: 5490.65364264163,
		road: 21049.5601388889,
		acceleration: 0.631481481481482
	},
	{
		id: 2961,
		time: 2960,
		velocity: 7.85277777777778,
		power: 2028.46064062539,
		road: 21057.2485185185,
		acceleration: 0.116574074074075
	},
	{
		id: 2962,
		time: 2961,
		velocity: 7.675,
		power: 962.937653401169,
		road: 21064.9800925926,
		acceleration: -0.0301851851851858
	},
	{
		id: 2963,
		time: 2962,
		velocity: 7.62166666666667,
		power: -749.100925429845,
		road: 21072.5647685185,
		acceleration: -0.263611111111111
	},
	{
		id: 2964,
		time: 2963,
		velocity: 7.06194444444444,
		power: -1522.19657263902,
		road: 21079.8289351852,
		acceleration: -0.377407407407407
	},
	{
		id: 2965,
		time: 2964,
		velocity: 6.54277777777778,
		power: -4207.60487323714,
		road: 21086.4963425926,
		acceleration: -0.816111111111112
	},
	{
		id: 2966,
		time: 2965,
		velocity: 5.17333333333333,
		power: -3432.17888447266,
		road: 21092.3751388889,
		acceleration: -0.76111111111111
	},
	{
		id: 2967,
		time: 2966,
		velocity: 4.77861111111111,
		power: -2972.96184836891,
		road: 21097.4968055555,
		acceleration: -0.753148148148148
	},
	{
		id: 2968,
		time: 2967,
		velocity: 4.28333333333333,
		power: -932.191671935074,
		road: 21102.0649074074,
		acceleration: -0.353981481481481
	},
	{
		id: 2969,
		time: 2968,
		velocity: 4.11138888888889,
		power: -624.339984674661,
		road: 21106.3098148148,
		acceleration: -0.292407407407407
	},
	{
		id: 2970,
		time: 2969,
		velocity: 3.90138888888889,
		power: -459.430010399766,
		road: 21110.2794444444,
		acceleration: -0.258148148148149
	},
	{
		id: 2971,
		time: 2970,
		velocity: 3.50888888888889,
		power: -823.908151742338,
		road: 21113.9337962963,
		acceleration: -0.372407407407407
	},
	{
		id: 2972,
		time: 2971,
		velocity: 2.99416666666667,
		power: -1022.1342339614,
		road: 21117.1688425926,
		acceleration: -0.466203703703703
	},
	{
		id: 2973,
		time: 2972,
		velocity: 2.50277777777778,
		power: -1509.17791389057,
		road: 21119.8031944444,
		acceleration: -0.735185185185185
	},
	{
		id: 2974,
		time: 2973,
		velocity: 1.30333333333333,
		power: -1105.9852977637,
		road: 21121.6973611111,
		acceleration: -0.745185185185185
	},
	{
		id: 2975,
		time: 2974,
		velocity: 0.758611111111111,
		power: -240.515846544551,
		road: 21123.0617129629,
		acceleration: -0.314444444444445
	},
	{
		id: 2976,
		time: 2975,
		velocity: 1.55944444444444,
		power: 558.539706987267,
		road: 21124.42125,
		acceleration: 0.304814814814815
	},
	{
		id: 2977,
		time: 2976,
		velocity: 2.21777777777778,
		power: 1069.82948200225,
		road: 21126.1878703703,
		acceleration: 0.509351851851852
	},
	{
		id: 2978,
		time: 2977,
		velocity: 2.28666666666667,
		power: 920.1035279497,
		road: 21128.3667129629,
		acceleration: 0.315092592592593
	},
	{
		id: 2979,
		time: 2978,
		velocity: 2.50472222222222,
		power: 915.691382770249,
		road: 21130.8333333333,
		acceleration: 0.260462962962963
	},
	{
		id: 2980,
		time: 2979,
		velocity: 2.99916666666667,
		power: 1199.70223674242,
		road: 21133.5933796296,
		acceleration: 0.326388888888889
	},
	{
		id: 2981,
		time: 2980,
		velocity: 3.26583333333333,
		power: 1477.14207556544,
		road: 21136.7006481481,
		acceleration: 0.368055555555555
	},
	{
		id: 2982,
		time: 2981,
		velocity: 3.60888888888889,
		power: 817.475327949931,
		road: 21140.0535185185,
		acceleration: 0.123148148148148
	},
	{
		id: 2983,
		time: 2982,
		velocity: 3.36861111111111,
		power: 718.389543728959,
		road: 21143.5103703703,
		acceleration: 0.0848148148148153
	},
	{
		id: 2984,
		time: 2983,
		velocity: 3.52027777777778,
		power: 407.776528595726,
		road: 21147.0039814814,
		acceleration: -0.0112962962962966
	},
	{
		id: 2985,
		time: 2984,
		velocity: 3.575,
		power: 679.645034657543,
		road: 21150.5263888889,
		acceleration: 0.068888888888889
	},
	{
		id: 2986,
		time: 2985,
		velocity: 3.57527777777778,
		power: 598.62326368066,
		road: 21154.104074074,
		acceleration: 0.041666666666667
	},
	{
		id: 2987,
		time: 2986,
		velocity: 3.64527777777778,
		power: 529.021596332824,
		road: 21157.7124537037,
		acceleration: 0.0197222222222218
	},
	{
		id: 2988,
		time: 2987,
		velocity: 3.63416666666667,
		power: 354.250580467093,
		road: 21161.3151388889,
		acceleration: -0.0311111111111111
	},
	{
		id: 2989,
		time: 2988,
		velocity: 3.48194444444444,
		power: 603.466894902565,
		road: 21164.9230092592,
		acceleration: 0.0414814814814819
	},
	{
		id: 2990,
		time: 2989,
		velocity: 3.76972222222222,
		power: 960.521772358687,
		road: 21168.6208796296,
		acceleration: 0.138518518518518
	},
	{
		id: 2991,
		time: 2990,
		velocity: 4.04972222222222,
		power: 1065.37929107697,
		road: 21172.4660648148,
		acceleration: 0.156111111111112
	},
	{
		id: 2992,
		time: 2991,
		velocity: 3.95027777777778,
		power: 1127.0701136013,
		road: 21176.4693518518,
		acceleration: 0.160092592592592
	},
	{
		id: 2993,
		time: 2992,
		velocity: 4.25,
		power: 638.95888479893,
		road: 21180.5663888889,
		acceleration: 0.0274074074074075
	},
	{
		id: 2994,
		time: 2993,
		velocity: 4.13194444444444,
		power: 688.286690886239,
		road: 21184.6963888889,
		acceleration: 0.0385185185185186
	},
	{
		id: 2995,
		time: 2994,
		velocity: 4.06583333333333,
		power: 193.91969111423,
		road: 21188.8020833333,
		acceleration: -0.0871296296296293
	},
	{
		id: 2996,
		time: 2995,
		velocity: 3.98861111111111,
		power: 240.02356509701,
		road: 21192.8273611111,
		acceleration: -0.0737037037037038
	},
	{
		id: 2997,
		time: 2996,
		velocity: 3.91083333333333,
		power: 181.132373313017,
		road: 21196.7718981481,
		acceleration: -0.0877777777777782
	},
	{
		id: 2998,
		time: 2997,
		velocity: 3.8025,
		power: 380.564615538965,
		road: 21200.6562037037,
		acceleration: -0.0326851851851848
	},
	{
		id: 2999,
		time: 2998,
		velocity: 3.89055555555556,
		power: 288.04675067826,
		road: 21204.4958333333,
		acceleration: -0.0566666666666666
	},
	{
		id: 3000,
		time: 2999,
		velocity: 3.74083333333333,
		power: 353.470484902735,
		road: 21208.2884722222,
		acceleration: -0.037314814814815
	},
	{
		id: 3001,
		time: 3000,
		velocity: 3.69055555555556,
		power: 274.557192158842,
		road: 21212.0334259259,
		acceleration: -0.0580555555555562
	},
	{
		id: 3002,
		time: 3001,
		velocity: 3.71638888888889,
		power: 581.189224984156,
		road: 21215.7637962963,
		acceleration: 0.0288888888888894
	},
	{
		id: 3003,
		time: 3002,
		velocity: 3.8275,
		power: 499.770336529809,
		road: 21219.5112037037,
		acceleration: 0.0051851851851854
	},
	{
		id: 3004,
		time: 3003,
		velocity: 3.70611111111111,
		power: 447.098778796325,
		road: 21223.2564351852,
		acceleration: -0.0095370370370369
	},
	{
		id: 3005,
		time: 3004,
		velocity: 3.68777777777778,
		power: 730.127066542878,
		road: 21227.0310648148,
		acceleration: 0.0683333333333334
	},
	{
		id: 3006,
		time: 3005,
		velocity: 4.0325,
		power: 2556.00308892793,
		road: 21231.1020833333,
		acceleration: 0.524444444444444
	},
	{
		id: 3007,
		time: 3006,
		velocity: 5.27944444444444,
		power: 3153.35448222963,
		road: 21235.7246759259,
		acceleration: 0.578703703703703
	},
	{
		id: 3008,
		time: 3007,
		velocity: 5.42388888888889,
		power: 2821.81874375488,
		road: 21240.8549537037,
		acceleration: 0.436666666666667
	},
	{
		id: 3009,
		time: 3008,
		velocity: 5.3425,
		power: 633.287932187223,
		road: 21246.1942592592,
		acceleration: -0.0186111111111122
	},
	{
		id: 3010,
		time: 3009,
		velocity: 5.22361111111111,
		power: 1012.21990246087,
		road: 21251.5518981481,
		acceleration: 0.0552777777777784
	},
	{
		id: 3011,
		time: 3010,
		velocity: 5.58972222222222,
		power: -1150.00266134369,
		road: 21256.7494444444,
		acceleration: -0.375462962962963
	},
	{
		id: 3012,
		time: 3011,
		velocity: 4.21611111111111,
		power: 223.27811539785,
		road: 21261.7123148148,
		acceleration: -0.0938888888888885
	},
	{
		id: 3013,
		time: 3012,
		velocity: 4.94194444444444,
		power: -27.1434118974821,
		road: 21266.555,
		acceleration: -0.146481481481481
	},
	{
		id: 3014,
		time: 3013,
		velocity: 5.15027777777778,
		power: 2011.31972669427,
		road: 21271.4693518518,
		acceleration: 0.289814814814815
	},
	{
		id: 3015,
		time: 3014,
		velocity: 5.08555555555556,
		power: 267.294978287804,
		road: 21276.4858796296,
		acceleration: -0.0854629629629633
	},
	{
		id: 3016,
		time: 3015,
		velocity: 4.68555555555556,
		power: -599.189409269103,
		road: 21281.3242129629,
		acceleration: -0.270925925925926
	},
	{
		id: 3017,
		time: 3016,
		velocity: 4.3375,
		power: -1427.7255648938,
		road: 21285.7894444444,
		acceleration: -0.475277777777777
	},
	{
		id: 3018,
		time: 3017,
		velocity: 3.65972222222222,
		power: -1473.17866299706,
		road: 21289.7531944444,
		acceleration: -0.527685185185185
	},
	{
		id: 3019,
		time: 3018,
		velocity: 3.1025,
		power: -1197.89155295946,
		road: 21293.2031481481,
		acceleration: -0.499907407407407
	},
	{
		id: 3020,
		time: 3019,
		velocity: 2.83777777777778,
		power: -626.523197290017,
		road: 21296.2277314814,
		acceleration: -0.350833333333334
	},
	{
		id: 3021,
		time: 3020,
		velocity: 2.60722222222222,
		power: -521.5113378694,
		road: 21298.9086574074,
		acceleration: -0.336481481481481
	},
	{
		id: 3022,
		time: 3021,
		velocity: 2.09305555555556,
		power: -468.107288299769,
		road: 21301.2507407407,
		acceleration: -0.341203703703704
	},
	{
		id: 3023,
		time: 3022,
		velocity: 1.81416666666667,
		power: -250.762037936326,
		road: 21303.2925925926,
		acceleration: -0.259259259259259
	},
	{
		id: 3024,
		time: 3023,
		velocity: 1.82944444444444,
		power: 935.338780490955,
		road: 21305.3764814814,
		acceleration: 0.343333333333334
	},
	{
		id: 3025,
		time: 3024,
		velocity: 3.12305555555556,
		power: 1653.0577657143,
		road: 21307.910324074,
		acceleration: 0.556574074074073
	},
	{
		id: 3026,
		time: 3025,
		velocity: 3.48388888888889,
		power: 1557.78794128222,
		road: 21310.9281481481,
		acceleration: 0.411388888888889
	},
	{
		id: 3027,
		time: 3026,
		velocity: 3.06361111111111,
		power: -179.614263041309,
		road: 21314.0549537037,
		acceleration: -0.193425925925927
	},
	{
		id: 3028,
		time: 3027,
		velocity: 2.54277777777778,
		power: -337.885137886257,
		road: 21316.9576388889,
		acceleration: -0.254814814814814
	},
	{
		id: 3029,
		time: 3028,
		velocity: 2.71944444444444,
		power: 192.249621917424,
		road: 21319.7039814814,
		acceleration: -0.0578703703703707
	},
	{
		id: 3030,
		time: 3029,
		velocity: 2.89,
		power: -21.4957387199194,
		road: 21322.3514351851,
		acceleration: -0.139907407407407
	},
	{
		id: 3031,
		time: 3030,
		velocity: 2.12305555555556,
		power: -35.8290756501448,
		road: 21324.8559259259,
		acceleration: -0.146018518518518
	},
	{
		id: 3032,
		time: 3031,
		velocity: 2.28138888888889,
		power: -324.065693160618,
		road: 21327.1476851851,
		acceleration: -0.279444444444445
	},
	{
		id: 3033,
		time: 3032,
		velocity: 2.05166666666667,
		power: -346.56711479622,
		road: 21329.1433333333,
		acceleration: -0.312777777777778
	},
	{
		id: 3034,
		time: 3033,
		velocity: 1.18472222222222,
		power: -873.941455716503,
		road: 21330.6023611111,
		acceleration: -0.760462962962963
	},
	{
		id: 3035,
		time: 3034,
		velocity: 0,
		power: -336.381152789729,
		road: 21331.3926851852,
		acceleration: -0.576944444444444
	},
	{
		id: 3036,
		time: 3035,
		velocity: 0.320833333333333,
		power: -17.991652499164,
		road: 21331.8078703703,
		acceleration: -0.173333333333333
	},
	{
		id: 3037,
		time: 3036,
		velocity: 0.664722222222222,
		power: 84.9361514990255,
		road: 21332.1899074074,
		acceleration: 0.107037037037037
	},
	{
		id: 3038,
		time: 3037,
		velocity: 0.321111111111111,
		power: 7.47382788132283,
		road: 21332.5719907407,
		acceleration: -0.106944444444444
	},
	{
		id: 3039,
		time: 3038,
		velocity: 0,
		power: 47.1314276118696,
		road: 21332.910324074,
		acceleration: 0.0194444444444445
	},
	{
		id: 3040,
		time: 3039,
		velocity: 0.723055555555556,
		power: 69.1128678218828,
		road: 21333.2905092592,
		acceleration: 0.0642592592592593
	},
	{
		id: 3041,
		time: 3040,
		velocity: 0.513888888888889,
		power: 49.8530603849022,
		road: 21333.702824074,
		acceleration: 0
	},
	{
		id: 3042,
		time: 3041,
		velocity: 0,
		power: -31.3689540801852,
		road: 21333.9946296296,
		acceleration: -0.241018518518519
	},
	{
		id: 3043,
		time: 3042,
		velocity: 0,
		power: -3.55121263807668,
		road: 21334.0802777777,
		acceleration: -0.171296296296296
	},
	{
		id: 3044,
		time: 3043,
		velocity: 0,
		power: 0,
		road: 21334.0802777777,
		acceleration: 0
	},
	{
		id: 3045,
		time: 3044,
		velocity: 0,
		power: 0,
		road: 21334.0802777777,
		acceleration: 0
	},
	{
		id: 3046,
		time: 3045,
		velocity: 0,
		power: 0,
		road: 21334.0802777777,
		acceleration: 0
	},
	{
		id: 3047,
		time: 3046,
		velocity: 0,
		power: 0,
		road: 21334.0802777777,
		acceleration: 0
	},
	{
		id: 3048,
		time: 3047,
		velocity: 0,
		power: 0,
		road: 21334.0802777777,
		acceleration: 0
	},
	{
		id: 3049,
		time: 3048,
		velocity: 0,
		power: 1.30281926517062,
		road: 21334.0896759259,
		acceleration: 0.0187962962962963
	},
	{
		id: 3050,
		time: 3049,
		velocity: 0.0563888888888889,
		power: 2.27093255243351,
		road: 21334.1084722222,
		acceleration: 0
	},
	{
		id: 3051,
		time: 3050,
		velocity: 0,
		power: 2.27093255243351,
		road: 21334.1272685185,
		acceleration: 0
	},
	{
		id: 3052,
		time: 3051,
		velocity: 0,
		power: 0.968111484730344,
		road: 21334.1366666666,
		acceleration: -0.0187962962962963
	},
	{
		id: 3053,
		time: 3052,
		velocity: 0,
		power: 0,
		road: 21334.1366666666,
		acceleration: 0
	},
	{
		id: 3054,
		time: 3053,
		velocity: 0,
		power: 0,
		road: 21334.1366666666,
		acceleration: 0
	}
];
export default train6;
