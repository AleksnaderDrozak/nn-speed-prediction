export const train5 = 
[
	{
		id: 1,
		time: 0,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 3,
		time: 2,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 4,
		time: 3,
		velocity: 0,
		power: 10.2098629118302,
		road: 0.0481481481481482,
		acceleration: 0.0962962962962963
	},
	{
		id: 5,
		time: 4,
		velocity: 0.288888888888889,
		power: 11.634800547507,
		road: 0.144444444444444,
		acceleration: 0
	},
	{
		id: 6,
		time: 5,
		velocity: 0,
		power: 11.634800547507,
		road: 0.240740740740741,
		acceleration: 0
	},
	{
		id: 7,
		time: 6,
		velocity: 0,
		power: 63.1498201247574,
		road: 0.440277777777778,
		acceleration: 0.206481481481481
	},
	{
		id: 8,
		time: 7,
		velocity: 0.908333333333333,
		power: 36.5960421228856,
		road: 0.743055555555556,
		acceleration: 0
	},
	{
		id: 9,
		time: 8,
		velocity: 0,
		power: 36.5960421228856,
		road: 1.04583333333333,
		acceleration: 0
	},
	{
		id: 10,
		time: 9,
		velocity: 0,
		power: -22.5941720224708,
		road: 1.21388888888889,
		acceleration: -0.269444444444444
	},
	{
		id: 11,
		time: 10,
		velocity: 0.1,
		power: 4.02728326421053,
		road: 1.24722222222222,
		acceleration: 0
	},
	{
		id: 12,
		time: 11,
		velocity: 0,
		power: 4.02728326421053,
		road: 1.28055555555556,
		acceleration: 0
	},
	{
		id: 13,
		time: 12,
		velocity: 0,
		power: 1.48731578947368,
		road: 1.29722222222222,
		acceleration: -0.0333333333333333
	},
	{
		id: 14,
		time: 13,
		velocity: 0,
		power: 0,
		road: 1.29722222222222,
		acceleration: 0
	},
	{
		id: 15,
		time: 14,
		velocity: 0,
		power: 0,
		road: 1.29722222222222,
		acceleration: 0
	},
	{
		id: 16,
		time: 15,
		velocity: 0,
		power: 3.64905161849427,
		road: 1.31958333333333,
		acceleration: 0.0447222222222222
	},
	{
		id: 17,
		time: 16,
		velocity: 0.134166666666667,
		power: 5.40329329546807,
		road: 1.36430555555556,
		acceleration: 0
	},
	{
		id: 18,
		time: 17,
		velocity: 0,
		power: 5.40329329546807,
		road: 1.40902777777778,
		acceleration: 0
	},
	{
		id: 19,
		time: 18,
		velocity: 0,
		power: 1.75421739766082,
		road: 1.43138888888889,
		acceleration: -0.0447222222222222
	},
	{
		id: 20,
		time: 19,
		velocity: 0,
		power: 0,
		road: 1.43138888888889,
		acceleration: 0
	},
	{
		id: 21,
		time: 20,
		velocity: 0,
		power: 0,
		road: 1.43138888888889,
		acceleration: 0
	},
	{
		id: 22,
		time: 21,
		velocity: 0,
		power: 0,
		road: 1.43138888888889,
		acceleration: 0
	},
	{
		id: 23,
		time: 22,
		velocity: 0,
		power: 0,
		road: 1.43138888888889,
		acceleration: 0
	},
	{
		id: 24,
		time: 23,
		velocity: 0,
		power: 0,
		road: 1.43138888888889,
		acceleration: 0
	},
	{
		id: 25,
		time: 24,
		velocity: 0,
		power: 0,
		road: 1.43138888888889,
		acceleration: 0
	},
	{
		id: 26,
		time: 25,
		velocity: 0,
		power: 0,
		road: 1.43138888888889,
		acceleration: 0
	},
	{
		id: 27,
		time: 26,
		velocity: 0,
		power: 0,
		road: 1.43138888888889,
		acceleration: 0
	},
	{
		id: 28,
		time: 27,
		velocity: 0,
		power: 0,
		road: 1.43138888888889,
		acceleration: 0
	},
	{
		id: 29,
		time: 28,
		velocity: 0,
		power: 74.9184033271437,
		road: 1.60087962962963,
		acceleration: 0.338981481481481
	},
	{
		id: 30,
		time: 29,
		velocity: 1.01694444444444,
		power: 498.264423089541,
		road: 2.26925925925926,
		acceleration: 0.658796296296296
	},
	{
		id: 31,
		time: 30,
		velocity: 1.97638888888889,
		power: 1204.29685600373,
		road: 3.65953703703704,
		acceleration: 0.785
	},
	{
		id: 32,
		time: 31,
		velocity: 2.355,
		power: 881.647798937289,
		road: 5.61518518518518,
		acceleration: 0.345740740740741
	},
	{
		id: 33,
		time: 32,
		velocity: 2.05416666666667,
		power: 215.961532488049,
		road: 7.7325,
		acceleration: -0.0224074074074072
	},
	{
		id: 34,
		time: 33,
		velocity: 1.90916666666667,
		power: -350.018879228254,
		road: 9.67902777777778,
		acceleration: -0.319166666666667
	},
	{
		id: 35,
		time: 34,
		velocity: 1.3975,
		power: -346.354807917841,
		road: 11.2880092592593,
		acceleration: -0.355925925925926
	},
	{
		id: 36,
		time: 35,
		velocity: 0.986388888888889,
		power: -329.201976835254,
		road: 12.5131481481481,
		acceleration: -0.411759259259259
	},
	{
		id: 37,
		time: 36,
		velocity: 0.673888888888889,
		power: -56.156300454588,
		road: 13.4363425925926,
		acceleration: -0.19212962962963
	},
	{
		id: 38,
		time: 37,
		velocity: 0.821111111111111,
		power: 92.3016572107012,
		road: 14.25875,
		acceleration: -0.00944444444444459
	},
	{
		id: 39,
		time: 38,
		velocity: 0.958055555555555,
		power: 279.109184176384,
		road: 15.1734259259259,
		acceleration: 0.193981481481481
	},
	{
		id: 40,
		time: 39,
		velocity: 1.25583333333333,
		power: 651.277797786442,
		road: 16.400787037037,
		acceleration: 0.431388888888889
	},
	{
		id: 41,
		time: 40,
		velocity: 2.11527777777778,
		power: 860.236818286175,
		road: 18.05375,
		acceleration: 0.419814814814815
	},
	{
		id: 42,
		time: 41,
		velocity: 2.2175,
		power: -47.061127154848,
		road: 19.8381018518519,
		acceleration: -0.157037037037037
	},
	{
		id: 43,
		time: 42,
		velocity: 0.784722222222222,
		power: -739.734959639606,
		road: 21.1913888888889,
		acceleration: -0.705092592592593
	},
	{
		id: 44,
		time: 43,
		velocity: 0,
		power: -365.69772648119,
		road: 21.8225462962963,
		acceleration: -0.739166666666667
	},
	{
		id: 45,
		time: 44,
		velocity: 0,
		power: 263.977704226054,
		road: 22.3075,
		acceleration: 0.446759259259259
	},
	{
		id: 46,
		time: 45,
		velocity: 2.125,
		power: 954.100162772698,
		road: 23.4087037037037,
		acceleration: 0.785740740740741
	},
	{
		id: 47,
		time: 46,
		velocity: 2.35722222222222,
		power: 1484.00588313303,
		road: 25.2605555555556,
		acceleration: 0.715555555555556
	},
	{
		id: 48,
		time: 47,
		velocity: 2.14666666666667,
		power: 2327.62194121957,
		road: 27.8738888888889,
		acceleration: 0.807407407407407
	},
	{
		id: 49,
		time: 48,
		velocity: 4.54722222222222,
		power: 1807.74764755521,
		road: 31.1178240740741,
		acceleration: 0.453796296296296
	},
	{
		id: 50,
		time: 49,
		velocity: 3.71861111111111,
		power: 2195.22569278329,
		road: 34.8323148148148,
		acceleration: 0.487314814814815
	},
	{
		id: 51,
		time: 50,
		velocity: 3.60861111111111,
		power: -975.199936992698,
		road: 38.5859722222222,
		acceleration: -0.408981481481482
	},
	{
		id: 52,
		time: 51,
		velocity: 3.32027777777778,
		power: -233.836792016936,
		road: 42.0323611111111,
		acceleration: -0.205555555555555
	},
	{
		id: 53,
		time: 52,
		velocity: 3.10194444444444,
		power: -254.906140568006,
		road: 45.2678240740741,
		acceleration: -0.216296296296296
	},
	{
		id: 54,
		time: 53,
		velocity: 2.95972222222222,
		power: -203.239810468577,
		road: 48.2934722222222,
		acceleration: -0.203333333333334
	},
	{
		id: 55,
		time: 54,
		velocity: 2.71027777777778,
		power: 137.659999847635,
		road: 51.1765740740741,
		acceleration: -0.0817592592592589
	},
	{
		id: 56,
		time: 55,
		velocity: 2.85666666666667,
		power: 483.020841998366,
		road: 54.0416203703704,
		acceleration: 0.0456481481481479
	},
	{
		id: 57,
		time: 56,
		velocity: 3.09666666666667,
		power: 1373.43778552005,
		road: 57.0997685185185,
		acceleration: 0.340555555555556
	},
	{
		id: 58,
		time: 57,
		velocity: 3.73194444444444,
		power: 2607.27376845721,
		road: 60.647962962963,
		acceleration: 0.639537037037037
	},
	{
		id: 59,
		time: 58,
		velocity: 4.77527777777778,
		power: 3130.98709527488,
		road: 64.840462962963,
		acceleration: 0.649074074074075
	},
	{
		id: 60,
		time: 59,
		velocity: 5.04388888888889,
		power: 3085.62681548111,
		road: 69.6266666666667,
		acceleration: 0.538333333333332
	},
	{
		id: 61,
		time: 60,
		velocity: 5.34694444444444,
		power: 3025.21288172565,
		road: 74.9116666666667,
		acceleration: 0.459259259259261
	},
	{
		id: 62,
		time: 61,
		velocity: 6.15305555555556,
		power: 4812.99368993446,
		road: 80.7839814814815,
		acceleration: 0.715370370370369
	},
	{
		id: 63,
		time: 62,
		velocity: 7.19,
		power: 6677.44061372442,
		road: 87.463287037037,
		acceleration: 0.89861111111111
	},
	{
		id: 64,
		time: 63,
		velocity: 8.04277777777778,
		power: 5582.45527953473,
		road: 94.906712962963,
		acceleration: 0.629629629629632
	},
	{
		id: 65,
		time: 64,
		velocity: 8.04194444444444,
		power: 5492.24947778385,
		road: 102.94212962963,
		acceleration: 0.554351851851851
	},
	{
		id: 66,
		time: 65,
		velocity: 8.85305555555555,
		power: 4093.38522142118,
		road: 111.424212962963,
		acceleration: 0.338981481481483
	},
	{
		id: 67,
		time: 66,
		velocity: 9.05972222222222,
		power: 4120.1148723845,
		road: 120.235740740741,
		acceleration: 0.319907407407406
	},
	{
		id: 68,
		time: 67,
		velocity: 9.00166666666667,
		power: 1349.19797549766,
		road: 129.199907407407,
		acceleration: -0.0146296296296295
	},
	{
		id: 69,
		time: 68,
		velocity: 8.80916666666667,
		power: 259.005258914506,
		road: 138.086111111111,
		acceleration: -0.141296296296296
	},
	{
		id: 70,
		time: 69,
		velocity: 8.63583333333333,
		power: 50.1852819564251,
		road: 146.819490740741,
		acceleration: -0.164351851851851
	},
	{
		id: 71,
		time: 70,
		velocity: 8.50861111111111,
		power: -1682.88384478899,
		road: 155.282361111111,
		acceleration: -0.376666666666667
	},
	{
		id: 72,
		time: 71,
		velocity: 7.67916666666667,
		power: -4646.65501015165,
		road: 163.165972222222,
		acceleration: -0.781851851851852
	},
	{
		id: 73,
		time: 72,
		velocity: 6.29027777777778,
		power: -5847.38042978307,
		road: 170.140462962963,
		acceleration: -1.03638888888889
	},
	{
		id: 74,
		time: 73,
		velocity: 5.39944444444444,
		power: -4895.67365721381,
		road: 176.090231481482,
		acceleration: -1.01305555555556
	},
	{
		id: 75,
		time: 74,
		velocity: 4.64,
		power: -1585.11828098433,
		road: 181.302083333333,
		acceleration: -0.462777777777778
	},
	{
		id: 76,
		time: 75,
		velocity: 4.90194444444444,
		power: -47.0415555164819,
		road: 186.207037037037,
		acceleration: -0.151018518518518
	},
	{
		id: 77,
		time: 76,
		velocity: 4.94638888888889,
		power: 2478.53801988376,
		road: 191.225601851852,
		acceleration: 0.378240740740741
	},
	{
		id: 78,
		time: 77,
		velocity: 5.77472222222222,
		power: 4255.52543716232,
		road: 196.765092592593,
		acceleration: 0.663611111111111
	},
	{
		id: 79,
		time: 78,
		velocity: 6.89277777777778,
		power: 5255.19017398487,
		road: 203.00462962963,
		acceleration: 0.736481481481482
	},
	{
		id: 80,
		time: 79,
		velocity: 7.15583333333333,
		power: 7663.00686788727,
		road: 210.101990740741,
		acceleration: 0.979166666666666
	},
	{
		id: 81,
		time: 80,
		velocity: 8.71222222222222,
		power: 8390.17431520586,
		road: 218.154305555556,
		acceleration: 0.930740740740741
	},
	{
		id: 82,
		time: 81,
		velocity: 9.685,
		power: 10804.4166149439,
		road: 227.211435185185,
		acceleration: 1.07888888888889
	},
	{
		id: 83,
		time: 82,
		velocity: 10.3925,
		power: 9381.86632351809,
		road: 237.208564814815,
		acceleration: 0.801111111111108
	},
	{
		id: 84,
		time: 83,
		velocity: 11.1155555555556,
		power: 6922.86376430196,
		road: 247.851805555556,
		acceleration: 0.491111111111113
	},
	{
		id: 85,
		time: 84,
		velocity: 11.1583333333333,
		power: 3065.14076653974,
		road: 258.790138888889,
		acceleration: 0.0990740740740712
	},
	{
		id: 86,
		time: 85,
		velocity: 10.6897222222222,
		power: -98.2971422018946,
		road: 269.676157407407,
		acceleration: -0.203703703703702
	},
	{
		id: 87,
		time: 86,
		velocity: 10.5044444444444,
		power: 1060.0725193328,
		road: 280.41587962963,
		acceleration: -0.0888888888888886
	},
	{
		id: 88,
		time: 87,
		velocity: 10.8916666666667,
		power: 3856.14722187953,
		road: 291.202175925926,
		acceleration: 0.182037037037038
	},
	{
		id: 89,
		time: 88,
		velocity: 11.2358333333333,
		power: 5619.5938851627,
		road: 302.248194444444,
		acceleration: 0.337407407407406
	},
	{
		id: 90,
		time: 89,
		velocity: 11.5166666666667,
		power: 4781.6971011677,
		road: 313.584166666667,
		acceleration: 0.242500000000001
	},
	{
		id: 91,
		time: 90,
		velocity: 11.6191666666667,
		power: 4820.72089920901,
		road: 325.158287037037,
		acceleration: 0.233796296296296
	},
	{
		id: 92,
		time: 91,
		velocity: 11.9372222222222,
		power: 4049.67445061613,
		road: 336.926944444445,
		acceleration: 0.155277777777776
	},
	{
		id: 93,
		time: 92,
		velocity: 11.9825,
		power: 4416.9223654599,
		road: 348.863333333333,
		acceleration: 0.180185185185188
	},
	{
		id: 94,
		time: 93,
		velocity: 12.1597222222222,
		power: 3211.96368576585,
		road: 360.924675925926,
		acceleration: 0.0697222222222198
	},
	{
		id: 95,
		time: 94,
		velocity: 12.1463888888889,
		power: 105.491120902949,
		road: 372.921203703704,
		acceleration: -0.199351851851851
	},
	{
		id: 96,
		time: 95,
		velocity: 11.3844444444444,
		power: -3049.1742073753,
		road: 384.578888888889,
		acceleration: -0.478333333333334
	},
	{
		id: 97,
		time: 96,
		velocity: 10.7247222222222,
		power: -5592.72132083246,
		road: 395.633888888889,
		acceleration: -0.727037037037038
	},
	{
		id: 98,
		time: 97,
		velocity: 9.96527777777778,
		power: -4885.1306178378,
		road: 405.983796296296,
		acceleration: -0.683148148148145
	},
	{
		id: 99,
		time: 98,
		velocity: 9.335,
		power: -3225.39253394045,
		road: 415.727916666667,
		acceleration: -0.528425925925927
	},
	{
		id: 100,
		time: 99,
		velocity: 9.13944444444444,
		power: -124.040009450579,
		road: 425.112361111111,
		acceleration: -0.190925925925926
	},
	{
		id: 101,
		time: 100,
		velocity: 9.3925,
		power: 4607.12712322448,
		road: 434.568194444444,
		acceleration: 0.333703703703703
	},
	{
		id: 102,
		time: 101,
		velocity: 10.3361111111111,
		power: 7006.5539770356,
		road: 444.470833333333,
		acceleration: 0.559907407407406
	},
	{
		id: 103,
		time: 102,
		velocity: 10.8191666666667,
		power: 12376.8412124796,
		road: 455.164537037037,
		acceleration: 1.02222222222222
	},
	{
		id: 104,
		time: 103,
		velocity: 12.4591666666667,
		power: 14453.1012790157,
		road: 466.911666666667,
		acceleration: 1.08462962962963
	},
	{
		id: 105,
		time: 104,
		velocity: 13.59,
		power: 16367.2716346874,
		road: 479.758287037037,
		acceleration: 1.11435185185185
	},
	{
		id: 106,
		time: 105,
		velocity: 14.1622222222222,
		power: 13821.4473722479,
		road: 493.568611111111,
		acceleration: 0.813055555555554
	},
	{
		id: 107,
		time: 106,
		velocity: 14.8983333333333,
		power: 11535.9426995115,
		road: 508.078518518519,
		acceleration: 0.586111111111112
	},
	{
		id: 108,
		time: 107,
		velocity: 15.3483333333333,
		power: 10843.4209332764,
		road: 523.130833333333,
		acceleration: 0.498703703703704
	},
	{
		id: 109,
		time: 108,
		velocity: 15.6583333333333,
		power: 8488.44630459956,
		road: 538.588703703704,
		acceleration: 0.312407407407406
	},
	{
		id: 110,
		time: 109,
		velocity: 15.8355555555556,
		power: 6599.56588322479,
		road: 554.289444444445,
		acceleration: 0.173333333333334
	},
	{
		id: 111,
		time: 110,
		velocity: 15.8683333333333,
		power: 2952.68010146661,
		road: 570.04125,
		acceleration: -0.0712037037037039
	},
	{
		id: 112,
		time: 111,
		velocity: 15.4447222222222,
		power: 543.139351078046,
		road: 585.643333333333,
		acceleration: -0.228240740740741
	},
	{
		id: 113,
		time: 112,
		velocity: 15.1508333333333,
		power: -3681.854988801,
		road: 600.875694444445,
		acceleration: -0.511203703703703
	},
	{
		id: 114,
		time: 113,
		velocity: 14.3347222222222,
		power: -2006.20041916489,
		road: 615.656111111111,
		acceleration: -0.392685185185186
	},
	{
		id: 115,
		time: 114,
		velocity: 14.2666666666667,
		power: -3018.68349134323,
		road: 630.008287037037,
		acceleration: -0.463796296296294
	},
	{
		id: 116,
		time: 115,
		velocity: 13.7594444444444,
		power: -786.879731864003,
		road: 643.980324074074,
		acceleration: -0.296481481481482
	},
	{
		id: 117,
		time: 116,
		velocity: 13.4452777777778,
		power: -2214.1125826934,
		road: 657.602962962963,
		acceleration: -0.402314814814815
	},
	{
		id: 118,
		time: 117,
		velocity: 13.0597222222222,
		power: -2001.60021622154,
		road: 670.83212962963,
		acceleration: -0.384629629629629
	},
	{
		id: 119,
		time: 118,
		velocity: 12.6055555555556,
		power: -2720.54412309385,
		road: 683.647731481482,
		acceleration: -0.442499999999999
	},
	{
		id: 120,
		time: 119,
		velocity: 12.1177777777778,
		power: -4339.25926417469,
		road: 695.950833333333,
		acceleration: -0.582500000000001
	},
	{
		id: 121,
		time: 120,
		velocity: 11.3122222222222,
		power: -7529.60311603741,
		road: 707.519953703704,
		acceleration: -0.885462962962963
	},
	{
		id: 122,
		time: 121,
		velocity: 9.94916666666667,
		power: -10973.0951352252,
		road: 718.002361111111,
		acceleration: -1.28796296296296
	},
	{
		id: 123,
		time: 122,
		velocity: 8.25388888888889,
		power: -10132.322544083,
		road: 727.173194444444,
		acceleration: -1.33518518518519
	},
	{
		id: 124,
		time: 123,
		velocity: 7.30666666666667,
		power: -9596.20454982582,
		road: 734.946944444444,
		acceleration: -1.45898148148148
	},
	{
		id: 125,
		time: 124,
		velocity: 5.57222222222222,
		power: -9169.36889167922,
		road: 741.137546296296,
		acceleration: -1.70731481481481
	},
	{
		id: 126,
		time: 125,
		velocity: 3.13194444444444,
		power: -7931.54971237344,
		road: 745.433194444444,
		acceleration: -2.08259259259259
	},
	{
		id: 127,
		time: 126,
		velocity: 1.05888888888889,
		power: -3808.88104987615,
		road: 747.758842592593,
		acceleration: -1.85740740740741
	},
	{
		id: 128,
		time: 127,
		velocity: 0,
		power: -759.590673436149,
		road: 748.633796296296,
		acceleration: -1.04398148148148
	},
	{
		id: 129,
		time: 128,
		velocity: 0,
		power: -37.6908094217024,
		road: 748.810277777778,
		acceleration: -0.352962962962963
	},
	{
		id: 130,
		time: 129,
		velocity: 0,
		power: 719.864539719662,
		road: 749.395416666667,
		acceleration: 1.17027777777778
	},
	{
		id: 131,
		time: 130,
		velocity: 3.51083333333333,
		power: 2785.47052490306,
		road: 751.280046296296,
		acceleration: 1.4287037037037
	},
	{
		id: 132,
		time: 131,
		velocity: 4.28611111111111,
		power: 4641.3240857196,
		road: 754.558055555556,
		acceleration: 1.35805555555556
	},
	{
		id: 133,
		time: 132,
		velocity: 4.07416666666667,
		power: 1181.51246963431,
		road: 758.600694444444,
		acceleration: 0.171203703703703
	},
	{
		id: 134,
		time: 133,
		velocity: 4.02444444444444,
		power: 152.401490768305,
		road: 762.680231481482,
		acceleration: -0.0974074074074078
	},
	{
		id: 135,
		time: 134,
		velocity: 3.99388888888889,
		power: 984.562558167213,
		road: 766.769444444445,
		acceleration: 0.116759259259259
	},
	{
		id: 136,
		time: 135,
		velocity: 4.42444444444444,
		power: 2085.69691583086,
		road: 771.101527777778,
		acceleration: 0.368981481481482
	},
	{
		id: 137,
		time: 136,
		velocity: 5.13138888888889,
		power: 7273.43166047284,
		road: 776.285092592593,
		acceleration: 1.33398148148148
	},
	{
		id: 138,
		time: 137,
		velocity: 7.99583333333333,
		power: 10783.4649175577,
		road: 782.91462962963,
		acceleration: 1.55796296296296
	},
	{
		id: 139,
		time: 138,
		velocity: 9.09833333333333,
		power: 15491.1396396139,
		road: 791.219583333334,
		acceleration: 1.79287037037037
	},
	{
		id: 140,
		time: 139,
		velocity: 10.51,
		power: 12338.9111278822,
		road: 800.992870370371,
		acceleration: 1.1437962962963
	},
	{
		id: 141,
		time: 140,
		velocity: 11.4272222222222,
		power: 9999.89017008748,
		road: 811.730324074074,
		acceleration: 0.784537037037035
	},
	{
		id: 142,
		time: 141,
		velocity: 11.4519444444444,
		power: 3706.75596192227,
		road: 822.934444444445,
		acceleration: 0.148796296296297
	},
	{
		id: 143,
		time: 142,
		velocity: 10.9563888888889,
		power: -910.953791901262,
		road: 834.071388888889,
		acceleration: -0.283148148148149
	},
	{
		id: 144,
		time: 143,
		velocity: 10.5777777777778,
		power: -55.820231910381,
		road: 844.966898148148,
		acceleration: -0.199722222222221
	},
	{
		id: 145,
		time: 144,
		velocity: 10.8527777777778,
		power: 1205.65856507861,
		road: 855.725,
		acceleration: -0.0750925925925934
	},
	{
		id: 146,
		time: 145,
		velocity: 10.7311111111111,
		power: 790.023730937593,
		road: 866.388657407408,
		acceleration: -0.113796296296297
	},
	{
		id: 147,
		time: 146,
		velocity: 10.2363888888889,
		power: 110.063973883757,
		road: 876.906018518519,
		acceleration: -0.178796296296294
	},
	{
		id: 148,
		time: 147,
		velocity: 10.3163888888889,
		power: -4425.50344649837,
		road: 887.011666666667,
		acceleration: -0.64462962962963
	},
	{
		id: 149,
		time: 148,
		velocity: 8.79722222222222,
		power: -5198.47922876422,
		road: 896.416157407407,
		acceleration: -0.757685185185185
	},
	{
		id: 150,
		time: 149,
		velocity: 7.96333333333333,
		power: -6650.30719810141,
		road: 904.948148148148,
		acceleration: -0.987314814814814
	},
	{
		id: 151,
		time: 150,
		velocity: 7.35444444444444,
		power: -3360.8387586255,
		road: 912.677453703704,
		acceleration: -0.618055555555557
	},
	{
		id: 152,
		time: 151,
		velocity: 6.94305555555556,
		power: -1202.5312115665,
		road: 919.932083333333,
		acceleration: -0.331296296296295
	},
	{
		id: 153,
		time: 152,
		velocity: 6.96944444444444,
		power: 1473.14019393908,
		road: 927.051851851852,
		acceleration: 0.0615740740740733
	},
	{
		id: 154,
		time: 153,
		velocity: 7.53916666666667,
		power: 2145.39046417516,
		road: 934.28,
		acceleration: 0.155185185185186
	},
	{
		id: 155,
		time: 154,
		velocity: 7.40861111111111,
		power: 823.350741480469,
		road: 941.566481481482,
		acceleration: -0.0385185185185186
	},
	{
		id: 156,
		time: 155,
		velocity: 6.85388888888889,
		power: -2656.07952803741,
		road: 948.556481481482,
		acceleration: -0.554444444444445
	},
	{
		id: 157,
		time: 156,
		velocity: 5.87583333333333,
		power: -2181.17056650971,
		road: 955.01625,
		acceleration: -0.506018518518519
	},
	{
		id: 158,
		time: 157,
		velocity: 5.89055555555556,
		power: -665.628112354711,
		road: 961.091296296296,
		acceleration: -0.263425925925926
	},
	{
		id: 159,
		time: 158,
		velocity: 6.06361111111111,
		power: 2432.64334383347,
		road: 967.170972222222,
		acceleration: 0.272685185185185
	},
	{
		id: 160,
		time: 159,
		velocity: 6.69388888888889,
		power: 4920.56827217146,
		road: 973.707083333334,
		acceleration: 0.640185185185185
	},
	{
		id: 161,
		time: 160,
		velocity: 7.81111111111111,
		power: 5637.66540129502,
		road: 980.89712962963,
		acceleration: 0.667685185185185
	},
	{
		id: 162,
		time: 161,
		velocity: 8.06666666666667,
		power: 6562.8324713588,
		road: 988.777361111111,
		acceleration: 0.712685185185184
	},
	{
		id: 163,
		time: 162,
		velocity: 8.83194444444444,
		power: 6523.5029852827,
		road: 997.330185185185,
		acceleration: 0.632500000000002
	},
	{
		id: 164,
		time: 163,
		velocity: 9.70861111111111,
		power: 13882.245989812,
		road: 1006.87333333333,
		acceleration: 1.34814814814815
	},
	{
		id: 165,
		time: 164,
		velocity: 12.1111111111111,
		power: 10096.2132807082,
		road: 1017.4937037037,
		acceleration: 0.806296296296299
	},
	{
		id: 166,
		time: 165,
		velocity: 11.2508333333333,
		power: 9473.36211413129,
		road: 1028.85435185185,
		acceleration: 0.674259259259259
	},
	{
		id: 167,
		time: 166,
		velocity: 11.7313888888889,
		power: 969.344134917206,
		road: 1040.49388888889,
		acceleration: -0.116481481481483
	},
	{
		id: 168,
		time: 167,
		velocity: 11.7616666666667,
		power: 5725.12611468614,
		road: 1052.22842592593,
		acceleration: 0.306481481481482
	},
	{
		id: 169,
		time: 168,
		velocity: 12.1702777777778,
		power: 1827.91873254909,
		road: 1064.09356481481,
		acceleration: -0.0452777777777769
	},
	{
		id: 170,
		time: 169,
		velocity: 11.5955555555556,
		power: 2874.06748709021,
		road: 1075.95962962963,
		acceleration: 0.0471296296296284
	},
	{
		id: 171,
		time: 170,
		velocity: 11.9030555555556,
		power: 1624.69262601448,
		road: 1087.81773148148,
		acceleration: -0.0630555555555556
	},
	{
		id: 172,
		time: 171,
		velocity: 11.9811111111111,
		power: 4150.50998459045,
		road: 1099.72337962963,
		acceleration: 0.158148148148149
	},
	{
		id: 173,
		time: 172,
		velocity: 12.07,
		power: 2699.93959273095,
		road: 1111.72175925926,
		acceleration: 0.0273148148148152
	},
	{
		id: 174,
		time: 173,
		velocity: 11.985,
		power: 2517.21110065723,
		road: 1123.73916666667,
		acceleration: 0.0107407407407383
	},
	{
		id: 175,
		time: 174,
		velocity: 12.0133333333333,
		power: 2245.10650200927,
		road: 1135.75546296296,
		acceleration: -0.0129629629629608
	},
	{
		id: 176,
		time: 175,
		velocity: 12.0311111111111,
		power: 1473.41918934416,
		road: 1147.72569444444,
		acceleration: -0.0791666666666693
	},
	{
		id: 177,
		time: 176,
		velocity: 11.7475,
		power: 3432.15469490254,
		road: 1159.70240740741,
		acceleration: 0.0921296296296301
	},
	{
		id: 178,
		time: 177,
		velocity: 12.2897222222222,
		power: 1453.46255103503,
		road: 1171.68458333333,
		acceleration: -0.0812037037037019
	},
	{
		id: 179,
		time: 178,
		velocity: 11.7875,
		power: 2383.2668976058,
		road: 1183.62685185185,
		acceleration: 0.00138888888888999
	},
	{
		id: 180,
		time: 179,
		velocity: 11.7516666666667,
		power: 1106.36421124709,
		road: 1195.5150462963,
		acceleration: -0.10953703703704
	},
	{
		id: 181,
		time: 180,
		velocity: 11.9611111111111,
		power: 3919.70887972738,
		road: 1207.4174537037,
		acceleration: 0.137962962962964
	},
	{
		id: 182,
		time: 181,
		velocity: 12.2013888888889,
		power: 4223.83965095986,
		road: 1219.46791666667,
		acceleration: 0.158148148148149
	},
	{
		id: 183,
		time: 182,
		velocity: 12.2261111111111,
		power: 4265.18697018118,
		road: 1231.67486111111,
		acceleration: 0.154814814814813
	},
	{
		id: 184,
		time: 183,
		velocity: 12.4255555555556,
		power: 3953.73129676664,
		road: 1244.02037037037,
		acceleration: 0.122314814814816
	},
	{
		id: 185,
		time: 184,
		velocity: 12.5683333333333,
		power: 4628.64721710154,
		road: 1256.51347222222,
		acceleration: 0.17287037037037
	},
	{
		id: 186,
		time: 185,
		velocity: 12.7447222222222,
		power: 4270.21917931747,
		road: 1269.16111111111,
		acceleration: 0.136203703703705
	},
	{
		id: 187,
		time: 186,
		velocity: 12.8341666666667,
		power: 4175.49051889467,
		road: 1281.93833333333,
		acceleration: 0.12296296296296
	},
	{
		id: 188,
		time: 187,
		velocity: 12.9372222222222,
		power: 1059.30553596272,
		road: 1294.71078703704,
		acceleration: -0.132499999999997
	},
	{
		id: 189,
		time: 188,
		velocity: 12.3472222222222,
		power: -2069.66483177024,
		road: 1307.2224537037,
		acceleration: -0.389074074074077
	},
	{
		id: 190,
		time: 189,
		velocity: 11.6669444444444,
		power: -2774.97780869337,
		road: 1319.31435185185,
		acceleration: -0.450462962962961
	},
	{
		id: 191,
		time: 190,
		velocity: 11.5858333333333,
		power: 190.344104330409,
		road: 1331.08671296296,
		acceleration: -0.188611111111113
	},
	{
		id: 192,
		time: 191,
		velocity: 11.7813888888889,
		power: 2587.91609630473,
		road: 1342.77856481482,
		acceleration: 0.0275925925925939
	},
	{
		id: 193,
		time: 192,
		velocity: 11.7497222222222,
		power: 3273.19605918867,
		road: 1354.52763888889,
		acceleration: 0.0868518518518506
	},
	{
		id: 194,
		time: 193,
		velocity: 11.8463888888889,
		power: -1558.10929558088,
		road: 1366.14805555556,
		acceleration: -0.344166666666666
	},
	{
		id: 195,
		time: 194,
		velocity: 10.7488888888889,
		power: -8532.45333706536,
		road: 1377.08986111111,
		acceleration: -1.01305555555555
	},
	{
		id: 196,
		time: 195,
		velocity: 8.71055555555555,
		power: -11853.4531833956,
		road: 1386.79361111111,
		acceleration: -1.46305555555556
	},
	{
		id: 197,
		time: 196,
		velocity: 7.45722222222222,
		power: -14552.1763356016,
		road: 1394.71949074074,
		acceleration: -2.09268518518518
	},
	{
		id: 198,
		time: 197,
		velocity: 4.47083333333333,
		power: -10715.0815159061,
		road: 1400.56046296296,
		acceleration: -2.07712962962963
	},
	{
		id: 199,
		time: 198,
		velocity: 2.47916666666667,
		power: -5495.36278689348,
		road: 1404.57337962963,
		acceleration: -1.57898148148148
	},
	{
		id: 200,
		time: 199,
		velocity: 2.72027777777778,
		power: -816.788224984497,
		road: 1407.58777777778,
		acceleration: -0.418055555555555
	},
	{
		id: 201,
		time: 200,
		velocity: 3.21666666666667,
		power: 2546.47067659359,
		road: 1410.75074074074,
		acceleration: 0.715185185185185
	},
	{
		id: 202,
		time: 201,
		velocity: 4.62472222222222,
		power: 4640.86219996235,
		road: 1414.80555555556,
		acceleration: 1.06851851851852
	},
	{
		id: 203,
		time: 202,
		velocity: 5.92583333333333,
		power: 5005.37436633608,
		road: 1419.8462962963,
		acceleration: 0.903333333333332
	},
	{
		id: 204,
		time: 203,
		velocity: 5.92666666666667,
		power: 4393.8692077811,
		road: 1425.66282407407,
		acceleration: 0.648240740740741
	},
	{
		id: 205,
		time: 204,
		velocity: 6.56944444444444,
		power: 3383.37225530401,
		road: 1432.0087962963,
		acceleration: 0.410648148148148
	},
	{
		id: 206,
		time: 205,
		velocity: 7.15777777777778,
		power: 5344.38322824517,
		road: 1438.89125,
		acceleration: 0.662314814814815
	},
	{
		id: 207,
		time: 206,
		velocity: 7.91361111111111,
		power: 5426.30477909205,
		road: 1446.40476851852,
		acceleration: 0.599814814814815
	},
	{
		id: 208,
		time: 207,
		velocity: 8.36888888888889,
		power: 5626.09996032689,
		road: 1454.50106481481,
		acceleration: 0.565740740740739
	},
	{
		id: 209,
		time: 208,
		velocity: 8.855,
		power: 5674.29736939433,
		road: 1463.14041666667,
		acceleration: 0.520370370370372
	},
	{
		id: 210,
		time: 209,
		velocity: 9.47472222222222,
		power: 6821.11098312868,
		road: 1472.34157407407,
		acceleration: 0.603240740740741
	},
	{
		id: 211,
		time: 210,
		velocity: 10.1786111111111,
		power: 8432.85137067166,
		road: 1482.20203703704,
		acceleration: 0.715370370370371
	},
	{
		id: 212,
		time: 211,
		velocity: 11.0011111111111,
		power: 10286.9600725806,
		road: 1492.83222222222,
		acceleration: 0.824074074074073
	},
	{
		id: 213,
		time: 212,
		velocity: 11.9469444444444,
		power: 8784.67246494513,
		road: 1504.18032407407,
		acceleration: 0.611759259259259
	},
	{
		id: 214,
		time: 213,
		velocity: 12.0138888888889,
		power: 8564.46012830985,
		road: 1516.10689814815,
		acceleration: 0.545185185185185
	},
	{
		id: 215,
		time: 214,
		velocity: 12.6366666666667,
		power: 8898.29136429186,
		road: 1528.57259259259,
		acceleration: 0.533055555555555
	},
	{
		id: 216,
		time: 215,
		velocity: 13.5461111111111,
		power: 12064.1495911022,
		road: 1541.675,
		acceleration: 0.740370370370371
	},
	{
		id: 217,
		time: 216,
		velocity: 14.235,
		power: 13173.4005044617,
		road: 1555.52768518519,
		acceleration: 0.760185185185186
	},
	{
		id: 218,
		time: 217,
		velocity: 14.9172222222222,
		power: 14796.5245379916,
		road: 1570.16537037037,
		acceleration: 0.809814814814816
	},
	{
		id: 219,
		time: 218,
		velocity: 15.9755555555556,
		power: 12550.8062607004,
		road: 1585.50592592593,
		acceleration: 0.595925925925926
	},
	{
		id: 220,
		time: 219,
		velocity: 16.0227777777778,
		power: 5848.76750205504,
		road: 1601.20611111111,
		acceleration: 0.123333333333335
	},
	{
		id: 221,
		time: 220,
		velocity: 15.2872222222222,
		power: -1133.6661749897,
		road: 1616.79768518518,
		acceleration: -0.340555555555557
	},
	{
		id: 222,
		time: 221,
		velocity: 14.9538888888889,
		power: -5126.40614696104,
		road: 1631.91337962963,
		acceleration: -0.611203703703705
	},
	{
		id: 223,
		time: 222,
		velocity: 14.1891666666667,
		power: -5334.34057250814,
		road: 1646.40787037037,
		acceleration: -0.631203703703701
	},
	{
		id: 224,
		time: 223,
		velocity: 13.3936111111111,
		power: -10954.1013038835,
		road: 1660.05,
		acceleration: -1.07351851851852
	},
	{
		id: 225,
		time: 224,
		velocity: 11.7333333333333,
		power: -15508.8675934631,
		road: 1672.38990740741,
		acceleration: -1.53092592592593
	},
	{
		id: 226,
		time: 225,
		velocity: 9.59638888888889,
		power: -18253.4167127127,
		road: 1682.96319444444,
		acceleration: -2.00231481481481
	},
	{
		id: 227,
		time: 226,
		velocity: 7.38666666666667,
		power: -13280.5653348097,
		road: 1691.64699074074,
		acceleration: -1.77666666666667
	},
	{
		id: 228,
		time: 227,
		velocity: 6.40333333333333,
		power: -6815.68688604078,
		road: 1698.86787037037,
		acceleration: -1.14916666666667
	},
	{
		id: 229,
		time: 228,
		velocity: 6.14888888888889,
		power: 184.107095074577,
		road: 1705.45296296296,
		acceleration: -0.122407407407407
	},
	{
		id: 230,
		time: 229,
		velocity: 7.01944444444444,
		power: 826.531616293095,
		road: 1711.96791666667,
		acceleration: -0.0178703703703693
	},
	{
		id: 231,
		time: 230,
		velocity: 6.34972222222222,
		power: 6359.35434065356,
		road: 1718.88037037037,
		acceleration: 0.81287037037037
	},
	{
		id: 232,
		time: 231,
		velocity: 8.5875,
		power: 6090.26608687405,
		road: 1726.53699074074,
		acceleration: 0.675462962962963
	},
	{
		id: 233,
		time: 232,
		velocity: 9.04583333333333,
		power: 8009.93793531931,
		road: 1734.94787037037,
		acceleration: 0.833055555555554
	},
	{
		id: 234,
		time: 233,
		velocity: 8.84888888888889,
		power: 1234.09266249335,
		road: 1743.76319444444,
		acceleration: -0.024166666666666
	},
	{
		id: 235,
		time: 234,
		velocity: 8.515,
		power: -305.523761442397,
		road: 1752.46296296296,
		acceleration: -0.206944444444446
	},
	{
		id: 236,
		time: 235,
		velocity: 8.425,
		power: -1435.96464090861,
		road: 1760.88601851852,
		acceleration: -0.346481481481481
	},
	{
		id: 237,
		time: 236,
		velocity: 7.80944444444444,
		power: -5339.71394590516,
		road: 1768.69564814815,
		acceleration: -0.880370370370369
	},
	{
		id: 238,
		time: 237,
		velocity: 5.87388888888889,
		power: -6970.22369532349,
		road: 1775.44555555556,
		acceleration: -1.23907407407407
	},
	{
		id: 239,
		time: 238,
		velocity: 4.70777777777778,
		power: -7182.16712881644,
		road: 1780.79796296296,
		acceleration: -1.55592592592593
	},
	{
		id: 240,
		time: 239,
		velocity: 3.14166666666667,
		power: -4951.94417648821,
		road: 1784.62259259259,
		acceleration: -1.49962962962963
	},
	{
		id: 241,
		time: 240,
		velocity: 1.375,
		power: -2818.23734133971,
		road: 1787.00972222222,
		acceleration: -1.37537037037037
	},
	{
		id: 242,
		time: 241,
		velocity: 0.581666666666667,
		power: -1024.21719107621,
		road: 1788.18555555556,
		acceleration: -1.04722222222222
	},
	{
		id: 243,
		time: 242,
		velocity: 0,
		power: -132.57386010884,
		road: 1788.60861111111,
		acceleration: -0.458333333333333
	},
	{
		id: 244,
		time: 243,
		velocity: 0,
		power: -6.09454005847953,
		road: 1788.70555555556,
		acceleration: -0.193888888888889
	},
	{
		id: 245,
		time: 244,
		velocity: 0,
		power: 0,
		road: 1788.70555555556,
		acceleration: 0
	},
	{
		id: 246,
		time: 245,
		velocity: 0,
		power: 0,
		road: 1788.70555555556,
		acceleration: 0
	},
	{
		id: 247,
		time: 246,
		velocity: 0,
		power: 0,
		road: 1788.70555555556,
		acceleration: 0
	},
	{
		id: 248,
		time: 247,
		velocity: 0,
		power: 0,
		road: 1788.70555555556,
		acceleration: 0
	},
	{
		id: 249,
		time: 248,
		velocity: 0,
		power: 0,
		road: 1788.70555555556,
		acceleration: 0
	},
	{
		id: 250,
		time: 249,
		velocity: 0,
		power: 0,
		road: 1788.70555555556,
		acceleration: 0
	},
	{
		id: 251,
		time: 250,
		velocity: 0,
		power: 0,
		road: 1788.70555555556,
		acceleration: 0
	},
	{
		id: 252,
		time: 251,
		velocity: 0,
		power: 0,
		road: 1788.70555555556,
		acceleration: 0
	},
	{
		id: 253,
		time: 252,
		velocity: 0,
		power: 356.340320911006,
		road: 1789.10842592593,
		acceleration: 0.805740740740741
	},
	{
		id: 254,
		time: 253,
		velocity: 2.41722222222222,
		power: 1704.61947991229,
		road: 1790.49712962963,
		acceleration: 1.16592592592593
	},
	{
		id: 255,
		time: 254,
		velocity: 3.49777777777778,
		power: 4620.49657042487,
		road: 1793.27819444444,
		acceleration: 1.6187962962963
	},
	{
		id: 256,
		time: 255,
		velocity: 4.85638888888889,
		power: 3157.82159422492,
		road: 1797.22217592593,
		acceleration: 0.707037037037037
	},
	{
		id: 257,
		time: 256,
		velocity: 4.53833333333333,
		power: 706.823414651948,
		road: 1801.53699074074,
		acceleration: 0.03462962962963
	},
	{
		id: 258,
		time: 257,
		velocity: 3.60166666666667,
		power: 1218.84184548732,
		road: 1805.94550925926,
		acceleration: 0.152777777777779
	},
	{
		id: 259,
		time: 258,
		velocity: 5.31472222222222,
		power: 4858.95248676253,
		road: 1810.87824074074,
		acceleration: 0.895648148148148
	},
	{
		id: 260,
		time: 259,
		velocity: 7.22527777777778,
		power: 11540.1783379331,
		road: 1817.15125,
		acceleration: 1.78490740740741
	},
	{
		id: 261,
		time: 260,
		velocity: 8.95638888888889,
		power: 15659.2330499366,
		road: 1825.2500462963,
		acceleration: 1.86666666666667
	},
	{
		id: 262,
		time: 261,
		velocity: 10.9147222222222,
		power: 16860.4086509134,
		road: 1835.09023148148,
		acceleration: 1.61611111111111
	},
	{
		id: 263,
		time: 262,
		velocity: 12.0736111111111,
		power: 12738.9445215186,
		road: 1846.23888888889,
		acceleration: 1.00083333333333
	},
	{
		id: 264,
		time: 263,
		velocity: 11.9588888888889,
		power: 5688.16388874672,
		road: 1858.03773148148,
		acceleration: 0.299537037037037
	},
	{
		id: 265,
		time: 264,
		velocity: 11.8133333333333,
		power: 4358.92543706668,
		road: 1870.07166666667,
		acceleration: 0.170648148148148
	},
	{
		id: 266,
		time: 265,
		velocity: 12.5855555555556,
		power: 8161.54603456497,
		road: 1882.43023148148,
		acceleration: 0.478611111111109
	},
	{
		id: 267,
		time: 266,
		velocity: 13.3947222222222,
		power: 10610.0545290173,
		road: 1895.34763888889,
		acceleration: 0.639074074074074
	},
	{
		id: 268,
		time: 267,
		velocity: 13.7305555555556,
		power: 8805.84744884794,
		road: 1908.81226851852,
		acceleration: 0.455370370370373
	},
	{
		id: 269,
		time: 268,
		velocity: 13.9516666666667,
		power: 7527.19229033294,
		road: 1922.67111111111,
		acceleration: 0.333055555555555
	},
	{
		id: 270,
		time: 269,
		velocity: 14.3938888888889,
		power: 7254.98127938804,
		road: 1936.84412037037,
		acceleration: 0.295277777777777
	},
	{
		id: 271,
		time: 270,
		velocity: 14.6163888888889,
		power: 6918.03413571895,
		road: 1951.29282407407,
		acceleration: 0.25611111111111
	},
	{
		id: 272,
		time: 271,
		velocity: 14.72,
		power: 5738.99020292644,
		road: 1965.95023148148,
		acceleration: 0.1612962962963
	},
	{
		id: 273,
		time: 272,
		velocity: 14.8777777777778,
		power: 8330.51716665588,
		road: 1980.85444444444,
		acceleration: 0.332314814814813
	},
	{
		id: 274,
		time: 273,
		velocity: 15.6133333333333,
		power: 13120.0986146037,
		road: 1996.24046296296,
		acceleration: 0.631296296296298
	},
	{
		id: 275,
		time: 274,
		velocity: 16.6138888888889,
		power: 15269.8478030174,
		road: 2012.30282407407,
		acceleration: 0.721388888888885
	},
	{
		id: 276,
		time: 275,
		velocity: 17.0419444444444,
		power: 17919.0959237444,
		road: 2029.13861111111,
		acceleration: 0.825462962962966
	},
	{
		id: 277,
		time: 276,
		velocity: 18.0897222222222,
		power: 18663.1430806652,
		road: 2046.78810185185,
		acceleration: 0.801944444444445
	},
	{
		id: 278,
		time: 277,
		velocity: 19.0197222222222,
		power: 18247.5970456131,
		road: 2065.19703703704,
		acceleration: 0.716944444444444
	},
	{
		id: 279,
		time: 278,
		velocity: 19.1927777777778,
		power: 9667.89217307585,
		road: 2084.06791666667,
		acceleration: 0.206944444444442
	},
	{
		id: 280,
		time: 279,
		velocity: 18.7105555555556,
		power: 4139.75235041722,
		road: 2102.99189814815,
		acceleration: -0.100740740740743
	},
	{
		id: 281,
		time: 280,
		velocity: 18.7175,
		power: 1334.6610008125,
		road: 2121.7399537037,
		acceleration: -0.251111111111108
	},
	{
		id: 282,
		time: 281,
		velocity: 18.4394444444444,
		power: 3097.764203807,
		road: 2140.28902777778,
		acceleration: -0.146851851851849
	},
	{
		id: 283,
		time: 282,
		velocity: 18.27,
		power: -820.550801411373,
		road: 2158.58324074074,
		acceleration: -0.36287037037037
	},
	{
		id: 284,
		time: 283,
		velocity: 17.6288888888889,
		power: 4245.28694462959,
		road: 2176.66287037037,
		acceleration: -0.0662962962963007
	},
	{
		id: 285,
		time: 284,
		velocity: 18.2405555555556,
		power: 4255.07205024642,
		road: 2194.67759259259,
		acceleration: -0.0635185185185172
	},
	{
		id: 286,
		time: 285,
		velocity: 18.0794444444444,
		power: 9257.48114922299,
		road: 2212.77185185185,
		acceleration: 0.222592592592591
	},
	{
		id: 287,
		time: 286,
		velocity: 18.2966666666667,
		power: 4382.11093822382,
		road: 2230.94657407407,
		acceleration: -0.0616666666666674
	},
	{
		id: 288,
		time: 287,
		velocity: 18.0555555555556,
		power: 4389.3673245801,
		road: 2249.06087962963,
		acceleration: -0.0591666666666626
	},
	{
		id: 289,
		time: 288,
		velocity: 17.9019444444444,
		power: 5824.05031253567,
		road: 2267.15773148148,
		acceleration: 0.0242592592592601
	},
	{
		id: 290,
		time: 289,
		velocity: 18.3694444444444,
		power: 7159.81258184278,
		road: 2285.31606481481,
		acceleration: 0.0987037037037055
	},
	{
		id: 291,
		time: 290,
		velocity: 18.3516666666667,
		power: 9965.81064457682,
		road: 2303.64925925926,
		acceleration: 0.251018518518517
	},
	{
		id: 292,
		time: 291,
		velocity: 18.655,
		power: 9829.74105931636,
		road: 2322.22342592593,
		acceleration: 0.230925925925924
	},
	{
		id: 293,
		time: 292,
		velocity: 19.0622222222222,
		power: 11181.3930764677,
		road: 2341.05935185185,
		acceleration: 0.292592592592595
	},
	{
		id: 294,
		time: 293,
		velocity: 19.2294444444444,
		power: 8643.59910926525,
		road: 2360.1124537037,
		acceleration: 0.141759259259256
	},
	{
		id: 295,
		time: 294,
		velocity: 19.0802777777778,
		power: 8131.06388990191,
		road: 2379.29046296296,
		acceleration: 0.108055555555556
	},
	{
		id: 296,
		time: 295,
		velocity: 19.3863888888889,
		power: 7258.10805938208,
		road: 2398.55101851852,
		acceleration: 0.0570370370370412
	},
	{
		id: 297,
		time: 296,
		velocity: 19.4005555555556,
		power: 11156.441542961,
		road: 2417.9700462963,
		acceleration: 0.259907407407404
	},
	{
		id: 298,
		time: 297,
		velocity: 19.86,
		power: 9135.0222883889,
		road: 2437.58990740741,
		acceleration: 0.14175925925926
	},
	{
		id: 299,
		time: 298,
		velocity: 19.8116666666667,
		power: 9931.09838223369,
		road: 2457.3687962963,
		acceleration: 0.176296296296297
	},
	{
		id: 300,
		time: 299,
		velocity: 19.9294444444444,
		power: 8082.0223832585,
		road: 2477.2724537037,
		acceleration: 0.0732407407407401
	},
	{
		id: 301,
		time: 300,
		velocity: 20.0797222222222,
		power: 8598.43239398903,
		road: 2497.26097222222,
		acceleration: 0.0964814814814829
	},
	{
		id: 302,
		time: 301,
		velocity: 20.1011111111111,
		power: 7246.88044874764,
		road: 2517.30944444445,
		acceleration: 0.0234259259259275
	},
	{
		id: 303,
		time: 302,
		velocity: 19.9997222222222,
		power: 6025.89852427682,
		road: 2537.34972222222,
		acceleration: -0.0398148148148181
	},
	{
		id: 304,
		time: 303,
		velocity: 19.9602777777778,
		power: 4705.74107588398,
		road: 2557.31708333333,
		acceleration: -0.106018518518521
	},
	{
		id: 305,
		time: 304,
		velocity: 19.7830555555556,
		power: 5419.51613256113,
		road: 2577.19865740741,
		acceleration: -0.0655555555555516
	},
	{
		id: 306,
		time: 305,
		velocity: 19.8030555555556,
		power: 4811.98897376774,
		road: 2597.00013888889,
		acceleration: -0.0946296296296332
	},
	{
		id: 307,
		time: 306,
		velocity: 19.6763888888889,
		power: 5817.88552633698,
		road: 2616.73476851852,
		acceleration: -0.0390740740740689
	},
	{
		id: 308,
		time: 307,
		velocity: 19.6658333333333,
		power: 6062.38669254447,
		road: 2636.43740740741,
		acceleration: -0.0249074074074116
	},
	{
		id: 309,
		time: 308,
		velocity: 19.7283333333333,
		power: 6387.80279565913,
		road: 2656.12407407407,
		acceleration: -0.00703703703703695
	},
	{
		id: 310,
		time: 309,
		velocity: 19.6552777777778,
		power: 7720.11737985888,
		road: 2675.83842592593,
		acceleration: 0.0624074074074095
	},
	{
		id: 311,
		time: 310,
		velocity: 19.8530555555556,
		power: 6136.37143253921,
		road: 2695.57287037037,
		acceleration: -0.0222222222222221
	},
	{
		id: 312,
		time: 311,
		velocity: 19.6616666666667,
		power: 8442.79132169956,
		road: 2715.34523148148,
		acceleration: 0.098055555555554
	},
	{
		id: 313,
		time: 312,
		velocity: 19.9494444444444,
		power: 7096.25187280346,
		road: 2735.17884259259,
		acceleration: 0.0244444444444447
	},
	{
		id: 314,
		time: 313,
		velocity: 19.9263888888889,
		power: 6454.3042718021,
		road: 2755.01986111111,
		acceleration: -0.00962962962962877
	},
	{
		id: 315,
		time: 314,
		velocity: 19.6327777777778,
		power: 2355.33925908744,
		road: 2774.74509259259,
		acceleration: -0.221944444444443
	},
	{
		id: 316,
		time: 315,
		velocity: 19.2836111111111,
		power: 6636.63440124104,
		road: 2794.3637962963,
		acceleration: 0.00888888888888673
	},
	{
		id: 317,
		time: 316,
		velocity: 19.9530555555556,
		power: 7245.95387980693,
		road: 2814.00708333333,
		acceleration: 0.0402777777777779
	},
	{
		id: 318,
		time: 317,
		velocity: 19.7536111111111,
		power: 17762.2794267867,
		road: 2833.95921296296,
		acceleration: 0.577407407407406
	},
	{
		id: 319,
		time: 318,
		velocity: 21.0158333333333,
		power: 8592.61670387537,
		road: 2854.24152777778,
		acceleration: 0.0829629629629665
	},
	{
		id: 320,
		time: 319,
		velocity: 20.2019444444444,
		power: 9821.72452884011,
		road: 2874.63574074074,
		acceleration: 0.140833333333333
	},
	{
		id: 321,
		time: 320,
		velocity: 20.1761111111111,
		power: 1334.78454498965,
		road: 2894.95467592593,
		acceleration: -0.291388888888889
	},
	{
		id: 322,
		time: 321,
		velocity: 20.1416666666667,
		power: 6620.23798123462,
		road: 2915.12097222222,
		acceleration: -0.0138888888888893
	},
	{
		id: 323,
		time: 322,
		velocity: 20.1602777777778,
		power: 7297.71307190354,
		road: 2935.29083333333,
		acceleration: 0.0210185185185168
	},
	{
		id: 324,
		time: 323,
		velocity: 20.2391666666667,
		power: 7479.25721921709,
		road: 2955.48587962963,
		acceleration: 0.0293518518518532
	},
	{
		id: 325,
		time: 324,
		velocity: 20.2297222222222,
		power: 7241.21708766576,
		road: 2975.70365740741,
		acceleration: 0.0161111111111083
	},
	{
		id: 326,
		time: 325,
		velocity: 20.2086111111111,
		power: 7752.95119368799,
		road: 2995.95013888889,
		acceleration: 0.0412962962962986
	},
	{
		id: 327,
		time: 326,
		velocity: 20.3630555555556,
		power: 6903.73013303487,
		road: 3016.21564814815,
		acceleration: -0.00324074074073977
	},
	{
		id: 328,
		time: 327,
		velocity: 20.22,
		power: 6943.37569464797,
		road: 3036.47898148148,
		acceleration: -0.00111111111111128
	},
	{
		id: 329,
		time: 328,
		velocity: 20.2052777777778,
		power: 6344.89667507083,
		road: 3056.72611111111,
		acceleration: -0.031296296296297
	},
	{
		id: 330,
		time: 329,
		velocity: 20.2691666666667,
		power: 5312.97288537057,
		road: 3076.91638888889,
		acceleration: -0.0824074074074055
	},
	{
		id: 331,
		time: 330,
		velocity: 19.9727777777778,
		power: 6295.54618851495,
		road: 3097.05074074074,
		acceleration: -0.0294444444444473
	},
	{
		id: 332,
		time: 331,
		velocity: 20.1169444444444,
		power: 5768.16379567879,
		road: 3117.14277777778,
		acceleration: -0.0551851851851843
	},
	{
		id: 333,
		time: 332,
		velocity: 20.1036111111111,
		power: 8357.90746239596,
		road: 3137.24662037037,
		acceleration: 0.0787962962962965
	},
	{
		id: 334,
		time: 333,
		velocity: 20.2091666666667,
		power: 6496.96580386757,
		road: 3157.38037037037,
		acceleration: -0.0189814814814824
	},
	{
		id: 335,
		time: 334,
		velocity: 20.06,
		power: 5706.12402122753,
		road: 3177.47537037037,
		acceleration: -0.0585185185185182
	},
	{
		id: 336,
		time: 335,
		velocity: 19.9280555555556,
		power: 5850.57544096435,
		road: 3197.51662037037,
		acceleration: -0.04898148148148
	},
	{
		id: 337,
		time: 336,
		velocity: 20.0622222222222,
		power: 7419.62758555802,
		road: 3217.54990740741,
		acceleration: 0.0330555555555527
	},
	{
		id: 338,
		time: 337,
		velocity: 20.1591666666667,
		power: 4691.12441831676,
		road: 3237.54583333334,
		acceleration: -0.107777777777777
	},
	{
		id: 339,
		time: 338,
		velocity: 19.6047222222222,
		power: -3029.18032963452,
		road: 3257.23467592593,
		acceleration: -0.506388888888889
	},
	{
		id: 340,
		time: 339,
		velocity: 18.5430555555556,
		power: -4455.33275802567,
		road: 3276.38185185185,
		acceleration: -0.576944444444447
	},
	{
		id: 341,
		time: 340,
		velocity: 18.4283333333333,
		power: -3546.16583785342,
		road: 3294.97981481482,
		acceleration: -0.52148148148148
	},
	{
		id: 342,
		time: 341,
		velocity: 18.0402777777778,
		power: -894.417411584482,
		road: 3313.13472222222,
		acceleration: -0.364629629629629
	},
	{
		id: 343,
		time: 342,
		velocity: 17.4491666666667,
		power: -6391.32308556214,
		road: 3330.76657407408,
		acceleration: -0.68148148148148
	},
	{
		id: 344,
		time: 343,
		velocity: 16.3838888888889,
		power: -4631.83190444459,
		road: 3347.77009259259,
		acceleration: -0.575185185185184
	},
	{
		id: 345,
		time: 344,
		velocity: 16.3147222222222,
		power: -6432.32340469844,
		road: 3364.1412962963,
		acceleration: -0.689444444444444
	},
	{
		id: 346,
		time: 345,
		velocity: 15.3808333333333,
		power: -1447.66578018703,
		road: 3379.98550925926,
		acceleration: -0.364537037037039
	},
	{
		id: 347,
		time: 346,
		velocity: 15.2902777777778,
		power: -3040.87128505211,
		road: 3395.41351851852,
		acceleration: -0.46787037037037
	},
	{
		id: 348,
		time: 347,
		velocity: 14.9111111111111,
		power: -5730.79364473493,
		road: 3410.27981481482,
		acceleration: -0.655555555555553
	},
	{
		id: 349,
		time: 348,
		velocity: 13.4141666666667,
		power: -9993.57732921587,
		road: 3424.32643518519,
		acceleration: -0.983796296296298
	},
	{
		id: 350,
		time: 349,
		velocity: 12.3388888888889,
		power: -13992.0828770495,
		road: 3437.20120370371,
		acceleration: -1.35990740740741
	},
	{
		id: 351,
		time: 350,
		velocity: 10.8313888888889,
		power: -9118.41339624337,
		road: 3448.88458333333,
		acceleration: -1.02287037037037
	},
	{
		id: 352,
		time: 351,
		velocity: 10.3455555555556,
		power: -6259.4878250599,
		road: 3459.65523148148,
		acceleration: -0.802592592592593
	},
	{
		id: 353,
		time: 352,
		velocity: 9.93111111111111,
		power: -2976.45495048672,
		road: 3469.77768518519,
		acceleration: -0.493796296296296
	},
	{
		id: 354,
		time: 353,
		velocity: 9.35,
		power: -3003.49173904623,
		road: 3479.39958333333,
		acceleration: -0.507314814814816
	},
	{
		id: 355,
		time: 354,
		velocity: 8.82361111111111,
		power: -1580.0947465283,
		road: 3488.5900462963,
		acceleration: -0.355555555555554
	},
	{
		id: 356,
		time: 355,
		velocity: 8.86444444444444,
		power: 348.020036045499,
		road: 3497.53689814815,
		acceleration: -0.131666666666668
	},
	{
		id: 357,
		time: 356,
		velocity: 8.955,
		power: 2987.56294706065,
		road: 3506.50643518519,
		acceleration: 0.177037037037037
	},
	{
		id: 358,
		time: 357,
		velocity: 9.35472222222222,
		power: 5487.87838091469,
		road: 3515.78694444445,
		acceleration: 0.444907407407408
	},
	{
		id: 359,
		time: 358,
		velocity: 10.1991666666667,
		power: 7565.10134663101,
		road: 3525.60347222222,
		acceleration: 0.627129629629628
	},
	{
		id: 360,
		time: 359,
		velocity: 10.8363888888889,
		power: 9155.36083461231,
		road: 3536.09652777778,
		acceleration: 0.725925925925928
	},
	{
		id: 361,
		time: 360,
		velocity: 11.5325,
		power: 10488.2523995356,
		road: 3547.34222222222,
		acceleration: 0.779351851851853
	},
	{
		id: 362,
		time: 361,
		velocity: 12.5372222222222,
		power: 9737.19949438156,
		road: 3559.30037037037,
		acceleration: 0.645555555555553
	},
	{
		id: 363,
		time: 362,
		velocity: 12.7730555555556,
		power: 11325.9365811541,
		road: 3571.94194444445,
		acceleration: 0.721296296296295
	},
	{
		id: 364,
		time: 363,
		velocity: 13.6963888888889,
		power: 11527.4775862455,
		road: 3585.28282407408,
		acceleration: 0.677314814814816
	},
	{
		id: 365,
		time: 364,
		velocity: 14.5691666666667,
		power: 12099.8733479514,
		road: 3599.29537037037,
		acceleration: 0.66601851851852
	},
	{
		id: 366,
		time: 365,
		velocity: 14.7711111111111,
		power: 13719.6897470168,
		road: 3614.00435185185,
		acceleration: 0.726851851851849
	},
	{
		id: 367,
		time: 366,
		velocity: 15.8769444444444,
		power: 12647.8744018174,
		road: 3629.37685185185,
		acceleration: 0.600185185185188
	},
	{
		id: 368,
		time: 367,
		velocity: 16.3697222222222,
		power: 12210.941873666,
		road: 3645.31481481482,
		acceleration: 0.53074074074074
	},
	{
		id: 369,
		time: 368,
		velocity: 16.3633333333333,
		power: 4182.44501379187,
		road: 3661.51546296296,
		acceleration: -0.0053703703703718
	},
	{
		id: 370,
		time: 369,
		velocity: 15.8608333333333,
		power: 883.654290173232,
		road: 3677.60546296296,
		acceleration: -0.215925925925925
	},
	{
		id: 371,
		time: 370,
		velocity: 15.7219444444444,
		power: -405.361605332765,
		road: 3693.43972222222,
		acceleration: -0.295555555555557
	},
	{
		id: 372,
		time: 371,
		velocity: 15.4766666666667,
		power: 3846.60286051533,
		road: 3709.12148148148,
		acceleration: -0.00944444444444237
	},
	{
		id: 373,
		time: 372,
		velocity: 15.8325,
		power: 5356.13861927327,
		road: 3724.84333333334,
		acceleration: 0.0896296296296288
	},
	{
		id: 374,
		time: 373,
		velocity: 15.9908333333333,
		power: 7692.19476383436,
		road: 3740.72842592593,
		acceleration: 0.236851851851853
	},
	{
		id: 375,
		time: 374,
		velocity: 16.1872222222222,
		power: 8219.26321689066,
		road: 3756.8612962963,
		acceleration: 0.258703703703706
	},
	{
		id: 376,
		time: 375,
		velocity: 16.6086111111111,
		power: 10307.7173823291,
		road: 3773.31115740741,
		acceleration: 0.375277777777775
	},
	{
		id: 377,
		time: 376,
		velocity: 17.1166666666667,
		power: 11324.7637657114,
		road: 3790.15638888889,
		acceleration: 0.415462962962962
	},
	{
		id: 378,
		time: 377,
		velocity: 17.4336111111111,
		power: 9130.9945468454,
		road: 3807.34013888889,
		acceleration: 0.261574074074076
	},
	{
		id: 379,
		time: 378,
		velocity: 17.3933333333333,
		power: 6267.32992830082,
		road: 3824.69481481482,
		acceleration: 0.080277777777777
	},
	{
		id: 380,
		time: 379,
		velocity: 17.3575,
		power: 8587.86751837188,
		road: 3842.19601851852,
		acceleration: 0.212777777777777
	},
	{
		id: 381,
		time: 380,
		velocity: 18.0719444444444,
		power: 8126.860682057,
		road: 3859.89162037037,
		acceleration: 0.176018518518518
	},
	{
		id: 382,
		time: 381,
		velocity: 17.9213888888889,
		power: 9057.72458735678,
		road: 3877.78574074074,
		acceleration: 0.22101851851852
	},
	{
		id: 383,
		time: 382,
		velocity: 18.0205555555556,
		power: 5429.91898704489,
		road: 3895.79282407408,
		acceleration: 0.00490740740740492
	},
	{
		id: 384,
		time: 383,
		velocity: 18.0866666666667,
		power: 6761.60238562819,
		road: 3913.8425462963,
		acceleration: 0.0803703703703711
	},
	{
		id: 385,
		time: 384,
		velocity: 18.1625,
		power: 3752.08755872941,
		road: 3931.88564814815,
		acceleration: -0.0936111111111089
	},
	{
		id: 386,
		time: 385,
		velocity: 17.7397222222222,
		power: 4385.9738332753,
		road: 3949.85476851852,
		acceleration: -0.0543518518518518
	},
	{
		id: 387,
		time: 386,
		velocity: 17.9236111111111,
		power: 4463.60347233763,
		road: 3967.77268518519,
		acceleration: -0.0480555555555533
	},
	{
		id: 388,
		time: 387,
		velocity: 18.0183333333333,
		power: 8161.19237909846,
		road: 3985.74898148148,
		acceleration: 0.164814814814811
	},
	{
		id: 389,
		time: 388,
		velocity: 18.2341666666667,
		power: 7755.56491219597,
		road: 4003.87490740741,
		acceleration: 0.134444444444448
	},
	{
		id: 390,
		time: 389,
		velocity: 18.3269444444444,
		power: 7048.34805667593,
		road: 4022.1125,
		acceleration: 0.0888888888888886
	},
	{
		id: 391,
		time: 390,
		velocity: 18.285,
		power: 6336.12886893913,
		road: 4040.41722222222,
		acceleration: 0.0453703703703674
	},
	{
		id: 392,
		time: 391,
		velocity: 18.3702777777778,
		power: 6584.24281760127,
		road: 4058.77333333334,
		acceleration: 0.0574074074074069
	},
	{
		id: 393,
		time: 392,
		velocity: 18.4991666666667,
		power: 8901.26103959734,
		road: 4077.24986111111,
		acceleration: 0.183425925925924
	},
	{
		id: 394,
		time: 393,
		velocity: 18.8352777777778,
		power: 12766.2390574138,
		road: 4096.01055555556,
		acceleration: 0.384907407407411
	},
	{
		id: 395,
		time: 394,
		velocity: 19.525,
		power: 15641.1256521997,
		road: 4115.22106481482,
		acceleration: 0.514722222222225
	},
	{
		id: 396,
		time: 395,
		velocity: 20.0433333333333,
		power: 21991.4948253048,
		road: 4135.09152777778,
		acceleration: 0.805185185185184
	},
	{
		id: 397,
		time: 396,
		velocity: 21.2508333333333,
		power: 16942.871290844,
		road: 4155.61296296297,
		acceleration: 0.49675925925926
	},
	{
		id: 398,
		time: 397,
		velocity: 21.0152777777778,
		power: 16142.3643898711,
		road: 4176.5962962963,
		acceleration: 0.427037037037032
	},
	{
		id: 399,
		time: 398,
		velocity: 21.3244444444444,
		power: 7059.21664436741,
		road: 4197.77694444445,
		acceleration: -0.0324074074074048
	},
	{
		id: 400,
		time: 399,
		velocity: 21.1536111111111,
		power: 8340.9842131508,
		road: 4218.95675925926,
		acceleration: 0.0307407407407396
	},
	{
		id: 401,
		time: 400,
		velocity: 21.1075,
		power: 10134.1558311292,
		road: 4240.20972222222,
		acceleration: 0.115555555555556
	},
	{
		id: 402,
		time: 401,
		velocity: 21.6711111111111,
		power: -7707.10791290335,
		road: 4261.14134259259,
		acceleration: -0.758240740740742
	},
	{
		id: 403,
		time: 402,
		velocity: 18.8788888888889,
		power: -2773.99801459555,
		road: 4281.44277777778,
		acceleration: -0.502129629629628
	},
	{
		id: 404,
		time: 403,
		velocity: 19.6011111111111,
		power: -7094.47474620487,
		road: 4301.13217592593,
		acceleration: -0.721944444444446
	},
	{
		id: 405,
		time: 404,
		velocity: 19.5052777777778,
		power: 6424.81411195859,
		road: 4320.46509259259,
		acceleration: 0.00898148148148437
	},
	{
		id: 406,
		time: 405,
		velocity: 18.9058333333333,
		power: 1581.14944044315,
		road: 4339.67777777778,
		acceleration: -0.249444444444446
	},
	{
		id: 407,
		time: 406,
		velocity: 18.8527777777778,
		power: 2279.90325869582,
		road: 4358.66319444445,
		acceleration: -0.205092592592592
	},
	{
		id: 408,
		time: 407,
		velocity: 18.89,
		power: 4917.35031163315,
		road: 4377.51837962963,
		acceleration: -0.055370370370369
	},
	{
		id: 409,
		time: 408,
		velocity: 18.7397222222222,
		power: 13133.4196939421,
		road: 4396.54069444445,
		acceleration: 0.389629629629628
	},
	{
		id: 410,
		time: 409,
		velocity: 20.0216666666667,
		power: 7877.98338390464,
		road: 4415.80310185185,
		acceleration: 0.0905555555555573
	},
	{
		id: 411,
		time: 410,
		velocity: 19.1616666666667,
		power: 9731.63191063435,
		road: 4435.20291666667,
		acceleration: 0.18425925925926
	},
	{
		id: 412,
		time: 411,
		velocity: 19.2925,
		power: 1146.60132119546,
		road: 4454.55657407408,
		acceleration: -0.276574074074077
	},
	{
		id: 413,
		time: 412,
		velocity: 19.1919444444444,
		power: 6334.80164764216,
		road: 4473.77625,
		acceleration: 0.00861111111111512
	},
	{
		id: 414,
		time: 413,
		velocity: 19.1875,
		power: 5345.96321864186,
		road: 4492.97800925926,
		acceleration: -0.0444444444444478
	},
	{
		id: 415,
		time: 414,
		velocity: 19.1591666666667,
		power: 6261.81377399485,
		road: 4512.16060185185,
		acceleration: 0.00611111111111029
	},
	{
		id: 416,
		time: 415,
		velocity: 19.2102777777778,
		power: 4314.15944518839,
		road: 4531.29708333333,
		acceleration: -0.0983333333333327
	},
	{
		id: 417,
		time: 416,
		velocity: 18.8925,
		power: 1972.46348059955,
		road: 4550.27351851852,
		acceleration: -0.221759259259258
	},
	{
		id: 418,
		time: 417,
		velocity: 18.4938888888889,
		power: -1833.22774312048,
		road: 4568.92601851852,
		acceleration: -0.426111111111108
	},
	{
		id: 419,
		time: 418,
		velocity: 17.9319444444444,
		power: -1349.03815806955,
		road: 4587.16935185185,
		acceleration: -0.392222222222227
	},
	{
		id: 420,
		time: 419,
		velocity: 17.7158333333333,
		power: -2377.12800655633,
		road: 4604.99365740741,
		acceleration: -0.445833333333333
	},
	{
		id: 421,
		time: 420,
		velocity: 17.1563888888889,
		power: -1459.09070127093,
		road: 4622.40212962963,
		acceleration: -0.385833333333334
	},
	{
		id: 422,
		time: 421,
		velocity: 16.7744444444444,
		power: -814.057506012149,
		road: 4639.44712962963,
		acceleration: -0.341111111111108
	},
	{
		id: 423,
		time: 422,
		velocity: 16.6925,
		power: -94.9935614936139,
		road: 4656.17601851852,
		acceleration: -0.291111111111114
	},
	{
		id: 424,
		time: 423,
		velocity: 16.2830555555556,
		power: 1808.22251702768,
		road: 4672.67620370371,
		acceleration: -0.166296296296295
	},
	{
		id: 425,
		time: 424,
		velocity: 16.2755555555556,
		power: 2866.04417589492,
		road: 4689.04555555556,
		acceleration: -0.0953703703703717
	},
	{
		id: 426,
		time: 425,
		velocity: 16.4063888888889,
		power: 2599.77790385101,
		road: 4705.3125,
		acceleration: -0.109444444444446
	},
	{
		id: 427,
		time: 426,
		velocity: 15.9547222222222,
		power: 404.68755891434,
		road: 4721.40120370371,
		acceleration: -0.247037037037035
	},
	{
		id: 428,
		time: 427,
		velocity: 15.5344444444444,
		power: -2026.79528539012,
		road: 4737.16537037037,
		acceleration: -0.402037037037038
	},
	{
		id: 429,
		time: 428,
		velocity: 15.2002777777778,
		power: -1361.85864939934,
		road: 4752.55175925926,
		acceleration: -0.353518518518516
	},
	{
		id: 430,
		time: 429,
		velocity: 14.8941666666667,
		power: -1090.88964132685,
		road: 4767.59592592593,
		acceleration: -0.330925925925925
	},
	{
		id: 431,
		time: 430,
		velocity: 14.5416666666667,
		power: -891.96761418828,
		road: 4782.31810185185,
		acceleration: -0.313055555555557
	},
	{
		id: 432,
		time: 431,
		velocity: 14.2611111111111,
		power: -5844.19477674537,
		road: 4796.54791666667,
		acceleration: -0.671666666666667
	},
	{
		id: 433,
		time: 432,
		velocity: 12.8791666666667,
		power: -4437.36146506566,
		road: 4810.15518518519,
		acceleration: -0.573425925925925
	},
	{
		id: 434,
		time: 433,
		velocity: 12.8213888888889,
		power: -1458.13493340046,
		road: 4823.30518518519,
		acceleration: -0.341111111111111
	},
	{
		id: 435,
		time: 434,
		velocity: 13.2377777777778,
		power: 5679.18545350955,
		road: 4836.39976851852,
		acceleration: 0.230277777777776
	},
	{
		id: 436,
		time: 435,
		velocity: 13.57,
		power: 3728.6393830316,
		road: 4849.6437962963,
		acceleration: 0.0686111111111138
	},
	{
		id: 437,
		time: 436,
		velocity: 13.0272222222222,
		power: -668.187797445241,
		road: 4862.78310185185,
		acceleration: -0.278055555555557
	},
	{
		id: 438,
		time: 437,
		velocity: 12.4036111111111,
		power: -2816.07434996969,
		road: 4875.55814814815,
		acceleration: -0.450462962962963
	},
	{
		id: 439,
		time: 438,
		velocity: 12.2186111111111,
		power: -1145.56922394853,
		road: 4887.9525,
		acceleration: -0.310925925925925
	},
	{
		id: 440,
		time: 439,
		velocity: 12.0944444444444,
		power: 585.768642870315,
		road: 4900.11125,
		acceleration: -0.160277777777779
	},
	{
		id: 441,
		time: 440,
		velocity: 11.9227777777778,
		power: -201.771451223999,
		road: 4912.07694444445,
		acceleration: -0.225833333333334
	},
	{
		id: 442,
		time: 441,
		velocity: 11.5411111111111,
		power: -1211.24102832729,
		road: 4923.77314814815,
		acceleration: -0.313148148148146
	},
	{
		id: 443,
		time: 442,
		velocity: 11.155,
		power: -725.705213771137,
		road: 4935.17902777778,
		acceleration: -0.2675
	},
	{
		id: 444,
		time: 443,
		velocity: 11.1202777777778,
		power: -814.722601685204,
		road: 4946.31412037037,
		acceleration: -0.274074074074075
	},
	{
		id: 445,
		time: 444,
		velocity: 10.7188888888889,
		power: 56.8040020893097,
		road: 4957.21768518519,
		acceleration: -0.188981481481481
	},
	{
		id: 446,
		time: 445,
		velocity: 10.5880555555556,
		power: -656.299531059016,
		road: 4967.89865740741,
		acceleration: -0.256203703703703
	},
	{
		id: 447,
		time: 446,
		velocity: 10.3516666666667,
		power: -1685.72768610486,
		road: 4978.27222222222,
		acceleration: -0.358611111111111
	},
	{
		id: 448,
		time: 447,
		velocity: 9.64305555555556,
		power: -5330.2628580819,
		road: 4988.09064814815,
		acceleration: -0.751666666666669
	},
	{
		id: 449,
		time: 448,
		velocity: 8.33305555555556,
		power: -6657.54748165758,
		road: 4997.05694444445,
		acceleration: -0.952592592592591
	},
	{
		id: 450,
		time: 449,
		velocity: 7.49388888888889,
		power: -6688.79442436057,
		road: 5005.02421296296,
		acceleration: -1.04546296296296
	},
	{
		id: 451,
		time: 450,
		velocity: 6.50666666666667,
		power: -6453.49195823446,
		road: 5011.89810185185,
		acceleration: -1.1412962962963
	},
	{
		id: 452,
		time: 451,
		velocity: 4.90916666666667,
		power: -4684.32864139347,
		road: 5017.70365740741,
		acceleration: -0.995370370370371
	},
	{
		id: 453,
		time: 452,
		velocity: 4.50777777777778,
		power: -4176.59729112241,
		road: 5022.48120370371,
		acceleration: -1.06064814814815
	},
	{
		id: 454,
		time: 453,
		velocity: 3.32472222222222,
		power: -3372.15921618686,
		road: 5026.18074074074,
		acceleration: -1.09537037037037
	},
	{
		id: 455,
		time: 454,
		velocity: 1.62305555555556,
		power: -3123.63707016913,
		road: 5028.5812962963,
		acceleration: -1.50259259259259
	},
	{
		id: 456,
		time: 455,
		velocity: 0,
		power: -1017.31337730024,
		road: 5029.67643518519,
		acceleration: -1.10824074074074
	},
	{
		id: 457,
		time: 456,
		velocity: 0,
		power: -105.96550060104,
		road: 5029.94694444445,
		acceleration: -0.541018518518518
	},
	{
		id: 458,
		time: 457,
		velocity: 0,
		power: 0,
		road: 5029.94694444445,
		acceleration: 0
	},
	{
		id: 459,
		time: 458,
		velocity: 0,
		power: 0,
		road: 5029.94694444445,
		acceleration: 0
	},
	{
		id: 460,
		time: 459,
		velocity: 0,
		power: 0,
		road: 5029.94694444445,
		acceleration: 0
	},
	{
		id: 461,
		time: 460,
		velocity: 0,
		power: 550.92691532353,
		road: 5030.4550925926,
		acceleration: 1.0162962962963
	},
	{
		id: 462,
		time: 461,
		velocity: 3.04888888888889,
		power: 2442.22804253772,
		road: 5032.16138888889,
		acceleration: 1.38
	},
	{
		id: 463,
		time: 462,
		velocity: 4.14,
		power: 6520.83289447647,
		road: 5035.51486111111,
		acceleration: 1.91435185185185
	},
	{
		id: 464,
		time: 463,
		velocity: 5.74305555555556,
		power: 5235.59327338672,
		road: 5040.32777777778,
		acceleration: 1.00453703703704
	},
	{
		id: 465,
		time: 464,
		velocity: 6.0625,
		power: 3954.66558003837,
		road: 5045.94101851852,
		acceleration: 0.596111111111111
	},
	{
		id: 466,
		time: 465,
		velocity: 5.92833333333333,
		power: 3192.56901073142,
		road: 5052.05282407408,
		acceleration: 0.401018518518518
	},
	{
		id: 467,
		time: 466,
		velocity: 6.94611111111111,
		power: 7555.64639954775,
		road: 5058.87087962963,
		acceleration: 1.01148148148148
	},
	{
		id: 468,
		time: 467,
		velocity: 9.09694444444444,
		power: 13500.8296120898,
		road: 5066.98620370371,
		acceleration: 1.58305555555556
	},
	{
		id: 469,
		time: 468,
		velocity: 10.6775,
		power: 15630.7483913904,
		road: 5076.65166666667,
		acceleration: 1.51722222222222
	},
	{
		id: 470,
		time: 469,
		velocity: 11.4977777777778,
		power: 10238.4457335365,
		road: 5087.47513888889,
		acceleration: 0.798796296296295
	},
	{
		id: 471,
		time: 470,
		velocity: 11.4933333333333,
		power: 11248.164647563,
		road: 5099.10328703704,
		acceleration: 0.810555555555554
	},
	{
		id: 472,
		time: 471,
		velocity: 13.1091666666667,
		power: 12771.1218022252,
		road: 5111.56615740741,
		acceleration: 0.85888888888889
	},
	{
		id: 473,
		time: 472,
		velocity: 14.0744444444444,
		power: 21096.3063234299,
		road: 5125.15555555556,
		acceleration: 1.39416666666667
	},
	{
		id: 474,
		time: 473,
		velocity: 15.6758333333333,
		power: 19289.2563464256,
		road: 5139.9963425926,
		acceleration: 1.10861111111111
	},
	{
		id: 475,
		time: 474,
		velocity: 16.435,
		power: 23981.3013612621,
		road: 5156.03694444445,
		acceleration: 1.29101851851852
	},
	{
		id: 476,
		time: 475,
		velocity: 17.9475,
		power: 24455.0522808104,
		road: 5173.31481481482,
		acceleration: 1.18351851851851
	},
	{
		id: 477,
		time: 476,
		velocity: 19.2263888888889,
		power: 23007.6558350303,
		road: 5191.67995370371,
		acceleration: 0.991018518518519
	},
	{
		id: 478,
		time: 477,
		velocity: 19.4080555555556,
		power: 15276.3421018398,
		road: 5210.7912962963,
		acceleration: 0.50138888888889
	},
	{
		id: 479,
		time: 478,
		velocity: 19.4516666666667,
		power: 6723.06989282496,
		road: 5230.1650462963,
		acceleration: 0.0234259259259275
	},
	{
		id: 480,
		time: 479,
		velocity: 19.2966666666667,
		power: 4776.50120688481,
		road: 5249.51027777778,
		acceleration: -0.0804629629629616
	},
	{
		id: 481,
		time: 480,
		velocity: 19.1666666666667,
		power: 3017.65501534845,
		road: 5268.72949074074,
		acceleration: -0.171574074074076
	},
	{
		id: 482,
		time: 481,
		velocity: 18.9369444444444,
		power: 1757.68337396481,
		road: 5287.74560185185,
		acceleration: -0.23462962962963
	},
	{
		id: 483,
		time: 482,
		velocity: 18.5927777777778,
		power: 425.434425669423,
		road: 5306.49351851852,
		acceleration: -0.30175925925926
	},
	{
		id: 484,
		time: 483,
		velocity: 18.2613888888889,
		power: 1275.43214120151,
		road: 5324.96675925926,
		acceleration: -0.247592592592596
	},
	{
		id: 485,
		time: 484,
		velocity: 18.1941666666667,
		power: 1895.76193958778,
		road: 5343.21296296297,
		acceleration: -0.206481481481479
	},
	{
		id: 486,
		time: 485,
		velocity: 17.9733333333333,
		power: 2196.78185869455,
		road: 5361.26398148148,
		acceleration: -0.183888888888891
	},
	{
		id: 487,
		time: 486,
		velocity: 17.7097222222222,
		power: -253.423720918561,
		road: 5379.06263888889,
		acceleration: -0.320833333333329
	},
	{
		id: 488,
		time: 487,
		velocity: 17.2316666666667,
		power: -106.700263594338,
		road: 5396.54782407408,
		acceleration: -0.306111111111111
	},
	{
		id: 489,
		time: 488,
		velocity: 17.055,
		power: -1522.10804515998,
		road: 5413.68703703704,
		acceleration: -0.385833333333334
	},
	{
		id: 490,
		time: 489,
		velocity: 16.5522222222222,
		power: 577.397529025626,
		road: 5430.50782407408,
		acceleration: -0.251018518518517
	},
	{
		id: 491,
		time: 490,
		velocity: 16.4786111111111,
		power: 60.0121269707972,
		road: 5447.06402777778,
		acceleration: -0.278148148148148
	},
	{
		id: 492,
		time: 491,
		velocity: 16.2205555555556,
		power: 2622.03694616079,
		road: 5463.42578703704,
		acceleration: -0.110740740740741
	},
	{
		id: 493,
		time: 492,
		velocity: 16.22,
		power: 2898.17352761045,
		road: 5479.68712962963,
		acceleration: -0.0900925925925939
	},
	{
		id: 494,
		time: 493,
		velocity: 16.2083333333333,
		power: 3624.85535690448,
		road: 5495.88282407408,
		acceleration: -0.0412037037037045
	},
	{
		id: 495,
		time: 494,
		velocity: 16.0969444444444,
		power: 3496.41584645235,
		road: 5512.03388888889,
		acceleration: -0.0480555555555533
	},
	{
		id: 496,
		time: 495,
		velocity: 16.0758333333333,
		power: 5228.48598469278,
		road: 5528.19282407408,
		acceleration: 0.0637962962962959
	},
	{
		id: 497,
		time: 496,
		velocity: 16.3997222222222,
		power: 6555.04133735495,
		road: 5544.45615740741,
		acceleration: 0.145000000000003
	},
	{
		id: 498,
		time: 497,
		velocity: 16.5319444444444,
		power: 7462.13748532537,
		road: 5560.88958333334,
		acceleration: 0.195185185185181
	},
	{
		id: 499,
		time: 498,
		velocity: 16.6613888888889,
		power: 7167.94354227645,
		road: 5577.50462962963,
		acceleration: 0.168055555555558
	},
	{
		id: 500,
		time: 499,
		velocity: 16.9038888888889,
		power: 4976.43433546221,
		road: 5594.21694444445,
		acceleration: 0.0264814814814827
	},
	{
		id: 501,
		time: 500,
		velocity: 16.6113888888889,
		power: 5036.17832777465,
		road: 5610.95708333334,
		acceleration: 0.029166666666665
	},
	{
		id: 502,
		time: 501,
		velocity: 16.7488888888889,
		power: 4325.45654077889,
		road: 5627.70407407408,
		acceleration: -0.0154629629629639
	},
	{
		id: 503,
		time: 502,
		velocity: 16.8575,
		power: 4114.7996760115,
		road: 5644.42939814815,
		acceleration: -0.0278703703703727
	},
	{
		id: 504,
		time: 503,
		velocity: 16.5277777777778,
		power: 3278.04663879453,
		road: 5661.10152777778,
		acceleration: -0.0785185185185178
	},
	{
		id: 505,
		time: 504,
		velocity: 16.5133333333333,
		power: 346.783964496491,
		road: 5677.60490740741,
		acceleration: -0.258981481481481
	},
	{
		id: 506,
		time: 505,
		velocity: 16.0805555555556,
		power: -722.626419422973,
		road: 5693.81768518519,
		acceleration: -0.322222222222223
	},
	{
		id: 507,
		time: 506,
		velocity: 15.5611111111111,
		power: -1663.47025851019,
		road: 5709.67986111111,
		acceleration: -0.37898148148148
	},
	{
		id: 508,
		time: 507,
		velocity: 15.3763888888889,
		power: 107.924728889406,
		road: 5725.22435185185,
		acceleration: -0.256388888888891
	},
	{
		id: 509,
		time: 508,
		velocity: 15.3113888888889,
		power: 3391.29710196956,
		road: 5740.62527777778,
		acceleration: -0.0307407407407396
	},
	{
		id: 510,
		time: 509,
		velocity: 15.4688888888889,
		power: 4258.78403955237,
		road: 5756.02495370371,
		acceleration: 0.0282407407407419
	},
	{
		id: 511,
		time: 510,
		velocity: 15.4611111111111,
		power: 4087.69411950112,
		road: 5771.44666666667,
		acceleration: 0.0158333333333331
	},
	{
		id: 512,
		time: 511,
		velocity: 15.3588888888889,
		power: 3601.52936890875,
		road: 5786.86773148148,
		acceleration: -0.0171296296296308
	},
	{
		id: 513,
		time: 512,
		velocity: 15.4175,
		power: 1639.16406687156,
		road: 5802.20611111111,
		acceleration: -0.148240740740743
	},
	{
		id: 514,
		time: 513,
		velocity: 15.0163888888889,
		power: 3453.68507564503,
		road: 5817.45953703704,
		acceleration: -0.0216666666666647
	},
	{
		id: 515,
		time: 514,
		velocity: 15.2938888888889,
		power: 4915.29737441817,
		road: 5832.74087962963,
		acceleration: 0.0775000000000006
	},
	{
		id: 516,
		time: 515,
		velocity: 15.65,
		power: 9067.00642737515,
		road: 5848.23569444445,
		acceleration: 0.349444444444444
	},
	{
		id: 517,
		time: 516,
		velocity: 16.0647222222222,
		power: 9945.9475958334,
		road: 5864.09851851852,
		acceleration: 0.386574074074074
	},
	{
		id: 518,
		time: 517,
		velocity: 16.4536111111111,
		power: 8645.05683914628,
		road: 5880.29606481482,
		acceleration: 0.282870370370368
	},
	{
		id: 519,
		time: 518,
		velocity: 16.4986111111111,
		power: 7537.52333278132,
		road: 5896.73490740741,
		acceleration: 0.199722222222221
	},
	{
		id: 520,
		time: 519,
		velocity: 16.6638888888889,
		power: 6115.41846976385,
		road: 5913.3250462963,
		acceleration: 0.102870370370375
	},
	{
		id: 521,
		time: 520,
		velocity: 16.7622222222222,
		power: 3925.13114526236,
		road: 5929.94847222223,
		acceleration: -0.0362962962962996
	},
	{
		id: 522,
		time: 521,
		velocity: 16.3897222222222,
		power: 764.255869937579,
		road: 5946.43763888889,
		acceleration: -0.232222222222219
	},
	{
		id: 523,
		time: 522,
		velocity: 15.9672222222222,
		power: -1084.11054057244,
		road: 5962.63800925926,
		acceleration: -0.345370370370372
	},
	{
		id: 524,
		time: 523,
		velocity: 15.7261111111111,
		power: 541.425026453568,
		road: 5978.54842592593,
		acceleration: -0.234537037037036
	},
	{
		id: 525,
		time: 524,
		velocity: 15.6861111111111,
		power: 451.255736233956,
		road: 5994.22365740741,
		acceleration: -0.235833333333334
	},
	{
		id: 526,
		time: 525,
		velocity: 15.2597222222222,
		power: 2868.57321962321,
		road: 6009.74601851852,
		acceleration: -0.0699074074074062
	},
	{
		id: 527,
		time: 526,
		velocity: 15.5163888888889,
		power: 2722.36130804684,
		road: 6025.19462962963,
		acceleration: -0.0775925925925929
	},
	{
		id: 528,
		time: 527,
		velocity: 15.4533333333333,
		power: 6304.13122834437,
		road: 6040.68597222222,
		acceleration: 0.163055555555554
	},
	{
		id: 529,
		time: 528,
		velocity: 15.7488888888889,
		power: 5019.46298947303,
		road: 6056.29467592593,
		acceleration: 0.071666666666669
	},
	{
		id: 530,
		time: 529,
		velocity: 15.7313888888889,
		power: 4807.04352277559,
		road: 6071.96671296297,
		acceleration: 0.0549999999999997
	},
	{
		id: 531,
		time: 530,
		velocity: 15.6183333333333,
		power: 4089.35952880527,
		road: 6087.66925925926,
		acceleration: 0.00601851851851798
	},
	{
		id: 532,
		time: 531,
		velocity: 15.7669444444444,
		power: 6150.0174663865,
		road: 6103.44486111111,
		acceleration: 0.140092592592593
	},
	{
		id: 533,
		time: 532,
		velocity: 16.1516666666667,
		power: 7856.72785618417,
		road: 6119.41226851852,
		acceleration: 0.243518518518519
	},
	{
		id: 534,
		time: 533,
		velocity: 16.3488888888889,
		power: 8688.45432539435,
		road: 6135.64337962963,
		acceleration: 0.283888888888889
	},
	{
		id: 535,
		time: 534,
		velocity: 16.6186111111111,
		power: 9175.33145351523,
		road: 6152.16611111111,
		acceleration: 0.299351851851853
	},
	{
		id: 536,
		time: 535,
		velocity: 17.0497222222222,
		power: 7355.59808982111,
		road: 6168.9250925926,
		acceleration: 0.173148148148144
	},
	{
		id: 537,
		time: 536,
		velocity: 16.8683333333333,
		power: 6063.38550940397,
		road: 6185.81421296297,
		acceleration: 0.0871296296296329
	},
	{
		id: 538,
		time: 537,
		velocity: 16.88,
		power: 3576.51761157172,
		road: 6202.71333333334,
		acceleration: -0.0671296296296298
	},
	{
		id: 539,
		time: 538,
		velocity: 16.8483333333333,
		power: 7327.02485155029,
		road: 6219.66023148148,
		acceleration: 0.162685185185182
	},
	{
		id: 540,
		time: 539,
		velocity: 17.3563888888889,
		power: 8813.72048724202,
		road: 6236.81046296297,
		acceleration: 0.243981481481484
	},
	{
		id: 541,
		time: 540,
		velocity: 17.6119444444444,
		power: 11648.0518946509,
		road: 6254.2813425926,
		acceleration: 0.397314814814813
	},
	{
		id: 542,
		time: 541,
		velocity: 18.0402777777778,
		power: 10862.2522443059,
		road: 6272.11574074074,
		acceleration: 0.329722222222223
	},
	{
		id: 543,
		time: 542,
		velocity: 18.3455555555556,
		power: 9920.64778703383,
		road: 6290.24453703704,
		acceleration: 0.259074074074071
	},
	{
		id: 544,
		time: 543,
		velocity: 18.3891666666667,
		power: 7490.26293259617,
		road: 6308.55828703704,
		acceleration: 0.110833333333332
	},
	{
		id: 545,
		time: 544,
		velocity: 18.3727777777778,
		power: 5255.78496483266,
		road: 6326.91828703704,
		acceleration: -0.0183333333333309
	},
	{
		id: 546,
		time: 545,
		velocity: 18.2905555555556,
		power: 5061.73620652585,
		road: 6345.25486111111,
		acceleration: -0.0285185185185171
	},
	{
		id: 547,
		time: 546,
		velocity: 18.3036111111111,
		power: 5210.4201583438,
		road: 6363.5675925926,
		acceleration: -0.0191666666666634
	},
	{
		id: 548,
		time: 547,
		velocity: 18.3152777777778,
		power: 5783.15649642463,
		road: 6381.8775462963,
		acceleration: 0.013611111111107
	},
	{
		id: 549,
		time: 548,
		velocity: 18.3313888888889,
		power: 5066.36664720713,
		road: 6400.18078703704,
		acceleration: -0.027037037037033
	},
	{
		id: 550,
		time: 549,
		velocity: 18.2225,
		power: 4049.37750094398,
		road: 6418.42888888889,
		acceleration: -0.0832407407407416
	},
	{
		id: 551,
		time: 550,
		velocity: 18.0655555555556,
		power: 5827.97404652386,
		road: 6436.64527777778,
		acceleration: 0.0198148148148114
	},
	{
		id: 552,
		time: 551,
		velocity: 18.3908333333333,
		power: 6209.17071598851,
		road: 6454.89180555556,
		acceleration: 0.0404629629629625
	},
	{
		id: 553,
		time: 552,
		velocity: 18.3438888888889,
		power: 4994.89260254631,
		road: 6473.14393518519,
		acceleration: -0.0292592592592591
	},
	{
		id: 554,
		time: 553,
		velocity: 17.9777777777778,
		power: 5249.43930143649,
		road: 6491.37449074074,
		acceleration: -0.0138888888888857
	},
	{
		id: 555,
		time: 554,
		velocity: 18.3491666666667,
		power: 4553.72397846694,
		road: 6509.57180555556,
		acceleration: -0.0525925925925925
	},
	{
		id: 556,
		time: 555,
		velocity: 18.1861111111111,
		power: 6702.13669265606,
		road: 6527.77800925926,
		acceleration: 0.0703703703703695
	},
	{
		id: 557,
		time: 556,
		velocity: 18.1888888888889,
		power: 6629.53988111921,
		road: 6546.05111111111,
		acceleration: 0.0634259259259267
	},
	{
		id: 558,
		time: 557,
		velocity: 18.5394444444444,
		power: 7138.94635089089,
		road: 6564.40055555556,
		acceleration: 0.0892592592592578
	},
	{
		id: 559,
		time: 558,
		velocity: 18.4538888888889,
		power: 3880.67279122308,
		road: 6582.74657407408,
		acceleration: -0.0961111111111137
	},
	{
		id: 560,
		time: 559,
		velocity: 17.9005555555556,
		power: 4212.15367892091,
		road: 6601.00736111111,
		acceleration: -0.0743518518518513
	},
	{
		id: 561,
		time: 560,
		velocity: 18.3163888888889,
		power: 4511.05867783522,
		road: 6619.20347222222,
		acceleration: -0.0549999999999997
	},
	{
		id: 562,
		time: 561,
		velocity: 18.2888888888889,
		power: 7781.14197814677,
		road: 6637.43759259259,
		acceleration: 0.13101851851852
	},
	{
		id: 563,
		time: 562,
		velocity: 18.2936111111111,
		power: 4839.27349226669,
		road: 6655.71763888889,
		acceleration: -0.039166666666663
	},
	{
		id: 564,
		time: 563,
		velocity: 18.1988888888889,
		power: 5043.59122338126,
		road: 6673.9649537037,
		acceleration: -0.026296296296298
	},
	{
		id: 565,
		time: 564,
		velocity: 18.21,
		power: 4947.9194572835,
		road: 6692.18375,
		acceleration: -0.0307407407407396
	},
	{
		id: 566,
		time: 565,
		velocity: 18.2013888888889,
		power: 4620.13039686186,
		road: 6710.36310185185,
		acceleration: -0.0481481481481545
	},
	{
		id: 567,
		time: 566,
		velocity: 18.0544444444444,
		power: 6391.23953728222,
		road: 6728.54513888889,
		acceleration: 0.0535185185185227
	},
	{
		id: 568,
		time: 567,
		velocity: 18.3705555555556,
		power: 5405.07711252367,
		road: 6746.75189814815,
		acceleration: -0.0040740740740759
	},
	{
		id: 569,
		time: 568,
		velocity: 18.1891666666667,
		power: 6050.70541730834,
		road: 6764.97282407408,
		acceleration: 0.0324074074074119
	},
	{
		id: 570,
		time: 569,
		velocity: 18.1516666666667,
		power: 4209.06008539697,
		road: 6783.1737037037,
		acceleration: -0.0725000000000016
	},
	{
		id: 571,
		time: 570,
		velocity: 18.1530555555556,
		power: 4935.25529793011,
		road: 6801.32384259259,
		acceleration: -0.0289814814814804
	},
	{
		id: 572,
		time: 571,
		velocity: 18.1022222222222,
		power: 3522.10361695677,
		road: 6819.40541666667,
		acceleration: -0.10814814814815
	},
	{
		id: 573,
		time: 572,
		velocity: 17.8272222222222,
		power: 7100.26840442863,
		road: 6837.48231481482,
		acceleration: 0.0987962962962996
	},
	{
		id: 574,
		time: 573,
		velocity: 18.4494444444444,
		power: 6111.36088254756,
		road: 6855.62805555556,
		acceleration: 0.0388888888888843
	},
	{
		id: 575,
		time: 574,
		velocity: 18.2188888888889,
		power: 8054.51958429696,
		road: 6873.86648148148,
		acceleration: 0.146481481481484
	},
	{
		id: 576,
		time: 575,
		velocity: 18.2666666666667,
		power: 3999.619755747,
		road: 6892.13476851852,
		acceleration: -0.0867592592592636
	},
	{
		id: 577,
		time: 576,
		velocity: 18.1891666666667,
		power: 5746.60944418393,
		road: 6910.36694444445,
		acceleration: 0.0145370370370408
	},
	{
		id: 578,
		time: 577,
		velocity: 18.2625,
		power: 5678.96780237068,
		road: 6928.61148148148,
		acceleration: 0.0101851851851826
	},
	{
		id: 579,
		time: 578,
		velocity: 18.2972222222222,
		power: 5725.40695890636,
		road: 6946.86731481482,
		acceleration: 0.0124074074074088
	},
	{
		id: 580,
		time: 579,
		velocity: 18.2263888888889,
		power: 5179.68013852652,
		road: 6965.12,
		acceleration: -0.0187037037037001
	},
	{
		id: 581,
		time: 580,
		velocity: 18.2063888888889,
		power: 5427.79434732283,
		road: 6983.3612962963,
		acceleration: -0.0040740740740759
	},
	{
		id: 582,
		time: 581,
		velocity: 18.285,
		power: 6147.29147874502,
		road: 7001.6187962963,
		acceleration: 0.0364814814814807
	},
	{
		id: 583,
		time: 582,
		velocity: 18.3358333333333,
		power: 7595.10818056043,
		road: 7019.9525,
		acceleration: 0.115925925925925
	},
	{
		id: 584,
		time: 583,
		velocity: 18.5541666666667,
		power: 2790.40134252254,
		road: 7038.26555555556,
		acceleration: -0.15722222222222
	},
	{
		id: 585,
		time: 584,
		velocity: 17.8133333333333,
		power: 2599.18360344688,
		road: 7056.41824074074,
		acceleration: -0.163518518518519
	},
	{
		id: 586,
		time: 585,
		velocity: 17.8452777777778,
		power: -2271.94361322173,
		road: 7074.26916666667,
		acceleration: -0.440000000000001
	},
	{
		id: 587,
		time: 586,
		velocity: 17.2341666666667,
		power: 2464.53101044918,
		road: 7091.82291666667,
		acceleration: -0.15435185185185
	},
	{
		id: 588,
		time: 587,
		velocity: 17.3502777777778,
		power: 2234.3126438214,
		road: 7109.21763888889,
		acceleration: -0.163703703703703
	},
	{
		id: 589,
		time: 588,
		velocity: 17.3541666666667,
		power: 6123.41468144022,
		road: 7126.56643518519,
		acceleration: 0.0718518518518501
	},
	{
		id: 590,
		time: 589,
		velocity: 17.4497222222222,
		power: 7156.95928125712,
		road: 7144.01597222222,
		acceleration: 0.12962962962963
	},
	{
		id: 591,
		time: 590,
		velocity: 17.7391666666667,
		power: 8447.03781333867,
		road: 7161.62976851852,
		acceleration: 0.198888888888888
	},
	{
		id: 592,
		time: 591,
		velocity: 17.9508333333333,
		power: 8622.25441673599,
		road: 7179.44277777778,
		acceleration: 0.199537037037036
	},
	{
		id: 593,
		time: 592,
		velocity: 18.0483333333333,
		power: 8083.41065989317,
		road: 7197.43532407408,
		acceleration: 0.15953703703704
	},
	{
		id: 594,
		time: 593,
		velocity: 18.2177777777778,
		power: 6078.85381796115,
		road: 7215.52722222222,
		acceleration: 0.039166666666663
	},
	{
		id: 595,
		time: 594,
		velocity: 18.0683333333333,
		power: 5138.750985986,
		road: 7233.63092592593,
		acceleration: -0.0155555555555544
	},
	{
		id: 596,
		time: 595,
		velocity: 18.0016666666667,
		power: 4001.01713330522,
		road: 7251.68703703704,
		acceleration: -0.0796296296296291
	},
	{
		id: 597,
		time: 596,
		velocity: 17.9788888888889,
		power: 2253.191583804,
		road: 7269.61472222222,
		acceleration: -0.177222222222223
	},
	{
		id: 598,
		time: 597,
		velocity: 17.5366666666667,
		power: 2811.85698701675,
		road: 7287.3837962963,
		acceleration: -0.140000000000001
	},
	{
		id: 599,
		time: 598,
		velocity: 17.5816666666667,
		power: 2557.10662647258,
		road: 7305.00745370371,
		acceleration: -0.150833333333335
	},
	{
		id: 600,
		time: 599,
		velocity: 17.5263888888889,
		power: 5613.25601902374,
		road: 7322.57203703704,
		acceleration: 0.032685185185187
	},
	{
		id: 601,
		time: 600,
		velocity: 17.6347222222222,
		power: 4446.4473392944,
		road: 7340.13462962963,
		acceleration: -0.0366666666666653
	},
	{
		id: 602,
		time: 601,
		velocity: 17.4716666666667,
		power: 3779.46455403529,
		road: 7357.64162037037,
		acceleration: -0.0745370370370395
	},
	{
		id: 603,
		time: 602,
		velocity: 17.3027777777778,
		power: -140.18918013897,
		road: 7374.95888888889,
		acceleration: -0.304907407407406
	},
	{
		id: 604,
		time: 603,
		velocity: 16.72,
		power: 2107.32644819283,
		road: 7392.04222222222,
		acceleration: -0.162962962962965
	},
	{
		id: 605,
		time: 604,
		velocity: 16.9827777777778,
		power: 946.976256065218,
		road: 7408.92930555556,
		acceleration: -0.229537037037034
	},
	{
		id: 606,
		time: 605,
		velocity: 16.6141666666667,
		power: 4090.72594772713,
		road: 7425.68638888889,
		acceleration: -0.0304629629629645
	},
	{
		id: 607,
		time: 606,
		velocity: 16.6286111111111,
		power: -450.848917747817,
		road: 7442.27277777778,
		acceleration: -0.310925925925929
	},
	{
		id: 608,
		time: 607,
		velocity: 16.05,
		power: 4633.29835166298,
		road: 7458.71120370371,
		acceleration: 0.0150000000000006
	},
	{
		id: 609,
		time: 608,
		velocity: 16.6591666666667,
		power: 2040.9635519545,
		road: 7475.08305555556,
		acceleration: -0.148148148148145
	},
	{
		id: 610,
		time: 609,
		velocity: 16.1841666666667,
		power: 9668.20551367382,
		road: 7491.54773148148,
		acceleration: 0.333796296296292
	},
	{
		id: 611,
		time: 610,
		velocity: 17.0513888888889,
		power: 8916.06823889686,
		road: 7508.31435185185,
		acceleration: 0.270092592592597
	},
	{
		id: 612,
		time: 611,
		velocity: 17.4694444444444,
		power: 4905.56902093447,
		road: 7525.22337962963,
		acceleration: 0.0147222222222183
	},
	{
		id: 613,
		time: 612,
		velocity: 16.2283333333333,
		power: 11524.809708768,
		road: 7542.34523148148,
		acceleration: 0.410925925925927
	},
	{
		id: 614,
		time: 613,
		velocity: 18.2841666666667,
		power: 9694.65942830644,
		road: 7559.81287037037,
		acceleration: 0.280648148148149
	},
	{
		id: 615,
		time: 614,
		velocity: 18.3113888888889,
		power: 20849.6301641058,
		road: 7577.86842592593,
		acceleration: 0.895185185185184
	},
	{
		id: 616,
		time: 615,
		velocity: 18.9138888888889,
		power: 9540.13450112365,
		road: 7596.47800925926,
		acceleration: 0.212870370370375
	},
	{
		id: 617,
		time: 616,
		velocity: 18.9227777777778,
		power: 12386.239351706,
		road: 7615.37203703704,
		acceleration: 0.356018518518518
	},
	{
		id: 618,
		time: 617,
		velocity: 19.3794444444444,
		power: 10524.4886103601,
		road: 7634.56291666667,
		acceleration: 0.237685185185185
	},
	{
		id: 619,
		time: 618,
		velocity: 19.6269444444444,
		power: 13527.875357946,
		road: 7654.06388888889,
		acceleration: 0.3825
	},
	{
		id: 620,
		time: 619,
		velocity: 20.0702777777778,
		power: 14799.4891539815,
		road: 7673.9687962963,
		acceleration: 0.425370370370366
	},
	{
		id: 621,
		time: 620,
		velocity: 20.6555555555556,
		power: 14299.6648969695,
		road: 7694.27398148148,
		acceleration: 0.375185185185188
	},
	{
		id: 622,
		time: 621,
		velocity: 20.7525,
		power: 7257.36794011562,
		road: 7714.76949074074,
		acceleration: 0.00546296296296234
	},
	{
		id: 623,
		time: 622,
		velocity: 20.0866666666667,
		power: 1848.15742758708,
		road: 7735.13458333333,
		acceleration: -0.2662962962963
	},
	{
		id: 624,
		time: 623,
		velocity: 19.8566666666667,
		power: -3599.93659448673,
		road: 7755.09648148148,
		acceleration: -0.54009259259259
	},
	{
		id: 625,
		time: 624,
		velocity: 19.1322222222222,
		power: -1999.48919387952,
		road: 7774.56425925926,
		acceleration: -0.44814814814815
	},
	{
		id: 626,
		time: 625,
		velocity: 18.7422222222222,
		power: -3574.07167536125,
		road: 7793.54444444444,
		acceleration: -0.527037037037033
	},
	{
		id: 627,
		time: 626,
		velocity: 18.2755555555556,
		power: -2132.85973400483,
		road: 7812.04078703704,
		acceleration: -0.440648148148153
	},
	{
		id: 628,
		time: 627,
		velocity: 17.8102777777778,
		power: -1158.72359824808,
		road: 7830.1274537037,
		acceleration: -0.3787037037037
	},
	{
		id: 629,
		time: 628,
		velocity: 17.6061111111111,
		power: -1472.04120956216,
		road: 7847.82930555556,
		acceleration: -0.390925925925927
	},
	{
		id: 630,
		time: 629,
		velocity: 17.1027777777778,
		power: -711.548004871291,
		road: 7865.16583333333,
		acceleration: -0.339722222222221
	},
	{
		id: 631,
		time: 630,
		velocity: 16.7911111111111,
		power: -377.408996763888,
		road: 7882.17564814815,
		acceleration: -0.313703703703705
	},
	{
		id: 632,
		time: 631,
		velocity: 16.665,
		power: 1223.36524796877,
		road: 7898.92402777778,
		acceleration: -0.209166666666665
	},
	{
		id: 633,
		time: 632,
		velocity: 16.4752777777778,
		power: -232.459046524293,
		road: 7915.42004629629,
		acceleration: -0.295555555555559
	},
	{
		id: 634,
		time: 633,
		velocity: 15.9044444444444,
		power: -1494.24665816467,
		road: 7931.58259259259,
		acceleration: -0.371388888888889
	},
	{
		id: 635,
		time: 634,
		velocity: 15.5508333333333,
		power: -1360.81159830575,
		road: 7947.38032407407,
		acceleration: -0.35824074074074
	},
	{
		id: 636,
		time: 635,
		velocity: 15.4005555555556,
		power: 384.171347250453,
		road: 7962.88046296296,
		acceleration: -0.236944444444443
	},
	{
		id: 637,
		time: 636,
		velocity: 15.1936111111111,
		power: 686.704224388579,
		road: 7978.15615740741,
		acceleration: -0.211944444444441
	},
	{
		id: 638,
		time: 637,
		velocity: 14.915,
		power: -41.6006397513304,
		road: 7993.19694444444,
		acceleration: -0.257870370370373
	},
	{
		id: 639,
		time: 638,
		velocity: 14.6269444444444,
		power: -20.3279559744241,
		road: 8007.98273148148,
		acceleration: -0.25212962962963
	},
	{
		id: 640,
		time: 639,
		velocity: 14.4372222222222,
		power: 3560.58899119277,
		road: 8022.64523148148,
		acceleration: 0.00555555555555465
	},
	{
		id: 641,
		time: 640,
		velocity: 14.9316666666667,
		power: 6743.62392787436,
		road: 8037.42402777778,
		acceleration: 0.227037037037039
	},
	{
		id: 642,
		time: 641,
		velocity: 15.3080555555556,
		power: 8040.22926242412,
		road: 8052.46847222222,
		acceleration: 0.304259259259258
	},
	{
		id: 643,
		time: 642,
		velocity: 15.35,
		power: 4751.42471424045,
		road: 8067.69916666667,
		acceleration: 0.068240740740741
	},
	{
		id: 644,
		time: 643,
		velocity: 15.1363888888889,
		power: 2684.25567465239,
		road: 8082.92712962963,
		acceleration: -0.0737037037037034
	},
	{
		id: 645,
		time: 644,
		velocity: 15.0869444444444,
		power: 2439.39825279558,
		road: 8098.07412037037,
		acceleration: -0.0882407407407406
	},
	{
		id: 646,
		time: 645,
		velocity: 15.0852777777778,
		power: 4896.80840328871,
		road: 8113.21782407407,
		acceleration: 0.081666666666667
	},
	{
		id: 647,
		time: 646,
		velocity: 15.3813888888889,
		power: 6743.94431863868,
		road: 8128.50361111111,
		acceleration: 0.202500000000001
	},
	{
		id: 648,
		time: 647,
		velocity: 15.6944444444444,
		power: 8134.22225255564,
		road: 8144.03296296296,
		acceleration: 0.284629629629629
	},
	{
		id: 649,
		time: 648,
		velocity: 15.9391666666667,
		power: 9250.28924085456,
		road: 8159.87555555556,
		acceleration: 0.341851851851853
	},
	{
		id: 650,
		time: 649,
		velocity: 16.4069444444444,
		power: 8301.98829051407,
		road: 8176.02078703704,
		acceleration: 0.263425925925922
	},
	{
		id: 651,
		time: 650,
		velocity: 16.4847222222222,
		power: 9941.87865658543,
		road: 8192.47365740741,
		acceleration: 0.351851851851855
	},
	{
		id: 652,
		time: 651,
		velocity: 16.9947222222222,
		power: 8215.28868510604,
		road: 8209.21625,
		acceleration: 0.22759259259259
	},
	{
		id: 653,
		time: 652,
		velocity: 17.0897222222222,
		power: 7797.90845930886,
		road: 8226.16837962963,
		acceleration: 0.191481481481485
	},
	{
		id: 654,
		time: 653,
		velocity: 17.0591666666667,
		power: 4826.2929701714,
		road: 8243.21856481482,
		acceleration: 0.00462962962962621
	},
	{
		id: 655,
		time: 654,
		velocity: 17.0086111111111,
		power: 3647.00673379321,
		road: 8260.23773148148,
		acceleration: -0.0666666666666664
	},
	{
		id: 656,
		time: 655,
		velocity: 16.8897222222222,
		power: 3679.06058593022,
		road: 8277.19226851852,
		acceleration: -0.0625925925925941
	},
	{
		id: 657,
		time: 656,
		velocity: 16.8713888888889,
		power: 3766.22282927508,
		road: 8294.08787037037,
		acceleration: -0.0552777777777749
	},
	{
		id: 658,
		time: 657,
		velocity: 16.8427777777778,
		power: 3094.87201592534,
		road: 8310.90856481482,
		acceleration: -0.0945370370370391
	},
	{
		id: 659,
		time: 658,
		velocity: 16.6061111111111,
		power: 3069.81468710661,
		road: 8327.63537037037,
		acceleration: -0.0932407407407396
	},
	{
		id: 660,
		time: 659,
		velocity: 16.5916666666667,
		power: 3421.22783677626,
		road: 8344.28120370371,
		acceleration: -0.0687037037037044
	},
	{
		id: 661,
		time: 660,
		velocity: 16.6366666666667,
		power: 4393.56606586942,
		road: 8360.88953703704,
		acceleration: -0.00629629629629846
	},
	{
		id: 662,
		time: 661,
		velocity: 16.5872222222222,
		power: 4517.22919855109,
		road: 8377.49550925926,
		acceleration: 0.00157407407407817
	},
	{
		id: 663,
		time: 662,
		velocity: 16.5963888888889,
		power: 4051.43570444694,
		road: 8394.08861111111,
		acceleration: -0.0273148148148152
	},
	{
		id: 664,
		time: 663,
		velocity: 16.5547222222222,
		power: 4102.28337874316,
		road: 8410.65643518519,
		acceleration: -0.0232407407407393
	},
	{
		id: 665,
		time: 664,
		velocity: 16.5175,
		power: 1952.4423661615,
		road: 8427.13435185185,
		acceleration: -0.156574074074076
	},
	{
		id: 666,
		time: 665,
		velocity: 16.1266666666667,
		power: -249.586050192919,
		road: 8443.38777777778,
		acceleration: -0.292407407407406
	},
	{
		id: 667,
		time: 666,
		velocity: 15.6775,
		power: -4439.89163560175,
		road: 8459.21393518519,
		acceleration: -0.562129629629633
	},
	{
		id: 668,
		time: 667,
		velocity: 14.8311111111111,
		power: -8668.88601971805,
		road: 8474.33078703704,
		acceleration: -0.856481481481481
	},
	{
		id: 669,
		time: 668,
		velocity: 13.5572222222222,
		power: -12465.118406509,
		road: 8488.43685185185,
		acceleration: -1.16509259259259
	},
	{
		id: 670,
		time: 669,
		velocity: 12.1822222222222,
		power: -9485.84182114193,
		road: 8501.46736111111,
		acceleration: -0.98601851851852
	},
	{
		id: 671,
		time: 670,
		velocity: 11.8730555555556,
		power: -5622.11224901368,
		road: 8513.6575,
		acceleration: -0.694722222222222
	},
	{
		id: 672,
		time: 671,
		velocity: 11.4730555555556,
		power: -7211.59186783677,
		road: 8525.06842592593,
		acceleration: -0.863703703703706
	},
	{
		id: 673,
		time: 672,
		velocity: 9.59111111111111,
		power: -9241.28502695098,
		road: 8535.48777777778,
		acceleration: -1.11944444444444
	},
	{
		id: 674,
		time: 673,
		velocity: 8.51472222222222,
		power: -9613.16516317844,
		road: 8544.7125,
		acceleration: -1.26981481481481
	},
	{
		id: 675,
		time: 674,
		velocity: 7.66361111111111,
		power: -6727.55602026382,
		road: 8552.78222222222,
		acceleration: -1.04018518518519
	},
	{
		id: 676,
		time: 675,
		velocity: 6.47055555555556,
		power: -5222.77852843136,
		road: 8559.86643518519,
		acceleration: -0.930833333333333
	},
	{
		id: 677,
		time: 676,
		velocity: 5.72222222222222,
		power: -5303.50620573587,
		road: 8565.95273148148,
		acceleration: -1.065
	},
	{
		id: 678,
		time: 677,
		velocity: 4.46861111111111,
		power: -3687.10302936781,
		road: 8571.05518518519,
		acceleration: -0.902685185185186
	},
	{
		id: 679,
		time: 678,
		velocity: 3.7625,
		power: -4176.01257397621,
		road: 8575.09337962963,
		acceleration: -1.22583333333333
	},
	{
		id: 680,
		time: 679,
		velocity: 2.04472222222222,
		power: -2518.84728121992,
		road: 8577.99513888889,
		acceleration: -1.04703703703704
	},
	{
		id: 681,
		time: 680,
		velocity: 1.3275,
		power: -1867.87915831889,
		road: 8579.7462962963,
		acceleration: -1.25416666666667
	},
	{
		id: 682,
		time: 681,
		velocity: 0,
		power: -411.051461804742,
		road: 8580.52958333333,
		acceleration: -0.681574074074074
	},
	{
		id: 683,
		time: 682,
		velocity: 0,
		power: -66.0193697368421,
		road: 8580.75083333333,
		acceleration: -0.4425
	},
	{
		id: 684,
		time: 683,
		velocity: 0,
		power: 0,
		road: 8580.75083333333,
		acceleration: 0
	},
	{
		id: 685,
		time: 684,
		velocity: 0,
		power: 0,
		road: 8580.75083333333,
		acceleration: 0
	},
	{
		id: 686,
		time: 685,
		velocity: 0,
		power: 0,
		road: 8580.75083333333,
		acceleration: 0
	},
	{
		id: 687,
		time: 686,
		velocity: 0,
		power: 0,
		road: 8580.75083333333,
		acceleration: 0
	},
	{
		id: 688,
		time: 687,
		velocity: 0,
		power: 106.669376710141,
		road: 8580.95833333333,
		acceleration: 0.415
	},
	{
		id: 689,
		time: 688,
		velocity: 1.245,
		power: 542.080806039296,
		road: 8581.69671296296,
		acceleration: 0.646759259259259
	},
	{
		id: 690,
		time: 689,
		velocity: 1.94027777777778,
		power: 2263.92002578083,
		road: 8583.3962037037,
		acceleration: 1.27546296296296
	},
	{
		id: 691,
		time: 690,
		velocity: 3.82638888888889,
		power: 5091.85069088918,
		road: 8586.52435185185,
		acceleration: 1.58185185185185
	},
	{
		id: 692,
		time: 691,
		velocity: 5.99055555555556,
		power: 6960.52715188635,
		road: 8591.16333333333,
		acceleration: 1.43981481481481
	},
	{
		id: 693,
		time: 692,
		velocity: 6.25972222222222,
		power: 7975.24321168623,
		road: 8597.14912037037,
		acceleration: 1.2537962962963
	},
	{
		id: 694,
		time: 693,
		velocity: 7.58777777777778,
		power: 6508.62174040342,
		road: 8604.17138888889,
		acceleration: 0.819166666666667
	},
	{
		id: 695,
		time: 694,
		velocity: 8.44805555555556,
		power: 11007.2564744327,
		road: 8612.23796296296,
		acceleration: 1.26944444444444
	},
	{
		id: 696,
		time: 695,
		velocity: 10.0680555555556,
		power: 11672.5634278147,
		road: 8621.51199074074,
		acceleration: 1.14546296296296
	},
	{
		id: 697,
		time: 696,
		velocity: 11.0241666666667,
		power: 11935.7597097453,
		road: 8631.86935185185,
		acceleration: 1.0212037037037
	},
	{
		id: 698,
		time: 697,
		velocity: 11.5116666666667,
		power: 6691.73196308355,
		road: 8642.95550925926,
		acceleration: 0.436388888888887
	},
	{
		id: 699,
		time: 698,
		velocity: 11.3772222222222,
		power: 2933.68107932242,
		road: 8654.29555555555,
		acceleration: 0.0713888888888885
	},
	{
		id: 700,
		time: 699,
		velocity: 11.2383333333333,
		power: 1684.03115953284,
		road: 8665.64902777778,
		acceleration: -0.0445370370370366
	},
	{
		id: 701,
		time: 700,
		velocity: 11.3780555555556,
		power: 3644.45299318503,
		road: 8677.0475462963,
		acceleration: 0.134629629629629
	},
	{
		id: 702,
		time: 701,
		velocity: 11.7811111111111,
		power: 4930.50974096715,
		road: 8688.63490740741,
		acceleration: 0.243055555555557
	},
	{
		id: 703,
		time: 702,
		velocity: 11.9675,
		power: 5727.3023794866,
		road: 8700.49361111111,
		acceleration: 0.299629629629631
	},
	{
		id: 704,
		time: 703,
		velocity: 12.2769444444444,
		power: 7796.85053175585,
		road: 8712.73013888889,
		acceleration: 0.456018518518519
	},
	{
		id: 705,
		time: 704,
		velocity: 13.1491666666667,
		power: 9308.8270269679,
		road: 8725.46810185185,
		acceleration: 0.54685185185185
	},
	{
		id: 706,
		time: 705,
		velocity: 13.6080555555556,
		power: 10252.1430657582,
		road: 8738.76958333333,
		acceleration: 0.580185185185186
	},
	{
		id: 707,
		time: 706,
		velocity: 14.0175,
		power: 7987.72120433054,
		road: 8752.5475,
		acceleration: 0.372685185185183
	},
	{
		id: 708,
		time: 707,
		velocity: 14.2672222222222,
		power: 7591.37460090547,
		road: 8766.67314814815,
		acceleration: 0.32277777777778
	},
	{
		id: 709,
		time: 708,
		velocity: 14.5763888888889,
		power: 7187.41708734222,
		road: 8781.09861111111,
		acceleration: 0.276851851851852
	},
	{
		id: 710,
		time: 709,
		velocity: 14.8480555555556,
		power: 5275.83766749386,
		road: 8795.72722222222,
		acceleration: 0.129444444444443
	},
	{
		id: 711,
		time: 710,
		velocity: 14.6555555555556,
		power: 5856.40248438138,
		road: 8810.50273148148,
		acceleration: 0.164351851851851
	},
	{
		id: 712,
		time: 711,
		velocity: 15.0694444444444,
		power: 7503.51570863291,
		road: 8825.49523148148,
		acceleration: 0.26962962962963
	},
	{
		id: 713,
		time: 712,
		velocity: 15.6569444444444,
		power: 10980.8417209938,
		road: 8840.86597222222,
		acceleration: 0.486851851851853
	},
	{
		id: 714,
		time: 713,
		velocity: 16.1161111111111,
		power: 9825.39820842503,
		road: 8856.67115740741,
		acceleration: 0.382037037037037
	},
	{
		id: 715,
		time: 714,
		velocity: 16.2155555555556,
		power: 5528.769380334,
		road: 8872.71138888889,
		acceleration: 0.0880555555555542
	},
	{
		id: 716,
		time: 715,
		velocity: 15.9211111111111,
		power: 2312.64857271963,
		road: 8888.7350462963,
		acceleration: -0.121203703703703
	},
	{
		id: 717,
		time: 716,
		velocity: 15.7525,
		power: 894.547637928264,
		road: 8904.59300925926,
		acceleration: -0.210185185185184
	},
	{
		id: 718,
		time: 717,
		velocity: 15.585,
		power: 2289.66907514203,
		road: 8920.28902777778,
		acceleration: -0.113703703703704
	},
	{
		id: 719,
		time: 718,
		velocity: 15.58,
		power: 2606.83369877877,
		road: 8935.88337962963,
		acceleration: -0.0896296296296324
	},
	{
		id: 720,
		time: 719,
		velocity: 15.4836111111111,
		power: 2927.82877860867,
		road: 8951.4000462963,
		acceleration: -0.0657407407407398
	},
	{
		id: 721,
		time: 720,
		velocity: 15.3877777777778,
		power: 2834.54610804828,
		road: 8966.84884259259,
		acceleration: -0.0700000000000003
	},
	{
		id: 722,
		time: 721,
		velocity: 15.37,
		power: 3671.3800933185,
		road: 8982.25666666667,
		acceleration: -0.0119444444444436
	},
	{
		id: 723,
		time: 722,
		velocity: 15.4477777777778,
		power: 3792.94851564804,
		road: 8997.65680555555,
		acceleration: -0.00342592592592617
	},
	{
		id: 724,
		time: 723,
		velocity: 15.3775,
		power: 4136.55229194348,
		road: 9013.06504629629,
		acceleration: 0.0196296296296303
	},
	{
		id: 725,
		time: 724,
		velocity: 15.4288888888889,
		power: 3307.65957101988,
		road: 9028.46490740741,
		acceleration: -0.0363888888888901
	},
	{
		id: 726,
		time: 725,
		velocity: 15.3386111111111,
		power: 3373.41075980279,
		road: 9043.83115740741,
		acceleration: -0.0308333333333337
	},
	{
		id: 727,
		time: 726,
		velocity: 15.285,
		power: 653.059962910056,
		road: 9059.07518518518,
		acceleration: -0.21361111111111
	},
	{
		id: 728,
		time: 727,
		velocity: 14.7880555555556,
		power: -1413.28053613303,
		road: 9074.03615740741,
		acceleration: -0.352499999999999
	},
	{
		id: 729,
		time: 728,
		velocity: 14.2811111111111,
		power: -3214.04161558625,
		road: 9088.58185185185,
		acceleration: -0.478055555555557
	},
	{
		id: 730,
		time: 729,
		velocity: 13.8508333333333,
		power: -1618.24512745134,
		road: 9102.70856481482,
		acceleration: -0.359907407407407
	},
	{
		id: 731,
		time: 730,
		velocity: 13.7083333333333,
		power: 250.095430506769,
		road: 9116.54708333333,
		acceleration: -0.216481481481482
	},
	{
		id: 732,
		time: 731,
		velocity: 13.6316666666667,
		power: 2773.13211110125,
		road: 9130.26643518519,
		acceleration: -0.0218518518518493
	},
	{
		id: 733,
		time: 732,
		velocity: 13.7852777777778,
		power: 4848.24337467626,
		road: 9144.04194444445,
		acceleration: 0.134166666666665
	},
	{
		id: 734,
		time: 733,
		velocity: 14.1108333333333,
		power: 6027.18852335607,
		road: 9157.99217592593,
		acceleration: 0.215277777777779
	},
	{
		id: 735,
		time: 734,
		velocity: 14.2775,
		power: 5648.14535921826,
		road: 9172.13893518519,
		acceleration: 0.177777777777777
	},
	{
		id: 736,
		time: 735,
		velocity: 14.3186111111111,
		power: 3932.13662453422,
		road: 9186.39791666667,
		acceleration: 0.0466666666666669
	},
	{
		id: 737,
		time: 736,
		velocity: 14.2508333333333,
		power: 3757.0778589211,
		road: 9200.69643518519,
		acceleration: 0.0324074074074066
	},
	{
		id: 738,
		time: 737,
		velocity: 14.3747222222222,
		power: 3365.61201579338,
		road: 9215.01273148148,
		acceleration: 0.00314814814814923
	},
	{
		id: 739,
		time: 738,
		velocity: 14.3280555555556,
		power: 4271.71726772975,
		road: 9229.36462962963,
		acceleration: 0.0680555555555546
	},
	{
		id: 740,
		time: 739,
		velocity: 14.455,
		power: 4047.56757539993,
		road: 9243.77532407408,
		acceleration: 0.0495370370370374
	},
	{
		id: 741,
		time: 740,
		velocity: 14.5233333333333,
		power: 3997.1943189091,
		road: 9258.23287037037,
		acceleration: 0.0441666666666638
	},
	{
		id: 742,
		time: 741,
		velocity: 14.4605555555556,
		power: 2916.24532832551,
		road: 9272.69537037037,
		acceleration: -0.0342592592592581
	},
	{
		id: 743,
		time: 742,
		velocity: 14.3522222222222,
		power: 2119.14085169726,
		road: 9287.09560185185,
		acceleration: -0.0902777777777768
	},
	{
		id: 744,
		time: 743,
		velocity: 14.2525,
		power: 2725.52326276999,
		road: 9301.42861111111,
		acceleration: -0.0441666666666656
	},
	{
		id: 745,
		time: 744,
		velocity: 14.3280555555556,
		power: 3478.44761333874,
		road: 9315.74523148148,
		acceleration: 0.011388888888888
	},
	{
		id: 746,
		time: 745,
		velocity: 14.3863888888889,
		power: 2185.11802139682,
		road: 9330.02643518519,
		acceleration: -0.0822222222222209
	},
	{
		id: 747,
		time: 746,
		velocity: 14.0058333333333,
		power: -261.135707158882,
		road: 9344.13699074074,
		acceleration: -0.259074074074077
	},
	{
		id: 748,
		time: 747,
		velocity: 13.5508333333333,
		power: -2971.29960591587,
		road: 9357.88782407407,
		acceleration: -0.46037037037037
	},
	{
		id: 749,
		time: 748,
		velocity: 13.0052777777778,
		power: -2540.82953182628,
		road: 9371.19481481481,
		acceleration: -0.427314814814814
	},
	{
		id: 750,
		time: 749,
		velocity: 12.7238888888889,
		power: -2804.31968232438,
		road: 9384.06356481481,
		acceleration: -0.449166666666668
	},
	{
		id: 751,
		time: 750,
		velocity: 12.2033333333333,
		power: -1967.3958111317,
		road: 9396.5175,
		acceleration: -0.380462962962962
	},
	{
		id: 752,
		time: 751,
		velocity: 11.8638888888889,
		power: -1366.051839465,
		road: 9408.61703703704,
		acceleration: -0.328333333333333
	},
	{
		id: 753,
		time: 752,
		velocity: 11.7388888888889,
		power: -176.90561448345,
		road: 9420.44143518518,
		acceleration: -0.221944444444445
	},
	{
		id: 754,
		time: 753,
		velocity: 11.5375,
		power: 1009.3416167752,
		road: 9432.09824074074,
		acceleration: -0.113240740740741
	},
	{
		id: 755,
		time: 754,
		velocity: 11.5241666666667,
		power: 1084.11971598908,
		road: 9443.64634259259,
		acceleration: -0.104166666666666
	},
	{
		id: 756,
		time: 755,
		velocity: 11.4263888888889,
		power: 2025.24116011191,
		road: 9455.13388888889,
		acceleration: -0.0169444444444427
	},
	{
		id: 757,
		time: 756,
		velocity: 11.4866666666667,
		power: 2334.65779962996,
		road: 9466.61865740741,
		acceleration: 0.011388888888888
	},
	{
		id: 758,
		time: 757,
		velocity: 11.5583333333333,
		power: 2813.81620756993,
		road: 9478.13611111111,
		acceleration: 0.0539814814814807
	},
	{
		id: 759,
		time: 758,
		velocity: 11.5883333333333,
		power: 1830.09807751787,
		road: 9489.66263888889,
		acceleration: -0.0358333333333309
	},
	{
		id: 760,
		time: 759,
		velocity: 11.3791666666667,
		power: 290.921261567323,
		road: 9501.08412037037,
		acceleration: -0.174259259259259
	},
	{
		id: 761,
		time: 760,
		velocity: 11.0355555555556,
		power: -653.207238532513,
		road: 9512.2887962963,
		acceleration: -0.259351851851854
	},
	{
		id: 762,
		time: 761,
		velocity: 10.8102777777778,
		power: -415.669568440238,
		road: 9523.24634259259,
		acceleration: -0.234907407407409
	},
	{
		id: 763,
		time: 762,
		velocity: 10.6744444444444,
		power: 804.197880700863,
		road: 9534.02907407407,
		acceleration: -0.114722222222222
	},
	{
		id: 764,
		time: 763,
		velocity: 10.6913888888889,
		power: 1887.79493187523,
		road: 9544.75069444444,
		acceleration: -0.00750000000000206
	},
	{
		id: 765,
		time: 764,
		velocity: 10.7877777777778,
		power: 1196.84291487584,
		road: 9555.43148148148,
		acceleration: -0.0741666666666667
	},
	{
		id: 766,
		time: 765,
		velocity: 10.4519444444444,
		power: 1790.952681772,
		road: 9566.06791666667,
		acceleration: -0.0145370370370355
	},
	{
		id: 767,
		time: 766,
		velocity: 10.6477777777778,
		power: 962.617785564749,
		road: 9576.64953703704,
		acceleration: -0.0950925925925912
	},
	{
		id: 768,
		time: 767,
		velocity: 10.5025,
		power: 707.248121242625,
		road: 9587.12439814815,
		acceleration: -0.118425925925926
	},
	{
		id: 769,
		time: 768,
		velocity: 10.0966666666667,
		power: -933.888402570548,
		road: 9597.39888888889,
		acceleration: -0.282314814814816
	},
	{
		id: 770,
		time: 769,
		velocity: 9.80083333333333,
		power: -2210.81595897881,
		road: 9607.32384259259,
		acceleration: -0.416759259259258
	},
	{
		id: 771,
		time: 770,
		velocity: 9.25222222222222,
		power: -3006.12363562775,
		road: 9616.78467592593,
		acceleration: -0.51148148148148
	},
	{
		id: 772,
		time: 771,
		velocity: 8.56222222222222,
		power: -2860.07062466624,
		road: 9625.73569444445,
		acceleration: -0.508148148148148
	},
	{
		id: 773,
		time: 772,
		velocity: 8.27638888888889,
		power: -2246.58382169486,
		road: 9634.20944444444,
		acceleration: -0.44638888888889
	},
	{
		id: 774,
		time: 773,
		velocity: 7.91305555555556,
		power: -2028.79668474701,
		road: 9642.24546296296,
		acceleration: -0.429074074074074
	},
	{
		id: 775,
		time: 774,
		velocity: 7.275,
		power: -245.731345841431,
		road: 9649.96972222222,
		acceleration: -0.194444444444445
	},
	{
		id: 776,
		time: 775,
		velocity: 7.69305555555556,
		power: 2987.24906882837,
		road: 9657.7187037037,
		acceleration: 0.24388888888889
	},
	{
		id: 777,
		time: 776,
		velocity: 8.64472222222222,
		power: 6589.93151661734,
		road: 9665.92856481481,
		acceleration: 0.67787037037037
	},
	{
		id: 778,
		time: 777,
		velocity: 9.30861111111111,
		power: 8883.0128355157,
		road: 9674.91018518518,
		acceleration: 0.865648148148146
	},
	{
		id: 779,
		time: 778,
		velocity: 10.29,
		power: 12928.2821640176,
		road: 9684.91101851852,
		acceleration: 1.17277777777778
	},
	{
		id: 780,
		time: 779,
		velocity: 12.1630555555556,
		power: 12599.4375763223,
		road: 9695.99592592592,
		acceleration: 0.99537037037037
	},
	{
		id: 781,
		time: 780,
		velocity: 12.2947222222222,
		power: 9807.64127297621,
		road: 9707.90643518518,
		acceleration: 0.655833333333334
	},
	{
		id: 782,
		time: 781,
		velocity: 12.2575,
		power: 6821.56044782523,
		road: 9720.32550925926,
		acceleration: 0.361296296296295
	},
	{
		id: 783,
		time: 782,
		velocity: 13.2469444444444,
		power: 9785.57339690296,
		road: 9733.2125,
		acceleration: 0.574537037037038
	},
	{
		id: 784,
		time: 783,
		velocity: 14.0183333333333,
		power: 16949.3579849899,
		road: 9746.91763888889,
		acceleration: 1.06175925925926
	},
	{
		id: 785,
		time: 784,
		velocity: 15.4427777777778,
		power: 17451.250888739,
		road: 9761.6487037037,
		acceleration: 0.990092592592593
	},
	{
		id: 786,
		time: 785,
		velocity: 16.2172222222222,
		power: 17714.1326511213,
		road: 9777.3325,
		acceleration: 0.91537037037037
	},
	{
		id: 787,
		time: 786,
		velocity: 16.7644444444444,
		power: 13853.3662793034,
		road: 9793.77458333333,
		acceleration: 0.601203703703703
	},
	{
		id: 788,
		time: 787,
		velocity: 17.2463888888889,
		power: 10157.9950841231,
		road: 9810.68689814815,
		acceleration: 0.339259259259258
	},
	{
		id: 789,
		time: 788,
		velocity: 17.235,
		power: 5672.42301379936,
		road: 9827.79592592593,
		acceleration: 0.0541666666666671
	},
	{
		id: 790,
		time: 789,
		velocity: 16.9269444444444,
		power: 2971.7592254945,
		road: 9844.87703703704,
		acceleration: -0.109999999999999
	},
	{
		id: 791,
		time: 790,
		velocity: 16.9163888888889,
		power: 1659.50663965323,
		road: 9861.80986111111,
		acceleration: -0.186574074074073
	},
	{
		id: 792,
		time: 791,
		velocity: 16.6752777777778,
		power: 4434.05975319258,
		road: 9878.64351851852,
		acceleration: -0.0117592592592572
	},
	{
		id: 793,
		time: 792,
		velocity: 16.8916666666667,
		power: 6408.6283846725,
		road: 9895.52569444444,
		acceleration: 0.108796296296294
	},
	{
		id: 794,
		time: 793,
		velocity: 17.2427777777778,
		power: 8652.15327449704,
		road: 9912.58171296296,
		acceleration: 0.238888888888891
	},
	{
		id: 795,
		time: 794,
		velocity: 17.3919444444444,
		power: 7018.54853566266,
		road: 9929.8224537037,
		acceleration: 0.130555555555553
	},
	{
		id: 796,
		time: 795,
		velocity: 17.2833333333333,
		power: 5288.9769713007,
		road: 9947.13986111111,
		acceleration: 0.022777777777776
	},
	{
		id: 797,
		time: 796,
		velocity: 17.3111111111111,
		power: 2734.93207245971,
		road: 9964.40375,
		acceleration: -0.129814814814811
	},
	{
		id: 798,
		time: 797,
		velocity: 17.0025,
		power: 327.258829074759,
		road: 9981.46699074074,
		acceleration: -0.271481481481484
	},
	{
		id: 799,
		time: 798,
		velocity: 16.4688888888889,
		power: -492.902603606907,
		road: 9998.23615740741,
		acceleration: -0.316666666666666
	},
	{
		id: 800,
		time: 799,
		velocity: 16.3611111111111,
		power: -968.458007796348,
		road: 10014.6762962963,
		acceleration: -0.34138888888889
	},
	{
		id: 801,
		time: 800,
		velocity: 15.9783333333333,
		power: 1457.89472804355,
		road: 10030.8553703704,
		acceleration: -0.180740740740738
	},
	{
		id: 802,
		time: 801,
		velocity: 15.9266666666667,
		power: 164.988631623623,
		road: 10046.8139814815,
		acceleration: -0.260185185185188
	},
	{
		id: 803,
		time: 802,
		velocity: 15.5805555555556,
		power: 1507.24759943062,
		road: 10062.5589814815,
		acceleration: -0.167037037037034
	},
	{
		id: 804,
		time: 803,
		velocity: 15.4772222222222,
		power: 1019.36108330812,
		road: 10078.1227314815,
		acceleration: -0.195462962962964
	},
	{
		id: 805,
		time: 804,
		velocity: 15.3402777777778,
		power: 1377.44744377511,
		road: 10093.5051851852,
		acceleration: -0.167129629629629
	},
	{
		id: 806,
		time: 805,
		velocity: 15.0791666666667,
		power: 1323.19626324233,
		road: 10108.7206018519,
		acceleration: -0.166944444444447
	},
	{
		id: 807,
		time: 806,
		velocity: 14.9763888888889,
		power: 179.656311787039,
		road: 10123.7315740741,
		acceleration: -0.241944444444442
	},
	{
		id: 808,
		time: 807,
		velocity: 14.6144444444444,
		power: 1136.54292831907,
		road: 10138.53625,
		acceleration: -0.170648148148151
	},
	{
		id: 809,
		time: 808,
		velocity: 14.5672222222222,
		power: 489.256558147794,
		road: 10153.149212963,
		acceleration: -0.212777777777776
	},
	{
		id: 810,
		time: 809,
		velocity: 14.3380555555556,
		power: 1076.61737028248,
		road: 10167.5725,
		acceleration: -0.166574074074076
	},
	{
		id: 811,
		time: 810,
		velocity: 14.1147222222222,
		power: -599.259482880079,
		road: 10181.7698611111,
		acceleration: -0.285277777777775
	},
	{
		id: 812,
		time: 811,
		velocity: 13.7113888888889,
		power: -940.209947799391,
		road: 10195.6709722222,
		acceleration: -0.307222222222222
	},
	{
		id: 813,
		time: 812,
		velocity: 13.4163888888889,
		power: -618.452623021182,
		road: 10209.27875,
		acceleration: -0.279444444444444
	},
	{
		id: 814,
		time: 813,
		velocity: 13.2763888888889,
		power: 427.700273798293,
		road: 10222.6494444444,
		acceleration: -0.194722222222223
	},
	{
		id: 815,
		time: 814,
		velocity: 13.1272222222222,
		power: 1391.99207644284,
		road: 10235.865,
		acceleration: -0.115555555555558
	},
	{
		id: 816,
		time: 815,
		velocity: 13.0697222222222,
		power: 2907.88296726998,
		road: 10249.0259722222,
		acceleration: 0.00638888888889078
	},
	{
		id: 817,
		time: 816,
		velocity: 13.2955555555556,
		power: 4093.80725195586,
		road: 10262.2394907407,
		acceleration: 0.0987037037037037
	},
	{
		id: 818,
		time: 817,
		velocity: 13.4233333333333,
		power: 5692.32906955632,
		road: 10275.6112037037,
		acceleration: 0.217685185185184
	},
	{
		id: 819,
		time: 818,
		velocity: 13.7227777777778,
		power: 5582.68744302119,
		road: 10289.1913425926,
		acceleration: 0.199166666666668
	},
	{
		id: 820,
		time: 819,
		velocity: 13.8930555555556,
		power: 6461.65450061969,
		road: 10302.9986574074,
		acceleration: 0.255185185185185
	},
	{
		id: 821,
		time: 820,
		velocity: 14.1888888888889,
		power: 6027.15676849543,
		road: 10317.0390277778,
		acceleration: 0.210925925925926
	},
	{
		id: 822,
		time: 821,
		velocity: 14.3555555555556,
		power: 6519.47541804203,
		road: 10331.3030555556,
		acceleration: 0.236388888888886
	},
	{
		id: 823,
		time: 822,
		velocity: 14.6022222222222,
		power: 6943.2572858861,
		road: 10345.8126851852,
		acceleration: 0.254814814814818
	},
	{
		id: 824,
		time: 823,
		velocity: 14.9533333333333,
		power: 5720.91795511759,
		road: 10360.5284259259,
		acceleration: 0.157407407407405
	},
	{
		id: 825,
		time: 824,
		velocity: 14.8277777777778,
		power: 4213.13894562241,
		road: 10375.3460648148,
		acceleration: 0.0463888888888917
	},
	{
		id: 826,
		time: 825,
		velocity: 14.7413888888889,
		power: 2660.2635706288,
		road: 10390.1553703704,
		acceleration: -0.0630555555555556
	},
	{
		id: 827,
		time: 826,
		velocity: 14.7641666666667,
		power: 2006.27976376603,
		road: 10404.8796296296,
		acceleration: -0.107037037037038
	},
	{
		id: 828,
		time: 827,
		velocity: 14.5066666666667,
		power: 1288.18627752384,
		road: 10419.4728240741,
		acceleration: -0.155092592592593
	},
	{
		id: 829,
		time: 828,
		velocity: 14.2761111111111,
		power: 449.39145009614,
		road: 10433.8825462963,
		acceleration: -0.211851851851851
	},
	{
		id: 830,
		time: 829,
		velocity: 14.1286111111111,
		power: -880.065751127951,
		road: 10448.0336111111,
		acceleration: -0.305462962962963
	},
	{
		id: 831,
		time: 830,
		velocity: 13.5902777777778,
		power: -1125.10624060855,
		road: 10461.8716666667,
		acceleration: -0.320555555555556
	},
	{
		id: 832,
		time: 831,
		velocity: 13.3144444444444,
		power: -1354.51758383852,
		road: 10475.3817592593,
		acceleration: -0.33537037037037
	},
	{
		id: 833,
		time: 832,
		velocity: 13.1225,
		power: 862.123949087335,
		road: 10488.6449074074,
		acceleration: -0.15851851851852
	},
	{
		id: 834,
		time: 833,
		velocity: 13.1147222222222,
		power: 1714.74660835132,
		road: 10501.7847685185,
		acceleration: -0.088055555555556
	},
	{
		id: 835,
		time: 834,
		velocity: 13.0502777777778,
		power: 3015.62197882589,
		road: 10514.8890277778,
		acceleration: 0.0168518518518539
	},
	{
		id: 836,
		time: 835,
		velocity: 13.1730555555556,
		power: 3414.52544548743,
		road: 10528.0255092593,
		acceleration: 0.0475925925925917
	},
	{
		id: 837,
		time: 836,
		velocity: 13.2575,
		power: 3746.91511430683,
		road: 10541.221712963,
		acceleration: 0.0718518518518536
	},
	{
		id: 838,
		time: 837,
		velocity: 13.2658333333333,
		power: 3020.46882934899,
		road: 10554.4602314815,
		acceleration: 0.012777777777778
	},
	{
		id: 839,
		time: 838,
		velocity: 13.2113888888889,
		power: 1811.00541399271,
		road: 10567.6641203704,
		acceleration: -0.082037037037038
	},
	{
		id: 840,
		time: 839,
		velocity: 13.0113888888889,
		power: 975.940563249239,
		road: 10580.7540277778,
		acceleration: -0.145925925925928
	},
	{
		id: 841,
		time: 840,
		velocity: 12.8280555555556,
		power: -165.804976500804,
		road: 10593.6536111111,
		acceleration: -0.234722222222221
	},
	{
		id: 842,
		time: 841,
		velocity: 12.5072222222222,
		power: 14.9717563427445,
		road: 10606.3274537037,
		acceleration: -0.216759259259259
	},
	{
		id: 843,
		time: 842,
		velocity: 12.3611111111111,
		power: 436.557126491487,
		road: 10618.8036574074,
		acceleration: -0.178518518518517
	},
	{
		id: 844,
		time: 843,
		velocity: 12.2925,
		power: 1108.13310146948,
		road: 10631.1311574074,
		acceleration: -0.118888888888891
	},
	{
		id: 845,
		time: 844,
		velocity: 12.1505555555556,
		power: 1258.15849079744,
		road: 10643.3474074074,
		acceleration: -0.10361111111111
	},
	{
		id: 846,
		time: 845,
		velocity: 12.0502777777778,
		power: 863.61793253951,
		road: 10655.4443055555,
		acceleration: -0.135092592592592
	},
	{
		id: 847,
		time: 846,
		velocity: 11.8872222222222,
		power: 835.120034435421,
		road: 10667.4062037037,
		acceleration: -0.134907407407407
	},
	{
		id: 848,
		time: 847,
		velocity: 11.7458333333333,
		power: 700.849498111066,
		road: 10679.2286111111,
		acceleration: -0.144074074074075
	},
	{
		id: 849,
		time: 848,
		velocity: 11.6180555555556,
		power: 1122.37995742618,
		road: 10690.9269907407,
		acceleration: -0.103981481481481
	},
	{
		id: 850,
		time: 849,
		velocity: 11.5752777777778,
		power: 386.168302046675,
		road: 10702.4894907407,
		acceleration: -0.167777777777776
	},
	{
		id: 851,
		time: 850,
		velocity: 11.2425,
		power: 472.556559223728,
		road: 10713.8894907407,
		acceleration: -0.157222222222222
	},
	{
		id: 852,
		time: 851,
		velocity: 11.1463888888889,
		power: 462.475690115452,
		road: 10725.1331018518,
		acceleration: -0.155555555555557
	},
	{
		id: 853,
		time: 852,
		velocity: 11.1086111111111,
		power: 838.590627293931,
		road: 10736.2400462963,
		acceleration: -0.117777777777778
	},
	{
		id: 854,
		time: 853,
		velocity: 10.8891666666667,
		power: 1290.27679335601,
		road: 10747.2516666667,
		acceleration: -0.0728703703703708
	},
	{
		id: 855,
		time: 854,
		velocity: 10.9277777777778,
		power: 346.991314629565,
		road: 10758.1463888889,
		acceleration: -0.160925925925925
	},
	{
		id: 856,
		time: 855,
		velocity: 10.6258333333333,
		power: 560.606399740076,
		road: 10768.8917592593,
		acceleration: -0.137777777777778
	},
	{
		id: 857,
		time: 856,
		velocity: 10.4758333333333,
		power: -121.287180964587,
		road: 10779.4669907407,
		acceleration: -0.202500000000001
	},
	{
		id: 858,
		time: 857,
		velocity: 10.3202777777778,
		power: 383.109185168673,
		road: 10789.8661111111,
		acceleration: -0.149722222222222
	},
	{
		id: 859,
		time: 858,
		velocity: 10.1766666666667,
		power: 883.988439395877,
		road: 10800.142037037,
		acceleration: -0.0966666666666658
	},
	{
		id: 860,
		time: 859,
		velocity: 10.1858333333333,
		power: 2274.52958299037,
		road: 10810.3927314815,
		acceleration: 0.0462037037037035
	},
	{
		id: 861,
		time: 860,
		velocity: 10.4588888888889,
		power: 1682.84736094474,
		road: 10820.6591203704,
		acceleration: -0.0148148148148142
	},
	{
		id: 862,
		time: 861,
		velocity: 10.1322222222222,
		power: 2416.23260456395,
		road: 10830.9477777778,
		acceleration: 0.0593518518518508
	},
	{
		id: 863,
		time: 862,
		velocity: 10.3638888888889,
		power: 1827.31353379379,
		road: 10841.2653240741,
		acceleration: -0.00157407407407462
	},
	{
		id: 864,
		time: 863,
		velocity: 10.4541666666667,
		power: 3645.02382377749,
		road: 10851.6716203704,
		acceleration: 0.179074074074073
	},
	{
		id: 865,
		time: 864,
		velocity: 10.6694444444444,
		power: 3340.09167882391,
		road: 10862.2381018518,
		acceleration: 0.141296296296296
	},
	{
		id: 866,
		time: 865,
		velocity: 10.7877777777778,
		power: 4482.20401002199,
		road: 10872.9974074074,
		acceleration: 0.244351851851853
	},
	{
		id: 867,
		time: 866,
		velocity: 11.1872222222222,
		power: 4531.40948617622,
		road: 10883.9971296296,
		acceleration: 0.236481481481482
	},
	{
		id: 868,
		time: 867,
		velocity: 11.3788888888889,
		power: 5481.11613167382,
		road: 10895.2705092593,
		acceleration: 0.310833333333333
	},
	{
		id: 869,
		time: 868,
		velocity: 11.7202777777778,
		power: 5359.00060024985,
		road: 10906.8406944444,
		acceleration: 0.282777777777778
	},
	{
		id: 870,
		time: 869,
		velocity: 12.0355555555556,
		power: 5906.85892285717,
		road: 10918.7097222222,
		acceleration: 0.314907407407409
	},
	{
		id: 871,
		time: 870,
		velocity: 12.3236111111111,
		power: 6010.30251713835,
		road: 10930.8893055555,
		acceleration: 0.306203703703702
	},
	{
		id: 872,
		time: 871,
		velocity: 12.6388888888889,
		power: 5916.37467095672,
		road: 10943.362962963,
		acceleration: 0.281944444444447
	},
	{
		id: 873,
		time: 872,
		velocity: 12.8813888888889,
		power: 5970.31314443991,
		road: 10956.1133796296,
		acceleration: 0.271574074074074
	},
	{
		id: 874,
		time: 873,
		velocity: 13.1383333333333,
		power: 4936.28473693081,
		road: 10969.0877314815,
		acceleration: 0.176296296296295
	},
	{
		id: 875,
		time: 874,
		velocity: 13.1677777777778,
		power: 2606.46117380336,
		road: 10982.1430555555,
		acceleration: -0.0143518518518508
	},
	{
		id: 876,
		time: 875,
		velocity: 12.8383333333333,
		power: -464.003556973753,
		road: 10995.0616203704,
		acceleration: -0.259166666666665
	},
	{
		id: 877,
		time: 876,
		velocity: 12.3608333333333,
		power: -1415.79309780456,
		road: 11007.6831944444,
		acceleration: -0.334814814814818
	},
	{
		id: 878,
		time: 877,
		velocity: 12.1633333333333,
		power: -1739.41739331231,
		road: 11019.9568981481,
		acceleration: -0.360925925925926
	},
	{
		id: 879,
		time: 878,
		velocity: 11.7555555555556,
		power: -761.675508498619,
		road: 11031.9127314815,
		acceleration: -0.274814814814816
	},
	{
		id: 880,
		time: 879,
		velocity: 11.5363888888889,
		power: 619.051508768757,
		road: 11043.6562037037,
		acceleration: -0.149907407407406
	},
	{
		id: 881,
		time: 880,
		velocity: 11.7136111111111,
		power: 2847.88682030311,
		road: 11055.3501388889,
		acceleration: 0.0508333333333315
	},
	{
		id: 882,
		time: 881,
		velocity: 11.9080555555556,
		power: 5300.27467336878,
		road: 11067.2006481481,
		acceleration: 0.262314814814815
	},
	{
		id: 883,
		time: 882,
		velocity: 12.3233333333333,
		power: 5156.57078653568,
		road: 11079.3006944444,
		acceleration: 0.236759259259262
	},
	{
		id: 884,
		time: 883,
		velocity: 12.4238888888889,
		power: 4630.93307874726,
		road: 11091.6098611111,
		acceleration: 0.18148148148148
	},
	{
		id: 885,
		time: 884,
		velocity: 12.4525,
		power: 3489.74595152983,
		road: 11104.0494444444,
		acceleration: 0.0793518518518521
	},
	{
		id: 886,
		time: 885,
		velocity: 12.5613888888889,
		power: 3812.03599330111,
		road: 11116.5801388889,
		acceleration: 0.10287037037037
	},
	{
		id: 887,
		time: 886,
		velocity: 12.7325,
		power: 3926.06362238626,
		road: 11129.2163425926,
		acceleration: 0.10814814814815
	},
	{
		id: 888,
		time: 887,
		velocity: 12.7769444444444,
		power: 4002.86268650432,
		road: 11141.9616666667,
		acceleration: 0.110092592592594
	},
	{
		id: 889,
		time: 888,
		velocity: 12.8916666666667,
		power: 3717.97534598096,
		road: 11154.8035185185,
		acceleration: 0.0829629629629611
	},
	{
		id: 890,
		time: 889,
		velocity: 12.9813888888889,
		power: 3629.04599703371,
		road: 11167.7232407407,
		acceleration: 0.0727777777777785
	},
	{
		id: 891,
		time: 890,
		velocity: 12.9952777777778,
		power: 1853.95837693728,
		road: 11180.64375,
		acceleration: -0.0712037037037057
	},
	{
		id: 892,
		time: 891,
		velocity: 12.6780555555556,
		power: -220.30919016131,
		road: 11193.4099537037,
		acceleration: -0.237407407407407
	},
	{
		id: 893,
		time: 892,
		velocity: 12.2691666666667,
		power: -1405.98331935508,
		road: 11205.890787037,
		acceleration: -0.333333333333334
	},
	{
		id: 894,
		time: 893,
		velocity: 11.9952777777778,
		power: -2032.03750940127,
		road: 11218.0119444444,
		acceleration: -0.386018518518519
	},
	{
		id: 895,
		time: 894,
		velocity: 11.52,
		power: -1967.06729249119,
		road: 11229.7496759259,
		acceleration: -0.380833333333333
	},
	{
		id: 896,
		time: 895,
		velocity: 11.1266666666667,
		power: -2511.8403599857,
		road: 11241.0808333333,
		acceleration: -0.432314814814813
	},
	{
		id: 897,
		time: 896,
		velocity: 10.6983333333333,
		power: -823.676594753543,
		road: 11252.0587962963,
		acceleration: -0.274074074074075
	},
	{
		id: 898,
		time: 897,
		velocity: 10.6977777777778,
		power: 1380.53569921638,
		road: 11262.8700462963,
		acceleration: -0.0593518518518508
	},
	{
		id: 899,
		time: 898,
		velocity: 10.9486111111111,
		power: 4093.76952660261,
		road: 11273.7518518518,
		acceleration: 0.200462962962964
	},
	{
		id: 900,
		time: 899,
		velocity: 11.2997222222222,
		power: 6324.84507453126,
		road: 11284.9316203704,
		acceleration: 0.395462962962961
	},
	{
		id: 901,
		time: 900,
		velocity: 11.8841666666667,
		power: 7215.45136206313,
		road: 11296.5335185185,
		acceleration: 0.448796296296297
	},
	{
		id: 902,
		time: 901,
		velocity: 12.295,
		power: 7387.07394320773,
		road: 11308.576712963,
		acceleration: 0.433796296296295
	},
	{
		id: 903,
		time: 902,
		velocity: 12.6011111111111,
		power: 7449.93221522772,
		road: 11321.0424537037,
		acceleration: 0.411296296296298
	},
	{
		id: 904,
		time: 903,
		velocity: 13.1180555555556,
		power: 8307.37449985773,
		road: 11333.9406481481,
		acceleration: 0.45361111111111
	},
	{
		id: 905,
		time: 904,
		velocity: 13.6558333333333,
		power: 7309.73440217862,
		road: 11347.2399537037,
		acceleration: 0.348611111111111
	},
	{
		id: 906,
		time: 905,
		velocity: 13.6469444444444,
		power: 4944.51332533824,
		road: 11360.789212963,
		acceleration: 0.151296296296294
	},
	{
		id: 907,
		time: 906,
		velocity: 13.5719444444444,
		power: 1851.59837853561,
		road: 11374.3698148148,
		acceleration: -0.0886111111111116
	},
	{
		id: 908,
		time: 907,
		velocity: 13.39,
		power: 2542.24594353747,
		road: 11387.8893518518,
		acceleration: -0.0335185185185143
	},
	{
		id: 909,
		time: 908,
		velocity: 13.5463888888889,
		power: 3630.173386174,
		road: 11401.4173611111,
		acceleration: 0.0504629629629623
	},
	{
		id: 910,
		time: 909,
		velocity: 13.7233333333333,
		power: 5428.52719033785,
		road: 11415.0627314815,
		acceleration: 0.18425925925926
	},
	{
		id: 911,
		time: 910,
		velocity: 13.9427777777778,
		power: 5322.54184394353,
		road: 11428.8843055555,
		acceleration: 0.168148148148145
	},
	{
		id: 912,
		time: 911,
		velocity: 14.0508333333333,
		power: 5553.3542587442,
		road: 11442.8787962963,
		acceleration: 0.177685185185187
	},
	{
		id: 913,
		time: 912,
		velocity: 14.2563888888889,
		power: 4735.37364556525,
		road: 11457.0174074074,
		acceleration: 0.110555555555553
	},
	{
		id: 914,
		time: 913,
		velocity: 14.2744444444444,
		power: 5104.3674070984,
		road: 11471.2776388889,
		acceleration: 0.132685185185187
	},
	{
		id: 915,
		time: 914,
		velocity: 14.4488888888889,
		power: 5050.43466114774,
		road: 11485.6658796296,
		acceleration: 0.123333333333333
	},
	{
		id: 916,
		time: 915,
		velocity: 14.6263888888889,
		power: 5077.37147025892,
		road: 11500.1758796296,
		acceleration: 0.120185185185186
	},
	{
		id: 917,
		time: 916,
		velocity: 14.635,
		power: 3613.80419044614,
		road: 11514.7521296296,
		acceleration: 0.0123148148148147
	},
	{
		id: 918,
		time: 917,
		velocity: 14.4858333333333,
		power: 2599.74175370106,
		road: 11529.3046296296,
		acceleration: -0.0598148148148141
	},
	{
		id: 919,
		time: 918,
		velocity: 14.4469444444444,
		power: 2333.65147307297,
		road: 11543.7887037037,
		acceleration: -0.0770370370370372
	},
	{
		id: 920,
		time: 919,
		velocity: 14.4038888888889,
		power: 2963.88410987385,
		road: 11558.2193518518,
		acceleration: -0.0298148148148165
	},
	{
		id: 921,
		time: 920,
		velocity: 14.3963888888889,
		power: 3200.85772121575,
		road: 11572.6291203704,
		acceleration: -0.0119444444444454
	},
	{
		id: 922,
		time: 921,
		velocity: 14.4111111111111,
		power: 3490.78297758492,
		road: 11587.0375,
		acceleration: 0.00916666666666899
	},
	{
		id: 923,
		time: 922,
		velocity: 14.4313888888889,
		power: 2040.16785338585,
		road: 11601.4029166667,
		acceleration: -0.0950925925925912
	},
	{
		id: 924,
		time: 923,
		velocity: 14.1111111111111,
		power: -322.855815216138,
		road: 11615.5884259259,
		acceleration: -0.264722222222224
	},
	{
		id: 925,
		time: 924,
		velocity: 13.6169444444444,
		power: -1704.72602910419,
		road: 11629.4592592593,
		acceleration: -0.364629629629629
	},
	{
		id: 926,
		time: 925,
		velocity: 13.3375,
		power: -1172.2617110069,
		road: 11642.9870833333,
		acceleration: -0.32138888888889
	},
	{
		id: 927,
		time: 926,
		velocity: 13.1469444444444,
		power: 309.448945613436,
		road: 11656.2531018518,
		acceleration: -0.202222222222222
	},
	{
		id: 928,
		time: 927,
		velocity: 13.0102777777778,
		power: 1278.35087507391,
		road: 11669.3569907407,
		acceleration: -0.122037037037035
	},
	{
		id: 929,
		time: 928,
		velocity: 12.9713888888889,
		power: 1570.42205489267,
		road: 11682.3518518518,
		acceleration: -0.0960185185185196
	},
	{
		id: 930,
		time: 929,
		velocity: 12.8588888888889,
		power: 1003.61884751334,
		road: 11695.2290740741,
		acceleration: -0.13925925925926
	},
	{
		id: 931,
		time: 930,
		velocity: 12.5925,
		power: 487.983920595654,
		road: 11707.9474537037,
		acceleration: -0.178425925925923
	},
	{
		id: 932,
		time: 931,
		velocity: 12.4361111111111,
		power: 1252.84327018555,
		road: 11720.5205555555,
		acceleration: -0.112129629629631
	},
	{
		id: 933,
		time: 932,
		velocity: 12.5225,
		power: 3323.48032426233,
		road: 11733.068287037,
		acceleration: 0.0613888888888905
	},
	{
		id: 934,
		time: 933,
		velocity: 12.7766666666667,
		power: 4999.39844587172,
		road: 11745.7443518518,
		acceleration: 0.195277777777775
	},
	{
		id: 935,
		time: 934,
		velocity: 13.0219444444444,
		power: 5081.27399786264,
		road: 11758.6144907407,
		acceleration: 0.192870370370372
	},
	{
		id: 936,
		time: 935,
		velocity: 13.1011111111111,
		power: 4608.12817639203,
		road: 11771.654537037,
		acceleration: 0.146944444444443
	},
	{
		id: 937,
		time: 936,
		velocity: 13.2175,
		power: 4011.59459177864,
		road: 11784.8151851852,
		acceleration: 0.0942592592592586
	},
	{
		id: 938,
		time: 937,
		velocity: 13.3047222222222,
		power: 4643.47265328204,
		road: 11798.0927314815,
		acceleration: 0.139537037037037
	},
	{
		id: 939,
		time: 938,
		velocity: 13.5197222222222,
		power: 4070.2229767724,
		road: 11811.4849537037,
		acceleration: 0.0898148148148152
	},
	{
		id: 940,
		time: 939,
		velocity: 13.4869444444444,
		power: 2571.5932565394,
		road: 11824.9079166667,
		acceleration: -0.0283333333333342
	},
	{
		id: 941,
		time: 940,
		velocity: 13.2197222222222,
		power: 672.193452341445,
		road: 11838.2293981481,
		acceleration: -0.174629629629628
	},
	{
		id: 942,
		time: 941,
		velocity: 12.9958333333333,
		power: 322.476519104954,
		road: 11851.3640740741,
		acceleration: -0.198981481481482
	},
	{
		id: 943,
		time: 942,
		velocity: 12.89,
		power: 874.88380111989,
		road: 11864.3235648148,
		acceleration: -0.151388888888889
	},
	{
		id: 944,
		time: 943,
		velocity: 12.7655555555556,
		power: 1521.52441651664,
		road: 11877.1593055555,
		acceleration: -0.0961111111111119
	},
	{
		id: 945,
		time: 944,
		velocity: 12.7075,
		power: 1422.58163183889,
		road: 11889.8960648148,
		acceleration: -0.101851851851849
	},
	{
		id: 946,
		time: 945,
		velocity: 12.5844444444444,
		power: 1117.09581893285,
		road: 11902.5196296296,
		acceleration: -0.124537037037038
	},
	{
		id: 947,
		time: 946,
		velocity: 12.3919444444444,
		power: 1160.86197143664,
		road: 11915.0218055555,
		acceleration: -0.118240740740742
	},
	{
		id: 948,
		time: 947,
		velocity: 12.3527777777778,
		power: 1993.09652073602,
		road: 11927.4417592593,
		acceleration: -0.0462037037037035
	},
	{
		id: 949,
		time: 948,
		velocity: 12.4458333333333,
		power: 2881.71272505202,
		road: 11939.8531481481,
		acceleration: 0.0290740740740727
	},
	{
		id: 950,
		time: 949,
		velocity: 12.4791666666667,
		power: 5804.59745313032,
		road: 11952.4130555555,
		acceleration: 0.267962962962963
	},
	{
		id: 951,
		time: 950,
		velocity: 13.1566666666667,
		power: 7889.87217012367,
		road: 11965.3166203704,
		acceleration: 0.419351851851854
	},
	{
		id: 952,
		time: 951,
		velocity: 13.7038888888889,
		power: 8833.40437479671,
		road: 11978.6625925926,
		acceleration: 0.465462962962961
	},
	{
		id: 953,
		time: 952,
		velocity: 13.8755555555556,
		power: 6562.62719047753,
		road: 11992.3751851852,
		acceleration: 0.267777777777779
	},
	{
		id: 954,
		time: 953,
		velocity: 13.96,
		power: 4652.14135897395,
		road: 12006.2786574074,
		acceleration: 0.113981481481481
	},
	{
		id: 955,
		time: 954,
		velocity: 14.0458333333333,
		power: 3978.52244816163,
		road: 12020.2691203704,
		acceleration: 0.0599999999999987
	},
	{
		id: 956,
		time: 955,
		velocity: 14.0555555555556,
		power: 3093.09349721184,
		road: 12034.2860185185,
		acceleration: -0.00712962962962749
	},
	{
		id: 957,
		time: 956,
		velocity: 13.9386111111111,
		power: 2017.98704273967,
		road: 12048.25625,
		acceleration: -0.0862037037037027
	},
	{
		id: 958,
		time: 957,
		velocity: 13.7872222222222,
		power: 702.133135238543,
		road: 12062.0922685185,
		acceleration: -0.182222222222226
	},
	{
		id: 959,
		time: 958,
		velocity: 13.5088888888889,
		power: 779.117753924695,
		road: 12075.7507407407,
		acceleration: -0.17287037037037
	},
	{
		id: 960,
		time: 959,
		velocity: 13.42,
		power: 55.2052935994899,
		road: 12089.2101388889,
		acceleration: -0.225277777777777
	},
	{
		id: 961,
		time: 960,
		velocity: 13.1113888888889,
		power: 630.065379915045,
		road: 12102.4685185185,
		acceleration: -0.176759259259258
	},
	{
		id: 962,
		time: 961,
		velocity: 12.9786111111111,
		power: -229.287280068685,
		road: 12115.5175925926,
		acceleration: -0.241851851851852
	},
	{
		id: 963,
		time: 962,
		velocity: 12.6944444444444,
		power: -962.358468050617,
		road: 12128.2965277778,
		acceleration: -0.298425925925926
	},
	{
		id: 964,
		time: 963,
		velocity: 12.2161111111111,
		power: -1846.54479649424,
		road: 12140.7411111111,
		acceleration: -0.37027777777778
	},
	{
		id: 965,
		time: 964,
		velocity: 11.8677777777778,
		power: -1098.4107435182,
		road: 12152.847962963,
		acceleration: -0.305185185185183
	},
	{
		id: 966,
		time: 965,
		velocity: 11.7788888888889,
		power: -158.557355435018,
		road: 12164.6919444444,
		acceleration: -0.220555555555556
	},
	{
		id: 967,
		time: 966,
		velocity: 11.5544444444444,
		power: -156.984736507353,
		road: 12176.3167592593,
		acceleration: -0.217777777777776
	},
	{
		id: 968,
		time: 967,
		velocity: 11.2144444444444,
		power: -800.075060487048,
		road: 12187.6956018518,
		acceleration: -0.274166666666668
	},
	{
		id: 969,
		time: 968,
		velocity: 10.9563888888889,
		power: -855.832971183731,
		road: 12198.7984722222,
		acceleration: -0.277777777777779
	},
	{
		id: 970,
		time: 969,
		velocity: 10.7211111111111,
		power: -1082.11822800851,
		road: 12209.613287037,
		acceleration: -0.298333333333332
	},
	{
		id: 971,
		time: 970,
		velocity: 10.3194444444444,
		power: -1255.1166080387,
		road: 12220.1214351852,
		acceleration: -0.315000000000001
	},
	{
		id: 972,
		time: 971,
		velocity: 10.0113888888889,
		power: -1086.62008150567,
		road: 12230.3231481481,
		acceleration: -0.29787037037037
	},
	{
		id: 973,
		time: 972,
		velocity: 9.8275,
		power: -1585.70464194169,
		road: 12240.200462963,
		acceleration: -0.350925925925926
	},
	{
		id: 974,
		time: 973,
		velocity: 9.26666666666667,
		power: -167.339689905624,
		road: 12249.8034722222,
		acceleration: -0.197685185185186
	},
	{
		id: 975,
		time: 974,
		velocity: 9.41833333333333,
		power: 100.793480537305,
		road: 12259.224537037,
		acceleration: -0.166203703703703
	},
	{
		id: 976,
		time: 975,
		velocity: 9.32888888888889,
		power: 1002.22591053376,
		road: 12268.5309259259,
		acceleration: -0.063148148148148
	},
	{
		id: 977,
		time: 976,
		velocity: 9.07722222222222,
		power: -733.721819669379,
		road: 12277.6763425926,
		acceleration: -0.258796296296294
	},
	{
		id: 978,
		time: 977,
		velocity: 8.64194444444444,
		power: -1381.84135277775,
		road: 12286.5245833333,
		acceleration: -0.335555555555557
	},
	{
		id: 979,
		time: 978,
		velocity: 8.32222222222222,
		power: -4413.83949943452,
		road: 12294.8431018518,
		acceleration: -0.723888888888889
	},
	{
		id: 980,
		time: 979,
		velocity: 6.90555555555556,
		power: -6700.13384348731,
		road: 12302.2447222222,
		acceleration: -1.10990740740741
	},
	{
		id: 981,
		time: 980,
		velocity: 5.31222222222222,
		power: -7595.53665738342,
		road: 12308.364212963,
		acceleration: -1.45435185185185
	},
	{
		id: 982,
		time: 981,
		velocity: 3.95916666666667,
		power: -4786.51534929437,
		road: 12313.1610648148,
		acceleration: -1.19092592592593
	},
	{
		id: 983,
		time: 982,
		velocity: 3.33277777777778,
		power: -1963.6882563874,
		road: 12317.027037037,
		acceleration: -0.670833333333333
	},
	{
		id: 984,
		time: 983,
		velocity: 3.29972222222222,
		power: -270.671808813696,
		road: 12320.4489351852,
		acceleration: -0.217314814814814
	},
	{
		id: 985,
		time: 984,
		velocity: 3.30722222222222,
		power: 1001.83821506913,
		road: 12323.8503703704,
		acceleration: 0.176388888888889
	},
	{
		id: 986,
		time: 985,
		velocity: 3.86194444444444,
		power: 1807.46991645781,
		road: 12327.5311111111,
		acceleration: 0.382222222222222
	},
	{
		id: 987,
		time: 986,
		velocity: 4.44638888888889,
		power: 3398.68583830676,
		road: 12331.7575925926,
		acceleration: 0.709259259259259
	},
	{
		id: 988,
		time: 987,
		velocity: 5.435,
		power: 4848.09494412401,
		road: 12336.77625,
		acceleration: 0.875092592592592
	},
	{
		id: 989,
		time: 988,
		velocity: 6.48722222222222,
		power: 4410.98507011767,
		road: 12342.560462963,
		acceleration: 0.656018518518518
	},
	{
		id: 990,
		time: 989,
		velocity: 6.41444444444444,
		power: 454.909547327483,
		road: 12348.637962963,
		acceleration: -0.0694444444444455
	},
	{
		id: 991,
		time: 990,
		velocity: 5.22666666666667,
		power: -2989.16309405348,
		road: 12354.331712963,
		acceleration: -0.698055555555555
	},
	{
		id: 992,
		time: 991,
		velocity: 4.39305555555556,
		power: -2991.48247590894,
		road: 12359.2881481481,
		acceleration: -0.776574074074073
	},
	{
		id: 993,
		time: 992,
		velocity: 4.08472222222222,
		power: -1004.73426298029,
		road: 12363.6663888889,
		acceleration: -0.379814814814814
	},
	{
		id: 994,
		time: 993,
		velocity: 4.08722222222222,
		power: 558.297485891037,
		road: 12367.85625,
		acceleration: 0.00305555555555515
	},
	{
		id: 995,
		time: 994,
		velocity: 4.40222222222222,
		power: 2648.19430709944,
		road: 12372.2926388889,
		acceleration: 0.49
	},
	{
		id: 996,
		time: 995,
		velocity: 5.55472222222222,
		power: 3788.35954104415,
		road: 12377.3012962963,
		acceleration: 0.654537037037036
	},
	{
		id: 997,
		time: 996,
		velocity: 6.05083333333333,
		power: 4966.05958844842,
		road: 12383.0210185185,
		acceleration: 0.767592592592594
	},
	{
		id: 998,
		time: 997,
		velocity: 6.705,
		power: 3184.72443294991,
		road: 12389.3157407407,
		acceleration: 0.382407407407407
	},
	{
		id: 999,
		time: 998,
		velocity: 6.70194444444444,
		power: 1545.09124094876,
		road: 12395.8502777778,
		acceleration: 0.0972222222222214
	},
	{
		id: 1000,
		time: 999,
		velocity: 6.3425,
		power: 823.317221751657,
		road: 12402.4234259259,
		acceleration: -0.0199999999999996
	},
	{
		id: 1001,
		time: 1000,
		velocity: 6.645,
		power: 3181.13818904227,
		road: 12409.1584259259,
		acceleration: 0.343703703703704
	},
	{
		id: 1002,
		time: 1001,
		velocity: 7.73305555555555,
		power: 6832.8261113755,
		road: 12416.4771296296,
		acceleration: 0.823703703703703
	},
	{
		id: 1003,
		time: 1002,
		velocity: 8.81361111111111,
		power: 8939.01195392088,
		road: 12424.6962037037,
		acceleration: 0.977037037037037
	},
	{
		id: 1004,
		time: 1003,
		velocity: 9.57611111111111,
		power: 6946.50696580376,
		road: 12433.72125,
		acceleration: 0.634907407407407
	},
	{
		id: 1005,
		time: 1004,
		velocity: 9.63777777777778,
		power: 4061.04700297023,
		road: 12443.1996296296,
		acceleration: 0.271759259259259
	},
	{
		id: 1006,
		time: 1005,
		velocity: 9.62888888888889,
		power: 2126.36799385506,
		road: 12452.8397685185,
		acceleration: 0.0517592592592599
	},
	{
		id: 1007,
		time: 1006,
		velocity: 9.73138888888889,
		power: 3065.39929789949,
		road: 12462.5805092593,
		acceleration: 0.149444444444445
	},
	{
		id: 1008,
		time: 1007,
		velocity: 10.0861111111111,
		power: 3124.73229014725,
		road: 12472.4705092593,
		acceleration: 0.149074074074075
	},
	{
		id: 1009,
		time: 1008,
		velocity: 10.0761111111111,
		power: 2043.92710144741,
		road: 12482.4507407407,
		acceleration: 0.0313888888888876
	},
	{
		id: 1010,
		time: 1009,
		velocity: 9.82555555555555,
		power: 1296.31351521764,
		road: 12492.4231481481,
		acceleration: -0.0470370370370379
	},
	{
		id: 1011,
		time: 1010,
		velocity: 9.945,
		power: 3793.62655381525,
		road: 12502.4778240741,
		acceleration: 0.211574074074074
	},
	{
		id: 1012,
		time: 1011,
		velocity: 10.7108333333333,
		power: 6368.29446235591,
		road: 12512.865787037,
		acceleration: 0.455000000000002
	},
	{
		id: 1013,
		time: 1012,
		velocity: 11.1905555555556,
		power: 8009.75331452105,
		road: 12523.7693055555,
		acceleration: 0.576111111111111
	},
	{
		id: 1014,
		time: 1013,
		velocity: 11.6733333333333,
		power: 7598.68733796481,
		road: 12535.2085648148,
		acceleration: 0.49537037037037
	},
	{
		id: 1015,
		time: 1014,
		velocity: 12.1969444444444,
		power: 6881.27052808123,
		road: 12547.0954166667,
		acceleration: 0.399814814814814
	},
	{
		id: 1016,
		time: 1015,
		velocity: 12.39,
		power: 5336.78707628676,
		road: 12559.3055092593,
		acceleration: 0.24666666666667
	},
	{
		id: 1017,
		time: 1016,
		velocity: 12.4133333333333,
		power: 4201.34868453447,
		road: 12571.7093518518,
		acceleration: 0.14083333333333
	},
	{
		id: 1018,
		time: 1017,
		velocity: 12.6194444444444,
		power: 4809.52701627588,
		road: 12584.2759259259,
		acceleration: 0.184629629629631
	},
	{
		id: 1019,
		time: 1018,
		velocity: 12.9438888888889,
		power: 7458.69747406737,
		road: 12597.1285648148,
		acceleration: 0.387500000000001
	},
	{
		id: 1020,
		time: 1019,
		velocity: 13.5758333333333,
		power: 8152.27408373334,
		road: 12610.3838425926,
		acceleration: 0.417777777777776
	},
	{
		id: 1021,
		time: 1020,
		velocity: 13.8727777777778,
		power: 6256.40976950271,
		road: 12623.9733333333,
		acceleration: 0.25064814814815
	},
	{
		id: 1022,
		time: 1021,
		velocity: 13.6958333333333,
		power: 3434.91144915192,
		road: 12637.7023148148,
		acceleration: 0.0283333333333307
	},
	{
		id: 1023,
		time: 1022,
		velocity: 13.6608333333333,
		power: 2799.32576198005,
		road: 12651.4353240741,
		acceleration: -0.0202777777777747
	},
	{
		id: 1024,
		time: 1023,
		velocity: 13.8119444444444,
		power: 5536.17862304916,
		road: 12665.2505092593,
		acceleration: 0.184629629629628
	},
	{
		id: 1025,
		time: 1024,
		velocity: 14.2497222222222,
		power: 6774.92621563562,
		road: 12679.2913425926,
		acceleration: 0.266666666666666
	},
	{
		id: 1026,
		time: 1025,
		velocity: 14.4608333333333,
		power: 5787.42619062404,
		road: 12693.5568055555,
		acceleration: 0.182592592592593
	},
	{
		id: 1027,
		time: 1026,
		velocity: 14.3597222222222,
		power: 3276.46465455589,
		road: 12707.91125,
		acceleration: -0.00462962962962798
	},
	{
		id: 1028,
		time: 1027,
		velocity: 14.2358333333333,
		power: 2648.41508155058,
		road: 12722.2385648148,
		acceleration: -0.0496296296296297
	},
	{
		id: 1029,
		time: 1028,
		velocity: 14.3119444444444,
		power: 3330.44192542067,
		road: 12736.5415740741,
		acceleration: 0.0010185185185172
	},
	{
		id: 1030,
		time: 1029,
		velocity: 14.3627777777778,
		power: 4233.22364847726,
		road: 12750.8780092593,
		acceleration: 0.0658333333333339
	},
	{
		id: 1031,
		time: 1030,
		velocity: 14.4333333333333,
		power: 4537.6494336438,
		road: 12765.2899074074,
		acceleration: 0.0850925925925949
	},
	{
		id: 1032,
		time: 1031,
		velocity: 14.5672222222222,
		power: 4349.95077772575,
		road: 12779.7786111111,
		acceleration: 0.068518518518518
	},
	{
		id: 1033,
		time: 1032,
		velocity: 14.5683333333333,
		power: 4430.90848376554,
		road: 12794.3374074074,
		acceleration: 0.0716666666666654
	},
	{
		id: 1034,
		time: 1033,
		velocity: 14.6483333333333,
		power: 3449.60308336938,
		road: 12808.9319907407,
		acceleration: -9.2592592592311E-05
	},
	{
		id: 1035,
		time: 1034,
		velocity: 14.5669444444444,
		power: 3960.7862340305,
		road: 12823.5444907407,
		acceleration: 0.0359259259259268
	},
	{
		id: 1036,
		time: 1035,
		velocity: 14.6761111111111,
		power: 1068.52443449811,
		road: 12838.0900462963,
		acceleration: -0.169814814814817
	},
	{
		id: 1037,
		time: 1036,
		velocity: 14.1388888888889,
		power: -747.312919603204,
		road: 12852.4018981481,
		acceleration: -0.297592592592594
	},
	{
		id: 1038,
		time: 1037,
		velocity: 13.6741666666667,
		power: -2870.90811670769,
		road: 12866.3386111111,
		acceleration: -0.452685185185185
	},
	{
		id: 1039,
		time: 1038,
		velocity: 13.3180555555556,
		power: -1775.00695879599,
		road: 12879.8649537037,
		acceleration: -0.368055555555555
	},
	{
		id: 1040,
		time: 1039,
		velocity: 13.0347222222222,
		power: -1218.15841147319,
		road: 12893.0461574074,
		acceleration: -0.322222222222221
	},
	{
		id: 1041,
		time: 1040,
		velocity: 12.7075,
		power: -884.457485767712,
		road: 12905.9198148148,
		acceleration: -0.29287037037037
	},
	{
		id: 1042,
		time: 1041,
		velocity: 12.4394444444444,
		power: -1309.18404836151,
		road: 12918.484212963,
		acceleration: -0.325648148148151
	},
	{
		id: 1043,
		time: 1042,
		velocity: 12.0577777777778,
		power: -1809.50239548511,
		road: 12930.7023611111,
		acceleration: -0.36685185185185
	},
	{
		id: 1044,
		time: 1043,
		velocity: 11.6069444444444,
		power: -1489.47305038685,
		road: 12942.5678703704,
		acceleration: -0.338425925925925
	},
	{
		id: 1045,
		time: 1044,
		velocity: 11.4241666666667,
		power: -785.056991936935,
		road: 12954.1271759259,
		acceleration: -0.273981481481483
	},
	{
		id: 1046,
		time: 1045,
		velocity: 11.2358333333333,
		power: -43.4748552824929,
		road: 12965.4476388889,
		acceleration: -0.203703703703702
	},
	{
		id: 1047,
		time: 1046,
		velocity: 10.9958333333333,
		power: -179.55007145158,
		road: 12976.5592592593,
		acceleration: -0.213981481481483
	},
	{
		id: 1048,
		time: 1047,
		velocity: 10.7822222222222,
		power: -456.722791415569,
		road: 12987.4447685185,
		acceleration: -0.238240740740739
	},
	{
		id: 1049,
		time: 1048,
		velocity: 10.5211111111111,
		power: 531.239760757792,
		road: 12998.14125,
		acceleration: -0.139814814814816
	},
	{
		id: 1050,
		time: 1049,
		velocity: 10.5763888888889,
		power: 2901.02561874067,
		road: 13008.8145833333,
		acceleration: 0.0935185185185201
	},
	{
		id: 1051,
		time: 1050,
		velocity: 11.0627777777778,
		power: 4513.68092720671,
		road: 13019.6562037037,
		acceleration: 0.243055555555555
	},
	{
		id: 1052,
		time: 1051,
		velocity: 11.2502777777778,
		power: 6811.35257136811,
		road: 13030.8397685185,
		acceleration: 0.440833333333332
	},
	{
		id: 1053,
		time: 1052,
		velocity: 11.8988888888889,
		power: 8005.32096757214,
		road: 13042.5015740741,
		acceleration: 0.51564814814815
	},
	{
		id: 1054,
		time: 1053,
		velocity: 12.6097222222222,
		power: 9403.62338313954,
		road: 13054.7187962963,
		acceleration: 0.595185185185183
	},
	{
		id: 1055,
		time: 1054,
		velocity: 13.0358333333333,
		power: 9324.18997742097,
		road: 13067.5058333333,
		acceleration: 0.544444444444444
	},
	{
		id: 1056,
		time: 1055,
		velocity: 13.5322222222222,
		power: 6998.61513567472,
		road: 13080.7293055555,
		acceleration: 0.328425925925927
	},
	{
		id: 1057,
		time: 1056,
		velocity: 13.595,
		power: 5467.39478066483,
		road: 13094.2143518518,
		acceleration: 0.194722222222222
	},
	{
		id: 1058,
		time: 1057,
		velocity: 13.62,
		power: 2342.36126065985,
		road: 13107.771712963,
		acceleration: -0.0500925925925948
	},
	{
		id: 1059,
		time: 1058,
		velocity: 13.3819444444444,
		power: 101.12532627238,
		road: 13121.1934722222,
		acceleration: -0.22111111111111
	},
	{
		id: 1060,
		time: 1059,
		velocity: 12.9316666666667,
		power: -1610.16722191073,
		road: 13134.3281018518,
		acceleration: -0.353148148148147
	},
	{
		id: 1061,
		time: 1060,
		velocity: 12.5605555555556,
		power: -1752.79541245421,
		road: 13147.104537037,
		acceleration: -0.363240740740739
	},
	{
		id: 1062,
		time: 1061,
		velocity: 12.2922222222222,
		power: -1995.13215686344,
		road: 13159.507962963,
		acceleration: -0.382777777777781
	},
	{
		id: 1063,
		time: 1062,
		velocity: 11.7833333333333,
		power: -1734.46628827157,
		road: 13171.5399074074,
		acceleration: -0.360185185185182
	},
	{
		id: 1064,
		time: 1063,
		velocity: 11.48,
		power: -1744.06214390775,
		road: 13183.2112962963,
		acceleration: -0.360925925925926
	},
	{
		id: 1065,
		time: 1064,
		velocity: 11.2094444444444,
		power: -1584.52048547921,
		road: 13194.5289814815,
		acceleration: -0.346481481481481
	},
	{
		id: 1066,
		time: 1065,
		velocity: 10.7438888888889,
		power: -1885.68088989032,
		road: 13205.4856018518,
		acceleration: -0.37564814814815
	},
	{
		id: 1067,
		time: 1066,
		velocity: 10.3530555555556,
		power: -1019.75364675429,
		road: 13216.1085185185,
		acceleration: -0.29175925925926
	},
	{
		id: 1068,
		time: 1067,
		velocity: 10.3341666666667,
		power: 833.17065120884,
		road: 13226.5331481481,
		acceleration: -0.104814814814814
	},
	{
		id: 1069,
		time: 1068,
		velocity: 10.4294444444444,
		power: 1679.79174541517,
		road: 13236.8964351852,
		acceleration: -0.0178703703703693
	},
	{
		id: 1070,
		time: 1069,
		velocity: 10.2994444444444,
		power: 418.017319646834,
		road: 13247.1786111111,
		acceleration: -0.144351851851853
	},
	{
		id: 1071,
		time: 1070,
		velocity: 9.90111111111111,
		power: -630.557766186951,
		road: 13257.2634259259,
		acceleration: -0.250370370370369
	},
	{
		id: 1072,
		time: 1071,
		velocity: 9.67833333333333,
		power: -3713.34139143122,
		road: 13266.9313888889,
		acceleration: -0.583333333333336
	},
	{
		id: 1073,
		time: 1072,
		velocity: 8.54944444444444,
		power: -6155.39450567182,
		road: 13275.8594444444,
		acceleration: -0.89648148148148
	},
	{
		id: 1074,
		time: 1073,
		velocity: 7.21166666666667,
		power: -7514.28421232979,
		road: 13283.7580555556,
		acceleration: -1.16240740740741
	},
	{
		id: 1075,
		time: 1074,
		velocity: 6.19111111111111,
		power: -5475.92398826373,
		road: 13290.5763888889,
		acceleration: -0.998148148148147
	},
	{
		id: 1076,
		time: 1075,
		velocity: 5.555,
		power: -3110.40366026917,
		road: 13296.5479166667,
		acceleration: -0.695462962962964
	},
	{
		id: 1077,
		time: 1076,
		velocity: 5.12527777777778,
		power: -2485.61728173939,
		road: 13301.8535648148,
		acceleration: -0.636296296296296
	},
	{
		id: 1078,
		time: 1077,
		velocity: 4.28222222222222,
		power: -2234.52019858746,
		road: 13306.5191203704,
		acceleration: -0.643888888888888
	},
	{
		id: 1079,
		time: 1078,
		velocity: 3.62333333333333,
		power: -1754.29313513748,
		road: 13310.5661574074,
		acceleration: -0.593148148148149
	},
	{
		id: 1080,
		time: 1079,
		velocity: 3.34583333333333,
		power: -678.782285056511,
		road: 13314.149537037,
		acceleration: -0.334166666666667
	},
	{
		id: 1081,
		time: 1080,
		velocity: 3.27972222222222,
		power: -58.2300491032545,
		road: 13317.4898148148,
		acceleration: -0.152037037037037
	},
	{
		id: 1082,
		time: 1081,
		velocity: 3.16722222222222,
		power: 156.064566608052,
		road: 13320.712962963,
		acceleration: -0.0822222222222218
	},
	{
		id: 1083,
		time: 1082,
		velocity: 3.09916666666667,
		power: 187.226554054925,
		road: 13323.8598611111,
		acceleration: -0.0702777777777781
	},
	{
		id: 1084,
		time: 1083,
		velocity: 3.06888888888889,
		power: 78.4186657238043,
		road: 13326.9187962963,
		acceleration: -0.105648148148148
	},
	{
		id: 1085,
		time: 1084,
		velocity: 2.85027777777778,
		power: 140.764774510523,
		road: 13329.88375,
		acceleration: -0.0823148148148141
	},
	{
		id: 1086,
		time: 1085,
		velocity: 2.85222222222222,
		power: 12.4736483422057,
		road: 13332.7438425926,
		acceleration: -0.127407407407407
	},
	{
		id: 1087,
		time: 1086,
		velocity: 2.68666666666667,
		power: 110.960881860506,
		road: 13335.4956481481,
		acceleration: -0.0891666666666673
	},
	{
		id: 1088,
		time: 1087,
		velocity: 2.58277777777778,
		power: 46.9680418926272,
		road: 13338.1465277778,
		acceleration: -0.112685185185185
	},
	{
		id: 1089,
		time: 1088,
		velocity: 2.51416666666667,
		power: 109.738720999755,
		road: 13340.6981944444,
		acceleration: -0.0857407407407411
	},
	{
		id: 1090,
		time: 1089,
		velocity: 2.42944444444444,
		power: 446.24697790084,
		road: 13343.234212963,
		acceleration: 0.0544444444444445
	},
	{
		id: 1091,
		time: 1090,
		velocity: 2.74611111111111,
		power: 1117.20314013326,
		road: 13345.9485648148,
		acceleration: 0.302222222222222
	},
	{
		id: 1092,
		time: 1091,
		velocity: 3.42083333333333,
		power: 1500.88814068052,
		road: 13349.0062962963,
		acceleration: 0.384537037037036
	},
	{
		id: 1093,
		time: 1092,
		velocity: 3.58305555555556,
		power: 1561.93529248338,
		road: 13352.4296296296,
		acceleration: 0.346666666666667
	},
	{
		id: 1094,
		time: 1093,
		velocity: 3.78611111111111,
		power: 547.231414052988,
		road: 13356.0387962963,
		acceleration: 0.0250000000000004
	},
	{
		id: 1095,
		time: 1094,
		velocity: 3.49583333333333,
		power: 600.872430621566,
		road: 13359.6799537037,
		acceleration: 0.0389814814814815
	},
	{
		id: 1096,
		time: 1095,
		velocity: 3.7,
		power: 287.260746847311,
		road: 13363.3148148148,
		acceleration: -0.051574074074074
	},
	{
		id: 1097,
		time: 1096,
		velocity: 3.63138888888889,
		power: 413.403272846189,
		road: 13366.9169907407,
		acceleration: -0.0137962962962965
	},
	{
		id: 1098,
		time: 1097,
		velocity: 3.45444444444444,
		power: -470.735332076894,
		road: 13370.3734722222,
		acceleration: -0.277592592592593
	},
	{
		id: 1099,
		time: 1098,
		velocity: 2.86722222222222,
		power: -822.603590855708,
		road: 13373.485462963,
		acceleration: -0.411388888888888
	},
	{
		id: 1100,
		time: 1099,
		velocity: 2.39722222222222,
		power: -1712.34842707305,
		road: 13375.9618518518,
		acceleration: -0.859814814814815
	},
	{
		id: 1101,
		time: 1100,
		velocity: 0.875,
		power: -1058.33731974461,
		road: 13377.6039814815,
		acceleration: -0.808703703703704
	},
	{
		id: 1102,
		time: 1101,
		velocity: 0.441111111111111,
		power: -533.200884597378,
		road: 13378.4422222222,
		acceleration: -0.799074074074074
	},
	{
		id: 1103,
		time: 1102,
		velocity: 0,
		power: 97.3203777921102,
		road: 13378.92375,
		acceleration: 0.0856481481481481
	},
	{
		id: 1104,
		time: 1103,
		velocity: 1.13194444444444,
		power: 258.787922109814,
		road: 13379.5893518518,
		acceleration: 0.2825
	},
	{
		id: 1105,
		time: 1104,
		velocity: 1.28861111111111,
		power: 454.043088100554,
		road: 13380.5751388889,
		acceleration: 0.35787037037037
	},
	{
		id: 1106,
		time: 1105,
		velocity: 1.07361111111111,
		power: 65.5682254707695,
		road: 13381.7063425926,
		acceleration: -0.0670370370370372
	},
	{
		id: 1107,
		time: 1106,
		velocity: 0.930833333333333,
		power: -113.195740247671,
		road: 13382.6786111111,
		acceleration: -0.250833333333333
	},
	{
		id: 1108,
		time: 1107,
		velocity: 0.536111111111111,
		power: -145.664203286262,
		road: 13383.3465277778,
		acceleration: -0.35787037037037
	},
	{
		id: 1109,
		time: 1108,
		velocity: 0,
		power: -57.7922046608803,
		road: 13383.6803703704,
		acceleration: -0.310277777777778
	},
	{
		id: 1110,
		time: 1109,
		velocity: 0,
		power: -4.33180912930474,
		road: 13383.7697222222,
		acceleration: -0.178703703703704
	},
	{
		id: 1111,
		time: 1110,
		velocity: 0,
		power: 0,
		road: 13383.7697222222,
		acceleration: 0
	},
	{
		id: 1112,
		time: 1111,
		velocity: 0,
		power: 0,
		road: 13383.7697222222,
		acceleration: 0
	},
	{
		id: 1113,
		time: 1112,
		velocity: 0,
		power: 0,
		road: 13383.7697222222,
		acceleration: 0
	},
	{
		id: 1114,
		time: 1113,
		velocity: 0,
		power: 0,
		road: 13383.7697222222,
		acceleration: 0
	},
	{
		id: 1115,
		time: 1114,
		velocity: 0,
		power: 0,
		road: 13383.7697222222,
		acceleration: 0
	},
	{
		id: 1116,
		time: 1115,
		velocity: 0,
		power: 469.146423700881,
		road: 13384.2363425926,
		acceleration: 0.933240740740741
	},
	{
		id: 1117,
		time: 1116,
		velocity: 2.79972222222222,
		power: 2159.41191561348,
		road: 13385.8227777778,
		acceleration: 1.30638888888889
	},
	{
		id: 1118,
		time: 1117,
		velocity: 3.91916666666667,
		power: 4683.68603535263,
		road: 13388.8194907407,
		acceleration: 1.51416666666667
	},
	{
		id: 1119,
		time: 1118,
		velocity: 4.5425,
		power: 2662.73385294688,
		road: 13392.8526388889,
		acceleration: 0.558703703703704
	},
	{
		id: 1120,
		time: 1119,
		velocity: 4.47583333333333,
		power: 1037.50167529973,
		road: 13397.2211111111,
		acceleration: 0.111944444444444
	},
	{
		id: 1121,
		time: 1120,
		velocity: 4.255,
		power: 430.049437661406,
		road: 13401.6277777778,
		acceleration: -0.0355555555555549
	},
	{
		id: 1122,
		time: 1121,
		velocity: 4.43583333333333,
		power: 2438.93479833391,
		road: 13406.2262037037,
		acceleration: 0.419074074074074
	},
	{
		id: 1123,
		time: 1122,
		velocity: 5.73305555555556,
		power: 2682.47800811558,
		road: 13411.2446759259,
		acceleration: 0.421018518518518
	},
	{
		id: 1124,
		time: 1123,
		velocity: 5.51805555555555,
		power: 2124.89079984618,
		road: 13416.6102314815,
		acceleration: 0.273148148148148
	},
	{
		id: 1125,
		time: 1124,
		velocity: 5.25527777777778,
		power: -746.956804642976,
		road: 13421.9672222222,
		acceleration: -0.290277777777778
	},
	{
		id: 1126,
		time: 1125,
		velocity: 4.86222222222222,
		power: -1298.65663351606,
		road: 13426.9717592593,
		acceleration: -0.414629629629629
	},
	{
		id: 1127,
		time: 1126,
		velocity: 4.27416666666667,
		power: -3887.03462361364,
		road: 13431.2181944444,
		acceleration: -1.10157407407407
	},
	{
		id: 1128,
		time: 1127,
		velocity: 1.95055555555556,
		power: -3754.80965251119,
		road: 13434.1794444444,
		acceleration: -1.4687962962963
	},
	{
		id: 1129,
		time: 1128,
		velocity: 0.455833333333333,
		power: -1860.6573713785,
		road: 13435.6939351852,
		acceleration: -1.42472222222222
	},
	{
		id: 1130,
		time: 1129,
		velocity: 0,
		power: -236.197487291952,
		road: 13436.1709722222,
		acceleration: -0.650185185185185
	},
	{
		id: 1131,
		time: 1130,
		velocity: 0,
		power: 239.552370592622,
		road: 13436.573287037,
		acceleration: 0.500740740740741
	},
	{
		id: 1132,
		time: 1131,
		velocity: 1.95805555555556,
		power: 1743.93963308938,
		road: 13437.8706018518,
		acceleration: 1.28925925925926
	},
	{
		id: 1133,
		time: 1132,
		velocity: 3.86777777777778,
		power: 3596.3775590115,
		road: 13440.4746296296,
		acceleration: 1.32416666666667
	},
	{
		id: 1134,
		time: 1133,
		velocity: 3.9725,
		power: 2304.69288827539,
		road: 13444.01625,
		acceleration: 0.551018518518518
	},
	{
		id: 1135,
		time: 1134,
		velocity: 3.61111111111111,
		power: -492.191376206044,
		road: 13447.6954166667,
		acceleration: -0.275925925925926
	},
	{
		id: 1136,
		time: 1135,
		velocity: 3.04,
		power: -1152.48761876835,
		road: 13450.9853240741,
		acceleration: -0.502592592592593
	},
	{
		id: 1137,
		time: 1136,
		velocity: 2.46472222222222,
		power: -1325.89602193449,
		road: 13453.7008333333,
		acceleration: -0.646203703703704
	},
	{
		id: 1138,
		time: 1137,
		velocity: 1.6725,
		power: -833.650481823639,
		road: 13455.8209722222,
		acceleration: -0.544537037037037
	},
	{
		id: 1139,
		time: 1138,
		velocity: 1.40638888888889,
		power: -453.051927204772,
		road: 13457.4584722222,
		acceleration: -0.420740740740741
	},
	{
		id: 1140,
		time: 1139,
		velocity: 1.2025,
		power: -104.14633319076,
		road: 13458.7798148148,
		acceleration: -0.211574074074074
	},
	{
		id: 1141,
		time: 1140,
		velocity: 1.03777777777778,
		power: -85.0296768640936,
		road: 13459.8909259259,
		acceleration: -0.208888888888889
	},
	{
		id: 1142,
		time: 1141,
		velocity: 0.779722222222222,
		power: -57.0955412024927,
		road: 13460.8005092593,
		acceleration: -0.194166666666667
	},
	{
		id: 1143,
		time: 1142,
		velocity: 0.62,
		power: -132.24553120582,
		road: 13461.4400462963,
		acceleration: -0.345925925925926
	},
	{
		id: 1144,
		time: 1143,
		velocity: 0,
		power: -42.20781462134,
		road: 13461.7766666667,
		acceleration: -0.259907407407407
	},
	{
		id: 1145,
		time: 1144,
		velocity: 0,
		power: -7.74706315789474,
		road: 13461.88,
		acceleration: -0.206666666666667
	},
	{
		id: 1146,
		time: 1145,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1147,
		time: 1146,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1148,
		time: 1147,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1149,
		time: 1148,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1150,
		time: 1149,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1151,
		time: 1150,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1152,
		time: 1151,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1153,
		time: 1152,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1154,
		time: 1153,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1155,
		time: 1154,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1156,
		time: 1155,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1157,
		time: 1156,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1158,
		time: 1157,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1159,
		time: 1158,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1160,
		time: 1159,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1161,
		time: 1160,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1162,
		time: 1161,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1163,
		time: 1162,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1164,
		time: 1163,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1165,
		time: 1164,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1166,
		time: 1165,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1167,
		time: 1166,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1168,
		time: 1167,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1169,
		time: 1168,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1170,
		time: 1169,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1171,
		time: 1170,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1172,
		time: 1171,
		velocity: 0,
		power: 0,
		road: 13461.88,
		acceleration: 0
	},
	{
		id: 1173,
		time: 1172,
		velocity: 0,
		power: 285.450824506808,
		road: 13462.2375,
		acceleration: 0.715
	},
	{
		id: 1174,
		time: 1173,
		velocity: 2.145,
		power: 1439.19190641036,
		road: 13463.4928703704,
		acceleration: 1.08074074074074
	},
	{
		id: 1175,
		time: 1174,
		velocity: 3.24222222222222,
		power: 4559.11934327485,
		road: 13466.1328240741,
		acceleration: 1.68842592592593
	},
	{
		id: 1176,
		time: 1175,
		velocity: 5.06527777777778,
		power: 4914.96363901304,
		road: 13470.1869444444,
		acceleration: 1.13990740740741
	},
	{
		id: 1177,
		time: 1176,
		velocity: 5.56472222222222,
		power: 4847.17706780741,
		road: 13475.2444444444,
		acceleration: 0.866851851851851
	},
	{
		id: 1178,
		time: 1177,
		velocity: 5.84277777777778,
		power: 5954.91246999688,
		road: 13481.1886111111,
		acceleration: 0.906481481481482
	},
	{
		id: 1179,
		time: 1178,
		velocity: 7.78472222222222,
		power: 10890.2746017773,
		road: 13488.3115277778,
		acceleration: 1.45101851851852
	},
	{
		id: 1180,
		time: 1179,
		velocity: 9.91777777777778,
		power: 13151.4181057399,
		road: 13496.8813888889,
		acceleration: 1.44287037037037
	},
	{
		id: 1181,
		time: 1180,
		velocity: 10.1713888888889,
		power: 11151.8495448769,
		road: 13506.679212963,
		acceleration: 1.01305555555556
	},
	{
		id: 1182,
		time: 1181,
		velocity: 10.8238888888889,
		power: 5760.35870538968,
		road: 13517.1766666667,
		acceleration: 0.386203703703703
	},
	{
		id: 1183,
		time: 1182,
		velocity: 11.0763888888889,
		power: 5359.87277835727,
		road: 13528.0293518519,
		acceleration: 0.324259259259257
	},
	{
		id: 1184,
		time: 1183,
		velocity: 11.1441666666667,
		power: 3920.62903797729,
		road: 13539.1309259259,
		acceleration: 0.173518518518518
	},
	{
		id: 1185,
		time: 1184,
		velocity: 11.3444444444444,
		power: 1960.7426540118,
		road: 13550.3122685185,
		acceleration: -0.0139814814814798
	},
	{
		id: 1186,
		time: 1185,
		velocity: 11.0344444444444,
		power: 2502.7619451138,
		road: 13561.5048611111,
		acceleration: 0.0364814814814789
	},
	{
		id: 1187,
		time: 1186,
		velocity: 11.2536111111111,
		power: 3979.2425359211,
		road: 13572.8007407407,
		acceleration: 0.170092592592596
	},
	{
		id: 1188,
		time: 1187,
		velocity: 11.8547222222222,
		power: 7433.56772870821,
		road: 13584.415462963,
		acceleration: 0.46759259259259
	},
	{
		id: 1189,
		time: 1188,
		velocity: 12.4372222222222,
		power: 8479.05279070896,
		road: 13596.5258796296,
		acceleration: 0.523796296296297
	},
	{
		id: 1190,
		time: 1189,
		velocity: 12.825,
		power: 7689.16157205774,
		road: 13609.11,
		acceleration: 0.423611111111109
	},
	{
		id: 1191,
		time: 1190,
		velocity: 13.1255555555556,
		power: 7272.65347302723,
		road: 13622.088287037,
		acceleration: 0.364722222222225
	},
	{
		id: 1192,
		time: 1191,
		velocity: 13.5313888888889,
		power: 7952.51689751125,
		road: 13635.446712963,
		acceleration: 0.395555555555555
	},
	{
		id: 1193,
		time: 1192,
		velocity: 14.0116666666667,
		power: 9293.72201533128,
		road: 13649.2384259259,
		acceleration: 0.47101851851852
	},
	{
		id: 1194,
		time: 1193,
		velocity: 14.5386111111111,
		power: 9577.03898304467,
		road: 13663.49625,
		acceleration: 0.461203703703703
	},
	{
		id: 1195,
		time: 1194,
		velocity: 14.915,
		power: 6020.10985216285,
		road: 13678.0771296296,
		acceleration: 0.184907407407406
	},
	{
		id: 1196,
		time: 1195,
		velocity: 14.5663888888889,
		power: 2062.16026266371,
		road: 13692.7002777778,
		acceleration: -0.100370370370369
	},
	{
		id: 1197,
		time: 1196,
		velocity: 14.2375,
		power: -284.811211939091,
		road: 13707.1404166667,
		acceleration: -0.265648148148149
	},
	{
		id: 1198,
		time: 1197,
		velocity: 14.1180555555556,
		power: 1169.2609403356,
		road: 13721.3699537037,
		acceleration: -0.155555555555555
	},
	{
		id: 1199,
		time: 1198,
		velocity: 14.0997222222222,
		power: 4711.77484862843,
		road: 13735.5747685185,
		acceleration: 0.106111111111112
	},
	{
		id: 1200,
		time: 1199,
		velocity: 14.5558333333333,
		power: 7044.74524302762,
		road: 13749.9667592593,
		acceleration: 0.26824074074074
	},
	{
		id: 1201,
		time: 1200,
		velocity: 14.9227777777778,
		power: 8402.15855921274,
		road: 13764.6673611111,
		acceleration: 0.348981481481481
	},
	{
		id: 1202,
		time: 1201,
		velocity: 15.1466666666667,
		power: 7257.76129252595,
		road: 13779.6684722222,
		acceleration: 0.252037037037038
	},
	{
		id: 1203,
		time: 1202,
		velocity: 15.3119444444444,
		power: 10504.6815098914,
		road: 13795.0233333333,
		acceleration: 0.455462962962962
	},
	{
		id: 1204,
		time: 1203,
		velocity: 16.2891666666667,
		power: 16346.2185260224,
		road: 13811.0050925926,
		acceleration: 0.798333333333334
	},
	{
		id: 1205,
		time: 1204,
		velocity: 17.5416666666667,
		power: 28835.9689945952,
		road: 13828.1201851852,
		acceleration: 1.46833333333333
	},
	{
		id: 1206,
		time: 1205,
		velocity: 19.7169444444444,
		power: 33030.1083833856,
		road: 13846.7345833333,
		acceleration: 1.53027777777778
	},
	{
		id: 1207,
		time: 1206,
		velocity: 20.88,
		power: 36077.220658977,
		road: 13866.8708796296,
		acceleration: 1.51351851851852
	},
	{
		id: 1208,
		time: 1207,
		velocity: 22.0822222222222,
		power: 25233.1917513578,
		road: 13888.1893981481,
		acceleration: 0.850925925925928
	},
	{
		id: 1209,
		time: 1208,
		velocity: 22.2697222222222,
		power: 26909.8266181926,
		road: 13910.3637037037,
		acceleration: 0.860648148148147
	},
	{
		id: 1210,
		time: 1209,
		velocity: 23.4619444444444,
		power: 21485.8919212188,
		road: 13933.2464351852,
		acceleration: 0.556203703703702
	},
	{
		id: 1211,
		time: 1210,
		velocity: 23.7508333333333,
		power: 21549.215311646,
		road: 13956.6683796296,
		acceleration: 0.522222222222222
	},
	{
		id: 1212,
		time: 1211,
		velocity: 23.8363888888889,
		power: 8814.21432231743,
		road: 13980.3243518519,
		acceleration: -0.0541666666666636
	},
	{
		id: 1213,
		time: 1212,
		velocity: 23.2994444444444,
		power: 1857.17201825836,
		road: 14003.7760648148,
		acceleration: -0.354351851851852
	},
	{
		id: 1214,
		time: 1213,
		velocity: 22.6877777777778,
		power: -3658.87290635372,
		road: 14026.7553703704,
		acceleration: -0.590462962962963
	},
	{
		id: 1215,
		time: 1214,
		velocity: 22.065,
		power: -3713.69414647328,
		road: 14049.1481944444,
		acceleration: -0.5825
	},
	{
		id: 1216,
		time: 1215,
		velocity: 21.5519444444444,
		power: 939.508548271539,
		road: 14071.0731018518,
		acceleration: -0.353333333333335
	},
	{
		id: 1217,
		time: 1216,
		velocity: 21.6277777777778,
		power: 7110.42863259838,
		road: 14092.795462963,
		acceleration: -0.0517592592592599
	},
	{
		id: 1218,
		time: 1217,
		velocity: 21.9097222222222,
		power: 7499.20162917609,
		road: 14114.47625,
		acceleration: -0.0313888888888911
	},
	{
		id: 1219,
		time: 1218,
		velocity: 21.4577777777778,
		power: 6198.78497238997,
		road: 14136.0955555556,
		acceleration: -0.0915740740740674
	},
	{
		id: 1220,
		time: 1219,
		velocity: 21.3530555555556,
		power: 3378.47069310382,
		road: 14157.5577777778,
		acceleration: -0.222592592592598
	},
	{
		id: 1221,
		time: 1220,
		velocity: 21.2419444444444,
		power: 1987.44631890508,
		road: 14178.7672222222,
		acceleration: -0.282962962962962
	},
	{
		id: 1222,
		time: 1221,
		velocity: 20.6088888888889,
		power: -5456.1018344484,
		road: 14199.5131018518,
		acceleration: -0.644166666666663
	},
	{
		id: 1223,
		time: 1222,
		velocity: 19.4205555555556,
		power: -15710.1076567694,
		road: 14219.3489814815,
		acceleration: -1.17583333333334
	},
	{
		id: 1224,
		time: 1223,
		velocity: 17.7144444444444,
		power: -25226.2968652467,
		road: 14237.720787037,
		acceleration: -1.75231481481481
	},
	{
		id: 1225,
		time: 1224,
		velocity: 15.3519444444444,
		power: -29625.7531755447,
		road: 14254.1326388889,
		acceleration: -2.16759259259259
	},
	{
		id: 1226,
		time: 1225,
		velocity: 12.9177777777778,
		power: -23256.7865839166,
		road: 14268.4906944444,
		acceleration: -1.94
	},
	{
		id: 1227,
		time: 1226,
		velocity: 11.8944444444444,
		power: -20472.1142089152,
		road: 14280.9072685185,
		acceleration: -1.94296296296296
	},
	{
		id: 1228,
		time: 1227,
		velocity: 9.52305555555556,
		power: -18869.2051611474,
		road: 14291.3058333333,
		acceleration: -2.09305555555556
	},
	{
		id: 1229,
		time: 1228,
		velocity: 6.63861111111111,
		power: -16872.9402913092,
		road: 14299.4921759259,
		acceleration: -2.33138888888889
	},
	{
		id: 1230,
		time: 1229,
		velocity: 4.90027777777778,
		power: -7981.29922010581,
		road: 14305.769212963,
		acceleration: -1.48722222222222
	},
	{
		id: 1231,
		time: 1230,
		velocity: 5.06138888888889,
		power: -2321.87158020857,
		road: 14310.9975462963,
		acceleration: -0.610185185185185
	},
	{
		id: 1232,
		time: 1231,
		velocity: 4.80805555555556,
		power: -979.564066135013,
		road: 14315.7420833333,
		acceleration: -0.357407407407408
	},
	{
		id: 1233,
		time: 1232,
		velocity: 3.82805555555556,
		power: -2031.4504537589,
		road: 14319.9871759259,
		acceleration: -0.641481481481482
	},
	{
		id: 1234,
		time: 1233,
		velocity: 3.13694444444444,
		power: -2544.63012478632,
		road: 14323.4582407407,
		acceleration: -0.906574074074074
	},
	{
		id: 1235,
		time: 1234,
		velocity: 2.08833333333333,
		power: -1721.612917979,
		road: 14326.0618981481,
		acceleration: -0.828240740740741
	},
	{
		id: 1236,
		time: 1235,
		velocity: 1.34333333333333,
		power: -1448.51657570856,
		road: 14327.7286111111,
		acceleration: -1.04564814814815
	},
	{
		id: 1237,
		time: 1236,
		velocity: 0,
		power: -428.593607413301,
		road: 14328.5244444444,
		acceleration: -0.696111111111111
	},
	{
		id: 1238,
		time: 1237,
		velocity: 0,
		power: -67.9262391812866,
		road: 14328.7483333333,
		acceleration: -0.447777777777778
	},
	{
		id: 1239,
		time: 1238,
		velocity: 0,
		power: 0,
		road: 14328.7483333333,
		acceleration: 0
	},
	{
		id: 1240,
		time: 1239,
		velocity: 0,
		power: 0,
		road: 14328.7483333333,
		acceleration: 0
	},
	{
		id: 1241,
		time: 1240,
		velocity: 0,
		power: 0,
		road: 14328.7483333333,
		acceleration: 0
	},
	{
		id: 1242,
		time: 1241,
		velocity: 0,
		power: 0,
		road: 14328.7483333333,
		acceleration: 0
	},
	{
		id: 1243,
		time: 1242,
		velocity: 0,
		power: 0,
		road: 14328.7483333333,
		acceleration: 0
	},
	{
		id: 1244,
		time: 1243,
		velocity: 0,
		power: 68.791003233208,
		road: 14328.9096296296,
		acceleration: 0.322592592592593
	},
	{
		id: 1245,
		time: 1244,
		velocity: 0.967777777777778,
		power: 852.872332257088,
		road: 14329.722037037,
		acceleration: 0.97962962962963
	},
	{
		id: 1246,
		time: 1245,
		velocity: 2.93888888888889,
		power: 4719.565666979,
		road: 14332.0344907407,
		acceleration: 2.02046296296296
	},
	{
		id: 1247,
		time: 1246,
		velocity: 6.06138888888889,
		power: 8477.85104188794,
		road: 14336.3276851852,
		acceleration: 1.94101851851852
	},
	{
		id: 1248,
		time: 1247,
		velocity: 6.79083333333333,
		power: 12720.6378915722,
		road: 14342.5854166667,
		acceleration: 1.98805555555556
	},
	{
		id: 1249,
		time: 1248,
		velocity: 8.90305555555556,
		power: 10285.9222820971,
		road: 14350.4437037037,
		acceleration: 1.21305555555555
	},
	{
		id: 1250,
		time: 1249,
		velocity: 9.70055555555555,
		power: 12320.3609692083,
		road: 14359.5331018519,
		acceleration: 1.24916666666667
	},
	{
		id: 1251,
		time: 1250,
		velocity: 10.5383333333333,
		power: 8324.98273156553,
		road: 14369.5892592593,
		acceleration: 0.684351851851853
	},
	{
		id: 1252,
		time: 1251,
		velocity: 10.9561111111111,
		power: 6911.66222876444,
		road: 14380.2325925926,
		acceleration: 0.49
	},
	{
		id: 1253,
		time: 1252,
		velocity: 11.1705555555556,
		power: 5211.93545007236,
		road: 14391.2705092593,
		acceleration: 0.299166666666666
	},
	{
		id: 1254,
		time: 1253,
		velocity: 11.4358333333333,
		power: 5015.58966796072,
		road: 14402.5905092593,
		acceleration: 0.265000000000001
	},
	{
		id: 1255,
		time: 1254,
		velocity: 11.7511111111111,
		power: 6105.67317887597,
		road: 14414.2165277778,
		acceleration: 0.347037037037039
	},
	{
		id: 1256,
		time: 1255,
		velocity: 12.2116666666667,
		power: 6833.02247920363,
		road: 14426.2104166667,
		acceleration: 0.388703703703701
	},
	{
		id: 1257,
		time: 1256,
		velocity: 12.6019444444444,
		power: 7542.94203542303,
		road: 14438.6103703704,
		acceleration: 0.423425925925926
	},
	{
		id: 1258,
		time: 1257,
		velocity: 13.0213888888889,
		power: 6864.78012550993,
		road: 14451.3936111111,
		acceleration: 0.343148148148149
	},
	{
		id: 1259,
		time: 1258,
		velocity: 13.2411111111111,
		power: 6025.36164368959,
		road: 14464.4776851852,
		acceleration: 0.258518518518517
	},
	{
		id: 1260,
		time: 1259,
		velocity: 13.3775,
		power: 5059.50871100974,
		road: 14477.776712963,
		acceleration: 0.17138888888889
	},
	{
		id: 1261,
		time: 1260,
		velocity: 13.5355555555556,
		power: 3495.67072712359,
		road: 14491.1836111111,
		acceleration: 0.0443518518518502
	},
	{
		id: 1262,
		time: 1261,
		velocity: 13.3741666666667,
		power: 1518.45932432571,
		road: 14504.5580092593,
		acceleration: -0.10935185185185
	},
	{
		id: 1263,
		time: 1262,
		velocity: 13.0494444444444,
		power: -2433.07212403619,
		road: 14517.6683333333,
		acceleration: -0.418796296296298
	},
	{
		id: 1264,
		time: 1263,
		velocity: 12.2791666666667,
		power: -5424.20292249425,
		road: 14530.2348148148,
		acceleration: -0.668888888888889
	},
	{
		id: 1265,
		time: 1264,
		velocity: 11.3675,
		power: -4980.82841859045,
		road: 14542.1438888889,
		acceleration: -0.645925925925928
	},
	{
		id: 1266,
		time: 1265,
		velocity: 11.1116666666667,
		power: -5295.76590410455,
		road: 14553.3835648148,
		acceleration: -0.692870370370366
	},
	{
		id: 1267,
		time: 1266,
		velocity: 10.2005555555556,
		power: -5365.01880977279,
		road: 14563.914537037,
		acceleration: -0.724537037037038
	},
	{
		id: 1268,
		time: 1267,
		velocity: 9.19388888888889,
		power: -5771.36435326018,
		road: 14573.6825,
		acceleration: -0.801481481481481
	},
	{
		id: 1269,
		time: 1268,
		velocity: 8.70722222222222,
		power: -6149.12512417618,
		road: 14582.6015277778,
		acceleration: -0.89638888888889
	},
	{
		id: 1270,
		time: 1269,
		velocity: 7.51138888888889,
		power: -5701.38951751478,
		road: 14590.6168055556,
		acceleration: -0.911111111111111
	},
	{
		id: 1271,
		time: 1270,
		velocity: 6.46055555555555,
		power: -8149.02292619191,
		road: 14597.4747685185,
		acceleration: -1.40351851851852
	},
	{
		id: 1272,
		time: 1271,
		velocity: 4.49666666666667,
		power: -6002.057419103,
		road: 14602.9856018518,
		acceleration: -1.29074074074074
	},
	{
		id: 1273,
		time: 1272,
		velocity: 3.63916666666667,
		power: -5514.70391764589,
		road: 14607.0718981481,
		acceleration: -1.55833333333333
	},
	{
		id: 1274,
		time: 1273,
		velocity: 1.78555555555556,
		power: -3318.35882350609,
		road: 14609.6295833333,
		acceleration: -1.49888888888889
	},
	{
		id: 1275,
		time: 1274,
		velocity: 0,
		power: -1235.60165569802,
		road: 14610.8312962963,
		acceleration: -1.21305555555556
	},
	{
		id: 1276,
		time: 1275,
		velocity: 0,
		power: -131.84594431449,
		road: 14611.1288888889,
		acceleration: -0.595185185185185
	},
	{
		id: 1277,
		time: 1276,
		velocity: 0,
		power: 0,
		road: 14611.1288888889,
		acceleration: 0
	},
	{
		id: 1278,
		time: 1277,
		velocity: 0,
		power: 0,
		road: 14611.1288888889,
		acceleration: 0
	},
	{
		id: 1279,
		time: 1278,
		velocity: 0,
		power: 0,
		road: 14611.1288888889,
		acceleration: 0
	},
	{
		id: 1280,
		time: 1279,
		velocity: 0,
		power: 0,
		road: 14611.1288888889,
		acceleration: 0
	},
	{
		id: 1281,
		time: 1280,
		velocity: 0,
		power: 0,
		road: 14611.1288888889,
		acceleration: 0
	},
	{
		id: 1282,
		time: 1281,
		velocity: 0,
		power: 0,
		road: 14611.1288888889,
		acceleration: 0
	},
	{
		id: 1283,
		time: 1282,
		velocity: 0,
		power: 0,
		road: 14611.1288888889,
		acceleration: 0
	},
	{
		id: 1284,
		time: 1283,
		velocity: 0,
		power: 0,
		road: 14611.1288888889,
		acceleration: 0
	},
	{
		id: 1285,
		time: 1284,
		velocity: 0,
		power: 100.133262326935,
		road: 14611.3290740741,
		acceleration: 0.40037037037037
	},
	{
		id: 1286,
		time: 1285,
		velocity: 1.20111111111111,
		power: 658.440910745741,
		road: 14612.1101851852,
		acceleration: 0.761481481481482
	},
	{
		id: 1287,
		time: 1286,
		velocity: 2.28444444444444,
		power: 2761.07502628959,
		road: 14613.9840277778,
		acceleration: 1.42398148148148
	},
	{
		id: 1288,
		time: 1287,
		velocity: 4.27194444444444,
		power: 3526.57711259602,
		road: 14617.0996759259,
		acceleration: 1.05962962962963
	},
	{
		id: 1289,
		time: 1288,
		velocity: 4.38,
		power: 3126.68363173243,
		road: 14621.0895833333,
		acceleration: 0.688888888888889
	},
	{
		id: 1290,
		time: 1289,
		velocity: 4.35111111111111,
		power: 1326.64565366351,
		road: 14625.5126388889,
		acceleration: 0.177407407407408
	},
	{
		id: 1291,
		time: 1290,
		velocity: 4.80416666666667,
		power: 2551.31880611316,
		road: 14630.2385648148,
		acceleration: 0.428333333333333
	},
	{
		id: 1292,
		time: 1291,
		velocity: 5.665,
		power: 4538.54992634018,
		road: 14635.5560648148,
		acceleration: 0.754814814814814
	},
	{
		id: 1293,
		time: 1292,
		velocity: 6.61555555555556,
		power: 6497.3363056882,
		road: 14641.7299537037,
		acceleration: 0.957962962962963
	},
	{
		id: 1294,
		time: 1293,
		velocity: 7.67805555555556,
		power: 9566.18618460892,
		road: 14648.9961111111,
		acceleration: 1.22657407407407
	},
	{
		id: 1295,
		time: 1294,
		velocity: 9.34472222222222,
		power: 8434.85128297893,
		road: 14657.3242592593,
		acceleration: 0.897407407407408
	},
	{
		id: 1296,
		time: 1295,
		velocity: 9.30777777777778,
		power: 6010.09970836493,
		road: 14666.3634722222,
		acceleration: 0.524722222222223
	},
	{
		id: 1297,
		time: 1296,
		velocity: 9.25222222222222,
		power: 2554.57528411348,
		road: 14675.72,
		acceleration: 0.109907407407407
	},
	{
		id: 1298,
		time: 1297,
		velocity: 9.67444444444445,
		power: 5132.94166394208,
		road: 14685.3223611111,
		acceleration: 0.381759259259258
	},
	{
		id: 1299,
		time: 1298,
		velocity: 10.4530555555556,
		power: 7357.9416586412,
		road: 14695.4061111111,
		acceleration: 0.581018518518519
	},
	{
		id: 1300,
		time: 1299,
		velocity: 10.9952777777778,
		power: 7341.54557806217,
		road: 14706.0466666667,
		acceleration: 0.532592592592593
	},
	{
		id: 1301,
		time: 1300,
		velocity: 11.2722222222222,
		power: 6071.64882178883,
		road: 14717.1420833333,
		acceleration: 0.37712962962963
	},
	{
		id: 1302,
		time: 1301,
		velocity: 11.5844444444444,
		power: 4404.01757355058,
		road: 14728.5286111111,
		acceleration: 0.205092592592591
	},
	{
		id: 1303,
		time: 1302,
		velocity: 11.6105555555556,
		power: 2783.26914301272,
		road: 14740.0433333333,
		acceleration: 0.0512962962962966
	},
	{
		id: 1304,
		time: 1303,
		velocity: 11.4261111111111,
		power: 1506.43980583917,
		road: 14751.5512962963,
		acceleration: -0.0648148148148167
	},
	{
		id: 1305,
		time: 1304,
		velocity: 11.39,
		power: 1573.67266620821,
		road: 14762.998287037,
		acceleration: -0.0571296296296282
	},
	{
		id: 1306,
		time: 1305,
		velocity: 11.4391666666667,
		power: 3606.16932026033,
		road: 14774.4805092593,
		acceleration: 0.127592592592592
	},
	{
		id: 1307,
		time: 1306,
		velocity: 11.8088888888889,
		power: 5610.16708484354,
		road: 14786.1757407407,
		acceleration: 0.298425925925926
	},
	{
		id: 1308,
		time: 1307,
		velocity: 12.2852777777778,
		power: 8479.97566958341,
		road: 14798.2822685185,
		acceleration: 0.524166666666666
	},
	{
		id: 1309,
		time: 1308,
		velocity: 13.0116666666667,
		power: 7859.69603614276,
		road: 14810.8696759259,
		acceleration: 0.437592592592594
	},
	{
		id: 1310,
		time: 1309,
		velocity: 13.1216666666667,
		power: 7582.12949472638,
		road: 14823.8700462963,
		acceleration: 0.388333333333334
	},
	{
		id: 1311,
		time: 1310,
		velocity: 13.4502777777778,
		power: 5299.79454015182,
		road: 14837.1599537037,
		acceleration: 0.190740740740742
	},
	{
		id: 1312,
		time: 1311,
		velocity: 13.5838888888889,
		power: 6089.15938910677,
		road: 14850.6662037037,
		acceleration: 0.241944444444444
	},
	{
		id: 1313,
		time: 1312,
		velocity: 13.8475,
		power: 4794.34497352977,
		road: 14864.3601851852,
		acceleration: 0.133518518518516
	},
	{
		id: 1314,
		time: 1313,
		velocity: 13.8508333333333,
		power: 5286.29279039993,
		road: 14878.2031481481,
		acceleration: 0.164444444444447
	},
	{
		id: 1315,
		time: 1314,
		velocity: 14.0772222222222,
		power: 4574.10992796137,
		road: 14892.1808796296,
		acceleration: 0.105092592592591
	},
	{
		id: 1316,
		time: 1315,
		velocity: 14.1627777777778,
		power: 5090.35335519627,
		road: 14906.2804166667,
		acceleration: 0.138518518518522
	},
	{
		id: 1317,
		time: 1316,
		velocity: 14.2663888888889,
		power: 4789.2063485765,
		road: 14920.5047222222,
		acceleration: 0.111018518518517
	},
	{
		id: 1318,
		time: 1317,
		velocity: 14.4102777777778,
		power: 4564.31879105002,
		road: 14934.8297685185,
		acceleration: 0.0904629629629632
	},
	{
		id: 1319,
		time: 1318,
		velocity: 14.4341666666667,
		power: 4448.48383149803,
		road: 14949.2393981481,
		acceleration: 0.0787037037037042
	},
	{
		id: 1320,
		time: 1319,
		velocity: 14.5025,
		power: 4234.82189102117,
		road: 14963.7186574074,
		acceleration: 0.0605555555555544
	},
	{
		id: 1321,
		time: 1320,
		velocity: 14.5919444444444,
		power: 5366.09721488385,
		road: 14978.2972222222,
		acceleration: 0.138055555555557
	},
	{
		id: 1322,
		time: 1321,
		velocity: 14.8483333333333,
		power: 5702.21846103718,
		road: 14993.0226388889,
		acceleration: 0.155648148148147
	},
	{
		id: 1323,
		time: 1322,
		velocity: 14.9694444444444,
		power: 5371.03784100238,
		road: 15007.8889351852,
		acceleration: 0.126111111111113
	},
	{
		id: 1324,
		time: 1323,
		velocity: 14.9702777777778,
		power: 4321.87974657756,
		road: 15022.8427777778,
		acceleration: 0.04898148148148
	},
	{
		id: 1325,
		time: 1324,
		velocity: 14.9952777777778,
		power: 4250.67556261721,
		road: 15037.8422685185,
		acceleration: 0.042314814814814
	},
	{
		id: 1326,
		time: 1325,
		velocity: 15.0963888888889,
		power: 4180.91509127907,
		road: 15052.8809259259,
		acceleration: 0.0360185185185191
	},
	{
		id: 1327,
		time: 1326,
		velocity: 15.0783333333333,
		power: 2242.07959918884,
		road: 15067.8885648148,
		acceleration: -0.0980555555555558
	},
	{
		id: 1328,
		time: 1327,
		velocity: 14.7011111111111,
		power: -713.302202403196,
		road: 15082.6964351852,
		acceleration: -0.301481481481481
	},
	{
		id: 1329,
		time: 1328,
		velocity: 14.1919444444444,
		power: -1298.83158784761,
		road: 15097.18375,
		acceleration: -0.339629629629629
	},
	{
		id: 1330,
		time: 1329,
		velocity: 14.0594444444444,
		power: -2001.95691389293,
		road: 15111.3070833333,
		acceleration: -0.388333333333335
	},
	{
		id: 1331,
		time: 1330,
		velocity: 13.5361111111111,
		power: 57.7205412082477,
		road: 15125.1209259259,
		acceleration: -0.230648148148147
	},
	{
		id: 1332,
		time: 1331,
		velocity: 13.5,
		power: 676.069138480064,
		road: 15138.729537037,
		acceleration: -0.179814814814817
	},
	{
		id: 1333,
		time: 1332,
		velocity: 13.52,
		power: 3060.76986719182,
		road: 15152.2515277778,
		acceleration: 0.00657407407407717
	},
	{
		id: 1334,
		time: 1333,
		velocity: 13.5558333333333,
		power: 4010.61657954013,
		road: 15165.8160648148,
		acceleration: 0.078518518518516
	},
	{
		id: 1335,
		time: 1334,
		velocity: 13.7355555555556,
		power: 6045.89204585335,
		road: 15179.5338888889,
		acceleration: 0.228055555555557
	},
	{
		id: 1336,
		time: 1335,
		velocity: 14.2041666666667,
		power: 7351.72031843336,
		road: 15193.522037037,
		acceleration: 0.312592592592592
	},
	{
		id: 1337,
		time: 1336,
		velocity: 14.4936111111111,
		power: 9033.3569231952,
		road: 15207.8741666667,
		acceleration: 0.41537037037037
	},
	{
		id: 1338,
		time: 1337,
		velocity: 14.9816666666667,
		power: 8657.55173651122,
		road: 15222.6163425926,
		acceleration: 0.364722222222222
	},
	{
		id: 1339,
		time: 1338,
		velocity: 15.2983333333333,
		power: 9138.29098672215,
		road: 15237.7291666667,
		acceleration: 0.376574074074076
	},
	{
		id: 1340,
		time: 1339,
		velocity: 15.6233333333333,
		power: 7839.51079679741,
		road: 15253.1650925926,
		acceleration: 0.269629629629629
	},
	{
		id: 1341,
		time: 1340,
		velocity: 15.7905555555556,
		power: 8075.76271903953,
		road: 15268.8715277778,
		acceleration: 0.27138888888889
	},
	{
		id: 1342,
		time: 1341,
		velocity: 16.1125,
		power: 6987.61477151845,
		road: 15284.8076851852,
		acceleration: 0.188055555555556
	},
	{
		id: 1343,
		time: 1342,
		velocity: 16.1875,
		power: 5938.02875950747,
		road: 15300.8942592593,
		acceleration: 0.112777777777779
	},
	{
		id: 1344,
		time: 1343,
		velocity: 16.1288888888889,
		power: 2004.32108327477,
		road: 15316.9659259259,
		acceleration: -0.142592592592592
	},
	{
		id: 1345,
		time: 1344,
		velocity: 15.6847222222222,
		power: 335.76645361368,
		road: 15332.8425925926,
		acceleration: -0.24740740740741
	},
	{
		id: 1346,
		time: 1345,
		velocity: 15.4452777777778,
		power: -1632.97117587092,
		road: 15348.4086574074,
		acceleration: -0.373796296296295
	},
	{
		id: 1347,
		time: 1346,
		velocity: 15.0075,
		power: -676.312702796705,
		road: 15363.6355092593,
		acceleration: -0.30462962962963
	},
	{
		id: 1348,
		time: 1347,
		velocity: 14.7708333333333,
		power: 450.898532271143,
		road: 15378.5989814815,
		acceleration: -0.222129629629631
	},
	{
		id: 1349,
		time: 1348,
		velocity: 14.7788888888889,
		power: 1418.19446692718,
		road: 15393.3763425926,
		acceleration: -0.150092592592594
	},
	{
		id: 1350,
		time: 1349,
		velocity: 14.5572222222222,
		power: 1116.47617490275,
		road: 15407.9946759259,
		acceleration: -0.167962962962962
	},
	{
		id: 1351,
		time: 1350,
		velocity: 14.2669444444444,
		power: 622.953305932278,
		road: 15422.4291666667,
		acceleration: -0.199722222222221
	},
	{
		id: 1352,
		time: 1351,
		velocity: 14.1797222222222,
		power: 712.770733418806,
		road: 15436.6691203704,
		acceleration: -0.189351851851853
	},
	{
		id: 1353,
		time: 1352,
		velocity: 13.9891666666667,
		power: -107.168261132832,
		road: 15450.69125,
		acceleration: -0.246296296296297
	},
	{
		id: 1354,
		time: 1353,
		velocity: 13.5280555555556,
		power: -286.026630334424,
		road: 15464.4621759259,
		acceleration: -0.25611111111111
	},
	{
		id: 1355,
		time: 1354,
		velocity: 13.4113888888889,
		power: -194.350184009364,
		road: 15477.9822685185,
		acceleration: -0.245555555555555
	},
	{
		id: 1356,
		time: 1355,
		velocity: 13.2525,
		power: 1833.6649730451,
		road: 15491.3375,
		acceleration: -0.0841666666666647
	},
	{
		id: 1357,
		time: 1356,
		velocity: 13.2755555555556,
		power: 2333.15435961191,
		road: 15504.6290740741,
		acceleration: -0.0431481481481484
	},
	{
		id: 1358,
		time: 1357,
		velocity: 13.2819444444444,
		power: 3938.18472683566,
		road: 15517.9403703704,
		acceleration: 0.0825925925925901
	},
	{
		id: 1359,
		time: 1358,
		velocity: 13.5002777777778,
		power: 5684.88559185218,
		road: 15531.3993981481,
		acceleration: 0.21287037037037
	},
	{
		id: 1360,
		time: 1359,
		velocity: 13.9141666666667,
		power: 5938.11872431678,
		road: 15545.075787037,
		acceleration: 0.221851851851852
	},
	{
		id: 1361,
		time: 1360,
		velocity: 13.9475,
		power: 4441.40321437769,
		road: 15558.9134722222,
		acceleration: 0.100740740740742
	},
	{
		id: 1362,
		time: 1361,
		velocity: 13.8025,
		power: 4737.78326479059,
		road: 15572.8608333333,
		acceleration: 0.118611111111111
	},
	{
		id: 1363,
		time: 1362,
		velocity: 14.27,
		power: 5135.2158856896,
		road: 15586.9388888889,
		acceleration: 0.142777777777777
	},
	{
		id: 1364,
		time: 1363,
		velocity: 14.3758333333333,
		power: 6338.36023415488,
		road: 15601.1999537037,
		acceleration: 0.223240740740742
	},
	{
		id: 1365,
		time: 1364,
		velocity: 14.4722222222222,
		power: 5017.12740641584,
		road: 15615.6321759259,
		acceleration: 0.119074074074074
	},
	{
		id: 1366,
		time: 1365,
		velocity: 14.6272222222222,
		power: 8275.64284496604,
		road: 15630.295,
		acceleration: 0.342129629629628
	},
	{
		id: 1367,
		time: 1366,
		velocity: 15.4022222222222,
		power: 10793.4475422537,
		road: 15645.3755555555,
		acceleration: 0.493333333333336
	},
	{
		id: 1368,
		time: 1367,
		velocity: 15.9522222222222,
		power: 9985.12813550582,
		road: 15660.9073611111,
		acceleration: 0.409166666666664
	},
	{
		id: 1369,
		time: 1368,
		velocity: 15.8547222222222,
		power: 8115.57875677551,
		road: 15676.7765277778,
		acceleration: 0.265555555555556
	},
	{
		id: 1370,
		time: 1369,
		velocity: 16.1988888888889,
		power: 2995.78601338279,
		road: 15692.7410648148,
		acceleration: -0.0748148148148164
	},
	{
		id: 1371,
		time: 1370,
		velocity: 15.7277777777778,
		power: 5432.36653186379,
		road: 15708.7105092593,
		acceleration: 0.0846296296296298
	},
	{
		id: 1372,
		time: 1371,
		velocity: 16.1086111111111,
		power: 3933.78365644861,
		road: 15724.7149074074,
		acceleration: -0.0147222222222201
	},
	{
		id: 1373,
		time: 1372,
		velocity: 16.1547222222222,
		power: 7653.81839572246,
		road: 15740.8235648148,
		acceleration: 0.22324074074074
	},
	{
		id: 1374,
		time: 1373,
		velocity: 16.3975,
		power: 7061.0514788783,
		road: 15757.1315740741,
		acceleration: 0.17546296296296
	},
	{
		id: 1375,
		time: 1374,
		velocity: 16.635,
		power: 7445.80398886161,
		road: 15773.6230092593,
		acceleration: 0.191388888888895
	},
	{
		id: 1376,
		time: 1375,
		velocity: 16.7288888888889,
		power: 8110.88543557862,
		road: 15790.3217592593,
		acceleration: 0.223240740740735
	},
	{
		id: 1377,
		time: 1376,
		velocity: 17.0672222222222,
		power: 6885.25215361588,
		road: 15807.2013425926,
		acceleration: 0.138425925925926
	},
	{
		id: 1378,
		time: 1377,
		velocity: 17.0502777777778,
		power: 7668.7546992844,
		road: 15824.2398611111,
		acceleration: 0.179444444444446
	},
	{
		id: 1379,
		time: 1378,
		velocity: 17.2672222222222,
		power: 5922.73671332935,
		road: 15841.4017592593,
		acceleration: 0.0673148148148179
	},
	{
		id: 1380,
		time: 1379,
		velocity: 17.2691666666667,
		power: 8040.0273750825,
		road: 15858.6923611111,
		acceleration: 0.190092592592588
	},
	{
		id: 1381,
		time: 1380,
		velocity: 17.6205555555556,
		power: 10927.0621038049,
		road: 15876.2525462963,
		acceleration: 0.349074074074075
	},
	{
		id: 1382,
		time: 1381,
		velocity: 18.3144444444444,
		power: 13804.9921513741,
		road: 15894.2335185185,
		acceleration: 0.4925
	},
	{
		id: 1383,
		time: 1382,
		velocity: 18.7466666666667,
		power: 17941.0188899436,
		road: 15912.8043518518,
		acceleration: 0.687222222222221
	},
	{
		id: 1384,
		time: 1383,
		velocity: 19.6822222222222,
		power: 18493.6538536039,
		road: 15932.0522685185,
		acceleration: 0.66694444444445
	},
	{
		id: 1385,
		time: 1384,
		velocity: 20.3152777777778,
		power: 16877.0760003775,
		road: 15951.9026388889,
		acceleration: 0.537962962962958
	},
	{
		id: 1386,
		time: 1385,
		velocity: 20.3605555555556,
		power: 6669.86882548911,
		road: 15972.0173611111,
		acceleration: -0.00925925925925597
	},
	{
		id: 1387,
		time: 1386,
		velocity: 19.6544444444444,
		power: -6028.32994893705,
		road: 15991.7944907407,
		acceleration: -0.665925925925926
	},
	{
		id: 1388,
		time: 1387,
		velocity: 18.3175,
		power: -21256.2533789481,
		road: 16010.4824537037,
		acceleration: -1.51240740740741
	},
	{
		id: 1389,
		time: 1388,
		velocity: 15.8233333333333,
		power: -23989.8652035042,
		road: 16027.532962963,
		acceleration: -1.7625
	},
	{
		id: 1390,
		time: 1389,
		velocity: 14.3669444444444,
		power: -25292.7002347274,
		road: 16042.7008796296,
		acceleration: -2.00268518518519
	},
	{
		id: 1391,
		time: 1390,
		velocity: 12.3094444444444,
		power: -16296.6009147237,
		road: 16056.1166203704,
		acceleration: -1.50166666666667
	},
	{
		id: 1392,
		time: 1391,
		velocity: 11.3183333333333,
		power: -15492.3482396229,
		road: 16067.9941203704,
		acceleration: -1.57481481481481
	},
	{
		id: 1393,
		time: 1392,
		velocity: 9.6425,
		power: -12617.6101018016,
		road: 16078.3509259259,
		acceleration: -1.46657407407407
	},
	{
		id: 1394,
		time: 1393,
		velocity: 7.90972222222222,
		power: -12413.1827733808,
		road: 16087.1477314815,
		acceleration: -1.65342592592592
	},
	{
		id: 1395,
		time: 1394,
		velocity: 6.35805555555556,
		power: -7446.30106457144,
		road: 16094.5069907407,
		acceleration: -1.22166666666667
	},
	{
		id: 1396,
		time: 1395,
		velocity: 5.9775,
		power: -4642.65583008804,
		road: 16100.7920833333,
		acceleration: -0.926666666666666
	},
	{
		id: 1397,
		time: 1396,
		velocity: 5.12972222222222,
		power: -3672.9361530462,
		road: 16106.1834722222,
		acceleration: -0.860740740740741
	},
	{
		id: 1398,
		time: 1397,
		velocity: 3.77583333333333,
		power: -4388.63386416635,
		road: 16110.5456944444,
		acceleration: -1.19759259259259
	},
	{
		id: 1399,
		time: 1398,
		velocity: 2.38472222222222,
		power: -3829.22669039906,
		road: 16113.5771759259,
		acceleration: -1.46388888888889
	},
	{
		id: 1400,
		time: 1399,
		velocity: 0.738055555555556,
		power: -1788.75496824883,
		road: 16115.2474074074,
		acceleration: -1.25861111111111
	},
	{
		id: 1401,
		time: 1400,
		velocity: 0,
		power: -406.815637308951,
		road: 16115.8908796296,
		acceleration: -0.794907407407408
	},
	{
		id: 1402,
		time: 1401,
		velocity: 0,
		power: -13.8080698992853,
		road: 16116.0138888889,
		acceleration: -0.246018518518519
	},
	{
		id: 1403,
		time: 1402,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1404,
		time: 1403,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1405,
		time: 1404,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1406,
		time: 1405,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1407,
		time: 1406,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1408,
		time: 1407,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1409,
		time: 1408,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1410,
		time: 1409,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1411,
		time: 1410,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1412,
		time: 1411,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1413,
		time: 1412,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1414,
		time: 1413,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1415,
		time: 1414,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1416,
		time: 1415,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1417,
		time: 1416,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1418,
		time: 1417,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1419,
		time: 1418,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1420,
		time: 1419,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1421,
		time: 1420,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1422,
		time: 1421,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1423,
		time: 1422,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1424,
		time: 1423,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1425,
		time: 1424,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1426,
		time: 1425,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1427,
		time: 1426,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1428,
		time: 1427,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1429,
		time: 1428,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1430,
		time: 1429,
		velocity: 0,
		power: 0,
		road: 16116.0138888889,
		acceleration: 0
	},
	{
		id: 1431,
		time: 1430,
		velocity: 0,
		power: 0.696951513544074,
		road: 16116.019212963,
		acceleration: 0.0106481481481481
	},
	{
		id: 1432,
		time: 1431,
		velocity: 0.0319444444444444,
		power: 1.2864874975235,
		road: 16116.0298611111,
		acceleration: 0
	},
	{
		id: 1433,
		time: 1432,
		velocity: 0,
		power: 1.2864874975235,
		road: 16116.0405092593,
		acceleration: 0
	},
	{
		id: 1434,
		time: 1433,
		velocity: 0,
		power: 0.589535656270305,
		road: 16116.0458333333,
		acceleration: -0.0106481481481481
	},
	{
		id: 1435,
		time: 1434,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1436,
		time: 1435,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1437,
		time: 1436,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1438,
		time: 1437,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1439,
		time: 1438,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1440,
		time: 1439,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1441,
		time: 1440,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1442,
		time: 1441,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1443,
		time: 1442,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1444,
		time: 1443,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1445,
		time: 1444,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1446,
		time: 1445,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1447,
		time: 1446,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1448,
		time: 1447,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1449,
		time: 1448,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1450,
		time: 1449,
		velocity: 0,
		power: 0,
		road: 16116.0458333333,
		acceleration: 0
	},
	{
		id: 1451,
		time: 1450,
		velocity: 0,
		power: 83.1865450241627,
		road: 16116.2258796296,
		acceleration: 0.360092592592593
	},
	{
		id: 1452,
		time: 1451,
		velocity: 1.08027777777778,
		power: 425.022271008792,
		road: 16116.8701388889,
		acceleration: 0.568333333333333
	},
	{
		id: 1453,
		time: 1452,
		velocity: 1.705,
		power: 2969.98666860015,
		road: 16118.6257407407,
		acceleration: 1.65435185185185
	},
	{
		id: 1454,
		time: 1453,
		velocity: 4.96305555555556,
		power: 5869.76084906763,
		road: 16122.045462963,
		acceleration: 1.67388888888889
	},
	{
		id: 1455,
		time: 1454,
		velocity: 6.10194444444444,
		power: 10421.2720744324,
		road: 16127.2783796296,
		acceleration: 1.9525
	},
	{
		id: 1456,
		time: 1455,
		velocity: 7.5625,
		power: 13038.0256564587,
		road: 16134.3751388889,
		acceleration: 1.77518518518518
	},
	{
		id: 1457,
		time: 1456,
		velocity: 10.2886111111111,
		power: 18851.2764325504,
		road: 16143.3727777778,
		acceleration: 2.02657407407408
	},
	{
		id: 1458,
		time: 1457,
		velocity: 12.1816666666667,
		power: 19701.8269864405,
		road: 16154.2376388889,
		acceleration: 1.70787037037037
	},
	{
		id: 1459,
		time: 1458,
		velocity: 12.6861111111111,
		power: 12247.0066899974,
		road: 16166.3797685185,
		acceleration: 0.846666666666666
	},
	{
		id: 1460,
		time: 1459,
		velocity: 12.8286111111111,
		power: 8359.0437546484,
		road: 16179.1775462963,
		acceleration: 0.464629629629631
	},
	{
		id: 1461,
		time: 1460,
		velocity: 13.5755555555556,
		power: 9167.43213338589,
		road: 16192.4558333333,
		acceleration: 0.496388888888887
	},
	{
		id: 1462,
		time: 1461,
		velocity: 14.1752777777778,
		power: 11655.7370573529,
		road: 16206.3051851852,
		acceleration: 0.645740740740742
	},
	{
		id: 1463,
		time: 1462,
		velocity: 14.7658333333333,
		power: 12193.3212153466,
		road: 16220.7949537037,
		acceleration: 0.635092592592592
	},
	{
		id: 1464,
		time: 1463,
		velocity: 15.4808333333333,
		power: 11582.9303464548,
		road: 16235.8762962963,
		acceleration: 0.548055555555557
	},
	{
		id: 1465,
		time: 1464,
		velocity: 15.8194444444444,
		power: 8563.11773989035,
		road: 16251.3888888889,
		acceleration: 0.314444444444442
	},
	{
		id: 1466,
		time: 1465,
		velocity: 15.7091666666667,
		power: 4029.92941519376,
		road: 16267.0602777778,
		acceleration: 0.00314814814814923
	},
	{
		id: 1467,
		time: 1466,
		velocity: 15.4902777777778,
		power: 3034.29646511267,
		road: 16282.702037037,
		acceleration: -0.0624074074074077
	},
	{
		id: 1468,
		time: 1467,
		velocity: 15.6322222222222,
		power: 96.9543628966912,
		road: 16298.1845833333,
		acceleration: -0.25601851851852
	},
	{
		id: 1469,
		time: 1468,
		velocity: 14.9411111111111,
		power: -455.030952751732,
		road: 16313.394537037,
		acceleration: -0.289166666666667
	},
	{
		id: 1470,
		time: 1469,
		velocity: 14.6227777777778,
		power: -4280.11130126703,
		road: 16328.1830555556,
		acceleration: -0.553703703703704
	},
	{
		id: 1471,
		time: 1470,
		velocity: 13.9711111111111,
		power: -2643.78741246657,
		road: 16342.4765740741,
		acceleration: -0.436296296296295
	},
	{
		id: 1472,
		time: 1471,
		velocity: 13.6322222222222,
		power: -2634.43836365092,
		road: 16356.334537037,
		acceleration: -0.434814814814814
	},
	{
		id: 1473,
		time: 1472,
		velocity: 13.3183333333333,
		power: -938.014599263295,
		road: 16369.8236574074,
		acceleration: -0.302870370370371
	},
	{
		id: 1474,
		time: 1473,
		velocity: 13.0625,
		power: -1582.85098268741,
		road: 16382.985787037,
		acceleration: -0.351111111111109
	},
	{
		id: 1475,
		time: 1474,
		velocity: 12.5788888888889,
		power: -2361.26664409707,
		road: 16395.765787037,
		acceleration: -0.413148148148149
	},
	{
		id: 1476,
		time: 1475,
		velocity: 12.0788888888889,
		power: -556.184561121308,
		road: 16408.2084259259,
		acceleration: -0.261574074074073
	},
	{
		id: 1477,
		time: 1476,
		velocity: 12.2777777777778,
		power: 2272.14564058926,
		road: 16420.5106944444,
		acceleration: -0.0191666666666688
	},
	{
		id: 1478,
		time: 1477,
		velocity: 12.5213888888889,
		power: 3560.64975261422,
		road: 16432.8480092593,
		acceleration: 0.0892592592592578
	},
	{
		id: 1479,
		time: 1478,
		velocity: 12.3466666666667,
		power: 1687.86568576256,
		road: 16445.1949074074,
		acceleration: -0.0700925925925926
	},
	{
		id: 1480,
		time: 1479,
		velocity: 12.0675,
		power: -1161.64128805114,
		road: 16457.3512962963,
		acceleration: -0.310925925925925
	},
	{
		id: 1481,
		time: 1480,
		velocity: 11.5886111111111,
		power: -4973.77062759419,
		road: 16469.026712963,
		acceleration: -0.651018518518518
	},
	{
		id: 1482,
		time: 1481,
		velocity: 10.3936111111111,
		power: -8080.75001087432,
		road: 16479.8893981481,
		acceleration: -0.974444444444444
	},
	{
		id: 1483,
		time: 1482,
		velocity: 9.14416666666667,
		power: -8024.8036427824,
		road: 16489.7464351852,
		acceleration: -1.03685185185185
	},
	{
		id: 1484,
		time: 1483,
		velocity: 8.47805555555555,
		power: -5216.90316835999,
		road: 16498.6925462963,
		acceleration: -0.785000000000002
	},
	{
		id: 1485,
		time: 1484,
		velocity: 8.03861111111111,
		power: -2749.249905482,
		road: 16506.9889814815,
		acceleration: -0.514351851851851
	},
	{
		id: 1486,
		time: 1485,
		velocity: 7.60111111111111,
		power: -1981.13510619367,
		road: 16514.8142592592,
		acceleration: -0.427962962962964
	},
	{
		id: 1487,
		time: 1486,
		velocity: 7.19416666666667,
		power: 4097.95361703584,
		road: 16522.6205092593,
		acceleration: 0.389907407407408
	},
	{
		id: 1488,
		time: 1487,
		velocity: 9.20833333333333,
		power: 9236.9645947,
		road: 16531.1091666667,
		acceleration: 0.974907407407407
	},
	{
		id: 1489,
		time: 1488,
		velocity: 10.5258333333333,
		power: 11542.7888988113,
		road: 16540.6321759259,
		acceleration: 1.0937962962963
	},
	{
		id: 1490,
		time: 1489,
		velocity: 10.4755555555556,
		power: 10486.5684894079,
		road: 16551.13125,
		acceleration: 0.858333333333333
	},
	{
		id: 1491,
		time: 1490,
		velocity: 11.7833333333333,
		power: 10000.5397119587,
		road: 16562.4241666667,
		acceleration: 0.729351851851852
	},
	{
		id: 1492,
		time: 1491,
		velocity: 12.7138888888889,
		power: 18463.2113823815,
		road: 16574.7594444444,
		acceleration: 1.35537037037037
	},
	{
		id: 1493,
		time: 1492,
		velocity: 14.5416666666667,
		power: 36894.9665346632,
		road: 16589.006712963,
		acceleration: 2.46861111111111
	},
	{
		id: 1494,
		time: 1493,
		velocity: 19.1891666666667,
		power: 52442.0543489,
		road: 16605.9598611111,
		acceleration: 2.94314814814815
	},
	{
		id: 1495,
		time: 1494,
		velocity: 21.5433333333333,
		power: 57581.7081305538,
		road: 16625.7302314815,
		acceleration: 2.6912962962963
	},
	{
		id: 1496,
		time: 1495,
		velocity: 22.6155555555556,
		power: 34270.6575115949,
		road: 16647.4711111111,
		acceleration: 1.24972222222222
	},
	{
		id: 1497,
		time: 1496,
		velocity: 22.9383333333333,
		power: 18441.796856677,
		road: 16670.0550925926,
		acceleration: 0.436481481481479
	},
	{
		id: 1498,
		time: 1497,
		velocity: 22.8527777777778,
		power: 8369.5268867474,
		road: 16692.8389351852,
		acceleration: -0.0367592592592629
	},
	{
		id: 1499,
		time: 1498,
		velocity: 22.5052777777778,
		power: 8217.72124962133,
		road: 16715.5833796296,
		acceleration: -0.0420370370370371
	},
	{
		id: 1500,
		time: 1499,
		velocity: 22.8122222222222,
		power: 9813.1178666461,
		road: 16738.3224537037,
		acceleration: 0.0312962962963006
	},
	{
		id: 1501,
		time: 1500,
		velocity: 22.9466666666667,
		power: 11774.7216123123,
		road: 16761.1359259259,
		acceleration: 0.117499999999996
	},
	{
		id: 1502,
		time: 1501,
		velocity: 22.8577777777778,
		power: 8829.11051369436,
		road: 16783.9985648148,
		acceleration: -0.0191666666666634
	},
	{
		id: 1503,
		time: 1502,
		velocity: 22.7547222222222,
		power: 7127.42447172697,
		road: 16806.8043981481,
		acceleration: -0.094444444444445
	},
	{
		id: 1504,
		time: 1503,
		velocity: 22.6633333333333,
		power: 7611.38135279873,
		road: 16829.5285185185,
		acceleration: -0.0689814814814795
	},
	{
		id: 1505,
		time: 1504,
		velocity: 22.6508333333333,
		power: 8053.38086560257,
		road: 16852.195,
		acceleration: -0.0462962962963012
	},
	{
		id: 1506,
		time: 1505,
		velocity: 22.6158333333333,
		power: 4435.31252545311,
		road: 16874.734212963,
		acceleration: -0.208240740740738
	},
	{
		id: 1507,
		time: 1506,
		velocity: 22.0386111111111,
		power: 1030.23524306875,
		road: 16896.9903240741,
		acceleration: -0.357962962962965
	},
	{
		id: 1508,
		time: 1507,
		velocity: 21.5769444444444,
		power: -610.828869852031,
		road: 16918.8546296296,
		acceleration: -0.425648148148145
	},
	{
		id: 1509,
		time: 1508,
		velocity: 21.3388888888889,
		power: 1982.43948682948,
		road: 16940.3602777778,
		acceleration: -0.291666666666668
	},
	{
		id: 1510,
		time: 1509,
		velocity: 21.1636111111111,
		power: 3117.47841805125,
		road: 16961.6058333333,
		acceleration: -0.22851851851852
	},
	{
		id: 1511,
		time: 1510,
		velocity: 20.8913888888889,
		power: 379.595213437173,
		road: 16982.5592592593,
		acceleration: -0.355740740740739
	},
	{
		id: 1512,
		time: 1511,
		velocity: 20.2716666666667,
		power: -2568.36248169722,
		road: 17003.0871759259,
		acceleration: -0.49527777777778
	},
	{
		id: 1513,
		time: 1512,
		velocity: 19.6777777777778,
		power: -5249.45914565461,
		road: 17023.0542592593,
		acceleration: -0.62638888888889
	},
	{
		id: 1514,
		time: 1513,
		velocity: 19.0122222222222,
		power: -4177.45289032272,
		road: 17042.4261574074,
		acceleration: -0.563981481481477
	},
	{
		id: 1515,
		time: 1514,
		velocity: 18.5797222222222,
		power: -3115.13672917646,
		road: 17061.2660185185,
		acceleration: -0.500092592592594
	},
	{
		id: 1516,
		time: 1515,
		velocity: 18.1775,
		power: -4553.72679702115,
		road: 17079.5677777778,
		acceleration: -0.576111111111111
	},
	{
		id: 1517,
		time: 1516,
		velocity: 17.2838888888889,
		power: -4907.38117023751,
		road: 17097.2845833333,
		acceleration: -0.593796296296297
	},
	{
		id: 1518,
		time: 1517,
		velocity: 16.7983333333333,
		power: -15103.6425133224,
		road: 17114.0912962963,
		acceleration: -1.22638888888889
	},
	{
		id: 1519,
		time: 1518,
		velocity: 14.4983333333333,
		power: -15772.5531707355,
		road: 17129.621712963,
		acceleration: -1.32620370370371
	},
	{
		id: 1520,
		time: 1519,
		velocity: 13.3052777777778,
		power: -20393.5079144849,
		road: 17143.6064351852,
		acceleration: -1.76518518518518
	},
	{
		id: 1521,
		time: 1520,
		velocity: 11.5027777777778,
		power: -3103.13528017472,
		road: 17156.4718055556,
		acceleration: -0.473518518518517
	},
	{
		id: 1522,
		time: 1521,
		velocity: 13.0777777777778,
		power: 3575.27315593495,
		road: 17169.1393518518,
		acceleration: 0.077870370370368
	},
	{
		id: 1523,
		time: 1522,
		velocity: 13.5388888888889,
		power: 13206.9634220017,
		road: 17182.260787037,
		acceleration: 0.829907407407408
	},
	{
		id: 1524,
		time: 1523,
		velocity: 13.9925,
		power: 8540.09344926596,
		road: 17196.0055555555,
		acceleration: 0.416759259259262
	},
	{
		id: 1525,
		time: 1524,
		velocity: 14.3280555555556,
		power: 8411.12151318385,
		road: 17210.1499074074,
		acceleration: 0.382407407407406
	},
	{
		id: 1526,
		time: 1525,
		velocity: 14.6861111111111,
		power: 7417.27995746612,
		road: 17224.6307407407,
		acceleration: 0.290555555555557
	},
	{
		id: 1527,
		time: 1526,
		velocity: 14.8641666666667,
		power: 7136.36512149214,
		road: 17239.3849074074,
		acceleration: 0.25611111111111
	},
	{
		id: 1528,
		time: 1527,
		velocity: 15.0963888888889,
		power: 4938.57398612038,
		road: 17254.3137037037,
		acceleration: 0.0931481481481473
	},
	{
		id: 1529,
		time: 1528,
		velocity: 14.9655555555556,
		power: 2773.1941823997,
		road: 17269.2594907407,
		acceleration: -0.0591666666666679
	},
	{
		id: 1530,
		time: 1529,
		velocity: 14.6866666666667,
		power: 2980.67936765751,
		road: 17284.1541666667,
		acceleration: -0.0430555555555561
	},
	{
		id: 1531,
		time: 1530,
		velocity: 14.9672222222222,
		power: 5305.32667944888,
		road: 17299.0866666667,
		acceleration: 0.118703703703703
	},
	{
		id: 1532,
		time: 1531,
		velocity: 15.3216666666667,
		power: 6932.19687174808,
		road: 17314.1906481481,
		acceleration: 0.224259259259261
	},
	{
		id: 1533,
		time: 1532,
		velocity: 15.3594444444444,
		power: 6196.27826770582,
		road: 17329.4889814815,
		acceleration: 0.164444444444444
	},
	{
		id: 1534,
		time: 1533,
		velocity: 15.4605555555556,
		power: 4451.77194209183,
		road: 17344.8901851852,
		acceleration: 0.0412962962962951
	},
	{
		id: 1535,
		time: 1534,
		velocity: 15.4455555555556,
		power: 3139.16397897903,
		road: 17360.2881481481,
		acceleration: -0.0477777777777764
	},
	{
		id: 1536,
		time: 1535,
		velocity: 15.2161111111111,
		power: 1537.63548787642,
		road: 17375.5851388889,
		acceleration: -0.154166666666667
	},
	{
		id: 1537,
		time: 1536,
		velocity: 14.9980555555556,
		power: -314.700512360485,
		road: 17390.6662962963,
		acceleration: -0.2775
	},
	{
		id: 1538,
		time: 1537,
		velocity: 14.6130555555556,
		power: 960.428280555837,
		road: 17405.5166666667,
		acceleration: -0.184074074074074
	},
	{
		id: 1539,
		time: 1538,
		velocity: 14.6638888888889,
		power: 2591.63934867545,
		road: 17420.2422685185,
		acceleration: -0.0654629629629628
	},
	{
		id: 1540,
		time: 1539,
		velocity: 14.8016666666667,
		power: 4168.20966087187,
		road: 17434.9586111111,
		acceleration: 0.0469444444444438
	},
	{
		id: 1541,
		time: 1540,
		velocity: 14.7538888888889,
		power: 3246.37462120012,
		road: 17449.6888888889,
		acceleration: -0.0190740740740711
	},
	{
		id: 1542,
		time: 1541,
		velocity: 14.6066666666667,
		power: 2431.27681932066,
		road: 17464.3718055556,
		acceleration: -0.0756481481481508
	},
	{
		id: 1543,
		time: 1542,
		velocity: 14.5747222222222,
		power: 3475.89588075315,
		road: 17479.0169444444,
		acceleration: 9.25925925940874E-05
	},
	{
		id: 1544,
		time: 1543,
		velocity: 14.7541666666667,
		power: 5338.9068152351,
		road: 17493.7273611111,
		acceleration: 0.130462962962962
	},
	{
		id: 1545,
		time: 1544,
		velocity: 14.9980555555556,
		power: 6870.60187284891,
		road: 17508.6182407407,
		acceleration: 0.230462962962962
	},
	{
		id: 1546,
		time: 1545,
		velocity: 15.2661111111111,
		power: 8961.9733849355,
		road: 17523.8043981481,
		acceleration: 0.360092592592594
	},
	{
		id: 1547,
		time: 1546,
		velocity: 15.8344444444444,
		power: 12106.111487166,
		road: 17539.4428703704,
		acceleration: 0.544537037037037
	},
	{
		id: 1548,
		time: 1547,
		velocity: 16.6316666666667,
		power: 18631.8514355551,
		road: 17555.8095833333,
		acceleration: 0.911944444444442
	},
	{
		id: 1549,
		time: 1548,
		velocity: 18.0019444444444,
		power: 21986.6774567531,
		road: 17573.1465277778,
		acceleration: 1.02851851851852
	},
	{
		id: 1550,
		time: 1549,
		velocity: 18.92,
		power: 35413.5365771835,
		road: 17591.8255555555,
		acceleration: 1.65564814814815
	},
	{
		id: 1551,
		time: 1550,
		velocity: 21.5986111111111,
		power: 35618.5414324992,
		road: 17612.0710185185,
		acceleration: 1.47722222222222
	},
	{
		id: 1552,
		time: 1551,
		velocity: 22.4336111111111,
		power: 34960.5927098832,
		road: 17633.7021296296,
		acceleration: 1.29407407407408
	},
	{
		id: 1553,
		time: 1552,
		velocity: 22.8022222222222,
		power: 17114.4536850713,
		road: 17656.1713888889,
		acceleration: 0.382222222222222
	},
	{
		id: 1554,
		time: 1553,
		velocity: 22.7452777777778,
		power: 8084.21607783052,
		road: 17678.8099074074,
		acceleration: -0.0437037037037058
	},
	{
		id: 1555,
		time: 1554,
		velocity: 22.3025,
		power: 3001.45094379026,
		road: 17701.2902314815,
		acceleration: -0.272685185185185
	},
	{
		id: 1556,
		time: 1555,
		velocity: 21.9841666666667,
		power: 1161.31722839003,
		road: 17723.4594907407,
		acceleration: -0.349444444444444
	},
	{
		id: 1557,
		time: 1556,
		velocity: 21.6969444444444,
		power: 2079.04177209053,
		road: 17745.3056018518,
		acceleration: -0.296851851851851
	},
	{
		id: 1558,
		time: 1557,
		velocity: 21.4119444444444,
		power: 3462.27841293282,
		road: 17766.892037037,
		acceleration: -0.2225
	},
	{
		id: 1559,
		time: 1558,
		velocity: 21.3166666666667,
		power: 3807.9577699857,
		road: 17788.2677777778,
		acceleration: -0.198888888888888
	},
	{
		id: 1560,
		time: 1559,
		velocity: 21.1002777777778,
		power: 3782.68871501219,
		road: 17809.4472222222,
		acceleration: -0.193703703703704
	},
	{
		id: 1561,
		time: 1560,
		velocity: 20.8308333333333,
		power: 3328.11150146904,
		road: 17830.4249537037,
		acceleration: -0.209722222222222
	},
	{
		id: 1562,
		time: 1561,
		velocity: 20.6875,
		power: 3151.70196549053,
		road: 17851.1918518518,
		acceleration: -0.211944444444445
	},
	{
		id: 1563,
		time: 1562,
		velocity: 20.4644444444444,
		power: 3719.31216655423,
		road: 17871.7642129629,
		acceleration: -0.177129629629629
	},
	{
		id: 1564,
		time: 1563,
		velocity: 20.2994444444444,
		power: 5923.63028265363,
		road: 17892.2175925926,
		acceleration: -0.0608333333333348
	},
	{
		id: 1565,
		time: 1564,
		velocity: 20.505,
		power: 8561.15229833372,
		road: 17912.6772685185,
		acceleration: 0.0734259259259282
	},
	{
		id: 1566,
		time: 1565,
		velocity: 20.6847222222222,
		power: 10777.3744597723,
		road: 17933.2637037037,
		acceleration: 0.18009259259259
	},
	{
		id: 1567,
		time: 1566,
		velocity: 20.8397222222222,
		power: 9339.72417197843,
		road: 17953.9905555555,
		acceleration: 0.100740740740743
	},
	{
		id: 1568,
		time: 1567,
		velocity: 20.8072222222222,
		power: 8082.20171926624,
		road: 17974.7850462963,
		acceleration: 0.0345370370370368
	},
	{
		id: 1569,
		time: 1568,
		velocity: 20.7883333333333,
		power: 6080.72319920129,
		road: 17995.5641666667,
		acceleration: -0.0652777777777764
	},
	{
		id: 1570,
		time: 1569,
		velocity: 20.6438888888889,
		power: 4852.56243948542,
		road: 18016.2488425926,
		acceleration: -0.123611111111114
	},
	{
		id: 1571,
		time: 1570,
		velocity: 20.4363888888889,
		power: 1294.92322752511,
		road: 18036.7229629629,
		acceleration: -0.297499999999999
	},
	{
		id: 1572,
		time: 1571,
		velocity: 19.8958333333333,
		power: 4992.89314061333,
		road: 18056.9973611111,
		acceleration: -0.101944444444445
	},
	{
		id: 1573,
		time: 1572,
		velocity: 20.3380555555556,
		power: 6821.82404945362,
		road: 18077.2179629629,
		acceleration: -0.00564814814814341
	},
	{
		id: 1574,
		time: 1573,
		velocity: 20.4194444444444,
		power: 9435.53509931239,
		road: 18097.4989351852,
		acceleration: 0.126388888888886
	},
	{
		id: 1575,
		time: 1574,
		velocity: 20.275,
		power: 7358.87588711435,
		road: 18117.8513888889,
		acceleration: 0.0165740740740752
	},
	{
		id: 1576,
		time: 1575,
		velocity: 20.3877777777778,
		power: 7261.63562069221,
		road: 18138.2176388889,
		acceleration: 0.0110185185185188
	},
	{
		id: 1577,
		time: 1576,
		velocity: 20.4525,
		power: 8142.67445569932,
		road: 18158.6167592592,
		acceleration: 0.054722222222221
	},
	{
		id: 1578,
		time: 1577,
		velocity: 20.4391666666667,
		power: 8283.98258731544,
		road: 18179.0729629629,
		acceleration: 0.0594444444444449
	},
	{
		id: 1579,
		time: 1578,
		velocity: 20.5661111111111,
		power: 9152.93180347935,
		road: 18199.6089351852,
		acceleration: 0.100092592592592
	},
	{
		id: 1580,
		time: 1579,
		velocity: 20.7527777777778,
		power: 9410.6270354064,
		road: 18220.2491203704,
		acceleration: 0.108333333333334
	},
	{
		id: 1581,
		time: 1580,
		velocity: 20.7641666666667,
		power: 9043.52292413167,
		road: 18240.9861574074,
		acceleration: 0.0853703703703701
	},
	{
		id: 1582,
		time: 1581,
		velocity: 20.8222222222222,
		power: 9251.40487495834,
		road: 18261.8117592592,
		acceleration: 0.0917592592592591
	},
	{
		id: 1583,
		time: 1582,
		velocity: 21.0280555555556,
		power: 9649.30775922491,
		road: 18282.7367592592,
		acceleration: 0.107037037037035
	},
	{
		id: 1584,
		time: 1583,
		velocity: 21.0852777777778,
		power: 11640.339144391,
		road: 18303.8144907407,
		acceleration: 0.198425925925925
	},
	{
		id: 1585,
		time: 1584,
		velocity: 21.4175,
		power: 11754.4392795174,
		road: 18325.0884259259,
		acceleration: 0.193981481481487
	},
	{
		id: 1586,
		time: 1585,
		velocity: 21.61,
		power: 13863.4393961176,
		road: 18346.6013425926,
		acceleration: 0.283981481481479
	},
	{
		id: 1587,
		time: 1586,
		velocity: 21.9372222222222,
		power: 12752.2590441996,
		road: 18368.3646759259,
		acceleration: 0.216851851851853
	},
	{
		id: 1588,
		time: 1587,
		velocity: 22.0680555555556,
		power: 12308.1150551257,
		road: 18390.3290277778,
		acceleration: 0.185185185185183
	},
	{
		id: 1589,
		time: 1588,
		velocity: 22.1655555555556,
		power: 11129.7778569325,
		road: 18412.4468518518,
		acceleration: 0.121759259259257
	},
	{
		id: 1590,
		time: 1589,
		velocity: 22.3025,
		power: 11928.9154885877,
		road: 18434.7018055555,
		acceleration: 0.152500000000003
	},
	{
		id: 1591,
		time: 1590,
		velocity: 22.5255555555556,
		power: 11473.6611553864,
		road: 18457.0951851852,
		acceleration: 0.124351851851848
	},
	{
		id: 1592,
		time: 1591,
		velocity: 22.5386111111111,
		power: 9098.68779451385,
		road: 18479.5561574074,
		acceleration: 0.0108333333333377
	},
	{
		id: 1593,
		time: 1592,
		velocity: 22.335,
		power: 6392.21646410121,
		road: 18501.9661574074,
		acceleration: -0.112777777777779
	},
	{
		id: 1594,
		time: 1593,
		velocity: 22.1872222222222,
		power: 2578.94211336862,
		road: 18524.1777314815,
		acceleration: -0.284074074074073
	},
	{
		id: 1595,
		time: 1594,
		velocity: 21.6863888888889,
		power: 610.405501663266,
		road: 18546.063287037,
		acceleration: -0.367962962962963
	},
	{
		id: 1596,
		time: 1595,
		velocity: 21.2311111111111,
		power: -4118.98832742533,
		road: 18567.4718055555,
		acceleration: -0.586111111111112
	},
	{
		id: 1597,
		time: 1596,
		velocity: 20.4288888888889,
		power: -14140.3432069935,
		road: 18588.045787037,
		acceleration: -1.08296296296296
	},
	{
		id: 1598,
		time: 1597,
		velocity: 18.4375,
		power: -19639.8176636111,
		road: 18607.3788888889,
		acceleration: -1.3987962962963
	},
	{
		id: 1599,
		time: 1598,
		velocity: 17.0347222222222,
		power: -13999.8026307204,
		road: 18625.4521296296,
		acceleration: -1.12092592592592
	},
	{
		id: 1600,
		time: 1599,
		velocity: 17.0661111111111,
		power: 1086.95199013219,
		road: 18642.8485648148,
		acceleration: -0.232685185185186
	},
	{
		id: 1601,
		time: 1600,
		velocity: 17.7394444444444,
		power: 13143.0909783416,
		road: 18660.3703703704,
		acceleration: 0.483425925925928
	},
	{
		id: 1602,
		time: 1601,
		velocity: 18.485,
		power: 19969.2797450594,
		road: 18678.5507407407,
		acceleration: 0.833703703703705
	},
	{
		id: 1603,
		time: 1602,
		velocity: 19.5672222222222,
		power: 24905.8331950669,
		road: 18697.6616666666,
		acceleration: 1.0274074074074
	},
	{
		id: 1604,
		time: 1603,
		velocity: 20.8216666666667,
		power: 26560.7032991507,
		road: 18717.7966203704,
		acceleration: 1.02064814814815
	},
	{
		id: 1605,
		time: 1604,
		velocity: 21.5469444444444,
		power: 28216.5017662846,
		road: 18738.9478240741,
		acceleration: 1.01185185185186
	},
	{
		id: 1606,
		time: 1605,
		velocity: 22.6027777777778,
		power: 25975.370507229,
		road: 18761.0175462963,
		acceleration: 0.825185185185184
	},
	{
		id: 1607,
		time: 1606,
		velocity: 23.2972222222222,
		power: 31523.0162080315,
		road: 18784.0018981481,
		acceleration: 1.00407407407407
	},
	{
		id: 1608,
		time: 1607,
		velocity: 24.5591666666667,
		power: 32272.1951371726,
		road: 18807.9642129629,
		acceleration: 0.951851851851856
	},
	{
		id: 1609,
		time: 1608,
		velocity: 25.4583333333333,
		power: 27541.2468921719,
		road: 18832.7444444444,
		acceleration: 0.683981481481482
	},
	{
		id: 1610,
		time: 1609,
		velocity: 25.3491666666667,
		power: 14730.8300808879,
		road: 18857.9289814815,
		acceleration: 0.124629629629631
	},
	{
		id: 1611,
		time: 1610,
		velocity: 24.9330555555556,
		power: 4210.26438596968,
		road: 18883.0224074074,
		acceleration: -0.306851851851857
	},
	{
		id: 1612,
		time: 1611,
		velocity: 24.5377777777778,
		power: -657.237549241326,
		road: 18907.7134259259,
		acceleration: -0.497962962962959
	},
	{
		id: 1613,
		time: 1612,
		velocity: 23.8552777777778,
		power: -2118.23254077557,
		road: 18931.8818518518,
		acceleration: -0.547222222222228
	},
	{
		id: 1614,
		time: 1613,
		velocity: 23.2913888888889,
		power: -8236.42770829586,
		road: 18955.3750925926,
		acceleration: -0.803148148148143
	},
	{
		id: 1615,
		time: 1614,
		velocity: 22.1283333333333,
		power: -18635.2346320371,
		road: 18977.8286111111,
		acceleration: -1.2762962962963
	},
	{
		id: 1616,
		time: 1615,
		velocity: 20.0263888888889,
		power: -27828.1284545376,
		road: 18998.7634259259,
		acceleration: -1.76111111111111
	},
	{
		id: 1617,
		time: 1616,
		velocity: 18.0080555555556,
		power: -25176.2884004297,
		road: 19017.9654629629,
		acceleration: -1.70444444444444
	},
	{
		id: 1618,
		time: 1617,
		velocity: 17.015,
		power: -22376.2744911696,
		road: 19035.4978703703,
		acceleration: -1.63481481481482
	},
	{
		id: 1619,
		time: 1618,
		velocity: 15.1219444444444,
		power: -20946.1012853061,
		road: 19051.3883796296,
		acceleration: -1.64898148148148
	},
	{
		id: 1620,
		time: 1619,
		velocity: 13.0611111111111,
		power: -16493.403452552,
		road: 19065.7305555555,
		acceleration: -1.44768518518518
	},
	{
		id: 1621,
		time: 1620,
		velocity: 12.6719444444444,
		power: -4361.40122430047,
		road: 19079.0637037037,
		acceleration: -0.570370370370371
	},
	{
		id: 1622,
		time: 1621,
		velocity: 13.4108333333333,
		power: 6154.05180083198,
		road: 19092.2435648148,
		acceleration: 0.263796296296297
	},
	{
		id: 1623,
		time: 1622,
		velocity: 13.8525,
		power: 11559.5199419698,
		road: 19105.8829629629,
		acceleration: 0.655277777777778
	},
	{
		id: 1624,
		time: 1623,
		velocity: 14.6377777777778,
		power: 12522.0320812112,
		road: 19120.1868518518,
		acceleration: 0.673703703703701
	},
	{
		id: 1625,
		time: 1624,
		velocity: 15.4319444444444,
		power: 17874.4256878956,
		road: 19135.3174074074,
		acceleration: 0.979629629629629
	},
	{
		id: 1626,
		time: 1625,
		velocity: 16.7913888888889,
		power: 18484.9925963377,
		road: 19151.4020833333,
		acceleration: 0.928611111111112
	},
	{
		id: 1627,
		time: 1626,
		velocity: 17.4236111111111,
		power: 15030.6457413109,
		road: 19168.2728240741,
		acceleration: 0.643518518518519
	},
	{
		id: 1628,
		time: 1627,
		velocity: 17.3625,
		power: 8494.30930499042,
		road: 19185.5738425926,
		acceleration: 0.217037037037038
	},
	{
		id: 1629,
		time: 1628,
		velocity: 17.4425,
		power: 7612.52461825416,
		road: 19203.0609722222,
		acceleration: 0.155185185185182
	},
	{
		id: 1630,
		time: 1629,
		velocity: 17.8891666666667,
		power: 8342.46640227195,
		road: 19220.7209259259,
		acceleration: 0.190462962962965
	},
	{
		id: 1631,
		time: 1630,
		velocity: 17.9338888888889,
		power: 7447.07215033267,
		road: 19238.54125,
		acceleration: 0.130277777777778
	},
	{
		id: 1632,
		time: 1631,
		velocity: 17.8333333333333,
		power: 1415.93676165682,
		road: 19256.3156018518,
		acceleration: -0.222222222222221
	},
	{
		id: 1633,
		time: 1632,
		velocity: 17.2225,
		power: -1094.61242632345,
		road: 19273.7962962963,
		acceleration: -0.365092592592593
	},
	{
		id: 1634,
		time: 1633,
		velocity: 16.8386111111111,
		power: -5700.23360025006,
		road: 19290.7739814815,
		acceleration: -0.640925925925927
	},
	{
		id: 1635,
		time: 1634,
		velocity: 15.9105555555556,
		power: -4076.60609507959,
		road: 19307.1617129629,
		acceleration: -0.538981481481478
	},
	{
		id: 1636,
		time: 1635,
		velocity: 15.6055555555556,
		power: -3817.15688183374,
		road: 19323.0194444444,
		acceleration: -0.52101851851852
	},
	{
		id: 1637,
		time: 1636,
		velocity: 15.2755555555556,
		power: 652.078817586855,
		road: 19338.5073611111,
		acceleration: -0.218611111111111
	},
	{
		id: 1638,
		time: 1637,
		velocity: 15.2547222222222,
		power: 2890.32118821781,
		road: 19353.8543981481,
		acceleration: -0.063148148148148
	},
	{
		id: 1639,
		time: 1638,
		velocity: 15.4161111111111,
		power: 7895.45106403572,
		road: 19369.3061574074,
		acceleration: 0.272592592592593
	},
	{
		id: 1640,
		time: 1639,
		velocity: 16.0933333333333,
		power: 9829.34397490278,
		road: 19385.0861111111,
		acceleration: 0.383796296296296
	},
	{
		id: 1641,
		time: 1640,
		velocity: 16.4061111111111,
		power: 12949.1349855465,
		road: 19401.3364814815,
		acceleration: 0.557037037037038
	},
	{
		id: 1642,
		time: 1641,
		velocity: 17.0872222222222,
		power: 6655.1264247824,
		road: 19417.9336574074,
		acceleration: 0.136574074074073
	},
	{
		id: 1643,
		time: 1642,
		velocity: 16.5030555555556,
		power: 2659.32170856038,
		road: 19434.5413888889,
		acceleration: -0.115462962962962
	},
	{
		id: 1644,
		time: 1643,
		velocity: 16.0597222222222,
		power: -2415.68656237685,
		road: 19450.8751388889,
		acceleration: -0.432500000000001
	},
	{
		id: 1645,
		time: 1644,
		velocity: 15.7897222222222,
		power: -1131.6701658587,
		road: 19466.8201388889,
		acceleration: -0.345000000000001
	},
	{
		id: 1646,
		time: 1645,
		velocity: 15.4680555555556,
		power: 2334.43885003399,
		road: 19482.5369907407,
		acceleration: -0.111296296296295
	},
	{
		id: 1647,
		time: 1646,
		velocity: 15.7258333333333,
		power: 4533.74269712926,
		road: 19498.2164351852,
		acceleration: 0.0364814814814807
	},
	{
		id: 1648,
		time: 1647,
		velocity: 15.8991666666667,
		power: 6403.10962615498,
		road: 19513.9925462963,
		acceleration: 0.156851851851851
	},
	{
		id: 1649,
		time: 1648,
		velocity: 15.9386111111111,
		power: 4138.99256673033,
		road: 19529.8490277778,
		acceleration: 0.00388888888889127
	},
	{
		id: 1650,
		time: 1649,
		velocity: 15.7375,
		power: 1960.85438389982,
		road: 19545.6384259259,
		acceleration: -0.138055555555557
	},
	{
		id: 1651,
		time: 1650,
		velocity: 15.485,
		power: 1360.42832170859,
		road: 19561.2717129629,
		acceleration: -0.174166666666666
	},
	{
		id: 1652,
		time: 1651,
		velocity: 15.4161111111111,
		power: 3208.02850834986,
		road: 19576.7943981481,
		acceleration: -0.0470370370370361
	},
	{
		id: 1653,
		time: 1652,
		velocity: 15.5963888888889,
		power: 7240.02157824693,
		road: 19592.4037962963,
		acceleration: 0.220462962962962
	},
	{
		id: 1654,
		time: 1653,
		velocity: 16.1463888888889,
		power: 7650.74383745647,
		road: 19608.2416666666,
		acceleration: 0.236481481481482
	},
	{
		id: 1655,
		time: 1654,
		velocity: 16.1255555555556,
		power: 4092.89362821384,
		road: 19624.1964814815,
		acceleration: -0.00259259259259537
	},
	{
		id: 1656,
		time: 1655,
		velocity: 15.5886111111111,
		power: 363.133986125778,
		road: 19640.0276388889,
		acceleration: -0.244722222222222
	},
	{
		id: 1657,
		time: 1656,
		velocity: 15.4122222222222,
		power: -6965.59244464635,
		road: 19655.3687962963,
		acceleration: -0.735277777777776
	},
	{
		id: 1658,
		time: 1657,
		velocity: 13.9197222222222,
		power: -10909.549595542,
		road: 19669.8245833333,
		acceleration: -1.03546296296296
	},
	{
		id: 1659,
		time: 1658,
		velocity: 12.4822222222222,
		power: -21144.8686141159,
		road: 19682.7973148148,
		acceleration: -1.93064814814815
	},
	{
		id: 1660,
		time: 1659,
		velocity: 9.62027777777778,
		power: -22224.52381561,
		road: 19693.6315740741,
		acceleration: -2.34629629629629
	},
	{
		id: 1661,
		time: 1660,
		velocity: 6.88083333333333,
		power: -18109.7907338135,
		road: 19702.0829629629,
		acceleration: -2.41944444444444
	},
	{
		id: 1662,
		time: 1661,
		velocity: 5.22388888888889,
		power: -10439.1650620147,
		road: 19708.3773611111,
		acceleration: -1.89453703703704
	},
	{
		id: 1663,
		time: 1662,
		velocity: 3.93666666666667,
		power: -4469.73727851372,
		road: 19713.1626388889,
		acceleration: -1.1237037037037
	},
	{
		id: 1664,
		time: 1663,
		velocity: 3.50972222222222,
		power: -2985.92954957102,
		road: 19716.8973148148,
		acceleration: -0.9775
	},
	{
		id: 1665,
		time: 1664,
		velocity: 2.29138888888889,
		power: -1225.07107119445,
		road: 19719.8591203704,
		acceleration: -0.568240740740741
	},
	{
		id: 1666,
		time: 1665,
		velocity: 2.23194444444444,
		power: -772.241031344184,
		road: 19722.305,
		acceleration: -0.463611111111111
	},
	{
		id: 1667,
		time: 1666,
		velocity: 2.11888888888889,
		power: 72.0508100975367,
		road: 19724.4715740741,
		acceleration: -0.0950000000000002
	},
	{
		id: 1668,
		time: 1667,
		velocity: 2.00638888888889,
		power: 92.7641892887602,
		road: 19726.5492592592,
		acceleration: -0.0827777777777774
	},
	{
		id: 1669,
		time: 1668,
		velocity: 1.98361111111111,
		power: 109.348877017326,
		road: 19728.549537037,
		acceleration: -0.0720370370370371
	},
	{
		id: 1670,
		time: 1669,
		velocity: 1.90277777777778,
		power: 35.608876122906,
		road: 19730.4588888889,
		acceleration: -0.109814814814815
	},
	{
		id: 1671,
		time: 1670,
		velocity: 1.67694444444444,
		power: -365.918585775305,
		road: 19732.1336111111,
		acceleration: -0.359444444444445
	},
	{
		id: 1672,
		time: 1671,
		velocity: 0.905277777777778,
		power: -564.973885143452,
		road: 19733.3114814815,
		acceleration: -0.634259259259259
	},
	{
		id: 1673,
		time: 1672,
		velocity: 0,
		power: -237.553431508984,
		road: 19733.8927314815,
		acceleration: -0.558981481481482
	},
	{
		id: 1674,
		time: 1673,
		velocity: 0,
		power: -24.9040857862248,
		road: 19734.0436111111,
		acceleration: -0.301759259259259
	},
	{
		id: 1675,
		time: 1674,
		velocity: 0,
		power: 0,
		road: 19734.0436111111,
		acceleration: 0
	},
	{
		id: 1676,
		time: 1675,
		velocity: 0,
		power: 0,
		road: 19734.0436111111,
		acceleration: 0
	},
	{
		id: 1677,
		time: 1676,
		velocity: 0,
		power: 0,
		road: 19734.0436111111,
		acceleration: 0
	},
	{
		id: 1678,
		time: 1677,
		velocity: 0,
		power: 0,
		road: 19734.0436111111,
		acceleration: 0
	},
	{
		id: 1679,
		time: 1678,
		velocity: 0,
		power: 213.228097593216,
		road: 19734.3486574074,
		acceleration: 0.610092592592593
	},
	{
		id: 1680,
		time: 1679,
		velocity: 1.83027777777778,
		power: 1563.79439358071,
		road: 19735.5698611111,
		acceleration: 1.22222222222222
	},
	{
		id: 1681,
		time: 1680,
		velocity: 3.66666666666667,
		power: 4110.78539653127,
		road: 19738.1696759259,
		acceleration: 1.535
	},
	{
		id: 1682,
		time: 1681,
		velocity: 4.605,
		power: 5058.58146874966,
		road: 19742.1397222222,
		acceleration: 1.20546296296296
	},
	{
		id: 1683,
		time: 1682,
		velocity: 5.44666666666667,
		power: 4239.41709965919,
		road: 19747.0923611111,
		acceleration: 0.759722222222222
	},
	{
		id: 1684,
		time: 1683,
		velocity: 5.94583333333333,
		power: 4707.11219495169,
		road: 19752.7868518518,
		acceleration: 0.723981481481482
	},
	{
		id: 1685,
		time: 1684,
		velocity: 6.77694444444444,
		power: 6481.66959916456,
		road: 19759.2916203704,
		acceleration: 0.896574074074074
	},
	{
		id: 1686,
		time: 1685,
		velocity: 8.13638888888889,
		power: 6093.21813465199,
		road: 19766.6038425926,
		acceleration: 0.718333333333334
	},
	{
		id: 1687,
		time: 1686,
		velocity: 8.10083333333333,
		power: 3188.03927639483,
		road: 19774.4089814815,
		acceleration: 0.2675
	},
	{
		id: 1688,
		time: 1687,
		velocity: 7.57944444444444,
		power: -924.719053949333,
		road: 19782.2047222222,
		acceleration: -0.286296296296296
	},
	{
		id: 1689,
		time: 1688,
		velocity: 7.2775,
		power: -1584.30207011593,
		road: 19789.6663425926,
		acceleration: -0.381944444444446
	},
	{
		id: 1690,
		time: 1689,
		velocity: 6.955,
		power: -872.056976366552,
		road: 19796.7946759259,
		acceleration: -0.28462962962963
	},
	{
		id: 1691,
		time: 1690,
		velocity: 6.72555555555556,
		power: -915.774710083791,
		road: 19803.6334259259,
		acceleration: -0.294537037037036
	},
	{
		id: 1692,
		time: 1691,
		velocity: 6.39388888888889,
		power: -2666.665375295,
		road: 19810.0304629629,
		acceleration: -0.588888888888889
	},
	{
		id: 1693,
		time: 1692,
		velocity: 5.18833333333333,
		power: -3893.87080271707,
		road: 19815.6988888889,
		acceleration: -0.868333333333333
	},
	{
		id: 1694,
		time: 1693,
		velocity: 4.12055555555556,
		power: -3892.05364084535,
		road: 19820.4300925926,
		acceleration: -1.00611111111111
	},
	{
		id: 1695,
		time: 1694,
		velocity: 3.37555555555556,
		power: -2344.29174822061,
		road: 19824.26875,
		acceleration: -0.778981481481481
	},
	{
		id: 1696,
		time: 1695,
		velocity: 2.85138888888889,
		power: -2898.18674857325,
		road: 19827.1153703704,
		acceleration: -1.20509259259259
	},
	{
		id: 1697,
		time: 1696,
		velocity: 0.505277777777778,
		power: -1588.10442821613,
		road: 19828.7968518518,
		acceleration: -1.12518518518518
	},
	{
		id: 1698,
		time: 1697,
		velocity: 0,
		power: -501.798727478255,
		road: 19829.4405092592,
		acceleration: -0.950462962962963
	},
	{
		id: 1699,
		time: 1698,
		velocity: 0,
		power: -3.26270566926575,
		road: 19829.5247222222,
		acceleration: -0.168425925925926
	},
	{
		id: 1700,
		time: 1699,
		velocity: 0,
		power: 1.39765480306105,
		road: 19829.5347222222,
		acceleration: 0.02
	},
	{
		id: 1701,
		time: 1700,
		velocity: 0.06,
		power: 2.41636223770105,
		road: 19829.5547222222,
		acceleration: 0
	},
	{
		id: 1702,
		time: 1701,
		velocity: 0,
		power: 2.41636223770105,
		road: 19829.5747222222,
		acceleration: 0
	},
	{
		id: 1703,
		time: 1702,
		velocity: 0,
		power: 1.01870526315789,
		road: 19829.5847222222,
		acceleration: -0.02
	},
	{
		id: 1704,
		time: 1703,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1705,
		time: 1704,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1706,
		time: 1705,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1707,
		time: 1706,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1708,
		time: 1707,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1709,
		time: 1708,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1710,
		time: 1709,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1711,
		time: 1710,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1712,
		time: 1711,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1713,
		time: 1712,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1714,
		time: 1713,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1715,
		time: 1714,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1716,
		time: 1715,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1717,
		time: 1716,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1718,
		time: 1717,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1719,
		time: 1718,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1720,
		time: 1719,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1721,
		time: 1720,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1722,
		time: 1721,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1723,
		time: 1722,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1724,
		time: 1723,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1725,
		time: 1724,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1726,
		time: 1725,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1727,
		time: 1726,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1728,
		time: 1727,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1729,
		time: 1728,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1730,
		time: 1729,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1731,
		time: 1730,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1732,
		time: 1731,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1733,
		time: 1732,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1734,
		time: 1733,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1735,
		time: 1734,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1736,
		time: 1735,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1737,
		time: 1736,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1738,
		time: 1737,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1739,
		time: 1738,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1740,
		time: 1739,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1741,
		time: 1740,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1742,
		time: 1741,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1743,
		time: 1742,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1744,
		time: 1743,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1745,
		time: 1744,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1746,
		time: 1745,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1747,
		time: 1746,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1748,
		time: 1747,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1749,
		time: 1748,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1750,
		time: 1749,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1751,
		time: 1750,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1752,
		time: 1751,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1753,
		time: 1752,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1754,
		time: 1753,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1755,
		time: 1754,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1756,
		time: 1755,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1757,
		time: 1756,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1758,
		time: 1757,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1759,
		time: 1758,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1760,
		time: 1759,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1761,
		time: 1760,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1762,
		time: 1761,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1763,
		time: 1762,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1764,
		time: 1763,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1765,
		time: 1764,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1766,
		time: 1765,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1767,
		time: 1766,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1768,
		time: 1767,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1769,
		time: 1768,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1770,
		time: 1769,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1771,
		time: 1770,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1772,
		time: 1771,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1773,
		time: 1772,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1774,
		time: 1773,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1775,
		time: 1774,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1776,
		time: 1775,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1777,
		time: 1776,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1778,
		time: 1777,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1779,
		time: 1778,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1780,
		time: 1779,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1781,
		time: 1780,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1782,
		time: 1781,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1783,
		time: 1782,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1784,
		time: 1783,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1785,
		time: 1784,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1786,
		time: 1785,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1787,
		time: 1786,
		velocity: 0,
		power: 0,
		road: 19829.5847222222,
		acceleration: 0
	},
	{
		id: 1788,
		time: 1787,
		velocity: 0,
		power: 126.533852827698,
		road: 19829.8131944444,
		acceleration: 0.456944444444444
	},
	{
		id: 1789,
		time: 1788,
		velocity: 1.37083333333333,
		power: 1078.61685795546,
		road: 19830.7890740741,
		acceleration: 1.03787037037037
	},
	{
		id: 1790,
		time: 1789,
		velocity: 3.11361111111111,
		power: 3885.69628082057,
		road: 19833.1034259259,
		acceleration: 1.63907407407407
	},
	{
		id: 1791,
		time: 1790,
		velocity: 4.91722222222222,
		power: 5589.71670855679,
		road: 19836.9371759259,
		acceleration: 1.39972222222222
	},
	{
		id: 1792,
		time: 1791,
		velocity: 5.57,
		power: 5827.23807295153,
		road: 19842.0049074074,
		acceleration: 1.06824074074074
	},
	{
		id: 1793,
		time: 1792,
		velocity: 6.31833333333333,
		power: 3091.43505870811,
		road: 19847.8135185185,
		acceleration: 0.413518518518519
	},
	{
		id: 1794,
		time: 1793,
		velocity: 6.15777777777778,
		power: 1887.70924503924,
		road: 19853.9173611111,
		acceleration: 0.176944444444445
	},
	{
		id: 1795,
		time: 1794,
		velocity: 6.10083333333333,
		power: 1428.57990800269,
		road: 19860.155462963,
		acceleration: 0.0915740740740745
	},
	{
		id: 1796,
		time: 1795,
		velocity: 6.59305555555556,
		power: 3004.60582691968,
		road: 19866.60875,
		acceleration: 0.338796296296297
	},
	{
		id: 1797,
		time: 1796,
		velocity: 7.17416666666667,
		power: 5615.75453211174,
		road: 19873.5776388889,
		acceleration: 0.692407407407407
	},
	{
		id: 1798,
		time: 1797,
		velocity: 8.17805555555556,
		power: 6852.38257550491,
		road: 19881.2797685185,
		acceleration: 0.774074074074075
	},
	{
		id: 1799,
		time: 1798,
		velocity: 8.91527777777778,
		power: 6885.9595343179,
		road: 19889.7139814815,
		acceleration: 0.690092592592592
	},
	{
		id: 1800,
		time: 1799,
		velocity: 9.24444444444445,
		power: 6207.75494240329,
		road: 19898.7664814815,
		acceleration: 0.546481481481482
	},
	{
		id: 1801,
		time: 1800,
		velocity: 9.8175,
		power: 6775.11063453556,
		road: 19908.3726851852,
		acceleration: 0.560925925925927
	},
	{
		id: 1802,
		time: 1801,
		velocity: 10.5980555555556,
		power: 7014.07189099825,
		road: 19918.5289351852,
		acceleration: 0.539166666666667
	},
	{
		id: 1803,
		time: 1802,
		velocity: 10.8619444444444,
		power: 5836.26782878143,
		road: 19929.1475925926,
		acceleration: 0.385648148148146
	},
	{
		id: 1804,
		time: 1803,
		velocity: 10.9744444444444,
		power: 4712.34177980705,
		road: 19940.0875462963,
		acceleration: 0.256944444444446
	},
	{
		id: 1805,
		time: 1804,
		velocity: 11.3688888888889,
		power: 3538.48103303346,
		road: 19951.2239351852,
		acceleration: 0.135925925925925
	},
	{
		id: 1806,
		time: 1805,
		velocity: 11.2697222222222,
		power: 2645.62634239051,
		road: 19962.4525925926,
		acceleration: 0.0486111111111107
	},
	{
		id: 1807,
		time: 1806,
		velocity: 11.1202777777778,
		power: -677.042073695342,
		road: 19973.5750462963,
		acceleration: -0.261018518518519
	},
	{
		id: 1808,
		time: 1807,
		velocity: 10.5858333333333,
		power: 233.272370803868,
		road: 19984.4809722222,
		acceleration: -0.172037037037036
	},
	{
		id: 1809,
		time: 1808,
		velocity: 10.7536111111111,
		power: 2828.73700297943,
		road: 19995.3405555555,
		acceleration: 0.0793518518518503
	},
	{
		id: 1810,
		time: 1809,
		velocity: 11.3583333333333,
		power: 5553.18954675606,
		road: 20006.4048148148,
		acceleration: 0.33
	},
	{
		id: 1811,
		time: 1810,
		velocity: 11.5758333333333,
		power: 3174.57854538105,
		road: 20017.6821296296,
		acceleration: 0.0961111111111119
	},
	{
		id: 1812,
		time: 1811,
		velocity: 11.0419444444444,
		power: -2996.69088287203,
		road: 20028.7673611111,
		acceleration: -0.480277777777777
	},
	{
		id: 1813,
		time: 1812,
		velocity: 9.9175,
		power: -7959.65077611219,
		road: 20039.1149074074,
		acceleration: -0.995092592592593
	},
	{
		id: 1814,
		time: 1813,
		velocity: 8.59055555555556,
		power: -15021.7388467391,
		road: 20047.9899537037,
		acceleration: -1.94990740740741
	},
	{
		id: 1815,
		time: 1814,
		velocity: 5.19222222222222,
		power: -14526.373470169,
		road: 20054.6700462963,
		acceleration: -2.44
	},
	{
		id: 1816,
		time: 1815,
		velocity: 2.5975,
		power: -7760.01367837097,
		road: 20059.1483333333,
		acceleration: -1.96361111111111
	},
	{
		id: 1817,
		time: 1816,
		velocity: 2.69972222222222,
		power: -2276.86380260815,
		road: 20062.1831944444,
		acceleration: -0.923240740740741
	},
	{
		id: 1818,
		time: 1817,
		velocity: 2.4225,
		power: 1473.94679740433,
		road: 20064.9693055555,
		acceleration: 0.425740740740741
	},
	{
		id: 1819,
		time: 1818,
		velocity: 3.87472222222222,
		power: 1971.65862132187,
		road: 20068.2210185185,
		acceleration: 0.505462962962962
	},
	{
		id: 1820,
		time: 1819,
		velocity: 4.21611111111111,
		power: 5653.3381761152,
		road: 20072.3736111111,
		acceleration: 1.2962962962963
	},
	{
		id: 1821,
		time: 1820,
		velocity: 6.31138888888889,
		power: 4830.71017845889,
		road: 20077.5902314815,
		acceleration: 0.83175925925926
	},
	{
		id: 1822,
		time: 1821,
		velocity: 6.37,
		power: 6422.44054897953,
		road: 20083.7012037037,
		acceleration: 0.956944444444443
	},
	{
		id: 1823,
		time: 1822,
		velocity: 7.08694444444444,
		power: 2221.8421650004,
		road: 20090.3890277778,
		acceleration: 0.19675925925926
	},
	{
		id: 1824,
		time: 1823,
		velocity: 6.90166666666667,
		power: 5602.04756090743,
		road: 20097.5106944444,
		acceleration: 0.670925925925927
	},
	{
		id: 1825,
		time: 1824,
		velocity: 8.38277777777778,
		power: 8601.81839627377,
		road: 20105.4551388889,
		acceleration: 0.974629629629631
	},
	{
		id: 1826,
		time: 1825,
		velocity: 10.0108333333333,
		power: 18345.4837239658,
		road: 20114.8259259259,
		acceleration: 1.87805555555555
	},
	{
		id: 1827,
		time: 1826,
		velocity: 12.5358333333333,
		power: 20675.0364890981,
		road: 20126.0063425926,
		acceleration: 1.7412037037037
	},
	{
		id: 1828,
		time: 1827,
		velocity: 13.6063888888889,
		power: 16666.4875357737,
		road: 20138.6398148148,
		acceleration: 1.16490740740741
	},
	{
		id: 1829,
		time: 1828,
		velocity: 13.5055555555556,
		power: 6825.31361525829,
		road: 20152.0090277778,
		acceleration: 0.306574074074074
	},
	{
		id: 1830,
		time: 1829,
		velocity: 13.4555555555556,
		power: 1061.24335078212,
		road: 20165.4581481481,
		acceleration: -0.146759259259259
	},
	{
		id: 1831,
		time: 1830,
		velocity: 13.1661111111111,
		power: 843.342906647466,
		road: 20178.7535648148,
		acceleration: -0.160648148148148
	},
	{
		id: 1832,
		time: 1831,
		velocity: 13.0236111111111,
		power: -580.171116824254,
		road: 20191.8334722222,
		acceleration: -0.270370370370369
	},
	{
		id: 1833,
		time: 1832,
		velocity: 12.6444444444444,
		power: -138.900042119838,
		road: 20204.6624074074,
		acceleration: -0.231574074074075
	},
	{
		id: 1834,
		time: 1833,
		velocity: 12.4713888888889,
		power: -679.532895303992,
		road: 20217.2389351852,
		acceleration: -0.273240740740739
	},
	{
		id: 1835,
		time: 1834,
		velocity: 12.2038888888889,
		power: 671.283697976513,
		road: 20229.6005092593,
		acceleration: -0.15666666666667
	},
	{
		id: 1836,
		time: 1835,
		velocity: 12.1744444444444,
		power: 1023.07884411371,
		road: 20241.8218055555,
		acceleration: -0.123888888888887
	},
	{
		id: 1837,
		time: 1836,
		velocity: 12.0997222222222,
		power: 695.366978570047,
		road: 20253.9064351852,
		acceleration: -0.149444444444446
	},
	{
		id: 1838,
		time: 1837,
		velocity: 11.7555555555556,
		power: 139.179211342403,
		road: 20265.81875,
		acceleration: -0.195185185185185
	},
	{
		id: 1839,
		time: 1838,
		velocity: 11.5888888888889,
		power: 36.9718643636264,
		road: 20277.5327314815,
		acceleration: -0.20148148148148
	},
	{
		id: 1840,
		time: 1839,
		velocity: 11.4952777777778,
		power: 2400.25538354417,
		road: 20289.1525,
		acceleration: 0.0130555555555549
	},
	{
		id: 1841,
		time: 1840,
		velocity: 11.7947222222222,
		power: 3277.0873446852,
		road: 20300.8238888889,
		acceleration: 0.0901851851851863
	},
	{
		id: 1842,
		time: 1841,
		velocity: 11.8594444444444,
		power: 4848.10411654063,
		road: 20312.6520833333,
		acceleration: 0.223425925925923
	},
	{
		id: 1843,
		time: 1842,
		velocity: 12.1655555555556,
		power: 2262.77728236975,
		road: 20324.5875,
		acceleration: -0.00898148148148081
	},
	{
		id: 1844,
		time: 1843,
		velocity: 11.7677777777778,
		power: -290.864534035931,
		road: 20336.4024537037,
		acceleration: -0.231944444444443
	},
	{
		id: 1845,
		time: 1844,
		velocity: 11.1636111111111,
		power: -1083.60817694978,
		road: 20347.9509259259,
		acceleration: -0.301018518518518
	},
	{
		id: 1846,
		time: 1845,
		velocity: 11.2625,
		power: -1991.48345393521,
		road: 20359.1565740741,
		acceleration: -0.384629629629631
	},
	{
		id: 1847,
		time: 1846,
		velocity: 10.6138888888889,
		power: -2053.89639267266,
		road: 20369.9736111111,
		acceleration: -0.392592592592592
	},
	{
		id: 1848,
		time: 1847,
		velocity: 9.98583333333333,
		power: -5190.14343611419,
		road: 20380.2355555555,
		acceleration: -0.717592592592593
	},
	{
		id: 1849,
		time: 1848,
		velocity: 9.10972222222222,
		power: -9856.939544256,
		road: 20389.4916666667,
		acceleration: -1.29407407407407
	},
	{
		id: 1850,
		time: 1849,
		velocity: 6.73166666666667,
		power: -9142.60222383429,
		road: 20397.4128240741,
		acceleration: -1.37583333333333
	},
	{
		id: 1851,
		time: 1850,
		velocity: 5.85833333333333,
		power: -6236.24066621276,
		road: 20404.0778240741,
		acceleration: -1.13648148148148
	},
	{
		id: 1852,
		time: 1851,
		velocity: 5.70027777777778,
		power: -748.932025780467,
		road: 20410.0347685185,
		acceleration: -0.279629629629631
	},
	{
		id: 1853,
		time: 1852,
		velocity: 5.89277777777778,
		power: 992.221389117706,
		road: 20415.8681018518,
		acceleration: 0.0324074074074074
	},
	{
		id: 1854,
		time: 1853,
		velocity: 5.95555555555556,
		power: 1977.81532937117,
		road: 20421.8187962963,
		acceleration: 0.202314814814814
	},
	{
		id: 1855,
		time: 1854,
		velocity: 6.30722222222222,
		power: 1494.03047926769,
		road: 20427.9251388889,
		acceleration: 0.108981481481482
	},
	{
		id: 1856,
		time: 1855,
		velocity: 6.21972222222222,
		power: 1251.06177016625,
		road: 20434.1177314815,
		acceleration: 0.063518518518519
	},
	{
		id: 1857,
		time: 1856,
		velocity: 6.14611111111111,
		power: 1201.44408079097,
		road: 20440.3684722222,
		acceleration: 0.0527777777777771
	},
	{
		id: 1858,
		time: 1857,
		velocity: 6.46555555555556,
		power: 1181.05889100071,
		road: 20446.6693055555,
		acceleration: 0.0474074074074071
	},
	{
		id: 1859,
		time: 1858,
		velocity: 6.36194444444444,
		power: -1132.54584411809,
		road: 20452.8226851852,
		acceleration: -0.342314814814815
	},
	{
		id: 1860,
		time: 1859,
		velocity: 5.11916666666667,
		power: -3953.69294611068,
		road: 20458.3566666666,
		acceleration: -0.896481481481482
	},
	{
		id: 1861,
		time: 1860,
		velocity: 3.77611111111111,
		power: -3848.8459937824,
		road: 20462.9297222222,
		acceleration: -1.02537037037037
	},
	{
		id: 1862,
		time: 1861,
		velocity: 3.28583333333333,
		power: -476.063228467136,
		road: 20466.8582407407,
		acceleration: -0.263703703703704
	},
	{
		id: 1863,
		time: 1862,
		velocity: 4.32805555555556,
		power: 2657.57976515239,
		road: 20470.9301851852,
		acceleration: 0.550555555555555
	},
	{
		id: 1864,
		time: 1863,
		velocity: 5.42777777777778,
		power: 5654.46265291713,
		road: 20475.8160185185,
		acceleration: 1.07722222222222
	},
	{
		id: 1865,
		time: 1864,
		velocity: 6.5175,
		power: 6953.19647891577,
		road: 20481.7799074074,
		acceleration: 1.07888888888889
	},
	{
		id: 1866,
		time: 1865,
		velocity: 7.56472222222222,
		power: 10726.3782100973,
		road: 20488.987037037,
		acceleration: 1.40759259259259
	},
	{
		id: 1867,
		time: 1866,
		velocity: 9.65055555555555,
		power: 16955.7006970809,
		road: 20497.8200462963,
		acceleration: 1.84416666666667
	},
	{
		id: 1868,
		time: 1867,
		velocity: 12.05,
		power: 22572.7466428772,
		road: 20508.5790277778,
		acceleration: 2.00777777777778
	},
	{
		id: 1869,
		time: 1868,
		velocity: 13.5880555555556,
		power: 24808.3995964977,
		road: 20521.2578703704,
		acceleration: 1.83194444444445
	},
	{
		id: 1870,
		time: 1869,
		velocity: 15.1463888888889,
		power: 23113.2286670044,
		road: 20535.5760648148,
		acceleration: 1.44675925925926
	},
	{
		id: 1871,
		time: 1870,
		velocity: 16.3902777777778,
		power: 35295.026842499,
		road: 20551.6306944444,
		acceleration: 2.02611111111111
	},
	{
		id: 1872,
		time: 1871,
		velocity: 19.6663888888889,
		power: 29746.2625018827,
		road: 20569.4190277778,
		acceleration: 1.4412962962963
	},
	{
		id: 1873,
		time: 1872,
		velocity: 19.4702777777778,
		power: 20322.3631237942,
		road: 20588.3248148148,
		acceleration: 0.793611111111108
	},
	{
		id: 1874,
		time: 1873,
		velocity: 18.7711111111111,
		power: -1553.54546858668,
		road: 20607.4185185185,
		acceleration: -0.417777777777779
	},
	{
		id: 1875,
		time: 1874,
		velocity: 18.4130555555556,
		power: 5264.26839758339,
		road: 20626.2850462963,
		acceleration: -0.0365740740740748
	},
	{
		id: 1876,
		time: 1875,
		velocity: 19.3605555555556,
		power: 26844.8243898261,
		road: 20645.6857407407,
		acceleration: 1.10490740740741
	},
	{
		id: 1877,
		time: 1876,
		velocity: 22.0858333333333,
		power: 31497.24691699,
		road: 20666.2547222222,
		acceleration: 1.23166666666667
	},
	{
		id: 1878,
		time: 1877,
		velocity: 22.1080555555556,
		power: 24754.059306214,
		road: 20687.8424074074,
		acceleration: 0.805740740740742
	},
	{
		id: 1879,
		time: 1878,
		velocity: 21.7777777777778,
		power: 5297.78727698979,
		road: 20709.7600462963,
		acceleration: -0.145833333333336
	},
	{
		id: 1880,
		time: 1879,
		velocity: 21.6483333333333,
		power: 2189.8259809947,
		road: 20731.4611111111,
		acceleration: -0.287314814814813
	},
	{
		id: 1881,
		time: 1880,
		velocity: 21.2461111111111,
		power: 2229.98153547517,
		road: 20752.8799537037,
		acceleration: -0.277129629629631
	},
	{
		id: 1882,
		time: 1881,
		velocity: 20.9463888888889,
		power: -2970.85233158794,
		road: 20773.8984259259,
		acceleration: -0.523611111111112
	},
	{
		id: 1883,
		time: 1882,
		velocity: 20.0775,
		power: -3920.95462188381,
		road: 20794.3734259259,
		acceleration: -0.563333333333333
	},
	{
		id: 1884,
		time: 1883,
		velocity: 19.5561111111111,
		power: -5357.23948034548,
		road: 20814.2511111111,
		acceleration: -0.631296296296298
	},
	{
		id: 1885,
		time: 1884,
		velocity: 19.0525,
		power: -4753.87442625443,
		road: 20833.5160648148,
		acceleration: -0.594166666666666
	},
	{
		id: 1886,
		time: 1885,
		velocity: 18.295,
		power: -6506.37115049109,
		road: 20852.1400462963,
		acceleration: -0.687777777777775
	},
	{
		id: 1887,
		time: 1886,
		velocity: 17.4927777777778,
		power: -16502.8570594603,
		road: 20869.7798148148,
		acceleration: -1.28064814814815
	},
	{
		id: 1888,
		time: 1887,
		velocity: 15.2105555555556,
		power: -16827.1010555288,
		road: 20886.10125,
		acceleration: -1.35601851851852
	},
	{
		id: 1889,
		time: 1888,
		velocity: 14.2269444444444,
		power: -15992.631222666,
		road: 20901.0582407407,
		acceleration: -1.37287037037037
	},
	{
		id: 1890,
		time: 1889,
		velocity: 13.3741666666667,
		power: -6810.47527320151,
		road: 20914.9539814815,
		acceleration: -0.749629629629631
	},
	{
		id: 1891,
		time: 1890,
		velocity: 12.9616666666667,
		power: -5401.49503504146,
		road: 20928.1476388889,
		acceleration: -0.654537037037034
	},
	{
		id: 1892,
		time: 1891,
		velocity: 12.2633333333333,
		power: -3304.56348315607,
		road: 20940.7681944444,
		acceleration: -0.491666666666667
	},
	{
		id: 1893,
		time: 1892,
		velocity: 11.8991666666667,
		power: -3773.57299463863,
		road: 20952.8744907407,
		acceleration: -0.536851851851853
	},
	{
		id: 1894,
		time: 1893,
		velocity: 11.3511111111111,
		power: -6465.95655340618,
		road: 20964.3153703704,
		acceleration: -0.793981481481481
	},
	{
		id: 1895,
		time: 1894,
		velocity: 9.88138888888889,
		power: -9628.72902637469,
		road: 20974.7819444444,
		acceleration: -1.15462962962963
	},
	{
		id: 1896,
		time: 1895,
		velocity: 8.43527777777778,
		power: -12665.1639849343,
		road: 20983.8508796296,
		acceleration: -1.64064814814815
	},
	{
		id: 1897,
		time: 1896,
		velocity: 6.42916666666667,
		power: -8262.82251824096,
		road: 20991.4478703704,
		acceleration: -1.30324074074074
	},
	{
		id: 1898,
		time: 1897,
		velocity: 5.97166666666667,
		power: -5306.76898313394,
		road: 20997.8842592592,
		acceleration: -1.01796296296296
	},
	{
		id: 1899,
		time: 1898,
		velocity: 5.38138888888889,
		power: -2010.96831763053,
		road: 21003.5522685185,
		acceleration: -0.518796296296297
	},
	{
		id: 1900,
		time: 1899,
		velocity: 4.87277777777778,
		power: -315.620901867692,
		road: 21008.8579629629,
		acceleration: -0.205833333333333
	},
	{
		id: 1901,
		time: 1900,
		velocity: 5.35416666666667,
		power: 388.186564768832,
		road: 21014.0290277778,
		acceleration: -0.0634259259259267
	},
	{
		id: 1902,
		time: 1901,
		velocity: 5.19111111111111,
		power: 959.393921013059,
		road: 21019.1949074074,
		acceleration: 0.053055555555555
	},
	{
		id: 1903,
		time: 1902,
		velocity: 5.03194444444444,
		power: 249.375320404219,
		road: 21024.3416666666,
		acceleration: -0.0912962962962958
	},
	{
		id: 1904,
		time: 1903,
		velocity: 5.08027777777778,
		power: 1414.92987596339,
		road: 21029.5154629629,
		acceleration: 0.145370370370371
	},
	{
		id: 1905,
		time: 1904,
		velocity: 5.62722222222222,
		power: 2263.45755403318,
		road: 21034.910787037,
		acceleration: 0.297685185185185
	},
	{
		id: 1906,
		time: 1905,
		velocity: 5.925,
		power: 1863.22365118104,
		road: 21040.5559259259,
		acceleration: 0.201944444444445
	},
	{
		id: 1907,
		time: 1906,
		velocity: 5.68611111111111,
		power: -1085.14355197861,
		road: 21046.1271296296,
		acceleration: -0.349814814814815
	},
	{
		id: 1908,
		time: 1907,
		velocity: 4.57777777777778,
		power: -3725.5249782096,
		road: 21051.0549537037,
		acceleration: -0.936944444444444
	},
	{
		id: 1909,
		time: 1908,
		velocity: 3.11416666666667,
		power: -4721.03614486556,
		road: 21054.7788888889,
		acceleration: -1.47083333333333
	},
	{
		id: 1910,
		time: 1909,
		velocity: 1.27361111111111,
		power: -2136.06524009308,
		road: 21057.2455092592,
		acceleration: -1.0437962962963
	},
	{
		id: 1911,
		time: 1910,
		velocity: 1.44638888888889,
		power: -757.702305168367,
		road: 21058.8815277778,
		acceleration: -0.617407407407407
	},
	{
		id: 1912,
		time: 1911,
		velocity: 1.26194444444444,
		power: 494.279290188675,
		road: 21060.3250925926,
		acceleration: 0.2325
	},
	{
		id: 1913,
		time: 1912,
		velocity: 1.97111111111111,
		power: 1897.69587150814,
		road: 21062.3212037037,
		acceleration: 0.872592592592593
	},
	{
		id: 1914,
		time: 1913,
		velocity: 4.06416666666667,
		power: 4529.86117634363,
		road: 21065.4498148148,
		acceleration: 1.39240740740741
	},
	{
		id: 1915,
		time: 1914,
		velocity: 5.43916666666667,
		power: 5483.56725255644,
		road: 21069.8599537037,
		acceleration: 1.17064814814815
	},
	{
		id: 1916,
		time: 1915,
		velocity: 5.48305555555556,
		power: 2963.22089069608,
		road: 21075.0825925926,
		acceleration: 0.454351851851852
	},
	{
		id: 1917,
		time: 1916,
		velocity: 5.42722222222222,
		power: 858.620165379678,
		road: 21080.5430555555,
		acceleration: 0.0212962962962964
	},
	{
		id: 1918,
		time: 1917,
		velocity: 5.50305555555556,
		power: 264.747267712009,
		road: 21085.9678703704,
		acceleration: -0.0925925925925926
	},
	{
		id: 1919,
		time: 1918,
		velocity: 5.20527777777778,
		power: -241.043255747614,
		road: 21091.2508333333,
		acceleration: -0.191111111111112
	},
	{
		id: 1920,
		time: 1919,
		velocity: 4.85388888888889,
		power: -3616.77249462374,
		road: 21095.9643518518,
		acceleration: -0.947777777777778
	},
	{
		id: 1921,
		time: 1920,
		velocity: 2.65972222222222,
		power: -5124.04507477684,
		road: 21099.3364351852,
		acceleration: -1.73509259259259
	},
	{
		id: 1922,
		time: 1921,
		velocity: 0,
		power: -2393.38250188183,
		road: 21101.0319907407,
		acceleration: -1.61796296296296
	},
	{
		id: 1923,
		time: 1922,
		velocity: 0,
		power: -307.196944451045,
		road: 21101.5277777778,
		acceleration: -0.781574074074074
	},
	{
		id: 1924,
		time: 1923,
		velocity: 0.315,
		power: 392.969517224838,
		road: 21102.0041666666,
		acceleration: 0.742777777777778
	},
	{
		id: 1925,
		time: 1924,
		velocity: 2.22833333333333,
		power: 1513.11850807772,
		road: 21103.3712962963,
		acceleration: 1.0387037037037
	},
	{
		id: 1926,
		time: 1925,
		velocity: 3.11611111111111,
		power: 3190.89897068946,
		road: 21105.86625,
		acceleration: 1.21694444444444
	},
	{
		id: 1927,
		time: 1926,
		velocity: 3.96583333333333,
		power: 3055.46059956929,
		road: 21109.3627777778,
		acceleration: 0.786203703703704
	},
	{
		id: 1928,
		time: 1927,
		velocity: 4.58694444444444,
		power: 4416.82592739601,
		road: 21113.7173148148,
		acceleration: 0.929814814814814
	},
	{
		id: 1929,
		time: 1928,
		velocity: 5.90555555555556,
		power: 4082.51185265388,
		road: 21118.8815277778,
		acceleration: 0.689537037037038
	},
	{
		id: 1930,
		time: 1929,
		velocity: 6.03444444444444,
		power: 2348.49111986432,
		road: 21124.5362962963,
		acceleration: 0.291574074074074
	},
	{
		id: 1931,
		time: 1930,
		velocity: 5.46166666666667,
		power: -1184.00199038094,
		road: 21130.1533796296,
		acceleration: -0.366944444444446
	},
	{
		id: 1932,
		time: 1931,
		velocity: 4.80472222222222,
		power: -1819.72062152237,
		road: 21135.330787037,
		acceleration: -0.512407407407406
	},
	{
		id: 1933,
		time: 1932,
		velocity: 4.49722222222222,
		power: -1202.28990259893,
		road: 21140.0478703703,
		acceleration: -0.408240740740741
	},
	{
		id: 1934,
		time: 1933,
		velocity: 4.23694444444444,
		power: 255.083168754552,
		road: 21144.5215277778,
		acceleration: -0.078611111111111
	},
	{
		id: 1935,
		time: 1934,
		velocity: 4.56888888888889,
		power: 2522.84551037122,
		road: 21149.1716666666,
		acceleration: 0.431574074074073
	},
	{
		id: 1936,
		time: 1935,
		velocity: 5.79194444444444,
		power: 4303.01861387563,
		road: 21154.3993055555,
		acceleration: 0.723425925925927
	},
	{
		id: 1937,
		time: 1936,
		velocity: 6.40722222222222,
		power: 5033.07465623752,
		road: 21160.3591203704,
		acceleration: 0.740925925925925
	},
	{
		id: 1938,
		time: 1937,
		velocity: 6.79166666666667,
		power: 2381.2067559191,
		road: 21166.8081481481,
		acceleration: 0.237500000000001
	},
	{
		id: 1939,
		time: 1938,
		velocity: 6.50444444444444,
		power: -546.859061961923,
		road: 21173.2559259259,
		acceleration: -0.239999999999999
	},
	{
		id: 1940,
		time: 1939,
		velocity: 5.68722222222222,
		power: -3768.97680138711,
		road: 21179.1751851852,
		acceleration: -0.817037037037037
	},
	{
		id: 1941,
		time: 1940,
		velocity: 4.34055555555556,
		power: -3206.17310160692,
		road: 21184.2846296296,
		acceleration: -0.802592592592593
	},
	{
		id: 1942,
		time: 1941,
		velocity: 4.09666666666667,
		power: -2058.15921104936,
		road: 21188.6769444444,
		acceleration: -0.631666666666667
	},
	{
		id: 1943,
		time: 1942,
		velocity: 3.79222222222222,
		power: -568.754950757444,
		road: 21192.6092129629,
		acceleration: -0.288425925925925
	},
	{
		id: 1944,
		time: 1943,
		velocity: 3.47527777777778,
		power: -206.806501906255,
		road: 21196.3002314815,
		acceleration: -0.194074074074074
	},
	{
		id: 1945,
		time: 1944,
		velocity: 3.51444444444444,
		power: 665.84330692751,
		road: 21199.9236111111,
		acceleration: 0.058796296296296
	},
	{
		id: 1946,
		time: 1945,
		velocity: 3.96861111111111,
		power: 1603.69260504464,
		road: 21203.7304629629,
		acceleration: 0.308148148148149
	},
	{
		id: 1947,
		time: 1946,
		velocity: 4.39972222222222,
		power: 2374.0010083497,
		road: 21207.9210185185,
		acceleration: 0.459259259259258
	},
	{
		id: 1948,
		time: 1947,
		velocity: 4.89222222222222,
		power: 2378.49817525174,
		road: 21212.5424074074,
		acceleration: 0.402407407407408
	},
	{
		id: 1949,
		time: 1948,
		velocity: 5.17583333333333,
		power: 1596.20031245572,
		road: 21217.4651388889,
		acceleration: 0.200277777777777
	},
	{
		id: 1950,
		time: 1949,
		velocity: 5.00055555555555,
		power: 366.652281535211,
		road: 21222.4559722222,
		acceleration: -0.0640740740740728
	},
	{
		id: 1951,
		time: 1950,
		velocity: 4.7,
		power: -351.233240401526,
		road: 21227.3063425926,
		acceleration: -0.216851851851851
	},
	{
		id: 1952,
		time: 1951,
		velocity: 4.52527777777778,
		power: 1033.34064992689,
		road: 21232.0918055555,
		acceleration: 0.0870370370370361
	},
	{
		id: 1953,
		time: 1952,
		velocity: 5.26166666666667,
		power: 1725.63780222386,
		road: 21237.0339814815,
		acceleration: 0.226388888888889
	},
	{
		id: 1954,
		time: 1953,
		velocity: 5.37916666666667,
		power: 2404.7339815553,
		road: 21242.2600925926,
		acceleration: 0.341481481481482
	},
	{
		id: 1955,
		time: 1954,
		velocity: 5.54972222222222,
		power: 128.186741202261,
		road: 21247.5978703704,
		acceleration: -0.118148148148149
	},
	{
		id: 1956,
		time: 1955,
		velocity: 4.90722222222222,
		power: -723.047217458807,
		road: 21252.7313425926,
		acceleration: -0.290462962962963
	},
	{
		id: 1957,
		time: 1956,
		velocity: 4.50777777777778,
		power: -1152.07563610874,
		road: 21257.5228703704,
		acceleration: -0.393425925925925
	},
	{
		id: 1958,
		time: 1957,
		velocity: 4.36944444444444,
		power: 150.278864424796,
		road: 21262.0656018518,
		acceleration: -0.104166666666666
	},
	{
		id: 1959,
		time: 1958,
		velocity: 4.59472222222222,
		power: 959.741311888482,
		road: 21266.5982407407,
		acceleration: 0.083981481481481
	},
	{
		id: 1960,
		time: 1959,
		velocity: 4.75972222222222,
		power: 1447.33980477584,
		road: 21271.26625,
		acceleration: 0.186759259259259
	},
	{
		id: 1961,
		time: 1960,
		velocity: 4.92972222222222,
		power: 1209.59534185532,
		road: 21276.0893981481,
		acceleration: 0.123518518518519
	},
	{
		id: 1962,
		time: 1961,
		velocity: 4.96527777777778,
		power: 1449.01904895707,
		road: 21281.0571759259,
		acceleration: 0.16574074074074
	},
	{
		id: 1963,
		time: 1962,
		velocity: 5.25694444444444,
		power: -752.5908178767,
		road: 21285.9565277778,
		acceleration: -0.302592592592592
	},
	{
		id: 1964,
		time: 1963,
		velocity: 4.02194444444444,
		power: -1829.28798667062,
		road: 21290.4194907407,
		acceleration: -0.570185185185186
	},
	{
		id: 1965,
		time: 1964,
		velocity: 3.25472222222222,
		power: -2668.28782115833,
		road: 21294.1533333333,
		acceleration: -0.888055555555556
	},
	{
		id: 1966,
		time: 1965,
		velocity: 2.59277777777778,
		power: -1220.72446966725,
		road: 21297.1631944444,
		acceleration: -0.559907407407407
	},
	{
		id: 1967,
		time: 1966,
		velocity: 2.34222222222222,
		power: -1070.43869794834,
		road: 21299.595787037,
		acceleration: -0.59462962962963
	},
	{
		id: 1968,
		time: 1967,
		velocity: 1.47083333333333,
		power: 599.816077823628,
		road: 21301.8088425926,
		acceleration: 0.155555555555555
	},
	{
		id: 1969,
		time: 1968,
		velocity: 3.05944444444444,
		power: 620.873307257893,
		road: 21304.1728240741,
		acceleration: 0.146296296296296
	},
	{
		id: 1970,
		time: 1969,
		velocity: 2.78111111111111,
		power: 1171.42876919734,
		road: 21306.7810185185,
		acceleration: 0.34212962962963
	},
	{
		id: 1971,
		time: 1970,
		velocity: 2.49722222222222,
		power: 35.8342770670632,
		road: 21309.5014351852,
		acceleration: -0.117685185185185
	},
	{
		id: 1972,
		time: 1971,
		velocity: 2.70638888888889,
		power: -638.588738924926,
		road: 21311.9607407407,
		acceleration: -0.404537037037037
	},
	{
		id: 1973,
		time: 1972,
		velocity: 1.5675,
		power: -657.414984979359,
		road: 21313.9813888889,
		acceleration: -0.472777777777778
	},
	{
		id: 1974,
		time: 1973,
		velocity: 1.07888888888889,
		power: -754.23985366398,
		road: 21315.4259722222,
		acceleration: -0.679351851851852
	},
	{
		id: 1975,
		time: 1974,
		velocity: 0.668333333333333,
		power: -315.526153068283,
		road: 21316.2696296296,
		acceleration: -0.5225
	},
	{
		id: 1976,
		time: 1975,
		velocity: 0,
		power: -83.9346739445976,
		road: 21316.6804166666,
		acceleration: -0.343240740740741
	},
	{
		id: 1977,
		time: 1976,
		velocity: 0.0491666666666667,
		power: -11.5299755204269,
		road: 21316.8081944444,
		acceleration: -0.222777777777778
	},
	{
		id: 1978,
		time: 1977,
		velocity: 0,
		power: 1.98007344233935,
		road: 21316.8245833333,
		acceleration: 0
	},
	{
		id: 1979,
		time: 1978,
		velocity: 0,
		power: 0.862805994152047,
		road: 21316.8327777778,
		acceleration: -0.0163888888888889
	},
	{
		id: 1980,
		time: 1979,
		velocity: 0,
		power: 0,
		road: 21316.8327777778,
		acceleration: 0
	},
	{
		id: 1981,
		time: 1980,
		velocity: 0,
		power: 0,
		road: 21316.8327777778,
		acceleration: 0
	},
	{
		id: 1982,
		time: 1981,
		velocity: 0,
		power: 0.869411276536039,
		road: 21316.8393055555,
		acceleration: 0.0130555555555556
	},
	{
		id: 1983,
		time: 1982,
		velocity: 0.0391666666666667,
		power: 1.57734594488494,
		road: 21316.8523611111,
		acceleration: 0
	},
	{
		id: 1984,
		time: 1983,
		velocity: 0,
		power: 1.57734594488494,
		road: 21316.8654166666,
		acceleration: 0
	},
	{
		id: 1985,
		time: 1984,
		velocity: 0,
		power: 0.707934064327485,
		road: 21316.8719444444,
		acceleration: -0.0130555555555556
	},
	{
		id: 1986,
		time: 1985,
		velocity: 0,
		power: 0,
		road: 21316.8719444444,
		acceleration: 0
	}
];
export default train5;
