export const train3 = 
[
	{
		id: 1,
		time: 0,
		velocity: 0.184166666666667,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 15.9396090144592,
		road: 0.169907407407407,
		acceleration: -0.0285185185185185
	},
	{
		id: 3,
		time: 2,
		velocity: 0.282777777777778,
		power: 31.699048308175,
		road: 0.353101851851852,
		acceleration: 0.0550925925925926
	},
	{
		id: 4,
		time: 3,
		velocity: 0.349444444444444,
		power: 75.6703545589696,
		road: 0.639490740740741,
		acceleration: 0.151296296296296
	},
	{
		id: 5,
		time: 4,
		velocity: 0.453888888888889,
		power: 9.93802923008635,
		road: 0.954398148148148,
		acceleration: -0.0942592592592593
	},
	{
		id: 6,
		time: 5,
		velocity: 0,
		power: 2.19583167892404,
		road: 1.16393518518519,
		acceleration: -0.116481481481481
	},
	{
		id: 7,
		time: 6,
		velocity: 0,
		power: -1.7032512345679,
		road: 1.23958333333333,
		acceleration: -0.151296296296296
	},
	{
		id: 8,
		time: 7,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 9,
		time: 8,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 10,
		time: 9,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 11,
		time: 10,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 12,
		time: 11,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 13,
		time: 12,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 14,
		time: 13,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 15,
		time: 14,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 16,
		time: 15,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 17,
		time: 16,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 18,
		time: 17,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 19,
		time: 18,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 20,
		time: 19,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 21,
		time: 20,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 22,
		time: 21,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 23,
		time: 22,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 24,
		time: 23,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 25,
		time: 24,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 26,
		time: 25,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 27,
		time: 26,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 28,
		time: 27,
		velocity: 0,
		power: 0,
		road: 1.23958333333333,
		acceleration: 0
	},
	{
		id: 29,
		time: 28,
		velocity: 0,
		power: 33.4339058525169,
		road: 1.34430555555556,
		acceleration: 0.209444444444444
	},
	{
		id: 30,
		time: 29,
		velocity: 0.628333333333333,
		power: 187.915113378365,
		road: 1.74032407407407,
		acceleration: 0.373148148148148
	},
	{
		id: 31,
		time: 30,
		velocity: 1.11944444444444,
		power: 546.968938659047,
		road: 2.59611111111111,
		acceleration: 0.546388888888889
	},
	{
		id: 32,
		time: 31,
		velocity: 1.63916666666667,
		power: 708.367826946129,
		road: 3.93902777777778,
		acceleration: 0.42787037037037
	},
	{
		id: 33,
		time: 32,
		velocity: 1.91194444444444,
		power: 671.458244908259,
		road: 5.63953703703704,
		acceleration: 0.287314814814815
	},
	{
		id: 34,
		time: 33,
		velocity: 1.98138888888889,
		power: 360.441392814814,
		road: 7.5200462962963,
		acceleration: 0.0726851851851849
	},
	{
		id: 35,
		time: 34,
		velocity: 1.85722222222222,
		power: -205.80417967644,
		road: 9.31171296296296,
		acceleration: -0.25037037037037
	},
	{
		id: 36,
		time: 35,
		velocity: 1.16083333333333,
		power: -444.042184812785,
		road: 10.7512037037037,
		acceleration: -0.453981481481481
	},
	{
		id: 37,
		time: 36,
		velocity: 0.619444444444444,
		power: -246.065998816717,
		road: 11.7725925925926,
		acceleration: -0.382222222222222
	},
	{
		id: 38,
		time: 37,
		velocity: 0.710555555555556,
		power: -7.12725809207934,
		road: 12.5340277777778,
		acceleration: -0.137685185185185
	},
	{
		id: 39,
		time: 38,
		velocity: 0.747777777777778,
		power: -44.0056403079186,
		road: 13.1233796296296,
		acceleration: -0.206481481481481
	},
	{
		id: 40,
		time: 39,
		velocity: 0,
		power: 23.4528021704088,
		road: 13.5731944444444,
		acceleration: -0.0725925925925927
	},
	{
		id: 41,
		time: 40,
		velocity: 0.492777777777778,
		power: -33.3111447916245,
		road: 13.8620833333333,
		acceleration: -0.249259259259259
	},
	{
		id: 42,
		time: 41,
		velocity: 0,
		power: 99.5761858558831,
		road: 14.1469907407407,
		acceleration: 0.241296296296296
	},
	{
		id: 43,
		time: 42,
		velocity: 0.723888888888889,
		power: 214.265582977832,
		road: 14.695,
		acceleration: 0.284907407407407
	},
	{
		id: 44,
		time: 43,
		velocity: 1.3475,
		power: 708.240084551742,
		road: 15.695,
		acceleration: 0.619074074074074
	},
	{
		id: 45,
		time: 44,
		velocity: 1.85722222222222,
		power: 669.27855448765,
		road: 17.1781481481482,
		acceleration: 0.347222222222222
	},
	{
		id: 46,
		time: 45,
		velocity: 1.76555555555556,
		power: -54.5279677303772,
		road: 18.7522222222222,
		acceleration: -0.16537037037037
	},
	{
		id: 47,
		time: 46,
		velocity: 0.851388888888889,
		power: -549.868682336838,
		road: 19.9340740740741,
		acceleration: -0.619074074074074
	},
	{
		id: 48,
		time: 47,
		velocity: 0,
		power: -59.3207046792123,
		road: 20.701712962963,
		acceleration: -0.209351851851852
	},
	{
		id: 49,
		time: 48,
		velocity: 1.1375,
		power: 631.547437686657,
		road: 21.6514351851852,
		acceleration: 0.573518518518519
	},
	{
		id: 50,
		time: 49,
		velocity: 2.57194444444444,
		power: 1776.75010035873,
		road: 23.3687962962963,
		acceleration: 0.961759259259259
	},
	{
		id: 51,
		time: 50,
		velocity: 2.88527777777778,
		power: 2481.28390872809,
		road: 25.9985648148148,
		acceleration: 0.863055555555555
	},
	{
		id: 52,
		time: 51,
		velocity: 3.72666666666667,
		power: 1545.32946616356,
		road: 29.2440277777778,
		acceleration: 0.368333333333334
	},
	{
		id: 53,
		time: 52,
		velocity: 3.67694444444444,
		power: 1893.06901210436,
		road: 32.8804166666667,
		acceleration: 0.413518518518518
	},
	{
		id: 54,
		time: 53,
		velocity: 4.12583333333333,
		power: 1030.84416822583,
		road: 36.7942592592593,
		acceleration: 0.14138888888889
	},
	{
		id: 55,
		time: 54,
		velocity: 4.15083333333333,
		power: 172.856279504937,
		road: 40.7338425925926,
		acceleration: -0.0899074074074084
	},
	{
		id: 56,
		time: 55,
		velocity: 3.40722222222222,
		power: -508.893906367721,
		road: 44.4894444444444,
		acceleration: -0.278055555555556
	},
	{
		id: 57,
		time: 56,
		velocity: 3.29166666666667,
		power: -1138.75667944457,
		road: 47.8612037037037,
		acceleration: -0.48962962962963
	},
	{
		id: 58,
		time: 57,
		velocity: 2.68194444444444,
		power: -369.962688872997,
		road: 50.8568518518519,
		acceleration: -0.262592592592592
	},
	{
		id: 59,
		time: 58,
		velocity: 2.61944444444444,
		power: -399.478300718085,
		road: 53.5780555555556,
		acceleration: -0.286296296296297
	},
	{
		id: 60,
		time: 59,
		velocity: 2.43277777777778,
		power: -292.322551671693,
		road: 56.0278240740741,
		acceleration: -0.256574074074074
	},
	{
		id: 61,
		time: 60,
		velocity: 1.91222222222222,
		power: 405.240178113375,
		road: 58.3750462962963,
		acceleration: 0.0514814814814821
	},
	{
		id: 62,
		time: 61,
		velocity: 2.77388888888889,
		power: 1954.36289506409,
		road: 61.0650925925926,
		acceleration: 0.634166666666666
	},
	{
		id: 63,
		time: 62,
		velocity: 4.33527777777778,
		power: 3613.31654800382,
		road: 64.5510185185185,
		acceleration: 0.957592592592592
	},
	{
		id: 64,
		time: 63,
		velocity: 4.785,
		power: 3026.08774861081,
		road: 68.8200925925926,
		acceleration: 0.608703703703704
	},
	{
		id: 65,
		time: 64,
		velocity: 4.6,
		power: 1321.21413863577,
		road: 73.4731944444444,
		acceleration: 0.159351851851852
	},
	{
		id: 66,
		time: 65,
		velocity: 4.81333333333333,
		power: 729.96943855477,
		road: 78.2169444444444,
		acceleration: 0.0219444444444443
	},
	{
		id: 67,
		time: 66,
		velocity: 4.85083333333333,
		power: 2961.21036671845,
		road: 83.2128703703704,
		acceleration: 0.482407407407407
	},
	{
		id: 68,
		time: 67,
		velocity: 6.04722222222222,
		power: 3151.60132457511,
		road: 88.6811111111111,
		acceleration: 0.462222222222223
	},
	{
		id: 69,
		time: 68,
		velocity: 6.2,
		power: 4173.13337035312,
		road: 94.672962962963,
		acceleration: 0.585
	},
	{
		id: 70,
		time: 69,
		velocity: 6.60583333333333,
		power: 2800.51437006372,
		road: 101.110694444444,
		acceleration: 0.30675925925926
	},
	{
		id: 71,
		time: 70,
		velocity: 6.9675,
		power: 2787.85876686664,
		road: 107.843055555556,
		acceleration: 0.282499999999999
	},
	{
		id: 72,
		time: 71,
		velocity: 7.0475,
		power: 2219.41476832097,
		road: 114.806851851852,
		acceleration: 0.180370370370372
	},
	{
		id: 73,
		time: 72,
		velocity: 7.14694444444444,
		power: 1524.44524917298,
		road: 121.896018518519,
		acceleration: 0.0703703703703695
	},
	{
		id: 74,
		time: 73,
		velocity: 7.17861111111111,
		power: 1336.17464390202,
		road: 129.040601851852,
		acceleration: 0.0404629629629634
	},
	{
		id: 75,
		time: 74,
		velocity: 7.16888888888889,
		power: 567.565459574057,
		road: 136.169259259259,
		acceleration: -0.0723148148148143
	},
	{
		id: 76,
		time: 75,
		velocity: 6.93,
		power: -58.7793695050596,
		road: 143.179814814815,
		acceleration: -0.16388888888889
	},
	{
		id: 77,
		time: 76,
		velocity: 6.68694444444444,
		power: 85.1034664857458,
		road: 150.038009259259,
		acceleration: -0.140833333333333
	},
	{
		id: 78,
		time: 77,
		velocity: 6.74638888888889,
		power: -58.2278899315739,
		road: 156.744861111111,
		acceleration: -0.161851851851852
	},
	{
		id: 79,
		time: 78,
		velocity: 6.44444444444444,
		power: -1417.01671893638,
		road: 163.17962962963,
		acceleration: -0.382314814814815
	},
	{
		id: 80,
		time: 79,
		velocity: 5.54,
		power: -4117.02313066494,
		road: 168.976435185185,
		acceleration: -0.893611111111112
	},
	{
		id: 81,
		time: 80,
		velocity: 4.06555555555556,
		power: -5211.74174362835,
		road: 173.672222222222,
		acceleration: -1.30842592592592
	},
	{
		id: 82,
		time: 81,
		velocity: 2.51916666666667,
		power: -1685.55561886769,
		road: 177.408564814815,
		acceleration: -0.610462962962963
	},
	{
		id: 83,
		time: 82,
		velocity: 3.70861111111111,
		power: 1233.15668090406,
		road: 180.955555555556,
		acceleration: 0.23175925925926
	},
	{
		id: 84,
		time: 83,
		velocity: 4.76083333333333,
		power: 3931.29621530908,
		road: 185.054907407407,
		acceleration: 0.872962962962963
	},
	{
		id: 85,
		time: 84,
		velocity: 5.13805555555556,
		power: 2708.33569193671,
		road: 189.819814814815,
		acceleration: 0.458148148148148
	},
	{
		id: 86,
		time: 85,
		velocity: 5.08305555555556,
		power: 1872.59191713994,
		road: 194.93537037037,
		acceleration: 0.243148148148149
	},
	{
		id: 87,
		time: 86,
		velocity: 5.49027777777778,
		power: 2871.61824888143,
		road: 200.378055555556,
		acceleration: 0.411111111111111
	},
	{
		id: 88,
		time: 87,
		velocity: 6.37138888888889,
		power: 3914.96570310656,
		road: 206.300416666667,
		acceleration: 0.548240740740739
	},
	{
		id: 89,
		time: 88,
		velocity: 6.72777777777778,
		power: 6377.93038507663,
		road: 212.926851851852,
		acceleration: 0.859907407407409
	},
	{
		id: 90,
		time: 89,
		velocity: 8.07,
		power: 5598.75679989408,
		road: 220.303101851852,
		acceleration: 0.639722222222222
	},
	{
		id: 91,
		time: 90,
		velocity: 8.29055555555555,
		power: 6532.60481938739,
		road: 228.344074074074,
		acceleration: 0.689722222222221
	},
	{
		id: 92,
		time: 91,
		velocity: 8.79694444444444,
		power: 3820.05785280611,
		road: 236.880694444444,
		acceleration: 0.301574074074074
	},
	{
		id: 93,
		time: 92,
		velocity: 8.97472222222222,
		power: 3281.74925308095,
		road: 245.678472222222,
		acceleration: 0.220740740740743
	},
	{
		id: 94,
		time: 93,
		velocity: 8.95277777777778,
		power: 2311.09166277322,
		road: 254.635787037037,
		acceleration: 0.0983333333333327
	},
	{
		id: 95,
		time: 94,
		velocity: 9.09194444444445,
		power: 2050.05223777988,
		road: 263.67462962963,
		acceleration: 0.0647222222222226
	},
	{
		id: 96,
		time: 95,
		velocity: 9.16888888888889,
		power: 1762.15183356858,
		road: 272.760694444444,
		acceleration: 0.0297222222222224
	},
	{
		id: 97,
		time: 96,
		velocity: 9.04194444444444,
		power: 1624.48934856609,
		road: 281.868194444444,
		acceleration: 0.0131481481481472
	},
	{
		id: 98,
		time: 97,
		velocity: 9.13138888888889,
		power: 547.547049767468,
		road: 290.927175925926,
		acceleration: -0.110185185185186
	},
	{
		id: 99,
		time: 98,
		velocity: 8.83833333333333,
		power: -1570.72055789812,
		road: 299.751944444444,
		acceleration: -0.35824074074074
	},
	{
		id: 100,
		time: 99,
		velocity: 7.96722222222222,
		power: -3778.18832427847,
		road: 308.075925925926,
		acceleration: -0.643333333333333
	},
	{
		id: 101,
		time: 100,
		velocity: 7.20138888888889,
		power: -4462.69937591834,
		road: 315.690185185185,
		acceleration: -0.776111111111112
	},
	{
		id: 102,
		time: 101,
		velocity: 6.51,
		power: -2868.68244356555,
		road: 322.621574074074,
		acceleration: -0.58962962962963
	},
	{
		id: 103,
		time: 102,
		velocity: 6.19833333333333,
		power: -1238.39076803776,
		road: 329.081898148148,
		acceleration: -0.352499999999999
	},
	{
		id: 104,
		time: 103,
		velocity: 6.14388888888889,
		power: -483.163570200621,
		road: 335.25037037037,
		acceleration: -0.231203703703704
	},
	{
		id: 105,
		time: 104,
		velocity: 5.81638888888889,
		power: 191.233407966863,
		road: 341.246203703704,
		acceleration: -0.114074074074074
	},
	{
		id: 106,
		time: 105,
		velocity: 5.85611111111111,
		power: 287.118631833501,
		road: 347.137175925926,
		acceleration: -0.0956481481481477
	},
	{
		id: 107,
		time: 106,
		velocity: 5.85694444444444,
		power: 1010.22748855254,
		road: 352.997638888889,
		acceleration: 0.0346296296296291
	},
	{
		id: 108,
		time: 107,
		velocity: 5.92027777777778,
		power: 378.208988438605,
		road: 358.836203703704,
		acceleration: -0.0784259259259255
	},
	{
		id: 109,
		time: 108,
		velocity: 5.62083333333333,
		power: -618.539801032334,
		road: 364.505416666667,
		acceleration: -0.260277777777779
	},
	{
		id: 110,
		time: 109,
		velocity: 5.07611111111111,
		power: -1231.63840552131,
		road: 369.851527777778,
		acceleration: -0.385925925925926
	},
	{
		id: 111,
		time: 110,
		velocity: 4.7625,
		power: -2415.13315300722,
		road: 374.670648148148,
		acceleration: -0.668055555555555
	},
	{
		id: 112,
		time: 111,
		velocity: 3.61666666666667,
		power: -3104.68585633766,
		road: 378.679675925926,
		acceleration: -0.95212962962963
	},
	{
		id: 113,
		time: 112,
		velocity: 2.21972222222222,
		power: -3282.07806360881,
		road: 381.542407407407,
		acceleration: -1.34046296296296
	},
	{
		id: 114,
		time: 113,
		velocity: 0.741111111111111,
		power: -1622.72259865303,
		road: 383.13212962963,
		acceleration: -1.20555555555556
	},
	{
		id: 115,
		time: 114,
		velocity: 0,
		power: -357.924897238293,
		road: 383.74912037037,
		acceleration: -0.739907407407407
	},
	{
		id: 116,
		time: 115,
		velocity: 0,
		power: -13.9844199480182,
		road: 383.872638888889,
		acceleration: -0.247037037037037
	},
	{
		id: 117,
		time: 116,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 118,
		time: 117,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 119,
		time: 118,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 120,
		time: 119,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 121,
		time: 120,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 122,
		time: 121,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 123,
		time: 122,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 124,
		time: 123,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 125,
		time: 124,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 126,
		time: 125,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 127,
		time: 126,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 128,
		time: 127,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 129,
		time: 128,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 130,
		time: 129,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 131,
		time: 130,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 132,
		time: 131,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 133,
		time: 132,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 134,
		time: 133,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 135,
		time: 134,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 136,
		time: 135,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 137,
		time: 136,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 138,
		time: 137,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 139,
		time: 138,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 140,
		time: 139,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 141,
		time: 140,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 142,
		time: 141,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 143,
		time: 142,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 144,
		time: 143,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 145,
		time: 144,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 146,
		time: 145,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 147,
		time: 146,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 148,
		time: 147,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 149,
		time: 148,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 150,
		time: 149,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 151,
		time: 150,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 152,
		time: 151,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 153,
		time: 152,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 154,
		time: 153,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 155,
		time: 154,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 156,
		time: 155,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 157,
		time: 156,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 158,
		time: 157,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 159,
		time: 158,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 160,
		time: 159,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 161,
		time: 160,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 162,
		time: 161,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 163,
		time: 162,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 164,
		time: 163,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 165,
		time: 164,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 166,
		time: 165,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 167,
		time: 166,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 168,
		time: 167,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 169,
		time: 168,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 170,
		time: 169,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 171,
		time: 170,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 172,
		time: 171,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 173,
		time: 172,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 174,
		time: 173,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 175,
		time: 174,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 176,
		time: 175,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 177,
		time: 176,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 178,
		time: 177,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 179,
		time: 178,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 180,
		time: 179,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 181,
		time: 180,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 182,
		time: 181,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 183,
		time: 182,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 184,
		time: 183,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 185,
		time: 184,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 186,
		time: 185,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 187,
		time: 186,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 188,
		time: 187,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 189,
		time: 188,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 190,
		time: 189,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 191,
		time: 190,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 192,
		time: 191,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 193,
		time: 192,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 194,
		time: 193,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 195,
		time: 194,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 196,
		time: 195,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 197,
		time: 196,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 198,
		time: 197,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 199,
		time: 198,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 200,
		time: 199,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 201,
		time: 200,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 202,
		time: 201,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 203,
		time: 202,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 204,
		time: 203,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 205,
		time: 204,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 206,
		time: 205,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 207,
		time: 206,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 208,
		time: 207,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 209,
		time: 208,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 210,
		time: 209,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 211,
		time: 210,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 212,
		time: 211,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 213,
		time: 212,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 214,
		time: 213,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 215,
		time: 214,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 216,
		time: 215,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 217,
		time: 216,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 218,
		time: 217,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 219,
		time: 218,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 220,
		time: 219,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 221,
		time: 220,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 222,
		time: 221,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 223,
		time: 222,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 224,
		time: 223,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 225,
		time: 224,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 226,
		time: 225,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 227,
		time: 226,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 228,
		time: 227,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 229,
		time: 228,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 230,
		time: 229,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 231,
		time: 230,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 232,
		time: 231,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 233,
		time: 232,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 234,
		time: 233,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 235,
		time: 234,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 236,
		time: 235,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 237,
		time: 236,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 238,
		time: 237,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 239,
		time: 238,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 240,
		time: 239,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 241,
		time: 240,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 242,
		time: 241,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 243,
		time: 242,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 244,
		time: 243,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 245,
		time: 244,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 246,
		time: 245,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 247,
		time: 246,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 248,
		time: 247,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 249,
		time: 248,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 250,
		time: 249,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 251,
		time: 250,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 252,
		time: 251,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 253,
		time: 252,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 254,
		time: 253,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 255,
		time: 254,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 256,
		time: 255,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 257,
		time: 256,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 258,
		time: 257,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 259,
		time: 258,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 260,
		time: 259,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 261,
		time: 260,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 262,
		time: 261,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 263,
		time: 262,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 264,
		time: 263,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 265,
		time: 264,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 266,
		time: 265,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 267,
		time: 266,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 268,
		time: 267,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 269,
		time: 268,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 270,
		time: 269,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 271,
		time: 270,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 272,
		time: 271,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 273,
		time: 272,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 274,
		time: 273,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 275,
		time: 274,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 276,
		time: 275,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 277,
		time: 276,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 278,
		time: 277,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 279,
		time: 278,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 280,
		time: 279,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 281,
		time: 280,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 282,
		time: 281,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 283,
		time: 282,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 284,
		time: 283,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 285,
		time: 284,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 286,
		time: 285,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 287,
		time: 286,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 288,
		time: 287,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 289,
		time: 288,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 290,
		time: 289,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 291,
		time: 290,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 292,
		time: 291,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 293,
		time: 292,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 294,
		time: 293,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 295,
		time: 294,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 296,
		time: 295,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 297,
		time: 296,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 298,
		time: 297,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 299,
		time: 298,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 300,
		time: 299,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 301,
		time: 300,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 302,
		time: 301,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 303,
		time: 302,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 304,
		time: 303,
		velocity: 0,
		power: 0,
		road: 383.872638888889,
		acceleration: 0
	},
	{
		id: 305,
		time: 304,
		velocity: 0,
		power: 12.514444553613,
		road: 383.928055555555,
		acceleration: 0.110833333333333
	},
	{
		id: 306,
		time: 305,
		velocity: 0.3325,
		power: 187.217302169061,
		road: 384.266759259259,
		acceleration: 0.455740740740741
	},
	{
		id: 307,
		time: 306,
		velocity: 1.36722222222222,
		power: 1441.58571634729,
		road: 385.42537037037,
		acceleration: 1.18407407407407
	},
	{
		id: 308,
		time: 307,
		velocity: 3.55222222222222,
		power: 4129.39031724428,
		road: 387.966666666667,
		acceleration: 1.5812962962963
	},
	{
		id: 309,
		time: 308,
		velocity: 5.07638888888889,
		power: 5425.3342025063,
		road: 391.947916666667,
		acceleration: 1.29861111111111
	},
	{
		id: 310,
		time: 309,
		velocity: 5.26305555555555,
		power: 2856.3372462003,
		road: 396.816851851852,
		acceleration: 0.476759259259259
	},
	{
		id: 311,
		time: 310,
		velocity: 4.9825,
		power: -28.0090755200738,
		road: 401.850416666667,
		acceleration: -0.1475
	},
	{
		id: 312,
		time: 311,
		velocity: 4.63388888888889,
		power: -528.014148661072,
		road: 406.682453703704,
		acceleration: -0.255555555555556
	},
	{
		id: 313,
		time: 312,
		velocity: 4.49638888888889,
		power: 1241.8327111443,
		road: 411.453611111111,
		acceleration: 0.133796296296296
	},
	{
		id: 314,
		time: 313,
		velocity: 5.38388888888889,
		power: 3581.08344355189,
		road: 416.587592592593,
		acceleration: 0.591851851851853
	},
	{
		id: 315,
		time: 314,
		velocity: 6.40944444444444,
		power: 5329.24611720436,
		road: 422.424444444444,
		acceleration: 0.813888888888888
	},
	{
		id: 316,
		time: 315,
		velocity: 6.93805555555556,
		power: 5668.25194905451,
		road: 429.0425,
		acceleration: 0.748518518518518
	},
	{
		id: 317,
		time: 316,
		velocity: 7.62944444444444,
		power: 6892.8129686575,
		road: 436.445,
		acceleration: 0.820370370370371
	},
	{
		id: 318,
		time: 317,
		velocity: 8.87055555555555,
		power: 10498.0897909342,
		road: 444.831574074074,
		acceleration: 1.14777777777778
	},
	{
		id: 319,
		time: 318,
		velocity: 10.3813888888889,
		power: 10942.7253724292,
		road: 454.309027777778,
		acceleration: 1.03398148148148
	},
	{
		id: 320,
		time: 319,
		velocity: 10.7313888888889,
		power: 9641.49732567911,
		road: 464.696296296296,
		acceleration: 0.785648148148146
	},
	{
		id: 321,
		time: 320,
		velocity: 11.2275,
		power: 8289.43175057237,
		road: 475.770648148148,
		acceleration: 0.588518518518519
	},
	{
		id: 322,
		time: 321,
		velocity: 12.1469444444444,
		power: 10614.5269446561,
		road: 487.510648148148,
		acceleration: 0.742777777777778
	},
	{
		id: 323,
		time: 322,
		velocity: 12.9597222222222,
		power: 10445.8230439846,
		road: 499.954537037037,
		acceleration: 0.665000000000001
	},
	{
		id: 324,
		time: 323,
		velocity: 13.2225,
		power: 8089.5446878486,
		road: 512.945833333333,
		acceleration: 0.429814814814813
	},
	{
		id: 325,
		time: 324,
		velocity: 13.4363888888889,
		power: 4857.29581225682,
		road: 526.230092592592,
		acceleration: 0.156111111111112
	},
	{
		id: 326,
		time: 325,
		velocity: 13.4280555555556,
		power: 2876.01893602869,
		road: 539.591111111111,
		acceleration: -0.00259259259259181
	},
	{
		id: 327,
		time: 326,
		velocity: 13.2147222222222,
		power: 264.1574629219,
		road: 552.848009259259,
		acceleration: -0.205648148148148
	},
	{
		id: 328,
		time: 327,
		velocity: 12.8194444444444,
		power: -2218.21557566656,
		road: 565.801342592593,
		acceleration: -0.401481481481483
	},
	{
		id: 329,
		time: 328,
		velocity: 12.2236111111111,
		power: -1395.60392050927,
		road: 578.387453703704,
		acceleration: -0.332962962962961
	},
	{
		id: 330,
		time: 329,
		velocity: 12.2158333333333,
		power: 587.648851666525,
		road: 590.725416666667,
		acceleration: -0.163333333333334
	},
	{
		id: 331,
		time: 330,
		velocity: 12.3294444444444,
		power: 3244.2766433226,
		road: 603.013796296296,
		acceleration: 0.0641666666666669
	},
	{
		id: 332,
		time: 331,
		velocity: 12.4161111111111,
		power: 1991.78091887625,
		road: 615.312777777778,
		acceleration: -0.0429629629629655
	},
	{
		id: 333,
		time: 332,
		velocity: 12.0869444444444,
		power: -2551.17933207223,
		road: 627.374675925926,
		acceleration: -0.431203703703702
	},
	{
		id: 334,
		time: 333,
		velocity: 11.0358333333333,
		power: -4601.0288888753,
		road: 638.910601851852,
		acceleration: -0.620740740740743
	},
	{
		id: 335,
		time: 334,
		velocity: 10.5538888888889,
		power: -5538.96618280362,
		road: 649.771666666667,
		acceleration: -0.72898148148148
	},
	{
		id: 336,
		time: 335,
		velocity: 9.9,
		power: -3377.10166138614,
		road: 660.001805555555,
		acceleration: -0.53287037037037
	},
	{
		id: 337,
		time: 336,
		velocity: 9.43722222222222,
		power: -5415.2953851156,
		road: 669.57912037037,
		acceleration: -0.77277777777778
	},
	{
		id: 338,
		time: 337,
		velocity: 8.23555555555556,
		power: -7998.82002344175,
		road: 678.197916666667,
		acceleration: -1.14425925925926
	},
	{
		id: 339,
		time: 338,
		velocity: 6.46722222222222,
		power: -9122.09982947697,
		road: 685.51,
		acceleration: -1.46916666666667
	},
	{
		id: 340,
		time: 339,
		velocity: 5.02972222222222,
		power: -5440.1770385668,
		road: 691.538888888889,
		acceleration: -1.09722222222222
	},
	{
		id: 341,
		time: 340,
		velocity: 4.94388888888889,
		power: -2021.26270081294,
		road: 696.743472222222,
		acceleration: -0.551388888888888
	},
	{
		id: 342,
		time: 341,
		velocity: 4.81305555555556,
		power: -371.30711372592,
		road: 701.561574074074,
		acceleration: -0.221574074074073
	},
	{
		id: 343,
		time: 342,
		velocity: 4.365,
		power: 869.810861958968,
		road: 706.295601851852,
		acceleration: 0.053425925925926
	},
	{
		id: 344,
		time: 343,
		velocity: 5.10416666666667,
		power: 4032.37229609463,
		road: 711.400925925926,
		acceleration: 0.689166666666667
	},
	{
		id: 345,
		time: 344,
		velocity: 6.88055555555556,
		power: 5545.83499979979,
		road: 717.274074074074,
		acceleration: 0.846481481481481
	},
	{
		id: 346,
		time: 345,
		velocity: 6.90444444444444,
		power: 1585.0740190605,
		road: 723.62662037037,
		acceleration: 0.112314814814815
	},
	{
		id: 347,
		time: 346,
		velocity: 5.44111111111111,
		power: -5031.93091174294,
		road: 729.512083333333,
		acceleration: -1.04648148148148
	},
	{
		id: 348,
		time: 347,
		velocity: 3.74111111111111,
		power: -5709.03751148859,
		road: 734.1575,
		acceleration: -1.43361111111111
	},
	{
		id: 349,
		time: 348,
		velocity: 2.60361111111111,
		power: -2859.25874801854,
		road: 737.578842592592,
		acceleration: -1.01453703703704
	},
	{
		id: 350,
		time: 349,
		velocity: 2.3975,
		power: -9.96217782605522,
		road: 740.425092592592,
		acceleration: -0.135648148148148
	},
	{
		id: 351,
		time: 350,
		velocity: 3.33416666666667,
		power: 3339.35883228764,
		road: 743.677638888889,
		acceleration: 0.948240740740741
	},
	{
		id: 352,
		time: 351,
		velocity: 5.44833333333333,
		power: 6374.45821023242,
		road: 748.094722222222,
		acceleration: 1.38083333333333
	},
	{
		id: 353,
		time: 352,
		velocity: 6.54,
		power: 6552.55869859639,
		road: 753.740092592593,
		acceleration: 1.07574074074074
	},
	{
		id: 354,
		time: 353,
		velocity: 6.56138888888889,
		power: 7353.11816384535,
		road: 760.425277777778,
		acceleration: 1.00388888888889
	},
	{
		id: 355,
		time: 354,
		velocity: 8.46,
		power: 7258.2217653048,
		road: 768.033657407407,
		acceleration: 0.8425
	},
	{
		id: 356,
		time: 355,
		velocity: 9.0675,
		power: 10085.3100638803,
		road: 776.597361111111,
		acceleration: 1.06814814814815
	},
	{
		id: 357,
		time: 356,
		velocity: 9.76583333333333,
		power: 6687.63579977493,
		road: 785.980740740741,
		acceleration: 0.571203703703704
	},
	{
		id: 358,
		time: 357,
		velocity: 10.1736111111111,
		power: 9666.37968525921,
		road: 796.060555555556,
		acceleration: 0.821666666666667
	},
	{
		id: 359,
		time: 358,
		velocity: 11.5325,
		power: 9056.46929449248,
		road: 806.892916666667,
		acceleration: 0.683425925925928
	},
	{
		id: 360,
		time: 359,
		velocity: 11.8161111111111,
		power: 7805.43064147177,
		road: 818.324444444444,
		acceleration: 0.514907407407406
	},
	{
		id: 361,
		time: 360,
		velocity: 11.7183333333333,
		power: 4279.33736204787,
		road: 830.101111111111,
		acceleration: 0.17537037037037
	},
	{
		id: 362,
		time: 361,
		velocity: 12.0586111111111,
		power: 4650.21530251793,
		road: 842.065138888889,
		acceleration: 0.199351851851851
	},
	{
		id: 363,
		time: 362,
		velocity: 12.4141666666667,
		power: 2381.39710646382,
		road: 854.127592592593,
		acceleration: -0.0024999999999995
	},
	{
		id: 364,
		time: 363,
		velocity: 11.7108333333333,
		power: 1423.29504880723,
		road: 866.146435185185,
		acceleration: -0.0847222222222239
	},
	{
		id: 365,
		time: 364,
		velocity: 11.8044444444444,
		power: -1629.61065462013,
		road: 877.947546296296,
		acceleration: -0.35074074074074
	},
	{
		id: 366,
		time: 365,
		velocity: 11.3619444444444,
		power: -1270.73207862843,
		road: 889.414398148148,
		acceleration: -0.317777777777779
	},
	{
		id: 367,
		time: 366,
		velocity: 10.7575,
		power: -2933.10112798872,
		road: 900.485138888889,
		acceleration: -0.474444444444444
	},
	{
		id: 368,
		time: 367,
		velocity: 10.3811111111111,
		power: -2124.83847249641,
		road: 911.118240740741,
		acceleration: -0.400833333333331
	},
	{
		id: 369,
		time: 368,
		velocity: 10.1594444444444,
		power: 268.185755185771,
		road: 921.470601851852,
		acceleration: -0.16064814814815
	},
	{
		id: 370,
		time: 369,
		velocity: 10.2755555555556,
		power: 2466.15296727153,
		road: 931.774583333333,
		acceleration: 0.06388888888889
	},
	{
		id: 371,
		time: 370,
		velocity: 10.5727777777778,
		power: 4191.22514703987,
		road: 942.226388888889,
		acceleration: 0.231759259259258
	},
	{
		id: 372,
		time: 371,
		velocity: 10.8547222222222,
		power: 3664.63960049992,
		road: 952.878842592593,
		acceleration: 0.169537037037037
	},
	{
		id: 373,
		time: 372,
		velocity: 10.7841666666667,
		power: 2553.71829295733,
		road: 963.64412037037,
		acceleration: 0.056111111111111
	},
	{
		id: 374,
		time: 373,
		velocity: 10.7411111111111,
		power: 1594.69997558794,
		road: 974.418657407407,
		acceleration: -0.0375925925925902
	},
	{
		id: 375,
		time: 374,
		velocity: 10.7419444444444,
		power: 1238.3269326119,
		road: 985.138888888889,
		acceleration: -0.0710185185185193
	},
	{
		id: 376,
		time: 375,
		velocity: 10.5711111111111,
		power: 2306.87260525005,
		road: 995.840694444444,
		acceleration: 0.0341666666666676
	},
	{
		id: 377,
		time: 376,
		velocity: 10.8436111111111,
		power: 3077.31186367061,
		road: 1006.61300925926,
		acceleration: 0.10685185185185
	},
	{
		id: 378,
		time: 377,
		velocity: 11.0625,
		power: 4311.93215200599,
		road: 1017.54814814815,
		acceleration: 0.218796296296297
	},
	{
		id: 379,
		time: 378,
		velocity: 11.2275,
		power: 5068.3961788156,
		road: 1028.73138888889,
		acceleration: 0.277407407407409
	},
	{
		id: 380,
		time: 379,
		velocity: 11.6758333333333,
		power: 4407.58776934745,
		road: 1040.15513888889,
		acceleration: 0.20361111111111
	},
	{
		id: 381,
		time: 380,
		velocity: 11.6733333333333,
		power: 3395.55739840288,
		road: 1051.73296296296,
		acceleration: 0.104537037037039
	},
	{
		id: 382,
		time: 381,
		velocity: 11.5411111111111,
		power: 760.995782555414,
		road: 1063.29615740741,
		acceleration: -0.1337962962963
	},
	{
		id: 383,
		time: 382,
		velocity: 11.2744444444444,
		power: -1125.90807921689,
		road: 1074.64041666667,
		acceleration: -0.304074074074073
	},
	{
		id: 384,
		time: 383,
		velocity: 10.7611111111111,
		power: -1564.49671035961,
		road: 1085.66023148148,
		acceleration: -0.344814814814814
	},
	{
		id: 385,
		time: 384,
		velocity: 10.5066666666667,
		power: -1981.71303413765,
		road: 1096.31435185185,
		acceleration: -0.386574074074074
	},
	{
		id: 386,
		time: 385,
		velocity: 10.1147222222222,
		power: -395.390246248521,
		road: 1106.66125,
		acceleration: -0.22787037037037
	},
	{
		id: 387,
		time: 386,
		velocity: 10.0775,
		power: 248.735065887808,
		road: 1116.81430555555,
		acceleration: -0.159814814814816
	},
	{
		id: 388,
		time: 387,
		velocity: 10.0272222222222,
		power: 1660.10902945856,
		road: 1126.88171296296,
		acceleration: -0.0114814814814803
	},
	{
		id: 389,
		time: 388,
		velocity: 10.0802777777778,
		power: 4411.58792092622,
		road: 1137.07740740741,
		acceleration: 0.268055555555554
	},
	{
		id: 390,
		time: 389,
		velocity: 10.8816666666667,
		power: 5159.082108469,
		road: 1147.57032407407,
		acceleration: 0.326388888888889
	},
	{
		id: 391,
		time: 390,
		velocity: 11.0063888888889,
		power: 4375.4019809412,
		road: 1158.34305555555,
		acceleration: 0.23324074074074
	},
	{
		id: 392,
		time: 391,
		velocity: 10.78,
		power: 2833.23305909042,
		road: 1169.27101851852,
		acceleration: 0.0772222222222219
	},
	{
		id: 393,
		time: 392,
		velocity: 11.1133333333333,
		power: 1740.63975320245,
		road: 1180.22342592593,
		acceleration: -0.0283333333333324
	},
	{
		id: 394,
		time: 393,
		velocity: 10.9213888888889,
		power: 3818.66112843034,
		road: 1191.24541666667,
		acceleration: 0.1675
	},
	{
		id: 395,
		time: 394,
		velocity: 11.2825,
		power: 4514.2798315219,
		road: 1202.46300925926,
		acceleration: 0.223703703703702
	},
	{
		id: 396,
		time: 395,
		velocity: 11.7844444444444,
		power: 6558.99887684708,
		road: 1213.98962962963,
		acceleration: 0.394351851851852
	},
	{
		id: 397,
		time: 396,
		velocity: 12.1044444444444,
		power: 9043.36999742527,
		road: 1226.00356481481,
		acceleration: 0.580277777777781
	},
	{
		id: 398,
		time: 397,
		velocity: 13.0233333333333,
		power: 8436.03034182119,
		road: 1238.55180555555,
		acceleration: 0.488333333333333
	},
	{
		id: 399,
		time: 398,
		velocity: 13.2494444444444,
		power: 10406.8297237903,
		road: 1251.64837962963,
		acceleration: 0.608333333333331
	},
	{
		id: 400,
		time: 399,
		velocity: 13.9294444444444,
		power: 6709.35542686808,
		road: 1265.19310185185,
		acceleration: 0.287962962962965
	},
	{
		id: 401,
		time: 400,
		velocity: 13.8872222222222,
		power: 3826.50954640116,
		road: 1278.91111111111,
		acceleration: 0.0586111111111105
	},
	{
		id: 402,
		time: 401,
		velocity: 13.4252777777778,
		power: -590.649571184896,
		road: 1292.51976851852,
		acceleration: -0.277314814814815
	},
	{
		id: 403,
		time: 402,
		velocity: 13.0975,
		power: -3486.73046463851,
		road: 1305.73861111111,
		acceleration: -0.502314814814815
	},
	{
		id: 404,
		time: 403,
		velocity: 12.3802777777778,
		power: -1337.7648480212,
		road: 1318.54162037037,
		acceleration: -0.32935185185185
	},
	{
		id: 405,
		time: 404,
		velocity: 12.4372222222222,
		power: -2125.74878321331,
		road: 1330.98305555555,
		acceleration: -0.393796296296298
	},
	{
		id: 406,
		time: 405,
		velocity: 11.9161111111111,
		power: -1581.56928277515,
		road: 1343.05412037037,
		acceleration: -0.346944444444443
	},
	{
		id: 407,
		time: 406,
		velocity: 11.3394444444444,
		power: -7833.73078703755,
		road: 1354.49194444444,
		acceleration: -0.919537037037037
	},
	{
		id: 408,
		time: 407,
		velocity: 9.67861111111111,
		power: -9572.85304323539,
		road: 1364.89282407407,
		acceleration: -1.15435185185185
	},
	{
		id: 409,
		time: 408,
		velocity: 8.45305555555556,
		power: -10990.8715205609,
		road: 1373.99537037037,
		acceleration: -1.44231481481481
	},
	{
		id: 410,
		time: 409,
		velocity: 7.0125,
		power: -7314.85754809327,
		road: 1381.80356481481,
		acceleration: -1.14638888888889
	},
	{
		id: 411,
		time: 410,
		velocity: 6.23944444444444,
		power: -6328.80492544342,
		road: 1388.46259259259,
		acceleration: -1.15194444444444
	},
	{
		id: 412,
		time: 411,
		velocity: 4.99722222222222,
		power: -5347.73167811486,
		road: 1393.96171296296,
		acceleration: -1.16787037037037
	},
	{
		id: 413,
		time: 412,
		velocity: 3.50888888888889,
		power: -4857.38973405516,
		road: 1398.20537037037,
		acceleration: -1.34305555555556
	},
	{
		id: 414,
		time: 413,
		velocity: 2.21027777777778,
		power: -2668.79966215376,
		road: 1401.24916666667,
		acceleration: -1.05666666666667
	},
	{
		id: 415,
		time: 414,
		velocity: 1.82722222222222,
		power: -902.354277897036,
		road: 1403.48694444444,
		acceleration: -0.55537037037037
	},
	{
		id: 416,
		time: 415,
		velocity: 1.84277777777778,
		power: 226.510019910917,
		road: 1405.44328703704,
		acceleration: -0.00749999999999962
	},
	{
		id: 417,
		time: 416,
		velocity: 2.18777777777778,
		power: 1124.72965498222,
		road: 1407.60509259259,
		acceleration: 0.418425925925926
	},
	{
		id: 418,
		time: 417,
		velocity: 3.0825,
		power: 1720.91353812457,
		road: 1410.25291666667,
		acceleration: 0.553611111111111
	},
	{
		id: 419,
		time: 418,
		velocity: 3.50361111111111,
		power: 2726.85030584269,
		road: 1413.54685185185,
		acceleration: 0.738611111111111
	},
	{
		id: 420,
		time: 419,
		velocity: 4.40361111111111,
		power: 2815.82407131209,
		road: 1417.51555555556,
		acceleration: 0.610925925925927
	},
	{
		id: 421,
		time: 420,
		velocity: 4.91527777777778,
		power: 3994.90131477959,
		road: 1422.17152777778,
		acceleration: 0.763611111111111
	},
	{
		id: 422,
		time: 421,
		velocity: 5.79444444444444,
		power: 4995.39715781839,
		road: 1427.61962962963,
		acceleration: 0.820648148148148
	},
	{
		id: 423,
		time: 422,
		velocity: 6.86555555555556,
		power: 5110.213573743,
		road: 1433.83578703704,
		acceleration: 0.715462962962963
	},
	{
		id: 424,
		time: 423,
		velocity: 7.06166666666667,
		power: 3544.88385143545,
		road: 1440.60824074074,
		acceleration: 0.39712962962963
	},
	{
		id: 425,
		time: 424,
		velocity: 6.98583333333333,
		power: 1647.13024716512,
		road: 1447.62509259259,
		acceleration: 0.0916666666666668
	},
	{
		id: 426,
		time: 425,
		velocity: 7.14055555555556,
		power: -254.584949821244,
		road: 1454.5912037037,
		acceleration: -0.193148148148149
	},
	{
		id: 427,
		time: 426,
		velocity: 6.48222222222222,
		power: -793.970403963899,
		road: 1461.32226851852,
		acceleration: -0.276944444444445
	},
	{
		id: 428,
		time: 427,
		velocity: 6.155,
		power: -1353.32104238459,
		road: 1467.72851851852,
		acceleration: -0.372685185185184
	},
	{
		id: 429,
		time: 428,
		velocity: 6.0225,
		power: -1424.48022675221,
		road: 1473.75009259259,
		acceleration: -0.396666666666667
	},
	{
		id: 430,
		time: 429,
		velocity: 5.29222222222222,
		power: 1109.39231277648,
		road: 1479.59976851852,
		acceleration: 0.0528703703703703
	},
	{
		id: 431,
		time: 430,
		velocity: 6.31361111111111,
		power: 1345.31289153318,
		road: 1485.52180555556,
		acceleration: 0.0918518518518505
	},
	{
		id: 432,
		time: 431,
		velocity: 6.29805555555556,
		power: 4302.88612975331,
		road: 1491.77680555556,
		acceleration: 0.574074074074075
	},
	{
		id: 433,
		time: 432,
		velocity: 7.01444444444444,
		power: 4865.70439403865,
		road: 1498.61597222222,
		acceleration: 0.594259259259259
	},
	{
		id: 434,
		time: 433,
		velocity: 8.09638888888889,
		power: 8747.0764685097,
		road: 1506.27231481481,
		acceleration: 1.04009259259259
	},
	{
		id: 435,
		time: 434,
		velocity: 9.41833333333333,
		power: 11634.5495276387,
		road: 1515.05851851852,
		acceleration: 1.21962962962963
	},
	{
		id: 436,
		time: 435,
		velocity: 10.6733333333333,
		power: 10841.058624577,
		road: 1524.93907407407,
		acceleration: 0.969074074074074
	},
	{
		id: 437,
		time: 436,
		velocity: 11.0036111111111,
		power: 4557.22860920043,
		road: 1535.43712962963,
		acceleration: 0.265925925925925
	},
	{
		id: 438,
		time: 437,
		velocity: 10.2161111111111,
		power: -2299.39807493669,
		road: 1545.85805555556,
		acceleration: -0.420185185185185
	},
	{
		id: 439,
		time: 438,
		velocity: 9.41277777777778,
		power: -5304.55079734929,
		road: 1555.69486111111,
		acceleration: -0.748055555555554
	},
	{
		id: 440,
		time: 439,
		velocity: 8.75944444444444,
		power: -4446.2029997798,
		road: 1564.81449074074,
		acceleration: -0.686296296296298
	},
	{
		id: 441,
		time: 440,
		velocity: 8.15722222222222,
		power: -3893.33379412342,
		road: 1573.26513888889,
		acceleration: -0.651666666666667
	},
	{
		id: 442,
		time: 441,
		velocity: 7.45777777777778,
		power: -3833.62032933113,
		road: 1581.05041666667,
		acceleration: -0.679074074074072
	},
	{
		id: 443,
		time: 442,
		velocity: 6.72222222222222,
		power: -2916.14951430222,
		road: 1588.20375,
		acceleration: -0.584814814814815
	},
	{
		id: 444,
		time: 443,
		velocity: 6.40277777777778,
		power: -1707.42608341767,
		road: 1594.85351851852,
		acceleration: -0.422314814814816
	},
	{
		id: 445,
		time: 444,
		velocity: 6.19083333333333,
		power: -663.738602688253,
		road: 1601.16189814815,
		acceleration: -0.260462962962963
	},
	{
		id: 446,
		time: 445,
		velocity: 5.94083333333333,
		power: -357.059091920198,
		road: 1607.2350462963,
		acceleration: -0.209999999999999
	},
	{
		id: 447,
		time: 446,
		velocity: 5.77277777777778,
		power: -308.752523258703,
		road: 1613.10212962963,
		acceleration: -0.20212962962963
	},
	{
		id: 448,
		time: 447,
		velocity: 5.58444444444444,
		power: -182.000112640901,
		road: 1618.77851851852,
		acceleration: -0.179259259259259
	},
	{
		id: 449,
		time: 448,
		velocity: 5.40305555555556,
		power: -1989.16047872395,
		road: 1624.09680555556,
		acceleration: -0.536944444444445
	},
	{
		id: 450,
		time: 449,
		velocity: 4.16194444444444,
		power: -3118.13676377231,
		road: 1628.72199074074,
		acceleration: -0.849259259259259
	},
	{
		id: 451,
		time: 450,
		velocity: 3.03666666666667,
		power: -3736.29483948493,
		road: 1632.30606481481,
		acceleration: -1.23296296296296
	},
	{
		id: 452,
		time: 451,
		velocity: 1.70416666666667,
		power: -2037.6204872802,
		road: 1634.77282407407,
		acceleration: -1.00166666666667
	},
	{
		id: 453,
		time: 452,
		velocity: 1.15694444444444,
		power: -1222.79301214242,
		road: 1636.23263888889,
		acceleration: -1.01222222222222
	},
	{
		id: 454,
		time: 453,
		velocity: 0,
		power: -279.4284825343,
		road: 1636.90231481481,
		acceleration: -0.568055555555556
	},
	{
		id: 455,
		time: 454,
		velocity: 0,
		power: -45.9709400961576,
		road: 1637.10375,
		acceleration: -0.368425925925926
	},
	{
		id: 456,
		time: 455,
		velocity: 0.0516666666666667,
		power: 2.08075540466809,
		road: 1637.12097222222,
		acceleration: 0
	},
	{
		id: 457,
		time: 456,
		velocity: 0,
		power: 2.08075540466809,
		road: 1637.13819444444,
		acceleration: 0
	},
	{
		id: 458,
		time: 457,
		velocity: 0,
		power: 0.899879239766082,
		road: 1637.14680555556,
		acceleration: -0.0172222222222222
	},
	{
		id: 459,
		time: 458,
		velocity: 0,
		power: 0,
		road: 1637.14680555556,
		acceleration: 0
	},
	{
		id: 460,
		time: 459,
		velocity: 0,
		power: 89.5492017844285,
		road: 1637.33462962963,
		acceleration: 0.375648148148148
	},
	{
		id: 461,
		time: 460,
		velocity: 1.12694444444444,
		power: 1297.23586068519,
		road: 1638.33212962963,
		acceleration: 1.2437037037037
	},
	{
		id: 462,
		time: 461,
		velocity: 3.73111111111111,
		power: 4839.78528017458,
		road: 1640.88486111111,
		acceleration: 1.86675925925926
	},
	{
		id: 463,
		time: 462,
		velocity: 5.60027777777778,
		power: 7352.31765088514,
		road: 1645.19907407407,
		acceleration: 1.6562037037037
	},
	{
		id: 464,
		time: 463,
		velocity: 6.09555555555556,
		power: 6372.73432412949,
		road: 1650.86074074074,
		acceleration: 1.0387037037037
	},
	{
		id: 465,
		time: 464,
		velocity: 6.84722222222222,
		power: 5082.50552854935,
		road: 1657.37625,
		acceleration: 0.668981481481483
	},
	{
		id: 466,
		time: 465,
		velocity: 7.60722222222222,
		power: 8243.14814854435,
		road: 1664.73587962963,
		acceleration: 1.01925925925926
	},
	{
		id: 467,
		time: 466,
		velocity: 9.15333333333333,
		power: 10793.9868092545,
		road: 1673.19166666667,
		acceleration: 1.17305555555556
	},
	{
		id: 468,
		time: 467,
		velocity: 10.3663888888889,
		power: 12748.3198161471,
		road: 1682.83763888889,
		acceleration: 1.20731481481481
	},
	{
		id: 469,
		time: 468,
		velocity: 11.2291666666667,
		power: 8539.71053814661,
		road: 1693.41550925926,
		acceleration: 0.656481481481482
	},
	{
		id: 470,
		time: 469,
		velocity: 11.1227777777778,
		power: 4207.82540150767,
		road: 1704.42421296296,
		acceleration: 0.205185185185185
	},
	{
		id: 471,
		time: 470,
		velocity: 10.9819444444444,
		power: 264.113783825558,
		road: 1715.45009259259,
		acceleration: -0.170833333333333
	},
	{
		id: 472,
		time: 471,
		velocity: 10.7166666666667,
		power: 1734.08357855293,
		road: 1726.37643518519,
		acceleration: -0.0282407407407419
	},
	{
		id: 473,
		time: 472,
		velocity: 11.0380555555556,
		power: 5571.52232442701,
		road: 1737.45412037037,
		acceleration: 0.330925925925929
	},
	{
		id: 474,
		time: 473,
		velocity: 11.9747222222222,
		power: 9422.26655874697,
		road: 1749.02282407407,
		acceleration: 0.651111111111108
	},
	{
		id: 475,
		time: 474,
		velocity: 12.67,
		power: 12300.5799814483,
		road: 1761.33421296296,
		acceleration: 0.834259259259261
	},
	{
		id: 476,
		time: 475,
		velocity: 13.5408333333333,
		power: 11794.1708403596,
		road: 1774.42273148148,
		acceleration: 0.720000000000001
	},
	{
		id: 477,
		time: 476,
		velocity: 14.1347222222222,
		power: 12525.8329237576,
		road: 1788.22884259259,
		acceleration: 0.715185185185184
	},
	{
		id: 478,
		time: 477,
		velocity: 14.8155555555556,
		power: 12749.1763232007,
		road: 1802.72972222222,
		acceleration: 0.674351851851853
	},
	{
		id: 479,
		time: 478,
		velocity: 15.5638888888889,
		power: 12506.3139773245,
		road: 1817.87157407407,
		acceleration: 0.60759259259259
	},
	{
		id: 480,
		time: 479,
		velocity: 15.9575,
		power: 13142.8718388129,
		road: 1833.62009259259,
		acceleration: 0.605740740740744
	},
	{
		id: 481,
		time: 480,
		velocity: 16.6327777777778,
		power: 11668.4644488577,
		road: 1849.90759259259,
		acceleration: 0.472222222222218
	},
	{
		id: 482,
		time: 481,
		velocity: 16.9805555555556,
		power: 11261.9093174027,
		road: 1866.64041666667,
		acceleration: 0.418425925925927
	},
	{
		id: 483,
		time: 482,
		velocity: 17.2127777777778,
		power: 7481.41971630914,
		road: 1883.66671296296,
		acceleration: 0.168518518518518
	},
	{
		id: 484,
		time: 483,
		velocity: 17.1383333333333,
		power: 5293.41819193544,
		road: 1900.7924537037,
		acceleration: 0.0303703703703704
	},
	{
		id: 485,
		time: 484,
		velocity: 17.0716666666667,
		power: 4826.58058145252,
		road: 1917.93402777778,
		acceleration: 0.00129629629629591
	},
	{
		id: 486,
		time: 485,
		velocity: 17.2166666666667,
		power: 6236.47217196758,
		road: 1935.11898148148,
		acceleration: 0.0854629629629642
	},
	{
		id: 487,
		time: 486,
		velocity: 17.3947222222222,
		power: 6104.67640312697,
		road: 1952.38375,
		acceleration: 0.0741666666666667
	},
	{
		id: 488,
		time: 487,
		velocity: 17.2941666666667,
		power: 2528.42848205833,
		road: 1969.61490740741,
		acceleration: -0.141388888888887
	},
	{
		id: 489,
		time: 488,
		velocity: 16.7925,
		power: -1835.36248332335,
		road: 1986.57402777778,
		acceleration: -0.402685185185184
	},
	{
		id: 490,
		time: 489,
		velocity: 16.1866666666667,
		power: -2806.38245569762,
		road: 2003.10231481481,
		acceleration: -0.45898148148148
	},
	{
		id: 491,
		time: 490,
		velocity: 15.9172222222222,
		power: -649.651809762859,
		road: 2019.24291666667,
		acceleration: -0.316388888888891
	},
	{
		id: 492,
		time: 491,
		velocity: 15.8433333333333,
		power: 1538.14826617119,
		road: 2035.14097222222,
		acceleration: -0.168703703703704
	},
	{
		id: 493,
		time: 492,
		velocity: 15.6805555555556,
		power: 2686.98485259595,
		road: 2050.91,
		acceleration: -0.0893518518518519
	},
	{
		id: 494,
		time: 493,
		velocity: 15.6491666666667,
		power: 2963.48361657688,
		road: 2066.6000462963,
		acceleration: -0.0686111111111103
	},
	{
		id: 495,
		time: 494,
		velocity: 15.6375,
		power: 3362.20173733512,
		road: 2082.23564814815,
		acceleration: -0.0402777777777779
	},
	{
		id: 496,
		time: 495,
		velocity: 15.5597222222222,
		power: 4375.90691913432,
		road: 2097.865,
		acceleration: 0.0277777777777786
	},
	{
		id: 497,
		time: 496,
		velocity: 15.7325,
		power: 4720.08835484639,
		road: 2113.53291666667,
		acceleration: 0.0493518518518492
	},
	{
		id: 498,
		time: 497,
		velocity: 15.7855555555556,
		power: 6413.93415809926,
		road: 2129.30439814815,
		acceleration: 0.157777777777779
	},
	{
		id: 499,
		time: 498,
		velocity: 16.0330555555556,
		power: 5840.49295398438,
		road: 2145.21175925926,
		acceleration: 0.113981481481481
	},
	{
		id: 500,
		time: 499,
		velocity: 16.0744444444444,
		power: 7333.18177659725,
		road: 2161.27833333333,
		acceleration: 0.204444444444444
	},
	{
		id: 501,
		time: 500,
		velocity: 16.3988888888889,
		power: 3895.90837982357,
		road: 2177.43592592593,
		acceleration: -0.0224074074074068
	},
	{
		id: 502,
		time: 501,
		velocity: 15.9658333333333,
		power: 1379.34700830053,
		road: 2193.49087962963,
		acceleration: -0.182870370370372
	},
	{
		id: 503,
		time: 502,
		velocity: 15.5258333333333,
		power: -4096.02150721768,
		road: 2209.18472222222,
		acceleration: -0.539351851851853
	},
	{
		id: 504,
		time: 503,
		velocity: 14.7808333333333,
		power: -2127.78303260335,
		road: 2224.40671296296,
		acceleration: -0.404351851851851
	},
	{
		id: 505,
		time: 504,
		velocity: 14.7527777777778,
		power: -74.4892798704113,
		road: 2239.29768518518,
		acceleration: -0.257685185185185
	},
	{
		id: 506,
		time: 505,
		velocity: 14.7527777777778,
		power: 4536.22129801343,
		road: 2254.09481481481,
		acceleration: 0.0700000000000003
	},
	{
		id: 507,
		time: 506,
		velocity: 14.9908333333333,
		power: 7150.44703337672,
		road: 2269.05037037037,
		acceleration: 0.246851851851854
	},
	{
		id: 508,
		time: 507,
		velocity: 15.4933333333333,
		power: 7509.79128580793,
		road: 2284.2587962963,
		acceleration: 0.258888888888887
	},
	{
		id: 509,
		time: 508,
		velocity: 15.5294444444444,
		power: 8491.07891168318,
		road: 2299.75199074074,
		acceleration: 0.31064814814815
	},
	{
		id: 510,
		time: 509,
		velocity: 15.9227777777778,
		power: 9742.18211728724,
		road: 2315.58787037037,
		acceleration: 0.37472222222222
	},
	{
		id: 511,
		time: 510,
		velocity: 16.6175,
		power: 9291.84407046886,
		road: 2331.77375,
		acceleration: 0.325277777777778
	},
	{
		id: 512,
		time: 511,
		velocity: 16.5052777777778,
		power: 7118.50685169696,
		road: 2348.20888888889,
		acceleration: 0.173240740740741
	},
	{
		id: 513,
		time: 512,
		velocity: 16.4425,
		power: 2912.08916039143,
		road: 2364.68287037037,
		acceleration: -0.0955555555555563
	},
	{
		id: 514,
		time: 513,
		velocity: 16.3308333333333,
		power: 3719.41632051577,
		road: 2381.08805555556,
		acceleration: -0.0420370370370406
	},
	{
		id: 515,
		time: 514,
		velocity: 16.3791666666667,
		power: 5583.70175395493,
		road: 2397.51027777778,
		acceleration: 0.0761111111111141
	},
	{
		id: 516,
		time: 515,
		velocity: 16.6708333333333,
		power: 4690.3050980868,
		road: 2413.97930555556,
		acceleration: 0.0175000000000018
	},
	{
		id: 517,
		time: 516,
		velocity: 16.3833333333333,
		power: 5386.72969800506,
		road: 2430.48717592593,
		acceleration: 0.0601851851851869
	},
	{
		id: 518,
		time: 517,
		velocity: 16.5597222222222,
		power: 4586.29754587905,
		road: 2447.02925925926,
		acceleration: 0.00824074074073877
	},
	{
		id: 519,
		time: 518,
		velocity: 16.6955555555556,
		power: 7337.32762242246,
		road: 2463.66435185185,
		acceleration: 0.177777777777774
	},
	{
		id: 520,
		time: 519,
		velocity: 16.9166666666667,
		power: 6484.55326593334,
		road: 2480.44722222222,
		acceleration: 0.117777777777782
	},
	{
		id: 521,
		time: 520,
		velocity: 16.9130555555556,
		power: 4025.90753173724,
		road: 2497.27060185185,
		acceleration: -0.0367592592592594
	},
	{
		id: 522,
		time: 521,
		velocity: 16.5852777777778,
		power: 2424.5675018065,
		road: 2514.00865740741,
		acceleration: -0.133888888888887
	},
	{
		id: 523,
		time: 522,
		velocity: 16.515,
		power: 2292.06089378582,
		road: 2530.61055555555,
		acceleration: -0.13842592592593
	},
	{
		id: 524,
		time: 523,
		velocity: 16.4977777777778,
		power: 3990.5452905632,
		road: 2547.12893518518,
		acceleration: -0.0286111111111111
	},
	{
		id: 525,
		time: 524,
		velocity: 16.4994444444444,
		power: 7748.5304277327,
		road: 2563.73550925926,
		acceleration: 0.205000000000002
	},
	{
		id: 526,
		time: 525,
		velocity: 17.13,
		power: 13929.9008148486,
		road: 2580.72796296296,
		acceleration: 0.566759259259261
	},
	{
		id: 527,
		time: 526,
		velocity: 18.1980555555556,
		power: 18434.0110214012,
		road: 2598.39717592592,
		acceleration: 0.786759259259256
	},
	{
		id: 528,
		time: 527,
		velocity: 18.8597222222222,
		power: 25616.9068819582,
		road: 2617.01675925926,
		acceleration: 1.11398148148148
	},
	{
		id: 529,
		time: 528,
		velocity: 20.4719444444444,
		power: 26209.2678138037,
		road: 2636.71467592592,
		acceleration: 1.04268518518519
	},
	{
		id: 530,
		time: 529,
		velocity: 21.3261111111111,
		power: 34854.5932854926,
		road: 2657.61675925926,
		acceleration: 1.36564814814815
	},
	{
		id: 531,
		time: 530,
		velocity: 22.9566666666667,
		power: 31338.447695828,
		road: 2679.73847222222,
		acceleration: 1.07361111111111
	},
	{
		id: 532,
		time: 531,
		velocity: 23.6927777777778,
		power: 39853.7570822849,
		road: 2703.06967592592,
		acceleration: 1.34537037037037
	},
	{
		id: 533,
		time: 532,
		velocity: 25.3622222222222,
		power: 41918.2686057839,
		road: 2727.7237037037,
		acceleration: 1.30027777777777
	},
	{
		id: 534,
		time: 533,
		velocity: 26.8575,
		power: 35962.1535069574,
		road: 2753.50287037037,
		acceleration: 0.949999999999999
	},
	{
		id: 535,
		time: 534,
		velocity: 26.5427777777778,
		power: 18303.2315963739,
		road: 2779.85921296296,
		acceleration: 0.204351851851854
	},
	{
		id: 536,
		time: 535,
		velocity: 25.9752777777778,
		power: 3950.91179727245,
		road: 2806.13814814815,
		acceleration: -0.359166666666667
	},
	{
		id: 537,
		time: 536,
		velocity: 25.78,
		power: 3820.38086690052,
		road: 2832.06157407407,
		acceleration: -0.351851851851848
	},
	{
		id: 538,
		time: 537,
		velocity: 25.4872222222222,
		power: 7909.00666003393,
		road: 2857.72064814815,
		acceleration: -0.176851851851854
	},
	{
		id: 539,
		time: 538,
		velocity: 25.4447222222222,
		power: 5878.75055712925,
		road: 2883.16578703704,
		acceleration: -0.251018518518521
	},
	{
		id: 540,
		time: 539,
		velocity: 25.0269444444444,
		power: 6765.60810403649,
		road: 2908.38254629629,
		acceleration: -0.205740740740737
	},
	{
		id: 541,
		time: 540,
		velocity: 24.87,
		power: 5086.48881477593,
		road: 2933.36324074074,
		acceleration: -0.266388888888894
	},
	{
		id: 542,
		time: 541,
		velocity: 24.6455555555556,
		power: 5502.75375482663,
		road: 2958.09092592592,
		acceleration: -0.239629629629622
	},
	{
		id: 543,
		time: 542,
		velocity: 24.3080555555556,
		power: 1991.38541409387,
		road: 2982.50986111111,
		acceleration: -0.377870370370374
	},
	{
		id: 544,
		time: 543,
		velocity: 23.7363888888889,
		power: 310.535778258817,
		road: 3006.52074074074,
		acceleration: -0.438240740740746
	},
	{
		id: 545,
		time: 544,
		velocity: 23.3308333333333,
		power: 2895.37620665282,
		road: 3030.15546296296,
		acceleration: -0.314074074074071
	},
	{
		id: 546,
		time: 545,
		velocity: 23.3658333333333,
		power: 4654.18840270012,
		road: 3053.51962962963,
		acceleration: -0.227037037037036
	},
	{
		id: 547,
		time: 546,
		velocity: 23.0552777777778,
		power: 550.691330338211,
		road: 3076.56953703703,
		acceleration: -0.401481481481483
	},
	{
		id: 548,
		time: 547,
		velocity: 22.1263888888889,
		power: -727.106480519399,
		road: 3099.19421296296,
		acceleration: -0.448981481481482
	},
	{
		id: 549,
		time: 548,
		velocity: 22.0188888888889,
		power: -1798.30797031706,
		road: 3121.35023148148,
		acceleration: -0.488333333333333
	},
	{
		id: 550,
		time: 549,
		velocity: 21.5902777777778,
		power: 1243.31332471097,
		road: 3143.09509259259,
		acceleration: -0.333981481481477
	},
	{
		id: 551,
		time: 550,
		velocity: 21.1244444444444,
		power: -5202.61833768417,
		road: 3164.35439814815,
		acceleration: -0.637129629629634
	},
	{
		id: 552,
		time: 551,
		velocity: 20.1075,
		power: -8553.52782311278,
		road: 3184.89537037037,
		acceleration: -0.799537037037034
	},
	{
		id: 553,
		time: 552,
		velocity: 19.1916666666667,
		power: -14638.8874069207,
		road: 3204.47458333333,
		acceleration: -1.12398148148149
	},
	{
		id: 554,
		time: 553,
		velocity: 17.7525,
		power: -15646.2072816008,
		road: 3222.8887037037,
		acceleration: -1.2062037037037
	},
	{
		id: 555,
		time: 554,
		velocity: 16.4888888888889,
		power: -17277.5221666427,
		road: 3240.0262037037,
		acceleration: -1.34703703703704
	},
	{
		id: 556,
		time: 555,
		velocity: 15.1505555555556,
		power: -21253.0611498815,
		road: 3255.64574074074,
		acceleration: -1.68888888888889
	},
	{
		id: 557,
		time: 556,
		velocity: 12.6858333333333,
		power: -22858.015355909,
		road: 3269.43523148148,
		acceleration: -1.9712037037037
	},
	{
		id: 558,
		time: 557,
		velocity: 10.5752777777778,
		power: -24623.042572587,
		road: 3281.02305555555,
		acceleration: -2.43212962962963
	},
	{
		id: 559,
		time: 558,
		velocity: 7.85416666666667,
		power: -18817.2565246865,
		road: 3290.23430555555,
		acceleration: -2.32101851851852
	},
	{
		id: 560,
		time: 559,
		velocity: 5.72277777777778,
		power: -14320.3144597597,
		road: 3297.11324074074,
		acceleration: -2.34361111111111
	},
	{
		id: 561,
		time: 560,
		velocity: 3.54444444444444,
		power: -9128.39562646589,
		road: 3301.70361111111,
		acceleration: -2.23351851851852
	},
	{
		id: 562,
		time: 561,
		velocity: 1.15361111111111,
		power: -4245.99819085142,
		road: 3304.22342592592,
		acceleration: -1.90759259259259
	},
	{
		id: 563,
		time: 562,
		velocity: 0,
		power: -973.71740872266,
		road: 3305.1987037037,
		acceleration: -1.18148148148148
	},
	{
		id: 564,
		time: 563,
		velocity: 0,
		power: -46.8136063515269,
		road: 3305.39097222222,
		acceleration: -0.384537037037037
	},
	{
		id: 565,
		time: 564,
		velocity: 0,
		power: 283.540176881453,
		road: 3305.74717592592,
		acceleration: 0.712407407407407
	},
	{
		id: 566,
		time: 565,
		velocity: 2.13722222222222,
		power: 1629.79378044493,
		road: 3307.05333333333,
		acceleration: 1.1875
	},
	{
		id: 567,
		time: 566,
		velocity: 3.5625,
		power: 4930.66755492246,
		road: 3309.82467592592,
		acceleration: 1.74287037037037
	},
	{
		id: 568,
		time: 567,
		velocity: 5.22861111111111,
		power: 4846.20681607918,
		road: 3314.00861111111,
		acceleration: 1.08231481481482
	},
	{
		id: 569,
		time: 568,
		velocity: 5.38416666666667,
		power: 2368.96301220891,
		road: 3318.91722222222,
		acceleration: 0.367037037037036
	},
	{
		id: 570,
		time: 569,
		velocity: 4.66361111111111,
		power: -829.804600025602,
		road: 3323.85027777777,
		acceleration: -0.318148148148148
	},
	{
		id: 571,
		time: 570,
		velocity: 4.27416666666667,
		power: 301.742250838052,
		road: 3328.58777777777,
		acceleration: -0.0729629629629622
	},
	{
		id: 572,
		time: 571,
		velocity: 5.16527777777778,
		power: 2923.70593527318,
		road: 3333.52958333333,
		acceleration: 0.481574074074072
	},
	{
		id: 573,
		time: 572,
		velocity: 6.10833333333333,
		power: 6158.72008185239,
		road: 3339.20972222222,
		acceleration: 0.995092592592592
	},
	{
		id: 574,
		time: 573,
		velocity: 7.25944444444444,
		power: 7507.39463220245,
		road: 3345.90092592592,
		acceleration: 1.02703703703704
	},
	{
		id: 575,
		time: 574,
		velocity: 8.24638888888889,
		power: 8415.64709831235,
		road: 3353.59958333333,
		acceleration: 0.987870370370371
	},
	{
		id: 576,
		time: 575,
		velocity: 9.07194444444444,
		power: 11778.765196521,
		road: 3362.4086574074,
		acceleration: 1.23296296296296
	},
	{
		id: 577,
		time: 576,
		velocity: 10.9583333333333,
		power: 9899.74022438651,
		road: 3372.2699074074,
		acceleration: 0.871388888888889
	},
	{
		id: 578,
		time: 577,
		velocity: 10.8605555555556,
		power: 10679.5914619605,
		road: 3382.99310185185,
		acceleration: 0.852499999999999
	},
	{
		id: 579,
		time: 578,
		velocity: 11.6294444444444,
		power: 3963.32209288672,
		road: 3394.22824074074,
		acceleration: 0.17138888888889
	},
	{
		id: 580,
		time: 579,
		velocity: 11.4725,
		power: 5774.79823362591,
		road: 3405.71185185185,
		acceleration: 0.325555555555555
	},
	{
		id: 581,
		time: 580,
		velocity: 11.8372222222222,
		power: 6982.27272003455,
		road: 3417.56375,
		acceleration: 0.411018518518517
	},
	{
		id: 582,
		time: 581,
		velocity: 12.8625,
		power: 11094.8590829642,
		road: 3429.98212962963,
		acceleration: 0.721944444444446
	},
	{
		id: 583,
		time: 582,
		velocity: 13.6383333333333,
		power: 16517.6393550996,
		road: 3443.2974537037,
		acceleration: 1.07194444444444
	},
	{
		id: 584,
		time: 583,
		velocity: 15.0530555555556,
		power: 15266.4477986806,
		road: 3457.58671296296,
		acceleration: 0.875925925925927
	},
	{
		id: 585,
		time: 584,
		velocity: 15.4902777777778,
		power: 17959.3928932451,
		road: 3472.8024537037,
		acceleration: 0.977037037037038
	},
	{
		id: 586,
		time: 585,
		velocity: 16.5694444444444,
		power: 15366.2960867919,
		road: 3488.87032407407,
		acceleration: 0.72722222222222
	},
	{
		id: 587,
		time: 586,
		velocity: 17.2347222222222,
		power: 14807.7608148519,
		road: 3505.62111111111,
		acceleration: 0.638611111111111
	},
	{
		id: 588,
		time: 587,
		velocity: 17.4061111111111,
		power: 10576.5741620321,
		road: 3522.86425925926,
		acceleration: 0.34611111111111
	},
	{
		id: 589,
		time: 588,
		velocity: 17.6077777777778,
		power: 10637.5449467401,
		road: 3540.44578703703,
		acceleration: 0.33064814814815
	},
	{
		id: 590,
		time: 589,
		velocity: 18.2266666666667,
		power: 17840.5456679175,
		road: 3558.55125,
		acceleration: 0.717222222222222
	},
	{
		id: 591,
		time: 590,
		velocity: 19.5577777777778,
		power: 22232.2640997182,
		road: 3577.46453703703,
		acceleration: 0.898425925925928
	},
	{
		id: 592,
		time: 591,
		velocity: 20.3030555555556,
		power: 23722.5628088028,
		road: 3597.27754629629,
		acceleration: 0.901018518518512
	},
	{
		id: 593,
		time: 592,
		velocity: 20.9297222222222,
		power: 14508.3229530208,
		road: 3617.72962962963,
		acceleration: 0.377129629629636
	},
	{
		id: 594,
		time: 593,
		velocity: 20.6891666666667,
		power: 5875.14210632028,
		road: 3638.33574074074,
		acceleration: -0.0690740740740736
	},
	{
		id: 595,
		time: 594,
		velocity: 20.0958333333333,
		power: 131.91728206643,
		road: 3658.72986111111,
		acceleration: -0.35490740740741
	},
	{
		id: 596,
		time: 595,
		velocity: 19.865,
		power: 2630.41725038581,
		road: 3678.83722222222,
		acceleration: -0.218611111111109
	},
	{
		id: 597,
		time: 596,
		velocity: 20.0333333333333,
		power: 10113.6750048778,
		road: 3698.92074074074,
		acceleration: 0.170925925925925
	},
	{
		id: 598,
		time: 597,
		velocity: 20.6086111111111,
		power: 13405.9116539875,
		road: 3719.25356481481,
		acceleration: 0.327685185185185
	},
	{
		id: 599,
		time: 598,
		velocity: 20.8480555555556,
		power: 15840.7943282553,
		road: 3739.96467592592,
		acceleration: 0.428888888888892
	},
	{
		id: 600,
		time: 599,
		velocity: 21.32,
		power: 13369.6283326357,
		road: 3761.0325,
		acceleration: 0.284537037037033
	},
	{
		id: 601,
		time: 600,
		velocity: 21.4622222222222,
		power: 11872.6051687493,
		road: 3782.34157407407,
		acceleration: 0.197962962962961
	},
	{
		id: 602,
		time: 601,
		velocity: 21.4419444444444,
		power: 9706.10733155867,
		road: 3803.79231481481,
		acceleration: 0.0853703703703772
	},
	{
		id: 603,
		time: 602,
		velocity: 21.5761111111111,
		power: 9811.20939944545,
		road: 3825.32898148148,
		acceleration: 0.0864814814814778
	},
	{
		id: 604,
		time: 603,
		velocity: 21.7216666666667,
		power: 10203.1270794481,
		road: 3846.95939814814,
		acceleration: 0.101018518518515
	},
	{
		id: 605,
		time: 604,
		velocity: 21.745,
		power: 8868.42032085564,
		road: 3868.65717592592,
		acceleration: 0.0337037037037042
	},
	{
		id: 606,
		time: 605,
		velocity: 21.6772222222222,
		power: 12299.2475044905,
		road: 3890.46814814814,
		acceleration: 0.192685185185184
	},
	{
		id: 607,
		time: 606,
		velocity: 22.2997222222222,
		power: 8811.90260991573,
		road: 3912.38611111111,
		acceleration: 0.021296296296299
	},
	{
		id: 608,
		time: 607,
		velocity: 21.8088888888889,
		power: 7727.60017253919,
		road: 3934.29967592592,
		acceleration: -0.0300925925925917
	},
	{
		id: 609,
		time: 608,
		velocity: 21.5869444444444,
		power: 596.602236458379,
		road: 3956.01606481481,
		acceleration: -0.36425925925926
	},
	{
		id: 610,
		time: 609,
		velocity: 21.2069444444444,
		power: 3956.12013291828,
		road: 3977.45347222222,
		acceleration: -0.193703703703704
	},
	{
		id: 611,
		time: 610,
		velocity: 21.2277777777778,
		power: 6708.58625280765,
		road: 3998.76657407407,
		acceleration: -0.0549074074074021
	},
	{
		id: 612,
		time: 611,
		velocity: 21.4222222222222,
		power: 12145.2079869423,
		road: 4020.15578703703,
		acceleration: 0.207129629629623
	},
	{
		id: 613,
		time: 612,
		velocity: 21.8283333333333,
		power: 16037.0632270841,
		road: 4041.83810185185,
		acceleration: 0.379074074074072
	},
	{
		id: 614,
		time: 613,
		velocity: 22.365,
		power: 18723.7792458769,
		road: 4063.9499537037,
		acceleration: 0.480000000000004
	},
	{
		id: 615,
		time: 614,
		velocity: 22.8622222222222,
		power: 20853.8186822807,
		road: 4086.57430555555,
		acceleration: 0.544999999999998
	},
	{
		id: 616,
		time: 615,
		velocity: 23.4633333333333,
		power: 18221.2462026509,
		road: 4109.66837962963,
		acceleration: 0.394444444444449
	},
	{
		id: 617,
		time: 616,
		velocity: 23.5483333333333,
		power: 14321.1668922171,
		road: 4133.06087962963,
		acceleration: 0.202407407407403
	},
	{
		id: 618,
		time: 617,
		velocity: 23.4694444444444,
		power: 7831.67490055257,
		road: 4156.51013888889,
		acceleration: -0.0888888888888886
	},
	{
		id: 619,
		time: 618,
		velocity: 23.1966666666667,
		power: 4761.22742024977,
		road: 4179.80504629629,
		acceleration: -0.219814814814811
	},
	{
		id: 620,
		time: 619,
		velocity: 22.8888888888889,
		power: -152.233631511035,
		road: 4202.77444444444,
		acceleration: -0.431203703703705
	},
	{
		id: 621,
		time: 620,
		velocity: 22.1758333333333,
		power: 3853.19490770736,
		road: 4225.40907407407,
		acceleration: -0.238333333333337
	},
	{
		id: 622,
		time: 621,
		velocity: 22.4816666666667,
		power: 10963.2203068867,
		road: 4247.97078703703,
		acceleration: 0.0925000000000011
	},
	{
		id: 623,
		time: 622,
		velocity: 23.1663888888889,
		power: 20666.7041472817,
		road: 4270.8387037037,
		acceleration: 0.519907407407409
	},
	{
		id: 624,
		time: 623,
		velocity: 23.7355555555556,
		power: 22127.1924636717,
		road: 4294.2412037037,
		acceleration: 0.549259259259259
	},
	{
		id: 625,
		time: 624,
		velocity: 24.1294444444444,
		power: 19499.8095459708,
		road: 4318.11944444444,
		acceleration: 0.402222222222221
	},
	{
		id: 626,
		time: 625,
		velocity: 24.3730555555556,
		power: 18783.8695171109,
		road: 4342.37282407407,
		acceleration: 0.348055555555554
	},
	{
		id: 627,
		time: 626,
		velocity: 24.7797222222222,
		power: 14222.1006268795,
		road: 4366.8699537037,
		acceleration: 0.139444444444447
	},
	{
		id: 628,
		time: 627,
		velocity: 24.5477777777778,
		power: 13675.5999419957,
		road: 4391.49171296296,
		acceleration: 0.109814814814815
	},
	{
		id: 629,
		time: 628,
		velocity: 24.7025,
		power: 9998.6236452464,
		road: 4416.14486111111,
		acceleration: -0.0470370370370361
	},
	{
		id: 630,
		time: 629,
		velocity: 24.6386111111111,
		power: 11580.1336002754,
		road: 4440.78467592592,
		acceleration: 0.0203703703703724
	},
	{
		id: 631,
		time: 630,
		velocity: 24.6088888888889,
		power: 11129.4170671327,
		road: 4465.43509259259,
		acceleration: 0.00083333333332547
	},
	{
		id: 632,
		time: 631,
		velocity: 24.705,
		power: 11541.2094895334,
		road: 4490.09481481481,
		acceleration: 0.0177777777777806
	},
	{
		id: 633,
		time: 632,
		velocity: 24.6919444444444,
		power: 10575.4033687459,
		road: 4514.75199074074,
		acceleration: -0.0228703703703701
	},
	{
		id: 634,
		time: 633,
		velocity: 24.5402777777778,
		power: 5066.31728425657,
		road: 4539.2725,
		acceleration: -0.25046296296296
	},
	{
		id: 635,
		time: 634,
		velocity: 23.9536111111111,
		power: 1655.72417363551,
		road: 4563.4749537037,
		acceleration: -0.38564814814815
	},
	{
		id: 636,
		time: 635,
		velocity: 23.535,
		power: -431.269897085721,
		road: 4587.25240740741,
		acceleration: -0.464351851851848
	},
	{
		id: 637,
		time: 636,
		velocity: 23.1472222222222,
		power: 1242.9772457854,
		road: 4610.60824074074,
		acceleration: -0.378888888888891
	},
	{
		id: 638,
		time: 637,
		velocity: 22.8169444444444,
		power: -38.721638314834,
		road: 4633.56180555555,
		acceleration: -0.425648148148145
	},
	{
		id: 639,
		time: 638,
		velocity: 22.2580555555556,
		power: -543.967013039769,
		road: 4656.08351851852,
		acceleration: -0.438055555555557
	},
	{
		id: 640,
		time: 639,
		velocity: 21.8330555555556,
		power: -1445.3828448987,
		road: 4678.15129629629,
		acceleration: -0.469814814814814
	},
	{
		id: 641,
		time: 640,
		velocity: 21.4075,
		power: -354.528896887181,
		road: 4699.78023148148,
		acceleration: -0.407870370370372
	},
	{
		id: 642,
		time: 641,
		velocity: 21.0344444444444,
		power: 515.214543261291,
		road: 4721.02708333333,
		acceleration: -0.356296296296296
	},
	{
		id: 643,
		time: 642,
		velocity: 20.7641666666667,
		power: -395.410460606988,
		road: 4741.89953703703,
		acceleration: -0.392499999999998
	},
	{
		id: 644,
		time: 643,
		velocity: 20.23,
		power: -563.041804689058,
		road: 4762.37958333333,
		acceleration: -0.392314814814817
	},
	{
		id: 645,
		time: 644,
		velocity: 19.8575,
		power: -4119.20503706699,
		road: 4782.37962962963,
		acceleration: -0.567685185185187
	},
	{
		id: 646,
		time: 645,
		velocity: 19.0611111111111,
		power: -4290.44901064743,
		road: 4801.81050925926,
		acceleration: -0.570648148148145
	},
	{
		id: 647,
		time: 646,
		velocity: 18.5180555555556,
		power: -3286.73229145548,
		road: 4820.70097222222,
		acceleration: -0.510185185185186
	},
	{
		id: 648,
		time: 647,
		velocity: 18.3269444444444,
		power: 205.912566597601,
		road: 4839.18222222222,
		acceleration: -0.308240740740739
	},
	{
		id: 649,
		time: 648,
		velocity: 18.1363888888889,
		power: 4199.77062910856,
		road: 4857.47134259259,
		acceleration: -0.07601851851852
	},
	{
		id: 650,
		time: 649,
		velocity: 18.29,
		power: 8338.28911465598,
		road: 4875.80166666666,
		acceleration: 0.158425925925926
	},
	{
		id: 651,
		time: 650,
		velocity: 18.8022222222222,
		power: 9103.86908126323,
		road: 4894.30791666666,
		acceleration: 0.193425925925926
	},
	{
		id: 652,
		time: 651,
		velocity: 18.7166666666667,
		power: 6916.19323729225,
		road: 4912.94314814815,
		acceleration: 0.0645370370370379
	},
	{
		id: 653,
		time: 652,
		velocity: 18.4836111111111,
		power: 3152.2080173087,
		road: 4931.53805555555,
		acceleration: -0.145185185185184
	},
	{
		id: 654,
		time: 653,
		velocity: 18.3666666666667,
		power: 4114.88018147337,
		road: 4950.01675925926,
		acceleration: -0.0872222222222234
	},
	{
		id: 655,
		time: 654,
		velocity: 18.455,
		power: 8762.66059004864,
		road: 4968.53856481481,
		acceleration: 0.173425925925926
	},
	{
		id: 656,
		time: 655,
		velocity: 19.0038888888889,
		power: 11548.087934931,
		road: 4987.30546296296,
		acceleration: 0.316759259259257
	},
	{
		id: 657,
		time: 656,
		velocity: 19.3169444444444,
		power: 12310.4146117148,
		road: 5006.40087962963,
		acceleration: 0.340277777777779
	},
	{
		id: 658,
		time: 657,
		velocity: 19.4758333333333,
		power: 11962.4442460266,
		road: 5025.81810185185,
		acceleration: 0.303333333333335
	},
	{
		id: 659,
		time: 658,
		velocity: 19.9138888888889,
		power: 8687.50741402782,
		road: 5045.44578703704,
		acceleration: 0.117592592592594
	},
	{
		id: 660,
		time: 659,
		velocity: 19.6697222222222,
		power: 6641.52828722848,
		road: 5065.13541666667,
		acceleration: 0.00629629629629491
	},
	{
		id: 661,
		time: 660,
		velocity: 19.4947222222222,
		power: 3022.69581021881,
		road: 5084.73675925926,
		acceleration: -0.18287037037037
	},
	{
		id: 662,
		time: 661,
		velocity: 19.3652777777778,
		power: 4499.83282161036,
		road: 5104.19699074074,
		acceleration: -0.0993518518518499
	},
	{
		id: 663,
		time: 662,
		velocity: 19.3716666666667,
		power: 5039.47852876672,
		road: 5123.57384259259,
		acceleration: -0.0674074074074085
	},
	{
		id: 664,
		time: 663,
		velocity: 19.2925,
		power: 6285.20329873884,
		road: 5142.9175,
		acceleration: 0.0010185185185172
	},
	{
		id: 665,
		time: 664,
		velocity: 19.3683333333333,
		power: 8639.74430114789,
		road: 5162.32425925926,
		acceleration: 0.125185185185188
	},
	{
		id: 666,
		time: 665,
		velocity: 19.7472222222222,
		power: 12210.9674877823,
		road: 5181.94625,
		acceleration: 0.305277777777775
	},
	{
		id: 667,
		time: 666,
		velocity: 20.2083333333333,
		power: 11124.8582576052,
		road: 5201.83759259259,
		acceleration: 0.233425925925925
	},
	{
		id: 668,
		time: 667,
		velocity: 20.0686111111111,
		power: 6003.87024701005,
		road: 5221.82615740741,
		acceleration: -0.038981481481482
	},
	{
		id: 669,
		time: 668,
		velocity: 19.6302777777778,
		power: -509.441265135118,
		road: 5241.60787037037,
		acceleration: -0.374722222222221
	},
	{
		id: 670,
		time: 669,
		velocity: 19.0841666666667,
		power: -686.050233424929,
		road: 5261.01398148148,
		acceleration: -0.376481481481481
	},
	{
		id: 671,
		time: 670,
		velocity: 18.9391666666667,
		power: 233.502240899591,
		road: 5280.07222222222,
		acceleration: -0.319259259259258
	},
	{
		id: 672,
		time: 671,
		velocity: 18.6725,
		power: 2291.01683215866,
		road: 5298.8712037037,
		acceleration: -0.199259259259261
	},
	{
		id: 673,
		time: 672,
		velocity: 18.4863888888889,
		power: 2975.12664244516,
		road: 5317.49259259259,
		acceleration: -0.155925925925924
	},
	{
		id: 674,
		time: 673,
		velocity: 18.4713888888889,
		power: 5713.62056897819,
		road: 5336.03634259259,
		acceleration: 0.000648148148144401
	},
	{
		id: 675,
		time: 674,
		velocity: 18.6744444444444,
		power: 7269.24250330776,
		road: 5354.62361111111,
		acceleration: 0.0863888888888908
	},
	{
		id: 676,
		time: 675,
		velocity: 18.7455555555556,
		power: 7208.90677179838,
		road: 5373.2937962963,
		acceleration: 0.079444444444448
	},
	{
		id: 677,
		time: 676,
		velocity: 18.7097222222222,
		power: 5707.88346736602,
		road: 5392.00074074074,
		acceleration: -0.00592592592592922
	},
	{
		id: 678,
		time: 677,
		velocity: 18.6566666666667,
		power: 4215.10986773168,
		road: 5410.66087962963,
		acceleration: -0.0876851851851832
	},
	{
		id: 679,
		time: 678,
		velocity: 18.4825,
		power: 3127.79714145546,
		road: 5429.20467592592,
		acceleration: -0.145
	},
	{
		id: 680,
		time: 679,
		velocity: 18.2747222222222,
		power: 885.312601569569,
		road: 5447.54273148148,
		acceleration: -0.266481481481485
	},
	{
		id: 681,
		time: 680,
		velocity: 17.8572222222222,
		power: -124.578814257654,
		road: 5465.58847222222,
		acceleration: -0.318148148148147
	},
	{
		id: 682,
		time: 681,
		velocity: 17.5280555555556,
		power: -1580.51713096678,
		road: 5483.27657407407,
		acceleration: -0.397129629629632
	},
	{
		id: 683,
		time: 682,
		velocity: 17.0833333333333,
		power: 2921.0418464758,
		road: 5500.70439814815,
		acceleration: -0.123425925925925
	},
	{
		id: 684,
		time: 683,
		velocity: 17.4869444444444,
		power: 7737.33331875947,
		road: 5518.15273148148,
		acceleration: 0.164444444444442
	},
	{
		id: 685,
		time: 684,
		velocity: 18.0213888888889,
		power: 13393.0278588767,
		road: 5535.92430555556,
		acceleration: 0.482037037037042
	},
	{
		id: 686,
		time: 685,
		velocity: 18.5294444444444,
		power: 12530.4795517524,
		road: 5554.13902777778,
		acceleration: 0.404259259259259
	},
	{
		id: 687,
		time: 686,
		velocity: 18.6997222222222,
		power: 7640.53177917921,
		road: 5572.61203703704,
		acceleration: 0.112314814814813
	},
	{
		id: 688,
		time: 687,
		velocity: 18.3583333333333,
		power: 5785.95348568156,
		road: 5591.1437962963,
		acceleration: 0.00518518518518718
	},
	{
		id: 689,
		time: 688,
		velocity: 18.545,
		power: 5175.33137972835,
		road: 5609.66375,
		acceleration: -0.0287962962962958
	},
	{
		id: 690,
		time: 689,
		velocity: 18.6133333333333,
		power: 5748.05289833993,
		road: 5628.17129629629,
		acceleration: 0.00398148148147826
	},
	{
		id: 691,
		time: 690,
		velocity: 18.3702777777778,
		power: 1802.09343345714,
		road: 5646.57287037037,
		acceleration: -0.215925925925927
	},
	{
		id: 692,
		time: 691,
		velocity: 17.8972222222222,
		power: 321.994806918365,
		road: 5664.71925925926,
		acceleration: -0.294444444444441
	},
	{
		id: 693,
		time: 692,
		velocity: 17.73,
		power: -339.210967733399,
		road: 5682.55513888889,
		acceleration: -0.326574074074074
	},
	{
		id: 694,
		time: 693,
		velocity: 17.3905555555556,
		power: 2751.61559656014,
		road: 5700.15837962963,
		acceleration: -0.138703703703701
	},
	{
		id: 695,
		time: 694,
		velocity: 17.4811111111111,
		power: 4253.12361315349,
		road: 5717.66907407407,
		acceleration: -0.0463888888888953
	},
	{
		id: 696,
		time: 695,
		velocity: 17.5908333333333,
		power: 6148.04338716134,
		road: 5735.18972222222,
		acceleration: 0.0662962962962972
	},
	{
		id: 697,
		time: 696,
		velocity: 17.5894444444444,
		power: 9725.26953442672,
		road: 5752.87888888889,
		acceleration: 0.270740740740742
	},
	{
		id: 698,
		time: 697,
		velocity: 18.2933333333333,
		power: 9290.00775905946,
		road: 5770.81953703704,
		acceleration: 0.232222222222223
	},
	{
		id: 699,
		time: 698,
		velocity: 18.2875,
		power: 8053.16202459088,
		road: 5788.95194444444,
		acceleration: 0.151296296296298
	},
	{
		id: 700,
		time: 699,
		velocity: 18.0433333333333,
		power: 1674.82601575595,
		road: 5807.05231481481,
		acceleration: -0.215370370370373
	},
	{
		id: 701,
		time: 700,
		velocity: 17.6472222222222,
		power: -145.085104693056,
		road: 5824.88740740741,
		acceleration: -0.315185185185186
	},
	{
		id: 702,
		time: 701,
		velocity: 17.3419444444444,
		power: -1438.98035955014,
		road: 5842.37203703704,
		acceleration: -0.385740740740737
	},
	{
		id: 703,
		time: 702,
		velocity: 16.8861111111111,
		power: 488.897583992258,
		road: 5859.53199074074,
		acceleration: -0.263611111111118
	},
	{
		id: 704,
		time: 703,
		velocity: 16.8563888888889,
		power: 1575.30162678867,
		road: 5876.46425925926,
		acceleration: -0.191759259259257
	},
	{
		id: 705,
		time: 704,
		velocity: 16.7666666666667,
		power: 3475.19070488875,
		road: 5893.26550925926,
		acceleration: -0.0702777777777754
	},
	{
		id: 706,
		time: 705,
		velocity: 16.6752777777778,
		power: 2455.17956143619,
		road: 5909.96615740741,
		acceleration: -0.130925925925929
	},
	{
		id: 707,
		time: 706,
		velocity: 16.4636111111111,
		power: 1209.46184139993,
		road: 5926.49884259259,
		acceleration: -0.204999999999998
	},
	{
		id: 708,
		time: 707,
		velocity: 16.1516666666667,
		power: 2112.22527135813,
		road: 5942.85740740741,
		acceleration: -0.14324074074074
	},
	{
		id: 709,
		time: 708,
		velocity: 16.2455555555556,
		power: 4593.61119008486,
		road: 5959.15319444445,
		acceleration: 0.0176851851851865
	},
	{
		id: 710,
		time: 709,
		velocity: 16.5166666666667,
		power: 6394.03491744492,
		road: 5975.52282407407,
		acceleration: 0.129999999999999
	},
	{
		id: 711,
		time: 710,
		velocity: 16.5416666666667,
		power: 5107.30192076597,
		road: 5991.97967592593,
		acceleration: 0.0444444444444443
	},
	{
		id: 712,
		time: 711,
		velocity: 16.3788888888889,
		power: 3711.35534692059,
		road: 6008.43662037037,
		acceleration: -0.0442592592592632
	},
	{
		id: 713,
		time: 712,
		velocity: 16.3838888888889,
		power: 5174.25788787956,
		road: 6024.89574074074,
		acceleration: 0.0486111111111143
	},
	{
		id: 714,
		time: 713,
		velocity: 16.6875,
		power: 6370.2433676302,
		road: 6041.43962962963,
		acceleration: 0.120925925925924
	},
	{
		id: 715,
		time: 714,
		velocity: 16.7416666666667,
		power: 6411.05418273143,
		road: 6058.10314814815,
		acceleration: 0.118333333333336
	},
	{
		id: 716,
		time: 715,
		velocity: 16.7388888888889,
		power: 5883.04309979257,
		road: 6074.86638888889,
		acceleration: 0.0811111111111096
	},
	{
		id: 717,
		time: 716,
		velocity: 16.9308333333333,
		power: 6338.72572827014,
		road: 6091.72296296296,
		acceleration: 0.105555555555554
	},
	{
		id: 718,
		time: 717,
		velocity: 17.0583333333333,
		power: 6287.13213602508,
		road: 6108.68134259259,
		acceleration: 0.0980555555555576
	},
	{
		id: 719,
		time: 718,
		velocity: 17.0330555555556,
		power: 3681.30727265649,
		road: 6125.65717592593,
		acceleration: -0.063148148148148
	},
	{
		id: 720,
		time: 719,
		velocity: 16.7413888888889,
		power: 2364.70634089797,
		road: 6142.53074074074,
		acceleration: -0.141388888888891
	},
	{
		id: 721,
		time: 720,
		velocity: 16.6341666666667,
		power: -124.938013648189,
		road: 6159.18777777778,
		acceleration: -0.291666666666664
	},
	{
		id: 722,
		time: 721,
		velocity: 16.1580555555556,
		power: -647.140749101301,
		road: 6175.53921296296,
		acceleration: -0.319537037037037
	},
	{
		id: 723,
		time: 722,
		velocity: 15.7827777777778,
		power: -1682.58775037427,
		road: 6191.54,
		acceleration: -0.38175925925926
	},
	{
		id: 724,
		time: 723,
		velocity: 15.4888888888889,
		power: -488.524084722638,
		road: 6207.20074074074,
		acceleration: -0.298333333333332
	},
	{
		id: 725,
		time: 724,
		velocity: 15.2630555555556,
		power: 378.848783226686,
		road: 6222.59467592593,
		acceleration: -0.235277777777778
	},
	{
		id: 726,
		time: 725,
		velocity: 15.0769444444444,
		power: 3260.45924899129,
		road: 6237.85342592593,
		acceleration: -0.0350925925925925
	},
	{
		id: 727,
		time: 726,
		velocity: 15.3836111111111,
		power: 3681.12436250987,
		road: 6253.09185185185,
		acceleration: -0.00555555555555465
	},
	{
		id: 728,
		time: 727,
		velocity: 15.2463888888889,
		power: -2435.42052714624,
		road: 6268.11532407407,
		acceleration: -0.424351851851853
	},
	{
		id: 729,
		time: 728,
		velocity: 13.8038888888889,
		power: -7892.00481756978,
		road: 6282.5175462963,
		acceleration: -0.818148148148147
	},
	{
		id: 730,
		time: 729,
		velocity: 12.9291666666667,
		power: -9299.86032432206,
		road: 6296.03509259259,
		acceleration: -0.951203703703705
	},
	{
		id: 731,
		time: 730,
		velocity: 12.3927777777778,
		power: -4499.57346816664,
		road: 6308.7825462963,
		acceleration: -0.588981481481483
	},
	{
		id: 732,
		time: 731,
		velocity: 12.0369444444444,
		power: -2023.39594884064,
		road: 6321.04291666667,
		acceleration: -0.385185185185184
	},
	{
		id: 733,
		time: 732,
		velocity: 11.7736111111111,
		power: -1939.14909665964,
		road: 6332.92162037037,
		acceleration: -0.378148148148149
	},
	{
		id: 734,
		time: 733,
		velocity: 11.2583333333333,
		power: -952.27877041747,
		road: 6344.46671296296,
		acceleration: -0.289074074074074
	},
	{
		id: 735,
		time: 734,
		velocity: 11.1697222222222,
		power: 866.336746761703,
		road: 6355.80736111111,
		acceleration: -0.119814814814815
	},
	{
		id: 736,
		time: 735,
		velocity: 11.4141666666667,
		power: 3653.95312168833,
		road: 6367.15689814815,
		acceleration: 0.137592592592592
	},
	{
		id: 737,
		time: 736,
		velocity: 11.6711111111111,
		power: 3981.36921934655,
		road: 6378.65578703704,
		acceleration: 0.161111111111111
	},
	{
		id: 738,
		time: 737,
		velocity: 11.6530555555556,
		power: 2536.01171532667,
		road: 6390.24833333333,
		acceleration: 0.0262037037037057
	},
	{
		id: 739,
		time: 738,
		velocity: 11.4927777777778,
		power: 1819.6589655086,
		road: 6401.83476851852,
		acceleration: -0.0384259259259263
	},
	{
		id: 740,
		time: 739,
		velocity: 11.5558333333333,
		power: 2154.78962793416,
		road: 6413.39828703704,
		acceleration: -0.00740740740740797
	},
	{
		id: 741,
		time: 740,
		velocity: 11.6308333333333,
		power: 4263.64214310982,
		road: 6425.04800925926,
		acceleration: 0.179814814814813
	},
	{
		id: 742,
		time: 741,
		velocity: 12.0322222222222,
		power: 6731.81706996596,
		road: 6436.97953703704,
		acceleration: 0.3837962962963
	},
	{
		id: 743,
		time: 742,
		velocity: 12.7072222222222,
		power: 8151.43470792954,
		road: 6449.34171296296,
		acceleration: 0.477499999999999
	},
	{
		id: 744,
		time: 743,
		velocity: 13.0633333333333,
		power: 8664.50286590671,
		road: 6462.18583333333,
		acceleration: 0.486388888888889
	},
	{
		id: 745,
		time: 744,
		velocity: 13.4913888888889,
		power: 7748.10471951931,
		road: 6475.46532407407,
		acceleration: 0.38435185185185
	},
	{
		id: 746,
		time: 745,
		velocity: 13.8602777777778,
		power: 7775.3550036791,
		road: 6489.11893518518,
		acceleration: 0.363888888888887
	},
	{
		id: 747,
		time: 746,
		velocity: 14.155,
		power: 7842.09710325168,
		road: 6503.12851851852,
		acceleration: 0.348055555555558
	},
	{
		id: 748,
		time: 747,
		velocity: 14.5355555555556,
		power: 7298.55054853431,
		road: 6517.45717592593,
		acceleration: 0.290092592592593
	},
	{
		id: 749,
		time: 748,
		velocity: 14.7305555555556,
		power: 6281.04503801377,
		road: 6532.03282407407,
		acceleration: 0.203888888888887
	},
	{
		id: 750,
		time: 749,
		velocity: 14.7666666666667,
		power: 5730.29767440414,
		road: 6546.78856481481,
		acceleration: 0.156296296296297
	},
	{
		id: 751,
		time: 750,
		velocity: 15.0044444444444,
		power: 5038.52429468444,
		road: 6561.67342592592,
		acceleration: 0.101944444444445
	},
	{
		id: 752,
		time: 751,
		velocity: 15.0363888888889,
		power: 5486.5604516605,
		road: 6576.67351851852,
		acceleration: 0.128518518518518
	},
	{
		id: 753,
		time: 752,
		velocity: 15.1522222222222,
		power: 5659.8626348185,
		road: 6591.80532407407,
		acceleration: 0.134907407407407
	},
	{
		id: 754,
		time: 753,
		velocity: 15.4091666666667,
		power: 7156.57870337744,
		road: 6607.11925925926,
		acceleration: 0.229351851851851
	},
	{
		id: 755,
		time: 754,
		velocity: 15.7244444444444,
		power: 8134.49103681316,
		road: 6622.68912037037,
		acceleration: 0.282500000000001
	},
	{
		id: 756,
		time: 755,
		velocity: 15.9997222222222,
		power: 7265.41010376333,
		road: 6638.50625,
		acceleration: 0.212037037037039
	},
	{
		id: 757,
		time: 756,
		velocity: 16.0452777777778,
		power: 5029.37942301542,
		road: 6654.45884259259,
		acceleration: 0.0588888888888874
	},
	{
		id: 758,
		time: 757,
		velocity: 15.9011111111111,
		power: 2922.09631873505,
		road: 6670.40138888889,
		acceleration: -0.0789814814814829
	},
	{
		id: 759,
		time: 758,
		velocity: 15.7627777777778,
		power: 3506.79793514401,
		road: 6686.28509259259,
		acceleration: -0.0387037037037032
	},
	{
		id: 760,
		time: 759,
		velocity: 15.9291666666667,
		power: 4119.9417268468,
		road: 6702.15060185185,
		acceleration: 0.00231481481481488
	},
	{
		id: 761,
		time: 760,
		velocity: 15.9080555555556,
		power: 3742.71919233731,
		road: 6718.00615740741,
		acceleration: -0.0222222222222221
	},
	{
		id: 762,
		time: 761,
		velocity: 15.6961111111111,
		power: 3123.72387800612,
		road: 6733.81972222222,
		acceleration: -0.061759259259258
	},
	{
		id: 763,
		time: 762,
		velocity: 15.7438888888889,
		power: 4966.69962306963,
		road: 6749.6325,
		acceleration: 0.0601851851851833
	},
	{
		id: 764,
		time: 763,
		velocity: 16.0886111111111,
		power: 8246.1238434273,
		road: 6765.60962962963,
		acceleration: 0.268518518518517
	},
	{
		id: 765,
		time: 764,
		velocity: 16.5016666666667,
		power: 9872.03604674552,
		road: 6781.89939814815,
		acceleration: 0.35675925925926
	},
	{
		id: 766,
		time: 765,
		velocity: 16.8141666666667,
		power: 8858.08123655227,
		road: 6798.505,
		acceleration: 0.274907407407408
	},
	{
		id: 767,
		time: 766,
		velocity: 16.9133333333333,
		power: 6157.46477555359,
		road: 6815.29657407407,
		acceleration: 0.0970370370370404
	},
	{
		id: 768,
		time: 767,
		velocity: 16.7927777777778,
		power: 4539.09490164284,
		road: 6832.13398148148,
		acceleration: -0.0053703703703718
	},
	{
		id: 769,
		time: 768,
		velocity: 16.7980555555556,
		power: 4862.44155036687,
		road: 6848.97597222222,
		acceleration: 0.0145370370370372
	},
	{
		id: 770,
		time: 769,
		velocity: 16.9569444444444,
		power: 7477.54234954257,
		road: 6865.91148148148,
		acceleration: 0.172499999999999
	},
	{
		id: 771,
		time: 770,
		velocity: 17.3102777777778,
		power: 9280.58036142513,
		road: 6883.06925925926,
		acceleration: 0.272037037037038
	},
	{
		id: 772,
		time: 771,
		velocity: 17.6141666666667,
		power: 9736.26381609365,
		road: 6900.50546296296,
		acceleration: 0.284814814814816
	},
	{
		id: 773,
		time: 772,
		velocity: 17.8113888888889,
		power: 9906.96750084739,
		road: 6918.22402777778,
		acceleration: 0.279907407407407
	},
	{
		id: 774,
		time: 773,
		velocity: 18.15,
		power: 9429.04795045359,
		road: 6936.20175925926,
		acceleration: 0.238425925925927
	},
	{
		id: 775,
		time: 774,
		velocity: 18.3294444444444,
		power: 10231.7250748156,
		road: 6954.43444444444,
		acceleration: 0.271481481481484
	},
	{
		id: 776,
		time: 775,
		velocity: 18.6258333333333,
		power: 12217.3238344745,
		road: 6972.98611111111,
		acceleration: 0.366481481481479
	},
	{
		id: 777,
		time: 776,
		velocity: 19.2494444444444,
		power: 14562.8187165313,
		road: 6991.95671296296,
		acceleration: 0.471388888888889
	},
	{
		id: 778,
		time: 777,
		velocity: 19.7436111111111,
		power: 17256.5481455573,
		road: 7011.45416666666,
		acceleration: 0.582314814814815
	},
	{
		id: 779,
		time: 778,
		velocity: 20.3727777777778,
		power: 16570.8550201715,
		road: 7031.49731481481,
		acceleration: 0.509074074074071
	},
	{
		id: 780,
		time: 779,
		velocity: 20.7766666666667,
		power: 14196.3801271397,
		road: 7051.97486111111,
		acceleration: 0.359722222222224
	},
	{
		id: 781,
		time: 780,
		velocity: 20.8227777777778,
		power: 9994.11349971287,
		road: 7072.69916666666,
		acceleration: 0.133796296296296
	},
	{
		id: 782,
		time: 781,
		velocity: 20.7741666666667,
		power: 6119.55807900206,
		road: 7093.45907407407,
		acceleration: -0.0625925925925941
	},
	{
		id: 783,
		time: 782,
		velocity: 20.5888888888889,
		power: 5909.15115873443,
		road: 7114.15236111111,
		acceleration: -0.0706481481481447
	},
	{
		id: 784,
		time: 783,
		velocity: 20.6108333333333,
		power: 2145.54688923358,
		road: 7134.6824537037,
		acceleration: -0.255740740740745
	},
	{
		id: 785,
		time: 784,
		velocity: 20.0069444444444,
		power: -750.874330982502,
		road: 7154.88662037037,
		acceleration: -0.396111111111111
	},
	{
		id: 786,
		time: 785,
		velocity: 19.4005555555556,
		power: -5186.57024151362,
		road: 7174.58231481481,
		acceleration: -0.620833333333334
	},
	{
		id: 787,
		time: 786,
		velocity: 18.7483333333333,
		power: -6746.09561012373,
		road: 7193.61675925926,
		acceleration: -0.701666666666668
	},
	{
		id: 788,
		time: 787,
		velocity: 17.9019444444444,
		power: -5417.11777923179,
		road: 7211.98754629629,
		acceleration: -0.625648148148144
	},
	{
		id: 789,
		time: 788,
		velocity: 17.5236111111111,
		power: -6136.47427633986,
		road: 7229.71236111111,
		acceleration: -0.666296296296295
	},
	{
		id: 790,
		time: 789,
		velocity: 16.7494444444444,
		power: -3041.358177246,
		road: 7246.86472222222,
		acceleration: -0.478611111111114
	},
	{
		id: 791,
		time: 790,
		velocity: 16.4661111111111,
		power: -1510.88080213906,
		road: 7263.58800925926,
		acceleration: -0.379537037037036
	},
	{
		id: 792,
		time: 791,
		velocity: 16.385,
		power: -762.844187775162,
		road: 7279.95791666667,
		acceleration: -0.327222222222222
	},
	{
		id: 793,
		time: 792,
		velocity: 15.7677777777778,
		power: -940.572881418845,
		road: 7295.99731481481,
		acceleration: -0.333796296296297
	},
	{
		id: 794,
		time: 793,
		velocity: 15.4647222222222,
		power: -3576.29108982545,
		road: 7311.6175462963,
		acceleration: -0.504537037037037
	},
	{
		id: 795,
		time: 794,
		velocity: 14.8713888888889,
		power: -4313.65148675735,
		road: 7326.70800925926,
		acceleration: -0.555
	},
	{
		id: 796,
		time: 795,
		velocity: 14.1027777777778,
		power: -8661.12137121811,
		road: 7341.08356481481,
		acceleration: -0.874814814814814
	},
	{
		id: 797,
		time: 796,
		velocity: 12.8402777777778,
		power: -8704.52079112668,
		road: 7354.56861111111,
		acceleration: -0.906203703703707
	},
	{
		id: 798,
		time: 797,
		velocity: 12.1527777777778,
		power: -9123.73620538513,
		road: 7367.11125,
		acceleration: -0.97861111111111
	},
	{
		id: 799,
		time: 798,
		velocity: 11.1669444444444,
		power: -6288.46126031191,
		road: 7378.77990740741,
		acceleration: -0.769351851851852
	},
	{
		id: 800,
		time: 799,
		velocity: 10.5322222222222,
		power: -8034.15761876603,
		road: 7389.57694444444,
		acceleration: -0.97388888888889
	},
	{
		id: 801,
		time: 800,
		velocity: 9.23111111111111,
		power: -5749.3962576756,
		road: 7399.49125,
		acceleration: -0.791574074074072
	},
	{
		id: 802,
		time: 801,
		velocity: 8.79222222222222,
		power: -5371.2536280711,
		road: 7408.61342592593,
		acceleration: -0.792685185185185
	},
	{
		id: 803,
		time: 802,
		velocity: 8.15416666666667,
		power: -3238.55103054559,
		road: 7417.05393518518,
		acceleration: -0.57064814814815
	},
	{
		id: 804,
		time: 803,
		velocity: 7.51916666666667,
		power: -3352.94649968945,
		road: 7424.90361111111,
		acceleration: -0.611018518518518
	},
	{
		id: 805,
		time: 804,
		velocity: 6.95916666666667,
		power: -5113.01528020007,
		road: 7431.99064814815,
		acceleration: -0.914259259259259
	},
	{
		id: 806,
		time: 805,
		velocity: 5.41138888888889,
		power: -4505.55080348614,
		road: 7438.16212962963,
		acceleration: -0.916851851851852
	},
	{
		id: 807,
		time: 806,
		velocity: 4.76861111111111,
		power: -6155.0052701087,
		road: 7443.15564814815,
		acceleration: -1.43907407407407
	},
	{
		id: 808,
		time: 807,
		velocity: 2.64194444444444,
		power: -4284.88884943834,
		road: 7446.73097222222,
		acceleration: -1.39731481481481
	},
	{
		id: 809,
		time: 808,
		velocity: 1.21944444444444,
		power: -2881.67998082575,
		road: 7448.81287037037,
		acceleration: -1.58953703703704
	},
	{
		id: 810,
		time: 809,
		velocity: 0,
		power: -604.103169006468,
		road: 7449.65967592593,
		acceleration: -0.880648148148148
	},
	{
		id: 811,
		time: 810,
		velocity: 0,
		power: -53.7103948992852,
		road: 7449.86291666667,
		acceleration: -0.406481481481481
	},
	{
		id: 812,
		time: 811,
		velocity: 0,
		power: 0,
		road: 7449.86291666667,
		acceleration: 0
	},
	{
		id: 813,
		time: 812,
		velocity: 0,
		power: 2.32955870186785,
		road: 7449.87842592593,
		acceleration: 0.0310185185185185
	},
	{
		id: 814,
		time: 813,
		velocity: 0.0930555555555556,
		power: 3.74760830691958,
		road: 7449.90944444444,
		acceleration: 0
	},
	{
		id: 815,
		time: 814,
		velocity: 0,
		power: 45.6635011631071,
		road: 7450.04916666667,
		acceleration: 0.217407407407407
	},
	{
		id: 816,
		time: 815,
		velocity: 0.652222222222222,
		power: 373.818633219667,
		road: 7450.5950462963,
		acceleration: 0.594907407407407
	},
	{
		id: 817,
		time: 816,
		velocity: 1.87777777777778,
		power: 2017.09830037606,
		road: 7452.08689814815,
		acceleration: 1.29703703703704
	},
	{
		id: 818,
		time: 817,
		velocity: 3.89111111111111,
		power: 4098.33257766739,
		road: 7454.92268518518,
		acceleration: 1.39083333333333
	},
	{
		id: 819,
		time: 818,
		velocity: 4.82472222222222,
		power: 4812.71133961748,
		road: 7459.00601851852,
		acceleration: 1.10425925925926
	},
	{
		id: 820,
		time: 819,
		velocity: 5.19055555555556,
		power: 2794.05386062299,
		road: 7463.87324074074,
		acceleration: 0.463518518518519
	},
	{
		id: 821,
		time: 820,
		velocity: 5.28166666666667,
		power: 2167.55945907671,
		road: 7469.11824074074,
		acceleration: 0.292037037037036
	},
	{
		id: 822,
		time: 821,
		velocity: 5.70083333333333,
		power: 1287.44154923921,
		road: 7474.56166666667,
		acceleration: 0.104814814814815
	},
	{
		id: 823,
		time: 822,
		velocity: 5.505,
		power: 474.815812833603,
		road: 7480.03106481481,
		acceleration: -0.0528703703703703
	},
	{
		id: 824,
		time: 823,
		velocity: 5.12305555555556,
		power: -858.215906672228,
		road: 7485.31703703704,
		acceleration: -0.313981481481481
	},
	{
		id: 825,
		time: 824,
		velocity: 4.75888888888889,
		power: -250.255211243093,
		road: 7490.34902777778,
		acceleration: -0.193981481481482
	},
	{
		id: 826,
		time: 825,
		velocity: 4.92305555555556,
		power: -183.921588242066,
		road: 7495.19375,
		acceleration: -0.180555555555555
	},
	{
		id: 827,
		time: 826,
		velocity: 4.58138888888889,
		power: 201.36807387548,
		road: 7499.90078703704,
		acceleration: -0.0948148148148151
	},
	{
		id: 828,
		time: 827,
		velocity: 4.47444444444444,
		power: 886.067328620849,
		road: 7504.59,
		acceleration: 0.059166666666667
	},
	{
		id: 829,
		time: 828,
		velocity: 5.10055555555556,
		power: 2027.80485650296,
		road: 7509.45768518518,
		acceleration: 0.297777777777778
	},
	{
		id: 830,
		time: 829,
		velocity: 5.47472222222222,
		power: 3842.92276952414,
		road: 7514.78231481481,
		acceleration: 0.616111111111111
	},
	{
		id: 831,
		time: 830,
		velocity: 6.32277777777778,
		power: 2935.43221284143,
		road: 7520.60685185185,
		acceleration: 0.383703703703705
	},
	{
		id: 832,
		time: 831,
		velocity: 6.25166666666667,
		power: 590.00553119178,
		road: 7526.6012037037,
		acceleration: -0.044074074074075
	},
	{
		id: 833,
		time: 832,
		velocity: 5.3425,
		power: -5107.6944052185,
		road: 7532.00412037037,
		acceleration: -1.1387962962963
	},
	{
		id: 834,
		time: 833,
		velocity: 2.90638888888889,
		power: -6041.88493099991,
		road: 7535.96634259259,
		acceleration: -1.74259259259259
	},
	{
		id: 835,
		time: 834,
		velocity: 1.02388888888889,
		power: -3444.5795283834,
		road: 7538.16685185185,
		acceleration: -1.78083333333333
	},
	{
		id: 836,
		time: 835,
		velocity: 0,
		power: -658.017277968824,
		road: 7538.9925462963,
		acceleration: -0.968796296296296
	},
	{
		id: 837,
		time: 836,
		velocity: 0,
		power: -34.5588845679012,
		road: 7539.16319444444,
		acceleration: -0.341296296296296
	},
	{
		id: 838,
		time: 837,
		velocity: 0,
		power: 0,
		road: 7539.16319444444,
		acceleration: 0
	},
	{
		id: 839,
		time: 838,
		velocity: 0,
		power: 0,
		road: 7539.16319444444,
		acceleration: 0
	},
	{
		id: 840,
		time: 839,
		velocity: 0,
		power: 4.44654099655211,
		road: 7539.18930555555,
		acceleration: 0.0522222222222222
	},
	{
		id: 841,
		time: 840,
		velocity: 0.156666666666667,
		power: 6.30945626211006,
		road: 7539.24152777778,
		acceleration: 0
	},
	{
		id: 842,
		time: 841,
		velocity: 0,
		power: 6.30945626211006,
		road: 7539.29375,
		acceleration: 0
	},
	{
		id: 843,
		time: 842,
		velocity: 0,
		power: 1.86287660818713,
		road: 7539.31986111111,
		acceleration: -0.0522222222222222
	},
	{
		id: 844,
		time: 843,
		velocity: 0,
		power: 0,
		road: 7539.31986111111,
		acceleration: 0
	},
	{
		id: 845,
		time: 844,
		velocity: 0,
		power: 107.637754609355,
		road: 7539.52842592593,
		acceleration: 0.41712962962963
	},
	{
		id: 846,
		time: 845,
		velocity: 1.25138888888889,
		power: 938.563073490365,
		road: 7540.43041666667,
		acceleration: 0.969722222222222
	},
	{
		id: 847,
		time: 846,
		velocity: 2.90916666666667,
		power: 2568.08529946995,
		road: 7542.42953703704,
		acceleration: 1.22453703703704
	},
	{
		id: 848,
		time: 847,
		velocity: 3.67361111111111,
		power: 3485.90721981961,
		road: 7545.56087962963,
		acceleration: 1.03990740740741
	},
	{
		id: 849,
		time: 848,
		velocity: 4.37111111111111,
		power: 2537.49174905678,
		road: 7549.48467592593,
		acceleration: 0.544999999999999
	},
	{
		id: 850,
		time: 849,
		velocity: 4.54416666666667,
		power: 1310.60157558198,
		road: 7553.77300925926,
		acceleration: 0.184074074074075
	},
	{
		id: 851,
		time: 850,
		velocity: 4.22583333333333,
		power: 1415.84712030683,
		road: 7558.25050925926,
		acceleration: 0.194259259259258
	},
	{
		id: 852,
		time: 851,
		velocity: 4.95388888888889,
		power: 2235.05066980372,
		road: 7563.00263888889,
		acceleration: 0.355
	},
	{
		id: 853,
		time: 852,
		velocity: 5.60916666666667,
		power: 4567.40530824332,
		road: 7568.31314814815,
		acceleration: 0.761759259259259
	},
	{
		id: 854,
		time: 853,
		velocity: 6.51111111111111,
		power: 4633.39192373241,
		road: 7574.33527777778,
		acceleration: 0.661481481481482
	},
	{
		id: 855,
		time: 854,
		velocity: 6.93833333333333,
		power: 5961.8575830758,
		road: 7581.07657407407,
		acceleration: 0.776851851851852
	},
	{
		id: 856,
		time: 855,
		velocity: 7.93972222222222,
		power: 7053.78960851615,
		road: 7588.61805555556,
		acceleration: 0.823518518518517
	},
	{
		id: 857,
		time: 856,
		velocity: 8.98166666666667,
		power: 7707.11554154995,
		road: 7596.9724537037,
		acceleration: 0.802314814814815
	},
	{
		id: 858,
		time: 857,
		velocity: 9.34527777777778,
		power: 7076.94632654138,
		road: 7606.05037037037,
		acceleration: 0.644722222222224
	},
	{
		id: 859,
		time: 858,
		velocity: 9.87388888888889,
		power: 5023.43368930735,
		road: 7615.63611111111,
		acceleration: 0.370925925925924
	},
	{
		id: 860,
		time: 859,
		velocity: 10.0944444444444,
		power: 4171.44215338325,
		road: 7625.53708333333,
		acceleration: 0.259537037037038
	},
	{
		id: 861,
		time: 860,
		velocity: 10.1238888888889,
		power: 477.755362321645,
		road: 7635.5012962963,
		acceleration: -0.133055555555556
	},
	{
		id: 862,
		time: 861,
		velocity: 9.47472222222222,
		power: -5213.30898763751,
		road: 7645.02226851852,
		acceleration: -0.753425925925926
	},
	{
		id: 863,
		time: 862,
		velocity: 7.83416666666667,
		power: -8241.11030708036,
		road: 7653.57611111111,
		acceleration: -1.18083333333333
	},
	{
		id: 864,
		time: 863,
		velocity: 6.58138888888889,
		power: -9147.8371666648,
		road: 7660.7949537037,
		acceleration: -1.48916666666667
	},
	{
		id: 865,
		time: 864,
		velocity: 5.00722222222222,
		power: -5333.77663264677,
		road: 7666.72222222222,
		acceleration: -1.09398148148148
	},
	{
		id: 866,
		time: 865,
		velocity: 4.55222222222222,
		power: -2746.94132935422,
		road: 7671.7437962963,
		acceleration: -0.717407407407407
	},
	{
		id: 867,
		time: 866,
		velocity: 4.42916666666667,
		power: -361.99005493262,
		road: 7676.29527777778,
		acceleration: -0.222777777777778
	},
	{
		id: 868,
		time: 867,
		velocity: 4.33888888888889,
		power: -332.172438063767,
		road: 7680.62601851852,
		acceleration: -0.218703703703704
	},
	{
		id: 869,
		time: 868,
		velocity: 3.89611111111111,
		power: -376.35250417177,
		road: 7684.73069444445,
		acceleration: -0.233425925925926
	},
	{
		id: 870,
		time: 869,
		velocity: 3.72888888888889,
		power: 667.445664668754,
		road: 7688.73814814815,
		acceleration: 0.0389814814814815
	},
	{
		id: 871,
		time: 870,
		velocity: 4.45583333333333,
		power: 2522.68766849167,
		road: 7693.00736111111,
		acceleration: 0.484537037037037
	},
	{
		id: 872,
		time: 871,
		velocity: 5.34972222222222,
		power: 3805.7241053101,
		road: 7697.86115740741,
		acceleration: 0.68462962962963
	},
	{
		id: 873,
		time: 872,
		velocity: 5.78277777777778,
		power: 3066.45227411746,
		road: 7703.28287037037,
		acceleration: 0.451203703703704
	},
	{
		id: 874,
		time: 873,
		velocity: 5.80944444444444,
		power: 3713.309618702,
		road: 7709.18745370371,
		acceleration: 0.514537037037037
	},
	{
		id: 875,
		time: 874,
		velocity: 6.89333333333333,
		power: 4608.71446852267,
		road: 7715.64888888889,
		acceleration: 0.599166666666666
	},
	{
		id: 876,
		time: 875,
		velocity: 7.58027777777778,
		power: 7177.47586027806,
		road: 7722.8550462963,
		acceleration: 0.890277777777778
	},
	{
		id: 877,
		time: 876,
		velocity: 8.48027777777778,
		power: 6365.54628141212,
		road: 7730.84328703704,
		acceleration: 0.673888888888889
	},
	{
		id: 878,
		time: 877,
		velocity: 8.915,
		power: 6611.31935554742,
		road: 7739.48546296296,
		acceleration: 0.633981481481483
	},
	{
		id: 879,
		time: 878,
		velocity: 9.48222222222222,
		power: 5723.52969215346,
		road: 7748.68375,
		acceleration: 0.478240740740739
	},
	{
		id: 880,
		time: 879,
		velocity: 9.915,
		power: 4086.044335759,
		road: 7758.25569444445,
		acceleration: 0.269074074074073
	},
	{
		id: 881,
		time: 880,
		velocity: 9.72222222222222,
		power: 827.684856323779,
		road: 7767.91712962963,
		acceleration: -0.0900925925925939
	},
	{
		id: 882,
		time: 881,
		velocity: 9.21194444444444,
		power: -2343.8076589006,
		road: 7777.31398148148,
		acceleration: -0.439074074074073
	},
	{
		id: 883,
		time: 882,
		velocity: 8.59777777777778,
		power: -3361.38647498133,
		road: 7786.20680555556,
		acceleration: -0.568981481481481
	},
	{
		id: 884,
		time: 883,
		velocity: 8.01527777777778,
		power: -3036.47466884768,
		road: 7794.54046296296,
		acceleration: -0.549351851851851
	},
	{
		id: 885,
		time: 884,
		velocity: 7.56388888888889,
		power: -2487.6073081141,
		road: 7802.35120370371,
		acceleration: -0.496481481481482
	},
	{
		id: 886,
		time: 885,
		velocity: 7.10833333333333,
		power: -2666.94879077349,
		road: 7809.64277777778,
		acceleration: -0.541851851851852
	},
	{
		id: 887,
		time: 886,
		velocity: 6.38972222222222,
		power: -1640.91081765181,
		road: 7816.46009259259,
		acceleration: -0.406666666666667
	},
	{
		id: 888,
		time: 887,
		velocity: 6.34388888888889,
		power: -914.01340112955,
		road: 7822.92425925926,
		acceleration: -0.299629629629629
	},
	{
		id: 889,
		time: 888,
		velocity: 6.20944444444444,
		power: 550.597425361148,
		road: 7829.20986111111,
		acceleration: -0.057500000000001
	},
	{
		id: 890,
		time: 889,
		velocity: 6.21722222222222,
		power: 1132.45078266158,
		road: 7835.48680555556,
		acceleration: 0.0401851851851855
	},
	{
		id: 891,
		time: 890,
		velocity: 6.46444444444444,
		power: 1342.50771728363,
		road: 7841.82032407408,
		acceleration: 0.0729629629629631
	},
	{
		id: 892,
		time: 891,
		velocity: 6.42833333333333,
		power: 1308.27165678379,
		road: 7848.2225462963,
		acceleration: 0.0644444444444447
	},
	{
		id: 893,
		time: 892,
		velocity: 6.41055555555555,
		power: -358.050774578016,
		road: 7854.55226851852,
		acceleration: -0.209444444444446
	},
	{
		id: 894,
		time: 893,
		velocity: 5.83611111111111,
		power: -1414.02163503307,
		road: 7860.5799537037,
		acceleration: -0.394629629629629
	},
	{
		id: 895,
		time: 894,
		velocity: 5.24444444444444,
		power: -2075.50994891531,
		road: 7866.14157407407,
		acceleration: -0.5375
	},
	{
		id: 896,
		time: 895,
		velocity: 4.79805555555556,
		power: -500.262303323351,
		road: 7871.31231481482,
		acceleration: -0.24425925925926
	},
	{
		id: 897,
		time: 896,
		velocity: 5.10333333333333,
		power: 1323.89772413763,
		road: 7876.42611111111,
		acceleration: 0.130370370370371
	},
	{
		id: 898,
		time: 897,
		velocity: 5.63555555555556,
		power: 3058.21055206716,
		road: 7881.83087962963,
		acceleration: 0.451574074074074
	},
	{
		id: 899,
		time: 898,
		velocity: 6.15277777777778,
		power: 3522.40845783441,
		road: 7887.70351851852,
		acceleration: 0.484166666666667
	},
	{
		id: 900,
		time: 899,
		velocity: 6.55583333333333,
		power: 5913.44427384344,
		road: 7894.21972222222,
		acceleration: 0.802962962962963
	},
	{
		id: 901,
		time: 900,
		velocity: 8.04444444444444,
		power: 6800.72691645392,
		road: 7901.54638888889,
		acceleration: 0.817962962962963
	},
	{
		id: 902,
		time: 901,
		velocity: 8.60666666666667,
		power: 8465.1209492634,
		road: 7909.74194444445,
		acceleration: 0.919814814814814
	},
	{
		id: 903,
		time: 902,
		velocity: 9.31527777777778,
		power: 6761.31630707158,
		road: 7918.70703703704,
		acceleration: 0.619259259259261
	},
	{
		id: 904,
		time: 903,
		velocity: 9.90222222222222,
		power: 8347.92332669763,
		road: 7928.34638888889,
		acceleration: 0.729259259259257
	},
	{
		id: 905,
		time: 904,
		velocity: 10.7944444444444,
		power: 7099.37048779166,
		road: 7938.61949074074,
		acceleration: 0.538240740740742
	},
	{
		id: 906,
		time: 905,
		velocity: 10.93,
		power: 4679.36794739626,
		road: 7949.29578703704,
		acceleration: 0.268148148148148
	},
	{
		id: 907,
		time: 906,
		velocity: 10.7066666666667,
		power: 1393.01281743252,
		road: 7960.07745370371,
		acceleration: -0.0574074074074069
	},
	{
		id: 908,
		time: 907,
		velocity: 10.6222222222222,
		power: 1572.15460564159,
		road: 7970.81106481482,
		acceleration: -0.038703703703705
	},
	{
		id: 909,
		time: 908,
		velocity: 10.8138888888889,
		power: 2717.26915481172,
		road: 7981.56162037037,
		acceleration: 0.0725925925925939
	},
	{
		id: 910,
		time: 909,
		velocity: 10.9244444444444,
		power: 3164.96718217037,
		road: 7992.40472222222,
		acceleration: 0.112500000000001
	},
	{
		id: 911,
		time: 910,
		velocity: 10.9597222222222,
		power: 4246.14469707735,
		road: 8003.40861111111,
		acceleration: 0.209074074074074
	},
	{
		id: 912,
		time: 911,
		velocity: 11.4411111111111,
		power: 4381.73787492747,
		road: 8014.62277777778,
		acceleration: 0.211481481481481
	},
	{
		id: 913,
		time: 912,
		velocity: 11.5588888888889,
		power: 4399.27992174287,
		road: 8026.04416666667,
		acceleration: 0.202962962962962
	},
	{
		id: 914,
		time: 913,
		velocity: 11.5686111111111,
		power: 2087.48395234116,
		road: 8037.56097222222,
		acceleration: -0.0121296296296283
	},
	{
		id: 915,
		time: 914,
		velocity: 11.4047222222222,
		power: 1501.40758630836,
		road: 8049.03944444445,
		acceleration: -0.0645370370370397
	},
	{
		id: 916,
		time: 915,
		velocity: 11.3652777777778,
		power: 1414.47616928361,
		road: 8060.45023148148,
		acceleration: -0.0708333333333311
	},
	{
		id: 917,
		time: 916,
		velocity: 11.3561111111111,
		power: 1710.33512799675,
		road: 8071.80453703704,
		acceleration: -0.0421296296296312
	},
	{
		id: 918,
		time: 917,
		velocity: 11.2783333333333,
		power: 2506.39644866277,
		road: 8083.15356481482,
		acceleration: 0.031574074074074
	},
	{
		id: 919,
		time: 918,
		velocity: 11.46,
		power: 3042.97652860835,
		road: 8094.55791666667,
		acceleration: 0.0790740740740734
	},
	{
		id: 920,
		time: 919,
		velocity: 11.5933333333333,
		power: 4075.10137582822,
		road: 8106.08597222222,
		acceleration: 0.168333333333335
	},
	{
		id: 921,
		time: 920,
		velocity: 11.7833333333333,
		power: 2693.11961363345,
		road: 8117.71773148148,
		acceleration: 0.0390740740740743
	},
	{
		id: 922,
		time: 921,
		velocity: 11.5772222222222,
		power: 2242.31487501608,
		road: 8129.36796296296,
		acceleration: -0.00212962962963026
	},
	{
		id: 923,
		time: 922,
		velocity: 11.5869444444444,
		power: 1544.81297796345,
		road: 8140.98509259259,
		acceleration: -0.0640740740740728
	},
	{
		id: 924,
		time: 923,
		velocity: 11.5911111111111,
		power: 2242.74953723512,
		road: 8152.57013888889,
		acceleration: -9.25925925940874E-05
	},
	{
		id: 925,
		time: 924,
		velocity: 11.5769444444444,
		power: 2048.63095401615,
		road: 8164.14643518519,
		acceleration: -0.0174074074074078
	},
	{
		id: 926,
		time: 925,
		velocity: 11.5347222222222,
		power: 1574.78004653587,
		road: 8175.68435185185,
		acceleration: -0.0593518518518508
	},
	{
		id: 927,
		time: 926,
		velocity: 11.4130555555556,
		power: 1841.7892441076,
		road: 8187.17569444445,
		acceleration: -0.0337962962962965
	},
	{
		id: 928,
		time: 927,
		velocity: 11.4755555555556,
		power: 3410.52086680997,
		road: 8198.70407407408,
		acceleration: 0.107870370370371
	},
	{
		id: 929,
		time: 928,
		velocity: 11.8583333333333,
		power: 4205.6395597335,
		road: 8210.37324074074,
		acceleration: 0.173703703703701
	},
	{
		id: 930,
		time: 929,
		velocity: 11.9341666666667,
		power: 5112.33570227227,
		road: 8222.25138888889,
		acceleration: 0.244259259259261
	},
	{
		id: 931,
		time: 930,
		velocity: 12.2083333333333,
		power: 4939.35758362502,
		road: 8234.36041666667,
		acceleration: 0.217500000000001
	},
	{
		id: 932,
		time: 931,
		velocity: 12.5108333333333,
		power: 5704.6279687438,
		road: 8246.71347222222,
		acceleration: 0.270555555555557
	},
	{
		id: 933,
		time: 932,
		velocity: 12.7458333333333,
		power: 4649.11638119287,
		road: 8259.28726851852,
		acceleration: 0.170925925925923
	},
	{
		id: 934,
		time: 933,
		velocity: 12.7211111111111,
		power: 3681.92724598592,
		road: 8271.98921296296,
		acceleration: 0.0853703703703701
	},
	{
		id: 935,
		time: 934,
		velocity: 12.7669444444444,
		power: 3749.47222337905,
		road: 8284.77763888889,
		acceleration: 0.0875925925925944
	},
	{
		id: 936,
		time: 935,
		velocity: 13.0086111111111,
		power: 5246.07135577487,
		road: 8297.71148148148,
		acceleration: 0.203240740740739
	},
	{
		id: 937,
		time: 936,
		velocity: 13.3308333333333,
		power: 7653.61526899044,
		road: 8310.93703703704,
		acceleration: 0.380185185185184
	},
	{
		id: 938,
		time: 937,
		velocity: 13.9075,
		power: 9228.57858912459,
		road: 8324.59037037037,
		acceleration: 0.475370370370374
	},
	{
		id: 939,
		time: 938,
		velocity: 14.4347222222222,
		power: 12015.8479041525,
		road: 8338.80328703704,
		acceleration: 0.643796296296296
	},
	{
		id: 940,
		time: 939,
		velocity: 15.2622222222222,
		power: 11794.602176549,
		road: 8353.62875,
		acceleration: 0.581296296296294
	},
	{
		id: 941,
		time: 940,
		velocity: 15.6513888888889,
		power: 9325.4560636765,
		road: 8368.93388888889,
		acceleration: 0.378055555555557
	},
	{
		id: 942,
		time: 941,
		velocity: 15.5688888888889,
		power: 4781.53011884004,
		road: 8384.4575462963,
		acceleration: 0.0589814814814815
	},
	{
		id: 943,
		time: 942,
		velocity: 15.4391666666667,
		power: 4173.73827135996,
		road: 8400.01902777778,
		acceleration: 0.0166666666666657
	},
	{
		id: 944,
		time: 943,
		velocity: 15.7013888888889,
		power: 7160.96338346562,
		road: 8415.69481481482,
		acceleration: 0.211944444444445
	},
	{
		id: 945,
		time: 944,
		velocity: 16.2047222222222,
		power: 9937.01011299417,
		road: 8431.66634259259,
		acceleration: 0.379537037037037
	},
	{
		id: 946,
		time: 945,
		velocity: 16.5777777777778,
		power: 9604.47727016528,
		road: 8447.9962962963,
		acceleration: 0.337314814814814
	},
	{
		id: 947,
		time: 946,
		velocity: 16.7133333333333,
		power: 4480.43107771199,
		road: 8464.49643518519,
		acceleration: 0.00305555555555515
	},
	{
		id: 948,
		time: 947,
		velocity: 16.2138888888889,
		power: 562.277854917424,
		road: 8480.87671296297,
		acceleration: -0.242777777777778
	},
	{
		id: 949,
		time: 948,
		velocity: 15.8494444444444,
		power: -3842.50442128151,
		road: 8496.87412037037,
		acceleration: -0.522962962962961
	},
	{
		id: 950,
		time: 949,
		velocity: 15.1444444444444,
		power: -15099.5148715011,
		road: 8511.95828703704,
		acceleration: -1.30351851851852
	},
	{
		id: 951,
		time: 950,
		velocity: 12.3033333333333,
		power: -31470.8893501544,
		road: 8525.01578703704,
		acceleration: -2.74981481481482
	},
	{
		id: 952,
		time: 951,
		velocity: 7.6,
		power: -33746.3956051191,
		road: 8534.79569444445,
		acceleration: -3.80537037037037
	},
	{
		id: 953,
		time: 952,
		velocity: 3.72833333333333,
		power: -20126.6329731715,
		road: 8540.84930555556,
		acceleration: -3.64722222222222
	},
	{
		id: 954,
		time: 953,
		velocity: 1.36166666666667,
		power: -6215.68955103361,
		road: 8543.95972222223,
		acceleration: -2.23916666666667
	},
	{
		id: 955,
		time: 954,
		velocity: 0.8825,
		power: -1446.47123411539,
		road: 8545.32916666667,
		acceleration: -1.24277777777778
	},
	{
		id: 956,
		time: 955,
		velocity: 0,
		power: -161.093750290876,
		road: 8545.85027777778,
		acceleration: -0.453888888888889
	},
	{
		id: 957,
		time: 956,
		velocity: 0,
		power: -23.2195039473684,
		road: 8545.99736111111,
		acceleration: -0.294166666666667
	},
	{
		id: 958,
		time: 957,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 959,
		time: 958,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 960,
		time: 959,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 961,
		time: 960,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 962,
		time: 961,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 963,
		time: 962,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 964,
		time: 963,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 965,
		time: 964,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 966,
		time: 965,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 967,
		time: 966,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 968,
		time: 967,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 969,
		time: 968,
		velocity: 0,
		power: 0,
		road: 8545.99736111111,
		acceleration: 0
	},
	{
		id: 970,
		time: 969,
		velocity: 0,
		power: 90.5156329164993,
		road: 8546.1863425926,
		acceleration: 0.377962962962963
	},
	{
		id: 971,
		time: 970,
		velocity: 1.13388888888889,
		power: 1106.54010787973,
		road: 8547.12324074074,
		acceleration: 1.11787037037037
	},
	{
		id: 972,
		time: 971,
		velocity: 3.35361111111111,
		power: 3140.13266057807,
		road: 8549.31060185186,
		acceleration: 1.38305555555556
	},
	{
		id: 973,
		time: 972,
		velocity: 4.14916666666667,
		power: 5414.54915143303,
		road: 8552.91351851852,
		acceleration: 1.44805555555555
	},
	{
		id: 974,
		time: 973,
		velocity: 5.47805555555556,
		power: 3836.19159752977,
		road: 8557.6012962963,
		acceleration: 0.721666666666667
	},
	{
		id: 975,
		time: 974,
		velocity: 5.51861111111111,
		power: 6067.97304855786,
		road: 8563.1525462963,
		acceleration: 1.00527777777778
	},
	{
		id: 976,
		time: 975,
		velocity: 7.165,
		power: 8642.71929200356,
		road: 8569.81245370371,
		acceleration: 1.21203703703704
	},
	{
		id: 977,
		time: 976,
		velocity: 9.11416666666667,
		power: 12769.1609627482,
		road: 8577.83282407408,
		acceleration: 1.50888888888889
	},
	{
		id: 978,
		time: 977,
		velocity: 10.0452777777778,
		power: 14911.5563248084,
		road: 8587.34148148148,
		acceleration: 1.46768518518519
	},
	{
		id: 979,
		time: 978,
		velocity: 11.5680555555556,
		power: 10522.9845057268,
		road: 8598.00578703704,
		acceleration: 0.843611111111111
	},
	{
		id: 980,
		time: 979,
		velocity: 11.645,
		power: 8812.40344917345,
		road: 8609.39722222223,
		acceleration: 0.610648148148149
	},
	{
		id: 981,
		time: 980,
		velocity: 11.8772222222222,
		power: 5121.63852346925,
		road: 8621.21800925926,
		acceleration: 0.248055555555554
	},
	{
		id: 982,
		time: 981,
		velocity: 12.3122222222222,
		power: 9661.37856017103,
		road: 8633.4700462963,
		acceleration: 0.614444444444445
	},
	{
		id: 983,
		time: 982,
		velocity: 13.4883333333333,
		power: 11121.8555640443,
		road: 8646.37032407408,
		acceleration: 0.682037037037039
	},
	{
		id: 984,
		time: 983,
		velocity: 13.9233333333333,
		power: 13005.8138451129,
		road: 8659.99541666667,
		acceleration: 0.767592592592592
	},
	{
		id: 985,
		time: 984,
		velocity: 14.615,
		power: 10801.5477383588,
		road: 8674.27893518519,
		acceleration: 0.549259259259257
	},
	{
		id: 986,
		time: 985,
		velocity: 15.1361111111111,
		power: 12304.2516170394,
		road: 8689.14421296297,
		acceleration: 0.614259259259258
	},
	{
		id: 987,
		time: 986,
		velocity: 15.7661111111111,
		power: 8360.32151593405,
		road: 8704.47203703704,
		acceleration: 0.310833333333335
	},
	{
		id: 988,
		time: 987,
		velocity: 15.5475,
		power: 6371.69463550696,
		road: 8720.03740740741,
		acceleration: 0.164259259259259
	},
	{
		id: 989,
		time: 988,
		velocity: 15.6288888888889,
		power: 5401.92658930496,
		road: 8735.73180555556,
		acceleration: 0.093796296296297
	},
	{
		id: 990,
		time: 989,
		velocity: 16.0475,
		power: 10253.6355434372,
		road: 8751.67412037037,
		acceleration: 0.402037037037038
	},
	{
		id: 991,
		time: 990,
		velocity: 16.7536111111111,
		power: 13485.9938457219,
		road: 8768.10671296297,
		acceleration: 0.578518518518518
	},
	{
		id: 992,
		time: 991,
		velocity: 17.3644444444444,
		power: 14643.8244388572,
		road: 8785.13268518519,
		acceleration: 0.60824074074074
	},
	{
		id: 993,
		time: 992,
		velocity: 17.8722222222222,
		power: 12934.0277878472,
		road: 8802.69689814815,
		acceleration: 0.468240740740743
	},
	{
		id: 994,
		time: 993,
		velocity: 18.1583333333333,
		power: 9019.90492794019,
		road: 8820.60430555556,
		acceleration: 0.218148148148146
	},
	{
		id: 995,
		time: 994,
		velocity: 18.0188888888889,
		power: 7260.5565614953,
		road: 8838.67495370371,
		acceleration: 0.108333333333334
	},
	{
		id: 996,
		time: 995,
		velocity: 18.1972222222222,
		power: 2810.88843470639,
		road: 8856.72560185185,
		acceleration: -0.148333333333333
	},
	{
		id: 997,
		time: 996,
		velocity: 17.7133333333333,
		power: 3111.42619095595,
		road: 8874.63870370371,
		acceleration: -0.126759259259259
	},
	{
		id: 998,
		time: 997,
		velocity: 17.6386111111111,
		power: 2306.05711915109,
		road: 8892.40361111111,
		acceleration: -0.169629629629632
	},
	{
		id: 999,
		time: 998,
		velocity: 17.6883333333333,
		power: 6194.5140500211,
		road: 8910.11435185185,
		acceleration: 0.0612962962962982
	},
	{
		id: 1000,
		time: 999,
		velocity: 17.8972222222222,
		power: 8700.72622984742,
		road: 8927.95708333333,
		acceleration: 0.202685185185185
	},
	{
		id: 1001,
		time: 1000,
		velocity: 18.2466666666667,
		power: 9701.44366513865,
		road: 8946.02592592593,
		acceleration: 0.249537037037037
	},
	{
		id: 1002,
		time: 1001,
		velocity: 18.4369444444444,
		power: 8112.19329713778,
		road: 8964.29375,
		acceleration: 0.148425925925931
	},
	{
		id: 1003,
		time: 1002,
		velocity: 18.3425,
		power: 5195.08555529093,
		road: 8982.62541666667,
		acceleration: -0.0207407407407452
	},
	{
		id: 1004,
		time: 1003,
		velocity: 18.1844444444444,
		power: 4174.89527542511,
		road: 9000.90810185185,
		acceleration: -0.0772222222222219
	},
	{
		id: 1005,
		time: 1004,
		velocity: 18.2052777777778,
		power: 5431.57718379722,
		road: 9019.15023148148,
		acceleration: -0.00388888888888772
	},
	{
		id: 1006,
		time: 1005,
		velocity: 18.3308333333333,
		power: 8203.03962115227,
		road: 9037.46611111111,
		acceleration: 0.151388888888889
	},
	{
		id: 1007,
		time: 1006,
		velocity: 18.6386111111111,
		power: 9152.13904623959,
		road: 9055.95615740741,
		acceleration: 0.196944444444441
	},
	{
		id: 1008,
		time: 1007,
		velocity: 18.7961111111111,
		power: 9635.09524531672,
		road: 9074.65162037037,
		acceleration: 0.213888888888892
	},
	{
		id: 1009,
		time: 1008,
		velocity: 18.9725,
		power: 8082.88631210118,
		road: 9093.51384259259,
		acceleration: 0.119629629629628
	},
	{
		id: 1010,
		time: 1009,
		velocity: 18.9975,
		power: 7285.26928597736,
		road: 9112.47157407408,
		acceleration: 0.0713888888888867
	},
	{
		id: 1011,
		time: 1010,
		velocity: 19.0102777777778,
		power: 6352.84772323856,
		road: 9131.47412037037,
		acceleration: 0.0182407407407439
	},
	{
		id: 1012,
		time: 1011,
		velocity: 19.0272222222222,
		power: 6233.6673820525,
		road: 9150.4913425926,
		acceleration: 0.0111111111111093
	},
	{
		id: 1013,
		time: 1012,
		velocity: 19.0308333333333,
		power: 5968.18557920077,
		road: 9169.51231481482,
		acceleration: -0.00361111111111256
	},
	{
		id: 1014,
		time: 1013,
		velocity: 18.9994444444444,
		power: 4704.40956402627,
		road: 9188.49564814815,
		acceleration: -0.0716666666666619
	},
	{
		id: 1015,
		time: 1014,
		velocity: 18.8122222222222,
		power: 4091.12177195921,
		road: 9207.39189814815,
		acceleration: -0.102500000000003
	},
	{
		id: 1016,
		time: 1015,
		velocity: 18.7233333333333,
		power: 4341.44573479012,
		road: 9226.19416666667,
		acceleration: -0.0854629629629642
	},
	{
		id: 1017,
		time: 1016,
		velocity: 18.7430555555556,
		power: 4343.21266459826,
		road: 9244.91245370371,
		acceleration: -0.0824999999999996
	},
	{
		id: 1018,
		time: 1017,
		velocity: 18.5647222222222,
		power: 4250.95905052486,
		road: 9263.54708333333,
		acceleration: -0.0848148148148127
	},
	{
		id: 1019,
		time: 1018,
		velocity: 18.4688888888889,
		power: 3199.81205345024,
		road: 9282.06916666667,
		acceleration: -0.140277777777779
	},
	{
		id: 1020,
		time: 1019,
		velocity: 18.3222222222222,
		power: 4475.05602595335,
		road: 9300.4887037037,
		acceleration: -0.0648148148148167
	},
	{
		id: 1021,
		time: 1020,
		velocity: 18.3702777777778,
		power: 3850.24008974091,
		road: 9318.82703703704,
		acceleration: -0.0975925925925907
	},
	{
		id: 1022,
		time: 1021,
		velocity: 18.1761111111111,
		power: 4694.51161388736,
		road: 9337.09310185185,
		acceleration: -0.0469444444444456
	},
	{
		id: 1023,
		time: 1022,
		velocity: 18.1813888888889,
		power: 4404.34205335768,
		road: 9355.30486111111,
		acceleration: -0.0616666666666674
	},
	{
		id: 1024,
		time: 1023,
		velocity: 18.1852777777778,
		power: 6146.10812835527,
		road: 9373.50513888889,
		acceleration: 0.0387037037037068
	},
	{
		id: 1025,
		time: 1024,
		velocity: 18.2922222222222,
		power: 8284.24497048811,
		road: 9391.80319444444,
		acceleration: 0.156851851851851
	},
	{
		id: 1026,
		time: 1025,
		velocity: 18.6519444444444,
		power: 6696.5092045353,
		road: 9410.21050925926,
		acceleration: 0.0616666666666674
	},
	{
		id: 1027,
		time: 1026,
		velocity: 18.3702777777778,
		power: 4915.08338668403,
		road: 9428.62875,
		acceleration: -0.0398148148148145
	},
	{
		id: 1028,
		time: 1027,
		velocity: 18.1727777777778,
		power: -1383.40994834739,
		road: 9446.83032407407,
		acceleration: -0.393518518518523
	},
	{
		id: 1029,
		time: 1028,
		velocity: 17.4713888888889,
		power: -2479.36585243622,
		road: 9464.60949074074,
		acceleration: -0.451296296296292
	},
	{
		id: 1030,
		time: 1029,
		velocity: 17.0163888888889,
		power: -4569.89025237069,
		road: 9481.87694444444,
		acceleration: -0.572129629629629
	},
	{
		id: 1031,
		time: 1030,
		velocity: 16.4563888888889,
		power: -4300.03154302028,
		road: 9498.58138888889,
		acceleration: -0.553888888888892
	},
	{
		id: 1032,
		time: 1031,
		velocity: 15.8097222222222,
		power: -8346.12528029574,
		road: 9514.60032407407,
		acceleration: -0.817129629629628
	},
	{
		id: 1033,
		time: 1032,
		velocity: 14.565,
		power: -16859.5318965218,
		road: 9529.49185185185,
		acceleration: -1.43768518518518
	},
	{
		id: 1034,
		time: 1033,
		velocity: 12.1433333333333,
		power: -22561.0737730414,
		road: 9542.65379629629,
		acceleration: -2.02148148148148
	},
	{
		id: 1035,
		time: 1034,
		velocity: 9.74527777777778,
		power: -20875.1177968258,
		road: 9553.71685185185,
		acceleration: -2.17629629629629
	},
	{
		id: 1036,
		time: 1035,
		velocity: 8.03611111111111,
		power: -10461.3027703605,
		road: 9563.01277777778,
		acceleration: -1.35796296296296
	},
	{
		id: 1037,
		time: 1036,
		velocity: 8.06944444444444,
		power: -4108.64649209494,
		road: 9571.28583333333,
		acceleration: -0.687777777777777
	},
	{
		id: 1038,
		time: 1037,
		velocity: 7.68194444444444,
		power: 109.61618837512,
		road: 9579.14125,
		acceleration: -0.147500000000001
	},
	{
		id: 1039,
		time: 1038,
		velocity: 7.59361111111111,
		power: -408.228131602826,
		road: 9586.81467592593,
		acceleration: -0.216481481481481
	},
	{
		id: 1040,
		time: 1039,
		velocity: 7.42,
		power: 1616.37452246805,
		road: 9594.41171296296,
		acceleration: 0.0637037037037027
	},
	{
		id: 1041,
		time: 1040,
		velocity: 7.87305555555556,
		power: 4242.5946257009,
		road: 9602.24421296296,
		acceleration: 0.407222222222224
	},
	{
		id: 1042,
		time: 1041,
		velocity: 8.81527777777778,
		power: 8654.25216804599,
		road: 9610.73180555556,
		acceleration: 0.902962962962963
	},
	{
		id: 1043,
		time: 1042,
		velocity: 10.1288888888889,
		power: 10092.1466830203,
		road: 9620.14495370371,
		acceleration: 0.948148148148148
	},
	{
		id: 1044,
		time: 1043,
		velocity: 10.7175,
		power: 9210.39537902834,
		road: 9630.40953703704,
		acceleration: 0.75472222222222
	},
	{
		id: 1045,
		time: 1044,
		velocity: 11.0794444444444,
		power: 6316.23494873948,
		road: 9641.25990740741,
		acceleration: 0.416851851851852
	},
	{
		id: 1046,
		time: 1045,
		velocity: 11.3794444444444,
		power: 4377.77409573566,
		road: 9652.42546296297,
		acceleration: 0.213518518518519
	},
	{
		id: 1047,
		time: 1046,
		velocity: 11.3580555555556,
		power: 125.46501627676,
		road: 9663.60472222222,
		acceleration: -0.186111111111112
	},
	{
		id: 1048,
		time: 1047,
		velocity: 10.5211111111111,
		power: -2234.72046905643,
		road: 9674.48611111111,
		acceleration: -0.409629629629629
	},
	{
		id: 1049,
		time: 1048,
		velocity: 10.1505555555556,
		power: -2000.52433446441,
		road: 9684.96787037037,
		acceleration: -0.38962962962963
	},
	{
		id: 1050,
		time: 1049,
		velocity: 10.1891666666667,
		power: 1132.39083888011,
		road: 9695.21944444445,
		acceleration: -0.0707407407407405
	},
	{
		id: 1051,
		time: 1050,
		velocity: 10.3088888888889,
		power: 4203.9843098264,
		road: 9705.55523148148,
		acceleration: 0.239166666666666
	},
	{
		id: 1052,
		time: 1051,
		velocity: 10.8680555555556,
		power: 6851.06973965461,
		road: 9716.25064814815,
		acceleration: 0.480092592592595
	},
	{
		id: 1053,
		time: 1052,
		velocity: 11.6294444444444,
		power: 8763.51938441925,
		road: 9727.49541666667,
		acceleration: 0.618611111111109
	},
	{
		id: 1054,
		time: 1053,
		velocity: 12.1647222222222,
		power: 6460.63737879767,
		road: 9739.23541666667,
		acceleration: 0.371851851851853
	},
	{
		id: 1055,
		time: 1054,
		velocity: 11.9836111111111,
		power: 2640.05878950748,
		road: 9751.17337962963,
		acceleration: 0.0240740740740755
	},
	{
		id: 1056,
		time: 1055,
		velocity: 11.7016666666667,
		power: 1297.24766511553,
		road: 9763.07685185185,
		acceleration: -0.0930555555555568
	},
	{
		id: 1057,
		time: 1056,
		velocity: 11.8855555555556,
		power: 2985.5149721692,
		road: 9774.96194444445,
		acceleration: 0.0562962962962956
	},
	{
		id: 1058,
		time: 1057,
		velocity: 12.1525,
		power: 5034.80857544916,
		road: 9786.9900925926,
		acceleration: 0.229814814814816
	},
	{
		id: 1059,
		time: 1058,
		velocity: 12.3911111111111,
		power: 5450.71436750962,
		road: 9799.25981481482,
		acceleration: 0.253333333333332
	},
	{
		id: 1060,
		time: 1059,
		velocity: 12.6455555555556,
		power: 5197.141178383,
		road: 9811.76615740741,
		acceleration: 0.219907407407408
	},
	{
		id: 1061,
		time: 1060,
		velocity: 12.8122222222222,
		power: 4811.87565723249,
		road: 9824.47166666667,
		acceleration: 0.178425925925925
	},
	{
		id: 1062,
		time: 1061,
		velocity: 12.9263888888889,
		power: 4198.10549974988,
		road: 9837.32717592593,
		acceleration: 0.121574074074076
	},
	{
		id: 1063,
		time: 1062,
		velocity: 13.0102777777778,
		power: 3696.8034583913,
		road: 9850.28194444445,
		acceleration: 0.0769444444444432
	},
	{
		id: 1064,
		time: 1063,
		velocity: 13.0430555555556,
		power: 4090.08391277975,
		road: 9863.32773148149,
		acceleration: 0.105092592592595
	},
	{
		id: 1065,
		time: 1064,
		velocity: 13.2416666666667,
		power: 6201.62135190319,
		road: 9876.55851851852,
		acceleration: 0.264907407407406
	},
	{
		id: 1066,
		time: 1065,
		velocity: 13.805,
		power: 8122.51562676174,
		road: 9890.11986111112,
		acceleration: 0.396203703703705
	},
	{
		id: 1067,
		time: 1066,
		velocity: 14.2316666666667,
		power: 10205.9819360083,
		road: 9904.14125,
		acceleration: 0.523888888888887
	},
	{
		id: 1068,
		time: 1067,
		velocity: 14.8133333333333,
		power: 9365.66081485692,
		road: 9918.63972222223,
		acceleration: 0.430277777777778
	},
	{
		id: 1069,
		time: 1068,
		velocity: 15.0958333333333,
		power: 8702.45114040075,
		road: 9933.53287037037,
		acceleration: 0.359074074074075
	},
	{
		id: 1070,
		time: 1069,
		velocity: 15.3088888888889,
		power: 7619.64153288783,
		road: 9948.7388425926,
		acceleration: 0.266574074074072
	},
	{
		id: 1071,
		time: 1070,
		velocity: 15.6130555555556,
		power: 7234.58628392394,
		road: 9964.19199074074,
		acceleration: 0.22777777777778
	},
	{
		id: 1072,
		time: 1071,
		velocity: 15.7791666666667,
		power: 6812.51552929992,
		road: 9979.85370370371,
		acceleration: 0.18935185185185
	},
	{
		id: 1073,
		time: 1072,
		velocity: 15.8769444444444,
		power: 5216.38953859421,
		road: 9995.6487962963,
		acceleration: 0.07740740740741
	},
	{
		id: 1074,
		time: 1073,
		velocity: 15.8452777777778,
		power: 6114.41523804164,
		road: 10011.54875,
		acceleration: 0.132314814814816
	},
	{
		id: 1075,
		time: 1074,
		velocity: 16.1761111111111,
		power: 3421.89502971685,
		road: 10027.4917592593,
		acceleration: -0.0462037037037053
	},
	{
		id: 1076,
		time: 1075,
		velocity: 15.7383333333333,
		power: 5598.07586677477,
		road: 10043.4594444444,
		acceleration: 0.0955555555555563
	},
	{
		id: 1077,
		time: 1076,
		velocity: 16.1319444444444,
		power: 2892.956456644,
		road: 10059.4339814815,
		acceleration: -0.0818518518518534
	},
	{
		id: 1078,
		time: 1077,
		velocity: 15.9305555555556,
		power: 2711.52784957722,
		road: 10075.3219907407,
		acceleration: -0.0912037037037035
	},
	{
		id: 1079,
		time: 1078,
		velocity: 15.4647222222222,
		power: -1135.07966113251,
		road: 10090.9935648148,
		acceleration: -0.341666666666667
	},
	{
		id: 1080,
		time: 1079,
		velocity: 15.1069444444444,
		power: -1129.27699100783,
		road: 10106.325787037,
		acceleration: -0.337037037037037
	},
	{
		id: 1081,
		time: 1080,
		velocity: 14.9194444444444,
		power: -600.276205673031,
		road: 10121.3412962963,
		acceleration: -0.29638888888889
	},
	{
		id: 1082,
		time: 1081,
		velocity: 14.5755555555556,
		power: -91.1658827883558,
		road: 10136.0804166667,
		acceleration: -0.256388888888887
	},
	{
		id: 1083,
		time: 1082,
		velocity: 14.3377777777778,
		power: -247.338367246132,
		road: 10150.5595833333,
		acceleration: -0.263518518518518
	},
	{
		id: 1084,
		time: 1083,
		velocity: 14.1288888888889,
		power: 1755.08899880697,
		road: 10164.85,
		acceleration: -0.113981481481485
	},
	{
		id: 1085,
		time: 1084,
		velocity: 14.2336111111111,
		power: 3615.46534742711,
		road: 10179.0953703704,
		acceleration: 0.0238888888888908
	},
	{
		id: 1086,
		time: 1085,
		velocity: 14.4094444444444,
		power: 5461.05762544069,
		road: 10193.430462963,
		acceleration: 0.155555555555557
	},
	{
		id: 1087,
		time: 1086,
		velocity: 14.5955555555556,
		power: 4652.51819656151,
		road: 10207.8891203704,
		acceleration: 0.0915740740740745
	},
	{
		id: 1088,
		time: 1087,
		velocity: 14.5083333333333,
		power: 3378.92957760978,
		road: 10222.3925,
		acceleration: -0.00212962962963026
	},
	{
		id: 1089,
		time: 1088,
		velocity: 14.4030555555556,
		power: 3175.93379229515,
		road: 10236.8865740741,
		acceleration: -0.0164814814814829
	},
	{
		id: 1090,
		time: 1089,
		velocity: 14.5461111111111,
		power: 2791.58922303324,
		road: 10251.3507407407,
		acceleration: -0.0433333333333312
	},
	{
		id: 1091,
		time: 1090,
		velocity: 14.3783333333333,
		power: 4701.49710662599,
		road: 10265.8401851852,
		acceleration: 0.0938888888888876
	},
	{
		id: 1092,
		time: 1091,
		velocity: 14.6847222222222,
		power: 3756.07303976781,
		road: 10280.3883333333,
		acceleration: 0.0235185185185198
	},
	{
		id: 1093,
		time: 1092,
		velocity: 14.6166666666667,
		power: 4427.0227362053,
		road: 10294.9832407407,
		acceleration: 0.0699999999999985
	},
	{
		id: 1094,
		time: 1093,
		velocity: 14.5883333333333,
		power: 2623.11147526915,
		road: 10309.5833796296,
		acceleration: -0.0595370370370372
	},
	{
		id: 1095,
		time: 1094,
		velocity: 14.5061111111111,
		power: 2568.7354256296,
		road: 10324.1229166667,
		acceleration: -0.0616666666666674
	},
	{
		id: 1096,
		time: 1095,
		velocity: 14.4316666666667,
		power: 1375.38625158312,
		road: 10338.5590277778,
		acceleration: -0.145185185185184
	},
	{
		id: 1097,
		time: 1096,
		velocity: 14.1527777777778,
		power: 1595.43309891896,
		road: 10352.8595833333,
		acceleration: -0.125925925925925
	},
	{
		id: 1098,
		time: 1097,
		velocity: 14.1283333333333,
		power: 319.780343130207,
		road: 10366.9890277778,
		acceleration: -0.216296296296296
	},
	{
		id: 1099,
		time: 1098,
		velocity: 13.7827777777778,
		power: 938.111888367872,
		road: 10380.9270833333,
		acceleration: -0.166481481481481
	},
	{
		id: 1100,
		time: 1099,
		velocity: 13.6533333333333,
		power: 621.003208170386,
		road: 10394.6884259259,
		acceleration: -0.186944444444444
	},
	{
		id: 1101,
		time: 1100,
		velocity: 13.5675,
		power: 987.422684152902,
		road: 10408.2785648148,
		acceleration: -0.155462962962963
	},
	{
		id: 1102,
		time: 1101,
		velocity: 13.3163888888889,
		power: 521.464437951691,
		road: 10421.6968518519,
		acceleration: -0.188240740740744
	},
	{
		id: 1103,
		time: 1102,
		velocity: 13.0886111111111,
		power: 529.109176187655,
		road: 10434.9288888889,
		acceleration: -0.184259259259258
	},
	{
		id: 1104,
		time: 1103,
		velocity: 13.0147222222222,
		power: 5064.12780323553,
		road: 10448.1562962963,
		acceleration: 0.175000000000002
	},
	{
		id: 1105,
		time: 1104,
		velocity: 13.8413888888889,
		power: 9262.65468836133,
		road: 10461.7134722222,
		acceleration: 0.484537037037033
	},
	{
		id: 1106,
		time: 1105,
		velocity: 14.5422222222222,
		power: 13624.7370425183,
		road: 10475.8954166667,
		acceleration: 0.765000000000001
	},
	{
		id: 1107,
		time: 1106,
		velocity: 15.3097222222222,
		power: 9463.23938053472,
		road: 10490.6698148148,
		acceleration: 0.419907407407408
	},
	{
		id: 1108,
		time: 1107,
		velocity: 15.1011111111111,
		power: 2950.3351625284,
		road: 10505.6305555556,
		acceleration: -0.0472222222222207
	},
	{
		id: 1109,
		time: 1108,
		velocity: 14.4005555555556,
		power: -2140.84610293754,
		road: 10520.366712963,
		acceleration: -0.401944444444444
	},
	{
		id: 1110,
		time: 1109,
		velocity: 14.1038888888889,
		power: -3820.77324562316,
		road: 10534.6406018519,
		acceleration: -0.522592592592593
	},
	{
		id: 1111,
		time: 1110,
		velocity: 13.5333333333333,
		power: -1976.45507029573,
		road: 10548.4607407407,
		acceleration: -0.384907407407406
	},
	{
		id: 1112,
		time: 1111,
		velocity: 13.2458333333333,
		power: -1588.3406077855,
		road: 10561.9118518519,
		acceleration: -0.353148148148149
	},
	{
		id: 1113,
		time: 1112,
		velocity: 13.0444444444444,
		power: 761.320018734259,
		road: 10575.1038425926,
		acceleration: -0.165092592592593
	},
	{
		id: 1114,
		time: 1113,
		velocity: 13.0380555555556,
		power: 2548.15074080257,
		road: 10588.2031018519,
		acceleration: -0.0203703703703688
	},
	{
		id: 1115,
		time: 1114,
		velocity: 13.1847222222222,
		power: 3922.61341161472,
		road: 10601.3362962963,
		acceleration: 0.0882407407407406
	},
	{
		id: 1116,
		time: 1115,
		velocity: 13.3091666666667,
		power: 544.983383226434,
		road: 10614.4234259259,
		acceleration: -0.180370370370373
	},
	{
		id: 1117,
		time: 1116,
		velocity: 12.4969444444444,
		power: -2315.0507588347,
		road: 10627.2156944444,
		acceleration: -0.409351851851852
	},
	{
		id: 1118,
		time: 1117,
		velocity: 11.9566666666667,
		power: -3302.2332108292,
		road: 10639.5563888889,
		acceleration: -0.493796296296296
	},
	{
		id: 1119,
		time: 1118,
		velocity: 11.8277777777778,
		power: -1879.55444206916,
		road: 10651.46375,
		acceleration: -0.37287037037037
	},
	{
		id: 1120,
		time: 1119,
		velocity: 11.3783333333333,
		power: -1363.77962335478,
		road: 10663.0214351852,
		acceleration: -0.326481481481482
	},
	{
		id: 1121,
		time: 1120,
		velocity: 10.9772222222222,
		power: -2247.61899299489,
		road: 10674.2115277778,
		acceleration: -0.408703703703704
	},
	{
		id: 1122,
		time: 1121,
		velocity: 10.6016666666667,
		power: -4887.90187331883,
		road: 10684.860787037,
		acceleration: -0.672962962962963
	},
	{
		id: 1123,
		time: 1122,
		velocity: 9.35944444444445,
		power: -10330.3323113532,
		road: 10694.5222222222,
		acceleration: -1.30268518518518
	},
	{
		id: 1124,
		time: 1123,
		velocity: 7.06916666666667,
		power: -12260.4376206996,
		road: 10702.6581018519,
		acceleration: -1.74842592592592
	},
	{
		id: 1125,
		time: 1124,
		velocity: 5.35638888888889,
		power: -8483.12468205633,
		road: 10709.1577314815,
		acceleration: -1.52407407407407
	},
	{
		id: 1126,
		time: 1125,
		velocity: 4.78722222222222,
		power: -4919.1780320495,
		road: 10714.3228703704,
		acceleration: -1.14490740740741
	},
	{
		id: 1127,
		time: 1126,
		velocity: 3.63444444444444,
		power: -2656.34386437415,
		road: 10718.5131018519,
		acceleration: -0.804907407407408
	},
	{
		id: 1128,
		time: 1127,
		velocity: 2.94166666666667,
		power: -1826.90926923398,
		road: 10721.9541666667,
		acceleration: -0.693425925925927
	},
	{
		id: 1129,
		time: 1128,
		velocity: 2.70694444444444,
		power: -875.221843173644,
		road: 10724.8216666667,
		acceleration: -0.453703703703703
	},
	{
		id: 1130,
		time: 1129,
		velocity: 2.27333333333333,
		power: -1184.34940191971,
		road: 10727.1262037037,
		acceleration: -0.672222222222222
	},
	{
		id: 1131,
		time: 1130,
		velocity: 0.925,
		power: -1112.74901846688,
		road: 10728.6434722222,
		acceleration: -0.902314814814815
	},
	{
		id: 1132,
		time: 1131,
		velocity: 0,
		power: -410.28900645568,
		road: 10729.3306944445,
		acceleration: -0.757777777777778
	},
	{
		id: 1133,
		time: 1132,
		velocity: 0,
		power: -26.406802631579,
		road: 10729.4848611111,
		acceleration: -0.308333333333333
	},
	{
		id: 1134,
		time: 1133,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1135,
		time: 1134,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1136,
		time: 1135,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1137,
		time: 1136,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1138,
		time: 1137,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1139,
		time: 1138,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1140,
		time: 1139,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1141,
		time: 1140,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1142,
		time: 1141,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1143,
		time: 1142,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1144,
		time: 1143,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1145,
		time: 1144,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1146,
		time: 1145,
		velocity: 0,
		power: 0,
		road: 10729.4848611111,
		acceleration: 0
	},
	{
		id: 1147,
		time: 1146,
		velocity: 0,
		power: 143.412495290087,
		road: 10729.7299074074,
		acceleration: 0.490092592592593
	},
	{
		id: 1148,
		time: 1147,
		velocity: 1.47027777777778,
		power: 1154.49961151586,
		road: 10730.7518055556,
		acceleration: 1.06361111111111
	},
	{
		id: 1149,
		time: 1148,
		velocity: 3.19083333333333,
		power: 3604.92743928564,
		road: 10733.0624537037,
		acceleration: 1.51388888888889
	},
	{
		id: 1150,
		time: 1149,
		velocity: 4.54166666666667,
		power: 4271.34249274128,
		road: 10736.6837962963,
		acceleration: 1.1075
	},
	{
		id: 1151,
		time: 1150,
		velocity: 4.79277777777778,
		power: 2698.29644702924,
		road: 10741.1105555556,
		acceleration: 0.503333333333333
	},
	{
		id: 1152,
		time: 1151,
		velocity: 4.70083333333333,
		power: 1335.01834672384,
		road: 10745.8666666667,
		acceleration: 0.15537037037037
	},
	{
		id: 1153,
		time: 1152,
		velocity: 5.00777777777778,
		power: 2270.04644324614,
		road: 10750.8685648148,
		acceleration: 0.336203703703703
	},
	{
		id: 1154,
		time: 1153,
		velocity: 5.80138888888889,
		power: 5353.95744599897,
		road: 10756.4689351852,
		acceleration: 0.860740740740741
	},
	{
		id: 1155,
		time: 1154,
		velocity: 7.28305555555556,
		power: 8007.50285974508,
		road: 10763.0622222222,
		acceleration: 1.12509259259259
	},
	{
		id: 1156,
		time: 1155,
		velocity: 8.38305555555555,
		power: 9701.7900246464,
		road: 10770.7965277778,
		acceleration: 1.15694444444444
	},
	{
		id: 1157,
		time: 1156,
		velocity: 9.27222222222222,
		power: 8193.20245356807,
		road: 10779.5175,
		acceleration: 0.81638888888889
	},
	{
		id: 1158,
		time: 1157,
		velocity: 9.73222222222222,
		power: 7178.70863075843,
		road: 10788.957037037,
		acceleration: 0.620740740740741
	},
	{
		id: 1159,
		time: 1158,
		velocity: 10.2452777777778,
		power: 4157.49249342998,
		road: 10798.8365740741,
		acceleration: 0.25925925925926
	},
	{
		id: 1160,
		time: 1159,
		velocity: 10.05,
		power: 3451.33024230213,
		road: 10808.9326851852,
		acceleration: 0.173888888888888
	},
	{
		id: 1161,
		time: 1160,
		velocity: 10.2538888888889,
		power: 3187.16156302315,
		road: 10819.1855092593,
		acceleration: 0.139537037037037
	},
	{
		id: 1162,
		time: 1161,
		velocity: 10.6638888888889,
		power: 4239.12326654145,
		road: 10829.6266666667,
		acceleration: 0.23712962962963
	},
	{
		id: 1163,
		time: 1162,
		velocity: 10.7613888888889,
		power: 3133.54879692224,
		road: 10840.2456944445,
		acceleration: 0.118611111111113
	},
	{
		id: 1164,
		time: 1163,
		velocity: 10.6097222222222,
		power: 1674.07224654006,
		road: 10850.9106018519,
		acceleration: -0.0268518518518537
	},
	{
		id: 1165,
		time: 1164,
		velocity: 10.5833333333333,
		power: 3394.20523384133,
		road: 10861.632037037,
		acceleration: 0.139907407407408
	},
	{
		id: 1166,
		time: 1165,
		velocity: 11.1811111111111,
		power: 6188.31715286308,
		road: 10872.6210185185,
		acceleration: 0.395185185185186
	},
	{
		id: 1167,
		time: 1166,
		velocity: 11.7952777777778,
		power: 7766.49096962429,
		road: 10884.0628703704,
		acceleration: 0.510555555555554
	},
	{
		id: 1168,
		time: 1167,
		velocity: 12.115,
		power: 6457.63825375996,
		road: 10895.9414814815,
		acceleration: 0.362962962962962
	},
	{
		id: 1169,
		time: 1168,
		velocity: 12.27,
		power: 4160.69786390805,
		road: 10908.0760648148,
		acceleration: 0.148981481481481
	},
	{
		id: 1170,
		time: 1169,
		velocity: 12.2422222222222,
		power: 4688.13299819994,
		road: 10920.3784722222,
		acceleration: 0.186666666666667
	},
	{
		id: 1171,
		time: 1170,
		velocity: 12.675,
		power: 6084.61011817173,
		road: 10932.9203703704,
		acceleration: 0.292314814814816
	},
	{
		id: 1172,
		time: 1171,
		velocity: 13.1469444444444,
		power: 7871.11488221614,
		road: 10945.8175462963,
		acceleration: 0.418240740740742
	},
	{
		id: 1173,
		time: 1172,
		velocity: 13.4969444444444,
		power: 7214.88151559829,
		road: 10959.0950462963,
		acceleration: 0.342407407407404
	},
	{
		id: 1174,
		time: 1173,
		velocity: 13.7022222222222,
		power: 6222.78528701137,
		road: 10972.6681944445,
		acceleration: 0.24888888888889
	},
	{
		id: 1175,
		time: 1174,
		velocity: 13.8936111111111,
		power: 5944.14750821943,
		road: 10986.4737962963,
		acceleration: 0.216018518518517
	},
	{
		id: 1176,
		time: 1175,
		velocity: 14.145,
		power: 7408.57138908798,
		road: 11000.5435648148,
		acceleration: 0.312314814814817
	},
	{
		id: 1177,
		time: 1176,
		velocity: 14.6391666666667,
		power: 7010.15328336601,
		road: 11014.9031944445,
		acceleration: 0.267407407407408
	},
	{
		id: 1178,
		time: 1177,
		velocity: 14.6958333333333,
		power: 6668.48320818927,
		road: 11029.5115740741,
		acceleration: 0.230092592592591
	},
	{
		id: 1179,
		time: 1178,
		velocity: 14.8352777777778,
		power: 4310.33966169584,
		road: 11044.2628703704,
		acceleration: 0.0557407407407418
	},
	{
		id: 1180,
		time: 1179,
		velocity: 14.8063888888889,
		power: 5180.96740208306,
		road: 11059.0990277778,
		acceleration: 0.113981481481481
	},
	{
		id: 1181,
		time: 1180,
		velocity: 15.0377777777778,
		power: 5425.38244510205,
		road: 11074.0552314815,
		acceleration: 0.126111111111111
	},
	{
		id: 1182,
		time: 1181,
		velocity: 15.2136111111111,
		power: 6259.96010700203,
		road: 11089.1632407407,
		acceleration: 0.1775
	},
	{
		id: 1183,
		time: 1182,
		velocity: 15.3388888888889,
		power: 6128.20871142098,
		road: 11104.4403703704,
		acceleration: 0.16074074074074
	},
	{
		id: 1184,
		time: 1183,
		velocity: 15.52,
		power: 3084.8046858028,
		road: 11119.7731481482,
		acceleration: -0.0494444444444451
	},
	{
		id: 1185,
		time: 1184,
		velocity: 15.0652777777778,
		power: 1398.52174242376,
		road: 11135.0001851852,
		acceleration: -0.162037037037038
	},
	{
		id: 1186,
		time: 1185,
		velocity: 14.8527777777778,
		power: 985.35711768846,
		road: 11150.0528703704,
		acceleration: -0.186666666666666
	},
	{
		id: 1187,
		time: 1186,
		velocity: 14.96,
		power: 5168.58655719173,
		road: 11165.0651388889,
		acceleration: 0.105833333333335
	},
	{
		id: 1188,
		time: 1187,
		velocity: 15.3827777777778,
		power: 10920.3642019488,
		road: 11180.37375,
		acceleration: 0.486851851851851
	},
	{
		id: 1189,
		time: 1188,
		velocity: 16.3133333333333,
		power: 12843.8401658169,
		road: 11196.2153703704,
		acceleration: 0.579166666666664
	},
	{
		id: 1190,
		time: 1189,
		velocity: 16.6975,
		power: 13766.2469044452,
		road: 11212.6448611111,
		acceleration: 0.596574074074077
	},
	{
		id: 1191,
		time: 1190,
		velocity: 17.1725,
		power: 10658.8731720267,
		road: 11229.5577314815,
		acceleration: 0.370185185185186
	},
	{
		id: 1192,
		time: 1191,
		velocity: 17.4238888888889,
		power: 9445.24709724496,
		road: 11246.7946296296,
		acceleration: 0.277870370370369
	},
	{
		id: 1193,
		time: 1192,
		velocity: 17.5311111111111,
		power: 7768.61798405104,
		road: 11264.2533796296,
		acceleration: 0.165833333333335
	},
	{
		id: 1194,
		time: 1193,
		velocity: 17.67,
		power: 5282.41816804302,
		road: 11281.8018518519,
		acceleration: 0.0136111111111106
	},
	{
		id: 1195,
		time: 1194,
		velocity: 17.4647222222222,
		power: 2143.24377478677,
		road: 11299.2715277778,
		acceleration: -0.171203703703704
	},
	{
		id: 1196,
		time: 1195,
		velocity: 17.0175,
		power: -507.217038330352,
		road: 11316.4929166667,
		acceleration: -0.325370370370372
	},
	{
		id: 1197,
		time: 1196,
		velocity: 16.6938888888889,
		power: -1657.14866394032,
		road: 11333.3563888889,
		acceleration: -0.390462962962964
	},
	{
		id: 1198,
		time: 1197,
		velocity: 16.2933333333333,
		power: -1835.02806950653,
		road: 11349.8262037037,
		acceleration: -0.396851851851849
	},
	{
		id: 1199,
		time: 1198,
		velocity: 15.8269444444444,
		power: -4084.54640255044,
		road: 11365.8281944444,
		acceleration: -0.538796296296299
	},
	{
		id: 1200,
		time: 1199,
		velocity: 15.0775,
		power: -3478.76166228561,
		road: 11381.3119444444,
		acceleration: -0.497685185185183
	},
	{
		id: 1201,
		time: 1200,
		velocity: 14.8002777777778,
		power: -4130.41945325989,
		road: 11396.2755555556,
		acceleration: -0.542592592592593
	},
	{
		id: 1202,
		time: 1201,
		velocity: 14.1991666666667,
		power: -2839.43260391043,
		road: 11410.7424074074,
		acceleration: -0.450925925925928
	},
	{
		id: 1203,
		time: 1202,
		velocity: 13.7247222222222,
		power: -2915.26598955018,
		road: 11424.755787037,
		acceleration: -0.456018518518519
	},
	{
		id: 1204,
		time: 1203,
		velocity: 13.4322222222222,
		power: -1121.5110200163,
		road: 11438.3819907407,
		acceleration: -0.318333333333332
	},
	{
		id: 1205,
		time: 1204,
		velocity: 13.2441666666667,
		power: 415.550897594014,
		road: 11451.7512037037,
		acceleration: -0.195648148148148
	},
	{
		id: 1206,
		time: 1205,
		velocity: 13.1377777777778,
		power: 793.505680751268,
		road: 11464.9413425926,
		acceleration: -0.162499999999998
	},
	{
		id: 1207,
		time: 1206,
		velocity: 12.9447222222222,
		power: 1150.65379482685,
		road: 11477.9847685185,
		acceleration: -0.130925925925927
	},
	{
		id: 1208,
		time: 1207,
		velocity: 12.8513888888889,
		power: 1963.05568866129,
		road: 11490.9312037037,
		acceleration: -0.0630555555555556
	},
	{
		id: 1209,
		time: 1208,
		velocity: 12.9486111111111,
		power: 3708.98412954577,
		road: 11503.8850925926,
		acceleration: 0.0779629629629621
	},
	{
		id: 1210,
		time: 1209,
		velocity: 13.1786111111111,
		power: 5143.42681472059,
		road: 11516.9718518519,
		acceleration: 0.18777777777778
	},
	{
		id: 1211,
		time: 1210,
		velocity: 13.4147222222222,
		power: 5795.47354304461,
		road: 11530.2672685185,
		acceleration: 0.229537037037035
	},
	{
		id: 1212,
		time: 1211,
		velocity: 13.6372222222222,
		power: 5611.81897756654,
		road: 11543.7797685185,
		acceleration: 0.204629629629629
	},
	{
		id: 1213,
		time: 1212,
		velocity: 13.7925,
		power: 5982.13783073222,
		road: 11557.5059722222,
		acceleration: 0.222777777777779
	},
	{
		id: 1214,
		time: 1213,
		velocity: 14.0830555555556,
		power: 6484.23916080766,
		road: 11571.4680555556,
		acceleration: 0.248981481481481
	},
	{
		id: 1215,
		time: 1214,
		velocity: 14.3841666666667,
		power: 6966.5864364692,
		road: 11585.6903240741,
		acceleration: 0.27138888888889
	},
	{
		id: 1216,
		time: 1215,
		velocity: 14.6066666666667,
		power: 6181.035065585,
		road: 11600.1493981482,
		acceleration: 0.202222222222222
	},
	{
		id: 1217,
		time: 1216,
		velocity: 14.6897222222222,
		power: 5263.06874613627,
		road: 11614.7739351852,
		acceleration: 0.128703703703705
	},
	{
		id: 1218,
		time: 1217,
		velocity: 14.7702777777778,
		power: 6414.14951446288,
		road: 11629.5643981482,
		acceleration: 0.203148148148147
	},
	{
		id: 1219,
		time: 1218,
		velocity: 15.2161111111111,
		power: 7440.53792398434,
		road: 11644.5882407407,
		acceleration: 0.263611111111111
	},
	{
		id: 1220,
		time: 1219,
		velocity: 15.4805555555556,
		power: 8260.57374251944,
		road: 11659.8964351852,
		acceleration: 0.305092592592594
	},
	{
		id: 1221,
		time: 1220,
		velocity: 15.6855555555556,
		power: 7344.75167891476,
		road: 11675.4717592593,
		acceleration: 0.229166666666668
	},
	{
		id: 1222,
		time: 1221,
		velocity: 15.9036111111111,
		power: 6550.58920340264,
		road: 11691.2450462963,
		acceleration: 0.166759259259258
	},
	{
		id: 1223,
		time: 1222,
		velocity: 15.9808333333333,
		power: 3803.24338540217,
		road: 11707.0927314815,
		acceleration: -0.0179629629629652
	},
	{
		id: 1224,
		time: 1223,
		velocity: 15.6316666666667,
		power: 171.211384508552,
		road: 11722.8038425926,
		acceleration: -0.255185185185185
	},
	{
		id: 1225,
		time: 1224,
		velocity: 15.1380555555556,
		power: -2483.94257895066,
		road: 11738.1725,
		acceleration: -0.429722222222219
	},
	{
		id: 1226,
		time: 1225,
		velocity: 14.6916666666667,
		power: -2555.70324313246,
		road: 11753.1101388889,
		acceleration: -0.432314814814816
	},
	{
		id: 1227,
		time: 1226,
		velocity: 14.3347222222222,
		power: -1916.32201453903,
		road: 11767.6393518519,
		acceleration: -0.384537037037038
	},
	{
		id: 1228,
		time: 1227,
		velocity: 13.9844444444444,
		power: 609.125589969963,
		road: 11781.8778240741,
		acceleration: -0.196944444444444
	},
	{
		id: 1229,
		time: 1228,
		velocity: 14.1008333333333,
		power: 2475.67671393986,
		road: 11795.9898148148,
		acceleration: -0.0560185185185187
	},
	{
		id: 1230,
		time: 1229,
		velocity: 14.1666666666667,
		power: 4988.53817653936,
		road: 11810.1382407407,
		acceleration: 0.128888888888889
	},
	{
		id: 1231,
		time: 1230,
		velocity: 14.3711111111111,
		power: 6249.70943313678,
		road: 11824.4580555556,
		acceleration: 0.213888888888889
	},
	{
		id: 1232,
		time: 1231,
		velocity: 14.7425,
		power: 8662.49338730626,
		road: 11839.0711574074,
		acceleration: 0.372685185185187
	},
	{
		id: 1233,
		time: 1232,
		velocity: 15.2847222222222,
		power: 10198.0684766264,
		road: 11854.0983333333,
		acceleration: 0.455462962962962
	},
	{
		id: 1234,
		time: 1233,
		velocity: 15.7375,
		power: 10475.8317465988,
		road: 11869.5760648148,
		acceleration: 0.445648148148148
	},
	{
		id: 1235,
		time: 1234,
		velocity: 16.0794444444444,
		power: 6669.39101698589,
		road: 11885.3636111111,
		acceleration: 0.173981481481484
	},
	{
		id: 1236,
		time: 1235,
		velocity: 15.8066666666667,
		power: 2465.16460287698,
		road: 11901.1853703704,
		acceleration: -0.105555555555558
	},
	{
		id: 1237,
		time: 1236,
		velocity: 15.4208333333333,
		power: -1785.67449613219,
		road: 11916.7622685185,
		acceleration: -0.384166666666665
	},
	{
		id: 1238,
		time: 1237,
		velocity: 14.9269444444444,
		power: -2379.93463133875,
		road: 11931.9363888889,
		acceleration: -0.42138888888889
	},
	{
		id: 1239,
		time: 1238,
		velocity: 14.5425,
		power: -2217.11077389705,
		road: 11946.6960648148,
		acceleration: -0.407499999999999
	},
	{
		id: 1240,
		time: 1239,
		velocity: 14.1983333333333,
		power: -2123.59850064259,
		road: 11961.0527314815,
		acceleration: -0.39851851851852
	},
	{
		id: 1241,
		time: 1240,
		velocity: 13.7313888888889,
		power: -2554.09818341645,
		road: 11974.9956944444,
		acceleration: -0.428888888888888
	},
	{
		id: 1242,
		time: 1241,
		velocity: 13.2558333333333,
		power: -2296.33510549235,
		road: 11988.52,
		acceleration: -0.408425925925926
	},
	{
		id: 1243,
		time: 1242,
		velocity: 12.9730555555556,
		power: -1649.19754331045,
		road: 12001.6619444444,
		acceleration: -0.356296296296298
	},
	{
		id: 1244,
		time: 1243,
		velocity: 12.6625,
		power: 410.428664466783,
		road: 12014.532037037,
		acceleration: -0.187407407407406
	},
	{
		id: 1245,
		time: 1244,
		velocity: 12.6936111111111,
		power: 2483.25831314881,
		road: 12027.3006481481,
		acceleration: -0.0155555555555562
	},
	{
		id: 1246,
		time: 1245,
		velocity: 12.9263888888889,
		power: 3606.10023000658,
		road: 12040.099212963,
		acceleration: 0.0754629629629626
	},
	{
		id: 1247,
		time: 1246,
		velocity: 12.8888888888889,
		power: 3108.63865608593,
		road: 12052.9519444444,
		acceleration: 0.0328703703703699
	},
	{
		id: 1248,
		time: 1247,
		velocity: 12.7922222222222,
		power: 1815.58633372094,
		road: 12065.7850925926,
		acceleration: -0.0720370370370365
	},
	{
		id: 1249,
		time: 1248,
		velocity: 12.7102777777778,
		power: 950.836823431717,
		road: 12078.5119907407,
		acceleration: -0.140462962962962
	},
	{
		id: 1250,
		time: 1249,
		velocity: 12.4675,
		power: -486.954114877465,
		road: 12091.0403240741,
		acceleration: -0.256666666666666
	},
	{
		id: 1251,
		time: 1250,
		velocity: 12.0222222222222,
		power: -1467.96579446114,
		road: 12103.2715277778,
		acceleration: -0.337592592592593
	},
	{
		id: 1252,
		time: 1251,
		velocity: 11.6975,
		power: -1022.13555617625,
		road: 12115.1852314815,
		acceleration: -0.297407407407407
	},
	{
		id: 1253,
		time: 1252,
		velocity: 11.5752777777778,
		power: 825.153705365729,
		road: 12126.8849074074,
		acceleration: -0.130648148148149
	},
	{
		id: 1254,
		time: 1253,
		velocity: 11.6302777777778,
		power: 2758.04172773361,
		road: 12138.5412962963,
		acceleration: 0.044074074074075
	},
	{
		id: 1255,
		time: 1254,
		velocity: 11.8297222222222,
		power: 3114.96986380368,
		road: 12150.256712963,
		acceleration: 0.0739814814814821
	},
	{
		id: 1256,
		time: 1255,
		velocity: 11.7972222222222,
		power: 2155.7520931112,
		road: 12162.0027314815,
		acceleration: -0.012777777777778
	},
	{
		id: 1257,
		time: 1256,
		velocity: 11.5919444444444,
		power: 310.569073782272,
		road: 12173.6543518519,
		acceleration: -0.176018518518518
	},
	{
		id: 1258,
		time: 1257,
		velocity: 11.3016666666667,
		power: -490.738905801106,
		road: 12185.0948611111,
		acceleration: -0.246203703703706
	},
	{
		id: 1259,
		time: 1258,
		velocity: 11.0586111111111,
		power: -1015.69705474351,
		road: 12196.2656944444,
		acceleration: -0.293148148148148
	},
	{
		id: 1260,
		time: 1259,
		velocity: 10.7125,
		power: -658.784666072631,
		road: 12207.1610648148,
		acceleration: -0.257777777777775
	},
	{
		id: 1261,
		time: 1260,
		velocity: 10.5283333333333,
		power: 77.139508755388,
		road: 12217.8355092593,
		acceleration: -0.184074074074076
	},
	{
		id: 1262,
		time: 1261,
		velocity: 10.5063888888889,
		power: 497.174246455748,
		road: 12228.3478703704,
		acceleration: -0.140092592592593
	},
	{
		id: 1263,
		time: 1262,
		velocity: 10.2922222222222,
		power: -833.142580050995,
		road: 12238.6541203704,
		acceleration: -0.27212962962963
	},
	{
		id: 1264,
		time: 1263,
		velocity: 9.71194444444444,
		power: -2418.40171059785,
		road: 12248.6051388889,
		acceleration: -0.438333333333333
	},
	{
		id: 1265,
		time: 1264,
		velocity: 9.19138888888889,
		power: -1811.63219374633,
		road: 12258.147962963,
		acceleration: -0.378055555555555
	},
	{
		id: 1266,
		time: 1265,
		velocity: 9.15805555555556,
		power: -151.286730952467,
		road: 12267.4053240741,
		acceleration: -0.192870370370372
	},
	{
		id: 1267,
		time: 1266,
		velocity: 9.13333333333333,
		power: 2417.62644853696,
		road: 12276.6164351852,
		acceleration: 0.100370370370371
	},
	{
		id: 1268,
		time: 1267,
		velocity: 9.4925,
		power: 3252.18769495278,
		road: 12285.9718518519,
		acceleration: 0.188240740740742
	},
	{
		id: 1269,
		time: 1268,
		velocity: 9.72277777777778,
		power: 4140.30190959268,
		road: 12295.5584722222,
		acceleration: 0.274166666666666
	},
	{
		id: 1270,
		time: 1269,
		velocity: 9.95583333333333,
		power: 5075.78411753426,
		road: 12305.4598611111,
		acceleration: 0.35537037037037
	},
	{
		id: 1271,
		time: 1270,
		velocity: 10.5586111111111,
		power: 4794.26080818063,
		road: 12315.6915740741,
		acceleration: 0.305277777777778
	},
	{
		id: 1272,
		time: 1271,
		velocity: 10.6386111111111,
		power: 5108.23313417551,
		road: 12326.2350462963,
		acceleration: 0.318240740740741
	},
	{
		id: 1273,
		time: 1272,
		velocity: 10.9105555555556,
		power: 3593.78272409417,
		road: 12337.0160185185,
		acceleration: 0.156759259259257
	},
	{
		id: 1274,
		time: 1273,
		velocity: 11.0288888888889,
		power: 3618.04230156478,
		road: 12347.9514814815,
		acceleration: 0.152222222222225
	},
	{
		id: 1275,
		time: 1274,
		velocity: 11.0952777777778,
		power: 3483.80665217506,
		road: 12359.0296759259,
		acceleration: 0.133240740740741
	},
	{
		id: 1276,
		time: 1275,
		velocity: 11.3102777777778,
		power: 2794.15827897807,
		road: 12370.2066666667,
		acceleration: 0.0643518518518515
	},
	{
		id: 1277,
		time: 1276,
		velocity: 11.2219444444444,
		power: 3060.80643833944,
		road: 12381.4590740741,
		acceleration: 0.0864814814814814
	},
	{
		id: 1278,
		time: 1277,
		velocity: 11.3547222222222,
		power: 1914.46951942733,
		road: 12392.7440740741,
		acceleration: -0.0212962962962955
	},
	{
		id: 1279,
		time: 1278,
		velocity: 11.2463888888889,
		power: 2779.44077651243,
		road: 12404.0476388889,
		acceleration: 0.0584259259259259
	},
	{
		id: 1280,
		time: 1279,
		velocity: 11.3972222222222,
		power: 2008.07770856651,
		road: 12415.3735185185,
		acceleration: -0.013796296296297
	},
	{
		id: 1281,
		time: 1280,
		velocity: 11.3133333333333,
		power: 2625.44250629369,
		road: 12426.7139351852,
		acceleration: 0.0428703703703714
	},
	{
		id: 1282,
		time: 1281,
		velocity: 11.375,
		power: 1325.48367950767,
		road: 12438.0373148148,
		acceleration: -0.0769444444444449
	},
	{
		id: 1283,
		time: 1282,
		velocity: 11.1663888888889,
		power: 1331.02725563551,
		road: 12449.2849074074,
		acceleration: -0.0746296296296318
	},
	{
		id: 1284,
		time: 1283,
		velocity: 11.0894444444444,
		power: 511.365850336147,
		road: 12460.4206018519,
		acceleration: -0.149166666666666
	},
	{
		id: 1285,
		time: 1284,
		velocity: 10.9275,
		power: 338.386894969254,
		road: 12471.4001851852,
		acceleration: -0.163055555555554
	},
	{
		id: 1286,
		time: 1285,
		velocity: 10.6772222222222,
		power: -465.613189759123,
		road: 12482.1791203704,
		acceleration: -0.238240740740741
	},
	{
		id: 1287,
		time: 1286,
		velocity: 10.3747222222222,
		power: -534.460622168807,
		road: 12492.7173148148,
		acceleration: -0.243240740740742
	},
	{
		id: 1288,
		time: 1287,
		velocity: 10.1977777777778,
		power: -1073.5417938728,
		road: 12502.9856018519,
		acceleration: -0.296574074074075
	},
	{
		id: 1289,
		time: 1288,
		velocity: 9.7875,
		power: -1797.37253217344,
		road: 12512.9191203704,
		acceleration: -0.372962962962962
	},
	{
		id: 1290,
		time: 1289,
		velocity: 9.25583333333333,
		power: -3601.86842143608,
		road: 12522.3773148148,
		acceleration: -0.577685185185185
	},
	{
		id: 1291,
		time: 1290,
		velocity: 8.46472222222222,
		power: -2951.55875956022,
		road: 12531.2866203704,
		acceleration: -0.520092592592592
	},
	{
		id: 1292,
		time: 1291,
		velocity: 8.22722222222222,
		power: -844.138940111144,
		road: 12539.7996759259,
		acceleration: -0.272407407407409
	},
	{
		id: 1293,
		time: 1292,
		velocity: 8.43861111111111,
		power: 2728.35426048037,
		road: 12548.2619907407,
		acceleration: 0.170925925925928
	},
	{
		id: 1294,
		time: 1293,
		velocity: 8.9775,
		power: 3760.43079674909,
		road: 12556.952037037,
		acceleration: 0.284537037037037
	},
	{
		id: 1295,
		time: 1294,
		velocity: 9.08083333333333,
		power: 5051.0055007412,
		road: 12565.9910185185,
		acceleration: 0.413333333333332
	},
	{
		id: 1296,
		time: 1295,
		velocity: 9.67861111111111,
		power: 4521.77447722672,
		road: 12575.4002777778,
		acceleration: 0.327222222222222
	},
	{
		id: 1297,
		time: 1296,
		velocity: 9.95916666666667,
		power: 5087.41251637257,
		road: 12585.1562962963,
		acceleration: 0.366296296296294
	},
	{
		id: 1298,
		time: 1297,
		velocity: 10.1797222222222,
		power: 4148.5253899708,
		road: 12595.219537037,
		acceleration: 0.248148148148148
	},
	{
		id: 1299,
		time: 1298,
		velocity: 10.4230555555556,
		power: 3522.43721348576,
		road: 12605.493287037,
		acceleration: 0.172870370370372
	},
	{
		id: 1300,
		time: 1299,
		velocity: 10.4777777777778,
		power: 2990.74002280205,
		road: 12615.9098148148,
		acceleration: 0.112685185185185
	},
	{
		id: 1301,
		time: 1300,
		velocity: 10.5177777777778,
		power: 2370.26337003268,
		road: 12626.4063888889,
		acceleration: 0.0474074074074053
	},
	{
		id: 1302,
		time: 1301,
		velocity: 10.5652777777778,
		power: 3143.76690483352,
		road: 12636.9872685185,
		acceleration: 0.121203703703705
	},
	{
		id: 1303,
		time: 1302,
		velocity: 10.8413888888889,
		power: 3993.13117175296,
		road: 12647.7275462963,
		acceleration: 0.19759259259259
	},
	{
		id: 1304,
		time: 1303,
		velocity: 11.1105555555556,
		power: 3570.28711855212,
		road: 12658.6409259259,
		acceleration: 0.148611111111114
	},
	{
		id: 1305,
		time: 1304,
		velocity: 11.0111111111111,
		power: 1755.58757800891,
		road: 12669.6148611111,
		acceleration: -0.0275000000000016
	},
	{
		id: 1306,
		time: 1305,
		velocity: 10.7588888888889,
		power: 574.163129092612,
		road: 12680.5055555556,
		acceleration: -0.13898148148148
	},
	{
		id: 1307,
		time: 1306,
		velocity: 10.6936111111111,
		power: 2126.25469692217,
		road: 12691.3330092593,
		acceleration: 0.0124999999999993
	},
	{
		id: 1308,
		time: 1307,
		velocity: 11.0486111111111,
		power: 3641.65712050751,
		road: 12702.2444907407,
		acceleration: 0.155555555555555
	},
	{
		id: 1309,
		time: 1308,
		velocity: 11.2255555555556,
		power: 6186.14782667915,
		road: 12713.4249537037,
		acceleration: 0.382407407407406
	},
	{
		id: 1310,
		time: 1309,
		velocity: 11.8408333333333,
		power: 5907.45240433909,
		road: 12724.9637962963,
		acceleration: 0.334351851851855
	},
	{
		id: 1311,
		time: 1310,
		velocity: 12.0516666666667,
		power: 6056.39795579419,
		road: 12736.8338425926,
		acceleration: 0.328055555555556
	},
	{
		id: 1312,
		time: 1311,
		velocity: 12.2097222222222,
		power: 2789.15489183177,
		road: 12748.8845833333,
		acceleration: 0.0333333333333332
	},
	{
		id: 1313,
		time: 1312,
		velocity: 11.9408333333333,
		power: 513.366442799396,
		road: 12760.8702314815,
		acceleration: -0.16351851851852
	},
	{
		id: 1314,
		time: 1313,
		velocity: 11.5611111111111,
		power: -1773.18888360167,
		road: 12772.5923611111,
		acceleration: -0.36351851851852
	},
	{
		id: 1315,
		time: 1314,
		velocity: 11.1191666666667,
		power: -1627.76797087513,
		road: 12783.9575,
		acceleration: -0.350462962962963
	},
	{
		id: 1316,
		time: 1315,
		velocity: 10.8894444444444,
		power: -1755.76828981224,
		road: 12794.9658796296,
		acceleration: -0.363055555555555
	},
	{
		id: 1317,
		time: 1316,
		velocity: 10.4719444444444,
		power: -1341.49641584312,
		road: 12805.6309722222,
		acceleration: -0.323518518518519
	},
	{
		id: 1318,
		time: 1317,
		velocity: 10.1486111111111,
		power: -1641.52037772176,
		road: 12815.9571296296,
		acceleration: -0.354351851851849
	},
	{
		id: 1319,
		time: 1318,
		velocity: 9.82638888888889,
		power: 0.72884782548856,
		road: 12826.0139351852,
		acceleration: -0.184351851851854
	},
	{
		id: 1320,
		time: 1319,
		velocity: 9.91888888888889,
		power: 1187.71857090725,
		road: 12835.9497685185,
		acceleration: -0.0575925925925915
	},
	{
		id: 1321,
		time: 1320,
		velocity: 9.97583333333333,
		power: 1802.90327905179,
		road: 12845.8608796296,
		acceleration: 0.00814814814814824
	},
	{
		id: 1322,
		time: 1321,
		velocity: 9.85083333333333,
		power: 1586.06670131958,
		road: 12855.7687037037,
		acceleration: -0.0147222222222236
	},
	{
		id: 1323,
		time: 1322,
		velocity: 9.87472222222222,
		power: 1017.41332671414,
		road: 12865.6321759259,
		acceleration: -0.0739814814814803
	},
	{
		id: 1324,
		time: 1323,
		velocity: 9.75388888888889,
		power: 818.9239250386,
		road: 12875.4119444444,
		acceleration: -0.093425925925926
	},
	{
		id: 1325,
		time: 1324,
		velocity: 9.57055555555556,
		power: -194.645362102202,
		road: 12885.044537037,
		acceleration: -0.200925925925926
	},
	{
		id: 1326,
		time: 1325,
		velocity: 9.27194444444444,
		power: -7.64067754504627,
		road: 12894.4874074074,
		acceleration: -0.178518518518517
	},
	{
		id: 1327,
		time: 1326,
		velocity: 9.21833333333333,
		power: 279.413946713088,
		road: 12903.7688425926,
		acceleration: -0.144351851851852
	},
	{
		id: 1328,
		time: 1327,
		velocity: 9.1375,
		power: 884.941711517666,
		road: 12912.9413425926,
		acceleration: -0.0735185185185188
	},
	{
		id: 1329,
		time: 1328,
		velocity: 9.05138888888889,
		power: 872.862001594227,
		road: 12922.0404166667,
		acceleration: -0.0733333333333341
	},
	{
		id: 1330,
		time: 1329,
		velocity: 8.99833333333333,
		power: 72.7230589846161,
		road: 12931.0206481482,
		acceleration: -0.164351851851851
	},
	{
		id: 1331,
		time: 1330,
		velocity: 8.64444444444444,
		power: 667.145585349373,
		road: 12939.8725,
		acceleration: -0.0924074074074071
	},
	{
		id: 1332,
		time: 1331,
		velocity: 8.77416666666667,
		power: 1526.76867886079,
		road: 12948.6835648148,
		acceleration: 0.0108333333333324
	},
	{
		id: 1333,
		time: 1332,
		velocity: 9.03083333333333,
		power: 3555.66376223622,
		road: 12957.6226851852,
		acceleration: 0.245277777777778
	},
	{
		id: 1334,
		time: 1333,
		velocity: 9.38027777777778,
		power: 5900.54877292867,
		road: 12966.9291666667,
		acceleration: 0.489444444444445
	},
	{
		id: 1335,
		time: 1334,
		velocity: 10.2425,
		power: 7983.61430193096,
		road: 12976.8130092593,
		acceleration: 0.665277777777776
	},
	{
		id: 1336,
		time: 1335,
		velocity: 11.0266666666667,
		power: 8333.65685263144,
		road: 12987.3493981482,
		acceleration: 0.639814814814814
	},
	{
		id: 1337,
		time: 1336,
		velocity: 11.2997222222222,
		power: 5655.24148524838,
		road: 12998.3766666667,
		acceleration: 0.341944444444447
	},
	{
		id: 1338,
		time: 1337,
		velocity: 11.2683333333333,
		power: 3010.8227011454,
		road: 13009.6160648148,
		acceleration: 0.082314814814815
	},
	{
		id: 1339,
		time: 1338,
		velocity: 11.2736111111111,
		power: 4330.15694560905,
		road: 13020.9959259259,
		acceleration: 0.198611111111109
	},
	{
		id: 1340,
		time: 1339,
		velocity: 11.8955555555556,
		power: 4750.15831531947,
		road: 13032.5883333333,
		acceleration: 0.226481481481482
	},
	{
		id: 1341,
		time: 1340,
		velocity: 11.9477777777778,
		power: 5532.65528187435,
		road: 13044.4355092593,
		acceleration: 0.283055555555556
	},
	{
		id: 1342,
		time: 1341,
		velocity: 12.1227777777778,
		power: 2795.25537005848,
		road: 13056.4418981482,
		acceleration: 0.0353703703703694
	},
	{
		id: 1343,
		time: 1342,
		velocity: 12.0016666666667,
		power: 1575.9906613344,
		road: 13068.4306481482,
		acceleration: -0.0706481481481465
	},
	{
		id: 1344,
		time: 1343,
		velocity: 11.7358333333333,
		power: -441.417752809472,
		road: 13080.2613425926,
		acceleration: -0.245462962962966
	},
	{
		id: 1345,
		time: 1344,
		velocity: 11.3863888888889,
		power: -483.444437650881,
		road: 13091.8458796296,
		acceleration: -0.246851851851851
	},
	{
		id: 1346,
		time: 1345,
		velocity: 11.2611111111111,
		power: -263.074547346799,
		road: 13103.1948148148,
		acceleration: -0.224351851851852
	},
	{
		id: 1347,
		time: 1346,
		velocity: 11.0627777777778,
		power: 199.562880827404,
		road: 13114.3422222222,
		acceleration: -0.178703703703704
	},
	{
		id: 1348,
		time: 1347,
		velocity: 10.8502777777778,
		power: -1508.77261450738,
		road: 13125.230462963,
		acceleration: -0.339629629629629
	},
	{
		id: 1349,
		time: 1348,
		velocity: 10.2422222222222,
		power: -1284.02675292525,
		road: 13135.7899537037,
		acceleration: -0.31787037037037
	},
	{
		id: 1350,
		time: 1349,
		velocity: 10.1091666666667,
		power: -1897.26555778932,
		road: 13145.9999074074,
		acceleration: -0.381203703703704
	},
	{
		id: 1351,
		time: 1350,
		velocity: 9.70666666666667,
		power: -3553.6064960706,
		road: 13155.7372685185,
		acceleration: -0.563981481481482
	},
	{
		id: 1352,
		time: 1351,
		velocity: 8.55027777777778,
		power: -6354.82218016311,
		road: 13164.7352314815,
		acceleration: -0.914814814814815
	},
	{
		id: 1353,
		time: 1352,
		velocity: 7.36472222222222,
		power: -9184.89272783155,
		road: 13172.5793981482,
		acceleration: -1.39277777777778
	},
	{
		id: 1354,
		time: 1353,
		velocity: 5.52833333333333,
		power: -8259.84532829266,
		road: 13178.9724074074,
		acceleration: -1.50953703703704
	},
	{
		id: 1355,
		time: 1354,
		velocity: 4.02166666666667,
		power: -6791.98710812598,
		road: 13183.7996759259,
		acceleration: -1.62194444444444
	},
	{
		id: 1356,
		time: 1355,
		velocity: 2.49888888888889,
		power: -3551.68323218191,
		road: 13187.1984722222,
		acceleration: -1.235
	},
	{
		id: 1357,
		time: 1356,
		velocity: 1.82333333333333,
		power: -1751.22291237588,
		road: 13189.51625,
		acceleration: -0.927037037037037
	},
	{
		id: 1358,
		time: 1357,
		velocity: 1.24055555555556,
		power: -553.992228009008,
		road: 13191.1243981482,
		acceleration: -0.492222222222222
	},
	{
		id: 1359,
		time: 1358,
		velocity: 1.02222222222222,
		power: -202.625163744926,
		road: 13192.3339351852,
		acceleration: -0.305
	},
	{
		id: 1360,
		time: 1359,
		velocity: 0.908333333333333,
		power: 210.035696636946,
		road: 13193.4281481482,
		acceleration: 0.0743518518518518
	},
	{
		id: 1361,
		time: 1360,
		velocity: 1.46361111111111,
		power: 587.272961741461,
		road: 13194.7327314815,
		acceleration: 0.346388888888889
	},
	{
		id: 1362,
		time: 1361,
		velocity: 2.06138888888889,
		power: 1437.04806953853,
		road: 13196.5603703704,
		acceleration: 0.699722222222222
	},
	{
		id: 1363,
		time: 1362,
		velocity: 3.0075,
		power: 1931.71941014324,
		road: 13199.0768981482,
		acceleration: 0.678055555555555
	},
	{
		id: 1364,
		time: 1363,
		velocity: 3.49777777777778,
		power: 1923.237150379,
		road: 13202.1913425926,
		acceleration: 0.517777777777777
	},
	{
		id: 1365,
		time: 1364,
		velocity: 3.61472222222222,
		power: 1165.4641325298,
		road: 13205.6738425926,
		acceleration: 0.218333333333334
	},
	{
		id: 1366,
		time: 1365,
		velocity: 3.6625,
		power: 751.134174725477,
		road: 13209.3069907407,
		acceleration: 0.0829629629629629
	},
	{
		id: 1367,
		time: 1366,
		velocity: 3.74666666666667,
		power: 340.028366616058,
		road: 13212.9631481482,
		acceleration: -0.0369444444444444
	},
	{
		id: 1368,
		time: 1367,
		velocity: 3.50388888888889,
		power: -194.539312959626,
		road: 13216.5046759259,
		acceleration: -0.192314814814815
	},
	{
		id: 1369,
		time: 1368,
		velocity: 3.08555555555556,
		power: -869.476050387282,
		road: 13219.7418981482,
		acceleration: -0.416296296296296
	},
	{
		id: 1370,
		time: 1369,
		velocity: 2.49777777777778,
		power: -500.474357912198,
		road: 13222.6131018519,
		acceleration: -0.315740740740741
	},
	{
		id: 1371,
		time: 1370,
		velocity: 2.55666666666667,
		power: -218.373783472179,
		road: 13225.2166203704,
		acceleration: -0.21962962962963
	},
	{
		id: 1372,
		time: 1371,
		velocity: 2.42666666666667,
		power: 222.526547249405,
		road: 13227.6922685185,
		acceleration: -0.0361111111111114
	},
	{
		id: 1373,
		time: 1372,
		velocity: 2.38944444444444,
		power: -483.022705852325,
		road: 13229.9730555556,
		acceleration: -0.353611111111111
	},
	{
		id: 1374,
		time: 1373,
		velocity: 1.49583333333333,
		power: -327.129554627758,
		road: 13231.9238425926,
		acceleration: -0.306388888888889
	},
	{
		id: 1375,
		time: 1374,
		velocity: 1.5075,
		power: -385.74657979427,
		road: 13233.5303703704,
		acceleration: -0.38212962962963
	},
	{
		id: 1376,
		time: 1375,
		velocity: 1.24305555555556,
		power: -44.4526083808078,
		road: 13234.8640277778,
		acceleration: -0.163611111111111
	},
	{
		id: 1377,
		time: 1376,
		velocity: 1.005,
		power: -34.171693786109,
		road: 13236.0363888889,
		acceleration: -0.158981481481482
	},
	{
		id: 1378,
		time: 1377,
		velocity: 1.03055555555556,
		power: 21.7414027341404,
		road: 13237.07625,
		acceleration: -0.106018518518518
	},
	{
		id: 1379,
		time: 1378,
		velocity: 0.925,
		power: -160.855036541585,
		road: 13237.8956018519,
		acceleration: -0.335
	},
	{
		id: 1380,
		time: 1379,
		velocity: 0,
		power: -98.2121054329539,
		road: 13238.3756944444,
		acceleration: -0.343518518518518
	},
	{
		id: 1381,
		time: 1380,
		velocity: 0,
		power: -26.406802631579,
		road: 13238.5298611111,
		acceleration: -0.308333333333333
	},
	{
		id: 1382,
		time: 1381,
		velocity: 0,
		power: 0,
		road: 13238.5298611111,
		acceleration: 0
	},
	{
		id: 1383,
		time: 1382,
		velocity: 0,
		power: 0,
		road: 13238.5298611111,
		acceleration: 0
	},
	{
		id: 1384,
		time: 1383,
		velocity: 0,
		power: 1.75197440386557,
		road: 13238.542037037,
		acceleration: 0.0243518518518519
	},
	{
		id: 1385,
		time: 1384,
		velocity: 0.0730555555555556,
		power: 69.9140778310134,
		road: 13238.7154166667,
		acceleration: 0.298055555555556
	},
	{
		id: 1386,
		time: 1385,
		velocity: 0.894166666666667,
		power: 265.73214145056,
		road: 13239.2408333333,
		acceleration: 0.406018518518518
	},
	{
		id: 1387,
		time: 1386,
		velocity: 1.21805555555556,
		power: 1359.23820057984,
		road: 13240.4824074074,
		acceleration: 1.0262962962963
	},
	{
		id: 1388,
		time: 1387,
		velocity: 3.15194444444444,
		power: 2076.82941964442,
		road: 13242.6719907407,
		acceleration: 0.869722222222222
	},
	{
		id: 1389,
		time: 1388,
		velocity: 3.50333333333333,
		power: 2697.78821261761,
		road: 13245.6995833333,
		acceleration: 0.806296296296296
	},
	{
		id: 1390,
		time: 1389,
		velocity: 3.63694444444444,
		power: 1211.03101546289,
		road: 13249.2431018519,
		acceleration: 0.225555555555556
	},
	{
		id: 1391,
		time: 1390,
		velocity: 3.82861111111111,
		power: 1041.13860135535,
		road: 13252.9785648148,
		acceleration: 0.158333333333333
	},
	{
		id: 1392,
		time: 1391,
		velocity: 3.97833333333333,
		power: 1196.85199199805,
		road: 13256.8864814815,
		acceleration: 0.186574074074073
	},
	{
		id: 1393,
		time: 1392,
		velocity: 4.19666666666667,
		power: 1280.66127245044,
		road: 13260.9838425926,
		acceleration: 0.192314814814815
	},
	{
		id: 1394,
		time: 1393,
		velocity: 4.40555555555556,
		power: 1758.65116565715,
		road: 13265.3218055556,
		acceleration: 0.288888888888889
	},
	{
		id: 1395,
		time: 1394,
		velocity: 4.845,
		power: 1235.17634868008,
		road: 13269.8774074074,
		acceleration: 0.14638888888889
	},
	{
		id: 1396,
		time: 1395,
		velocity: 4.63583333333333,
		power: 454.175736943646,
		road: 13274.4883796296,
		acceleration: -0.0356481481481481
	},
	{
		id: 1397,
		time: 1396,
		velocity: 4.29861111111111,
		power: -267.698503737768,
		road: 13278.980787037,
		acceleration: -0.201481481481482
	},
	{
		id: 1398,
		time: 1397,
		velocity: 4.24055555555556,
		power: -576.954889521883,
		road: 13283.2322222222,
		acceleration: -0.280462962962963
	},
	{
		id: 1399,
		time: 1398,
		velocity: 3.79444444444444,
		power: -498.623835033173,
		road: 13287.2092592593,
		acceleration: -0.268333333333334
	},
	{
		id: 1400,
		time: 1399,
		velocity: 3.49361111111111,
		power: -408.210514429202,
		road: 13290.926712963,
		acceleration: -0.250833333333334
	},
	{
		id: 1401,
		time: 1400,
		velocity: 3.48805555555556,
		power: -86.5010004986243,
		road: 13294.4386111111,
		acceleration: -0.160277777777778
	},
	{
		id: 1402,
		time: 1401,
		velocity: 3.31361111111111,
		power: 182.445768525602,
		road: 13297.8317592593,
		acceleration: -0.0772222222222219
	},
	{
		id: 1403,
		time: 1402,
		velocity: 3.26194444444444,
		power: 551.815074735102,
		road: 13301.2055555556,
		acceleration: 0.0385185185185186
	},
	{
		id: 1404,
		time: 1403,
		velocity: 3.60361111111111,
		power: 827.840290220074,
		road: 13304.6578703704,
		acceleration: 0.118518518518518
	},
	{
		id: 1405,
		time: 1404,
		velocity: 3.66916666666667,
		power: 991.121611627565,
		road: 13308.2475462963,
		acceleration: 0.156203703703704
	},
	{
		id: 1406,
		time: 1405,
		velocity: 3.73055555555556,
		power: 416.397767978175,
		road: 13311.9077777778,
		acceleration: -0.0150925925925929
	},
	{
		id: 1407,
		time: 1406,
		velocity: 3.55833333333333,
		power: 284.256759070966,
		road: 13315.5343518519,
		acceleration: -0.0522222222222224
	},
	{
		id: 1408,
		time: 1407,
		velocity: 3.5125,
		power: -521.410484696445,
		road: 13318.9882407407,
		acceleration: -0.293148148148148
	},
	{
		id: 1409,
		time: 1408,
		velocity: 2.85111111111111,
		power: -27.4448616658726,
		road: 13322.2244444445,
		acceleration: -0.142222222222223
	},
	{
		id: 1410,
		time: 1409,
		velocity: 3.13166666666667,
		power: 709.582528156226,
		road: 13325.439212963,
		acceleration: 0.0993518518518526
	},
	{
		id: 1411,
		time: 1410,
		velocity: 3.81055555555556,
		power: 1182.44018856757,
		road: 13328.8209259259,
		acceleration: 0.234537037037037
	},
	{
		id: 1412,
		time: 1411,
		velocity: 3.55472222222222,
		power: 366.17569267101,
		road: 13332.3081018519,
		acceleration: -0.0236111111111104
	},
	{
		id: 1413,
		time: 1412,
		velocity: 3.06083333333333,
		power: -371.541958539425,
		road: 13335.6581944445,
		acceleration: -0.250555555555556
	},
	{
		id: 1414,
		time: 1413,
		velocity: 3.05888888888889,
		power: -716.382361898556,
		road: 13338.6923148148,
		acceleration: -0.381388888888889
	},
	{
		id: 1415,
		time: 1414,
		velocity: 2.41055555555556,
		power: -283.448729135165,
		road: 13341.4150925926,
		acceleration: -0.241296296296296
	},
	{
		id: 1416,
		time: 1415,
		velocity: 2.33694444444444,
		power: -407.538314688673,
		road: 13343.8641203704,
		acceleration: -0.306203703703704
	},
	{
		id: 1417,
		time: 1416,
		velocity: 2.14027777777778,
		power: 5.95774331287343,
		road: 13346.0963425926,
		acceleration: -0.127407407407407
	},
	{
		id: 1418,
		time: 1417,
		velocity: 2.02833333333333,
		power: -100.663070093138,
		road: 13348.1743981482,
		acceleration: -0.180925925925927
	},
	{
		id: 1419,
		time: 1418,
		velocity: 1.79416666666667,
		power: -735.921974377785,
		road: 13349.8683796296,
		acceleration: -0.587222222222222
	},
	{
		id: 1420,
		time: 1419,
		velocity: 0.378611111111111,
		power: -551.791446546162,
		road: 13350.9306944445,
		acceleration: -0.676111111111111
	},
	{
		id: 1421,
		time: 1420,
		velocity: 0,
		power: -189.547956032264,
		road: 13351.3559259259,
		acceleration: -0.598055555555556
	},
	{
		id: 1422,
		time: 1421,
		velocity: 0,
		power: 0.0792869233268351,
		road: 13351.4190277778,
		acceleration: -0.126203703703704
	},
	{
		id: 1423,
		time: 1422,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1424,
		time: 1423,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1425,
		time: 1424,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1426,
		time: 1425,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1427,
		time: 1426,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1428,
		time: 1427,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1429,
		time: 1428,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1430,
		time: 1429,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1431,
		time: 1430,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1432,
		time: 1431,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1433,
		time: 1432,
		velocity: 0,
		power: 0,
		road: 13351.4190277778,
		acceleration: 0
	},
	{
		id: 1434,
		time: 1433,
		velocity: 0,
		power: 91.4481857345998,
		road: 13351.6091203704,
		acceleration: 0.380185185185185
	},
	{
		id: 1435,
		time: 1434,
		velocity: 1.14055555555556,
		power: 1029.77027186922,
		road: 13352.5209722222,
		acceleration: 1.06333333333333
	},
	{
		id: 1436,
		time: 1435,
		velocity: 3.19,
		power: 2829.2079306069,
		road: 13354.6125,
		acceleration: 1.29601851851852
	},
	{
		id: 1437,
		time: 1436,
		velocity: 3.88805555555556,
		power: 4258.16317904523,
		road: 13357.9559722222,
		acceleration: 1.20787037037037
	},
	{
		id: 1438,
		time: 1437,
		velocity: 4.76416666666667,
		power: 2779.15685951899,
		road: 13362.1809722222,
		acceleration: 0.555185185185186
	},
	{
		id: 1439,
		time: 1438,
		velocity: 4.85555555555556,
		power: 2715.67439905159,
		road: 13366.915462963,
		acceleration: 0.463796296296296
	},
	{
		id: 1440,
		time: 1439,
		velocity: 5.27944444444444,
		power: 3594.7373132222,
		road: 13372.1703240741,
		acceleration: 0.576944444444444
	},
	{
		id: 1441,
		time: 1440,
		velocity: 6.495,
		power: 5192.2256202123,
		road: 13378.1005555556,
		acceleration: 0.773796296296297
	},
	{
		id: 1442,
		time: 1441,
		velocity: 7.17694444444444,
		power: 7264.8008726534,
		road: 13384.9024074074,
		acceleration: 0.969444444444444
	},
	{
		id: 1443,
		time: 1442,
		velocity: 8.18777777777778,
		power: 7375.8011775409,
		road: 13392.61125,
		acceleration: 0.844537037037036
	},
	{
		id: 1444,
		time: 1443,
		velocity: 9.02861111111111,
		power: 10252.2328612957,
		road: 13401.2786111111,
		acceleration: 1.0725
	},
	{
		id: 1445,
		time: 1444,
		velocity: 10.3944444444444,
		power: 8324.32148611286,
		road: 13410.849212963,
		acceleration: 0.733981481481482
	},
	{
		id: 1446,
		time: 1445,
		velocity: 10.3897222222222,
		power: 4731.24239418254,
		road: 13420.9404166667,
		acceleration: 0.307222222222222
	},
	{
		id: 1447,
		time: 1446,
		velocity: 9.95027777777778,
		power: -1764.14511949302,
		road: 13431.0009722222,
		acceleration: -0.36851851851852
	},
	{
		id: 1448,
		time: 1447,
		velocity: 9.28888888888889,
		power: -1992.92811562349,
		road: 13440.6790740741,
		acceleration: -0.396388888888886
	},
	{
		id: 1449,
		time: 1448,
		velocity: 9.20055555555555,
		power: -1026.10421486384,
		road: 13450.0130092593,
		acceleration: -0.291944444444447
	},
	{
		id: 1450,
		time: 1449,
		velocity: 9.07444444444444,
		power: 1535.25094409567,
		road: 13459.2012037037,
		acceleration: 0.000462962962963331
	},
	{
		id: 1451,
		time: 1450,
		velocity: 9.29027777777778,
		power: 3849.88659369991,
		road: 13468.5183796296,
		acceleration: 0.257500000000002
	},
	{
		id: 1452,
		time: 1451,
		velocity: 9.97305555555556,
		power: 6505.07315849105,
		road: 13478.2257407408,
		acceleration: 0.522870370370368
	},
	{
		id: 1453,
		time: 1452,
		velocity: 10.6430555555556,
		power: 8478.22686703623,
		road: 13488.5324537037,
		acceleration: 0.675833333333333
	},
	{
		id: 1454,
		time: 1453,
		velocity: 11.3177777777778,
		power: 6181.46337126102,
		road: 13499.3791203704,
		acceleration: 0.404074074074074
	},
	{
		id: 1455,
		time: 1454,
		velocity: 11.1852777777778,
		power: 5025.18777867143,
		road: 13510.5644444445,
		acceleration: 0.273240740740743
	},
	{
		id: 1456,
		time: 1455,
		velocity: 11.4627777777778,
		power: 5714.97100435511,
		road: 13522.0464814815,
		acceleration: 0.320185185185183
	},
	{
		id: 1457,
		time: 1456,
		velocity: 12.2783333333333,
		power: 7999.91932726377,
		road: 13533.9376851852,
		acceleration: 0.498148148148148
	},
	{
		id: 1458,
		time: 1457,
		velocity: 12.6797222222222,
		power: 6129.94968134702,
		road: 13546.2329166667,
		acceleration: 0.309907407407408
	},
	{
		id: 1459,
		time: 1458,
		velocity: 12.3925,
		power: 1165.22732832849,
		road: 13558.6253703704,
		acceleration: -0.115462962962965
	},
	{
		id: 1460,
		time: 1459,
		velocity: 11.9319444444444,
		power: -1502.78819189847,
		road: 13570.7899074074,
		acceleration: -0.340370370370369
	},
	{
		id: 1461,
		time: 1460,
		velocity: 11.6586111111111,
		power: -2233.64631300851,
		road: 13582.582037037,
		acceleration: -0.404444444444447
	},
	{
		id: 1462,
		time: 1461,
		velocity: 11.1791666666667,
		power: -377.258584638847,
		road: 13594.0538888889,
		acceleration: -0.236111111111109
	},
	{
		id: 1463,
		time: 1462,
		velocity: 11.2236111111111,
		power: 3047.70148610026,
		road: 13605.4476388889,
		acceleration: 0.0799074074074095
	},
	{
		id: 1464,
		time: 1463,
		velocity: 11.8983333333333,
		power: 8677.80715270599,
		road: 13617.1670833333,
		acceleration: 0.571481481481481
	},
	{
		id: 1465,
		time: 1464,
		velocity: 12.8936111111111,
		power: 13257.5361973145,
		road: 13629.6225462963,
		acceleration: 0.900555555555554
	},
	{
		id: 1466,
		time: 1465,
		velocity: 13.9252777777778,
		power: 15107.3755358552,
		road: 13643.0052777778,
		acceleration: 0.953981481481483
	},
	{
		id: 1467,
		time: 1466,
		velocity: 14.7602777777778,
		power: 19030.5964733446,
		road: 13657.432962963,
		acceleration: 1.13592592592593
	},
	{
		id: 1468,
		time: 1467,
		velocity: 16.3013888888889,
		power: 23030.4201443652,
		road: 13673.0664814815,
		acceleration: 1.27574074074074
	},
	{
		id: 1469,
		time: 1468,
		velocity: 17.7525,
		power: 28300.4039897275,
		road: 13690.0629166667,
		acceleration: 1.45009259259259
	},
	{
		id: 1470,
		time: 1469,
		velocity: 19.1105555555556,
		power: 26403.2336522269,
		road: 13708.3789814815,
		acceleration: 1.18916666666667
	},
	{
		id: 1471,
		time: 1470,
		velocity: 19.8688888888889,
		power: 19536.2618038854,
		road: 13727.650462963,
		acceleration: 0.721666666666668
	},
	{
		id: 1472,
		time: 1471,
		velocity: 19.9175,
		power: 8354.75239765398,
		road: 13747.3315277778,
		acceleration: 0.0975000000000001
	},
	{
		id: 1473,
		time: 1472,
		velocity: 19.4030555555556,
		power: 1779.80737152189,
		road: 13766.9367592593,
		acceleration: -0.249166666666667
	},
	{
		id: 1474,
		time: 1473,
		velocity: 19.1213888888889,
		power: -2971.56088661151,
		road: 13786.1688425926,
		acceleration: -0.497129629629629
	},
	{
		id: 1475,
		time: 1474,
		velocity: 18.4261111111111,
		power: -2224.38771185025,
		road: 13804.9275925926,
		acceleration: -0.44953703703704
	},
	{
		id: 1476,
		time: 1475,
		velocity: 18.0544444444444,
		power: -4400.74252839714,
		road: 13823.1780555556,
		acceleration: -0.567037037037036
	},
	{
		id: 1477,
		time: 1476,
		velocity: 17.4202777777778,
		power: -3753.69823661503,
		road: 13840.8821759259,
		acceleration: -0.525648148148147
	},
	{
		id: 1478,
		time: 1477,
		velocity: 16.8491666666667,
		power: -5351.99944374728,
		road: 13858.01375,
		acceleration: -0.619444444444447
	},
	{
		id: 1479,
		time: 1478,
		velocity: 16.1961111111111,
		power: -4475.84988364204,
		road: 13874.553287037,
		acceleration: -0.564629629629628
	},
	{
		id: 1480,
		time: 1479,
		velocity: 15.7263888888889,
		power: -4098.95579139515,
		road: 13890.5406481482,
		acceleration: -0.53972222222222
	},
	{
		id: 1481,
		time: 1480,
		velocity: 15.23,
		power: -4299.81018983922,
		road: 13905.9815277778,
		acceleration: -0.55324074074074
	},
	{
		id: 1482,
		time: 1481,
		velocity: 14.5363888888889,
		power: -3768.22844289804,
		road: 13920.8871296296,
		acceleration: -0.517314814814817
	},
	{
		id: 1483,
		time: 1482,
		velocity: 14.1744444444444,
		power: -9455.05017922568,
		road: 13935.0644907408,
		acceleration: -0.939166666666665
	},
	{
		id: 1484,
		time: 1483,
		velocity: 12.4125,
		power: -16534.5099368916,
		road: 13947.9911574074,
		acceleration: -1.56222222222222
	},
	{
		id: 1485,
		time: 1484,
		velocity: 9.84972222222222,
		power: -19190.3393028411,
		road: 13959.1345833333,
		acceleration: -2.00425925925926
	},
	{
		id: 1486,
		time: 1485,
		velocity: 8.16166666666667,
		power: -14151.2326667831,
		road: 13968.3846759259,
		acceleration: -1.78240740740741
	},
	{
		id: 1487,
		time: 1486,
		velocity: 7.06527777777778,
		power: -6941.63059617596,
		road: 13976.1956481482,
		acceleration: -1.09583333333333
	},
	{
		id: 1488,
		time: 1487,
		velocity: 6.56222222222222,
		power: -4141.45059711051,
		road: 13983.0647222222,
		acceleration: -0.787962962962964
	},
	{
		id: 1489,
		time: 1488,
		velocity: 5.79777777777778,
		power: -2773.63287598401,
		road: 13989.22875,
		acceleration: -0.622129629629629
	},
	{
		id: 1490,
		time: 1489,
		velocity: 5.19888888888889,
		power: -3224.10486890621,
		road: 13994.6994907407,
		acceleration: -0.764444444444444
	},
	{
		id: 1491,
		time: 1490,
		velocity: 4.26888888888889,
		power: -2296.91993315804,
		road: 13999.4641666667,
		acceleration: -0.647685185185185
	},
	{
		id: 1492,
		time: 1491,
		velocity: 3.85472222222222,
		power: -2097.64066202522,
		road: 14003.5673611111,
		acceleration: -0.675277777777778
	},
	{
		id: 1493,
		time: 1492,
		velocity: 3.17305555555556,
		power: -1373.97933326762,
		road: 14007.0584722222,
		acceleration: -0.548888888888889
	},
	{
		id: 1494,
		time: 1493,
		velocity: 2.62222222222222,
		power: -1217.75388727088,
		road: 14009.9901388889,
		acceleration: -0.57
	},
	{
		id: 1495,
		time: 1494,
		velocity: 2.14472222222222,
		power: -1466.41462539722,
		road: 14012.2259259259,
		acceleration: -0.821759259259259
	},
	{
		id: 1496,
		time: 1495,
		velocity: 0.707777777777778,
		power: -980.893317191203,
		road: 14013.6137962963,
		acceleration: -0.874074074074074
	},
	{
		id: 1497,
		time: 1496,
		velocity: 0,
		power: -330.175764333071,
		road: 14014.2071759259,
		acceleration: -0.714907407407407
	},
	{
		id: 1498,
		time: 1497,
		velocity: 0,
		power: -12.11372014295,
		road: 14014.3251388889,
		acceleration: -0.235925925925926
	},
	{
		id: 1499,
		time: 1498,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1500,
		time: 1499,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1501,
		time: 1500,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1502,
		time: 1501,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1503,
		time: 1502,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1504,
		time: 1503,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1505,
		time: 1504,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1506,
		time: 1505,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1507,
		time: 1506,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1508,
		time: 1507,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1509,
		time: 1508,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1510,
		time: 1509,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1511,
		time: 1510,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1512,
		time: 1511,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1513,
		time: 1512,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1514,
		time: 1513,
		velocity: 0,
		power: 0,
		road: 14014.3251388889,
		acceleration: 0
	},
	{
		id: 1515,
		time: 1514,
		velocity: 0,
		power: 218.583481142993,
		road: 14014.6343518519,
		acceleration: 0.618425925925926
	},
	{
		id: 1516,
		time: 1515,
		velocity: 1.85527777777778,
		power: 1373.06016854111,
		road: 14015.8064351852,
		acceleration: 1.10731481481481
	},
	{
		id: 1517,
		time: 1516,
		velocity: 3.32194444444444,
		power: 4385.6123044221,
		road: 14018.3685185185,
		acceleration: 1.67268518518519
	},
	{
		id: 1518,
		time: 1517,
		velocity: 5.01805555555556,
		power: 5507.63289357365,
		road: 14022.4151851852,
		acceleration: 1.29648148148148
	},
	{
		id: 1519,
		time: 1518,
		velocity: 5.74472222222222,
		power: 7347.17983983206,
		road: 14027.7613425926,
		acceleration: 1.3025
	},
	{
		id: 1520,
		time: 1519,
		velocity: 7.22944444444444,
		power: 12167.6783980306,
		road: 14034.6149537037,
		acceleration: 1.71240740740741
	},
	{
		id: 1521,
		time: 1520,
		velocity: 10.1552777777778,
		power: 19563.8516531563,
		road: 14043.4074074074,
		acceleration: 2.16527777777778
	},
	{
		id: 1522,
		time: 1521,
		velocity: 12.2405555555556,
		power: 29188.952079479,
		road: 14054.5562962963,
		acceleration: 2.54759259259259
	},
	{
		id: 1523,
		time: 1522,
		velocity: 14.8722222222222,
		power: 24158.1400257184,
		road: 14067.819537037,
		acceleration: 1.68111111111111
	},
	{
		id: 1524,
		time: 1523,
		velocity: 15.1986111111111,
		power: 19636.2284850241,
		road: 14082.4988888889,
		acceleration: 1.15111111111111
	},
	{
		id: 1525,
		time: 1524,
		velocity: 15.6938888888889,
		power: 10724.7311146717,
		road: 14097.9847685185,
		acceleration: 0.461944444444445
	},
	{
		id: 1526,
		time: 1525,
		velocity: 16.2580555555556,
		power: 8867.18687026845,
		road: 14113.8590277778,
		acceleration: 0.314814814814815
	},
	{
		id: 1527,
		time: 1526,
		velocity: 16.1430555555556,
		power: 3885.07155241701,
		road: 14129.8814351852,
		acceleration: -0.0185185185185155
	},
	{
		id: 1528,
		time: 1527,
		velocity: 15.6383333333333,
		power: -1595.15252164571,
		road: 14145.7075462963,
		acceleration: -0.374074074074077
	},
	{
		id: 1529,
		time: 1528,
		velocity: 15.1358333333333,
		power: -7230.03625479109,
		road: 14160.9693518519,
		acceleration: -0.754537037037037
	},
	{
		id: 1530,
		time: 1529,
		velocity: 13.8794444444444,
		power: -9437.90487965987,
		road: 14175.3888888889,
		acceleration: -0.93
	},
	{
		id: 1531,
		time: 1530,
		velocity: 12.8483333333333,
		power: -12495.6578594437,
		road: 14188.7391203704,
		acceleration: -1.20861111111111
	},
	{
		id: 1532,
		time: 1531,
		velocity: 11.51,
		power: -11112.0111362826,
		road: 14200.9006481482,
		acceleration: -1.1687962962963
	},
	{
		id: 1533,
		time: 1532,
		velocity: 10.3730555555556,
		power: -8655.44744234221,
		road: 14211.9693518519,
		acceleration: -1.01685185185185
	},
	{
		id: 1534,
		time: 1533,
		velocity: 9.79777777777778,
		power: -6557.28394439867,
		road: 14222.0972222222,
		acceleration: -0.864814814814816
	},
	{
		id: 1535,
		time: 1534,
		velocity: 8.91555555555555,
		power: -4086.57242847187,
		road: 14231.4754166667,
		acceleration: -0.634537037037038
	},
	{
		id: 1536,
		time: 1535,
		velocity: 8.46944444444444,
		power: -2987.94174175891,
		road: 14240.2724537037,
		acceleration: -0.527777777777775
	},
	{
		id: 1537,
		time: 1536,
		velocity: 8.21444444444444,
		power: -2079.81715050878,
		road: 14248.5910648148,
		acceleration: -0.429074074074077
	},
	{
		id: 1538,
		time: 1537,
		velocity: 7.62833333333333,
		power: -4729.36805627456,
		road: 14256.2919907408,
		acceleration: -0.806296296296295
	},
	{
		id: 1539,
		time: 1538,
		velocity: 6.05055555555556,
		power: -6684.08731052471,
		road: 14262.9885648148,
		acceleration: -1.20240740740741
	},
	{
		id: 1540,
		time: 1539,
		velocity: 4.60722222222222,
		power: -7544.72793168064,
		road: 14268.2589814815,
		acceleration: -1.64990740740741
	},
	{
		id: 1541,
		time: 1540,
		velocity: 2.67861111111111,
		power: -4826.20799400139,
		road: 14271.9476851852,
		acceleration: -1.51351851851852
	},
	{
		id: 1542,
		time: 1541,
		velocity: 1.51,
		power: -2292.45662150247,
		road: 14274.300787037,
		acceleration: -1.15768518518519
	},
	{
		id: 1543,
		time: 1542,
		velocity: 1.13416666666667,
		power: -962.191212078991,
		road: 14275.6286111111,
		acceleration: -0.89287037037037
	},
	{
		id: 1544,
		time: 1543,
		velocity: 0,
		power: -224.147496672262,
		road: 14276.2583333333,
		acceleration: -0.503333333333333
	},
	{
		id: 1545,
		time: 1544,
		velocity: 0,
		power: -44.8638527777778,
		road: 14276.4473611111,
		acceleration: -0.378055555555556
	},
	{
		id: 1546,
		time: 1545,
		velocity: 0,
		power: 0,
		road: 14276.4473611111,
		acceleration: 0
	},
	{
		id: 1547,
		time: 1546,
		velocity: 0,
		power: 0,
		road: 14276.4473611111,
		acceleration: 0
	},
	{
		id: 1548,
		time: 1547,
		velocity: 0,
		power: 0,
		road: 14276.4473611111,
		acceleration: 0
	},
	{
		id: 1549,
		time: 1548,
		velocity: 0,
		power: 0,
		road: 14276.4473611111,
		acceleration: 0
	},
	{
		id: 1550,
		time: 1549,
		velocity: 0,
		power: 0,
		road: 14276.4473611111,
		acceleration: 0
	},
	{
		id: 1551,
		time: 1550,
		velocity: 0,
		power: 0,
		road: 14276.4473611111,
		acceleration: 0
	},
	{
		id: 1552,
		time: 1551,
		velocity: 0,
		power: 0,
		road: 14276.4473611111,
		acceleration: 0
	},
	{
		id: 1553,
		time: 1552,
		velocity: 0,
		power: 103.045274165161,
		road: 14276.6508333333,
		acceleration: 0.406944444444444
	},
	{
		id: 1554,
		time: 1553,
		velocity: 1.22083333333333,
		power: 1241.80735553475,
		road: 14277.649537037,
		acceleration: 1.18351851851852
	},
	{
		id: 1555,
		time: 1554,
		velocity: 3.55055555555556,
		power: 4156.96650215028,
		road: 14280.0769907408,
		acceleration: 1.67398148148148
	},
	{
		id: 1556,
		time: 1555,
		velocity: 5.02194444444444,
		power: 6526.49921318674,
		road: 14284.1224537037,
		acceleration: 1.56203703703704
	},
	{
		id: 1557,
		time: 1556,
		velocity: 5.90694444444444,
		power: 6078.07204135461,
		road: 14289.4746296296,
		acceleration: 1.05138888888889
	},
	{
		id: 1558,
		time: 1557,
		velocity: 6.70472222222222,
		power: 8632.05087540904,
		road: 14295.9750462963,
		acceleration: 1.24509259259259
	},
	{
		id: 1559,
		time: 1558,
		velocity: 8.75722222222222,
		power: 11406.5309597261,
		road: 14303.7844444445,
		acceleration: 1.37287037037037
	},
	{
		id: 1560,
		time: 1559,
		velocity: 10.0255555555556,
		power: 11154.8097126007,
		road: 14312.8401388889,
		acceleration: 1.11972222222222
	},
	{
		id: 1561,
		time: 1560,
		velocity: 10.0638888888889,
		power: 5629.34840041458,
		road: 14322.665462963,
		acceleration: 0.419537037037038
	},
	{
		id: 1562,
		time: 1561,
		velocity: 10.0158333333333,
		power: 1551.89508339226,
		road: 14332.6897685185,
		acceleration: -0.0215740740740742
	},
	{
		id: 1563,
		time: 1562,
		velocity: 9.96083333333333,
		power: 853.586528712495,
		road: 14342.6565277778,
		acceleration: -0.0935185185185183
	},
	{
		id: 1564,
		time: 1563,
		velocity: 9.78333333333333,
		power: 1291.6179823981,
		road: 14352.5537037037,
		acceleration: -0.0456481481481479
	},
	{
		id: 1565,
		time: 1564,
		velocity: 9.87888888888889,
		power: -344.033585206414,
		road: 14362.3189814815,
		acceleration: -0.218148148148149
	},
	{
		id: 1566,
		time: 1565,
		velocity: 9.30638888888889,
		power: -626.160432008811,
		road: 14371.8513888889,
		acceleration: -0.247592592592591
	},
	{
		id: 1567,
		time: 1566,
		velocity: 9.04055555555555,
		power: -1744.44284047068,
		road: 14381.0730092593,
		acceleration: -0.373981481481483
	},
	{
		id: 1568,
		time: 1567,
		velocity: 8.75694444444444,
		power: -845.741340302239,
		road: 14389.971712963,
		acceleration: -0.271851851851851
	},
	{
		id: 1569,
		time: 1568,
		velocity: 8.49083333333333,
		power: -1953.25428251397,
		road: 14398.5302777778,
		acceleration: -0.408425925925926
	},
	{
		id: 1570,
		time: 1569,
		velocity: 7.81527777777778,
		power: -3270.05874934755,
		road: 14406.5894444445,
		acceleration: -0.59037037037037
	},
	{
		id: 1571,
		time: 1570,
		velocity: 6.98583333333333,
		power: -3255.16256569955,
		road: 14414.0445833333,
		acceleration: -0.617685185185186
	},
	{
		id: 1572,
		time: 1571,
		velocity: 6.63777777777778,
		power: -2355.37092400769,
		road: 14420.9340740741,
		acceleration: -0.513611111111111
	},
	{
		id: 1573,
		time: 1572,
		velocity: 6.27444444444444,
		power: -1618.88027275914,
		road: 14427.3589351852,
		acceleration: -0.415648148148148
	},
	{
		id: 1574,
		time: 1573,
		velocity: 5.73888888888889,
		power: -1387.97455643316,
		road: 14433.3808333333,
		acceleration: -0.390277777777778
	},
	{
		id: 1575,
		time: 1574,
		velocity: 5.46694444444444,
		power: -1976.14449420233,
		road: 14438.9484259259,
		acceleration: -0.518333333333334
	},
	{
		id: 1576,
		time: 1575,
		velocity: 4.71944444444444,
		power: -1811.6616365208,
		road: 14443.9971296296,
		acceleration: -0.519444444444445
	},
	{
		id: 1577,
		time: 1576,
		velocity: 4.18055555555556,
		power: -2681.837358244,
		road: 14448.3959722222,
		acceleration: -0.780277777777778
	},
	{
		id: 1578,
		time: 1577,
		velocity: 3.12611111111111,
		power: -1539.16802039674,
		road: 14452.1193518519,
		acceleration: -0.570648148148148
	},
	{
		id: 1579,
		time: 1578,
		velocity: 3.0075,
		power: -938.538354758152,
		road: 14455.3371296296,
		acceleration: -0.440555555555555
	},
	{
		id: 1580,
		time: 1579,
		velocity: 2.85888888888889,
		power: 482.562311525387,
		road: 14458.3526851852,
		acceleration: 0.0361111111111105
	},
	{
		id: 1581,
		time: 1580,
		velocity: 3.23444444444444,
		power: 1296.69146827578,
		road: 14461.5344444445,
		acceleration: 0.296296296296297
	},
	{
		id: 1582,
		time: 1581,
		velocity: 3.89638888888889,
		power: 1941.10288687691,
		road: 14465.0850462963,
		acceleration: 0.441388888888889
	},
	{
		id: 1583,
		time: 1582,
		velocity: 4.18305555555555,
		power: 1700.29899024247,
		road: 14469.0160648148,
		acceleration: 0.319444444444445
	},
	{
		id: 1584,
		time: 1583,
		velocity: 4.19277777777778,
		power: 1191.48006543983,
		road: 14473.1885648148,
		acceleration: 0.163518518518519
	},
	{
		id: 1585,
		time: 1584,
		velocity: 4.38694444444444,
		power: 1398.78630824439,
		road: 14477.5429166667,
		acceleration: 0.200185185185185
	},
	{
		id: 1586,
		time: 1585,
		velocity: 4.78361111111111,
		power: 1292.95357862111,
		road: 14482.077962963,
		acceleration: 0.161203703703705
	},
	{
		id: 1587,
		time: 1586,
		velocity: 4.67638888888889,
		power: 700.882077340558,
		road: 14486.7036574074,
		acceleration: 0.0200925925925919
	},
	{
		id: 1588,
		time: 1587,
		velocity: 4.44722222222222,
		power: -1229.59056232685,
		road: 14491.12375,
		acceleration: -0.431296296296297
	},
	{
		id: 1589,
		time: 1588,
		velocity: 3.48972222222222,
		power: -1339.31536859381,
		road: 14495.0818981482,
		acceleration: -0.492592592592592
	},
	{
		id: 1590,
		time: 1589,
		velocity: 3.19861111111111,
		power: -1221.22083858077,
		road: 14498.5406944445,
		acceleration: -0.506111111111112
	},
	{
		id: 1591,
		time: 1590,
		velocity: 2.92888888888889,
		power: 517.590402512303,
		road: 14501.7643981482,
		acceleration: 0.0359259259259264
	},
	{
		id: 1592,
		time: 1591,
		velocity: 3.5975,
		power: 2373.34143761955,
		road: 14505.2931018519,
		acceleration: 0.574074074074074
	},
	{
		id: 1593,
		time: 1592,
		velocity: 4.92083333333333,
		power: 3231.29646337224,
		road: 14509.4495833333,
		acceleration: 0.681481481481482
	},
	{
		id: 1594,
		time: 1593,
		velocity: 4.97333333333333,
		power: 2295.47440433583,
		road: 14514.1348148148,
		acceleration: 0.376018518518518
	},
	{
		id: 1595,
		time: 1594,
		velocity: 4.72555555555556,
		power: 1599.48553898381,
		road: 14519.106712963,
		acceleration: 0.197314814814814
	},
	{
		id: 1596,
		time: 1595,
		velocity: 5.51277777777778,
		power: 3128.46827830644,
		road: 14524.4156944445,
		acceleration: 0.476851851851853
	},
	{
		id: 1597,
		time: 1596,
		velocity: 6.40388888888889,
		power: 6238.22642832746,
		road: 14530.4343055556,
		acceleration: 0.942407407407407
	},
	{
		id: 1598,
		time: 1597,
		velocity: 7.55277777777778,
		power: 4708.20138967176,
		road: 14537.2126388889,
		acceleration: 0.577037037037036
	},
	{
		id: 1599,
		time: 1598,
		velocity: 7.24388888888889,
		power: 1396.02278961694,
		road: 14544.3050925926,
		acceleration: 0.0512037037037034
	},
	{
		id: 1600,
		time: 1599,
		velocity: 6.5575,
		power: -1982.34044944067,
		road: 14551.1948148148,
		acceleration: -0.456666666666665
	},
	{
		id: 1601,
		time: 1600,
		velocity: 6.18277777777778,
		power: -2035.8918843376,
		road: 14557.6141203704,
		acceleration: -0.484166666666667
	},
	{
		id: 1602,
		time: 1601,
		velocity: 5.79138888888889,
		power: -1547.55090635882,
		road: 14563.5812037037,
		acceleration: -0.420277777777779
	},
	{
		id: 1603,
		time: 1602,
		velocity: 5.29666666666667,
		power: -307.959986772307,
		road: 14569.2368055556,
		acceleration: -0.202685185185185
	},
	{
		id: 1604,
		time: 1603,
		velocity: 5.57472222222222,
		power: 1150.46934341011,
		road: 14574.8268518519,
		acceleration: 0.071574074074074
	},
	{
		id: 1605,
		time: 1604,
		velocity: 6.00611111111111,
		power: 1979.19574903872,
		road: 14580.5612962963,
		acceleration: 0.217222222222222
	},
	{
		id: 1606,
		time: 1605,
		velocity: 5.94833333333333,
		power: 1241.17145886717,
		road: 14586.4419444445,
		acceleration: 0.0751851851851848
	},
	{
		id: 1607,
		time: 1606,
		velocity: 5.80027777777778,
		power: -1689.38070848982,
		road: 14592.1311574074,
		acceleration: -0.458055555555554
	},
	{
		id: 1608,
		time: 1607,
		velocity: 4.63194444444444,
		power: -3418.74176331714,
		road: 14597.1629166667,
		acceleration: -0.856851851851852
	},
	{
		id: 1609,
		time: 1608,
		velocity: 3.37777777777778,
		power: -3582.99214453573,
		road: 14601.2344444445,
		acceleration: -1.06361111111111
	},
	{
		id: 1610,
		time: 1609,
		velocity: 2.60944444444444,
		power: -1868.46306013712,
		road: 14604.3962962963,
		acceleration: -0.755740740740741
	},
	{
		id: 1611,
		time: 1610,
		velocity: 2.36472222222222,
		power: -1554.38060048137,
		road: 14606.7697685185,
		acceleration: -0.821018518518518
	},
	{
		id: 1612,
		time: 1611,
		velocity: 0.914722222222222,
		power: -1073.56366509439,
		road: 14608.2978240741,
		acceleration: -0.869814814814815
	},
	{
		id: 1613,
		time: 1612,
		velocity: 0,
		power: -437.511714649846,
		road: 14608.9968518519,
		acceleration: -0.788240740740741
	},
	{
		id: 1614,
		time: 1613,
		velocity: 0,
		power: -25.6185878330085,
		road: 14609.1493055556,
		acceleration: -0.304907407407407
	},
	{
		id: 1615,
		time: 1614,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1616,
		time: 1615,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1617,
		time: 1616,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1618,
		time: 1617,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1619,
		time: 1618,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1620,
		time: 1619,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1621,
		time: 1620,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1622,
		time: 1621,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1623,
		time: 1622,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1624,
		time: 1623,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1625,
		time: 1624,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1626,
		time: 1625,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1627,
		time: 1626,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1628,
		time: 1627,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1629,
		time: 1628,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1630,
		time: 1629,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1631,
		time: 1630,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1632,
		time: 1631,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1633,
		time: 1632,
		velocity: 0,
		power: 0,
		road: 14609.1493055556,
		acceleration: 0
	},
	{
		id: 1634,
		time: 1633,
		velocity: 0,
		power: 256.033910531635,
		road: 14609.4863425926,
		acceleration: 0.674074074074074
	},
	{
		id: 1635,
		time: 1634,
		velocity: 2.02222222222222,
		power: 1798.49325517332,
		road: 14610.811712963,
		acceleration: 1.30259259259259
	},
	{
		id: 1636,
		time: 1635,
		velocity: 3.90777777777778,
		power: 5099.13903546928,
		road: 14613.6641203704,
		acceleration: 1.75148148148148
	},
	{
		id: 1637,
		time: 1636,
		velocity: 5.25444444444444,
		power: 4888.97352797032,
		road: 14617.9271759259,
		acceleration: 1.06981481481481
	},
	{
		id: 1638,
		time: 1637,
		velocity: 5.23166666666667,
		power: 3005.08742182033,
		road: 14622.9680092593,
		acceleration: 0.485740740740741
	},
	{
		id: 1639,
		time: 1638,
		velocity: 5.365,
		power: 2758.07507655107,
		road: 14628.444537037,
		acceleration: 0.385648148148147
	},
	{
		id: 1640,
		time: 1639,
		velocity: 6.41138888888889,
		power: 5124.6037260427,
		road: 14634.4860185185,
		acceleration: 0.744259259259261
	},
	{
		id: 1641,
		time: 1640,
		velocity: 7.46444444444444,
		power: 9290.98210897169,
		road: 14641.5165740741,
		acceleration: 1.23388888888889
	},
	{
		id: 1642,
		time: 1641,
		velocity: 9.06666666666667,
		power: 10856.8241025727,
		road: 14649.7718981482,
		acceleration: 1.21564814814815
	},
	{
		id: 1643,
		time: 1642,
		velocity: 10.0583333333333,
		power: 13387.159350249,
		road: 14659.2844444445,
		acceleration: 1.2987962962963
	},
	{
		id: 1644,
		time: 1643,
		velocity: 11.3608333333333,
		power: 11135.249903119,
		road: 14669.9010648148,
		acceleration: 0.90935185185185
	},
	{
		id: 1645,
		time: 1644,
		velocity: 11.7947222222222,
		power: 8175.16101944616,
		road: 14681.2500462963,
		acceleration: 0.555370370370369
	},
	{
		id: 1646,
		time: 1645,
		velocity: 11.7244444444444,
		power: 3486.47810412862,
		road: 14692.9310185185,
		acceleration: 0.108611111111111
	},
	{
		id: 1647,
		time: 1646,
		velocity: 11.6866666666667,
		power: 4475.29860664105,
		road: 14704.7614351852,
		acceleration: 0.190277777777778
	},
	{
		id: 1648,
		time: 1647,
		velocity: 12.3655555555556,
		power: 9624.9830887194,
		road: 14716.9934722222,
		acceleration: 0.612962962962966
	},
	{
		id: 1649,
		time: 1648,
		velocity: 13.5633333333333,
		power: 13718.0902948199,
		road: 14729.9743518519,
		acceleration: 0.884722222222219
	},
	{
		id: 1650,
		time: 1649,
		velocity: 14.3408333333333,
		power: 17177.4071078445,
		road: 14743.9237037037,
		acceleration: 1.05222222222222
	},
	{
		id: 1651,
		time: 1650,
		velocity: 15.5222222222222,
		power: 16991.7478007216,
		road: 14758.8674537037,
		acceleration: 0.936574074074075
	},
	{
		id: 1652,
		time: 1651,
		velocity: 16.3730555555556,
		power: 17614.6301884901,
		road: 14774.725787037,
		acceleration: 0.892592592592592
	},
	{
		id: 1653,
		time: 1652,
		velocity: 17.0186111111111,
		power: 12320.5882284194,
		road: 14791.2786111111,
		acceleration: 0.496388888888887
	},
	{
		id: 1654,
		time: 1653,
		velocity: 17.0113888888889,
		power: 7054.24764625963,
		road: 14808.1541666667,
		acceleration: 0.149074074074075
	},
	{
		id: 1655,
		time: 1654,
		velocity: 16.8202777777778,
		power: 4030.50622570987,
		road: 14825.084212963,
		acceleration: -0.0400925925925932
	},
	{
		id: 1656,
		time: 1655,
		velocity: 16.8983333333333,
		power: 6809.5537061113,
		road: 14842.0589814815,
		acceleration: 0.129537037037036
	},
	{
		id: 1657,
		time: 1656,
		velocity: 17.4,
		power: 8229.7893917239,
		road: 14859.2028703704,
		acceleration: 0.208703703703705
	},
	{
		id: 1658,
		time: 1657,
		velocity: 17.4463888888889,
		power: 7448.43795463319,
		road: 14876.5275,
		acceleration: 0.152777777777779
	},
	{
		id: 1659,
		time: 1658,
		velocity: 17.3566666666667,
		power: 6044.90449132163,
		road: 14893.9603703704,
		acceleration: 0.0637037037037018
	},
	{
		id: 1660,
		time: 1659,
		velocity: 17.5911111111111,
		power: 6868.98645038122,
		road: 14911.4797685185,
		acceleration: 0.109351851851855
	},
	{
		id: 1661,
		time: 1660,
		velocity: 17.7744444444444,
		power: 8031.16654188501,
		road: 14929.1398611111,
		acceleration: 0.172037037037036
	},
	{
		id: 1662,
		time: 1661,
		velocity: 17.8727777777778,
		power: 8000.61871470557,
		road: 14946.9671759259,
		acceleration: 0.162407407407404
	},
	{
		id: 1663,
		time: 1662,
		velocity: 18.0783333333333,
		power: 8561.62184769025,
		road: 14964.9691203704,
		acceleration: 0.186851851851852
	},
	{
		id: 1664,
		time: 1663,
		velocity: 18.335,
		power: 7944.15618664678,
		road: 14983.1362037037,
		acceleration: 0.143425925925929
	},
	{
		id: 1665,
		time: 1664,
		velocity: 18.3030555555556,
		power: 6241.06723314672,
		road: 15001.3958796296,
		acceleration: 0.0417592592592584
	},
	{
		id: 1666,
		time: 1665,
		velocity: 18.2036111111111,
		power: 2759.74261358584,
		road: 15019.5985648148,
		acceleration: -0.15574074074074
	},
	{
		id: 1667,
		time: 1666,
		velocity: 17.8677777777778,
		power: 1364.72436202608,
		road: 15037.6078703704,
		acceleration: -0.231018518518518
	},
	{
		id: 1668,
		time: 1667,
		velocity: 17.61,
		power: 141.245029638483,
		road: 15055.3533796296,
		acceleration: -0.29657407407408
	},
	{
		id: 1669,
		time: 1668,
		velocity: 17.3138888888889,
		power: 3982.56194336001,
		road: 15072.9184259259,
		acceleration: -0.0643518518518462
	},
	{
		id: 1670,
		time: 1669,
		velocity: 17.6747222222222,
		power: 6875.63423385699,
		road: 15090.5047222222,
		acceleration: 0.106851851851847
	},
	{
		id: 1671,
		time: 1670,
		velocity: 17.9305555555556,
		power: 11138.8395122479,
		road: 15108.3180092593,
		acceleration: 0.347129629629631
	},
	{
		id: 1672,
		time: 1671,
		velocity: 18.3552777777778,
		power: 10508.0801830057,
		road: 15126.4512037037,
		acceleration: 0.292685185185185
	},
	{
		id: 1673,
		time: 1672,
		velocity: 18.5527777777778,
		power: 8000.43762049803,
		road: 15144.7999074074,
		acceleration: 0.138333333333335
	},
	{
		id: 1674,
		time: 1673,
		velocity: 18.3455555555556,
		power: 4757.81983349417,
		road: 15163.1938425926,
		acceleration: -0.0478703703703687
	},
	{
		id: 1675,
		time: 1674,
		velocity: 18.2116666666667,
		power: -544.352790932888,
		road: 15181.3912037037,
		acceleration: -0.345277777777781
	},
	{
		id: 1676,
		time: 1675,
		velocity: 17.5169444444444,
		power: -2692.6328542782,
		road: 15199.1839351852,
		acceleration: -0.463981481481479
	},
	{
		id: 1677,
		time: 1676,
		velocity: 16.9536111111111,
		power: -4658.37628003231,
		road: 15216.4559259259,
		acceleration: -0.577500000000001
	},
	{
		id: 1678,
		time: 1677,
		velocity: 16.4791666666667,
		power: -4519.07307419832,
		road: 15233.1553703704,
		acceleration: -0.56759259259259
	},
	{
		id: 1679,
		time: 1678,
		velocity: 15.8141666666667,
		power: -4363.02613842615,
		road: 15249.2925,
		acceleration: -0.557037037037039
	},
	{
		id: 1680,
		time: 1679,
		velocity: 15.2825,
		power: -10025.7144206989,
		road: 15264.6797222222,
		acceleration: -0.942777777777778
	},
	{
		id: 1681,
		time: 1680,
		velocity: 13.6508333333333,
		power: -10309.8034009609,
		road: 15279.0988888889,
		acceleration: -0.993333333333334
	},
	{
		id: 1682,
		time: 1681,
		velocity: 12.8341666666667,
		power: -12094.2758048677,
		road: 15292.4323611111,
		acceleration: -1.17805555555555
	},
	{
		id: 1683,
		time: 1682,
		velocity: 11.7483333333333,
		power: -8103.40368807859,
		road: 15304.7249537037,
		acceleration: -0.903703703703703
	},
	{
		id: 1684,
		time: 1683,
		velocity: 10.9397222222222,
		power: -8419.60471998953,
		road: 15316.0766666667,
		acceleration: -0.978055555555557
	},
	{
		id: 1685,
		time: 1684,
		velocity: 9.9,
		power: -7525.57802493171,
		road: 15326.4650925926,
		acceleration: -0.948518518518519
	},
	{
		id: 1686,
		time: 1685,
		velocity: 8.90277777777778,
		power: -6280.64868451775,
		road: 15335.9423148148,
		acceleration: -0.87388888888889
	},
	{
		id: 1687,
		time: 1686,
		velocity: 8.31805555555555,
		power: -5576.35548022477,
		road: 15344.5580092593,
		acceleration: -0.849166666666665
	},
	{
		id: 1688,
		time: 1687,
		velocity: 7.3525,
		power: -3485.83672783109,
		road: 15352.4354166667,
		acceleration: -0.627407407407406
	},
	{
		id: 1689,
		time: 1688,
		velocity: 7.02055555555556,
		power: -3455.05212418545,
		road: 15359.6696296296,
		acceleration: -0.658981481481482
	},
	{
		id: 1690,
		time: 1689,
		velocity: 6.34111111111111,
		power: -2173.68045783694,
		road: 15366.3264814815,
		acceleration: -0.495740740740742
	},
	{
		id: 1691,
		time: 1690,
		velocity: 5.86527777777778,
		power: -2318.18253972967,
		road: 15372.4624537037,
		acceleration: -0.546018518518517
	},
	{
		id: 1692,
		time: 1691,
		velocity: 5.3825,
		power: -1656.90793948012,
		road: 15378.0981018519,
		acceleration: -0.454629629629631
	},
	{
		id: 1693,
		time: 1692,
		velocity: 4.97722222222222,
		power: -1324.0934235341,
		road: 15383.3012037037,
		acceleration: -0.410462962962962
	},
	{
		id: 1694,
		time: 1693,
		velocity: 4.63388888888889,
		power: -668.742231084213,
		road: 15388.15625,
		acceleration: -0.285648148148148
	},
	{
		id: 1695,
		time: 1694,
		velocity: 4.52555555555556,
		power: 608.83976502333,
		road: 15392.8665740741,
		acceleration: -0.00379629629629719
	},
	{
		id: 1696,
		time: 1695,
		velocity: 4.96583333333333,
		power: 2362.73928471449,
		road: 15397.75875,
		acceleration: 0.367500000000001
	},
	{
		id: 1697,
		time: 1696,
		velocity: 5.73638888888889,
		power: 3899.93640862847,
		road: 15403.1438425926,
		acceleration: 0.618333333333332
	},
	{
		id: 1698,
		time: 1697,
		velocity: 6.38055555555555,
		power: 5403.05022842537,
		road: 15409.2307870371,
		acceleration: 0.785370370370372
	},
	{
		id: 1699,
		time: 1698,
		velocity: 7.32194444444445,
		power: 4851.55980127093,
		road: 15416.01,
		acceleration: 0.599166666666666
	},
	{
		id: 1700,
		time: 1699,
		velocity: 7.53388888888889,
		power: 5549.58187918246,
		road: 15423.4041203704,
		acceleration: 0.630648148148148
	},
	{
		id: 1701,
		time: 1700,
		velocity: 8.2725,
		power: 4556.51914957265,
		road: 15431.3340277778,
		acceleration: 0.440925925925925
	},
	{
		id: 1702,
		time: 1701,
		velocity: 8.64472222222222,
		power: 4553.59380486969,
		road: 15439.6873611111,
		acceleration: 0.405925925925926
	},
	{
		id: 1703,
		time: 1702,
		velocity: 8.75166666666667,
		power: 1508.64136239652,
		road: 15448.2518055556,
		acceleration: 0.0162962962962965
	},
	{
		id: 1704,
		time: 1703,
		velocity: 8.32138888888889,
		power: 1661.24468505235,
		road: 15456.8414814815,
		acceleration: 0.0341666666666658
	},
	{
		id: 1705,
		time: 1704,
		velocity: 8.74722222222222,
		power: 1906.7425447304,
		road: 15465.4794444445,
		acceleration: 0.0624074074074095
	},
	{
		id: 1706,
		time: 1705,
		velocity: 8.93888888888889,
		power: 3702.43753619546,
		road: 15474.2838888889,
		acceleration: 0.270555555555553
	},
	{
		id: 1707,
		time: 1706,
		velocity: 9.13305555555555,
		power: 2158.03814560789,
		road: 15483.2633796296,
		acceleration: 0.0795370370370367
	},
	{
		id: 1708,
		time: 1707,
		velocity: 8.98583333333333,
		power: 222.512803884235,
		road: 15492.2094444445,
		acceleration: -0.146388888888888
	},
	{
		id: 1709,
		time: 1708,
		velocity: 8.49972222222222,
		power: -935.967864137361,
		road: 15500.9408333333,
		acceleration: -0.282962962962962
	},
	{
		id: 1710,
		time: 1709,
		velocity: 8.28416666666667,
		power: -708.000236209757,
		road: 15509.4029166667,
		acceleration: -0.255648148148149
	},
	{
		id: 1711,
		time: 1710,
		velocity: 8.21888888888889,
		power: -257.188671344926,
		road: 15517.637962963,
		acceleration: -0.198425925925926
	},
	{
		id: 1712,
		time: 1711,
		velocity: 7.90444444444444,
		power: -925.706343571432,
		road: 15525.63125,
		acceleration: -0.285092592592592
	},
	{
		id: 1713,
		time: 1712,
		velocity: 7.42888888888889,
		power: -1351.77627449639,
		road: 15533.3091666667,
		acceleration: -0.345648148148148
	},
	{
		id: 1714,
		time: 1713,
		velocity: 7.18194444444444,
		power: -284.732135931056,
		road: 15540.7149074074,
		acceleration: -0.198703703703703
	},
	{
		id: 1715,
		time: 1714,
		velocity: 7.30833333333333,
		power: 1499.74610978206,
		road: 15548.0499074074,
		acceleration: 0.0572222222222223
	},
	{
		id: 1716,
		time: 1715,
		velocity: 7.60055555555556,
		power: 4538.46189347173,
		road: 15555.6474537037,
		acceleration: 0.46787037037037
	},
	{
		id: 1717,
		time: 1716,
		velocity: 8.58555555555556,
		power: 5125.37954796473,
		road: 15563.7299537037,
		acceleration: 0.502037037037037
	},
	{
		id: 1718,
		time: 1717,
		velocity: 8.81444444444444,
		power: 5356.78764181228,
		road: 15572.3070370371,
		acceleration: 0.487129629629631
	},
	{
		id: 1719,
		time: 1718,
		velocity: 9.06194444444444,
		power: 3526.06252466346,
		road: 15581.2485185185,
		acceleration: 0.241666666666665
	},
	{
		id: 1720,
		time: 1719,
		velocity: 9.31055555555556,
		power: 3152.10572823877,
		road: 15590.4042592593,
		acceleration: 0.186851851851852
	},
	{
		id: 1721,
		time: 1720,
		velocity: 9.375,
		power: 1456.83810611423,
		road: 15599.6483796296,
		acceleration: -0.0100925925925939
	},
	{
		id: 1722,
		time: 1721,
		velocity: 9.03166666666667,
		power: -88.5116491633787,
		road: 15608.7950925926,
		acceleration: -0.18472222222222
	},
	{
		id: 1723,
		time: 1722,
		velocity: 8.75638888888889,
		power: -74.2576339183861,
		road: 15617.75875,
		acceleration: -0.18138888888889
	},
	{
		id: 1724,
		time: 1723,
		velocity: 8.83083333333333,
		power: 937.800366632102,
		road: 15626.6016666667,
		acceleration: -0.0600925925925928
	},
	{
		id: 1725,
		time: 1724,
		velocity: 8.85138888888889,
		power: 1834.7412677064,
		road: 15635.4378703704,
		acceleration: 0.0466666666666651
	},
	{
		id: 1726,
		time: 1725,
		velocity: 8.89638888888889,
		power: 1576.41284697109,
		road: 15644.3049074074,
		acceleration: 0.0150000000000006
	},
	{
		id: 1727,
		time: 1726,
		velocity: 8.87583333333333,
		power: 1601.04505198646,
		road: 15653.1881481482,
		acceleration: 0.0174074074074078
	},
	{
		id: 1728,
		time: 1727,
		velocity: 8.90361111111111,
		power: 1136.84348664116,
		road: 15662.0614814815,
		acceleration: -0.0372222222222227
	},
	{
		id: 1729,
		time: 1728,
		velocity: 8.78472222222222,
		power: 1134.44135287166,
		road: 15670.8979166667,
		acceleration: -0.0365740740740748
	},
	{
		id: 1730,
		time: 1729,
		velocity: 8.76611111111111,
		power: 1469.9860446769,
		road: 15679.717962963,
		acceleration: 0.00379629629629719
	},
	{
		id: 1731,
		time: 1730,
		velocity: 8.915,
		power: 2007.37623227063,
		road: 15688.5731481482,
		acceleration: 0.06648148148148
	},
	{
		id: 1732,
		time: 1731,
		velocity: 8.98416666666667,
		power: 2562.79560864337,
		road: 15697.5256018519,
		acceleration: 0.128055555555559
	},
	{
		id: 1733,
		time: 1732,
		velocity: 9.15027777777778,
		power: 1910.33617525894,
		road: 15706.5662962963,
		acceleration: 0.0484259259259261
	},
	{
		id: 1734,
		time: 1733,
		velocity: 9.06027777777778,
		power: 1532.03007714491,
		road: 15715.6330555556,
		acceleration: 0.0037037037037031
	},
	{
		id: 1735,
		time: 1734,
		velocity: 8.99527777777778,
		power: 2425.18426278077,
		road: 15724.7541203704,
		acceleration: 0.104907407407406
	},
	{
		id: 1736,
		time: 1735,
		velocity: 9.465,
		power: 4121.2546399054,
		road: 15734.0716666667,
		acceleration: 0.288055555555555
	},
	{
		id: 1737,
		time: 1736,
		velocity: 9.92444444444445,
		power: 5517.94954661797,
		road: 15743.7426388889,
		acceleration: 0.418796296296296
	},
	{
		id: 1738,
		time: 1737,
		velocity: 10.2516666666667,
		power: 4294.78832696276,
		road: 15753.7561111111,
		acceleration: 0.266203703703704
	},
	{
		id: 1739,
		time: 1738,
		velocity: 10.2636111111111,
		power: 3691.55230906029,
		road: 15763.9985185185,
		acceleration: 0.191666666666665
	},
	{
		id: 1740,
		time: 1739,
		velocity: 10.4994444444444,
		power: 3120.46861826113,
		road: 15774.3999537037,
		acceleration: 0.12638888888889
	},
	{
		id: 1741,
		time: 1740,
		velocity: 10.6308333333333,
		power: 4038.40657175705,
		road: 15784.9698148148,
		acceleration: 0.210462962962962
	},
	{
		id: 1742,
		time: 1741,
		velocity: 10.895,
		power: 4091.74819896531,
		road: 15795.7475925926,
		acceleration: 0.205370370370373
	},
	{
		id: 1743,
		time: 1742,
		velocity: 11.1155555555556,
		power: 4741.23030648309,
		road: 15806.7560648148,
		acceleration: 0.256018518518518
	},
	{
		id: 1744,
		time: 1743,
		velocity: 11.3988888888889,
		power: 4774.49551308192,
		road: 15818.015462963,
		acceleration: 0.245833333333332
	},
	{
		id: 1745,
		time: 1744,
		velocity: 11.6325,
		power: 5123.77288297816,
		road: 15829.53,
		acceleration: 0.264444444444447
	},
	{
		id: 1746,
		time: 1745,
		velocity: 11.9088888888889,
		power: 5000.65874183908,
		road: 15841.2968055556,
		acceleration: 0.240092592592591
	},
	{
		id: 1747,
		time: 1746,
		velocity: 12.1191666666667,
		power: 4194.97765879259,
		road: 15853.2633333333,
		acceleration: 0.159351851851852
	},
	{
		id: 1748,
		time: 1747,
		velocity: 12.1105555555556,
		power: 2590.32645208858,
		road: 15865.3175,
		acceleration: 0.0159259259259255
	},
	{
		id: 1749,
		time: 1748,
		velocity: 11.9566666666667,
		power: 1294.69089426864,
		road: 15877.331712963,
		acceleration: -0.0958333333333314
	},
	{
		id: 1750,
		time: 1749,
		velocity: 11.8316666666667,
		power: 170.051888905797,
		road: 15889.2020833333,
		acceleration: -0.191851851851855
	},
	{
		id: 1751,
		time: 1750,
		velocity: 11.535,
		power: 495.617407449414,
		road: 15900.8964814815,
		acceleration: -0.160092592592592
	},
	{
		id: 1752,
		time: 1751,
		velocity: 11.4763888888889,
		power: 236.908988265488,
		road: 15912.420462963,
		acceleration: -0.18074074074074
	},
	{
		id: 1753,
		time: 1752,
		velocity: 11.2894444444444,
		power: 533.177661152317,
		road: 15923.7786111111,
		acceleration: -0.150925925925923
	},
	{
		id: 1754,
		time: 1753,
		velocity: 11.0822222222222,
		power: 424.722941916928,
		road: 15934.9820833333,
		acceleration: -0.158425925925926
	},
	{
		id: 1755,
		time: 1754,
		velocity: 11.0011111111111,
		power: 274.959732288966,
		road: 15946.0213425926,
		acceleration: -0.170000000000002
	},
	{
		id: 1756,
		time: 1755,
		velocity: 10.7794444444444,
		power: -2734.79533433737,
		road: 15956.7456944445,
		acceleration: -0.459814814814816
	},
	{
		id: 1757,
		time: 1756,
		velocity: 9.70277777777778,
		power: -3692.82479286562,
		road: 15966.9572685185,
		acceleration: -0.56574074074074
	},
	{
		id: 1758,
		time: 1757,
		velocity: 9.30388888888889,
		power: -4648.70150323663,
		road: 15976.5417592593,
		acceleration: -0.688425925925927
	},
	{
		id: 1759,
		time: 1758,
		velocity: 8.71416666666667,
		power: -2635.10391407122,
		road: 15985.5417592593,
		acceleration: -0.480555555555556
	},
	{
		id: 1760,
		time: 1759,
		velocity: 8.26111111111111,
		power: -3221.89404789864,
		road: 15994.0178703704,
		acceleration: -0.56722222222222
	},
	{
		id: 1761,
		time: 1760,
		velocity: 7.60222222222222,
		power: -2245.3562227423,
		road: 16001.9806481482,
		acceleration: -0.459444444444446
	},
	{
		id: 1762,
		time: 1761,
		velocity: 7.33583333333333,
		power: -2672.54364454627,
		road: 16009.4461574074,
		acceleration: -0.535092592592592
	},
	{
		id: 1763,
		time: 1762,
		velocity: 6.65583333333333,
		power: -1721.9051941385,
		road: 16016.4371759259,
		acceleration: -0.413888888888889
	},
	{
		id: 1764,
		time: 1763,
		velocity: 6.36055555555556,
		power: -2163.33105172825,
		road: 16022.9714351852,
		acceleration: -0.49962962962963
	},
	{
		id: 1765,
		time: 1764,
		velocity: 5.83694444444444,
		power: -1845.44588533846,
		road: 16029.0214351852,
		acceleration: -0.468888888888888
	},
	{
		id: 1766,
		time: 1765,
		velocity: 5.24916666666667,
		power: -1612.84829555078,
		road: 16034.6127314815,
		acceleration: -0.448518518518519
	},
	{
		id: 1767,
		time: 1766,
		velocity: 5.015,
		power: -1657.34728591382,
		road: 16039.7385185185,
		acceleration: -0.482499999999999
	},
	{
		id: 1768,
		time: 1767,
		velocity: 4.38944444444444,
		power: -2256.03785587732,
		road: 16044.2927314815,
		acceleration: -0.660648148148148
	},
	{
		id: 1769,
		time: 1768,
		velocity: 3.26722222222222,
		power: -3384.05408356691,
		road: 16047.9635185185,
		acceleration: -1.1062037037037
	},
	{
		id: 1770,
		time: 1769,
		velocity: 1.69638888888889,
		power: -3015.65396208434,
		road: 16050.3496296296,
		acceleration: -1.46314814814815
	},
	{
		id: 1771,
		time: 1770,
		velocity: 0,
		power: -1010.94682928553,
		road: 16051.4596296296,
		acceleration: -1.08907407407407
	},
	{
		id: 1772,
		time: 1771,
		velocity: 0,
		power: -117.300728281352,
		road: 16051.7423611111,
		acceleration: -0.565462962962963
	},
	{
		id: 1773,
		time: 1772,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1774,
		time: 1773,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1775,
		time: 1774,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1776,
		time: 1775,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1777,
		time: 1776,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1778,
		time: 1777,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1779,
		time: 1778,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1780,
		time: 1779,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1781,
		time: 1780,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1782,
		time: 1781,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1783,
		time: 1782,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1784,
		time: 1783,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1785,
		time: 1784,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1786,
		time: 1785,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1787,
		time: 1786,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1788,
		time: 1787,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1789,
		time: 1788,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1790,
		time: 1789,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1791,
		time: 1790,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1792,
		time: 1791,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1793,
		time: 1792,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1794,
		time: 1793,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1795,
		time: 1794,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1796,
		time: 1795,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1797,
		time: 1796,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1798,
		time: 1797,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1799,
		time: 1798,
		velocity: 0,
		power: 0,
		road: 16051.7423611111,
		acceleration: 0
	},
	{
		id: 1800,
		time: 1799,
		velocity: 0,
		power: 1.86098471287468,
		road: 16051.7551851852,
		acceleration: 0.0256481481481482
	},
	{
		id: 1801,
		time: 1800,
		velocity: 0.0769444444444445,
		power: 3.0987644225005,
		road: 16051.7808333333,
		acceleration: 0
	},
	{
		id: 1802,
		time: 1801,
		velocity: 0,
		power: 199.196058893316,
		road: 16052.0865277778,
		acceleration: 0.560092592592593
	},
	{
		id: 1803,
		time: 1802,
		velocity: 1.68027777777778,
		power: 1374.17495484328,
		road: 16053.2376851852,
		acceleration: 1.13083333333333
	},
	{
		id: 1804,
		time: 1803,
		velocity: 3.46944444444444,
		power: 3791.32052321113,
		road: 16055.7001388889,
		acceleration: 1.49175925925926
	},
	{
		id: 1805,
		time: 1804,
		velocity: 4.47527777777778,
		power: 4047.4032068603,
		road: 16059.4146759259,
		acceleration: 1.01240740740741
	},
	{
		id: 1806,
		time: 1805,
		velocity: 4.7175,
		power: 2117.98943769623,
		road: 16063.8193981482,
		acceleration: 0.367962962962963
	},
	{
		id: 1807,
		time: 1806,
		velocity: 4.57333333333333,
		power: 479.909638458853,
		road: 16068.39375,
		acceleration: -0.0287037037037043
	},
	{
		id: 1808,
		time: 1807,
		velocity: 4.38916666666667,
		power: 436.538788502001,
		road: 16072.9348611111,
		acceleration: -0.0377777777777775
	},
	{
		id: 1809,
		time: 1808,
		velocity: 4.60416666666667,
		power: 589.689690068017,
		road: 16077.4562962963,
		acceleration: -0.00157407407407373
	},
	{
		id: 1810,
		time: 1809,
		velocity: 4.56861111111111,
		power: 1412.47825318761,
		road: 16082.0684722222,
		acceleration: 0.183055555555555
	},
	{
		id: 1811,
		time: 1810,
		velocity: 4.93833333333333,
		power: 2729.04069942902,
		road: 16086.993287037,
		acceleration: 0.442222222222222
	},
	{
		id: 1812,
		time: 1811,
		velocity: 5.93083333333333,
		power: 7028.53768836823,
		road: 16092.7126851852,
		acceleration: 1.14694444444444
	},
	{
		id: 1813,
		time: 1812,
		velocity: 8.00944444444444,
		power: 8324.83297148568,
		road: 16099.567037037,
		acceleration: 1.12296296296296
	},
	{
		id: 1814,
		time: 1813,
		velocity: 8.30722222222222,
		power: 8319.02146467911,
		road: 16107.4556481482,
		acceleration: 0.945555555555555
	},
	{
		id: 1815,
		time: 1814,
		velocity: 8.7675,
		power: 4701.30535090985,
		road: 16116.0209259259,
		acceleration: 0.407777777777778
	},
	{
		id: 1816,
		time: 1815,
		velocity: 9.23277777777778,
		power: 5024.83576133959,
		road: 16124.9975925926,
		acceleration: 0.414999999999999
	},
	{
		id: 1817,
		time: 1816,
		velocity: 9.55222222222222,
		power: 2444.16701025753,
		road: 16134.232962963,
		acceleration: 0.102407407407409
	},
	{
		id: 1818,
		time: 1817,
		velocity: 9.07472222222222,
		power: 42.4956238791732,
		road: 16143.4343981482,
		acceleration: -0.170277777777777
	},
	{
		id: 1819,
		time: 1818,
		velocity: 8.72194444444444,
		power: -2161.97158817479,
		road: 16152.3371296296,
		acceleration: -0.427129629629631
	},
	{
		id: 1820,
		time: 1819,
		velocity: 8.27083333333333,
		power: -2042.02236413607,
		road: 16160.8158333333,
		acceleration: -0.420925925925923
	},
	{
		id: 1821,
		time: 1820,
		velocity: 7.81194444444444,
		power: -3814.80759935006,
		road: 16168.75,
		acceleration: -0.66814814814815
	},
	{
		id: 1822,
		time: 1821,
		velocity: 6.7175,
		power: -3750.7943102269,
		road: 16175.9996296296,
		acceleration: -0.700925925925926
	},
	{
		id: 1823,
		time: 1822,
		velocity: 6.16805555555556,
		power: -2842.06400755104,
		road: 16182.5962962963,
		acceleration: -0.605
	},
	{
		id: 1824,
		time: 1823,
		velocity: 5.99694444444444,
		power: -3952.38611635561,
		road: 16188.4626388889,
		acceleration: -0.855648148148148
	},
	{
		id: 1825,
		time: 1824,
		velocity: 4.15055555555556,
		power: -4479.41892534536,
		road: 16193.3481018519,
		acceleration: -1.10611111111111
	},
	{
		id: 1826,
		time: 1825,
		velocity: 2.84972222222222,
		power: -3394.41314147098,
		road: 16197.1414351852,
		acceleration: -1.07814814814815
	},
	{
		id: 1827,
		time: 1826,
		velocity: 2.7625,
		power: -777.713434308784,
		road: 16200.1951851852,
		acceleration: -0.401018518518518
	},
	{
		id: 1828,
		time: 1827,
		velocity: 2.9475,
		power: 866.46986452595,
		road: 16203.1374537037,
		acceleration: 0.178055555555555
	},
	{
		id: 1829,
		time: 1828,
		velocity: 3.38388888888889,
		power: 482.521013781945,
		road: 16206.1858333333,
		acceleration: 0.0341666666666662
	},
	{
		id: 1830,
		time: 1829,
		velocity: 2.865,
		power: -529.265621417559,
		road: 16209.0891666667,
		acceleration: -0.324259259259259
	},
	{
		id: 1831,
		time: 1830,
		velocity: 1.97472222222222,
		power: -672.689232991438,
		road: 16211.6250462963,
		acceleration: -0.410648148148148
	},
	{
		id: 1832,
		time: 1831,
		velocity: 2.15194444444444,
		power: -211.295349581904,
		road: 16213.8402314815,
		acceleration: -0.230740740740741
	},
	{
		id: 1833,
		time: 1832,
		velocity: 2.17277777777778,
		power: -527.467863905125,
		road: 16215.7280092593,
		acceleration: -0.424074074074074
	},
	{
		id: 1834,
		time: 1833,
		velocity: 0.7025,
		power: -735.254972872568,
		road: 16217.0450925926,
		acceleration: -0.717314814814815
	},
	{
		id: 1835,
		time: 1834,
		velocity: 0,
		power: -337.081936394133,
		road: 16217.6413888889,
		acceleration: -0.724259259259259
	},
	{
		id: 1836,
		time: 1835,
		velocity: 0,
		power: -11.8282513157895,
		road: 16217.7584722222,
		acceleration: -0.234166666666667
	},
	{
		id: 1837,
		time: 1836,
		velocity: 0,
		power: 31.5657052516127,
		road: 16217.859537037,
		acceleration: 0.20212962962963
	},
	{
		id: 1838,
		time: 1837,
		velocity: 0.606388888888889,
		power: 107.447959937588,
		road: 16218.1766666667,
		acceleration: 0.23
	},
	{
		id: 1839,
		time: 1838,
		velocity: 0.69,
		power: 211.991585987187,
		road: 16218.7425925926,
		acceleration: 0.267592592592593
	},
	{
		id: 1840,
		time: 1839,
		velocity: 0.802777777777778,
		power: 209.388733950925,
		road: 16219.5204166667,
		acceleration: 0.156203703703704
	},
	{
		id: 1841,
		time: 1840,
		velocity: 1.075,
		power: 212.433288231815,
		road: 16220.4349074074,
		acceleration: 0.11712962962963
	},
	{
		id: 1842,
		time: 1841,
		velocity: 1.04138888888889,
		power: 133.661898336951,
		road: 16221.4158333333,
		acceleration: 0.0157407407407408
	},
	{
		id: 1843,
		time: 1842,
		velocity: 0.85,
		power: -176.855502264044,
		road: 16222.225462963,
		acceleration: -0.358333333333333
	},
	{
		id: 1844,
		time: 1843,
		velocity: 0,
		power: -95.0339804943482,
		road: 16222.6823611111,
		acceleration: -0.34712962962963
	},
	{
		id: 1845,
		time: 1844,
		velocity: 0,
		power: -20.9104473684211,
		road: 16222.8240277778,
		acceleration: -0.283333333333333
	},
	{
		id: 1846,
		time: 1845,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1847,
		time: 1846,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1848,
		time: 1847,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1849,
		time: 1848,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1850,
		time: 1849,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1851,
		time: 1850,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1852,
		time: 1851,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1853,
		time: 1852,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1854,
		time: 1853,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1855,
		time: 1854,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1856,
		time: 1855,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1857,
		time: 1856,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1858,
		time: 1857,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1859,
		time: 1858,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1860,
		time: 1859,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1861,
		time: 1860,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1862,
		time: 1861,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1863,
		time: 1862,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1864,
		time: 1863,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1865,
		time: 1864,
		velocity: 0,
		power: 0,
		road: 16222.8240277778,
		acceleration: 0
	},
	{
		id: 1866,
		time: 1865,
		velocity: 0,
		power: 91.0201845802539,
		road: 16223.0136111111,
		acceleration: 0.379166666666667
	},
	{
		id: 1867,
		time: 1866,
		velocity: 1.1375,
		power: 1214.37619817317,
		road: 16223.9868518519,
		acceleration: 1.18814814814815
	},
	{
		id: 1868,
		time: 1867,
		velocity: 3.56444444444444,
		power: 3774.98111981836,
		road: 16226.335787037,
		acceleration: 1.56324074074074
	},
	{
		id: 1869,
		time: 1868,
		velocity: 4.68972222222222,
		power: 6043.81323469728,
		road: 16230.2180555556,
		acceleration: 1.50342592592593
	},
	{
		id: 1870,
		time: 1869,
		velocity: 5.64777777777778,
		power: 5790.79847625471,
		road: 16235.3720370371,
		acceleration: 1.04
	},
	{
		id: 1871,
		time: 1870,
		velocity: 6.68444444444444,
		power: 9879.79078992096,
		road: 16241.7812037037,
		acceleration: 1.47037037037037
	},
	{
		id: 1872,
		time: 1871,
		velocity: 9.10083333333333,
		power: 16226.3634145207,
		road: 16249.8937962963,
		acceleration: 1.93648148148148
	},
	{
		id: 1873,
		time: 1872,
		velocity: 11.4572222222222,
		power: 19760.9572284512,
		road: 16259.9169444445,
		acceleration: 1.88462962962963
	},
	{
		id: 1874,
		time: 1873,
		velocity: 12.3383333333333,
		power: 22753.6047316606,
		road: 16271.7838888889,
		acceleration: 1.80296296296296
	},
	{
		id: 1875,
		time: 1874,
		velocity: 14.5097222222222,
		power: 23929.6420049434,
		road: 16285.3595833333,
		acceleration: 1.61453703703704
	},
	{
		id: 1876,
		time: 1875,
		velocity: 16.3008333333333,
		power: 36743.058458951,
		road: 16300.8515740741,
		acceleration: 2.21805555555556
	},
	{
		id: 1877,
		time: 1876,
		velocity: 18.9925,
		power: 35875.0840541121,
		road: 16318.3720833333,
		acceleration: 1.83898148148148
	},
	{
		id: 1878,
		time: 1877,
		velocity: 20.0266666666667,
		power: 30146.5794999634,
		road: 16337.4696296296,
		acceleration: 1.3150925925926
	},
	{
		id: 1879,
		time: 1878,
		velocity: 20.2461111111111,
		power: 13903.1723802878,
		road: 16357.4127777778,
		acceleration: 0.376111111111111
	},
	{
		id: 1880,
		time: 1879,
		velocity: 20.1208333333333,
		power: 1840.31247675427,
		road: 16377.4156018519,
		acceleration: -0.256759259259265
	},
	{
		id: 1881,
		time: 1880,
		velocity: 19.2563888888889,
		power: -13047.941574757,
		road: 16396.7688425926,
		acceleration: -1.0424074074074
	},
	{
		id: 1882,
		time: 1881,
		velocity: 17.1188888888889,
		power: -29750.0699182847,
		road: 16414.5746296296,
		acceleration: -2.0525
	},
	{
		id: 1883,
		time: 1882,
		velocity: 13.9633333333333,
		power: -36682.5456564869,
		road: 16429.9770833333,
		acceleration: -2.75416666666666
	},
	{
		id: 1884,
		time: 1883,
		velocity: 10.9938888888889,
		power: -30153.5591061549,
		road: 16442.6458333333,
		acceleration: -2.71324074074074
	},
	{
		id: 1885,
		time: 1884,
		velocity: 8.97916666666667,
		power: -17766.5445109018,
		road: 16452.9602314815,
		acceleration: -1.99546296296296
	},
	{
		id: 1886,
		time: 1885,
		velocity: 7.97694444444444,
		power: -7162.94358363597,
		road: 16461.7640277778,
		acceleration: -1.02574074074074
	},
	{
		id: 1887,
		time: 1886,
		velocity: 7.91666666666667,
		power: -1728.84612941438,
		road: 16469.8606018519,
		acceleration: -0.388703703703705
	},
	{
		id: 1888,
		time: 1887,
		velocity: 7.81305555555555,
		power: 2782.69091853221,
		road: 16477.86375,
		acceleration: 0.201851851851851
	},
	{
		id: 1889,
		time: 1888,
		velocity: 8.5825,
		power: 9936.92781077537,
		road: 16486.4881944445,
		acceleration: 1.04074074074074
	},
	{
		id: 1890,
		time: 1889,
		velocity: 11.0388888888889,
		power: 15889.9661261106,
		road: 16496.384212963,
		acceleration: 1.50240740740741
	},
	{
		id: 1891,
		time: 1890,
		velocity: 12.3202777777778,
		power: 20261.8449597732,
		road: 16507.8564814815,
		acceleration: 1.65009259259259
	},
	{
		id: 1892,
		time: 1891,
		velocity: 13.5327777777778,
		power: 19932.3132077892,
		road: 16520.84625,
		acceleration: 1.38490740740741
	},
	{
		id: 1893,
		time: 1892,
		velocity: 15.1936111111111,
		power: 21621.5598247781,
		road: 16535.1953703704,
		acceleration: 1.33379629629629
	},
	{
		id: 1894,
		time: 1893,
		velocity: 16.3216666666667,
		power: 20732.5416844802,
		road: 16550.7752777778,
		acceleration: 1.12777777777778
	},
	{
		id: 1895,
		time: 1894,
		velocity: 16.9161111111111,
		power: 16345.4534648709,
		road: 16567.2956944445,
		acceleration: 0.753240740740736
	},
	{
		id: 1896,
		time: 1895,
		velocity: 17.4533333333333,
		power: 15382.5454704804,
		road: 16584.5124074074,
		acceleration: 0.639351851851856
	},
	{
		id: 1897,
		time: 1896,
		velocity: 18.2397222222222,
		power: 13575.1657866483,
		road: 16602.2948148148,
		acceleration: 0.492037037037036
	},
	{
		id: 1898,
		time: 1897,
		velocity: 18.3922222222222,
		power: 10555.4586328917,
		road: 16620.4698148148,
		acceleration: 0.293148148148148
	},
	{
		id: 1899,
		time: 1898,
		velocity: 18.3327777777778,
		power: 5580.46454068484,
		road: 16638.7921759259,
		acceleration: 0.00157407407407462
	},
	{
		id: 1900,
		time: 1899,
		velocity: 18.2444444444444,
		power: 4746.71018681555,
		road: 16657.0927314815,
		acceleration: -0.0451851851851863
	},
	{
		id: 1901,
		time: 1900,
		velocity: 18.2566666666667,
		power: 5661.98126839666,
		road: 16675.3745833333,
		acceleration: 0.00777777777777899
	},
	{
		id: 1902,
		time: 1901,
		velocity: 18.3561111111111,
		power: 5996.19711226921,
		road: 16693.6734259259,
		acceleration: 0.0262037037037004
	},
	{
		id: 1903,
		time: 1902,
		velocity: 18.3230555555556,
		power: 5850.39154951494,
		road: 16711.9938888889,
		acceleration: 0.017037037037035
	},
	{
		id: 1904,
		time: 1903,
		velocity: 18.3077777777778,
		power: 5295.79394328388,
		road: 16730.3155555556,
		acceleration: -0.0146296296296242
	},
	{
		id: 1905,
		time: 1904,
		velocity: 18.3122222222222,
		power: 5529.31371355824,
		road: 16748.6293981482,
		acceleration: -0.00101851851852075
	},
	{
		id: 1906,
		time: 1905,
		velocity: 18.32,
		power: 5695.74041023828,
		road: 16766.9468981482,
		acceleration: 0.00833333333333641
	},
	{
		id: 1907,
		time: 1906,
		velocity: 18.3327777777778,
		power: 5413.40612238517,
		road: 16785.2646759259,
		acceleration: -0.00777777777777899
	},
	{
		id: 1908,
		time: 1907,
		velocity: 18.2888888888889,
		power: 5610.06786190328,
		road: 16803.5803240741,
		acceleration: 0.00351851851851492
	},
	{
		id: 1909,
		time: 1908,
		velocity: 18.3305555555556,
		power: 5203.93312393386,
		road: 16821.8880555556,
		acceleration: -0.0193518518518516
	},
	{
		id: 1910,
		time: 1909,
		velocity: 18.2747222222222,
		power: 5336.72500393552,
		road: 16840.1805092593,
		acceleration: -0.0112037037037034
	},
	{
		id: 1911,
		time: 1910,
		velocity: 18.2552777777778,
		power: 5858.04913136732,
		road: 16858.4765740741,
		acceleration: 0.018425925925925
	},
	{
		id: 1912,
		time: 1911,
		velocity: 18.3858333333333,
		power: 5647.29614566297,
		road: 16876.7848148148,
		acceleration: 0.00592592592592567
	},
	{
		id: 1913,
		time: 1912,
		velocity: 18.2925,
		power: 5694.30234760598,
		road: 16895.1001851852,
		acceleration: 0.00833333333333286
	},
	{
		id: 1914,
		time: 1913,
		velocity: 18.2802777777778,
		power: 4754.36630718037,
		road: 16913.3974074074,
		acceleration: -0.0446296296296289
	},
	{
		id: 1915,
		time: 1914,
		velocity: 18.2519444444444,
		power: 5402.42092791955,
		road: 16931.6689814815,
		acceleration: -0.00666666666666416
	},
	{
		id: 1916,
		time: 1915,
		velocity: 18.2725,
		power: 4824.55649272695,
		road: 16949.9177777778,
		acceleration: -0.0388888888888914
	},
	{
		id: 1917,
		time: 1916,
		velocity: 18.1636111111111,
		power: 2823.49853382883,
		road: 16968.0718055556,
		acceleration: -0.150648148148147
	},
	{
		id: 1918,
		time: 1917,
		velocity: 17.8,
		power: -120.936808915236,
		road: 16985.9927777778,
		acceleration: -0.315462962962961
	},
	{
		id: 1919,
		time: 1918,
		velocity: 17.3261111111111,
		power: -5105.99921749278,
		road: 17003.4535648148,
		acceleration: -0.60490740740741
	},
	{
		id: 1920,
		time: 1919,
		velocity: 16.3488888888889,
		power: -6555.09862642425,
		road: 17020.2646759259,
		acceleration: -0.694444444444443
	},
	{
		id: 1921,
		time: 1920,
		velocity: 15.7166666666667,
		power: -7929.11069897795,
		road: 17036.3339814815,
		acceleration: -0.789166666666667
	},
	{
		id: 1922,
		time: 1921,
		velocity: 14.9586111111111,
		power: -5565.06310040335,
		road: 17051.6888888889,
		acceleration: -0.639629629629631
	},
	{
		id: 1923,
		time: 1922,
		velocity: 14.43,
		power: -5607.89102986547,
		road: 17066.3997222222,
		acceleration: -0.648518518518516
	},
	{
		id: 1924,
		time: 1923,
		velocity: 13.7711111111111,
		power: -3621.98681603848,
		road: 17080.5320833334,
		acceleration: -0.508425925925927
	},
	{
		id: 1925,
		time: 1924,
		velocity: 13.4333333333333,
		power: -3096.13709941823,
		road: 17094.1751851852,
		acceleration: -0.470092592592591
	},
	{
		id: 1926,
		time: 1925,
		velocity: 13.0197222222222,
		power: -554.825338631579,
		road: 17107.447962963,
		acceleration: -0.270555555555555
	},
	{
		id: 1927,
		time: 1926,
		velocity: 12.9594444444444,
		power: 4176.79193007972,
		road: 17120.6385648148,
		acceleration: 0.106203703703702
	},
	{
		id: 1928,
		time: 1927,
		velocity: 13.7519444444444,
		power: 14252.5781777462,
		road: 17134.3116666667,
		acceleration: 0.858796296296296
	},
	{
		id: 1929,
		time: 1928,
		velocity: 15.5961111111111,
		power: 18037.4301066329,
		road: 17148.9356481482,
		acceleration: 1.04296296296296
	},
	{
		id: 1930,
		time: 1929,
		velocity: 16.0883333333333,
		power: 18036.7077100684,
		road: 17164.5527314815,
		acceleration: 0.943240740740743
	},
	{
		id: 1931,
		time: 1930,
		velocity: 16.5816666666667,
		power: 15942.0977275621,
		road: 17181.0079166667,
		acceleration: 0.732962962962961
	},
	{
		id: 1932,
		time: 1931,
		velocity: 17.795,
		power: 18859.0054102296,
		road: 17198.2535185185,
		acceleration: 0.847870370370369
	},
	{
		id: 1933,
		time: 1932,
		velocity: 18.6319444444444,
		power: 21417.9842402498,
		road: 17216.38375,
		acceleration: 0.921388888888892
	},
	{
		id: 1934,
		time: 1933,
		velocity: 19.3458333333333,
		power: 15101.3671662132,
		road: 17235.2293055556,
		acceleration: 0.509259259259256
	},
	{
		id: 1935,
		time: 1934,
		velocity: 19.3227777777778,
		power: 9726.85291552046,
		road: 17254.4264814815,
		acceleration: 0.193981481481483
	},
	{
		id: 1936,
		time: 1935,
		velocity: 19.2138888888889,
		power: 6588.14145147263,
		road: 17273.7301388889,
		acceleration: 0.0189814814814788
	},
	{
		id: 1937,
		time: 1936,
		velocity: 19.4027777777778,
		power: 6778.93995317585,
		road: 17293.0574537037,
		acceleration: 0.028333333333336
	},
	{
		id: 1938,
		time: 1937,
		velocity: 19.4077777777778,
		power: 7268.44107320382,
		road: 17312.425462963,
		acceleration: 0.0530555555555559
	},
	{
		id: 1939,
		time: 1938,
		velocity: 19.3730555555556,
		power: 6528.16479744767,
		road: 17331.8259259259,
		acceleration: 0.0118518518518513
	},
	{
		id: 1940,
		time: 1939,
		velocity: 19.4383333333333,
		power: 6524.70802212461,
		road: 17351.2379166667,
		acceleration: 0.0112037037037069
	},
	{
		id: 1941,
		time: 1940,
		velocity: 19.4413888888889,
		power: 6130.86400256034,
		road: 17370.6505092593,
		acceleration: -0.0100000000000016
	},
	{
		id: 1942,
		time: 1941,
		velocity: 19.3430555555556,
		power: 6601.13713099453,
		road: 17390.0656944445,
		acceleration: 0.0151851851851852
	},
	{
		id: 1943,
		time: 1942,
		velocity: 19.4838888888889,
		power: 6015.24623470567,
		road: 17409.4803240741,
		acceleration: -0.0162962962962965
	},
	{
		id: 1944,
		time: 1943,
		velocity: 19.3925,
		power: 6521.26451602124,
		road: 17428.8923148148,
		acceleration: 0.0110185185185188
	},
	{
		id: 1945,
		time: 1944,
		velocity: 19.3761111111111,
		power: 5697.48492983746,
		road: 17448.2933796296,
		acceleration: -0.0328703703703717
	},
	{
		id: 1946,
		time: 1945,
		velocity: 19.3852777777778,
		power: 5802.23194228514,
		road: 17467.6649537037,
		acceleration: -0.0261111111111063
	},
	{
		id: 1947,
		time: 1946,
		velocity: 19.3141666666667,
		power: 5941.19246033161,
		road: 17487.0145833333,
		acceleration: -0.0177777777777806
	},
	{
		id: 1948,
		time: 1947,
		velocity: 19.3227777777778,
		power: 5080.22356056224,
		road: 17506.3239351852,
		acceleration: -0.0627777777777787
	},
	{
		id: 1949,
		time: 1948,
		velocity: 19.1969444444444,
		power: 6963.67812199437,
		road: 17525.6216666667,
		acceleration: 0.0395370370370358
	},
	{
		id: 1950,
		time: 1949,
		velocity: 19.4327777777778,
		power: 5843.92091633324,
		road: 17544.9284722222,
		acceleration: -0.021388888888886
	},
	{
		id: 1951,
		time: 1950,
		velocity: 19.2586111111111,
		power: 7905.55660325033,
		road: 17564.2688888889,
		acceleration: 0.0886111111111099
	},
	{
		id: 1952,
		time: 1951,
		velocity: 19.4627777777778,
		power: 5896.85489341403,
		road: 17583.6430555556,
		acceleration: -0.0211111111111109
	},
	{
		id: 1953,
		time: 1952,
		velocity: 19.3694444444444,
		power: 6745.29971501943,
		road: 17603.0189351852,
		acceleration: 0.0245370370370352
	},
	{
		id: 1954,
		time: 1953,
		velocity: 19.3322222222222,
		power: 5908.05161279159,
		road: 17622.3967592593,
		acceleration: -0.020648148148144
	},
	{
		id: 1955,
		time: 1954,
		velocity: 19.4008333333333,
		power: 6411.65127763596,
		road: 17641.7676388889,
		acceleration: 0.00675925925925824
	},
	{
		id: 1956,
		time: 1955,
		velocity: 19.3897222222222,
		power: 6839.33435559561,
		road: 17661.1564351852,
		acceleration: 0.0290740740740745
	},
	{
		id: 1957,
		time: 1956,
		velocity: 19.4194444444444,
		power: 6583.89522463962,
		road: 17680.5669907408,
		acceleration: 0.0144444444444396
	},
	{
		id: 1958,
		time: 1957,
		velocity: 19.4441666666667,
		power: 6278.47948766171,
		road: 17699.9836574074,
		acceleration: -0.00222222222222257
	},
	{
		id: 1959,
		time: 1958,
		velocity: 19.3830555555556,
		power: 6234.71283328672,
		road: 17719.3969907408,
		acceleration: -0.00444444444444514
	},
	{
		id: 1960,
		time: 1959,
		velocity: 19.4061111111111,
		power: 5565.07964991801,
		road: 17738.7882870371,
		acceleration: -0.0396296296296264
	},
	{
		id: 1961,
		time: 1960,
		velocity: 19.3252777777778,
		power: 6020.4668477036,
		road: 17758.1527314815,
		acceleration: -0.0140740740740775
	},
	{
		id: 1962,
		time: 1961,
		velocity: 19.3408333333333,
		power: 5797.00459403945,
		road: 17777.4974537037,
		acceleration: -0.0253703703703678
	},
	{
		id: 1963,
		time: 1962,
		velocity: 19.33,
		power: 6542.94425055734,
		road: 17796.8370370371,
		acceleration: 0.0150925925925947
	},
	{
		id: 1964,
		time: 1963,
		velocity: 19.3705555555556,
		power: 6554.1888532122,
		road: 17816.191712963,
		acceleration: 0.0150925925925911
	},
	{
		id: 1965,
		time: 1964,
		velocity: 19.3861111111111,
		power: 6893.31882223494,
		road: 17835.5701388889,
		acceleration: 0.0324074074074083
	},
	{
		id: 1966,
		time: 1965,
		velocity: 19.4272222222222,
		power: 6314.15869588272,
		road: 17854.9650462963,
		acceleration: 0.000555555555557419
	},
	{
		id: 1967,
		time: 1966,
		velocity: 19.3722222222222,
		power: 6625.20124775538,
		road: 17874.3687037037,
		acceleration: 0.0169444444444409
	},
	{
		id: 1968,
		time: 1967,
		velocity: 19.4369444444444,
		power: 5963.74063941555,
		road: 17893.7715277778,
		acceleration: -0.0186111111111131
	},
	{
		id: 1969,
		time: 1968,
		velocity: 19.3713888888889,
		power: 6476.51059762831,
		road: 17913.1696296297,
		acceleration: 0.00916666666666899
	},
	{
		id: 1970,
		time: 1969,
		velocity: 19.3997222222222,
		power: 6655.42209546142,
		road: 17932.5814351852,
		acceleration: 0.0182407407407403
	},
	{
		id: 1971,
		time: 1970,
		velocity: 19.4916666666667,
		power: 6384.42734207789,
		road: 17952.0039814815,
		acceleration: 0.00324074074074332
	},
	{
		id: 1972,
		time: 1971,
		velocity: 19.3811111111111,
		power: 7409.17253488369,
		road: 17971.4566666667,
		acceleration: 0.0570370370370377
	},
	{
		id: 1973,
		time: 1972,
		velocity: 19.5708333333333,
		power: 7940.82133235778,
		road: 17990.9791203704,
		acceleration: 0.0824999999999996
	},
	{
		id: 1974,
		time: 1973,
		velocity: 19.7391666666667,
		power: 7548.75328328883,
		road: 18010.5720833334,
		acceleration: 0.0585185185185182
	},
	{
		id: 1975,
		time: 1974,
		velocity: 19.5566666666667,
		power: 5613.98261401824,
		road: 18030.1718981482,
		acceleration: -0.0448148148148135
	},
	{
		id: 1976,
		time: 1975,
		velocity: 19.4363888888889,
		power: 4212.16837617649,
		road: 18049.6909259259,
		acceleration: -0.116759259259261
	},
	{
		id: 1977,
		time: 1976,
		velocity: 19.3888888888889,
		power: 5611.87379414557,
		road: 18069.1320833334,
		acceleration: -0.038981481481482
	},
	{
		id: 1978,
		time: 1977,
		velocity: 19.4397222222222,
		power: 5837.21999859568,
		road: 18088.5409259259,
		acceleration: -0.0256481481481501
	},
	{
		id: 1979,
		time: 1978,
		velocity: 19.3594444444444,
		power: 6625.82660548019,
		road: 18107.9454166667,
		acceleration: 0.0169444444444444
	},
	{
		id: 1980,
		time: 1979,
		velocity: 19.4397222222222,
		power: 6155.58870114573,
		road: 18127.3541203704,
		acceleration: -0.00851851851851393
	},
	{
		id: 1981,
		time: 1980,
		velocity: 19.4141666666667,
		power: 6777.97972907769,
		road: 18146.7708796297,
		acceleration: 0.0246296296296258
	},
	{
		id: 1982,
		time: 1981,
		velocity: 19.4333333333333,
		power: 6230.53604503901,
		road: 18166.1973611111,
		acceleration: -0.00518518518518363
	},
	{
		id: 1983,
		time: 1982,
		velocity: 19.4241666666667,
		power: 6449.8782184171,
		road: 18185.6245370371,
		acceleration: 0.00657407407407362
	},
	{
		id: 1984,
		time: 1983,
		velocity: 19.4338888888889,
		power: 6831.17695613911,
		road: 18205.0681944445,
		acceleration: 0.0263888888888886
	},
	{
		id: 1985,
		time: 1984,
		velocity: 19.5125,
		power: 6731.49097846578,
		road: 18224.5350925926,
		acceleration: 0.0200925925925937
	},
	{
		id: 1986,
		time: 1985,
		velocity: 19.4844444444444,
		power: 6394.17084521735,
		road: 18244.0128240741,
		acceleration: 0.00157407407407106
	},
	{
		id: 1987,
		time: 1986,
		velocity: 19.4386111111111,
		power: 5902.29233316074,
		road: 18263.4791666667,
		acceleration: -0.0243518518518506
	},
	{
		id: 1988,
		time: 1987,
		velocity: 19.4394444444444,
		power: 5566.95931684314,
		road: 18282.9127777778,
		acceleration: -0.0411111111111104
	},
	{
		id: 1989,
		time: 1988,
		velocity: 19.3611111111111,
		power: 6197.78008403983,
		road: 18302.3226851852,
		acceleration: -0.00629629629629491
	},
	{
		id: 1990,
		time: 1989,
		velocity: 19.4197222222222,
		power: 6553.08589983233,
		road: 18321.7357870371,
		acceleration: 0.0126851851851875
	},
	{
		id: 1991,
		time: 1990,
		velocity: 19.4775,
		power: 6539.71708387456,
		road: 18341.1609722222,
		acceleration: 0.011481481481475
	},
	{
		id: 1992,
		time: 1991,
		velocity: 19.3955555555556,
		power: 6040.42273827366,
		road: 18360.5842592593,
		acceleration: -0.0152777777777757
	},
	{
		id: 1993,
		time: 1992,
		velocity: 19.3738888888889,
		power: 5831.19737227148,
		road: 18379.9870370371,
		acceleration: -0.0257407407407406
	},
	{
		id: 1994,
		time: 1993,
		velocity: 19.4002777777778,
		power: 6415.93421731205,
		road: 18399.38,
		acceleration: 0.00611111111111029
	},
	{
		id: 1995,
		time: 1994,
		velocity: 19.4138888888889,
		power: 6580.17797917621,
		road: 18418.7832870371,
		acceleration: 0.0145370370370408
	},
	{
		id: 1996,
		time: 1995,
		velocity: 19.4175,
		power: 5906.49287044449,
		road: 18438.1830555556,
		acceleration: -0.0215740740740777
	},
	{
		id: 1997,
		time: 1996,
		velocity: 19.3355555555556,
		power: 6075.09799111942,
		road: 18457.5661111111,
		acceleration: -0.0118518518518478
	},
	{
		id: 1998,
		time: 1997,
		velocity: 19.3783333333333,
		power: 5987.74850962009,
		road: 18476.9352314815,
		acceleration: -0.0160185185185213
	},
	{
		id: 1999,
		time: 1998,
		velocity: 19.3694444444444,
		power: 6867.91488827138,
		road: 18496.3118981482,
		acceleration: 0.0311111111111089
	},
	{
		id: 2000,
		time: 1999,
		velocity: 19.4288888888889,
		power: 6165.08803912718,
		road: 18515.7005092593,
		acceleration: -0.00722222222221802
	},
	{
		id: 2001,
		time: 2000,
		velocity: 19.3566666666667,
		power: 6398.27700760786,
		road: 18535.0881944445,
		acceleration: 0.0053703703703718
	},
	{
		id: 2002,
		time: 2001,
		velocity: 19.3855555555556,
		power: 6209.32680891342,
		road: 18554.4761574074,
		acceleration: -0.00481481481481794
	},
	{
		id: 2003,
		time: 2002,
		velocity: 19.4144444444444,
		power: 6782.97219359949,
		road: 18573.8745370371,
		acceleration: 0.0256481481481465
	},
	{
		id: 2004,
		time: 2003,
		velocity: 19.4336111111111,
		power: 6929.04724272171,
		road: 18593.3018981482,
		acceleration: 0.0323148148148142
	},
	{
		id: 2005,
		time: 2004,
		velocity: 19.4825,
		power: 5889.62324232773,
		road: 18612.7335648148,
		acceleration: -0.0237037037037027
	},
	{
		id: 2006,
		time: 2005,
		velocity: 19.3433333333333,
		power: 5578.14112773901,
		road: 18632.13375,
		acceleration: -0.0392592592592571
	},
	{
		id: 2007,
		time: 2006,
		velocity: 19.3158333333333,
		power: 4023.53190975467,
		road: 18651.4541666667,
		acceleration: -0.12027777777778
	},
	{
		id: 2008,
		time: 2007,
		velocity: 19.1216666666667,
		power: 495.631471507274,
		road: 18670.5614351852,
		acceleration: -0.306018518518517
	},
	{
		id: 2009,
		time: 2008,
		velocity: 18.4252777777778,
		power: -2089.76418094832,
		road: 18689.2948611111,
		acceleration: -0.441666666666666
	},
	{
		id: 2010,
		time: 2009,
		velocity: 17.9908333333333,
		power: -5283.62054919438,
		road: 18707.49875,
		acceleration: -0.617407407407409
	},
	{
		id: 2011,
		time: 2010,
		velocity: 17.2694444444444,
		power: -3293.30189135929,
		road: 18725.1449537037,
		acceleration: -0.497962962962962
	},
	{
		id: 2012,
		time: 2011,
		velocity: 16.9313888888889,
		power: -3447.74956712984,
		road: 18742.2905092593,
		acceleration: -0.50333333333333
	},
	{
		id: 2013,
		time: 2012,
		velocity: 16.4808333333333,
		power: -1650.33968768093,
		road: 18758.9904166667,
		acceleration: -0.387962962962966
	},
	{
		id: 2014,
		time: 2013,
		velocity: 16.1055555555556,
		power: -3651.21792557847,
		road: 18775.2406944445,
		acceleration: -0.511296296296297
	},
	{
		id: 2015,
		time: 2014,
		velocity: 15.3975,
		power: -4151.6037822978,
		road: 18790.9637962963,
		acceleration: -0.543055555555554
	},
	{
		id: 2016,
		time: 2015,
		velocity: 14.8516666666667,
		power: -6998.71981519875,
		road: 18806.0446759259,
		acceleration: -0.74138888888889
	},
	{
		id: 2017,
		time: 2016,
		velocity: 13.8813888888889,
		power: -11352.2818583297,
		road: 18820.2150462963,
		acceleration: -1.07962962962963
	},
	{
		id: 2018,
		time: 2017,
		velocity: 12.1586111111111,
		power: -16667.4685669351,
		road: 18833.0551388889,
		acceleration: -1.58092592592592
	},
	{
		id: 2019,
		time: 2018,
		velocity: 10.1088888888889,
		power: -16718.1452612512,
		road: 18844.220462963,
		acceleration: -1.76861111111111
	},
	{
		id: 2020,
		time: 2019,
		velocity: 8.57555555555555,
		power: -14956.0491783213,
		road: 18853.5734259259,
		acceleration: -1.85611111111111
	},
	{
		id: 2021,
		time: 2020,
		velocity: 6.59027777777778,
		power: -8120.01272960908,
		road: 18861.3701851852,
		acceleration: -1.2562962962963
	},
	{
		id: 2022,
		time: 2021,
		velocity: 6.34,
		power: -2969.88134764752,
		road: 18868.2343518519,
		acceleration: -0.608888888888889
	},
	{
		id: 2023,
		time: 2022,
		velocity: 6.74888888888889,
		power: 4316.63111378445,
		road: 18875.0502314815,
		acceleration: 0.512314814814815
	},
	{
		id: 2024,
		time: 2023,
		velocity: 8.12722222222222,
		power: 8990.61938285641,
		road: 18882.6627777778,
		acceleration: 1.08101851851852
	},
	{
		id: 2025,
		time: 2024,
		velocity: 9.58305555555556,
		power: 14122.4878762287,
		road: 18891.5628703704,
		acceleration: 1.49407407407407
	},
	{
		id: 2026,
		time: 2025,
		velocity: 11.2311111111111,
		power: 13042.8539123186,
		road: 18901.7861574074,
		acceleration: 1.15231481481482
	},
	{
		id: 2027,
		time: 2026,
		velocity: 11.5841666666667,
		power: 10997.4067095306,
		road: 18913.0006481482,
		acceleration: 0.830092592592592
	},
	{
		id: 2028,
		time: 2027,
		velocity: 12.0733333333333,
		power: 9337.93554589842,
		road: 18924.9363888889,
		acceleration: 0.612407407407407
	},
	{
		id: 2029,
		time: 2028,
		velocity: 13.0683333333333,
		power: 13286.8466706288,
		road: 18937.6181944445,
		acceleration: 0.87972222222222
	},
	{
		id: 2030,
		time: 2029,
		velocity: 14.2233333333333,
		power: 17991.438721107,
		road: 18951.31125,
		acceleration: 1.14277777777778
	},
	{
		id: 2031,
		time: 2030,
		velocity: 15.5016666666667,
		power: 17143.601993542,
		road: 18966.0589814815,
		acceleration: 0.966574074074074
	},
	{
		id: 2032,
		time: 2031,
		velocity: 15.9680555555556,
		power: 11907.1867290367,
		road: 18981.5603703704,
		acceleration: 0.540740740740739
	},
	{
		id: 2033,
		time: 2032,
		velocity: 15.8455555555556,
		power: 2736.38675344417,
		road: 18997.2896759259,
		acceleration: -0.0849074074074068
	},
	{
		id: 2034,
		time: 2033,
		velocity: 15.2469444444444,
		power: -2204.8778722453,
		road: 19012.7707407408,
		acceleration: -0.411574074074071
	},
	{
		id: 2035,
		time: 2034,
		velocity: 14.7333333333333,
		power: -3683.35068085526,
		road: 19027.7903703704,
		acceleration: -0.511296296296299
	},
	{
		id: 2036,
		time: 2035,
		velocity: 14.3116666666667,
		power: -1971.75188202984,
		road: 19042.3599537037,
		acceleration: -0.388796296296297
	},
	{
		id: 2037,
		time: 2036,
		velocity: 14.0805555555556,
		power: -622.225217967278,
		road: 19056.5914351852,
		acceleration: -0.287407407407407
	},
	{
		id: 2038,
		time: 2037,
		velocity: 13.8711111111111,
		power: -739.77618446112,
		road: 19070.5329166667,
		acceleration: -0.292592592592591
	},
	{
		id: 2039,
		time: 2038,
		velocity: 13.4338888888889,
		power: -949.553351500495,
		road: 19084.175462963,
		acceleration: -0.305277777777777
	},
	{
		id: 2040,
		time: 2039,
		velocity: 13.1647222222222,
		power: 616.623331043345,
		road: 19097.5751388889,
		acceleration: -0.180462962962963
	},
	{
		id: 2041,
		time: 2040,
		velocity: 13.3297222222222,
		power: 4424.82280049135,
		road: 19110.9438425926,
		acceleration: 0.118518518518519
	},
	{
		id: 2042,
		time: 2041,
		velocity: 13.7894444444444,
		power: 6017.53138006182,
		road: 19124.4890277778,
		acceleration: 0.234444444444444
	},
	{
		id: 2043,
		time: 2042,
		velocity: 13.8680555555556,
		power: 5158.81899980449,
		road: 19138.2310648148,
		acceleration: 0.15925925925926
	},
	{
		id: 2044,
		time: 2043,
		velocity: 13.8075,
		power: 2783.17392938779,
		road: 19152.0407870371,
		acceleration: -0.0238888888888908
	},
	{
		id: 2045,
		time: 2044,
		velocity: 13.7177777777778,
		power: 2866.28017566374,
		road: 19165.8300925926,
		acceleration: -0.0169444444444444
	},
	{
		id: 2046,
		time: 2045,
		velocity: 13.8172222222222,
		power: 3679.84639866709,
		road: 19179.6331018519,
		acceleration: 0.044351851851852
	},
	{
		id: 2047,
		time: 2046,
		velocity: 13.9405555555556,
		power: 4792.1715580871,
		road: 19193.5208796297,
		acceleration: 0.125185185185186
	},
	{
		id: 2048,
		time: 2047,
		velocity: 14.0933333333333,
		power: 4562.4647368688,
		road: 19207.5228703704,
		acceleration: 0.103240740740739
	},
	{
		id: 2049,
		time: 2048,
		velocity: 14.1269444444444,
		power: 4515.35903487454,
		road: 19221.6243518519,
		acceleration: 0.0957407407407427
	},
	{
		id: 2050,
		time: 2049,
		velocity: 14.2277777777778,
		power: 3326.19166527102,
		road: 19235.7765740741,
		acceleration: 0.00574074074073749
	},
	{
		id: 2051,
		time: 2050,
		velocity: 14.1105555555556,
		power: 3558.06022039268,
		road: 19249.9428703704,
		acceleration: 0.0224074074074103
	},
	{
		id: 2052,
		time: 2051,
		velocity: 14.1941666666667,
		power: 4574.5229126093,
		road: 19264.167962963,
		acceleration: 0.0951851851851835
	},
	{
		id: 2053,
		time: 2052,
		velocity: 14.5133333333333,
		power: 6559.12548700571,
		road: 19278.5571759259,
		acceleration: 0.233055555555557
	},
	{
		id: 2054,
		time: 2053,
		velocity: 14.8097222222222,
		power: 6930.3387909552,
		road: 19293.1868055556,
		acceleration: 0.247777777777777
	},
	{
		id: 2055,
		time: 2054,
		velocity: 14.9375,
		power: 4560.86952738637,
		road: 19307.9763425926,
		acceleration: 0.0720370370370365
	},
	{
		id: 2056,
		time: 2055,
		velocity: 14.7294444444444,
		power: 2217.4427615212,
		road: 19322.7551388889,
		acceleration: -0.0935185185185183
	},
	{
		id: 2057,
		time: 2056,
		velocity: 14.5291666666667,
		power: 1476.98412661286,
		road: 19337.4156018519,
		acceleration: -0.143148148148148
	},
	{
		id: 2058,
		time: 2057,
		velocity: 14.5080555555556,
		power: 2397.34816357952,
		road: 19351.9673148148,
		acceleration: -0.0743518518518513
	},
	{
		id: 2059,
		time: 2058,
		velocity: 14.5063888888889,
		power: 3746.72448468369,
		road: 19366.4936574074,
		acceleration: 0.0236111111111104
	},
	{
		id: 2060,
		time: 2059,
		velocity: 14.6,
		power: 5389.88125806061,
		road: 19381.1010648148,
		acceleration: 0.13851851851852
	},
	{
		id: 2061,
		time: 2060,
		velocity: 14.9236111111111,
		power: 6377.82296122557,
		road: 19395.8783333334,
		acceleration: 0.201203703703701
	},
	{
		id: 2062,
		time: 2061,
		velocity: 15.11,
		power: 5864.758609343,
		road: 19410.8346296296,
		acceleration: 0.156851851851853
	},
	{
		id: 2063,
		time: 2062,
		velocity: 15.0705555555556,
		power: 2975.24881732316,
		road: 19425.8458333334,
		acceleration: -0.0470370370370361
	},
	{
		id: 2064,
		time: 2063,
		velocity: 14.7825,
		power: 1594.38999345866,
		road: 19440.7630092593,
		acceleration: -0.141018518518521
	},
	{
		id: 2065,
		time: 2064,
		velocity: 14.6869444444444,
		power: 1490.33639859722,
		road: 19455.5372222222,
		acceleration: -0.144907407407407
	},
	{
		id: 2066,
		time: 2065,
		velocity: 14.6358333333333,
		power: 2970.37603759636,
		road: 19470.2203703704,
		acceleration: -0.0372222222222209
	},
	{
		id: 2067,
		time: 2066,
		velocity: 14.6708333333333,
		power: 2862.10535921789,
		road: 19484.8630555556,
		acceleration: -0.043703703703704
	},
	{
		id: 2068,
		time: 2067,
		velocity: 14.5558333333333,
		power: 3153.08504280181,
		road: 19499.472962963,
		acceleration: -0.0218518518518493
	},
	{
		id: 2069,
		time: 2068,
		velocity: 14.5702777777778,
		power: 2321.66217583579,
		road: 19514.0319444445,
		acceleration: -0.0800000000000001
	},
	{
		id: 2070,
		time: 2069,
		velocity: 14.4308333333333,
		power: 2534.94837024226,
		road: 19528.5196296296,
		acceleration: -0.0625925925925923
	},
	{
		id: 2071,
		time: 2070,
		velocity: 14.3680555555556,
		power: 1703.85307617578,
		road: 19542.9158333334,
		acceleration: -0.12037037037037
	},
	{
		id: 2072,
		time: 2071,
		velocity: 14.2091666666667,
		power: 3420.49762793084,
		road: 19557.2550462963,
		acceleration: 0.00638888888888722
	},
	{
		id: 2073,
		time: 2072,
		velocity: 14.45,
		power: 3915.59684585354,
		road: 19571.6182870371,
		acceleration: 0.0416666666666661
	},
	{
		id: 2074,
		time: 2073,
		velocity: 14.4930555555556,
		power: 4503.98992398391,
		road: 19586.0434259259,
		acceleration: 0.0821296296296303
	},
	{
		id: 2075,
		time: 2074,
		velocity: 14.4555555555556,
		power: 3642.46111905688,
		road: 19600.5185648148,
		acceleration: 0.0178703703703693
	},
	{
		id: 2076,
		time: 2075,
		velocity: 14.5036111111111,
		power: 2568.91932812618,
		road: 19614.9730555556,
		acceleration: -0.0591666666666644
	},
	{
		id: 2077,
		time: 2076,
		velocity: 14.3155555555556,
		power: 689.87014363745,
		road: 19629.3015740741,
		acceleration: -0.192777777777778
	},
	{
		id: 2078,
		time: 2077,
		velocity: 13.8772222222222,
		power: -1751.41781294139,
		road: 19643.3490740741,
		acceleration: -0.369259259259263
	},
	{
		id: 2079,
		time: 2078,
		velocity: 13.3958333333333,
		power: -3353.0040367484,
		road: 19656.9669907408,
		acceleration: -0.489907407407404
	},
	{
		id: 2080,
		time: 2079,
		velocity: 12.8458333333333,
		power: -2616.81034949078,
		road: 19670.1232407408,
		acceleration: -0.433425925925928
	},
	{
		id: 2081,
		time: 2080,
		velocity: 12.5769444444444,
		power: -8617.81298955152,
		road: 19682.5930092593,
		acceleration: -0.939537037037036
	},
	{
		id: 2082,
		time: 2081,
		velocity: 10.5772222222222,
		power: -12499.619976307,
		road: 19693.9140740741,
		acceleration: -1.35787037037037
	},
	{
		id: 2083,
		time: 2082,
		velocity: 8.77222222222222,
		power: -12573.5727023444,
		road: 19703.7971296297,
		acceleration: -1.51814814814815
	},
	{
		id: 2084,
		time: 2083,
		velocity: 8.0225,
		power: -6802.44673294364,
		road: 19712.4221759259,
		acceleration: -0.997870370370372
	},
	{
		id: 2085,
		time: 2084,
		velocity: 7.58361111111111,
		power: -4026.98858239428,
		road: 19720.1953240741,
		acceleration: -0.705925925925924
	},
	{
		id: 2086,
		time: 2085,
		velocity: 6.65444444444444,
		power: -3812.89559415726,
		road: 19727.2537962963,
		acceleration: -0.723425925925926
	},
	{
		id: 2087,
		time: 2086,
		velocity: 5.85222222222222,
		power: -4570.36372631861,
		road: 19733.490462963,
		acceleration: -0.920185185185185
	},
	{
		id: 2088,
		time: 2087,
		velocity: 4.82305555555556,
		power: -4792.87419945743,
		road: 19738.7126388889,
		acceleration: -1.1087962962963
	},
	{
		id: 2089,
		time: 2088,
		velocity: 3.32805555555556,
		power: -3681.9555891289,
		road: 19742.8424074074,
		acceleration: -1.07601851851852
	},
	{
		id: 2090,
		time: 2089,
		velocity: 2.62416666666667,
		power: -2494.91846314745,
		road: 19745.9438888889,
		acceleration: -0.980555555555556
	},
	{
		id: 2091,
		time: 2090,
		velocity: 1.88138888888889,
		power: -1253.1759115171,
		road: 19748.196712963,
		acceleration: -0.716759259259259
	},
	{
		id: 2092,
		time: 2091,
		velocity: 1.17777777777778,
		power: -823.088278216711,
		road: 19749.746712963,
		acceleration: -0.688888888888889
	},
	{
		id: 2093,
		time: 2092,
		velocity: 0.5575,
		power: -422.021585463376,
		road: 19750.6387037037,
		acceleration: -0.62712962962963
	},
	{
		id: 2094,
		time: 2093,
		velocity: 0,
		power: -95.9501447619798,
		road: 19751.0208333334,
		acceleration: -0.392592592592593
	},
	{
		id: 2095,
		time: 2094,
		velocity: 0,
		power: -5.13222763157895,
		road: 19751.11375,
		acceleration: -0.185833333333333
	},
	{
		id: 2096,
		time: 2095,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2097,
		time: 2096,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2098,
		time: 2097,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2099,
		time: 2098,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2100,
		time: 2099,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2101,
		time: 2100,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2102,
		time: 2101,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2103,
		time: 2102,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2104,
		time: 2103,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2105,
		time: 2104,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2106,
		time: 2105,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2107,
		time: 2106,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2108,
		time: 2107,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2109,
		time: 2108,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2110,
		time: 2109,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2111,
		time: 2110,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2112,
		time: 2111,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2113,
		time: 2112,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2114,
		time: 2113,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2115,
		time: 2114,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2116,
		time: 2115,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2117,
		time: 2116,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2118,
		time: 2117,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2119,
		time: 2118,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2120,
		time: 2119,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2121,
		time: 2120,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2122,
		time: 2121,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2123,
		time: 2122,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2124,
		time: 2123,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2125,
		time: 2124,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2126,
		time: 2125,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2127,
		time: 2126,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2128,
		time: 2127,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2129,
		time: 2128,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2130,
		time: 2129,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2131,
		time: 2130,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2132,
		time: 2131,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2133,
		time: 2132,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2134,
		time: 2133,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2135,
		time: 2134,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2136,
		time: 2135,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2137,
		time: 2136,
		velocity: 0,
		power: 0,
		road: 19751.11375,
		acceleration: 0
	},
	{
		id: 2138,
		time: 2137,
		velocity: 0,
		power: 41.5731670672036,
		road: 19751.2333796297,
		acceleration: 0.239259259259259
	},
	{
		id: 2139,
		time: 2138,
		velocity: 0.717777777777778,
		power: 150.859069025829,
		road: 19751.6165740741,
		acceleration: 0.28787037037037
	},
	{
		id: 2140,
		time: 2139,
		velocity: 0.863611111111111,
		power: 564.503757915045,
		road: 19752.4409722222,
		acceleration: 0.594537037037037
	},
	{
		id: 2141,
		time: 2140,
		velocity: 1.78361111111111,
		power: 1234.69263944703,
		road: 19753.9342592593,
		acceleration: 0.743240740740741
	},
	{
		id: 2142,
		time: 2141,
		velocity: 2.9475,
		power: 3276.23765876519,
		road: 19756.4264351852,
		acceleration: 1.25453703703704
	},
	{
		id: 2143,
		time: 2142,
		velocity: 4.62722222222222,
		power: 4961.42268541357,
		road: 19760.1751388889,
		acceleration: 1.25851851851852
	},
	{
		id: 2144,
		time: 2143,
		velocity: 5.55916666666667,
		power: 4813.87046821831,
		road: 19765.0071296297,
		acceleration: 0.908055555555555
	},
	{
		id: 2145,
		time: 2144,
		velocity: 5.67166666666667,
		power: 2474.65641100564,
		road: 19770.4598611111,
		acceleration: 0.333425925925926
	},
	{
		id: 2146,
		time: 2145,
		velocity: 5.6275,
		power: 1119.36980273566,
		road: 19776.1108333334,
		acceleration: 0.0630555555555556
	},
	{
		id: 2147,
		time: 2146,
		velocity: 5.74833333333333,
		power: 1847.18929122572,
		road: 19781.888425926,
		acceleration: 0.190185185185186
	},
	{
		id: 2148,
		time: 2147,
		velocity: 6.24222222222222,
		power: 4585.59957439407,
		road: 19788.0763425926,
		acceleration: 0.630462962962963
	},
	{
		id: 2149,
		time: 2148,
		velocity: 7.51888888888889,
		power: 7047.57206872456,
		road: 19795.0345370371,
		acceleration: 0.910092592592591
	},
	{
		id: 2150,
		time: 2149,
		velocity: 8.47861111111111,
		power: 9645.51938663302,
		road: 19803.0021296297,
		acceleration: 1.1087037037037
	},
	{
		id: 2151,
		time: 2150,
		velocity: 9.56833333333333,
		power: 11513.4706771848,
		road: 19812.1012962963,
		acceleration: 1.15444444444445
	},
	{
		id: 2152,
		time: 2151,
		velocity: 10.9822222222222,
		power: 8483.65965390593,
		road: 19822.1295370371,
		acceleration: 0.703703703703702
	},
	{
		id: 2153,
		time: 2152,
		velocity: 10.5897222222222,
		power: 3475.41816564727,
		road: 19832.5893981482,
		acceleration: 0.159537037037037
	},
	{
		id: 2154,
		time: 2153,
		velocity: 10.0469444444444,
		power: -4304.34126683941,
		road: 19842.8149537037,
		acceleration: -0.628148148148147
	},
	{
		id: 2155,
		time: 2154,
		velocity: 9.09777777777778,
		power: -5435.69913155965,
		road: 19852.3375,
		acceleration: -0.777870370370371
	},
	{
		id: 2156,
		time: 2155,
		velocity: 8.25611111111111,
		power: -3575.70989596142,
		road: 19861.1728703704,
		acceleration: -0.596481481481479
	},
	{
		id: 2157,
		time: 2156,
		velocity: 8.2575,
		power: -2195.73309997035,
		road: 19869.4881018519,
		acceleration: -0.443796296296297
	},
	{
		id: 2158,
		time: 2157,
		velocity: 7.76638888888889,
		power: -191.538719602028,
		road: 19877.4871296296,
		acceleration: -0.188611111111111
	},
	{
		id: 2159,
		time: 2158,
		velocity: 7.69027777777778,
		power: 875.70019180953,
		road: 19885.3690277778,
		acceleration: -0.045648148148147
	},
	{
		id: 2160,
		time: 2159,
		velocity: 8.12055555555556,
		power: 1497.61965947285,
		road: 19893.2468055556,
		acceleration: 0.0374074074074056
	},
	{
		id: 2161,
		time: 2160,
		velocity: 7.87861111111111,
		power: -2125.34401742654,
		road: 19900.9174074074,
		acceleration: -0.451759259259259
	},
	{
		id: 2162,
		time: 2161,
		velocity: 6.335,
		power: -5021.69504473078,
		road: 19907.9069444445,
		acceleration: -0.91037037037037
	},
	{
		id: 2163,
		time: 2162,
		velocity: 5.38944444444444,
		power: -7112.91790553825,
		road: 19913.7248148148,
		acceleration: -1.43296296296296
	},
	{
		id: 2164,
		time: 2163,
		velocity: 3.57972222222222,
		power: -3637.62669532415,
		road: 19918.341712963,
		acceleration: -0.968981481481482
	},
	{
		id: 2165,
		time: 2164,
		velocity: 3.42805555555556,
		power: -1646.23217484393,
		road: 19922.1804166667,
		acceleration: -0.587407407407407
	},
	{
		id: 2166,
		time: 2165,
		velocity: 3.62722222222222,
		power: 579.751656473416,
		road: 19925.7438425926,
		acceleration: 0.0368518518518517
	},
	{
		id: 2167,
		time: 2166,
		velocity: 3.69027777777778,
		power: -286.466282340963,
		road: 19929.2151388889,
		acceleration: -0.221111111111111
	},
	{
		id: 2168,
		time: 2167,
		velocity: 2.76472222222222,
		power: -2125.21859807104,
		road: 19932.1249074074,
		acceleration: -0.901944444444445
	},
	{
		id: 2169,
		time: 2168,
		velocity: 0.921388888888889,
		power: -1924.34658840087,
		road: 19933.9686574074,
		acceleration: -1.23009259259259
	},
	{
		id: 2170,
		time: 2169,
		velocity: 0,
		power: -577.627740241875,
		road: 19934.7365740741,
		acceleration: -0.921574074074074
	},
	{
		id: 2171,
		time: 2170,
		velocity: 0,
		power: -26.1285952404159,
		road: 19934.8901388889,
		acceleration: -0.30712962962963
	},
	{
		id: 2172,
		time: 2171,
		velocity: 0,
		power: 15.0192673945102,
		road: 19934.9528240741,
		acceleration: 0.12537037037037
	},
	{
		id: 2173,
		time: 2172,
		velocity: 0.376111111111111,
		power: 180.379739149946,
		road: 19935.2936574074,
		acceleration: 0.430925925925926
	},
	{
		id: 2174,
		time: 2173,
		velocity: 1.29277777777778,
		power: 623.985184747188,
		road: 19936.1641203704,
		acceleration: 0.628333333333333
	},
	{
		id: 2175,
		time: 2174,
		velocity: 1.885,
		power: 836.564722047666,
		road: 19937.5931481482,
		acceleration: 0.488796296296296
	},
	{
		id: 2176,
		time: 2175,
		velocity: 1.8425,
		power: -417.837069589094,
		road: 19939.0511111111,
		acceleration: -0.430925925925926
	},
	{
		id: 2177,
		time: 2176,
		velocity: 0,
		power: -440.253256757058,
		road: 19939.9794444445,
		acceleration: -0.628333333333333
	},
	{
		id: 2178,
		time: 2177,
		velocity: 0,
		power: -141.572851315789,
		road: 19940.2865277778,
		acceleration: -0.614166666666667
	},
	{
		id: 2179,
		time: 2178,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2180,
		time: 2179,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2181,
		time: 2180,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2182,
		time: 2181,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2183,
		time: 2182,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2184,
		time: 2183,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2185,
		time: 2184,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2186,
		time: 2185,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2187,
		time: 2186,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2188,
		time: 2187,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2189,
		time: 2188,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2190,
		time: 2189,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2191,
		time: 2190,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2192,
		time: 2191,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2193,
		time: 2192,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2194,
		time: 2193,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2195,
		time: 2194,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2196,
		time: 2195,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2197,
		time: 2196,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2198,
		time: 2197,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2199,
		time: 2198,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2200,
		time: 2199,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2201,
		time: 2200,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2202,
		time: 2201,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2203,
		time: 2202,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2204,
		time: 2203,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2205,
		time: 2204,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2206,
		time: 2205,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2207,
		time: 2206,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2208,
		time: 2207,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2209,
		time: 2208,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2210,
		time: 2209,
		velocity: 0,
		power: 0,
		road: 19940.2865277778,
		acceleration: 0
	},
	{
		id: 2211,
		time: 2210,
		velocity: 0,
		power: 49.7694238420746,
		road: 19940.4198148148,
		acceleration: 0.266574074074074
	},
	{
		id: 2212,
		time: 2211,
		velocity: 0.799722222222222,
		power: 540.770606783956,
		road: 19941.0648148148,
		acceleration: 0.756851851851852
	},
	{
		id: 2213,
		time: 2212,
		velocity: 2.27055555555556,
		power: 2463.10632667635,
		road: 19942.7805092593,
		acceleration: 1.38453703703704
	},
	{
		id: 2214,
		time: 2213,
		velocity: 4.15361111111111,
		power: 4731.05390037038,
		road: 19945.9166203704,
		acceleration: 1.4562962962963
	},
	{
		id: 2215,
		time: 2214,
		velocity: 5.16861111111111,
		power: 4812.92794634172,
		road: 19950.2909722222,
		acceleration: 1.02018518518518
	},
	{
		id: 2216,
		time: 2215,
		velocity: 5.33111111111111,
		power: 4044.40307154224,
		road: 19955.5116666667,
		acceleration: 0.6725
	},
	{
		id: 2217,
		time: 2216,
		velocity: 6.17111111111111,
		power: 3390.22950113005,
		road: 19961.3033796297,
		acceleration: 0.469537037037037
	},
	{
		id: 2218,
		time: 2217,
		velocity: 6.57722222222222,
		power: 3490.45668470705,
		road: 19967.5490740741,
		acceleration: 0.438425925925925
	},
	{
		id: 2219,
		time: 2218,
		velocity: 6.64638888888889,
		power: 1734.3493382698,
		road: 19974.077962963,
		acceleration: 0.127962962962964
	},
	{
		id: 2220,
		time: 2219,
		velocity: 6.555,
		power: 2151.59240052344,
		road: 19980.76375,
		acceleration: 0.185833333333334
	},
	{
		id: 2221,
		time: 2220,
		velocity: 7.13472222222222,
		power: 4035.16082203512,
		road: 19987.7677777778,
		acceleration: 0.450648148148147
	},
	{
		id: 2222,
		time: 2221,
		velocity: 7.99833333333333,
		power: 6860.60055619059,
		road: 19995.39,
		acceleration: 0.785740740740741
	},
	{
		id: 2223,
		time: 2222,
		velocity: 8.91222222222222,
		power: 8509.9763018509,
		road: 20003.8495370371,
		acceleration: 0.888888888888888
	},
	{
		id: 2224,
		time: 2223,
		velocity: 9.80138888888889,
		power: 5856.29416624275,
		road: 20013.0021296297,
		acceleration: 0.497222222222224
	},
	{
		id: 2225,
		time: 2224,
		velocity: 9.49,
		power: 1694.66939583538,
		road: 20022.4092592593,
		acceleration: 0.0118518518518513
	},
	{
		id: 2226,
		time: 2225,
		velocity: 8.94777777777778,
		power: -2350.85357201893,
		road: 20031.6005092593,
		acceleration: -0.443611111111112
	},
	{
		id: 2227,
		time: 2226,
		velocity: 8.47055555555555,
		power: -5577.99432191514,
		road: 20040.1426851852,
		acceleration: -0.854537037037035
	},
	{
		id: 2228,
		time: 2227,
		velocity: 6.92638888888889,
		power: -6977.74063887356,
		road: 20047.6920370371,
		acceleration: -1.13111111111111
	},
	{
		id: 2229,
		time: 2228,
		velocity: 5.55444444444444,
		power: -5492.33084680121,
		road: 20054.1533333334,
		acceleration: -1.045
	},
	{
		id: 2230,
		time: 2229,
		velocity: 5.33555555555555,
		power: -1994.74240116276,
		road: 20059.8346296297,
		acceleration: -0.515000000000001
	},
	{
		id: 2231,
		time: 2230,
		velocity: 5.38138888888889,
		power: 2401.74079858006,
		road: 20065.4125,
		acceleration: 0.308148148148149
	},
	{
		id: 2232,
		time: 2231,
		velocity: 6.47888888888889,
		power: 3501.26155878541,
		road: 20071.3793518519,
		acceleration: 0.469814814814815
	},
	{
		id: 2233,
		time: 2232,
		velocity: 6.745,
		power: 5550.10543873253,
		road: 20077.9493981482,
		acceleration: 0.736574074074074
	},
	{
		id: 2234,
		time: 2233,
		velocity: 7.59111111111111,
		power: 4293.62092684368,
		road: 20085.1240740741,
		acceleration: 0.472685185185185
	},
	{
		id: 2235,
		time: 2234,
		velocity: 7.89694444444444,
		power: 6026.81291623158,
		road: 20092.8636574074,
		acceleration: 0.657129629629631
	},
	{
		id: 2236,
		time: 2235,
		velocity: 8.71638888888889,
		power: 8543.70172624054,
		road: 20101.3748611111,
		acceleration: 0.886111111111111
	},
	{
		id: 2237,
		time: 2236,
		velocity: 10.2494444444444,
		power: 10764.3660182878,
		road: 20110.8372685185,
		acceleration: 1.01629629629629
	},
	{
		id: 2238,
		time: 2237,
		velocity: 10.9458333333333,
		power: 9302.85683905434,
		road: 20121.1855555556,
		acceleration: 0.755462962962962
	},
	{
		id: 2239,
		time: 2238,
		velocity: 10.9827777777778,
		power: 6816.36181170537,
		road: 20132.1403703704,
		acceleration: 0.457592592592594
	},
	{
		id: 2240,
		time: 2239,
		velocity: 11.6222222222222,
		power: 9003.65327717369,
		road: 20143.63375,
		acceleration: 0.619537037037038
	},
	{
		id: 2241,
		time: 2240,
		velocity: 12.8044444444444,
		power: 13168.845888087,
		road: 20155.8937037037,
		acceleration: 0.913611111111109
	},
	{
		id: 2242,
		time: 2241,
		velocity: 13.7236111111111,
		power: 12450.9924535414,
		road: 20168.9961111111,
		acceleration: 0.771296296296297
	},
	{
		id: 2243,
		time: 2242,
		velocity: 13.9361111111111,
		power: 6253.74129342366,
		road: 20182.6087962963,
		acceleration: 0.24925925925926
	},
	{
		id: 2244,
		time: 2243,
		velocity: 13.5522222222222,
		power: 222.303006547183,
		road: 20196.2385648148,
		acceleration: -0.215092592592594
	},
	{
		id: 2245,
		time: 2244,
		velocity: 13.0783333333333,
		power: -1810.0423296221,
		road: 20209.5758333333,
		acceleration: -0.369907407407407
	},
	{
		id: 2246,
		time: 2245,
		velocity: 12.8263888888889,
		power: -1771.13127301129,
		road: 20222.545462963,
		acceleration: -0.365370370370369
	},
	{
		id: 2247,
		time: 2246,
		velocity: 12.4561111111111,
		power: -2832.92874338434,
		road: 20235.1060185185,
		acceleration: -0.452777777777778
	},
	{
		id: 2248,
		time: 2247,
		velocity: 11.72,
		power: -5971.14546311966,
		road: 20247.0745370371,
		acceleration: -0.731296296296298
	},
	{
		id: 2249,
		time: 2248,
		velocity: 10.6325,
		power: -7396.29161027243,
		road: 20258.2308796296,
		acceleration: -0.893055555555554
	},
	{
		id: 2250,
		time: 2249,
		velocity: 9.77694444444444,
		power: -5287.86627908388,
		road: 20268.5786574074,
		acceleration: -0.724074074074075
	},
	{
		id: 2251,
		time: 2250,
		velocity: 9.54777777777778,
		power: -2309.47983757389,
		road: 20278.3497222222,
		acceleration: -0.42935185185185
	},
	{
		id: 2252,
		time: 2251,
		velocity: 9.34444444444445,
		power: 363.106029055822,
		road: 20287.8371296296,
		acceleration: -0.137962962962963
	},
	{
		id: 2253,
		time: 2252,
		velocity: 9.36305555555556,
		power: 1419.68802774281,
		road: 20297.2461111111,
		acceleration: -0.0188888888888901
	},
	{
		id: 2254,
		time: 2253,
		velocity: 9.49111111111111,
		power: 718.584382387808,
		road: 20306.5976388889,
		acceleration: -0.0960185185185196
	},
	{
		id: 2255,
		time: 2254,
		velocity: 9.05638888888889,
		power: -668.665228217677,
		road: 20315.775462963,
		acceleration: -0.251388888888886
	},
	{
		id: 2256,
		time: 2255,
		velocity: 8.60888888888889,
		power: -1898.15471825671,
		road: 20324.629212963,
		acceleration: -0.396759259259261
	},
	{
		id: 2257,
		time: 2256,
		velocity: 8.30083333333333,
		power: -1857.93817092433,
		road: 20333.0853240741,
		acceleration: -0.39851851851852
	},
	{
		id: 2258,
		time: 2257,
		velocity: 7.86083333333333,
		power: -1006.8151755995,
		road: 20341.1947222222,
		acceleration: -0.294907407407407
	},
	{
		id: 2259,
		time: 2258,
		velocity: 7.72416666666667,
		power: -1008.26350078902,
		road: 20349.007962963,
		acceleration: -0.297407407407407
	},
	{
		id: 2260,
		time: 2259,
		velocity: 7.40861111111111,
		power: -205.032485128065,
		road: 20356.5784259259,
		acceleration: -0.188148148148149
	},
	{
		id: 2261,
		time: 2260,
		velocity: 7.29638888888889,
		power: 2389.79282721539,
		road: 20364.1410648148,
		acceleration: 0.1725
	},
	{
		id: 2262,
		time: 2261,
		velocity: 8.24166666666667,
		power: 5682.39794264444,
		road: 20372.0843055556,
		acceleration: 0.588703703703705
	},
	{
		id: 2263,
		time: 2262,
		velocity: 9.17472222222222,
		power: 9628.33739689102,
		road: 20380.8157407407,
		acceleration: 0.987685185185185
	},
	{
		id: 2264,
		time: 2263,
		velocity: 10.2594444444444,
		power: 11340.225561937,
		road: 20390.5612037037,
		acceleration: 1.04037037037037
	},
	{
		id: 2265,
		time: 2264,
		velocity: 11.3627777777778,
		power: 8514.54996740318,
		road: 20401.1531944445,
		acceleration: 0.652685185185184
	},
	{
		id: 2266,
		time: 2265,
		velocity: 11.1327777777778,
		power: 3945.29736608606,
		road: 20412.1616203704,
		acceleration: 0.180185185185188
	},
	{
		id: 2267,
		time: 2266,
		velocity: 10.8,
		power: -46.8940616965659,
		road: 20423.1600925926,
		acceleration: -0.200092592592593
	},
	{
		id: 2268,
		time: 2267,
		velocity: 10.7625,
		power: 2196.34019482156,
		road: 20434.0668981482,
		acceleration: 0.0167592592592598
	},
	{
		id: 2269,
		time: 2268,
		velocity: 11.1830555555556,
		power: 6409.71327338467,
		road: 20445.1858333333,
		acceleration: 0.407499999999999
	},
	{
		id: 2270,
		time: 2269,
		velocity: 12.0225,
		power: 5971.41498868012,
		road: 20456.6799537037,
		acceleration: 0.34287037037037
	},
	{
		id: 2271,
		time: 2270,
		velocity: 11.7911111111111,
		power: 2774.25523272636,
		road: 20468.3677314815,
		acceleration: 0.0444444444444443
	},
	{
		id: 2272,
		time: 2271,
		velocity: 11.3163888888889,
		power: -657.839172663953,
		road: 20479.9464351852,
		acceleration: -0.262592592592593
	},
	{
		id: 2273,
		time: 2272,
		velocity: 11.2347222222222,
		power: 879.510373265358,
		road: 20491.3340740741,
		acceleration: -0.119537037037036
	},
	{
		id: 2274,
		time: 2273,
		velocity: 11.4325,
		power: 3038.13464153113,
		road: 20502.7019444445,
		acceleration: 0.0800000000000001
	},
	{
		id: 2275,
		time: 2274,
		velocity: 11.5563888888889,
		power: 3998.01780149353,
		road: 20514.1913425926,
		acceleration: 0.163055555555557
	},
	{
		id: 2276,
		time: 2275,
		velocity: 11.7238888888889,
		power: 3424.95740229972,
		road: 20525.8149537037,
		acceleration: 0.105370370370371
	},
	{
		id: 2277,
		time: 2276,
		velocity: 11.7486111111111,
		power: 3391.47750705495,
		road: 20537.5404166667,
		acceleration: 0.0983333333333309
	},
	{
		id: 2278,
		time: 2277,
		velocity: 11.8513888888889,
		power: 3459.33655602362,
		road: 20549.3652777778,
		acceleration: 0.100462962962963
	},
	{
		id: 2279,
		time: 2278,
		velocity: 12.0252777777778,
		power: 4361.82037955937,
		road: 20561.3274537037,
		acceleration: 0.174166666666666
	},
	{
		id: 2280,
		time: 2279,
		velocity: 12.2711111111111,
		power: 5045.40249463988,
		road: 20573.48875,
		acceleration: 0.224074074074073
	},
	{
		id: 2281,
		time: 2280,
		velocity: 12.5236111111111,
		power: 5014.29761049754,
		road: 20585.8674537037,
		acceleration: 0.210740740740741
	},
	{
		id: 2282,
		time: 2281,
		velocity: 12.6575,
		power: 5527.19680639692,
		road: 20598.4727777778,
		acceleration: 0.2425
	},
	{
		id: 2283,
		time: 2282,
		velocity: 12.9986111111111,
		power: 5781.80197516876,
		road: 20611.3247685185,
		acceleration: 0.250833333333334
	},
	{
		id: 2284,
		time: 2283,
		velocity: 13.2761111111111,
		power: 6511.31276262773,
		road: 20624.4497685185,
		acceleration: 0.295185185185186
	},
	{
		id: 2285,
		time: 2284,
		velocity: 13.5430555555556,
		power: 4635.34212128749,
		road: 20637.790462963,
		acceleration: 0.136203703703703
	},
	{
		id: 2286,
		time: 2285,
		velocity: 13.4072222222222,
		power: 2336.04885482561,
		road: 20651.1764351852,
		acceleration: -0.0456481481481479
	},
	{
		id: 2287,
		time: 2286,
		velocity: 13.1391666666667,
		power: 644.61576368198,
		road: 20664.4516203704,
		acceleration: -0.175925925925926
	},
	{
		id: 2288,
		time: 2287,
		velocity: 13.0152777777778,
		power: 653.543328711912,
		road: 20677.5528703704,
		acceleration: -0.171944444444446
	},
	{
		id: 2289,
		time: 2288,
		velocity: 12.8913888888889,
		power: 1153.14018726733,
		road: 20690.5037962963,
		acceleration: -0.128703703703703
	},
	{
		id: 2290,
		time: 2289,
		velocity: 12.7530555555556,
		power: 638.442286055587,
		road: 20703.3065740741,
		acceleration: -0.167592592592595
	},
	{
		id: 2291,
		time: 2290,
		velocity: 12.5125,
		power: 974.362630505166,
		road: 20715.9570833333,
		acceleration: -0.136944444444445
	},
	{
		id: 2292,
		time: 2291,
		velocity: 12.4805555555556,
		power: 1000.85256884319,
		road: 20728.4731481482,
		acceleration: -0.131944444444443
	},
	{
		id: 2293,
		time: 2292,
		velocity: 12.3572222222222,
		power: 142.816877208783,
		road: 20740.8225925926,
		acceleration: -0.201296296296295
	},
	{
		id: 2294,
		time: 2293,
		velocity: 11.9086111111111,
		power: -1245.81749683083,
		road: 20752.9124537037,
		acceleration: -0.31787037037037
	},
	{
		id: 2295,
		time: 2294,
		velocity: 11.5269444444444,
		power: -1425.32896743853,
		road: 20764.6771296296,
		acceleration: -0.332500000000001
	},
	{
		id: 2296,
		time: 2295,
		velocity: 11.3597222222222,
		power: -1325.12376605812,
		road: 20776.114212963,
		acceleration: -0.322685185185183
	},
	{
		id: 2297,
		time: 2296,
		velocity: 10.9405555555556,
		power: -3063.21237176712,
		road: 20787.1462962963,
		acceleration: -0.487314814814816
	},
	{
		id: 2298,
		time: 2297,
		velocity: 10.065,
		power: -3798.31279678974,
		road: 20797.6501851852,
		acceleration: -0.569074074074074
	},
	{
		id: 2299,
		time: 2298,
		velocity: 9.6525,
		power: -3982.98934488947,
		road: 20807.5673148148,
		acceleration: -0.604444444444447
	},
	{
		id: 2300,
		time: 2299,
		velocity: 9.12722222222222,
		power: -2574.84627667527,
		road: 20816.9496296296,
		acceleration: -0.465185185185184
	},
	{
		id: 2301,
		time: 2300,
		velocity: 8.66944444444444,
		power: -2690.65290155026,
		road: 20825.8546296296,
		acceleration: -0.489444444444445
	},
	{
		id: 2302,
		time: 2301,
		velocity: 8.18416666666667,
		power: -2018.82346674977,
		road: 20834.3056018519,
		acceleration: -0.418611111111112
	},
	{
		id: 2303,
		time: 2302,
		velocity: 7.87138888888889,
		power: -2477.51678418765,
		road: 20842.3027777778,
		acceleration: -0.48898148148148
	},
	{
		id: 2304,
		time: 2303,
		velocity: 7.2025,
		power: -2339.7178460059,
		road: 20849.8121296296,
		acceleration: -0.486666666666667
	},
	{
		id: 2305,
		time: 2304,
		velocity: 6.72416666666667,
		power: -2361.81541308071,
		road: 20856.8235185185,
		acceleration: -0.509259259259259
	},
	{
		id: 2306,
		time: 2305,
		velocity: 6.34361111111111,
		power: -2288.99297044099,
		road: 20863.3193981481,
		acceleration: -0.52175925925926
	},
	{
		id: 2307,
		time: 2306,
		velocity: 5.63722222222222,
		power: -1903.31828222303,
		road: 20869.3135648148,
		acceleration: -0.481666666666666
	},
	{
		id: 2308,
		time: 2307,
		velocity: 5.27916666666667,
		power: -1977.67270570685,
		road: 20874.8052314815,
		acceleration: -0.523333333333333
	},
	{
		id: 2309,
		time: 2308,
		velocity: 4.77361111111111,
		power: -1326.15282089042,
		road: 20879.8254166667,
		acceleration: -0.419629629629631
	},
	{
		id: 2310,
		time: 2309,
		velocity: 4.37833333333333,
		power: -2988.69075444177,
		road: 20884.2075925926,
		acceleration: -0.856388888888888
	},
	{
		id: 2311,
		time: 2310,
		velocity: 2.71,
		power: -2727.71991474307,
		road: 20887.6807407407,
		acceleration: -0.961666666666666
	},
	{
		id: 2312,
		time: 2311,
		velocity: 1.88861111111111,
		power: -2852.08418111257,
		road: 20889.9433333333,
		acceleration: -1.45944444444444
	},
	{
		id: 2313,
		time: 2312,
		velocity: 0,
		power: -794.421373645423,
		road: 20891.024537037,
		acceleration: -0.903333333333333
	},
	{
		id: 2314,
		time: 2313,
		velocity: 0,
		power: -149.699379158545,
		road: 20891.3393055556,
		acceleration: -0.629537037037037
	},
	{
		id: 2315,
		time: 2314,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2316,
		time: 2315,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2317,
		time: 2316,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2318,
		time: 2317,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2319,
		time: 2318,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2320,
		time: 2319,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2321,
		time: 2320,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2322,
		time: 2321,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2323,
		time: 2322,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2324,
		time: 2323,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2325,
		time: 2324,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2326,
		time: 2325,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2327,
		time: 2326,
		velocity: 0,
		power: 0,
		road: 20891.3393055556,
		acceleration: 0
	},
	{
		id: 2328,
		time: 2327,
		velocity: 0,
		power: 151.542500327955,
		road: 20891.5919907407,
		acceleration: 0.50537037037037
	},
	{
		id: 2329,
		time: 2328,
		velocity: 1.51611111111111,
		power: 1249.99334321498,
		road: 20892.6540277778,
		acceleration: 1.11333333333333
	},
	{
		id: 2330,
		time: 2329,
		velocity: 3.34,
		power: 4668.81682221829,
		road: 20895.180787037,
		acceleration: 1.81611111111111
	},
	{
		id: 2331,
		time: 2330,
		velocity: 5.44833333333333,
		power: 5528.32096354267,
		road: 20899.2606018518,
		acceleration: 1.29
	},
	{
		id: 2332,
		time: 2331,
		velocity: 5.38611111111111,
		power: 5038.95273381503,
		road: 20904.4273611111,
		acceleration: 0.883888888888889
	},
	{
		id: 2333,
		time: 2332,
		velocity: 5.99166666666667,
		power: 3735.16103186023,
		road: 20910.2973611111,
		acceleration: 0.522592592592592
	},
	{
		id: 2334,
		time: 2333,
		velocity: 7.01611111111111,
		power: 5825.31892300158,
		road: 20916.8223611111,
		acceleration: 0.787407407407407
	},
	{
		id: 2335,
		time: 2334,
		velocity: 7.74833333333333,
		power: 6018.16619939272,
		road: 20924.0972222222,
		acceleration: 0.712314814814816
	},
	{
		id: 2336,
		time: 2335,
		velocity: 8.12861111111111,
		power: 2831.54326389204,
		road: 20931.8398148148,
		acceleration: 0.223148148148148
	},
	{
		id: 2337,
		time: 2336,
		velocity: 7.68555555555556,
		power: -2451.44176916309,
		road: 20939.4445833333,
		acceleration: -0.498796296296296
	},
	{
		id: 2338,
		time: 2337,
		velocity: 6.25194444444444,
		power: -8140.73039537015,
		road: 20946.0785185185,
		acceleration: -1.44287037037037
	},
	{
		id: 2339,
		time: 2338,
		velocity: 3.8,
		power: -9561.35638918474,
		road: 20950.8702777778,
		acceleration: -2.24148148148148
	},
	{
		id: 2340,
		time: 2339,
		velocity: 0.961111111111111,
		power: -4869.25660392847,
		road: 20953.4993055556,
		acceleration: -2.08398148148148
	},
	{
		id: 2341,
		time: 2340,
		velocity: 0,
		power: -1029.16683163275,
		road: 20954.4530092593,
		acceleration: -1.26666666666667
	},
	{
		id: 2342,
		time: 2341,
		velocity: 0,
		power: -29.2643719948018,
		road: 20954.6131944444,
		acceleration: -0.32037037037037
	},
	{
		id: 2343,
		time: 2342,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2344,
		time: 2343,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2345,
		time: 2344,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2346,
		time: 2345,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2347,
		time: 2346,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2348,
		time: 2347,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2349,
		time: 2348,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2350,
		time: 2349,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2351,
		time: 2350,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2352,
		time: 2351,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2353,
		time: 2352,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2354,
		time: 2353,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2355,
		time: 2354,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2356,
		time: 2355,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2357,
		time: 2356,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2358,
		time: 2357,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2359,
		time: 2358,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2360,
		time: 2359,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2361,
		time: 2360,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2362,
		time: 2361,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2363,
		time: 2362,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2364,
		time: 2363,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2365,
		time: 2364,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2366,
		time: 2365,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2367,
		time: 2366,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2368,
		time: 2367,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2369,
		time: 2368,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2370,
		time: 2369,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2371,
		time: 2370,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2372,
		time: 2371,
		velocity: 0,
		power: 0,
		road: 20954.6131944444,
		acceleration: 0
	},
	{
		id: 2373,
		time: 2372,
		velocity: 0,
		power: 0.555312600218656,
		road: 20954.6175,
		acceleration: 0.00861111111111111
	},
	{
		id: 2374,
		time: 2373,
		velocity: 0.0258333333333333,
		power: 1.04037666242562,
		road: 20954.6261111111,
		acceleration: 0
	},
	{
		id: 2375,
		time: 2374,
		velocity: 0,
		power: 1.04037666242562,
		road: 20954.6347222222,
		acceleration: 0
	},
	{
		id: 2376,
		time: 2375,
		velocity: 0,
		power: 0.485063888888889,
		road: 20954.6390277778,
		acceleration: -0.00861111111111111
	},
	{
		id: 2377,
		time: 2376,
		velocity: 0,
		power: 0,
		road: 20954.6390277778,
		acceleration: 0
	},
	{
		id: 2378,
		time: 2377,
		velocity: 0,
		power: 0,
		road: 20954.6390277778,
		acceleration: 0
	},
	{
		id: 2379,
		time: 2378,
		velocity: 0,
		power: 234.672545381942,
		road: 20954.960462963,
		acceleration: 0.64287037037037
	},
	{
		id: 2380,
		time: 2379,
		velocity: 1.92861111111111,
		power: 2154.39676130025,
		road: 20956.3541203704,
		acceleration: 1.50157407407407
	},
	{
		id: 2381,
		time: 2380,
		velocity: 4.50472222222222,
		power: 6569.34207685348,
		road: 20959.5236574074,
		acceleration: 2.05018518518518
	},
	{
		id: 2382,
		time: 2381,
		velocity: 6.15055555555556,
		power: 7352.87972087817,
		road: 20964.4355092593,
		acceleration: 1.43444444444444
	},
	{
		id: 2383,
		time: 2382,
		velocity: 6.23194444444444,
		power: 5098.44384446609,
		road: 20970.4375,
		acceleration: 0.745833333333334
	},
	{
		id: 2384,
		time: 2383,
		velocity: 6.74222222222222,
		power: 3140.79706971674,
		road: 20976.9887037037,
		acceleration: 0.352592592592592
	},
	{
		id: 2385,
		time: 2384,
		velocity: 7.20833333333333,
		power: 3943.88752239032,
		road: 20983.9372685185,
		acceleration: 0.44212962962963
	},
	{
		id: 2386,
		time: 2385,
		velocity: 7.55833333333333,
		power: 7119.46080480655,
		road: 20991.5203240741,
		acceleration: 0.826851851851852
	},
	{
		id: 2387,
		time: 2386,
		velocity: 9.22277777777778,
		power: 10387.9127519477,
		road: 21000.0705092593,
		acceleration: 1.10740740740741
	},
	{
		id: 2388,
		time: 2387,
		velocity: 10.5305555555556,
		power: 9897.20233516759,
		road: 21009.6284259259,
		acceleration: 0.908055555555558
	},
	{
		id: 2389,
		time: 2388,
		velocity: 10.2825,
		power: 2088.48728371906,
		road: 21019.6575925926,
		acceleration: 0.0344444444444427
	},
	{
		id: 2390,
		time: 2389,
		velocity: 9.32611111111111,
		power: -4516.4542042383,
		road: 21029.3695833333,
		acceleration: -0.668796296296296
	},
	{
		id: 2391,
		time: 2390,
		velocity: 8.52416666666667,
		power: -7100.67042005268,
		road: 21038.2409259259,
		acceleration: -1.0125
	},
	{
		id: 2392,
		time: 2391,
		velocity: 7.245,
		power: -5624.9893710395,
		road: 21046.1510185185,
		acceleration: -0.91
	},
	{
		id: 2393,
		time: 2392,
		velocity: 6.59611111111111,
		power: -6607.22460871242,
		road: 21053.0236111111,
		acceleration: -1.165
	},
	{
		id: 2394,
		time: 2393,
		velocity: 5.02916666666667,
		power: -6340.42491052356,
		road: 21058.6479166667,
		acceleration: -1.33157407407407
	},
	{
		id: 2395,
		time: 2394,
		velocity: 3.25027777777778,
		power: -4934.4766686836,
		road: 21062.9308796296,
		acceleration: -1.35111111111111
	},
	{
		id: 2396,
		time: 2395,
		velocity: 2.54277777777778,
		power: -2622.75677192581,
		road: 21066.0252777778,
		acceleration: -1.02601851851852
	},
	{
		id: 2397,
		time: 2396,
		velocity: 1.95111111111111,
		power: -1431.40595200042,
		road: 21068.1936111111,
		acceleration: -0.826111111111111
	},
	{
		id: 2398,
		time: 2397,
		velocity: 0.771944444444444,
		power: -907.693953239315,
		road: 21069.5250925926,
		acceleration: -0.847592592592593
	},
	{
		id: 2399,
		time: 2398,
		velocity: 0,
		power: -288.50439334873,
		road: 21070.1075925926,
		acceleration: -0.65037037037037
	},
	{
		id: 2400,
		time: 2399,
		velocity: 0,
		power: -15.8189473846654,
		road: 21070.23625,
		acceleration: -0.257314814814815
	},
	{
		id: 2401,
		time: 2400,
		velocity: 0,
		power: 0,
		road: 21070.23625,
		acceleration: 0
	},
	{
		id: 2402,
		time: 2401,
		velocity: 0,
		power: 0,
		road: 21070.23625,
		acceleration: 0
	},
	{
		id: 2403,
		time: 2402,
		velocity: 0,
		power: 0,
		road: 21070.23625,
		acceleration: 0
	},
	{
		id: 2404,
		time: 2403,
		velocity: 0,
		power: 0,
		road: 21070.23625,
		acceleration: 0
	},
	{
		id: 2405,
		time: 2404,
		velocity: 0,
		power: 90.5931668475326,
		road: 21070.4253240741,
		acceleration: 0.378148148148148
	},
	{
		id: 2406,
		time: 2405,
		velocity: 1.13444444444444,
		power: 631.469205200017,
		road: 21071.1806018519,
		acceleration: 0.754259259259259
	},
	{
		id: 2407,
		time: 2406,
		velocity: 2.26277777777778,
		power: 1367.59506824506,
		road: 21072.7177314815,
		acceleration: 0.809444444444445
	},
	{
		id: 2408,
		time: 2407,
		velocity: 2.42833333333333,
		power: 1896.27367966773,
		road: 21075.0271296296,
		acceleration: 0.735092592592593
	},
	{
		id: 2409,
		time: 2408,
		velocity: 3.33972222222222,
		power: 1361.741243763,
		road: 21077.8887962963,
		acceleration: 0.369444444444445
	},
	{
		id: 2410,
		time: 2409,
		velocity: 3.37111111111111,
		power: 1646.76824519668,
		road: 21081.1356944444,
		acceleration: 0.401018518518518
	},
	{
		id: 2411,
		time: 2410,
		velocity: 3.63138888888889,
		power: 773.63776228758,
		road: 21084.6325,
		acceleration: 0.098796296296296
	},
	{
		id: 2412,
		time: 2411,
		velocity: 3.63611111111111,
		power: 841.755724434697,
		road: 21088.2344444444,
		acceleration: 0.111481481481482
	},
	{
		id: 2413,
		time: 2412,
		velocity: 3.70555555555556,
		power: 591.108076058951,
		road: 21091.9093518518,
		acceleration: 0.0344444444444445
	},
	{
		id: 2414,
		time: 2413,
		velocity: 3.73472222222222,
		power: 598.165734725002,
		road: 21095.6188425926,
		acceleration: 0.0347222222222219
	},
	{
		id: 2415,
		time: 2414,
		velocity: 3.74027777777778,
		power: 613.066587847221,
		road: 21099.3642592593,
		acceleration: 0.03712962962963
	},
	{
		id: 2416,
		time: 2415,
		velocity: 3.81694444444444,
		power: 361.71561467841,
		road: 21103.1114351852,
		acceleration: -0.0336111111111115
	},
	{
		id: 2417,
		time: 2416,
		velocity: 3.63388888888889,
		power: 228.61229891803,
		road: 21106.8068518518,
		acceleration: -0.0699074074074066
	},
	{
		id: 2418,
		time: 2417,
		velocity: 3.53055555555556,
		power: 1131.31511638677,
		road: 21110.5584722222,
		acceleration: 0.182314814814815
	},
	{
		id: 2419,
		time: 2418,
		velocity: 4.36388888888889,
		power: 1057.5420103757,
		road: 21114.4754166667,
		acceleration: 0.148333333333333
	},
	{
		id: 2420,
		time: 2419,
		velocity: 4.07888888888889,
		power: 2344.36769723784,
		road: 21118.6906481481,
		acceleration: 0.448240740740741
	},
	{
		id: 2421,
		time: 2420,
		velocity: 4.87527777777778,
		power: 3693.22796072299,
		road: 21123.4668518518,
		acceleration: 0.673703703703704
	},
	{
		id: 2422,
		time: 2421,
		velocity: 6.385,
		power: 8356.90981924394,
		road: 21129.2648148148,
		acceleration: 1.36981481481482
	},
	{
		id: 2423,
		time: 2422,
		velocity: 8.18833333333333,
		power: 9141.66969427502,
		road: 21136.3481481481,
		acceleration: 1.20092592592593
	},
	{
		id: 2424,
		time: 2423,
		velocity: 8.47805555555555,
		power: 6500.74204361023,
		road: 21144.3755092593,
		acceleration: 0.68712962962963
	},
	{
		id: 2425,
		time: 2424,
		velocity: 8.44638888888889,
		power: 2183.36984335183,
		road: 21152.7988888889,
		acceleration: 0.104907407407408
	},
	{
		id: 2426,
		time: 2425,
		velocity: 8.50305555555556,
		power: 1765.17665566069,
		road: 21161.2997222222,
		acceleration: 0.0499999999999989
	},
	{
		id: 2427,
		time: 2426,
		velocity: 8.62805555555556,
		power: 1555.8940373904,
		road: 21169.837037037,
		acceleration: 0.0229629629629624
	},
	{
		id: 2428,
		time: 2427,
		velocity: 8.51527777777778,
		power: -150.494442190941,
		road: 21178.2926388889,
		acceleration: -0.186388888888889
	},
	{
		id: 2429,
		time: 2428,
		velocity: 7.94388888888889,
		power: -2492.52123709847,
		road: 21186.4114814815,
		acceleration: -0.487129629629629
	},
	{
		id: 2430,
		time: 2429,
		velocity: 7.16666666666667,
		power: -2898.39633243792,
		road: 21194.00625,
		acceleration: -0.561018518518519
	},
	{
		id: 2431,
		time: 2430,
		velocity: 6.83222222222222,
		power: -3047.72692225102,
		road: 21201.0143518518,
		acceleration: -0.612314814814815
	},
	{
		id: 2432,
		time: 2431,
		velocity: 6.10694444444444,
		power: -2077.56003647857,
		road: 21207.4716666667,
		acceleration: -0.48925925925926
	},
	{
		id: 2433,
		time: 2432,
		velocity: 5.69888888888889,
		power: -793.483957565937,
		road: 21213.5415277778,
		acceleration: -0.285648148148148
	},
	{
		id: 2434,
		time: 2433,
		velocity: 5.97527777777778,
		power: 31.0822658485775,
		road: 21219.3980092593,
		acceleration: -0.14111111111111
	},
	{
		id: 2435,
		time: 2434,
		velocity: 5.68361111111111,
		power: 701.92321059648,
		road: 21225.1747685185,
		acceleration: -0.0183333333333335
	},
	{
		id: 2436,
		time: 2435,
		velocity: 5.64388888888889,
		power: -487.800903426842,
		road: 21230.8242592593,
		acceleration: -0.236203703703704
	},
	{
		id: 2437,
		time: 2436,
		velocity: 5.26666666666667,
		power: -451.227419398026,
		road: 21236.2398611111,
		acceleration: -0.231574074074073
	},
	{
		id: 2438,
		time: 2437,
		velocity: 4.98888888888889,
		power: 445.699600482566,
		road: 21241.5126388889,
		acceleration: -0.0540740740740748
	},
	{
		id: 2439,
		time: 2438,
		velocity: 5.48166666666667,
		power: -38.0939437925213,
		road: 21246.683287037,
		acceleration: -0.150185185185185
	},
	{
		id: 2440,
		time: 2439,
		velocity: 4.81611111111111,
		power: -540.219431845642,
		road: 21251.6509722222,
		acceleration: -0.25574074074074
	},
	{
		id: 2441,
		time: 2440,
		velocity: 4.22166666666667,
		power: -1814.03204033415,
		road: 21256.2118518518,
		acceleration: -0.55787037037037
	},
	{
		id: 2442,
		time: 2441,
		velocity: 3.80805555555556,
		power: -1898.90925420834,
		road: 21260.1732407407,
		acceleration: -0.641111111111112
	},
	{
		id: 2443,
		time: 2442,
		velocity: 2.89277777777778,
		power: -1782.79172485231,
		road: 21263.461712963,
		acceleration: -0.704722222222223
	},
	{
		id: 2444,
		time: 2443,
		velocity: 2.1075,
		power: -729.36913958114,
		road: 21266.1912037037,
		acceleration: -0.41324074074074
	},
	{
		id: 2445,
		time: 2444,
		velocity: 2.56833333333333,
		power: -551.000217034804,
		road: 21268.5243518518,
		acceleration: -0.379444444444444
	},
	{
		id: 2446,
		time: 2445,
		velocity: 1.75444444444444,
		power: -597.271506389806,
		road: 21270.4385185185,
		acceleration: -0.458518518518519
	},
	{
		id: 2447,
		time: 2446,
		velocity: 0.731944444444444,
		power: -826.41544587132,
		road: 21271.71875,
		acceleration: -0.809351851851852
	},
	{
		id: 2448,
		time: 2447,
		velocity: 0.140277777777778,
		power: -252.603043781879,
		road: 21272.3018981481,
		acceleration: -0.584814814814815
	},
	{
		id: 2449,
		time: 2448,
		velocity: 0,
		power: -18.6167141762942,
		road: 21272.4706481481,
		acceleration: -0.243981481481481
	},
	{
		id: 2450,
		time: 2449,
		velocity: 0,
		power: 1.78900105588044,
		road: 21272.4940277778,
		acceleration: -0.0467592592592593
	},
	{
		id: 2451,
		time: 2450,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2452,
		time: 2451,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2453,
		time: 2452,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2454,
		time: 2453,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2455,
		time: 2454,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2456,
		time: 2455,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2457,
		time: 2456,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2458,
		time: 2457,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2459,
		time: 2458,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2460,
		time: 2459,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2461,
		time: 2460,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2462,
		time: 2461,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2463,
		time: 2462,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2464,
		time: 2463,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2465,
		time: 2464,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2466,
		time: 2465,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	},
	{
		id: 2467,
		time: 2466,
		velocity: 0,
		power: 0,
		road: 21272.4940277778,
		acceleration: 0
	}
];
export default train3;
