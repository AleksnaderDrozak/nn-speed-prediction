export const train1 = 
[
	{
		id: 1,
		time: 0,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 3,
		time: 2,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 4,
		time: 3,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 5,
		time: 4,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 6,
		time: 5,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 7,
		time: 6,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 8,
		time: 7,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 9,
		time: 8,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 10,
		time: 9,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 11,
		time: 10,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 12,
		time: 11,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 13,
		time: 12,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 14,
		time: 13,
		velocity: 0,
		power: 185.447036968014,
		road: 0.282546296296296,
		acceleration: 0.565092592592593
	},
	{
		id: 15,
		time: 14,
		velocity: 1.69527777777778,
		power: 889.982398215676,
		road: 1.26259259259259,
		acceleration: 0.829907407407407
	},
	{
		id: 16,
		time: 15,
		velocity: 2.48972222222222,
		power: 1788.23546793977,
		road: 3.10467592592593,
		acceleration: 0.894166666666667
	},
	{
		id: 17,
		time: 16,
		velocity: 2.6825,
		power: 452.623849453939,
		road: 5.43115740740741,
		acceleration: 0.0746296296296296
	},
	{
		id: 18,
		time: 17,
		velocity: 1.91916666666667,
		power: -710.049518040252,
		road: 7.55361111111111,
		acceleration: -0.482685185185185
	},
	{
		id: 19,
		time: 18,
		velocity: 1.04166666666667,
		power: -679.376552407772,
		road: 9.14518518518518,
		acceleration: -0.579074074074074
	},
	{
		id: 20,
		time: 19,
		velocity: 0.945277777777778,
		power: -307.334932863633,
		road: 10.2343055555556,
		acceleration: -0.425833333333333
	},
	{
		id: 21,
		time: 20,
		velocity: 0.641666666666667,
		power: -146.12349792186,
		road: 10.9368981481481,
		acceleration: -0.347222222222222
	},
	{
		id: 22,
		time: 21,
		velocity: 0,
		power: -65.991419101506,
		road: 11.3083333333333,
		acceleration: -0.315092592592593
	},
	{
		id: 23,
		time: 22,
		velocity: 0,
		power: -8.74951900584796,
		road: 11.4152777777778,
		acceleration: -0.213888888888889
	},
	{
		id: 24,
		time: 23,
		velocity: 0,
		power: 15.7402342678762,
		road: 11.4799537037037,
		acceleration: 0.129351851851852
	},
	{
		id: 25,
		time: 24,
		velocity: 0.388055555555556,
		power: 15.629193357065,
		road: 11.6093055555556,
		acceleration: 0
	},
	{
		id: 26,
		time: 25,
		velocity: 0,
		power: 143.30930016555,
		road: 11.9190277777778,
		acceleration: 0.360740740740741
	},
	{
		id: 27,
		time: 26,
		velocity: 1.08222222222222,
		power: 245.302436249601,
		road: 12.5502777777778,
		acceleration: 0.282314814814815
	},
	{
		id: 28,
		time: 27,
		velocity: 1.235,
		power: 444.761162750377,
		road: 13.504537037037,
		acceleration: 0.363703703703704
	},
	{
		id: 29,
		time: 28,
		velocity: 1.09111111111111,
		power: -210.846077492671,
		road: 14.4602777777778,
		acceleration: -0.360740740740741
	},
	{
		id: 30,
		time: 29,
		velocity: 0,
		power: -153.268279561474,
		road: 15.0298148148148,
		acceleration: -0.411666666666667
	},
	{
		id: 31,
		time: 30,
		velocity: 0,
		power: 2.2302942970246,
		road: 15.3336111111111,
		acceleration: -0.119814814814815
	},
	{
		id: 32,
		time: 31,
		velocity: 0.731666666666667,
		power: 392.399501755612,
		road: 15.8874537037037,
		acceleration: 0.619907407407407
	},
	{
		id: 33,
		time: 32,
		velocity: 1.85972222222222,
		power: 1220.99972811825,
		road: 17.18375,
		acceleration: 0.865
	},
	{
		id: 34,
		time: 33,
		velocity: 2.595,
		power: 1735.67251985246,
		road: 19.2833333333333,
		acceleration: 0.741574074074074
	},
	{
		id: 35,
		time: 34,
		velocity: 2.95638888888889,
		power: 1619.08132997918,
		road: 22.0017592592593,
		acceleration: 0.496111111111111
	},
	{
		id: 36,
		time: 35,
		velocity: 3.34805555555556,
		power: 1333.55816258937,
		road: 25.1266203703704,
		acceleration: 0.316759259259259
	},
	{
		id: 37,
		time: 36,
		velocity: 3.54527777777778,
		power: 2142.22449320867,
		road: 28.6618055555556,
		acceleration: 0.503888888888889
	},
	{
		id: 38,
		time: 37,
		velocity: 4.46805555555556,
		power: 1989.75015078997,
		road: 32.6438888888889,
		acceleration: 0.389907407407408
	},
	{
		id: 39,
		time: 38,
		velocity: 4.51777777777778,
		power: 1754.42210476646,
		road: 36.9656944444444,
		acceleration: 0.289537037037037
	},
	{
		id: 40,
		time: 39,
		velocity: 4.41388888888889,
		power: -838.80352502622,
		road: 41.2605555555556,
		acceleration: -0.343425925925927
	},
	{
		id: 41,
		time: 40,
		velocity: 3.43777777777778,
		power: -1550.64054479163,
		road: 45.1033333333333,
		acceleration: -0.56074074074074
	},
	{
		id: 42,
		time: 41,
		velocity: 2.83555555555556,
		power: -889.122577173821,
		road: 48.4593055555556,
		acceleration: -0.41287037037037
	},
	{
		id: 43,
		time: 42,
		velocity: 3.17527777777778,
		power: 706.264947038091,
		road: 51.6585648148148,
		acceleration: 0.0994444444444444
	},
	{
		id: 44,
		time: 43,
		velocity: 3.73611111111111,
		power: 3200.48547972524,
		road: 55.3026388888889,
		acceleration: 0.790185185185185
	},
	{
		id: 45,
		time: 44,
		velocity: 5.20611111111111,
		power: 3572.23933603246,
		road: 59.7002777777778,
		acceleration: 0.716944444444445
	},
	{
		id: 46,
		time: 45,
		velocity: 5.32611111111111,
		power: 3226.15771113917,
		road: 64.7235648148148,
		acceleration: 0.534351851851852
	},
	{
		id: 47,
		time: 46,
		velocity: 5.33916666666667,
		power: 883.782574838986,
		road: 70.0300462962963,
		acceleration: 0.0320370370370364
	},
	{
		id: 48,
		time: 47,
		velocity: 5.30222222222222,
		power: 1263.71385544491,
		road: 75.4044444444444,
		acceleration: 0.103796296296297
	},
	{
		id: 49,
		time: 48,
		velocity: 5.6375,
		power: 1833.88669490917,
		road: 80.932962962963,
		acceleration: 0.204444444444444
	},
	{
		id: 50,
		time: 49,
		velocity: 5.9525,
		power: 3995.59513634935,
		road: 86.8456018518519,
		acceleration: 0.563796296296296
	},
	{
		id: 51,
		time: 50,
		velocity: 6.99361111111111,
		power: 4042.563884055,
		road: 93.2943518518519,
		acceleration: 0.508425925925926
	},
	{
		id: 52,
		time: 51,
		velocity: 7.16277777777778,
		power: 5550.30708174762,
		road: 100.334074074074,
		acceleration: 0.673518518518518
	},
	{
		id: 53,
		time: 52,
		velocity: 7.97305555555555,
		power: 5446.36359583423,
		road: 108.003425925926,
		acceleration: 0.585740740740739
	},
	{
		id: 54,
		time: 53,
		velocity: 8.75083333333333,
		power: 5939.30321730782,
		road: 116.260555555556,
		acceleration: 0.589814814814816
	},
	{
		id: 55,
		time: 54,
		velocity: 8.93222222222222,
		power: 6031.06639117074,
		road: 125.085787037037,
		acceleration: 0.546388888888888
	},
	{
		id: 56,
		time: 55,
		velocity: 9.61222222222222,
		power: 3596.85954258308,
		road: 134.301481481481,
		acceleration: 0.234537037037038
	},
	{
		id: 57,
		time: 56,
		velocity: 9.45444444444444,
		power: 1860.90735254493,
		road: 143.650601851852,
		acceleration: 0.0323148148148142
	},
	{
		id: 58,
		time: 57,
		velocity: 9.02916666666667,
		power: -2698.93167328555,
		road: 152.77337962963,
		acceleration: -0.484999999999999
	},
	{
		id: 59,
		time: 58,
		velocity: 8.15722222222222,
		power: -8245.9924810729,
		road: 161.047037037037,
		acceleration: -1.21324074074074
	},
	{
		id: 60,
		time: 59,
		velocity: 5.81472222222222,
		power: -9607.30310623101,
		road: 167.899953703704,
		acceleration: -1.62824074074074
	},
	{
		id: 61,
		time: 60,
		velocity: 4.14444444444444,
		power: -7242.78564749138,
		road: 173.139814814815,
		acceleration: -1.59787037037037
	},
	{
		id: 62,
		time: 61,
		velocity: 3.36361111111111,
		power: -2495.2058654627,
		road: 177.187824074074,
		acceleration: -0.785833333333333
	},
	{
		id: 63,
		time: 62,
		velocity: 3.45722222222222,
		power: 1001.86095087605,
		road: 180.916805555556,
		acceleration: 0.147777777777778
	},
	{
		id: 64,
		time: 63,
		velocity: 4.58777777777778,
		power: 2564.57090485504,
		road: 184.98337962963,
		acceleration: 0.527407407407408
	},
	{
		id: 65,
		time: 64,
		velocity: 4.94583333333333,
		power: 4620.74646178792,
		road: 189.75337962963,
		acceleration: 0.879444444444444
	},
	{
		id: 66,
		time: 65,
		velocity: 6.09555555555556,
		power: 4628.22643477056,
		road: 195.327453703704,
		acceleration: 0.728703703703704
	},
	{
		id: 67,
		time: 66,
		velocity: 6.77388888888889,
		power: 8051.31255383951,
		road: 201.840185185185,
		acceleration: 1.14861111111111
	},
	{
		id: 68,
		time: 67,
		velocity: 8.39166666666667,
		power: 7863.35460723772,
		road: 209.394398148148,
		acceleration: 0.93435185185185
	},
	{
		id: 69,
		time: 68,
		velocity: 8.89861111111111,
		power: 10373.7703617875,
		road: 217.966851851852,
		acceleration: 1.10212962962963
	},
	{
		id: 70,
		time: 69,
		velocity: 10.0802777777778,
		power: 7298.62749195943,
		road: 227.407361111111,
		acceleration: 0.633981481481483
	},
	{
		id: 71,
		time: 70,
		velocity: 10.2936111111111,
		power: 5370.58697698606,
		road: 237.356527777778,
		acceleration: 0.383333333333333
	},
	{
		id: 72,
		time: 71,
		velocity: 10.0486111111111,
		power: 2525.92399262582,
		road: 247.534675925926,
		acceleration: 0.0746296296296283
	},
	{
		id: 73,
		time: 72,
		velocity: 10.3041666666667,
		power: 2371.58828068145,
		road: 257.778333333333,
		acceleration: 0.0563888888888897
	},
	{
		id: 74,
		time: 73,
		velocity: 10.4627777777778,
		power: 4826.96738816744,
		road: 268.198888888889,
		acceleration: 0.297407407407409
	},
	{
		id: 75,
		time: 74,
		velocity: 10.9408333333333,
		power: 4497.70664549843,
		road: 278.892824074074,
		acceleration: 0.24935185185185
	},
	{
		id: 76,
		time: 75,
		velocity: 11.0522222222222,
		power: 5125.9288429374,
		road: 289.858981481482,
		acceleration: 0.295092592592594
	},
	{
		id: 77,
		time: 76,
		velocity: 11.3480555555556,
		power: 5777.80402681189,
		road: 301.14162037037,
		acceleration: 0.33787037037037
	},
	{
		id: 78,
		time: 77,
		velocity: 11.9544444444444,
		power: 5800.73498679368,
		road: 312.75337962963,
		acceleration: 0.32037037037037
	},
	{
		id: 79,
		time: 78,
		velocity: 12.0133333333333,
		power: 6535.09067067312,
		road: 324.70787037037,
		acceleration: 0.365092592592594
	},
	{
		id: 80,
		time: 79,
		velocity: 12.4433333333333,
		power: 5561.58686552349,
		road: 336.976342592593,
		acceleration: 0.262870370370369
	},
	{
		id: 81,
		time: 80,
		velocity: 12.7430555555556,
		power: 8489.59254674711,
		road: 349.619305555556,
		acceleration: 0.486111111111112
	},
	{
		id: 82,
		time: 81,
		velocity: 13.4716666666667,
		power: 16793.2219543285,
		road: 363.045787037037,
		acceleration: 1.08092592592592
	},
	{
		id: 83,
		time: 82,
		velocity: 15.6861111111111,
		power: 11771.6900541312,
		road: 377.323148148148,
		acceleration: 0.620833333333332
	},
	{
		id: 84,
		time: 83,
		velocity: 14.6055555555556,
		power: 433.002693882587,
		road: 391.80375,
		acceleration: -0.214351851851852
	},
	{
		id: 85,
		time: 84,
		velocity: 12.8286111111111,
		power: -20836.2990849539,
		road: 405.250694444444,
		acceleration: -1.85296296296296
	},
	{
		id: 86,
		time: 85,
		velocity: 10.1272222222222,
		power: -17366.3656943052,
		road: 416.886574074074,
		acceleration: -1.76916666666667
	},
	{
		id: 87,
		time: 86,
		velocity: 9.29805555555556,
		power: -9031.89937850922,
		road: 427.079861111111,
		acceleration: -1.11601851851852
	},
	{
		id: 88,
		time: 87,
		velocity: 9.48055555555556,
		power: 3392.12883903606,
		road: 436.807777777778,
		acceleration: 0.185277777777777
	},
	{
		id: 89,
		time: 88,
		velocity: 10.6830555555556,
		power: 9457.57447237829,
		road: 447.021064814815,
		acceleration: 0.785462962962963
	},
	{
		id: 90,
		time: 89,
		velocity: 11.6544444444444,
		power: 14646.4195588461,
		road: 458.214166666667,
		acceleration: 1.17416666666666
	},
	{
		id: 91,
		time: 90,
		velocity: 13.0030555555556,
		power: 15399.8544241189,
		road: 470.542361111111,
		acceleration: 1.09601851851852
	},
	{
		id: 92,
		time: 91,
		velocity: 13.9711111111111,
		power: 18296.9130371375,
		road: 484.014861111111,
		acceleration: 1.19259259259259
	},
	{
		id: 93,
		time: 92,
		velocity: 15.2322222222222,
		power: 16461.4194134657,
		road: 498.552962962963,
		acceleration: 0.938611111111111
	},
	{
		id: 94,
		time: 93,
		velocity: 15.8188888888889,
		power: 18556.2597540506,
		road: 514.055046296296,
		acceleration: 0.98935185185185
	},
	{
		id: 95,
		time: 94,
		velocity: 16.9391666666667,
		power: 17709.1975225005,
		road: 530.475925925926,
		acceleration: 0.84824074074074
	},
	{
		id: 96,
		time: 95,
		velocity: 17.7769444444444,
		power: 18292.6892678631,
		road: 547.727453703704,
		acceleration: 0.813055555555554
	},
	{
		id: 97,
		time: 96,
		velocity: 18.2580555555556,
		power: 10880.4918497288,
		road: 565.551203703704,
		acceleration: 0.331388888888892
	},
	{
		id: 98,
		time: 97,
		velocity: 17.9333333333333,
		power: 3752.62929091471,
		road: 583.495462962963,
		acceleration: -0.0903703703703691
	},
	{
		id: 99,
		time: 98,
		velocity: 17.5058333333333,
		power: -2928.14920381282,
		road: 601.156296296297,
		acceleration: -0.476481481481482
	},
	{
		id: 100,
		time: 99,
		velocity: 16.8286111111111,
		power: -3127.23500833627,
		road: 618.336851851852,
		acceleration: -0.484074074074076
	},
	{
		id: 101,
		time: 100,
		velocity: 16.4811111111111,
		power: -5529.16346371594,
		road: 634.959861111111,
		acceleration: -0.631018518518516
	},
	{
		id: 102,
		time: 101,
		velocity: 15.6127777777778,
		power: -3050.36762355127,
		road: 651.031574074074,
		acceleration: -0.471574074074075
	},
	{
		id: 103,
		time: 102,
		velocity: 15.4138888888889,
		power: -4084.82601079435,
		road: 666.598194444445,
		acceleration: -0.538611111111109
	},
	{
		id: 104,
		time: 103,
		velocity: 14.8652777777778,
		power: -3383.86999743597,
		road: 681.650277777778,
		acceleration: -0.490462962962964
	},
	{
		id: 105,
		time: 104,
		velocity: 14.1413888888889,
		power: -6705.73369033901,
		road: 696.091527777778,
		acceleration: -0.731203703703706
	},
	{
		id: 106,
		time: 105,
		velocity: 13.2202777777778,
		power: -11499.6094747551,
		road: 709.60625,
		acceleration: -1.12185185185185
	},
	{
		id: 107,
		time: 106,
		velocity: 11.4997222222222,
		power: -12650.6499329657,
		road: 721.914861111111,
		acceleration: -1.29037037037037
	},
	{
		id: 108,
		time: 107,
		velocity: 10.2702777777778,
		power: -13213.0219089093,
		road: 732.846805555556,
		acceleration: -1.46296296296296
	},
	{
		id: 109,
		time: 108,
		velocity: 8.83138888888889,
		power: -7300.69228869325,
		road: 742.5625,
		acceleration: -0.969537037037039
	},
	{
		id: 110,
		time: 109,
		velocity: 8.59111111111111,
		power: -6567.60507116196,
		road: 751.313981481482,
		acceleration: -0.958888888888888
	},
	{
		id: 111,
		time: 110,
		velocity: 7.39361111111111,
		power: -1994.85654287593,
		road: 759.373981481482,
		acceleration: -0.424074074074074
	},
	{
		id: 112,
		time: 111,
		velocity: 7.55916666666667,
		power: 2328.94934535482,
		road: 767.295046296296,
		acceleration: 0.146203703703704
	},
	{
		id: 113,
		time: 112,
		velocity: 9.02972222222222,
		power: 8776.90462236353,
		road: 775.750509259259,
		acceleration: 0.922592592592593
	},
	{
		id: 114,
		time: 113,
		velocity: 10.1613888888889,
		power: 9946.886999713,
		road: 785.135092592593,
		acceleration: 0.935648148148147
	},
	{
		id: 115,
		time: 114,
		velocity: 10.3661111111111,
		power: 6568.64419202379,
		road: 795.23625,
		acceleration: 0.497500000000001
	},
	{
		id: 116,
		time: 115,
		velocity: 10.5222222222222,
		power: 3095.32864773135,
		road: 805.64787037037,
		acceleration: 0.123425925925925
	},
	{
		id: 117,
		time: 116,
		velocity: 10.5316666666667,
		power: 1532.22155148999,
		road: 816.103564814815,
		acceleration: -0.0352777777777771
	},
	{
		id: 118,
		time: 117,
		velocity: 10.2602777777778,
		power: 969.055376422999,
		road: 826.496388888889,
		acceleration: -0.0904629629629632
	},
	{
		id: 119,
		time: 118,
		velocity: 10.2508333333333,
		power: 1477.92343392083,
		road: 836.825277777778,
		acceleration: -0.0374074074074073
	},
	{
		id: 120,
		time: 119,
		velocity: 10.4194444444444,
		power: 1471.94929388164,
		road: 847.116944444444,
		acceleration: -0.0370370370370381
	},
	{
		id: 121,
		time: 120,
		velocity: 10.1491666666667,
		power: 1681.74833212528,
		road: 857.382638888889,
		acceleration: -0.0149074074074065
	},
	{
		id: 122,
		time: 121,
		velocity: 10.2061111111111,
		power: 1771.21575877389,
		road: 867.638148148148,
		acceleration: -0.00546296296296234
	},
	{
		id: 123,
		time: 122,
		velocity: 10.4030555555556,
		power: 1926.69377303451,
		road: 877.896111111111,
		acceleration: 0.0103703703703708
	},
	{
		id: 124,
		time: 123,
		velocity: 10.1802777777778,
		power: 1307.95425640955,
		road: 888.133055555556,
		acceleration: -0.0524074074074079
	},
	{
		id: 125,
		time: 124,
		velocity: 10.0488888888889,
		power: 249.331348013316,
		road: 898.264074074074,
		acceleration: -0.159444444444445
	},
	{
		id: 126,
		time: 125,
		velocity: 9.92472222222222,
		power: 611.580865863044,
		road: 908.255648148148,
		acceleration: -0.119444444444445
	},
	{
		id: 127,
		time: 126,
		velocity: 9.82194444444444,
		power: 992.203004753519,
		road: 918.148842592593,
		acceleration: -0.0773148148148142
	},
	{
		id: 128,
		time: 127,
		velocity: 9.81694444444445,
		power: 1145.447527775,
		road: 927.973657407407,
		acceleration: -0.0594444444444449
	},
	{
		id: 129,
		time: 128,
		velocity: 9.74638888888889,
		power: 2085.51012456456,
		road: 937.789398148148,
		acceleration: 0.0412962962962968
	},
	{
		id: 130,
		time: 129,
		velocity: 9.94583333333333,
		power: 2343.90964983965,
		road: 947.659259259259,
		acceleration: 0.0669444444444451
	},
	{
		id: 131,
		time: 130,
		velocity: 10.0177777777778,
		power: 2296.49213790756,
		road: 957.592407407407,
		acceleration: 0.0596296296296277
	},
	{
		id: 132,
		time: 131,
		velocity: 9.92527777777778,
		power: 3876.1424653973,
		road: 967.665,
		acceleration: 0.21925925925926
	},
	{
		id: 133,
		time: 132,
		velocity: 10.6036111111111,
		power: 4979.66009666704,
		road: 978.006018518519,
		acceleration: 0.317592592592591
	},
	{
		id: 134,
		time: 133,
		velocity: 10.9705555555556,
		power: 6047.25845527077,
		road: 988.706296296296,
		acceleration: 0.400925925925929
	},
	{
		id: 135,
		time: 134,
		velocity: 11.1280555555556,
		power: 3319.68236859447,
		road: 999.668287037037,
		acceleration: 0.122499999999999
	},
	{
		id: 136,
		time: 135,
		velocity: 10.9711111111111,
		power: 1512.98382326601,
		road: 1010.66587962963,
		acceleration: -0.0512962962962966
	},
	{
		id: 137,
		time: 136,
		velocity: 10.8166666666667,
		power: -268.901456895546,
		road: 1021.52787037037,
		acceleration: -0.219907407407408
	},
	{
		id: 138,
		time: 137,
		velocity: 10.4683333333333,
		power: 435.629040182541,
		road: 1032.20546296296,
		acceleration: -0.148888888888889
	},
	{
		id: 139,
		time: 138,
		velocity: 10.5244444444444,
		power: 3801.96262143914,
		road: 1042.89916666667,
		acceleration: 0.181111111111111
	},
	{
		id: 140,
		time: 139,
		velocity: 11.36,
		power: 3685.43316791162,
		road: 1053.76435185185,
		acceleration: 0.161851851851852
	},
	{
		id: 141,
		time: 140,
		velocity: 10.9538888888889,
		power: 4495.56274532268,
		road: 1064.82541666667,
		acceleration: 0.229907407407408
	},
	{
		id: 142,
		time: 141,
		velocity: 11.2141666666667,
		power: 2760.89528011102,
		road: 1076.03152777778,
		acceleration: 0.0601851851851851
	},
	{
		id: 143,
		time: 142,
		velocity: 11.5405555555556,
		power: 5078.09411722964,
		road: 1087.40175925926,
		acceleration: 0.268055555555556
	},
	{
		id: 144,
		time: 143,
		velocity: 11.7580555555556,
		power: 3453.87523444259,
		road: 1098.9612962963,
		acceleration: 0.110555555555555
	},
	{
		id: 145,
		time: 144,
		velocity: 11.5458333333333,
		power: 1564.24818112755,
		road: 1110.54537037037,
		acceleration: -0.061481481481481
	},
	{
		id: 146,
		time: 145,
		velocity: 11.3561111111111,
		power: -1992.83393215369,
		road: 1121.90662037037,
		acceleration: -0.384166666666665
	},
	{
		id: 147,
		time: 146,
		velocity: 10.6055555555556,
		power: -2540.63119310192,
		road: 1132.85657407407,
		acceleration: -0.438425925925927
	},
	{
		id: 148,
		time: 147,
		velocity: 10.2305555555556,
		power: -3117.43400553975,
		road: 1143.33657407407,
		acceleration: -0.501481481481482
	},
	{
		id: 149,
		time: 148,
		velocity: 9.85166666666667,
		power: -2343.16650937892,
		road: 1153.35106481481,
		acceleration: -0.429537037037036
	},
	{
		id: 150,
		time: 149,
		velocity: 9.31694444444444,
		power: -2341.68944354188,
		road: 1162.93291666667,
		acceleration: -0.435740740740741
	},
	{
		id: 151,
		time: 150,
		velocity: 8.92333333333333,
		power: -2913.68754689335,
		road: 1172.04185185185,
		acceleration: -0.510092592592592
	},
	{
		id: 152,
		time: 151,
		velocity: 8.32138888888889,
		power: -2066.52223231974,
		road: 1180.68541666667,
		acceleration: -0.420648148148148
	},
	{
		id: 153,
		time: 152,
		velocity: 8.055,
		power: -1449.03758539097,
		road: 1188.94356481481,
		acceleration: -0.350185185185186
	},
	{
		id: 154,
		time: 153,
		velocity: 7.87277777777778,
		power: -42.1400320996521,
		road: 1196.94212962963,
		acceleration: -0.168981481481482
	},
	{
		id: 155,
		time: 154,
		velocity: 7.81444444444444,
		power: 1213.22178761391,
		road: 1204.85541666667,
		acceleration: -0.00157407407407284
	},
	{
		id: 156,
		time: 155,
		velocity: 8.05027777777778,
		power: 3088.8137288199,
		road: 1212.88805555556,
		acceleration: 0.240277777777776
	},
	{
		id: 157,
		time: 156,
		velocity: 8.59361111111111,
		power: 4025.9080082108,
		road: 1221.21166666667,
		acceleration: 0.341666666666669
	},
	{
		id: 158,
		time: 157,
		velocity: 8.83944444444444,
		power: 4835.62093891925,
		road: 1229.91291666667,
		acceleration: 0.413611111111109
	},
	{
		id: 159,
		time: 158,
		velocity: 9.29111111111111,
		power: 5173.71078074349,
		road: 1239.03171296296,
		acceleration: 0.421481481481482
	},
	{
		id: 160,
		time: 159,
		velocity: 9.85805555555555,
		power: 7040.56030676637,
		road: 1248.65541666667,
		acceleration: 0.588333333333333
	},
	{
		id: 161,
		time: 160,
		velocity: 10.6044444444444,
		power: 8607.84902638668,
		road: 1258.91986111111,
		acceleration: 0.693148148148149
	},
	{
		id: 162,
		time: 161,
		velocity: 11.3705555555556,
		power: 8779.14934730547,
		road: 1269.85453703704,
		acceleration: 0.647314814814814
	},
	{
		id: 163,
		time: 162,
		velocity: 11.8,
		power: 7089.22887879938,
		road: 1281.33574074074,
		acceleration: 0.445740740740741
	},
	{
		id: 164,
		time: 163,
		velocity: 11.9416666666667,
		power: 4646.58840528418,
		road: 1293.14310185185,
		acceleration: 0.206574074074075
	},
	{
		id: 165,
		time: 164,
		velocity: 11.9902777777778,
		power: 798.207852661668,
		road: 1304.98583333333,
		acceleration: -0.135833333333336
	},
	{
		id: 166,
		time: 165,
		velocity: 11.3925,
		power: -1035.53312105041,
		road: 1316.61212962963,
		acceleration: -0.297037037037036
	},
	{
		id: 167,
		time: 166,
		velocity: 11.0505555555556,
		power: -2431.56828832182,
		road: 1327.87722222222,
		acceleration: -0.42537037037037
	},
	{
		id: 168,
		time: 167,
		velocity: 10.7141666666667,
		power: -2061.19077315104,
		road: 1338.73310185185,
		acceleration: -0.393055555555556
	},
	{
		id: 169,
		time: 168,
		velocity: 10.2133333333333,
		power: -1803.12104386433,
		road: 1349.2075,
		acceleration: -0.369907407407407
	},
	{
		id: 170,
		time: 169,
		velocity: 9.94083333333333,
		power: -515.217815679432,
		road: 1359.3775,
		acceleration: -0.238888888888889
	},
	{
		id: 171,
		time: 170,
		velocity: 9.9975,
		power: 195.335959451954,
		road: 1369.34662037037,
		acceleration: -0.162870370370371
	},
	{
		id: 172,
		time: 171,
		velocity: 9.72472222222222,
		power: -1543.49842315868,
		road: 1379.06060185185,
		acceleration: -0.347407407407404
	},
	{
		id: 173,
		time: 172,
		velocity: 8.89861111111111,
		power: -5792.83222717376,
		road: 1388.18018518518,
		acceleration: -0.84138888888889
	},
	{
		id: 174,
		time: 173,
		velocity: 7.47333333333333,
		power: -10610.422175396,
		road: 1396.09305555556,
		acceleration: -1.57203703703704
	},
	{
		id: 175,
		time: 174,
		velocity: 5.00861111111111,
		power: -11123.6047043595,
		road: 1402.18513888889,
		acceleration: -2.06953703703704
	},
	{
		id: 176,
		time: 175,
		velocity: 2.69,
		power: -7186.93861996283,
		road: 1406.24074074074,
		acceleration: -2.00342592592593
	},
	{
		id: 177,
		time: 176,
		velocity: 1.46305555555556,
		power: -2591.36648667313,
		road: 1408.66601851852,
		acceleration: -1.25722222222222
	},
	{
		id: 178,
		time: 177,
		velocity: 1.23694444444444,
		power: -448.603542431147,
		road: 1410.2487962963,
		acceleration: -0.427777777777778
	},
	{
		id: 179,
		time: 178,
		velocity: 1.40666666666667,
		power: 427.080607642357,
		road: 1411.70773148148,
		acceleration: 0.180092592592592
	},
	{
		id: 180,
		time: 179,
		velocity: 2.00333333333333,
		power: 506.444585511953,
		road: 1413.35439814815,
		acceleration: 0.19537037037037
	},
	{
		id: 181,
		time: 180,
		velocity: 1.82305555555556,
		power: 441.992567682682,
		road: 1415.16296296296,
		acceleration: 0.128425925925926
	},
	{
		id: 182,
		time: 181,
		velocity: 1.79194444444444,
		power: -9.18234404096264,
		road: 1416.96842592593,
		acceleration: -0.13462962962963
	},
	{
		id: 183,
		time: 182,
		velocity: 1.59944444444444,
		power: 1245.20268875927,
		road: 1418.96972222222,
		acceleration: 0.526296296296296
	},
	{
		id: 184,
		time: 183,
		velocity: 3.40194444444444,
		power: 2664.8094859453,
		road: 1421.68541666667,
		acceleration: 0.9025
	},
	{
		id: 185,
		time: 184,
		velocity: 4.49944444444444,
		power: 5995.7764855278,
		road: 1425.59236111111,
		acceleration: 1.48
	},
	{
		id: 186,
		time: 185,
		velocity: 6.03944444444444,
		power: 4909.24398106465,
		road: 1430.67643518519,
		acceleration: 0.874259259259258
	},
	{
		id: 187,
		time: 186,
		velocity: 6.02472222222222,
		power: 4170.01879489297,
		road: 1436.50097222222,
		acceleration: 0.606666666666668
	},
	{
		id: 188,
		time: 187,
		velocity: 6.31944444444444,
		power: 1931.19148431689,
		road: 1442.71763888889,
		acceleration: 0.177592592592593
	},
	{
		id: 189,
		time: 188,
		velocity: 6.57222222222222,
		power: 1666.98863569451,
		road: 1449.08564814815,
		acceleration: 0.125092592592591
	},
	{
		id: 190,
		time: 189,
		velocity: 6.4,
		power: 902.318135053732,
		road: 1455.51467592593,
		acceleration: -0.00305555555555515
	},
	{
		id: 191,
		time: 190,
		velocity: 6.31027777777778,
		power: -575.811019395715,
		road: 1461.81925925926,
		acceleration: -0.245833333333334
	},
	{
		id: 192,
		time: 191,
		velocity: 5.83472222222222,
		power: -1778.55317269118,
		road: 1467.7700462963,
		acceleration: -0.461759259259258
	},
	{
		id: 193,
		time: 192,
		velocity: 5.01472222222222,
		power: -3272.11359866694,
		road: 1473.09490740741,
		acceleration: -0.790092592592592
	},
	{
		id: 194,
		time: 193,
		velocity: 3.94,
		power: -4325.26181678148,
		road: 1477.43041666667,
		acceleration: -1.18861111111111
	},
	{
		id: 195,
		time: 194,
		velocity: 2.26888888888889,
		power: -4243.22530902578,
		road: 1480.33583333333,
		acceleration: -1.67157407407407
	},
	{
		id: 196,
		time: 195,
		velocity: 0,
		power: -1586.87343749021,
		road: 1481.7487962963,
		acceleration: -1.31333333333333
	},
	{
		id: 197,
		time: 196,
		velocity: 0,
		power: -225.252767901235,
		road: 1482.12694444444,
		acceleration: -0.756296296296296
	},
	{
		id: 198,
		time: 197,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 199,
		time: 198,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 200,
		time: 199,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 201,
		time: 200,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 202,
		time: 201,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 203,
		time: 202,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 204,
		time: 203,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 205,
		time: 204,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 206,
		time: 205,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 207,
		time: 206,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 208,
		time: 207,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 209,
		time: 208,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 210,
		time: 209,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 211,
		time: 210,
		velocity: 0,
		power: 0,
		road: 1482.12694444444,
		acceleration: 0
	},
	{
		id: 212,
		time: 211,
		velocity: 0,
		power: 79.3627088022954,
		road: 1482.30217592593,
		acceleration: 0.350462962962963
	},
	{
		id: 213,
		time: 212,
		velocity: 1.05138888888889,
		power: 529.871575463393,
		road: 1482.99324074074,
		acceleration: 0.681203703703704
	},
	{
		id: 214,
		time: 213,
		velocity: 2.04361111111111,
		power: 3233.77970411988,
		road: 1484.86893518518,
		acceleration: 1.68805555555556
	},
	{
		id: 215,
		time: 214,
		velocity: 5.06416666666667,
		power: 5340.77838172611,
		road: 1488.33342592593,
		acceleration: 1.48953703703704
	},
	{
		id: 216,
		time: 215,
		velocity: 5.52,
		power: 5759.28644930156,
		road: 1493.1074537037,
		acceleration: 1.12953703703704
	},
	{
		id: 217,
		time: 216,
		velocity: 5.43222222222222,
		power: 2221.72149414414,
		road: 1498.58740740741,
		acceleration: 0.282314814814814
	},
	{
		id: 218,
		time: 217,
		velocity: 5.91111111111111,
		power: 2498.15568960373,
		road: 1504.36296296296,
		acceleration: 0.308888888888888
	},
	{
		id: 219,
		time: 218,
		velocity: 6.44666666666667,
		power: 6173.66707090146,
		road: 1510.72787037037,
		acceleration: 0.869814814814815
	},
	{
		id: 220,
		time: 219,
		velocity: 8.04166666666667,
		power: 10882.7085809517,
		road: 1518.21222222222,
		acceleration: 1.36907407407407
	},
	{
		id: 221,
		time: 220,
		velocity: 10.0183333333333,
		power: 15714.6279469304,
		road: 1527.2112962963,
		acceleration: 1.66037037037037
	},
	{
		id: 222,
		time: 221,
		velocity: 11.4277777777778,
		power: 23263.470978762,
		road: 1538.06731481481,
		acceleration: 2.05351851851852
	},
	{
		id: 223,
		time: 222,
		velocity: 14.2022222222222,
		power: 21277.7876694916,
		road: 1550.72208333333,
		acceleration: 1.54398148148148
	},
	{
		id: 224,
		time: 223,
		velocity: 14.6502777777778,
		power: 25666.1230623286,
		road: 1564.97069444444,
		acceleration: 1.64370370370371
	},
	{
		id: 225,
		time: 224,
		velocity: 16.3588888888889,
		power: 25294.3063902129,
		road: 1580.74583333333,
		acceleration: 1.40935185185185
	},
	{
		id: 226,
		time: 225,
		velocity: 18.4302777777778,
		power: 32706.6880464946,
		road: 1598.06416666667,
		acceleration: 1.67703703703704
	},
	{
		id: 227,
		time: 226,
		velocity: 19.6813888888889,
		power: 38315.6706453775,
		road: 1617.10550925926,
		acceleration: 1.76898148148148
	},
	{
		id: 228,
		time: 227,
		velocity: 21.6658333333333,
		power: 34270.8387513389,
		road: 1637.71527777778,
		acceleration: 1.36787037037037
	},
	{
		id: 229,
		time: 228,
		velocity: 22.5338888888889,
		power: 13240.9988188901,
		road: 1659.13833333333,
		acceleration: 0.258703703703709
	},
	{
		id: 230,
		time: 229,
		velocity: 20.4575,
		power: -22081.7812592615,
		road: 1679.95157407407,
		acceleration: -1.47833333333334
	},
	{
		id: 231,
		time: 230,
		velocity: 17.2308333333333,
		power: -41654.8621983124,
		road: 1698.70240740741,
		acceleration: -2.64648148148148
	},
	{
		id: 232,
		time: 231,
		velocity: 14.5944444444444,
		power: -36191.7720794634,
		road: 1714.81819444444,
		acceleration: -2.62361111111111
	},
	{
		id: 233,
		time: 232,
		velocity: 12.5866666666667,
		power: -20028.6516443117,
		road: 1728.75083333333,
		acceleration: -1.74268518518518
	},
	{
		id: 234,
		time: 233,
		velocity: 12.0027777777778,
		power: -8427.07669013657,
		road: 1741.35319444444,
		acceleration: -0.917870370370371
	},
	{
		id: 235,
		time: 234,
		velocity: 11.8408333333333,
		power: -3573.9439298463,
		road: 1753.23541666667,
		acceleration: -0.522407407407407
	},
	{
		id: 236,
		time: 235,
		velocity: 11.0194444444444,
		power: -1225.26436405472,
		road: 1764.69962962963,
		acceleration: -0.31361111111111
	},
	{
		id: 237,
		time: 236,
		velocity: 11.0619444444444,
		power: -410.419185081516,
		road: 1775.8887962963,
		acceleration: -0.236481481481482
	},
	{
		id: 238,
		time: 237,
		velocity: 11.1313888888889,
		power: 5683.4839185294,
		road: 1787.12564814815,
		acceleration: 0.33185185185185
	},
	{
		id: 239,
		time: 238,
		velocity: 12.015,
		power: 9553.92334120408,
		road: 1798.85300925926,
		acceleration: 0.649166666666668
	},
	{
		id: 240,
		time: 239,
		velocity: 13.0094444444444,
		power: 13002.0036716159,
		road: 1811.34273148148,
		acceleration: 0.875555555555556
	},
	{
		id: 241,
		time: 240,
		velocity: 13.7580555555556,
		power: 11072.3139912052,
		road: 1824.59462962963,
		acceleration: 0.648796296296297
	},
	{
		id: 242,
		time: 241,
		velocity: 13.9613888888889,
		power: 7621.30736603746,
		road: 1838.34416666667,
		acceleration: 0.346481481481483
	},
	{
		id: 243,
		time: 242,
		velocity: 14.0488888888889,
		power: 4450.55511216647,
		road: 1852.315,
		acceleration: 0.0961111111111084
	},
	{
		id: 244,
		time: 243,
		velocity: 14.0463888888889,
		power: 2702.40806227463,
		road: 1866.31597222222,
		acceleration: -0.0358333333333309
	},
	{
		id: 245,
		time: 244,
		velocity: 13.8538888888889,
		power: 3422.63820014233,
		road: 1880.30819444444,
		acceleration: 0.0183333333333309
	},
	{
		id: 246,
		time: 245,
		velocity: 14.1038888888889,
		power: 3777.86912574956,
		road: 1894.33148148148,
		acceleration: 0.0437962962962963
	},
	{
		id: 247,
		time: 246,
		velocity: 14.1777777777778,
		power: 2690.4252867594,
		road: 1908.35791666667,
		acceleration: -0.0374999999999979
	},
	{
		id: 248,
		time: 247,
		velocity: 13.7413888888889,
		power: -999.671991508238,
		road: 1922.21,
		acceleration: -0.311203703703708
	},
	{
		id: 249,
		time: 248,
		velocity: 13.1702777777778,
		power: -2176.53544976661,
		road: 1935.70694444444,
		acceleration: -0.399074074074074
	},
	{
		id: 250,
		time: 249,
		velocity: 12.9805555555556,
		power: -218.39666396645,
		road: 1948.88300925926,
		acceleration: -0.242685185185186
	},
	{
		id: 251,
		time: 250,
		velocity: 13.0133333333333,
		power: 1724.18167112217,
		road: 1961.89569444444,
		acceleration: -0.0840740740740706
	},
	{
		id: 252,
		time: 251,
		velocity: 12.9180555555556,
		power: 2439.9238897436,
		road: 1974.85393518519,
		acceleration: -0.024814814814814
	},
	{
		id: 253,
		time: 252,
		velocity: 12.9061111111111,
		power: 2391.30913344901,
		road: 1987.78578703704,
		acceleration: -0.0279629629629632
	},
	{
		id: 254,
		time: 253,
		velocity: 12.9294444444444,
		power: 2852.7496038556,
		road: 2000.70851851852,
		acceleration: 0.00972222222222285
	},
	{
		id: 255,
		time: 254,
		velocity: 12.9472222222222,
		power: 3448.93401902686,
		road: 2013.66453703704,
		acceleration: 0.0568518518518495
	},
	{
		id: 256,
		time: 255,
		velocity: 13.0766666666667,
		power: 3643.21719764836,
		road: 2026.68407407407,
		acceleration: 0.0701851851851867
	},
	{
		id: 257,
		time: 256,
		velocity: 13.14,
		power: 3896.43818313421,
		road: 2039.7824537037,
		acceleration: 0.0874999999999986
	},
	{
		id: 258,
		time: 257,
		velocity: 13.2097222222222,
		power: 3185.0946227035,
		road: 2052.93888888889,
		acceleration: 0.0286111111111111
	},
	{
		id: 259,
		time: 258,
		velocity: 13.1625,
		power: 2019.83015103694,
		road: 2066.07777777778,
		acceleration: -0.0637037037037036
	},
	{
		id: 260,
		time: 259,
		velocity: 12.9488888888889,
		power: 2263.10965994351,
		road: 2079.16342592593,
		acceleration: -0.0427777777777774
	},
	{
		id: 261,
		time: 260,
		velocity: 13.0813888888889,
		power: 2342.53999674059,
		road: 2092.2100462963,
		acceleration: -0.0352777777777789
	},
	{
		id: 262,
		time: 261,
		velocity: 13.0566666666667,
		power: 3918.18642762704,
		road: 2105.28412037037,
		acceleration: 0.0901851851851863
	},
	{
		id: 263,
		time: 262,
		velocity: 13.2194444444444,
		power: 3567.65058330062,
		road: 2118.43296296296,
		acceleration: 0.0593518518518508
	},
	{
		id: 264,
		time: 263,
		velocity: 13.2594444444444,
		power: 4865.80626948758,
		road: 2131.69046296296,
		acceleration: 0.157962962962962
	},
	{
		id: 265,
		time: 264,
		velocity: 13.5305555555556,
		power: -850.153196257042,
		road: 2144.88041666667,
		acceleration: -0.293055555555554
	},
	{
		id: 266,
		time: 265,
		velocity: 12.3402777777778,
		power: 812.130929703264,
		road: 2157.84555555556,
		acceleration: -0.156574074074074
	},
	{
		id: 267,
		time: 266,
		velocity: 12.7897222222222,
		power: -123.458170140459,
		road: 2170.61763888889,
		acceleration: -0.229537037037035
	},
	{
		id: 268,
		time: 267,
		velocity: 12.8419444444444,
		power: 4348.08447435239,
		road: 2183.34458333333,
		acceleration: 0.139259259259259
	},
	{
		id: 269,
		time: 268,
		velocity: 12.7580555555556,
		power: 2784.73926343706,
		road: 2196.14523148148,
		acceleration: 0.00814814814814824
	},
	{
		id: 270,
		time: 269,
		velocity: 12.8141666666667,
		power: 3120.70710577548,
		road: 2208.96740740741,
		acceleration: 0.0349074074074061
	},
	{
		id: 271,
		time: 270,
		velocity: 12.9466666666667,
		power: 3503.41797753979,
		road: 2221.83921296296,
		acceleration: 0.0643518518518515
	},
	{
		id: 272,
		time: 271,
		velocity: 12.9511111111111,
		power: 3422.05853766221,
		road: 2234.77097222222,
		acceleration: 0.0555555555555571
	},
	{
		id: 273,
		time: 272,
		velocity: 12.9808333333333,
		power: 4924.72809892912,
		road: 2247.81657407407,
		acceleration: 0.172129629629628
	},
	{
		id: 274,
		time: 273,
		velocity: 13.4630555555556,
		power: 5372.77020136831,
		road: 2261.04787037037,
		acceleration: 0.199259259259259
	},
	{
		id: 275,
		time: 274,
		velocity: 13.5488888888889,
		power: 5967.31858665006,
		road: 2274.49648148148,
		acceleration: 0.23537037037037
	},
	{
		id: 276,
		time: 275,
		velocity: 13.6869444444444,
		power: 5068.60016098615,
		road: 2288.14111111111,
		acceleration: 0.156666666666666
	},
	{
		id: 277,
		time: 276,
		velocity: 13.9330555555556,
		power: 4750.565133187,
		road: 2301.92722222222,
		acceleration: 0.126296296296296
	},
	{
		id: 278,
		time: 277,
		velocity: 13.9277777777778,
		power: 4229.99240719224,
		road: 2315.81782407407,
		acceleration: 0.0826851851851877
	},
	{
		id: 279,
		time: 278,
		velocity: 13.935,
		power: 3740.65227531199,
		road: 2329.77152777778,
		acceleration: 0.0435185185185176
	},
	{
		id: 280,
		time: 279,
		velocity: 14.0636111111111,
		power: 4386.30042553527,
		road: 2343.79166666667,
		acceleration: 0.0893518518518519
	},
	{
		id: 281,
		time: 280,
		velocity: 14.1958333333333,
		power: 4948.26106290932,
		road: 2357.91986111111,
		acceleration: 0.126759259259259
	},
	{
		id: 282,
		time: 281,
		velocity: 14.3152777777778,
		power: 6031.11926174327,
		road: 2372.21106481481,
		acceleration: 0.199259259259259
	},
	{
		id: 283,
		time: 282,
		velocity: 14.6613888888889,
		power: 5704.55805726567,
		road: 2386.68541666667,
		acceleration: 0.167037037037039
	},
	{
		id: 284,
		time: 283,
		velocity: 14.6969444444444,
		power: 5754.97548740082,
		road: 2401.32490740741,
		acceleration: 0.16324074074074
	},
	{
		id: 285,
		time: 284,
		velocity: 14.805,
		power: 5444.64733961383,
		road: 2416.11333333333,
		acceleration: 0.134629629629629
	},
	{
		id: 286,
		time: 285,
		velocity: 15.0652777777778,
		power: 5193.89975581061,
		road: 2431.0249537037,
		acceleration: 0.111759259259259
	},
	{
		id: 287,
		time: 286,
		velocity: 15.0322222222222,
		power: 5338.92388433528,
		road: 2446.05101851852,
		acceleration: 0.11712962962963
	},
	{
		id: 288,
		time: 287,
		velocity: 15.1563888888889,
		power: 998.595292783795,
		road: 2461.04342592593,
		acceleration: -0.184444444444443
	},
	{
		id: 289,
		time: 288,
		velocity: 14.5119444444444,
		power: 3895.21027885419,
		road: 2475.95393518518,
		acceleration: 0.0206481481481475
	},
	{
		id: 290,
		time: 289,
		velocity: 15.0941666666667,
		power: 2317.18369368882,
		road: 2490.83018518518,
		acceleration: -0.0891666666666655
	},
	{
		id: 291,
		time: 290,
		velocity: 14.8888888888889,
		power: 7450.13819371855,
		road: 2505.79550925926,
		acceleration: 0.267314814814814
	},
	{
		id: 292,
		time: 291,
		velocity: 15.3138888888889,
		power: 4669.28388048731,
		road: 2520.92768518518,
		acceleration: 0.0663888888888877
	},
	{
		id: 293,
		time: 292,
		velocity: 15.2933333333333,
		power: 8687.8412023274,
		road: 2536.25953703704,
		acceleration: 0.332962962962963
	},
	{
		id: 294,
		time: 293,
		velocity: 15.8877777777778,
		power: 11347.5262532445,
		road: 2552.00134259259,
		acceleration: 0.486944444444443
	},
	{
		id: 295,
		time: 294,
		velocity: 16.7747222222222,
		power: 13047.2404166637,
		road: 2568.26773148148,
		acceleration: 0.562222222222227
	},
	{
		id: 296,
		time: 295,
		velocity: 16.98,
		power: 15831.4195419311,
		road: 2585.16092592592,
		acceleration: 0.691388888888888
	},
	{
		id: 297,
		time: 296,
		velocity: 17.9619444444444,
		power: 9834.4962411803,
		road: 2602.54652777778,
		acceleration: 0.293425925925924
	},
	{
		id: 298,
		time: 297,
		velocity: 17.655,
		power: 7260.71552910001,
		road: 2620.14347222222,
		acceleration: 0.129259259259257
	},
	{
		id: 299,
		time: 298,
		velocity: 17.3677777777778,
		power: 1082.53436334266,
		road: 2637.68685185185,
		acceleration: -0.236388888888886
	},
	{
		id: 300,
		time: 299,
		velocity: 17.2527777777778,
		power: 753.910145797252,
		road: 2654.98675925926,
		acceleration: -0.250555555555554
	},
	{
		id: 301,
		time: 300,
		velocity: 16.9033333333333,
		power: 1768.15202919911,
		road: 2672.06953703704,
		acceleration: -0.183703703703706
	},
	{
		id: 302,
		time: 301,
		velocity: 16.8166666666667,
		power: 468.846643695627,
		road: 2688.93115740741,
		acceleration: -0.258611111111112
	},
	{
		id: 303,
		time: 302,
		velocity: 16.4769444444444,
		power: 1948.12315876104,
		road: 2705.58277777778,
		acceleration: -0.16138888888889
	},
	{
		id: 304,
		time: 303,
		velocity: 16.4191666666667,
		power: 842.467034195791,
		road: 2722.04041666667,
		acceleration: -0.226574074074072
	},
	{
		id: 305,
		time: 304,
		velocity: 16.1369444444444,
		power: 2371.56872925061,
		road: 2738.3225,
		acceleration: -0.124537037037037
	},
	{
		id: 306,
		time: 305,
		velocity: 16.1033333333333,
		power: 1748.8843933113,
		road: 2754.46185185185,
		acceleration: -0.160925925925923
	},
	{
		id: 307,
		time: 306,
		velocity: 15.9363888888889,
		power: 3663.85322562762,
		road: 2770.50393518518,
		acceleration: -0.0336111111111137
	},
	{
		id: 308,
		time: 307,
		velocity: 16.0361111111111,
		power: 4916.30776863508,
		road: 2786.55310185185,
		acceleration: 0.0477777777777781
	},
	{
		id: 309,
		time: 308,
		velocity: 16.2466666666667,
		power: 5489.59713115958,
		road: 2802.66740740741,
		acceleration: 0.0824999999999996
	},
	{
		id: 310,
		time: 309,
		velocity: 16.1838888888889,
		power: 5648.02571773239,
		road: 2818.86759259259,
		acceleration: 0.0892592592592614
	},
	{
		id: 311,
		time: 310,
		velocity: 16.3038888888889,
		power: 3864.30959833739,
		road: 2835.09893518518,
		acceleration: -0.026944444444446
	},
	{
		id: 312,
		time: 311,
		velocity: 16.1658333333333,
		power: 2983.70942831426,
		road: 2851.27578703704,
		acceleration: -0.0820370370370362
	},
	{
		id: 313,
		time: 312,
		velocity: 15.9377777777778,
		power: 1371.1944867777,
		road: 2867.3200462963,
		acceleration: -0.183148148148151
	},
	{
		id: 314,
		time: 313,
		velocity: 15.7544444444444,
		power: 1373.69922584003,
		road: 2883.18337962963,
		acceleration: -0.178703703703704
	},
	{
		id: 315,
		time: 314,
		velocity: 15.6297222222222,
		power: 2741.60732433991,
		road: 2898.91504629629,
		acceleration: -0.0846296296296263
	},
	{
		id: 316,
		time: 315,
		velocity: 15.6838888888889,
		power: 3129.08469838355,
		road: 2914.57606481481,
		acceleration: -0.0566666666666702
	},
	{
		id: 317,
		time: 316,
		velocity: 15.5844444444444,
		power: 4180.92979709689,
		road: 2930.21592592592,
		acceleration: 0.0143518518518526
	},
	{
		id: 318,
		time: 317,
		velocity: 15.6727777777778,
		power: 1326.47365945307,
		road: 2945.77560185185,
		acceleration: -0.174722222222222
	},
	{
		id: 319,
		time: 318,
		velocity: 15.1597222222222,
		power: 3301.38381576054,
		road: 2961.22865740741,
		acceleration: -0.0385185185185186
	},
	{
		id: 320,
		time: 319,
		velocity: 15.4688888888889,
		power: 4036.98510554059,
		road: 2976.66833333333,
		acceleration: 0.0117592592592608
	},
	{
		id: 321,
		time: 320,
		velocity: 15.7080555555556,
		power: 9377.35760765603,
		road: 2992.29523148148,
		acceleration: 0.362685185185185
	},
	{
		id: 322,
		time: 321,
		velocity: 16.2477777777778,
		power: 9266.32517286667,
		road: 3008.27115740741,
		acceleration: 0.335370370370368
	},
	{
		id: 323,
		time: 322,
		velocity: 16.475,
		power: 9289.0839602857,
		road: 3024.57407407407,
		acceleration: 0.31861111111111
	},
	{
		id: 324,
		time: 323,
		velocity: 16.6638888888889,
		power: 9572.24492577765,
		road: 3041.19578703704,
		acceleration: 0.318981481481483
	},
	{
		id: 325,
		time: 324,
		velocity: 17.2047222222222,
		power: 8000.18920123778,
		road: 3058.08060185185,
		acceleration: 0.207222222222221
	},
	{
		id: 326,
		time: 325,
		velocity: 17.0966666666667,
		power: 6441.5666388059,
		road: 3075.12106481481,
		acceleration: 0.104074074074074
	},
	{
		id: 327,
		time: 326,
		velocity: 16.9761111111111,
		power: 1560.1712445123,
		road: 3092.11643518518,
		acceleration: -0.194259259259255
	},
	{
		id: 328,
		time: 327,
		velocity: 16.6219444444444,
		power: 772.870761296288,
		road: 3108.89569444444,
		acceleration: -0.237962962962964
	},
	{
		id: 329,
		time: 328,
		velocity: 16.3827777777778,
		power: 392.381797120364,
		road: 3125.42763888889,
		acceleration: -0.256666666666668
	},
	{
		id: 330,
		time: 329,
		velocity: 16.2061111111111,
		power: 3011.22132507847,
		road: 3141.78833333333,
		acceleration: -0.0858333333333334
	},
	{
		id: 331,
		time: 330,
		velocity: 16.3644444444444,
		power: 4187.62254584721,
		road: 3158.10162037037,
		acceleration: -0.00898148148148081
	},
	{
		id: 332,
		time: 331,
		velocity: 16.3558333333333,
		power: 5914.37687470859,
		road: 3174.46032407407,
		acceleration: 0.0998148148148132
	},
	{
		id: 333,
		time: 332,
		velocity: 16.5055555555556,
		power: 5918.82524137803,
		road: 3190.91694444444,
		acceleration: 0.0960185185185196
	},
	{
		id: 334,
		time: 333,
		velocity: 16.6525,
		power: 3560.01631115886,
		road: 3207.39430555555,
		acceleration: -0.0545370370370364
	},
	{
		id: 335,
		time: 334,
		velocity: 16.1922222222222,
		power: 1768.11509893984,
		road: 3223.76166666667,
		acceleration: -0.165462962962962
	},
	{
		id: 336,
		time: 335,
		velocity: 16.0091666666667,
		power: -1364.12590162896,
		road: 3239.86518518518,
		acceleration: -0.362222222222226
	},
	{
		id: 337,
		time: 336,
		velocity: 15.5658333333333,
		power: -180.464267564563,
		road: 3255.64768518518,
		acceleration: -0.279814814814813
	},
	{
		id: 338,
		time: 337,
		velocity: 15.3527777777778,
		power: 124.844035425675,
		road: 3271.16291666666,
		acceleration: -0.254722222222222
	},
	{
		id: 339,
		time: 338,
		velocity: 15.245,
		power: 3603.18295998094,
		road: 3286.54296296296,
		acceleration: -0.0156481481481503
	},
	{
		id: 340,
		time: 339,
		velocity: 15.5188888888889,
		power: 6620.51594420675,
		road: 3302.00800925926,
		acceleration: 0.185648148148148
	},
	{
		id: 341,
		time: 340,
		velocity: 15.9097222222222,
		power: 9102.72793487472,
		road: 3317.73518518518,
		acceleration: 0.338611111111112
	},
	{
		id: 342,
		time: 341,
		velocity: 16.2608333333333,
		power: 7723.02984888155,
		road: 3333.74791666666,
		acceleration: 0.232500000000002
	},
	{
		id: 343,
		time: 342,
		velocity: 16.2163888888889,
		power: 6551.96474014993,
		road: 3349.95064814815,
		acceleration: 0.147500000000001
	},
	{
		id: 344,
		time: 343,
		velocity: 16.3522222222222,
		power: 5305.15840296544,
		road: 3366.25856481481,
		acceleration: 0.0628703703703692
	},
	{
		id: 345,
		time: 344,
		velocity: 16.4494444444444,
		power: 6147.77790860482,
		road: 3382.65449074074,
		acceleration: 0.113148148148149
	},
	{
		id: 346,
		time: 345,
		velocity: 16.5558333333333,
		power: 7158.52729318435,
		road: 3399.1925,
		acceleration: 0.171018518518519
	},
	{
		id: 347,
		time: 346,
		velocity: 16.8652777777778,
		power: 7420.69700942019,
		road: 3415.90569444444,
		acceleration: 0.179351851851848
	},
	{
		id: 348,
		time: 347,
		velocity: 16.9875,
		power: 6246.62865688391,
		road: 3432.75856481481,
		acceleration: 0.100000000000001
	},
	{
		id: 349,
		time: 348,
		velocity: 16.8558333333333,
		power: 4318.05985918275,
		road: 3449.65092592592,
		acceleration: -0.0210185185185168
	},
	{
		id: 350,
		time: 349,
		velocity: 16.8022222222222,
		power: 2634.36950440323,
		road: 3466.4712037037,
		acceleration: -0.123148148148147
	},
	{
		id: 351,
		time: 350,
		velocity: 16.6180555555556,
		power: 2519.32233245233,
		road: 3483.16652777778,
		acceleration: -0.126759259259259
	},
	{
		id: 352,
		time: 351,
		velocity: 16.4755555555556,
		power: 2658.87034697012,
		road: 3499.7412037037,
		acceleration: -0.114537037037039
	},
	{
		id: 353,
		time: 352,
		velocity: 16.4586111111111,
		power: 3181.03654713887,
		road: 3516.21930555555,
		acceleration: -0.0786111111111083
	},
	{
		id: 354,
		time: 353,
		velocity: 16.3822222222222,
		power: 2230.39989770007,
		road: 3532.59009259259,
		acceleration: -0.136018518518522
	},
	{
		id: 355,
		time: 354,
		velocity: 16.0675,
		power: 1974.91321065178,
		road: 3548.81856481481,
		acceleration: -0.148611111111109
	},
	{
		id: 356,
		time: 355,
		velocity: 16.0127777777778,
		power: 915.020383874398,
		road: 3564.86625,
		acceleration: -0.212962962962967
	},
	{
		id: 357,
		time: 356,
		velocity: 15.7433333333333,
		power: 649.501653337786,
		road: 3580.69458333333,
		acceleration: -0.225740740740736
	},
	{
		id: 358,
		time: 357,
		velocity: 15.3902777777778,
		power: 1858.81205622836,
		road: 3596.33949074074,
		acceleration: -0.141111111111114
	},
	{
		id: 359,
		time: 358,
		velocity: 15.5894444444444,
		power: 3943.69233542846,
		road: 3611.91421296296,
		acceleration: 0.000740740740740264
	},
	{
		id: 360,
		time: 359,
		velocity: 15.7455555555556,
		power: 7083.77674269991,
		road: 3627.59263888889,
		acceleration: 0.206666666666667
	},
	{
		id: 361,
		time: 360,
		velocity: 16.0102777777778,
		power: 6299.52260269298,
		road: 3643.44763888889,
		acceleration: 0.146481481481482
	},
	{
		id: 362,
		time: 361,
		velocity: 16.0288888888889,
		power: 5988.99523377262,
		road: 3659.43601851852,
		acceleration: 0.120277777777776
	},
	{
		id: 363,
		time: 362,
		velocity: 16.1063888888889,
		power: 4687.90628525469,
		road: 3675.50069444444,
		acceleration: 0.0323148148148142
	},
	{
		id: 364,
		time: 363,
		velocity: 16.1072222222222,
		power: 2911.44183586957,
		road: 3691.54023148148,
		acceleration: -0.0825925925925919
	},
	{
		id: 365,
		time: 364,
		velocity: 15.7811111111111,
		power: 2862.14817217625,
		road: 3707.49680555555,
		acceleration: -0.0833333333333321
	},
	{
		id: 366,
		time: 365,
		velocity: 15.8563888888889,
		power: 2604.91630371788,
		road: 3723.36291666667,
		acceleration: -0.0975925925925925
	},
	{
		id: 367,
		time: 366,
		velocity: 15.8144444444444,
		power: 4525.57975886622,
		road: 3739.19537037037,
		acceleration: 0.0302777777777781
	},
	{
		id: 368,
		time: 367,
		velocity: 15.8719444444444,
		power: 4188.11384119131,
		road: 3755.04662037037,
		acceleration: 0.00731481481481211
	},
	{
		id: 369,
		time: 368,
		velocity: 15.8783333333333,
		power: 4987.66272739746,
		road: 3770.93092592592,
		acceleration: 0.0587962962962987
	},
	{
		id: 370,
		time: 369,
		velocity: 15.9908333333333,
		power: 4951.86481232162,
		road: 3786.87175925926,
		acceleration: 0.0542592592592595
	},
	{
		id: 371,
		time: 370,
		velocity: 16.0347222222222,
		power: 5024.20680514575,
		road: 3802.86814814815,
		acceleration: 0.0568518518518513
	},
	{
		id: 372,
		time: 371,
		velocity: 16.0488888888889,
		power: 4500.83751545837,
		road: 3818.90356481481,
		acceleration: 0.0212037037037049
	},
	{
		id: 373,
		time: 372,
		velocity: 16.0544444444444,
		power: 3628.4786239421,
		road: 3834.93185185185,
		acceleration: -0.0354629629629635
	},
	{
		id: 374,
		time: 373,
		velocity: 15.9283333333333,
		power: 2846.23748575546,
		road: 3850.90004629629,
		acceleration: -0.0847222222222239
	},
	{
		id: 375,
		time: 374,
		velocity: 15.7947222222222,
		power: 475.112631602209,
		road: 3866.7074537037,
		acceleration: -0.236851851851849
	},
	{
		id: 376,
		time: 375,
		velocity: 15.3438888888889,
		power: -891.478266514681,
		road: 3882.23472222222,
		acceleration: -0.323425925925928
	},
	{
		id: 377,
		time: 376,
		velocity: 14.9580555555556,
		power: -1044.76430287366,
		road: 3897.43546296296,
		acceleration: -0.329629629629631
	},
	{
		id: 378,
		time: 377,
		velocity: 14.8058333333333,
		power: -1467.41661852909,
		road: 3912.29375,
		acceleration: -0.355277777777777
	},
	{
		id: 379,
		time: 378,
		velocity: 14.2780555555556,
		power: 1038.05961018715,
		road: 3926.88787037037,
		acceleration: -0.173055555555553
	},
	{
		id: 380,
		time: 379,
		velocity: 14.4388888888889,
		power: -781.540229480372,
		road: 3941.24513888889,
		acceleration: -0.300648148148149
	},
	{
		id: 381,
		time: 380,
		velocity: 13.9038888888889,
		power: -26.7286590444045,
		road: 3955.33143518518,
		acceleration: -0.241296296296296
	},
	{
		id: 382,
		time: 381,
		velocity: 13.5541666666667,
		power: -712.996034753091,
		road: 3969.1525,
		acceleration: -0.289166666666665
	},
	{
		id: 383,
		time: 382,
		velocity: 13.5713888888889,
		power: 3096.40281251215,
		road: 3982.83106481481,
		acceleration: 0.00416666666666288
	},
	{
		id: 384,
		time: 383,
		velocity: 13.9163888888889,
		power: 6124.47621638023,
		road: 3996.62680555555,
		acceleration: 0.230185185185189
	},
	{
		id: 385,
		time: 384,
		velocity: 14.2447222222222,
		power: 8574.64059261265,
		road: 4010.73597222222,
		acceleration: 0.396666666666665
	},
	{
		id: 386,
		time: 385,
		velocity: 14.7613888888889,
		power: 7864.32638790727,
		road: 4025.20523148148,
		acceleration: 0.323518518518519
	},
	{
		id: 387,
		time: 386,
		velocity: 14.8869444444444,
		power: 9370.39591370743,
		road: 4040.04101851852,
		acceleration: 0.409537037037039
	},
	{
		id: 388,
		time: 387,
		velocity: 15.4733333333333,
		power: 7467.35345324556,
		road: 4055.21055555555,
		acceleration: 0.257962962962962
	},
	{
		id: 389,
		time: 388,
		velocity: 15.5352777777778,
		power: 7473.4207150335,
		road: 4070.63185185185,
		acceleration: 0.245555555555557
	},
	{
		id: 390,
		time: 389,
		velocity: 15.6236111111111,
		power: 5850.51488206047,
		road: 4086.23962962963,
		acceleration: 0.127407407407405
	},
	{
		id: 391,
		time: 390,
		velocity: 15.8555555555556,
		power: 7089.12199056313,
		road: 4102.01236111111,
		acceleration: 0.202499999999999
	},
	{
		id: 392,
		time: 391,
		velocity: 16.1427777777778,
		power: 7978.79133199677,
		road: 4118.01129629629,
		acceleration: 0.249907407407409
	},
	{
		id: 393,
		time: 392,
		velocity: 16.3733333333333,
		power: 7990.70495828835,
		road: 4134.25435185185,
		acceleration: 0.238333333333337
	},
	{
		id: 394,
		time: 393,
		velocity: 16.5705555555556,
		power: 8208.16886512471,
		road: 4150.73666666667,
		acceleration: 0.240185185185183
	},
	{
		id: 395,
		time: 394,
		velocity: 16.8633333333333,
		power: 6571.05361115254,
		road: 4167.40319444444,
		acceleration: 0.12824074074074
	},
	{
		id: 396,
		time: 395,
		velocity: 16.7580555555556,
		power: 6396.52697669506,
		road: 4184.18990740741,
		acceleration: 0.112129629629631
	},
	{
		id: 397,
		time: 396,
		velocity: 16.9069444444444,
		power: 5619.16179818915,
		road: 4201.06282407407,
		acceleration: 0.0602777777777739
	},
	{
		id: 398,
		time: 397,
		velocity: 17.0441666666667,
		power: 7090.43518985989,
		road: 4218.03925925926,
		acceleration: 0.146759259259259
	},
	{
		id: 399,
		time: 398,
		velocity: 17.1983333333333,
		power: 6453.34852836904,
		road: 4235.14018518518,
		acceleration: 0.102222222222224
	},
	{
		id: 400,
		time: 399,
		velocity: 17.2136111111111,
		power: 9186.07423903037,
		road: 4252.42212962963,
		acceleration: 0.259814814814817
	},
	{
		id: 401,
		time: 400,
		velocity: 17.8236111111111,
		power: 7033.10325428864,
		road: 4269.89458333333,
		acceleration: 0.121203703703699
	},
	{
		id: 402,
		time: 401,
		velocity: 17.5619444444444,
		power: 6783.4494533788,
		road: 4287.47837962963,
		acceleration: 0.101481481481486
	},
	{
		id: 403,
		time: 402,
		velocity: 17.5180555555556,
		power: 1987.07876745438,
		road: 4305.02166666667,
		acceleration: -0.182500000000001
	},
	{
		id: 404,
		time: 403,
		velocity: 17.2761111111111,
		power: 2685.2303056923,
		road: 4322.40555555555,
		acceleration: -0.136296296296297
	},
	{
		id: 405,
		time: 404,
		velocity: 17.1530555555556,
		power: 3798.86092758276,
		road: 4339.68828703704,
		acceleration: -0.0660185185185185
	},
	{
		id: 406,
		time: 405,
		velocity: 17.32,
		power: 5477.99792498397,
		road: 4356.95606481481,
		acceleration: 0.0361111111111114
	},
	{
		id: 407,
		time: 406,
		velocity: 17.3844444444444,
		power: 5676.27282001871,
		road: 4374.26513888889,
		acceleration: 0.0464814814814822
	},
	{
		id: 408,
		time: 407,
		velocity: 17.2925,
		power: 3185.47126789308,
		road: 4391.54592592593,
		acceleration: -0.10305555555556
	},
	{
		id: 409,
		time: 408,
		velocity: 17.0108333333333,
		power: 1046.41670642587,
		road: 4408.66087962963,
		acceleration: -0.228611111111107
	},
	{
		id: 410,
		time: 409,
		velocity: 16.6986111111111,
		power: 672.953641232172,
		road: 4425.53837962963,
		acceleration: -0.246296296296293
	},
	{
		id: 411,
		time: 410,
		velocity: 16.5536111111111,
		power: -632.699197797226,
		road: 4442.13148148148,
		acceleration: -0.322500000000002
	},
	{
		id: 412,
		time: 411,
		velocity: 16.0433333333333,
		power: 21.0167805310647,
		road: 4458.42546296296,
		acceleration: -0.275740740740741
	},
	{
		id: 413,
		time: 412,
		velocity: 15.8713888888889,
		power: -1.78998031277349,
		road: 4474.44546296296,
		acceleration: -0.272222222222222
	},
	{
		id: 414,
		time: 413,
		velocity: 15.7369444444444,
		power: 271.043884938615,
		road: 4490.20462962963,
		acceleration: -0.249444444444448
	},
	{
		id: 415,
		time: 414,
		velocity: 15.295,
		power: 1459.94988308321,
		road: 4505.7562962963,
		acceleration: -0.165555555555553
	},
	{
		id: 416,
		time: 415,
		velocity: 15.3747222222222,
		power: 3040.15323658205,
		road: 4521.19726851852,
		acceleration: -0.0558333333333358
	},
	{
		id: 417,
		time: 416,
		velocity: 15.5694444444444,
		power: 4803.70871013202,
		road: 4536.64208333333,
		acceleration: 0.0635185185185208
	},
	{
		id: 418,
		time: 417,
		velocity: 15.4855555555556,
		power: 4799.48195845348,
		road: 4552.14907407407,
		acceleration: 0.0608333333333331
	},
	{
		id: 419,
		time: 418,
		velocity: 15.5572222222222,
		power: 4827.295819096,
		road: 4567.71666666667,
		acceleration: 0.0603703703703697
	},
	{
		id: 420,
		time: 419,
		velocity: 15.7505555555556,
		power: 5307.96594457564,
		road: 4583.35925925926,
		acceleration: 0.0896296296296288
	},
	{
		id: 421,
		time: 420,
		velocity: 15.7544444444444,
		power: 5777.68396279698,
		road: 4599.105,
		acceleration: 0.116666666666669
	},
	{
		id: 422,
		time: 421,
		velocity: 15.9072222222222,
		power: 4989.24893955776,
		road: 4614.93949074074,
		acceleration: 0.0608333333333331
	},
	{
		id: 423,
		time: 422,
		velocity: 15.9330555555556,
		power: 4804.30526984282,
		road: 4630.82768518519,
		acceleration: 0.0465740740740728
	},
	{
		id: 424,
		time: 423,
		velocity: 15.8941666666667,
		power: 4532.43393663927,
		road: 4646.75282407407,
		acceleration: 0.0273148148148152
	},
	{
		id: 425,
		time: 424,
		velocity: 15.9891666666667,
		power: 4625.42697988106,
		road: 4662.70777777778,
		acceleration: 0.0323148148148125
	},
	{
		id: 426,
		time: 425,
		velocity: 16.03,
		power: 4277.87803789353,
		road: 4678.68328703704,
		acceleration: 0.00879629629629619
	},
	{
		id: 427,
		time: 426,
		velocity: 15.9205555555556,
		power: 2631.97761621491,
		road: 4694.61435185185,
		acceleration: -0.0976851851851848
	},
	{
		id: 428,
		time: 427,
		velocity: 15.6961111111111,
		power: -499.639418617699,
		road: 4710.34648148148,
		acceleration: -0.300185185185182
	},
	{
		id: 429,
		time: 428,
		velocity: 15.1294444444444,
		power: -1679.26898048484,
		road: 4725.74092592593,
		acceleration: -0.375185185185186
	},
	{
		id: 430,
		time: 429,
		velocity: 14.795,
		power: -1752.91885061268,
		road: 4740.75939814815,
		acceleration: -0.376759259259259
	},
	{
		id: 431,
		time: 430,
		velocity: 14.5658333333333,
		power: 798.990735261447,
		road: 4755.49296296296,
		acceleration: -0.193055555555555
	},
	{
		id: 432,
		time: 431,
		velocity: 14.5502777777778,
		power: 2867.41917589053,
		road: 4770.10875,
		acceleration: -0.0425000000000022
	},
	{
		id: 433,
		time: 432,
		velocity: 14.6675,
		power: 4673.88386319154,
		road: 4784.7462962963,
		acceleration: 0.0860185185185198
	},
	{
		id: 434,
		time: 433,
		velocity: 14.8238888888889,
		power: 5845.62554603731,
		road: 4799.50893518518,
		acceleration: 0.164166666666667
	},
	{
		id: 435,
		time: 434,
		velocity: 15.0427777777778,
		power: 7764.62973237372,
		road: 4814.49768518519,
		acceleration: 0.288055555555555
	},
	{
		id: 436,
		time: 435,
		velocity: 15.5316666666667,
		power: 8356.33632341933,
		road: 4829.78680555556,
		acceleration: 0.312685185185186
	},
	{
		id: 437,
		time: 436,
		velocity: 15.7619444444444,
		power: 5992.39333146955,
		road: 4845.30273148148,
		acceleration: 0.140925925925925
	},
	{
		id: 438,
		time: 437,
		velocity: 15.4655555555556,
		power: 3040.86868031987,
		road: 4860.85944444444,
		acceleration: -0.0593518518518508
	},
	{
		id: 439,
		time: 438,
		velocity: 15.3536111111111,
		power: 1672.54093556011,
		road: 4876.31208333333,
		acceleration: -0.148796296296297
	},
	{
		id: 440,
		time: 439,
		velocity: 15.3155555555556,
		power: 3230.91427223617,
		road: 4891.67018518518,
		acceleration: -0.0402777777777779
	},
	{
		id: 441,
		time: 440,
		velocity: 15.3447222222222,
		power: 1089.3937587278,
		road: 4906.9162962963,
		acceleration: -0.183703703703706
	},
	{
		id: 442,
		time: 441,
		velocity: 14.8025,
		power: 1248.00821676101,
		road: 4921.98615740741,
		acceleration: -0.168796296296295
	},
	{
		id: 443,
		time: 442,
		velocity: 14.8091666666667,
		power: -931.376483809476,
		road: 4936.81305555556,
		acceleration: -0.317129629629628
	},
	{
		id: 444,
		time: 443,
		velocity: 14.3933333333333,
		power: 1276.97835250518,
		road: 4951.40347222222,
		acceleration: -0.155833333333334
	},
	{
		id: 445,
		time: 444,
		velocity: 14.335,
		power: -180.489003872791,
		road: 4965.78736111111,
		acceleration: -0.257222222222223
	},
	{
		id: 446,
		time: 445,
		velocity: 14.0375,
		power: 610.682599799113,
		road: 4979.945,
		acceleration: -0.195277777777777
	},
	{
		id: 447,
		time: 446,
		velocity: 13.8075,
		power: 437.604587254783,
		road: 4993.90277777778,
		acceleration: -0.204444444444446
	},
	{
		id: 448,
		time: 447,
		velocity: 13.7216666666667,
		power: 939.572927005661,
		road: 5007.67685185185,
		acceleration: -0.162962962962963
	},
	{
		id: 449,
		time: 448,
		velocity: 13.5486111111111,
		power: 2427.74654887827,
		road: 5021.34606481481,
		acceleration: -0.0467592592592574
	},
	{
		id: 450,
		time: 449,
		velocity: 13.6672222222222,
		power: 2920.22541131709,
		road: 5034.98782407407,
		acceleration: -0.00814814814814824
	},
	{
		id: 451,
		time: 450,
		velocity: 13.6972222222222,
		power: 3611.25473953466,
		road: 5048.64763888889,
		acceleration: 0.0442592592592597
	},
	{
		id: 452,
		time: 451,
		velocity: 13.6813888888889,
		power: 3380.5629540049,
		road: 5062.34226851852,
		acceleration: 0.0253703703703696
	},
	{
		id: 453,
		time: 452,
		velocity: 13.7433333333333,
		power: 4102.88607256031,
		road: 5076.08888888889,
		acceleration: 0.0786111111111101
	},
	{
		id: 454,
		time: 453,
		velocity: 13.9330555555556,
		power: 4624.19728905683,
		road: 5089.93199074074,
		acceleration: 0.114351851851854
	},
	{
		id: 455,
		time: 454,
		velocity: 14.0244444444444,
		power: 5714.63213720214,
		road: 5103.92712962963,
		acceleration: 0.189722222222219
	},
	{
		id: 456,
		time: 455,
		velocity: 14.3125,
		power: 5836.11591347674,
		road: 5118.11208333333,
		acceleration: 0.189907407407409
	},
	{
		id: 457,
		time: 456,
		velocity: 14.5027777777778,
		power: 6519.69184536689,
		road: 5132.50694444444,
		acceleration: 0.229907407407408
	},
	{
		id: 458,
		time: 457,
		velocity: 14.7141666666667,
		power: 5469.04311031189,
		road: 5147.08939814815,
		acceleration: 0.145277777777778
	},
	{
		id: 459,
		time: 458,
		velocity: 14.7483333333333,
		power: 3801.15272580579,
		road: 5161.75578703704,
		acceleration: 0.0225925925925914
	},
	{
		id: 460,
		time: 459,
		velocity: 14.5705555555556,
		power: 1472.02958408403,
		road: 5176.36236111111,
		acceleration: -0.142222222222223
	},
	{
		id: 461,
		time: 460,
		velocity: 14.2875,
		power: 387.488979113632,
		road: 5190.78949074074,
		acceleration: -0.216666666666665
	},
	{
		id: 462,
		time: 461,
		velocity: 14.0983333333333,
		power: -615.563040416289,
		road: 5204.96518518518,
		acceleration: -0.286203703703704
	},
	{
		id: 463,
		time: 462,
		velocity: 13.7119444444444,
		power: -616.133144692441,
		road: 5218.85643518518,
		acceleration: -0.282685185185187
	},
	{
		id: 464,
		time: 463,
		velocity: 13.4394444444444,
		power: -776.547798908866,
		road: 5232.46055555555,
		acceleration: -0.291574074074072
	},
	{
		id: 465,
		time: 464,
		velocity: 13.2236111111111,
		power: 183.855446474139,
		road: 5245.81212962963,
		acceleration: -0.213518518518519
	},
	{
		id: 466,
		time: 465,
		velocity: 13.0713888888889,
		power: 937.373498402454,
		road: 5258.98162037037,
		acceleration: -0.150648148148148
	},
	{
		id: 467,
		time: 466,
		velocity: 12.9875,
		power: 1845.3439505976,
		road: 5272.03805555555,
		acceleration: -0.0754629629629608
	},
	{
		id: 468,
		time: 467,
		velocity: 12.9972222222222,
		power: 1386.84405008448,
		road: 5285.00171296296,
		acceleration: -0.110092592592594
	},
	{
		id: 469,
		time: 468,
		velocity: 12.7411111111111,
		power: 3053.27264549888,
		road: 5297.92333333333,
		acceleration: 0.0260185185185158
	},
	{
		id: 470,
		time: 469,
		velocity: 13.0655555555556,
		power: 3136.31467465765,
		road: 5310.87384259259,
		acceleration: 0.0317592592592604
	},
	{
		id: 471,
		time: 470,
		velocity: 13.0925,
		power: 5163.51082738363,
		road: 5323.93550925926,
		acceleration: 0.190555555555555
	},
	{
		id: 472,
		time: 471,
		velocity: 13.3127777777778,
		power: 4391.63171450859,
		road: 5337.15351851852,
		acceleration: 0.122129629629631
	},
	{
		id: 473,
		time: 472,
		velocity: 13.4319444444444,
		power: 4599.09690628477,
		road: 5350.49916666666,
		acceleration: 0.133148148148146
	},
	{
		id: 474,
		time: 473,
		velocity: 13.4919444444444,
		power: 2971.18181223894,
		road: 5363.91296296296,
		acceleration: 0.00314814814814746
	},
	{
		id: 475,
		time: 474,
		velocity: 13.3222222222222,
		power: 2145.24241139117,
		road: 5377.29805555555,
		acceleration: -0.0605555555555544
	},
	{
		id: 476,
		time: 475,
		velocity: 13.2502777777778,
		power: -866.669979806818,
		road: 5390.50560185185,
		acceleration: -0.294537037037037
	},
	{
		id: 477,
		time: 476,
		velocity: 12.6083333333333,
		power: -2248.33634343232,
		road: 5403.36393518518,
		acceleration: -0.403888888888888
	},
	{
		id: 478,
		time: 477,
		velocity: 12.1105555555556,
		power: -4243.81391685951,
		road: 5415.73365740741,
		acceleration: -0.573333333333334
	},
	{
		id: 479,
		time: 478,
		velocity: 11.5302777777778,
		power: -3224.25349771392,
		road: 5427.57069444444,
		acceleration: -0.492037037037036
	},
	{
		id: 480,
		time: 479,
		velocity: 11.1322222222222,
		power: -2401.42704818398,
		road: 5438.95083333333,
		acceleration: -0.421759259259261
	},
	{
		id: 481,
		time: 480,
		velocity: 10.8452777777778,
		power: 448.733784204896,
		road: 5450.04291666666,
		acceleration: -0.154351851851851
	},
	{
		id: 482,
		time: 481,
		velocity: 11.0672222222222,
		power: 2196.54673805777,
		road: 5461.06439814815,
		acceleration: 0.0131481481481472
	},
	{
		id: 483,
		time: 482,
		velocity: 11.1716666666667,
		power: 4965.4862166589,
		road: 5472.22689814815,
		acceleration: 0.26888888888889
	},
	{
		id: 484,
		time: 483,
		velocity: 11.6519444444444,
		power: 6015.88566281776,
		road: 5483.69800925926,
		acceleration: 0.348333333333334
	},
	{
		id: 485,
		time: 484,
		velocity: 12.1122222222222,
		power: 8248.86550535404,
		road: 5495.60282407407,
		acceleration: 0.519074074074075
	},
	{
		id: 486,
		time: 485,
		velocity: 12.7288888888889,
		power: 8645.45653157632,
		road: 5508.02467592592,
		acceleration: 0.514999999999997
	},
	{
		id: 487,
		time: 486,
		velocity: 13.1969444444444,
		power: 7463.75016937861,
		road: 5520.89736111111,
		acceleration: 0.386666666666668
	},
	{
		id: 488,
		time: 487,
		velocity: 13.2722222222222,
		power: 4130.06457698027,
		road: 5534.01606481481,
		acceleration: 0.105370370370371
	},
	{
		id: 489,
		time: 488,
		velocity: 13.045,
		power: 1478.33242281839,
		road: 5547.13425925926,
		acceleration: -0.106388888888892
	},
	{
		id: 490,
		time: 489,
		velocity: 12.8777777777778,
		power: 415.752838058839,
		road: 5560.1049074074,
		acceleration: -0.188703703703702
	},
	{
		id: 491,
		time: 490,
		velocity: 12.7061111111111,
		power: 430.907113519796,
		road: 5572.88907407407,
		acceleration: -0.184259259259262
	},
	{
		id: 492,
		time: 491,
		velocity: 12.4922222222222,
		power: 683.954409731115,
		road: 5585.50097222222,
		acceleration: -0.160277777777774
	},
	{
		id: 493,
		time: 492,
		velocity: 12.3969444444444,
		power: 495.161466576378,
		road: 5597.9462037037,
		acceleration: -0.173055555555557
	},
	{
		id: 494,
		time: 493,
		velocity: 12.1869444444444,
		power: 1329.57135641939,
		road: 5610.25509259259,
		acceleration: -0.0996296296296304
	},
	{
		id: 495,
		time: 494,
		velocity: 12.1933333333333,
		power: 1407.44514491282,
		road: 5622.46879629629,
		acceleration: -0.0907407407407419
	},
	{
		id: 496,
		time: 495,
		velocity: 12.1247222222222,
		power: 2598.98662401471,
		road: 5634.64351851851,
		acceleration: 0.0127777777777816
	},
	{
		id: 497,
		time: 496,
		velocity: 12.2252777777778,
		power: 3350.6638928635,
		road: 5646.86254629629,
		acceleration: 0.0758333333333301
	},
	{
		id: 498,
		time: 497,
		velocity: 12.4208333333333,
		power: 4877.44248671706,
		road: 5659.21958333333,
		acceleration: 0.200185185185186
	},
	{
		id: 499,
		time: 498,
		velocity: 12.7252777777778,
		power: 5929.53989596325,
		road: 5671.8149537037,
		acceleration: 0.276481481481483
	},
	{
		id: 500,
		time: 499,
		velocity: 13.0547222222222,
		power: 4657.2277111938,
		road: 5684.62898148148,
		acceleration: 0.160833333333333
	},
	{
		id: 501,
		time: 500,
		velocity: 12.9033333333333,
		power: 6915.91520365219,
		road: 5697.68902777777,
		acceleration: 0.331203703703704
	},
	{
		id: 502,
		time: 501,
		velocity: 13.7188888888889,
		power: 7823.10223405114,
		road: 5711.10560185185,
		acceleration: 0.381851851851852
	},
	{
		id: 503,
		time: 502,
		velocity: 14.2002777777778,
		power: 13250.9462145691,
		road: 5725.09032407407,
		acceleration: 0.754444444444442
	},
	{
		id: 504,
		time: 503,
		velocity: 15.1666666666667,
		power: 13612.7416853032,
		road: 5739.81138888889,
		acceleration: 0.718240740740745
	},
	{
		id: 505,
		time: 504,
		velocity: 15.8736111111111,
		power: 17056.5357404261,
		road: 5755.33467592592,
		acceleration: 0.886203703703702
	},
	{
		id: 506,
		time: 505,
		velocity: 16.8588888888889,
		power: 11544.4188735054,
		road: 5771.53597222222,
		acceleration: 0.469814814814816
	},
	{
		id: 507,
		time: 506,
		velocity: 16.5761111111111,
		power: 7882.60194700575,
		road: 5788.08041666666,
		acceleration: 0.216481481481477
	},
	{
		id: 508,
		time: 507,
		velocity: 16.5230555555556,
		power: 1916.60832716209,
		road: 5804.6524537037,
		acceleration: -0.161296296296292
	},
	{
		id: 509,
		time: 508,
		velocity: 16.375,
		power: 1819.88642523389,
		road: 5821.06222222222,
		acceleration: -0.16324074074074
	},
	{
		id: 510,
		time: 509,
		velocity: 16.0863888888889,
		power: 1824.92438866669,
		road: 5837.31097222222,
		acceleration: -0.158796296296295
	},
	{
		id: 511,
		time: 510,
		velocity: 16.0466666666667,
		power: 3198.87849509291,
		road: 5853.44689814815,
		acceleration: -0.0668518518518511
	},
	{
		id: 512,
		time: 511,
		velocity: 16.1744444444444,
		power: 5754.52629619776,
		road: 5869.59847222222,
		acceleration: 0.0981481481481445
	},
	{
		id: 513,
		time: 512,
		velocity: 16.3808333333333,
		power: 5744.21302693208,
		road: 5885.84587962963,
		acceleration: 0.0935185185185183
	},
	{
		id: 514,
		time: 513,
		velocity: 16.3272222222222,
		power: 5844.26529072804,
		road: 5902.18805555555,
		acceleration: 0.0960185185185196
	},
	{
		id: 515,
		time: 514,
		velocity: 16.4625,
		power: 4959.05072757533,
		road: 5918.59666666666,
		acceleration: 0.0368518518518499
	},
	{
		id: 516,
		time: 515,
		velocity: 16.4913888888889,
		power: 5904.09079502478,
		road: 5935.07087962963,
		acceleration: 0.0943518518518545
	},
	{
		id: 517,
		time: 516,
		velocity: 16.6102777777778,
		power: 5091.29782018138,
		road: 5951.61236111111,
		acceleration: 0.0401851851851838
	},
	{
		id: 518,
		time: 517,
		velocity: 16.5830555555556,
		power: 5040.84878074088,
		road: 5968.19171296296,
		acceleration: 0.0355555555555576
	},
	{
		id: 519,
		time: 518,
		velocity: 16.5980555555556,
		power: 4083.55361536023,
		road: 5984.77634259259,
		acceleration: -0.0250000000000021
	},
	{
		id: 520,
		time: 519,
		velocity: 16.5352777777778,
		power: 3420.45115528863,
		road: 6001.31578703704,
		acceleration: -0.0653703703703705
	},
	{
		id: 521,
		time: 520,
		velocity: 16.3869444444444,
		power: 3577.41493067935,
		road: 6017.79578703703,
		acceleration: -0.0535185185185156
	},
	{
		id: 522,
		time: 521,
		velocity: 16.4375,
		power: 4481.45207820717,
		road: 6034.25138888889,
		acceleration: 0.0047222222222203
	},
	{
		id: 523,
		time: 522,
		velocity: 16.5494444444444,
		power: 6088.49910231705,
		road: 6050.76162037037,
		acceleration: 0.104537037037037
	},
	{
		id: 524,
		time: 523,
		velocity: 16.7005555555556,
		power: 5765.03256829412,
		road: 6067.36425925926,
		acceleration: 0.0802777777777806
	},
	{
		id: 525,
		time: 524,
		velocity: 16.6783333333333,
		power: 3453.14898404485,
		road: 6083.97425925926,
		acceleration: -0.0655555555555587
	},
	{
		id: 526,
		time: 525,
		velocity: 16.3527777777778,
		power: 779.797133551286,
		road: 6100.4361574074,
		acceleration: -0.230648148148148
	},
	{
		id: 527,
		time: 526,
		velocity: 16.0086111111111,
		power: 1192.73090678254,
		road: 6116.68300925926,
		acceleration: -0.199444444444445
	},
	{
		id: 528,
		time: 527,
		velocity: 16.08,
		power: 1772.54287660174,
		road: 6132.75134259259,
		acceleration: -0.157592592592591
	},
	{
		id: 529,
		time: 528,
		velocity: 15.88,
		power: 4309.54577217597,
		road: 6148.74597222222,
		acceleration: 0.0101851851851862
	},
	{
		id: 530,
		time: 529,
		velocity: 16.0391666666667,
		power: 4011.01163850038,
		road: 6164.74101851851,
		acceleration: -0.00935185185185183
	},
	{
		id: 531,
		time: 530,
		velocity: 16.0519444444444,
		power: 6353.53300833127,
		road: 6180.80185185185,
		acceleration: 0.140925925925924
	},
	{
		id: 532,
		time: 531,
		velocity: 16.3027777777778,
		power: 5980.71303369867,
		road: 6196.98879629629,
		acceleration: 0.111296296296295
	},
	{
		id: 533,
		time: 532,
		velocity: 16.3730555555556,
		power: 6892.33286928462,
		road: 6213.31333333333,
		acceleration: 0.163888888888891
	},
	{
		id: 534,
		time: 533,
		velocity: 16.5436111111111,
		power: 5728.60921606102,
		road: 6229.76194444444,
		acceleration: 0.0842592592592624
	},
	{
		id: 535,
		time: 534,
		velocity: 16.5555555555556,
		power: 6563.97394173727,
		road: 6246.31898148148,
		acceleration: 0.132592592592587
	},
	{
		id: 536,
		time: 535,
		velocity: 16.7708333333333,
		power: 5676.54280422607,
		road: 6262.97851851851,
		acceleration: 0.072407407407411
	},
	{
		id: 537,
		time: 536,
		velocity: 16.7608333333333,
		power: 6015.14981512795,
		road: 6279.71939814814,
		acceleration: 0.090277777777775
	},
	{
		id: 538,
		time: 537,
		velocity: 16.8263888888889,
		power: 5109.26981882008,
		road: 6296.52111111111,
		acceleration: 0.0313888888888876
	},
	{
		id: 539,
		time: 538,
		velocity: 16.865,
		power: 5771.85022625281,
		road: 6313.37379629629,
		acceleration: 0.0705555555555577
	},
	{
		id: 540,
		time: 539,
		velocity: 16.9725,
		power: 5564.21273336634,
		road: 6330.28935185185,
		acceleration: 0.0551851851851808
	},
	{
		id: 541,
		time: 540,
		velocity: 16.9919444444444,
		power: 6453.23968118706,
		road: 6347.28583333333,
		acceleration: 0.106666666666669
	},
	{
		id: 542,
		time: 541,
		velocity: 17.185,
		power: 6039.04507910071,
		road: 6364.37435185185,
		acceleration: 0.0774074074074065
	},
	{
		id: 543,
		time: 542,
		velocity: 17.2047222222222,
		power: 6080.30286706782,
		road: 6381.5399537037,
		acceleration: 0.0767592592592621
	},
	{
		id: 544,
		time: 543,
		velocity: 17.2222222222222,
		power: 4908.02362191864,
		road: 6398.74587962962,
		acceleration: 0.00388888888888772
	},
	{
		id: 545,
		time: 544,
		velocity: 17.1966666666667,
		power: 5340.78339093312,
		road: 6415.96851851851,
		acceleration: 0.0295370370370378
	},
	{
		id: 546,
		time: 545,
		velocity: 17.2933333333333,
		power: 5640.00693770124,
		road: 6433.22902777777,
		acceleration: 0.0462037037037035
	},
	{
		id: 547,
		time: 546,
		velocity: 17.3608333333333,
		power: 6047.16720077138,
		road: 6450.54689814814,
		acceleration: 0.0685185185185198
	},
	{
		id: 548,
		time: 547,
		velocity: 17.4022222222222,
		power: 4565.09336752837,
		road: 6467.88814814814,
		acceleration: -0.0217592592592588
	},
	{
		id: 549,
		time: 548,
		velocity: 17.2280555555556,
		power: 4071.71890383357,
		road: 6485.19337962962,
		acceleration: -0.0502777777777794
	},
	{
		id: 550,
		time: 549,
		velocity: 17.21,
		power: 4094.9337455678,
		road: 6502.44986111111,
		acceleration: -0.0472222222222207
	},
	{
		id: 551,
		time: 550,
		velocity: 17.2605555555556,
		power: 5524.84511957097,
		road: 6519.70249999999,
		acceleration: 0.0395370370370358
	},
	{
		id: 552,
		time: 551,
		velocity: 17.3466666666667,
		power: 6225.83319662787,
		road: 6537.01467592592,
		acceleration: 0.0795370370370385
	},
	{
		id: 553,
		time: 552,
		velocity: 17.4486111111111,
		power: 7038.04356528276,
		road: 6554.4286574074,
		acceleration: 0.124074074074073
	},
	{
		id: 554,
		time: 553,
		velocity: 17.6327777777778,
		power: 7732.18875164229,
		road: 6571.98425925925,
		acceleration: 0.159166666666664
	},
	{
		id: 555,
		time: 554,
		velocity: 17.8241666666667,
		power: 7503.37501894635,
		road: 6589.68884259259,
		acceleration: 0.138796296296299
	},
	{
		id: 556,
		time: 555,
		velocity: 17.865,
		power: 6072.9339556808,
		road: 6607.48810185185,
		acceleration: 0.0505555555555546
	},
	{
		id: 557,
		time: 556,
		velocity: 17.7844444444444,
		power: 4984.98307909211,
		road: 6625.30564814814,
		acceleration: -0.0139814814814834
	},
	{
		id: 558,
		time: 557,
		velocity: 17.7822222222222,
		power: 3477.86864609558,
		road: 6643.06592592592,
		acceleration: -0.100555555555555
	},
	{
		id: 559,
		time: 558,
		velocity: 17.5633333333333,
		power: 3602.18777963943,
		road: 6660.73083333333,
		acceleration: -0.0901851851851845
	},
	{
		id: 560,
		time: 559,
		velocity: 17.5138888888889,
		power: 2076.52585867801,
		road: 6678.26222222222,
		acceleration: -0.17685185185185
	},
	{
		id: 561,
		time: 560,
		velocity: 17.2516666666667,
		power: 2356.26025204059,
		road: 6695.6274074074,
		acceleration: -0.155555555555559
	},
	{
		id: 562,
		time: 561,
		velocity: 17.0966666666667,
		power: 2958.1812745993,
		road: 6712.85717592592,
		acceleration: -0.115277777777777
	},
	{
		id: 563,
		time: 562,
		velocity: 17.1680555555556,
		power: 4924.36388482614,
		road: 6730.03231481481,
		acceleration: 0.00601851851852331
	},
	{
		id: 564,
		time: 563,
		velocity: 17.2697222222222,
		power: 6191.02308674892,
		road: 6747.25111111111,
		acceleration: 0.0812962962962942
	},
	{
		id: 565,
		time: 564,
		velocity: 17.3405555555556,
		power: 6608.27891778661,
		road: 6764.56189814814,
		acceleration: 0.102685185185187
	},
	{
		id: 566,
		time: 565,
		velocity: 17.4761111111111,
		power: 6558.39575522724,
		road: 6781.97175925925,
		acceleration: 0.0954629629629622
	},
	{
		id: 567,
		time: 566,
		velocity: 17.5561111111111,
		power: 6816.25589488356,
		road: 6799.48263888888,
		acceleration: 0.106574074074071
	},
	{
		id: 568,
		time: 567,
		velocity: 17.6602777777778,
		power: 5900.14281605787,
		road: 6817.0712037037,
		acceleration: 0.0487962962962989
	},
	{
		id: 569,
		time: 568,
		velocity: 17.6225,
		power: 5052.78791723083,
		road: 6834.68296296296,
		acceleration: -0.00240740740741074
	},
	{
		id: 570,
		time: 569,
		velocity: 17.5488888888889,
		power: 4454.370450665,
		road: 6852.2749074074,
		acceleration: -0.0372222222222192
	},
	{
		id: 571,
		time: 570,
		velocity: 17.5486111111111,
		power: 4437.23898883733,
		road: 6869.82976851851,
		acceleration: -0.036944444444444
	},
	{
		id: 572,
		time: 571,
		velocity: 17.5116666666667,
		power: 4166.19145172889,
		road: 6887.34037037036,
		acceleration: -0.0515740740740753
	},
	{
		id: 573,
		time: 572,
		velocity: 17.3941666666667,
		power: 3513.01220231851,
		road: 6904.78101851851,
		acceleration: -0.0883333333333312
	},
	{
		id: 574,
		time: 573,
		velocity: 17.2836111111111,
		power: 2786.29779130442,
		road: 6922.11314814814,
		acceleration: -0.128703703703707
	},
	{
		id: 575,
		time: 574,
		velocity: 17.1255555555556,
		power: 3131.15260582449,
		road: 6939.32874999999,
		acceleration: -0.104351851851852
	},
	{
		id: 576,
		time: 575,
		velocity: 17.0811111111111,
		power: 5356.15839549484,
		road: 6956.50824074074,
		acceleration: 0.0321296296296296
	},
	{
		id: 577,
		time: 576,
		velocity: 17.38,
		power: 6798.88776245425,
		road: 6973.76212962962,
		acceleration: 0.116666666666667
	},
	{
		id: 578,
		time: 577,
		velocity: 17.4755555555556,
		power: 8754.02283477134,
		road: 6991.18759259259,
		acceleration: 0.226481481481482
	},
	{
		id: 579,
		time: 578,
		velocity: 17.7605555555556,
		power: 8009.47581227558,
		road: 7008.81249999999,
		acceleration: 0.172407407407409
	},
	{
		id: 580,
		time: 579,
		velocity: 17.8972222222222,
		power: 9561.29428828243,
		road: 7026.65027777777,
		acceleration: 0.25333333333333
	},
	{
		id: 581,
		time: 580,
		velocity: 18.2355555555556,
		power: 8557.2856956283,
		road: 7044.70671296296,
		acceleration: 0.183981481481482
	},
	{
		id: 582,
		time: 581,
		velocity: 18.3125,
		power: 8942.82917512634,
		road: 7062.95361111111,
		acceleration: 0.196944444444448
	},
	{
		id: 583,
		time: 582,
		velocity: 18.4880555555556,
		power: 5813.57845832471,
		road: 7081.30583333333,
		acceleration: 0.0137037037037011
	},
	{
		id: 584,
		time: 583,
		velocity: 18.2766666666667,
		power: 5235.95343321855,
		road: 7099.65537037036,
		acceleration: -0.0190740740740729
	},
	{
		id: 585,
		time: 584,
		velocity: 18.2552777777778,
		power: 5137.50939253544,
		road: 7117.98342592592,
		acceleration: -0.0238888888888873
	},
	{
		id: 586,
		time: 585,
		velocity: 18.4163888888889,
		power: 8406.81927974527,
		road: 7136.37916666666,
		acceleration: 0.159259259259258
	},
	{
		id: 587,
		time: 586,
		velocity: 18.7544444444444,
		power: 11109.8333960428,
		road: 7155.00458333333,
		acceleration: 0.300092592592591
	},
	{
		id: 588,
		time: 587,
		velocity: 19.1555555555556,
		power: 12706.3573601778,
		road: 7173.9649537037,
		acceleration: 0.369814814814816
	},
	{
		id: 589,
		time: 588,
		velocity: 19.5258333333333,
		power: 16629.6860900336,
		road: 7193.38717592592,
		acceleration: 0.553888888888888
	},
	{
		id: 590,
		time: 589,
		velocity: 20.4161111111111,
		power: 19393.168652234,
		road: 7213.4149074074,
		acceleration: 0.65712962962963
	},
	{
		id: 591,
		time: 590,
		velocity: 21.1269444444444,
		power: 19762.1270042496,
		road: 7234.08583333333,
		acceleration: 0.629259259259257
	},
	{
		id: 592,
		time: 591,
		velocity: 21.4136111111111,
		power: 18591.4911360103,
		road: 7255.33671296296,
		acceleration: 0.530648148148153
	},
	{
		id: 593,
		time: 592,
		velocity: 22.0080555555556,
		power: 16010.864990989,
		road: 7277.0411574074,
		acceleration: 0.376481481481477
	},
	{
		id: 594,
		time: 593,
		velocity: 22.2563888888889,
		power: 12659.5194376729,
		road: 7299.03402777777,
		acceleration: 0.200370370370372
	},
	{
		id: 595,
		time: 594,
		velocity: 22.0147222222222,
		power: 8802.14941337464,
		road: 7321.13351851851,
		acceleration: 0.0128703703703721
	},
	{
		id: 596,
		time: 595,
		velocity: 22.0466666666667,
		power: 5972.33892603695,
		road: 7343.18013888888,
		acceleration: -0.118611111111115
	},
	{
		id: 597,
		time: 596,
		velocity: 21.9005555555556,
		power: 3325.85538426317,
		road: 7365.04842592592,
		acceleration: -0.238055555555555
	},
	{
		id: 598,
		time: 597,
		velocity: 21.3005555555556,
		power: 463.200197159058,
		road: 7386.61425925925,
		acceleration: -0.366851851851848
	},
	{
		id: 599,
		time: 598,
		velocity: 20.9461111111111,
		power: -302.388317392589,
		road: 7407.79916666666,
		acceleration: -0.395000000000003
	},
	{
		id: 600,
		time: 599,
		velocity: 20.7155555555556,
		power: 2284.59541543882,
		road: 7428.65749999999,
		acceleration: -0.258148148148145
	},
	{
		id: 601,
		time: 600,
		velocity: 20.5261111111111,
		power: 1581.49104503794,
		road: 7449.24379629629,
		acceleration: -0.28592592592593
	},
	{
		id: 602,
		time: 601,
		velocity: 20.0883333333333,
		power: 521.905751106054,
		road: 7469.52106481481,
		acceleration: -0.332129629629627
	},
	{
		id: 603,
		time: 602,
		velocity: 19.7191666666667,
		power: -1394.56120208162,
		road: 7489.42046296296,
		acceleration: -0.423611111111114
	},
	{
		id: 604,
		time: 603,
		velocity: 19.2552777777778,
		power: -616.380693555149,
		road: 7508.92074074073,
		acceleration: -0.374629629629627
	},
	{
		id: 605,
		time: 604,
		velocity: 18.9644444444444,
		power: -1263.20717617881,
		road: 7528.03259259259,
		acceleration: -0.402222222222225
	},
	{
		id: 606,
		time: 605,
		velocity: 18.5125,
		power: -986.15691005961,
		road: 7546.75337962962,
		acceleration: -0.379907407407408
	},
	{
		id: 607,
		time: 606,
		velocity: 18.1155555555556,
		power: -1188.50300835397,
		road: 7565.09189814814,
		acceleration: -0.384629629629629
	},
	{
		id: 608,
		time: 607,
		velocity: 17.8105555555556,
		power: -1541.83979054023,
		road: 7583.03874999999,
		acceleration: -0.398703703703703
	},
	{
		id: 609,
		time: 608,
		velocity: 17.3163888888889,
		power: -1062.61230109025,
		road: 7600.60398148147,
		acceleration: -0.364537037037039
	},
	{
		id: 610,
		time: 609,
		velocity: 17.0219444444444,
		power: -2431.98007371875,
		road: 7617.7661574074,
		acceleration: -0.441574074074072
	},
	{
		id: 611,
		time: 610,
		velocity: 16.4858333333333,
		power: -698.868273012317,
		road: 7634.54273148147,
		acceleration: -0.329629629629629
	},
	{
		id: 612,
		time: 611,
		velocity: 16.3275,
		power: -535.29940379822,
		road: 7650.9974537037,
		acceleration: -0.314074074074075
	},
	{
		id: 613,
		time: 612,
		velocity: 16.0797222222222,
		power: -345.930759713197,
		road: 7667.14671296296,
		acceleration: -0.296851851851851
	},
	{
		id: 614,
		time: 613,
		velocity: 15.5952777777778,
		power: -1713.99715697754,
		road: 7682.95666666666,
		acceleration: -0.38175925925926
	},
	{
		id: 615,
		time: 614,
		velocity: 15.1822222222222,
		power: -1727.37308835037,
		road: 7698.38634259258,
		acceleration: -0.378796296296295
	},
	{
		id: 616,
		time: 615,
		velocity: 14.9433333333333,
		power: -568.497042053274,
		road: 7713.47898148147,
		acceleration: -0.295277777777777
	},
	{
		id: 617,
		time: 616,
		velocity: 14.7094444444444,
		power: -707.945735904462,
		road: 7728.27351851851,
		acceleration: -0.300925925925926
	},
	{
		id: 618,
		time: 617,
		velocity: 14.2794444444444,
		power: 539.638873617194,
		road: 7742.8137037037,
		acceleration: -0.207777777777778
	},
	{
		id: 619,
		time: 618,
		velocity: 14.32,
		power: -257.544511719852,
		road: 7757.11916666666,
		acceleration: -0.261666666666667
	},
	{
		id: 620,
		time: 619,
		velocity: 13.9244444444444,
		power: 1929.10383393393,
		road: 7771.24532407407,
		acceleration: -0.0969444444444463
	},
	{
		id: 621,
		time: 620,
		velocity: 13.9886111111111,
		power: 2870.27161723448,
		road: 7785.31037037036,
		acceleration: -0.0252777777777773
	},
	{
		id: 622,
		time: 621,
		velocity: 14.2441666666667,
		power: 3248.42334601841,
		road: 7799.36439814814,
		acceleration: 0.00324074074074332
	},
	{
		id: 623,
		time: 622,
		velocity: 13.9341666666667,
		power: 1729.47729751063,
		road: 7813.36574074073,
		acceleration: -0.108611111111113
	},
	{
		id: 624,
		time: 623,
		velocity: 13.6627777777778,
		power: -94.997068819073,
		road: 7827.19157407407,
		acceleration: -0.242407407407407
	},
	{
		id: 625,
		time: 624,
		velocity: 13.5169444444444,
		power: 1435.00001022783,
		road: 7840.83509259259,
		acceleration: -0.122222222222222
	},
	{
		id: 626,
		time: 625,
		velocity: 13.5675,
		power: 2041.12895963085,
		road: 7854.38097222222,
		acceleration: -0.0730555555555554
	},
	{
		id: 627,
		time: 626,
		velocity: 13.4436111111111,
		power: 3436.02124170944,
		road: 7867.90805555555,
		acceleration: 0.0354629629629617
	},
	{
		id: 628,
		time: 627,
		velocity: 13.6233333333333,
		power: 3921.20805119973,
		road: 7881.48837962962,
		acceleration: 0.0710185185185193
	},
	{
		id: 629,
		time: 628,
		velocity: 13.7805555555556,
		power: 5504.69706143185,
		road: 7895.19777777777,
		acceleration: 0.187129629629627
	},
	{
		id: 630,
		time: 629,
		velocity: 14.005,
		power: 4809.88224161467,
		road: 7909.06444444444,
		acceleration: 0.127407407407409
	},
	{
		id: 631,
		time: 630,
		velocity: 14.0055555555556,
		power: 5216.28105564124,
		road: 7923.07078703703,
		acceleration: 0.151944444444444
	},
	{
		id: 632,
		time: 631,
		velocity: 14.2363888888889,
		power: 4227.69972669333,
		road: 7937.1899537037,
		acceleration: 0.0737037037037034
	},
	{
		id: 633,
		time: 632,
		velocity: 14.2261111111111,
		power: 4660.28207946483,
		road: 7951.39708333333,
		acceleration: 0.102222222222224
	},
	{
		id: 634,
		time: 633,
		velocity: 14.3122222222222,
		power: 3804.52534647176,
		road: 7965.6736574074,
		acceleration: 0.0366666666666671
	},
	{
		id: 635,
		time: 634,
		velocity: 14.3463888888889,
		power: 4620.36808713855,
		road: 7980.01550925925,
		acceleration: 0.0938888888888894
	},
	{
		id: 636,
		time: 635,
		velocity: 14.5077777777778,
		power: 4720.04906730678,
		road: 7994.45296296296,
		acceleration: 0.0973148148148155
	},
	{
		id: 637,
		time: 636,
		velocity: 14.6041666666667,
		power: 3994.02482422949,
		road: 8008.96013888888,
		acceleration: 0.0421296296296276
	},
	{
		id: 638,
		time: 637,
		velocity: 14.4727777777778,
		power: 2546.48129623231,
		road: 8023.45736111111,
		acceleration: -0.0620370370370367
	},
	{
		id: 639,
		time: 638,
		velocity: 14.3216666666667,
		power: 541.965832548652,
		road: 8037.82143518518,
		acceleration: -0.20425925925926
	},
	{
		id: 640,
		time: 639,
		velocity: 13.9913888888889,
		power: -1092.0557023864,
		road: 8051.92305555555,
		acceleration: -0.320648148148148
	},
	{
		id: 641,
		time: 640,
		velocity: 13.5108333333333,
		power: -2412.62471946321,
		road: 8065.65546296296,
		acceleration: -0.417777777777776
	},
	{
		id: 642,
		time: 641,
		velocity: 13.0683333333333,
		power: -1991.8085692167,
		road: 8078.98689814814,
		acceleration: -0.384166666666669
	},
	{
		id: 643,
		time: 642,
		velocity: 12.8388888888889,
		power: -908.043248064367,
		road: 8091.97833333333,
		acceleration: -0.295833333333331
	},
	{
		id: 644,
		time: 643,
		velocity: 12.6233333333333,
		power: -455.210322494954,
		road: 8104.69379629629,
		acceleration: -0.256111111111114
	},
	{
		id: 645,
		time: 644,
		velocity: 12.3,
		power: -1100.95682525411,
		road: 8117.1275,
		acceleration: -0.307407407407405
	},
	{
		id: 646,
		time: 645,
		velocity: 11.9166666666667,
		power: -817.458229405953,
		road: 8129.26694444444,
		acceleration: -0.281111111111111
	},
	{
		id: 647,
		time: 646,
		velocity: 11.78,
		power: -471.018045463306,
		road: 8141.14157407407,
		acceleration: -0.248518518518521
	},
	{
		id: 648,
		time: 647,
		velocity: 11.5544444444444,
		power: 1827.09077865636,
		road: 8152.87111111111,
		acceleration: -0.0416666666666643
	},
	{
		id: 649,
		time: 648,
		velocity: 11.7916666666667,
		power: 3068.40952384681,
		road: 8164.61421296296,
		acceleration: 0.0687962962962967
	},
	{
		id: 650,
		time: 649,
		velocity: 11.9863888888889,
		power: 3617.28705479494,
		road: 8176.44874999999,
		acceleration: 0.114074074074075
	},
	{
		id: 651,
		time: 650,
		velocity: 11.8966666666667,
		power: 2798.2554637785,
		road: 8188.35976851851,
		acceleration: 0.0388888888888861
	},
	{
		id: 652,
		time: 651,
		velocity: 11.9083333333333,
		power: 2304.44214452406,
		road: 8200.28768518518,
		acceleration: -0.00509259259259132
	},
	{
		id: 653,
		time: 652,
		velocity: 11.9711111111111,
		power: 1055.42802488481,
		road: 8212.15624999999,
		acceleration: -0.113611111111112
	},
	{
		id: 654,
		time: 653,
		velocity: 11.5558333333333,
		power: 1784.67468378703,
		road: 8223.94449074074,
		acceleration: -0.0470370370370361
	},
	{
		id: 655,
		time: 654,
		velocity: 11.7672222222222,
		power: -834.401764654649,
		road: 8235.56976851851,
		acceleration: -0.27888888888889
	},
	{
		id: 656,
		time: 655,
		velocity: 11.1344444444444,
		power: -185.487066537987,
		road: 8246.94685185185,
		acceleration: -0.217499999999998
	},
	{
		id: 657,
		time: 656,
		velocity: 10.9033333333333,
		power: -2121.98742102038,
		road: 8258.01638888888,
		acceleration: -0.397592592592595
	},
	{
		id: 658,
		time: 657,
		velocity: 10.5744444444444,
		power: -1812.13471655152,
		road: 8268.70226851851,
		acceleration: -0.369722222222222
	},
	{
		id: 659,
		time: 658,
		velocity: 10.0252777777778,
		power: -1484.98423074425,
		road: 8279.03407407407,
		acceleration: -0.338425925925925
	},
	{
		id: 660,
		time: 659,
		velocity: 9.88805555555555,
		power: -1574.14386917603,
		road: 8289.02212962962,
		acceleration: -0.349074074074075
	},
	{
		id: 661,
		time: 660,
		velocity: 9.52722222222222,
		power: -583.962516737159,
		road: 8298.71384259259,
		acceleration: -0.243611111111113
	},
	{
		id: 662,
		time: 661,
		velocity: 9.29444444444444,
		power: -2183.56812195167,
		road: 8308.07287037037,
		acceleration: -0.421759259259259
	},
	{
		id: 663,
		time: 662,
		velocity: 8.62277777777778,
		power: -2026.42146313353,
		road: 8317.01578703703,
		acceleration: -0.410462962962962
	},
	{
		id: 664,
		time: 663,
		velocity: 8.29583333333333,
		power: -1948.72182937169,
		road: 8325.54930555555,
		acceleration: -0.408333333333333
	},
	{
		id: 665,
		time: 664,
		velocity: 8.06944444444444,
		power: -347.503929740712,
		road: 8333.7737037037,
		acceleration: -0.209907407407407
	},
	{
		id: 666,
		time: 665,
		velocity: 7.99305555555556,
		power: 549.587107449755,
		road: 8341.84685185185,
		acceleration: -0.0925925925925934
	},
	{
		id: 667,
		time: 666,
		velocity: 8.01805555555556,
		power: 2434.47352632385,
		road: 8349.94935185185,
		acceleration: 0.151296296296296
	},
	{
		id: 668,
		time: 667,
		velocity: 8.52333333333333,
		power: 3523.94328788714,
		road: 8358.26685185185,
		acceleration: 0.278703703703703
	},
	{
		id: 669,
		time: 668,
		velocity: 8.82916666666667,
		power: 3823.94968469372,
		road: 8366.8725,
		acceleration: 0.297592592592592
	},
	{
		id: 670,
		time: 669,
		velocity: 8.91083333333333,
		power: 3156.11424201599,
		road: 8375.72828703703,
		acceleration: 0.202685185185185
	},
	{
		id: 671,
		time: 670,
		velocity: 9.13138888888889,
		power: 3040.14691024919,
		road: 8384.77509259259,
		acceleration: 0.179351851851852
	},
	{
		id: 672,
		time: 671,
		velocity: 9.36722222222222,
		power: 4381.79227603554,
		road: 8394.07097222222,
		acceleration: 0.318796296296295
	},
	{
		id: 673,
		time: 672,
		velocity: 9.86722222222222,
		power: 5223.38885626874,
		road: 8403.72041666666,
		acceleration: 0.388333333333335
	},
	{
		id: 674,
		time: 673,
		velocity: 10.2963888888889,
		power: 5919.5219623065,
		road: 8413.78055555555,
		acceleration: 0.433055555555555
	},
	{
		id: 675,
		time: 674,
		velocity: 10.6663888888889,
		power: 3412.03415779475,
		road: 8424.1361574074,
		acceleration: 0.157870370370372
	},
	{
		id: 676,
		time: 675,
		velocity: 10.3408333333333,
		power: 1084.18932688051,
		road: 8434.53125,
		acceleration: -0.0788888888888888
	},
	{
		id: 677,
		time: 676,
		velocity: 10.0597222222222,
		power: 500.556323153324,
		road: 8444.81888888889,
		acceleration: -0.136018518518519
	},
	{
		id: 678,
		time: 677,
		velocity: 10.2583333333333,
		power: 4279.80138924567,
		road: 8455.16175925926,
		acceleration: 0.246481481481483
	},
	{
		id: 679,
		time: 678,
		velocity: 11.0802777777778,
		power: 5544.08714473508,
		road: 8465.80546296296,
		acceleration: 0.355185185185183
	},
	{
		id: 680,
		time: 679,
		velocity: 11.1252777777778,
		power: 5059.82529405899,
		road: 8476.7711574074,
		acceleration: 0.288796296296297
	},
	{
		id: 681,
		time: 680,
		velocity: 11.1247222222222,
		power: 5460.83604257961,
		road: 8488.03597222222,
		acceleration: 0.309444444444443
	},
	{
		id: 682,
		time: 681,
		velocity: 12.0086111111111,
		power: 8740.43882936307,
		road: 8499.74449074074,
		acceleration: 0.577962962962964
	},
	{
		id: 683,
		time: 682,
		velocity: 12.8591666666667,
		power: 13066.4192810252,
		road: 8512.185,
		acceleration: 0.886018518518519
	},
	{
		id: 684,
		time: 683,
		velocity: 13.7827777777778,
		power: 13887.3545713975,
		road: 8525.50101851851,
		acceleration: 0.864999999999998
	},
	{
		id: 685,
		time: 684,
		velocity: 14.6036111111111,
		power: 13783.7468912683,
		road: 8539.63981481481,
		acceleration: 0.780555555555557
	},
	{
		id: 686,
		time: 685,
		velocity: 15.2008333333333,
		power: 11123.5831083663,
		road: 8554.43685185185,
		acceleration: 0.535925925925923
	},
	{
		id: 687,
		time: 686,
		velocity: 15.3905555555556,
		power: 9283.99965690947,
		road: 8569.69097222222,
		acceleration: 0.378240740740742
	},
	{
		id: 688,
		time: 687,
		velocity: 15.7383333333333,
		power: 7306.46630918845,
		road: 8585.24796296296,
		acceleration: 0.227500000000001
	},
	{
		id: 689,
		time: 688,
		velocity: 15.8833333333333,
		power: 5420.43253662874,
		road: 8600.96574074074,
		acceleration: 0.0940740740740722
	},
	{
		id: 690,
		time: 689,
		velocity: 15.6727777777778,
		power: 3223.78542234726,
		road: 8616.70416666666,
		acceleration: -0.0527777777777771
	},
	{
		id: 691,
		time: 690,
		velocity: 15.58,
		power: 3290.54810382815,
		road: 8632.39282407407,
		acceleration: -0.0467592592592592
	},
	{
		id: 692,
		time: 691,
		velocity: 15.7430555555556,
		power: 4875.58675445373,
		road: 8648.0874537037,
		acceleration: 0.0587037037037046
	},
	{
		id: 693,
		time: 692,
		velocity: 15.8488888888889,
		power: 6695.69839537664,
		road: 8663.89874999999,
		acceleration: 0.174629629629628
	},
	{
		id: 694,
		time: 693,
		velocity: 16.1038888888889,
		power: 7492.26380627114,
		road: 8679.9062037037,
		acceleration: 0.217685185185186
	},
	{
		id: 695,
		time: 694,
		velocity: 16.3961111111111,
		power: 8552.75092148794,
		road: 8696.15949074074,
		acceleration: 0.273981481481485
	},
	{
		id: 696,
		time: 695,
		velocity: 16.6708333333333,
		power: 8148.06721140372,
		road: 8712.66731481481,
		acceleration: 0.235092592592594
	},
	{
		id: 697,
		time: 696,
		velocity: 16.8091666666667,
		power: 8235.35048519601,
		road: 8729.40717592592,
		acceleration: 0.22898148148148
	},
	{
		id: 698,
		time: 697,
		velocity: 17.0830555555556,
		power: 6093.72083049423,
		road: 8746.30583333333,
		acceleration: 0.0886111111111134
	},
	{
		id: 699,
		time: 698,
		velocity: 16.9366666666667,
		power: 7259.97890165728,
		road: 8763.32638888889,
		acceleration: 0.155185185185186
	},
	{
		id: 700,
		time: 699,
		velocity: 17.2747222222222,
		power: 6348.996757959,
		road: 8780.47152777777,
		acceleration: 0.0939814814814781
	},
	{
		id: 701,
		time: 700,
		velocity: 17.365,
		power: 7869.57257193705,
		road: 8797.75375,
		acceleration: 0.180185185185184
	},
	{
		id: 702,
		time: 701,
		velocity: 17.4772222222222,
		power: 5602.59664428637,
		road: 8815.14546296296,
		acceleration: 0.0387962962962938
	},
	{
		id: 703,
		time: 702,
		velocity: 17.3911111111111,
		power: 4615.65449756122,
		road: 8832.5461574074,
		acceleration: -0.0208333333333286
	},
	{
		id: 704,
		time: 703,
		velocity: 17.3025,
		power: 1619.22038172459,
		road: 8849.8374074074,
		acceleration: -0.198055555555559
	},
	{
		id: 705,
		time: 704,
		velocity: 16.8830555555556,
		power: 1864.51643040083,
		road: 8866.94046296296,
		acceleration: -0.178333333333331
	},
	{
		id: 706,
		time: 705,
		velocity: 16.8561111111111,
		power: 1276.95436142217,
		road: 8883.84953703703,
		acceleration: -0.209629629629632
	},
	{
		id: 707,
		time: 706,
		velocity: 16.6736111111111,
		power: 4660.85618032112,
		road: 8900.65546296296,
		acceleration: 0.00333333333333385
	},
	{
		id: 708,
		time: 707,
		velocity: 16.8930555555556,
		power: 4550.66104202551,
		road: 8917.46129629629,
		acceleration: -0.00351851851851492
	},
	{
		id: 709,
		time: 708,
		velocity: 16.8455555555556,
		power: 8494.10822985113,
		road: 8934.38333333333,
		acceleration: 0.235925925925923
	},
	{
		id: 710,
		time: 709,
		velocity: 17.3813888888889,
		power: 8628.73299947589,
		road: 8951.53953703703,
		acceleration: 0.232407407407408
	},
	{
		id: 711,
		time: 710,
		velocity: 17.5902777777778,
		power: 8509.36272723225,
		road: 8968.91898148148,
		acceleration: 0.214074074074073
	},
	{
		id: 712,
		time: 711,
		velocity: 17.4877777777778,
		power: 4969.63314016135,
		road: 8986.40412037037,
		acceleration: -0.00268518518518235
	},
	{
		id: 713,
		time: 712,
		velocity: 17.3733333333333,
		power: 1055.47257808872,
		road: 9003.77097222222,
		acceleration: -0.233888888888892
	},
	{
		id: 714,
		time: 713,
		velocity: 16.8886111111111,
		power: 64.1755865124672,
		road: 9020.87666666666,
		acceleration: -0.288425925925925
	},
	{
		id: 715,
		time: 714,
		velocity: 16.6225,
		power: -277.911721770337,
		road: 9037.6861574074,
		acceleration: -0.303981481481479
	},
	{
		id: 716,
		time: 715,
		velocity: 16.4613888888889,
		power: 1318.55100264084,
		road: 9054.24430555555,
		acceleration: -0.198703703703703
	},
	{
		id: 717,
		time: 716,
		velocity: 16.2925,
		power: 1227.40586567941,
		road: 9070.60319444444,
		acceleration: -0.199814814814818
	},
	{
		id: 718,
		time: 717,
		velocity: 16.0230555555556,
		power: -2016.95001843717,
		road: 9086.66009259259,
		acceleration: -0.404166666666665
	},
	{
		id: 719,
		time: 718,
		velocity: 15.2488888888889,
		power: -2669.75809377738,
		road: 9102.29296296296,
		acceleration: -0.443888888888891
	},
	{
		id: 720,
		time: 719,
		velocity: 14.9608333333333,
		power: -4243.25610240233,
		road: 9117.42888888889,
		acceleration: -0.549999999999999
	},
	{
		id: 721,
		time: 720,
		velocity: 14.3730555555556,
		power: -2133.05337571193,
		road: 9132.08935185185,
		acceleration: -0.400925925925925
	},
	{
		id: 722,
		time: 721,
		velocity: 14.0461111111111,
		power: -1758.33274728056,
		road: 9146.3637037037,
		acceleration: -0.371296296296295
	},
	{
		id: 723,
		time: 722,
		velocity: 13.8469444444444,
		power: 321.051327846979,
		road: 9160.34560185185,
		acceleration: -0.213611111111113
	},
	{
		id: 724,
		time: 723,
		velocity: 13.7322222222222,
		power: 972.02418441684,
		road: 9174.14023148148,
		acceleration: -0.160925925925927
	},
	{
		id: 725,
		time: 724,
		velocity: 13.5633333333333,
		power: 1254.84297920618,
		road: 9187.78634259259,
		acceleration: -0.136111111111109
	},
	{
		id: 726,
		time: 725,
		velocity: 13.4386111111111,
		power: 1097.05796807966,
		road: 9201.29180555555,
		acceleration: -0.145185185185188
	},
	{
		id: 727,
		time: 726,
		velocity: 13.2966666666667,
		power: -441.176924648649,
		road: 9214.5937037037,
		acceleration: -0.261944444444442
	},
	{
		id: 728,
		time: 727,
		velocity: 12.7775,
		power: -1145.93261721559,
		road: 9227.60703703703,
		acceleration: -0.315185185185186
	},
	{
		id: 729,
		time: 728,
		velocity: 12.4930555555556,
		power: -1163.22603480449,
		road: 9240.30560185185,
		acceleration: -0.314351851851852
	},
	{
		id: 730,
		time: 729,
		velocity: 12.3536111111111,
		power: 1157.88875942911,
		road: 9252.78796296296,
		acceleration: -0.118055555555557
	},
	{
		id: 731,
		time: 730,
		velocity: 12.4233333333333,
		power: 2472.76625802404,
		road: 9265.20842592592,
		acceleration: -0.00574074074073927
	},
	{
		id: 732,
		time: 731,
		velocity: 12.4758333333333,
		power: 3297.09904128448,
		road: 9277.6574074074,
		acceleration: 0.0627777777777787
	},
	{
		id: 733,
		time: 732,
		velocity: 12.5419444444444,
		power: 3793.36872415804,
		road: 9290.18842592592,
		acceleration: 0.101296296296294
	},
	{
		id: 734,
		time: 733,
		velocity: 12.7272222222222,
		power: 3739.17164552983,
		road: 9302.81657407407,
		acceleration: 0.0929629629629645
	},
	{
		id: 735,
		time: 734,
		velocity: 12.7547222222222,
		power: 4564.2326212887,
		road: 9315.56916666666,
		acceleration: 0.155925925925924
	},
	{
		id: 736,
		time: 735,
		velocity: 13.0097222222222,
		power: 5061.48398579538,
		road: 9328.49407407407,
		acceleration: 0.188703703703705
	},
	{
		id: 737,
		time: 736,
		velocity: 13.2933333333333,
		power: 5983.78496492894,
		road: 9341.63935185185,
		acceleration: 0.252037037037038
	},
	{
		id: 738,
		time: 737,
		velocity: 13.5108333333333,
		power: 6088.12128958834,
		road: 9355.03439814814,
		acceleration: 0.247499999999999
	},
	{
		id: 739,
		time: 738,
		velocity: 13.7522222222222,
		power: 5844.16940878898,
		road: 9368.66171296296,
		acceleration: 0.217037037037036
	},
	{
		id: 740,
		time: 739,
		velocity: 13.9444444444444,
		power: 5139.22673738451,
		road: 9382.47486111111,
		acceleration: 0.15462962962963
	},
	{
		id: 741,
		time: 740,
		velocity: 13.9747222222222,
		power: 1387.56881763672,
		road: 9396.30027777777,
		acceleration: -0.130092592592593
	},
	{
		id: 742,
		time: 741,
		velocity: 13.3619444444444,
		power: -394.69080720221,
		road: 9409.92939814814,
		acceleration: -0.262499999999999
	},
	{
		id: 743,
		time: 742,
		velocity: 13.1569444444444,
		power: -3124.33411542717,
		road: 9423.19055555555,
		acceleration: -0.473425925925923
	},
	{
		id: 744,
		time: 743,
		velocity: 12.5544444444444,
		power: -861.121136687858,
		road: 9436.06949074074,
		acceleration: -0.29101851851852
	},
	{
		id: 745,
		time: 744,
		velocity: 12.4888888888889,
		power: -1840.09998840591,
		road: 9448.61796296296,
		acceleration: -0.369907407407409
	},
	{
		id: 746,
		time: 745,
		velocity: 12.0472222222222,
		power: -990.643263979564,
		road: 9460.83319444444,
		acceleration: -0.296574074074075
	},
	{
		id: 747,
		time: 746,
		velocity: 11.6647222222222,
		power: -2369.29281943877,
		road: 9472.69203703703,
		acceleration: -0.416203703703705
	},
	{
		id: 748,
		time: 747,
		velocity: 11.2402777777778,
		power: -1898.31046635887,
		road: 9484.15518518518,
		acceleration: -0.375185185185183
	},
	{
		id: 749,
		time: 748,
		velocity: 10.9216666666667,
		power: -1801.23857836408,
		road: 9495.24717592592,
		acceleration: -0.36712962962963
	},
	{
		id: 750,
		time: 749,
		velocity: 10.5633333333333,
		power: -1338.87599877407,
		road: 9505.99398148148,
		acceleration: -0.323240740740742
	},
	{
		id: 751,
		time: 750,
		velocity: 10.2705555555556,
		power: -1755.92685421212,
		road: 9516.39638888888,
		acceleration: -0.365555555555554
	},
	{
		id: 752,
		time: 751,
		velocity: 9.825,
		power: -2838.52351904662,
		road: 9526.3749537037,
		acceleration: -0.482129629629631
	},
	{
		id: 753,
		time: 752,
		velocity: 9.11694444444444,
		power: -3380.74750820271,
		road: 9535.83592592592,
		acceleration: -0.553055555555556
	},
	{
		id: 754,
		time: 753,
		velocity: 8.61138888888889,
		power: -3220.72799389719,
		road: 9544.74444444444,
		acceleration: -0.55185185185185
	},
	{
		id: 755,
		time: 754,
		velocity: 8.16944444444444,
		power: -2253.38812290491,
		road: 9553.15263888889,
		acceleration: -0.448796296296297
	},
	{
		id: 756,
		time: 755,
		velocity: 7.77055555555556,
		power: -1789.20613185177,
		road: 9561.13703703703,
		acceleration: -0.398796296296296
	},
	{
		id: 757,
		time: 756,
		velocity: 7.415,
		power: -1814.5084909995,
		road: 9568.71634259259,
		acceleration: -0.411388888888889
	},
	{
		id: 758,
		time: 757,
		velocity: 6.93527777777778,
		power: -812.998960897635,
		road: 9575.9524537037,
		acceleration: -0.274999999999999
	},
	{
		id: 759,
		time: 758,
		velocity: 6.94555555555556,
		power: -400.723126504669,
		road: 9582.94347222222,
		acceleration: -0.215185185185185
	},
	{
		id: 760,
		time: 759,
		velocity: 6.76944444444444,
		power: 1137.48662621425,
		road: 9589.83657407407,
		acceleration: 0.0193518518518525
	},
	{
		id: 761,
		time: 760,
		velocity: 6.99333333333333,
		power: 2088.02303443976,
		road: 9596.81912037037,
		acceleration: 0.159537037037036
	},
	{
		id: 762,
		time: 761,
		velocity: 7.42416666666667,
		power: 4346.87835737816,
		road: 9604.11583333333,
		acceleration: 0.468796296296297
	},
	{
		id: 763,
		time: 762,
		velocity: 8.17583333333333,
		power: 5794.99174377045,
		road: 9611.95435185185,
		acceleration: 0.614814814814813
	},
	{
		id: 764,
		time: 763,
		velocity: 8.83777777777778,
		power: 7195.74251723886,
		road: 9620.46046296296,
		acceleration: 0.720370370370372
	},
	{
		id: 765,
		time: 764,
		velocity: 9.58527777777778,
		power: 6339.56012809508,
		road: 9629.60351851852,
		acceleration: 0.553518518518519
	},
	{
		id: 766,
		time: 765,
		velocity: 9.83638888888889,
		power: 5201.43280812235,
		road: 9639.21754629629,
		acceleration: 0.388425925925926
	},
	{
		id: 767,
		time: 766,
		velocity: 10.0030555555556,
		power: 4304.6970073104,
		road: 9649.16138888889,
		acceleration: 0.271203703703705
	},
	{
		id: 768,
		time: 767,
		velocity: 10.3988888888889,
		power: 5204.44959495036,
		road: 9659.41384259259,
		acceleration: 0.346018518518516
	},
	{
		id: 769,
		time: 768,
		velocity: 10.8744444444444,
		power: 5885.4180686097,
		road: 9670.03449074074,
		acceleration: 0.39037037037037
	},
	{
		id: 770,
		time: 769,
		velocity: 11.1741666666667,
		power: 4430.38355333799,
		road: 9680.96550925926,
		acceleration: 0.230370370370371
	},
	{
		id: 771,
		time: 770,
		velocity: 11.09,
		power: 2893.7525378895,
		road: 9692.05027777777,
		acceleration: 0.0771296296296295
	},
	{
		id: 772,
		time: 771,
		velocity: 11.1058333333333,
		power: 3512.97622199231,
		road: 9703.23925925926,
		acceleration: 0.131296296296295
	},
	{
		id: 773,
		time: 772,
		velocity: 11.5680555555556,
		power: 6188.48756177064,
		road: 9714.67699074074,
		acceleration: 0.366203703703704
	},
	{
		id: 774,
		time: 773,
		velocity: 12.1886111111111,
		power: 7458.30482506364,
		road: 9726.52453703703,
		acceleration: 0.453425925925927
	},
	{
		id: 775,
		time: 774,
		velocity: 12.4661111111111,
		power: 8488.46807103674,
		road: 9738.85305555555,
		acceleration: 0.508518518518517
	},
	{
		id: 776,
		time: 775,
		velocity: 13.0936111111111,
		power: 6371.4158909951,
		road: 9751.58851851851,
		acceleration: 0.305370370370371
	},
	{
		id: 777,
		time: 776,
		velocity: 13.1047222222222,
		power: 6227.93770321332,
		road: 9764.61555555555,
		acceleration: 0.277777777777779
	},
	{
		id: 778,
		time: 777,
		velocity: 13.2994444444444,
		power: 4820.48497158501,
		road: 9777.85898148148,
		acceleration: 0.154999999999999
	},
	{
		id: 779,
		time: 778,
		velocity: 13.5586111111111,
		power: 6369.2939377198,
		road: 9791.31310185185,
		acceleration: 0.266388888888887
	},
	{
		id: 780,
		time: 779,
		velocity: 13.9038888888889,
		power: 6928.64799786235,
		road: 9805.04768518518,
		acceleration: 0.294537037037038
	},
	{
		id: 781,
		time: 780,
		velocity: 14.1830555555556,
		power: 7021.70975070428,
		road: 9819.07249999999,
		acceleration: 0.285925925925927
	},
	{
		id: 782,
		time: 781,
		velocity: 14.4163888888889,
		power: 6613.41613818847,
		road: 9833.36129629629,
		acceleration: 0.242037037037036
	},
	{
		id: 783,
		time: 782,
		velocity: 14.63,
		power: 5691.48558582188,
		road: 9847.85374999999,
		acceleration: 0.165277777777778
	},
	{
		id: 784,
		time: 783,
		velocity: 14.6788888888889,
		power: 5352.60240700083,
		road: 9862.49601851851,
		acceleration: 0.134351851851852
	},
	{
		id: 785,
		time: 784,
		velocity: 14.8194444444444,
		power: 4600.66349043349,
		road: 9877.2437037037,
		acceleration: 0.0764814814814816
	},
	{
		id: 786,
		time: 785,
		velocity: 14.8594444444444,
		power: 4223.36235696409,
		road: 9892.05333333333,
		acceleration: 0.0474074074074089
	},
	{
		id: 787,
		time: 786,
		velocity: 14.8211111111111,
		power: 3665.36289082781,
		road: 9906.89018518518,
		acceleration: 0.00703703703703695
	},
	{
		id: 788,
		time: 787,
		velocity: 14.8405555555556,
		power: 3169.82246996184,
		road: 9921.71675925925,
		acceleration: -0.0275925925925939
	},
	{
		id: 789,
		time: 788,
		velocity: 14.7766666666667,
		power: 3246.49306039198,
		road: 9936.51884259259,
		acceleration: -0.0213888888888878
	},
	{
		id: 790,
		time: 789,
		velocity: 14.7569444444444,
		power: 3343.04630915365,
		road: 9951.30324074073,
		acceleration: -0.0139814814814816
	},
	{
		id: 791,
		time: 790,
		velocity: 14.7986111111111,
		power: 3648.8921237411,
		road: 9966.08453703703,
		acceleration: 0.00777777777777722
	},
	{
		id: 792,
		time: 791,
		velocity: 14.8,
		power: 4177.61278062148,
		road: 9980.89185185184,
		acceleration: 0.0442592592592579
	},
	{
		id: 793,
		time: 792,
		velocity: 14.8897222222222,
		power: 4826.4942675735,
		road: 9995.76504629629,
		acceleration: 0.0875000000000021
	},
	{
		id: 794,
		time: 793,
		velocity: 15.0611111111111,
		power: 5626.69532254319,
		road: 10010.7514351852,
		acceleration: 0.138888888888889
	},
	{
		id: 795,
		time: 794,
		velocity: 15.2166666666667,
		power: 6782.44634734074,
		road: 10025.9128240741,
		acceleration: 0.211111111111109
	},
	{
		id: 796,
		time: 795,
		velocity: 15.5230555555556,
		power: 7403.85078668027,
		road: 10041.3010185185,
		acceleration: 0.242500000000003
	},
	{
		id: 797,
		time: 796,
		velocity: 15.7886111111111,
		power: 8191.24857822707,
		road: 10056.9514814815,
		acceleration: 0.282037037037037
	},
	{
		id: 798,
		time: 797,
		velocity: 16.0627777777778,
		power: 8952.85373429133,
		road: 10072.9011111111,
		acceleration: 0.316296296296294
	},
	{
		id: 799,
		time: 798,
		velocity: 16.4719444444444,
		power: 7960.46351609061,
		road: 10089.1275,
		acceleration: 0.237222222222222
	},
	{
		id: 800,
		time: 799,
		velocity: 16.5002777777778,
		power: 8622.34100420507,
		road: 10105.6058333333,
		acceleration: 0.266666666666669
	},
	{
		id: 801,
		time: 800,
		velocity: 16.8627777777778,
		power: 8998.31773698134,
		road: 10122.3555555555,
		acceleration: 0.276111111111113
	},
	{
		id: 802,
		time: 801,
		velocity: 17.3002777777778,
		power: 8207.55390152963,
		road: 10139.3506481481,
		acceleration: 0.214629629629627
	},
	{
		id: 803,
		time: 802,
		velocity: 17.1441666666667,
		power: 6258.88511341863,
		road: 10156.4972685185,
		acceleration: 0.0884259259259252
	},
	{
		id: 804,
		time: 803,
		velocity: 17.1280555555556,
		power: 4579.16422414764,
		road: 10173.680462963,
		acceleration: -0.0152777777777757
	},
	{
		id: 805,
		time: 804,
		velocity: 17.2544444444444,
		power: 6235.95949840386,
		road: 10190.8980555555,
		acceleration: 0.0840740740740742
	},
	{
		id: 806,
		time: 805,
		velocity: 17.3963888888889,
		power: 6962.63107747019,
		road: 10208.2194907407,
		acceleration: 0.12361111111111
	},
	{
		id: 807,
		time: 806,
		velocity: 17.4988888888889,
		power: 7155.86012437835,
		road: 10225.6675462963,
		acceleration: 0.129629629629626
	},
	{
		id: 808,
		time: 807,
		velocity: 17.6433333333333,
		power: 7230.22105007285,
		road: 10243.2445833333,
		acceleration: 0.128333333333337
	},
	{
		id: 809,
		time: 808,
		velocity: 17.7813888888889,
		power: 6717.82184346727,
		road: 10260.9323611111,
		acceleration: 0.0931481481481455
	},
	{
		id: 810,
		time: 809,
		velocity: 17.7783333333333,
		power: 6114.84616156285,
		road: 10278.6939814815,
		acceleration: 0.0545370370370364
	},
	{
		id: 811,
		time: 810,
		velocity: 17.8069444444444,
		power: 4765.47275771953,
		road: 10296.4701851852,
		acceleration: -0.0253703703703678
	},
	{
		id: 812,
		time: 811,
		velocity: 17.7052777777778,
		power: 3977.63056194301,
		road: 10314.1986574074,
		acceleration: -0.0700925925925979
	},
	{
		id: 813,
		time: 812,
		velocity: 17.5680555555556,
		power: 2915.58586400741,
		road: 10331.8272222222,
		acceleration: -0.129722222222217
	},
	{
		id: 814,
		time: 813,
		velocity: 17.4177777777778,
		power: 2945.41025105886,
		road: 10349.3288425926,
		acceleration: -0.124166666666667
	},
	{
		id: 815,
		time: 814,
		velocity: 17.3327777777778,
		power: 1863.88458014412,
		road: 10366.6760185185,
		acceleration: -0.184722222222224
	},
	{
		id: 816,
		time: 815,
		velocity: 17.0138888888889,
		power: 726.08059083084,
		road: 10383.8065740741,
		acceleration: -0.248518518518516
	},
	{
		id: 817,
		time: 816,
		velocity: 16.6722222222222,
		power: -251.419184929429,
		road: 10400.6612962963,
		acceleration: -0.30314814814815
	},
	{
		id: 818,
		time: 817,
		velocity: 16.4233333333333,
		power: -424.268100805179,
		road: 10417.2101388889,
		acceleration: -0.308611111111112
	},
	{
		id: 819,
		time: 818,
		velocity: 16.0880555555556,
		power: 143.996172441876,
		road: 10433.4710648148,
		acceleration: -0.267222222222223
	},
	{
		id: 820,
		time: 819,
		velocity: 15.8705555555556,
		power: 714.024915382381,
		road: 10449.4856944444,
		acceleration: -0.225370370370369
	},
	{
		id: 821,
		time: 820,
		velocity: 15.7472222222222,
		power: 1891.447861641,
		road: 10465.315787037,
		acceleration: -0.143703703703704
	},
	{
		id: 822,
		time: 821,
		velocity: 15.6569444444444,
		power: 2774.17498913975,
		road: 10481.0330092593,
		acceleration: -0.0820370370370362
	},
	{
		id: 823,
		time: 822,
		velocity: 15.6244444444444,
		power: 2705.62813218085,
		road: 10496.6671296296,
		acceleration: -0.0841666666666683
	},
	{
		id: 824,
		time: 823,
		velocity: 15.4947222222222,
		power: 2659.19242693189,
		road: 10512.2167592593,
		acceleration: -0.0848148148148162
	},
	{
		id: 825,
		time: 824,
		velocity: 15.4025,
		power: 3232.97857646709,
		road: 10527.7018981481,
		acceleration: -0.0441666666666638
	},
	{
		id: 826,
		time: 825,
		velocity: 15.4919444444444,
		power: 3894.86433821169,
		road: 10543.1656018518,
		acceleration: 0.00129629629629591
	},
	{
		id: 827,
		time: 826,
		velocity: 15.4986111111111,
		power: 4427.99411398026,
		road: 10558.648287037,
		acceleration: 0.0366666666666671
	},
	{
		id: 828,
		time: 827,
		velocity: 15.5125,
		power: 4861.09798692215,
		road: 10574.1812962963,
		acceleration: 0.0639814814814805
	},
	{
		id: 829,
		time: 828,
		velocity: 15.6838888888889,
		power: 5159.90529034774,
		road: 10589.7868981481,
		acceleration: 0.0812037037037054
	},
	{
		id: 830,
		time: 829,
		velocity: 15.7422222222222,
		power: 5825.2977518454,
		road: 10605.4938425926,
		acceleration: 0.12148148148148
	},
	{
		id: 831,
		time: 830,
		velocity: 15.8769444444444,
		power: 5638.80493170644,
		road: 10621.3137037037,
		acceleration: 0.104351851851852
	},
	{
		id: 832,
		time: 831,
		velocity: 15.9969444444444,
		power: 5736.39091004235,
		road: 10637.2389351852,
		acceleration: 0.106388888888887
	},
	{
		id: 833,
		time: 832,
		velocity: 16.0613888888889,
		power: 6252.53395335993,
		road: 10653.2848611111,
		acceleration: 0.135000000000003
	},
	{
		id: 834,
		time: 833,
		velocity: 16.2819444444444,
		power: 5348.51151720776,
		road: 10669.4342592593,
		acceleration: 0.0719444444444406
	},
	{
		id: 835,
		time: 834,
		velocity: 16.2127777777778,
		power: 4255.80609206641,
		road: 10685.6195833333,
		acceleration: -9.25925925869819E-05
	},
	{
		id: 836,
		time: 835,
		velocity: 16.0611111111111,
		power: 1199.00438914381,
		road: 10701.7071759259,
		acceleration: -0.195370370370375
	},
	{
		id: 837,
		time: 836,
		velocity: 15.6958333333333,
		power: -41.3131685787484,
		road: 10717.5611574074,
		acceleration: -0.271851851851849
	},
	{
		id: 838,
		time: 837,
		velocity: 15.3972222222222,
		power: -78.8909855998451,
		road: 10733.1443981481,
		acceleration: -0.26962962962963
	},
	{
		id: 839,
		time: 838,
		velocity: 15.2522222222222,
		power: 831.450128822594,
		road: 10748.4910648148,
		acceleration: -0.203518518518521
	},
	{
		id: 840,
		time: 839,
		velocity: 15.0852777777778,
		power: 1738.36364923548,
		road: 10763.6672685185,
		acceleration: -0.137407407407405
	},
	{
		id: 841,
		time: 840,
		velocity: 14.985,
		power: 2332.36951024672,
		road: 10778.7281481481,
		acceleration: -0.0932407407407396
	},
	{
		id: 842,
		time: 841,
		velocity: 14.9725,
		power: 3293.69938959599,
		road: 10793.7301388889,
		acceleration: -0.0245370370370388
	},
	{
		id: 843,
		time: 842,
		velocity: 15.0116666666667,
		power: 4086.33671735448,
		road: 10808.7351851852,
		acceleration: 0.0306481481481509
	},
	{
		id: 844,
		time: 843,
		velocity: 15.0769444444444,
		power: 4481.73990168405,
		road: 10823.7838425926,
		acceleration: 0.0565740740740726
	},
	{
		id: 845,
		time: 844,
		velocity: 15.1422222222222,
		power: 4835.59399884285,
		road: 10838.9000462963,
		acceleration: 0.0785185185185178
	},
	{
		id: 846,
		time: 845,
		velocity: 15.2472222222222,
		power: 5301.6907897141,
		road: 10854.1089814815,
		acceleration: 0.106944444444444
	},
	{
		id: 847,
		time: 846,
		velocity: 15.3977777777778,
		power: 5222.38175180851,
		road: 10869.4200462963,
		acceleration: 0.0973148148148155
	},
	{
		id: 848,
		time: 847,
		velocity: 15.4341666666667,
		power: 5030.33998377363,
		road: 10884.8200925926,
		acceleration: 0.0806481481481462
	},
	{
		id: 849,
		time: 848,
		velocity: 15.4891666666667,
		power: 4537.54409620081,
		road: 10900.2828703704,
		acceleration: 0.0448148148148135
	},
	{
		id: 850,
		time: 849,
		velocity: 15.5322222222222,
		power: 4105.65196197408,
		road: 10915.7753240741,
		acceleration: 0.0145370370370408
	},
	{
		id: 851,
		time: 850,
		velocity: 15.4777777777778,
		power: 4258.60506491896,
		road: 10931.2871296296,
		acceleration: 0.0241666666666625
	},
	{
		id: 852,
		time: 851,
		velocity: 15.5616666666667,
		power: 3318.85355609158,
		road: 10946.7915277778,
		acceleration: -0.0389814814814802
	},
	{
		id: 853,
		time: 852,
		velocity: 15.4152777777778,
		power: 3502.09862817759,
		road: 10962.2636574074,
		acceleration: -0.0255555555555542
	},
	{
		id: 854,
		time: 853,
		velocity: 15.4011111111111,
		power: 2642.34716346584,
		road: 10977.6819444444,
		acceleration: -0.0821296296296303
	},
	{
		id: 855,
		time: 854,
		velocity: 15.3152777777778,
		power: 3509.47803456379,
		road: 10993.0483796296,
		acceleration: -0.0215740740740724
	},
	{
		id: 856,
		time: 855,
		velocity: 15.3505555555556,
		power: 3732.41802130388,
		road: 11008.4010648148,
		acceleration: -0.00592592592592744
	},
	{
		id: 857,
		time: 856,
		velocity: 15.3833333333333,
		power: 4221.67724910863,
		road: 11023.7643055556,
		acceleration: 0.0270370370370383
	},
	{
		id: 858,
		time: 857,
		velocity: 15.3963888888889,
		power: 3799.69805942799,
		road: 11039.14,
		acceleration: -0.00212962962963026
	},
	{
		id: 859,
		time: 858,
		velocity: 15.3441666666667,
		power: 3147.80874662385,
		road: 11054.4917592593,
		acceleration: -0.0457407407407402
	},
	{
		id: 860,
		time: 859,
		velocity: 15.2461111111111,
		power: 2577.22463788204,
		road: 11069.7792592593,
		acceleration: -0.0827777777777783
	},
	{
		id: 861,
		time: 860,
		velocity: 15.1480555555556,
		power: 2434.70949629476,
		road: 11084.9803240741,
		acceleration: -0.0900925925925939
	},
	{
		id: 862,
		time: 861,
		velocity: 15.0738888888889,
		power: 1722.46697475524,
		road: 11100.0681944444,
		acceleration: -0.136296296296294
	},
	{
		id: 863,
		time: 862,
		velocity: 14.8372222222222,
		power: 1923.04276869832,
		road: 11115.0283796296,
		acceleration: -0.119074074074074
	},
	{
		id: 864,
		time: 863,
		velocity: 14.7908333333333,
		power: 1905.85772538082,
		road: 11129.8704166667,
		acceleration: -0.117222222222223
	},
	{
		id: 865,
		time: 864,
		velocity: 14.7222222222222,
		power: 1558.69433458456,
		road: 11144.584537037,
		acceleration: -0.138611111111111
	},
	{
		id: 866,
		time: 865,
		velocity: 14.4213888888889,
		power: 1350.33148189445,
		road: 11159.1543055556,
		acceleration: -0.150092592592593
	},
	{
		id: 867,
		time: 866,
		velocity: 14.3405555555556,
		power: 872.191336710965,
		road: 11173.5585185185,
		acceleration: -0.181018518518519
	},
	{
		id: 868,
		time: 867,
		velocity: 14.1791666666667,
		power: 1144.45600332057,
		road: 11187.7934722222,
		acceleration: -0.157500000000001
	},
	{
		id: 869,
		time: 868,
		velocity: 13.9488888888889,
		power: 1087.83865007385,
		road: 11201.8705555556,
		acceleration: -0.158240740740739
	},
	{
		id: 870,
		time: 869,
		velocity: 13.8658333333333,
		power: 1276.30450887816,
		road: 11215.7981018519,
		acceleration: -0.140833333333333
	},
	{
		id: 871,
		time: 870,
		velocity: 13.7566666666667,
		power: 1919.67189522229,
		road: 11229.6105092593,
		acceleration: -0.089444444444446
	},
	{
		id: 872,
		time: 871,
		velocity: 13.6805555555556,
		power: 1675.2311440556,
		road: 11243.3254166667,
		acceleration: -0.105555555555554
	},
	{
		id: 873,
		time: 872,
		velocity: 13.5491666666667,
		power: 1255.44715137872,
		road: 11256.9200925926,
		acceleration: -0.134907407407409
	},
	{
		id: 874,
		time: 873,
		velocity: 13.3519444444444,
		power: 200.525434479732,
		road: 11270.3406481482,
		acceleration: -0.213333333333333
	},
	{
		id: 875,
		time: 874,
		velocity: 13.0405555555556,
		power: 750.940281873283,
		road: 11283.5712037037,
		acceleration: -0.166666666666668
	},
	{
		id: 876,
		time: 875,
		velocity: 13.0491666666667,
		power: 2020.05301326816,
		road: 11296.6868981482,
		acceleration: -0.0630555555555539
	},
	{
		id: 877,
		time: 876,
		velocity: 13.1627777777778,
		power: 3502.03430354972,
		road: 11309.7987962963,
		acceleration: 0.055462962962963
	},
	{
		id: 878,
		time: 877,
		velocity: 13.2069444444444,
		power: 3251.43378122499,
		road: 11322.9553703704,
		acceleration: 0.0338888888888889
	},
	{
		id: 879,
		time: 878,
		velocity: 13.1508333333333,
		power: 2930.89764698923,
		road: 11336.1327314815,
		acceleration: 0.00768518518518491
	},
	{
		id: 880,
		time: 879,
		velocity: 13.1858333333333,
		power: 3340.78779515726,
		road: 11349.3336574074,
		acceleration: 0.0394444444444453
	},
	{
		id: 881,
		time: 880,
		velocity: 13.3252777777778,
		power: 3768.41814526683,
		road: 11362.5899537037,
		acceleration: 0.0712962962962962
	},
	{
		id: 882,
		time: 881,
		velocity: 13.3647222222222,
		power: 4178.50836025362,
		road: 11375.932037037,
		acceleration: 0.100277777777777
	},
	{
		id: 883,
		time: 882,
		velocity: 13.4866666666667,
		power: 5175.02217611029,
		road: 11389.4104166667,
		acceleration: 0.172314814814815
	},
	{
		id: 884,
		time: 883,
		velocity: 13.8422222222222,
		power: 6287.16138139872,
		road: 11403.0989351852,
		acceleration: 0.247962962962964
	},
	{
		id: 885,
		time: 884,
		velocity: 14.1086111111111,
		power: 7012.68708639891,
		road: 11417.0558796296,
		acceleration: 0.288888888888886
	},
	{
		id: 886,
		time: 885,
		velocity: 14.3533333333333,
		power: 6157.03692464014,
		road: 11431.2635185185,
		acceleration: 0.212500000000002
	},
	{
		id: 887,
		time: 886,
		velocity: 14.4797222222222,
		power: 6069.9503590716,
		road: 11445.6756018519,
		acceleration: 0.19638888888889
	},
	{
		id: 888,
		time: 887,
		velocity: 14.6977777777778,
		power: 6534.40108618634,
		road: 11460.2958333333,
		acceleration: 0.219907407407407
	},
	{
		id: 889,
		time: 888,
		velocity: 15.0130555555556,
		power: 8314.71462361463,
		road: 11475.1918518519,
		acceleration: 0.331666666666665
	},
	{
		id: 890,
		time: 889,
		velocity: 15.4747222222222,
		power: 8891.11841774197,
		road: 11490.4298148148,
		acceleration: 0.352222222222224
	},
	{
		id: 891,
		time: 890,
		velocity: 15.7544444444444,
		power: 6405.16729762929,
		road: 11505.9286574074,
		acceleration: 0.169537037037037
	},
	{
		id: 892,
		time: 891,
		velocity: 15.5216666666667,
		power: 1571.09109876379,
		road: 11521.4337962963,
		acceleration: -0.156944444444443
	},
	{
		id: 893,
		time: 892,
		velocity: 15.0038888888889,
		power: -579.40414339595,
		road: 11536.7111111111,
		acceleration: -0.298703703703705
	},
	{
		id: 894,
		time: 893,
		velocity: 14.8583333333333,
		power: -1903.2716724309,
		road: 11551.645787037,
		acceleration: -0.386574074074073
	},
	{
		id: 895,
		time: 894,
		velocity: 14.3619444444444,
		power: -697.051603836491,
		road: 11566.2384259259,
		acceleration: -0.297499999999999
	},
	{
		id: 896,
		time: 895,
		velocity: 14.1113888888889,
		power: -153.972627756029,
		road: 11580.5551851852,
		acceleration: -0.254259259259261
	},
	{
		id: 897,
		time: 896,
		velocity: 14.0955555555556,
		power: 898.422671971711,
		road: 11594.6583796296,
		acceleration: -0.17287037037037
	},
	{
		id: 898,
		time: 897,
		velocity: 13.8433333333333,
		power: 841.034644270685,
		road: 11608.5883333333,
		acceleration: -0.173611111111112
	},
	{
		id: 899,
		time: 898,
		velocity: 13.5905555555556,
		power: -772.883983920278,
		road: 11622.2853240741,
		acceleration: -0.292314814814812
	},
	{
		id: 900,
		time: 899,
		velocity: 13.2186111111111,
		power: -1068.07999384062,
		road: 11635.6800925926,
		acceleration: -0.312129629629631
	},
	{
		id: 901,
		time: 900,
		velocity: 12.9069444444444,
		power: -1041.98762022343,
		road: 11648.7650925926,
		acceleration: -0.307407407407409
	},
	{
		id: 902,
		time: 901,
		velocity: 12.6683333333333,
		power: 386.452791759584,
		road: 11661.6019907407,
		acceleration: -0.188796296296294
	},
	{
		id: 903,
		time: 902,
		velocity: 12.6522222222222,
		power: 989.301272350719,
		road: 11674.2763888889,
		acceleration: -0.136203703703703
	},
	{
		id: 904,
		time: 903,
		velocity: 12.4983333333333,
		power: 1840.9828445586,
		road: 11686.8511111111,
		acceleration: -0.0631481481481497
	},
	{
		id: 905,
		time: 904,
		velocity: 12.4788888888889,
		power: 1756.06239980818,
		road: 11699.36,
		acceleration: -0.068518518518518
	},
	{
		id: 906,
		time: 905,
		velocity: 12.4466666666667,
		power: 2761.51862975445,
		road: 11711.8429166667,
		acceleration: 0.0165740740740734
	},
	{
		id: 907,
		time: 906,
		velocity: 12.5480555555556,
		power: 3617.80216238095,
		road: 11724.3773611111,
		acceleration: 0.0864814814814796
	},
	{
		id: 908,
		time: 907,
		velocity: 12.7383333333333,
		power: 4737.81292739606,
		road: 11737.0421296296,
		acceleration: 0.174166666666668
	},
	{
		id: 909,
		time: 908,
		velocity: 12.9691666666667,
		power: 5062.8873247794,
		road: 11749.8901851852,
		acceleration: 0.192407407407405
	},
	{
		id: 910,
		time: 909,
		velocity: 13.1252777777778,
		power: 5884.61756892129,
		road: 11762.9584722222,
		acceleration: 0.248055555555558
	},
	{
		id: 911,
		time: 910,
		velocity: 13.4825,
		power: 5747.37944229719,
		road: 11776.2634259259,
		acceleration: 0.225277777777777
	},
	{
		id: 912,
		time: 911,
		velocity: 13.645,
		power: 4689.99697024417,
		road: 11789.7481944444,
		acceleration: 0.134351851851854
	},
	{
		id: 913,
		time: 912,
		velocity: 13.5283333333333,
		power: 3157.76471092467,
		road: 11803.3065740741,
		acceleration: 0.0128703703703703
	},
	{
		id: 914,
		time: 913,
		velocity: 13.5211111111111,
		power: 2468.20887569601,
		road: 11816.8513888889,
		acceleration: -0.0400000000000027
	},
	{
		id: 915,
		time: 914,
		velocity: 13.525,
		power: 2888.41336535118,
		road: 11830.3728240741,
		acceleration: -0.00675925925925824
	},
	{
		id: 916,
		time: 915,
		velocity: 13.5080555555556,
		power: 2483.12882348494,
		road: 11843.8721296296,
		acceleration: -0.0374999999999996
	},
	{
		id: 917,
		time: 916,
		velocity: 13.4086111111111,
		power: 1377.05400617997,
		road: 11857.2919444444,
		acceleration: -0.121481481481482
	},
	{
		id: 918,
		time: 917,
		velocity: 13.1605555555556,
		power: -1094.77559052472,
		road: 11870.4947222222,
		acceleration: -0.312592592592592
	},
	{
		id: 919,
		time: 918,
		velocity: 12.5702777777778,
		power: -1684.4016954439,
		road: 11883.3622222222,
		acceleration: -0.357962962962961
	},
	{
		id: 920,
		time: 919,
		velocity: 12.3347222222222,
		power: -1613.13406164678,
		road: 11895.8753240741,
		acceleration: -0.350833333333334
	},
	{
		id: 921,
		time: 920,
		velocity: 12.1080555555556,
		power: -801.337797254238,
		road: 11908.0729166667,
		acceleration: -0.280185185185186
	},
	{
		id: 922,
		time: 921,
		velocity: 11.7297222222222,
		power: -44.3236548105276,
		road: 11920.0244907407,
		acceleration: -0.211851851851851
	},
	{
		id: 923,
		time: 922,
		velocity: 11.6991666666667,
		power: -1452.26804258059,
		road: 11931.7027777778,
		acceleration: -0.334722222222224
	},
	{
		id: 924,
		time: 923,
		velocity: 11.1038888888889,
		power: -641.160706428832,
		road: 11943.0839351852,
		acceleration: -0.259537037037036
	},
	{
		id: 925,
		time: 924,
		velocity: 10.9511111111111,
		power: -2195.8212868497,
		road: 11954.132962963,
		acceleration: -0.404722222222222
	},
	{
		id: 926,
		time: 925,
		velocity: 10.485,
		power: -5758.37804795108,
		road: 11964.5962962963,
		acceleration: -0.766666666666666
	},
	{
		id: 927,
		time: 926,
		velocity: 8.80388888888889,
		power: -9628.87489105135,
		road: 11974.0527777778,
		acceleration: -1.24703703703704
	},
	{
		id: 928,
		time: 927,
		velocity: 7.21,
		power: -11051.342770061,
		road: 11982.080462963,
		acceleration: -1.61055555555556
	},
	{
		id: 929,
		time: 928,
		velocity: 5.65333333333333,
		power: -8067.76138568101,
		road: 11988.5738425926,
		acceleration: -1.45805555555556
	},
	{
		id: 930,
		time: 929,
		velocity: 4.42972222222222,
		power: -4472.02661584303,
		road: 11993.817962963,
		acceleration: -1.04046296296296
	},
	{
		id: 931,
		time: 930,
		velocity: 4.08861111111111,
		power: -1707.82641721367,
		road: 11998.2706481482,
		acceleration: -0.542407407407408
	},
	{
		id: 932,
		time: 931,
		velocity: 4.02611111111111,
		power: -218.661935915871,
		road: 12002.3555555556,
		acceleration: -0.193148148148148
	},
	{
		id: 933,
		time: 932,
		velocity: 3.85027777777778,
		power: -1015.96629370228,
		road: 12006.1345833333,
		acceleration: -0.418611111111111
	},
	{
		id: 934,
		time: 933,
		velocity: 2.83277777777778,
		power: -1829.92444278783,
		road: 12009.3366203704,
		acceleration: -0.73537037037037
	},
	{
		id: 935,
		time: 934,
		velocity: 1.82,
		power: -1776.45436197944,
		road: 12011.7113425926,
		acceleration: -0.919259259259259
	},
	{
		id: 936,
		time: 935,
		velocity: 1.0925,
		power: -699.4544537719,
		road: 12013.3347685185,
		acceleration: -0.583333333333333
	},
	{
		id: 937,
		time: 936,
		velocity: 1.08277777777778,
		power: -219.132014831439,
		road: 12014.5035185185,
		acceleration: -0.326018518518518
	},
	{
		id: 938,
		time: 937,
		velocity: 0.841944444444445,
		power: -0.736284810803703,
		road: 12015.4448611111,
		acceleration: -0.128796296296296
	},
	{
		id: 939,
		time: 938,
		velocity: 0.706111111111111,
		power: -12.7305548366695,
		road: 12016.249537037,
		acceleration: -0.144537037037037
	},
	{
		id: 940,
		time: 939,
		velocity: 0.649166666666667,
		power: -85.8215944395469,
		road: 12016.8416203704,
		acceleration: -0.280648148148148
	},
	{
		id: 941,
		time: 940,
		velocity: 0,
		power: 20.5703362980331,
		road: 12017.255787037,
		acceleration: -0.0751851851851852
	},
	{
		id: 942,
		time: 941,
		velocity: 0.480555555555556,
		power: 94.9585923625628,
		road: 12017.6852314815,
		acceleration: 0.105740740740741
	},
	{
		id: 943,
		time: 942,
		velocity: 0.966388888888889,
		power: 254.461031237489,
		road: 12018.3163888889,
		acceleration: 0.297685185185185
	},
	{
		id: 944,
		time: 943,
		velocity: 0.893055555555555,
		power: -21.5067103275069,
		road: 12019.0162962963,
		acceleration: -0.160185185185185
	},
	{
		id: 945,
		time: 944,
		velocity: 0,
		power: 67.5761032048582,
		road: 12019.6303240741,
		acceleration: -0.011574074074074
	},
	{
		id: 946,
		time: 945,
		velocity: 0.931666666666667,
		power: 228.793661723711,
		road: 12020.3438425926,
		acceleration: 0.210555555555555
	},
	{
		id: 947,
		time: 946,
		velocity: 1.52472222222222,
		power: 975.612442310481,
		road: 12021.5316666667,
		acceleration: 0.738055555555556
	},
	{
		id: 948,
		time: 947,
		velocity: 2.21416666666667,
		power: 712.635224577319,
		road: 12023.2434722222,
		acceleration: 0.309907407407408
	},
	{
		id: 949,
		time: 948,
		velocity: 1.86138888888889,
		power: 573.055683227549,
		road: 12025.1998611111,
		acceleration: 0.179259259259259
	},
	{
		id: 950,
		time: 949,
		velocity: 2.0625,
		power: 221.886072759444,
		road: 12027.2383796296,
		acceleration: -0.0150000000000001
	},
	{
		id: 951,
		time: 950,
		velocity: 2.16916666666667,
		power: 534.536760858231,
		road: 12029.3386111111,
		acceleration: 0.138425925925926
	},
	{
		id: 952,
		time: 951,
		velocity: 2.27666666666667,
		power: 507.250060138488,
		road: 12031.5631481482,
		acceleration: 0.110185185185185
	},
	{
		id: 953,
		time: 952,
		velocity: 2.39305555555556,
		power: 581.600510386141,
		road: 12033.9082407407,
		acceleration: 0.130925925925926
	},
	{
		id: 954,
		time: 953,
		velocity: 2.56194444444444,
		power: 774.651856454675,
		road: 12036.4161111111,
		acceleration: 0.19462962962963
	},
	{
		id: 955,
		time: 954,
		velocity: 2.86055555555556,
		power: 884.877274813772,
		road: 12039.1275,
		acceleration: 0.212407407407407
	},
	{
		id: 956,
		time: 955,
		velocity: 3.03027777777778,
		power: 984.827143309304,
		road: 12042.0561574074,
		acceleration: 0.22212962962963
	},
	{
		id: 957,
		time: 956,
		velocity: 3.22833333333333,
		power: 752.215288558939,
		road: 12045.1572685185,
		acceleration: 0.122777777777778
	},
	{
		id: 958,
		time: 957,
		velocity: 3.22888888888889,
		power: 748.61593840474,
		road: 12048.3756944444,
		acceleration: 0.111851851851852
	},
	{
		id: 959,
		time: 958,
		velocity: 3.36583333333333,
		power: 549.945821180131,
		road: 12051.6712037037,
		acceleration: 0.0423148148148149
	},
	{
		id: 960,
		time: 959,
		velocity: 3.35527777777778,
		power: 569.89365370369,
		road: 12055.0109259259,
		acceleration: 0.0461111111111112
	},
	{
		id: 961,
		time: 960,
		velocity: 3.36722222222222,
		power: 639.098576068324,
		road: 12058.4059259259,
		acceleration: 0.0644444444444439
	},
	{
		id: 962,
		time: 961,
		velocity: 3.55916666666667,
		power: 814.236904789458,
		road: 12061.8891666667,
		acceleration: 0.112037037037037
	},
	{
		id: 963,
		time: 962,
		velocity: 3.69138888888889,
		power: 853.139873989769,
		road: 12065.4860185185,
		acceleration: 0.115185185185185
	},
	{
		id: 964,
		time: 963,
		velocity: 3.71277777777778,
		power: 715.864398821892,
		road: 12069.1751388889,
		acceleration: 0.0693518518518519
	},
	{
		id: 965,
		time: 964,
		velocity: 3.76722222222222,
		power: 328.79975059647,
		road: 12072.8781481482,
		acceleration: -0.0415740740740742
	},
	{
		id: 966,
		time: 965,
		velocity: 3.56666666666667,
		power: 264.868509533252,
		road: 12076.5311111111,
		acceleration: -0.0585185185185182
	},
	{
		id: 967,
		time: 966,
		velocity: 3.53722222222222,
		power: 64.9069510265677,
		road: 12080.0971296296,
		acceleration: -0.11537037037037
	},
	{
		id: 968,
		time: 967,
		velocity: 3.42111111111111,
		power: 142.03846350715,
		road: 12083.56,
		acceleration: -0.0909259259259256
	},
	{
		id: 969,
		time: 968,
		velocity: 3.29388888888889,
		power: -212.585868983801,
		road: 12086.8768518519,
		acceleration: -0.201111111111111
	},
	{
		id: 970,
		time: 969,
		velocity: 2.93388888888889,
		power: -410.113092988807,
		road: 12089.9566203704,
		acceleration: -0.273055555555555
	},
	{
		id: 971,
		time: 970,
		velocity: 2.60194444444444,
		power: -152.37712553827,
		road: 12092.8056944445,
		acceleration: -0.188333333333334
	},
	{
		id: 972,
		time: 971,
		velocity: 2.72888888888889,
		power: 411.005328991003,
		road: 12095.5730092593,
		acceleration: 0.0248148148148153
	},
	{
		id: 973,
		time: 972,
		velocity: 3.00833333333333,
		power: 1092.15129449845,
		road: 12098.4843055556,
		acceleration: 0.263148148148148
	},
	{
		id: 974,
		time: 973,
		velocity: 3.39138888888889,
		power: 1337.21506159406,
		road: 12101.6809722222,
		acceleration: 0.307592592592593
	},
	{
		id: 975,
		time: 974,
		velocity: 3.65166666666667,
		power: 1044.34510001547,
		road: 12105.1241666667,
		acceleration: 0.185462962962962
	},
	{
		id: 976,
		time: 975,
		velocity: 3.56472222222222,
		power: 305.468554654746,
		road: 12108.6387037037,
		acceleration: -0.0427777777777774
	},
	{
		id: 977,
		time: 976,
		velocity: 3.26305555555556,
		power: -87.9987507569478,
		road: 12112.0512962963,
		acceleration: -0.161111111111111
	},
	{
		id: 978,
		time: 977,
		velocity: 3.16833333333333,
		power: 43.1114484505439,
		road: 12115.3235648148,
		acceleration: -0.119537037037037
	},
	{
		id: 979,
		time: 978,
		velocity: 3.20611111111111,
		power: 40.5246653405084,
		road: 12118.4763425926,
		acceleration: -0.119444444444444
	},
	{
		id: 980,
		time: 979,
		velocity: 2.90472222222222,
		power: 224.363190267718,
		road: 12121.5416203704,
		acceleration: -0.0555555555555554
	},
	{
		id: 981,
		time: 980,
		velocity: 3.00166666666667,
		power: 593.030974140681,
		road: 12124.6144444445,
		acceleration: 0.0706481481481478
	},
	{
		id: 982,
		time: 981,
		velocity: 3.41805555555556,
		power: 1072.61598082917,
		road: 12127.8316203704,
		acceleration: 0.218055555555555
	},
	{
		id: 983,
		time: 982,
		velocity: 3.55888888888889,
		power: 1258.63973242612,
		road: 12131.2828703704,
		acceleration: 0.250092592592593
	},
	{
		id: 984,
		time: 983,
		velocity: 3.75194444444444,
		power: 1083.78001579039,
		road: 12134.9474537037,
		acceleration: 0.176574074074074
	},
	{
		id: 985,
		time: 984,
		velocity: 3.94777777777778,
		power: 948.799952086766,
		road: 12138.7634722222,
		acceleration: 0.126296296296296
	},
	{
		id: 986,
		time: 985,
		velocity: 3.93777777777778,
		power: 795.373314925738,
		road: 12142.6815277778,
		acceleration: 0.0777777777777779
	},
	{
		id: 987,
		time: 986,
		velocity: 3.98527777777778,
		power: 540.813300199068,
		road: 12146.6422685185,
		acceleration: 0.00759259259259215
	},
	{
		id: 988,
		time: 987,
		velocity: 3.97055555555556,
		power: 515.657756328018,
		road: 12150.6071759259,
		acceleration: 0.000740740740740709
	},
	{
		id: 989,
		time: 988,
		velocity: 3.94,
		power: 490.549295875868,
		road: 12154.569537037,
		acceleration: -0.00583333333333247
	},
	{
		id: 990,
		time: 989,
		velocity: 3.96777777777778,
		power: 499.298862843092,
		road: 12158.5273148148,
		acceleration: -0.00333333333333385
	},
	{
		id: 991,
		time: 990,
		velocity: 3.96055555555556,
		power: 594.066444368,
		road: 12162.4941666667,
		acceleration: 0.0214814814814819
	},
	{
		id: 992,
		time: 991,
		velocity: 4.00444444444444,
		power: 397.177182863974,
		road: 12166.4564351852,
		acceleration: -0.0306481481481486
	},
	{
		id: 993,
		time: 992,
		velocity: 3.87583333333333,
		power: 525.460848525988,
		road: 12170.4053703704,
		acceleration: 0.00398148148148181
	},
	{
		id: 994,
		time: 993,
		velocity: 3.9725,
		power: 528.894288504335,
		road: 12174.3586574074,
		acceleration: 0.00472222222222207
	},
	{
		id: 995,
		time: 994,
		velocity: 4.01861111111111,
		power: 579.025601462594,
		road: 12178.3231018519,
		acceleration: 0.0175925925925928
	},
	{
		id: 996,
		time: 995,
		velocity: 3.92861111111111,
		power: 446.577246340341,
		road: 12182.2875462963,
		acceleration: -0.0175925925925928
	},
	{
		id: 997,
		time: 996,
		velocity: 3.91972222222222,
		power: 495.2133884593,
		road: 12186.2410648148,
		acceleration: -0.00425925925925918
	},
	{
		id: 998,
		time: 997,
		velocity: 4.00583333333333,
		power: 632.852761390791,
		road: 12190.2083333333,
		acceleration: 0.0317592592592595
	},
	{
		id: 999,
		time: 998,
		velocity: 4.02388888888889,
		power: 536.97593267998,
		road: 12194.1942592593,
		acceleration: 0.00555555555555554
	},
	{
		id: 1000,
		time: 999,
		velocity: 3.93638888888889,
		power: 483.146869958664,
		road: 12198.1786574074,
		acceleration: -0.00861111111111157
	},
	{
		id: 1001,
		time: 1000,
		velocity: 3.98,
		power: 394.63851731096,
		road: 12202.1430555556,
		acceleration: -0.0313888888888889
	},
	{
		id: 1002,
		time: 1001,
		velocity: 3.92972222222222,
		power: 319.871704547756,
		road: 12206.0666666667,
		acceleration: -0.0501851851851853
	},
	{
		id: 1003,
		time: 1002,
		velocity: 3.78583333333333,
		power: 260.549244861454,
		road: 12209.9327777778,
		acceleration: -0.0648148148148144
	},
	{
		id: 1004,
		time: 1003,
		velocity: 3.78555555555556,
		power: 145.748599437871,
		road: 12213.7190277778,
		acceleration: -0.0949074074074074
	},
	{
		id: 1005,
		time: 1004,
		velocity: 3.645,
		power: 299.879152169365,
		road: 12217.4327777778,
		acceleration: -0.0500925925925926
	},
	{
		id: 1006,
		time: 1005,
		velocity: 3.63555555555556,
		power: 291.840229492115,
		road: 12221.0959722222,
		acceleration: -0.0510185185185184
	},
	{
		id: 1007,
		time: 1006,
		velocity: 3.6325,
		power: 370.683910162573,
		road: 12224.7201388889,
		acceleration: -0.0270370370370374
	},
	{
		id: 1008,
		time: 1007,
		velocity: 3.56388888888889,
		power: 500.718077223107,
		road: 12228.3363425926,
		acceleration: 0.0111111111111111
	},
	{
		id: 1009,
		time: 1008,
		velocity: 3.66888888888889,
		power: 1450.59642679607,
		road: 12232.09375,
		acceleration: 0.271296296296296
	},
	{
		id: 1010,
		time: 1009,
		velocity: 4.44638888888889,
		power: 2324.09080600745,
		road: 12236.2152314815,
		acceleration: 0.456851851851852
	},
	{
		id: 1011,
		time: 1010,
		velocity: 4.93444444444444,
		power: 2656.70253645433,
		road: 12240.8005092593,
		acceleration: 0.470740740740741
	},
	{
		id: 1012,
		time: 1011,
		velocity: 5.08111111111111,
		power: 1662.98151073139,
		road: 12245.7282407407,
		acceleration: 0.214166666666666
	},
	{
		id: 1013,
		time: 1012,
		velocity: 5.08888888888889,
		power: 496.726052356931,
		road: 12250.7443981482,
		acceleration: -0.037314814814815
	},
	{
		id: 1014,
		time: 1013,
		velocity: 4.8225,
		power: -98.7507585063287,
		road: 12255.6608333333,
		acceleration: -0.16212962962963
	},
	{
		id: 1015,
		time: 1014,
		velocity: 4.59472222222222,
		power: 337.020966196578,
		road: 12260.462962963,
		acceleration: -0.0664814814814809
	},
	{
		id: 1016,
		time: 1015,
		velocity: 4.88944444444444,
		power: 1528.96257266892,
		road: 12265.3269444444,
		acceleration: 0.190185185185184
	},
	{
		id: 1017,
		time: 1016,
		velocity: 5.39305555555555,
		power: 3127.92922697823,
		road: 12270.5309722222,
		acceleration: 0.489907407407408
	},
	{
		id: 1018,
		time: 1017,
		velocity: 6.06444444444444,
		power: 3538.51531010183,
		road: 12276.2335185185,
		acceleration: 0.507129629629629
	},
	{
		id: 1019,
		time: 1018,
		velocity: 6.41083333333333,
		power: 3757.91549838914,
		road: 12282.4338425926,
		acceleration: 0.488425925925926
	},
	{
		id: 1020,
		time: 1019,
		velocity: 6.85833333333333,
		power: 2977.5319061098,
		road: 12289.0393981482,
		acceleration: 0.322037037037037
	},
	{
		id: 1021,
		time: 1020,
		velocity: 7.03055555555556,
		power: 2981.3316907387,
		road: 12295.9554166667,
		acceleration: 0.298888888888888
	},
	{
		id: 1022,
		time: 1021,
		velocity: 7.3075,
		power: 2549.66496362512,
		road: 12303.1294907407,
		acceleration: 0.217222222222222
	},
	{
		id: 1023,
		time: 1022,
		velocity: 7.51,
		power: 3151.01401440504,
		road: 12310.5559259259,
		acceleration: 0.2875
	},
	{
		id: 1024,
		time: 1023,
		velocity: 7.89305555555555,
		power: 4206.11648395015,
		road: 12318.3296759259,
		acceleration: 0.40712962962963
	},
	{
		id: 1025,
		time: 1024,
		velocity: 8.52888888888889,
		power: 5340.35700688327,
		road: 12326.5648148148,
		acceleration: 0.515648148148149
	},
	{
		id: 1026,
		time: 1025,
		velocity: 9.05694444444444,
		power: 5767.59341590205,
		road: 12335.3184722222,
		acceleration: 0.521388888888888
	},
	{
		id: 1027,
		time: 1026,
		velocity: 9.45722222222222,
		power: 5686.70590072636,
		road: 12344.5677777778,
		acceleration: 0.469907407407408
	},
	{
		id: 1028,
		time: 1027,
		velocity: 9.93861111111111,
		power: 5680.51120076415,
		road: 12354.2691203704,
		acceleration: 0.434166666666666
	},
	{
		id: 1029,
		time: 1028,
		velocity: 10.3594444444444,
		power: 6745.12464581013,
		road: 12364.4425462963,
		acceleration: 0.51
	},
	{
		id: 1030,
		time: 1029,
		velocity: 10.9872222222222,
		power: 5820.58111851097,
		road: 12375.062962963,
		acceleration: 0.383981481481481
	},
	{
		id: 1031,
		time: 1030,
		velocity: 11.0905555555556,
		power: 4388.97458169577,
		road: 12385.9887037037,
		acceleration: 0.226666666666668
	},
	{
		id: 1032,
		time: 1031,
		velocity: 11.0394444444444,
		power: 1737.02123166409,
		road: 12397.0124537037,
		acceleration: -0.0306481481481491
	},
	{
		id: 1033,
		time: 1032,
		velocity: 10.8952777777778,
		power: 1242.25065651295,
		road: 12407.9826388889,
		acceleration: -0.0764814814814834
	},
	{
		id: 1034,
		time: 1033,
		velocity: 10.8611111111111,
		power: 80.8088168194306,
		road: 12418.8216666667,
		acceleration: -0.185833333333331
	},
	{
		id: 1035,
		time: 1034,
		velocity: 10.4819444444444,
		power: 1305.85533641335,
		road: 12429.5356481482,
		acceleration: -0.0642592592592592
	},
	{
		id: 1036,
		time: 1035,
		velocity: 10.7025,
		power: 2284.77837650455,
		road: 12440.2335648148,
		acceleration: 0.0321296296296314
	},
	{
		id: 1037,
		time: 1036,
		velocity: 10.9575,
		power: 5229.20071581298,
		road: 12451.1028703704,
		acceleration: 0.310648148148147
	},
	{
		id: 1038,
		time: 1037,
		velocity: 11.4138888888889,
		power: 6095.15233904225,
		road: 12462.3134722222,
		acceleration: 0.371944444444445
	},
	{
		id: 1039,
		time: 1038,
		velocity: 11.8183333333333,
		power: 6090.14419210829,
		road: 12473.884537037,
		acceleration: 0.348981481481482
	},
	{
		id: 1040,
		time: 1039,
		velocity: 12.0044444444444,
		power: 4804.39968259262,
		road: 12485.739212963,
		acceleration: 0.21824074074074
	},
	{
		id: 1041,
		time: 1040,
		velocity: 12.0686111111111,
		power: 3573.43166941356,
		road: 12497.7544907407,
		acceleration: 0.102962962962962
	},
	{
		id: 1042,
		time: 1041,
		velocity: 12.1272222222222,
		power: 2642.44036786887,
		road: 12509.8311111111,
		acceleration: 0.0197222222222244
	},
	{
		id: 1043,
		time: 1042,
		velocity: 12.0636111111111,
		power: 4244.26822617691,
		road: 12521.9950462963,
		acceleration: 0.154907407407403
	},
	{
		id: 1044,
		time: 1043,
		velocity: 12.5333333333333,
		power: 4327.17890237016,
		road: 12534.3140277778,
		acceleration: 0.155185185185186
	},
	{
		id: 1045,
		time: 1044,
		velocity: 12.5927777777778,
		power: 5682.58208894308,
		road: 12546.8403703704,
		acceleration: 0.259537037037038
	},
	{
		id: 1046,
		time: 1045,
		velocity: 12.8422222222222,
		power: 6044.75711281047,
		road: 12559.6341666667,
		acceleration: 0.275370370370371
	},
	{
		id: 1047,
		time: 1046,
		velocity: 13.3594444444444,
		power: 5878.84221285426,
		road: 12572.6897685185,
		acceleration: 0.248240740740741
	},
	{
		id: 1048,
		time: 1047,
		velocity: 13.3375,
		power: 4791.03343529959,
		road: 12585.9455555556,
		acceleration: 0.152129629629629
	},
	{
		id: 1049,
		time: 1048,
		velocity: 13.2986111111111,
		power: 2719.97962309592,
		road: 12599.2705555556,
		acceleration: -0.0137037037037047
	},
	{
		id: 1050,
		time: 1049,
		velocity: 13.3183333333333,
		power: 3808.98081427994,
		road: 12612.6241203704,
		acceleration: 0.0708333333333329
	},
	{
		id: 1051,
		time: 1050,
		velocity: 13.55,
		power: 4206.12098815877,
		road: 12626.0624074074,
		acceleration: 0.0986111111111114
	},
	{
		id: 1052,
		time: 1051,
		velocity: 13.5944444444444,
		power: 3663.21786738627,
		road: 12639.5767592593,
		acceleration: 0.053518518518521
	},
	{
		id: 1053,
		time: 1052,
		velocity: 13.4788888888889,
		power: 1877.99119203731,
		road: 12653.0756481482,
		acceleration: -0.084444444444447
	},
	{
		id: 1054,
		time: 1053,
		velocity: 13.2966666666667,
		power: 125.239336929719,
		road: 12666.423287037,
		acceleration: -0.218055555555553
	},
	{
		id: 1055,
		time: 1054,
		velocity: 12.9402777777778,
		power: 326.925039808923,
		road: 12679.5625462963,
		acceleration: -0.198703703703705
	},
	{
		id: 1056,
		time: 1055,
		velocity: 12.8827777777778,
		power: 1677.63803494648,
		road: 12692.55875,
		acceleration: -0.0874074074074063
	},
	{
		id: 1057,
		time: 1056,
		velocity: 13.0344444444444,
		power: 3231.36049574785,
		road: 12705.5306018519,
		acceleration: 0.0387037037037032
	},
	{
		id: 1058,
		time: 1057,
		velocity: 13.0563888888889,
		power: 3644.01150651221,
		road: 12718.5568055556,
		acceleration: 0.0700000000000003
	},
	{
		id: 1059,
		time: 1058,
		velocity: 13.0927777777778,
		power: 3336.22536428897,
		road: 12731.6396296296,
		acceleration: 0.0432407407407407
	},
	{
		id: 1060,
		time: 1059,
		velocity: 13.1641666666667,
		power: 4039.12144632512,
		road: 12744.7924537037,
		acceleration: 0.0967592592592599
	},
	{
		id: 1061,
		time: 1060,
		velocity: 13.3466666666667,
		power: 4078.91857991482,
		road: 12758.041712963,
		acceleration: 0.0961111111111119
	},
	{
		id: 1062,
		time: 1061,
		velocity: 13.3811111111111,
		power: 3206.90502796469,
		road: 12771.3515740741,
		acceleration: 0.0250925925925891
	},
	{
		id: 1063,
		time: 1062,
		velocity: 13.2394444444444,
		power: 332.962777766581,
		road: 12784.5741666667,
		acceleration: -0.199629629629628
	},
	{
		id: 1064,
		time: 1063,
		velocity: 12.7477777777778,
		power: -663.241242210389,
		road: 12797.5589351852,
		acceleration: -0.276018518518519
	},
	{
		id: 1065,
		time: 1064,
		velocity: 12.5530555555556,
		power: -2206.4629545558,
		road: 12810.205462963,
		acceleration: -0.400462962962962
	},
	{
		id: 1066,
		time: 1065,
		velocity: 12.0380555555556,
		power: -1597.45590916839,
		road: 12822.4773611111,
		acceleration: -0.348796296296294
	},
	{
		id: 1067,
		time: 1066,
		velocity: 11.7013888888889,
		power: -2345.64980993432,
		road: 12834.3678703704,
		acceleration: -0.413981481481484
	},
	{
		id: 1068,
		time: 1067,
		velocity: 11.3111111111111,
		power: -836.132227035496,
		road: 12845.9121296296,
		acceleration: -0.278518518518517
	},
	{
		id: 1069,
		time: 1068,
		velocity: 11.2025,
		power: -328.599793136415,
		road: 12857.2022222222,
		acceleration: -0.229814814814816
	},
	{
		id: 1070,
		time: 1069,
		velocity: 11.0119444444444,
		power: 2073.74963721752,
		road: 12868.3758333333,
		acceleration: -0.00314814814814923
	},
	{
		id: 1071,
		time: 1070,
		velocity: 11.3016666666667,
		power: 3171.80592651362,
		road: 12879.5968981482,
		acceleration: 0.0980555555555558
	},
	{
		id: 1072,
		time: 1071,
		velocity: 11.4966666666667,
		power: 4439.88586157498,
		road: 12890.9714814815,
		acceleration: 0.20898148148148
	},
	{
		id: 1073,
		time: 1072,
		velocity: 11.6388888888889,
		power: 2589.35018877379,
		road: 12902.4676851852,
		acceleration: 0.0342592592592581
	},
	{
		id: 1074,
		time: 1073,
		velocity: 11.4044444444444,
		power: 941.932496178964,
		road: 12913.9234259259,
		acceleration: -0.115185185185183
	},
	{
		id: 1075,
		time: 1074,
		velocity: 11.1511111111111,
		power: -903.579126978259,
		road: 12925.1800462963,
		acceleration: -0.283055555555556
	},
	{
		id: 1076,
		time: 1075,
		velocity: 10.7897222222222,
		power: -1076.94387175071,
		road: 12936.1460185185,
		acceleration: -0.29824074074074
	},
	{
		id: 1077,
		time: 1076,
		velocity: 10.5097222222222,
		power: -1593.09279033637,
		road: 12946.7887037037,
		acceleration: -0.348333333333331
	},
	{
		id: 1078,
		time: 1077,
		velocity: 10.1061111111111,
		power: -1461.31623824815,
		road: 12957.0891666667,
		acceleration: -0.336111111111112
	},
	{
		id: 1079,
		time: 1078,
		velocity: 9.78138888888889,
		power: -3481.35776167069,
		road: 12966.9450925926,
		acceleration: -0.552962962962965
	},
	{
		id: 1080,
		time: 1079,
		velocity: 8.85083333333333,
		power: -3527.6197623585,
		road: 12976.2371759259,
		acceleration: -0.574722222222221
	},
	{
		id: 1081,
		time: 1080,
		velocity: 8.38194444444444,
		power: -3727.6874700824,
		road: 12984.931712963,
		acceleration: -0.62037037037037
	},
	{
		id: 1082,
		time: 1081,
		velocity: 7.92027777777778,
		power: -2688.84923391376,
		road: 12993.0599537037,
		acceleration: -0.512222222222222
	},
	{
		id: 1083,
		time: 1082,
		velocity: 7.31416666666667,
		power: -3289.50081602964,
		road: 13000.6237037037,
		acceleration: -0.61675925925926
	},
	{
		id: 1084,
		time: 1083,
		velocity: 6.53166666666667,
		power: -3308.10369260312,
		road: 13007.550787037,
		acceleration: -0.656574074074073
	},
	{
		id: 1085,
		time: 1084,
		velocity: 5.95055555555556,
		power: -3284.12256194226,
		road: 13013.7984259259,
		acceleration: -0.702314814814815
	},
	{
		id: 1086,
		time: 1085,
		velocity: 5.20722222222222,
		power: -3010.80145545891,
		road: 13019.3365277778,
		acceleration: -0.716759259259259
	},
	{
		id: 1087,
		time: 1086,
		velocity: 4.38138888888889,
		power: -2156.07621618318,
		road: 13024.2131481482,
		acceleration: -0.606203703703703
	},
	{
		id: 1088,
		time: 1087,
		velocity: 4.13194444444444,
		power: -1217.59272575034,
		road: 13028.5705092593,
		acceleration: -0.432314814814815
	},
	{
		id: 1089,
		time: 1088,
		velocity: 3.91027777777778,
		power: -141.283102480684,
		road: 13032.6250462963,
		acceleration: -0.173333333333333
	},
	{
		id: 1090,
		time: 1089,
		velocity: 3.86138888888889,
		power: 122.908692825861,
		road: 13036.5414351852,
		acceleration: -0.102962962962963
	},
	{
		id: 1091,
		time: 1090,
		velocity: 3.82305555555556,
		power: 207.746776516327,
		road: 13040.3671296296,
		acceleration: -0.0784259259259263
	},
	{
		id: 1092,
		time: 1091,
		velocity: 3.675,
		power: 253.610469268222,
		road: 13044.1215277778,
		acceleration: -0.0641666666666669
	},
	{
		id: 1093,
		time: 1092,
		velocity: 3.66888888888889,
		power: 177.632088294091,
		road: 13047.8017592593,
		acceleration: -0.0841666666666665
	},
	{
		id: 1094,
		time: 1093,
		velocity: 3.57055555555556,
		power: 295.789969983544,
		road: 13051.4156481482,
		acceleration: -0.0485185185185184
	},
	{
		id: 1095,
		time: 1094,
		velocity: 3.52944444444444,
		power: -35.5550523796664,
		road: 13054.9327777778,
		acceleration: -0.145
	},
	{
		id: 1096,
		time: 1095,
		velocity: 3.23388888888889,
		power: -243.698481451277,
		road: 13058.2721296296,
		acceleration: -0.210555555555555
	},
	{
		id: 1097,
		time: 1096,
		velocity: 2.93888888888889,
		power: -75.8643288048846,
		road: 13061.427037037,
		acceleration: -0.158333333333333
	},
	{
		id: 1098,
		time: 1097,
		velocity: 3.05444444444444,
		power: 431.75860502845,
		road: 13064.5101851852,
		acceleration: 0.0148148148148146
	},
	{
		id: 1099,
		time: 1098,
		velocity: 3.27833333333333,
		power: -340.54579359216,
		road: 13067.4740277778,
		acceleration: -0.253425925925926
	},
	{
		id: 1100,
		time: 1099,
		velocity: 2.17861111111111,
		power: 28.9828296635026,
		road: 13070.250787037,
		acceleration: -0.120740740740741
	},
	{
		id: 1101,
		time: 1100,
		velocity: 2.69222222222222,
		power: -116.801225200103,
		road: 13072.8781018519,
		acceleration: -0.178148148148148
	},
	{
		id: 1102,
		time: 1101,
		velocity: 2.74388888888889,
		power: 726.467448459645,
		road: 13075.4968981482,
		acceleration: 0.161111111111111
	},
	{
		id: 1103,
		time: 1102,
		velocity: 2.66194444444444,
		power: 303.553005970902,
		road: 13078.1899074074,
		acceleration: -0.0126851851851857
	},
	{
		id: 1104,
		time: 1103,
		velocity: 2.65416666666667,
		power: -358.106384436863,
		road: 13080.7369444445,
		acceleration: -0.279259259259259
	},
	{
		id: 1105,
		time: 1104,
		velocity: 1.90611111111111,
		power: -531.850939923824,
		road: 13082.9527314815,
		acceleration: -0.383240740740741
	},
	{
		id: 1106,
		time: 1105,
		velocity: 1.51222222222222,
		power: -588.645791565011,
		road: 13084.7384722222,
		acceleration: -0.476851851851852
	},
	{
		id: 1107,
		time: 1106,
		velocity: 1.22361111111111,
		power: -134.536631574516,
		road: 13086.1719907407,
		acceleration: -0.227592592592593
	},
	{
		id: 1108,
		time: 1107,
		velocity: 1.22333333333333,
		power: 3.43474921319775,
		road: 13087.4289814815,
		acceleration: -0.125462962962963
	},
	{
		id: 1109,
		time: 1108,
		velocity: 1.13583333333333,
		power: 27.8433623561025,
		road: 13088.5719907407,
		acceleration: -0.1025
	},
	{
		id: 1110,
		time: 1109,
		velocity: 0.916111111111111,
		power: 14.0932681264391,
		road: 13089.6068981482,
		acceleration: -0.113703703703704
	},
	{
		id: 1111,
		time: 1110,
		velocity: 0.882222222222222,
		power: 404.184938906629,
		road: 13090.7135185185,
		acceleration: 0.257129629629629
	},
	{
		id: 1112,
		time: 1111,
		velocity: 1.90722222222222,
		power: 1223.0163687522,
		road: 13092.2926388889,
		acceleration: 0.687870370370371
	},
	{
		id: 1113,
		time: 1112,
		velocity: 2.97972222222222,
		power: 2608.1950376307,
		road: 13094.7172222222,
		acceleration: 1.00305555555555
	},
	{
		id: 1114,
		time: 1113,
		velocity: 3.89138888888889,
		power: 2392.04747528416,
		road: 13097.9646759259,
		acceleration: 0.642685185185186
	},
	{
		id: 1115,
		time: 1114,
		velocity: 3.83527777777778,
		power: 1330.8649804553,
		road: 13101.6558333333,
		acceleration: 0.244722222222222
	},
	{
		id: 1116,
		time: 1115,
		velocity: 3.71388888888889,
		power: 115.676609470181,
		road: 13105.4178703704,
		acceleration: -0.102962962962963
	},
	{
		id: 1117,
		time: 1116,
		velocity: 3.5825,
		power: 171.039129387178,
		road: 13109.0855092593,
		acceleration: -0.0858333333333325
	},
	{
		id: 1118,
		time: 1117,
		velocity: 3.57777777777778,
		power: 141.091944818922,
		road: 13112.6637037037,
		acceleration: -0.0930555555555559
	},
	{
		id: 1119,
		time: 1118,
		velocity: 3.43472222222222,
		power: 161.229303616006,
		road: 13116.1525925926,
		acceleration: -0.0855555555555556
	},
	{
		id: 1120,
		time: 1119,
		velocity: 3.32583333333333,
		power: 180.975816050263,
		road: 13119.5597222222,
		acceleration: -0.077962962962963
	},
	{
		id: 1121,
		time: 1120,
		velocity: 3.34388888888889,
		power: 290.002372433935,
		road: 13122.9066666667,
		acceleration: -0.0424074074074072
	},
	{
		id: 1122,
		time: 1121,
		velocity: 3.3075,
		power: 361.010665826769,
		road: 13126.222962963,
		acceleration: -0.0188888888888892
	},
	{
		id: 1123,
		time: 1122,
		velocity: 3.26916666666667,
		power: 148.473535079526,
		road: 13129.4870833333,
		acceleration: -0.0854629629629629
	},
	{
		id: 1124,
		time: 1123,
		velocity: 3.0875,
		power: 171.316404297275,
		road: 13132.6702777778,
		acceleration: -0.0763888888888888
	},
	{
		id: 1125,
		time: 1124,
		velocity: 3.07833333333333,
		power: 69.7717699414932,
		road: 13135.760787037,
		acceleration: -0.108981481481481
	},
	{
		id: 1126,
		time: 1125,
		velocity: 2.94222222222222,
		power: 200.341331122862,
		road: 13138.7656944444,
		acceleration: -0.0622222222222222
	},
	{
		id: 1127,
		time: 1126,
		velocity: 2.90083333333333,
		power: 108.273556353851,
		road: 13141.6928703704,
		acceleration: -0.0932407407407414
	},
	{
		id: 1128,
		time: 1127,
		velocity: 2.79861111111111,
		power: 146.309112919369,
		road: 13144.5345833333,
		acceleration: -0.0776851851851852
	},
	{
		id: 1129,
		time: 1128,
		velocity: 2.70916666666667,
		power: 78.818632841358,
		road: 13147.286712963,
		acceleration: -0.101481481481481
	},
	{
		id: 1130,
		time: 1129,
		velocity: 2.59638888888889,
		power: 87.1092073179483,
		road: 13149.9397222222,
		acceleration: -0.0967592592592594
	},
	{
		id: 1131,
		time: 1130,
		velocity: 2.50833333333333,
		power: 186.636178622135,
		road: 13152.5169444444,
		acceleration: -0.0548148148148147
	},
	{
		id: 1132,
		time: 1131,
		velocity: 2.54472222222222,
		power: 506.106502832758,
		road: 13155.1042592593,
		acceleration: 0.0750000000000002
	},
	{
		id: 1133,
		time: 1132,
		velocity: 2.82138888888889,
		power: 856.273288084566,
		road: 13157.8288888889,
		acceleration: 0.199629629629629
	},
	{
		id: 1134,
		time: 1133,
		velocity: 3.10722222222222,
		power: 951.923717764249,
		road: 13160.7584259259,
		acceleration: 0.210185185185185
	},
	{
		id: 1135,
		time: 1134,
		velocity: 3.17527777777778,
		power: 340.415182979109,
		road: 13163.7860185185,
		acceleration: -0.0140740740740739
	},
	{
		id: 1136,
		time: 1135,
		velocity: 2.77916666666667,
		power: -240.815202231277,
		road: 13166.6968981481,
		acceleration: -0.219351851851852
	},
	{
		id: 1137,
		time: 1136,
		velocity: 2.44916666666667,
		power: -332.055484046351,
		road: 13169.3668518519,
		acceleration: -0.2625
	},
	{
		id: 1138,
		time: 1137,
		velocity: 2.38777777777778,
		power: 29.0515179653568,
		road: 13171.8462962963,
		acceleration: -0.118518518518519
	},
	{
		id: 1139,
		time: 1138,
		velocity: 2.42361111111111,
		power: 403.531319374807,
		road: 13174.2881944444,
		acceleration: 0.0434259259259262
	},
	{
		id: 1140,
		time: 1139,
		velocity: 2.57944444444444,
		power: 775.391165031736,
		road: 13176.8460185185,
		acceleration: 0.188425925925926
	},
	{
		id: 1141,
		time: 1140,
		velocity: 2.95305555555556,
		power: 902.371491956323,
		road: 13179.6045833333,
		acceleration: 0.213055555555556
	},
	{
		id: 1142,
		time: 1141,
		velocity: 3.06277777777778,
		power: 826.66498280469,
		road: 13182.5513425926,
		acceleration: 0.163333333333334
	},
	{
		id: 1143,
		time: 1142,
		velocity: 3.06944444444444,
		power: 541.995281252521,
		road: 13185.6068981481,
		acceleration: 0.0542592592592595
	},
	{
		id: 1144,
		time: 1143,
		velocity: 3.11583333333333,
		power: 560.088516933489,
		road: 13188.7180092593,
		acceleration: 0.0568518518518517
	},
	{
		id: 1145,
		time: 1144,
		velocity: 3.23333333333333,
		power: 615.565475740652,
		road: 13191.8931481482,
		acceleration: 0.0712037037037034
	},
	{
		id: 1146,
		time: 1145,
		velocity: 3.28305555555556,
		power: 572.262745269226,
		road: 13195.1303703704,
		acceleration: 0.0529629629629627
	},
	{
		id: 1147,
		time: 1146,
		velocity: 3.27472222222222,
		power: 525.497692816212,
		road: 13198.411712963,
		acceleration: 0.0352777777777784
	},
	{
		id: 1148,
		time: 1147,
		velocity: 3.33916666666667,
		power: 537.259937975892,
		road: 13201.729212963,
		acceleration: 0.0370370370370368
	},
	{
		id: 1149,
		time: 1148,
		velocity: 3.39416666666667,
		power: 554.013788341339,
		road: 13205.0853240741,
		acceleration: 0.0401851851851851
	},
	{
		id: 1150,
		time: 1149,
		velocity: 3.39527777777778,
		power: 436.380087466981,
		road: 13208.4626851852,
		acceleration: 0.00231481481481488
	},
	{
		id: 1151,
		time: 1150,
		velocity: 3.34611111111111,
		power: 274.55900171769,
		road: 13211.8174537037,
		acceleration: -0.0474999999999999
	},
	{
		id: 1152,
		time: 1151,
		velocity: 3.25166666666667,
		power: 376.236147000675,
		road: 13215.1412962963,
		acceleration: -0.0143518518518522
	},
	{
		id: 1153,
		time: 1152,
		velocity: 3.35222222222222,
		power: 373.847676691807,
		road: 13218.4506944444,
		acceleration: -0.0145370370370368
	},
	{
		id: 1154,
		time: 1153,
		velocity: 3.3025,
		power: 490.824576249217,
		road: 13221.7640740741,
		acceleration: 0.0225
	},
	{
		id: 1155,
		time: 1154,
		velocity: 3.31916666666667,
		power: 398.058807542368,
		road: 13225.0850462963,
		acceleration: -0.00731481481481477
	},
	{
		id: 1156,
		time: 1155,
		velocity: 3.33027777777778,
		power: 253.269317189892,
		road: 13228.3761574074,
		acceleration: -0.0524074074074079
	},
	{
		id: 1157,
		time: 1156,
		velocity: 3.14527777777778,
		power: -303.94556869117,
		road: 13231.5237037037,
		acceleration: -0.234722222222222
	},
	{
		id: 1158,
		time: 1157,
		velocity: 2.615,
		power: -525.572609782633,
		road: 13234.3912962963,
		acceleration: -0.325185185185185
	},
	{
		id: 1159,
		time: 1158,
		velocity: 2.35472222222222,
		power: -786.176833144301,
		road: 13236.8632407407,
		acceleration: -0.466111111111111
	},
	{
		id: 1160,
		time: 1159,
		velocity: 1.74694444444444,
		power: -1099.63693139679,
		road: 13238.7262037037,
		acceleration: -0.751851851851852
	},
	{
		id: 1161,
		time: 1160,
		velocity: 0.359444444444444,
		power: -681.390131389156,
		road: 13239.820787037,
		acceleration: -0.784907407407407
	},
	{
		id: 1162,
		time: 1161,
		velocity: 0,
		power: -177.063674442054,
		road: 13240.2317592593,
		acceleration: -0.582314814814815
	},
	{
		id: 1163,
		time: 1162,
		velocity: 0,
		power: 0.437870597790773,
		road: 13240.2916666667,
		acceleration: -0.119814814814815
	},
	{
		id: 1164,
		time: 1163,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1165,
		time: 1164,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1166,
		time: 1165,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1167,
		time: 1166,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1168,
		time: 1167,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1169,
		time: 1168,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1170,
		time: 1169,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1171,
		time: 1170,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1172,
		time: 1171,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1173,
		time: 1172,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1174,
		time: 1173,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1175,
		time: 1174,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1176,
		time: 1175,
		velocity: 0,
		power: 0,
		road: 13240.2916666667,
		acceleration: 0
	},
	{
		id: 1177,
		time: 1176,
		velocity: 0,
		power: 282.519238259057,
		road: 13240.6471759259,
		acceleration: 0.711018518518518
	},
	{
		id: 1178,
		time: 1177,
		velocity: 2.13305555555556,
		power: 1384.61776503146,
		road: 13241.8842592593,
		acceleration: 1.05212962962963
	},
	{
		id: 1179,
		time: 1178,
		velocity: 3.15638888888889,
		power: 3575.04315820461,
		road: 13244.3469444444,
		acceleration: 1.39907407407407
	},
	{
		id: 1180,
		time: 1179,
		velocity: 4.19722222222222,
		power: 3381.10523533568,
		road: 13247.9377314815,
		acceleration: 0.857129629629629
	},
	{
		id: 1181,
		time: 1180,
		velocity: 4.70444444444444,
		power: 2815.7270255603,
		road: 13252.233287037,
		acceleration: 0.552407407407408
	},
	{
		id: 1182,
		time: 1181,
		velocity: 4.81361111111111,
		power: 1229.8645261014,
		road: 13256.8747685185,
		acceleration: 0.139444444444444
	},
	{
		id: 1183,
		time: 1182,
		velocity: 4.61555555555556,
		power: 1049.13642480332,
		road: 13261.6319907407,
		acceleration: 0.0920370370370369
	},
	{
		id: 1184,
		time: 1183,
		velocity: 4.98055555555556,
		power: 2063.45510695902,
		road: 13266.5839351852,
		acceleration: 0.297407407407408
	},
	{
		id: 1185,
		time: 1184,
		velocity: 5.70583333333333,
		power: 2583.10590722062,
		road: 13271.8701388889,
		acceleration: 0.371111111111111
	},
	{
		id: 1186,
		time: 1185,
		velocity: 5.72888888888889,
		power: 2202.14078032732,
		road: 13277.4760185185,
		acceleration: 0.26824074074074
	},
	{
		id: 1187,
		time: 1186,
		velocity: 5.78527777777778,
		power: 1274.43196162289,
		road: 13283.2588425926,
		acceleration: 0.0856481481481488
	},
	{
		id: 1188,
		time: 1187,
		velocity: 5.96277777777778,
		power: 1770.66557366965,
		road: 13289.1685648148,
		acceleration: 0.168148148148148
	},
	{
		id: 1189,
		time: 1188,
		velocity: 6.23333333333333,
		power: 1785.95041094741,
		road: 13295.2429166667,
		acceleration: 0.161111111111111
	},
	{
		id: 1190,
		time: 1189,
		velocity: 6.26861111111111,
		power: 1272.97237473193,
		road: 13301.4315277778,
		acceleration: 0.0674074074074076
	},
	{
		id: 1191,
		time: 1190,
		velocity: 6.165,
		power: 729.885875933259,
		road: 13307.6411111111,
		acceleration: -0.0254629629629637
	},
	{
		id: 1192,
		time: 1191,
		velocity: 6.15694444444444,
		power: 380.42230478762,
		road: 13313.7961111111,
		acceleration: -0.0837037037037032
	},
	{
		id: 1193,
		time: 1192,
		velocity: 6.0175,
		power: 516.669653660433,
		road: 13319.8798148148,
		acceleration: -0.0588888888888892
	},
	{
		id: 1194,
		time: 1193,
		velocity: 5.98833333333333,
		power: 588.234078278566,
		road: 13325.9114351852,
		acceleration: -0.0452777777777769
	},
	{
		id: 1195,
		time: 1194,
		velocity: 6.02111111111111,
		power: 380.031954428635,
		road: 13331.8801851852,
		acceleration: -0.0804629629629634
	},
	{
		id: 1196,
		time: 1195,
		velocity: 5.77611111111111,
		power: 92.2682302155441,
		road: 13337.7436111111,
		acceleration: -0.130185185185185
	},
	{
		id: 1197,
		time: 1196,
		velocity: 5.59777777777778,
		power: -424.665155990826,
		road: 13343.4298611111,
		acceleration: -0.224166666666666
	},
	{
		id: 1198,
		time: 1197,
		velocity: 5.34861111111111,
		power: -391.726289125998,
		road: 13348.894212963,
		acceleration: -0.21962962962963
	},
	{
		id: 1199,
		time: 1198,
		velocity: 5.11722222222222,
		power: -520.44622727856,
		road: 13354.125,
		acceleration: -0.2475
	},
	{
		id: 1200,
		time: 1199,
		velocity: 4.85527777777778,
		power: -525.097492208529,
		road: 13359.1058796296,
		acceleration: -0.252314814814816
	},
	{
		id: 1201,
		time: 1200,
		velocity: 4.59166666666667,
		power: -1148.18377808125,
		road: 13363.7609722222,
		acceleration: -0.399259259259259
	},
	{
		id: 1202,
		time: 1201,
		velocity: 3.91944444444444,
		power: -2832.37372145476,
		road: 13367.7767592593,
		acceleration: -0.879351851851852
	},
	{
		id: 1203,
		time: 1202,
		velocity: 2.21722222222222,
		power: -3110.75764475882,
		road: 13370.7319907407,
		acceleration: -1.24175925925926
	},
	{
		id: 1204,
		time: 1203,
		velocity: 0.866388888888889,
		power: -1876.67115446056,
		road: 13372.4131018519,
		acceleration: -1.30648148148148
	},
	{
		id: 1205,
		time: 1204,
		velocity: 0,
		power: -381.380575555033,
		road: 13373.0714351852,
		acceleration: -0.739074074074074
	},
	{
		id: 1206,
		time: 1205,
		velocity: 0,
		power: -22.0609464100065,
		road: 13373.2158333333,
		acceleration: -0.288796296296296
	},
	{
		id: 1207,
		time: 1206,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1208,
		time: 1207,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1209,
		time: 1208,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1210,
		time: 1209,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1211,
		time: 1210,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1212,
		time: 1211,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1213,
		time: 1212,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1214,
		time: 1213,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1215,
		time: 1214,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1216,
		time: 1215,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1217,
		time: 1216,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1218,
		time: 1217,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1219,
		time: 1218,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1220,
		time: 1219,
		velocity: 0,
		power: 0,
		road: 13373.2158333333,
		acceleration: 0
	},
	{
		id: 1221,
		time: 1220,
		velocity: 0,
		power: 54.4803525719116,
		road: 13373.3564814815,
		acceleration: 0.281296296296296
	},
	{
		id: 1222,
		time: 1221,
		velocity: 0.843888888888889,
		power: 322.44284133662,
		road: 13373.8917592593,
		acceleration: 0.507962962962963
	},
	{
		id: 1223,
		time: 1222,
		velocity: 1.52388888888889,
		power: 574.431253313067,
		road: 13374.9135185185,
		acceleration: 0.465
	},
	{
		id: 1224,
		time: 1223,
		velocity: 1.395,
		power: 1507.04807398028,
		road: 13376.5800462963,
		acceleration: 0.824537037037037
	},
	{
		id: 1225,
		time: 1224,
		velocity: 3.3175,
		power: 1935.34604673844,
		road: 13379.0127314815,
		acceleration: 0.707777777777778
	},
	{
		id: 1226,
		time: 1225,
		velocity: 3.64722222222222,
		power: 3861.30145330941,
		road: 13382.3431481481,
		acceleration: 1.08768518518518
	},
	{
		id: 1227,
		time: 1226,
		velocity: 4.65805555555556,
		power: 2680.46736904447,
		road: 13386.4892592593,
		acceleration: 0.543703703703704
	},
	{
		id: 1228,
		time: 1227,
		velocity: 4.94861111111111,
		power: 2373.85094507784,
		road: 13391.1080555556,
		acceleration: 0.401666666666667
	},
	{
		id: 1229,
		time: 1228,
		velocity: 4.85222222222222,
		power: 1421.10025802145,
		road: 13396.0098148148,
		acceleration: 0.164259259259259
	},
	{
		id: 1230,
		time: 1229,
		velocity: 5.15083333333333,
		power: 3354.80791830729,
		road: 13401.2585648148,
		acceleration: 0.529722222222222
	},
	{
		id: 1231,
		time: 1230,
		velocity: 6.53777777777778,
		power: 6085.99645048932,
		road: 13407.2340740741,
		acceleration: 0.923796296296298
	},
	{
		id: 1232,
		time: 1231,
		velocity: 7.62361111111111,
		power: 7899.95210260616,
		road: 13414.1910185185,
		acceleration: 1.03907407407407
	},
	{
		id: 1233,
		time: 1232,
		velocity: 8.26805555555556,
		power: 6325.5998904814,
		road: 13422.0115277778,
		acceleration: 0.688055555555555
	},
	{
		id: 1234,
		time: 1233,
		velocity: 8.60194444444444,
		power: 4384.82634135807,
		road: 13430.368287037,
		acceleration: 0.384444444444446
	},
	{
		id: 1235,
		time: 1234,
		velocity: 8.77694444444444,
		power: 4514.628627085,
		road: 13439.1034722222,
		acceleration: 0.372407407407405
	},
	{
		id: 1236,
		time: 1235,
		velocity: 9.38527777777778,
		power: 6189.04212768271,
		road: 13448.2910185185,
		acceleration: 0.532314814814818
	},
	{
		id: 1237,
		time: 1236,
		velocity: 10.1988888888889,
		power: 6441.47641742915,
		road: 13458.0025462963,
		acceleration: 0.515648148148147
	},
	{
		id: 1238,
		time: 1237,
		velocity: 10.3238888888889,
		power: 4185.38790718587,
		road: 13468.097037037,
		acceleration: 0.250277777777779
	},
	{
		id: 1239,
		time: 1238,
		velocity: 10.1361111111111,
		power: 2207.52086431937,
		road: 13478.3365277778,
		acceleration: 0.0397222222222222
	},
	{
		id: 1240,
		time: 1239,
		velocity: 10.3180555555556,
		power: 2167.16039878516,
		road: 13488.6130555556,
		acceleration: 0.0343518518518522
	},
	{
		id: 1241,
		time: 1240,
		velocity: 10.4269444444444,
		power: 2973.17318169054,
		road: 13498.9635648148,
		acceleration: 0.11361111111111
	},
	{
		id: 1242,
		time: 1241,
		velocity: 10.4769444444444,
		power: 1867.47055630532,
		road: 13509.370787037,
		acceleration: -0.000185185185184622
	},
	{
		id: 1243,
		time: 1242,
		velocity: 10.3175,
		power: 463.917230036844,
		road: 13519.7076388889,
		acceleration: -0.140555555555556
	},
	{
		id: 1244,
		time: 1243,
		velocity: 10.0052777777778,
		power: 1021.97172135577,
		road: 13529.9334722222,
		acceleration: -0.0814814814814788
	},
	{
		id: 1245,
		time: 1244,
		velocity: 10.2325,
		power: 2987.58954699227,
		road: 13540.178287037,
		acceleration: 0.119444444444442
	},
	{
		id: 1246,
		time: 1245,
		velocity: 10.6758333333333,
		power: 4898.27263191234,
		road: 13550.6340740741,
		acceleration: 0.302500000000002
	},
	{
		id: 1247,
		time: 1246,
		velocity: 10.9127777777778,
		power: 3915.9965640693,
		road: 13561.337037037,
		acceleration: 0.191851851851849
	},
	{
		id: 1248,
		time: 1247,
		velocity: 10.8080555555556,
		power: 2030.9273780721,
		road: 13572.137962963,
		acceleration: 0.0040740740740759
	},
	{
		id: 1249,
		time: 1248,
		velocity: 10.6880555555556,
		power: 603.530080357751,
		road: 13582.874212963,
		acceleration: -0.133425925925927
	},
	{
		id: 1250,
		time: 1249,
		velocity: 10.5125,
		power: 913.435658125244,
		road: 13593.4933796296,
		acceleration: -0.100740740740742
	},
	{
		id: 1251,
		time: 1250,
		velocity: 10.5058333333333,
		power: 2964.19644945759,
		road: 13604.1131018518,
		acceleration: 0.101851851851851
	},
	{
		id: 1252,
		time: 1251,
		velocity: 10.9936111111111,
		power: 4371.43948283205,
		road: 13614.8998148148,
		acceleration: 0.232129629629632
	},
	{
		id: 1253,
		time: 1252,
		velocity: 11.2088888888889,
		power: 5753.89106299426,
		road: 13625.976712963,
		acceleration: 0.34824074074074
	},
	{
		id: 1254,
		time: 1253,
		velocity: 11.5505555555556,
		power: 4458.50933151864,
		road: 13637.3335185185,
		acceleration: 0.211574074074074
	},
	{
		id: 1255,
		time: 1254,
		velocity: 11.6283333333333,
		power: 4199.19119990734,
		road: 13648.8853703704,
		acceleration: 0.178518518518517
	},
	{
		id: 1256,
		time: 1255,
		velocity: 11.7444444444444,
		power: 3463.44691806847,
		road: 13660.5794907407,
		acceleration: 0.106018518518521
	},
	{
		id: 1257,
		time: 1256,
		velocity: 11.8686111111111,
		power: 3760.89474189503,
		road: 13672.3905092593,
		acceleration: 0.127777777777775
	},
	{
		id: 1258,
		time: 1257,
		velocity: 12.0116666666667,
		power: 3544.97480675747,
		road: 13684.3174074074,
		acceleration: 0.103981481481481
	},
	{
		id: 1259,
		time: 1258,
		velocity: 12.0563888888889,
		power: 4387.78315291577,
		road: 13696.3821759259,
		acceleration: 0.171759259259261
	},
	{
		id: 1260,
		time: 1259,
		velocity: 12.3838888888889,
		power: 4320.17416611,
		road: 13708.6120833333,
		acceleration: 0.15851851851852
	},
	{
		id: 1261,
		time: 1260,
		velocity: 12.4872222222222,
		power: 5134.86652629688,
		road: 13721.0307407407,
		acceleration: 0.21898148148148
	},
	{
		id: 1262,
		time: 1261,
		velocity: 12.7133333333333,
		power: 6583.40460952924,
		road: 13733.7215740741,
		acceleration: 0.32537037037037
	},
	{
		id: 1263,
		time: 1262,
		velocity: 13.36,
		power: 8715.94644586086,
		road: 13746.8118518518,
		acceleration: 0.473518518518519
	},
	{
		id: 1264,
		time: 1263,
		velocity: 13.9077777777778,
		power: 9545.33867315222,
		road: 13760.3912962963,
		acceleration: 0.504814814814814
	},
	{
		id: 1265,
		time: 1264,
		velocity: 14.2277777777778,
		power: 6404.43340752981,
		road: 13774.3448611111,
		acceleration: 0.243425925925926
	},
	{
		id: 1266,
		time: 1265,
		velocity: 14.0902777777778,
		power: 2956.42210792508,
		road: 13788.4106944444,
		acceleration: -0.0188888888888883
	},
	{
		id: 1267,
		time: 1266,
		velocity: 13.8511111111111,
		power: -257.873400990003,
		road: 13802.3389814815,
		acceleration: -0.256203703703704
	},
	{
		id: 1268,
		time: 1267,
		velocity: 13.4591666666667,
		power: -1179.11594988517,
		road: 13815.9777314815,
		acceleration: -0.322870370370373
	},
	{
		id: 1269,
		time: 1268,
		velocity: 13.1216666666667,
		power: -2615.70432011023,
		road: 13829.2384259259,
		acceleration: -0.433240740740738
	},
	{
		id: 1270,
		time: 1269,
		velocity: 12.5513888888889,
		power: -2410.53964805081,
		road: 13842.0739351852,
		acceleration: -0.417129629629633
	},
	{
		id: 1271,
		time: 1270,
		velocity: 12.2077777777778,
		power: -4060.98946773063,
		road: 13854.4218055555,
		acceleration: -0.558148148148145
	},
	{
		id: 1272,
		time: 1271,
		velocity: 11.4472222222222,
		power: -4607.25343447099,
		road: 13866.1825,
		acceleration: -0.616203703703704
	},
	{
		id: 1273,
		time: 1272,
		velocity: 10.7027777777778,
		power: -5320.66272983961,
		road: 13877.2852777778,
		acceleration: -0.69962962962963
	},
	{
		id: 1274,
		time: 1273,
		velocity: 10.1088888888889,
		power: -4760.03641690252,
		road: 13887.704212963,
		acceleration: -0.668055555555556
	},
	{
		id: 1275,
		time: 1274,
		velocity: 9.44305555555555,
		power: -3850.90851342726,
		road: 13897.4918981481,
		acceleration: -0.594444444444443
	},
	{
		id: 1276,
		time: 1275,
		velocity: 8.91944444444444,
		power: -3872.73757290678,
		road: 13906.6734259259,
		acceleration: -0.617870370370373
	},
	{
		id: 1277,
		time: 1276,
		velocity: 8.25527777777778,
		power: -3099.8943069009,
		road: 13915.2721296296,
		acceleration: -0.547777777777776
	},
	{
		id: 1278,
		time: 1277,
		velocity: 7.79972222222222,
		power: -3775.90856947597,
		road: 13923.2670833333,
		acceleration: -0.659722222222222
	},
	{
		id: 1279,
		time: 1278,
		velocity: 6.94027777777778,
		power: -2928.70453485904,
		road: 13930.6444907407,
		acceleration: -0.575370370370371
	},
	{
		id: 1280,
		time: 1279,
		velocity: 6.52916666666667,
		power: -1913.26808967084,
		road: 13937.5107407407,
		acceleration: -0.446944444444444
	},
	{
		id: 1281,
		time: 1280,
		velocity: 6.45888888888889,
		power: -1618.75716344192,
		road: 13943.9458796296,
		acceleration: -0.415277777777778
	},
	{
		id: 1282,
		time: 1281,
		velocity: 5.69444444444444,
		power: -1202.04835639102,
		road: 13949.9948611111,
		acceleration: -0.357037037037036
	},
	{
		id: 1283,
		time: 1282,
		velocity: 5.45805555555556,
		power: -1747.86866142002,
		road: 13955.6294907407,
		acceleration: -0.471666666666667
	},
	{
		id: 1284,
		time: 1283,
		velocity: 5.04388888888889,
		power: -899.600122869644,
		road: 13960.8664814815,
		acceleration: -0.323611111111112
	},
	{
		id: 1285,
		time: 1284,
		velocity: 4.72361111111111,
		power: -2286.8311016619,
		road: 13965.618287037,
		acceleration: -0.646759259259259
	},
	{
		id: 1286,
		time: 1285,
		velocity: 3.51777777777778,
		power: -3535.75091535981,
		road: 13969.4988888889,
		acceleration: -1.09564814814815
	},
	{
		id: 1287,
		time: 1286,
		velocity: 1.75694444444444,
		power: -3073.27907636553,
		road: 13972.1564351852,
		acceleration: -1.35046296296296
	},
	{
		id: 1288,
		time: 1287,
		velocity: 0.672222222222222,
		power: -1381.64422557993,
		road: 13973.5524537037,
		acceleration: -1.17259259259259
	},
	{
		id: 1289,
		time: 1288,
		velocity: 0,
		power: -224.323153144727,
		road: 13974.0693518518,
		acceleration: -0.585648148148148
	},
	{
		id: 1290,
		time: 1289,
		velocity: 0,
		power: -10.2472218973359,
		road: 13974.1813888889,
		acceleration: -0.224074074074074
	},
	{
		id: 1291,
		time: 1290,
		velocity: 0,
		power: 0,
		road: 13974.1813888889,
		acceleration: 0
	},
	{
		id: 1292,
		time: 1291,
		velocity: 0,
		power: 0,
		road: 13974.1813888889,
		acceleration: 0
	},
	{
		id: 1293,
		time: 1292,
		velocity: 0,
		power: 0,
		road: 13974.1813888889,
		acceleration: 0
	},
	{
		id: 1294,
		time: 1293,
		velocity: 0,
		power: 0,
		road: 13974.1813888889,
		acceleration: 0
	},
	{
		id: 1295,
		time: 1294,
		velocity: 0,
		power: 0,
		road: 13974.1813888889,
		acceleration: 0
	},
	{
		id: 1296,
		time: 1295,
		velocity: 0,
		power: 0,
		road: 13974.1813888889,
		acceleration: 0
	},
	{
		id: 1297,
		time: 1296,
		velocity: 0,
		power: 0,
		road: 13974.1813888889,
		acceleration: 0
	},
	{
		id: 1298,
		time: 1297,
		velocity: 0,
		power: 206.480947715792,
		road: 13974.4811111111,
		acceleration: 0.599444444444444
	},
	{
		id: 1299,
		time: 1298,
		velocity: 1.79833333333333,
		power: 1687.88093569535,
		road: 13975.7293981481,
		acceleration: 1.29768518518519
	},
	{
		id: 1300,
		time: 1299,
		velocity: 3.89305555555556,
		power: 5028.20824742844,
		road: 13978.5124537037,
		acceleration: 1.77185185185185
	},
	{
		id: 1301,
		time: 1300,
		velocity: 5.31555555555556,
		power: 6005.72379255552,
		road: 13982.8425462963,
		acceleration: 1.32222222222222
	},
	{
		id: 1302,
		time: 1301,
		velocity: 5.765,
		power: 3896.55830061093,
		road: 13988.1485185185,
		acceleration: 0.629537037037037
	},
	{
		id: 1303,
		time: 1302,
		velocity: 5.78166666666667,
		power: 3916.10405118712,
		road: 13994.0450925926,
		acceleration: 0.551666666666668
	},
	{
		id: 1304,
		time: 1303,
		velocity: 6.97055555555556,
		power: 6962.49251902044,
		road: 14000.692037037,
		acceleration: 0.949074074074074
	},
	{
		id: 1305,
		time: 1304,
		velocity: 8.61222222222222,
		power: 10431.0367242226,
		road: 14008.4401851852,
		acceleration: 1.25333333333333
	},
	{
		id: 1306,
		time: 1305,
		velocity: 9.54166666666667,
		power: 10032.6152920071,
		road: 14017.3221296296,
		acceleration: 1.01425925925926
	},
	{
		id: 1307,
		time: 1306,
		velocity: 10.0133333333333,
		power: 7029.63726684807,
		road: 14027.0022222222,
		acceleration: 0.582037037037038
	},
	{
		id: 1308,
		time: 1307,
		velocity: 10.3583333333333,
		power: 4579.70302521129,
		road: 14037.1183333333,
		acceleration: 0.289999999999999
	},
	{
		id: 1309,
		time: 1308,
		velocity: 10.4116666666667,
		power: 5002.424110867,
		road: 14047.537037037,
		acceleration: 0.315185185185186
	},
	{
		id: 1310,
		time: 1309,
		velocity: 10.9588888888889,
		power: 2853.44342078961,
		road: 14058.15875,
		acceleration: 0.0908333333333324
	},
	{
		id: 1311,
		time: 1310,
		velocity: 10.6308333333333,
		power: 1640.43604103561,
		road: 14068.8109722222,
		acceleration: -0.0298148148148147
	},
	{
		id: 1312,
		time: 1311,
		velocity: 10.3222222222222,
		power: 463.120612620547,
		road: 14079.3761111111,
		acceleration: -0.144351851851852
	},
	{
		id: 1313,
		time: 1312,
		velocity: 10.5258333333333,
		power: 2080.23025029874,
		road: 14089.8781944444,
		acceleration: 0.0182407407407421
	},
	{
		id: 1314,
		time: 1313,
		velocity: 10.6855555555556,
		power: 2350.44378086007,
		road: 14100.4114814815,
		acceleration: 0.0441666666666638
	},
	{
		id: 1315,
		time: 1314,
		velocity: 10.4547222222222,
		power: -145.340332796907,
		road: 14110.8650462963,
		acceleration: -0.20361111111111
	},
	{
		id: 1316,
		time: 1315,
		velocity: 9.915,
		power: -1864.10885977624,
		road: 14121.0277314815,
		acceleration: -0.378148148148147
	},
	{
		id: 1317,
		time: 1316,
		velocity: 9.55111111111111,
		power: -1253.16538293899,
		road: 14130.8434722222,
		acceleration: -0.315740740740742
	},
	{
		id: 1318,
		time: 1317,
		velocity: 9.5075,
		power: 139.57378835872,
		road: 14140.4194444444,
		acceleration: -0.163796296296297
	},
	{
		id: 1319,
		time: 1318,
		velocity: 9.42361111111111,
		power: 1371.48479294987,
		road: 14149.9004166667,
		acceleration: -0.0262037037037022
	},
	{
		id: 1320,
		time: 1319,
		velocity: 9.4725,
		power: 1404.87631693529,
		road: 14159.3573611111,
		acceleration: -0.0218518518518529
	},
	{
		id: 1321,
		time: 1320,
		velocity: 9.44194444444444,
		power: 1205.74495817562,
		road: 14168.7818055555,
		acceleration: -0.0431481481481466
	},
	{
		id: 1322,
		time: 1321,
		velocity: 9.29416666666667,
		power: 564.377133480126,
		road: 14178.1280555555,
		acceleration: -0.113240740740741
	},
	{
		id: 1323,
		time: 1322,
		velocity: 9.13277777777778,
		power: 900.929208553083,
		road: 14187.3809722222,
		acceleration: -0.0734259259259265
	},
	{
		id: 1324,
		time: 1323,
		velocity: 9.22166666666667,
		power: 2567.45881756688,
		road: 14196.6545833333,
		acceleration: 0.114814814814814
	},
	{
		id: 1325,
		time: 1324,
		velocity: 9.63861111111111,
		power: 6312.2023958936,
		road: 14206.2415740741,
		acceleration: 0.511944444444445
	},
	{
		id: 1326,
		time: 1325,
		velocity: 10.6686111111111,
		power: 14639.7546766209,
		road: 14216.7225462963,
		acceleration: 1.27601851851852
	},
	{
		id: 1327,
		time: 1326,
		velocity: 13.0497222222222,
		power: 17940.9302529031,
		road: 14228.5345833333,
		acceleration: 1.38611111111111
	},
	{
		id: 1328,
		time: 1327,
		velocity: 13.7969444444444,
		power: 15629.9430277472,
		road: 14241.5568518518,
		acceleration: 1.03435185185185
	},
	{
		id: 1329,
		time: 1328,
		velocity: 13.7716666666667,
		power: 6687.18589694607,
		road: 14255.2358333333,
		acceleration: 0.279074074074074
	},
	{
		id: 1330,
		time: 1329,
		velocity: 13.8869444444444,
		power: 3767.50854934655,
		road: 14269.0791203704,
		acceleration: 0.0495370370370392
	},
	{
		id: 1331,
		time: 1330,
		velocity: 13.9455555555556,
		power: 7147.99767867002,
		road: 14283.0950925926,
		acceleration: 0.295833333333333
	},
	{
		id: 1332,
		time: 1331,
		velocity: 14.6591666666667,
		power: 7047.69758582227,
		road: 14297.3956018518,
		acceleration: 0.273240740740741
	},
	{
		id: 1333,
		time: 1332,
		velocity: 14.7066666666667,
		power: 805.905226369051,
		road: 14311.7404166667,
		acceleration: -0.184629629629631
	},
	{
		id: 1334,
		time: 1333,
		velocity: 13.3916666666667,
		power: -8840.51769364417,
		road: 14325.54,
		acceleration: -0.905833333333334
	},
	{
		id: 1335,
		time: 1334,
		velocity: 11.9416666666667,
		power: -15003.5806323273,
		road: 14338.1547222222,
		acceleration: -1.46388888888889
	},
	{
		id: 1336,
		time: 1335,
		velocity: 10.315,
		power: -12901.0575647156,
		road: 14349.3331944444,
		acceleration: -1.40861111111111
	},
	{
		id: 1337,
		time: 1336,
		velocity: 9.16583333333333,
		power: -9695.82332001519,
		road: 14359.2004166667,
		acceleration: -1.21388888888889
	},
	{
		id: 1338,
		time: 1337,
		velocity: 8.3,
		power: -7576.75557151567,
		road: 14367.9191203704,
		acceleration: -1.08314814814815
	},
	{
		id: 1339,
		time: 1338,
		velocity: 7.06555555555556,
		power: -5270.33942596399,
		road: 14375.6577777778,
		acceleration: -0.876944444444444
	},
	{
		id: 1340,
		time: 1339,
		velocity: 6.535,
		power: -3428.14262123145,
		road: 14382.6218055555,
		acceleration: -0.672314814814815
	},
	{
		id: 1341,
		time: 1340,
		velocity: 6.28305555555556,
		power: -2144.62529841708,
		road: 14388.9976388889,
		acceleration: -0.504074074074075
	},
	{
		id: 1342,
		time: 1341,
		velocity: 5.55333333333333,
		power: -2777.38675914123,
		road: 14394.7962962963,
		acceleration: -0.650277777777777
	},
	{
		id: 1343,
		time: 1342,
		velocity: 4.58416666666667,
		power: -3084.42064652354,
		road: 14399.8794907407,
		acceleration: -0.780648148148147
	},
	{
		id: 1344,
		time: 1343,
		velocity: 3.94111111111111,
		power: -1518.19779729796,
		road: 14404.3232407407,
		acceleration: -0.498240740740741
	},
	{
		id: 1345,
		time: 1344,
		velocity: 4.05861111111111,
		power: -709.044477683818,
		road: 14408.357037037,
		acceleration: -0.321666666666667
	},
	{
		id: 1346,
		time: 1345,
		velocity: 3.61916666666667,
		power: 11.3251473043038,
		road: 14412.1637962963,
		acceleration: -0.132407407407408
	},
	{
		id: 1347,
		time: 1346,
		velocity: 3.54388888888889,
		power: -193.033128940945,
		road: 14415.8090277778,
		acceleration: -0.190648148148149
	},
	{
		id: 1348,
		time: 1347,
		velocity: 3.48666666666667,
		power: 361.45236880392,
		road: 14419.3455555556,
		acceleration: -0.0267592592592596
	},
	{
		id: 1349,
		time: 1348,
		velocity: 3.53888888888889,
		power: -338.562681311179,
		road: 14422.7493518518,
		acceleration: -0.238703703703703
	},
	{
		id: 1350,
		time: 1349,
		velocity: 2.82777777777778,
		power: -1829.06933183396,
		road: 14425.6335648148,
		acceleration: -0.800462962962963
	},
	{
		id: 1351,
		time: 1350,
		velocity: 1.08527777777778,
		power: -1886.21563872241,
		road: 14427.5277314815,
		acceleration: -1.17962962962963
	},
	{
		id: 1352,
		time: 1351,
		velocity: 0,
		power: -643.196792968135,
		road: 14428.360787037,
		acceleration: -0.942592592592592
	},
	{
		id: 1353,
		time: 1352,
		velocity: 0,
		power: -40.1374436809617,
		road: 14428.5416666667,
		acceleration: -0.361759259259259
	},
	{
		id: 1354,
		time: 1353,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1355,
		time: 1354,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1356,
		time: 1355,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1357,
		time: 1356,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1358,
		time: 1357,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1359,
		time: 1358,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1360,
		time: 1359,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1361,
		time: 1360,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1362,
		time: 1361,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1363,
		time: 1362,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1364,
		time: 1363,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1365,
		time: 1364,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1366,
		time: 1365,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1367,
		time: 1366,
		velocity: 0,
		power: 0,
		road: 14428.5416666667,
		acceleration: 0
	},
	{
		id: 1368,
		time: 1367,
		velocity: 0,
		power: 218.104780163706,
		road: 14428.8505092593,
		acceleration: 0.617685185185185
	},
	{
		id: 1369,
		time: 1368,
		velocity: 1.85305555555556,
		power: 1578.55827846791,
		road: 14430.0806944444,
		acceleration: 1.225
	},
	{
		id: 1370,
		time: 1369,
		velocity: 3.675,
		power: 4550.45621670112,
		road: 14432.7543518518,
		acceleration: 1.66194444444444
	},
	{
		id: 1371,
		time: 1370,
		velocity: 4.98583333333333,
		power: 4880.86519923328,
		road: 14436.8223148148,
		acceleration: 1.12666666666667
	},
	{
		id: 1372,
		time: 1371,
		velocity: 5.23305555555555,
		power: 3241.10460245804,
		road: 14441.7306481481,
		acceleration: 0.554074074074074
	},
	{
		id: 1373,
		time: 1372,
		velocity: 5.33722222222222,
		power: 2717.28166149423,
		road: 14447.1099537037,
		acceleration: 0.38787037037037
	},
	{
		id: 1374,
		time: 1373,
		velocity: 6.14944444444444,
		power: 4288.38068964507,
		road: 14452.9931481481,
		acceleration: 0.619907407407407
	},
	{
		id: 1375,
		time: 1374,
		velocity: 7.09277777777778,
		power: 6516.56327641894,
		road: 14459.6266666667,
		acceleration: 0.880740740740741
	},
	{
		id: 1376,
		time: 1375,
		velocity: 7.97944444444444,
		power: 7454.25781349332,
		road: 14467.1421296296,
		acceleration: 0.883148148148148
	},
	{
		id: 1377,
		time: 1376,
		velocity: 8.79888888888889,
		power: 5653.73411214768,
		road: 14475.3769907407,
		acceleration: 0.555648148148149
	},
	{
		id: 1378,
		time: 1377,
		velocity: 8.75972222222222,
		power: 2327.0254556163,
		road: 14483.9478703704,
		acceleration: 0.116388888888888
	},
	{
		id: 1379,
		time: 1378,
		velocity: 8.32861111111111,
		power: -1457.58502897209,
		road: 14492.4025462963,
		acceleration: -0.348796296296296
	},
	{
		id: 1380,
		time: 1379,
		velocity: 7.7525,
		power: -5935.29604935368,
		road: 14500.2020833333,
		acceleration: -0.961481481481482
	},
	{
		id: 1381,
		time: 1380,
		velocity: 5.87527777777778,
		power: -7531.68501489991,
		road: 14506.8488425926,
		acceleration: -1.34407407407407
	},
	{
		id: 1382,
		time: 1381,
		velocity: 4.29638888888889,
		power: -5414.41573438374,
		road: 14512.2213888889,
		acceleration: -1.20435185185185
	},
	{
		id: 1383,
		time: 1382,
		velocity: 4.13944444444444,
		power: -2061.20914434125,
		road: 14516.6790277778,
		acceleration: -0.625462962962962
	},
	{
		id: 1384,
		time: 1383,
		velocity: 3.99888888888889,
		power: 29.3942387894207,
		road: 14520.7593518518,
		acceleration: -0.129166666666668
	},
	{
		id: 1385,
		time: 1384,
		velocity: 3.90888888888889,
		power: 833.955984584925,
		road: 14524.8150462963,
		acceleration: 0.0799074074074078
	},
	{
		id: 1386,
		time: 1385,
		velocity: 4.37916666666667,
		power: 1983.81872096279,
		road: 14529.0863888889,
		acceleration: 0.35138888888889
	},
	{
		id: 1387,
		time: 1386,
		velocity: 5.05305555555556,
		power: 4356.47070251652,
		road: 14533.9358796296,
		acceleration: 0.804907407407407
	},
	{
		id: 1388,
		time: 1387,
		velocity: 6.32361111111111,
		power: 6652.46982739385,
		road: 14539.7196759259,
		acceleration: 1.0637037037037
	},
	{
		id: 1389,
		time: 1388,
		velocity: 7.57027777777778,
		power: 9723.39171946143,
		road: 14546.6910185185,
		acceleration: 1.31138888888889
	},
	{
		id: 1390,
		time: 1389,
		velocity: 8.98722222222222,
		power: 11406.8068508954,
		road: 14554.9596296296,
		acceleration: 1.28314814814815
	},
	{
		id: 1391,
		time: 1390,
		velocity: 10.1730555555556,
		power: 11949.6812489004,
		road: 14564.4421759259,
		acceleration: 1.14472222222222
	},
	{
		id: 1392,
		time: 1391,
		velocity: 11.0044444444444,
		power: 8244.68823325504,
		road: 14574.8198148148,
		acceleration: 0.645462962962963
	},
	{
		id: 1393,
		time: 1392,
		velocity: 10.9236111111111,
		power: 3628.75519396498,
		road: 14585.6002777778,
		acceleration: 0.160185185185187
	},
	{
		id: 1394,
		time: 1393,
		velocity: 10.6536111111111,
		power: 373.013951135307,
		road: 14596.3825,
		acceleration: -0.156666666666666
	},
	{
		id: 1395,
		time: 1394,
		velocity: 10.5344444444444,
		power: 1000.41575001813,
		road: 14607.0399074074,
		acceleration: -0.0929629629629645
	},
	{
		id: 1396,
		time: 1395,
		velocity: 10.6447222222222,
		power: 3775.73597303912,
		road: 14617.7399537037,
		acceleration: 0.17824074074074
	},
	{
		id: 1397,
		time: 1396,
		velocity: 11.1883333333333,
		power: 5818.53309452636,
		road: 14628.7096759259,
		acceleration: 0.361111111111109
	},
	{
		id: 1398,
		time: 1397,
		velocity: 11.6177777777778,
		power: 6192.5498036123,
		road: 14640.0464351852,
		acceleration: 0.372962962962964
	},
	{
		id: 1399,
		time: 1398,
		velocity: 11.7636111111111,
		power: 3327.83705481477,
		road: 14651.6189814815,
		acceleration: 0.0986111111111114
	},
	{
		id: 1400,
		time: 1399,
		velocity: 11.4841666666667,
		power: 1027.34176279475,
		road: 14663.1859722222,
		acceleration: -0.109722222222222
	},
	{
		id: 1401,
		time: 1400,
		velocity: 11.2886111111111,
		power: 1131.84587933104,
		road: 14674.6491203704,
		acceleration: -0.0979629629629617
	},
	{
		id: 1402,
		time: 1401,
		velocity: 11.4697222222222,
		power: 3451.17541213521,
		road: 14686.1202314815,
		acceleration: 0.113888888888889
	},
	{
		id: 1403,
		time: 1402,
		velocity: 11.8258333333333,
		power: 5332.95649237411,
		road: 14697.7858796296,
		acceleration: 0.275185185185185
	},
	{
		id: 1404,
		time: 1403,
		velocity: 12.1141666666667,
		power: 6094.06826687369,
		road: 14709.7519907407,
		acceleration: 0.325740740740741
	},
	{
		id: 1405,
		time: 1404,
		velocity: 12.4469444444444,
		power: 5770.21214373234,
		road: 14722.0212962963,
		acceleration: 0.280648148148147
	},
	{
		id: 1406,
		time: 1405,
		velocity: 12.6677777777778,
		power: 6289.93605408292,
		road: 14734.5850462963,
		acceleration: 0.308240740740739
	},
	{
		id: 1407,
		time: 1406,
		velocity: 13.0388888888889,
		power: 5577.36094437154,
		road: 14747.4204166667,
		acceleration: 0.235000000000001
	},
	{
		id: 1408,
		time: 1407,
		velocity: 13.1519444444444,
		power: 4058.24843655047,
		road: 14760.4253703704,
		acceleration: 0.104166666666666
	},
	{
		id: 1409,
		time: 1408,
		velocity: 12.9802777777778,
		power: 1678.32832911393,
		road: 14773.4385185185,
		acceleration: -0.0877777777777773
	},
	{
		id: 1410,
		time: 1409,
		velocity: 12.7755555555556,
		power: -38.7270803350188,
		road: 14786.2958796296,
		acceleration: -0.223796296296296
	},
	{
		id: 1411,
		time: 1410,
		velocity: 12.4805555555556,
		power: 336.173050774198,
		road: 14798.9464351852,
		acceleration: -0.189814814814817
	},
	{
		id: 1412,
		time: 1411,
		velocity: 12.4108333333333,
		power: 1706.15932629397,
		road: 14811.4656018518,
		acceleration: -0.0729629629629613
	},
	{
		id: 1413,
		time: 1412,
		velocity: 12.5566666666667,
		power: 3565.39269192429,
		road: 14823.989537037,
		acceleration: 0.0825000000000014
	},
	{
		id: 1414,
		time: 1413,
		velocity: 12.7280555555556,
		power: 4967.80340529509,
		road: 14836.6513888889,
		acceleration: 0.193333333333333
	},
	{
		id: 1415,
		time: 1414,
		velocity: 12.9908333333333,
		power: 5236.54659861973,
		road: 14849.5128703704,
		acceleration: 0.205925925925925
	},
	{
		id: 1416,
		time: 1415,
		velocity: 13.1744444444444,
		power: 5680.53069552162,
		road: 14862.5928703704,
		acceleration: 0.231111111111112
	},
	{
		id: 1417,
		time: 1416,
		velocity: 13.4213888888889,
		power: 5555.45614587268,
		road: 14875.8936111111,
		acceleration: 0.21037037037037
	},
	{
		id: 1418,
		time: 1417,
		velocity: 13.6219444444444,
		power: 4785.82495199378,
		road: 14889.3706018518,
		acceleration: 0.142129629629629
	},
	{
		id: 1419,
		time: 1418,
		velocity: 13.6008333333333,
		power: 3666.04251345404,
		road: 14902.9444444444,
		acceleration: 0.0515740740740753
	},
	{
		id: 1420,
		time: 1419,
		velocity: 13.5761111111111,
		power: 2638.71757848132,
		road: 14916.5300462963,
		acceleration: -0.0280555555555573
	},
	{
		id: 1421,
		time: 1420,
		velocity: 13.5377777777778,
		power: 2935.76018826188,
		road: 14930.0993055555,
		acceleration: -0.00462962962962976
	},
	{
		id: 1422,
		time: 1421,
		velocity: 13.5869444444444,
		power: 2526.1437388999,
		road: 14943.6484259259,
		acceleration: -0.0356481481481481
	},
	{
		id: 1423,
		time: 1422,
		velocity: 13.4691666666667,
		power: 1433.15372225818,
		road: 14957.1205555555,
		acceleration: -0.118333333333332
	},
	{
		id: 1424,
		time: 1423,
		velocity: 13.1827777777778,
		power: 1269.9360560222,
		road: 14970.4693981481,
		acceleration: -0.128240740740742
	},
	{
		id: 1425,
		time: 1424,
		velocity: 13.2022222222222,
		power: 1468.68523918906,
		road: 14983.699212963,
		acceleration: -0.109814814814813
	},
	{
		id: 1426,
		time: 1425,
		velocity: 13.1397222222222,
		power: 2305.23281019244,
		road: 14996.8534259259,
		acceleration: -0.0413888888888891
	},
	{
		id: 1427,
		time: 1426,
		velocity: 13.0586111111111,
		power: 1769.11425068537,
		road: 15009.9456944444,
		acceleration: -0.0825000000000014
	},
	{
		id: 1428,
		time: 1427,
		velocity: 12.9547222222222,
		power: 2411.36335194853,
		road: 15022.9819907407,
		acceleration: -0.0294444444444437
	},
	{
		id: 1429,
		time: 1428,
		velocity: 13.0513888888889,
		power: 2268.74908460583,
		road: 15035.9836111111,
		acceleration: -0.0399074074074068
	},
	{
		id: 1430,
		time: 1429,
		velocity: 12.9388888888889,
		power: 1119.85095974551,
		road: 15048.8999537037,
		acceleration: -0.130648148148151
	},
	{
		id: 1431,
		time: 1430,
		velocity: 12.5627777777778,
		power: -552.496752704723,
		road: 15061.6188888889,
		acceleration: -0.264166666666664
	},
	{
		id: 1432,
		time: 1431,
		velocity: 12.2588888888889,
		power: -1254.07976182975,
		road: 15074.0456018518,
		acceleration: -0.320277777777779
	},
	{
		id: 1433,
		time: 1432,
		velocity: 11.9780555555556,
		power: -290.838053410441,
		road: 15086.1943055555,
		acceleration: -0.23574074074074
	},
	{
		id: 1434,
		time: 1433,
		velocity: 11.8555555555556,
		power: 550.871593697727,
		road: 15098.1453240741,
		acceleration: -0.159629629629629
	},
	{
		id: 1435,
		time: 1434,
		velocity: 11.78,
		power: 1158.45749880362,
		road: 15109.9648148148,
		acceleration: -0.103425925925926
	},
	{
		id: 1436,
		time: 1435,
		velocity: 11.6677777777778,
		power: 1502.64289635112,
		road: 15121.6972222222,
		acceleration: -0.0707407407407423
	},
	{
		id: 1437,
		time: 1436,
		velocity: 11.6433333333333,
		power: 1818.20045569761,
		road: 15133.37375,
		acceleration: -0.0410185185185163
	},
	{
		id: 1438,
		time: 1437,
		velocity: 11.6569444444444,
		power: 2169.75082104023,
		road: 15145.0254166667,
		acceleration: -0.00870370370370566
	},
	{
		id: 1439,
		time: 1438,
		velocity: 11.6416666666667,
		power: 2419.63264955773,
		road: 15156.6795833333,
		acceleration: 0.0137037037037029
	},
	{
		id: 1440,
		time: 1439,
		velocity: 11.6844444444444,
		power: 2410.89552840012,
		road: 15168.3468518518,
		acceleration: 0.0125000000000011
	},
	{
		id: 1441,
		time: 1440,
		velocity: 11.6944444444444,
		power: 2279.18163561466,
		road: 15180.0206018518,
		acceleration: 0.000462962962963331
	},
	{
		id: 1442,
		time: 1441,
		velocity: 11.6430555555556,
		power: 2071.1935756146,
		road: 15191.6856018518,
		acceleration: -0.0179629629629652
	},
	{
		id: 1443,
		time: 1442,
		velocity: 11.6305555555556,
		power: 2169.75082104027,
		road: 15203.3372685185,
		acceleration: -0.0087037037037021
	},
	{
		id: 1444,
		time: 1443,
		velocity: 11.6683333333333,
		power: 3112.57594973932,
		road: 15215.022037037,
		acceleration: 0.074907407407407
	},
	{
		id: 1445,
		time: 1444,
		velocity: 11.8677777777778,
		power: 3188.21489683447,
		road: 15226.7836574074,
		acceleration: 0.0787962962962965
	},
	{
		id: 1446,
		time: 1445,
		velocity: 11.8669444444444,
		power: 2936.34855050162,
		road: 15238.6116666667,
		acceleration: 0.0539814814814807
	},
	{
		id: 1447,
		time: 1446,
		velocity: 11.8302777777778,
		power: 1876.15158726966,
		road: 15250.4465740741,
		acceleration: -0.0401851851851838
	},
	{
		id: 1448,
		time: 1447,
		velocity: 11.7472222222222,
		power: 1690.65572546449,
		road: 15262.2337037037,
		acceleration: -0.055370370370369
	},
	{
		id: 1449,
		time: 1448,
		velocity: 11.7008333333333,
		power: 2102.28227855074,
		road: 15273.9843055555,
		acceleration: -0.0176851851851865
	},
	{
		id: 1450,
		time: 1449,
		velocity: 11.7772222222222,
		power: 2925.37672213601,
		road: 15285.7536111111,
		acceleration: 0.0550925925925903
	},
	{
		id: 1451,
		time: 1450,
		velocity: 11.9125,
		power: 2983.57577781734,
		road: 15297.5795833333,
		acceleration: 0.058240740740743
	},
	{
		id: 1452,
		time: 1451,
		velocity: 11.8755555555556,
		power: 2811.81851003756,
		road: 15309.4553240741,
		acceleration: 0.0412962962962951
	},
	{
		id: 1453,
		time: 1452,
		velocity: 11.9011111111111,
		power: 2261.69532878194,
		road: 15321.3478240741,
		acceleration: -0.00777777777777722
	},
	{
		id: 1454,
		time: 1453,
		velocity: 11.8891666666667,
		power: 1984.39832606716,
		road: 15333.2206018518,
		acceleration: -0.0316666666666663
	},
	{
		id: 1455,
		time: 1454,
		velocity: 11.7805555555556,
		power: 2662.16799273375,
		road: 15345.0916666667,
		acceleration: 0.0282407407407419
	},
	{
		id: 1456,
		time: 1455,
		velocity: 11.9858333333333,
		power: 1750.69416329307,
		road: 15356.9508796296,
		acceleration: -0.0519444444444463
	},
	{
		id: 1457,
		time: 1456,
		velocity: 11.7333333333333,
		power: 2056.72890739784,
		road: 15368.7722222222,
		acceleration: -0.023796296296295
	},
	{
		id: 1458,
		time: 1457,
		velocity: 11.7091666666667,
		power: 1801.90854376284,
		road: 15380.5589351852,
		acceleration: -0.0454629629629633
	},
	{
		id: 1459,
		time: 1458,
		velocity: 11.8494444444444,
		power: 3107.13021162018,
		road: 15392.3580092593,
		acceleration: 0.0701851851851849
	},
	{
		id: 1460,
		time: 1459,
		velocity: 11.9438888888889,
		power: 3017.89563886628,
		road: 15404.2221296296,
		acceleration: 0.0599074074074082
	},
	{
		id: 1461,
		time: 1460,
		velocity: 11.8888888888889,
		power: 1970.19003791807,
		road: 15416.0996759259,
		acceleration: -0.0330555555555563
	},
	{
		id: 1462,
		time: 1461,
		velocity: 11.7502777777778,
		power: 1243.46655977651,
		road: 15427.9128240741,
		acceleration: -0.0957407407407409
	},
	{
		id: 1463,
		time: 1462,
		velocity: 11.6566666666667,
		power: 805.393879669199,
		road: 15439.6118981481,
		acceleration: -0.132407407407408
	},
	{
		id: 1464,
		time: 1463,
		velocity: 11.4916666666667,
		power: 1411.71402364316,
		road: 15451.2069907407,
		acceleration: -0.0755555555555549
	},
	{
		id: 1465,
		time: 1464,
		velocity: 11.5236111111111,
		power: 1733.84124942497,
		road: 15462.7418981481,
		acceleration: -0.0448148148148171
	},
	{
		id: 1466,
		time: 1465,
		velocity: 11.5222222222222,
		power: 2136.70675814454,
		road: 15474.2506944444,
		acceleration: -0.0074074074074062
	},
	{
		id: 1467,
		time: 1466,
		velocity: 11.4694444444444,
		power: 2239.45045326283,
		road: 15485.7568055556,
		acceleration: 0.00203703703703795
	},
	{
		id: 1468,
		time: 1467,
		velocity: 11.5297222222222,
		power: 1887.74700447125,
		road: 15497.2491203704,
		acceleration: -0.0296296296296301
	},
	{
		id: 1469,
		time: 1468,
		velocity: 11.4333333333333,
		power: 2201.315411704,
		road: 15508.7263425926,
		acceleration: -0.000555555555555642
	},
	{
		id: 1470,
		time: 1469,
		velocity: 11.4677777777778,
		power: 1217.27870537222,
		road: 15520.1585648148,
		acceleration: -0.089444444444446
	},
	{
		id: 1471,
		time: 1470,
		velocity: 11.2613888888889,
		power: 1832.90576154652,
		road: 15531.5304166667,
		acceleration: -0.0312962962962935
	},
	{
		id: 1472,
		time: 1471,
		velocity: 11.3394444444444,
		power: 1541.60416996547,
		road: 15542.8581018519,
		acceleration: -0.0570370370370377
	},
	{
		id: 1473,
		time: 1472,
		velocity: 11.2966666666667,
		power: 2188.7928485919,
		road: 15554.1591203704,
		acceleration: 0.00370370370370487
	},
	{
		id: 1474,
		time: 1473,
		velocity: 11.2725,
		power: 1238.03523802788,
		road: 15565.4201851852,
		acceleration: -0.0836111111111109
	},
	{
		id: 1475,
		time: 1474,
		velocity: 11.0886111111111,
		power: -395.276018603443,
		road: 15576.5223148148,
		acceleration: -0.234259259259261
	},
	{
		id: 1476,
		time: 1475,
		velocity: 10.5938888888889,
		power: -3043.96343416327,
		road: 15587.2624074074,
		acceleration: -0.489814814814816
	},
	{
		id: 1477,
		time: 1476,
		velocity: 9.80305555555555,
		power: -4174.65940916958,
		road: 15597.4495833333,
		acceleration: -0.616018518518519
	},
	{
		id: 1478,
		time: 1477,
		velocity: 9.24055555555555,
		power: -3094.82483229927,
		road: 15607.0700925926,
		acceleration: -0.517314814814814
	},
	{
		id: 1479,
		time: 1478,
		velocity: 9.04194444444444,
		power: -1076.65083215175,
		road: 15616.282962963,
		acceleration: -0.297962962962963
	},
	{
		id: 1480,
		time: 1479,
		velocity: 8.90916666666667,
		power: -168.822791731107,
		road: 15625.2506018519,
		acceleration: -0.192499999999999
	},
	{
		id: 1481,
		time: 1480,
		velocity: 8.66305555555556,
		power: 4.51503271901894,
		road: 15634.0368055556,
		acceleration: -0.170370370370373
	},
	{
		id: 1482,
		time: 1481,
		velocity: 8.53083333333333,
		power: -51.2626521067535,
		road: 15642.6500925926,
		acceleration: -0.175462962962962
	},
	{
		id: 1483,
		time: 1482,
		velocity: 8.38277777777778,
		power: -202.560827860684,
		road: 15651.0793055556,
		acceleration: -0.192685185185184
	},
	{
		id: 1484,
		time: 1483,
		velocity: 8.085,
		power: -112.302512375609,
		road: 15659.3221759259,
		acceleration: -0.18
	},
	{
		id: 1485,
		time: 1484,
		velocity: 7.99083333333333,
		power: 965.724536398889,
		road: 15667.4550925926,
		acceleration: -0.0399074074074086
	},
	{
		id: 1486,
		time: 1485,
		velocity: 8.26305555555555,
		power: 2328.58316596263,
		road: 15675.6350462963,
		acceleration: 0.133981481481483
	},
	{
		id: 1487,
		time: 1486,
		velocity: 8.48694444444445,
		power: 2464.53062435539,
		road: 15683.9543981481,
		acceleration: 0.144814814814815
	},
	{
		id: 1488,
		time: 1487,
		velocity: 8.42527777777778,
		power: 3075.9559470217,
		road: 15692.4522222222,
		acceleration: 0.212129629629629
	},
	{
		id: 1489,
		time: 1488,
		velocity: 8.89944444444444,
		power: 2978.04106210366,
		road: 15701.1508796296,
		acceleration: 0.189537037037036
	},
	{
		id: 1490,
		time: 1489,
		velocity: 9.05555555555556,
		power: 3929.5015293546,
		road: 15710.0889351852,
		acceleration: 0.289259259259259
	},
	{
		id: 1491,
		time: 1490,
		velocity: 9.29305555555555,
		power: 3751.14227432624,
		road: 15719.2978703704,
		acceleration: 0.2525
	},
	{
		id: 1492,
		time: 1491,
		velocity: 9.65694444444445,
		power: 4166.44835160442,
		road: 15728.7748148148,
		acceleration: 0.28351851851852
	},
	{
		id: 1493,
		time: 1492,
		velocity: 9.90611111111111,
		power: 3842.98043610071,
		road: 15738.5102777778,
		acceleration: 0.233518518518519
	},
	{
		id: 1494,
		time: 1493,
		velocity: 9.99361111111111,
		power: 2489.76599987761,
		road: 15748.403287037,
		acceleration: 0.0815740740740729
	},
	{
		id: 1495,
		time: 1494,
		velocity: 9.90166666666667,
		power: 1593.69794448755,
		road: 15758.3298611111,
		acceleration: -0.0144444444444449
	},
	{
		id: 1496,
		time: 1495,
		velocity: 9.86277777777778,
		power: 844.353545693853,
		road: 15768.2029166667,
		acceleration: -0.0925925925925917
	},
	{
		id: 1497,
		time: 1496,
		velocity: 9.71583333333333,
		power: 864.29708565899,
		road: 15777.9853703704,
		acceleration: -0.0886111111111099
	},
	{
		id: 1498,
		time: 1497,
		velocity: 9.63583333333333,
		power: 590.387127293906,
		road: 15787.6654166667,
		acceleration: -0.116203703703706
	},
	{
		id: 1499,
		time: 1498,
		velocity: 9.51416666666667,
		power: 632.190325260156,
		road: 15797.2325462963,
		acceleration: -0.109629629629628
	},
	{
		id: 1500,
		time: 1499,
		velocity: 9.38694444444444,
		power: 575.596938304196,
		road: 15806.6879166667,
		acceleration: -0.113888888888889
	},
	{
		id: 1501,
		time: 1500,
		velocity: 9.29416666666667,
		power: 795.020397336815,
		road: 15816.0425925926,
		acceleration: -0.0874999999999986
	},
	{
		id: 1502,
		time: 1501,
		velocity: 9.25166666666667,
		power: 1003.41237031396,
		road: 15825.3223148148,
		acceleration: -0.0624074074074095
	},
	{
		id: 1503,
		time: 1502,
		velocity: 9.19972222222222,
		power: 1096.1948338768,
		road: 15834.5455555556,
		acceleration: -0.0505555555555564
	},
	{
		id: 1504,
		time: 1503,
		velocity: 9.1425,
		power: 678.150922148854,
		road: 15843.6951388889,
		acceleration: -0.0967592592592581
	},
	{
		id: 1505,
		time: 1504,
		velocity: 8.96138888888889,
		power: 1034.67348942953,
		road: 15852.7693055556,
		acceleration: -0.0540740740740748
	},
	{
		id: 1506,
		time: 1505,
		velocity: 9.0375,
		power: 605.635700080172,
		road: 15861.7652777778,
		acceleration: -0.102314814814815
	},
	{
		id: 1507,
		time: 1506,
		velocity: 8.83555555555555,
		power: 1212.74789379225,
		road: 15870.6952314815,
		acceleration: -0.0297222222222207
	},
	{
		id: 1508,
		time: 1507,
		velocity: 8.87222222222222,
		power: 992.614410840984,
		road: 15879.5830092593,
		acceleration: -0.0546296296296305
	},
	{
		id: 1509,
		time: 1508,
		velocity: 8.87361111111111,
		power: 2238.56047776241,
		road: 15888.4893981481,
		acceleration: 0.0918518518518514
	},
	{
		id: 1510,
		time: 1509,
		velocity: 9.11111111111111,
		power: 2903.40282460634,
		road: 15897.52375,
		acceleration: 0.164074074074076
	},
	{
		id: 1511,
		time: 1510,
		velocity: 9.36444444444444,
		power: 3429.18784327786,
		road: 15906.7476388889,
		acceleration: 0.215
	},
	{
		id: 1512,
		time: 1511,
		velocity: 9.51861111111111,
		power: 3062.98117757024,
		road: 15916.1611574074,
		acceleration: 0.164259259259257
	},
	{
		id: 1513,
		time: 1512,
		velocity: 9.60388888888889,
		power: 2425.74893433051,
		road: 15925.7009259259,
		acceleration: 0.0882407407407406
	},
	{
		id: 1514,
		time: 1513,
		velocity: 9.62916666666667,
		power: 1862.5440793827,
		road: 15935.297037037,
		acceleration: 0.0244444444444447
	},
	{
		id: 1515,
		time: 1514,
		velocity: 9.59194444444444,
		power: 1063.77763016577,
		road: 15944.8741203704,
		acceleration: -0.0625
	},
	{
		id: 1516,
		time: 1515,
		velocity: 9.41638888888889,
		power: 430.754261021137,
		road: 15954.3547685185,
		acceleration: -0.13037037037037
	},
	{
		id: 1517,
		time: 1516,
		velocity: 9.23805555555555,
		power: -658.335431859682,
		road: 15963.645,
		acceleration: -0.250462962962963
	},
	{
		id: 1518,
		time: 1517,
		velocity: 8.84055555555556,
		power: -465.531781731218,
		road: 15972.6962037037,
		acceleration: -0.227592592592591
	},
	{
		id: 1519,
		time: 1518,
		velocity: 8.73361111111111,
		power: -314.707305286936,
		road: 15981.529212963,
		acceleration: -0.208796296296295
	},
	{
		id: 1520,
		time: 1519,
		velocity: 8.61166666666667,
		power: 214.481264057265,
		road: 15990.1860185185,
		acceleration: -0.143611111111113
	},
	{
		id: 1521,
		time: 1520,
		velocity: 8.40972222222222,
		power: -391.313925321076,
		road: 15998.6628240741,
		acceleration: -0.21638888888889
	},
	{
		id: 1522,
		time: 1521,
		velocity: 8.08444444444444,
		power: -376.207390238562,
		road: 16006.9245833333,
		acceleration: -0.213703703703702
	},
	{
		id: 1523,
		time: 1522,
		velocity: 7.97055555555556,
		power: -1934.3111353959,
		road: 16014.8700925926,
		acceleration: -0.418796296296296
	},
	{
		id: 1524,
		time: 1523,
		velocity: 7.15333333333333,
		power: -3587.79158205394,
		road: 16022.2723148148,
		acceleration: -0.667777777777778
	},
	{
		id: 1525,
		time: 1524,
		velocity: 6.08111111111111,
		power: -4109.36572948554,
		road: 16028.9403703704,
		acceleration: -0.800555555555555
	},
	{
		id: 1526,
		time: 1525,
		velocity: 5.56888888888889,
		power: -3923.89017041857,
		road: 16034.7814351852,
		acceleration: -0.853425925925926
	},
	{
		id: 1527,
		time: 1526,
		velocity: 4.59305555555556,
		power: -3390.77836964979,
		road: 16039.7671296296,
		acceleration: -0.857314814814814
	},
	{
		id: 1528,
		time: 1527,
		velocity: 3.50916666666667,
		power: -3482.57047835879,
		road: 16043.80125,
		acceleration: -1.04583333333333
	},
	{
		id: 1529,
		time: 1528,
		velocity: 2.43138888888889,
		power: -2508.46887801038,
		road: 16046.8063425926,
		acceleration: -1.01222222222222
	},
	{
		id: 1530,
		time: 1529,
		velocity: 1.55638888888889,
		power: -1448.51829697638,
		road: 16048.8704166667,
		acceleration: -0.869814814814815
	},
	{
		id: 1531,
		time: 1530,
		velocity: 0.899722222222222,
		power: -717.676559162269,
		road: 16050.1363888889,
		acceleration: -0.726388888888889
	},
	{
		id: 1532,
		time: 1531,
		velocity: 0.252222222222222,
		power: -238.432174652715,
		road: 16050.7797685185,
		acceleration: -0.518796296296296
	},
	{
		id: 1533,
		time: 1532,
		velocity: 0,
		power: -38.2169876974237,
		road: 16051.0137962963,
		acceleration: -0.299907407407407
	},
	{
		id: 1534,
		time: 1533,
		velocity: 0,
		power: 1.73061319038337,
		road: 16051.0558333333,
		acceleration: -0.0840740740740741
	},
	{
		id: 1535,
		time: 1534,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1536,
		time: 1535,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1537,
		time: 1536,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1538,
		time: 1537,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1539,
		time: 1538,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1540,
		time: 1539,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1541,
		time: 1540,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1542,
		time: 1541,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1543,
		time: 1542,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1544,
		time: 1543,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1545,
		time: 1544,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1546,
		time: 1545,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1547,
		time: 1546,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1548,
		time: 1547,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1549,
		time: 1548,
		velocity: 0,
		power: 0,
		road: 16051.0558333333,
		acceleration: 0
	},
	{
		id: 1550,
		time: 1549,
		velocity: 0,
		power: 74.4244775243987,
		road: 16051.2246759259,
		acceleration: 0.337685185185185
	},
	{
		id: 1551,
		time: 1550,
		velocity: 1.01305555555556,
		power: 945.253055630225,
		road: 16052.080787037,
		acceleration: 1.03685185185185
	},
	{
		id: 1552,
		time: 1551,
		velocity: 3.11055555555556,
		power: 3239.13841356243,
		road: 16054.197037037,
		acceleration: 1.48342592592593
	},
	{
		id: 1553,
		time: 1552,
		velocity: 4.45027777777778,
		power: 4661.34571569076,
		road: 16057.6905555556,
		acceleration: 1.27111111111111
	},
	{
		id: 1554,
		time: 1553,
		velocity: 4.82638888888889,
		power: 3021.58725606736,
		road: 16062.1103240741,
		acceleration: 0.581388888888889
	},
	{
		id: 1555,
		time: 1554,
		velocity: 4.85472222222222,
		power: 2794.79430767756,
		road: 16067.0481018519,
		acceleration: 0.45462962962963
	},
	{
		id: 1556,
		time: 1555,
		velocity: 5.81416666666667,
		power: 4666.04888245218,
		road: 16072.5842592593,
		acceleration: 0.74212962962963
	},
	{
		id: 1557,
		time: 1556,
		velocity: 7.05277777777778,
		power: 8260.19237574299,
		road: 16079.0840277778,
		acceleration: 1.18509259259259
	},
	{
		id: 1558,
		time: 1557,
		velocity: 8.41,
		power: 10930.419075301,
		road: 16086.8364351852,
		acceleration: 1.32018518518518
	},
	{
		id: 1559,
		time: 1558,
		velocity: 9.77472222222222,
		power: 12164.1395294591,
		road: 16095.869212963,
		acceleration: 1.24055555555556
	},
	{
		id: 1560,
		time: 1559,
		velocity: 10.7744444444444,
		power: 9558.35685818995,
		road: 16105.9286111111,
		acceleration: 0.812685185185185
	},
	{
		id: 1561,
		time: 1560,
		velocity: 10.8480555555556,
		power: 5539.97070453984,
		road: 16116.5717592593,
		acceleration: 0.354814814814816
	},
	{
		id: 1562,
		time: 1561,
		velocity: 10.8391666666667,
		power: 2912.03123843718,
		road: 16127.4359259259,
		acceleration: 0.0872222222222199
	},
	{
		id: 1563,
		time: 1562,
		velocity: 11.0361111111111,
		power: 5391.56044513,
		road: 16138.5010185185,
		acceleration: 0.314629629629632
	},
	{
		id: 1564,
		time: 1563,
		velocity: 11.7919444444444,
		power: 7273.28331025,
		road: 16149.9556481482,
		acceleration: 0.464444444444442
	},
	{
		id: 1565,
		time: 1564,
		velocity: 12.2325,
		power: 8666.79896427486,
		road: 16161.9181944445,
		acceleration: 0.551388888888891
	},
	{
		id: 1566,
		time: 1565,
		velocity: 12.6902777777778,
		power: 6287.61826663665,
		road: 16174.3151851852,
		acceleration: 0.317499999999999
	},
	{
		id: 1567,
		time: 1566,
		velocity: 12.7444444444444,
		power: 4041.76748265345,
		road: 16186.9302314815,
		acceleration: 0.118611111111113
	},
	{
		id: 1568,
		time: 1567,
		velocity: 12.5883333333333,
		power: 1326.0025309088,
		road: 16199.5510185185,
		acceleration: -0.107129629629631
	},
	{
		id: 1569,
		time: 1568,
		velocity: 12.3688888888889,
		power: -806.280386615145,
		road: 16211.9769907407,
		acceleration: -0.282500000000001
	},
	{
		id: 1570,
		time: 1569,
		velocity: 11.8969444444444,
		power: -1536.62359247426,
		road: 16224.0901388889,
		acceleration: -0.343148148148147
	},
	{
		id: 1571,
		time: 1570,
		velocity: 11.5588888888889,
		power: -736.1211768032,
		road: 16235.8960185185,
		acceleration: -0.27138888888889
	},
	{
		id: 1572,
		time: 1571,
		velocity: 11.5547222222222,
		power: 865.682658660384,
		road: 16247.5036111111,
		acceleration: -0.125185185185185
	},
	{
		id: 1573,
		time: 1572,
		velocity: 11.5213888888889,
		power: 2031.63278926267,
		road: 16259.0397222222,
		acceleration: -0.017777777777777
	},
	{
		id: 1574,
		time: 1573,
		velocity: 11.5055555555556,
		power: 1857.86019762042,
		road: 16270.5505092593,
		acceleration: -0.0328703703703717
	},
	{
		id: 1575,
		time: 1574,
		velocity: 11.4561111111111,
		power: 2245.09434051937,
		road: 16282.0462962963,
		acceleration: 0.00287037037037052
	},
	{
		id: 1576,
		time: 1575,
		velocity: 11.53,
		power: 1788.10125921318,
		road: 16293.5243518519,
		acceleration: -0.038333333333334
	},
	{
		id: 1577,
		time: 1576,
		velocity: 11.3905555555556,
		power: 2123.20633023205,
		road: 16304.9797222222,
		acceleration: -0.00703703703703695
	},
	{
		id: 1578,
		time: 1577,
		velocity: 11.435,
		power: 930.263733341485,
		road: 16316.3740740741,
		acceleration: -0.114999999999998
	},
	{
		id: 1579,
		time: 1578,
		velocity: 11.185,
		power: 1122.44895020352,
		road: 16327.6634259259,
		acceleration: -0.0950000000000024
	},
	{
		id: 1580,
		time: 1579,
		velocity: 11.1055555555556,
		power: 496.180339142816,
		road: 16338.8297222222,
		acceleration: -0.15111111111111
	},
	{
		id: 1581,
		time: 1580,
		velocity: 10.9816666666667,
		power: 839.735552136586,
		road: 16349.8623611111,
		acceleration: -0.116203703703704
	},
	{
		id: 1582,
		time: 1581,
		velocity: 10.8363888888889,
		power: 975.532843693684,
		road: 16360.7863888889,
		acceleration: -0.101018518518519
	},
	{
		id: 1583,
		time: 1582,
		velocity: 10.8025,
		power: 824.40373059373,
		road: 16371.6031944444,
		acceleration: -0.113425925925926
	},
	{
		id: 1584,
		time: 1583,
		velocity: 10.6413888888889,
		power: -827.54625138081,
		road: 16382.2268981482,
		acceleration: -0.272777777777778
	},
	{
		id: 1585,
		time: 1584,
		velocity: 10.0180555555556,
		power: -2468.60559801053,
		road: 16392.4946296296,
		acceleration: -0.439166666666665
	},
	{
		id: 1586,
		time: 1585,
		velocity: 9.485,
		power: -3432.06341242892,
		road: 16402.2678240741,
		acceleration: -0.549907407407408
	},
	{
		id: 1587,
		time: 1586,
		velocity: 8.99166666666667,
		power: -2717.43985430438,
		road: 16411.5240740741,
		acceleration: -0.483981481481482
	},
	{
		id: 1588,
		time: 1587,
		velocity: 8.56611111111111,
		power: -1961.7541034724,
		road: 16420.3358333333,
		acceleration: -0.404999999999999
	},
	{
		id: 1589,
		time: 1588,
		velocity: 8.27,
		power: -1539.85226855393,
		road: 16428.7654166667,
		acceleration: -0.359351851851851
	},
	{
		id: 1590,
		time: 1589,
		velocity: 7.91361111111111,
		power: -1610.31454338191,
		road: 16436.8283796296,
		acceleration: -0.37388888888889
	},
	{
		id: 1591,
		time: 1590,
		velocity: 7.44444444444444,
		power: -1826.00190113019,
		road: 16444.4990277778,
		acceleration: -0.41074074074074
	},
	{
		id: 1592,
		time: 1591,
		velocity: 7.03777777777778,
		power: -1065.6378579968,
		road: 16451.8089351852,
		acceleration: -0.310740740740741
	},
	{
		id: 1593,
		time: 1592,
		velocity: 6.98138888888889,
		power: 1099.67888430584,
		road: 16458.9661111111,
		acceleration: 0.00527777777777683
	},
	{
		id: 1594,
		time: 1593,
		velocity: 7.46027777777778,
		power: 2762.3789722326,
		road: 16466.246712963,
		acceleration: 0.241574074074074
	},
	{
		id: 1595,
		time: 1594,
		velocity: 7.7625,
		power: 6163.62516145166,
		road: 16473.9859722222,
		acceleration: 0.675740740740741
	},
	{
		id: 1596,
		time: 1595,
		velocity: 9.00861111111111,
		power: 6340.06742911572,
		road: 16482.3764351852,
		acceleration: 0.626666666666665
	},
	{
		id: 1597,
		time: 1596,
		velocity: 9.34027777777778,
		power: 7937.36573175011,
		road: 16491.4524537037,
		acceleration: 0.744444444444444
	},
	{
		id: 1598,
		time: 1597,
		velocity: 9.99583333333333,
		power: 6396.26346955675,
		road: 16501.1563888889,
		acceleration: 0.511388888888892
	},
	{
		id: 1599,
		time: 1598,
		velocity: 10.5427777777778,
		power: 6403.82224673914,
		road: 16511.3525462963,
		acceleration: 0.473055555555554
	},
	{
		id: 1600,
		time: 1599,
		velocity: 10.7594444444444,
		power: 6338.1420577408,
		road: 16522.0017592593,
		acceleration: 0.433055555555557
	},
	{
		id: 1601,
		time: 1600,
		velocity: 11.295,
		power: 8016.85535623453,
		road: 16533.1460185185,
		acceleration: 0.557037037037036
	},
	{
		id: 1602,
		time: 1601,
		velocity: 12.2138888888889,
		power: 10604.3011640705,
		road: 16544.9373148148,
		acceleration: 0.737037037037039
	},
	{
		id: 1603,
		time: 1602,
		velocity: 12.9705555555556,
		power: 11895.8106042791,
		road: 16557.4857407407,
		acceleration: 0.777222222222221
	},
	{
		id: 1604,
		time: 1603,
		velocity: 13.6266666666667,
		power: 10182.7908309247,
		road: 16570.712962963,
		acceleration: 0.580370370370369
	},
	{
		id: 1605,
		time: 1604,
		velocity: 13.955,
		power: 7149.74357387504,
		road: 16584.3877314815,
		acceleration: 0.314722222222223
	},
	{
		id: 1606,
		time: 1605,
		velocity: 13.9147222222222,
		power: 3963.7655744344,
		road: 16598.2516666667,
		acceleration: 0.0636111111111131
	},
	{
		id: 1607,
		time: 1606,
		velocity: 13.8175,
		power: 2522.07090372543,
		road: 16612.1246296296,
		acceleration: -0.0455555555555573
	},
	{
		id: 1608,
		time: 1607,
		velocity: 13.8183333333333,
		power: 3245.16141065643,
		road: 16625.9796296296,
		acceleration: 0.00962962962963232
	},
	{
		id: 1609,
		time: 1608,
		velocity: 13.9436111111111,
		power: 4666.95173945947,
		road: 16639.896712963,
		acceleration: 0.114537037037035
	},
	{
		id: 1610,
		time: 1609,
		velocity: 14.1611111111111,
		power: 6783.33249119203,
		road: 16654.0030092593,
		acceleration: 0.263888888888889
	},
	{
		id: 1611,
		time: 1610,
		velocity: 14.61,
		power: 7008.51760051441,
		road: 16668.3745833333,
		acceleration: 0.266666666666666
	},
	{
		id: 1612,
		time: 1611,
		velocity: 14.7436111111111,
		power: 5417.32049761119,
		road: 16682.9504166667,
		acceleration: 0.141851851851854
	},
	{
		id: 1613,
		time: 1612,
		velocity: 14.5866666666667,
		power: 3704.70918820409,
		road: 16697.6052314815,
		acceleration: 0.0161111111111101
	},
	{
		id: 1614,
		time: 1613,
		velocity: 14.6583333333333,
		power: 3773.28922579787,
		road: 16712.278287037,
		acceleration: 0.0203703703703706
	},
	{
		id: 1615,
		time: 1614,
		velocity: 14.8047222222222,
		power: 5329.75603249344,
		road: 16727.0256481482,
		acceleration: 0.12824074074074
	},
	{
		id: 1616,
		time: 1615,
		velocity: 14.9713888888889,
		power: 5033.59301113126,
		road: 16741.8883796296,
		acceleration: 0.102499999999999
	},
	{
		id: 1617,
		time: 1616,
		velocity: 14.9658333333333,
		power: 3254.14274503366,
		road: 16756.7903240741,
		acceleration: -0.0240740740740737
	},
	{
		id: 1618,
		time: 1617,
		velocity: 14.7325,
		power: 3125.44103177101,
		road: 16771.6641203704,
		acceleration: -0.0322222222222219
	},
	{
		id: 1619,
		time: 1618,
		velocity: 14.8747222222222,
		power: 2833.7324672773,
		road: 16786.4960648148,
		acceleration: -0.0514814814814812
	},
	{
		id: 1620,
		time: 1619,
		velocity: 14.8113888888889,
		power: 3676.18036671652,
		road: 16801.3066203704,
		acceleration: 0.00870370370370566
	},
	{
		id: 1621,
		time: 1620,
		velocity: 14.7586111111111,
		power: 3176.94654049261,
		road: 16816.1083796296,
		acceleration: -0.0262962962962998
	},
	{
		id: 1622,
		time: 1621,
		velocity: 14.7958333333333,
		power: 3880.71552924024,
		road: 16830.90875,
		acceleration: 0.0235185185185198
	},
	{
		id: 1623,
		time: 1622,
		velocity: 14.8819444444444,
		power: 4483.21003502444,
		road: 16845.7531018519,
		acceleration: 0.0644444444444439
	},
	{
		id: 1624,
		time: 1623,
		velocity: 14.9519444444444,
		power: 4457.74448354609,
		road: 16860.6598148148,
		acceleration: 0.0602777777777774
	},
	{
		id: 1625,
		time: 1624,
		velocity: 14.9766666666667,
		power: 4433.76547757061,
		road: 16875.6248611111,
		acceleration: 0.0563888888888897
	},
	{
		id: 1626,
		time: 1625,
		velocity: 15.0511111111111,
		power: 3508.99859708602,
		road: 16890.6135648148,
		acceleration: -0.00907407407407312
	},
	{
		id: 1627,
		time: 1626,
		velocity: 14.9247222222222,
		power: 3528.91786592092,
		road: 16905.5940277778,
		acceleration: -0.00740740740740797
	},
	{
		id: 1628,
		time: 1627,
		velocity: 14.9544444444444,
		power: 2858.36107080708,
		road: 16920.5441203704,
		acceleration: -0.0533333333333328
	},
	{
		id: 1629,
		time: 1628,
		velocity: 14.8911111111111,
		power: 2574.5998294942,
		road: 16935.4318518519,
		acceleration: -0.0713888888888903
	},
	{
		id: 1630,
		time: 1629,
		velocity: 14.7105555555556,
		power: 2393.71364576267,
		road: 16950.2429166667,
		acceleration: -0.0819444444444439
	},
	{
		id: 1631,
		time: 1630,
		velocity: 14.7086111111111,
		power: 1826.77114627847,
		road: 16964.953287037,
		acceleration: -0.119444444444444
	},
	{
		id: 1632,
		time: 1631,
		velocity: 14.5327777777778,
		power: 1817.00345558367,
		road: 16979.5453703704,
		acceleration: -0.11712962962963
	},
	{
		id: 1633,
		time: 1632,
		velocity: 14.3591666666667,
		power: 1242.37186312306,
		road: 16994.00125,
		acceleration: -0.155277777777776
	},
	{
		id: 1634,
		time: 1633,
		velocity: 14.2427777777778,
		power: 1665.69810359295,
		road: 17008.3188888889,
		acceleration: -0.121203703703705
	},
	{
		id: 1635,
		time: 1634,
		velocity: 14.1691666666667,
		power: 1137.7162374463,
		road: 17022.4975462963,
		acceleration: -0.15675925925926
	},
	{
		id: 1636,
		time: 1635,
		velocity: 13.8888888888889,
		power: 212.709935613874,
		road: 17036.4868981481,
		acceleration: -0.221851851851852
	},
	{
		id: 1637,
		time: 1636,
		velocity: 13.5772222222222,
		power: 93.7478203147272,
		road: 17050.2517592593,
		acceleration: -0.22712962962963
	},
	{
		id: 1638,
		time: 1637,
		velocity: 13.4877777777778,
		power: 436.96470617561,
		road: 17063.8044444444,
		acceleration: -0.197222222222223
	},
	{
		id: 1639,
		time: 1638,
		velocity: 13.2972222222222,
		power: 58.1576193587247,
		road: 17077.1468981481,
		acceleration: -0.22324074074074
	},
	{
		id: 1640,
		time: 1639,
		velocity: 12.9075,
		power: -1777.98260936525,
		road: 17090.1946296296,
		acceleration: -0.366203703703704
	},
	{
		id: 1641,
		time: 1640,
		velocity: 12.3891666666667,
		power: -1634.88895306877,
		road: 17102.8826388889,
		acceleration: -0.353240740740739
	},
	{
		id: 1642,
		time: 1641,
		velocity: 12.2375,
		power: 1374.88827226069,
		road: 17115.3443518519,
		acceleration: -0.0993518518518517
	},
	{
		id: 1643,
		time: 1642,
		velocity: 12.6094444444444,
		power: 3845.31967189014,
		road: 17127.8105092593,
		acceleration: 0.10824074074074
	},
	{
		id: 1644,
		time: 1643,
		velocity: 12.7138888888889,
		power: 7359.22044882181,
		road: 17140.5247685185,
		acceleration: 0.387962962962963
	},
	{
		id: 1645,
		time: 1644,
		velocity: 13.4013888888889,
		power: 8195.38375165861,
		road: 17153.6478703704,
		acceleration: 0.429722222222223
	},
	{
		id: 1646,
		time: 1645,
		velocity: 13.8986111111111,
		power: 9565.19869823594,
		road: 17167.2386111111,
		acceleration: 0.505555555555555
	},
	{
		id: 1647,
		time: 1646,
		velocity: 14.2305555555556,
		power: 8331.02291032573,
		road: 17181.2736574074,
		acceleration: 0.383055555555556
	},
	{
		id: 1648,
		time: 1647,
		velocity: 14.5505555555556,
		power: 8235.3976950276,
		road: 17195.6773148148,
		acceleration: 0.354166666666666
	},
	{
		id: 1649,
		time: 1648,
		velocity: 14.9611111111111,
		power: 8137.76446732218,
		road: 17210.4218981482,
		acceleration: 0.327685185185187
	},
	{
		id: 1650,
		time: 1649,
		velocity: 15.2136111111111,
		power: 8003.57060946328,
		road: 17225.480787037,
		acceleration: 0.300925925925926
	},
	{
		id: 1651,
		time: 1650,
		velocity: 15.4533333333333,
		power: 7902.97128671496,
		road: 17240.8293981482,
		acceleration: 0.278518518518517
	},
	{
		id: 1652,
		time: 1651,
		velocity: 15.7966666666667,
		power: 6694.92983558766,
		road: 17256.4099074074,
		acceleration: 0.185277777777781
	},
	{
		id: 1653,
		time: 1652,
		velocity: 15.7694444444444,
		power: 4769.67017308089,
		road: 17272.1087962963,
		acceleration: 0.0514814814814812
	},
	{
		id: 1654,
		time: 1653,
		velocity: 15.6077777777778,
		power: 1990.0540615307,
		road: 17287.7670833333,
		acceleration: -0.132685185185185
	},
	{
		id: 1655,
		time: 1654,
		velocity: 15.3986111111111,
		power: 1631.082116471,
		road: 17303.2824537037,
		acceleration: -0.15314814814815
	},
	{
		id: 1656,
		time: 1655,
		velocity: 15.31,
		power: 2005.43654716149,
		road: 17318.6591203704,
		acceleration: -0.12425925925926
	},
	{
		id: 1657,
		time: 1656,
		velocity: 15.235,
		power: 2818.08216153541,
		road: 17333.9406018519,
		acceleration: -0.0661111111111126
	},
	{
		id: 1658,
		time: 1657,
		velocity: 15.2002777777778,
		power: 4370.31008091395,
		road: 17349.2093518519,
		acceleration: 0.0406481481481489
	},
	{
		id: 1659,
		time: 1658,
		velocity: 15.4319444444444,
		power: 5235.53426499657,
		road: 17364.5469907407,
		acceleration: 0.0971296296296291
	},
	{
		id: 1660,
		time: 1659,
		velocity: 15.5263888888889,
		power: 7135.3775029738,
		road: 17380.0426851852,
		acceleration: 0.218981481481482
	},
	{
		id: 1661,
		time: 1660,
		velocity: 15.8572222222222,
		power: 7527.30941581347,
		road: 17395.7649074074,
		acceleration: 0.234074074074075
	},
	{
		id: 1662,
		time: 1661,
		velocity: 16.1341666666667,
		power: 8691.99486242916,
		road: 17411.7527314815,
		acceleration: 0.29712962962963
	},
	{
		id: 1663,
		time: 1662,
		velocity: 16.4177777777778,
		power: 8427.22843238067,
		road: 17428.0216666667,
		acceleration: 0.265092592592591
	},
	{
		id: 1664,
		time: 1663,
		velocity: 16.6525,
		power: 7113.81708120928,
		road: 17444.5084259259,
		acceleration: 0.170555555555556
	},
	{
		id: 1665,
		time: 1664,
		velocity: 16.6458333333333,
		power: 5942.38824988168,
		road: 17461.1258796296,
		acceleration: 0.090833333333336
	},
	{
		id: 1666,
		time: 1665,
		velocity: 16.6902777777778,
		power: 5711.04950519837,
		road: 17477.8252314815,
		acceleration: 0.0729629629629613
	},
	{
		id: 1667,
		time: 1666,
		velocity: 16.8713888888889,
		power: 5884.10229603125,
		road: 17494.6013888889,
		acceleration: 0.0806481481481462
	},
	{
		id: 1668,
		time: 1667,
		velocity: 16.8877777777778,
		power: 6651.24268156613,
		road: 17511.4798611111,
		acceleration: 0.123981481481486
	},
	{
		id: 1669,
		time: 1668,
		velocity: 17.0622222222222,
		power: 6712.85256416204,
		road: 17528.4815277778,
		acceleration: 0.122407407407405
	},
	{
		id: 1670,
		time: 1669,
		velocity: 17.2386111111111,
		power: 6666.8791195426,
		road: 17545.6016203704,
		acceleration: 0.114444444444441
	},
	{
		id: 1671,
		time: 1670,
		velocity: 17.2311111111111,
		power: 4736.92581964745,
		road: 17562.77625,
		acceleration: -0.0053703703703647
	},
	{
		id: 1672,
		time: 1671,
		velocity: 17.0461111111111,
		power: 3442.15461106961,
		road: 17579.9068055556,
		acceleration: -0.0827777777777783
	},
	{
		id: 1673,
		time: 1672,
		velocity: 16.9902777777778,
		power: 2550.97285719122,
		road: 17596.9289351852,
		acceleration: -0.134074074074075
	},
	{
		id: 1674,
		time: 1673,
		velocity: 16.8288888888889,
		power: 3131.7201630674,
		road: 17613.8365740741,
		acceleration: -0.0949074074074083
	},
	{
		id: 1675,
		time: 1674,
		velocity: 16.7613888888889,
		power: 2908.21012572582,
		road: 17630.6438888889,
		acceleration: -0.105740740740739
	},
	{
		id: 1676,
		time: 1675,
		velocity: 16.6730555555556,
		power: 3431.85801893457,
		road: 17647.3631481481,
		acceleration: -0.0703703703703695
	},
	{
		id: 1677,
		time: 1676,
		velocity: 16.6177777777778,
		power: 3020.58339404131,
		road: 17664.0004166667,
		acceleration: -0.0936111111111124
	},
	{
		id: 1678,
		time: 1677,
		velocity: 16.4805555555556,
		power: 3024.84474374737,
		road: 17680.5456018519,
		acceleration: -0.0905555555555573
	},
	{
		id: 1679,
		time: 1678,
		velocity: 16.4013888888889,
		power: 2714.12163085623,
		road: 17696.9918518519,
		acceleration: -0.107314814814814
	},
	{
		id: 1680,
		time: 1679,
		velocity: 16.2958333333333,
		power: 2137.53714959091,
		road: 17713.3141203704,
		acceleration: -0.140648148148149
	},
	{
		id: 1681,
		time: 1680,
		velocity: 16.0586111111111,
		power: 1405.04988658494,
		road: 17729.474212963,
		acceleration: -0.183703703703703
	},
	{
		id: 1682,
		time: 1681,
		velocity: 15.8502777777778,
		power: 566.748133610397,
		road: 17745.4256018519,
		acceleration: -0.233703703703704
	},
	{
		id: 1683,
		time: 1682,
		velocity: 15.5947222222222,
		power: 1028.8035717359,
		road: 17761.1608333333,
		acceleration: -0.198611111111113
	},
	{
		id: 1684,
		time: 1683,
		velocity: 15.4627777777778,
		power: 1329.58543839542,
		road: 17776.7096296296,
		acceleration: -0.174259259259259
	},
	{
		id: 1685,
		time: 1684,
		velocity: 15.3275,
		power: 2148.24113669382,
		road: 17792.1136574074,
		acceleration: -0.115277777777777
	},
	{
		id: 1686,
		time: 1685,
		velocity: 15.2488888888889,
		power: 1801.51466199118,
		road: 17807.3922222222,
		acceleration: -0.135648148148148
	},
	{
		id: 1687,
		time: 1686,
		velocity: 15.0558333333333,
		power: 1875.23625309999,
		road: 17822.5393518519,
		acceleration: -0.127222222222224
	},
	{
		id: 1688,
		time: 1687,
		velocity: 14.9458333333333,
		power: 2197.37627559823,
		road: 17837.5719444445,
		acceleration: -0.101851851851849
	},
	{
		id: 1689,
		time: 1688,
		velocity: 14.9433333333333,
		power: 3048.61078305513,
		road: 17852.5334259259,
		acceleration: -0.0403703703703719
	},
	{
		id: 1690,
		time: 1689,
		velocity: 14.9347222222222,
		power: 4480.70033633636,
		road: 17867.5044444445,
		acceleration: 0.0594444444444449
	},
	{
		id: 1691,
		time: 1690,
		velocity: 15.1241666666667,
		power: 5726.54413217858,
		road: 17882.57625,
		acceleration: 0.142129629629629
	},
	{
		id: 1692,
		time: 1691,
		velocity: 15.3697222222222,
		power: 6837.91678447609,
		road: 17897.8244907407,
		acceleration: 0.210740740740743
	},
	{
		id: 1693,
		time: 1692,
		velocity: 15.5669444444444,
		power: 6761.62380878829,
		road: 17913.2760185185,
		acceleration: 0.195833333333333
	},
	{
		id: 1694,
		time: 1693,
		velocity: 15.7116666666667,
		power: 7262.60137756843,
		road: 17928.9352314815,
		acceleration: 0.219537037037036
	},
	{
		id: 1695,
		time: 1694,
		velocity: 16.0283333333333,
		power: 6514.65925522061,
		road: 17944.7846759259,
		acceleration: 0.160925925925925
	},
	{
		id: 1696,
		time: 1695,
		velocity: 16.0497222222222,
		power: 7312.13689384335,
		road: 17960.8169444445,
		acceleration: 0.204722222222223
	},
	{
		id: 1697,
		time: 1696,
		velocity: 16.3258333333333,
		power: 4562.65478734915,
		road: 17976.9621759259,
		acceleration: 0.0212037037037049
	},
	{
		id: 1698,
		time: 1697,
		velocity: 16.0919444444444,
		power: 2695.70978168311,
		road: 17993.0687037037,
		acceleration: -0.0986111111111114
	},
	{
		id: 1699,
		time: 1698,
		velocity: 15.7538888888889,
		power: 4826.27546285542,
		road: 18009.1463425926,
		acceleration: 0.0408333333333317
	},
	{
		id: 1700,
		time: 1699,
		velocity: 16.4483333333333,
		power: 7080.10408832961,
		road: 18025.3355092593,
		acceleration: 0.182222222222222
	},
	{
		id: 1701,
		time: 1700,
		velocity: 16.6386111111111,
		power: 10222.346349465,
		road: 18041.8002777778,
		acceleration: 0.36898148148148
	},
	{
		id: 1702,
		time: 1701,
		velocity: 16.8608333333333,
		power: 6873.14070231391,
		road: 18058.5218981482,
		acceleration: 0.144722222222224
	},
	{
		id: 1703,
		time: 1702,
		velocity: 16.8825,
		power: 6284.76620298949,
		road: 18075.3672222222,
		acceleration: 0.102685185185184
	},
	{
		id: 1704,
		time: 1703,
		velocity: 16.9466666666667,
		power: 5342.10915065703,
		road: 18092.2845833333,
		acceleration: 0.0413888888888891
	},
	{
		id: 1705,
		time: 1704,
		velocity: 16.985,
		power: 6443.5559617181,
		road: 18109.275787037,
		acceleration: 0.106296296296296
	},
	{
		id: 1706,
		time: 1705,
		velocity: 17.2013888888889,
		power: 5265.83711203753,
		road: 18126.3357407408,
		acceleration: 0.0312037037037065
	},
	{
		id: 1707,
		time: 1706,
		velocity: 17.0402777777778,
		power: 5399.56524303747,
		road: 18143.4303240741,
		acceleration: 0.0380555555555553
	},
	{
		id: 1708,
		time: 1707,
		velocity: 17.0991666666667,
		power: 3794.72396121828,
		road: 18160.5140740741,
		acceleration: -0.0597222222222236
	},
	{
		id: 1709,
		time: 1708,
		velocity: 17.0222222222222,
		power: 3911.37261153829,
		road: 18177.5425925926,
		acceleration: -0.0507407407407428
	},
	{
		id: 1710,
		time: 1709,
		velocity: 16.8880555555556,
		power: 3265.36432795383,
		road: 18194.5016203704,
		acceleration: -0.0882407407407371
	},
	{
		id: 1711,
		time: 1710,
		velocity: 16.8344444444444,
		power: 3454.86514176664,
		road: 18211.3795370371,
		acceleration: -0.0739814814814821
	},
	{
		id: 1712,
		time: 1711,
		velocity: 16.8002777777778,
		power: 3657.51766037314,
		road: 18228.1908333333,
		acceleration: -0.0592592592592602
	},
	{
		id: 1713,
		time: 1712,
		velocity: 16.7102777777778,
		power: 3629.58453166001,
		road: 18244.942962963,
		acceleration: -0.0590740740740721
	},
	{
		id: 1714,
		time: 1713,
		velocity: 16.6572222222222,
		power: 2721.53983006859,
		road: 18261.6089351852,
		acceleration: -0.113240740740743
	},
	{
		id: 1715,
		time: 1714,
		velocity: 16.4605555555556,
		power: 3237.97517577089,
		road: 18278.1793518519,
		acceleration: -0.0778703703703698
	},
	{
		id: 1716,
		time: 1715,
		velocity: 16.4766666666667,
		power: 3202.72758747569,
		road: 18294.6719907408,
		acceleration: -0.0776851851851816
	},
	{
		id: 1717,
		time: 1716,
		velocity: 16.4241666666667,
		power: 4619.75563168637,
		road: 18311.1324537037,
		acceleration: 0.0133333333333283
	},
	{
		id: 1718,
		time: 1717,
		velocity: 16.5005555555556,
		power: 4477.44726894289,
		road: 18327.6015740741,
		acceleration: 0.00398148148148181
	},
	{
		id: 1719,
		time: 1718,
		velocity: 16.4886111111111,
		power: 4525.73818683422,
		road: 18344.0761111111,
		acceleration: 0.00685185185185233
	},
	{
		id: 1720,
		time: 1719,
		velocity: 16.4447222222222,
		power: 3136.738082446,
		road: 18360.5139814815,
		acceleration: -0.0801851851851829
	},
	{
		id: 1721,
		time: 1720,
		velocity: 16.26,
		power: 3576.49501464868,
		road: 18376.886712963,
		acceleration: -0.0500925925925948
	},
	{
		id: 1722,
		time: 1721,
		velocity: 16.3383333333333,
		power: 3348.4721482294,
		road: 18393.202962963,
		acceleration: -0.0628703703703728
	},
	{
		id: 1723,
		time: 1722,
		velocity: 16.2561111111111,
		power: 5098.23833632403,
		road: 18409.5125462963,
		acceleration: 0.0495370370370409
	},
	{
		id: 1724,
		time: 1723,
		velocity: 16.4086111111111,
		power: 4664.9214294774,
		road: 18425.8571296296,
		acceleration: 0.0204629629629629
	},
	{
		id: 1725,
		time: 1724,
		velocity: 16.3997222222222,
		power: 4630.96925687679,
		road: 18442.2207407407,
		acceleration: 0.0175925925925924
	},
	{
		id: 1726,
		time: 1725,
		velocity: 16.3088888888889,
		power: 3228.45219794875,
		road: 18458.5575462963,
		acceleration: -0.0712037037037021
	},
	{
		id: 1727,
		time: 1726,
		velocity: 16.195,
		power: 2843.18513057587,
		road: 18474.812037037,
		acceleration: -0.0934259259259278
	},
	{
		id: 1728,
		time: 1727,
		velocity: 16.1194444444444,
		power: 3811.41346208628,
		road: 18491.0052777778,
		acceleration: -0.0290740740740745
	},
	{
		id: 1729,
		time: 1728,
		velocity: 16.2216666666667,
		power: 4586.18367407578,
		road: 18507.194537037,
		acceleration: 0.0211111111111109
	},
	{
		id: 1730,
		time: 1729,
		velocity: 16.2583333333333,
		power: 4857.00989914893,
		road: 18523.4131018519,
		acceleration: 0.0374999999999979
	},
	{
		id: 1731,
		time: 1730,
		velocity: 16.2319444444444,
		power: 3781.52186537762,
		road: 18539.6344444444,
		acceleration: -0.0319444444444414
	},
	{
		id: 1732,
		time: 1731,
		velocity: 16.1258333333333,
		power: 4166.03645171284,
		road: 18555.8365740741,
		acceleration: -0.00648148148148309
	},
	{
		id: 1733,
		time: 1732,
		velocity: 16.2388888888889,
		power: 4361.01691955352,
		road: 18572.0385185185,
		acceleration: 0.00611111111111029
	},
	{
		id: 1734,
		time: 1733,
		velocity: 16.2502777777778,
		power: 3910.55309184376,
		road: 18588.2321759259,
		acceleration: -0.0226851851851855
	},
	{
		id: 1735,
		time: 1734,
		velocity: 16.0577777777778,
		power: 3119.46103159452,
		road: 18604.3783333333,
		acceleration: -0.0723148148148169
	},
	{
		id: 1736,
		time: 1735,
		velocity: 16.0219444444444,
		power: 2476.2531286196,
		road: 18620.4326388889,
		acceleration: -0.111388888888886
	},
	{
		id: 1737,
		time: 1736,
		velocity: 15.9161111111111,
		power: 3381.81975739831,
		road: 18636.4063425926,
		acceleration: -0.0498148148148143
	},
	{
		id: 1738,
		time: 1737,
		velocity: 15.9083333333333,
		power: 2364.31995578764,
		road: 18652.2980555556,
		acceleration: -0.114166666666666
	},
	{
		id: 1739,
		time: 1738,
		velocity: 15.6794444444444,
		power: 2184.85198446711,
		road: 18668.0712962963,
		acceleration: -0.122777777777777
	},
	{
		id: 1740,
		time: 1739,
		velocity: 15.5477777777778,
		power: 1452.59398820614,
		road: 18683.699212963,
		acceleration: -0.167870370370371
	},
	{
		id: 1741,
		time: 1740,
		velocity: 15.4047222222222,
		power: 1465.41780073066,
		road: 18699.1616666667,
		acceleration: -0.163055555555554
	},
	{
		id: 1742,
		time: 1741,
		velocity: 15.1902777777778,
		power: 693.311088763071,
		road: 18714.4368518519,
		acceleration: -0.211481481481481
	},
	{
		id: 1743,
		time: 1742,
		velocity: 14.9133333333333,
		power: -237.475176912153,
		road: 18729.4706018519,
		acceleration: -0.27138888888889
	},
	{
		id: 1744,
		time: 1743,
		velocity: 14.5905555555556,
		power: -2718.41584635922,
		road: 18744.1472685185,
		acceleration: -0.442777777777778
	},
	{
		id: 1745,
		time: 1744,
		velocity: 13.8619444444444,
		power: -8113.58774586461,
		road: 18758.1805555556,
		acceleration: -0.843981481481483
	},
	{
		id: 1746,
		time: 1745,
		velocity: 12.3813888888889,
		power: -12582.3867701126,
		road: 18771.1731018519,
		acceleration: -1.2375
	},
	{
		id: 1747,
		time: 1746,
		velocity: 10.8780555555556,
		power: -12197.1653318615,
		road: 18782.8989814815,
		acceleration: -1.29583333333333
	},
	{
		id: 1748,
		time: 1747,
		velocity: 9.97444444444444,
		power: -8551.95821736413,
		road: 18793.4568055556,
		acceleration: -1.04027777777778
	},
	{
		id: 1749,
		time: 1748,
		velocity: 9.26055555555556,
		power: -7084.96472824557,
		road: 18803.0159259259,
		acceleration: -0.957129629629629
	},
	{
		id: 1750,
		time: 1749,
		velocity: 8.00666666666667,
		power: -6865.42465761602,
		road: 18811.5915277778,
		acceleration: -1.00990740740741
	},
	{
		id: 1751,
		time: 1750,
		velocity: 6.94472222222222,
		power: -7116.54608341899,
		road: 18819.0833796296,
		acceleration: -1.15759259259259
	},
	{
		id: 1752,
		time: 1751,
		velocity: 5.78777777777778,
		power: -4894.27489343196,
		road: 18825.52125,
		acceleration: -0.950370370370372
	},
	{
		id: 1753,
		time: 1752,
		velocity: 5.15555555555556,
		power: -2515.23250916246,
		road: 18831.1772685185,
		acceleration: -0.613333333333333
	},
	{
		id: 1754,
		time: 1753,
		velocity: 5.10472222222222,
		power: 20.533670655363,
		road: 18836.4571296296,
		acceleration: -0.138981481481481
	},
	{
		id: 1755,
		time: 1754,
		velocity: 5.37083333333333,
		power: 1904.90798536383,
		road: 18841.7839814815,
		acceleration: 0.232962962962962
	},
	{
		id: 1756,
		time: 1755,
		velocity: 5.85444444444444,
		power: 3236.14158580055,
		road: 18847.4547685185,
		acceleration: 0.454907407407408
	},
	{
		id: 1757,
		time: 1756,
		velocity: 6.46944444444444,
		power: 4240.88812741317,
		road: 18853.6391666667,
		acceleration: 0.572314814814816
	},
	{
		id: 1758,
		time: 1757,
		velocity: 7.08777777777778,
		power: 5777.76636183866,
		road: 18860.477037037,
		acceleration: 0.734629629629629
	},
	{
		id: 1759,
		time: 1758,
		velocity: 8.05833333333333,
		power: 6452.0480058748,
		road: 18868.0500462963,
		acceleration: 0.735648148148148
	},
	{
		id: 1760,
		time: 1759,
		velocity: 8.67638888888889,
		power: 5952.8028642191,
		road: 18876.2876388889,
		acceleration: 0.593518518518517
	},
	{
		id: 1761,
		time: 1760,
		velocity: 8.86833333333333,
		power: 4496.2731167537,
		road: 18885.0076388889,
		acceleration: 0.371296296296297
	},
	{
		id: 1762,
		time: 1761,
		velocity: 9.17222222222222,
		power: 4143.72184713253,
		road: 18894.0666203704,
		acceleration: 0.306666666666667
	},
	{
		id: 1763,
		time: 1762,
		velocity: 9.59638888888889,
		power: 4059.72441394609,
		road: 18903.4184722222,
		acceleration: 0.279074074074073
	},
	{
		id: 1764,
		time: 1763,
		velocity: 9.70555555555555,
		power: 3938.27853466596,
		road: 18913.0350462963,
		acceleration: 0.250370370370373
	},
	{
		id: 1765,
		time: 1764,
		velocity: 9.92333333333333,
		power: 4181.84417160056,
		road: 18922.9079166667,
		acceleration: 0.262222222222221
	},
	{
		id: 1766,
		time: 1765,
		velocity: 10.3830555555556,
		power: 5246.48890755881,
		road: 18933.0893518519,
		acceleration: 0.354907407407408
	},
	{
		id: 1767,
		time: 1766,
		velocity: 10.7702777777778,
		power: 5525.9031111931,
		road: 18943.628287037,
		acceleration: 0.360092592592595
	},
	{
		id: 1768,
		time: 1767,
		velocity: 11.0036111111111,
		power: 4856.3496549324,
		road: 18954.4849537037,
		acceleration: 0.275370370370368
	},
	{
		id: 1769,
		time: 1768,
		velocity: 11.2091666666667,
		power: 3151.44810826227,
		road: 18965.5308333333,
		acceleration: 0.103055555555557
	},
	{
		id: 1770,
		time: 1769,
		velocity: 11.0794444444444,
		power: 1045.98780566388,
		road: 18976.5797685185,
		acceleration: -0.0969444444444445
	},
	{
		id: 1771,
		time: 1770,
		velocity: 10.7127777777778,
		power: -302.678190833983,
		road: 18987.4685185185,
		acceleration: -0.223425925925925
	},
	{
		id: 1772,
		time: 1771,
		velocity: 10.5388888888889,
		power: 1399.89689015179,
		road: 18998.2175925926,
		acceleration: -0.0559259259259264
	},
	{
		id: 1773,
		time: 1772,
		velocity: 10.9116666666667,
		power: 3508.79477987392,
		road: 19009.0126388889,
		acceleration: 0.147870370370367
	},
	{
		id: 1774,
		time: 1773,
		velocity: 11.1563888888889,
		power: 4803.56404198662,
		road: 19020.0128240741,
		acceleration: 0.262407407407409
	},
	{
		id: 1775,
		time: 1774,
		velocity: 11.3261111111111,
		power: 3192.00850643063,
		road: 19031.1949537037,
		acceleration: 0.101481481481482
	},
	{
		id: 1776,
		time: 1775,
		velocity: 11.2161111111111,
		power: 1674.34916693479,
		road: 19042.4069907407,
		acceleration: -0.0416666666666643
	},
	{
		id: 1777,
		time: 1776,
		velocity: 11.0313888888889,
		power: 1827.47466152754,
		road: 19053.585,
		acceleration: -0.0263888888888921
	},
	{
		id: 1778,
		time: 1777,
		velocity: 11.2469444444444,
		power: 3392.0656277611,
		road: 19064.8090740741,
		acceleration: 0.11851851851852
	},
	{
		id: 1779,
		time: 1778,
		velocity: 11.5716666666667,
		power: 6095.43759191897,
		road: 19076.2705092593,
		acceleration: 0.356203703703704
	},
	{
		id: 1780,
		time: 1779,
		velocity: 12.1,
		power: 8374.89364245201,
		road: 19088.1751388889,
		acceleration: 0.530185185185184
	},
	{
		id: 1781,
		time: 1780,
		velocity: 12.8375,
		power: 10645.5015992281,
		road: 19100.6830092593,
		acceleration: 0.676296296296298
	},
	{
		id: 1782,
		time: 1781,
		velocity: 13.6005555555556,
		power: 10832.2112350767,
		road: 19113.8474537037,
		acceleration: 0.636851851851851
	},
	{
		id: 1783,
		time: 1782,
		velocity: 14.0105555555556,
		power: 9358.20531507105,
		road: 19127.5706018519,
		acceleration: 0.480555555555556
	},
	{
		id: 1784,
		time: 1783,
		velocity: 14.2791666666667,
		power: 6262.27799481639,
		road: 19141.6473611111,
		acceleration: 0.226666666666665
	},
	{
		id: 1785,
		time: 1784,
		velocity: 14.2805555555556,
		power: 4651.99922535124,
		road: 19155.8875925926,
		acceleration: 0.100277777777778
	},
	{
		id: 1786,
		time: 1785,
		velocity: 14.3113888888889,
		power: 4244.47599897343,
		road: 19170.2115277778,
		acceleration: 0.0671296296296315
	},
	{
		id: 1787,
		time: 1786,
		velocity: 14.4805555555556,
		power: 3600.98120891748,
		road: 19184.5783333333,
		acceleration: 0.0186111111111078
	},
	{
		id: 1788,
		time: 1787,
		velocity: 14.3363888888889,
		power: 3409.30979802931,
		road: 19198.9565740741,
		acceleration: 0.00425925925926052
	},
	{
		id: 1789,
		time: 1788,
		velocity: 14.3241666666667,
		power: 1563.88200035213,
		road: 19213.2726388889,
		acceleration: -0.128611111111111
	},
	{
		id: 1790,
		time: 1789,
		velocity: 14.0947222222222,
		power: -315.44246640674,
		road: 19227.3927777778,
		acceleration: -0.263240740740741
	},
	{
		id: 1791,
		time: 1790,
		velocity: 13.5466666666667,
		power: -1687.9753360184,
		road: 19241.1998148148,
		acceleration: -0.362962962962962
	},
	{
		id: 1792,
		time: 1791,
		velocity: 13.2352777777778,
		power: -2093.24733763578,
		road: 19254.6291666667,
		acceleration: -0.392407407407408
	},
	{
		id: 1793,
		time: 1792,
		velocity: 12.9175,
		power: -874.186583709823,
		road: 19267.7153240741,
		acceleration: -0.293981481481483
	},
	{
		id: 1794,
		time: 1793,
		velocity: 12.6647222222222,
		power: -138.682290274168,
		road: 19280.53875,
		acceleration: -0.231481481481481
	},
	{
		id: 1795,
		time: 1794,
		velocity: 12.5408333333333,
		power: 1397.48127737512,
		road: 19293.1954166667,
		acceleration: -0.102037037037036
	},
	{
		id: 1796,
		time: 1795,
		velocity: 12.6113888888889,
		power: 5450.98758826633,
		road: 19305.91625,
		acceleration: 0.230370370370371
	},
	{
		id: 1797,
		time: 1796,
		velocity: 13.3558333333333,
		power: 7994.9884834168,
		road: 19318.9616203704,
		acceleration: 0.418703703703702
	},
	{
		id: 1798,
		time: 1797,
		velocity: 13.7969444444444,
		power: 8214.90820572339,
		road: 19332.4212037037,
		acceleration: 0.409722222222221
	},
	{
		id: 1799,
		time: 1798,
		velocity: 13.8405555555556,
		power: 5656.26962454847,
		road: 19346.18375,
		acceleration: 0.196203703703706
	},
	{
		id: 1800,
		time: 1799,
		velocity: 13.9444444444444,
		power: 3421.88640005027,
		road: 19360.0556018519,
		acceleration: 0.0224074074074068
	},
	{
		id: 1801,
		time: 1800,
		velocity: 13.8641666666667,
		power: 4252.43168000025,
		road: 19373.9801851852,
		acceleration: 0.0830555555555552
	},
	{
		id: 1802,
		time: 1801,
		velocity: 14.0897222222222,
		power: 4207.12003083934,
		road: 19387.9845833333,
		acceleration: 0.0765740740740721
	},
	{
		id: 1803,
		time: 1802,
		velocity: 14.1741666666667,
		power: 4252.95409164038,
		road: 19402.065787037,
		acceleration: 0.077037037037039
	},
	{
		id: 1804,
		time: 1803,
		velocity: 14.0952777777778,
		power: 3225.211527809,
		road: 19416.1851851852,
		acceleration: -0.00064814814814973
	},
	{
		id: 1805,
		time: 1804,
		velocity: 14.0877777777778,
		power: 1650.13187219421,
		road: 19430.24625,
		acceleration: -0.116018518518517
	},
	{
		id: 1806,
		time: 1805,
		velocity: 13.8261111111111,
		power: -124.591529208502,
		road: 19444.1265740741,
		acceleration: -0.245462962962963
	},
	{
		id: 1807,
		time: 1806,
		velocity: 13.3588888888889,
		power: -1128.36430572106,
		road: 19457.7248611111,
		acceleration: -0.31861111111111
	},
	{
		id: 1808,
		time: 1807,
		velocity: 13.1319444444444,
		power: 450.927454511786,
		road: 19471.0676388889,
		acceleration: -0.192407407407408
	},
	{
		id: 1809,
		time: 1808,
		velocity: 13.2488888888889,
		power: 2331.34506534783,
		road: 19484.2935185185,
		acceleration: -0.0413888888888891
	},
	{
		id: 1810,
		time: 1809,
		velocity: 13.2347222222222,
		power: 3008.02028626137,
		road: 19497.5050462963,
		acceleration: 0.0126851851851857
	},
	{
		id: 1811,
		time: 1810,
		velocity: 13.17,
		power: 1909.58878229096,
		road: 19510.6861111111,
		acceleration: -0.0736111111111111
	},
	{
		id: 1812,
		time: 1811,
		velocity: 13.0280555555556,
		power: 811.299610079553,
		road: 19523.7510648148,
		acceleration: -0.15861111111111
	},
	{
		id: 1813,
		time: 1812,
		velocity: 12.7588888888889,
		power: 1075.91144184588,
		road: 19536.6695833333,
		acceleration: -0.13425925925926
	},
	{
		id: 1814,
		time: 1813,
		velocity: 12.7672222222222,
		power: 2544.64872250618,
		road: 19549.514537037,
		acceleration: -0.0128703703703703
	},
	{
		id: 1815,
		time: 1814,
		velocity: 12.9894444444444,
		power: 5638.96112688547,
		road: 19562.4700462963,
		acceleration: 0.23398148148148
	},
	{
		id: 1816,
		time: 1815,
		velocity: 13.4608333333333,
		power: 5238.60014969372,
		road: 19575.6383333333,
		acceleration: 0.191574074074074
	},
	{
		id: 1817,
		time: 1816,
		velocity: 13.3419444444444,
		power: 4124.78514220888,
		road: 19588.9510185185,
		acceleration: 0.0972222222222214
	},
	{
		id: 1818,
		time: 1817,
		velocity: 13.2811111111111,
		power: 1060.33266137231,
		road: 19602.2406018519,
		acceleration: -0.143425925925925
	},
	{
		id: 1819,
		time: 1818,
		velocity: 13.0305555555556,
		power: -181.691599509366,
		road: 19615.3391203704,
		acceleration: -0.238703703703704
	},
	{
		id: 1820,
		time: 1819,
		velocity: 12.6258333333333,
		power: 1837.11105197053,
		road: 19628.281712963,
		acceleration: -0.0731481481481495
	},
	{
		id: 1821,
		time: 1820,
		velocity: 13.0616666666667,
		power: 2508.60908656395,
		road: 19641.1790277778,
		acceleration: -0.017407407407406
	},
	{
		id: 1822,
		time: 1821,
		velocity: 12.9783333333333,
		power: 295.566392481168,
		road: 19653.9699074074,
		acceleration: -0.195462962962964
	},
	{
		id: 1823,
		time: 1822,
		velocity: 12.0394444444444,
		power: -8495.86124835883,
		road: 19666.1928703704,
		acceleration: -0.940370370370369
	},
	{
		id: 1824,
		time: 1823,
		velocity: 10.2405555555556,
		power: -10981.2109657936,
		road: 19677.3297685185,
		acceleration: -1.23175925925926
	},
	{
		id: 1825,
		time: 1824,
		velocity: 9.28305555555555,
		power: -9624.58642551057,
		road: 19687.2500925926,
		acceleration: -1.20138888888889
	},
	{
		id: 1826,
		time: 1825,
		velocity: 8.43527777777778,
		power: -6469.04270774052,
		road: 19696.1000462963,
		acceleration: -0.939351851851852
	},
	{
		id: 1827,
		time: 1826,
		velocity: 7.4225,
		power: -7960.7248566181,
		road: 19703.8605555556,
		acceleration: -1.23953703703704
	},
	{
		id: 1828,
		time: 1827,
		velocity: 5.56444444444444,
		power: -6764.38368629574,
		road: 19710.3799537037,
		acceleration: -1.24268518518518
	},
	{
		id: 1829,
		time: 1828,
		velocity: 4.70722222222222,
		power: -7027.7375397612,
		road: 19715.4819444444,
		acceleration: -1.59212962962963
	},
	{
		id: 1830,
		time: 1829,
		velocity: 2.64611111111111,
		power: -4499.33724367308,
		road: 19719.0576851852,
		acceleration: -1.46037037037037
	},
	{
		id: 1831,
		time: 1830,
		velocity: 1.18333333333333,
		power: -2812.85494408414,
		road: 19721.1187037037,
		acceleration: -1.56907407407407
	},
	{
		id: 1832,
		time: 1831,
		velocity: 0,
		power: -597.115135388544,
		road: 19721.9541666667,
		acceleration: -0.882037037037037
	},
	{
		id: 1833,
		time: 1832,
		velocity: 0,
		power: -49.8708567251462,
		road: 19722.1513888889,
		acceleration: -0.394444444444444
	},
	{
		id: 1834,
		time: 1833,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1835,
		time: 1834,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1836,
		time: 1835,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1837,
		time: 1836,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1838,
		time: 1837,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1839,
		time: 1838,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1840,
		time: 1839,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1841,
		time: 1840,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1842,
		time: 1841,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1843,
		time: 1842,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1844,
		time: 1843,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1845,
		time: 1844,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1846,
		time: 1845,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1847,
		time: 1846,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1848,
		time: 1847,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1849,
		time: 1848,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1850,
		time: 1849,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1851,
		time: 1850,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1852,
		time: 1851,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1853,
		time: 1852,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1854,
		time: 1853,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1855,
		time: 1854,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1856,
		time: 1855,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1857,
		time: 1856,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1858,
		time: 1857,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1859,
		time: 1858,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1860,
		time: 1859,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1861,
		time: 1860,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1862,
		time: 1861,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1863,
		time: 1862,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1864,
		time: 1863,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1865,
		time: 1864,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1866,
		time: 1865,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1867,
		time: 1866,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1868,
		time: 1867,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1869,
		time: 1868,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1870,
		time: 1869,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1871,
		time: 1870,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1872,
		time: 1871,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1873,
		time: 1872,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1874,
		time: 1873,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1875,
		time: 1874,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1876,
		time: 1875,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1877,
		time: 1876,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1878,
		time: 1877,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1879,
		time: 1878,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1880,
		time: 1879,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1881,
		time: 1880,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1882,
		time: 1881,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1883,
		time: 1882,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1884,
		time: 1883,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1885,
		time: 1884,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1886,
		time: 1885,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1887,
		time: 1886,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1888,
		time: 1887,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1889,
		time: 1888,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1890,
		time: 1889,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1891,
		time: 1890,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1892,
		time: 1891,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1893,
		time: 1892,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1894,
		time: 1893,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1895,
		time: 1894,
		velocity: 0,
		power: 0,
		road: 19722.1513888889,
		acceleration: 0
	},
	{
		id: 1896,
		time: 1895,
		velocity: 0,
		power: 43.0476534551027,
		road: 19722.2735648148,
		acceleration: 0.244351851851852
	},
	{
		id: 1897,
		time: 1896,
		velocity: 0.733055555555555,
		power: 402.315727046933,
		road: 19722.833287037,
		acceleration: 0.630740740740741
	},
	{
		id: 1898,
		time: 1897,
		velocity: 1.89222222222222,
		power: 1807.02607661803,
		road: 19724.2956018518,
		acceleration: 1.17444444444444
	},
	{
		id: 1899,
		time: 1898,
		velocity: 3.52333333333333,
		power: 3091.25131554008,
		road: 19726.9039814815,
		acceleration: 1.11768518518518
	},
	{
		id: 1900,
		time: 1899,
		velocity: 4.08611111111111,
		power: 3078.21166313061,
		road: 19730.4598611111,
		acceleration: 0.777314814814815
	},
	{
		id: 1901,
		time: 1900,
		velocity: 4.22416666666667,
		power: 862.483999012434,
		road: 19734.4500462963,
		acceleration: 0.0912962962962967
	},
	{
		id: 1902,
		time: 1901,
		velocity: 3.79722222222222,
		power: -309.803580845913,
		road: 19738.3762962963,
		acceleration: -0.219166666666667
	},
	{
		id: 1903,
		time: 1902,
		velocity: 3.42861111111111,
		power: -635.263338885487,
		road: 19742.0340277778,
		acceleration: -0.31787037037037
	},
	{
		id: 1904,
		time: 1903,
		velocity: 3.27055555555556,
		power: -238.134038034613,
		road: 19745.4289351852,
		acceleration: -0.207777777777778
	},
	{
		id: 1905,
		time: 1904,
		velocity: 3.17388888888889,
		power: -374.662443677197,
		road: 19748.5910185185,
		acceleration: -0.25787037037037
	},
	{
		id: 1906,
		time: 1905,
		velocity: 2.655,
		power: -353.717710874381,
		road: 19751.4938888889,
		acceleration: -0.260555555555555
	},
	{
		id: 1907,
		time: 1906,
		velocity: 2.48888888888889,
		power: -429.179141338849,
		road: 19754.114537037,
		acceleration: -0.303888888888889
	},
	{
		id: 1908,
		time: 1907,
		velocity: 2.26222222222222,
		power: -539.438925580943,
		road: 19756.393287037,
		acceleration: -0.379907407407407
	},
	{
		id: 1909,
		time: 1908,
		velocity: 1.51527777777778,
		power: -604.287191654473,
		road: 19758.2453703704,
		acceleration: -0.473425925925926
	},
	{
		id: 1910,
		time: 1909,
		velocity: 1.06861111111111,
		power: -458.721189431325,
		road: 19759.6205555556,
		acceleration: -0.48037037037037
	},
	{
		id: 1911,
		time: 1910,
		velocity: 0.821111111111111,
		power: -194.288226186699,
		road: 19760.5853240741,
		acceleration: -0.340462962962963
	},
	{
		id: 1912,
		time: 1911,
		velocity: 0.493888888888889,
		power: -133.479139761711,
		road: 19761.2017592593,
		acceleration: -0.356203703703704
	},
	{
		id: 1913,
		time: 1912,
		velocity: 0,
		power: -41.7448255288294,
		road: 19761.5032407407,
		acceleration: -0.273703703703704
	},
	{
		id: 1914,
		time: 1913,
		velocity: 0,
		power: -2.89312024041585,
		road: 19761.5855555556,
		acceleration: -0.16462962962963
	},
	{
		id: 1915,
		time: 1914,
		velocity: 0,
		power: 84.2684802617344,
		road: 19761.7669444444,
		acceleration: 0.362777777777778
	},
	{
		id: 1916,
		time: 1915,
		velocity: 1.08833333333333,
		power: 345.573851308622,
		road: 19762.3687962963,
		acceleration: 0.478148148148148
	},
	{
		id: 1917,
		time: 1916,
		velocity: 1.43444444444444,
		power: 702.680936923857,
		road: 19763.4793518518,
		acceleration: 0.539259259259259
	},
	{
		id: 1918,
		time: 1917,
		velocity: 1.61777777777778,
		power: 425.709088172467,
		road: 19764.9480555556,
		acceleration: 0.177037037037037
	},
	{
		id: 1919,
		time: 1918,
		velocity: 1.61944444444444,
		power: -174.457202039941,
		road: 19766.3765740741,
		acceleration: -0.257407407407407
	},
	{
		id: 1920,
		time: 1919,
		velocity: 0.662222222222222,
		power: -401.509811018618,
		road: 19767.4067592593,
		acceleration: -0.539259259259259
	},
	{
		id: 1921,
		time: 1920,
		velocity: 0,
		power: -191.627129512907,
		road: 19767.8974074074,
		acceleration: -0.539814814814815
	},
	{
		id: 1922,
		time: 1921,
		velocity: 0,
		power: -9.74624587394411,
		road: 19768.0077777778,
		acceleration: -0.220740740740741
	},
	{
		id: 1923,
		time: 1922,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1924,
		time: 1923,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1925,
		time: 1924,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1926,
		time: 1925,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1927,
		time: 1926,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1928,
		time: 1927,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1929,
		time: 1928,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1930,
		time: 1929,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1931,
		time: 1930,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1932,
		time: 1931,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1933,
		time: 1932,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1934,
		time: 1933,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1935,
		time: 1934,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1936,
		time: 1935,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1937,
		time: 1936,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1938,
		time: 1937,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1939,
		time: 1938,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1940,
		time: 1939,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1941,
		time: 1940,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1942,
		time: 1941,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1943,
		time: 1942,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1944,
		time: 1943,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1945,
		time: 1944,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1946,
		time: 1945,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1947,
		time: 1946,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1948,
		time: 1947,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1949,
		time: 1948,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1950,
		time: 1949,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1951,
		time: 1950,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1952,
		time: 1951,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1953,
		time: 1952,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1954,
		time: 1953,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1955,
		time: 1954,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1956,
		time: 1955,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1957,
		time: 1956,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1958,
		time: 1957,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1959,
		time: 1958,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1960,
		time: 1959,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1961,
		time: 1960,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1962,
		time: 1961,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1963,
		time: 1962,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1964,
		time: 1963,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1965,
		time: 1964,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1966,
		time: 1965,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1967,
		time: 1966,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1968,
		time: 1967,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1969,
		time: 1968,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1970,
		time: 1969,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1971,
		time: 1970,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1972,
		time: 1971,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1973,
		time: 1972,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1974,
		time: 1973,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1975,
		time: 1974,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1976,
		time: 1975,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1977,
		time: 1976,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1978,
		time: 1977,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1979,
		time: 1978,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1980,
		time: 1979,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1981,
		time: 1980,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1982,
		time: 1981,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1983,
		time: 1982,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1984,
		time: 1983,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1985,
		time: 1984,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1986,
		time: 1985,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1987,
		time: 1986,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1988,
		time: 1987,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1989,
		time: 1988,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1990,
		time: 1989,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1991,
		time: 1990,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1992,
		time: 1991,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1993,
		time: 1992,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1994,
		time: 1993,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1995,
		time: 1994,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1996,
		time: 1995,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1997,
		time: 1996,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1998,
		time: 1997,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 1999,
		time: 1998,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2000,
		time: 1999,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2001,
		time: 2000,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2002,
		time: 2001,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2003,
		time: 2002,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2004,
		time: 2003,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2005,
		time: 2004,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2006,
		time: 2005,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2007,
		time: 2006,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2008,
		time: 2007,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2009,
		time: 2008,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2010,
		time: 2009,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2011,
		time: 2010,
		velocity: 0,
		power: 0,
		road: 19768.0077777778,
		acceleration: 0
	},
	{
		id: 2012,
		time: 2011,
		velocity: 0,
		power: 125.667171834035,
		road: 19768.2353703704,
		acceleration: 0.455185185185185
	},
	{
		id: 2013,
		time: 2012,
		velocity: 1.36555555555556,
		power: 384.531677614952,
		road: 19768.9220833333,
		acceleration: 0.463055555555556
	},
	{
		id: 2014,
		time: 2013,
		velocity: 1.38916666666667,
		power: 2092.67482815341,
		road: 19770.4828240741,
		acceleration: 1.285
	},
	{
		id: 2015,
		time: 2014,
		velocity: 3.855,
		power: 5351.01638622952,
		road: 19773.5412962963,
		acceleration: 1.71046296296296
	},
	{
		id: 2016,
		time: 2015,
		velocity: 6.49694444444444,
		power: 9143.57884116591,
		road: 19778.379212963,
		acceleration: 1.84842592592593
	},
	{
		id: 2017,
		time: 2016,
		velocity: 6.93444444444444,
		power: 9788.73731954776,
		road: 19784.8599074074,
		acceleration: 1.43712962962963
	},
	{
		id: 2018,
		time: 2017,
		velocity: 8.16638888888889,
		power: 6436.6839860436,
		road: 19792.4263425926,
		acceleration: 0.734351851851851
	},
	{
		id: 2019,
		time: 2018,
		velocity: 8.7,
		power: 6678.96737691115,
		road: 19800.7009259259,
		acceleration: 0.681944444444443
	},
	{
		id: 2020,
		time: 2019,
		velocity: 8.98027777777778,
		power: 3619.73187722102,
		road: 19809.4485185185,
		acceleration: 0.264074074074076
	},
	{
		id: 2021,
		time: 2020,
		velocity: 8.95861111111111,
		power: 24.6860824622048,
		road: 19818.2441203704,
		acceleration: -0.168055555555556
	},
	{
		id: 2022,
		time: 2021,
		velocity: 8.19583333333333,
		power: -1644.75137516389,
		road: 19826.7701851852,
		acceleration: -0.371018518518518
	},
	{
		id: 2023,
		time: 2022,
		velocity: 7.86722222222222,
		power: -3038.27310693416,
		road: 19834.8306944444,
		acceleration: -0.560092592592594
	},
	{
		id: 2024,
		time: 2023,
		velocity: 7.27833333333333,
		power: -1948.61463379989,
		road: 19842.3959722222,
		acceleration: -0.43037037037037
	},
	{
		id: 2025,
		time: 2024,
		velocity: 6.90472222222222,
		power: 1046.44713274999,
		road: 19849.742037037,
		acceleration: -0.00805555555555504
	},
	{
		id: 2026,
		time: 2025,
		velocity: 7.84305555555556,
		power: 4159.4884812586,
		road: 19857.29375,
		acceleration: 0.419351851851852
	},
	{
		id: 2027,
		time: 2026,
		velocity: 8.53638888888889,
		power: 7301.34229996062,
		road: 19865.4433333333,
		acceleration: 0.776388888888889
	},
	{
		id: 2028,
		time: 2027,
		velocity: 9.23388888888889,
		power: 7322.54735117699,
		road: 19874.3279166667,
		acceleration: 0.693611111111109
	},
	{
		id: 2029,
		time: 2028,
		velocity: 9.92388888888889,
		power: 5636.88457572547,
		road: 19883.7833333333,
		acceleration: 0.448055555555557
	},
	{
		id: 2030,
		time: 2029,
		velocity: 9.88055555555555,
		power: 2258.10245528159,
		road: 19893.494537037,
		acceleration: 0.063518518518519
	},
	{
		id: 2031,
		time: 2030,
		velocity: 9.42444444444444,
		power: -999.016757401871,
		road: 19903.0931944444,
		acceleration: -0.288611111111111
	},
	{
		id: 2032,
		time: 2031,
		velocity: 9.05805555555556,
		power: -1802.54371572864,
		road: 19912.3575,
		acceleration: -0.380092592592593
	},
	{
		id: 2033,
		time: 2032,
		velocity: 8.74027777777778,
		power: -2393.94506407546,
		road: 19921.2038888889,
		acceleration: -0.45574074074074
	},
	{
		id: 2034,
		time: 2033,
		velocity: 8.05722222222222,
		power: -3674.29754193785,
		road: 19929.5068055556,
		acceleration: -0.631203703703703
	},
	{
		id: 2035,
		time: 2034,
		velocity: 7.16444444444444,
		power: -3416.88079468612,
		road: 19937.1798148148,
		acceleration: -0.628611111111112
	},
	{
		id: 2036,
		time: 2035,
		velocity: 6.85444444444444,
		power: -2277.79089885661,
		road: 19944.2922222222,
		acceleration: -0.492592592592593
	},
	{
		id: 2037,
		time: 2036,
		velocity: 6.57944444444444,
		power: -1451.29377981095,
		road: 19950.9677777778,
		acceleration: -0.381111111111111
	},
	{
		id: 2038,
		time: 2037,
		velocity: 6.02111111111111,
		power: -2279.54727864451,
		road: 19957.1853703704,
		acceleration: -0.534814814814815
	},
	{
		id: 2039,
		time: 2038,
		velocity: 5.25,
		power: -3199.37283346788,
		road: 19962.7612037037,
		acceleration: -0.748703703703704
	},
	{
		id: 2040,
		time: 2039,
		velocity: 4.33333333333333,
		power: -2348.27802803259,
		road: 19967.6388888889,
		acceleration: -0.647592592592592
	},
	{
		id: 2041,
		time: 2040,
		velocity: 4.07833333333333,
		power: -2262.62334046594,
		road: 19971.8405555556,
		acceleration: -0.704444444444444
	},
	{
		id: 2042,
		time: 2041,
		velocity: 3.13666666666667,
		power: -1386.04505422554,
		road: 19975.4186574074,
		acceleration: -0.542685185185185
	},
	{
		id: 2043,
		time: 2042,
		velocity: 2.70527777777778,
		power: -1657.03311495439,
		road: 19978.3626851852,
		acceleration: -0.725462962962963
	},
	{
		id: 2044,
		time: 2043,
		velocity: 1.90194444444444,
		power: -1787.67524581892,
		road: 19980.4212037037,
		acceleration: -1.04555555555556
	},
	{
		id: 2045,
		time: 2044,
		velocity: 0,
		power: -795.48759464589,
		road: 19981.5060648148,
		acceleration: -0.901759259259259
	},
	{
		id: 2046,
		time: 2045,
		velocity: 0,
		power: -152.090933934373,
		road: 19981.8230555555,
		acceleration: -0.633981481481481
	},
	{
		id: 2047,
		time: 2046,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2048,
		time: 2047,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2049,
		time: 2048,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2050,
		time: 2049,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2051,
		time: 2050,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2052,
		time: 2051,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2053,
		time: 2052,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2054,
		time: 2053,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2055,
		time: 2054,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2056,
		time: 2055,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2057,
		time: 2056,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2058,
		time: 2057,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2059,
		time: 2058,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2060,
		time: 2059,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2061,
		time: 2060,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2062,
		time: 2061,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2063,
		time: 2062,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2064,
		time: 2063,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2065,
		time: 2064,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2066,
		time: 2065,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2067,
		time: 2066,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2068,
		time: 2067,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2069,
		time: 2068,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2070,
		time: 2069,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2071,
		time: 2070,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2072,
		time: 2071,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2073,
		time: 2072,
		velocity: 0,
		power: 0,
		road: 19981.8230555555,
		acceleration: 0
	},
	{
		id: 2074,
		time: 2073,
		velocity: 0,
		power: 137.353157076701,
		road: 19982.0622685185,
		acceleration: 0.478425925925926
	},
	{
		id: 2075,
		time: 2074,
		velocity: 1.43527777777778,
		power: 1281.82665358521,
		road: 19983.1173611111,
		acceleration: 1.15333333333333
	},
	{
		id: 2076,
		time: 2075,
		velocity: 3.46,
		power: 3671.27175131376,
		road: 19985.4968518518,
		acceleration: 1.49546296296296
	},
	{
		id: 2077,
		time: 2076,
		velocity: 4.48638888888889,
		power: 4014.86507144877,
		road: 19989.1374074074,
		acceleration: 1.02666666666667
	},
	{
		id: 2078,
		time: 2077,
		velocity: 4.51527777777778,
		power: 2418.63784148555,
		road: 19993.5131944444,
		acceleration: 0.443796296296296
	},
	{
		id: 2079,
		time: 2078,
		velocity: 4.79138888888889,
		power: 1952.80836851049,
		road: 19998.2575,
		acceleration: 0.293240740740741
	},
	{
		id: 2080,
		time: 2079,
		velocity: 5.36611111111111,
		power: 4382.66396863319,
		road: 20003.5155092592,
		acceleration: 0.734166666666668
	},
	{
		id: 2081,
		time: 2080,
		velocity: 6.71777777777778,
		power: 6127.74017916443,
		road: 20009.5964351852,
		acceleration: 0.911666666666666
	},
	{
		id: 2082,
		time: 2081,
		velocity: 7.52638888888889,
		power: 6519.54034924734,
		road: 20016.5487962963,
		acceleration: 0.831203703703704
	},
	{
		id: 2083,
		time: 2082,
		velocity: 7.85972222222222,
		power: 3342.56912106173,
		road: 20024.0706481481,
		acceleration: 0.307777777777777
	},
	{
		id: 2084,
		time: 2083,
		velocity: 7.64111111111111,
		power: 312.654949244292,
		road: 20031.6879166667,
		acceleration: -0.116944444444444
	},
	{
		id: 2085,
		time: 2084,
		velocity: 7.17555555555556,
		power: -1733.82477798622,
		road: 20039.0438888889,
		acceleration: -0.405648148148148
	},
	{
		id: 2086,
		time: 2085,
		velocity: 6.64277777777778,
		power: -1345.85201873155,
		road: 20046.0181944444,
		acceleration: -0.357685185185185
	},
	{
		id: 2087,
		time: 2086,
		velocity: 6.56805555555556,
		power: -812.049043070713,
		road: 20052.6733333333,
		acceleration: -0.280648148148147
	},
	{
		id: 2088,
		time: 2087,
		velocity: 6.33361111111111,
		power: 2081.39085156209,
		road: 20059.2778703704,
		acceleration: 0.179444444444444
	},
	{
		id: 2089,
		time: 2088,
		velocity: 7.18111111111111,
		power: 4461.28270600649,
		road: 20066.232037037,
		acceleration: 0.519814814814815
	},
	{
		id: 2090,
		time: 2089,
		velocity: 8.1275,
		power: 6219.08135567712,
		road: 20073.7981944444,
		acceleration: 0.704166666666666
	},
	{
		id: 2091,
		time: 2090,
		velocity: 8.44611111111111,
		power: 5770.55687629228,
		road: 20082.0031944444,
		acceleration: 0.57351851851852
	},
	{
		id: 2092,
		time: 2091,
		velocity: 8.90166666666667,
		power: 5511.55048857424,
		road: 20090.7409722222,
		acceleration: 0.492037037037036
	},
	{
		id: 2093,
		time: 2092,
		velocity: 9.60361111111111,
		power: 5859.79391750342,
		road: 20099.9703703704,
		acceleration: 0.491203703703704
	},
	{
		id: 2094,
		time: 2093,
		velocity: 9.91972222222222,
		power: 6717.52958627066,
		road: 20109.7166203704,
		acceleration: 0.5425
	},
	{
		id: 2095,
		time: 2094,
		velocity: 10.5291666666667,
		power: 4953.58886954817,
		road: 20119.8965277778,
		acceleration: 0.324814814814815
	},
	{
		id: 2096,
		time: 2095,
		velocity: 10.5780555555556,
		power: 5458.64171573143,
		road: 20130.4161574074,
		acceleration: 0.354629629629631
	},
	{
		id: 2097,
		time: 2096,
		velocity: 10.9836111111111,
		power: 3666.0813224558,
		road: 20141.1950462963,
		acceleration: 0.163888888888886
	},
	{
		id: 2098,
		time: 2097,
		velocity: 11.0208333333333,
		power: 2650.75920614901,
		road: 20152.0863888889,
		acceleration: 0.0610185185185177
	},
	{
		id: 2099,
		time: 2098,
		velocity: 10.7611111111111,
		power: -767.638869147843,
		road: 20162.8743981481,
		acceleration: -0.267685185185183
	},
	{
		id: 2100,
		time: 2099,
		velocity: 10.1805555555556,
		power: -1226.38469348788,
		road: 20173.3725,
		acceleration: -0.312129629629629
	},
	{
		id: 2101,
		time: 2100,
		velocity: 10.0844444444444,
		power: -1266.3920413736,
		road: 20183.5563425926,
		acceleration: -0.31638888888889
	},
	{
		id: 2102,
		time: 2101,
		velocity: 9.81194444444444,
		power: 666.702479170373,
		road: 20193.5253703704,
		acceleration: -0.113240740740741
	},
	{
		id: 2103,
		time: 2102,
		velocity: 9.84083333333333,
		power: 1613.6867048722,
		road: 20203.4318981481,
		acceleration: -0.011759259259259
	},
	{
		id: 2104,
		time: 2103,
		velocity: 10.0491666666667,
		power: 3247.29463806241,
		road: 20213.4115277778,
		acceleration: 0.157962962962962
	},
	{
		id: 2105,
		time: 2104,
		velocity: 10.2858333333333,
		power: 3133.52276378333,
		road: 20223.5398611111,
		acceleration: 0.139444444444445
	},
	{
		id: 2106,
		time: 2105,
		velocity: 10.2591666666667,
		power: 3209.29090815721,
		road: 20233.8084722222,
		acceleration: 0.141111111111114
	},
	{
		id: 2107,
		time: 2106,
		velocity: 10.4725,
		power: 2013.18573731725,
		road: 20244.1558333333,
		acceleration: 0.016388888888887
	},
	{
		id: 2108,
		time: 2107,
		velocity: 10.335,
		power: 100.464314321787,
		road: 20254.4231018518,
		acceleration: -0.176574074074074
	},
	{
		id: 2109,
		time: 2108,
		velocity: 9.72944444444444,
		power: -3085.26202217414,
		road: 20264.3474537037,
		acceleration: -0.509259259259261
	},
	{
		id: 2110,
		time: 2109,
		velocity: 8.94472222222222,
		power: -3510.41319214784,
		road: 20273.7322685185,
		acceleration: -0.569814814814814
	},
	{
		id: 2111,
		time: 2110,
		velocity: 8.62555555555556,
		power: -3063.91448713118,
		road: 20282.5643055555,
		acceleration: -0.53574074074074
	},
	{
		id: 2112,
		time: 2111,
		velocity: 8.12222222222222,
		power: -1933.02122946011,
		road: 20290.9236111111,
		acceleration: -0.409722222222223
	},
	{
		id: 2113,
		time: 2112,
		velocity: 7.71555555555556,
		power: -2152.43849157405,
		road: 20298.8540277778,
		acceleration: -0.448055555555555
	},
	{
		id: 2114,
		time: 2113,
		velocity: 7.28138888888889,
		power: -1755.35273259882,
		road: 20306.3579166667,
		acceleration: -0.404999999999998
	},
	{
		id: 2115,
		time: 2114,
		velocity: 6.90722222222222,
		power: -1227.30047213117,
		road: 20313.4908333333,
		acceleration: -0.336944444444446
	},
	{
		id: 2116,
		time: 2115,
		velocity: 6.70472222222222,
		power: -50.2882780076675,
		road: 20320.3743981481,
		acceleration: -0.161759259259258
	},
	{
		id: 2117,
		time: 2116,
		velocity: 6.79611111111111,
		power: 1195.75074356027,
		road: 20327.1925,
		acceleration: 0.0308333333333337
	},
	{
		id: 2118,
		time: 2117,
		velocity: 6.99972222222222,
		power: 1726.06179499696,
		road: 20334.0806944444,
		acceleration: 0.10935185185185
	},
	{
		id: 2119,
		time: 2118,
		velocity: 7.03277777777778,
		power: 964.05839853802,
		road: 20341.0193518518,
		acceleration: -0.00842592592592517
	},
	{
		id: 2120,
		time: 2119,
		velocity: 6.77083333333333,
		power: -84.2697614744325,
		road: 20347.8704166667,
		acceleration: -0.166759259259258
	},
	{
		id: 2121,
		time: 2120,
		velocity: 6.49944444444444,
		power: -860.968119904794,
		road: 20354.4937037037,
		acceleration: -0.288796296296296
	},
	{
		id: 2122,
		time: 2121,
		velocity: 6.16638888888889,
		power: -932.191304059732,
		road: 20360.8201388889,
		acceleration: -0.304907407407408
	},
	{
		id: 2123,
		time: 2122,
		velocity: 5.85611111111111,
		power: -90.9195792798451,
		road: 20366.9121296296,
		acceleration: -0.16398148148148
	},
	{
		id: 2124,
		time: 2123,
		velocity: 6.0075,
		power: 380.135945165754,
		road: 20372.8818981481,
		acceleration: -0.0804629629629643
	},
	{
		id: 2125,
		time: 2124,
		velocity: 5.925,
		power: 199.322513838686,
		road: 20378.7558796296,
		acceleration: -0.111111111111111
	},
	{
		id: 2126,
		time: 2125,
		velocity: 5.52277777777778,
		power: -791.967445847607,
		road: 20384.4281018518,
		acceleration: -0.292407407407407
	},
	{
		id: 2127,
		time: 2126,
		velocity: 5.13027777777778,
		power: -1089.29313397784,
		road: 20389.7751851852,
		acceleration: -0.357870370370371
	},
	{
		id: 2128,
		time: 2127,
		velocity: 4.85138888888889,
		power: -615.531142056801,
		road: 20394.8081481481,
		acceleration: -0.27037037037037
	},
	{
		id: 2129,
		time: 2128,
		velocity: 4.71166666666667,
		power: -666.859111015662,
		road: 20399.562037037,
		acceleration: -0.287777777777778
	},
	{
		id: 2130,
		time: 2129,
		velocity: 4.26694444444444,
		power: -1422.09168500799,
		road: 20403.9316203704,
		acceleration: -0.480833333333333
	},
	{
		id: 2131,
		time: 2130,
		velocity: 3.40888888888889,
		power: -2355.4218476969,
		road: 20407.660462963,
		acceleration: -0.800648148148148
	},
	{
		id: 2132,
		time: 2131,
		velocity: 2.30972222222222,
		power: -2190.72482932413,
		road: 20410.5191203704,
		acceleration: -0.939722222222222
	},
	{
		id: 2133,
		time: 2132,
		velocity: 1.44777777777778,
		power: -1327.73552390376,
		road: 20412.4874537037,
		acceleration: -0.840925925925926
	},
	{
		id: 2134,
		time: 2133,
		velocity: 0.886111111111111,
		power: -707.331864221071,
		road: 20413.6503703704,
		acceleration: -0.769907407407408
	},
	{
		id: 2135,
		time: 2134,
		velocity: 0,
		power: -180.495879464374,
		road: 20414.187037037,
		acceleration: -0.482592592592593
	},
	{
		id: 2136,
		time: 2135,
		velocity: 0,
		power: -23.4829290123457,
		road: 20414.3347222222,
		acceleration: -0.29537037037037
	},
	{
		id: 2137,
		time: 2136,
		velocity: 0,
		power: 0,
		road: 20414.3347222222,
		acceleration: 0
	},
	{
		id: 2138,
		time: 2137,
		velocity: 0,
		power: 52.202958543635,
		road: 20414.4718518518,
		acceleration: 0.274259259259259
	},
	{
		id: 2139,
		time: 2138,
		velocity: 0.822777777777778,
		power: 348.170463073714,
		road: 20415.0183796296,
		acceleration: 0.544537037037037
	},
	{
		id: 2140,
		time: 2139,
		velocity: 1.63361111111111,
		power: 1283.23569935316,
		road: 20416.3006944444,
		acceleration: 0.927037037037037
	},
	{
		id: 2141,
		time: 2140,
		velocity: 2.78111111111111,
		power: 2007.78537994104,
		road: 20418.4694444444,
		acceleration: 0.845833333333333
	},
	{
		id: 2142,
		time: 2141,
		velocity: 3.36027777777778,
		power: 2905.16104462553,
		road: 20421.4998611111,
		acceleration: 0.8775
	},
	{
		id: 2143,
		time: 2142,
		velocity: 4.26611111111111,
		power: 3226.93177531318,
		road: 20425.343287037,
		acceleration: 0.748518518518519
	},
	{
		id: 2144,
		time: 2143,
		velocity: 5.02666666666667,
		power: 3939.68115281462,
		road: 20429.9422222222,
		acceleration: 0.7625
	},
	{
		id: 2145,
		time: 2144,
		velocity: 5.64777777777778,
		power: 2892.44020038914,
		road: 20435.1437037037,
		acceleration: 0.442592592592593
	},
	{
		id: 2146,
		time: 2145,
		velocity: 5.59388888888889,
		power: 2664.14924854067,
		road: 20440.744212963,
		acceleration: 0.355462962962963
	},
	{
		id: 2147,
		time: 2146,
		velocity: 6.09305555555556,
		power: 4571.75780193327,
		road: 20446.8425462963,
		acceleration: 0.640185185185185
	},
	{
		id: 2148,
		time: 2147,
		velocity: 7.56833333333333,
		power: 6924.3685684166,
		road: 20453.7137037037,
		acceleration: 0.905462962962962
	},
	{
		id: 2149,
		time: 2148,
		velocity: 8.31027777777778,
		power: 9786.1151225917,
		road: 20461.6075925926,
		acceleration: 1.14
	},
	{
		id: 2150,
		time: 2149,
		velocity: 9.51305555555555,
		power: 10091.4069618993,
		road: 20470.5758796296,
		acceleration: 1.0087962962963
	},
	{
		id: 2151,
		time: 2150,
		velocity: 10.5947222222222,
		power: 9742.30738147098,
		road: 20480.47375,
		acceleration: 0.850370370370371
	},
	{
		id: 2152,
		time: 2151,
		velocity: 10.8613888888889,
		power: 6194.80052696558,
		road: 20491.0102314815,
		acceleration: 0.42685185185185
	},
	{
		id: 2153,
		time: 2152,
		velocity: 10.7936111111111,
		power: 4222.07869033824,
		road: 20501.8671759259,
		acceleration: 0.214074074074075
	},
	{
		id: 2154,
		time: 2153,
		velocity: 11.2369444444444,
		power: 6177.83600348868,
		road: 20513.0227777778,
		acceleration: 0.383240740740741
	},
	{
		id: 2155,
		time: 2154,
		velocity: 12.0111111111111,
		power: 7916.38092787161,
		road: 20524.6260185185,
		acceleration: 0.512037037037038
	},
	{
		id: 2156,
		time: 2155,
		velocity: 12.3297222222222,
		power: 5701.52725351904,
		road: 20536.6299074074,
		acceleration: 0.289259259259261
	},
	{
		id: 2157,
		time: 2156,
		velocity: 12.1047222222222,
		power: 2176.52737675546,
		road: 20548.7671759259,
		acceleration: -0.0225000000000009
	},
	{
		id: 2158,
		time: 2157,
		velocity: 11.9436111111111,
		power: 410.258105748912,
		road: 20560.8064814815,
		acceleration: -0.173425925925926
	},
	{
		id: 2159,
		time: 2158,
		velocity: 11.8094444444444,
		power: -110.378927297952,
		road: 20572.6509259259,
		acceleration: -0.216296296296298
	},
	{
		id: 2160,
		time: 2159,
		velocity: 11.4558333333333,
		power: 260.248066715086,
		road: 20584.2969907407,
		acceleration: -0.180462962962961
	},
	{
		id: 2161,
		time: 2160,
		velocity: 11.4022222222222,
		power: 69.433715659995,
		road: 20595.7552777778,
		acceleration: -0.195092592592594
	},
	{
		id: 2162,
		time: 2161,
		velocity: 11.2241666666667,
		power: 1193.78219075566,
		road: 20607.0715277778,
		acceleration: -0.0889814814814827
	},
	{
		id: 2163,
		time: 2162,
		velocity: 11.1888888888889,
		power: 2185.76135693469,
		road: 20618.3454166667,
		acceleration: 0.00425925925926052
	},
	{
		id: 2164,
		time: 2163,
		velocity: 11.415,
		power: 3730.57056710402,
		road: 20629.6937962963,
		acceleration: 0.144722222222223
	},
	{
		id: 2165,
		time: 2164,
		velocity: 11.6583333333333,
		power: 5450.6531364701,
		road: 20641.2601851852,
		acceleration: 0.291296296296297
	},
	{
		id: 2166,
		time: 2165,
		velocity: 12.0627777777778,
		power: 6117.64189989218,
		road: 20653.1387037037,
		acceleration: 0.332962962962963
	},
	{
		id: 2167,
		time: 2166,
		velocity: 12.4138888888889,
		power: 7748.7985669524,
		road: 20665.4085185185,
		acceleration: 0.449629629629628
	},
	{
		id: 2168,
		time: 2167,
		velocity: 13.0072222222222,
		power: 9014.65458386833,
		road: 20678.1638425926,
		acceleration: 0.521388888888888
	},
	{
		id: 2169,
		time: 2168,
		velocity: 13.6269444444444,
		power: 9386.90618704402,
		road: 20691.4368981481,
		acceleration: 0.514074074074074
	},
	{
		id: 2170,
		time: 2169,
		velocity: 13.9561111111111,
		power: 7875.62324572078,
		road: 20705.1509722222,
		acceleration: 0.367962962962963
	},
	{
		id: 2171,
		time: 2170,
		velocity: 14.1111111111111,
		power: 5018.12403846109,
		road: 20719.1184259259,
		acceleration: 0.138796296296297
	},
	{
		id: 2172,
		time: 2171,
		velocity: 14.0433333333333,
		power: 3835.53945911242,
		road: 20733.1786574074,
		acceleration: 0.0467592592592609
	},
	{
		id: 2173,
		time: 2172,
		velocity: 14.0963888888889,
		power: 1898.4688031346,
		road: 20747.2138425926,
		acceleration: -0.0968518518518522
	},
	{
		id: 2174,
		time: 2173,
		velocity: 13.8205555555556,
		power: 801.930984960509,
		road: 20761.1126388889,
		acceleration: -0.175925925925926
	},
	{
		id: 2175,
		time: 2174,
		velocity: 13.5155555555556,
		power: -818.840802400402,
		road: 20774.7757407407,
		acceleration: -0.295462962962965
	},
	{
		id: 2176,
		time: 2175,
		velocity: 13.21,
		power: -763.439567794823,
		road: 20788.1470833333,
		acceleration: -0.288055555555555
	},
	{
		id: 2177,
		time: 2176,
		velocity: 12.9563888888889,
		power: -1021.12694457641,
		road: 20801.2215740741,
		acceleration: -0.305648148148146
	},
	{
		id: 2178,
		time: 2177,
		velocity: 12.5986111111111,
		power: -877.99859338297,
		road: 20813.9975,
		acceleration: -0.291481481481483
	},
	{
		id: 2179,
		time: 2178,
		velocity: 12.3355555555556,
		power: -1530.45260222723,
		road: 20826.4558333333,
		acceleration: -0.343703703703703
	},
	{
		id: 2180,
		time: 2179,
		velocity: 11.9252777777778,
		power: -1997.56235549395,
		road: 20838.550787037,
		acceleration: -0.383055555555556
	},
	{
		id: 2181,
		time: 2180,
		velocity: 11.4494444444444,
		power: -7616.13465821607,
		road: 20850.0048148148,
		acceleration: -0.898796296296293
	},
	{
		id: 2182,
		time: 2181,
		velocity: 9.63916666666667,
		power: -8955.08036102688,
		road: 20860.4656944444,
		acceleration: -1.0875
	},
	{
		id: 2183,
		time: 2182,
		velocity: 8.66277777777778,
		power: -9048.51878133961,
		road: 20869.7848148148,
		acceleration: -1.19601851851852
	},
	{
		id: 2184,
		time: 2183,
		velocity: 7.86138888888889,
		power: -5070.17063103049,
		road: 20878.1025,
		acceleration: -0.806851851851854
	},
	{
		id: 2185,
		time: 2184,
		velocity: 7.21861111111111,
		power: -4031.07119562646,
		road: 20885.6565277778,
		acceleration: -0.720462962962962
	},
	{
		id: 2186,
		time: 2185,
		velocity: 6.50138888888889,
		power: -5146.54781296747,
		road: 20892.3708796296,
		acceleration: -0.958888888888888
	},
	{
		id: 2187,
		time: 2186,
		velocity: 4.98472222222222,
		power: -6478.34470467634,
		road: 20897.9189814815,
		acceleration: -1.37361111111111
	},
	{
		id: 2188,
		time: 2187,
		velocity: 3.09777777777778,
		power: -7284.64433091161,
		road: 20901.696712963,
		acceleration: -2.16712962962963
	},
	{
		id: 2189,
		time: 2188,
		velocity: 0,
		power: -2706.9801811018,
		road: 20903.5600925926,
		acceleration: -1.66157407407407
	},
	{
		id: 2190,
		time: 2189,
		velocity: 0,
		power: -442.686755815464,
		road: 20904.0763888889,
		acceleration: -1.03259259259259
	},
	{
		id: 2191,
		time: 2190,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2192,
		time: 2191,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2193,
		time: 2192,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2194,
		time: 2193,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2195,
		time: 2194,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2196,
		time: 2195,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2197,
		time: 2196,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2198,
		time: 2197,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2199,
		time: 2198,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2200,
		time: 2199,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2201,
		time: 2200,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2202,
		time: 2201,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2203,
		time: 2202,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2204,
		time: 2203,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2205,
		time: 2204,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2206,
		time: 2205,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2207,
		time: 2206,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2208,
		time: 2207,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2209,
		time: 2208,
		velocity: 0,
		power: 0,
		road: 20904.0763888889,
		acceleration: 0
	},
	{
		id: 2210,
		time: 2209,
		velocity: 0,
		power: 247.684213749666,
		road: 20904.4074074074,
		acceleration: 0.662037037037037
	},
	{
		id: 2211,
		time: 2210,
		velocity: 1.98611111111111,
		power: 2334.24920276942,
		road: 20905.8551851852,
		acceleration: 1.57148148148148
	},
	{
		id: 2212,
		time: 2211,
		velocity: 4.71444444444445,
		power: 6912.33365570797,
		road: 20909.1326851852,
		acceleration: 2.08796296296296
	},
	{
		id: 2213,
		time: 2212,
		velocity: 6.26388888888889,
		power: 7258.50494355205,
		road: 20914.1453240741,
		acceleration: 1.38231481481481
	},
	{
		id: 2214,
		time: 2213,
		velocity: 6.13305555555556,
		power: 4165.73365734735,
		road: 20920.1407407407,
		acceleration: 0.583240740740741
	},
	{
		id: 2215,
		time: 2214,
		velocity: 6.46416666666667,
		power: 2895.02780434998,
		road: 20926.5884722222,
		acceleration: 0.321388888888889
	},
	{
		id: 2216,
		time: 2215,
		velocity: 7.22805555555556,
		power: 4314.42617322227,
		road: 20933.450462963,
		acceleration: 0.50712962962963
	},
	{
		id: 2217,
		time: 2216,
		velocity: 7.65444444444444,
		power: 2580.82810787355,
		road: 20940.6753703704,
		acceleration: 0.218703703703703
	},
	{
		id: 2218,
		time: 2217,
		velocity: 7.12027777777778,
		power: -690.047149515159,
		road: 20947.8809722222,
		acceleration: -0.257314814814815
	},
	{
		id: 2219,
		time: 2218,
		velocity: 6.45611111111111,
		power: -2315.6769358732,
		road: 20954.7026388889,
		acceleration: -0.510555555555555
	},
	{
		id: 2220,
		time: 2219,
		velocity: 6.12277777777778,
		power: -2390.10025799919,
		road: 20960.9943981482,
		acceleration: -0.54925925925926
	},
	{
		id: 2221,
		time: 2220,
		velocity: 5.4725,
		power: -888.777141094518,
		road: 20966.8584259259,
		acceleration: -0.306203703703702
	},
	{
		id: 2222,
		time: 2221,
		velocity: 5.5375,
		power: -664.927609946053,
		road: 20972.4341666667,
		acceleration: -0.270370370370371
	},
	{
		id: 2223,
		time: 2222,
		velocity: 5.31166666666667,
		power: -198.288166373575,
		road: 20977.7834722222,
		acceleration: -0.1825
	},
	{
		id: 2224,
		time: 2223,
		velocity: 4.925,
		power: -454.315902805787,
		road: 20982.9238888889,
		acceleration: -0.235277777777778
	},
	{
		id: 2225,
		time: 2224,
		velocity: 4.83166666666667,
		power: 19.5787565905942,
		road: 20987.8781481482,
		acceleration: -0.137037037037037
	},
	{
		id: 2226,
		time: 2225,
		velocity: 4.90055555555556,
		power: 1001.96559832538,
		road: 20992.8005092593,
		acceleration: 0.0732407407407409
	},
	{
		id: 2227,
		time: 2226,
		velocity: 5.14472222222222,
		power: 1464.43860069567,
		road: 20997.8415277778,
		acceleration: 0.164074074074075
	},
	{
		id: 2228,
		time: 2227,
		velocity: 5.32388888888889,
		power: 1786.8317290059,
		road: 21003.0729166667,
		acceleration: 0.216666666666666
	},
	{
		id: 2229,
		time: 2228,
		velocity: 5.55055555555556,
		power: 1755.7484980253,
		road: 21008.5105092593,
		acceleration: 0.195740740740741
	},
	{
		id: 2230,
		time: 2229,
		velocity: 5.73194444444445,
		power: 1481.69880723243,
		road: 21014.1125925926,
		acceleration: 0.13324074074074
	},
	{
		id: 2231,
		time: 2230,
		velocity: 5.72361111111111,
		power: 536.886045922844,
		road: 21019.7586574074,
		acceleration: -0.0452777777777778
	},
	{
		id: 2232,
		time: 2231,
		velocity: 5.41472222222222,
		power: -99.2573230036803,
		road: 21025.3003240741,
		acceleration: -0.163518518518519
	},
	{
		id: 2233,
		time: 2232,
		velocity: 5.24138888888889,
		power: -96.7257564047818,
		road: 21030.6789351852,
		acceleration: -0.162592592592593
	},
	{
		id: 2234,
		time: 2233,
		velocity: 5.23583333333333,
		power: 357.854764695126,
		road: 21035.9405555556,
		acceleration: -0.0713888888888885
	},
	{
		id: 2235,
		time: 2234,
		velocity: 5.20055555555556,
		power: 669.62877179251,
		road: 21041.1625925926,
		acceleration: -0.00777777777777722
	},
	{
		id: 2236,
		time: 2235,
		velocity: 5.21805555555556,
		power: 185.859765992043,
		road: 21046.3284722222,
		acceleration: -0.104537037037037
	},
	{
		id: 2237,
		time: 2236,
		velocity: 4.92222222222222,
		power: 981.150534499987,
		road: 21051.4713425926,
		acceleration: 0.0585185185185182
	},
	{
		id: 2238,
		time: 2237,
		velocity: 5.37611111111111,
		power: 1100.44612987293,
		road: 21056.6832407407,
		acceleration: 0.0795370370370367
	},
	{
		id: 2239,
		time: 2238,
		velocity: 5.45666666666667,
		power: 682.140816886259,
		road: 21061.9318518519,
		acceleration: -0.00611111111111118
	},
	{
		id: 2240,
		time: 2239,
		velocity: 4.90388888888889,
		power: -920.631021411298,
		road: 21067.0110648148,
		acceleration: -0.332685185185184
	},
	{
		id: 2241,
		time: 2240,
		velocity: 4.37805555555556,
		power: -207.761022219676,
		road: 21071.8310185185,
		acceleration: -0.185833333333334
	},
	{
		id: 2242,
		time: 2241,
		velocity: 4.89916666666667,
		power: 517.472205288217,
		road: 21076.5458796296,
		acceleration: -0.0243518518518515
	},
	{
		id: 2243,
		time: 2242,
		velocity: 4.83083333333333,
		power: 1066.7919967503,
		road: 21081.296712963,
		acceleration: 0.0962962962962957
	},
	{
		id: 2244,
		time: 2243,
		velocity: 4.66694444444444,
		power: 345.011617448952,
		road: 21086.0637037037,
		acceleration: -0.0639814814814805
	},
	{
		id: 2245,
		time: 2244,
		velocity: 4.70722222222222,
		power: 298.328466236421,
		road: 21090.7622222222,
		acceleration: -0.0729629629629631
	},
	{
		id: 2246,
		time: 2245,
		velocity: 4.61194444444445,
		power: 524.586034974521,
		road: 21095.4138425926,
		acceleration: -0.020833333333333
	},
	{
		id: 2247,
		time: 2246,
		velocity: 4.60444444444444,
		power: 70.9739102969777,
		road: 21099.9936111111,
		acceleration: -0.122870370370371
	},
	{
		id: 2248,
		time: 2247,
		velocity: 4.33861111111111,
		power: 443.254062950073,
		road: 21104.4943981481,
		acceleration: -0.0350925925925925
	},
	{
		id: 2249,
		time: 2248,
		velocity: 4.50666666666667,
		power: 576.129553863924,
		road: 21108.9759722222,
		acceleration: -0.00333333333333385
	},
	{
		id: 2250,
		time: 2249,
		velocity: 4.59444444444444,
		power: 2182.97167743547,
		road: 21113.6328240741,
		acceleration: 0.353888888888889
	},
	{
		id: 2251,
		time: 2250,
		velocity: 5.40027777777778,
		power: 2773.25988730875,
		road: 21118.6846296296,
		acceleration: 0.436018518518519
	},
	{
		id: 2252,
		time: 2251,
		velocity: 5.81472222222222,
		power: 3825.80024526479,
		road: 21124.2440740741,
		acceleration: 0.579259259259259
	},
	{
		id: 2253,
		time: 2252,
		velocity: 6.33222222222222,
		power: 3085.50471438837,
		road: 21130.2876851852,
		acceleration: 0.389074074074074
	},
	{
		id: 2254,
		time: 2253,
		velocity: 6.5675,
		power: 2914.24865093787,
		road: 21136.6899537037,
		acceleration: 0.328240740740741
	},
	{
		id: 2255,
		time: 2254,
		velocity: 6.79944444444445,
		power: 1435.90922206639,
		road: 21143.2946759259,
		acceleration: 0.0766666666666671
	},
	{
		id: 2256,
		time: 2255,
		velocity: 6.56222222222222,
		power: 544.535043962511,
		road: 21149.9050462963,
		acceleration: -0.0653703703703705
	},
	{
		id: 2257,
		time: 2256,
		velocity: 6.37138888888889,
		power: 0.52677996187265,
		road: 21156.4071759259,
		acceleration: -0.151111111111111
	},
	{
		id: 2258,
		time: 2257,
		velocity: 6.34611111111111,
		power: 94.4601834732794,
		road: 21162.7664814815,
		acceleration: -0.134537037037037
	},
	{
		id: 2259,
		time: 2258,
		velocity: 6.15861111111111,
		power: -193.990485827884,
		road: 21168.9675462963,
		acceleration: -0.181944444444444
	},
	{
		id: 2260,
		time: 2259,
		velocity: 5.82555555555556,
		power: -796.47623245209,
		road: 21174.9337037037,
		acceleration: -0.287870370370371
	},
	{
		id: 2261,
		time: 2260,
		velocity: 5.4825,
		power: -934.468590552762,
		road: 21180.5963888889,
		acceleration: -0.319074074074074
	},
	{
		id: 2262,
		time: 2261,
		velocity: 5.20138888888889,
		power: -385.53856786437,
		road: 21185.9900462963,
		acceleration: -0.218981481481482
	},
	{
		id: 2263,
		time: 2262,
		velocity: 5.16861111111111,
		power: 264.870225895901,
		road: 21191.2293981481,
		acceleration: -0.0896296296296297
	},
	{
		id: 2264,
		time: 2263,
		velocity: 5.21361111111111,
		power: 514.793380670516,
		road: 21196.4050462963,
		acceleration: -0.0377777777777775
	},
	{
		id: 2265,
		time: 2264,
		velocity: 5.08805555555556,
		power: 1161.45555018354,
		road: 21201.607962963,
		acceleration: 0.0923148148148139
	},
	{
		id: 2266,
		time: 2265,
		velocity: 5.44555555555556,
		power: 696.268086823325,
		road: 21206.8554166667,
		acceleration: -0.00324074074073977
	},
	{
		id: 2267,
		time: 2266,
		velocity: 5.20388888888889,
		power: 44.557190711494,
		road: 21212.034537037,
		acceleration: -0.133425925925926
	},
	{
		id: 2268,
		time: 2267,
		velocity: 4.68777777777778,
		power: -719.985796097409,
		road: 21217,
		acceleration: -0.293888888888889
	},
	{
		id: 2269,
		time: 2268,
		velocity: 4.56388888888889,
		power: -424.602673039503,
		road: 21221.7010648148,
		acceleration: -0.234907407407408
	},
	{
		id: 2270,
		time: 2269,
		velocity: 4.49916666666667,
		power: -185.921258821481,
		road: 21226.1935185185,
		acceleration: -0.182314814814814
	},
	{
		id: 2271,
		time: 2270,
		velocity: 4.14083333333333,
		power: -100.983263904634,
		road: 21230.5135648148,
		acceleration: -0.162500000000001
	},
	{
		id: 2272,
		time: 2271,
		velocity: 4.07638888888889,
		power: -188.029923265649,
		road: 21234.6599537037,
		acceleration: -0.184814814814815
	},
	{
		id: 2273,
		time: 2272,
		velocity: 3.94472222222222,
		power: 4.32348850175979,
		road: 21238.6463425926,
		acceleration: -0.135185185185185
	},
	{
		id: 2274,
		time: 2273,
		velocity: 3.73527777777778,
		power: -111.210472968651,
		road: 21242.482037037,
		acceleration: -0.166203703703703
	},
	{
		id: 2275,
		time: 2274,
		velocity: 3.57777777777778,
		power: 34.2296924498697,
		road: 21246.1719907407,
		acceleration: -0.125277777777779
	},
	{
		id: 2276,
		time: 2275,
		velocity: 3.56888888888889,
		power: 6.84455638224459,
		road: 21249.7330555555,
		acceleration: -0.1325
	},
	{
		id: 2277,
		time: 2276,
		velocity: 3.33777777777778,
		power: -99.0950785919931,
		road: 21253.1456018518,
		acceleration: -0.164537037037037
	},
	{
		id: 2278,
		time: 2277,
		velocity: 3.08416666666667,
		power: -485.497396122335,
		road: 21256.3289814815,
		acceleration: -0.293796296296296
	},
	{
		id: 2279,
		time: 2278,
		velocity: 2.6875,
		power: -578.543999393568,
		road: 21259.1930092593,
		acceleration: -0.344907407407407
	},
	{
		id: 2280,
		time: 2279,
		velocity: 2.30305555555556,
		power: -199.039800342512,
		road: 21261.7784259259,
		acceleration: -0.212314814814815
	},
	{
		id: 2281,
		time: 2280,
		velocity: 2.44722222222222,
		power: -245.566155848124,
		road: 21264.1375462963,
		acceleration: -0.240277777777778
	},
	{
		id: 2282,
		time: 2281,
		velocity: 1.96666666666667,
		power: -109.340466531258,
		road: 21266.2846759259,
		acceleration: -0.183703703703703
	},
	{
		id: 2283,
		time: 2282,
		velocity: 1.75194444444444,
		power: -477.804656650934,
		road: 21268.1394444444,
		acceleration: -0.401018518518519
	},
	{
		id: 2284,
		time: 2283,
		velocity: 1.24416666666667,
		power: -407.168972930776,
		road: 21269.5803703704,
		acceleration: -0.426666666666667
	},
	{
		id: 2285,
		time: 2284,
		velocity: 0.686666666666667,
		power: -404.369780457985,
		road: 21270.5159722222,
		acceleration: -0.583981481481482
	},
	{
		id: 2286,
		time: 2285,
		velocity: 0,
		power: -118.681114958659,
		road: 21270.9522222222,
		acceleration: -0.414722222222222
	},
	{
		id: 2287,
		time: 2286,
		velocity: 0,
		power: -10.9894374269006,
		road: 21271.0666666667,
		acceleration: -0.228888888888889
	},
	{
		id: 2288,
		time: 2287,
		velocity: 0,
		power: 0,
		road: 21271.0666666667,
		acceleration: 0
	},
	{
		id: 2289,
		time: 2288,
		velocity: 0,
		power: 0,
		road: 21271.0666666667,
		acceleration: 0
	},
	{
		id: 2290,
		time: 2289,
		velocity: 0,
		power: 0,
		road: 21271.0666666667,
		acceleration: 0
	},
	{
		id: 2291,
		time: 2290,
		velocity: 0,
		power: 0,
		road: 21271.0666666667,
		acceleration: 0
	},
	{
		id: 2292,
		time: 2291,
		velocity: 0,
		power: 0,
		road: 21271.0666666667,
		acceleration: 0
	},
	{
		id: 2293,
		time: 2292,
		velocity: 0,
		power: 0,
		road: 21271.0666666667,
		acceleration: 0
	},
	{
		id: 2294,
		time: 2293,
		velocity: 0,
		power: 0,
		road: 21271.0666666667,
		acceleration: 0
	},
	{
		id: 2295,
		time: 2294,
		velocity: 0,
		power: 0,
		road: 21271.0666666667,
		acceleration: 0
	}
];
export default train1;
