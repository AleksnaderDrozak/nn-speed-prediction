export const train4 = 
[
	{
		id: 1,
		time: 0,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 69.7774875362614,
		road: 0.162638888888889,
		acceleration: 0.325277777777778
	},
	{
		id: 3,
		time: 2,
		velocity: 0.975833333333333,
		power: 39.3180598678444,
		road: 0.487916666666667,
		acceleration: 0
	},
	{
		id: 4,
		time: 3,
		velocity: 0,
		power: 39.3180598678444,
		road: 0.813194444444444,
		acceleration: 0
	},
	{
		id: 5,
		time: 4,
		velocity: 0,
		power: -17.7996502336544,
		road: 1.03171296296296,
		acceleration: -0.213518518518518
	},
	{
		id: 6,
		time: 5,
		velocity: 0.335277777777778,
		power: 13.5032762074536,
		road: 1.14347222222222,
		acceleration: 0
	},
	{
		id: 7,
		time: 6,
		velocity: 0,
		power: 13.5032762074536,
		road: 1.25523148148148,
		acceleration: 0
	},
	{
		id: 8,
		time: 7,
		velocity: 0,
		power: 0.834880880441845,
		road: 1.31111111111111,
		acceleration: -0.111759259259259
	},
	{
		id: 9,
		time: 8,
		velocity: 0,
		power: 0,
		road: 1.31111111111111,
		acceleration: 0
	},
	{
		id: 10,
		time: 9,
		velocity: 0,
		power: 0,
		road: 1.31111111111111,
		acceleration: 0
	},
	{
		id: 11,
		time: 10,
		velocity: 0,
		power: 0,
		road: 1.31111111111111,
		acceleration: 0
	},
	{
		id: 12,
		time: 11,
		velocity: 0,
		power: 0,
		road: 1.31111111111111,
		acceleration: 0
	},
	{
		id: 13,
		time: 12,
		velocity: 0,
		power: 20.7446607909906,
		road: 1.38861111111111,
		acceleration: 0.155
	},
	{
		id: 14,
		time: 13,
		velocity: 0.465,
		power: 151.641328584393,
		road: 1.72074074074074,
		acceleration: 0.354259259259259
	},
	{
		id: 15,
		time: 14,
		velocity: 1.06277777777778,
		power: 482.080143819682,
		road: 2.49467592592593,
		acceleration: 0.529351851851852
	},
	{
		id: 16,
		time: 15,
		velocity: 1.58805555555556,
		power: 905.192481898791,
		road: 3.82726851851852,
		acceleration: 0.587962962962963
	},
	{
		id: 17,
		time: 16,
		velocity: 2.22888888888889,
		power: 1033.61212816773,
		road: 5.68282407407407,
		acceleration: 0.457962962962963
	},
	{
		id: 18,
		time: 17,
		velocity: 2.43666666666667,
		power: -322.645563956287,
		road: 7.61453703703704,
		acceleration: -0.305648148148148
	},
	{
		id: 19,
		time: 18,
		velocity: 0.671111111111111,
		power: -535.182991186721,
		road: 9.14458333333333,
		acceleration: -0.497685185185185
	},
	{
		id: 20,
		time: 19,
		velocity: 0.735833333333333,
		power: -374.649297328761,
		road: 10.1687962962963,
		acceleration: -0.513981481481481
	},
	{
		id: 21,
		time: 20,
		velocity: 0.894722222222222,
		power: 64.2637965862042,
		road: 10.9174074074074,
		acceleration: -0.0372222222222223
	},
	{
		id: 22,
		time: 21,
		velocity: 0.559444444444444,
		power: 50.3939366436783,
		road: 11.6212962962963,
		acceleration: -0.0522222222222223
	},
	{
		id: 23,
		time: 22,
		velocity: 0.579166666666667,
		power: 132.319818014971,
		road: 12.3332407407407,
		acceleration: 0.0683333333333334
	},
	{
		id: 24,
		time: 23,
		velocity: 1.09972222222222,
		power: 342.808661625027,
		road: 13.2194444444444,
		acceleration: 0.280185185185185
	},
	{
		id: 25,
		time: 24,
		velocity: 1.4,
		power: 163.731847798088,
		road: 14.2643518518519,
		acceleration: 0.0372222222222223
	},
	{
		id: 26,
		time: 25,
		velocity: 0.690833333333333,
		power: -76.6817364509748,
		road: 15.2216203703704,
		acceleration: -0.2125
	},
	{
		id: 27,
		time: 26,
		velocity: 0.462222222222222,
		power: -40.4738967707122,
		road: 15.9806018518518,
		acceleration: -0.184074074074074
	},
	{
		id: 28,
		time: 27,
		velocity: 0.847777777777778,
		power: 345.428129199605,
		road: 16.8047222222222,
		acceleration: 0.314351851851852
	},
	{
		id: 29,
		time: 28,
		velocity: 1.63388888888889,
		power: 1329.47949913293,
		road: 18.2178240740741,
		acceleration: 0.863611111111111
	},
	{
		id: 30,
		time: 29,
		velocity: 3.05305555555556,
		power: 2536.89215578036,
		road: 20.5666666666667,
		acceleration: 1.00787037037037
	},
	{
		id: 31,
		time: 30,
		velocity: 3.87138888888889,
		power: 2768.9784693333,
		road: 23.8034259259259,
		acceleration: 0.767962962962963
	},
	{
		id: 32,
		time: 31,
		velocity: 3.93777777777778,
		power: 1930.40182878848,
		road: 27.6225462962963,
		acceleration: 0.396759259259259
	},
	{
		id: 33,
		time: 32,
		velocity: 4.24333333333333,
		power: 1309.07664919689,
		road: 31.7390277777778,
		acceleration: 0.197962962962963
	},
	{
		id: 34,
		time: 33,
		velocity: 4.46527777777778,
		power: 1133.40155289889,
		road: 36.0248611111111,
		acceleration: 0.14074074074074
	},
	{
		id: 35,
		time: 34,
		velocity: 4.36,
		power: 100.984935281216,
		road: 40.324537037037,
		acceleration: -0.113055555555555
	},
	{
		id: 36,
		time: 35,
		velocity: 3.90416666666667,
		power: -1468.70687709599,
		road: 44.3052314814815,
		acceleration: -0.524907407407407
	},
	{
		id: 37,
		time: 36,
		velocity: 2.89055555555556,
		power: -1444.71326473413,
		road: 47.734537037037,
		acceleration: -0.577870370370371
	},
	{
		id: 38,
		time: 37,
		velocity: 2.62638888888889,
		power: -482.155679442462,
		road: 50.7237037037037,
		acceleration: -0.302407407407407
	},
	{
		id: 39,
		time: 38,
		velocity: 2.99694444444444,
		power: 1206.45920844198,
		road: 53.7084259259259,
		acceleration: 0.293518518518518
	},
	{
		id: 40,
		time: 39,
		velocity: 3.77111111111111,
		power: 2833.23380050128,
		road: 57.2001388888889,
		acceleration: 0.720462962962964
	},
	{
		id: 41,
		time: 40,
		velocity: 4.78777777777778,
		power: 2734.98081416157,
		road: 61.3320833333333,
		acceleration: 0.559999999999999
	},
	{
		id: 42,
		time: 41,
		velocity: 4.67694444444444,
		power: 1818.95844755625,
		road: 65.8848148148148,
		acceleration: 0.281574074074075
	},
	{
		id: 43,
		time: 42,
		velocity: 4.61583333333333,
		power: 717.915038796552,
		road: 70.58875,
		acceleration: 0.020833333333333
	},
	{
		id: 44,
		time: 43,
		velocity: 4.85027777777778,
		power: 1427.19248069454,
		road: 75.3893981481482,
		acceleration: 0.172592592592593
	},
	{
		id: 45,
		time: 44,
		velocity: 5.19472222222222,
		power: 2751.69023007396,
		road: 80.4892592592593,
		acceleration: 0.425833333333333
	},
	{
		id: 46,
		time: 45,
		velocity: 5.89333333333333,
		power: 3714.48541027618,
		road: 86.0791203703704,
		acceleration: 0.554166666666667
	},
	{
		id: 47,
		time: 46,
		velocity: 6.51277777777778,
		power: 4339.68416501081,
		road: 92.2419907407407,
		acceleration: 0.591851851851851
	},
	{
		id: 48,
		time: 47,
		velocity: 6.97027777777778,
		power: 3451.3240626526,
		road: 98.8972685185185,
		acceleration: 0.392962962962963
	},
	{
		id: 49,
		time: 48,
		velocity: 7.07222222222222,
		power: 2662.84698273942,
		road: 105.872314814815,
		acceleration: 0.246574074074074
	},
	{
		id: 50,
		time: 49,
		velocity: 7.2525,
		power: -11.9987012800376,
		road: 112.892175925926,
		acceleration: -0.156944444444445
	},
	{
		id: 51,
		time: 50,
		velocity: 6.49944444444444,
		power: -631.761599173466,
		road: 119.708055555556,
		acceleration: -0.251018518518518
	},
	{
		id: 52,
		time: 51,
		velocity: 6.31916666666667,
		power: -1704.31100584511,
		road: 126.184537037037,
		acceleration: -0.427777777777778
	},
	{
		id: 53,
		time: 52,
		velocity: 5.96916666666667,
		power: -120.799378954189,
		road: 132.362407407407,
		acceleration: -0.169444444444444
	},
	{
		id: 54,
		time: 53,
		velocity: 5.99111111111111,
		power: -3.67905430772254,
		road: 138.381342592593,
		acceleration: -0.148425925925926
	},
	{
		id: 55,
		time: 54,
		velocity: 5.87388888888889,
		power: 1296.7223018537,
		road: 144.36625,
		acceleration: 0.0803703703703702
	},
	{
		id: 56,
		time: 55,
		velocity: 6.21027777777778,
		power: 1863.40570187466,
		road: 150.4775,
		acceleration: 0.172314814814815
	},
	{
		id: 57,
		time: 56,
		velocity: 6.50805555555556,
		power: 1089.66849081806,
		road: 156.692546296296,
		acceleration: 0.035277777777778
	},
	{
		id: 58,
		time: 57,
		velocity: 5.97972222222222,
		power: -774.567152524946,
		road: 162.784212962963,
		acceleration: -0.282037037037037
	},
	{
		id: 59,
		time: 58,
		velocity: 5.36416666666667,
		power: -3892.7034361627,
		road: 168.290648148148,
		acceleration: -0.888425925925926
	},
	{
		id: 60,
		time: 59,
		velocity: 3.84277777777778,
		power: -3617.47292375065,
		road: 172.86712962963,
		acceleration: -0.971481481481481
	},
	{
		id: 61,
		time: 60,
		velocity: 3.06527777777778,
		power: -2110.60575685018,
		road: 176.591805555556,
		acceleration: -0.73212962962963
	},
	{
		id: 62,
		time: 61,
		velocity: 3.16777777777778,
		power: 661.641846266406,
		road: 179.986157407407,
		acceleration: 0.0714814814814813
	},
	{
		id: 63,
		time: 62,
		velocity: 4.05722222222222,
		power: 1591.954132488,
		road: 183.582083333333,
		acceleration: 0.331666666666667
	},
	{
		id: 64,
		time: 63,
		velocity: 4.06027777777778,
		power: 2663.11412713654,
		road: 187.622592592593,
		acceleration: 0.5575
	},
	{
		id: 65,
		time: 64,
		velocity: 4.84027777777778,
		power: 2048.80926774733,
		road: 192.112685185185,
		acceleration: 0.341666666666667
	},
	{
		id: 66,
		time: 65,
		velocity: 5.08222222222222,
		power: 2610.78501100484,
		road: 196.985231481482,
		acceleration: 0.42324074074074
	},
	{
		id: 67,
		time: 66,
		velocity: 5.33,
		power: 1303.37169891294,
		road: 202.131527777778,
		acceleration: 0.12425925925926
	},
	{
		id: 68,
		time: 67,
		velocity: 5.21305555555556,
		power: 729.745318536815,
		road: 207.342314814815,
		acceleration: 0.00472222222222207
	},
	{
		id: 69,
		time: 68,
		velocity: 5.09638888888889,
		power: 272.06091682413,
		road: 212.511944444445,
		acceleration: -0.0870370370370361
	},
	{
		id: 70,
		time: 69,
		velocity: 5.06888888888889,
		power: 910.727960526048,
		road: 217.66,
		acceleration: 0.0438888888888886
	},
	{
		id: 71,
		time: 70,
		velocity: 5.34472222222222,
		power: 1311.10356951064,
		road: 222.890509259259,
		acceleration: 0.121018518518519
	},
	{
		id: 72,
		time: 71,
		velocity: 5.45944444444444,
		power: 1380.06959205577,
		road: 228.24537037037,
		acceleration: 0.127685185185184
	},
	{
		id: 73,
		time: 72,
		velocity: 5.45194444444444,
		power: 878.036719811173,
		road: 233.67712962963,
		acceleration: 0.0261111111111116
	},
	{
		id: 74,
		time: 73,
		velocity: 5.42305555555556,
		power: 621.558750043708,
		road: 239.110138888889,
		acceleration: -0.0236111111111112
	},
	{
		id: 75,
		time: 74,
		velocity: 5.38861111111111,
		power: 498.161551573275,
		road: 244.508009259259,
		acceleration: -0.0466666666666669
	},
	{
		id: 76,
		time: 75,
		velocity: 5.31194444444444,
		power: 410.087180284093,
		road: 249.851203703704,
		acceleration: -0.0626851851851846
	},
	{
		id: 77,
		time: 76,
		velocity: 5.235,
		power: 607.926693746964,
		road: 255.151805555556,
		acceleration: -0.0225
	},
	{
		id: 78,
		time: 77,
		velocity: 5.32111111111111,
		power: 583.945614186292,
		road: 260.42787037037,
		acceleration: -0.0265740740740741
	},
	{
		id: 79,
		time: 78,
		velocity: 5.23222222222222,
		power: 780.005551760323,
		road: 265.697037037037,
		acceleration: 0.012777777777778
	},
	{
		id: 80,
		time: 79,
		velocity: 5.27333333333333,
		power: 676.539858789744,
		road: 270.968611111111,
		acceleration: -0.00796296296296362
	},
	{
		id: 81,
		time: 80,
		velocity: 5.29722222222222,
		power: 848.957973420788,
		road: 276.249259259259,
		acceleration: 0.0261111111111108
	},
	{
		id: 82,
		time: 81,
		velocity: 5.31055555555556,
		power: 939.730141836057,
		road: 281.564351851852,
		acceleration: 0.0427777777777791
	},
	{
		id: 83,
		time: 82,
		velocity: 5.40166666666667,
		power: 978.46967912377,
		road: 286.925092592593,
		acceleration: 0.0485185185185184
	},
	{
		id: 84,
		time: 83,
		velocity: 5.44277777777778,
		power: 1068.18022661401,
		road: 292.341898148148,
		acceleration: 0.0636111111111113
	},
	{
		id: 85,
		time: 84,
		velocity: 5.50138888888889,
		power: 763.161260619921,
		road: 297.79212962963,
		acceleration: 0.00324074074073977
	},
	{
		id: 86,
		time: 85,
		velocity: 5.41138888888889,
		power: 875.652728167757,
		road: 303.256203703704,
		acceleration: 0.0244444444444447
	},
	{
		id: 87,
		time: 86,
		velocity: 5.51611111111111,
		power: 816.981624269959,
		road: 308.73875,
		acceleration: 0.0125000000000002
	},
	{
		id: 88,
		time: 87,
		velocity: 5.53888888888889,
		power: 838.939216300133,
		road: 314.235648148148,
		acceleration: 0.0162037037037033
	},
	{
		id: 89,
		time: 88,
		velocity: 5.46,
		power: 818.750464710313,
		road: 319.746574074074,
		acceleration: 0.0118518518518513
	},
	{
		id: 90,
		time: 89,
		velocity: 5.55166666666667,
		power: 738.452779888228,
		road: 325.26162037037,
		acceleration: -0.0036111111111099
	},
	{
		id: 91,
		time: 90,
		velocity: 5.52805555555556,
		power: 291.918978145703,
		road: 330.730833333333,
		acceleration: -0.088055555555556
	},
	{
		id: 92,
		time: 91,
		velocity: 5.19583333333333,
		power: -470.668387241982,
		road: 336.037731481481,
		acceleration: -0.236574074074075
	},
	{
		id: 93,
		time: 92,
		velocity: 4.84194444444444,
		power: -797.243348766748,
		road: 341.072175925926,
		acceleration: -0.308333333333333
	},
	{
		id: 94,
		time: 93,
		velocity: 4.60305555555556,
		power: -732.95799908592,
		road: 345.80087962963,
		acceleration: -0.303148148148149
	},
	{
		id: 95,
		time: 94,
		velocity: 4.28638888888889,
		power: -879.034801977516,
		road: 350.20375,
		acceleration: -0.348518518518518
	},
	{
		id: 96,
		time: 95,
		velocity: 3.79638888888889,
		power: -1806.03313523408,
		road: 354.121574074074,
		acceleration: -0.621574074074074
	},
	{
		id: 97,
		time: 96,
		velocity: 2.73833333333333,
		power: -2046.3386531053,
		road: 357.325509259259,
		acceleration: -0.806203703703703
	},
	{
		id: 98,
		time: 97,
		velocity: 1.86777777777778,
		power: -1880.59801835029,
		road: 359.631157407407,
		acceleration: -0.99037037037037
	},
	{
		id: 99,
		time: 98,
		velocity: 0.825277777777778,
		power: -1006.72899837403,
		road: 360.985231481481,
		acceleration: -0.912777777777778
	},
	{
		id: 100,
		time: 99,
		velocity: 0,
		power: -213.076608540745,
		road: 361.649907407407,
		acceleration: -0.466018518518519
	},
	{
		id: 101,
		time: 100,
		velocity: 0.469722222222222,
		power: -41.1129782135808,
		road: 361.944027777778,
		acceleration: -0.275092592592593
	},
	{
		id: 102,
		time: 101,
		velocity: 0,
		power: 18.9190337989736,
		road: 362.100601851852,
		acceleration: 0
	},
	{
		id: 103,
		time: 102,
		velocity: 0,
		power: -2.15410216049383,
		road: 362.178888888889,
		acceleration: -0.156574074074074
	},
	{
		id: 104,
		time: 103,
		velocity: 0,
		power: 0,
		road: 362.178888888889,
		acceleration: 0
	},
	{
		id: 105,
		time: 104,
		velocity: 0,
		power: 0,
		road: 362.178888888889,
		acceleration: 0
	},
	{
		id: 106,
		time: 105,
		velocity: 0,
		power: 3.18337637841423,
		road: 362.198935185185,
		acceleration: 0.0400925925925926
	},
	{
		id: 107,
		time: 106,
		velocity: 0.120277777777778,
		power: 4.84393761712736,
		road: 362.239027777778,
		acceleration: 0
	},
	{
		id: 108,
		time: 107,
		velocity: 0,
		power: 4.84393761712736,
		road: 362.27912037037,
		acceleration: 0
	},
	{
		id: 109,
		time: 108,
		velocity: 0,
		power: 1.66054374593892,
		road: 362.299166666667,
		acceleration: -0.0400925925925926
	},
	{
		id: 110,
		time: 109,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 111,
		time: 110,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 112,
		time: 111,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 113,
		time: 112,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 114,
		time: 113,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 115,
		time: 114,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 116,
		time: 115,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 117,
		time: 116,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 118,
		time: 117,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 119,
		time: 118,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 120,
		time: 119,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 121,
		time: 120,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 122,
		time: 121,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 123,
		time: 122,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 124,
		time: 123,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 125,
		time: 124,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 126,
		time: 125,
		velocity: 0,
		power: 0,
		road: 362.299166666667,
		acceleration: 0
	},
	{
		id: 127,
		time: 126,
		velocity: 0,
		power: 584.514645311029,
		road: 362.823472222222,
		acceleration: 1.04861111111111
	},
	{
		id: 128,
		time: 127,
		velocity: 3.14583333333333,
		power: 2210.56017221194,
		road: 364.501898148148,
		acceleration: 1.25962962962963
	},
	{
		id: 129,
		time: 128,
		velocity: 3.77888888888889,
		power: 5208.6055960612,
		road: 367.622777777778,
		acceleration: 1.62527777777778
	},
	{
		id: 130,
		time: 129,
		velocity: 4.87583333333333,
		power: 3198.14289704929,
		road: 371.882731481481,
		acceleration: 0.652870370370371
	},
	{
		id: 131,
		time: 130,
		velocity: 5.10444444444444,
		power: 2261.22356271347,
		road: 376.64875,
		acceleration: 0.359259259259259
	},
	{
		id: 132,
		time: 131,
		velocity: 4.85666666666667,
		power: 316.620034074912,
		road: 381.55787037037,
		acceleration: -0.0730555555555554
	},
	{
		id: 133,
		time: 132,
		velocity: 4.65666666666667,
		power: -271.516937390944,
		road: 386.330416666667,
		acceleration: -0.200092592592592
	},
	{
		id: 134,
		time: 133,
		velocity: 4.50416666666667,
		power: 3.04470192567471,
		road: 390.933611111111,
		acceleration: -0.138611111111111
	},
	{
		id: 135,
		time: 134,
		velocity: 4.44083333333333,
		power: 55.5327555436741,
		road: 395.404722222222,
		acceleration: -0.125555555555557
	},
	{
		id: 136,
		time: 135,
		velocity: 4.28,
		power: 376.52345499304,
		road: 399.789166666667,
		acceleration: -0.0477777777777764
	},
	{
		id: 137,
		time: 136,
		velocity: 4.36083333333333,
		power: 716.492932863421,
		road: 404.166805555556,
		acceleration: 0.0341666666666658
	},
	{
		id: 138,
		time: 137,
		velocity: 4.54333333333333,
		power: 1447.83259973797,
		road: 408.661712962963,
		acceleration: 0.200370370370371
	},
	{
		id: 139,
		time: 138,
		velocity: 4.88111111111111,
		power: 1716.71750072233,
		road: 413.378425925926,
		acceleration: 0.243240740740741
	},
	{
		id: 140,
		time: 139,
		velocity: 5.09055555555556,
		power: 786.315654030943,
		road: 418.231712962963,
		acceleration: 0.0299074074074071
	},
	{
		id: 141,
		time: 140,
		velocity: 4.63305555555556,
		power: -260.925372394565,
		road: 423.001064814815,
		acceleration: -0.197777777777778
	},
	{
		id: 142,
		time: 141,
		velocity: 4.28777777777778,
		power: -643.920159777006,
		road: 427.527175925926,
		acceleration: -0.288703703703704
	},
	{
		id: 143,
		time: 142,
		velocity: 4.22444444444444,
		power: 58.5607105679043,
		road: 431.84712962963,
		acceleration: -0.12361111111111
	},
	{
		id: 144,
		time: 143,
		velocity: 4.26222222222222,
		power: 745.01281115857,
		road: 436.128055555556,
		acceleration: 0.0455555555555556
	},
	{
		id: 145,
		time: 144,
		velocity: 4.42444444444444,
		power: 2476.01976086812,
		road: 440.650509259259,
		acceleration: 0.437499999999999
	},
	{
		id: 146,
		time: 145,
		velocity: 5.53694444444444,
		power: 4037.23669121944,
		road: 445.738287037037,
		acceleration: 0.693148148148148
	},
	{
		id: 147,
		time: 146,
		velocity: 6.34166666666667,
		power: 4729.78638761723,
		road: 451.52912037037,
		acceleration: 0.712962962962965
	},
	{
		id: 148,
		time: 147,
		velocity: 6.56333333333333,
		power: 4736.5450590281,
		road: 457.986666666667,
		acceleration: 0.620462962962962
	},
	{
		id: 149,
		time: 148,
		velocity: 7.39833333333333,
		power: 4588.12516495475,
		road: 465.019722222222,
		acceleration: 0.530555555555556
	},
	{
		id: 150,
		time: 149,
		velocity: 7.93333333333333,
		power: 5839.21169734967,
		road: 472.640601851852,
		acceleration: 0.645092592592593
	},
	{
		id: 151,
		time: 150,
		velocity: 8.49861111111111,
		power: 3846.23466915068,
		road: 480.750925925926,
		acceleration: 0.333796296296297
	},
	{
		id: 152,
		time: 151,
		velocity: 8.39972222222222,
		power: 3415.40270969541,
		road: 489.157916666667,
		acceleration: 0.259537037037035
	},
	{
		id: 153,
		time: 152,
		velocity: 8.71194444444444,
		power: 2924.57470035805,
		road: 497.787962962963,
		acceleration: 0.186574074074077
	},
	{
		id: 154,
		time: 153,
		velocity: 9.05833333333333,
		power: 4179.32054572537,
		road: 506.672361111111,
		acceleration: 0.322129629629629
	},
	{
		id: 155,
		time: 154,
		velocity: 9.36611111111111,
		power: 2816.63984151421,
		road: 515.792824074074,
		acceleration: 0.149999999999999
	},
	{
		id: 156,
		time: 155,
		velocity: 9.16194444444444,
		power: 541.07565956086,
		road: 524.93212962963,
		acceleration: -0.112314814814814
	},
	{
		id: 157,
		time: 156,
		velocity: 8.72138888888889,
		power: 30.0209347142315,
		road: 533.930509259259,
		acceleration: -0.169537037037037
	},
	{
		id: 158,
		time: 157,
		velocity: 8.8575,
		power: 675.162533339005,
		road: 542.798240740741,
		acceleration: -0.0917592592592591
	},
	{
		id: 159,
		time: 158,
		velocity: 8.88666666666667,
		power: 2102.82738520117,
		road: 551.658888888889,
		acceleration: 0.0775925925925911
	},
	{
		id: 160,
		time: 159,
		velocity: 8.95416666666667,
		power: 1165.40728355017,
		road: 560.541296296296,
		acceleration: -0.0340740740740735
	},
	{
		id: 161,
		time: 160,
		velocity: 8.75527777777778,
		power: 386.954485486106,
		road: 569.344212962963,
		acceleration: -0.124907407407406
	},
	{
		id: 162,
		time: 161,
		velocity: 8.51194444444444,
		power: -166.992856346294,
		road: 577.989768518519,
		acceleration: -0.189814814814817
	},
	{
		id: 163,
		time: 162,
		velocity: 8.38472222222222,
		power: -209.46480712424,
		road: 586.443564814815,
		acceleration: -0.193703703703703
	},
	{
		id: 164,
		time: 163,
		velocity: 8.17416666666667,
		power: 372.887405377457,
		road: 594.741018518519,
		acceleration: -0.118981481481482
	},
	{
		id: 165,
		time: 164,
		velocity: 8.155,
		power: 372.69737261189,
		road: 602.920370370371,
		acceleration: -0.117222222222221
	},
	{
		id: 166,
		time: 165,
		velocity: 8.03305555555556,
		power: 1259.51169935969,
		road: 611.040324074074,
		acceleration: -0.00157407407407462
	},
	{
		id: 167,
		time: 166,
		velocity: 8.16944444444444,
		power: 421.909114683527,
		road: 619.104953703704,
		acceleration: -0.109074074074075
	},
	{
		id: 168,
		time: 167,
		velocity: 7.82777777777778,
		power: -1112.61881312944,
		road: 626.959537037037,
		acceleration: -0.311018518518519
	},
	{
		id: 169,
		time: 168,
		velocity: 7.1,
		power: -1612.34147237008,
		road: 634.466157407408,
		acceleration: -0.384907407407407
	},
	{
		id: 170,
		time: 169,
		velocity: 7.01472222222222,
		power: -2468.53708260844,
		road: 641.518611111111,
		acceleration: -0.523425925925926
	},
	{
		id: 171,
		time: 170,
		velocity: 6.2575,
		power: -1822.80561205643,
		road: 648.087592592593,
		acceleration: -0.443518518518519
	},
	{
		id: 172,
		time: 171,
		velocity: 5.76944444444444,
		power: -2368.23165788144,
		road: 654.155462962963,
		acceleration: -0.558703703703703
	},
	{
		id: 173,
		time: 172,
		velocity: 5.33861111111111,
		power: -1938.78373734488,
		road: 659.687268518519,
		acceleration: -0.513425925925927
	},
	{
		id: 174,
		time: 173,
		velocity: 4.71722222222222,
		power: -1419.72037747467,
		road: 664.743703703704,
		acceleration: -0.437314814814814
	},
	{
		id: 175,
		time: 174,
		velocity: 4.4575,
		power: -677.759928184972,
		road: 669.435555555556,
		acceleration: -0.291851851851852
	},
	{
		id: 176,
		time: 175,
		velocity: 4.46305555555556,
		power: 149.982703013631,
		road: 673.929675925926,
		acceleration: -0.103611111111111
	},
	{
		id: 177,
		time: 176,
		velocity: 4.40638888888889,
		power: 680.797829253588,
		road: 678.383194444445,
		acceleration: 0.0224074074074077
	},
	{
		id: 178,
		time: 177,
		velocity: 4.52472222222222,
		power: 535.038787231021,
		road: 682.841805555556,
		acceleration: -0.0122222222222224
	},
	{
		id: 179,
		time: 178,
		velocity: 4.42638888888889,
		power: -1499.63631728892,
		road: 687.037453703704,
		acceleration: -0.513703703703704
	},
	{
		id: 180,
		time: 179,
		velocity: 2.86527777777778,
		power: -3030.38572086131,
		road: 690.440092592593,
		acceleration: -1.07231481481481
	},
	{
		id: 181,
		time: 180,
		velocity: 1.30777777777778,
		power: -2161.46821496969,
		road: 692.747546296296,
		acceleration: -1.11805555555556
	},
	{
		id: 182,
		time: 181,
		velocity: 1.07222222222222,
		power: -599.069218810344,
		road: 694.216574074074,
		acceleration: -0.558796296296296
	},
	{
		id: 183,
		time: 182,
		velocity: 1.18888888888889,
		power: 107.670837329017,
		road: 695.390462962963,
		acceleration: -0.0314814814814817
	},
	{
		id: 184,
		time: 183,
		velocity: 1.21333333333333,
		power: 211.255301817252,
		road: 696.578287037037,
		acceleration: 0.0593518518518521
	},
	{
		id: 185,
		time: 184,
		velocity: 1.25027777777778,
		power: 227.220506535822,
		road: 697.827546296297,
		acceleration: 0.0635185185185185
	},
	{
		id: 186,
		time: 185,
		velocity: 1.37944444444444,
		power: 226.188499217913,
		road: 699.135555555556,
		acceleration: 0.0539814814814814
	},
	{
		id: 187,
		time: 186,
		velocity: 1.37527777777778,
		power: 298.248279852125,
		road: 700.519907407408,
		acceleration: 0.0987037037037037
	},
	{
		id: 188,
		time: 187,
		velocity: 1.54638888888889,
		power: 244.766153314273,
		road: 701.977824074074,
		acceleration: 0.0484259259259259
	},
	{
		id: 189,
		time: 188,
		velocity: 1.52472222222222,
		power: 294.855056665219,
		road: 703.497870370371,
		acceleration: 0.0758333333333332
	},
	{
		id: 190,
		time: 189,
		velocity: 1.60277777777778,
		power: -97.8881526389087,
		road: 704.956111111111,
		acceleration: -0.199444444444445
	},
	{
		id: 191,
		time: 190,
		velocity: 0.948055555555555,
		power: -397.893506845071,
		road: 706.060509259259,
		acceleration: -0.50824074074074
	},
	{
		id: 192,
		time: 191,
		velocity: 0,
		power: -224.668461225383,
		road: 706.643657407408,
		acceleration: -0.534259259259259
	},
	{
		id: 193,
		time: 192,
		velocity: 0,
		power: -28.2154084957765,
		road: 706.801666666667,
		acceleration: -0.316018518518518
	},
	{
		id: 194,
		time: 193,
		velocity: 0,
		power: 0,
		road: 706.801666666667,
		acceleration: 0
	},
	{
		id: 195,
		time: 194,
		velocity: 0,
		power: 0,
		road: 706.801666666667,
		acceleration: 0
	},
	{
		id: 196,
		time: 195,
		velocity: 0,
		power: 4.57949618464743,
		road: 706.82837962963,
		acceleration: 0.0534259259259259
	},
	{
		id: 197,
		time: 196,
		velocity: 0.160277777777778,
		power: 6.45489067988616,
		road: 706.881805555556,
		acceleration: 0
	},
	{
		id: 198,
		time: 197,
		velocity: 0,
		power: 6.45489067988616,
		road: 706.935231481482,
		acceleration: 0
	},
	{
		id: 199,
		time: 198,
		velocity: 0,
		power: 1.87535310266407,
		road: 706.961944444445,
		acceleration: -0.0534259259259259
	},
	{
		id: 200,
		time: 199,
		velocity: 0,
		power: 0,
		road: 706.961944444445,
		acceleration: 0
	},
	{
		id: 201,
		time: 200,
		velocity: 0,
		power: 13.4638694932016,
		road: 707.020185185185,
		acceleration: 0.116481481481481
	},
	{
		id: 202,
		time: 201,
		velocity: 0.349444444444444,
		power: 64.3448707385191,
		road: 707.232685185185,
		acceleration: 0.192037037037037
	},
	{
		id: 203,
		time: 202,
		velocity: 0.576111111111111,
		power: 99.0526837332419,
		road: 707.614351851852,
		acceleration: 0.146296296296296
	},
	{
		id: 204,
		time: 203,
		velocity: 0.438888888888889,
		power: 71.4292606186797,
		road: 708.08537037037,
		acceleration: 0.0324074074074074
	},
	{
		id: 205,
		time: 204,
		velocity: 0.446666666666667,
		power: -23.8887083199641,
		road: 708.476574074074,
		acceleration: -0.192037037037037
	},
	{
		id: 206,
		time: 205,
		velocity: 0,
		power: 58.2733000826353,
		road: 708.802361111111,
		acceleration: 0.0612037037037038
	},
	{
		id: 207,
		time: 206,
		velocity: 0.6225,
		power: -5.69848147669491,
		road: 709.084305555556,
		acceleration: -0.148888888888889
	},
	{
		id: 208,
		time: 207,
		velocity: 0,
		power: 25.0745632564827,
		road: 709.291805555556,
		acceleration: 0
	},
	{
		id: 209,
		time: 208,
		velocity: 0,
		power: -7.86020921052632,
		road: 709.395555555556,
		acceleration: -0.2075
	},
	{
		id: 210,
		time: 209,
		velocity: 0,
		power: 0,
		road: 709.395555555556,
		acceleration: 0
	},
	{
		id: 211,
		time: 210,
		velocity: 0,
		power: 0,
		road: 709.395555555556,
		acceleration: 0
	},
	{
		id: 212,
		time: 211,
		velocity: 0,
		power: 0,
		road: 709.395555555556,
		acceleration: 0
	},
	{
		id: 213,
		time: 212,
		velocity: 0,
		power: 0,
		road: 709.395555555556,
		acceleration: 0
	},
	{
		id: 214,
		time: 213,
		velocity: 0,
		power: 0,
		road: 709.395555555556,
		acceleration: 0
	},
	{
		id: 215,
		time: 214,
		velocity: 0,
		power: 0,
		road: 709.395555555556,
		acceleration: 0
	},
	{
		id: 216,
		time: 215,
		velocity: 0,
		power: 56.249941739819,
		road: 709.538888888889,
		acceleration: 0.286666666666667
	},
	{
		id: 217,
		time: 216,
		velocity: 0.86,
		power: 153.683475861417,
		road: 709.956111111111,
		acceleration: 0.261111111111111
	},
	{
		id: 218,
		time: 217,
		velocity: 0.783333333333333,
		power: 273.346179081493,
		road: 710.648333333333,
		acceleration: 0.288888888888889
	},
	{
		id: 219,
		time: 218,
		velocity: 0.866666666666667,
		power: 116.08947725735,
		road: 711.493518518519,
		acceleration: 0.0170370370370371
	},
	{
		id: 220,
		time: 219,
		velocity: 0.911111111111111,
		power: 179.818208851004,
		road: 712.389166666667,
		acceleration: 0.083888888888889
	},
	{
		id: 221,
		time: 220,
		velocity: 1.035,
		power: -121.064441482695,
		road: 713.182314814815,
		acceleration: -0.288888888888889
	},
	{
		id: 222,
		time: 221,
		velocity: 0,
		power: -82.8931672459192,
		road: 713.679166666667,
		acceleration: -0.303703703703704
	},
	{
		id: 223,
		time: 222,
		velocity: 0,
		power: -35.5391763157895,
		road: 713.851666666667,
		acceleration: -0.345
	},
	{
		id: 224,
		time: 223,
		velocity: 0,
		power: 0,
		road: 713.851666666667,
		acceleration: 0
	},
	{
		id: 225,
		time: 224,
		velocity: 0,
		power: 0,
		road: 713.851666666667,
		acceleration: 0
	},
	{
		id: 226,
		time: 225,
		velocity: 0,
		power: 0,
		road: 713.851666666667,
		acceleration: 0
	},
	{
		id: 227,
		time: 226,
		velocity: 0,
		power: 0,
		road: 713.851666666667,
		acceleration: 0
	},
	{
		id: 228,
		time: 227,
		velocity: 0,
		power: 99.1581859523568,
		road: 714.050740740741,
		acceleration: 0.398148148148148
	},
	{
		id: 229,
		time: 228,
		velocity: 1.19444444444444,
		power: 463.81538206175,
		road: 714.74,
		acceleration: 0.582222222222222
	},
	{
		id: 230,
		time: 229,
		velocity: 1.74666666666667,
		power: 885.517853346504,
		road: 716.020787037037,
		acceleration: 0.600833333333333
	},
	{
		id: 231,
		time: 230,
		velocity: 1.8025,
		power: -353.293445405311,
		road: 717.402916666667,
		acceleration: -0.398148148148148
	},
	{
		id: 232,
		time: 231,
		velocity: 0,
		power: -384.040127505171,
		road: 718.294861111111,
		acceleration: -0.582222222222222
	},
	{
		id: 233,
		time: 232,
		velocity: 0,
		power: -134.704619736842,
		road: 718.595277777778,
		acceleration: -0.600833333333333
	},
	{
		id: 234,
		time: 233,
		velocity: 0,
		power: 51.3169075669768,
		road: 718.731018518518,
		acceleration: 0.271481481481481
	},
	{
		id: 235,
		time: 234,
		velocity: 0.814444444444444,
		power: 309.237826296374,
		road: 719.251898148148,
		acceleration: 0.498796296296296
	},
	{
		id: 236,
		time: 235,
		velocity: 1.49638888888889,
		power: 905.477880063233,
		road: 720.381018518518,
		acceleration: 0.717685185185185
	},
	{
		id: 237,
		time: 236,
		velocity: 2.15305555555556,
		power: 1722.5822118642,
		road: 722.281944444444,
		acceleration: 0.825925925925926
	},
	{
		id: 238,
		time: 237,
		velocity: 3.29222222222222,
		power: 2418.9027997252,
		road: 724.99912037037,
		acceleration: 0.806574074074074
	},
	{
		id: 239,
		time: 238,
		velocity: 3.91611111111111,
		power: 3266.84873405002,
		road: 728.538472222222,
		acceleration: 0.837777777777777
	},
	{
		id: 240,
		time: 239,
		velocity: 4.66638888888889,
		power: 2341.21140964398,
		road: 732.722685185185,
		acceleration: 0.451944444444444
	},
	{
		id: 241,
		time: 240,
		velocity: 4.64805555555556,
		power: 1628.44830020328,
		road: 737.252638888889,
		acceleration: 0.239537037037038
	},
	{
		id: 242,
		time: 241,
		velocity: 4.63472222222222,
		power: 1795.39340730574,
		road: 742.030046296296,
		acceleration: 0.255370370370369
	},
	{
		id: 243,
		time: 242,
		velocity: 5.4325,
		power: 2351.36073766952,
		road: 747.10787037037,
		acceleration: 0.345462962962963
	},
	{
		id: 244,
		time: 243,
		velocity: 5.68444444444444,
		power: 1113.19213473094,
		road: 752.397592592592,
		acceleration: 0.0783333333333331
	},
	{
		id: 245,
		time: 244,
		velocity: 4.86972222222222,
		power: -264.124949060558,
		road: 757.628518518518,
		acceleration: -0.195925925925925
	},
	{
		id: 246,
		time: 245,
		velocity: 4.84472222222222,
		power: 2744.95670738549,
		road: 762.960648148148,
		acceleration: 0.398333333333333
	},
	{
		id: 247,
		time: 246,
		velocity: 6.87944444444444,
		power: 6912.64962813873,
		road: 769.018055555555,
		acceleration: 1.05222222222222
	},
	{
		id: 248,
		time: 247,
		velocity: 8.02638888888889,
		power: 8663.21483269106,
		road: 776.160925925926,
		acceleration: 1.11870370370371
	},
	{
		id: 249,
		time: 248,
		velocity: 8.20083333333333,
		power: 4562.05675483336,
		road: 784.084259259259,
		acceleration: 0.442222222222221
	},
	{
		id: 250,
		time: 249,
		velocity: 8.20611111111111,
		power: 1246.68985552984,
		road: 792.226759259259,
		acceleration: -0.00388888888888772
	},
	{
		id: 251,
		time: 250,
		velocity: 8.01472222222222,
		power: 638.942824275963,
		road: 800.326574074074,
		acceleration: -0.0814814814814824
	},
	{
		id: 252,
		time: 251,
		velocity: 7.95638888888889,
		power: 492.851973166224,
		road: 808.336203703704,
		acceleration: -0.0988888888888884
	},
	{
		id: 253,
		time: 252,
		velocity: 7.90944444444444,
		power: 1239.82425765357,
		road: 816.296666666667,
		acceleration: 0.000555555555554754
	},
	{
		id: 254,
		time: 253,
		velocity: 8.01638888888889,
		power: 1518.22578068903,
		road: 824.275740740741,
		acceleration: 0.036666666666668
	},
	{
		id: 255,
		time: 254,
		velocity: 8.06638888888889,
		power: 1737.89558762741,
		road: 832.305,
		acceleration: 0.0637037037037036
	},
	{
		id: 256,
		time: 255,
		velocity: 8.10055555555556,
		power: 692.09953658888,
		road: 840.329583333333,
		acceleration: -0.0730555555555563
	},
	{
		id: 257,
		time: 256,
		velocity: 7.79722222222222,
		power: -218.417594844641,
		road: 848.221851851852,
		acceleration: -0.191574074074074
	},
	{
		id: 258,
		time: 257,
		velocity: 7.49166666666667,
		power: -2202.79920188958,
		road: 855.785462962963,
		acceleration: -0.46574074074074
	},
	{
		id: 259,
		time: 258,
		velocity: 6.70333333333333,
		power: -2284.98245791955,
		road: 862.868796296296,
		acceleration: -0.494814814814816
	},
	{
		id: 260,
		time: 259,
		velocity: 6.31277777777778,
		power: -1431.13141167463,
		road: 869.51537037037,
		acceleration: -0.378703703703703
	},
	{
		id: 261,
		time: 260,
		velocity: 6.35555555555556,
		power: 1976.94827293088,
		road: 876.055787037037,
		acceleration: 0.166388888888888
	},
	{
		id: 262,
		time: 261,
		velocity: 7.2025,
		power: 3177.93070077654,
		road: 882.848657407407,
		acceleration: 0.338518518518519
	},
	{
		id: 263,
		time: 262,
		velocity: 7.32833333333333,
		power: 3787.49065204189,
		road: 890.010601851852,
		acceleration: 0.39962962962963
	},
	{
		id: 264,
		time: 263,
		velocity: 7.55444444444445,
		power: 3022.17972734518,
		road: 897.504768518519,
		acceleration: 0.264814814814815
	},
	{
		id: 265,
		time: 264,
		velocity: 7.99694444444444,
		power: 2270.11833195625,
		road: 905.205833333333,
		acceleration: 0.148981481481481
	},
	{
		id: 266,
		time: 265,
		velocity: 7.77527777777778,
		power: 2474.40357878136,
		road: 913.065694444444,
		acceleration: 0.168611111111111
	},
	{
		id: 267,
		time: 266,
		velocity: 8.06027777777778,
		power: 1775.67939725501,
		road: 921.045138888889,
		acceleration: 0.070555555555555
	},
	{
		id: 268,
		time: 267,
		velocity: 8.20861111111111,
		power: 3197.56482715087,
		road: 929.183888888889,
		acceleration: 0.248055555555556
	},
	{
		id: 269,
		time: 268,
		velocity: 8.51944444444444,
		power: 3523.59557060331,
		road: 937.583425925926,
		acceleration: 0.273518518518518
	},
	{
		id: 270,
		time: 269,
		velocity: 8.88083333333333,
		power: 3940.94275290305,
		road: 946.272916666667,
		acceleration: 0.306388888888888
	},
	{
		id: 271,
		time: 270,
		velocity: 9.12777777777778,
		power: 3443.28998777195,
		road: 955.231111111111,
		acceleration: 0.231018518518521
	},
	{
		id: 272,
		time: 271,
		velocity: 9.2125,
		power: 3328.77623769062,
		road: 964.407824074074,
		acceleration: 0.206018518518519
	},
	{
		id: 273,
		time: 272,
		velocity: 9.49888888888889,
		power: 2592.7547607447,
		road: 973.745046296296,
		acceleration: 0.114999999999997
	},
	{
		id: 274,
		time: 273,
		velocity: 9.47277777777778,
		power: 2146.14648051137,
		road: 983.170555555555,
		acceleration: 0.0615740740740751
	},
	{
		id: 275,
		time: 274,
		velocity: 9.39722222222222,
		power: -1163.3706214639,
		road: 992.473101851852,
		acceleration: -0.307499999999999
	},
	{
		id: 276,
		time: 275,
		velocity: 8.57638888888889,
		power: -2225.62579637816,
		road: 1001.40486111111,
		acceleration: -0.434074074074074
	},
	{
		id: 277,
		time: 276,
		velocity: 8.17055555555556,
		power: -2175.93572395467,
		road: 1009.90101851852,
		acceleration: -0.437129629629631
	},
	{
		id: 278,
		time: 277,
		velocity: 8.08583333333333,
		power: 866.755755150239,
		road: 1018.15092592593,
		acceleration: -0.055370370370369
	},
	{
		id: 279,
		time: 278,
		velocity: 8.41027777777778,
		power: 2141.04867473261,
		road: 1026.42606481481,
		acceleration: 0.105833333333333
	},
	{
		id: 280,
		time: 279,
		velocity: 8.48805555555555,
		power: 2281.4303506067,
		road: 1034.81347222222,
		acceleration: 0.118703703703703
	},
	{
		id: 281,
		time: 280,
		velocity: 8.44194444444444,
		power: 1781.94700011849,
		road: 1043.28675925926,
		acceleration: 0.0530555555555541
	},
	{
		id: 282,
		time: 281,
		velocity: 8.56944444444444,
		power: 3828.48461000022,
		road: 1051.93430555556,
		acceleration: 0.295462962962963
	},
	{
		id: 283,
		time: 282,
		velocity: 9.37444444444444,
		power: 4018.3012166937,
		road: 1060.87921296296,
		acceleration: 0.299259259259259
	},
	{
		id: 284,
		time: 283,
		velocity: 9.33972222222222,
		power: 4012.55678100733,
		road: 1070.11412037037,
		acceleration: 0.280740740740741
	},
	{
		id: 285,
		time: 284,
		velocity: 9.41166666666667,
		power: 1622.85401683502,
		road: 1079.49175925926,
		acceleration: 0.00472222222222207
	},
	{
		id: 286,
		time: 285,
		velocity: 9.38861111111111,
		power: 805.024014145363,
		road: 1088.82875,
		acceleration: -0.0860185185185181
	},
	{
		id: 287,
		time: 286,
		velocity: 9.08166666666667,
		power: 548.537765534686,
		road: 1098.06615740741,
		acceleration: -0.113148148148149
	},
	{
		id: 288,
		time: 287,
		velocity: 9.07222222222222,
		power: 1480.84383464253,
		road: 1107.24425925926,
		acceleration: -0.00546296296296234
	},
	{
		id: 289,
		time: 288,
		velocity: 9.37222222222222,
		power: 3008.31250511822,
		road: 1116.50236111111,
		acceleration: 0.165462962962964
	},
	{
		id: 290,
		time: 289,
		velocity: 9.57805555555556,
		power: 3574.57693566297,
		road: 1125.95287037037,
		acceleration: 0.219351851851851
	},
	{
		id: 291,
		time: 290,
		velocity: 9.73027777777778,
		power: 4398.63538471572,
		road: 1135.66060185185,
		acceleration: 0.295092592592594
	},
	{
		id: 292,
		time: 291,
		velocity: 10.2575,
		power: 2640.43364862303,
		road: 1145.56444444444,
		acceleration: 0.0971296296296291
	},
	{
		id: 293,
		time: 292,
		velocity: 9.86944444444445,
		power: 2748.88540419051,
		road: 1155.56912037037,
		acceleration: 0.104537037037037
	},
	{
		id: 294,
		time: 293,
		velocity: 10.0438888888889,
		power: 2236.6353714204,
		road: 1165.65013888889,
		acceleration: 0.0481481481481474
	},
	{
		id: 295,
		time: 294,
		velocity: 10.4019444444444,
		power: 3888.77087290128,
		road: 1175.86194444444,
		acceleration: 0.213425925925925
	},
	{
		id: 296,
		time: 295,
		velocity: 10.5097222222222,
		power: 4849.4479499424,
		road: 1186.32893518518,
		acceleration: 0.296944444444446
	},
	{
		id: 297,
		time: 296,
		velocity: 10.9347222222222,
		power: 3508.44995394665,
		road: 1197.02060185185,
		acceleration: 0.152407407407406
	},
	{
		id: 298,
		time: 297,
		velocity: 10.8591666666667,
		power: 2977.77025406719,
		road: 1207.8362037037,
		acceleration: 0.0954629629629622
	},
	{
		id: 299,
		time: 298,
		velocity: 10.7961111111111,
		power: 3033.08674310205,
		road: 1218.74805555556,
		acceleration: 0.0970370370370386
	},
	{
		id: 300,
		time: 299,
		velocity: 11.2258333333333,
		power: 2905.49315803793,
		road: 1229.74912037037,
		acceleration: 0.0813888888888883
	},
	{
		id: 301,
		time: 300,
		velocity: 11.1033333333333,
		power: 3065.50209279656,
		road: 1240.8375,
		acceleration: 0.0932407407407414
	},
	{
		id: 302,
		time: 301,
		velocity: 11.0758333333333,
		power: 2513.6624444335,
		road: 1251.99189814815,
		acceleration: 0.0387962962962956
	},
	{
		id: 303,
		time: 302,
		velocity: 11.3422222222222,
		power: 2186.48763531606,
		road: 1263.16935185185,
		acceleration: 0.00731481481481744
	},
	{
		id: 304,
		time: 303,
		velocity: 11.1252777777778,
		power: 1778.48022577159,
		road: 1274.33513888889,
		acceleration: -0.0306481481481509
	},
	{
		id: 305,
		time: 304,
		velocity: 10.9838888888889,
		power: 371.601400786851,
		road: 1285.4049537037,
		acceleration: -0.161296296296294
	},
	{
		id: 306,
		time: 305,
		velocity: 10.8583333333333,
		power: 410.371544602135,
		road: 1296.31657407407,
		acceleration: -0.155092592592593
	},
	{
		id: 307,
		time: 306,
		velocity: 10.66,
		power: 617.793910734535,
		road: 1307.08435185185,
		acceleration: -0.132592592592594
	},
	{
		id: 308,
		time: 307,
		velocity: 10.5861111111111,
		power: -972.847404349243,
		road: 1317.64236111111,
		acceleration: -0.286944444444444
	},
	{
		id: 309,
		time: 308,
		velocity: 9.9975,
		power: -2082.33629584704,
		road: 1327.85680555556,
		acceleration: -0.400185185185185
	},
	{
		id: 310,
		time: 309,
		velocity: 9.45944444444445,
		power: -4150.89467787896,
		road: 1337.5562962963,
		acceleration: -0.62972222222222
	},
	{
		id: 311,
		time: 310,
		velocity: 8.69694444444444,
		power: -2584.18869392306,
		road: 1346.70532407407,
		acceleration: -0.471203703703704
	},
	{
		id: 312,
		time: 311,
		velocity: 8.58388888888889,
		power: -2715.08424286575,
		road: 1355.36930555556,
		acceleration: -0.498888888888889
	},
	{
		id: 313,
		time: 312,
		velocity: 7.96277777777778,
		power: -847.756121036548,
		road: 1363.64703703704,
		acceleration: -0.273611111111112
	},
	{
		id: 314,
		time: 313,
		velocity: 7.87611111111111,
		power: -1906.27904776776,
		road: 1371.58027777778,
		acceleration: -0.41537037037037
	},
	{
		id: 315,
		time: 314,
		velocity: 7.33777777777778,
		power: -2791.68015288865,
		road: 1379.02953703704,
		acceleration: -0.552592592592593
	},
	{
		id: 316,
		time: 315,
		velocity: 6.305,
		power: -3406.58125369337,
		road: 1385.86356481481,
		acceleration: -0.67787037037037
	},
	{
		id: 317,
		time: 316,
		velocity: 5.8425,
		power: -6188.3248604043,
		road: 1391.73032407407,
		acceleration: -1.25666666666667
	},
	{
		id: 318,
		time: 317,
		velocity: 3.56777777777778,
		power: -5250.02262886394,
		road: 1396.29342592593,
		acceleration: -1.35064814814815
	},
	{
		id: 319,
		time: 318,
		velocity: 2.25305555555556,
		power: -3469.89305121516,
		road: 1399.55375,
		acceleration: -1.25490740740741
	},
	{
		id: 320,
		time: 319,
		velocity: 2.07777777777778,
		power: -764.828391682745,
		road: 1401.95328703704,
		acceleration: -0.466666666666666
	},
	{
		id: 321,
		time: 320,
		velocity: 2.16777777777778,
		power: 1300.70106151915,
		road: 1404.34125,
		acceleration: 0.443518518518518
	},
	{
		id: 322,
		time: 321,
		velocity: 3.58361111111111,
		power: 3787.53298728224,
		road: 1407.51337962963,
		acceleration: 1.12481481481481
	},
	{
		id: 323,
		time: 322,
		velocity: 5.45222222222222,
		power: 5461.50838165503,
		road: 1411.84296296296,
		acceleration: 1.19009259259259
	},
	{
		id: 324,
		time: 323,
		velocity: 5.73805555555556,
		power: 6661.18992369603,
		road: 1417.33361111111,
		acceleration: 1.13203703703704
	},
	{
		id: 325,
		time: 324,
		velocity: 6.97972222222222,
		power: 6284.71030969043,
		road: 1423.82384259259,
		acceleration: 0.86712962962963
	},
	{
		id: 326,
		time: 325,
		velocity: 8.05361111111111,
		power: 12611.8599063665,
		road: 1431.52731481481,
		acceleration: 1.55935185185185
	},
	{
		id: 327,
		time: 326,
		velocity: 10.4161111111111,
		power: 15915.6407942678,
		road: 1440.82125,
		acceleration: 1.62157407407407
	},
	{
		id: 328,
		time: 327,
		velocity: 11.8444444444444,
		power: 14602.1317362256,
		road: 1451.54407407407,
		acceleration: 1.2362037037037
	},
	{
		id: 329,
		time: 328,
		velocity: 11.7622222222222,
		power: 8141.94256650026,
		road: 1463.15106481481,
		acceleration: 0.532129629629633
	},
	{
		id: 330,
		time: 329,
		velocity: 12.0125,
		power: 5010.56129275672,
		road: 1475.13898148148,
		acceleration: 0.229722222222218
	},
	{
		id: 331,
		time: 330,
		velocity: 12.5336111111111,
		power: 10020.6530511252,
		road: 1487.55740740741,
		acceleration: 0.631296296296297
	},
	{
		id: 332,
		time: 331,
		velocity: 13.6561111111111,
		power: 11113.0188781222,
		road: 1500.62509259259,
		acceleration: 0.667222222222223
	},
	{
		id: 333,
		time: 332,
		velocity: 14.0141666666667,
		power: 13199.9197763087,
		road: 1514.41046296296,
		acceleration: 0.768148148148148
	},
	{
		id: 334,
		time: 333,
		velocity: 14.8380555555556,
		power: 12434.1064991259,
		road: 1528.90592592593,
		acceleration: 0.652037037037038
	},
	{
		id: 335,
		time: 334,
		velocity: 15.6122222222222,
		power: 17116.1389003286,
		road: 1544.18398148148,
		acceleration: 0.913148148148148
	},
	{
		id: 336,
		time: 335,
		velocity: 16.7536111111111,
		power: 16444.2825475486,
		road: 1560.31462962963,
		acceleration: 0.792037037037037
	},
	{
		id: 337,
		time: 336,
		velocity: 17.2141666666667,
		power: 10435.0688341805,
		road: 1577.02532407407,
		acceleration: 0.368055555555557
	},
	{
		id: 338,
		time: 337,
		velocity: 16.7163888888889,
		power: -1583.1988252498,
		road: 1593.72814814815,
		acceleration: -0.3837962962963
	},
	{
		id: 339,
		time: 338,
		velocity: 15.6022222222222,
		power: -7166.22241499839,
		road: 1609.86972222222,
		acceleration: -0.738703703703704
	},
	{
		id: 340,
		time: 339,
		velocity: 14.9980555555556,
		power: -11012.0606428112,
		road: 1625.1350462963,
		acceleration: -1.01379629629629
	},
	{
		id: 341,
		time: 340,
		velocity: 13.675,
		power: -10020.493123721,
		road: 1639.40467592593,
		acceleration: -0.977592592592593
	},
	{
		id: 342,
		time: 341,
		velocity: 12.6694444444444,
		power: -20037.3695246005,
		road: 1652.25824074074,
		acceleration: -1.85453703703704
	},
	{
		id: 343,
		time: 342,
		velocity: 9.43444444444444,
		power: -22789.8054561238,
		road: 1662.97226851852,
		acceleration: -2.42453703703704
	},
	{
		id: 344,
		time: 343,
		velocity: 6.40138888888889,
		power: -21875.7509563996,
		road: 1670.95134259259,
		acceleration: -3.04537037037037
	},
	{
		id: 345,
		time: 344,
		velocity: 3.53333333333333,
		power: -10442.2431382192,
		road: 1676.31037037037,
		acceleration: -2.19472222222222
	},
	{
		id: 346,
		time: 345,
		velocity: 2.85027777777778,
		power: -4358.30847658373,
		road: 1679.8575,
		acceleration: -1.42907407407407
	},
	{
		id: 347,
		time: 346,
		velocity: 2.11416666666667,
		power: -2229.0859196529,
		road: 1682.1012037037,
		acceleration: -1.17777777777778
	},
	{
		id: 348,
		time: 347,
		velocity: 0,
		power: -733.751357291739,
		road: 1683.39199074074,
		acceleration: -0.728055555555555
	},
	{
		id: 349,
		time: 348,
		velocity: 0.666111111111111,
		power: -40.7710042604979,
		road: 1684.22912037037,
		acceleration: -0.179259259259259
	},
	{
		id: 350,
		time: 349,
		velocity: 1.57638888888889,
		power: 1702.95559987341,
		road: 1685.57805555555,
		acceleration: 1.20287037037037
	},
	{
		id: 351,
		time: 350,
		velocity: 3.60861111111111,
		power: 4048.44176299949,
		road: 1688.25847222222,
		acceleration: 1.46009259259259
	},
	{
		id: 352,
		time: 351,
		velocity: 5.04638888888889,
		power: 5350.69032048222,
		road: 1692.29800925926,
		acceleration: 1.25814814814815
	},
	{
		id: 353,
		time: 352,
		velocity: 5.35083333333333,
		power: 2795.63268645758,
		road: 1697.19652777778,
		acceleration: 0.459814814814814
	},
	{
		id: 354,
		time: 353,
		velocity: 4.98805555555556,
		power: -867.238470343401,
		road: 1702.16240740741,
		acceleration: -0.325092592592592
	},
	{
		id: 355,
		time: 354,
		velocity: 4.07111111111111,
		power: -2100.47480268186,
		road: 1706.6499537037,
		acceleration: -0.631574074074074
	},
	{
		id: 356,
		time: 355,
		velocity: 3.45611111111111,
		power: -1046.22034943026,
		road: 1710.61462962963,
		acceleration: -0.414166666666667
	},
	{
		id: 357,
		time: 356,
		velocity: 3.74555555555556,
		power: 2102.36676707754,
		road: 1714.58305555555,
		acceleration: 0.421666666666667
	},
	{
		id: 358,
		time: 357,
		velocity: 5.33611111111111,
		power: 3392.18357130701,
		road: 1719.08916666667,
		acceleration: 0.653703703703703
	},
	{
		id: 359,
		time: 358,
		velocity: 5.41722222222222,
		power: 5469.38351756971,
		road: 1724.39305555555,
		acceleration: 0.941851851851852
	},
	{
		id: 360,
		time: 359,
		velocity: 6.57111111111111,
		power: 5032.04224001562,
		road: 1730.52513888889,
		acceleration: 0.714537037037037
	},
	{
		id: 361,
		time: 360,
		velocity: 7.47972222222222,
		power: 5714.67124916136,
		road: 1737.37606481481,
		acceleration: 0.723148148148148
	},
	{
		id: 362,
		time: 361,
		velocity: 7.58666666666667,
		power: 4047.36834539831,
		road: 1744.79601851852,
		acceleration: 0.414907407407408
	},
	{
		id: 363,
		time: 362,
		velocity: 7.81583333333333,
		power: 1656.27405369746,
		road: 1752.45680555555,
		acceleration: 0.0667592592592596
	},
	{
		id: 364,
		time: 363,
		velocity: 7.68,
		power: 120.458984829363,
		road: 1760.07921296296,
		acceleration: -0.143518518518519
	},
	{
		id: 365,
		time: 364,
		velocity: 7.15611111111111,
		power: -2533.98244015289,
		road: 1767.36847222222,
		acceleration: -0.522777777777778
	},
	{
		id: 366,
		time: 365,
		velocity: 6.2475,
		power: -4728.98181557914,
		road: 1773.94217592592,
		acceleration: -0.908333333333333
	},
	{
		id: 367,
		time: 366,
		velocity: 4.955,
		power: -5289.7383376019,
		road: 1779.48740740741,
		acceleration: -1.14861111111111
	},
	{
		id: 368,
		time: 367,
		velocity: 3.71027777777778,
		power: -6169.31897544715,
		road: 1783.59976851852,
		acceleration: -1.71712962962963
	},
	{
		id: 369,
		time: 368,
		velocity: 1.09611111111111,
		power: -3502.39847935251,
		road: 1786.02773148148,
		acceleration: -1.65166666666667
	},
	{
		id: 370,
		time: 369,
		velocity: 0,
		power: -1033.70118625964,
		road: 1787.01148148148,
		acceleration: -1.23675925925926
	},
	{
		id: 371,
		time: 370,
		velocity: 0,
		power: -41.1630746263808,
		road: 1787.19416666667,
		acceleration: -0.36537037037037
	},
	{
		id: 372,
		time: 371,
		velocity: 0,
		power: 0,
		road: 1787.19416666667,
		acceleration: 0
	},
	{
		id: 373,
		time: 372,
		velocity: 0,
		power: 0,
		road: 1787.19416666667,
		acceleration: 0
	},
	{
		id: 374,
		time: 373,
		velocity: 0,
		power: 0,
		road: 1787.19416666667,
		acceleration: 0
	},
	{
		id: 375,
		time: 374,
		velocity: 0,
		power: 0,
		road: 1787.19416666667,
		acceleration: 0
	},
	{
		id: 376,
		time: 375,
		velocity: 0,
		power: 0,
		road: 1787.19416666667,
		acceleration: 0
	},
	{
		id: 377,
		time: 376,
		velocity: 0,
		power: 0,
		road: 1787.19416666667,
		acceleration: 0
	},
	{
		id: 378,
		time: 377,
		velocity: 0,
		power: 55.1788154442364,
		road: 1787.33587962963,
		acceleration: 0.283425925925926
	},
	{
		id: 379,
		time: 378,
		velocity: 0.850277777777778,
		power: 972.238280879,
		road: 1788.17009259259,
		acceleration: 1.10157407407407
	},
	{
		id: 380,
		time: 379,
		velocity: 3.30472222222222,
		power: 3934.46317457992,
		road: 1790.41398148148,
		acceleration: 1.71777777777778
	},
	{
		id: 381,
		time: 380,
		velocity: 5.15333333333333,
		power: 5392.90968443152,
		road: 1794.19921296296,
		acceleration: 1.36490740740741
	},
	{
		id: 382,
		time: 381,
		velocity: 4.945,
		power: 2846.5576883364,
		road: 1798.91467592592,
		acceleration: 0.495555555555556
	},
	{
		id: 383,
		time: 382,
		velocity: 4.79138888888889,
		power: 1238.87654766284,
		road: 1803.93694444444,
		acceleration: 0.118055555555556
	},
	{
		id: 384,
		time: 383,
		velocity: 5.5075,
		power: 4473.0238948154,
		road: 1809.37865740741,
		acceleration: 0.720833333333332
	},
	{
		id: 385,
		time: 384,
		velocity: 7.1075,
		power: 8744.15765189747,
		road: 1815.81921296296,
		acceleration: 1.27685185185185
	},
	{
		id: 386,
		time: 385,
		velocity: 8.62194444444444,
		power: 16392.3786187016,
		road: 1823.88375,
		acceleration: 1.97111111111111
	},
	{
		id: 387,
		time: 386,
		velocity: 11.4208333333333,
		power: 17968.6282376351,
		road: 1833.7937962963,
		acceleration: 1.71990740740741
	},
	{
		id: 388,
		time: 387,
		velocity: 12.2672222222222,
		power: 14251.6085903543,
		road: 1845.12342592593,
		acceleration: 1.11925925925926
	},
	{
		id: 389,
		time: 388,
		velocity: 11.9797222222222,
		power: 4204.84384561284,
		road: 1857.09273148148,
		acceleration: 0.160092592592592
	},
	{
		id: 390,
		time: 389,
		velocity: 11.9011111111111,
		power: 3783.73397904886,
		road: 1869.20083333333,
		acceleration: 0.1175
	},
	{
		id: 391,
		time: 390,
		velocity: 12.6197222222222,
		power: 7244.1559533886,
		road: 1881.56782407407,
		acceleration: 0.400277777777776
	},
	{
		id: 392,
		time: 391,
		velocity: 13.1805555555556,
		power: 11078.9651897499,
		road: 1894.47398148148,
		acceleration: 0.678055555555558
	},
	{
		id: 393,
		time: 392,
		velocity: 13.9352777777778,
		power: 6682.98170284513,
		road: 1907.86625,
		acceleration: 0.294166666666667
	},
	{
		id: 394,
		time: 393,
		velocity: 13.5022222222222,
		power: 2306.99974257371,
		road: 1921.37981481481,
		acceleration: -0.0515740740740753
	},
	{
		id: 395,
		time: 394,
		velocity: 13.0258333333333,
		power: -2042.65945206366,
		road: 1934.67356481481,
		acceleration: -0.388055555555557
	},
	{
		id: 396,
		time: 395,
		velocity: 12.7711111111111,
		power: -595.981899923591,
		road: 1947.63810185185,
		acceleration: -0.270370370370369
	},
	{
		id: 397,
		time: 396,
		velocity: 12.6911111111111,
		power: 244.138389073381,
		road: 1960.36810185185,
		acceleration: -0.198703703703705
	},
	{
		id: 398,
		time: 397,
		velocity: 12.4297222222222,
		power: -209.111235379765,
		road: 1972.88212962963,
		acceleration: -0.23324074074074
	},
	{
		id: 399,
		time: 398,
		velocity: 12.0713888888889,
		power: -1758.81314802262,
		road: 1985.09828703704,
		acceleration: -0.362499999999999
	},
	{
		id: 400,
		time: 399,
		velocity: 11.6036111111111,
		power: -4166.05282060361,
		road: 1996.84462962963,
		acceleration: -0.57712962962963
	},
	{
		id: 401,
		time: 400,
		velocity: 10.6983333333333,
		power: -8523.66809452439,
		road: 2007.79662037037,
		acceleration: -1.01157407407407
	},
	{
		id: 402,
		time: 401,
		velocity: 9.03666666666667,
		power: -10297.0185497245,
		road: 2017.60069444444,
		acceleration: -1.28425925925926
	},
	{
		id: 403,
		time: 402,
		velocity: 7.75083333333333,
		power: -7318.52314571273,
		road: 2026.23259259259,
		acceleration: -1.06009259259259
	},
	{
		id: 404,
		time: 403,
		velocity: 7.51805555555556,
		power: -4417.907422141,
		road: 2033.95324074074,
		acceleration: -0.762407407407408
	},
	{
		id: 405,
		time: 404,
		velocity: 6.74944444444444,
		power: -2002.44893894332,
		road: 2041.06675925926,
		acceleration: -0.451851851851851
	},
	{
		id: 406,
		time: 405,
		velocity: 6.39527777777778,
		power: -2822.42494914502,
		road: 2047.65310185185,
		acceleration: -0.602500000000001
	},
	{
		id: 407,
		time: 406,
		velocity: 5.71055555555556,
		power: -1534.65748955964,
		road: 2053.7312962963,
		acceleration: -0.413796296296295
	},
	{
		id: 408,
		time: 407,
		velocity: 5.50805555555556,
		power: 52.8572728404152,
		road: 2059.53421296296,
		acceleration: -0.13675925925926
	},
	{
		id: 409,
		time: 408,
		velocity: 5.985,
		power: 2123.62264768805,
		road: 2065.3862962963,
		acceleration: 0.235092592592593
	},
	{
		id: 410,
		time: 409,
		velocity: 6.41583333333333,
		power: 3288.77170606496,
		road: 2071.56157407407,
		acceleration: 0.411296296296296
	},
	{
		id: 411,
		time: 410,
		velocity: 6.74194444444444,
		power: 2325.42826634877,
		road: 2078.05523148148,
		acceleration: 0.225462962962964
	},
	{
		id: 412,
		time: 411,
		velocity: 6.66138888888889,
		power: 1220.10664317175,
		road: 2084.68236111111,
		acceleration: 0.0414814814814806
	},
	{
		id: 413,
		time: 412,
		velocity: 6.54027777777778,
		power: 104.959262112173,
		road: 2091.26273148148,
		acceleration: -0.134999999999999
	},
	{
		id: 414,
		time: 413,
		velocity: 6.33694444444444,
		power: 122.384652162571,
		road: 2097.71018518518,
		acceleration: -0.130833333333334
	},
	{
		id: 415,
		time: 414,
		velocity: 6.26888888888889,
		power: -220.175389231697,
		road: 2103.99898148148,
		acceleration: -0.186481481481482
	},
	{
		id: 416,
		time: 415,
		velocity: 5.98083333333333,
		power: -208.826127298297,
		road: 2110.10236111111,
		acceleration: -0.184351851851852
	},
	{
		id: 417,
		time: 416,
		velocity: 5.78388888888889,
		power: -295.910538428345,
		road: 2116.0137037037,
		acceleration: -0.199722222222221
	},
	{
		id: 418,
		time: 417,
		velocity: 5.66972222222222,
		power: 629.903859435799,
		road: 2121.80921296296,
		acceleration: -0.031944444444445
	},
	{
		id: 419,
		time: 418,
		velocity: 5.885,
		power: 1076.55445992573,
		road: 2127.61314814815,
		acceleration: 0.0487962962962962
	},
	{
		id: 420,
		time: 419,
		velocity: 5.93027777777778,
		power: 1724.43129521989,
		road: 2133.52148148148,
		acceleration: 0.159999999999999
	},
	{
		id: 421,
		time: 420,
		velocity: 6.14972222222222,
		power: 1453.04898492774,
		road: 2139.56236111111,
		acceleration: 0.105092592592593
	},
	{
		id: 422,
		time: 421,
		velocity: 6.20027777777778,
		power: 1616.97546216312,
		road: 2145.71953703704,
		acceleration: 0.1275
	},
	{
		id: 423,
		time: 422,
		velocity: 6.31277777777778,
		power: 833.385421737797,
		road: 2151.93638888889,
		acceleration: -0.00814814814814824
	},
	{
		id: 424,
		time: 423,
		velocity: 6.12527777777778,
		power: 918.029490700406,
		road: 2158.15226851852,
		acceleration: 0.00620370370370349
	},
	{
		id: 425,
		time: 424,
		velocity: 6.21888888888889,
		power: 1178.32333803697,
		road: 2164.39583333333,
		acceleration: 0.0491666666666664
	},
	{
		id: 426,
		time: 425,
		velocity: 6.46027777777778,
		power: 2017.83318744463,
		road: 2170.75574074074,
		acceleration: 0.18351851851852
	},
	{
		id: 427,
		time: 426,
		velocity: 6.67583333333333,
		power: 1804.38767762895,
		road: 2177.27722222222,
		acceleration: 0.13962962962963
	},
	{
		id: 428,
		time: 427,
		velocity: 6.63777777777778,
		power: 905.279018882814,
		road: 2183.86486111111,
		acceleration: -0.00731481481481566
	},
	{
		id: 429,
		time: 428,
		velocity: 6.43833333333333,
		power: -147.295688665385,
		road: 2190.36134259259,
		acceleration: -0.174999999999999
	},
	{
		id: 430,
		time: 429,
		velocity: 6.15083333333333,
		power: -168.195274094949,
		road: 2196.68138888889,
		acceleration: -0.177870370370371
	},
	{
		id: 431,
		time: 430,
		velocity: 6.10416666666667,
		power: 262.139637457709,
		road: 2202.86037037037,
		acceleration: -0.10425925925926
	},
	{
		id: 432,
		time: 431,
		velocity: 6.12555555555556,
		power: 959.42453447889,
		road: 2208.99518518518,
		acceleration: 0.0159259259259272
	},
	{
		id: 433,
		time: 432,
		velocity: 6.19861111111111,
		power: 1118.71675033966,
		road: 2215.15902777778,
		acceleration: 0.0421296296296303
	},
	{
		id: 434,
		time: 433,
		velocity: 6.23055555555556,
		power: 1455.19481810925,
		road: 2221.39208333333,
		acceleration: 0.0962962962962948
	},
	{
		id: 435,
		time: 434,
		velocity: 6.41444444444444,
		power: 1469.89465175136,
		road: 2227.72046296296,
		acceleration: 0.0943518518518518
	},
	{
		id: 436,
		time: 435,
		velocity: 6.48166666666667,
		power: 1722.80397586653,
		road: 2234.1612962963,
		acceleration: 0.130555555555556
	},
	{
		id: 437,
		time: 436,
		velocity: 6.62222222222222,
		power: 2168.56286295998,
		road: 2240.76412037037,
		acceleration: 0.193425925925927
	},
	{
		id: 438,
		time: 437,
		velocity: 6.99472222222222,
		power: 3366.73680033633,
		road: 2247.64388888889,
		acceleration: 0.360462962962963
	},
	{
		id: 439,
		time: 438,
		velocity: 7.56305555555556,
		power: 4557.07788007798,
		road: 2254.95282407407,
		acceleration: 0.497870370370371
	},
	{
		id: 440,
		time: 439,
		velocity: 8.11583333333333,
		power: 5080.74821278361,
		road: 2262.7712037037,
		acceleration: 0.521018518518519
	},
	{
		id: 441,
		time: 440,
		velocity: 8.55777777777778,
		power: 4772.17368655795,
		road: 2271.06907407407,
		acceleration: 0.437962962962963
	},
	{
		id: 442,
		time: 441,
		velocity: 8.87694444444444,
		power: 3473.37524780091,
		road: 2279.71222222222,
		acceleration: 0.252592592592594
	},
	{
		id: 443,
		time: 442,
		velocity: 8.87361111111111,
		power: 1673.14733689659,
		road: 2288.49625,
		acceleration: 0.029166666666665
	},
	{
		id: 444,
		time: 443,
		velocity: 8.64527777777778,
		power: -489.198948459717,
		road: 2297.18032407407,
		acceleration: -0.229074074074074
	},
	{
		id: 445,
		time: 444,
		velocity: 8.18972222222222,
		power: -2249.86320547555,
		road: 2305.52490740741,
		acceleration: -0.449907407407407
	},
	{
		id: 446,
		time: 445,
		velocity: 7.52388888888889,
		power: -2821.15682078761,
		road: 2313.37462962963,
		acceleration: -0.539814814814815
	},
	{
		id: 447,
		time: 446,
		velocity: 7.02583333333333,
		power: -2324.91111238249,
		road: 2320.70898148148,
		acceleration: -0.490925925925926
	},
	{
		id: 448,
		time: 447,
		velocity: 6.71694444444444,
		power: -1614.49154555017,
		road: 2327.59759259259,
		acceleration: -0.400555555555556
	},
	{
		id: 449,
		time: 448,
		velocity: 6.32222222222222,
		power: -821.654949012356,
		road: 2334.14416666667,
		acceleration: -0.283518518518517
	},
	{
		id: 450,
		time: 449,
		velocity: 6.17527777777778,
		power: -406.347800978132,
		road: 2340.44018518518,
		acceleration: -0.217592592592593
	},
	{
		id: 451,
		time: 450,
		velocity: 6.06416666666667,
		power: 136.330983041157,
		road: 2346.56486111111,
		acceleration: -0.125092592592594
	},
	{
		id: 452,
		time: 451,
		velocity: 5.94694444444444,
		power: -55.5805589635803,
		road: 2352.54833333333,
		acceleration: -0.157314814814814
	},
	{
		id: 453,
		time: 452,
		velocity: 5.70333333333333,
		power: 155.635878094115,
		road: 2358.39384259259,
		acceleration: -0.118611111111111
	},
	{
		id: 454,
		time: 453,
		velocity: 5.70833333333333,
		power: -317.534468587597,
		road: 2364.07787037037,
		acceleration: -0.204351851851851
	},
	{
		id: 455,
		time: 454,
		velocity: 5.33388888888889,
		power: -497.353547321017,
		road: 2369.53972222222,
		acceleration: -0.239999999999999
	},
	{
		id: 456,
		time: 455,
		velocity: 4.98333333333333,
		power: -922.398544579765,
		road: 2374.71657407407,
		acceleration: -0.33
	},
	{
		id: 457,
		time: 456,
		velocity: 4.71833333333333,
		power: -811.5794132999,
		road: 2379.57009259259,
		acceleration: -0.316666666666667
	},
	{
		id: 458,
		time: 457,
		velocity: 4.38388888888889,
		power: -755.113803154644,
		road: 2384.10819444444,
		acceleration: -0.314166666666666
	},
	{
		id: 459,
		time: 458,
		velocity: 4.04083333333333,
		power: -776.575110449582,
		road: 2388.32351851852,
		acceleration: -0.331388888888889
	},
	{
		id: 460,
		time: 459,
		velocity: 3.72416666666667,
		power: -541.665949663822,
		road: 2392.23217592593,
		acceleration: -0.281944444444445
	},
	{
		id: 461,
		time: 460,
		velocity: 3.53805555555556,
		power: -598.890649887523,
		road: 2395.84518518518,
		acceleration: -0.309351851851852
	},
	{
		id: 462,
		time: 461,
		velocity: 3.11277777777778,
		power: 78.1363711739651,
		road: 2399.24865740741,
		acceleration: -0.109722222222222
	},
	{
		id: 463,
		time: 462,
		velocity: 3.395,
		power: 530.413018658082,
		road: 2402.61342592593,
		acceleration: 0.0323148148148147
	},
	{
		id: 464,
		time: 463,
		velocity: 3.635,
		power: 1304.7777681809,
		road: 2406.12300925926,
		acceleration: 0.257314814814814
	},
	{
		id: 465,
		time: 464,
		velocity: 3.88472222222222,
		power: 1381.37831731428,
		road: 2409.88685185185,
		acceleration: 0.251203703703704
	},
	{
		id: 466,
		time: 465,
		velocity: 4.14861111111111,
		power: 1375.54238202237,
		road: 2413.88907407407,
		acceleration: 0.225555555555555
	},
	{
		id: 467,
		time: 466,
		velocity: 4.31166666666667,
		power: 1099.53273390641,
		road: 2418.0737962963,
		acceleration: 0.139444444444445
	},
	{
		id: 468,
		time: 467,
		velocity: 4.30305555555556,
		power: 785.546924190921,
		road: 2422.35597222222,
		acceleration: 0.055462962962963
	},
	{
		id: 469,
		time: 468,
		velocity: 4.315,
		power: 1132.48350891322,
		road: 2426.73300925926,
		acceleration: 0.13425925925926
	},
	{
		id: 470,
		time: 469,
		velocity: 4.71444444444445,
		power: 1301.33541554667,
		road: 2431.25907407407,
		acceleration: 0.163796296296297
	},
	{
		id: 471,
		time: 470,
		velocity: 4.79444444444445,
		power: 1549.19542473842,
		road: 2435.97018518518,
		acceleration: 0.206296296296296
	},
	{
		id: 472,
		time: 471,
		velocity: 4.93388888888889,
		power: 607.783975346794,
		road: 2440.78074074074,
		acceleration: -0.00740740740740797
	},
	{
		id: 473,
		time: 472,
		velocity: 4.69222222222222,
		power: 155.170605510062,
		road: 2445.53472222222,
		acceleration: -0.105740740740741
	},
	{
		id: 474,
		time: 473,
		velocity: 4.47722222222222,
		power: 66.7182652170473,
		road: 2450.17365740741,
		acceleration: -0.124351851851852
	},
	{
		id: 475,
		time: 474,
		velocity: 4.56083333333333,
		power: -55.0739000465602,
		road: 2454.67458333333,
		acceleration: -0.151666666666667
	},
	{
		id: 476,
		time: 475,
		velocity: 4.23722222222222,
		power: -28.3680062128558,
		road: 2459.02722222222,
		acceleration: -0.144907407407407
	},
	{
		id: 477,
		time: 476,
		velocity: 4.0425,
		power: -47.6143467483278,
		road: 2463.23277777778,
		acceleration: -0.149259259259259
	},
	{
		id: 478,
		time: 477,
		velocity: 4.11305555555556,
		power: 145.683244800435,
		road: 2467.31412037037,
		acceleration: -0.0991666666666662
	},
	{
		id: 479,
		time: 478,
		velocity: 3.93972222222222,
		power: -260.915878636187,
		road: 2471.24287037037,
		acceleration: -0.206018518518519
	},
	{
		id: 480,
		time: 479,
		velocity: 3.42444444444444,
		power: -1083.50296712624,
		road: 2474.84273148148,
		acceleration: -0.451759259259259
	},
	{
		id: 481,
		time: 480,
		velocity: 2.75777777777778,
		power: -675.191958521213,
		road: 2478.03884259259,
		acceleration: -0.355740740740741
	},
	{
		id: 482,
		time: 481,
		velocity: 2.8725,
		power: 74.6058578690546,
		road: 2481.00416666667,
		acceleration: -0.105833333333333
	},
	{
		id: 483,
		time: 482,
		velocity: 3.10694444444444,
		power: 881.705869212323,
		road: 2484.00513888889,
		acceleration: 0.177129629629629
	},
	{
		id: 484,
		time: 483,
		velocity: 3.28916666666667,
		power: 1148.79938057703,
		road: 2487.21652777778,
		acceleration: 0.243703703703704
	},
	{
		id: 485,
		time: 484,
		velocity: 3.60361111111111,
		power: 1080.64910014747,
		road: 2490.64861111111,
		acceleration: 0.197685185185186
	},
	{
		id: 486,
		time: 485,
		velocity: 3.7,
		power: 1290.25977492954,
		road: 2494.29828703704,
		acceleration: 0.2375
	},
	{
		id: 487,
		time: 486,
		velocity: 4.00166666666667,
		power: 1395.35832491798,
		road: 2498.18768518518,
		acceleration: 0.241944444444445
	},
	{
		id: 488,
		time: 487,
		velocity: 4.32944444444444,
		power: 1405.71224297331,
		road: 2502.30916666667,
		acceleration: 0.222222222222222
	},
	{
		id: 489,
		time: 488,
		velocity: 4.36666666666667,
		power: 951.649444666368,
		road: 2506.5899537037,
		acceleration: 0.096388888888888
	},
	{
		id: 490,
		time: 489,
		velocity: 4.29083333333333,
		power: 48.9008965544911,
		road: 2510.85615740741,
		acceleration: -0.125555555555555
	},
	{
		id: 491,
		time: 490,
		velocity: 3.95277777777778,
		power: -234.893274772268,
		road: 2514.96101851852,
		acceleration: -0.197129629629631
	},
	{
		id: 492,
		time: 491,
		velocity: 3.77527777777778,
		power: 305.345884832632,
		road: 2518.93958333333,
		acceleration: -0.0554629629629626
	},
	{
		id: 493,
		time: 492,
		velocity: 4.12444444444444,
		power: 658.832372604629,
		road: 2522.90967592593,
		acceleration: 0.0385185185185186
	},
	{
		id: 494,
		time: 493,
		velocity: 4.06833333333333,
		power: 881.01314349774,
		road: 2526.94569444444,
		acceleration: 0.0933333333333333
	},
	{
		id: 495,
		time: 494,
		velocity: 4.05527777777778,
		power: -281.513638146284,
		road: 2530.92296296296,
		acceleration: -0.210833333333333
	},
	{
		id: 496,
		time: 495,
		velocity: 3.49194444444444,
		power: -1050.28876474071,
		road: 2534.57592592593,
		acceleration: -0.437777777777778
	},
	{
		id: 497,
		time: 496,
		velocity: 2.755,
		power: -1082.48350086064,
		road: 2537.76458333333,
		acceleration: -0.490833333333333
	},
	{
		id: 498,
		time: 497,
		velocity: 2.58277777777778,
		power: -754.70059830138,
		road: 2540.49643518519,
		acceleration: -0.422777777777777
	},
	{
		id: 499,
		time: 498,
		velocity: 2.22361111111111,
		power: -223.762387336136,
		road: 2542.9025462963,
		acceleration: -0.228703703703704
	},
	{
		id: 500,
		time: 499,
		velocity: 2.06888888888889,
		power: -123.788081764544,
		road: 2545.09953703704,
		acceleration: -0.189537037037038
	},
	{
		id: 501,
		time: 500,
		velocity: 2.01416666666667,
		power: 239.066358815021,
		road: 2547.19689814815,
		acceleration: -0.00972222222222241
	},
	{
		id: 502,
		time: 501,
		velocity: 2.19444444444444,
		power: 246.418207565179,
		road: 2549.28662037037,
		acceleration: -0.00555555555555554
	},
	{
		id: 503,
		time: 502,
		velocity: 2.05222222222222,
		power: 299.27941816523,
		road: 2551.38384259259,
		acceleration: 0.0205555555555557
	},
	{
		id: 504,
		time: 503,
		velocity: 2.07583333333333,
		power: 348.63276523305,
		road: 2553.51268518519,
		acceleration: 0.042685185185185
	},
	{
		id: 505,
		time: 504,
		velocity: 2.3225,
		power: 740.976300359256,
		road: 2555.77069444444,
		acceleration: 0.215648148148148
	},
	{
		id: 506,
		time: 505,
		velocity: 2.69916666666667,
		power: 1288.48717620844,
		road: 2558.33569444444,
		acceleration: 0.398333333333333
	},
	{
		id: 507,
		time: 506,
		velocity: 3.27083333333333,
		power: 2448.67046143443,
		road: 2561.44791666667,
		acceleration: 0.696111111111111
	},
	{
		id: 508,
		time: 507,
		velocity: 4.41083333333333,
		power: 3141.47965502401,
		road: 2565.27287037037,
		acceleration: 0.729351851851852
	},
	{
		id: 509,
		time: 508,
		velocity: 4.88722222222222,
		power: 3353.49478336222,
		road: 2569.78435185185,
		acceleration: 0.643703703703704
	},
	{
		id: 510,
		time: 509,
		velocity: 5.20194444444444,
		power: 2210.71208967171,
		road: 2574.77986111111,
		acceleration: 0.324351851851852
	},
	{
		id: 511,
		time: 510,
		velocity: 5.38388888888889,
		power: 3075.56321382627,
		road: 2580.16611111111,
		acceleration: 0.457129629629629
	},
	{
		id: 512,
		time: 511,
		velocity: 6.25861111111111,
		power: 4997.73928437758,
		road: 2586.14666666667,
		acceleration: 0.731481481481481
	},
	{
		id: 513,
		time: 512,
		velocity: 7.39638888888889,
		power: 5909.25641252863,
		road: 2592.87800925926,
		acceleration: 0.770092592592593
	},
	{
		id: 514,
		time: 513,
		velocity: 7.69416666666667,
		power: 5131.10396205847,
		road: 2600.27958333333,
		acceleration: 0.57037037037037
	},
	{
		id: 515,
		time: 514,
		velocity: 7.96972222222222,
		power: 4546.25929311836,
		road: 2608.18708333333,
		acceleration: 0.441481481481483
	},
	{
		id: 516,
		time: 515,
		velocity: 8.72083333333333,
		power: 4704.86052227665,
		road: 2616.52828703704,
		acceleration: 0.425925925925926
	},
	{
		id: 517,
		time: 516,
		velocity: 8.97194444444444,
		power: 4752.05577630168,
		road: 2625.28222222222,
		acceleration: 0.399537037037035
	},
	{
		id: 518,
		time: 517,
		velocity: 9.16833333333333,
		power: 5567.00424691272,
		road: 2634.46666666667,
		acceleration: 0.461481481481483
	},
	{
		id: 519,
		time: 518,
		velocity: 10.1052777777778,
		power: 6823.43992658226,
		road: 2644.16106481481,
		acceleration: 0.558425925925924
	},
	{
		id: 520,
		time: 519,
		velocity: 10.6472222222222,
		power: 7155.80857909538,
		road: 2654.40777777778,
		acceleration: 0.546203703703705
	},
	{
		id: 521,
		time: 520,
		velocity: 10.8069444444444,
		power: 4035.55042412033,
		road: 2665.03134259259,
		acceleration: 0.207499999999998
	},
	{
		id: 522,
		time: 521,
		velocity: 10.7277777777778,
		power: 1783.74876797786,
		road: 2675.74986111111,
		acceleration: -0.0175925925925888
	},
	{
		id: 523,
		time: 522,
		velocity: 10.5944444444444,
		power: 2475.28030876953,
		road: 2686.48435185185,
		acceleration: 0.0495370370370338
	},
	{
		id: 524,
		time: 523,
		velocity: 10.9555555555556,
		power: 4979.51567369535,
		road: 2697.38597222222,
		acceleration: 0.284722222222223
	},
	{
		id: 525,
		time: 524,
		velocity: 11.5819444444444,
		power: 6026.53954205421,
		road: 2708.61222222222,
		acceleration: 0.364537037037037
	},
	{
		id: 526,
		time: 525,
		velocity: 11.6880555555556,
		power: 6005.46597202159,
		road: 2720.19115740741,
		acceleration: 0.340833333333336
	},
	{
		id: 527,
		time: 526,
		velocity: 11.9780555555556,
		power: 3184.82271504621,
		road: 2731.97925925926,
		acceleration: 0.077499999999997
	},
	{
		id: 528,
		time: 527,
		velocity: 11.8144444444444,
		power: 4146.35046654673,
		road: 2743.885,
		acceleration: 0.157777777777779
	},
	{
		id: 529,
		time: 528,
		velocity: 12.1613888888889,
		power: 5294.31083659763,
		road: 2755.99375,
		acceleration: 0.248240740740739
	},
	{
		id: 530,
		time: 529,
		velocity: 12.7227777777778,
		power: 10161.0118716158,
		road: 2768.54277777778,
		acceleration: 0.632314814814814
	},
	{
		id: 531,
		time: 530,
		velocity: 13.7113888888889,
		power: 10294.8381148391,
		road: 2781.70509259259,
		acceleration: 0.59425925925926
	},
	{
		id: 532,
		time: 531,
		velocity: 13.9441666666667,
		power: 7665.32279028633,
		road: 2795.34273148148,
		acceleration: 0.356388888888889
	},
	{
		id: 533,
		time: 532,
		velocity: 13.7919444444444,
		power: 3664.07618394959,
		road: 2809.17953703704,
		acceleration: 0.0419444444444448
	},
	{
		id: 534,
		time: 533,
		velocity: 13.8372222222222,
		power: 3722.6820455162,
		road: 2823.05972222222,
		acceleration: 0.0448148148148153
	},
	{
		id: 535,
		time: 534,
		velocity: 14.0786111111111,
		power: 4760.93190660331,
		road: 2837.02217592593,
		acceleration: 0.119722222222222
	},
	{
		id: 536,
		time: 535,
		velocity: 14.1511111111111,
		power: 5550.90982226278,
		road: 2851.13064814815,
		acceleration: 0.172314814814815
	},
	{
		id: 537,
		time: 536,
		velocity: 14.3541666666667,
		power: 6935.82157527465,
		road: 2865.45712962963,
		acceleration: 0.263703703703705
	},
	{
		id: 538,
		time: 537,
		velocity: 14.8697222222222,
		power: 6330.20328052625,
		road: 2880.01949074074,
		acceleration: 0.208055555555555
	},
	{
		id: 539,
		time: 538,
		velocity: 14.7752777777778,
		power: 4882.30440783417,
		road: 2894.73476851852,
		acceleration: 0.0977777777777789
	},
	{
		id: 540,
		time: 539,
		velocity: 14.6475,
		power: 2204.36767275585,
		road: 2909.4525462963,
		acceleration: -0.0927777777777781
	},
	{
		id: 541,
		time: 540,
		velocity: 14.5913888888889,
		power: 2132.21586051035,
		road: 2924.07625,
		acceleration: -0.0953703703703717
	},
	{
		id: 542,
		time: 541,
		velocity: 14.4891666666667,
		power: 3779.39514900886,
		road: 2938.66416666667,
		acceleration: 0.0237962962962968
	},
	{
		id: 543,
		time: 542,
		velocity: 14.7188888888889,
		power: 3841.06090121031,
		road: 2953.27763888889,
		acceleration: 0.0273148148148135
	},
	{
		id: 544,
		time: 543,
		velocity: 14.6733333333333,
		power: 4437.99014467563,
		road: 2967.93888888889,
		acceleration: 0.068240740740741
	},
	{
		id: 545,
		time: 544,
		velocity: 14.6938888888889,
		power: 2459.30478244356,
		road: 2982.59777777778,
		acceleration: -0.0729629629629613
	},
	{
		id: 546,
		time: 545,
		velocity: 14.5,
		power: -1404.94740622197,
		road: 2997.04671296296,
		acceleration: -0.346944444444446
	},
	{
		id: 547,
		time: 546,
		velocity: 13.6325,
		power: -1154.17620621974,
		road: 3011.15949074074,
		acceleration: -0.32537037037037
	},
	{
		id: 548,
		time: 547,
		velocity: 13.7177777777778,
		power: -2614.12457199088,
		road: 3024.89300925926,
		acceleration: -0.433148148148149
	},
	{
		id: 549,
		time: 548,
		velocity: 13.2005555555556,
		power: 1196.68660053102,
		road: 3038.34185185185,
		acceleration: -0.136203703703702
	},
	{
		id: 550,
		time: 549,
		velocity: 13.2238888888889,
		power: -988.027624927931,
		road: 3051.57041666667,
		acceleration: -0.304351851851854
	},
	{
		id: 551,
		time: 550,
		velocity: 12.8047222222222,
		power: 678.74191256296,
		road: 3064.56287037037,
		acceleration: -0.16787037037037
	},
	{
		id: 552,
		time: 551,
		velocity: 12.6969444444444,
		power: 915.869737598431,
		road: 3077.39861111111,
		acceleration: -0.145555555555553
	},
	{
		id: 553,
		time: 552,
		velocity: 12.7872222222222,
		power: -113.314052203678,
		road: 3090.04805555556,
		acceleration: -0.227037037037039
	},
	{
		id: 554,
		time: 553,
		velocity: 12.1236111111111,
		power: 2006.8884350487,
		road: 3102.56018518519,
		acceleration: -0.0475925925925935
	},
	{
		id: 555,
		time: 554,
		velocity: 12.5541666666667,
		power: 1128.55199056908,
		road: 3114.98884259259,
		acceleration: -0.119351851851849
	},
	{
		id: 556,
		time: 555,
		velocity: 12.4291666666667,
		power: 1192.62930550527,
		road: 3127.30212962963,
		acceleration: -0.111388888888889
	},
	{
		id: 557,
		time: 556,
		velocity: 11.7894444444444,
		power: 2005.34541459879,
		road: 3139.53967592593,
		acceleration: -0.0400925925925932
	},
	{
		id: 558,
		time: 557,
		velocity: 12.4338888888889,
		power: 2698.66380280114,
		road: 3151.76699074074,
		acceleration: 0.0196296296296286
	},
	{
		id: 559,
		time: 558,
		velocity: 12.4880555555556,
		power: 6592.45076374368,
		road: 3164.17541666667,
		acceleration: 0.342592592592593
	},
	{
		id: 560,
		time: 559,
		velocity: 12.8172222222222,
		power: 4026.276266547,
		road: 3176.81333333333,
		acceleration: 0.116388888888888
	},
	{
		id: 561,
		time: 560,
		velocity: 12.7830555555556,
		power: 4444.28837203329,
		road: 3189.58212962963,
		acceleration: 0.145370370370371
	},
	{
		id: 562,
		time: 561,
		velocity: 12.9241666666667,
		power: 4954.32965896502,
		road: 3202.51347222222,
		acceleration: 0.179722222222223
	},
	{
		id: 563,
		time: 562,
		velocity: 13.3563888888889,
		power: 5180.05056304542,
		road: 3215.62935185185,
		acceleration: 0.189351851851853
	},
	{
		id: 564,
		time: 563,
		velocity: 13.3511111111111,
		power: 4738.52287485844,
		road: 3228.91328703704,
		acceleration: 0.146759259259257
	},
	{
		id: 565,
		time: 564,
		velocity: 13.3644444444444,
		power: 1316.97638924698,
		road: 3242.20893518519,
		acceleration: -0.123333333333333
	},
	{
		id: 566,
		time: 565,
		velocity: 12.9863888888889,
		power: 135.265164801217,
		road: 3255.33601851852,
		acceleration: -0.213796296296296
	},
	{
		id: 567,
		time: 566,
		velocity: 12.7097222222222,
		power: -1687.87993653899,
		road: 3268.17712962963,
		acceleration: -0.358148148148148
	},
	{
		id: 568,
		time: 567,
		velocity: 12.29,
		power: -1131.66372377026,
		road: 3280.68393518519,
		acceleration: -0.310462962962962
	},
	{
		id: 569,
		time: 568,
		velocity: 12.055,
		power: 1095.87826204736,
		road: 3292.97592592593,
		acceleration: -0.119166666666667
	},
	{
		id: 570,
		time: 569,
		velocity: 12.3522222222222,
		power: 2413.38932545999,
		road: 3305.20587962963,
		acceleration: -0.00490740740740847
	},
	{
		id: 571,
		time: 570,
		velocity: 12.2752777777778,
		power: 2999.46150586802,
		road: 3317.45569444445,
		acceleration: 0.0446296296296307
	},
	{
		id: 572,
		time: 571,
		velocity: 12.1888888888889,
		power: 1624.32197057678,
		road: 3329.69148148148,
		acceleration: -0.0726851851851862
	},
	{
		id: 573,
		time: 572,
		velocity: 12.1341666666667,
		power: -318.038090404308,
		road: 3341.77226851852,
		acceleration: -0.237314814814814
	},
	{
		id: 574,
		time: 573,
		velocity: 11.5633333333333,
		power: -453.040705833883,
		road: 3353.61111111111,
		acceleration: -0.246574074074076
	},
	{
		id: 575,
		time: 574,
		velocity: 11.4491666666667,
		power: -556.681050260158,
		road: 3365.19990740741,
		acceleration: -0.253518518518517
	},
	{
		id: 576,
		time: 575,
		velocity: 11.3736111111111,
		power: 2193.06018187661,
		road: 3376.66152777778,
		acceleration: -0.000833333333334352
	},
	{
		id: 577,
		time: 576,
		velocity: 11.5608333333333,
		power: 3120.81282898426,
		road: 3388.16398148148,
		acceleration: 0.0825000000000014
	},
	{
		id: 578,
		time: 577,
		velocity: 11.6966666666667,
		power: 6460.83534929154,
		road: 3399.89393518519,
		acceleration: 0.372499999999999
	},
	{
		id: 579,
		time: 578,
		velocity: 12.4911111111111,
		power: 7521.06327198056,
		road: 3412.02972222222,
		acceleration: 0.439166666666669
	},
	{
		id: 580,
		time: 579,
		velocity: 12.8783333333333,
		power: 15363.1777500851,
		road: 3424.9,
		acceleration: 1.02981481481481
	},
	{
		id: 581,
		time: 580,
		velocity: 14.7861111111111,
		power: 17123.5330187933,
		road: 3438.81134259259,
		acceleration: 1.05231481481482
	},
	{
		id: 582,
		time: 581,
		velocity: 15.6480555555556,
		power: 23586.9601174094,
		road: 3453.93648148148,
		acceleration: 1.37527777777778
	},
	{
		id: 583,
		time: 582,
		velocity: 17.0041666666667,
		power: 17136.8675161815,
		road: 3470.16351851852,
		acceleration: 0.828518518518516
	},
	{
		id: 584,
		time: 583,
		velocity: 17.2716666666667,
		power: 14682.7251983353,
		road: 3487.11291666667,
		acceleration: 0.616203703703704
	},
	{
		id: 585,
		time: 584,
		velocity: 17.4966666666667,
		power: 6434.67139521103,
		road: 3504.41666666667,
		acceleration: 0.0925000000000011
	},
	{
		id: 586,
		time: 585,
		velocity: 17.2816666666667,
		power: 3171.11870694951,
		road: 3521.71444444445,
		acceleration: -0.104444444444443
	},
	{
		id: 587,
		time: 586,
		velocity: 16.9583333333333,
		power: -688.674635724967,
		road: 3538.79300925926,
		acceleration: -0.333981481481484
	},
	{
		id: 588,
		time: 587,
		velocity: 16.4947222222222,
		power: 1521.20886153765,
		road: 3555.60847222222,
		acceleration: -0.19222222222222
	},
	{
		id: 589,
		time: 588,
		velocity: 16.705,
		power: 3724.93154090792,
		road: 3572.30222222222,
		acceleration: -0.0512037037037025
	},
	{
		id: 590,
		time: 589,
		velocity: 16.8047222222222,
		power: 6329.42806428082,
		road: 3589.02569444445,
		acceleration: 0.110648148148147
	},
	{
		id: 591,
		time: 590,
		velocity: 16.8266666666667,
		power: 2427.85802178259,
		road: 3605.73800925926,
		acceleration: -0.132962962962964
	},
	{
		id: 592,
		time: 591,
		velocity: 16.3061111111111,
		power: 1357.16903814375,
		road: 3622.28583333333,
		acceleration: -0.196018518518517
	},
	{
		id: 593,
		time: 592,
		velocity: 16.2166666666667,
		power: -1793.41432937971,
		road: 3638.53976851852,
		acceleration: -0.39175925925926
	},
	{
		id: 594,
		time: 593,
		velocity: 15.6513888888889,
		power: -295.779005069924,
		road: 3654.45300925926,
		acceleration: -0.28962962962963
	},
	{
		id: 595,
		time: 594,
		velocity: 15.4372222222222,
		power: 160.270547756429,
		road: 3670.09412037037,
		acceleration: -0.254629629629628
	},
	{
		id: 596,
		time: 595,
		velocity: 15.4527777777778,
		power: 3180.36061963559,
		road: 3685.58398148148,
		acceleration: -0.0478703703703722
	},
	{
		id: 597,
		time: 596,
		velocity: 15.5077777777778,
		power: 5385.44145321974,
		road: 3701.09990740741,
		acceleration: 0.100000000000001
	},
	{
		id: 598,
		time: 597,
		velocity: 15.7372222222222,
		power: 6580.60167031953,
		road: 3716.75296296296,
		acceleration: 0.174259259259259
	},
	{
		id: 599,
		time: 598,
		velocity: 15.9755555555556,
		power: 6989.51665139858,
		road: 3732.58958333333,
		acceleration: 0.19287037037037
	},
	{
		id: 600,
		time: 599,
		velocity: 16.0863888888889,
		power: 7535.52420648005,
		road: 3748.63203703704,
		acceleration: 0.218796296296301
	},
	{
		id: 601,
		time: 600,
		velocity: 16.3936111111111,
		power: 8716.5056862177,
		road: 3764.92509259259,
		acceleration: 0.282407407407405
	},
	{
		id: 602,
		time: 601,
		velocity: 16.8227777777778,
		power: 7873.58986040943,
		road: 3781.46736111111,
		acceleration: 0.216018518518517
	},
	{
		id: 603,
		time: 602,
		velocity: 16.7344444444444,
		power: 8004.26224703116,
		road: 3798.22449074074,
		acceleration: 0.2137037037037
	},
	{
		id: 604,
		time: 603,
		velocity: 17.0347222222222,
		power: 6117.00559082703,
		road: 3815.13328703704,
		acceleration: 0.0896296296296342
	},
	{
		id: 605,
		time: 604,
		velocity: 17.0916666666667,
		power: 6334.37108591437,
		road: 3832.13643518519,
		acceleration: 0.0990740740740748
	},
	{
		id: 606,
		time: 605,
		velocity: 17.0316666666667,
		power: 5707.43976567734,
		road: 3849.21782407408,
		acceleration: 0.0574074074074069
	},
	{
		id: 607,
		time: 606,
		velocity: 17.2069444444444,
		power: 4953.20383320328,
		road: 3866.33291666667,
		acceleration: 0.0100000000000016
	},
	{
		id: 608,
		time: 607,
		velocity: 17.1216666666667,
		power: 2798.281267093,
		road: 3883.39300925926,
		acceleration: -0.120000000000001
	},
	{
		id: 609,
		time: 608,
		velocity: 16.6716666666667,
		power: 978.095402585069,
		road: 3900.27930555556,
		acceleration: -0.227592592592597
	},
	{
		id: 610,
		time: 609,
		velocity: 16.5241666666667,
		power: -814.781781346691,
		road: 3916.88472222222,
		acceleration: -0.334166666666665
	},
	{
		id: 611,
		time: 610,
		velocity: 16.1191666666667,
		power: 1970.29869644196,
		road: 3933.24685185185,
		acceleration: -0.152407407407406
	},
	{
		id: 612,
		time: 611,
		velocity: 16.2144444444444,
		power: -1000.66073449027,
		road: 3949.36337962963,
		acceleration: -0.338796296296298
	},
	{
		id: 613,
		time: 612,
		velocity: 15.5077777777778,
		power: -1695.86566663917,
		road: 3965.12050925926,
		acceleration: -0.380000000000001
	},
	{
		id: 614,
		time: 613,
		velocity: 14.9791666666667,
		power: -4680.39690204621,
		road: 3980.39782407408,
		acceleration: -0.579629629629626
	},
	{
		id: 615,
		time: 614,
		velocity: 14.4755555555556,
		power: -1509.58332189603,
		road: 3995.20643518519,
		acceleration: -0.357777777777782
	},
	{
		id: 616,
		time: 615,
		velocity: 14.4344444444444,
		power: -474.529597383818,
		road: 4009.69611111111,
		acceleration: -0.280092592592593
	},
	{
		id: 617,
		time: 616,
		velocity: 14.1388888888889,
		power: 2174.82279856261,
		road: 4024.00388888889,
		acceleration: -0.0837037037037014
	},
	{
		id: 618,
		time: 617,
		velocity: 14.2244444444444,
		power: 846.325384415531,
		road: 4038.18069444445,
		acceleration: -0.17824074074074
	},
	{
		id: 619,
		time: 618,
		velocity: 13.8997222222222,
		power: 3018.61173921245,
		road: 4052.26101851852,
		acceleration: -0.0147222222222236
	},
	{
		id: 620,
		time: 619,
		velocity: 14.0947222222222,
		power: 3296.81948495603,
		road: 4066.33703703704,
		acceleration: 0.00611111111111207
	},
	{
		id: 621,
		time: 620,
		velocity: 14.2427777777778,
		power: 4941.04079853535,
		road: 4080.47893518519,
		acceleration: 0.12564814814815
	},
	{
		id: 622,
		time: 621,
		velocity: 14.2766666666667,
		power: 6323.42039059783,
		road: 4094.79342592593,
		acceleration: 0.219537037037036
	},
	{
		id: 623,
		time: 622,
		velocity: 14.7533333333333,
		power: 6231.21058634411,
		road: 4109.31902777778,
		acceleration: 0.202685185185187
	},
	{
		id: 624,
		time: 623,
		velocity: 14.8508333333333,
		power: 4256.89235493381,
		road: 4123.97375,
		acceleration: 0.0555555555555536
	},
	{
		id: 625,
		time: 624,
		velocity: 14.4433333333333,
		power: 5126.90748120631,
		road: 4138.71333333334,
		acceleration: 0.114166666666668
	},
	{
		id: 626,
		time: 625,
		velocity: 15.0958333333333,
		power: 4860.21517015972,
		road: 4153.55555555556,
		acceleration: 0.0911111111111111
	},
	{
		id: 627,
		time: 626,
		velocity: 15.1241666666667,
		power: 7995.67263184813,
		road: 4168.59407407408,
		acceleration: 0.301481481481481
	},
	{
		id: 628,
		time: 627,
		velocity: 15.3477777777778,
		power: 5731.26424160657,
		road: 4183.85055555556,
		acceleration: 0.134444444444446
	},
	{
		id: 629,
		time: 628,
		velocity: 15.4991666666667,
		power: 5830.26303855385,
		road: 4199.24194444445,
		acceleration: 0.135370370370369
	},
	{
		id: 630,
		time: 629,
		velocity: 15.5302777777778,
		power: 5214.63587415175,
		road: 4214.74550925926,
		acceleration: 0.0889814814814809
	},
	{
		id: 631,
		time: 630,
		velocity: 15.6147222222222,
		power: 10189.8251094836,
		road: 4230.49824074074,
		acceleration: 0.409351851851852
	},
	{
		id: 632,
		time: 631,
		velocity: 16.7272222222222,
		power: 8816.04863622361,
		road: 4246.6050462963,
		acceleration: 0.298796296296294
	},
	{
		id: 633,
		time: 632,
		velocity: 16.4266666666667,
		power: 8858.11459463718,
		road: 4263.00416666667,
		acceleration: 0.285833333333336
	},
	{
		id: 634,
		time: 633,
		velocity: 16.4722222222222,
		power: 2461.04327684637,
		road: 4279.48402777778,
		acceleration: -0.124351851851856
	},
	{
		id: 635,
		time: 634,
		velocity: 16.3541666666667,
		power: -2741.50013816469,
		road: 4295.67555555556,
		acceleration: -0.452314814814811
	},
	{
		id: 636,
		time: 635,
		velocity: 15.0697222222222,
		power: 1307.16629492908,
		road: 4311.54925925926,
		acceleration: -0.183333333333334
	},
	{
		id: 637,
		time: 636,
		velocity: 15.9222222222222,
		power: -2359.77431518248,
		road: 4327.11995370371,
		acceleration: -0.422685185185188
	},
	{
		id: 638,
		time: 637,
		velocity: 15.0861111111111,
		power: 4587.1813140467,
		road: 4342.50486111111,
		acceleration: 0.051111111111112
	},
	{
		id: 639,
		time: 638,
		velocity: 15.2230555555556,
		power: -299.76429484313,
		road: 4357.77560185185,
		acceleration: -0.279444444444445
	},
	{
		id: 640,
		time: 639,
		velocity: 15.0838888888889,
		power: 2995.01030452243,
		road: 4372.88231481482,
		acceleration: -0.0486111111111089
	},
	{
		id: 641,
		time: 640,
		velocity: 14.9402777777778,
		power: 1330.60120669449,
		road: 4387.88398148148,
		acceleration: -0.161481481481484
	},
	{
		id: 642,
		time: 641,
		velocity: 14.7386111111111,
		power: 1107.77926317807,
		road: 4402.71824074074,
		acceleration: -0.173333333333332
	},
	{
		id: 643,
		time: 642,
		velocity: 14.5638888888889,
		power: 1597.79802588712,
		road: 4417.39833333334,
		acceleration: -0.135
	},
	{
		id: 644,
		time: 643,
		velocity: 14.5352777777778,
		power: 1384.20753300483,
		road: 4431.93745370371,
		acceleration: -0.146944444444443
	},
	{
		id: 645,
		time: 644,
		velocity: 14.2977777777778,
		power: 5760.50453779347,
		road: 4446.48694444445,
		acceleration: 0.167685185185185
	},
	{
		id: 646,
		time: 645,
		velocity: 15.0669444444444,
		power: 4059.52158880497,
		road: 4461.14101851852,
		acceleration: 0.0414814814814815
	},
	{
		id: 647,
		time: 646,
		velocity: 14.6597222222222,
		power: 6468.9351844715,
		road: 4475.91962962963,
		acceleration: 0.207592592592592
	},
	{
		id: 648,
		time: 647,
		velocity: 14.9205555555556,
		power: 5305.47041456144,
		road: 4490.86120370371,
		acceleration: 0.118333333333334
	},
	{
		id: 649,
		time: 648,
		velocity: 15.4219444444444,
		power: 9044.55257525488,
		road: 4506.04490740741,
		acceleration: 0.365925925925925
	},
	{
		id: 650,
		time: 649,
		velocity: 15.7575,
		power: 8962.31030296461,
		road: 4521.58157407408,
		acceleration: 0.34
	},
	{
		id: 651,
		time: 650,
		velocity: 15.9405555555556,
		power: 7526.0869927793,
		road: 4537.40277777778,
		acceleration: 0.229074074074076
	},
	{
		id: 652,
		time: 651,
		velocity: 16.1091666666667,
		power: 4912.96759899626,
		road: 4553.36398148148,
		acceleration: 0.0509259259259256
	},
	{
		id: 653,
		time: 652,
		velocity: 15.9102777777778,
		power: 4542.96589450059,
		road: 4569.36328703704,
		acceleration: 0.0252777777777755
	},
	{
		id: 654,
		time: 653,
		velocity: 16.0163888888889,
		power: 1.46600983322096,
		road: 4585.24050925926,
		acceleration: -0.269444444444442
	},
	{
		id: 655,
		time: 654,
		velocity: 15.3008333333333,
		power: -844.517541593901,
		road: 4600.8225,
		acceleration: -0.321018518518519
	},
	{
		id: 656,
		time: 655,
		velocity: 14.9472222222222,
		power: -3957.04342261748,
		road: 4615.97888888889,
		acceleration: -0.530185185185184
	},
	{
		id: 657,
		time: 656,
		velocity: 14.4258333333333,
		power: 53.8987926070779,
		road: 4630.74689814815,
		acceleration: -0.246574074074074
	},
	{
		id: 658,
		time: 657,
		velocity: 14.5611111111111,
		power: 1449.39668874358,
		road: 4645.32009259259,
		acceleration: -0.143055555555557
	},
	{
		id: 659,
		time: 658,
		velocity: 14.5180555555556,
		power: 4511.286816762,
		road: 4659.86083333334,
		acceleration: 0.0781481481481503
	},
	{
		id: 660,
		time: 659,
		velocity: 14.6602777777778,
		power: 4537.84630839161,
		road: 4674.47916666667,
		acceleration: 0.0770370370370372
	},
	{
		id: 661,
		time: 660,
		velocity: 14.7922222222222,
		power: 4980.43543823648,
		road: 4689.18851851852,
		acceleration: 0.104999999999999
	},
	{
		id: 662,
		time: 661,
		velocity: 14.8330555555556,
		power: 2877.4966306305,
		road: 4703.92759259259,
		acceleration: -0.0455555555555538
	},
	{
		id: 663,
		time: 662,
		velocity: 14.5236111111111,
		power: 1376.08778846139,
		road: 4718.56893518519,
		acceleration: -0.14990740740741
	},
	{
		id: 664,
		time: 663,
		velocity: 14.3425,
		power: 459.807553049011,
		road: 4733.02930555556,
		acceleration: -0.212037037037035
	},
	{
		id: 665,
		time: 664,
		velocity: 14.1969444444444,
		power: 1656.35802902082,
		road: 4747.32300925926,
		acceleration: -0.121296296296295
	},
	{
		id: 666,
		time: 665,
		velocity: 14.1597222222222,
		power: 1650.10469429866,
		road: 4761.49666666667,
		acceleration: -0.118796296296299
	},
	{
		id: 667,
		time: 666,
		velocity: 13.9861111111111,
		power: 1453.61543413226,
		road: 4775.54574074074,
		acceleration: -0.130370370370368
	},
	{
		id: 668,
		time: 667,
		velocity: 13.8058333333333,
		power: 1770.37083580837,
		road: 4789.47773148148,
		acceleration: -0.103796296296299
	},
	{
		id: 669,
		time: 668,
		velocity: 13.8483333333333,
		power: 1463.48279240249,
		road: 4803.29574074074,
		acceleration: -0.124166666666667
	},
	{
		id: 670,
		time: 669,
		velocity: 13.6136111111111,
		power: 2509.81457101519,
		road: 4817.03046296297,
		acceleration: -0.0424074074074063
	},
	{
		id: 671,
		time: 670,
		velocity: 13.6786111111111,
		power: 2292.09967211659,
		road: 4830.71518518519,
		acceleration: -0.0575925925925915
	},
	{
		id: 672,
		time: 671,
		velocity: 13.6755555555556,
		power: -278.683737769052,
		road: 4844.245,
		acceleration: -0.252222222222223
	},
	{
		id: 673,
		time: 672,
		velocity: 12.8569444444444,
		power: -6697.14618326676,
		road: 4857.26773148148,
		acceleration: -0.761944444444444
	},
	{
		id: 674,
		time: 673,
		velocity: 11.3927777777778,
		power: -7257.447896535,
		road: 4869.49245370371,
		acceleration: -0.834074074074074
	},
	{
		id: 675,
		time: 674,
		velocity: 11.1733333333333,
		power: -2268.26484051313,
		road: 4881.09597222222,
		acceleration: -0.408333333333333
	},
	{
		id: 676,
		time: 675,
		velocity: 11.6319444444444,
		power: 1460.00269976133,
		road: 4892.4625462963,
		acceleration: -0.0655555555555587
	},
	{
		id: 677,
		time: 676,
		velocity: 11.1961111111111,
		power: 2893.30930467638,
		road: 4903.82967592593,
		acceleration: 0.0666666666666682
	},
	{
		id: 678,
		time: 677,
		velocity: 11.3733333333333,
		power: 210.808037669775,
		road: 4915.14013888889,
		acceleration: -0.18
	},
	{
		id: 679,
		time: 678,
		velocity: 11.0919444444444,
		power: 3253.07077327141,
		road: 4926.41240740741,
		acceleration: 0.103611111111112
	},
	{
		id: 680,
		time: 679,
		velocity: 11.5069444444444,
		power: 2309.84131634732,
		road: 4937.74347222222,
		acceleration: 0.0139814814814816
	},
	{
		id: 681,
		time: 680,
		velocity: 11.4152777777778,
		power: 3462.63416367225,
		road: 4949.14050925926,
		acceleration: 0.117962962962963
	},
	{
		id: 682,
		time: 681,
		velocity: 11.4458333333333,
		power: 4588.62716666814,
		road: 4960.70319444445,
		acceleration: 0.213333333333333
	},
	{
		id: 683,
		time: 682,
		velocity: 12.1469444444444,
		power: 7040.54783049413,
		road: 4972.57981481482,
		acceleration: 0.414537037037036
	},
	{
		id: 684,
		time: 683,
		velocity: 12.6588888888889,
		power: 10006.7390148681,
		road: 4984.97953703704,
		acceleration: 0.631666666666666
	},
	{
		id: 685,
		time: 684,
		velocity: 13.3408333333333,
		power: 6728.59867946962,
		road: 4997.85833333334,
		acceleration: 0.326481481481485
	},
	{
		id: 686,
		time: 685,
		velocity: 13.1263888888889,
		power: 3051.90419353634,
		road: 5010.91111111111,
		acceleration: 0.0214814814814801
	},
	{
		id: 687,
		time: 686,
		velocity: 12.7233333333333,
		power: -122.311888549662,
		road: 5023.85870370371,
		acceleration: -0.23185185185185
	},
	{
		id: 688,
		time: 687,
		velocity: 12.6452777777778,
		power: 1986.98905345057,
		road: 5036.66175925926,
		acceleration: -0.0572222222222223
	},
	{
		id: 689,
		time: 688,
		velocity: 12.9547222222222,
		power: 2771.40745945447,
		road: 5049.44009259259,
		acceleration: 0.00777777777777544
	},
	{
		id: 690,
		time: 689,
		velocity: 12.7466666666667,
		power: 4470.01822013345,
		road: 5062.29421296297,
		acceleration: 0.143796296296296
	},
	{
		id: 691,
		time: 690,
		velocity: 13.0766666666667,
		power: 5479.56820605051,
		road: 5075.32884259259,
		acceleration: 0.217222222222224
	},
	{
		id: 692,
		time: 691,
		velocity: 13.6063888888889,
		power: 7457.86951126695,
		road: 5088.65152777778,
		acceleration: 0.358888888888886
	},
	{
		id: 693,
		time: 692,
		velocity: 13.8233333333333,
		power: 7469.12402385606,
		road: 5102.32333333334,
		acceleration: 0.339351851851852
	},
	{
		id: 694,
		time: 693,
		velocity: 14.0947222222222,
		power: 5686.94192972873,
		road: 5116.26,
		acceleration: 0.190370370370372
	},
	{
		id: 695,
		time: 694,
		velocity: 14.1775,
		power: 4477.02330361804,
		road: 5130.33875,
		acceleration: 0.093796296296297
	},
	{
		id: 696,
		time: 695,
		velocity: 14.1047222222222,
		power: 4682.03597297454,
		road: 5144.51689814815,
		acceleration: 0.104999999999999
	},
	{
		id: 697,
		time: 696,
		velocity: 14.4097222222222,
		power: 4818.60337489916,
		road: 5158.80287037037,
		acceleration: 0.110648148148149
	},
	{
		id: 698,
		time: 697,
		velocity: 14.5094444444444,
		power: 5578.54730103068,
		road: 5173.22430555556,
		acceleration: 0.160277777777775
	},
	{
		id: 699,
		time: 698,
		velocity: 14.5855555555556,
		power: 4559.75586540749,
		road: 5187.76666666667,
		acceleration: 0.0815740740740765
	},
	{
		id: 700,
		time: 699,
		velocity: 14.6544444444444,
		power: 4973.90604890996,
		road: 5202.40356481482,
		acceleration: 0.1075
	},
	{
		id: 701,
		time: 700,
		velocity: 14.8319444444444,
		power: 5140.99496910006,
		road: 5217.15162037037,
		acceleration: 0.114814814814814
	},
	{
		id: 702,
		time: 701,
		velocity: 14.93,
		power: 4922.84109673212,
		road: 5232.00462962963,
		acceleration: 0.095092592592593
	},
	{
		id: 703,
		time: 702,
		velocity: 14.9397222222222,
		power: 4412.85601514447,
		road: 5246.93333333334,
		acceleration: 0.0562962962962956
	},
	{
		id: 704,
		time: 703,
		velocity: 15.0008333333333,
		power: 3778.28991702972,
		road: 5261.89550925926,
		acceleration: 0.0106481481481495
	},
	{
		id: 705,
		time: 704,
		velocity: 14.9619444444444,
		power: 3655.58179114032,
		road: 5276.86393518519,
		acceleration: 0.00185185185185155
	},
	{
		id: 706,
		time: 705,
		velocity: 14.9452777777778,
		power: -1180.49828347538,
		road: 5291.66606481482,
		acceleration: -0.334444444444447
	},
	{
		id: 707,
		time: 706,
		velocity: 13.9975,
		power: 2169.48910809948,
		road: 5306.25509259259,
		acceleration: -0.0917592592592591
	},
	{
		id: 708,
		time: 707,
		velocity: 14.6866666666667,
		power: 2327.33347673659,
		road: 5320.75921296296,
		acceleration: -0.0780555555555544
	},
	{
		id: 709,
		time: 708,
		velocity: 14.7111111111111,
		power: 5589.15305336715,
		road: 5335.30212962963,
		acceleration: 0.155648148148149
	},
	{
		id: 710,
		time: 709,
		velocity: 14.4644444444444,
		power: 2594.4688328699,
		road: 5349.89222222222,
		acceleration: -0.0612962962962964
	},
	{
		id: 711,
		time: 710,
		velocity: 14.5027777777778,
		power: 1861.48456825438,
		road: 5364.39583333333,
		acceleration: -0.111666666666666
	},
	{
		id: 712,
		time: 711,
		velocity: 14.3761111111111,
		power: 1980.65731118391,
		road: 5378.79347222222,
		acceleration: -0.10027777777778
	},
	{
		id: 713,
		time: 712,
		velocity: 14.1636111111111,
		power: 987.543117189215,
		road: 5393.05615740741,
		acceleration: -0.169629629629629
	},
	{
		id: 714,
		time: 713,
		velocity: 13.9938888888889,
		power: 2174.69165402732,
		road: 5407.19449074074,
		acceleration: -0.0790740740740752
	},
	{
		id: 715,
		time: 714,
		velocity: 14.1388888888889,
		power: 2813.35061179571,
		road: 5421.27824074074,
		acceleration: -0.0300925925925899
	},
	{
		id: 716,
		time: 715,
		velocity: 14.0733333333333,
		power: 3237.98411715742,
		road: 5435.34791666667,
		acceleration: 0.00194444444444208
	},
	{
		id: 717,
		time: 716,
		velocity: 13.9997222222222,
		power: 2559.53800003046,
		road: 5449.39462962963,
		acceleration: -0.0478703703703705
	},
	{
		id: 718,
		time: 717,
		velocity: 13.9952777777778,
		power: 2304.64282757524,
		road: 5463.38476851852,
		acceleration: -0.0652777777777747
	},
	{
		id: 719,
		time: 718,
		velocity: 13.8775,
		power: 1362.27274935686,
		road: 5477.27550925926,
		acceleration: -0.133518518518519
	},
	{
		id: 720,
		time: 719,
		velocity: 13.5991666666667,
		power: -532.331183821723,
		road: 5490.96259259259,
		acceleration: -0.273796296296297
	},
	{
		id: 721,
		time: 720,
		velocity: 13.1738888888889,
		power: -476.158358232694,
		road: 5504.37972222222,
		acceleration: -0.26611111111111
	},
	{
		id: 722,
		time: 721,
		velocity: 13.0791666666667,
		power: -466.409922957281,
		road: 5517.53273148148,
		acceleration: -0.262129629629632
	},
	{
		id: 723,
		time: 722,
		velocity: 12.8127777777778,
		power: 506.871559947606,
		road: 5530.46435185185,
		acceleration: -0.180648148148146
	},
	{
		id: 724,
		time: 723,
		velocity: 12.6319444444444,
		power: 3570.22351875655,
		road: 5543.34046296296,
		acceleration: 0.0696296296296275
	},
	{
		id: 725,
		time: 724,
		velocity: 13.2880555555556,
		power: 5107.7782105363,
		road: 5556.34574074074,
		acceleration: 0.188703703703705
	},
	{
		id: 726,
		time: 725,
		velocity: 13.3788888888889,
		power: 8189.32688497662,
		road: 5569.65402777778,
		acceleration: 0.417314814814812
	},
	{
		id: 727,
		time: 726,
		velocity: 13.8838888888889,
		power: 5584.06845393578,
		road: 5583.26976851852,
		acceleration: 0.197592592592592
	},
	{
		id: 728,
		time: 727,
		velocity: 13.8808333333333,
		power: 5189.20016022263,
		road: 5597.06393518519,
		acceleration: 0.159259259259262
	},
	{
		id: 729,
		time: 728,
		velocity: 13.8566666666667,
		power: 3169.55890884052,
		road: 5610.93935185185,
		acceleration: 0.00324074074073977
	},
	{
		id: 730,
		time: 729,
		velocity: 13.8936111111111,
		power: 3118.5947788477,
		road: 5624.81606481482,
		acceleration: -0.000648148148147953
	},
	{
		id: 731,
		time: 730,
		velocity: 13.8788888888889,
		power: 3759.1063931967,
		road: 5638.71587962963,
		acceleration: 0.0468518518518515
	},
	{
		id: 732,
		time: 731,
		velocity: 13.9972222222222,
		power: 3235.05621364372,
		road: 5652.64236111111,
		acceleration: 0.00648148148148309
	},
	{
		id: 733,
		time: 732,
		velocity: 13.9130555555556,
		power: 5047.64514246553,
		road: 5666.64189814815,
		acceleration: 0.139629629629628
	},
	{
		id: 734,
		time: 733,
		velocity: 14.2977777777778,
		power: 5059.3371469361,
		road: 5680.77856481482,
		acceleration: 0.134629629629632
	},
	{
		id: 735,
		time: 734,
		velocity: 14.4011111111111,
		power: 6525.32972011284,
		road: 5695.09953703704,
		acceleration: 0.233981481481479
	},
	{
		id: 736,
		time: 735,
		velocity: 14.615,
		power: 4349.07065461651,
		road: 5709.57203703704,
		acceleration: 0.0690740740740754
	},
	{
		id: 737,
		time: 736,
		velocity: 14.505,
		power: 3024.33894179712,
		road: 5724.06537037037,
		acceleration: -0.0274074074074093
	},
	{
		id: 738,
		time: 737,
		velocity: 14.3188888888889,
		power: -276.863009011088,
		road: 5738.41314814815,
		acceleration: -0.263703703703703
	},
	{
		id: 739,
		time: 738,
		velocity: 13.8238888888889,
		power: -1451.38773483434,
		road: 5752.45564814815,
		acceleration: -0.346851851851852
	},
	{
		id: 740,
		time: 739,
		velocity: 13.4644444444444,
		power: -2819.98387228835,
		road: 5766.10027777778,
		acceleration: -0.448888888888888
	},
	{
		id: 741,
		time: 740,
		velocity: 12.9722222222222,
		power: -1666.49330835851,
		road: 5779.34138888889,
		acceleration: -0.358148148148148
	},
	{
		id: 742,
		time: 741,
		velocity: 12.7494444444444,
		power: 470.607874145097,
		road: 5792.3112962963,
		acceleration: -0.184259259259258
	},
	{
		id: 743,
		time: 742,
		velocity: 12.9116666666667,
		power: 2990.60774711206,
		road: 5805.20009259259,
		acceleration: 0.0220370370370375
	},
	{
		id: 744,
		time: 743,
		velocity: 13.0383333333333,
		power: 5080.21107869179,
		road: 5818.19342592593,
		acceleration: 0.187037037037037
	},
	{
		id: 745,
		time: 744,
		velocity: 13.3105555555556,
		power: 7344.13089949468,
		road: 5831.45699074074,
		acceleration: 0.353425925925926
	},
	{
		id: 746,
		time: 745,
		velocity: 13.9719444444444,
		power: 8431.93992663627,
		road: 5845.10458333333,
		acceleration: 0.414629629629626
	},
	{
		id: 747,
		time: 746,
		velocity: 14.2822222222222,
		power: 8139.43967729764,
		road: 5859.14375,
		acceleration: 0.36851851851852
	},
	{
		id: 748,
		time: 747,
		velocity: 14.4161111111111,
		power: 6082.82148948006,
		road: 5873.46791666667,
		acceleration: 0.201481481481483
	},
	{
		id: 749,
		time: 748,
		velocity: 14.5763888888889,
		power: 5166.25607275148,
		road: 5887.95657407407,
		acceleration: 0.1275
	},
	{
		id: 750,
		time: 749,
		velocity: 14.6647222222222,
		power: 4164.10287615546,
		road: 5902.53486111111,
		acceleration: 0.0517592592592582
	},
	{
		id: 751,
		time: 750,
		velocity: 14.5713888888889,
		power: 2623.02200645009,
		road: 5917.10962962963,
		acceleration: -0.0587962962962951
	},
	{
		id: 752,
		time: 751,
		velocity: 14.4,
		power: 93.8570774956258,
		road: 5931.53601851852,
		acceleration: -0.237962962962962
	},
	{
		id: 753,
		time: 752,
		velocity: 13.9508333333333,
		power: 187.069910877575,
		road: 5945.72981481482,
		acceleration: -0.227222222222224
	},
	{
		id: 754,
		time: 753,
		velocity: 13.8897222222222,
		power: -1384.16826903078,
		road: 5959.63962962963,
		acceleration: -0.34074074074074
	},
	{
		id: 755,
		time: 754,
		velocity: 13.3777777777778,
		power: 698.497089527158,
		road: 5973.28962962963,
		acceleration: -0.178888888888892
	},
	{
		id: 756,
		time: 755,
		velocity: 13.4141666666667,
		power: 473.883628961567,
		road: 5986.7537962963,
		acceleration: -0.192777777777778
	},
	{
		id: 757,
		time: 756,
		velocity: 13.3113888888889,
		power: 2860.24879407815,
		road: 6000.11958333333,
		acceleration: -0.00398148148148003
	},
	{
		id: 758,
		time: 757,
		velocity: 13.3658333333333,
		power: 3928.6786125088,
		road: 6013.5225462963,
		acceleration: 0.0783333333333331
	},
	{
		id: 759,
		time: 758,
		velocity: 13.6491666666667,
		power: 4637.56738433619,
		road: 6027.02935185185,
		acceleration: 0.129351851851855
	},
	{
		id: 760,
		time: 759,
		velocity: 13.6994444444444,
		power: 6154.00586494386,
		road: 6040.71967592593,
		acceleration: 0.237685185185182
	},
	{
		id: 761,
		time: 760,
		velocity: 14.0788888888889,
		power: 6463.7225986293,
		road: 6054.65328703704,
		acceleration: 0.24888888888889
	},
	{
		id: 762,
		time: 761,
		velocity: 14.3958333333333,
		power: 7397.68627992041,
		road: 6068.86324074074,
		acceleration: 0.303796296296298
	},
	{
		id: 763,
		time: 762,
		velocity: 14.6108333333333,
		power: 9127.63417838722,
		road: 6083.42953703704,
		acceleration: 0.408888888888889
	},
	{
		id: 764,
		time: 763,
		velocity: 15.3055555555556,
		power: 9741.69613112352,
		road: 6098.41347222222,
		acceleration: 0.426388888888887
	},
	{
		id: 765,
		time: 764,
		velocity: 15.675,
		power: 11514.0065613735,
		road: 6113.86925925926,
		acceleration: 0.517314814814815
	},
	{
		id: 766,
		time: 765,
		velocity: 16.1627777777778,
		power: 11038.7474907194,
		road: 6129.81050925926,
		acceleration: 0.45361111111111
	},
	{
		id: 767,
		time: 766,
		velocity: 16.6663888888889,
		power: 10317.8910446414,
		road: 6146.16921296296,
		acceleration: 0.381296296296298
	},
	{
		id: 768,
		time: 767,
		velocity: 16.8188888888889,
		power: 9243.27434209984,
		road: 6162.86569444445,
		acceleration: 0.29425925925926
	},
	{
		id: 769,
		time: 768,
		velocity: 17.0455555555556,
		power: 6326.32805301742,
		road: 6179.76087962963,
		acceleration: 0.103148148148151
	},
	{
		id: 770,
		time: 769,
		velocity: 16.9758333333333,
		power: 6544.96329166771,
		road: 6196.76365740741,
		acceleration: 0.112037037037034
	},
	{
		id: 771,
		time: 770,
		velocity: 17.155,
		power: 4474.48723376693,
		road: 6213.81398148148,
		acceleration: -0.0169444444444444
	},
	{
		id: 772,
		time: 771,
		velocity: 16.9947222222222,
		power: 2655.70249963757,
		road: 6230.79263888889,
		acceleration: -0.12638888888889
	},
	{
		id: 773,
		time: 772,
		velocity: 16.5966666666667,
		power: 1006.05170917499,
		road: 6247.59611111111,
		acceleration: -0.223981481481484
	},
	{
		id: 774,
		time: 773,
		velocity: 16.4830555555556,
		power: 1800.0639032275,
		road: 6264.20282407408,
		acceleration: -0.169537037037035
	},
	{
		id: 775,
		time: 774,
		velocity: 16.4861111111111,
		power: -299.655474989999,
		road: 6280.57592592593,
		acceleration: -0.297685185185184
	},
	{
		id: 776,
		time: 775,
		velocity: 15.7036111111111,
		power: -287.323148004521,
		road: 6296.65425925926,
		acceleration: -0.291851851851854
	},
	{
		id: 777,
		time: 776,
		velocity: 15.6075,
		power: -1119.05923409757,
		road: 6312.41578703704,
		acceleration: -0.341759259259257
	},
	{
		id: 778,
		time: 777,
		velocity: 15.4608333333333,
		power: 1911.52535484062,
		road: 6327.93921296297,
		acceleration: -0.134444444444446
	},
	{
		id: 779,
		time: 778,
		velocity: 15.3002777777778,
		power: 1626.05650234371,
		road: 6343.32032407408,
		acceleration: -0.150185185185183
	},
	{
		id: 780,
		time: 779,
		velocity: 15.1569444444444,
		power: 1644.65740555136,
		road: 6358.55370370371,
		acceleration: -0.145277777777778
	},
	{
		id: 781,
		time: 780,
		velocity: 15.025,
		power: 2847.86125682239,
		road: 6373.68467592593,
		acceleration: -0.0595370370370372
	},
	{
		id: 782,
		time: 781,
		velocity: 15.1216666666667,
		power: 3338.27124635722,
		road: 6388.77375,
		acceleration: -0.0242592592592601
	},
	{
		id: 783,
		time: 782,
		velocity: 15.0841666666667,
		power: 5611.60161891238,
		road: 6403.91625,
		acceleration: 0.131111111111112
	},
	{
		id: 784,
		time: 783,
		velocity: 15.4183333333333,
		power: 6521.88628907747,
		road: 6419.21759259259,
		acceleration: 0.186574074074073
	},
	{
		id: 785,
		time: 784,
		velocity: 15.6813888888889,
		power: 8028.71395389783,
		road: 6434.75087962963,
		acceleration: 0.277314814814815
	},
	{
		id: 786,
		time: 785,
		velocity: 15.9161111111111,
		power: 7491.50419117217,
		road: 6450.53708333334,
		acceleration: 0.228518518518518
	},
	{
		id: 787,
		time: 786,
		velocity: 16.1038888888889,
		power: 6816.51320047599,
		road: 6466.52476851852,
		acceleration: 0.174444444444447
	},
	{
		id: 788,
		time: 787,
		velocity: 16.2047222222222,
		power: 7123.43125409317,
		road: 6482.69268518519,
		acceleration: 0.186018518518519
	},
	{
		id: 789,
		time: 788,
		velocity: 16.4741666666667,
		power: 6795.90676192552,
		road: 6499.03212962963,
		acceleration: 0.157037037037036
	},
	{
		id: 790,
		time: 789,
		velocity: 16.575,
		power: 7381.6215018794,
		road: 6515.54328703704,
		acceleration: 0.186388888888885
	},
	{
		id: 791,
		time: 790,
		velocity: 16.7638888888889,
		power: 6365.83183737668,
		road: 6532.20541666667,
		acceleration: 0.115555555555559
	},
	{
		id: 792,
		time: 791,
		velocity: 16.8208333333333,
		power: 7519.0424096866,
		road: 6549.01578703704,
		acceleration: 0.180925925925926
	},
	{
		id: 793,
		time: 792,
		velocity: 17.1177777777778,
		power: 5731.52223843035,
		road: 6565.94902777778,
		acceleration: 0.0648148148148131
	},
	{
		id: 794,
		time: 793,
		velocity: 16.9583333333333,
		power: 5282.7421906642,
		road: 6582.93226851852,
		acceleration: 0.0351851851851848
	},
	{
		id: 795,
		time: 794,
		velocity: 16.9263888888889,
		power: 1855.50421201439,
		road: 6599.84611111111,
		acceleration: -0.17398148148148
	},
	{
		id: 796,
		time: 795,
		velocity: 16.5958333333333,
		power: 944.041270270895,
		road: 6616.5600462963,
		acceleration: -0.225833333333334
	},
	{
		id: 797,
		time: 796,
		velocity: 16.2808333333333,
		power: 191.022972501993,
		road: 6633.02699074074,
		acceleration: -0.26814814814815
	},
	{
		id: 798,
		time: 797,
		velocity: 16.1219444444444,
		power: 1598.09836001528,
		road: 6649.27319444445,
		acceleration: -0.173333333333332
	},
	{
		id: 799,
		time: 798,
		velocity: 16.0758333333333,
		power: 4238.77156235647,
		road: 6665.4325925926,
		acceleration: -0.000277777777778709
	},
	{
		id: 800,
		time: 799,
		velocity: 16.28,
		power: 6026.90346100433,
		road: 6681.64837962963,
		acceleration: 0.113055555555558
	},
	{
		id: 801,
		time: 800,
		velocity: 16.4611111111111,
		power: 6502.10642118995,
		road: 6697.98976851852,
		acceleration: 0.138148148148147
	},
	{
		id: 802,
		time: 801,
		velocity: 16.4902777777778,
		power: 4522.37334771816,
		road: 6714.40462962963,
		acceleration: 0.00879629629629619
	},
	{
		id: 803,
		time: 802,
		velocity: 16.3063888888889,
		power: 2847.95472086359,
		road: 6730.77560185186,
		acceleration: -0.096574074074077
	},
	{
		id: 804,
		time: 803,
		velocity: 16.1713888888889,
		power: 2389.94576839658,
		road: 6747.03689814815,
		acceleration: -0.122777777777774
	},
	{
		id: 805,
		time: 804,
		velocity: 16.1219444444444,
		power: 3443.34416483253,
		road: 6763.21069444445,
		acceleration: -0.0522222222222233
	},
	{
		id: 806,
		time: 805,
		velocity: 16.1497222222222,
		power: 3954.59336763752,
		road: 6779.34939814815,
		acceleration: -0.0179629629629616
	},
	{
		id: 807,
		time: 806,
		velocity: 16.1175,
		power: 3836.28141460576,
		road: 6795.46666666667,
		acceleration: -0.024907407407408
	},
	{
		id: 808,
		time: 807,
		velocity: 16.0472222222222,
		power: 2309.81628709809,
		road: 6811.51050925926,
		acceleration: -0.121944444444445
	},
	{
		id: 809,
		time: 808,
		velocity: 15.7838888888889,
		power: 2140.14983531963,
		road: 6827.42856481482,
		acceleration: -0.129629629629632
	},
	{
		id: 810,
		time: 809,
		velocity: 15.7286111111111,
		power: 598.410705397899,
		road: 6843.16814814815,
		acceleration: -0.227314814814813
	},
	{
		id: 811,
		time: 810,
		velocity: 15.3652777777778,
		power: 1331.68725360976,
		road: 6858.70712962963,
		acceleration: -0.173888888888891
	},
	{
		id: 812,
		time: 811,
		velocity: 15.2622222222222,
		power: 1277.7134247203,
		road: 6874.07240740741,
		acceleration: -0.173518518518517
	},
	{
		id: 813,
		time: 812,
		velocity: 15.2080555555556,
		power: 2768.13350088936,
		road: 6889.31671296297,
		acceleration: -0.0684259259259257
	},
	{
		id: 814,
		time: 813,
		velocity: 15.16,
		power: 3187.37247732848,
		road: 6904.50782407408,
		acceleration: -0.037962962962963
	},
	{
		id: 815,
		time: 814,
		velocity: 15.1483333333333,
		power: 5321.33401823554,
		road: 6919.73375,
		acceleration: 0.107592592592594
	},
	{
		id: 816,
		time: 815,
		velocity: 15.5308333333333,
		power: 4500.3267878759,
		road: 6935.0375925926,
		acceleration: 0.0482407407407397
	},
	{
		id: 817,
		time: 816,
		velocity: 15.3047222222222,
		power: 4871.7479746231,
		road: 6950.40120370371,
		acceleration: 0.0712962962962962
	},
	{
		id: 818,
		time: 817,
		velocity: 15.3622222222222,
		power: 2564.28375668582,
		road: 6965.75763888889,
		acceleration: -0.0856481481481506
	},
	{
		id: 819,
		time: 818,
		velocity: 15.2738888888889,
		power: 3315.02206969676,
		road: 6981.05495370371,
		acceleration: -0.032592592592593
	},
	{
		id: 820,
		time: 819,
		velocity: 15.2069444444444,
		power: 2100.7001004163,
		road: 6996.27912037038,
		acceleration: -0.113703703703701
	},
	{
		id: 821,
		time: 820,
		velocity: 15.0211111111111,
		power: 3309.38106962673,
		road: 7011.43226851852,
		acceleration: -0.0283333333333342
	},
	{
		id: 822,
		time: 821,
		velocity: 15.1888888888889,
		power: 1364.27185011955,
		road: 7026.49101851852,
		acceleration: -0.160462962962963
	},
	{
		id: 823,
		time: 822,
		velocity: 14.7255555555556,
		power: 607.011134660352,
		road: 7041.36481481482,
		acceleration: -0.209444444444443
	},
	{
		id: 824,
		time: 823,
		velocity: 14.3927777777778,
		power: 307.439846378257,
		road: 7056.02060185186,
		acceleration: -0.226574074074076
	},
	{
		id: 825,
		time: 824,
		velocity: 14.5091666666667,
		power: 4309.98031608852,
		road: 7070.59430555556,
		acceleration: 0.0624074074074059
	},
	{
		id: 826,
		time: 825,
		velocity: 14.9127777777778,
		power: 6851.21226203335,
		road: 7085.31791666667,
		acceleration: 0.237407407407408
	},
	{
		id: 827,
		time: 826,
		velocity: 15.105,
		power: 7634.85842893638,
		road: 7100.29990740741,
		acceleration: 0.279351851851851
	},
	{
		id: 828,
		time: 827,
		velocity: 15.3472222222222,
		power: 5500.80765475305,
		road: 7115.48245370371,
		acceleration: 0.12175925925926
	},
	{
		id: 829,
		time: 828,
		velocity: 15.2780555555556,
		power: 4000.61976080908,
		road: 7130.7338425926,
		acceleration: 0.0159259259259272
	},
	{
		id: 830,
		time: 829,
		velocity: 15.1527777777778,
		power: 1813.0368973377,
		road: 7145.92685185186,
		acceleration: -0.132685185185187
	},
	{
		id: 831,
		time: 830,
		velocity: 14.9491666666667,
		power: 4094.86553153881,
		road: 7161.06671296297,
		acceleration: 0.0263888888888903
	},
	{
		id: 832,
		time: 831,
		velocity: 15.3572222222222,
		power: 4784.45165537912,
		road: 7176.25583333334,
		acceleration: 0.0721296296296323
	},
	{
		id: 833,
		time: 832,
		velocity: 15.3691666666667,
		power: 4473.22275874312,
		road: 7191.50523148149,
		acceleration: 0.0484259259259261
	},
	{
		id: 834,
		time: 833,
		velocity: 15.0944444444444,
		power: 2804.32849549564,
		road: 7206.74592592593,
		acceleration: -0.0658333333333356
	},
	{
		id: 835,
		time: 834,
		velocity: 15.1597222222222,
		power: 2489.77436410241,
		road: 7221.91106481482,
		acceleration: -0.085277777777776
	},
	{
		id: 836,
		time: 835,
		velocity: 15.1133333333333,
		power: 3395.81260375765,
		road: 7237.02305555556,
		acceleration: -0.0210185185185221
	},
	{
		id: 837,
		time: 836,
		velocity: 15.0313888888889,
		power: 2841.04305333485,
		road: 7252.09541666667,
		acceleration: -0.0582407407407395
	},
	{
		id: 838,
		time: 837,
		velocity: 14.985,
		power: 1739.83568831009,
		road: 7267.0725,
		acceleration: -0.132314814814817
	},
	{
		id: 839,
		time: 838,
		velocity: 14.7163888888889,
		power: 1803.18529397533,
		road: 7281.92111111112,
		acceleration: -0.124629629629629
	},
	{
		id: 840,
		time: 839,
		velocity: 14.6575,
		power: 2486.65340805218,
		road: 7296.67060185186,
		acceleration: -0.0736111111111093
	},
	{
		id: 841,
		time: 840,
		velocity: 14.7641666666667,
		power: 4060.3714773107,
		road: 7311.40263888889,
		acceleration: 0.038703703703705
	},
	{
		id: 842,
		time: 841,
		velocity: 14.8325,
		power: 6355.417713296,
		road: 7326.25212962963,
		acceleration: 0.196203703703704
	},
	{
		id: 843,
		time: 842,
		velocity: 15.2461111111111,
		power: 5671.52678881167,
		road: 7341.2700462963,
		acceleration: 0.140648148148145
	},
	{
		id: 844,
		time: 843,
		velocity: 15.1861111111111,
		power: 4974.45130057054,
		road: 7356.40203703704,
		acceleration: 0.0875000000000021
	},
	{
		id: 845,
		time: 844,
		velocity: 15.095,
		power: 3830.56591687577,
		road: 7371.58115740741,
		acceleration: 0.00675925925925824
	},
	{
		id: 846,
		time: 845,
		velocity: 15.2663888888889,
		power: 3605.79696539651,
		road: 7386.75930555556,
		acceleration: -0.00870370370370388
	},
	{
		id: 847,
		time: 846,
		velocity: 15.16,
		power: 4360.29742608988,
		road: 7401.95444444445,
		acceleration: 0.042685185185185
	},
	{
		id: 848,
		time: 847,
		velocity: 15.2230555555556,
		power: 4398.41429862691,
		road: 7417.19277777778,
		acceleration: 0.043703703703704
	},
	{
		id: 849,
		time: 848,
		velocity: 15.3975,
		power: 5751.09625299073,
		road: 7432.51935185186,
		acceleration: 0.132777777777777
	},
	{
		id: 850,
		time: 849,
		velocity: 15.5583333333333,
		power: 5790.65123084451,
		road: 7447.97722222223,
		acceleration: 0.129814814814814
	},
	{
		id: 851,
		time: 850,
		velocity: 15.6125,
		power: 4521.65990831565,
		road: 7463.52037037038,
		acceleration: 0.040740740740743
	},
	{
		id: 852,
		time: 851,
		velocity: 15.5197222222222,
		power: 4547.06426734694,
		road: 7479.10435185186,
		acceleration: 0.0409259259259258
	},
	{
		id: 853,
		time: 852,
		velocity: 15.6811111111111,
		power: 3619.47999337186,
		road: 7494.69796296297,
		acceleration: -0.0216666666666665
	},
	{
		id: 854,
		time: 853,
		velocity: 15.5475,
		power: 6280.25767150279,
		road: 7510.35768518519,
		acceleration: 0.153888888888886
	},
	{
		id: 855,
		time: 854,
		velocity: 15.9813888888889,
		power: 5305.66729189402,
		road: 7526.1363425926,
		acceleration: 0.0839814814814819
	},
	{
		id: 856,
		time: 855,
		velocity: 15.9330555555556,
		power: 5503.00451750875,
		road: 7542.00370370371,
		acceleration: 0.093425925925926
	},
	{
		id: 857,
		time: 856,
		velocity: 15.8277777777778,
		power: 3851.3642664629,
		road: 7557.90939814815,
		acceleration: -0.0167592592592598
	},
	{
		id: 858,
		time: 857,
		velocity: 15.9311111111111,
		power: 2770.90868696701,
		road: 7573.76356481482,
		acceleration: -0.086296296296295
	},
	{
		id: 859,
		time: 858,
		velocity: 15.6741666666667,
		power: 5150.62496733284,
		road: 7589.6100925926,
		acceleration: 0.0710185185185175
	},
	{
		id: 860,
		time: 859,
		velocity: 16.0408333333333,
		power: 6825.22014012603,
		road: 7605.5800462963,
		acceleration: 0.175833333333335
	},
	{
		id: 861,
		time: 860,
		velocity: 16.4586111111111,
		power: 10074.1685976838,
		road: 7621.82412037038,
		acceleration: 0.372407407407408
	},
	{
		id: 862,
		time: 861,
		velocity: 16.7913888888889,
		power: 8179.77320252941,
		road: 7638.37194444445,
		acceleration: 0.235092592592594
	},
	{
		id: 863,
		time: 862,
		velocity: 16.7461111111111,
		power: 7048.41096187298,
		road: 7655.11467592593,
		acceleration: 0.154722222222219
	},
	{
		id: 864,
		time: 863,
		velocity: 16.9227777777778,
		power: 5522.41625095268,
		road: 7671.96240740741,
		acceleration: 0.0552777777777784
	},
	{
		id: 865,
		time: 864,
		velocity: 16.9572222222222,
		power: 5049.75567568446,
		road: 7688.85000000001,
		acceleration: 0.0244444444444447
	},
	{
		id: 866,
		time: 865,
		velocity: 16.8194444444444,
		power: 3715.79777306617,
		road: 7705.72101851852,
		acceleration: -0.0575925925925915
	},
	{
		id: 867,
		time: 866,
		velocity: 16.75,
		power: 2911.69096243385,
		road: 7722.51074074075,
		acceleration: -0.105
	},
	{
		id: 868,
		time: 867,
		velocity: 16.6422222222222,
		power: 2968.22507231127,
		road: 7739.19875000001,
		acceleration: -0.0984259259259268
	},
	{
		id: 869,
		time: 868,
		velocity: 16.5241666666667,
		power: 1646.64669454466,
		road: 7755.74865740741,
		acceleration: -0.177777777777777
	},
	{
		id: 870,
		time: 869,
		velocity: 16.2166666666667,
		power: -8.50487750811769,
		road: 7772.07060185186,
		acceleration: -0.278148148148148
	},
	{
		id: 871,
		time: 870,
		velocity: 15.8077777777778,
		power: 727.630542283503,
		road: 7788.14064814815,
		acceleration: -0.225648148148148
	},
	{
		id: 872,
		time: 871,
		velocity: 15.8472222222222,
		power: 952.2107878165,
		road: 7803.99472222223,
		acceleration: -0.206296296296296
	},
	{
		id: 873,
		time: 872,
		velocity: 15.5977777777778,
		power: 2103.07181793612,
		road: 7819.68268518519,
		acceleration: -0.125925925925927
	},
	{
		id: 874,
		time: 873,
		velocity: 15.43,
		power: 692.355036074819,
		road: 7835.19944444445,
		acceleration: -0.21648148148148
	},
	{
		id: 875,
		time: 874,
		velocity: 15.1977777777778,
		power: 873.940075649013,
		road: 7850.50805555556,
		acceleration: -0.199814814814815
	},
	{
		id: 876,
		time: 875,
		velocity: 14.9983333333333,
		power: 1142.81100238474,
		road: 7865.62814814815,
		acceleration: -0.177222222222222
	},
	{
		id: 877,
		time: 876,
		velocity: 14.8983333333333,
		power: 1403.42798128297,
		road: 7880.58199074075,
		acceleration: -0.15527777777778
	},
	{
		id: 878,
		time: 877,
		velocity: 14.7319444444444,
		power: 1928.77709075523,
		road: 7895.40069444445,
		acceleration: -0.114999999999998
	},
	{
		id: 879,
		time: 878,
		velocity: 14.6533333333333,
		power: -478.700692004563,
		road: 7910.02078703704,
		acceleration: -0.282222222222224
	},
	{
		id: 880,
		time: 879,
		velocity: 14.0516666666667,
		power: -2266.9955569259,
		road: 7924.29546296297,
		acceleration: -0.408611111111112
	},
	{
		id: 881,
		time: 880,
		velocity: 13.5061111111111,
		power: -2494.76829658513,
		road: 7938.15370370371,
		acceleration: -0.424259259259259
	},
	{
		id: 882,
		time: 881,
		velocity: 13.3805555555556,
		power: -534.102817067766,
		road: 7951.66393518519,
		acceleration: -0.271759259259261
	},
	{
		id: 883,
		time: 882,
		velocity: 13.2363888888889,
		power: 1592.29582923671,
		road: 7964.98712962964,
		acceleration: -0.102314814814813
	},
	{
		id: 884,
		time: 883,
		velocity: 13.1991666666667,
		power: 2384.64502113236,
		road: 7978.24018518519,
		acceleration: -0.0379629629629648
	},
	{
		id: 885,
		time: 884,
		velocity: 13.2666666666667,
		power: 2878.58606890112,
		road: 7991.4750925926,
		acceleration: 0.00166666666666693
	},
	{
		id: 886,
		time: 885,
		velocity: 13.2413888888889,
		power: 4576.98372932722,
		road: 8004.77745370371,
		acceleration: 0.133240740740742
	},
	{
		id: 887,
		time: 886,
		velocity: 13.5988888888889,
		power: 6487.39358309507,
		road: 8018.28287037038,
		acceleration: 0.272870370370372
	},
	{
		id: 888,
		time: 887,
		velocity: 14.0852777777778,
		power: 8892.05614668557,
		road: 8032.14277777778,
		acceleration: 0.436111111111108
	},
	{
		id: 889,
		time: 888,
		velocity: 14.5497222222222,
		power: 4618.92637558575,
		road: 8046.27189814815,
		acceleration: 0.102314814814815
	},
	{
		id: 890,
		time: 889,
		velocity: 13.9058333333333,
		power: 1184.37349166228,
		road: 8060.3763425926,
		acceleration: -0.151666666666666
	},
	{
		id: 891,
		time: 890,
		velocity: 13.6302777777778,
		power: 102.351086952292,
		road: 8074.29050925926,
		acceleration: -0.228888888888889
	},
	{
		id: 892,
		time: 891,
		velocity: 13.8630555555556,
		power: 746.823061960559,
		road: 8088.00203703704,
		acceleration: -0.176388888888889
	},
	{
		id: 893,
		time: 892,
		velocity: 13.3766666666667,
		power: 673.141214085246,
		road: 8101.53606481482,
		acceleration: -0.178611111111113
	},
	{
		id: 894,
		time: 893,
		velocity: 13.0944444444444,
		power: -2016.00015272814,
		road: 8114.78787037038,
		acceleration: -0.385833333333332
	},
	{
		id: 895,
		time: 894,
		velocity: 12.7055555555556,
		power: 2586.3337883804,
		road: 8127.8388425926,
		acceleration: -0.0158333333333331
	},
	{
		id: 896,
		time: 895,
		velocity: 13.3291666666667,
		power: 5792.72177102418,
		road: 8140.99990740741,
		acceleration: 0.23601851851852
	},
	{
		id: 897,
		time: 896,
		velocity: 13.8025,
		power: 8132.61649324712,
		road: 8154.48000000001,
		acceleration: 0.402037037037038
	},
	{
		id: 898,
		time: 897,
		velocity: 13.9116666666667,
		power: 7045.82179268639,
		road: 8168.31023148149,
		acceleration: 0.29824074074074
	},
	{
		id: 899,
		time: 898,
		velocity: 14.2238888888889,
		power: 6025.25532632927,
		road: 8182.39393518519,
		acceleration: 0.208703703703703
	},
	{
		id: 900,
		time: 899,
		velocity: 14.4286111111111,
		power: 5108.71125896815,
		road: 8196.64861111112,
		acceleration: 0.133240740740741
	},
	{
		id: 901,
		time: 900,
		velocity: 14.3113888888889,
		power: 3283.63805302283,
		road: 8210.96842592593,
		acceleration: -0.00296296296296283
	},
	{
		id: 902,
		time: 901,
		velocity: 14.215,
		power: 1902.25462163991,
		road: 8225.23546296297,
		acceleration: -0.102592592592591
	},
	{
		id: 903,
		time: 902,
		velocity: 14.1208333333333,
		power: 1620.25921269702,
		road: 8239.39092592593,
		acceleration: -0.120555555555555
	},
	{
		id: 904,
		time: 903,
		velocity: 13.9497222222222,
		power: 2008.22351348693,
		road: 8253.44157407408,
		acceleration: -0.0890740740740767
	},
	{
		id: 905,
		time: 904,
		velocity: 13.9477777777778,
		power: 2493.93100108619,
		road: 8267.42226851853,
		acceleration: -0.0508333333333315
	},
	{
		id: 906,
		time: 905,
		velocity: 13.9683333333333,
		power: 3214.138244108,
		road: 8281.37949074075,
		acceleration: 0.0038888888888895
	},
	{
		id: 907,
		time: 906,
		velocity: 13.9613888888889,
		power: 2648.23779599398,
		road: 8295.31962962964,
		acceleration: -0.0380555555555571
	},
	{
		id: 908,
		time: 907,
		velocity: 13.8336111111111,
		power: 2469.65221876349,
		road: 8309.21564814815,
		acceleration: -0.0501851851851853
	},
	{
		id: 909,
		time: 908,
		velocity: 13.8177777777778,
		power: 1985.13498628656,
		road: 8323.04412037038,
		acceleration: -0.0849074074074085
	},
	{
		id: 910,
		time: 909,
		velocity: 13.7066666666667,
		power: 652.836022548668,
		road: 8336.73851851852,
		acceleration: -0.183240740740738
	},
	{
		id: 911,
		time: 910,
		velocity: 13.2838888888889,
		power: -265.204542827237,
		road: 8350.21606481482,
		acceleration: -0.250462962962967
	},
	{
		id: 912,
		time: 911,
		velocity: 13.0663888888889,
		power: -2026.10885113904,
		road: 8363.37518518519,
		acceleration: -0.386388888888888
	},
	{
		id: 913,
		time: 912,
		velocity: 12.5475,
		power: -1190.35513270328,
		road: 8376.18245370371,
		acceleration: -0.317314814814814
	},
	{
		id: 914,
		time: 913,
		velocity: 12.3319444444444,
		power: -611.385968734331,
		road: 8388.6975925926,
		acceleration: -0.266944444444444
	},
	{
		id: 915,
		time: 914,
		velocity: 12.2655555555556,
		power: 2369.82902553171,
		road: 8401.07273148149,
		acceleration: -0.0130555555555532
	},
	{
		id: 916,
		time: 915,
		velocity: 12.5083333333333,
		power: 3510.34042420662,
		road: 8413.48245370371,
		acceleration: 0.0822222222222209
	},
	{
		id: 917,
		time: 916,
		velocity: 12.5786111111111,
		power: 4444.28370643984,
		road: 8426.01120370371,
		acceleration: 0.155833333333334
	},
	{
		id: 918,
		time: 917,
		velocity: 12.7330555555556,
		power: 3553.87050482766,
		road: 8438.6563425926,
		acceleration: 0.0769444444444432
	},
	{
		id: 919,
		time: 918,
		velocity: 12.7391666666667,
		power: 3955.95784604741,
		road: 8451.39324074075,
		acceleration: 0.106574074074075
	},
	{
		id: 920,
		time: 919,
		velocity: 12.8983333333333,
		power: 2859.20379285569,
		road: 8464.19060185186,
		acceleration: 0.0143518518518508
	},
	{
		id: 921,
		time: 920,
		velocity: 12.7761111111111,
		power: 5287.02044533969,
		road: 8477.09902777778,
		acceleration: 0.207777777777778
	},
	{
		id: 922,
		time: 921,
		velocity: 13.3625,
		power: 4657.6441517165,
		road: 8490.18578703704,
		acceleration: 0.148888888888889
	},
	{
		id: 923,
		time: 922,
		velocity: 13.345,
		power: 6973.07295703351,
		road: 8503.50740740741,
		acceleration: 0.320833333333333
	},
	{
		id: 924,
		time: 923,
		velocity: 13.7386111111111,
		power: 2516.52112567368,
		road: 8516.97250000001,
		acceleration: -0.0338888888888889
	},
	{
		id: 925,
		time: 924,
		velocity: 13.2608333333333,
		power: 4606.68471982993,
		road: 8530.48402777778,
		acceleration: 0.126759259259261
	},
	{
		id: 926,
		time: 925,
		velocity: 13.7252777777778,
		power: 336.609668636633,
		road: 8543.95712962964,
		acceleration: -0.203611111111114
	},
	{
		id: 927,
		time: 926,
		velocity: 13.1277777777778,
		power: 2246.86678746329,
		road: 8557.30268518519,
		acceleration: -0.0514814814814812
	},
	{
		id: 928,
		time: 927,
		velocity: 13.1063888888889,
		power: 591.826764757239,
		road: 8570.53287037038,
		acceleration: -0.179259259259258
	},
	{
		id: 929,
		time: 928,
		velocity: 13.1875,
		power: 2313.96803211922,
		road: 8583.65356481482,
		acceleration: -0.0397222222222222
	},
	{
		id: 930,
		time: 929,
		velocity: 13.0086111111111,
		power: 1396.69841733096,
		road: 8596.6987962963,
		acceleration: -0.111203703703703
	},
	{
		id: 931,
		time: 930,
		velocity: 12.7727777777778,
		power: 582.6166359744,
		road: 8609.60143518519,
		acceleration: -0.173981481481484
	},
	{
		id: 932,
		time: 931,
		velocity: 12.6655555555556,
		power: 661.074309309134,
		road: 8622.33486111112,
		acceleration: -0.164444444444445
	},
	{
		id: 933,
		time: 932,
		velocity: 12.5152777777778,
		power: 1781.87228792261,
		road: 8634.95148148149,
		acceleration: -0.0691666666666642
	},
	{
		id: 934,
		time: 933,
		velocity: 12.5652777777778,
		power: 3075.30021397462,
		road: 8647.55291666667,
		acceleration: 0.0387962962962938
	},
	{
		id: 935,
		time: 934,
		velocity: 12.7819444444444,
		power: 4056.859817688,
		road: 8660.23236111112,
		acceleration: 0.117222222222225
	},
	{
		id: 936,
		time: 935,
		velocity: 12.8669444444444,
		power: 4752.29783143871,
		road: 8673.05453703704,
		acceleration: 0.168240740740739
	},
	{
		id: 937,
		time: 936,
		velocity: 13.07,
		power: 3444.03990612972,
		road: 8685.98944444445,
		acceleration: 0.0572222222222241
	},
	{
		id: 938,
		time: 937,
		velocity: 12.9536111111111,
		power: 3328.37130699356,
		road: 8698.97597222223,
		acceleration: 0.0460185185185171
	},
	{
		id: 939,
		time: 938,
		velocity: 13.005,
		power: 4463.66272811394,
		road: 8712.05240740741,
		acceleration: 0.133796296296296
	},
	{
		id: 940,
		time: 939,
		velocity: 13.4713888888889,
		power: 5826.06703107811,
		road: 8725.3125925926,
		acceleration: 0.233703703703704
	},
	{
		id: 941,
		time: 940,
		velocity: 13.6547222222222,
		power: 7603.5314413275,
		road: 8738.86787037037,
		acceleration: 0.356481481481481
	},
	{
		id: 942,
		time: 941,
		velocity: 14.0744444444444,
		power: 7291.76026194354,
		road: 8752.75814814815,
		acceleration: 0.313518518518519
	},
	{
		id: 943,
		time: 942,
		velocity: 14.4119444444444,
		power: 9377.09081170457,
		road: 8767.02805555556,
		acceleration: 0.445740740740741
	},
	{
		id: 944,
		time: 943,
		velocity: 14.9919444444444,
		power: 9976.44819928436,
		road: 8781.75064814815,
		acceleration: 0.45962962962963
	},
	{
		id: 945,
		time: 944,
		velocity: 15.4533333333333,
		power: 8399.21857645168,
		road: 8796.86569444445,
		acceleration: 0.325277777777778
	},
	{
		id: 946,
		time: 945,
		velocity: 15.3877777777778,
		power: 5576.35343467344,
		road: 8812.20356481482,
		acceleration: 0.120370370370372
	},
	{
		id: 947,
		time: 946,
		velocity: 15.3530555555556,
		power: 3515.43192494479,
		road: 8827.59069444445,
		acceleration: -0.0218518518518529
	},
	{
		id: 948,
		time: 947,
		velocity: 15.3877777777778,
		power: 4910.61991544817,
		road: 8843.00291666667,
		acceleration: 0.0720370370370382
	},
	{
		id: 949,
		time: 948,
		velocity: 15.6038888888889,
		power: 5647.78952051761,
		road: 8858.51018518519,
		acceleration: 0.118055555555554
	},
	{
		id: 950,
		time: 949,
		velocity: 15.7072222222222,
		power: 6647.48079047472,
		road: 8874.16578703704,
		acceleration: 0.17861111111111
	},
	{
		id: 951,
		time: 950,
		velocity: 15.9236111111111,
		power: 6270.51758840001,
		road: 8889.9837962963,
		acceleration: 0.146203703703705
	},
	{
		id: 952,
		time: 951,
		velocity: 16.0425,
		power: 5276.35960961578,
		road: 8905.91291666667,
		acceleration: 0.07601851851852
	},
	{
		id: 953,
		time: 952,
		velocity: 15.9352777777778,
		power: 5176.76368536458,
		road: 8921.91337962963,
		acceleration: 0.0666666666666682
	},
	{
		id: 954,
		time: 953,
		velocity: 16.1236111111111,
		power: 5009.69244324534,
		road: 8937.97388888889,
		acceleration: 0.0534259259259215
	},
	{
		id: 955,
		time: 954,
		velocity: 16.2027777777778,
		power: 4654.91751283202,
		road: 8954.07550925926,
		acceleration: 0.0287962962962993
	},
	{
		id: 956,
		time: 955,
		velocity: 16.0216666666667,
		power: 3346.89534725497,
		road: 8970.16365740741,
		acceleration: -0.0557407407407453
	},
	{
		id: 957,
		time: 956,
		velocity: 15.9563888888889,
		power: 3182.81670961485,
		road: 8986.19166666667,
		acceleration: -0.0645370370370362
	},
	{
		id: 958,
		time: 957,
		velocity: 16.0091666666667,
		power: 4222.12481259126,
		road: 9002.18958333334,
		acceleration: 0.00435185185185283
	},
	{
		id: 959,
		time: 958,
		velocity: 16.0347222222222,
		power: 3818.26529176679,
		road: 9018.1787962963,
		acceleration: -0.0217592592592588
	},
	{
		id: 960,
		time: 959,
		velocity: 15.8911111111111,
		power: 3095.17753208866,
		road: 9034.12328703704,
		acceleration: -0.0676851851851836
	},
	{
		id: 961,
		time: 960,
		velocity: 15.8061111111111,
		power: 1498.99121540519,
		road: 9049.94916666667,
		acceleration: -0.169537037037038
	},
	{
		id: 962,
		time: 961,
		velocity: 15.5261111111111,
		power: 2424.17688934255,
		road: 9065.63800925926,
		acceleration: -0.104537037037035
	},
	{
		id: 963,
		time: 962,
		velocity: 15.5775,
		power: 2923.69028068612,
		road: 9081.24027777778,
		acceleration: -0.0686111111111138
	},
	{
		id: 964,
		time: 963,
		velocity: 15.6002777777778,
		power: 5855.93579098116,
		road: 9096.87162037038,
		acceleration: 0.126759259259263
	},
	{
		id: 965,
		time: 964,
		velocity: 15.9063888888889,
		power: 5649.9612344322,
		road: 9112.62037037038,
		acceleration: 0.108055555555554
	},
	{
		id: 966,
		time: 965,
		velocity: 15.9016666666667,
		power: 5808.52178566493,
		road: 9128.4800925926,
		acceleration: 0.113888888888889
	},
	{
		id: 967,
		time: 966,
		velocity: 15.9419444444444,
		power: 4437.70324723448,
		road: 9144.40726851852,
		acceleration: 0.0210185185185168
	},
	{
		id: 968,
		time: 967,
		velocity: 15.9694444444444,
		power: 5069.6941607705,
		road: 9160.37541666667,
		acceleration: 0.0609259259259289
	},
	{
		id: 969,
		time: 968,
		velocity: 16.0844444444444,
		power: 5828.97785253135,
		road: 9176.4275925926,
		acceleration: 0.107129629629631
	},
	{
		id: 970,
		time: 969,
		velocity: 16.2633333333333,
		power: 6228.58979522553,
		road: 9192.59736111112,
		acceleration: 0.128055555555552
	},
	{
		id: 971,
		time: 970,
		velocity: 16.3536111111111,
		power: 6533.14766197438,
		road: 9208.90203703704,
		acceleration: 0.14175925925926
	},
	{
		id: 972,
		time: 971,
		velocity: 16.5097222222222,
		power: 5619.76669452954,
		road: 9225.31694444445,
		acceleration: 0.0787037037037059
	},
	{
		id: 973,
		time: 972,
		velocity: 16.4994444444444,
		power: 5582.0543433736,
		road: 9241.80782407408,
		acceleration: 0.0732407407407401
	},
	{
		id: 974,
		time: 973,
		velocity: 16.5733333333333,
		power: 4352.35000086396,
		road: 9258.33236111112,
		acceleration: -0.00592592592592212
	},
	{
		id: 975,
		time: 974,
		velocity: 16.4919444444444,
		power: 3813.67787060423,
		road: 9274.83430555556,
		acceleration: -0.0392592592592642
	},
	{
		id: 976,
		time: 975,
		velocity: 16.3816666666667,
		power: 3993.19604398032,
		road: 9291.30324074074,
		acceleration: -0.0267592592592614
	},
	{
		id: 977,
		time: 976,
		velocity: 16.4930555555556,
		power: 4518.37022765092,
		road: 9307.76226851852,
		acceleration: 0.00694444444444287
	},
	{
		id: 978,
		time: 977,
		velocity: 16.5127777777778,
		power: 4886.24240267655,
		road: 9324.23958333334,
		acceleration: 0.0296296296296354
	},
	{
		id: 979,
		time: 978,
		velocity: 16.4705555555556,
		power: 4089.00693442499,
		road: 9340.72115740741,
		acceleration: -0.0211111111111144
	},
	{
		id: 980,
		time: 979,
		velocity: 16.4297222222222,
		power: 3103.83930399661,
		road: 9357.15115740741,
		acceleration: -0.0820370370370362
	},
	{
		id: 981,
		time: 980,
		velocity: 16.2666666666667,
		power: 2590.32430499799,
		road: 9373.48416666667,
		acceleration: -0.111944444444443
	},
	{
		id: 982,
		time: 981,
		velocity: 16.1347222222222,
		power: 1568.06807395788,
		road: 9389.67425925926,
		acceleration: -0.173888888888889
	},
	{
		id: 983,
		time: 982,
		velocity: 15.9080555555556,
		power: 1370.81877896174,
		road: 9405.68620370371,
		acceleration: -0.182407407407407
	},
	{
		id: 984,
		time: 983,
		velocity: 15.7194444444444,
		power: 1616.19368773006,
		road: 9421.52587962963,
		acceleration: -0.16212962962963
	},
	{
		id: 985,
		time: 984,
		velocity: 15.6483333333333,
		power: 2434.36920481242,
		road: 9437.23231481482,
		acceleration: -0.104351851851852
	},
	{
		id: 986,
		time: 985,
		velocity: 15.595,
		power: 1806.11396817335,
		road: 9452.8150462963,
		acceleration: -0.143055555555552
	},
	{
		id: 987,
		time: 986,
		velocity: 15.2902777777778,
		power: 904.643655265932,
		road: 9468.2262962963,
		acceleration: -0.199907407407409
	},
	{
		id: 988,
		time: 987,
		velocity: 15.0486111111111,
		power: 492.086758689903,
		road: 9483.42569444445,
		acceleration: -0.223796296296296
	},
	{
		id: 989,
		time: 988,
		velocity: 14.9236111111111,
		power: 933.787511411273,
		road: 9498.41870370371,
		acceleration: -0.188981481481481
	},
	{
		id: 990,
		time: 989,
		velocity: 14.7233333333333,
		power: 998.822296011026,
		road: 9513.22699074074,
		acceleration: -0.180462962962963
	},
	{
		id: 991,
		time: 990,
		velocity: 14.5072222222222,
		power: -1158.54760519988,
		road: 9527.77995370371,
		acceleration: -0.330185185185186
	},
	{
		id: 992,
		time: 991,
		velocity: 13.9330555555556,
		power: -1611.28690080252,
		road: 9541.98782407408,
		acceleration: -0.359999999999999
	},
	{
		id: 993,
		time: 992,
		velocity: 13.6433333333333,
		power: -2682.73841581591,
		road: 9555.79648148148,
		acceleration: -0.438425925925927
	},
	{
		id: 994,
		time: 993,
		velocity: 13.1919444444444,
		power: -1872.74670412783,
		road: 9569.19837962963,
		acceleration: -0.375092592592594
	},
	{
		id: 995,
		time: 994,
		velocity: 12.8077777777778,
		power: -2975.10579097941,
		road: 9582.18143518519,
		acceleration: -0.462592592592591
	},
	{
		id: 996,
		time: 995,
		velocity: 12.2555555555556,
		power: -3151.72108955812,
		road: 9594.69333333334,
		acceleration: -0.479722222222222
	},
	{
		id: 997,
		time: 996,
		velocity: 11.7527777777778,
		power: -5294.6339687089,
		road: 9606.62893518519,
		acceleration: -0.67287037037037
	},
	{
		id: 998,
		time: 997,
		velocity: 10.7891666666667,
		power: -7610.97572181239,
		road: 9617.77111111111,
		acceleration: -0.913981481481482
	},
	{
		id: 999,
		time: 998,
		velocity: 9.51361111111111,
		power: -8928.86360761057,
		road: 9627.90106481482,
		acceleration: -1.11046296296296
	},
	{
		id: 1000,
		time: 999,
		velocity: 8.42138888888889,
		power: -6278.43669964147,
		road: 9637.02736111111,
		acceleration: -0.896851851851851
	},
	{
		id: 1001,
		time: 1000,
		velocity: 8.09861111111111,
		power: -4079.45365143592,
		road: 9645.36490740741,
		acceleration: -0.680648148148149
	},
	{
		id: 1002,
		time: 1001,
		velocity: 7.47166666666667,
		power: -2260.13607765497,
		road: 9653.12847222222,
		acceleration: -0.467314814814816
	},
	{
		id: 1003,
		time: 1002,
		velocity: 7.01944444444444,
		power: -2781.64347558403,
		road: 9660.37819444445,
		acceleration: -0.560370370370371
	},
	{
		id: 1004,
		time: 1003,
		velocity: 6.4175,
		power: 3370.65681402716,
		road: 9667.51782407408,
		acceleration: 0.340185185185186
	},
	{
		id: 1005,
		time: 1004,
		velocity: 8.49222222222222,
		power: 7830.28170680029,
		road: 9675.27708333334,
		acceleration: 0.899074074074073
	},
	{
		id: 1006,
		time: 1005,
		velocity: 9.71666666666667,
		power: 12591.6593052998,
		road: 9684.14546296297,
		acceleration: 1.31916666666667
	},
	{
		id: 1007,
		time: 1006,
		velocity: 10.375,
		power: 7467.15741127899,
		road: 9693.98087962963,
		acceleration: 0.614907407407408
	},
	{
		id: 1008,
		time: 1007,
		velocity: 10.3369444444444,
		power: 3841.67802649302,
		road: 9704.22717592593,
		acceleration: 0.206851851851852
	},
	{
		id: 1009,
		time: 1008,
		velocity: 10.3372222222222,
		power: -365.632728178218,
		road: 9714.46490740741,
		acceleration: -0.223981481481481
	},
	{
		id: 1010,
		time: 1009,
		velocity: 9.70305555555555,
		power: 1023.78281098386,
		road: 9724.55152777778,
		acceleration: -0.0782407407407408
	},
	{
		id: 1011,
		time: 1010,
		velocity: 10.1022222222222,
		power: -726.243088983505,
		road: 9734.46916666667,
		acceleration: -0.259722222222221
	},
	{
		id: 1012,
		time: 1011,
		velocity: 9.55805555555555,
		power: 133.87919316625,
		road: 9744.17393518519,
		acceleration: -0.16601851851852
	},
	{
		id: 1013,
		time: 1012,
		velocity: 9.205,
		power: -779.80304543922,
		road: 9753.66347222223,
		acceleration: -0.264444444444443
	},
	{
		id: 1014,
		time: 1013,
		velocity: 9.30888888888889,
		power: 1100.17056292561,
		road: 9762.99444444445,
		acceleration: -0.0526851851851848
	},
	{
		id: 1015,
		time: 1014,
		velocity: 9.4,
		power: 3823.68105361841,
		road: 9772.42319444445,
		acceleration: 0.248240740740741
	},
	{
		id: 1016,
		time: 1015,
		velocity: 9.94972222222222,
		power: 5204.60264582818,
		road: 9782.16597222222,
		acceleration: 0.379814814814814
	},
	{
		id: 1017,
		time: 1016,
		velocity: 10.4483333333333,
		power: 6719.76387789465,
		road: 9792.35185185185,
		acceleration: 0.506388888888887
	},
	{
		id: 1018,
		time: 1017,
		velocity: 10.9191666666667,
		power: 5443.51254214996,
		road: 9802.96453703704,
		acceleration: 0.347222222222221
	},
	{
		id: 1019,
		time: 1018,
		velocity: 10.9913888888889,
		power: 5315.26176419821,
		road: 9813.90810185186,
		acceleration: 0.314537037037038
	},
	{
		id: 1020,
		time: 1019,
		velocity: 11.3919444444444,
		power: 4421.32958726835,
		road: 9825.11666666667,
		acceleration: 0.215462962962967
	},
	{
		id: 1021,
		time: 1020,
		velocity: 11.5655555555556,
		power: 5762.46573725254,
		road: 9836.59532407408,
		acceleration: 0.324722222222221
	},
	{
		id: 1022,
		time: 1021,
		velocity: 11.9655555555556,
		power: 5878.06280770294,
		road: 9848.39453703704,
		acceleration: 0.316388888888888
	},
	{
		id: 1023,
		time: 1022,
		velocity: 12.3411111111111,
		power: 7501.08372338179,
		road: 9860.56935185186,
		acceleration: 0.434814814814814
	},
	{
		id: 1024,
		time: 1023,
		velocity: 12.87,
		power: 8290.17204904934,
		road: 9873.19689814815,
		acceleration: 0.470648148148147
	},
	{
		id: 1025,
		time: 1024,
		velocity: 13.3775,
		power: 7237.18827628861,
		road: 9886.2387962963,
		acceleration: 0.358055555555557
	},
	{
		id: 1026,
		time: 1025,
		velocity: 13.4152777777778,
		power: 3999.09659549039,
		road: 9899.50430555556,
		acceleration: 0.0891666666666655
	},
	{
		id: 1027,
		time: 1026,
		velocity: 13.1375,
		power: 1398.09318569839,
		road: 9912.75643518519,
		acceleration: -0.115925925925925
	},
	{
		id: 1028,
		time: 1027,
		velocity: 13.0297222222222,
		power: 852.910330655905,
		road: 9925.87245370371,
		acceleration: -0.156296296296295
	},
	{
		id: 1029,
		time: 1028,
		velocity: 12.9463888888889,
		power: 1365.69114991303,
		road: 9938.85421296297,
		acceleration: -0.112222222222224
	},
	{
		id: 1030,
		time: 1029,
		velocity: 12.8008333333333,
		power: 1799.10132145069,
		road: 9951.74245370371,
		acceleration: -0.0748148148148129
	},
	{
		id: 1031,
		time: 1030,
		velocity: 12.8052777777778,
		power: 2832.81299114716,
		road: 9964.59842592593,
		acceleration: 0.0102777777777749
	},
	{
		id: 1032,
		time: 1031,
		velocity: 12.9772222222222,
		power: 4052.4550224476,
		road: 9977.51319444445,
		acceleration: 0.107314814814817
	},
	{
		id: 1033,
		time: 1032,
		velocity: 13.1227777777778,
		power: 4649.16915787285,
		road: 9990.55666666667,
		acceleration: 0.150092592592593
	},
	{
		id: 1034,
		time: 1033,
		velocity: 13.2555555555556,
		power: 3925.10003824166,
		road: 10003.7188425926,
		acceleration: 0.087314814814814
	},
	{
		id: 1035,
		time: 1034,
		velocity: 13.2391666666667,
		power: 3445.69599536274,
		road: 10016.9480555556,
		acceleration: 0.0467592592592592
	},
	{
		id: 1036,
		time: 1035,
		velocity: 13.2630555555556,
		power: 3453.68509938329,
		road: 10030.2235185185,
		acceleration: 0.0457407407407402
	},
	{
		id: 1037,
		time: 1036,
		velocity: 13.3927777777778,
		power: 5216.1395634128,
		road: 10043.6116666667,
		acceleration: 0.17962962962963
	},
	{
		id: 1038,
		time: 1037,
		velocity: 13.7780555555556,
		power: 5849.15711360634,
		road: 10057.1993055556,
		acceleration: 0.219351851851853
	},
	{
		id: 1039,
		time: 1038,
		velocity: 13.9211111111111,
		power: 6509.93290963522,
		road: 10071.0255555556,
		acceleration: 0.25787037037037
	},
	{
		id: 1040,
		time: 1039,
		velocity: 14.1663888888889,
		power: 5759.0485292022,
		road: 10085.0759722222,
		acceleration: 0.190462962962963
	},
	{
		id: 1041,
		time: 1040,
		velocity: 14.3494444444444,
		power: 6443.07727658093,
		road: 10099.3370833333,
		acceleration: 0.230925925925925
	},
	{
		id: 1042,
		time: 1041,
		velocity: 14.6138888888889,
		power: 5611.99836023855,
		road: 10113.794212963,
		acceleration: 0.161111111111111
	},
	{
		id: 1043,
		time: 1042,
		velocity: 14.6497222222222,
		power: 4767.52394688402,
		road: 10128.3793055556,
		acceleration: 0.094814814814816
	},
	{
		id: 1044,
		time: 1043,
		velocity: 14.6338888888889,
		power: 5347.24837335591,
		road: 10143.0775925926,
		acceleration: 0.131574074074074
	},
	{
		id: 1045,
		time: 1044,
		velocity: 15.0086111111111,
		power: 6320.84408734483,
		road: 10157.938287037,
		acceleration: 0.193240740740741
	},
	{
		id: 1046,
		time: 1045,
		velocity: 15.2294444444444,
		power: 6672.91173029629,
		road: 10172.9997685185,
		acceleration: 0.208333333333334
	},
	{
		id: 1047,
		time: 1046,
		velocity: 15.2588888888889,
		power: 5600.77505675002,
		road: 10188.22875,
		acceleration: 0.126666666666667
	},
	{
		id: 1048,
		time: 1047,
		velocity: 15.3886111111111,
		power: 2574.15707026693,
		road: 10203.4800925926,
		acceleration: -0.0819444444444457
	},
	{
		id: 1049,
		time: 1048,
		velocity: 14.9836111111111,
		power: 983.773564812785,
		road: 10218.5963888889,
		acceleration: -0.18814814814815
	},
	{
		id: 1050,
		time: 1049,
		velocity: 14.6944444444444,
		power: -956.720774939471,
		road: 10233.4589351852,
		acceleration: -0.319351851851851
	},
	{
		id: 1051,
		time: 1050,
		velocity: 14.4305555555556,
		power: 507.073309777531,
		road: 10248.0562037037,
		acceleration: -0.211203703703704
	},
	{
		id: 1052,
		time: 1051,
		velocity: 14.35,
		power: 1668.15410440477,
		road: 10262.4859722222,
		acceleration: -0.123796296296296
	},
	{
		id: 1053,
		time: 1052,
		velocity: 14.3230555555556,
		power: 1799.29938462348,
		road: 10276.7981944444,
		acceleration: -0.111296296296297
	},
	{
		id: 1054,
		time: 1053,
		velocity: 14.0966666666667,
		power: 2415.66418885356,
		road: 10291.0229166667,
		acceleration: -0.0637037037037018
	},
	{
		id: 1055,
		time: 1054,
		velocity: 14.1588888888889,
		power: 2162.48896876483,
		road: 10305.1756018519,
		acceleration: -0.0803703703703711
	},
	{
		id: 1056,
		time: 1055,
		velocity: 14.0819444444444,
		power: 2458.61804164263,
		road: 10319.2598611111,
		acceleration: -0.0564814814814802
	},
	{
		id: 1057,
		time: 1056,
		velocity: 13.9272222222222,
		power: 2157.5569503613,
		road: 10333.2773611111,
		acceleration: -0.0770370370370372
	},
	{
		id: 1058,
		time: 1057,
		velocity: 13.9277777777778,
		power: 1686.43498818118,
		road: 10347.2013888889,
		acceleration: -0.109907407407409
	},
	{
		id: 1059,
		time: 1058,
		velocity: 13.7522222222222,
		power: 3024.18486023282,
		road: 10361.0667592593,
		acceleration: -0.0074074074074062
	},
	{
		id: 1060,
		time: 1059,
		velocity: 13.905,
		power: 2358.62758874034,
		road: 10374.9000462963,
		acceleration: -0.056759259259259
	},
	{
		id: 1061,
		time: 1060,
		velocity: 13.7575,
		power: 2565.93923800929,
		road: 10388.6851388889,
		acceleration: -0.0396296296296299
	},
	{
		id: 1062,
		time: 1061,
		velocity: 13.6333333333333,
		power: 687.936612546606,
		road: 10402.3603240741,
		acceleration: -0.180185185185184
	},
	{
		id: 1063,
		time: 1062,
		velocity: 13.3644444444444,
		power: 1477.32111687771,
		road: 10415.8873148148,
		acceleration: -0.116203703703704
	},
	{
		id: 1064,
		time: 1063,
		velocity: 13.4088888888889,
		power: 2319.70606419853,
		road: 10429.3318981481,
		acceleration: -0.0486111111111125
	},
	{
		id: 1065,
		time: 1064,
		velocity: 13.4875,
		power: 4009.71206484631,
		road: 10442.7933796296,
		acceleration: 0.082407407407409
	},
	{
		id: 1066,
		time: 1065,
		velocity: 13.6116666666667,
		power: 3693.45219480387,
		road: 10456.3237037037,
		acceleration: 0.0552777777777766
	},
	{
		id: 1067,
		time: 1066,
		velocity: 13.5747222222222,
		power: 4458.60331753222,
		road: 10469.9372222222,
		acceleration: 0.111111111111111
	},
	{
		id: 1068,
		time: 1067,
		velocity: 13.8208333333333,
		power: 4488.00404821686,
		road: 10483.6607407407,
		acceleration: 0.108888888888888
	},
	{
		id: 1069,
		time: 1068,
		velocity: 13.9383333333333,
		power: 6375.83501038525,
		road: 10497.5606944444,
		acceleration: 0.243981481481482
	},
	{
		id: 1070,
		time: 1069,
		velocity: 14.3066666666667,
		power: 7199.71144211698,
		road: 10511.7283796296,
		acceleration: 0.291481481481481
	},
	{
		id: 1071,
		time: 1070,
		velocity: 14.6952777777778,
		power: 6979.75238523023,
		road: 10526.1722222222,
		acceleration: 0.260833333333332
	},
	{
		id: 1072,
		time: 1071,
		velocity: 14.7208333333333,
		power: 4954.28737992262,
		road: 10540.7997222222,
		acceleration: 0.106481481481483
	},
	{
		id: 1073,
		time: 1072,
		velocity: 14.6261111111111,
		power: 1869.62035894129,
		road: 10555.4233796296,
		acceleration: -0.114166666666666
	},
	{
		id: 1074,
		time: 1073,
		velocity: 14.3527777777778,
		power: -1530.82192898564,
		road: 10569.8121759259,
		acceleration: -0.355555555555554
	},
	{
		id: 1075,
		time: 1074,
		velocity: 13.6541666666667,
		power: -2716.15005370601,
		road: 10583.8026388889,
		acceleration: -0.441111111111113
	},
	{
		id: 1076,
		time: 1075,
		velocity: 13.3027777777778,
		power: -2265.21830697669,
		road: 10597.3694907407,
		acceleration: -0.406111111111112
	},
	{
		id: 1077,
		time: 1076,
		velocity: 13.1344444444444,
		power: 585.561084840168,
		road: 10610.6430092593,
		acceleration: -0.180555555555554
	},
	{
		id: 1078,
		time: 1077,
		velocity: 13.1125,
		power: 2789.25886698598,
		road: 10623.8243981482,
		acceleration: -0.00370370370370487
	},
	{
		id: 1079,
		time: 1078,
		velocity: 13.2916666666667,
		power: 3632.32656092497,
		road: 10637.0350462963,
		acceleration: 0.0622222222222231
	},
	{
		id: 1080,
		time: 1079,
		velocity: 13.3211111111111,
		power: 6965.31956205902,
		road: 10650.4347222222,
		acceleration: 0.315833333333332
	},
	{
		id: 1081,
		time: 1080,
		velocity: 14.06,
		power: 9282.34440834489,
		road: 10664.2273611111,
		acceleration: 0.470092592592593
	},
	{
		id: 1082,
		time: 1081,
		velocity: 14.7019444444444,
		power: 10643.1631271448,
		road: 10678.5234259259,
		acceleration: 0.536759259259258
	},
	{
		id: 1083,
		time: 1082,
		velocity: 14.9313888888889,
		power: 7081.26409822095,
		road: 10693.2155555556,
		acceleration: 0.255370370370372
	},
	{
		id: 1084,
		time: 1083,
		velocity: 14.8261111111111,
		power: 4051.30033813311,
		road: 10708.0525,
		acceleration: 0.0342592592592581
	},
	{
		id: 1085,
		time: 1084,
		velocity: 14.8047222222222,
		power: 1178.15587243343,
		road: 10722.8231018519,
		acceleration: -0.166944444444443
	},
	{
		id: 1086,
		time: 1085,
		velocity: 14.4305555555556,
		power: 36.0763677102106,
		road: 10737.3880092593,
		acceleration: -0.244444444444445
	},
	{
		id: 1087,
		time: 1086,
		velocity: 14.0927777777778,
		power: -2330.9289139744,
		road: 10751.6241203704,
		acceleration: -0.413148148148146
	},
	{
		id: 1088,
		time: 1087,
		velocity: 13.5652777777778,
		power: -1679.66149527647,
		road: 10765.4723611111,
		acceleration: -0.362592592592593
	},
	{
		id: 1089,
		time: 1088,
		velocity: 13.3427777777778,
		power: -1735.9590856242,
		road: 10778.9568981482,
		acceleration: -0.364814814814817
	},
	{
		id: 1090,
		time: 1089,
		velocity: 12.9983333333333,
		power: 77.031251284977,
		road: 10792.1493055556,
		acceleration: -0.219444444444443
	},
	{
		id: 1091,
		time: 1090,
		velocity: 12.9069444444444,
		power: 291.08457442406,
		road: 10805.1325,
		acceleration: -0.198981481481482
	},
	{
		id: 1092,
		time: 1091,
		velocity: 12.7458333333333,
		power: 2225.59147426985,
		road: 10817.9964814815,
		acceleration: -0.0394444444444417
	},
	{
		id: 1093,
		time: 1092,
		velocity: 12.88,
		power: 2501.65110906033,
		road: 10830.8326851852,
		acceleration: -0.0161111111111136
	},
	{
		id: 1094,
		time: 1093,
		velocity: 12.8586111111111,
		power: 3670.76468841368,
		road: 10843.6999074074,
		acceleration: 0.0781481481481503
	},
	{
		id: 1095,
		time: 1094,
		velocity: 12.9802777777778,
		power: 3577.93510049762,
		road: 10856.6401388889,
		acceleration: 0.06787037037037
	},
	{
		id: 1096,
		time: 1095,
		velocity: 13.0836111111111,
		power: 5745.87291856336,
		road: 10869.7321759259,
		acceleration: 0.23574074074074
	},
	{
		id: 1097,
		time: 1096,
		velocity: 13.5658333333333,
		power: 5340.19231338814,
		road: 10883.0386574074,
		acceleration: 0.193148148148149
	},
	{
		id: 1098,
		time: 1097,
		velocity: 13.5597222222222,
		power: 5666.11887519351,
		road: 10896.54625,
		acceleration: 0.209074074074074
	},
	{
		id: 1099,
		time: 1098,
		velocity: 13.7108333333333,
		power: 3747.84707089081,
		road: 10910.1861111111,
		acceleration: 0.0554629629629613
	},
	{
		id: 1100,
		time: 1099,
		velocity: 13.7322222222222,
		power: 4012.50592195293,
		road: 10923.8903703704,
		acceleration: 0.0733333333333359
	},
	{
		id: 1101,
		time: 1100,
		velocity: 13.7797222222222,
		power: 3895.2343410601,
		road: 10937.6622222222,
		acceleration: 0.0618518518518503
	},
	{
		id: 1102,
		time: 1101,
		velocity: 13.8963888888889,
		power: 2962.52246747528,
		road: 10951.4600462963,
		acceleration: -0.0099074074074057
	},
	{
		id: 1103,
		time: 1102,
		velocity: 13.7025,
		power: 3166.53731738948,
		road: 10965.2557407407,
		acceleration: 0.00564814814814874
	},
	{
		id: 1104,
		time: 1103,
		velocity: 13.7966666666667,
		power: 3144.19495769813,
		road: 10979.0561574074,
		acceleration: 0.00379629629629541
	},
	{
		id: 1105,
		time: 1104,
		velocity: 13.9077777777778,
		power: 3436.37650436067,
		road: 10992.8712037037,
		acceleration: 0.0254629629629619
	},
	{
		id: 1106,
		time: 1105,
		velocity: 13.7788888888889,
		power: 4401.54690732491,
		road: 11006.7470833333,
		acceleration: 0.0962037037037042
	},
	{
		id: 1107,
		time: 1106,
		velocity: 14.0852777777778,
		power: 3779.51802519002,
		road: 11020.6943981482,
		acceleration: 0.0466666666666669
	},
	{
		id: 1108,
		time: 1107,
		velocity: 14.0477777777778,
		power: 3907.94258179347,
		road: 11034.6922685185,
		acceleration: 0.0544444444444441
	},
	{
		id: 1109,
		time: 1108,
		velocity: 13.9422222222222,
		power: 1349.39081595775,
		road: 11048.6493518519,
		acceleration: -0.136018518518519
	},
	{
		id: 1110,
		time: 1109,
		velocity: 13.6772222222222,
		power: 504.0270822836,
		road: 11062.4402314815,
		acceleration: -0.19638888888889
	},
	{
		id: 1111,
		time: 1110,
		velocity: 13.4586111111111,
		power: 510.801078786813,
		road: 11076.0367592593,
		acceleration: -0.192314814814814
	},
	{
		id: 1112,
		time: 1111,
		velocity: 13.3652777777778,
		power: 2188.80043304454,
		road: 11089.5073611111,
		acceleration: -0.0595370370370372
	},
	{
		id: 1113,
		time: 1112,
		velocity: 13.4986111111111,
		power: 3619.48333915269,
		road: 11102.9741203704,
		acceleration: 0.051851851851854
	},
	{
		id: 1114,
		time: 1113,
		velocity: 13.6141666666667,
		power: 4510.9193224876,
		road: 11116.5256481482,
		acceleration: 0.117685185185184
	},
	{
		id: 1115,
		time: 1114,
		velocity: 13.7183333333333,
		power: 3207.59478108692,
		road: 11130.1433796296,
		acceleration: 0.0147222222222236
	},
	{
		id: 1116,
		time: 1115,
		velocity: 13.5427777777778,
		power: 1155.0041167397,
		road: 11143.6975925926,
		acceleration: -0.14175925925926
	},
	{
		id: 1117,
		time: 1116,
		velocity: 13.1888888888889,
		power: -1192.66814999918,
		road: 11157.0202777778,
		acceleration: -0.321296296296298
	},
	{
		id: 1118,
		time: 1117,
		velocity: 12.7544444444444,
		power: -1979.67153114458,
		road: 11169.9912037037,
		acceleration: -0.382222222222222
	},
	{
		id: 1119,
		time: 1118,
		velocity: 12.3961111111111,
		power: -1301.23926579123,
		road: 11182.6083796296,
		acceleration: -0.325277777777778
	},
	{
		id: 1120,
		time: 1119,
		velocity: 12.2130555555556,
		power: -2386.76836366581,
		road: 11194.8547685185,
		acceleration: -0.416296296296297
	},
	{
		id: 1121,
		time: 1120,
		velocity: 11.5055555555556,
		power: -2588.62568398323,
		road: 11206.6750925926,
		acceleration: -0.435833333333333
	},
	{
		id: 1122,
		time: 1121,
		velocity: 11.0886111111111,
		power: -2666.57252019919,
		road: 11218.0543981482,
		acceleration: -0.446203703703702
	},
	{
		id: 1123,
		time: 1122,
		velocity: 10.8744444444444,
		power: -985.129481562822,
		road: 11229.065787037,
		acceleration: -0.28962962962963
	},
	{
		id: 1124,
		time: 1123,
		velocity: 10.6366666666667,
		power: 210.972474452214,
		road: 11239.8461574074,
		acceleration: -0.172407407407409
	},
	{
		id: 1125,
		time: 1124,
		velocity: 10.5713888888889,
		power: 219.528816205559,
		road: 11250.4557407407,
		acceleration: -0.169166666666666
	},
	{
		id: 1126,
		time: 1125,
		velocity: 10.3669444444444,
		power: 310.161260051985,
		road: 11260.9018518519,
		acceleration: -0.157777777777778
	},
	{
		id: 1127,
		time: 1126,
		velocity: 10.1633333333333,
		power: 1.56329748301046,
		road: 11271.1756944444,
		acceleration: -0.18675925925926
	},
	{
		id: 1128,
		time: 1127,
		velocity: 10.0111111111111,
		power: -259.518744388187,
		road: 11281.2503240741,
		acceleration: -0.211666666666668
	},
	{
		id: 1129,
		time: 1128,
		velocity: 9.73194444444444,
		power: 177.190470476449,
		road: 11291.1372685185,
		acceleration: -0.163703703703703
	},
	{
		id: 1130,
		time: 1129,
		velocity: 9.67222222222222,
		power: 301.704647573471,
		road: 11300.8682407407,
		acceleration: -0.148240740740739
	},
	{
		id: 1131,
		time: 1130,
		velocity: 9.56638888888889,
		power: 1050.834876561,
		road: 11310.4925925926,
		acceleration: -0.0650000000000013
	},
	{
		id: 1132,
		time: 1131,
		velocity: 9.53694444444444,
		power: 1189.20421068104,
		road: 11320.0601851852,
		acceleration: -0.0485185185185202
	},
	{
		id: 1133,
		time: 1132,
		velocity: 9.52666666666667,
		power: 1455.88385354426,
		road: 11329.5943518519,
		acceleration: -0.0183333333333326
	},
	{
		id: 1134,
		time: 1133,
		velocity: 9.51138888888889,
		power: 756.352450098073,
		road: 11339.0722222222,
		acceleration: -0.0942592592592586
	},
	{
		id: 1135,
		time: 1134,
		velocity: 9.25416666666667,
		power: -395.493860318016,
		road: 11348.3925,
		acceleration: -0.220925925925926
	},
	{
		id: 1136,
		time: 1135,
		velocity: 8.86388888888889,
		power: -944.628448201666,
		road: 11357.4607407407,
		acceleration: -0.283148148148149
	},
	{
		id: 1137,
		time: 1136,
		velocity: 8.66194444444444,
		power: -523.954114152634,
		road: 11366.2706018519,
		acceleration: -0.233611111111111
	},
	{
		id: 1138,
		time: 1137,
		velocity: 8.55333333333333,
		power: 819.061581007086,
		road: 11374.9285185185,
		acceleration: -0.0702777777777772
	},
	{
		id: 1139,
		time: 1138,
		velocity: 8.65305555555556,
		power: 1503.30647814051,
		road: 11383.5581018519,
		acceleration: 0.0136111111111106
	},
	{
		id: 1140,
		time: 1139,
		velocity: 8.70277777777778,
		power: 1386.17550625253,
		road: 11392.1940740741,
		acceleration: -0.000833333333332575
	},
	{
		id: 1141,
		time: 1140,
		velocity: 8.55083333333333,
		power: 363.730274434085,
		road: 11400.7675,
		acceleration: -0.12425925925926
	},
	{
		id: 1142,
		time: 1141,
		velocity: 8.28027777777778,
		power: 50.1603236676787,
		road: 11409.1981944444,
		acceleration: -0.161203703703704
	},
	{
		id: 1143,
		time: 1142,
		velocity: 8.21916666666667,
		power: 272.027136103058,
		road: 11417.4825,
		acceleration: -0.131574074074075
	},
	{
		id: 1144,
		time: 1143,
		velocity: 8.15611111111111,
		power: 2025.26425204765,
		road: 11425.7468055556,
		acceleration: 0.0915740740740745
	},
	{
		id: 1145,
		time: 1144,
		velocity: 8.555,
		power: 2886.21088716618,
		road: 11434.1536111111,
		acceleration: 0.193425925925927
	},
	{
		id: 1146,
		time: 1145,
		velocity: 8.79944444444445,
		power: 2341.5372425313,
		road: 11442.7163888889,
		acceleration: 0.118518518518519
	},
	{
		id: 1147,
		time: 1146,
		velocity: 8.51166666666667,
		power: 2159.59292206743,
		road: 11451.3843981481,
		acceleration: 0.0919444444444455
	},
	{
		id: 1148,
		time: 1147,
		velocity: 8.83083333333333,
		power: 2277.65186230427,
		road: 11460.1494907407,
		acceleration: 0.10222222222222
	},
	{
		id: 1149,
		time: 1148,
		velocity: 9.10611111111111,
		power: 3940.71620908158,
		road: 11469.1102777778,
		acceleration: 0.289166666666665
	},
	{
		id: 1150,
		time: 1149,
		velocity: 9.37916666666667,
		power: 3681.32455203877,
		road: 11478.3374074074,
		acceleration: 0.24351851851852
	},
	{
		id: 1151,
		time: 1150,
		velocity: 9.56138888888889,
		power: 3980.07801937243,
		road: 11487.8176388889,
		acceleration: 0.262685185185186
	},
	{
		id: 1152,
		time: 1151,
		velocity: 9.89416666666667,
		power: 3827.96870456809,
		road: 11497.5453703704,
		acceleration: 0.232314814814815
	},
	{
		id: 1153,
		time: 1152,
		velocity: 10.0761111111111,
		power: 3135.87342280214,
		road: 11507.46375,
		acceleration: 0.148981481481481
	},
	{
		id: 1154,
		time: 1153,
		velocity: 10.0083333333333,
		power: 2453.93472032874,
		road: 11517.492962963,
		acceleration: 0.0726851851851844
	},
	{
		id: 1155,
		time: 1154,
		velocity: 10.1122222222222,
		power: 1220.59223167973,
		road: 11527.5302314815,
		acceleration: -0.0565740740740726
	},
	{
		id: 1156,
		time: 1155,
		velocity: 9.90638888888889,
		power: -409.186060667319,
		road: 11537.4262037037,
		acceleration: -0.22601851851852
	},
	{
		id: 1157,
		time: 1156,
		velocity: 9.33027777777778,
		power: -1792.76580236839,
		road: 11547.0214351852,
		acceleration: -0.375462962962963
	},
	{
		id: 1158,
		time: 1157,
		velocity: 8.98583333333333,
		power: -1695.8123015914,
		road: 11556.2447222222,
		acceleration: -0.368425925925925
	},
	{
		id: 1159,
		time: 1158,
		velocity: 8.80111111111111,
		power: -397.317641135869,
		road: 11565.1742592593,
		acceleration: -0.219074074074074
	},
	{
		id: 1160,
		time: 1159,
		velocity: 8.67305555555556,
		power: -9.79818666742063,
		road: 11573.9084722222,
		acceleration: -0.171574074074075
	},
	{
		id: 1161,
		time: 1160,
		velocity: 8.47111111111111,
		power: -1328.64886941103,
		road: 11582.3906481481,
		acceleration: -0.332500000000001
	},
	{
		id: 1162,
		time: 1161,
		velocity: 7.80361111111111,
		power: -1760.0848616871,
		road: 11590.5104166667,
		acceleration: -0.392314814814814
	},
	{
		id: 1163,
		time: 1162,
		velocity: 7.49611111111111,
		power: -2118.47251717557,
		road: 11598.2090277778,
		acceleration: -0.45
	},
	{
		id: 1164,
		time: 1163,
		velocity: 7.12111111111111,
		power: -1572.94687291504,
		road: 11605.490462963,
		acceleration: -0.384351851851852
	},
	{
		id: 1165,
		time: 1164,
		velocity: 6.65055555555556,
		power: -1966.58376458451,
		road: 11612.3520833333,
		acceleration: -0.455277777777777
	},
	{
		id: 1166,
		time: 1165,
		velocity: 6.13027777777778,
		power: -1783.3805977746,
		road: 11618.764537037,
		acceleration: -0.443055555555556
	},
	{
		id: 1167,
		time: 1166,
		velocity: 5.79194444444444,
		power: -1758.93633060455,
		road: 11624.7265740741,
		acceleration: -0.457777777777777
	},
	{
		id: 1168,
		time: 1167,
		velocity: 5.27722222222222,
		power: -1778.28095946564,
		road: 11630.2171296296,
		acceleration: -0.485185185185187
	},
	{
		id: 1169,
		time: 1168,
		velocity: 4.67472222222222,
		power: -1521.92365011523,
		road: 11635.2346759259,
		acceleration: -0.460833333333333
	},
	{
		id: 1170,
		time: 1169,
		velocity: 4.40944444444444,
		power: -2253.33703318692,
		road: 11639.6860185185,
		acceleration: -0.671574074074075
	},
	{
		id: 1171,
		time: 1170,
		velocity: 3.2625,
		power: -1447.08573660134,
		road: 11643.5357407407,
		acceleration: -0.531666666666666
	},
	{
		id: 1172,
		time: 1171,
		velocity: 3.07972222222222,
		power: -1112.76756489974,
		road: 11646.8773611111,
		acceleration: -0.484537037037037
	},
	{
		id: 1173,
		time: 1172,
		velocity: 2.95583333333333,
		power: 1117.65598753693,
		road: 11650.0931944444,
		acceleration: 0.232962962962963
	},
	{
		id: 1174,
		time: 1173,
		velocity: 3.96138888888889,
		power: 3731.93903004896,
		road: 11653.8771296296,
		acceleration: 0.90324074074074
	},
	{
		id: 1175,
		time: 1174,
		velocity: 5.78944444444444,
		power: 5544.92531260938,
		road: 11658.6535185185,
		acceleration: 1.08166666666667
	},
	{
		id: 1176,
		time: 1175,
		velocity: 6.20083333333333,
		power: 8091.4820347615,
		road: 11664.6112962963,
		acceleration: 1.28111111111111
	},
	{
		id: 1177,
		time: 1176,
		velocity: 7.80472222222222,
		power: 11147.6298055233,
		road: 11671.9311111111,
		acceleration: 1.44296296296296
	},
	{
		id: 1178,
		time: 1177,
		velocity: 10.1183333333333,
		power: 13420.532963143,
		road: 11680.6914351852,
		acceleration: 1.43805555555556
	},
	{
		id: 1179,
		time: 1178,
		velocity: 10.515,
		power: 10080.2487476358,
		road: 11690.6125,
		acceleration: 0.883425925925925
	},
	{
		id: 1180,
		time: 1179,
		velocity: 10.455,
		power: 4778.93675689423,
		road: 11701.1190740741,
		acceleration: 0.287592592592592
	},
	{
		id: 1181,
		time: 1180,
		velocity: 10.9811111111111,
		power: 8409.76414393434,
		road: 11712.0744907407,
		acceleration: 0.610092592592592
	},
	{
		id: 1182,
		time: 1181,
		velocity: 12.3452777777778,
		power: 11857.4374743329,
		road: 11723.7644444444,
		acceleration: 0.858981481481484
	},
	{
		id: 1183,
		time: 1182,
		velocity: 13.0319444444444,
		power: 15803.8818417361,
		road: 11736.42875,
		acceleration: 1.08972222222222
	},
	{
		id: 1184,
		time: 1183,
		velocity: 14.2502777777778,
		power: 14799.2556133156,
		road: 11750.0888888889,
		acceleration: 0.901944444444442
	},
	{
		id: 1185,
		time: 1184,
		velocity: 15.0511111111111,
		power: 12472.7861112727,
		road: 11764.5295833333,
		acceleration: 0.659166666666669
	},
	{
		id: 1186,
		time: 1185,
		velocity: 15.0094444444444,
		power: 10415.0238342345,
		road: 11779.5358333333,
		acceleration: 0.471944444444445
	},
	{
		id: 1187,
		time: 1186,
		velocity: 15.6661111111111,
		power: 10266.2638687971,
		road: 11794.9943981481,
		acceleration: 0.432685185185186
	},
	{
		id: 1188,
		time: 1187,
		velocity: 16.3491666666667,
		power: 11330.3256412977,
		road: 11810.9066203704,
		acceleration: 0.47462962962963
	},
	{
		id: 1189,
		time: 1188,
		velocity: 16.4333333333333,
		power: 9481.2370671185,
		road: 11827.2212962963,
		acceleration: 0.330277777777777
	},
	{
		id: 1190,
		time: 1189,
		velocity: 16.6569444444444,
		power: 7310.63500249555,
		road: 11843.7906944444,
		acceleration: 0.179166666666664
	},
	{
		id: 1191,
		time: 1190,
		velocity: 16.8866666666667,
		power: 7230.28534517211,
		road: 11860.5327314815,
		acceleration: 0.166111111111114
	},
	{
		id: 1192,
		time: 1191,
		velocity: 16.9316666666667,
		power: 5746.39081501004,
		road: 11877.3921759259,
		acceleration: 0.0687037037037044
	},
	{
		id: 1193,
		time: 1192,
		velocity: 16.8630555555556,
		power: 4494.04590086486,
		road: 11894.2809722222,
		acceleration: -0.0100000000000051
	},
	{
		id: 1194,
		time: 1193,
		velocity: 16.8566666666667,
		power: 2481.16756231133,
		road: 11911.0984722222,
		acceleration: -0.132592592592591
	},
	{
		id: 1195,
		time: 1194,
		velocity: 16.5338888888889,
		power: 1730.26811022912,
		road: 11927.7619907407,
		acceleration: -0.175370370370366
	},
	{
		id: 1196,
		time: 1195,
		velocity: 16.3369444444444,
		power: -2353.90785965359,
		road: 11944.1234259259,
		acceleration: -0.428796296296298
	},
	{
		id: 1197,
		time: 1196,
		velocity: 15.5702777777778,
		power: -3397.96831521212,
		road: 11960.0237037037,
		acceleration: -0.493518518518519
	},
	{
		id: 1198,
		time: 1197,
		velocity: 15.0533333333333,
		power: -3461.9916481087,
		road: 11975.4290277778,
		acceleration: -0.496388888888889
	},
	{
		id: 1199,
		time: 1198,
		velocity: 14.8477777777778,
		power: -843.490610227487,
		road: 11990.4295833333,
		acceleration: -0.31314814814815
	},
	{
		id: 1200,
		time: 1199,
		velocity: 14.6308333333333,
		power: 486.951549400698,
		road: 12005.1659259259,
		acceleration: -0.215277777777775
	},
	{
		id: 1201,
		time: 1200,
		velocity: 14.4075,
		power: 36.897552403836,
		road: 12019.6729166667,
		acceleration: -0.243425925925926
	},
	{
		id: 1202,
		time: 1201,
		velocity: 14.1175,
		power: 669.016433768512,
		road: 12033.9614351852,
		acceleration: -0.19351851851852
	},
	{
		id: 1203,
		time: 1202,
		velocity: 14.0502777777778,
		power: 16.4636393166554,
		road: 12048.0342592593,
		acceleration: -0.23787037037037
	},
	{
		id: 1204,
		time: 1203,
		velocity: 13.6938888888889,
		power: 23.4583182344759,
		road: 12061.8713425926,
		acceleration: -0.233611111111109
	},
	{
		id: 1205,
		time: 1204,
		velocity: 13.4166666666667,
		power: 817.809147431525,
		road: 12075.5068981481,
		acceleration: -0.169444444444444
	},
	{
		id: 1206,
		time: 1205,
		velocity: 13.5419444444444,
		power: 2869.95077493117,
		road: 12089.0532407407,
		acceleration: -0.00898148148148437
	},
	{
		id: 1207,
		time: 1206,
		velocity: 13.6669444444444,
		power: 3108.35246400792,
		road: 12102.5998148148,
		acceleration: 0.00944444444444414
	},
	{
		id: 1208,
		time: 1207,
		velocity: 13.445,
		power: 4355.67955559306,
		road: 12116.2029166667,
		acceleration: 0.103611111111112
	},
	{
		id: 1209,
		time: 1208,
		velocity: 13.8527777777778,
		power: 5979.23678032659,
		road: 12129.9681481481,
		acceleration: 0.220648148148149
	},
	{
		id: 1210,
		time: 1209,
		velocity: 14.3288888888889,
		power: 9090.18244082787,
		road: 12144.0616666667,
		acceleration: 0.435925925925924
	},
	{
		id: 1211,
		time: 1210,
		velocity: 14.7527777777778,
		power: 8377.34293147633,
		road: 12158.5528240741,
		acceleration: 0.359351851851853
	},
	{
		id: 1212,
		time: 1211,
		velocity: 14.9308333333333,
		power: 6375.25149651693,
		road: 12173.3243055556,
		acceleration: 0.201296296296299
	},
	{
		id: 1213,
		time: 1212,
		velocity: 14.9327777777778,
		power: 4647.53763306556,
		road: 12188.2331944444,
		acceleration: 0.0735185185185152
	},
	{
		id: 1214,
		time: 1213,
		velocity: 14.9733333333333,
		power: 3080.84324111397,
		road: 12203.1603240741,
		acceleration: -0.0370370370370345
	},
	{
		id: 1215,
		time: 1214,
		velocity: 14.8197222222222,
		power: 1654.77507555599,
		road: 12218.0014814815,
		acceleration: -0.134907407407407
	},
	{
		id: 1216,
		time: 1215,
		velocity: 14.5280555555556,
		power: -1107.38140486801,
		road: 12232.6116203704,
		acceleration: -0.327129629629631
	},
	{
		id: 1217,
		time: 1216,
		velocity: 13.9919444444444,
		power: -2055.4862887886,
		road: 12246.861712963,
		acceleration: -0.392962962962963
	},
	{
		id: 1218,
		time: 1217,
		velocity: 13.6408333333333,
		power: -1389.48679191043,
		road: 12260.7448611111,
		acceleration: -0.340925925925925
	},
	{
		id: 1219,
		time: 1218,
		velocity: 13.5052777777778,
		power: -1942.60698751868,
		road: 12274.267037037,
		acceleration: -0.38101851851852
	},
	{
		id: 1220,
		time: 1219,
		velocity: 12.8488888888889,
		power: -1305.58377762619,
		road: 12287.4341666667,
		acceleration: -0.329074074074073
	},
	{
		id: 1221,
		time: 1220,
		velocity: 12.6536111111111,
		power: -1433.06349597813,
		road: 12300.2681018519,
		acceleration: -0.337314814814816
	},
	{
		id: 1222,
		time: 1221,
		velocity: 12.4933333333333,
		power: 2495.61866825662,
		road: 12312.9277777778,
		acceleration: -0.0112037037037034
	},
	{
		id: 1223,
		time: 1222,
		velocity: 12.8152777777778,
		power: 4091.68930480784,
		road: 12325.6412037037,
		acceleration: 0.118703703703703
	},
	{
		id: 1224,
		time: 1223,
		velocity: 13.0097222222222,
		power: 5415.13314713277,
		road: 12338.5237037037,
		acceleration: 0.219444444444443
	},
	{
		id: 1225,
		time: 1224,
		velocity: 13.1516666666667,
		power: 4878.17029763462,
		road: 12351.5994444444,
		acceleration: 0.167037037037039
	},
	{
		id: 1226,
		time: 1225,
		velocity: 13.3163888888889,
		power: 4435.78513568629,
		road: 12364.8214351852,
		acceleration: 0.125462962962963
	},
	{
		id: 1227,
		time: 1226,
		velocity: 13.3861111111111,
		power: 4646.73740560505,
		road: 12378.1744444444,
		acceleration: 0.136574074074074
	},
	{
		id: 1228,
		time: 1227,
		velocity: 13.5613888888889,
		power: 3632.1718086988,
		road: 12391.6225,
		acceleration: 0.0535185185185192
	},
	{
		id: 1229,
		time: 1228,
		velocity: 13.4769444444444,
		power: 3681.52579319955,
		road: 12405.125,
		acceleration: 0.055370370370369
	},
	{
		id: 1230,
		time: 1229,
		velocity: 13.5522222222222,
		power: 3993.49142483174,
		road: 12418.6937037037,
		acceleration: 0.0770370370370372
	},
	{
		id: 1231,
		time: 1230,
		velocity: 13.7925,
		power: 3819.17061633406,
		road: 12432.3314351852,
		acceleration: 0.0610185185185195
	},
	{
		id: 1232,
		time: 1231,
		velocity: 13.66,
		power: 4049.87219141108,
		road: 12446.0377314815,
		acceleration: 0.0761111111111106
	},
	{
		id: 1233,
		time: 1232,
		velocity: 13.7805555555556,
		power: 2949.08902249114,
		road: 12459.7775462963,
		acceleration: -0.0090740740740749
	},
	{
		id: 1234,
		time: 1233,
		velocity: 13.7652777777778,
		power: 3353.97474751247,
		road: 12473.5236111111,
		acceleration: 0.0215740740740742
	},
	{
		id: 1235,
		time: 1234,
		velocity: 13.7247222222222,
		power: 1688.87931500201,
		road: 12487.2283333333,
		acceleration: -0.104259259259258
	},
	{
		id: 1236,
		time: 1235,
		velocity: 13.4677777777778,
		power: -1151.02346426502,
		road: 12500.7212037037,
		acceleration: -0.319444444444445
	},
	{
		id: 1237,
		time: 1236,
		velocity: 12.8069444444444,
		power: -2020.88753034024,
		road: 12513.8613888889,
		acceleration: -0.385925925925926
	},
	{
		id: 1238,
		time: 1237,
		velocity: 12.5669444444444,
		power: -2349.57842040062,
		road: 12526.6025,
		acceleration: -0.412222222222223
	},
	{
		id: 1239,
		time: 1238,
		velocity: 12.2311111111111,
		power: -157.193003078311,
		road: 12539.0236574074,
		acceleration: -0.227685185185184
	},
	{
		id: 1240,
		time: 1239,
		velocity: 12.1238888888889,
		power: -256.072605982515,
		road: 12551.2143518518,
		acceleration: -0.23324074074074
	},
	{
		id: 1241,
		time: 1240,
		velocity: 11.8672222222222,
		power: 3148.36776536311,
		road: 12563.3197222222,
		acceleration: 0.0625925925925905
	},
	{
		id: 1242,
		time: 1241,
		velocity: 12.4188888888889,
		power: 4681.82749812934,
		road: 12575.5511111111,
		acceleration: 0.189444444444446
	},
	{
		id: 1243,
		time: 1242,
		velocity: 12.6922222222222,
		power: 7954.09590530461,
		road: 12588.1012037037,
		acceleration: 0.447962962962963
	},
	{
		id: 1244,
		time: 1243,
		velocity: 13.2111111111111,
		power: 6277.85758061173,
		road: 12601.0191203704,
		acceleration: 0.287685185185186
	},
	{
		id: 1245,
		time: 1244,
		velocity: 13.2819444444444,
		power: 5276.33274470151,
		road: 12614.1783796296,
		acceleration: 0.195
	},
	{
		id: 1246,
		time: 1245,
		velocity: 13.2772222222222,
		power: 2839.03400261514,
		road: 12627.4340740741,
		acceleration: -0.00212962962963203
	},
	{
		id: 1247,
		time: 1246,
		velocity: 13.2047222222222,
		power: 1886.35063308964,
		road: 12640.6505092593,
		acceleration: -0.0763888888888875
	},
	{
		id: 1248,
		time: 1247,
		velocity: 13.0527777777778,
		power: 1480.38060305643,
		road: 12653.7755555556,
		acceleration: -0.106388888888889
	},
	{
		id: 1249,
		time: 1248,
		velocity: 12.9580555555556,
		power: 1590.88705399006,
		road: 12666.7998611111,
		acceleration: -0.095092592592593
	},
	{
		id: 1250,
		time: 1249,
		velocity: 12.9194444444444,
		power: 1983.52472923723,
		road: 12679.7459259259,
		acceleration: -0.0613888888888887
	},
	{
		id: 1251,
		time: 1250,
		velocity: 12.8686111111111,
		power: 2060.01407626378,
		road: 12692.6344907407,
		acceleration: -0.0536111111111115
	},
	{
		id: 1252,
		time: 1251,
		velocity: 12.7972222222222,
		power: 2245.4956861615,
		road: 12705.4776388889,
		acceleration: -0.0372222222222209
	},
	{
		id: 1253,
		time: 1252,
		velocity: 12.8077777777778,
		power: 3073.67950555629,
		road: 12718.3174074074,
		acceleration: 0.0304629629629609
	},
	{
		id: 1254,
		time: 1253,
		velocity: 12.96,
		power: 3662.38419829407,
		road: 12731.2106481481,
		acceleration: 0.0764814814814798
	},
	{
		id: 1255,
		time: 1254,
		velocity: 13.0266666666667,
		power: 4011.36323565456,
		road: 12744.1927777778,
		acceleration: 0.101296296296301
	},
	{
		id: 1256,
		time: 1255,
		velocity: 13.1116666666667,
		power: 3114.85042755827,
		road: 12757.2389351852,
		acceleration: 0.0267592592592578
	},
	{
		id: 1257,
		time: 1256,
		velocity: 13.0402777777778,
		power: 3457.89202241293,
		road: 12770.3249074074,
		acceleration: 0.0528703703703712
	},
	{
		id: 1258,
		time: 1257,
		velocity: 13.1852777777778,
		power: 3996.81569909511,
		road: 12783.4838888889,
		acceleration: 0.0931481481481455
	},
	{
		id: 1259,
		time: 1258,
		velocity: 13.3911111111111,
		power: 4706.99930665289,
		road: 12796.761712963,
		acceleration: 0.14453703703704
	},
	{
		id: 1260,
		time: 1259,
		velocity: 13.4738888888889,
		power: 3588.20767090716,
		road: 12810.1381481481,
		acceleration: 0.0526851851851831
	},
	{
		id: 1261,
		time: 1260,
		velocity: 13.3433333333333,
		power: 3274.57289632064,
		road: 12823.5543055556,
		acceleration: 0.0267592592592596
	},
	{
		id: 1262,
		time: 1261,
		velocity: 13.4713888888889,
		power: 2731.23535670277,
		road: 12836.9759259259,
		acceleration: -0.0158333333333331
	},
	{
		id: 1263,
		time: 1262,
		velocity: 13.4263888888889,
		power: 3546.38943832114,
		road: 12850.4132407407,
		acceleration: 0.0472222222222225
	},
	{
		id: 1264,
		time: 1263,
		velocity: 13.485,
		power: 3143.99217872223,
		road: 12863.8815740741,
		acceleration: 0.0148148148148142
	},
	{
		id: 1265,
		time: 1264,
		velocity: 13.5158333333333,
		power: 3331.0311505611,
		road: 12877.3716203704,
		acceleration: 0.0286111111111111
	},
	{
		id: 1266,
		time: 1265,
		velocity: 13.5122222222222,
		power: 2934.56094453946,
		road: 12890.8746759259,
		acceleration: -0.00259259259259181
	},
	{
		id: 1267,
		time: 1266,
		velocity: 13.4772222222222,
		power: 2536.19153981946,
		road: 12904.3599537037,
		acceleration: -0.032962962962964
	},
	{
		id: 1268,
		time: 1267,
		velocity: 13.4169444444444,
		power: 2533.17089533919,
		road: 12917.8126388889,
		acceleration: -0.0322222222222219
	},
	{
		id: 1269,
		time: 1268,
		velocity: 13.4155555555556,
		power: 2396.83319383041,
		road: 12931.2283333333,
		acceleration: -0.0417592592592584
	},
	{
		id: 1270,
		time: 1269,
		velocity: 13.3519444444444,
		power: 4027.38361505524,
		road: 12944.6655092593,
		acceleration: 0.0847222222222204
	},
	{
		id: 1271,
		time: 1270,
		velocity: 13.6711111111111,
		power: 4575.99571513799,
		road: 12958.2066203704,
		acceleration: 0.12314814814815
	},
	{
		id: 1272,
		time: 1271,
		velocity: 13.785,
		power: 4446.60277097693,
		road: 12971.8635185185,
		acceleration: 0.108425925925925
	},
	{
		id: 1273,
		time: 1272,
		velocity: 13.6772222222222,
		power: 3502.37209194506,
		road: 12985.5913888889,
		acceleration: 0.0335185185185178
	},
	{
		id: 1274,
		time: 1273,
		velocity: 13.7716666666667,
		power: 2447.58756245193,
		road: 12999.3126388889,
		acceleration: -0.0467592592592592
	},
	{
		id: 1275,
		time: 1274,
		velocity: 13.6447222222222,
		power: 2307.14434830784,
		road: 13012.9825,
		acceleration: -0.0560185185185169
	},
	{
		id: 1276,
		time: 1275,
		velocity: 13.5091666666667,
		power: 1875.56214889566,
		road: 13026.5807407407,
		acceleration: -0.0872222222222216
	},
	{
		id: 1277,
		time: 1276,
		velocity: 13.51,
		power: 2785.13470628575,
		road: 13040.1275925926,
		acceleration: -0.0155555555555562
	},
	{
		id: 1278,
		time: 1277,
		velocity: 13.5980555555556,
		power: 2985.49139832145,
		road: 13053.6667592593,
		acceleration: 0.000185185185184622
	},
	{
		id: 1279,
		time: 1278,
		velocity: 13.5097222222222,
		power: 3018.41224965029,
		road: 13067.2073611111,
		acceleration: 0.0026851851851859
	},
	{
		id: 1280,
		time: 1279,
		velocity: 13.5180555555556,
		power: 3118.13149194244,
		road: 13080.7543981482,
		acceleration: 0.0101851851851862
	},
	{
		id: 1281,
		time: 1280,
		velocity: 13.6286111111111,
		power: 3782.8761481339,
		road: 13094.3366666667,
		acceleration: 0.0602777777777774
	},
	{
		id: 1282,
		time: 1281,
		velocity: 13.6905555555556,
		power: 3720.96638050087,
		road: 13107.975787037,
		acceleration: 0.0534259259259251
	},
	{
		id: 1283,
		time: 1282,
		velocity: 13.6783333333333,
		power: 3212.23492125078,
		road: 13121.6482407407,
		acceleration: 0.0132407407407396
	},
	{
		id: 1284,
		time: 1283,
		velocity: 13.6683333333333,
		power: 3232.80162290463,
		road: 13135.3344907407,
		acceleration: 0.0143518518518544
	},
	{
		id: 1285,
		time: 1284,
		velocity: 13.7336111111111,
		power: 3883.17032147179,
		road: 13149.0592592593,
		acceleration: 0.0626851851851828
	},
	{
		id: 1286,
		time: 1285,
		velocity: 13.8663888888889,
		power: 3704.11238357246,
		road: 13162.8388888889,
		acceleration: 0.0470370370370361
	},
	{
		id: 1287,
		time: 1286,
		velocity: 13.8094444444444,
		power: 3282.73337778796,
		road: 13176.6490277778,
		acceleration: 0.0139814814814816
	},
	{
		id: 1288,
		time: 1287,
		velocity: 13.7755555555556,
		power: 3234.27933681355,
		road: 13190.4711111111,
		acceleration: 0.00990740740740925
	},
	{
		id: 1289,
		time: 1288,
		velocity: 13.8961111111111,
		power: 3442.68815063074,
		road: 13204.3106944444,
		acceleration: 0.0250925925925927
	},
	{
		id: 1290,
		time: 1289,
		velocity: 13.8847222222222,
		power: 3430.4742681292,
		road: 13218.1744907407,
		acceleration: 0.0233333333333334
	},
	{
		id: 1291,
		time: 1290,
		velocity: 13.8455555555556,
		power: 3078.01629704826,
		road: 13232.0481481482,
		acceleration: -0.00361111111111079
	},
	{
		id: 1292,
		time: 1291,
		velocity: 13.8852777777778,
		power: 1831.8979631147,
		road: 13245.8718055556,
		acceleration: -0.0963888888888889
	},
	{
		id: 1293,
		time: 1292,
		velocity: 13.5955555555556,
		power: -240.284656248658,
		road: 13259.5218055556,
		acceleration: -0.250925925925927
	},
	{
		id: 1294,
		time: 1293,
		velocity: 13.0927777777778,
		power: -1175.4301535209,
		road: 13272.8862037037,
		acceleration: -0.320277777777777
	},
	{
		id: 1295,
		time: 1294,
		velocity: 12.9244444444444,
		power: -1285.07381085234,
		road: 13285.9271759259,
		acceleration: -0.326574074074076
	},
	{
		id: 1296,
		time: 1295,
		velocity: 12.6158333333333,
		power: -817.102776866523,
		road: 13298.6618055556,
		acceleration: -0.28611111111111
	},
	{
		id: 1297,
		time: 1296,
		velocity: 12.2344444444444,
		power: -2078.37798682864,
		road: 13311.0584722222,
		acceleration: -0.389814814814814
	},
	{
		id: 1298,
		time: 1297,
		velocity: 11.755,
		power: -1640.00833714354,
		road: 13323.0842592593,
		acceleration: -0.351944444444447
	},
	{
		id: 1299,
		time: 1298,
		velocity: 11.56,
		power: -1207.23020105551,
		road: 13334.7776851852,
		acceleration: -0.312777777777775
	},
	{
		id: 1300,
		time: 1299,
		velocity: 11.2961111111111,
		power: -1684.89636598455,
		road: 13346.1368518519,
		acceleration: -0.355740740740742
	},
	{
		id: 1301,
		time: 1300,
		velocity: 10.6877777777778,
		power: -2017.92756803887,
		road: 13357.1240740741,
		acceleration: -0.388148148148147
	},
	{
		id: 1302,
		time: 1301,
		velocity: 10.3955555555556,
		power: -2432.08945019971,
		road: 13367.7013425926,
		acceleration: -0.431759259259261
	},
	{
		id: 1303,
		time: 1302,
		velocity: 10.0008333333333,
		power: -2343.71016776126,
		road: 13377.8487962963,
		acceleration: -0.427870370370369
	},
	{
		id: 1304,
		time: 1303,
		velocity: 9.40416666666667,
		power: -3112.84932762821,
		road: 13387.5233333333,
		acceleration: -0.517962962962963
	},
	{
		id: 1305,
		time: 1304,
		velocity: 8.84166666666667,
		power: -2287.81245430339,
		road: 13396.7207407407,
		acceleration: -0.436296296296295
	},
	{
		id: 1306,
		time: 1305,
		velocity: 8.69194444444445,
		power: -2616.54019553974,
		road: 13405.4575,
		acceleration: -0.485000000000001
	},
	{
		id: 1307,
		time: 1306,
		velocity: 7.94916666666667,
		power: -5866.89584398405,
		road: 13413.4859259259,
		acceleration: -0.931666666666666
	},
	{
		id: 1308,
		time: 1307,
		velocity: 6.04666666666667,
		power: -8065.68326955773,
		road: 13420.3540277778,
		acceleration: -1.38898148148148
	},
	{
		id: 1309,
		time: 1308,
		velocity: 4.525,
		power: -8146.27838623571,
		road: 13425.6458333333,
		acceleration: -1.76361111111111
	},
	{
		id: 1310,
		time: 1309,
		velocity: 2.65833333333333,
		power: -5238.63481465356,
		road: 13429.2153703704,
		acceleration: -1.68092592592593
	},
	{
		id: 1311,
		time: 1310,
		velocity: 1.00388888888889,
		power: -2581.83690712792,
		road: 13431.1902777778,
		acceleration: -1.50833333333333
	},
	{
		id: 1312,
		time: 1311,
		velocity: 0,
		power: -558.840686011106,
		road: 13431.967962963,
		acceleration: -0.886111111111111
	},
	{
		id: 1313,
		time: 1312,
		velocity: 0,
		power: -32.8271079597141,
		road: 13432.1352777778,
		acceleration: -0.33462962962963
	},
	{
		id: 1314,
		time: 1313,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1315,
		time: 1314,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1316,
		time: 1315,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1317,
		time: 1316,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1318,
		time: 1317,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1319,
		time: 1318,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1320,
		time: 1319,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1321,
		time: 1320,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1322,
		time: 1321,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1323,
		time: 1322,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1324,
		time: 1323,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1325,
		time: 1324,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1326,
		time: 1325,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1327,
		time: 1326,
		velocity: 0,
		power: 0,
		road: 13432.1352777778,
		acceleration: 0
	},
	{
		id: 1328,
		time: 1327,
		velocity: 0,
		power: 699.104056269506,
		road: 13432.7114814815,
		acceleration: 1.15240740740741
	},
	{
		id: 1329,
		time: 1328,
		velocity: 3.45722222222222,
		power: 2517.3804617967,
		road: 13434.5292592593,
		acceleration: 1.33074074074074
	},
	{
		id: 1330,
		time: 1329,
		velocity: 3.99222222222222,
		power: 5539.11524564048,
		road: 13437.8295833333,
		acceleration: 1.63435185185185
	},
	{
		id: 1331,
		time: 1330,
		velocity: 4.90305555555556,
		power: 1523.02137372358,
		road: 13442.0675462963,
		acceleration: 0.240925925925925
	},
	{
		id: 1332,
		time: 1331,
		velocity: 4.18,
		power: -347.131147078174,
		road: 13446.3141666667,
		acceleration: -0.223611111111111
	},
	{
		id: 1333,
		time: 1332,
		velocity: 3.32138888888889,
		power: -1978.64157221581,
		road: 13450.1064351852,
		acceleration: -0.685092592592592
	},
	{
		id: 1334,
		time: 1333,
		velocity: 2.84777777777778,
		power: -1179.81854917935,
		road: 13453.2946296296,
		acceleration: -0.523055555555556
	},
	{
		id: 1335,
		time: 1334,
		velocity: 2.61083333333333,
		power: 198.497216144011,
		road: 13456.1913425926,
		acceleration: -0.0599074074074073
	},
	{
		id: 1336,
		time: 1335,
		velocity: 3.14166666666667,
		power: 1650.96073029001,
		road: 13459.2738888889,
		acceleration: 0.431574074074074
	},
	{
		id: 1337,
		time: 1336,
		velocity: 4.1425,
		power: 3524.17551880523,
		road: 13463.0023611111,
		acceleration: 0.860277777777778
	},
	{
		id: 1338,
		time: 1337,
		velocity: 5.19166666666667,
		power: 3399.4125695917,
		road: 13467.4903240741,
		acceleration: 0.658703703703704
	},
	{
		id: 1339,
		time: 1338,
		velocity: 5.11777777777778,
		power: 2713.98912583214,
		road: 13472.5207407407,
		acceleration: 0.426203703703703
	},
	{
		id: 1340,
		time: 1339,
		velocity: 5.42111111111111,
		power: 1977.12802946829,
		road: 13477.8863425926,
		acceleration: 0.244166666666668
	},
	{
		id: 1341,
		time: 1340,
		velocity: 5.92416666666667,
		power: 4075.08368173483,
		road: 13483.6714351852,
		acceleration: 0.594814814814814
	},
	{
		id: 1342,
		time: 1341,
		velocity: 6.90222222222222,
		power: 5853.11405522324,
		road: 13490.1531944445,
		acceleration: 0.798518518518519
	},
	{
		id: 1343,
		time: 1342,
		velocity: 7.81666666666667,
		power: 10064.9663815114,
		road: 13497.659212963,
		acceleration: 1.25
	},
	{
		id: 1344,
		time: 1343,
		velocity: 9.67416666666667,
		power: 15426.2119733476,
		road: 13506.6088425926,
		acceleration: 1.63722222222222
	},
	{
		id: 1345,
		time: 1344,
		velocity: 11.8138888888889,
		power: 15231.3155118001,
		road: 13517.0480092593,
		acceleration: 1.34185185185185
	},
	{
		id: 1346,
		time: 1345,
		velocity: 11.8422222222222,
		power: 9541.28682873642,
		road: 13528.4944907407,
		acceleration: 0.672777777777778
	},
	{
		id: 1347,
		time: 1346,
		velocity: 11.6925,
		power: 593.136537688668,
		road: 13540.2015740741,
		acceleration: -0.151574074074073
	},
	{
		id: 1348,
		time: 1347,
		velocity: 11.3591666666667,
		power: -494.248998438552,
		road: 13551.7093055556,
		acceleration: -0.247129629629629
	},
	{
		id: 1349,
		time: 1348,
		velocity: 11.1008333333333,
		power: -57.5810122052105,
		road: 13562.9912037037,
		acceleration: -0.204537037037037
	},
	{
		id: 1350,
		time: 1349,
		velocity: 11.0788888888889,
		power: 1529.4985953089,
		road: 13574.1439814815,
		acceleration: -0.0537037037037038
	},
	{
		id: 1351,
		time: 1350,
		velocity: 11.1980555555556,
		power: 3646.38475989681,
		road: 13585.3416203704,
		acceleration: 0.143425925925925
	},
	{
		id: 1352,
		time: 1351,
		velocity: 11.5311111111111,
		power: 4210.25409667385,
		road: 13596.7051388889,
		acceleration: 0.188333333333333
	},
	{
		id: 1353,
		time: 1352,
		velocity: 11.6438888888889,
		power: 3847.67915772357,
		road: 13608.2365740741,
		acceleration: 0.147500000000001
	},
	{
		id: 1354,
		time: 1353,
		velocity: 11.6405555555556,
		power: 2823.11554649586,
		road: 13619.8671759259,
		acceleration: 0.0508333333333315
	},
	{
		id: 1355,
		time: 1354,
		velocity: 11.6836111111111,
		power: 3228.78820635542,
		road: 13631.5656018519,
		acceleration: 0.0848148148148162
	},
	{
		id: 1356,
		time: 1355,
		velocity: 11.8983333333333,
		power: 4634.55438317324,
		road: 13643.4083333333,
		acceleration: 0.203796296296296
	},
	{
		id: 1357,
		time: 1356,
		velocity: 12.2519444444444,
		power: 7636.66808984089,
		road: 13655.5764351852,
		acceleration: 0.446944444444444
	},
	{
		id: 1358,
		time: 1357,
		velocity: 13.0244444444444,
		power: 8836.57198155298,
		road: 13668.2252314815,
		acceleration: 0.514444444444443
	},
	{
		id: 1359,
		time: 1358,
		velocity: 13.4416666666667,
		power: 8739.2823178762,
		road: 13681.3671759259,
		acceleration: 0.471851851851852
	},
	{
		id: 1360,
		time: 1359,
		velocity: 13.6675,
		power: 8378.17955503049,
		road: 13694.9522685185,
		acceleration: 0.414444444444447
	},
	{
		id: 1361,
		time: 1360,
		velocity: 14.2677777777778,
		power: 7723.47789326742,
		road: 13708.9155092593,
		acceleration: 0.341851851851851
	},
	{
		id: 1362,
		time: 1361,
		velocity: 14.4672222222222,
		power: 8022.36123586064,
		road: 13723.2218055556,
		acceleration: 0.344259259259259
	},
	{
		id: 1363,
		time: 1362,
		velocity: 14.7002777777778,
		power: 5350.08470920776,
		road: 13737.7693518519,
		acceleration: 0.13824074074074
	},
	{
		id: 1364,
		time: 1363,
		velocity: 14.6825,
		power: 3161.72256721388,
		road: 13752.375462963,
		acceleration: -0.0211111111111109
	},
	{
		id: 1365,
		time: 1364,
		velocity: 14.4038888888889,
		power: -200.267795691387,
		road: 13766.8410648148,
		acceleration: -0.259907407407407
	},
	{
		id: 1366,
		time: 1365,
		velocity: 13.9205555555556,
		power: -805.790677239199,
		road: 13781.0265277778,
		acceleration: -0.30037037037037
	},
	{
		id: 1367,
		time: 1366,
		velocity: 13.7813888888889,
		power: -213.577374542966,
		road: 13794.9355092593,
		acceleration: -0.252592592592592
	},
	{
		id: 1368,
		time: 1367,
		velocity: 13.6461111111111,
		power: 5949.63898211606,
		road: 13808.8243981482,
		acceleration: 0.212407407407408
	},
	{
		id: 1369,
		time: 1368,
		velocity: 14.5577777777778,
		power: 6876.04089385281,
		road: 13822.9542592593,
		acceleration: 0.269537037037036
	},
	{
		id: 1370,
		time: 1369,
		velocity: 14.59,
		power: 7090.10249303362,
		road: 13837.3544444445,
		acceleration: 0.271111111111113
	},
	{
		id: 1371,
		time: 1370,
		velocity: 14.4594444444444,
		power: 2605.48377571914,
		road: 13851.8611574074,
		acceleration: -0.0580555555555566
	},
	{
		id: 1372,
		time: 1371,
		velocity: 14.3836111111111,
		power: 4890.10288302144,
		road: 13866.3917592593,
		acceleration: 0.105833333333333
	},
	{
		id: 1373,
		time: 1372,
		velocity: 14.9075,
		power: 6097.61766878339,
		road: 13881.068287037,
		acceleration: 0.186018518518519
	},
	{
		id: 1374,
		time: 1373,
		velocity: 15.0175,
		power: 52.6493983713497,
		road: 13895.7155092593,
		acceleration: -0.244629629629632
	},
	{
		id: 1375,
		time: 1374,
		velocity: 13.6497222222222,
		power: -10392.7545449449,
		road: 13909.7330555556,
		acceleration: -1.01472222222222
	},
	{
		id: 1376,
		time: 1375,
		velocity: 11.8633333333333,
		power: -12307.8553244079,
		road: 13922.6326388889,
		acceleration: -1.22120370370371
	},
	{
		id: 1377,
		time: 1376,
		velocity: 11.3538888888889,
		power: -6775.83668476594,
		road: 13934.519212963,
		acceleration: -0.804814814814813
	},
	{
		id: 1378,
		time: 1377,
		velocity: 11.2352777777778,
		power: -1352.6539025313,
		road: 13945.8408796296,
		acceleration: -0.325000000000001
	},
	{
		id: 1379,
		time: 1378,
		velocity: 10.8883333333333,
		power: -82.3992284232662,
		road: 13956.897962963,
		acceleration: -0.204166666666667
	},
	{
		id: 1380,
		time: 1379,
		velocity: 10.7413888888889,
		power: -17.3661992697973,
		road: 13967.7551851852,
		acceleration: -0.195555555555552
	},
	{
		id: 1381,
		time: 1380,
		velocity: 10.6486111111111,
		power: 336.128949857151,
		road: 13978.4352777778,
		acceleration: -0.158703703703704
	},
	{
		id: 1382,
		time: 1381,
		velocity: 10.4122222222222,
		power: 2533.09340121254,
		road: 13989.065462963,
		acceleration: 0.0588888888888874
	},
	{
		id: 1383,
		time: 1382,
		velocity: 10.9180555555556,
		power: -29.4007419989919,
		road: 13999.6284722222,
		acceleration: -0.193240740740741
	},
	{
		id: 1384,
		time: 1383,
		velocity: 10.0688888888889,
		power: -789.417946126087,
		road: 14009.9609722222,
		acceleration: -0.267777777777777
	},
	{
		id: 1385,
		time: 1384,
		velocity: 9.60888888888889,
		power: -3505.65947988372,
		road: 14019.8826851852,
		acceleration: -0.553796296296298
	},
	{
		id: 1386,
		time: 1385,
		velocity: 9.25666666666667,
		power: -2227.83639621192,
		road: 14029.3147222222,
		acceleration: -0.425555555555555
	},
	{
		id: 1387,
		time: 1386,
		velocity: 8.79222222222222,
		power: -2507.87380002453,
		road: 14038.3009722222,
		acceleration: -0.466018518518517
	},
	{
		id: 1388,
		time: 1387,
		velocity: 8.21083333333333,
		power: -3532.59308449283,
		road: 14046.750787037,
		acceleration: -0.606851851851854
	},
	{
		id: 1389,
		time: 1388,
		velocity: 7.43611111111111,
		power: -6429.26287765323,
		road: 14054.3738425926,
		acceleration: -1.04666666666667
	},
	{
		id: 1390,
		time: 1389,
		velocity: 5.65222222222222,
		power: -6987.11217069607,
		road: 14060.8288425926,
		acceleration: -1.28944444444444
	},
	{
		id: 1391,
		time: 1390,
		velocity: 4.3425,
		power: -7841.89553952638,
		road: 14065.7256018519,
		acceleration: -1.82703703703704
	},
	{
		id: 1392,
		time: 1391,
		velocity: 1.955,
		power: -5053.5749660303,
		road: 14068.7668055556,
		acceleration: -1.88407407407407
	},
	{
		id: 1393,
		time: 1392,
		velocity: 0,
		power: -1719.63855677246,
		road: 14070.1422222222,
		acceleration: -1.4475
	},
	{
		id: 1394,
		time: 1393,
		velocity: 0,
		power: -161.792713157895,
		road: 14070.4680555556,
		acceleration: -0.651666666666667
	},
	{
		id: 1395,
		time: 1394,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1396,
		time: 1395,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1397,
		time: 1396,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1398,
		time: 1397,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1399,
		time: 1398,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1400,
		time: 1399,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1401,
		time: 1400,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1402,
		time: 1401,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1403,
		time: 1402,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1404,
		time: 1403,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1405,
		time: 1404,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1406,
		time: 1405,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1407,
		time: 1406,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1408,
		time: 1407,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1409,
		time: 1408,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1410,
		time: 1409,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1411,
		time: 1410,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1412,
		time: 1411,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1413,
		time: 1412,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1414,
		time: 1413,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1415,
		time: 1414,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1416,
		time: 1415,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1417,
		time: 1416,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1418,
		time: 1417,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1419,
		time: 1418,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1420,
		time: 1419,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1421,
		time: 1420,
		velocity: 0,
		power: 0,
		road: 14070.4680555556,
		acceleration: 0
	},
	{
		id: 1422,
		time: 1421,
		velocity: 0,
		power: 415.868970389036,
		road: 14070.9056481482,
		acceleration: 0.875185185185185
	},
	{
		id: 1423,
		time: 1422,
		velocity: 2.62555555555556,
		power: 2873.28376543475,
		road: 14072.606712963,
		acceleration: 1.65175925925926
	},
	{
		id: 1424,
		time: 1423,
		velocity: 4.95527777777778,
		power: 6906.17458853575,
		road: 14076.1058333333,
		acceleration: 1.94435185185185
	},
	{
		id: 1425,
		time: 1424,
		velocity: 5.83305555555556,
		power: 6737.28275711462,
		road: 14081.2017592593,
		acceleration: 1.24925925925926
	},
	{
		id: 1426,
		time: 1425,
		velocity: 6.37333333333333,
		power: 8146.22557905524,
		road: 14087.5247685185,
		acceleration: 1.20490740740741
	},
	{
		id: 1427,
		time: 1426,
		velocity: 8.57,
		power: 12890.4992393807,
		road: 14095.246712963,
		acceleration: 1.59296296296296
	},
	{
		id: 1428,
		time: 1427,
		velocity: 10.6119444444444,
		power: 21736.8736791492,
		road: 14104.8616666667,
		acceleration: 2.19305555555555
	},
	{
		id: 1429,
		time: 1428,
		velocity: 12.9525,
		power: 20678.0951549352,
		road: 14116.4104166667,
		acceleration: 1.67453703703704
	},
	{
		id: 1430,
		time: 1429,
		velocity: 13.5936111111111,
		power: 16893.1879541051,
		road: 14129.368287037,
		acceleration: 1.1437037037037
	},
	{
		id: 1431,
		time: 1430,
		velocity: 14.0430555555556,
		power: 13838.0970094531,
		road: 14143.2996296296,
		acceleration: 0.80324074074074
	},
	{
		id: 1432,
		time: 1431,
		velocity: 15.3622222222222,
		power: 15939.0925620739,
		road: 14158.0719907407,
		acceleration: 0.878796296296297
	},
	{
		id: 1433,
		time: 1432,
		velocity: 16.23,
		power: 22212.5925161193,
		road: 14173.884212963,
		acceleration: 1.20092592592593
	},
	{
		id: 1434,
		time: 1433,
		velocity: 17.6458333333333,
		power: 18486.7042603168,
		road: 14190.7269444445,
		acceleration: 0.86009259259259
	},
	{
		id: 1435,
		time: 1434,
		velocity: 17.9425,
		power: 12124.1402470529,
		road: 14208.2121759259,
		acceleration: 0.424907407407407
	},
	{
		id: 1436,
		time: 1435,
		velocity: 17.5047222222222,
		power: 739.294490012822,
		road: 14225.7811574074,
		acceleration: -0.257407407407406
	},
	{
		id: 1437,
		time: 1436,
		velocity: 16.8736111111111,
		power: -3528.35733601833,
		road: 14242.9671759259,
		acceleration: -0.508518518518514
	},
	{
		id: 1438,
		time: 1437,
		velocity: 16.4169444444444,
		power: -9398.98644451503,
		road: 14259.4605555556,
		acceleration: -0.876759259259263
	},
	{
		id: 1439,
		time: 1438,
		velocity: 14.8744444444444,
		power: -8737.56886033571,
		road: 14275.0905092593,
		acceleration: -0.850092592592594
	},
	{
		id: 1440,
		time: 1439,
		velocity: 14.3233333333333,
		power: -11271.7757526565,
		road: 14289.7689814815,
		acceleration: -1.05287037037037
	},
	{
		id: 1441,
		time: 1440,
		velocity: 13.2583333333333,
		power: -7616.02764438433,
		road: 14303.5138425926,
		acceleration: -0.81435185185185
	},
	{
		id: 1442,
		time: 1441,
		velocity: 12.4313888888889,
		power: -9967.1238297258,
		road: 14316.3340740741,
		acceleration: -1.03490740740741
	},
	{
		id: 1443,
		time: 1442,
		velocity: 11.2186111111111,
		power: -12561.7972162183,
		road: 14327.9687962963,
		acceleration: -1.33611111111111
	},
	{
		id: 1444,
		time: 1443,
		velocity: 9.25,
		power: -13182.115618108,
		road: 14338.1638425926,
		acceleration: -1.54324074074074
	},
	{
		id: 1445,
		time: 1444,
		velocity: 7.80166666666667,
		power: -10649.823465332,
		road: 14346.8588888889,
		acceleration: -1.45675925925926
	},
	{
		id: 1446,
		time: 1445,
		velocity: 6.84833333333333,
		power: -6493.53383828808,
		road: 14354.2868055556,
		acceleration: -1.0775
	},
	{
		id: 1447,
		time: 1446,
		velocity: 6.0175,
		power: -5381.2208314223,
		road: 14360.6565277778,
		acceleration: -1.03888888888889
	},
	{
		id: 1448,
		time: 1447,
		velocity: 4.685,
		power: -5397.77062849063,
		road: 14365.8928703704,
		acceleration: -1.22787037037037
	},
	{
		id: 1449,
		time: 1448,
		velocity: 3.16472222222222,
		power: -5508.0547999729,
		road: 14369.6817592593,
		acceleration: -1.66703703703704
	},
	{
		id: 1450,
		time: 1449,
		velocity: 1.01638888888889,
		power: -2952.15433518562,
		road: 14371.8562962963,
		acceleration: -1.56166666666667
	},
	{
		id: 1451,
		time: 1450,
		velocity: 0,
		power: -761.005612785734,
		road: 14372.7225462963,
		acceleration: -1.05490740740741
	},
	{
		id: 1452,
		time: 1451,
		velocity: 0,
		power: -33.9045341293047,
		road: 14372.8919444444,
		acceleration: -0.338796296296296
	},
	{
		id: 1453,
		time: 1452,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1454,
		time: 1453,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1455,
		time: 1454,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1456,
		time: 1455,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1457,
		time: 1456,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1458,
		time: 1457,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1459,
		time: 1458,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1460,
		time: 1459,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1461,
		time: 1460,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1462,
		time: 1461,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1463,
		time: 1462,
		velocity: 0,
		power: 0,
		road: 14372.8919444444,
		acceleration: 0
	},
	{
		id: 1464,
		time: 1463,
		velocity: 0,
		power: 42.9936051144117,
		road: 14373.0140277778,
		acceleration: 0.244166666666667
	},
	{
		id: 1465,
		time: 1464,
		velocity: 0.7325,
		power: 949.102430034511,
		road: 14373.8174074074,
		acceleration: 1.11842592592593
	},
	{
		id: 1466,
		time: 1465,
		velocity: 3.35527777777778,
		power: 4245.86615884699,
		road: 14376.0965277778,
		acceleration: 1.83305555555556
	},
	{
		id: 1467,
		time: 1466,
		velocity: 5.49916666666667,
		power: 7654.87729423688,
		road: 14380.2046296296,
		acceleration: 1.82490740740741
	},
	{
		id: 1468,
		time: 1467,
		velocity: 6.20722222222222,
		power: 9572.90847448718,
		road: 14386.0180092593,
		acceleration: 1.58564814814815
	},
	{
		id: 1469,
		time: 1468,
		velocity: 8.11222222222222,
		power: 13422.714992099,
		road: 14393.4887962963,
		acceleration: 1.72916666666667
	},
	{
		id: 1470,
		time: 1469,
		velocity: 10.6866666666667,
		power: 20737.6016241215,
		road: 14402.8928240741,
		acceleration: 2.13731481481481
	},
	{
		id: 1471,
		time: 1470,
		velocity: 12.6191666666667,
		power: 19964.3882393517,
		road: 14414.1921296296,
		acceleration: 1.65324074074074
	},
	{
		id: 1472,
		time: 1471,
		velocity: 13.0719444444444,
		power: 17236.491111973,
		road: 14426.9182407407,
		acceleration: 1.20037037037037
	},
	{
		id: 1473,
		time: 1472,
		velocity: 14.2877777777778,
		power: 15613.0734596539,
		road: 14440.7194444444,
		acceleration: 0.949814814814815
	},
	{
		id: 1474,
		time: 1473,
		velocity: 15.4686111111111,
		power: 24774.4669087676,
		road: 14455.731712963,
		acceleration: 1.47231481481481
	},
	{
		id: 1475,
		time: 1474,
		velocity: 17.4888888888889,
		power: 27755.0306427285,
		road: 14472.2197685185,
		acceleration: 1.47925925925926
	},
	{
		id: 1476,
		time: 1475,
		velocity: 18.7255555555556,
		power: 26168.8070329557,
		road: 14490.0602777778,
		acceleration: 1.22564814814815
	},
	{
		id: 1477,
		time: 1476,
		velocity: 19.1455555555556,
		power: 17749.3872268356,
		road: 14508.8438888889,
		acceleration: 0.660555555555558
	},
	{
		id: 1478,
		time: 1477,
		velocity: 19.4705555555556,
		power: 17912.0818930082,
		road: 14528.2690740741,
		acceleration: 0.622592592592589
	},
	{
		id: 1479,
		time: 1478,
		velocity: 20.5933333333333,
		power: 21936.5523785672,
		road: 14548.3963888889,
		acceleration: 0.781666666666666
	},
	{
		id: 1480,
		time: 1479,
		velocity: 21.4905555555556,
		power: 21308.5521278882,
		road: 14569.2608796296,
		acceleration: 0.692685185185184
	},
	{
		id: 1481,
		time: 1480,
		velocity: 21.5486111111111,
		power: 10085.5246590465,
		road: 14590.527962963,
		acceleration: 0.112500000000004
	},
	{
		id: 1482,
		time: 1481,
		velocity: 20.9308333333333,
		power: 1510.21436848663,
		road: 14611.6986111111,
		acceleration: -0.305370370370369
	},
	{
		id: 1483,
		time: 1482,
		velocity: 20.5744444444444,
		power: -1638.45484384164,
		road: 14632.4900462963,
		acceleration: -0.453055555555558
	},
	{
		id: 1484,
		time: 1483,
		velocity: 20.1894444444444,
		power: -1602.21270860715,
		road: 14652.8336111111,
		acceleration: -0.442685185185187
	},
	{
		id: 1485,
		time: 1484,
		velocity: 19.6027777777778,
		power: -2097.71472716112,
		road: 14672.7256481482,
		acceleration: -0.46037037037037
	},
	{
		id: 1486,
		time: 1485,
		velocity: 19.1933333333333,
		power: -1824.32464272621,
		road: 14692.1683333333,
		acceleration: -0.438333333333333
	},
	{
		id: 1487,
		time: 1486,
		velocity: 18.8744444444444,
		power: -419.878367449867,
		road: 14711.2144444445,
		acceleration: -0.354814814814812
	},
	{
		id: 1488,
		time: 1487,
		velocity: 18.5383333333333,
		power: 1600.07791740049,
		road: 14729.9649537037,
		acceleration: -0.236388888888889
	},
	{
		id: 1489,
		time: 1488,
		velocity: 18.4841666666667,
		power: 2597.37542714527,
		road: 14748.5098148148,
		acceleration: -0.174907407407407
	},
	{
		id: 1490,
		time: 1489,
		velocity: 18.3497222222222,
		power: 1848.87675143401,
		road: 14766.86125,
		acceleration: -0.211944444444445
	},
	{
		id: 1491,
		time: 1490,
		velocity: 17.9025,
		power: -762.271073316497,
		road: 14784.9289814815,
		acceleration: -0.355462962962964
	},
	{
		id: 1492,
		time: 1491,
		velocity: 17.4177777777778,
		power: -871.518569938136,
		road: 14802.6411574074,
		acceleration: -0.355648148148148
	},
	{
		id: 1493,
		time: 1492,
		velocity: 17.2827777777778,
		power: 855.044842946959,
		road: 14820.052037037,
		acceleration: -0.246944444444445
	},
	{
		id: 1494,
		time: 1493,
		velocity: 17.1616666666667,
		power: 4692.2080151061,
		road: 14837.3334722222,
		acceleration: -0.0119444444444454
	},
	{
		id: 1495,
		time: 1494,
		velocity: 17.3819444444444,
		power: 5732.01295097679,
		road: 14854.6340277778,
		acceleration: 0.0501851851851853
	},
	{
		id: 1496,
		time: 1495,
		velocity: 17.4333333333333,
		power: 7294.40032774976,
		road: 14872.0298148148,
		acceleration: 0.140277777777779
	},
	{
		id: 1497,
		time: 1496,
		velocity: 17.5825,
		power: 8122.25552026388,
		road: 14889.5868981482,
		acceleration: 0.182314814814816
	},
	{
		id: 1498,
		time: 1497,
		velocity: 17.9288888888889,
		power: 8077.42244984903,
		road: 14907.320787037,
		acceleration: 0.171296296296294
	},
	{
		id: 1499,
		time: 1498,
		velocity: 17.9472222222222,
		power: 7857.93827816798,
		road: 14925.215787037,
		acceleration: 0.150925925925929
	},
	{
		id: 1500,
		time: 1499,
		velocity: 18.0352777777778,
		power: 6286.88989101206,
		road: 14943.21375,
		acceleration: 0.0549999999999997
	},
	{
		id: 1501,
		time: 1500,
		velocity: 18.0938888888889,
		power: 7388.55975121684,
		road: 14961.2968055556,
		acceleration: 0.115185185185183
	},
	{
		id: 1502,
		time: 1501,
		velocity: 18.2927777777778,
		power: 6426.85534505222,
		road: 14979.4655092593,
		acceleration: 0.056111111111111
	},
	{
		id: 1503,
		time: 1502,
		velocity: 18.2036111111111,
		power: 7037.83929632958,
		road: 14997.7063425926,
		acceleration: 0.0881481481481465
	},
	{
		id: 1504,
		time: 1503,
		velocity: 18.3583333333333,
		power: 7721.96032227929,
		road: 15016.0525462963,
		acceleration: 0.122592592592593
	},
	{
		id: 1505,
		time: 1504,
		velocity: 18.6605555555556,
		power: 10169.2774761969,
		road: 15034.5861111111,
		acceleration: 0.252129629629632
	},
	{
		id: 1506,
		time: 1505,
		velocity: 18.96,
		power: 14098.6440403801,
		road: 15053.4713425926,
		acceleration: 0.451203703703701
	},
	{
		id: 1507,
		time: 1506,
		velocity: 19.7119444444444,
		power: 13435.1920352424,
		road: 15072.7767592593,
		acceleration: 0.389166666666664
	},
	{
		id: 1508,
		time: 1507,
		velocity: 19.8280555555556,
		power: 10642.6436889209,
		road: 15092.387962963,
		acceleration: 0.22240740740741
	},
	{
		id: 1509,
		time: 1508,
		velocity: 19.6272222222222,
		power: 6367.25393688857,
		road: 15112.1056944445,
		acceleration: -0.00935185185185006
	},
	{
		id: 1510,
		time: 1509,
		velocity: 19.6838888888889,
		power: 5162.39808704025,
		road: 15131.7829166667,
		acceleration: -0.071666666666669
	},
	{
		id: 1511,
		time: 1510,
		velocity: 19.6130555555556,
		power: 6637.61979957605,
		road: 15151.4282407408,
		acceleration: 0.00787037037036953
	},
	{
		id: 1512,
		time: 1511,
		velocity: 19.6508333333333,
		power: 5794.44898789936,
		road: 15171.0593055556,
		acceleration: -0.0363888888888866
	},
	{
		id: 1513,
		time: 1512,
		velocity: 19.5747222222222,
		power: 6992.56553670078,
		road: 15190.6859259259,
		acceleration: 0.0274999999999999
	},
	{
		id: 1514,
		time: 1513,
		velocity: 19.6955555555556,
		power: 7152.71808198491,
		road: 15210.3436574074,
		acceleration: 0.0347222222222214
	},
	{
		id: 1515,
		time: 1514,
		velocity: 19.755,
		power: 6803.88274312796,
		road: 15230.0263425926,
		acceleration: 0.0151851851851852
	},
	{
		id: 1516,
		time: 1515,
		velocity: 19.6202777777778,
		power: 5930.46426027748,
		road: 15249.7012037037,
		acceleration: -0.0308333333333337
	},
	{
		id: 1517,
		time: 1516,
		velocity: 19.6030555555556,
		power: 6597.91997772466,
		road: 15269.3631944445,
		acceleration: 0.00509259259259309
	},
	{
		id: 1518,
		time: 1517,
		velocity: 19.7702777777778,
		power: 7567.56482202899,
		road: 15289.0553703704,
		acceleration: 0.0552777777777784
	},
	{
		id: 1519,
		time: 1518,
		velocity: 19.7861111111111,
		power: 5446.21253947203,
		road: 15308.7466203704,
		acceleration: -0.0571296296296282
	},
	{
		id: 1520,
		time: 1519,
		velocity: 19.4316666666667,
		power: 4343.7848309494,
		road: 15328.352962963,
		acceleration: -0.112685185185185
	},
	{
		id: 1521,
		time: 1520,
		velocity: 19.4322222222222,
		power: 4010.4484095419,
		road: 15347.8397222222,
		acceleration: -0.126481481481484
	},
	{
		id: 1522,
		time: 1521,
		velocity: 19.4066666666667,
		power: 2332.6578632038,
		road: 15367.1574537037,
		acceleration: -0.211574074074072
	},
	{
		id: 1523,
		time: 1522,
		velocity: 18.7969444444444,
		power: 2000.00325285698,
		road: 15386.2575925926,
		acceleration: -0.223611111111111
	},
	{
		id: 1524,
		time: 1523,
		velocity: 18.7613888888889,
		power: 4850.66513518303,
		road: 15405.2145833333,
		acceleration: -0.0626851851851882
	},
	{
		id: 1525,
		time: 1524,
		velocity: 19.2186111111111,
		power: 10885.2930917817,
		road: 15424.2724074074,
		acceleration: 0.264351851851853
	},
	{
		id: 1526,
		time: 1525,
		velocity: 19.59,
		power: 10686.2786512361,
		road: 15443.5825462963,
		acceleration: 0.240277777777781
	},
	{
		id: 1527,
		time: 1526,
		velocity: 19.4822222222222,
		power: 8238.1092466851,
		road: 15463.062962963,
		acceleration: 0.100277777777777
	},
	{
		id: 1528,
		time: 1527,
		velocity: 19.5194444444444,
		power: 5703.18516678106,
		road: 15482.5751388889,
		acceleration: -0.0367592592592594
	},
	{
		id: 1529,
		time: 1528,
		velocity: 19.4797222222222,
		power: 5763.55118087227,
		road: 15502.0528240741,
		acceleration: -0.0322222222222202
	},
	{
		id: 1530,
		time: 1529,
		velocity: 19.3855555555556,
		power: 5921.84412880445,
		road: 15521.5030555556,
		acceleration: -0.022685185185189
	},
	{
		id: 1531,
		time: 1530,
		velocity: 19.4513888888889,
		power: 5433.53962382147,
		road: 15540.9181481482,
		acceleration: -0.04759259259259
	},
	{
		id: 1532,
		time: 1531,
		velocity: 19.3369444444444,
		power: 7115.882852433,
		road: 15560.3309259259,
		acceleration: 0.0429629629629638
	},
	{
		id: 1533,
		time: 1532,
		velocity: 19.5144444444444,
		power: 6137.68335502539,
		road: 15579.7600462963,
		acceleration: -0.0102777777777803
	},
	{
		id: 1534,
		time: 1533,
		velocity: 19.4205555555556,
		power: 6223.30439940898,
		road: 15599.1813425926,
		acceleration: -0.00537037037036825
	},
	{
		id: 1535,
		time: 1534,
		velocity: 19.3208333333333,
		power: 6720.15412300651,
		road: 15618.610462963,
		acceleration: 0.0210185185185203
	},
	{
		id: 1536,
		time: 1535,
		velocity: 19.5775,
		power: 6834.64391827197,
		road: 15638.0631944445,
		acceleration: 0.026203703703704
	},
	{
		id: 1537,
		time: 1536,
		velocity: 19.4991666666667,
		power: 6948.10377050523,
		road: 15657.5445833333,
		acceleration: 0.0311111111111089
	},
	{
		id: 1538,
		time: 1537,
		velocity: 19.4141666666667,
		power: 6701.88958797475,
		road: 15677.05,
		acceleration: 0.0169444444444444
	},
	{
		id: 1539,
		time: 1538,
		velocity: 19.6283333333333,
		power: 5414.93812476212,
		road: 15696.5382407408,
		acceleration: -0.0512962962962966
	},
	{
		id: 1540,
		time: 1539,
		velocity: 19.3452777777778,
		power: 3621.15779443226,
		road: 15715.9287037037,
		acceleration: -0.144259259259261
	},
	{
		id: 1541,
		time: 1540,
		velocity: 18.9813888888889,
		power: -1815.12966132232,
		road: 15735.0309259259,
		acceleration: -0.432222222222222
	},
	{
		id: 1542,
		time: 1541,
		velocity: 18.3316666666667,
		power: -2116.01547598301,
		road: 15753.6959722222,
		acceleration: -0.442129629629626
	},
	{
		id: 1543,
		time: 1542,
		velocity: 18.0188888888889,
		power: -1665.51576324841,
		road: 15771.9348148148,
		acceleration: -0.410277777777779
	},
	{
		id: 1544,
		time: 1543,
		velocity: 17.7505555555556,
		power: 1069.12919151604,
		road: 15789.8456018519,
		acceleration: -0.245833333333337
	},
	{
		id: 1545,
		time: 1544,
		velocity: 17.5941666666667,
		power: 689.479519269311,
		road: 15807.5023148148,
		acceleration: -0.262314814814811
	},
	{
		id: 1546,
		time: 1545,
		velocity: 17.2319444444444,
		power: 2258.25426799165,
		road: 15824.9460648148,
		acceleration: -0.163611111111113
	},
	{
		id: 1547,
		time: 1546,
		velocity: 17.2597222222222,
		power: 3094.67590407105,
		road: 15842.2533333334,
		acceleration: -0.109351851851848
	},
	{
		id: 1548,
		time: 1547,
		velocity: 17.2661111111111,
		power: 7682.66166578684,
		road: 15859.5891203704,
		acceleration: 0.166388888888886
	},
	{
		id: 1549,
		time: 1548,
		velocity: 17.7311111111111,
		power: 12752.3255528562,
		road: 15877.2342592593,
		acceleration: 0.452314814814816
	},
	{
		id: 1550,
		time: 1549,
		velocity: 18.6166666666667,
		power: 17470.3057908682,
		road: 15895.4493055556,
		acceleration: 0.6875
	},
	{
		id: 1551,
		time: 1550,
		velocity: 19.3286111111111,
		power: 16783.8383803142,
		road: 15914.3088425926,
		acceleration: 0.601481481481482
	},
	{
		id: 1552,
		time: 1551,
		velocity: 19.5355555555556,
		power: 14282.8496047058,
		road: 15933.6844444445,
		acceleration: 0.430648148148151
	},
	{
		id: 1553,
		time: 1552,
		velocity: 19.9086111111111,
		power: 11096.3273336615,
		road: 15953.3960185185,
		acceleration: 0.241296296296294
	},
	{
		id: 1554,
		time: 1553,
		velocity: 20.0525,
		power: 16434.7781613211,
		road: 15973.477962963,
		acceleration: 0.499444444444443
	},
	{
		id: 1555,
		time: 1554,
		velocity: 21.0338888888889,
		power: 25144.3568554648,
		road: 15994.2555092593,
		acceleration: 0.891759259259256
	},
	{
		id: 1556,
		time: 1555,
		velocity: 22.5838888888889,
		power: 24626.5129168263,
		road: 16015.8773611111,
		acceleration: 0.796851851851855
	},
	{
		id: 1557,
		time: 1556,
		velocity: 22.4430555555556,
		power: 16855.1946541185,
		road: 16038.0905092593,
		acceleration: 0.385740740740744
	},
	{
		id: 1558,
		time: 1557,
		velocity: 22.1911111111111,
		power: 255.598743882295,
		road: 16060.3,
		acceleration: -0.393055555555559
	},
	{
		id: 1559,
		time: 1558,
		velocity: 21.4047222222222,
		power: -6097.33073033826,
		road: 16081.9703703704,
		acceleration: -0.685185185185183
	},
	{
		id: 1560,
		time: 1559,
		velocity: 20.3875,
		power: -15253.9222660127,
		road: 16102.7297685185,
		acceleration: -1.13675925925926
	},
	{
		id: 1561,
		time: 1560,
		velocity: 18.7808333333333,
		power: -13233.4482797122,
		road: 16122.3968981482,
		acceleration: -1.04777777777778
	},
	{
		id: 1562,
		time: 1561,
		velocity: 18.2613888888889,
		power: -11559.5544465309,
		road: 16141.0547685185,
		acceleration: -0.970740740740741
	},
	{
		id: 1563,
		time: 1562,
		velocity: 17.4752777777778,
		power: -7022.64992970955,
		road: 16158.8681481482,
		acceleration: -0.71824074074074
	},
	{
		id: 1564,
		time: 1563,
		velocity: 16.6261111111111,
		power: -8555.18612048805,
		road: 16175.9144444445,
		acceleration: -0.815925925925928
	},
	{
		id: 1565,
		time: 1564,
		velocity: 15.8136111111111,
		power: -10540.5272575608,
		road: 16192.0743055556,
		acceleration: -0.956944444444442
	},
	{
		id: 1566,
		time: 1565,
		velocity: 14.6044444444444,
		power: -9165.57561471096,
		road: 16207.3117592593,
		acceleration: -0.88787037037037
	},
	{
		id: 1567,
		time: 1566,
		velocity: 13.9625,
		power: -10583.3642221113,
		road: 16221.5961111111,
		acceleration: -1.01833333333333
	},
	{
		id: 1568,
		time: 1567,
		velocity: 12.7586111111111,
		power: -9126.0969781568,
		road: 16234.8982870371,
		acceleration: -0.946018518518519
	},
	{
		id: 1569,
		time: 1568,
		velocity: 11.7663888888889,
		power: -8917.22439478745,
		road: 16247.2421296296,
		acceleration: -0.970648148148149
	},
	{
		id: 1570,
		time: 1569,
		velocity: 11.0505555555556,
		power: -6754.19640230897,
		road: 16258.6906018519,
		acceleration: -0.820092592592591
	},
	{
		id: 1571,
		time: 1570,
		velocity: 10.2983333333333,
		power: -3713.964923475,
		road: 16269.4516203704,
		acceleration: -0.554814814814817
	},
	{
		id: 1572,
		time: 1571,
		velocity: 10.1019444444444,
		power: -5457.77468694859,
		road: 16279.5593518519,
		acceleration: -0.751759259259259
	},
	{
		id: 1573,
		time: 1572,
		velocity: 8.79527777777778,
		power: -3754.68135922776,
		road: 16288.9934722222,
		acceleration: -0.595462962962962
	},
	{
		id: 1574,
		time: 1573,
		velocity: 8.51194444444444,
		power: -7350.0761424592,
		road: 16297.5965277778,
		acceleration: -1.06666666666667
	},
	{
		id: 1575,
		time: 1574,
		velocity: 6.90194444444444,
		power: -6481.35697212947,
		road: 16305.1346296296,
		acceleration: -1.06324074074074
	},
	{
		id: 1576,
		time: 1575,
		velocity: 5.60555555555556,
		power: -5518.66252572524,
		road: 16311.6179166667,
		acceleration: -1.04638888888889
	},
	{
		id: 1577,
		time: 1576,
		velocity: 5.37277777777778,
		power: -2560.52737450624,
		road: 16317.2668518519,
		acceleration: -0.622314814814815
	},
	{
		id: 1578,
		time: 1577,
		velocity: 5.035,
		power: -772.112166470238,
		road: 16322.4550462963,
		acceleration: -0.299166666666665
	},
	{
		id: 1579,
		time: 1578,
		velocity: 4.70805555555556,
		power: -865.204944098317,
		road: 16327.3298611111,
		acceleration: -0.327592592592593
	},
	{
		id: 1580,
		time: 1579,
		velocity: 4.39,
		power: 1052.95944114491,
		road: 16332.0873148148,
		acceleration: 0.0928703703703704
	},
	{
		id: 1581,
		time: 1580,
		velocity: 5.31361111111111,
		power: 3603.05897527662,
		road: 16337.1916203704,
		acceleration: 0.600833333333333
	},
	{
		id: 1582,
		time: 1581,
		velocity: 6.51055555555556,
		power: 7108.76146949534,
		road: 16343.1500925926,
		acceleration: 1.1075
	},
	{
		id: 1583,
		time: 1582,
		velocity: 7.7125,
		power: 11535.0262494598,
		road: 16350.4177777778,
		acceleration: 1.51092592592592
	},
	{
		id: 1584,
		time: 1583,
		velocity: 9.84638888888889,
		power: 10594.2201237851,
		road: 16359.0043518519,
		acceleration: 1.12685185185185
	},
	{
		id: 1585,
		time: 1584,
		velocity: 9.89111111111111,
		power: 7373.43254477262,
		road: 16368.4740740741,
		acceleration: 0.639444444444445
	},
	{
		id: 1586,
		time: 1585,
		velocity: 9.63083333333333,
		power: 605.672290980552,
		road: 16378.2057870371,
		acceleration: -0.115462962962962
	},
	{
		id: 1587,
		time: 1586,
		velocity: 9.5,
		power: 1823.2221345698,
		road: 16387.8884722222,
		acceleration: 0.0174074074074078
	},
	{
		id: 1588,
		time: 1587,
		velocity: 9.94333333333333,
		power: 590.555163249102,
		road: 16397.5221759259,
		acceleration: -0.115370370370371
	},
	{
		id: 1589,
		time: 1588,
		velocity: 9.28472222222222,
		power: 1084.24968476005,
		road: 16407.0684259259,
		acceleration: -0.0595370370370354
	},
	{
		id: 1590,
		time: 1589,
		velocity: 9.32138888888889,
		power: -503.268523948533,
		road: 16416.4681944445,
		acceleration: -0.233425925925928
	},
	{
		id: 1591,
		time: 1590,
		velocity: 9.24305555555556,
		power: 625.699578821118,
		road: 16425.6991203704,
		acceleration: -0.104259259259258
	},
	{
		id: 1592,
		time: 1591,
		velocity: 8.97194444444444,
		power: 401.602992957529,
		road: 16434.8139351852,
		acceleration: -0.127962962962965
	},
	{
		id: 1593,
		time: 1592,
		velocity: 8.9375,
		power: 116.961665500165,
		road: 16443.7852314815,
		acceleration: -0.159074074074072
	},
	{
		id: 1594,
		time: 1593,
		velocity: 8.76583333333333,
		power: 927.958230944448,
		road: 16452.6461574074,
		acceleration: -0.0616666666666674
	},
	{
		id: 1595,
		time: 1594,
		velocity: 8.78694444444444,
		power: 855.938646291314,
		road: 16461.4418518519,
		acceleration: -0.0687962962962967
	},
	{
		id: 1596,
		time: 1595,
		velocity: 8.73111111111111,
		power: 669.576432463241,
		road: 16470.1583796296,
		acceleration: -0.0895370370370365
	},
	{
		id: 1597,
		time: 1596,
		velocity: 8.49722222222222,
		power: -1459.44105879676,
		road: 16478.6558796296,
		acceleration: -0.348518518518517
	},
	{
		id: 1598,
		time: 1597,
		velocity: 7.74138888888889,
		power: -4084.256033072,
		road: 16486.6283333333,
		acceleration: -0.701574074074075
	},
	{
		id: 1599,
		time: 1598,
		velocity: 6.62638888888889,
		power: -6942.5079060749,
		road: 16493.6527777778,
		acceleration: -1.19444444444444
	},
	{
		id: 1600,
		time: 1599,
		velocity: 4.91388888888889,
		power: -5772.80021354601,
		road: 16499.4860648148,
		acceleration: -1.18787037037037
	},
	{
		id: 1601,
		time: 1600,
		velocity: 4.17777777777778,
		power: -3356.93515366244,
		road: 16504.2871759259,
		acceleration: -0.876481481481481
	},
	{
		id: 1602,
		time: 1601,
		velocity: 3.99694444444444,
		power: -1031.25144887678,
		road: 16508.4510648148,
		acceleration: -0.397962962962963
	},
	{
		id: 1603,
		time: 1602,
		velocity: 3.72,
		power: 80.9302356530979,
		road: 16512.3588888889,
		acceleration: -0.114166666666667
	},
	{
		id: 1604,
		time: 1603,
		velocity: 3.83527777777778,
		power: -496.526551213836,
		road: 16516.0716203704,
		acceleration: -0.276018518518518
	},
	{
		id: 1605,
		time: 1604,
		velocity: 3.16888888888889,
		power: -1156.39573388677,
		road: 16519.3962962963,
		acceleration: -0.500092592592592
	},
	{
		id: 1606,
		time: 1605,
		velocity: 2.21972222222222,
		power: -2651.12387598314,
		road: 16521.831712963,
		acceleration: -1.27842592592593
	},
	{
		id: 1607,
		time: 1606,
		velocity: 0,
		power: -1115.36474844396,
		road: 16523.0997685185,
		acceleration: -1.0562962962963
	},
	{
		id: 1608,
		time: 1607,
		velocity: 0,
		power: -214.627537833008,
		road: 16523.4697222222,
		acceleration: -0.739907407407407
	},
	{
		id: 1609,
		time: 1608,
		velocity: 0,
		power: 0,
		road: 16523.4697222222,
		acceleration: 0
	},
	{
		id: 1610,
		time: 1609,
		velocity: 0,
		power: 0,
		road: 16523.4697222222,
		acceleration: 0
	},
	{
		id: 1611,
		time: 1610,
		velocity: 0,
		power: 182.259864288138,
		road: 16523.7495833333,
		acceleration: 0.559722222222222
	},
	{
		id: 1612,
		time: 1611,
		velocity: 1.67916666666667,
		power: 1109.67328267087,
		road: 16524.8015740741,
		acceleration: 0.984537037037037
	},
	{
		id: 1613,
		time: 1612,
		velocity: 2.95361111111111,
		power: 3924.90448967686,
		road: 16527.1581944445,
		acceleration: 1.62472222222222
	},
	{
		id: 1614,
		time: 1613,
		velocity: 4.87416666666667,
		power: 5906.43025575515,
		road: 16531.0568518519,
		acceleration: 1.45935185185185
	},
	{
		id: 1615,
		time: 1614,
		velocity: 6.05722222222222,
		power: 9138.81823616127,
		road: 16536.4968518519,
		acceleration: 1.62333333333333
	},
	{
		id: 1616,
		time: 1615,
		velocity: 7.82361111111111,
		power: 12696.3897959527,
		road: 16543.60875,
		acceleration: 1.72046296296296
	},
	{
		id: 1617,
		time: 1616,
		velocity: 10.0355555555556,
		power: 13758.1024052595,
		road: 16552.3245833333,
		acceleration: 1.48740740740741
	},
	{
		id: 1618,
		time: 1617,
		velocity: 10.5194444444444,
		power: 15633.9512833885,
		road: 16562.4975,
		acceleration: 1.42675925925926
	},
	{
		id: 1619,
		time: 1618,
		velocity: 12.1038888888889,
		power: 11630.6023260015,
		road: 16573.8224074074,
		acceleration: 0.877222222222223
	},
	{
		id: 1620,
		time: 1619,
		velocity: 12.6672222222222,
		power: 13656.1667648176,
		road: 16586.064537037,
		acceleration: 0.957222222222221
	},
	{
		id: 1621,
		time: 1620,
		velocity: 13.3911111111111,
		power: 10746.107289192,
		road: 16599.1052777778,
		acceleration: 0.640000000000001
	},
	{
		id: 1622,
		time: 1621,
		velocity: 14.0238888888889,
		power: 12153.4885939632,
		road: 16612.8135648148,
		acceleration: 0.695092592592593
	},
	{
		id: 1623,
		time: 1622,
		velocity: 14.7525,
		power: 8074.91701921379,
		road: 16627.0456018519,
		acceleration: 0.35240740740741
	},
	{
		id: 1624,
		time: 1623,
		velocity: 14.4483333333333,
		power: 5849.14334159657,
		road: 16641.5420833333,
		acceleration: 0.176481481481481
	},
	{
		id: 1625,
		time: 1624,
		velocity: 14.5533333333333,
		power: 1106.62613734468,
		road: 16656.04375,
		acceleration: -0.166111111111112
	},
	{
		id: 1626,
		time: 1625,
		velocity: 14.2541666666667,
		power: 1750.09659742493,
		road: 16670.4043055556,
		acceleration: -0.116111111111111
	},
	{
		id: 1627,
		time: 1626,
		velocity: 14.1,
		power: 1506.4123642642,
		road: 16684.6413425926,
		acceleration: -0.130925925925927
	},
	{
		id: 1628,
		time: 1627,
		velocity: 14.1605555555556,
		power: 4099.89278140483,
		road: 16698.8434722222,
		acceleration: 0.0611111111111118
	},
	{
		id: 1629,
		time: 1628,
		velocity: 14.4375,
		power: 7713.69274134365,
		road: 16713.2346296296,
		acceleration: 0.316944444444445
	},
	{
		id: 1630,
		time: 1629,
		velocity: 15.0508333333333,
		power: 9401.72407673495,
		road: 16727.9925462963,
		acceleration: 0.416574074074074
	},
	{
		id: 1631,
		time: 1630,
		velocity: 15.4102777777778,
		power: 9897.26786081138,
		road: 16743.17125,
		acceleration: 0.425000000000001
	},
	{
		id: 1632,
		time: 1631,
		velocity: 15.7125,
		power: 11871.2145791943,
		road: 16758.8262962963,
		acceleration: 0.527685185185183
	},
	{
		id: 1633,
		time: 1632,
		velocity: 16.6338888888889,
		power: 15416.8981637079,
		road: 16775.1020833333,
		acceleration: 0.713796296296298
	},
	{
		id: 1634,
		time: 1633,
		velocity: 17.5516666666667,
		power: 27041.2761428208,
		road: 16792.4033796296,
		acceleration: 1.33722222222222
	},
	{
		id: 1635,
		time: 1634,
		velocity: 19.7241666666667,
		power: 30997.4076805227,
		road: 16811.077962963,
		acceleration: 1.40935185185185
	},
	{
		id: 1636,
		time: 1635,
		velocity: 20.8619444444444,
		power: 39401.7508513242,
		road: 16831.2950462963,
		acceleration: 1.67564814814815
	},
	{
		id: 1637,
		time: 1636,
		velocity: 22.5786111111111,
		power: 32105.0751386165,
		road: 16852.9281018519,
		acceleration: 1.15629629629629
	},
	{
		id: 1638,
		time: 1637,
		velocity: 23.1930555555556,
		power: 22245.5314758281,
		road: 16875.4475925926,
		acceleration: 0.616574074074077
	},
	{
		id: 1639,
		time: 1638,
		velocity: 22.7116666666667,
		power: 8956.71986438213,
		road: 16898.2695833333,
		acceleration: -0.0115740740740726
	},
	{
		id: 1640,
		time: 1639,
		velocity: 22.5438888888889,
		power: 2201.61645944865,
		road: 16920.9282407407,
		acceleration: -0.315092592592595
	},
	{
		id: 1641,
		time: 1640,
		velocity: 22.2477777777778,
		power: 2463.6402161668,
		road: 16943.2824537037,
		acceleration: -0.2937962962963
	},
	{
		id: 1642,
		time: 1641,
		velocity: 21.8302777777778,
		power: 791.011163163303,
		road: 16965.3082407407,
		acceleration: -0.363055555555555
	},
	{
		id: 1643,
		time: 1642,
		velocity: 21.4547222222222,
		power: -1857.02290185873,
		road: 16986.9125925926,
		acceleration: -0.479814814814812
	},
	{
		id: 1644,
		time: 1643,
		velocity: 20.8083333333333,
		power: -1195.5900987938,
		road: 17008.057962963,
		acceleration: -0.438148148148148
	},
	{
		id: 1645,
		time: 1644,
		velocity: 20.5158333333333,
		power: -4891.36659131923,
		road: 17028.6770833333,
		acceleration: -0.61435185185185
	},
	{
		id: 1646,
		time: 1645,
		velocity: 19.6116666666667,
		power: -3767.87832245028,
		road: 17048.7141203704,
		acceleration: -0.549814814814816
	},
	{
		id: 1647,
		time: 1646,
		velocity: 19.1588888888889,
		power: -5306.40388053133,
		road: 17068.1635648148,
		acceleration: -0.625370370370369
	},
	{
		id: 1648,
		time: 1647,
		velocity: 18.6397222222222,
		power: -5277.65675099236,
		road: 17086.9903703704,
		acceleration: -0.61990740740741
	},
	{
		id: 1649,
		time: 1648,
		velocity: 17.7519444444444,
		power: -5911.54284160434,
		road: 17105.1805092593,
		acceleration: -0.653425925925927
	},
	{
		id: 1650,
		time: 1649,
		velocity: 17.1986111111111,
		power: -6770.25835687881,
		road: 17122.6917592593,
		acceleration: -0.70435185185185
	},
	{
		id: 1651,
		time: 1650,
		velocity: 16.5266666666667,
		power: -3256.83365477054,
		road: 17139.6058333333,
		acceleration: -0.489999999999998
	},
	{
		id: 1652,
		time: 1651,
		velocity: 16.2819444444444,
		power: -4190.96342536521,
		road: 17156.0017592593,
		acceleration: -0.546296296296294
	},
	{
		id: 1653,
		time: 1652,
		velocity: 15.5597222222222,
		power: -6384.12279828817,
		road: 17171.7789814815,
		acceleration: -0.691111111111114
	},
	{
		id: 1654,
		time: 1653,
		velocity: 14.4533333333333,
		power: -598.768328119258,
		road: 17187.0606018519,
		acceleration: -0.300092592592593
	},
	{
		id: 1655,
		time: 1654,
		velocity: 15.3816666666667,
		power: 6164.02365870375,
		road: 17202.2751851852,
		acceleration: 0.166018518518518
	},
	{
		id: 1656,
		time: 1655,
		velocity: 16.0577777777778,
		power: 13880.1508575399,
		road: 17217.9047222222,
		acceleration: 0.663888888888891
	},
	{
		id: 1657,
		time: 1656,
		velocity: 16.445,
		power: 10133.909669665,
		road: 17234.057037037,
		acceleration: 0.381666666666662
	},
	{
		id: 1658,
		time: 1657,
		velocity: 16.5266666666667,
		power: 6346.151718866,
		road: 17250.4628703704,
		acceleration: 0.125370370370373
	},
	{
		id: 1659,
		time: 1658,
		velocity: 16.4338888888889,
		power: 4108.81879721541,
		road: 17266.9218518519,
		acceleration: -0.0190740740740765
	},
	{
		id: 1660,
		time: 1659,
		velocity: 16.3877777777778,
		power: 3746.2697644458,
		road: 17283.3507407407,
		acceleration: -0.0411111111111104
	},
	{
		id: 1661,
		time: 1660,
		velocity: 16.4033333333333,
		power: 4294.89034121954,
		road: 17299.7563888889,
		acceleration: -0.00537037037036825
	},
	{
		id: 1662,
		time: 1661,
		velocity: 16.4177777777778,
		power: 6333.30904591062,
		road: 17316.2203703704,
		acceleration: 0.122037037037039
	},
	{
		id: 1663,
		time: 1662,
		velocity: 16.7538888888889,
		power: 7175.57346305902,
		road: 17332.8297685185,
		acceleration: 0.168796296296293
	},
	{
		id: 1664,
		time: 1663,
		velocity: 16.9097222222222,
		power: 7503.89195431655,
		road: 17349.6141666667,
		acceleration: 0.181203703703702
	},
	{
		id: 1665,
		time: 1664,
		velocity: 16.9613888888889,
		power: 6043.35650297837,
		road: 17366.5315277778,
		acceleration: 0.0847222222222257
	},
	{
		id: 1666,
		time: 1665,
		velocity: 17.0080555555556,
		power: 5361.81703933322,
		road: 17383.5113425926,
		acceleration: 0.0401851851851838
	},
	{
		id: 1667,
		time: 1666,
		velocity: 17.0302777777778,
		power: 6244.44810116237,
		road: 17400.5571296296,
		acceleration: 0.0917592592592591
	},
	{
		id: 1668,
		time: 1667,
		velocity: 17.2366666666667,
		power: 6339.87310321692,
		road: 17417.6956481482,
		acceleration: 0.093703703703703
	},
	{
		id: 1669,
		time: 1668,
		velocity: 17.2891666666667,
		power: 4913.79824061295,
		road: 17434.8834722222,
		acceleration: 0.00490740740740847
	},
	{
		id: 1670,
		time: 1669,
		velocity: 17.045,
		power: 4980.37813726263,
		road: 17452.0781018519,
		acceleration: 0.00870370370370566
	},
	{
		id: 1671,
		time: 1670,
		velocity: 17.2627777777778,
		power: 4574.64826746517,
		road: 17469.2691666667,
		acceleration: -0.0158333333333331
	},
	{
		id: 1672,
		time: 1671,
		velocity: 17.2416666666667,
		power: 5943.80234279279,
		road: 17486.4855092593,
		acceleration: 0.0663888888888877
	},
	{
		id: 1673,
		time: 1672,
		velocity: 17.2441666666667,
		power: 6405.64930641259,
		road: 17503.7806018519,
		acceleration: 0.0911111111111111
	},
	{
		id: 1674,
		time: 1673,
		velocity: 17.5361111111111,
		power: 5511.46004299121,
		road: 17521.1385648148,
		acceleration: 0.0346296296296309
	},
	{
		id: 1675,
		time: 1674,
		velocity: 17.3455555555556,
		power: 4552.13988112704,
		road: 17538.5021759259,
		acceleration: -0.0233333333333334
	},
	{
		id: 1676,
		time: 1675,
		velocity: 17.1741666666667,
		power: 2842.42179530048,
		road: 17555.7920833333,
		acceleration: -0.124074074074073
	},
	{
		id: 1677,
		time: 1676,
		velocity: 17.1638888888889,
		power: 3793.12235730381,
		road: 17572.9881944445,
		acceleration: -0.0635185185185208
	},
	{
		id: 1678,
		time: 1677,
		velocity: 17.155,
		power: 4432.18785401902,
		road: 17590.1409722222,
		acceleration: -0.0231481481481453
	},
	{
		id: 1679,
		time: 1678,
		velocity: 17.1047222222222,
		power: 3943.67399205974,
		road: 17607.2563425926,
		acceleration: -0.0516666666666694
	},
	{
		id: 1680,
		time: 1679,
		velocity: 17.0088888888889,
		power: 2903.07350227396,
		road: 17624.2894907408,
		acceleration: -0.112777777777779
	},
	{
		id: 1681,
		time: 1680,
		velocity: 16.8166666666667,
		power: 3495.86503811413,
		road: 17641.229537037,
		acceleration: -0.0734259259259247
	},
	{
		id: 1682,
		time: 1681,
		velocity: 16.8844444444444,
		power: 4629.05283508803,
		road: 17658.1318055556,
		acceleration: -0.00212962962962848
	},
	{
		id: 1683,
		time: 1682,
		velocity: 17.0025,
		power: 5094.5045005983,
		road: 17675.0461111111,
		acceleration: 0.0262037037037004
	},
	{
		id: 1684,
		time: 1683,
		velocity: 16.8952777777778,
		power: 4768.31114775362,
		road: 17691.97625,
		acceleration: 0.00546296296296589
	},
	{
		id: 1685,
		time: 1684,
		velocity: 16.9008333333333,
		power: 3436.73189534489,
		road: 17708.8712962963,
		acceleration: -0.0756481481481472
	},
	{
		id: 1686,
		time: 1685,
		velocity: 16.7755555555556,
		power: 4459.35573617083,
		road: 17725.7231018519,
		acceleration: -0.0108333333333306
	},
	{
		id: 1687,
		time: 1686,
		velocity: 16.8627777777778,
		power: 4207.34390424517,
		road: 17742.5565740741,
		acceleration: -0.0258333333333347
	},
	{
		id: 1688,
		time: 1687,
		velocity: 16.8233333333333,
		power: 4073.36165065091,
		road: 17759.3605555556,
		acceleration: -0.0331481481481468
	},
	{
		id: 1689,
		time: 1688,
		velocity: 16.6761111111111,
		power: 2443.72400780263,
		road: 17776.0818518519,
		acceleration: -0.132222222222225
	},
	{
		id: 1690,
		time: 1689,
		velocity: 16.4661111111111,
		power: 1968.19369714601,
		road: 17792.657962963,
		acceleration: -0.15814814814815
	},
	{
		id: 1691,
		time: 1690,
		velocity: 16.3488888888889,
		power: 2202.22436015271,
		road: 17809.0853240741,
		acceleration: -0.139351851851849
	},
	{
		id: 1692,
		time: 1691,
		velocity: 16.2580555555556,
		power: 1734.53541183346,
		road: 17825.3603703704,
		acceleration: -0.165277777777774
	},
	{
		id: 1693,
		time: 1692,
		velocity: 15.9702777777778,
		power: 1094.90575158052,
		road: 17841.4516666667,
		acceleration: -0.202222222222227
	},
	{
		id: 1694,
		time: 1693,
		velocity: 15.7422222222222,
		power: 199.544533804916,
		road: 17857.3137962963,
		acceleration: -0.256111111111112
	},
	{
		id: 1695,
		time: 1694,
		velocity: 15.4897222222222,
		power: 1005.44746817227,
		road: 17872.9488888889,
		acceleration: -0.197962962962961
	},
	{
		id: 1696,
		time: 1695,
		velocity: 15.3763888888889,
		power: -387.023092607102,
		road: 17888.3413425926,
		acceleration: -0.287314814814815
	},
	{
		id: 1697,
		time: 1696,
		velocity: 14.8802777777778,
		power: 1159.84323817171,
		road: 17903.5016666667,
		acceleration: -0.176944444444445
	},
	{
		id: 1698,
		time: 1697,
		velocity: 14.9588888888889,
		power: 4143.91564569964,
		road: 17918.5893518519,
		acceleration: 0.0316666666666663
	},
	{
		id: 1699,
		time: 1698,
		velocity: 15.4713888888889,
		power: 7178.60042354331,
		road: 17933.8106018518,
		acceleration: 0.235462962962963
	},
	{
		id: 1700,
		time: 1699,
		velocity: 15.5866666666667,
		power: 8696.35932988275,
		road: 17949.3116203704,
		acceleration: 0.324074074074073
	},
	{
		id: 1701,
		time: 1700,
		velocity: 15.9311111111111,
		power: 6614.85942235762,
		road: 17965.0607407407,
		acceleration: 0.17212962962963
	},
	{
		id: 1702,
		time: 1701,
		velocity: 15.9877777777778,
		power: 6501.06803830598,
		road: 17980.9744907407,
		acceleration: 0.15712962962963
	},
	{
		id: 1703,
		time: 1702,
		velocity: 16.0580555555556,
		power: 4801.66068113275,
		road: 17996.9876388889,
		acceleration: 0.0416666666666661
	},
	{
		id: 1704,
		time: 1703,
		velocity: 16.0561111111111,
		power: 4481.38728323018,
		road: 18013.0314351852,
		acceleration: 0.0196296296296303
	},
	{
		id: 1705,
		time: 1704,
		velocity: 16.0466666666667,
		power: 2140.48622062536,
		road: 18029.0193055556,
		acceleration: -0.131481481481481
	},
	{
		id: 1706,
		time: 1705,
		velocity: 15.6636111111111,
		power: 1551.42881263585,
		road: 18044.8582407407,
		acceleration: -0.166388888888887
	},
	{
		id: 1707,
		time: 1706,
		velocity: 15.5569444444444,
		power: -955.703565083333,
		road: 18060.4496759259,
		acceleration: -0.32861111111111
	},
	{
		id: 1708,
		time: 1707,
		velocity: 15.0608333333333,
		power: -80.3836982126184,
		road: 18075.7443981481,
		acceleration: -0.264814814814816
	},
	{
		id: 1709,
		time: 1708,
		velocity: 14.8691666666667,
		power: -698.180773634661,
		road: 18090.7551388889,
		acceleration: -0.303148148148148
	},
	{
		id: 1710,
		time: 1709,
		velocity: 14.6475,
		power: 1280.23064912145,
		road: 18105.5343518518,
		acceleration: -0.159907407407408
	},
	{
		id: 1711,
		time: 1710,
		velocity: 14.5811111111111,
		power: 3315.0942839919,
		road: 18120.2271296296,
		acceleration: -0.0129629629629626
	},
	{
		id: 1712,
		time: 1711,
		velocity: 14.8302777777778,
		power: 5846.59376645344,
		road: 18134.9954166667,
		acceleration: 0.16398148148148
	},
	{
		id: 1713,
		time: 1712,
		velocity: 15.1394444444444,
		power: 6597.87094143436,
		road: 18149.9498148148,
		acceleration: 0.208240740740742
	},
	{
		id: 1714,
		time: 1713,
		velocity: 15.2058333333333,
		power: 6684.03035782676,
		road: 18165.1105092593,
		acceleration: 0.204351851851852
	},
	{
		id: 1715,
		time: 1714,
		velocity: 15.4433333333333,
		power: 4838.63340900582,
		road: 18180.4091666667,
		acceleration: 0.0715740740740731
	},
	{
		id: 1716,
		time: 1715,
		velocity: 15.3541666666667,
		power: 6140.88780759289,
		road: 18195.8213888889,
		acceleration: 0.155555555555557
	},
	{
		id: 1717,
		time: 1716,
		velocity: 15.6725,
		power: 5610.7597801418,
		road: 18211.3683333333,
		acceleration: 0.113888888888889
	},
	{
		id: 1718,
		time: 1717,
		velocity: 15.785,
		power: 7016.28948978818,
		road: 18227.0726851852,
		acceleration: 0.200925925925924
	},
	{
		id: 1719,
		time: 1718,
		velocity: 15.9569444444444,
		power: 5058.28131281667,
		road: 18242.9101388889,
		acceleration: 0.06527777777778
	},
	{
		id: 1720,
		time: 1719,
		velocity: 15.8683333333333,
		power: 4814.46516489333,
		road: 18258.80375,
		acceleration: 0.0470370370370361
	},
	{
		id: 1721,
		time: 1720,
		velocity: 15.9261111111111,
		power: 4120.90189273798,
		road: 18274.7211574074,
		acceleration: 0.000555555555555642
	},
	{
		id: 1722,
		time: 1721,
		velocity: 15.9586111111111,
		power: 4238.79617064926,
		road: 18290.6429166667,
		acceleration: 0.00814814814815001
	},
	{
		id: 1723,
		time: 1722,
		velocity: 15.8927777777778,
		power: 3478.00869463324,
		road: 18306.5481018518,
		acceleration: -0.0412962962962986
	},
	{
		id: 1724,
		time: 1723,
		velocity: 15.8022222222222,
		power: 3354.97760999212,
		road: 18322.4086574074,
		acceleration: -0.0479629629629628
	},
	{
		id: 1725,
		time: 1724,
		velocity: 15.8147222222222,
		power: 3966.82596522785,
		road: 18338.2418981481,
		acceleration: -0.00666666666666771
	},
	{
		id: 1726,
		time: 1725,
		velocity: 15.8727777777778,
		power: 5210.63235265178,
		road: 18354.1088888889,
		acceleration: 0.0741666666666685
	},
	{
		id: 1727,
		time: 1726,
		velocity: 16.0247222222222,
		power: 5511.76546388192,
		road: 18370.058287037,
		acceleration: 0.0906481481481478
	},
	{
		id: 1728,
		time: 1727,
		velocity: 16.0866666666667,
		power: 4713.50621851379,
		road: 18386.0709722222,
		acceleration: 0.0359259259259268
	},
	{
		id: 1729,
		time: 1728,
		velocity: 15.9805555555556,
		power: 2897.03570471789,
		road: 18402.0606018518,
		acceleration: -0.082037037037038
	},
	{
		id: 1730,
		time: 1729,
		velocity: 15.7786111111111,
		power: 1972.36101716405,
		road: 18417.9393981481,
		acceleration: -0.13962962962963
	},
	{
		id: 1731,
		time: 1730,
		velocity: 15.6677777777778,
		power: 1786.26977685413,
		road: 18433.6742592592,
		acceleration: -0.148240740740741
	},
	{
		id: 1732,
		time: 1731,
		velocity: 15.5358333333333,
		power: 3044.13897459889,
		road: 18449.3043055555,
		acceleration: -0.0613888888888887
	},
	{
		id: 1733,
		time: 1732,
		velocity: 15.5944444444444,
		power: 3246.97015693302,
		road: 18464.8806018518,
		acceleration: -0.0461111111111112
	},
	{
		id: 1734,
		time: 1733,
		velocity: 15.5294444444444,
		power: 5637.64605049114,
		road: 18480.4903703704,
		acceleration: 0.113055555555555
	},
	{
		id: 1735,
		time: 1734,
		velocity: 15.875,
		power: 5528.42948296184,
		road: 18496.2073148148,
		acceleration: 0.101296296296297
	},
	{
		id: 1736,
		time: 1735,
		velocity: 15.8983333333333,
		power: 6996.20388680233,
		road: 18512.0709259259,
		acceleration: 0.192037037037037
	},
	{
		id: 1737,
		time: 1736,
		velocity: 16.1055555555556,
		power: 4566.66710580881,
		road: 18528.0444444444,
		acceleration: 0.0277777777777786
	},
	{
		id: 1738,
		time: 1737,
		velocity: 15.9583333333333,
		power: 3637.76841092462,
		road: 18544.0153703704,
		acceleration: -0.0329629629629657
	},
	{
		id: 1739,
		time: 1738,
		velocity: 15.7994444444444,
		power: 1164.1364841623,
		road: 18559.8736111111,
		acceleration: -0.192407407407405
	},
	{
		id: 1740,
		time: 1739,
		velocity: 15.5283333333333,
		power: -258.597927678201,
		road: 18575.4944907407,
		acceleration: -0.282314814814816
	},
	{
		id: 1741,
		time: 1740,
		velocity: 15.1113888888889,
		power: -1048.50326659011,
		road: 18590.8085648148,
		acceleration: -0.331296296296296
	},
	{
		id: 1742,
		time: 1741,
		velocity: 14.8055555555556,
		power: 147.648260519556,
		road: 18605.8347685185,
		acceleration: -0.244444444444445
	},
	{
		id: 1743,
		time: 1742,
		velocity: 14.795,
		power: 1942.99969021822,
		road: 18620.6813888889,
		acceleration: -0.114722222222222
	},
	{
		id: 1744,
		time: 1743,
		velocity: 14.7672222222222,
		power: 3638.66102608035,
		road: 18635.4739814815,
		acceleration: 0.00666666666666771
	},
	{
		id: 1745,
		time: 1744,
		velocity: 14.8255555555556,
		power: 3475.50221702089,
		road: 18650.2674537037,
		acceleration: -0.00490740740740847
	},
	{
		id: 1746,
		time: 1745,
		velocity: 14.7802777777778,
		power: 2783.99395783449,
		road: 18665.0319907407,
		acceleration: -0.0529629629629635
	},
	{
		id: 1747,
		time: 1746,
		velocity: 14.6083333333333,
		power: 1506.55794115455,
		road: 18679.6994444444,
		acceleration: -0.141203703703702
	},
	{
		id: 1748,
		time: 1747,
		velocity: 14.4019444444444,
		power: -912.943897807617,
		road: 18694.1406944444,
		acceleration: -0.311203703703704
	},
	{
		id: 1749,
		time: 1748,
		velocity: 13.8466666666667,
		power: -1487.58866588655,
		road: 18708.2512962963,
		acceleration: -0.350092592592594
	},
	{
		id: 1750,
		time: 1749,
		velocity: 13.5580555555556,
		power: -1423.74825834543,
		road: 18722.0155555556,
		acceleration: -0.342592592592592
	},
	{
		id: 1751,
		time: 1750,
		velocity: 13.3741666666667,
		power: -1429.13129362169,
		road: 18735.4382407407,
		acceleration: -0.340555555555557
	},
	{
		id: 1752,
		time: 1751,
		velocity: 12.825,
		power: -3529.49388736046,
		road: 18748.437037037,
		acceleration: -0.50722222222222
	},
	{
		id: 1753,
		time: 1752,
		velocity: 12.0363888888889,
		power: -8665.71626177617,
		road: 18760.7058333333,
		acceleration: -0.952777777777778
	},
	{
		id: 1754,
		time: 1753,
		velocity: 10.5158333333333,
		power: -7831.37493314252,
		road: 18772.0358333333,
		acceleration: -0.924814814814816
	},
	{
		id: 1755,
		time: 1754,
		velocity: 10.0505555555556,
		power: -8050.48315719565,
		road: 18782.4018981481,
		acceleration: -1.00305555555556
	},
	{
		id: 1756,
		time: 1755,
		velocity: 9.02722222222222,
		power: -2924.86883833174,
		road: 18792.0169907407,
		acceleration: -0.498888888888889
	},
	{
		id: 1757,
		time: 1756,
		velocity: 9.01916666666667,
		power: -4782.10782424387,
		road: 18801.0170833333,
		acceleration: -0.731111111111112
	},
	{
		id: 1758,
		time: 1757,
		velocity: 7.85722222222222,
		power: -3182.81548587511,
		road: 18809.3680555556,
		acceleration: -0.56712962962963
	},
	{
		id: 1759,
		time: 1758,
		velocity: 7.32583333333333,
		power: -4370.23867887254,
		road: 18817.0563888889,
		acceleration: -0.758148148148147
	},
	{
		id: 1760,
		time: 1759,
		velocity: 6.74472222222222,
		power: -4529.96702306,
		road: 18823.9427314815,
		acceleration: -0.845833333333334
	},
	{
		id: 1761,
		time: 1760,
		velocity: 5.31972222222222,
		power: -4199.94967768183,
		road: 18829.9654166667,
		acceleration: -0.88148148148148
	},
	{
		id: 1762,
		time: 1761,
		velocity: 4.68138888888889,
		power: -3706.29274637618,
		road: 18835.0960648148,
		acceleration: -0.902592592592593
	},
	{
		id: 1763,
		time: 1762,
		velocity: 4.03694444444444,
		power: -1174.86436218027,
		road: 18839.5677777778,
		acceleration: -0.415277777777778
	},
	{
		id: 1764,
		time: 1763,
		velocity: 4.07388888888889,
		power: -695.989619661749,
		road: 18843.6741666667,
		acceleration: -0.315370370370371
	},
	{
		id: 1765,
		time: 1764,
		velocity: 3.73527777777778,
		power: -8.78624543958505,
		road: 18847.55375,
		acceleration: -0.138240740740741
	},
	{
		id: 1766,
		time: 1765,
		velocity: 3.62222222222222,
		power: -203.841698480358,
		road: 18851.2677314815,
		acceleration: -0.192962962962963
	},
	{
		id: 1767,
		time: 1766,
		velocity: 3.495,
		power: 575.874298834805,
		road: 18854.9012962963,
		acceleration: 0.0321296296296296
	},
	{
		id: 1768,
		time: 1767,
		velocity: 3.83166666666667,
		power: 1859.80428592622,
		road: 18858.7383333333,
		acceleration: 0.374814814814815
	},
	{
		id: 1769,
		time: 1768,
		velocity: 4.74666666666667,
		power: 3181.94514736103,
		road: 18863.0796296296,
		acceleration: 0.633703703703703
	},
	{
		id: 1770,
		time: 1769,
		velocity: 5.39611111111111,
		power: 5400.91870778391,
		road: 18868.219537037,
		acceleration: 0.963518518518518
	},
	{
		id: 1771,
		time: 1770,
		velocity: 6.72222222222222,
		power: 7027.53002333349,
		road: 18874.3679166667,
		acceleration: 1.05342592592593
	},
	{
		id: 1772,
		time: 1771,
		velocity: 7.90694444444444,
		power: 9428.31285460317,
		road: 18881.6452314815,
		acceleration: 1.20444444444444
	},
	{
		id: 1773,
		time: 1772,
		velocity: 9.00944444444444,
		power: 8940.49136461509,
		road: 18890.0031944444,
		acceleration: 0.956851851851853
	},
	{
		id: 1774,
		time: 1773,
		velocity: 9.59277777777778,
		power: 7556.52287567396,
		road: 18899.184212963,
		acceleration: 0.689259259259259
	},
	{
		id: 1775,
		time: 1774,
		velocity: 9.97472222222222,
		power: 4060.87484108157,
		road: 18908.8406018518,
		acceleration: 0.261481481481482
	},
	{
		id: 1776,
		time: 1775,
		velocity: 9.79388888888889,
		power: 1395.73077454531,
		road: 18918.6120833333,
		acceleration: -0.031296296296297
	},
	{
		id: 1777,
		time: 1776,
		velocity: 9.49888888888889,
		power: -1616.35553343469,
		road: 18928.1897685185,
		acceleration: -0.356296296296296
	},
	{
		id: 1778,
		time: 1777,
		velocity: 8.90583333333333,
		power: -4108.2438056121,
		road: 18937.2646759259,
		acceleration: -0.64925925925926
	},
	{
		id: 1779,
		time: 1778,
		velocity: 7.84611111111111,
		power: -4828.06129152109,
		road: 18945.628287037,
		acceleration: -0.773333333333333
	},
	{
		id: 1780,
		time: 1779,
		velocity: 7.17888888888889,
		power: -3724.7893404042,
		road: 18953.2688888889,
		acceleration: -0.672685185185186
	},
	{
		id: 1781,
		time: 1780,
		velocity: 6.88777777777778,
		power: -2071.54152754272,
		road: 18960.3413888889,
		acceleration: -0.463518518518518
	},
	{
		id: 1782,
		time: 1781,
		velocity: 6.45555555555555,
		power: -447.221963269394,
		road: 18967.0707407407,
		acceleration: -0.222777777777778
	},
	{
		id: 1783,
		time: 1782,
		velocity: 6.51055555555556,
		power: 1036.86313597199,
		road: 18973.6949537037,
		acceleration: 0.0124999999999993
	},
	{
		id: 1784,
		time: 1783,
		velocity: 6.92527777777778,
		power: 2415.44479942973,
		road: 18980.4372685185,
		acceleration: 0.223703703703705
	},
	{
		id: 1785,
		time: 1784,
		velocity: 7.12666666666667,
		power: 3839.24209098929,
		road: 18987.4994444444,
		acceleration: 0.416018518518517
	},
	{
		id: 1786,
		time: 1785,
		velocity: 7.75861111111111,
		power: 5056.48997341782,
		road: 18995.0421759259,
		acceleration: 0.545092592592593
	},
	{
		id: 1787,
		time: 1786,
		velocity: 8.56055555555556,
		power: 5972.35081245412,
		road: 19003.1615740741,
		acceleration: 0.608240740740741
	},
	{
		id: 1788,
		time: 1787,
		velocity: 8.95138888888889,
		power: 5292.29751082541,
		road: 19011.8212037037,
		acceleration: 0.472222222222221
	},
	{
		id: 1789,
		time: 1788,
		velocity: 9.17527777777778,
		power: 3575.44587499588,
		road: 19020.8385185185,
		acceleration: 0.243148148148149
	},
	{
		id: 1790,
		time: 1789,
		velocity: 9.29,
		power: 3567.82175074607,
		road: 19030.0919907407,
		acceleration: 0.229166666666668
	},
	{
		id: 1791,
		time: 1790,
		velocity: 9.63888888888889,
		power: 4675.27350079429,
		road: 19039.6280555556,
		acceleration: 0.336018518518516
	},
	{
		id: 1792,
		time: 1791,
		velocity: 10.1833333333333,
		power: 6899.63757040036,
		road: 19049.6033333333,
		acceleration: 0.542407407407406
	},
	{
		id: 1793,
		time: 1792,
		velocity: 10.9172222222222,
		power: 6891.51373785219,
		road: 19060.0994907407,
		acceleration: 0.499351851851852
	},
	{
		id: 1794,
		time: 1793,
		velocity: 11.1369444444444,
		power: 4340.81285831484,
		road: 19070.9580555556,
		acceleration: 0.225462962962965
	},
	{
		id: 1795,
		time: 1794,
		velocity: 10.8597222222222,
		power: 1261.55048094776,
		road: 19081.8924537037,
		acceleration: -0.0737962962962957
	},
	{
		id: 1796,
		time: 1795,
		velocity: 10.6958333333333,
		power: 2005.49314165205,
		road: 19092.7893055556,
		acceleration: -0.00129629629629591
	},
	{
		id: 1797,
		time: 1796,
		velocity: 11.1330555555556,
		power: 5027.24721630059,
		road: 19103.8263425926,
		acceleration: 0.281666666666666
	},
	{
		id: 1798,
		time: 1797,
		velocity: 11.7047222222222,
		power: 5744.41206245286,
		road: 19115.1697685185,
		acceleration: 0.331111111111111
	},
	{
		id: 1799,
		time: 1798,
		velocity: 11.6891666666667,
		power: 2871.82656284955,
		road: 19126.7080092593,
		acceleration: 0.0585185185185182
	},
	{
		id: 1800,
		time: 1799,
		velocity: 11.3086111111111,
		power: 1216.20737637974,
		road: 19138.2297222222,
		acceleration: -0.0915740740740745
	},
	{
		id: 1801,
		time: 1800,
		velocity: 11.43,
		power: 3726.22682257164,
		road: 19149.7736111111,
		acceleration: 0.135925925925926
	},
	{
		id: 1802,
		time: 1801,
		velocity: 12.0969444444444,
		power: 7506.44866864424,
		road: 19161.614537037,
		acceleration: 0.458148148148148
	},
	{
		id: 1803,
		time: 1802,
		velocity: 12.6830555555556,
		power: 9181.57997291602,
		road: 19173.9673148148,
		acceleration: 0.565555555555555
	},
	{
		id: 1804,
		time: 1803,
		velocity: 13.1266666666667,
		power: 7320.55477706496,
		road: 19186.7918518519,
		acceleration: 0.377962962962963
	},
	{
		id: 1805,
		time: 1804,
		velocity: 13.2308333333333,
		power: 4759.96886883155,
		road: 19199.8837962963,
		acceleration: 0.156851851851853
	},
	{
		id: 1806,
		time: 1805,
		velocity: 13.1536111111111,
		power: 1297.4740282725,
		road: 19212.9938425926,
		acceleration: -0.120648148148149
	},
	{
		id: 1807,
		time: 1806,
		velocity: 12.7647222222222,
		power: -137.324099929667,
		road: 19225.9271296296,
		acceleration: -0.232870370370371
	},
	{
		id: 1808,
		time: 1807,
		velocity: 12.5322222222222,
		power: -1166.35708812413,
		road: 19238.5868055556,
		acceleration: -0.314351851851853
	},
	{
		id: 1809,
		time: 1808,
		velocity: 12.2105555555556,
		power: 56.0873218560732,
		road: 19250.9846296296,
		acceleration: -0.209351851851849
	},
	{
		id: 1810,
		time: 1809,
		velocity: 12.1366666666667,
		power: 770.265921188837,
		road: 19263.205,
		acceleration: -0.145555555555555
	},
	{
		id: 1811,
		time: 1810,
		velocity: 12.0955555555556,
		power: 3947.01854572075,
		road: 19275.41625,
		acceleration: 0.127314814814813
	},
	{
		id: 1812,
		time: 1811,
		velocity: 12.5925,
		power: 4744.33539589343,
		road: 19287.7853240741,
		acceleration: 0.188333333333333
	},
	{
		id: 1813,
		time: 1812,
		velocity: 12.7016666666667,
		power: 4441.7565586909,
		road: 19300.3261111111,
		acceleration: 0.155092592592593
	},
	{
		id: 1814,
		time: 1813,
		velocity: 12.5608333333333,
		power: 1048.02672816992,
		road: 19312.8800462963,
		acceleration: -0.128796296296295
	},
	{
		id: 1815,
		time: 1814,
		velocity: 12.2061111111111,
		power: 295.399529668257,
		road: 19325.2750462963,
		acceleration: -0.189074074074075
	},
	{
		id: 1816,
		time: 1815,
		velocity: 12.1344444444444,
		power: 1864.35236790188,
		road: 19337.5489351852,
		acceleration: -0.0531481481481482
	},
	{
		id: 1817,
		time: 1816,
		velocity: 12.4013888888889,
		power: 4931.23747527239,
		road: 19349.8987962963,
		acceleration: 0.205092592592592
	},
	{
		id: 1818,
		time: 1817,
		velocity: 12.8213888888889,
		power: 5532.88281927714,
		road: 19362.4734722222,
		acceleration: 0.244537037037036
	},
	{
		id: 1819,
		time: 1818,
		velocity: 12.8680555555556,
		power: 6013.20600007041,
		road: 19375.305787037,
		acceleration: 0.270740740740742
	},
	{
		id: 1820,
		time: 1819,
		velocity: 13.2136111111111,
		power: 4781.91240103729,
		road: 19388.35375,
		acceleration: 0.160555555555558
	},
	{
		id: 1821,
		time: 1820,
		velocity: 13.3030555555556,
		power: 4835.36993150781,
		road: 19401.5608796296,
		acceleration: 0.157777777777778
	},
	{
		id: 1822,
		time: 1821,
		velocity: 13.3413888888889,
		power: 4753.56265979773,
		road: 19414.9192592593,
		acceleration: 0.144722222222221
	},
	{
		id: 1823,
		time: 1822,
		velocity: 13.6477777777778,
		power: 5924.82342653375,
		road: 19428.4636574074,
		acceleration: 0.227314814814815
	},
	{
		id: 1824,
		time: 1823,
		velocity: 13.985,
		power: 6477.00643022704,
		road: 19442.2504166667,
		acceleration: 0.257407407407408
	},
	{
		id: 1825,
		time: 1824,
		velocity: 14.1136111111111,
		power: 4726.43784102972,
		road: 19456.224212963,
		acceleration: 0.116666666666667
	},
	{
		id: 1826,
		time: 1825,
		velocity: 13.9977777777778,
		power: 3225.52049477736,
		road: 19470.2574537037,
		acceleration: 0.00222222222222257
	},
	{
		id: 1827,
		time: 1826,
		velocity: 13.9916666666667,
		power: 1940.24625682577,
		road: 19484.2455555556,
		acceleration: -0.0925000000000011
	},
	{
		id: 1828,
		time: 1827,
		velocity: 13.8361111111111,
		power: 1464.2298877146,
		road: 19498.1246296296,
		acceleration: -0.125555555555556
	},
	{
		id: 1829,
		time: 1828,
		velocity: 13.6211111111111,
		power: -112.076395226916,
		road: 19511.8200462963,
		acceleration: -0.241759259259258
	},
	{
		id: 1830,
		time: 1829,
		velocity: 13.2663888888889,
		power: -1360.57267610424,
		road: 19525.227037037,
		acceleration: -0.335092592592593
	},
	{
		id: 1831,
		time: 1830,
		velocity: 12.8308333333333,
		power: -2458.10293425729,
		road: 19538.2560648148,
		acceleration: -0.420833333333333
	},
	{
		id: 1832,
		time: 1831,
		velocity: 12.3586111111111,
		power: -2110.46546069916,
		road: 19550.8784259259,
		acceleration: -0.3925
	},
	{
		id: 1833,
		time: 1832,
		velocity: 12.0888888888889,
		power: -353.031202523642,
		road: 19563.1831018519,
		acceleration: -0.242870370370371
	},
	{
		id: 1834,
		time: 1833,
		velocity: 12.1022222222222,
		power: 1362.72073188589,
		road: 19575.3199537037,
		acceleration: -0.0927777777777798
	},
	{
		id: 1835,
		time: 1834,
		velocity: 12.0802777777778,
		power: 1955.70504403214,
		road: 19587.3905555556,
		acceleration: -0.0397222222222204
	},
	{
		id: 1836,
		time: 1835,
		velocity: 11.9697222222222,
		power: 1421.49622652593,
		road: 19599.3989814815,
		acceleration: -0.0846296296296298
	},
	{
		id: 1837,
		time: 1836,
		velocity: 11.8483333333333,
		power: 738.27343326059,
		road: 19611.2940277778,
		acceleration: -0.142129629629629
	},
	{
		id: 1838,
		time: 1837,
		velocity: 11.6538888888889,
		power: 895.781239522754,
		road: 19623.0552314815,
		acceleration: -0.125555555555556
	},
	{
		id: 1839,
		time: 1838,
		velocity: 11.5930555555556,
		power: 1721.4475585189,
		road: 19634.7288425926,
		acceleration: -0.0496296296296297
	},
	{
		id: 1840,
		time: 1839,
		velocity: 11.6994444444444,
		power: 2089.08975274586,
		road: 19646.3698148148,
		acceleration: -0.0156481481481503
	},
	{
		id: 1841,
		time: 1840,
		velocity: 11.6069444444444,
		power: 3056.31859066883,
		road: 19658.0381944444,
		acceleration: 0.0704629629629636
	},
	{
		id: 1842,
		time: 1841,
		velocity: 11.8044444444444,
		power: 2669.98516746039,
		road: 19669.7587962963,
		acceleration: 0.0339814814814812
	},
	{
		id: 1843,
		time: 1842,
		velocity: 11.8013888888889,
		power: 569.6440482807,
		road: 19681.4199537037,
		acceleration: -0.152870370370371
	},
	{
		id: 1844,
		time: 1843,
		velocity: 11.1483333333333,
		power: -1396.10380198658,
		road: 19692.8400925926,
		acceleration: -0.329166666666666
	},
	{
		id: 1845,
		time: 1844,
		velocity: 10.8169444444444,
		power: -2943.38157632064,
		road: 19703.8575925926,
		acceleration: -0.476111111111109
	},
	{
		id: 1846,
		time: 1845,
		velocity: 10.3730555555556,
		power: -2033.99359088229,
		road: 19714.4409259259,
		acceleration: -0.392222222222223
	},
	{
		id: 1847,
		time: 1846,
		velocity: 9.97166666666667,
		power: -3466.68050793722,
		road: 19724.5557407407,
		acceleration: -0.544814814814814
	},
	{
		id: 1848,
		time: 1847,
		velocity: 9.1825,
		power: -4692.01959198346,
		road: 19734.0495833333,
		acceleration: -0.697129629629631
	},
	{
		id: 1849,
		time: 1848,
		velocity: 8.28166666666667,
		power: -9611.11929023792,
		road: 19742.5144907407,
		acceleration: -1.36074074074074
	},
	{
		id: 1850,
		time: 1849,
		velocity: 5.88944444444444,
		power: -11636.9116465997,
		road: 19749.3235185185,
		acceleration: -1.95101851851852
	},
	{
		id: 1851,
		time: 1850,
		velocity: 3.32944444444444,
		power: -4593.39490329798,
		road: 19754.6298611111,
		acceleration: -1.05435185185185
	},
	{
		id: 1852,
		time: 1851,
		velocity: 5.11861111111111,
		power: 3788.58574696076,
		road: 19759.7289814815,
		acceleration: 0.639907407407407
	},
	{
		id: 1853,
		time: 1852,
		velocity: 7.80916666666667,
		power: 13530.1602805644,
		road: 19766.1759259259,
		acceleration: 2.05574074074074
	},
	{
		id: 1854,
		time: 1853,
		velocity: 9.49666666666667,
		power: 13225.9928019525,
		road: 19774.4113888889,
		acceleration: 1.5212962962963
	},
	{
		id: 1855,
		time: 1854,
		velocity: 9.6825,
		power: 9252.26000394257,
		road: 19783.8341666667,
		acceleration: 0.853333333333334
	},
	{
		id: 1856,
		time: 1855,
		velocity: 10.3691666666667,
		power: 8193.75043819937,
		road: 19794.0130555556,
		acceleration: 0.658888888888889
	},
	{
		id: 1857,
		time: 1856,
		velocity: 11.4733333333333,
		power: 15657.1285010434,
		road: 19805.159212963,
		acceleration: 1.27564814814815
	},
	{
		id: 1858,
		time: 1857,
		velocity: 13.5094444444444,
		power: 21250.4017779429,
		road: 19817.72125,
		acceleration: 1.55611111111111
	},
	{
		id: 1859,
		time: 1858,
		velocity: 15.0375,
		power: 21722.3450064398,
		road: 19831.7525462963,
		acceleration: 1.38240740740741
	},
	{
		id: 1860,
		time: 1859,
		velocity: 15.6205555555556,
		power: 6129.53204963406,
		road: 19846.5660185185,
		acceleration: 0.181944444444442
	},
	{
		id: 1861,
		time: 1860,
		velocity: 14.0552777777778,
		power: -9821.92958910262,
		road: 19860.9916203704,
		acceleration: -0.957685185185184
	},
	{
		id: 1862,
		time: 1861,
		velocity: 12.1644444444444,
		power: -15857.7348777844,
		road: 19874.1962962963,
		acceleration: -1.48416666666667
	},
	{
		id: 1863,
		time: 1862,
		velocity: 11.1680555555556,
		power: -10121.6306210633,
		road: 19886.1097222222,
		acceleration: -1.09833333333333
	},
	{
		id: 1864,
		time: 1863,
		velocity: 10.7602777777778,
		power: -3598.50413732135,
		road: 19897.205462963,
		acceleration: -0.537037037037038
	},
	{
		id: 1865,
		time: 1864,
		velocity: 10.5533333333333,
		power: 1241.10836726943,
		road: 19907.9964814815,
		acceleration: -0.0724074074074093
	},
	{
		id: 1866,
		time: 1865,
		velocity: 10.9508333333333,
		power: 4568.11836448598,
		road: 19918.8744907408,
		acceleration: 0.246388888888889
	},
	{
		id: 1867,
		time: 1866,
		velocity: 11.4994444444444,
		power: 7427.76417638116,
		road: 19930.1225925926,
		acceleration: 0.493796296296296
	},
	{
		id: 1868,
		time: 1867,
		velocity: 12.0347222222222,
		power: 7700.97147309251,
		road: 19941.8590277778,
		acceleration: 0.482870370370371
	},
	{
		id: 1869,
		time: 1868,
		velocity: 12.3994444444444,
		power: 4820.55720677573,
		road: 19953.9411574074,
		acceleration: 0.208518518518517
	},
	{
		id: 1870,
		time: 1869,
		velocity: 12.125,
		power: 1969.14465478014,
		road: 19966.1069444445,
		acceleration: -0.0412037037037027
	},
	{
		id: 1871,
		time: 1870,
		velocity: 11.9111111111111,
		power: -497.989032185165,
		road: 19978.1259722222,
		acceleration: -0.252314814814815
	},
	{
		id: 1872,
		time: 1871,
		velocity: 11.6425,
		power: -238.084779884978,
		road: 19989.9054166667,
		acceleration: -0.226851851851851
	},
	{
		id: 1873,
		time: 1872,
		velocity: 11.4444444444444,
		power: -1701.14487181228,
		road: 20001.3928703704,
		acceleration: -0.357129629629631
	},
	{
		id: 1874,
		time: 1873,
		velocity: 10.8397222222222,
		power: -1114.7483437753,
		road: 20012.5505555556,
		acceleration: -0.302407407407406
	},
	{
		id: 1875,
		time: 1874,
		velocity: 10.7352777777778,
		power: -649.373579005623,
		road: 20023.4286574074,
		acceleration: -0.256759259259258
	},
	{
		id: 1876,
		time: 1875,
		velocity: 10.6741666666667,
		power: 322.295674541279,
		road: 20034.0984259259,
		acceleration: -0.15990740740741
	},
	{
		id: 1877,
		time: 1876,
		velocity: 10.36,
		power: -1253.07634919486,
		road: 20044.5308333333,
		acceleration: -0.314814814814815
	},
	{
		id: 1878,
		time: 1877,
		velocity: 9.79083333333333,
		power: -3551.601488877,
		road: 20054.5275,
		acceleration: -0.556666666666665
	},
	{
		id: 1879,
		time: 1878,
		velocity: 9.00416666666667,
		power: -4856.65876946393,
		road: 20063.8849537037,
		acceleration: -0.72175925925926
	},
	{
		id: 1880,
		time: 1879,
		velocity: 8.19472222222222,
		power: -5741.37049240119,
		road: 20072.4448611111,
		acceleration: -0.873333333333333
	},
	{
		id: 1881,
		time: 1880,
		velocity: 7.17083333333333,
		power: -4124.43784204094,
		road: 20080.2082407408,
		acceleration: -0.719722222222223
	},
	{
		id: 1882,
		time: 1881,
		velocity: 6.845,
		power: -1684.53496431517,
		road: 20087.4105092593,
		acceleration: -0.4025
	},
	{
		id: 1883,
		time: 1882,
		velocity: 6.98722222222222,
		power: -1733.73363719303,
		road: 20094.2006018519,
		acceleration: -0.421851851851851
	},
	{
		id: 1884,
		time: 1883,
		velocity: 5.90527777777778,
		power: -601.12671366338,
		road: 20100.6553703704,
		acceleration: -0.248796296296296
	},
	{
		id: 1885,
		time: 1884,
		velocity: 6.09861111111111,
		power: 1304.70613128382,
		road: 20107.0184722222,
		acceleration: 0.0654629629629628
	},
	{
		id: 1886,
		time: 1885,
		velocity: 7.18361111111111,
		power: 7627.00281600747,
		road: 20113.9182407408,
		acceleration: 1.00787037037037
	},
	{
		id: 1887,
		time: 1886,
		velocity: 8.92888888888889,
		power: 11019.4519330156,
		road: 20121.9598148148,
		acceleration: 1.27574074074074
	},
	{
		id: 1888,
		time: 1887,
		velocity: 9.92583333333333,
		power: 11018.2892869345,
		road: 20131.1789814815,
		acceleration: 1.07944444444444
	},
	{
		id: 1889,
		time: 1888,
		velocity: 10.4219444444444,
		power: 5822.87555232442,
		road: 20141.1525,
		acceleration: 0.429259259259258
	},
	{
		id: 1890,
		time: 1889,
		velocity: 10.2166666666667,
		power: 1878.92965482397,
		road: 20151.3443981482,
		acceleration: 0.00750000000000028
	},
	{
		id: 1891,
		time: 1890,
		velocity: 9.94833333333333,
		power: -1625.17939254981,
		road: 20161.3629166667,
		acceleration: -0.354259259259258
	},
	{
		id: 1892,
		time: 1891,
		velocity: 9.35916666666667,
		power: -3299.74597986468,
		road: 20170.9337962963,
		acceleration: -0.54101851851852
	},
	{
		id: 1893,
		time: 1892,
		velocity: 8.59361111111111,
		power: -3298.94257329844,
		road: 20179.9555092593,
		acceleration: -0.557314814814813
	},
	{
		id: 1894,
		time: 1893,
		velocity: 8.27638888888889,
		power: -1156.04391027207,
		road: 20188.5433796296,
		acceleration: -0.31037037037037
	},
	{
		id: 1895,
		time: 1894,
		velocity: 8.42805555555556,
		power: 255.697101728543,
		road: 20196.9087037037,
		acceleration: -0.134722222222223
	},
	{
		id: 1896,
		time: 1895,
		velocity: 8.18944444444444,
		power: 3648.51607702586,
		road: 20205.3499074074,
		acceleration: 0.286481481481482
	},
	{
		id: 1897,
		time: 1896,
		velocity: 9.13583333333333,
		power: 5265.26071182648,
		road: 20214.1625,
		acceleration: 0.456296296296296
	},
	{
		id: 1898,
		time: 1897,
		velocity: 9.79694444444444,
		power: 6108.90701899625,
		road: 20223.4600925926,
		acceleration: 0.513703703703703
	},
	{
		id: 1899,
		time: 1898,
		velocity: 9.73055555555556,
		power: 2745.71425006946,
		road: 20233.0746759259,
		acceleration: 0.120277777777778
	},
	{
		id: 1900,
		time: 1899,
		velocity: 9.49666666666667,
		power: -1693.86043644484,
		road: 20242.5666203704,
		acceleration: -0.365555555555554
	},
	{
		id: 1901,
		time: 1900,
		velocity: 8.70027777777778,
		power: -2627.97452551182,
		road: 20251.6367592593,
		acceleration: -0.478055555555557
	},
	{
		id: 1902,
		time: 1901,
		velocity: 8.29638888888889,
		power: -3254.66747521784,
		road: 20260.1835648148,
		acceleration: -0.56861111111111
	},
	{
		id: 1903,
		time: 1902,
		velocity: 7.79083333333333,
		power: -1955.280489488,
		road: 20268.2365277778,
		acceleration: -0.419074074074076
	},
	{
		id: 1904,
		time: 1903,
		velocity: 7.44305555555556,
		power: -1935.1224378331,
		road: 20275.8665740741,
		acceleration: -0.426759259259258
	},
	{
		id: 1905,
		time: 1904,
		velocity: 7.01611111111111,
		power: -1179.32118899504,
		road: 20283.1192592593,
		acceleration: -0.327962962962963
	},
	{
		id: 1906,
		time: 1905,
		velocity: 6.80694444444444,
		power: -1278.87046867524,
		road: 20290.0335648148,
		acceleration: -0.348796296296297
	},
	{
		id: 1907,
		time: 1906,
		velocity: 6.39666666666667,
		power: -466.840279461667,
		road: 20296.6603703704,
		acceleration: -0.226203703703703
	},
	{
		id: 1908,
		time: 1907,
		velocity: 6.3375,
		power: -222.432233549437,
		road: 20303.0805555556,
		acceleration: -0.187037037037038
	},
	{
		id: 1909,
		time: 1908,
		velocity: 6.24583333333333,
		power: -1306.87066062851,
		road: 20309.2209722222,
		acceleration: -0.3725
	},
	{
		id: 1910,
		time: 1909,
		velocity: 5.27916666666667,
		power: -2391.04750777264,
		road: 20314.8801388889,
		acceleration: -0.59
	},
	{
		id: 1911,
		time: 1910,
		velocity: 4.5675,
		power: -3607.36569606956,
		road: 20319.7868518519,
		acceleration: -0.914907407407408
	},
	{
		id: 1912,
		time: 1911,
		velocity: 3.50111111111111,
		power: -2138.66098878437,
		road: 20323.8934259259,
		acceleration: -0.685370370370369
	},
	{
		id: 1913,
		time: 1912,
		velocity: 3.22305555555556,
		power: -1101.62864943967,
		road: 20327.4258333333,
		acceleration: -0.462962962962963
	},
	{
		id: 1914,
		time: 1913,
		velocity: 3.17861111111111,
		power: -155.25100196503,
		road: 20330.6346759259,
		acceleration: -0.184166666666666
	},
	{
		id: 1915,
		time: 1914,
		velocity: 2.94861111111111,
		power: 24.5841929386675,
		road: 20333.6893518519,
		acceleration: -0.124166666666667
	},
	{
		id: 1916,
		time: 1915,
		velocity: 2.85055555555556,
		power: -153.321841328559,
		road: 20336.5880092593,
		acceleration: -0.18787037037037
	},
	{
		id: 1917,
		time: 1916,
		velocity: 2.615,
		power: -62.7289952058066,
		road: 20339.3148148148,
		acceleration: -0.155833333333333
	},
	{
		id: 1918,
		time: 1917,
		velocity: 2.48111111111111,
		power: -1011.67150782738,
		road: 20341.6722222222,
		acceleration: -0.582962962962963
	},
	{
		id: 1919,
		time: 1918,
		velocity: 1.10166666666667,
		power: -1063.14327101343,
		road: 20343.3368518519,
		acceleration: -0.802592592592593
	},
	{
		id: 1920,
		time: 1919,
		velocity: 0.207222222222222,
		power: -563.0767070667,
		road: 20344.1866666667,
		acceleration: -0.827037037037037
	},
	{
		id: 1921,
		time: 1920,
		velocity: 0,
		power: 83.4950276661346,
		road: 20344.6535185185,
		acceleration: 0.0611111111111111
	},
	{
		id: 1922,
		time: 1921,
		velocity: 1.285,
		power: 825.739833893191,
		road: 20345.5648611111,
		acceleration: 0.82787037037037
	},
	{
		id: 1923,
		time: 1922,
		velocity: 2.69083333333333,
		power: 2136.21707870865,
		road: 20347.4293981482,
		acceleration: 1.07851851851852
	},
	{
		id: 1924,
		time: 1923,
		velocity: 3.23555555555556,
		power: 1799.88333080878,
		road: 20350.1199537037,
		acceleration: 0.573518518518518
	},
	{
		id: 1925,
		time: 1924,
		velocity: 3.00555555555556,
		power: 274.956602364814,
		road: 20353.0800462963,
		acceleration: -0.0344444444444445
	},
	{
		id: 1926,
		time: 1925,
		velocity: 2.5875,
		power: -325.551876774648,
		road: 20355.8960648148,
		acceleration: -0.253703703703704
	},
	{
		id: 1927,
		time: 1926,
		velocity: 2.47444444444444,
		power: -205.577273776114,
		road: 20358.4776851852,
		acceleration: -0.215092592592592
	},
	{
		id: 1928,
		time: 1927,
		velocity: 2.36027777777778,
		power: 224.598931437567,
		road: 20360.9345370371,
		acceleration: -0.0344444444444445
	},
	{
		id: 1929,
		time: 1928,
		velocity: 2.48416666666667,
		power: 1174.32523180478,
		road: 20363.5455555556,
		acceleration: 0.342777777777777
	},
	{
		id: 1930,
		time: 1929,
		velocity: 3.50277777777778,
		power: 2290.96036092312,
		road: 20366.6502777778,
		acceleration: 0.64462962962963
	},
	{
		id: 1931,
		time: 1930,
		velocity: 4.29416666666667,
		power: 3951.91076595114,
		road: 20370.5437962963,
		acceleration: 0.932962962962963
	},
	{
		id: 1932,
		time: 1931,
		velocity: 5.28305555555556,
		power: 2981.77267771091,
		road: 20375.1731018519,
		acceleration: 0.538611111111112
	},
	{
		id: 1933,
		time: 1932,
		velocity: 5.11861111111111,
		power: 1849.70368459276,
		road: 20380.1947685185,
		acceleration: 0.246111111111111
	},
	{
		id: 1934,
		time: 1933,
		velocity: 5.0325,
		power: 1273.88911349865,
		road: 20385.397037037,
		acceleration: 0.115092592592593
	},
	{
		id: 1935,
		time: 1934,
		velocity: 5.62833333333333,
		power: 2850.40071045255,
		road: 20390.8593055556,
		acceleration: 0.404907407407407
	},
	{
		id: 1936,
		time: 1935,
		velocity: 6.33333333333333,
		power: 3300.50131618607,
		road: 20396.7455092593,
		acceleration: 0.442962962962964
	},
	{
		id: 1937,
		time: 1936,
		velocity: 6.36138888888889,
		power: 3390.5460269389,
		road: 20403.0606018519,
		acceleration: 0.414814814814815
	},
	{
		id: 1938,
		time: 1937,
		velocity: 6.87277777777778,
		power: 2904.70944898594,
		road: 20409.7356481482,
		acceleration: 0.305092592592592
	},
	{
		id: 1939,
		time: 1938,
		velocity: 7.24861111111111,
		power: 4243.29120028167,
		road: 20416.8011574074,
		acceleration: 0.475833333333334
	},
	{
		id: 1940,
		time: 1939,
		velocity: 7.78888888888889,
		power: 4535.49761155099,
		road: 20424.3409722222,
		acceleration: 0.472777777777777
	},
	{
		id: 1941,
		time: 1940,
		velocity: 8.29111111111111,
		power: 5434.36848833889,
		road: 20432.3899074074,
		acceleration: 0.545462962962964
	},
	{
		id: 1942,
		time: 1941,
		velocity: 8.885,
		power: 5408.31307995216,
		road: 20440.9586574074,
		acceleration: 0.494166666666667
	},
	{
		id: 1943,
		time: 1942,
		velocity: 9.27138888888889,
		power: 6311.62633864583,
		road: 20450.0518981482,
		acceleration: 0.554814814814815
	},
	{
		id: 1944,
		time: 1943,
		velocity: 9.95555555555556,
		power: 3648.89207045497,
		road: 20459.535462963,
		acceleration: 0.225833333333332
	},
	{
		id: 1945,
		time: 1944,
		velocity: 9.5625,
		power: 433.767417550927,
		road: 20469.0665277778,
		acceleration: -0.130833333333333
	},
	{
		id: 1946,
		time: 1945,
		velocity: 8.87888888888889,
		power: -3256.28357213659,
		road: 20478.2586574074,
		acceleration: -0.547037037037038
	},
	{
		id: 1947,
		time: 1946,
		velocity: 8.31444444444444,
		power: -3177.25455784333,
		road: 20486.8993981482,
		acceleration: -0.55574074074074
	},
	{
		id: 1948,
		time: 1947,
		velocity: 7.89527777777778,
		power: -1161.59411313769,
		road: 20495.1052314815,
		acceleration: -0.314074074074075
	},
	{
		id: 1949,
		time: 1948,
		velocity: 7.93666666666667,
		power: 1554.92203593789,
		road: 20503.1732407408,
		acceleration: 0.0384259259259263
	},
	{
		id: 1950,
		time: 1949,
		velocity: 8.42972222222222,
		power: 3591.64985111272,
		road: 20511.4068055556,
		acceleration: 0.292685185185187
	},
	{
		id: 1951,
		time: 1950,
		velocity: 8.77333333333333,
		power: 3182.2417110036,
		road: 20519.8994907408,
		acceleration: 0.225555555555554
	},
	{
		id: 1952,
		time: 1951,
		velocity: 8.61333333333333,
		power: 2171.2260098856,
		road: 20528.5519444445,
		acceleration: 0.0939814814814817
	},
	{
		id: 1953,
		time: 1952,
		velocity: 8.71166666666667,
		power: 1875.31453837482,
		road: 20537.2790740741,
		acceleration: 0.0553703703703725
	},
	{
		id: 1954,
		time: 1953,
		velocity: 8.93944444444445,
		power: 1768.99975963763,
		road: 20546.0543518519,
		acceleration: 0.040925925925924
	},
	{
		id: 1955,
		time: 1954,
		velocity: 8.73611111111111,
		power: 1262.32603755628,
		road: 20554.8400925926,
		acceleration: -0.0199999999999978
	},
	{
		id: 1956,
		time: 1955,
		velocity: 8.65166666666667,
		power: 984.922565886894,
		road: 20563.5896759259,
		acceleration: -0.0523148148148156
	},
	{
		id: 1957,
		time: 1956,
		velocity: 8.7825,
		power: 2016.09006237258,
		road: 20572.3486574074,
		acceleration: 0.0711111111111098
	},
	{
		id: 1958,
		time: 1957,
		velocity: 8.94944444444445,
		power: 3103.0796707081,
		road: 20581.240462963,
		acceleration: 0.194537037037039
	},
	{
		id: 1959,
		time: 1958,
		velocity: 9.23527777777778,
		power: 4620.65568411463,
		road: 20590.4068055556,
		acceleration: 0.354537037037035
	},
	{
		id: 1960,
		time: 1959,
		velocity: 9.84611111111111,
		power: 3583.6030774792,
		road: 20599.8605092593,
		acceleration: 0.220185185185185
	},
	{
		id: 1961,
		time: 1960,
		velocity: 9.61,
		power: 1972.57286838162,
		road: 20609.4427777778,
		acceleration: 0.0369444444444458
	},
	{
		id: 1962,
		time: 1961,
		velocity: 9.34611111111111,
		power: -1023.74999042719,
		road: 20618.8977777778,
		acceleration: -0.291481481481483
	},
	{
		id: 1963,
		time: 1962,
		velocity: 8.97166666666667,
		power: -1170.3291183049,
		road: 20628.0525925926,
		acceleration: -0.308888888888887
	},
	{
		id: 1964,
		time: 1963,
		velocity: 8.68333333333333,
		power: 48.713437585259,
		road: 20636.9697222222,
		acceleration: -0.166481481481481
	},
	{
		id: 1965,
		time: 1964,
		velocity: 8.84666666666667,
		power: 2249.55671371407,
		road: 20645.8506944445,
		acceleration: 0.0941666666666663
	},
	{
		id: 1966,
		time: 1965,
		velocity: 9.25416666666667,
		power: 4430.77493304951,
		road: 20654.9474537037,
		acceleration: 0.337407407407406
	},
	{
		id: 1967,
		time: 1966,
		velocity: 9.69555555555556,
		power: 5758.88537989835,
		road: 20664.4421759259,
		acceleration: 0.45851851851852
	},
	{
		id: 1968,
		time: 1967,
		velocity: 10.2222222222222,
		power: 6168.38860132058,
		road: 20674.3995833334,
		acceleration: 0.46685185185185
	},
	{
		id: 1969,
		time: 1968,
		velocity: 10.6547222222222,
		power: 6551.81455506142,
		road: 20684.8257407408,
		acceleration: 0.470648148148149
	},
	{
		id: 1970,
		time: 1969,
		velocity: 11.1075,
		power: 5383.10086912873,
		road: 20695.6512962963,
		acceleration: 0.328148148148147
	},
	{
		id: 1971,
		time: 1970,
		velocity: 11.2066666666667,
		power: 3941.36679817589,
		road: 20706.729212963,
		acceleration: 0.176574074074075
	},
	{
		id: 1972,
		time: 1971,
		velocity: 11.1844444444444,
		power: 1696.27629077455,
		road: 20717.8764814815,
		acceleration: -0.0378703703703671
	},
	{
		id: 1973,
		time: 1972,
		velocity: 10.9938888888889,
		power: 636.563967591592,
		road: 20728.9368055556,
		acceleration: -0.136018518518522
	},
	{
		id: 1974,
		time: 1973,
		velocity: 10.7986111111111,
		power: -233.992367743048,
		road: 20739.8207407408,
		acceleration: -0.216759259259257
	},
	{
		id: 1975,
		time: 1974,
		velocity: 10.5341666666667,
		power: 434.729081779965,
		road: 20750.5216203704,
		acceleration: -0.149351851851852
	},
	{
		id: 1976,
		time: 1975,
		velocity: 10.5458333333333,
		power: 647.370827762632,
		road: 20761.0848148148,
		acceleration: -0.126018518518517
	},
	{
		id: 1977,
		time: 1976,
		velocity: 10.4205555555556,
		power: 1325.85135930977,
		road: 20771.5568055556,
		acceleration: -0.0563888888888897
	},
	{
		id: 1978,
		time: 1977,
		velocity: 10.365,
		power: 196.105523319588,
		road: 20781.9165740741,
		acceleration: -0.168055555555556
	},
	{
		id: 1979,
		time: 1978,
		velocity: 10.0416666666667,
		power: -46.6392549665068,
		road: 20792.0969907408,
		acceleration: -0.190648148148147
	},
	{
		id: 1980,
		time: 1979,
		velocity: 9.84861111111111,
		power: 537.034846734963,
		road: 20802.1181944445,
		acceleration: -0.12777777777778
	},
	{
		id: 1981,
		time: 1980,
		velocity: 9.98166666666667,
		power: 1650.75227610423,
		road: 20812.0709259259,
		acceleration: -0.00916666666666544
	},
	{
		id: 1982,
		time: 1981,
		velocity: 10.0141666666667,
		power: 2632.11124570411,
		road: 20822.0654166667,
		acceleration: 0.0926851851851858
	},
	{
		id: 1983,
		time: 1982,
		velocity: 10.1266666666667,
		power: 2620.49556691063,
		road: 20832.1502314815,
		acceleration: 0.0879629629629619
	},
	{
		id: 1984,
		time: 1983,
		velocity: 10.2455555555556,
		power: 2939.0804697173,
		road: 20842.3374537037,
		acceleration: 0.116851851851854
	},
	{
		id: 1985,
		time: 1984,
		velocity: 10.3647222222222,
		power: 3392.60108491234,
		road: 20852.6617592593,
		acceleration: 0.157314814814814
	},
	{
		id: 1986,
		time: 1985,
		velocity: 10.5986111111111,
		power: 3163.63944190624,
		road: 20863.1287037037,
		acceleration: 0.127962962962963
	},
	{
		id: 1987,
		time: 1986,
		velocity: 10.6294444444444,
		power: 2568.67865222035,
		road: 20873.6920370371,
		acceleration: 0.0648148148148131
	},
	{
		id: 1988,
		time: 1987,
		velocity: 10.5591666666667,
		power: 1064.71745663615,
		road: 20884.2456018519,
		acceleration: -0.0843518518518511
	},
	{
		id: 1989,
		time: 1988,
		velocity: 10.3455555555556,
		power: 357.321478083641,
		road: 20894.6805555556,
		acceleration: -0.152870370370369
	},
	{
		id: 1990,
		time: 1989,
		velocity: 10.1708333333333,
		power: -99.57940947478,
		road: 20904.9406018519,
		acceleration: -0.196944444444444
	},
	{
		id: 1991,
		time: 1990,
		velocity: 9.96833333333333,
		power: -346.577930063486,
		road: 20914.9918981482,
		acceleration: -0.220555555555556
	},
	{
		id: 1992,
		time: 1991,
		velocity: 9.68388888888889,
		power: -1034.92737596041,
		road: 20924.786712963,
		acceleration: -0.292407407407408
	},
	{
		id: 1993,
		time: 1992,
		velocity: 9.29361111111111,
		power: -978.246524602469,
		road: 20934.2921296296,
		acceleration: -0.286388888888888
	},
	{
		id: 1994,
		time: 1993,
		velocity: 9.10916666666667,
		power: -3206.60344994749,
		road: 20943.3821296296,
		acceleration: -0.544444444444444
	},
	{
		id: 1995,
		time: 1994,
		velocity: 8.05055555555555,
		power: -6454.12766969989,
		road: 20951.7094907408,
		acceleration: -0.980833333333334
	},
	{
		id: 1996,
		time: 1995,
		velocity: 6.35111111111111,
		power: -8663.57359842618,
		road: 20958.8286574074,
		acceleration: -1.43555555555556
	},
	{
		id: 1997,
		time: 1996,
		velocity: 4.8025,
		power: -7821.38165902544,
		road: 20964.4216666667,
		acceleration: -1.61675925925926
	},
	{
		id: 1998,
		time: 1997,
		velocity: 3.20027777777778,
		power: -6091.73514484234,
		road: 20968.3139351852,
		acceleration: -1.78472222222222
	},
	{
		id: 1999,
		time: 1998,
		velocity: 0.996944444444444,
		power: -2772.84815532855,
		road: 20970.6127314815,
		acceleration: -1.40222222222222
	},
	{
		id: 2000,
		time: 1999,
		velocity: 0.595833333333333,
		power: -946.852109476596,
		road: 20971.6770370371,
		acceleration: -1.06675925925926
	},
	{
		id: 2001,
		time: 2000,
		velocity: 0,
		power: -70.7597131298891,
		road: 20972.0418055556,
		acceleration: -0.332314814814815
	},
	{
		id: 2002,
		time: 2001,
		velocity: 0,
		power: -6.68723611111111,
		road: 20972.1411111111,
		acceleration: -0.198611111111111
	},
	{
		id: 2003,
		time: 2002,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2004,
		time: 2003,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2005,
		time: 2004,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2006,
		time: 2005,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2007,
		time: 2006,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2008,
		time: 2007,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2009,
		time: 2008,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2010,
		time: 2009,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2011,
		time: 2010,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2012,
		time: 2011,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2013,
		time: 2012,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2014,
		time: 2013,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2015,
		time: 2014,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2016,
		time: 2015,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2017,
		time: 2016,
		velocity: 0,
		power: 0,
		road: 20972.1411111111,
		acceleration: 0
	},
	{
		id: 2018,
		time: 2017,
		velocity: 0,
		power: 174.572231443066,
		road: 20972.4143981482,
		acceleration: 0.546574074074074
	},
	{
		id: 2019,
		time: 2018,
		velocity: 1.63972222222222,
		power: 1577.74740127123,
		road: 20973.5991203704,
		acceleration: 1.2762962962963
	},
	{
		id: 2020,
		time: 2019,
		velocity: 3.82888888888889,
		power: 4289.35073338081,
		road: 20976.2189814815,
		acceleration: 1.59398148148148
	},
	{
		id: 2021,
		time: 2020,
		velocity: 4.78194444444444,
		power: 4947.71324624743,
		road: 20980.2188888889,
		acceleration: 1.16611111111111
	},
	{
		id: 2022,
		time: 2021,
		velocity: 5.13805555555556,
		power: 2757.10816804012,
		road: 20985.0330555556,
		acceleration: 0.462407407407407
	},
	{
		id: 2023,
		time: 2022,
		velocity: 5.21611111111111,
		power: 2916.00585150295,
		road: 20990.2983333333,
		acceleration: 0.439814814814815
	},
	{
		id: 2024,
		time: 2023,
		velocity: 6.10138888888889,
		power: 4185.34029189999,
		road: 20996.090462963,
		acceleration: 0.613888888888889
	},
	{
		id: 2025,
		time: 2024,
		velocity: 6.97972222222222,
		power: 4399.08579651058,
		road: 21002.4765740741,
		acceleration: 0.574074074074074
	},
	{
		id: 2026,
		time: 2025,
		velocity: 6.93833333333333,
		power: 1534.98095698869,
		road: 21009.1934722222,
		acceleration: 0.0875000000000012
	},
	{
		id: 2027,
		time: 2026,
		velocity: 6.36388888888889,
		power: -1542.57702307177,
		road: 21015.7546759259,
		acceleration: -0.39888888888889
	},
	{
		id: 2028,
		time: 2027,
		velocity: 5.78305555555556,
		power: -1840.7121042987,
		road: 21021.884212963,
		acceleration: -0.464444444444443
	},
	{
		id: 2029,
		time: 2028,
		velocity: 5.545,
		power: -1088.11069550285,
		road: 21027.6086111111,
		acceleration: -0.345833333333334
	},
	{
		id: 2030,
		time: 2029,
		velocity: 5.32638888888889,
		power: -1127.14481345237,
		road: 21032.9778240741,
		acceleration: -0.364537037037037
	},
	{
		id: 2031,
		time: 2030,
		velocity: 4.68944444444444,
		power: 121.634664925794,
		road: 21038.1061574074,
		acceleration: -0.117222222222222
	},
	{
		id: 2032,
		time: 2031,
		velocity: 5.19333333333333,
		power: 1649.08749955007,
		road: 21043.2726388889,
		acceleration: 0.193518518518518
	},
	{
		id: 2033,
		time: 2032,
		velocity: 5.90694444444444,
		power: 3321.96866539067,
		road: 21048.7809259259,
		acceleration: 0.490092592592593
	},
	{
		id: 2034,
		time: 2033,
		velocity: 6.15972222222222,
		power: 3456.87556644001,
		road: 21054.7643518519,
		acceleration: 0.460185185185185
	},
	{
		id: 2035,
		time: 2034,
		velocity: 6.57388888888889,
		power: 3175.28570892302,
		road: 21061.1635648148,
		acceleration: 0.371388888888888
	},
	{
		id: 2036,
		time: 2035,
		velocity: 7.02111111111111,
		power: 3078.71225787797,
		road: 21067.9118055556,
		acceleration: 0.326666666666667
	},
	{
		id: 2037,
		time: 2036,
		velocity: 7.13972222222222,
		power: 1558.70285537495,
		road: 21074.8639351852,
		acceleration: 0.0811111111111114
	},
	{
		id: 2038,
		time: 2037,
		velocity: 6.81722222222222,
		power: 125.339416185761,
		road: 21081.7889351852,
		acceleration: -0.135370370370371
	},
	{
		id: 2039,
		time: 2038,
		velocity: 6.615,
		power: -1343.10449642487,
		road: 21088.464212963,
		acceleration: -0.364074074074074
	},
	{
		id: 2040,
		time: 2039,
		velocity: 6.0475,
		power: -2231.57574649613,
		road: 21094.6944444445,
		acceleration: -0.526018518518518
	},
	{
		id: 2041,
		time: 2040,
		velocity: 5.23916666666667,
		power: -3709.52350381144,
		road: 21100.2371759259,
		acceleration: -0.848981481481482
	},
	{
		id: 2042,
		time: 2041,
		velocity: 4.06805555555556,
		power: -3023.38856755616,
		road: 21104.9475925926,
		acceleration: -0.815648148148148
	},
	{
		id: 2043,
		time: 2042,
		velocity: 3.60055555555556,
		power: -2048.68742498661,
		road: 21108.9097685185,
		acceleration: -0.680833333333334
	},
	{
		id: 2044,
		time: 2043,
		velocity: 3.19666666666667,
		power: -1513.49254901048,
		road: 21112.2241666667,
		acceleration: -0.614722222222222
	},
	{
		id: 2045,
		time: 2044,
		velocity: 2.22388888888889,
		power: -1322.21307587146,
		road: 21114.9056018519,
		acceleration: -0.651203703703704
	},
	{
		id: 2046,
		time: 2045,
		velocity: 1.64694444444444,
		power: -1618.42123293245,
		road: 21116.7286574074,
		acceleration: -1.06555555555556
	},
	{
		id: 2047,
		time: 2046,
		velocity: 0,
		power: -530.412628310602,
		road: 21117.6518518519,
		acceleration: -0.734166666666667
	},
	{
		id: 2048,
		time: 2047,
		velocity: 0.0213888888888889,
		power: -112.442508099385,
		road: 21117.9334722222,
		acceleration: -0.548981481481482
	},
	{
		id: 2049,
		time: 2048,
		velocity: 0,
		power: 0.861387038847304,
		road: 21117.9406018519,
		acceleration: 0
	},
	{
		id: 2050,
		time: 2049,
		velocity: 0,
		power: 0.406615285899935,
		road: 21117.9441666667,
		acceleration: -0.00712962962962963
	},
	{
		id: 2051,
		time: 2050,
		velocity: 0,
		power: 31.8227956923592,
		road: 21118.0457407407,
		acceleration: 0.203148148148148
	},
	{
		id: 2052,
		time: 2051,
		velocity: 0.609444444444444,
		power: 482.891408613603,
		road: 21118.6249074074,
		acceleration: 0.752037037037037
	},
	{
		id: 2053,
		time: 2052,
		velocity: 2.25611111111111,
		power: 1380.9759091189,
		road: 21120.032962963,
		acceleration: 0.905740740740741
	},
	{
		id: 2054,
		time: 2053,
		velocity: 2.71722222222222,
		power: 1855.5747666964,
		road: 21122.2666203704,
		acceleration: 0.745462962962963
	},
	{
		id: 2055,
		time: 2054,
		velocity: 2.84583333333333,
		power: 774.888923507284,
		road: 21124.9589351852,
		acceleration: 0.171851851851851
	},
	{
		id: 2056,
		time: 2055,
		velocity: 2.77166666666667,
		power: -548.221309776829,
		road: 21127.5605092593,
		acceleration: -0.353333333333334
	},
	{
		id: 2057,
		time: 2056,
		velocity: 1.65722222222222,
		power: -556.293301991251,
		road: 21129.7887037037,
		acceleration: -0.393425925925926
	},
	{
		id: 2058,
		time: 2057,
		velocity: 1.66555555555556,
		power: -604.658686886975,
		road: 21131.5773148148,
		acceleration: -0.48574074074074
	},
	{
		id: 2059,
		time: 2058,
		velocity: 1.31444444444444,
		power: -510.32822166365,
		road: 21132.8468518519,
		acceleration: -0.552407407407407
	},
	{
		id: 2060,
		time: 2059,
		velocity: 0,
		power: -289.905634151875,
		road: 21133.5625925926,
		acceleration: -0.555185185185185
	},
	{
		id: 2061,
		time: 2060,
		velocity: 0,
		power: -64.4668893437297,
		road: 21133.7816666667,
		acceleration: -0.438148148148148
	},
	{
		id: 2062,
		time: 2061,
		velocity: 0,
		power: 71.3217700063269,
		road: 21133.9463888889,
		acceleration: 0.329444444444444
	},
	{
		id: 2063,
		time: 2062,
		velocity: 0.988333333333333,
		power: 471.385615803823,
		road: 21134.5952314815,
		acceleration: 0.638796296296296
	},
	{
		id: 2064,
		time: 2063,
		velocity: 1.91638888888889,
		power: 936.47568844338,
		road: 21135.8828240741,
		acceleration: 0.638703703703703
	},
	{
		id: 2065,
		time: 2064,
		velocity: 1.91611111111111,
		power: 511.36132224923,
		road: 21137.58375,
		acceleration: 0.187962962962963
	},
	{
		id: 2066,
		time: 2065,
		velocity: 1.55222222222222,
		power: -640.39357084907,
		road: 21139.0900925926,
		acceleration: -0.57712962962963
	},
	{
		id: 2067,
		time: 2066,
		velocity: 0.185,
		power: -434.917016947891,
		road: 21139.9885185185,
		acceleration: -0.638703703703704
	},
	{
		id: 2068,
		time: 2067,
		velocity: 0,
		power: -118.330551747696,
		road: 21140.3088888889,
		acceleration: -0.517407407407407
	},
	{
		id: 2069,
		time: 2068,
		velocity: 0,
		power: 1.92390263157895,
		road: 21140.3397222222,
		acceleration: -0.0616666666666667
	},
	{
		id: 2070,
		time: 2069,
		velocity: 0,
		power: 0,
		road: 21140.3397222222,
		acceleration: 0
	},
	{
		id: 2071,
		time: 2070,
		velocity: 0,
		power: 0,
		road: 21140.3397222222,
		acceleration: 0
	},
	{
		id: 2072,
		time: 2071,
		velocity: 0,
		power: 266.433059134541,
		road: 21140.6841203704,
		acceleration: 0.688796296296296
	},
	{
		id: 2073,
		time: 2072,
		velocity: 2.06638888888889,
		power: 1805.34809631053,
		road: 21142.0208333333,
		acceleration: 1.29583333333333
	},
	{
		id: 2074,
		time: 2073,
		velocity: 3.8875,
		power: 4710.8393575551,
		road: 21144.8246759259,
		acceleration: 1.63842592592593
	},
	{
		id: 2075,
		time: 2074,
		velocity: 4.91527777777778,
		power: 4065.26702165523,
		road: 21148.9040277778,
		acceleration: 0.912592592592592
	},
	{
		id: 2076,
		time: 2075,
		velocity: 4.80416666666667,
		power: 507.489698983964,
		road: 21153.4292592593,
		acceleration: -0.020833333333333
	},
	{
		id: 2077,
		time: 2076,
		velocity: 3.825,
		power: -703.528812989696,
		road: 21157.7900925926,
		acceleration: -0.307962962962963
	},
	{
		id: 2078,
		time: 2077,
		velocity: 3.99138888888889,
		power: 444.98498251404,
		road: 21161.9841666667,
		acceleration: -0.025555555555556
	},
	{
		id: 2079,
		time: 2078,
		velocity: 4.7275,
		power: 2898.01673683488,
		road: 21166.4386574074,
		acceleration: 0.546388888888889
	},
	{
		id: 2080,
		time: 2079,
		velocity: 5.46416666666667,
		power: 5120.68632633864,
		road: 21171.6155555556,
		acceleration: 0.898425925925926
	},
	{
		id: 2081,
		time: 2080,
		velocity: 6.68666666666667,
		power: 6127.05808062727,
		road: 21177.6973611111,
		acceleration: 0.91138888888889
	},
	{
		id: 2082,
		time: 2081,
		velocity: 7.46166666666667,
		power: 4741.92177576839,
		road: 21184.5232407407,
		acceleration: 0.576759259259259
	},
	{
		id: 2083,
		time: 2082,
		velocity: 7.19444444444444,
		power: 1728.00949662874,
		road: 21191.6861574074,
		acceleration: 0.0973148148148155
	},
	{
		id: 2084,
		time: 2083,
		velocity: 6.97861111111111,
		power: 313.516441664667,
		road: 21198.8426388889,
		acceleration: -0.110185185185186
	},
	{
		id: 2085,
		time: 2084,
		velocity: 7.13111111111111,
		power: 1478.31609085082,
		road: 21205.9749537037,
		acceleration: 0.061851851851852
	},
	{
		id: 2086,
		time: 2085,
		velocity: 7.38,
		power: 1633.67349621544,
		road: 21213.1790740741,
		acceleration: 0.0817592592592593
	},
	{
		id: 2087,
		time: 2086,
		velocity: 7.22388888888889,
		power: -596.921975299281,
		road: 21220.302037037,
		acceleration: -0.244074074074074
	},
	{
		id: 2088,
		time: 2087,
		velocity: 6.39888888888889,
		power: -4924.30655947947,
		road: 21226.8306018519,
		acceleration: -0.944722222222222
	},
	{
		id: 2089,
		time: 2088,
		velocity: 4.54583333333333,
		power: -5716.43070599767,
		road: 21232.2608333333,
		acceleration: -1.25194444444445
	},
	{
		id: 2090,
		time: 2089,
		velocity: 3.46805555555556,
		power: -4247.47979017777,
		road: 21236.4643055556,
		acceleration: -1.20157407407407
	},
	{
		id: 2091,
		time: 2090,
		velocity: 2.79416666666667,
		power: -1829.0404443084,
		road: 21239.7027777778,
		acceleration: -0.728425925925926
	},
	{
		id: 2092,
		time: 2091,
		velocity: 2.36055555555556,
		power: -632.060091628814,
		road: 21242.3872222222,
		acceleration: -0.37962962962963
	},
	{
		id: 2093,
		time: 2092,
		velocity: 2.32916666666667,
		power: 555.466880885848,
		road: 21244.9313888889,
		acceleration: 0.0990740740740739
	},
	{
		id: 2094,
		time: 2093,
		velocity: 3.09138888888889,
		power: 536.708354615221,
		road: 21247.5667592593,
		acceleration: 0.0833333333333335
	},
	{
		id: 2095,
		time: 2094,
		velocity: 2.61055555555556,
		power: 1183.03632971349,
		road: 21250.3980092593,
		acceleration: 0.308425925925926
	},
	{
		id: 2096,
		time: 2095,
		velocity: 3.25444444444444,
		power: 1371.86828620949,
		road: 21253.5465277778,
		acceleration: 0.326111111111111
	},
	{
		id: 2097,
		time: 2096,
		velocity: 4.06972222222222,
		power: 3407.785659114,
		road: 21257.2721759259,
		acceleration: 0.828148148148148
	},
	{
		id: 2098,
		time: 2097,
		velocity: 5.095,
		power: 2827.30721566202,
		road: 21261.6803703704,
		acceleration: 0.536944444444445
	},
	{
		id: 2099,
		time: 2098,
		velocity: 4.86527777777778,
		power: 1775.78593078046,
		road: 21266.4815277778,
		acceleration: 0.248981481481481
	},
	{
		id: 2100,
		time: 2099,
		velocity: 4.81666666666667,
		power: 958.891587478588,
		road: 21271.4383796296,
		acceleration: 0.0624074074074077
	},
	{
		id: 2101,
		time: 2100,
		velocity: 5.28222222222222,
		power: 443.933369626214,
		road: 21276.4028703704,
		acceleration: -0.0471296296296302
	},
	{
		id: 2102,
		time: 2101,
		velocity: 4.72388888888889,
		power: -126.405061506221,
		road: 21281.2597685185,
		acceleration: -0.168055555555554
	},
	{
		id: 2103,
		time: 2102,
		velocity: 4.3125,
		power: 813.638633165576,
		road: 21286.0518518519,
		acceleration: 0.0384259259259263
	},
	{
		id: 2104,
		time: 2103,
		velocity: 5.3975,
		power: 1426.0319626162,
		road: 21290.9460648148,
		acceleration: 0.165833333333333
	},
	{
		id: 2105,
		time: 2104,
		velocity: 5.22138888888889,
		power: 2094.74809769573,
		road: 21296.0673611111,
		acceleration: 0.288333333333333
	},
	{
		id: 2106,
		time: 2105,
		velocity: 5.1775,
		power: -644.85648822759,
		road: 21301.1955555556,
		acceleration: -0.274537037037037
	},
	{
		id: 2107,
		time: 2106,
		velocity: 4.57388888888889,
		power: -1536.77020459459,
		road: 21305.9461574074,
		acceleration: -0.480648148148148
	},
	{
		id: 2108,
		time: 2107,
		velocity: 3.77944444444444,
		power: -2224.52109445323,
		road: 21310.1062962963,
		acceleration: -0.700277777777778
	},
	{
		id: 2109,
		time: 2108,
		velocity: 3.07666666666667,
		power: -1611.8708510812,
		road: 21313.6065740741,
		acceleration: -0.619444444444445
	},
	{
		id: 2110,
		time: 2109,
		velocity: 2.71555555555556,
		power: -696.199210797156,
		road: 21316.6087037037,
		acceleration: -0.376851851851852
	},
	{
		id: 2111,
		time: 2110,
		velocity: 2.64888888888889,
		power: -546.996041049549,
		road: 21319.2475,
		acceleration: -0.349814814814815
	},
	{
		id: 2112,
		time: 2111,
		velocity: 2.02722222222222,
		power: -783.344676845637,
		road: 21321.4596296296,
		acceleration: -0.503518518518518
	},
	{
		id: 2113,
		time: 2112,
		velocity: 1.205,
		power: -1014.71535597454,
		road: 21323.0105555556,
		acceleration: -0.818888888888889
	},
	{
		id: 2114,
		time: 2113,
		velocity: 0.192222222222222,
		power: -417.266862489429,
		road: 21323.8141666667,
		acceleration: -0.675740740740741
	},
	{
		id: 2115,
		time: 2114,
		velocity: 0,
		power: -36.8546936006045,
		road: 21324.1598611111,
		acceleration: -0.240092592592593
	},
	{
		id: 2116,
		time: 2115,
		velocity: 0.484722222222222,
		power: 11.6418966392341,
		road: 21324.3534722222,
		acceleration: -0.0640740740740741
	},
	{
		id: 2117,
		time: 2116,
		velocity: 0,
		power: 75.3151157936911,
		road: 21324.6076388889,
		acceleration: 0.185185185185185
	},
	{
		id: 2118,
		time: 2117,
		velocity: 0.555555555555556,
		power: 15.6385655945723,
		road: 21324.9172685185,
		acceleration: -0.0742592592592592
	},
	{
		id: 2119,
		time: 2118,
		velocity: 0.261944444444444,
		power: 32.9338612075836,
		road: 21325.1897685185,
		acceleration: 0
	},
	{
		id: 2120,
		time: 2119,
		velocity: 0,
		power: -9.82592424657461,
		road: 21325.3696759259,
		acceleration: -0.185185185185185
	},
	{
		id: 2121,
		time: 2120,
		velocity: 0,
		power: 31.9363288305137,
		road: 21325.5118055556,
		acceleration: 0.10962962962963
	},
	{
		id: 2122,
		time: 2121,
		velocity: 0.590833333333333,
		power: 141.900468275321,
		road: 21325.86,
		acceleration: 0.3025
	},
	{
		id: 2123,
		time: 2122,
		velocity: 0.9075,
		power: 142.419071748017,
		road: 21326.4279166667,
		acceleration: 0.136944444444445
	},
	{
		id: 2124,
		time: 2123,
		velocity: 0.410833333333333,
		power: 16.3348563138177,
		road: 21327.0151388889,
		acceleration: -0.0983333333333333
	},
	{
		id: 2125,
		time: 2124,
		velocity: 0.295833333333333,
		power: -64.1056447181626,
		road: 21327.4019444444,
		acceleration: -0.3025
	},
	{
		id: 2126,
		time: 2125,
		velocity: 0,
		power: -1.48932543072594,
		road: 21327.5690277778,
		acceleration: -0.136944444444444
	},
	{
		id: 2127,
		time: 2126,
		velocity: 0,
		power: 1.35081652046784,
		road: 21327.6183333333,
		acceleration: -0.0986111111111111
	},
	{
		id: 2128,
		time: 2127,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2129,
		time: 2128,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2130,
		time: 2129,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2131,
		time: 2130,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2132,
		time: 2131,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2133,
		time: 2132,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2134,
		time: 2133,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2135,
		time: 2134,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2136,
		time: 2135,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2137,
		time: 2136,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2138,
		time: 2137,
		velocity: 0,
		power: 0,
		road: 21327.6183333333,
		acceleration: 0
	},
	{
		id: 2139,
		time: 2138,
		velocity: 0,
		power: 46.154180899932,
		road: 21327.7457407407,
		acceleration: 0.254814814814815
	},
	{
		id: 2140,
		time: 2139,
		velocity: 0.764444444444444,
		power: 40.4624288216063,
		road: 21328.0158333333,
		acceleration: 0.0305555555555556
	},
	{
		id: 2141,
		time: 2140,
		velocity: 0.0916666666666667,
		power: 34.4904634038561,
		road: 21328.3012037037,
		acceleration: 0
	},
	{
		id: 2142,
		time: 2141,
		velocity: 0,
		power: -19.0479808206825,
		road: 21328.4591666667,
		acceleration: -0.254814814814815
	},
	{
		id: 2143,
		time: 2142,
		velocity: 0,
		power: 1.40357748538012,
		road: 21328.4744444444,
		acceleration: -0.0305555555555556
	},
	{
		id: 2144,
		time: 2143,
		velocity: 0,
		power: 0,
		road: 21328.4744444444,
		acceleration: 0
	},
	{
		id: 2145,
		time: 2144,
		velocity: 0,
		power: 0,
		road: 21328.4744444444,
		acceleration: 0
	},
	{
		id: 2146,
		time: 2145,
		velocity: 0,
		power: 1.01962736266333,
		road: 21328.4819907407,
		acceleration: 0.0150925925925926
	},
	{
		id: 2147,
		time: 2146,
		velocity: 0.0452777777777778,
		power: 4.51013450357312,
		road: 21328.5121759259,
		acceleration: 0.0301851851851852
	}
];
export default train4;
