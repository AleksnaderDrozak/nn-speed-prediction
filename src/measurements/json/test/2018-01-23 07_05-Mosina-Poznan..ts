export const test7 = [
	{
		id: 1,
		time: 0,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 3,
		time: 2,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 4,
		time: 3,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 5,
		time: 4,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 6,
		time: 5,
		velocity: 0,
		power: 11.4916997670535,
		road: 0.0522685185185185,
		acceleration: 0.104537037037037
	},
	{
		id: 7,
		time: 6,
		velocity: 0.313611111111111,
		power: 12.6305649012285,
		road: 0.156805555555556,
		acceleration: 0
	},
	{
		id: 8,
		time: 7,
		velocity: 0,
		power: 12.6305649012285,
		road: 0.261342592592593,
		acceleration: 0
	},
	{
		id: 9,
		time: 8,
		velocity: 0,
		power: 1.13855505198181,
		road: 0.313611111111111,
		acceleration: -0.104537037037037
	},
	{
		id: 10,
		time: 9,
		velocity: 0,
		power: 0,
		road: 0.313611111111111,
		acceleration: 0
	},
	{
		id: 11,
		time: 10,
		velocity: 0,
		power: 0,
		road: 0.313611111111111,
		acceleration: 0
	},
	{
		id: 12,
		time: 11,
		velocity: 0,
		power: 0,
		road: 0.313611111111111,
		acceleration: 0
	},
	{
		id: 13,
		time: 12,
		velocity: 0,
		power: 139.310681870817,
		road: 0.554722222222222,
		acceleration: 0.482222222222222
	},
	{
		id: 14,
		time: 13,
		velocity: 1.44666666666667,
		power: 58.3219485369553,
		road: 1.03694444444444,
		acceleration: 0
	},
	{
		id: 15,
		time: 14,
		velocity: 0,
		power: 58.3219485369553,
		road: 1.51916666666667,
		acceleration: 0
	},
	{
		id: 16,
		time: 15,
		velocity: 0,
		power: -81.0191707602339,
		road: 1.76027777777778,
		acceleration: -0.482222222222222
	},
	{
		id: 17,
		time: 16,
		velocity: 0,
		power: 0,
		road: 1.76027777777778,
		acceleration: 0
	},
	{
		id: 18,
		time: 17,
		velocity: 0,
		power: 0,
		road: 1.76027777777778,
		acceleration: 0
	},
	{
		id: 19,
		time: 18,
		velocity: 0,
		power: 0,
		road: 1.76027777777778,
		acceleration: 0
	},
	{
		id: 20,
		time: 19,
		velocity: 0,
		power: 36.5685051497277,
		road: 1.87092592592593,
		acceleration: 0.221296296296296
	},
	{
		id: 21,
		time: 20,
		velocity: 0.663888888888889,
		power: 162.571747371246,
		road: 2.25296296296296,
		acceleration: 0.321481481481481
	},
	{
		id: 22,
		time: 21,
		velocity: 0.964444444444444,
		power: 365.318044272148,
		road: 2.9924537037037,
		acceleration: 0.393425925925926
	},
	{
		id: 23,
		time: 22,
		velocity: 1.18027777777778,
		power: 431.052923217193,
		road: 4.07467592592593,
		acceleration: 0.292037037037037
	},
	{
		id: 24,
		time: 23,
		velocity: 1.54,
		power: 544.632216038426,
		road: 5.44782407407407,
		acceleration: 0.289814814814815
	},
	{
		id: 25,
		time: 24,
		velocity: 1.83388888888889,
		power: 585.61552580984,
		road: 7.08949074074074,
		acceleration: 0.247222222222222
	},
	{
		id: 26,
		time: 25,
		velocity: 1.92194444444444,
		power: 307.443816087804,
		road: 8.88064814814815,
		acceleration: 0.0517592592592591
	},
	{
		id: 27,
		time: 26,
		velocity: 1.69527777777778,
		power: 33.1403484142706,
		road: 10.6430092592593,
		acceleration: -0.109351851851852
	},
	{
		id: 28,
		time: 27,
		velocity: 1.50583333333333,
		power: -438.039420911208,
		road: 12.1311111111111,
		acceleration: -0.439166666666667
	},
	{
		id: 29,
		time: 28,
		velocity: 0.604444444444444,
		power: -408.453157294582,
		road: 13.1170833333333,
		acceleration: -0.565092592592592
	},
	{
		id: 30,
		time: 29,
		velocity: 0,
		power: -160.479167795021,
		road: 13.569537037037,
		acceleration: -0.501944444444444
	},
	{
		id: 31,
		time: 30,
		velocity: 0,
		power: 38.3758731767684,
		road: 13.7964814814815,
		acceleration: 0.0509259259259259
	},
	{
		id: 32,
		time: 31,
		velocity: 0.757222222222222,
		power: 76.9175640921089,
		road: 14.1132407407407,
		acceleration: 0.128703703703704
	},
	{
		id: 33,
		time: 32,
		velocity: 0.386111111111111,
		power: 46.0750925632478,
		road: 14.4943518518518,
		acceleration: 0
	},
	{
		id: 34,
		time: 33,
		velocity: 0,
		power: 17.1823092331605,
		road: 14.8380555555556,
		acceleration: -0.0748148148148148
	},
	{
		id: 35,
		time: 34,
		velocity: 0.532777777777778,
		power: 207.098356938022,
		road: 15.3113888888889,
		acceleration: 0.334074074074074
	},
	{
		id: 36,
		time: 35,
		velocity: 1.38833333333333,
		power: 628.09829754699,
		road: 16.243287037037,
		acceleration: 0.583055555555556
	},
	{
		id: 37,
		time: 36,
		velocity: 1.74916666666667,
		power: 215.537985333102,
		road: 17.4934722222222,
		acceleration: 0.0535185185185185
	},
	{
		id: 38,
		time: 37,
		velocity: 0.693333333333333,
		power: -331.695501826425,
		road: 18.5390277777778,
		acceleration: -0.462777777777778
	},
	{
		id: 39,
		time: 38,
		velocity: 0,
		power: -225.529933142039,
		road: 19.0616666666667,
		acceleration: -0.583055555555555
	},
	{
		id: 40,
		time: 39,
		velocity: 0,
		power: 483.678299582813,
		road: 19.6573148148148,
		acceleration: 0.729074074074074
	},
	{
		id: 41,
		time: 40,
		velocity: 2.88055555555556,
		power: 2259.48687981848,
		road: 21.2849074074074,
		acceleration: 1.33481481481481
	},
	{
		id: 42,
		time: 41,
		velocity: 4.00444444444444,
		power: 4584.12649375559,
		road: 24.3114814814815,
		acceleration: 1.46314814814815
	},
	{
		id: 43,
		time: 42,
		velocity: 4.38944444444444,
		power: 3304.15531767745,
		road: 28.4241666666667,
		acceleration: 0.709074074074075
	},
	{
		id: 44,
		time: 43,
		velocity: 5.00777777777778,
		power: 1498.76487028992,
		road: 32.9944444444444,
		acceleration: 0.20611111111111
	},
	{
		id: 45,
		time: 44,
		velocity: 4.62277777777778,
		power: 476.628390352331,
		road: 37.6518518518518,
		acceleration: -0.0318518518518518
	},
	{
		id: 46,
		time: 45,
		velocity: 4.29388888888889,
		power: -9.76271262906799,
		road: 42.2226388888889,
		acceleration: -0.141388888888888
	},
	{
		id: 47,
		time: 46,
		velocity: 4.58361111111111,
		power: 275.464772910171,
		road: 46.6859259259259,
		acceleration: -0.0736111111111111
	},
	{
		id: 48,
		time: 47,
		velocity: 4.40194444444444,
		power: 2144.24364960464,
		road: 51.2880092592592,
		acceleration: 0.351203703703703
	},
	{
		id: 49,
		time: 48,
		velocity: 5.3475,
		power: 3108.91724494611,
		road: 56.32,
		acceleration: 0.508611111111111
	},
	{
		id: 50,
		time: 49,
		velocity: 6.10944444444444,
		power: 5886.59205487849,
		road: 62.0716203703704,
		acceleration: 0.930648148148148
	},
	{
		id: 51,
		time: 50,
		velocity: 7.19388888888889,
		power: 7411.96529489006,
		road: 68.7919444444444,
		acceleration: 1.00675925925926
	},
	{
		id: 52,
		time: 51,
		velocity: 8.36777777777778,
		power: 9439.08169359833,
		road: 76.5722685185185,
		acceleration: 1.11324074074074
	},
	{
		id: 53,
		time: 52,
		velocity: 9.44916666666667,
		power: 10320.8095521858,
		road: 85.4348148148148,
		acceleration: 1.0512037037037
	},
	{
		id: 54,
		time: 53,
		velocity: 10.3475,
		power: 8967.5729483587,
		road: 95.2135648148148,
		acceleration: 0.781203703703705
	},
	{
		id: 55,
		time: 54,
		velocity: 10.7113888888889,
		power: 4751.47654047354,
		road: 105.530833333333,
		acceleration: 0.295833333333333
	},
	{
		id: 56,
		time: 55,
		velocity: 10.3366666666667,
		power: -346.449065116616,
		road: 115.884537037037,
		acceleration: -0.222962962962962
	},
	{
		id: 57,
		time: 56,
		velocity: 9.67861111111111,
		power: -5027.07019358891,
		road: 125.768564814815,
		acceleration: -0.71638888888889
	},
	{
		id: 58,
		time: 57,
		velocity: 8.56222222222222,
		power: -4259.09129714459,
		road: 134.963657407407,
		acceleration: -0.661481481481481
	},
	{
		id: 59,
		time: 58,
		velocity: 8.35222222222222,
		power: -3613.4082151492,
		road: 143.521898148148,
		acceleration: -0.612222222222222
	},
	{
		id: 60,
		time: 59,
		velocity: 7.84194444444444,
		power: -1842.67763855123,
		road: 151.571805555556,
		acceleration: -0.404444444444445
	},
	{
		id: 61,
		time: 60,
		velocity: 7.34888888888889,
		power: -3871.76933268875,
		road: 159.068518518518,
		acceleration: -0.701944444444444
	},
	{
		id: 62,
		time: 61,
		velocity: 6.24638888888889,
		power: -2968.69729099735,
		road: 165.909212962963,
		acceleration: -0.610092592592593
	},
	{
		id: 63,
		time: 62,
		velocity: 6.01166666666667,
		power: -4734.42254120164,
		road: 171.959212962963,
		acceleration: -0.971296296296297
	},
	{
		id: 64,
		time: 63,
		velocity: 4.435,
		power: -2814.46779103384,
		road: 177.16787037037,
		acceleration: -0.711388888888888
	},
	{
		id: 65,
		time: 64,
		velocity: 4.11222222222222,
		power: -1170.11224634777,
		road: 181.818611111111,
		acceleration: -0.404444444444444
	},
	{
		id: 66,
		time: 65,
		velocity: 4.79833333333333,
		power: 3373.80818282814,
		road: 186.570740740741,
		acceleration: 0.607222222222222
	},
	{
		id: 67,
		time: 66,
		velocity: 6.25666666666667,
		power: 6777.0858758292,
		road: 192.188472222222,
		acceleration: 1.12398148148148
	},
	{
		id: 68,
		time: 67,
		velocity: 7.48416666666667,
		power: 10479.4160811658,
		road: 199.089259259259,
		acceleration: 1.44212962962963
	},
	{
		id: 69,
		time: 68,
		velocity: 9.12472222222222,
		power: 12437.972398131,
		road: 207.412685185185,
		acceleration: 1.40314814814815
	},
	{
		id: 70,
		time: 69,
		velocity: 10.4661111111111,
		power: 11933.7001612774,
		road: 217.00125,
		acceleration: 1.12712962962963
	},
	{
		id: 71,
		time: 70,
		velocity: 10.8655555555556,
		power: 8747.502704275,
		road: 227.49587962963,
		acceleration: 0.685000000000001
	},
	{
		id: 72,
		time: 71,
		velocity: 11.1797222222222,
		power: 7094.98437135589,
		road: 238.570694444444,
		acceleration: 0.475370370370371
	},
	{
		id: 73,
		time: 72,
		velocity: 11.8922222222222,
		power: 9092.90339978318,
		road: 250.19162037037,
		acceleration: 0.616851851851854
	},
	{
		id: 74,
		time: 73,
		velocity: 12.7161111111111,
		power: 11795.212938794,
		road: 262.515972222222,
		acceleration: 0.789999999999999
	},
	{
		id: 75,
		time: 74,
		velocity: 13.5497222222222,
		power: 14258.3386255607,
		road: 275.689583333333,
		acceleration: 0.908518518518516
	},
	{
		id: 76,
		time: 75,
		velocity: 14.6177777777778,
		power: 13481.6306093387,
		road: 289.702083333333,
		acceleration: 0.769259259259261
	},
	{
		id: 77,
		time: 76,
		velocity: 15.0238888888889,
		power: 11162.1846111803,
		road: 304.373009259259,
		acceleration: 0.54759259259259
	},
	{
		id: 78,
		time: 77,
		velocity: 15.1925,
		power: 6769.22833292705,
		road: 319.425462962963,
		acceleration: 0.215462962962965
	},
	{
		id: 79,
		time: 78,
		velocity: 15.2641666666667,
		power: 4984.60822524253,
		road: 334.628333333333,
		acceleration: 0.0853703703703701
	},
	{
		id: 80,
		time: 79,
		velocity: 15.28,
		power: 275.07000474646,
		road: 349.755185185185,
		acceleration: -0.237407407407405
	},
	{
		id: 81,
		time: 80,
		velocity: 14.4802777777778,
		power: -3274.48137116093,
		road: 364.522083333333,
		acceleration: -0.482500000000002
	},
	{
		id: 82,
		time: 81,
		velocity: 13.8166666666667,
		power: -7416.20675292933,
		road: 378.652916666667,
		acceleration: -0.78962962962963
	},
	{
		id: 83,
		time: 82,
		velocity: 12.9111111111111,
		power: -6449.34848770882,
		road: 392.02212962963,
		acceleration: -0.733611111111109
	},
	{
		id: 84,
		time: 83,
		velocity: 12.2794444444444,
		power: -6667.91142815685,
		road: 404.638981481481,
		acceleration: -0.771111111111114
	},
	{
		id: 85,
		time: 84,
		velocity: 11.5033333333333,
		power: -4599.7614360625,
		road: 416.564259259259,
		acceleration: -0.612037037037036
	},
	{
		id: 86,
		time: 85,
		velocity: 11.075,
		power: -2344.23426316061,
		road: 427.97537037037,
		acceleration: -0.416296296296297
	},
	{
		id: 87,
		time: 86,
		velocity: 11.0305555555556,
		power: 3276.00657336728,
		road: 439.231527777778,
		acceleration: 0.10638888888889
	},
	{
		id: 88,
		time: 87,
		velocity: 11.8225,
		power: 10994.5323819746,
		road: 450.931203703704,
		acceleration: 0.780648148148146
	},
	{
		id: 89,
		time: 88,
		velocity: 13.4169444444444,
		power: 17097.7685491314,
		road: 463.618148148148,
		acceleration: 1.19388888888889
	},
	{
		id: 90,
		time: 89,
		velocity: 14.6122222222222,
		power: 20546.0741398135,
		road: 477.555324074074,
		acceleration: 1.30657407407407
	},
	{
		id: 91,
		time: 90,
		velocity: 15.7422222222222,
		power: 21984.8261516488,
		road: 492.772592592593,
		acceleration: 1.25361111111111
	},
	{
		id: 92,
		time: 91,
		velocity: 17.1777777777778,
		power: 24111.2745481157,
		road: 509.241851851852,
		acceleration: 1.25037037037037
	},
	{
		id: 93,
		time: 92,
		velocity: 18.3633333333333,
		power: 25031.2220194148,
		road: 526.924027777778,
		acceleration: 1.17546296296296
	},
	{
		id: 94,
		time: 93,
		velocity: 19.2686111111111,
		power: 18096.2463481664,
		road: 545.540185185185,
		acceleration: 0.692500000000003
	},
	{
		id: 95,
		time: 94,
		velocity: 19.2552777777778,
		power: 6857.49606632086,
		road: 564.525925925926,
		acceleration: 0.0466666666666669
	},
	{
		id: 96,
		time: 95,
		velocity: 18.5033333333333,
		power: -3490.07496362114,
		road: 583.275046296296,
		acceleration: -0.519907407407409
	},
	{
		id: 97,
		time: 96,
		velocity: 17.7088888888889,
		power: -6091.02529173767,
		road: 601.432361111111,
		acceleration: -0.663703703703703
	},
	{
		id: 98,
		time: 97,
		velocity: 17.2641666666667,
		power: -6390.72151941627,
		road: 618.916944444445,
		acceleration: -0.681759259259255
	},
	{
		id: 99,
		time: 98,
		velocity: 16.4580555555556,
		power: -4608.18763703434,
		road: 635.773935185185,
		acceleration: -0.573425925925928
	},
	{
		id: 100,
		time: 99,
		velocity: 15.9886111111111,
		power: -7235.9770068875,
		road: 651.972916666667,
		acceleration: -0.742592592592596
	},
	{
		id: 101,
		time: 100,
		velocity: 15.0363888888889,
		power: -5916.33085692644,
		road: 667.469444444445,
		acceleration: -0.662314814814813
	},
	{
		id: 102,
		time: 101,
		velocity: 14.4711111111111,
		power: -5227.20037667981,
		road: 682.324722222222,
		acceleration: -0.620185185185186
	},
	{
		id: 103,
		time: 102,
		velocity: 14.1280555555556,
		power: -5121.63372633589,
		road: 696.560694444445,
		acceleration: -0.618425925925926
	},
	{
		id: 104,
		time: 103,
		velocity: 13.1811111111111,
		power: -11285.6908529889,
		road: 709.931157407408,
		acceleration: -1.11259259259259
	},
	{
		id: 105,
		time: 104,
		velocity: 11.1333333333333,
		power: -18682.0453514163,
		road: 721.817638888889,
		acceleration: -1.85537037037037
	},
	{
		id: 106,
		time: 105,
		velocity: 8.56194444444444,
		power: -20454.7587709675,
		road: 731.586481481482,
		acceleration: -2.3799074074074
	},
	{
		id: 107,
		time: 106,
		velocity: 6.04138888888889,
		power: -14693.1188754518,
		road: 739.051203703704,
		acceleration: -2.22833333333333
	},
	{
		id: 108,
		time: 107,
		velocity: 4.44833333333333,
		power: -9740.12318929048,
		road: 744.365416666667,
		acceleration: -2.07268518518519
	},
	{
		id: 109,
		time: 108,
		velocity: 2.34388888888889,
		power: -4287.17003008636,
		road: 747.945046296297,
		acceleration: -1.39648148148148
	},
	{
		id: 110,
		time: 109,
		velocity: 1.85194444444444,
		power: -2357.11649361389,
		road: 750.213472222223,
		acceleration: -1.22592592592593
	},
	{
		id: 111,
		time: 110,
		velocity: 0.770555555555556,
		power: -782.84789638299,
		road: 751.478287037037,
		acceleration: -0.781296296296296
	},
	{
		id: 112,
		time: 111,
		velocity: 0,
		power: -262.379812797314,
		road: 752.043796296297,
		acceleration: -0.617314814814815
	},
	{
		id: 113,
		time: 112,
		velocity: 0,
		power: -15.7341586419753,
		road: 752.172222222223,
		acceleration: -0.256851851851852
	},
	{
		id: 114,
		time: 113,
		velocity: 0,
		power: 0,
		road: 752.172222222223,
		acceleration: 0
	},
	{
		id: 115,
		time: 114,
		velocity: 0,
		power: 426.402139482066,
		road: 752.615694444445,
		acceleration: 0.886944444444445
	},
	{
		id: 116,
		time: 115,
		velocity: 2.66083333333333,
		power: 2698.71249381063,
		road: 754.288564814815,
		acceleration: 1.57185185185185
	},
	{
		id: 117,
		time: 116,
		velocity: 4.71555555555556,
		power: 6078.69812047016,
		road: 757.636666666667,
		acceleration: 1.77861111111111
	},
	{
		id: 118,
		time: 117,
		velocity: 5.33583333333333,
		power: 6464.8123580334,
		road: 762.502824074074,
		acceleration: 1.2575
	},
	{
		id: 119,
		time: 118,
		velocity: 6.43333333333333,
		power: 6746.90479360353,
		road: 768.514120370371,
		acceleration: 1.03277777777778
	},
	{
		id: 120,
		time: 119,
		velocity: 7.81388888888889,
		power: 14043.6441448557,
		road: 775.95425925926,
		acceleration: 1.82490740740741
	},
	{
		id: 121,
		time: 120,
		velocity: 10.8105555555556,
		power: 15644.2374230118,
		road: 785.115833333334,
		acceleration: 1.61796296296296
	},
	{
		id: 122,
		time: 121,
		velocity: 11.2872222222222,
		power: 14025.2233892385,
		road: 795.687083333334,
		acceleration: 1.20138888888889
	},
	{
		id: 123,
		time: 122,
		velocity: 11.4180555555556,
		power: 2339.26712676012,
		road: 806.869768518519,
		acceleration: 0.0214814814814837
	},
	{
		id: 124,
		time: 123,
		velocity: 10.875,
		power: 1026.15319555347,
		road: 818.012777777778,
		acceleration: -0.100833333333334
	},
	{
		id: 125,
		time: 124,
		velocity: 10.9847222222222,
		power: 652.396873874602,
		road: 829.038425925926,
		acceleration: -0.133888888888889
	},
	{
		id: 126,
		time: 125,
		velocity: 11.0163888888889,
		power: 2549.22701909989,
		road: 840.021157407408,
		acceleration: 0.0480555555555551
	},
	{
		id: 127,
		time: 126,
		velocity: 11.0191666666667,
		power: 1963.0005142979,
		road: 851.023657407408,
		acceleration: -0.00851851851851926
	},
	{
		id: 128,
		time: 127,
		velocity: 10.9591666666667,
		power: 1543.56206023817,
		road: 861.99800925926,
		acceleration: -0.0477777777777799
	},
	{
		id: 129,
		time: 128,
		velocity: 10.8730555555556,
		power: 1725.22328296852,
		road: 872.933796296297,
		acceleration: -0.0293518518518496
	},
	{
		id: 130,
		time: 129,
		velocity: 10.9311111111111,
		power: 848.611639712199,
		road: 883.798888888889,
		acceleration: -0.112037037037036
	},
	{
		id: 131,
		time: 130,
		velocity: 10.6230555555556,
		power: -1001.76385390599,
		road: 894.462916666667,
		acceleration: -0.290092592592595
	},
	{
		id: 132,
		time: 131,
		velocity: 10.0027777777778,
		power: -3388.27095730323,
		road: 904.715138888889,
		acceleration: -0.533518518518518
	},
	{
		id: 133,
		time: 132,
		velocity: 9.33055555555556,
		power: -803.900299048296,
		road: 914.566712962963,
		acceleration: -0.267777777777779
	},
	{
		id: 134,
		time: 133,
		velocity: 9.81972222222222,
		power: 2503.26764353275,
		road: 924.328425925926,
		acceleration: 0.088055555555556
	},
	{
		id: 135,
		time: 134,
		velocity: 10.2669444444444,
		power: 6274.15109690165,
		road: 934.369907407408,
		acceleration: 0.471481481481483
	},
	{
		id: 136,
		time: 135,
		velocity: 10.745,
		power: 7010.11083237604,
		road: 944.901342592593,
		acceleration: 0.508425925925925
	},
	{
		id: 137,
		time: 136,
		velocity: 11.345,
		power: 5694.40925978982,
		road: 955.861898148148,
		acceleration: 0.349814814814813
	},
	{
		id: 138,
		time: 137,
		velocity: 11.3163888888889,
		power: 2950.25301096404,
		road: 967.036898148148,
		acceleration: 0.079074074074077
	},
	{
		id: 139,
		time: 138,
		velocity: 10.9822222222222,
		power: -639.522551454244,
		road: 978.122824074074,
		acceleration: -0.257222222222223
	},
	{
		id: 140,
		time: 139,
		velocity: 10.5733333333333,
		power: -2119.2693313929,
		road: 988.880462962963,
		acceleration: -0.399351851851852
	},
	{
		id: 141,
		time: 140,
		velocity: 10.1183333333333,
		power: 395.179628341815,
		road: 999.363518518519,
		acceleration: -0.149814814814814
	},
	{
		id: 142,
		time: 141,
		velocity: 10.5327777777778,
		power: 2597.35252860206,
		road: 1009.80768518519,
		acceleration: 0.0720370370370365
	},
	{
		id: 143,
		time: 142,
		velocity: 10.7894444444444,
		power: 5274.95498283071,
		road: 1020.45217592593,
		acceleration: 0.32861111111111
	},
	{
		id: 144,
		time: 143,
		velocity: 11.1041666666667,
		power: 5136.95940486065,
		road: 1031.40930555556,
		acceleration: 0.296666666666667
	},
	{
		id: 145,
		time: 144,
		velocity: 11.4227777777778,
		power: 6997.825248441,
		road: 1042.73875,
		acceleration: 0.447962962962965
	},
	{
		id: 146,
		time: 145,
		velocity: 12.1333333333333,
		power: 5822.22128443752,
		road: 1054.45041666667,
		acceleration: 0.316481481481478
	},
	{
		id: 147,
		time: 146,
		velocity: 12.0536111111111,
		power: 3799.61135987072,
		road: 1066.38337962963,
		acceleration: 0.126111111111113
	},
	{
		id: 148,
		time: 147,
		velocity: 11.8011111111111,
		power: -880.544434956523,
		road: 1078.23712962963,
		acceleration: -0.284537037037035
	},
	{
		id: 149,
		time: 148,
		velocity: 11.2797222222222,
		power: 172.360708494963,
		road: 1089.85462962963,
		acceleration: -0.187962962962963
	},
	{
		id: 150,
		time: 149,
		velocity: 11.4897222222222,
		power: 1953.60212477415,
		road: 1101.36606481481,
		acceleration: -0.0241666666666696
	},
	{
		id: 151,
		time: 150,
		velocity: 11.7286111111111,
		power: 4805.57394654496,
		road: 1112.98060185185,
		acceleration: 0.230370370370371
	},
	{
		id: 152,
		time: 151,
		velocity: 11.9708333333333,
		power: 4495.86549123291,
		road: 1124.80648148148,
		acceleration: 0.192314814814816
	},
	{
		id: 153,
		time: 152,
		velocity: 12.0666666666667,
		power: 4271.37036268495,
		road: 1136.81069444444,
		acceleration: 0.164351851851851
	},
	{
		id: 154,
		time: 153,
		velocity: 12.2216666666667,
		power: 3069.64791669956,
		road: 1148.92481481481,
		acceleration: 0.055462962962963
	},
	{
		id: 155,
		time: 154,
		velocity: 12.1372222222222,
		power: 2380.97958757449,
		road: 1161.06421296296,
		acceleration: -0.00490740740740847
	},
	{
		id: 156,
		time: 155,
		velocity: 12.0519444444444,
		power: 2493.55069240413,
		road: 1173.20356481481,
		acceleration: 0.00481481481481616
	},
	{
		id: 157,
		time: 156,
		velocity: 12.2361111111111,
		power: 3007.26057483211,
		road: 1185.36944444444,
		acceleration: 0.0482407407407397
	},
	{
		id: 158,
		time: 157,
		velocity: 12.2819444444444,
		power: 3794.2739595108,
		road: 1197.61583333333,
		acceleration: 0.112777777777778
	},
	{
		id: 159,
		time: 158,
		velocity: 12.3902777777778,
		power: 3141.11896031911,
		road: 1209.94555555556,
		acceleration: 0.0538888888888867
	},
	{
		id: 160,
		time: 159,
		velocity: 12.3977777777778,
		power: 3364.39646443217,
		road: 1222.3375,
		acceleration: 0.0705555555555559
	},
	{
		id: 161,
		time: 160,
		velocity: 12.4936111111111,
		power: 4252.43842301951,
		road: 1234.83527777778,
		acceleration: 0.141111111111114
	},
	{
		id: 162,
		time: 161,
		velocity: 12.8136111111111,
		power: 6059.16703345036,
		road: 1247.54416666667,
		acceleration: 0.281111111111109
	},
	{
		id: 163,
		time: 162,
		velocity: 13.2411111111111,
		power: 8659.83500116958,
		road: 1260.62833333333,
		acceleration: 0.469444444444445
	},
	{
		id: 164,
		time: 163,
		velocity: 13.9019444444444,
		power: 6612.15187759325,
		road: 1274.08967592593,
		acceleration: 0.284907407407408
	},
	{
		id: 165,
		time: 164,
		velocity: 13.6683333333333,
		power: 3955.07129985934,
		road: 1287.72916666667,
		acceleration: 0.0713888888888903
	},
	{
		id: 166,
		time: 165,
		velocity: 13.4552777777778,
		power: -1620.58999001996,
		road: 1301.22638888889,
		acceleration: -0.355925925925925
	},
	{
		id: 167,
		time: 166,
		velocity: 12.8341666666667,
		power: -5784.47565622888,
		road: 1314.20087962963,
		acceleration: -0.689537037037038
	},
	{
		id: 168,
		time: 167,
		velocity: 11.5997222222222,
		power: -8128.73353785248,
		road: 1326.37509259259,
		acceleration: -0.911018518518517
	},
	{
		id: 169,
		time: 168,
		velocity: 10.7222222222222,
		power: -10056.1479800687,
		road: 1337.52185185185,
		acceleration: -1.14388888888889
	},
	{
		id: 170,
		time: 169,
		velocity: 9.4025,
		power: -9451.11808118061,
		road: 1347.50810185185,
		acceleration: -1.17712962962963
	},
	{
		id: 171,
		time: 170,
		velocity: 8.06833333333333,
		power: -7608.58707206891,
		road: 1356.36898148148,
		acceleration: -1.07361111111111
	},
	{
		id: 172,
		time: 171,
		velocity: 7.50138888888889,
		power: -6883.71438318222,
		road: 1364.14722222222,
		acceleration: -1.09166666666667
	},
	{
		id: 173,
		time: 172,
		velocity: 6.1275,
		power: -6635.71579798167,
		road: 1370.77722222222,
		acceleration: -1.20481481481481
	},
	{
		id: 174,
		time: 173,
		velocity: 4.45388888888889,
		power: -5084.26749925207,
		road: 1376.24324074074,
		acceleration: -1.12314814814815
	},
	{
		id: 175,
		time: 174,
		velocity: 4.13194444444444,
		power: -2516.16522370571,
		road: 1380.78662037037,
		acceleration: -0.72212962962963
	},
	{
		id: 176,
		time: 175,
		velocity: 3.96111111111111,
		power: -260.11346868343,
		road: 1384.86699074074,
		acceleration: -0.203888888888889
	},
	{
		id: 177,
		time: 176,
		velocity: 3.84222222222222,
		power: -32.79786402167,
		road: 1388.77300925926,
		acceleration: -0.144814814814815
	},
	{
		id: 178,
		time: 177,
		velocity: 3.6975,
		power: 50.5089021603145,
		road: 1392.54597222222,
		acceleration: -0.121296296296296
	},
	{
		id: 179,
		time: 178,
		velocity: 3.59722222222222,
		power: 46.4845146699962,
		road: 1396.1975462963,
		acceleration: -0.121481481481482
	},
	{
		id: 180,
		time: 179,
		velocity: 3.47777777777778,
		power: -55.0258659699622,
		road: 1399.71296296296,
		acceleration: -0.150833333333333
	},
	{
		id: 181,
		time: 180,
		velocity: 3.245,
		power: -760.546115424519,
		road: 1402.96300925926,
		acceleration: -0.379907407407407
	},
	{
		id: 182,
		time: 181,
		velocity: 2.4575,
		power: -1120.33110959394,
		road: 1405.745,
		acceleration: -0.556203703703704
	},
	{
		id: 183,
		time: 182,
		velocity: 1.80916666666667,
		power: -867.52583741593,
		road: 1407.97907407407,
		acceleration: -0.53962962962963
	},
	{
		id: 184,
		time: 183,
		velocity: 1.62611111111111,
		power: -297.656691200132,
		road: 1409.79212962963,
		acceleration: -0.302407407407407
	},
	{
		id: 185,
		time: 184,
		velocity: 1.55027777777778,
		power: 342.007244456649,
		road: 1411.49532407407,
		acceleration: 0.0826851851851853
	},
	{
		id: 186,
		time: 185,
		velocity: 2.05722222222222,
		power: 1378.11211282411,
		road: 1413.53171296296,
		acceleration: 0.583703703703704
	},
	{
		id: 187,
		time: 186,
		velocity: 3.37722222222222,
		power: 2977.01982925945,
		road: 1416.35046296296,
		acceleration: 0.981018518518518
	},
	{
		id: 188,
		time: 187,
		velocity: 4.49333333333333,
		power: 5319.09296780064,
		road: 1420.30060185185,
		acceleration: 1.28175925925926
	},
	{
		id: 189,
		time: 188,
		velocity: 5.9025,
		power: 5406.35670358933,
		road: 1425.38064814815,
		acceleration: 0.978055555555556
	},
	{
		id: 190,
		time: 189,
		velocity: 6.31138888888889,
		power: 5340.71078053462,
		road: 1431.34680555556,
		acceleration: 0.794166666666666
	},
	{
		id: 191,
		time: 190,
		velocity: 6.87583333333333,
		power: 2861.79009690848,
		road: 1437.86523148148,
		acceleration: 0.310370370370371
	},
	{
		id: 192,
		time: 191,
		velocity: 6.83361111111111,
		power: 910.410267099032,
		road: 1444.53439814815,
		acceleration: -0.00888888888888939
	},
	{
		id: 193,
		time: 192,
		velocity: 6.28472222222222,
		power: -1223.34271855868,
		road: 1451.02444444444,
		acceleration: -0.349351851851851
	},
	{
		id: 194,
		time: 193,
		velocity: 5.82777777777778,
		power: -2618.01798072567,
		road: 1457.03689814815,
		acceleration: -0.605833333333334
	},
	{
		id: 195,
		time: 194,
		velocity: 5.01611111111111,
		power: -3166.30445020242,
		road: 1462.36185185185,
		acceleration: -0.769166666666667
	},
	{
		id: 196,
		time: 195,
		velocity: 3.97722222222222,
		power: -4482.95506857634,
		road: 1466.6875462963,
		acceleration: -1.22935185185185
	},
	{
		id: 197,
		time: 196,
		velocity: 2.13972222222222,
		power: -3561.74173990311,
		road: 1469.71162037037,
		acceleration: -1.37388888888889
	},
	{
		id: 198,
		time: 197,
		velocity: 0.894444444444444,
		power: -1899.60067798779,
		road: 1471.38587962963,
		acceleration: -1.32574074074074
	},
	{
		id: 199,
		time: 198,
		velocity: 0,
		power: -363.288885555068,
		road: 1472.04064814815,
		acceleration: -0.713240740740741
	},
	{
		id: 200,
		time: 199,
		velocity: 0,
		power: -24.0960717998701,
		road: 1472.18972222222,
		acceleration: -0.298148148148148
	},
	{
		id: 201,
		time: 200,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 202,
		time: 201,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 203,
		time: 202,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 204,
		time: 203,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 205,
		time: 204,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 206,
		time: 205,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 207,
		time: 206,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 208,
		time: 207,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 209,
		time: 208,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 210,
		time: 209,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 211,
		time: 210,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 212,
		time: 211,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 213,
		time: 212,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 214,
		time: 213,
		velocity: 0,
		power: 0,
		road: 1472.18972222222,
		acceleration: 0
	},
	{
		id: 215,
		time: 214,
		velocity: 0,
		power: 9.28733807505914,
		road: 1472.23476851852,
		acceleration: 0.0900925925925926
	},
	{
		id: 216,
		time: 215,
		velocity: 0.270277777777778,
		power: 43.8394259642142,
		road: 1472.40060185185,
		acceleration: 0.151481481481481
	},
	{
		id: 217,
		time: 216,
		velocity: 0.454444444444444,
		power: 481.341170203084,
		road: 1473.00115740741,
		acceleration: 0.717962962962963
	},
	{
		id: 218,
		time: 217,
		velocity: 2.15388888888889,
		power: 2370.90139297642,
		road: 1474.65291666667,
		acceleration: 1.38444444444444
	},
	{
		id: 219,
		time: 218,
		velocity: 4.42361111111111,
		power: 5446.39914750978,
		road: 1477.8325462963,
		acceleration: 1.6712962962963
	},
	{
		id: 220,
		time: 219,
		velocity: 5.46833333333333,
		power: 5516.99563973665,
		road: 1482.41226851852,
		acceleration: 1.12888888888889
	},
	{
		id: 221,
		time: 220,
		velocity: 5.54055555555556,
		power: 2096.39868516297,
		road: 1487.69375,
		acceleration: 0.274629629629629
	},
	{
		id: 222,
		time: 221,
		velocity: 5.2475,
		power: 776.950003193393,
		road: 1493.11597222222,
		acceleration: 0.00685185185185233
	},
	{
		id: 223,
		time: 222,
		velocity: 5.48888888888889,
		power: 1551.54677262162,
		road: 1498.61777777778,
		acceleration: 0.152314814814814
	},
	{
		id: 224,
		time: 223,
		velocity: 5.9975,
		power: 2332.97488291193,
		road: 1504.33740740741,
		acceleration: 0.283333333333333
	},
	{
		id: 225,
		time: 224,
		velocity: 6.0975,
		power: 2303.42627442086,
		road: 1510.32717592593,
		acceleration: 0.256944444444445
	},
	{
		id: 226,
		time: 225,
		velocity: 6.25972222222222,
		power: 1876.61191328775,
		road: 1516.53,
		acceleration: 0.169166666666666
	},
	{
		id: 227,
		time: 226,
		velocity: 6.505,
		power: 1575.57241102993,
		road: 1522.87300925926,
		acceleration: 0.111203703703704
	},
	{
		id: 228,
		time: 227,
		velocity: 6.43111111111111,
		power: 1467.06154673381,
		road: 1529.31597222222,
		acceleration: 0.088703703703704
	},
	{
		id: 229,
		time: 228,
		velocity: 6.52583333333333,
		power: 1184.47072604233,
		road: 1535.82337962963,
		acceleration: 0.0401851851851847
	},
	{
		id: 230,
		time: 229,
		velocity: 6.62555555555556,
		power: 1385.49272358786,
		road: 1542.38606481481,
		acceleration: 0.0703703703703704
	},
	{
		id: 231,
		time: 230,
		velocity: 6.64222222222222,
		power: 913.563152133036,
		road: 1548.98083333333,
		acceleration: -0.00620370370370438
	},
	{
		id: 232,
		time: 231,
		velocity: 6.50722222222222,
		power: 606.808502342815,
		road: 1555.54527777778,
		acceleration: -0.0544444444444441
	},
	{
		id: 233,
		time: 232,
		velocity: 6.46222222222222,
		power: 411.349805669533,
		road: 1562.04023148148,
		acceleration: -0.0845370370370375
	},
	{
		id: 234,
		time: 233,
		velocity: 6.38861111111111,
		power: 452.356587122815,
		road: 1568.45472222222,
		acceleration: -0.0763888888888884
	},
	{
		id: 235,
		time: 234,
		velocity: 6.27805555555556,
		power: 683.187372051856,
		road: 1574.8124537037,
		acceleration: -0.0371296296296295
	},
	{
		id: 236,
		time: 235,
		velocity: 6.35083333333333,
		power: 540.922533426369,
		road: 1581.12180555556,
		acceleration: -0.0596296296296286
	},
	{
		id: 237,
		time: 236,
		velocity: 6.20972222222222,
		power: 309.850399124272,
		road: 1587.35287037037,
		acceleration: -0.0969444444444454
	},
	{
		id: 238,
		time: 237,
		velocity: 5.98722222222222,
		power: -1669.62402023266,
		road: 1593.31444444444,
		acceleration: -0.442037037037037
	},
	{
		id: 239,
		time: 238,
		velocity: 5.02472222222222,
		power: -3783.16030659095,
		road: 1598.60726851852,
		acceleration: -0.895462962962963
	},
	{
		id: 240,
		time: 239,
		velocity: 3.52333333333333,
		power: -5059.91884339009,
		road: 1602.73888888889,
		acceleration: -1.42694444444444
	},
	{
		id: 241,
		time: 240,
		velocity: 1.70638888888889,
		power: -3315.18855065559,
		road: 1605.44574074074,
		acceleration: -1.42259259259259
	},
	{
		id: 242,
		time: 241,
		velocity: 0.756944444444444,
		power: -1396.28879933616,
		road: 1606.85407407407,
		acceleration: -1.17444444444444
	},
	{
		id: 243,
		time: 242,
		velocity: 0,
		power: -224.349879495679,
		road: 1607.39078703704,
		acceleration: -0.568796296296296
	},
	{
		id: 244,
		time: 243,
		velocity: 0,
		power: -14.9139745776478,
		road: 1607.51694444444,
		acceleration: -0.252314814814815
	},
	{
		id: 245,
		time: 244,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 246,
		time: 245,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 247,
		time: 246,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 248,
		time: 247,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 249,
		time: 248,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 250,
		time: 249,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 251,
		time: 250,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 252,
		time: 251,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 253,
		time: 252,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 254,
		time: 253,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 255,
		time: 254,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 256,
		time: 255,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 257,
		time: 256,
		velocity: 0,
		power: 0,
		road: 1607.51694444444,
		acceleration: 0
	},
	{
		id: 258,
		time: 257,
		velocity: 0,
		power: 321.222601734436,
		road: 1607.89796296296,
		acceleration: 0.762037037037037
	},
	{
		id: 259,
		time: 258,
		velocity: 2.28611111111111,
		power: 1968.56558875397,
		road: 1609.3237037037,
		acceleration: 1.32740740740741
	},
	{
		id: 260,
		time: 259,
		velocity: 3.98222222222222,
		power: 5046.06144958023,
		road: 1612.25412037037,
		acceleration: 1.68194444444444
	},
	{
		id: 261,
		time: 260,
		velocity: 5.04583333333333,
		power: 5070.22576630653,
		road: 1616.57435185185,
		acceleration: 1.09768518518519
	},
	{
		id: 262,
		time: 261,
		velocity: 5.57916666666667,
		power: 3278.89598386542,
		road: 1621.70837962963,
		acceleration: 0.529907407407407
	},
	{
		id: 263,
		time: 262,
		velocity: 5.57194444444445,
		power: 2105.76502244543,
		road: 1627.23550925926,
		acceleration: 0.256296296296296
	},
	{
		id: 264,
		time: 263,
		velocity: 5.81472222222222,
		power: 1777.44652100623,
		road: 1632.98055555556,
		acceleration: 0.179537037037038
	},
	{
		id: 265,
		time: 264,
		velocity: 6.11777777777778,
		power: 2163.37038790625,
		road: 1638.93287037037,
		acceleration: 0.234999999999999
	},
	{
		id: 266,
		time: 265,
		velocity: 6.27694444444444,
		power: 1987.86617274592,
		road: 1645.09787037037,
		acceleration: 0.190370370370371
	},
	{
		id: 267,
		time: 266,
		velocity: 6.38583333333333,
		power: 1906.23794753446,
		road: 1651.44106481481,
		acceleration: 0.166018518518518
	},
	{
		id: 268,
		time: 267,
		velocity: 6.61583333333333,
		power: 2388.98369487229,
		road: 1657.98351851852,
		acceleration: 0.2325
	},
	{
		id: 269,
		time: 268,
		velocity: 6.97444444444444,
		power: 3226.19357407185,
		road: 1664.8137037037,
		acceleration: 0.342962962962963
	},
	{
		id: 270,
		time: 269,
		velocity: 7.41472222222222,
		power: 4105.89527659067,
		road: 1672.03578703704,
		acceleration: 0.440833333333334
	},
	{
		id: 271,
		time: 270,
		velocity: 7.93833333333333,
		power: 5074.25705817653,
		road: 1679.74375,
		acceleration: 0.530925925925926
	},
	{
		id: 272,
		time: 271,
		velocity: 8.56722222222222,
		power: 4217.18503400841,
		road: 1687.90611111111,
		acceleration: 0.37787037037037
	},
	{
		id: 273,
		time: 272,
		velocity: 8.54833333333333,
		power: 1567.62221600955,
		road: 1696.27240740741,
		acceleration: 0.0300000000000011
	},
	{
		id: 274,
		time: 273,
		velocity: 8.02833333333333,
		power: -1582.85101634665,
		road: 1704.46962962963,
		acceleration: -0.368148148148148
	},
	{
		id: 275,
		time: 274,
		velocity: 7.46277777777778,
		power: -1733.50550717098,
		road: 1712.28532407407,
		acceleration: -0.394907407407408
	},
	{
		id: 276,
		time: 275,
		velocity: 7.36361111111111,
		power: -569.428374166193,
		road: 1719.78412037037,
		acceleration: -0.23888888888889
	},
	{
		id: 277,
		time: 276,
		velocity: 7.31166666666667,
		power: 774.963767368391,
		road: 1727.13990740741,
		acceleration: -0.0471296296296293
	},
	{
		id: 278,
		time: 277,
		velocity: 7.32138888888889,
		power: 700.22261884424,
		road: 1734.4437962963,
		acceleration: -0.0566666666666666
	},
	{
		id: 279,
		time: 278,
		velocity: 7.19361111111111,
		power: 812.392438973267,
		road: 1741.69967592593,
		acceleration: -0.0393518518518512
	},
	{
		id: 280,
		time: 279,
		velocity: 7.19361111111111,
		power: 478.052091455743,
		road: 1748.8925462963,
		acceleration: -0.0866666666666678
	},
	{
		id: 281,
		time: 280,
		velocity: 7.06138888888889,
		power: 1134.46552456545,
		road: 1756.04731481482,
		acceleration: 0.010462962962964
	},
	{
		id: 282,
		time: 281,
		velocity: 7.225,
		power: 1939.89698820832,
		road: 1763.27009259259,
		acceleration: 0.125555555555555
	},
	{
		id: 283,
		time: 282,
		velocity: 7.57027777777778,
		power: 4986.63819978456,
		road: 1770.82282407407,
		acceleration: 0.534351851851852
	},
	{
		id: 284,
		time: 283,
		velocity: 8.66444444444444,
		power: 8224.26818192533,
		road: 1779.08277777778,
		acceleration: 0.88009259259259
	},
	{
		id: 285,
		time: 284,
		velocity: 9.86527777777778,
		power: 11898.3375514111,
		road: 1788.3674537037,
		acceleration: 1.16935185185185
	},
	{
		id: 286,
		time: 285,
		velocity: 11.0783333333333,
		power: 9359.80580999896,
		road: 1798.62231481482,
		acceleration: 0.771018518518519
	},
	{
		id: 287,
		time: 286,
		velocity: 10.9775,
		power: 8595.45643557537,
		road: 1809.57666666667,
		acceleration: 0.627962962962963
	},
	{
		id: 288,
		time: 287,
		velocity: 11.7491666666667,
		power: 7314.41890237777,
		road: 1821.0774537037,
		acceleration: 0.464907407407408
	},
	{
		id: 289,
		time: 288,
		velocity: 12.4730555555556,
		power: 11986.6279936923,
		road: 1833.22263888889,
		acceleration: 0.823888888888888
	},
	{
		id: 290,
		time: 289,
		velocity: 13.4491666666667,
		power: 12531.4692928674,
		road: 1846.17555555556,
		acceleration: 0.791574074074076
	},
	{
		id: 291,
		time: 290,
		velocity: 14.1238888888889,
		power: 11889.3697948078,
		road: 1859.86259259259,
		acceleration: 0.676666666666664
	},
	{
		id: 292,
		time: 291,
		velocity: 14.5030555555556,
		power: 8709.85417086959,
		road: 1874.08773148148,
		acceleration: 0.399537037037037
	},
	{
		id: 293,
		time: 292,
		velocity: 14.6477777777778,
		power: 6631.92418870276,
		road: 1888.62805555556,
		acceleration: 0.230833333333335
	},
	{
		id: 294,
		time: 293,
		velocity: 14.8163888888889,
		power: 5379.81448733366,
		road: 1903.35023148148,
		acceleration: 0.132870370370371
	},
	{
		id: 295,
		time: 294,
		velocity: 14.9016666666667,
		power: 5143.63655438169,
		road: 1918.19435185185,
		acceleration: 0.111018518518518
	},
	{
		id: 296,
		time: 295,
		velocity: 14.9808333333333,
		power: 3835.87573300109,
		road: 1933.10226851852,
		acceleration: 0.016574074074077
	},
	{
		id: 297,
		time: 296,
		velocity: 14.8661111111111,
		power: 2677.15023798443,
		road: 1947.98643518519,
		acceleration: -0.0640740740740764
	},
	{
		id: 298,
		time: 297,
		velocity: 14.7094444444444,
		power: -472.378485068406,
		road: 1962.69703703704,
		acceleration: -0.283055555555556
	},
	{
		id: 299,
		time: 298,
		velocity: 14.1316666666667,
		power: -1821.70658214591,
		road: 1977.07777777778,
		acceleration: -0.376666666666667
	},
	{
		id: 300,
		time: 299,
		velocity: 13.7361111111111,
		power: -3793.41122583181,
		road: 1991.00916666667,
		acceleration: -0.522037037037036
	},
	{
		id: 301,
		time: 300,
		velocity: 13.1433333333333,
		power: -3937.31886895631,
		road: 2004.4112962963,
		acceleration: -0.536481481481481
	},
	{
		id: 302,
		time: 301,
		velocity: 12.5222222222222,
		power: -3562.54153968878,
		road: 2017.28976851852,
		acceleration: -0.510833333333332
	},
	{
		id: 303,
		time: 302,
		velocity: 12.2036111111111,
		power: -1808.17163578457,
		road: 2029.72930555556,
		acceleration: -0.367037037037042
	},
	{
		id: 304,
		time: 303,
		velocity: 12.0422222222222,
		power: 1338.9508960909,
		road: 2041.93708333333,
		acceleration: -0.0964814814814812
	},
	{
		id: 305,
		time: 304,
		velocity: 12.2327777777778,
		power: 5013.75254667619,
		road: 2054.20467592593,
		acceleration: 0.216111111111115
	},
	{
		id: 306,
		time: 305,
		velocity: 12.8519444444444,
		power: 7330.37142604979,
		road: 2066.7775462963,
		acceleration: 0.394444444444442
	},
	{
		id: 307,
		time: 306,
		velocity: 13.2255555555556,
		power: 8409.4808412257,
		road: 2079.77523148148,
		acceleration: 0.455185185185185
	},
	{
		id: 308,
		time: 307,
		velocity: 13.5983333333333,
		power: 6333.10326997936,
		road: 2093.13476851852,
		acceleration: 0.268518518518519
	},
	{
		id: 309,
		time: 308,
		velocity: 13.6575,
		power: 4883.62525096247,
		road: 2106.70148148148,
		acceleration: 0.145833333333334
	},
	{
		id: 310,
		time: 309,
		velocity: 13.6630555555556,
		power: 3859.44901561555,
		road: 2120.3725462963,
		acceleration: 0.062870370370371
	},
	{
		id: 311,
		time: 310,
		velocity: 13.7869444444444,
		power: 5264.40013933153,
		road: 2134.15773148148,
		acceleration: 0.165370370370368
	},
	{
		id: 312,
		time: 311,
		velocity: 14.1536111111111,
		power: 8041.0333854037,
		road: 2148.20592592593,
		acceleration: 0.360648148148149
	},
	{
		id: 313,
		time: 312,
		velocity: 14.745,
		power: 8101.93972186497,
		road: 2162.60675925926,
		acceleration: 0.34462962962963
	},
	{
		id: 314,
		time: 313,
		velocity: 14.8208333333333,
		power: 7936.1630882159,
		road: 2177.33699074074,
		acceleration: 0.314166666666667
	},
	{
		id: 315,
		time: 314,
		velocity: 15.0961111111111,
		power: 7742.61513511531,
		road: 2192.36648148148,
		acceleration: 0.284351851851852
	},
	{
		id: 316,
		time: 315,
		velocity: 15.5980555555556,
		power: 8117.36956336212,
		road: 2207.68550925926,
		acceleration: 0.294722222222219
	},
	{
		id: 317,
		time: 316,
		velocity: 15.705,
		power: 9792.01858058503,
		road: 2223.34611111111,
		acceleration: 0.388425925925928
	},
	{
		id: 318,
		time: 317,
		velocity: 16.2613888888889,
		power: 8675.71760602843,
		road: 2239.34856481481,
		acceleration: 0.295277777777775
	},
	{
		id: 319,
		time: 318,
		velocity: 16.4838888888889,
		power: 7677.64972378488,
		road: 2255.60736111111,
		acceleration: 0.217407407407411
	},
	{
		id: 320,
		time: 319,
		velocity: 16.3572222222222,
		power: 4797.77590203496,
		road: 2271.98865740741,
		acceleration: 0.0275925925925904
	},
	{
		id: 321,
		time: 320,
		velocity: 16.3441666666667,
		power: 3813.86147820349,
		road: 2288.3662037037,
		acceleration: -0.0350925925925907
	},
	{
		id: 322,
		time: 321,
		velocity: 16.3786111111111,
		power: 4056.03394665473,
		road: 2304.71685185185,
		acceleration: -0.0187037037037037
	},
	{
		id: 323,
		time: 322,
		velocity: 16.3011111111111,
		power: 5606.90125669979,
		road: 2321.09777777778,
		acceleration: 0.0792592592592598
	},
	{
		id: 324,
		time: 323,
		velocity: 16.5819444444444,
		power: 6592.9013324359,
		road: 2337.58703703704,
		acceleration: 0.137407407407409
	},
	{
		id: 325,
		time: 324,
		velocity: 16.7908333333333,
		power: 7359.64862005847,
		road: 2354.23430555556,
		acceleration: 0.17861111111111
	},
	{
		id: 326,
		time: 325,
		velocity: 16.8369444444444,
		power: 6506.96477244353,
		road: 2371.03018518519,
		acceleration: 0.118611111111115
	},
	{
		id: 327,
		time: 326,
		velocity: 16.9377777777778,
		power: 4535.6603447877,
		road: 2387.88231481481,
		acceleration: -0.0061111111111174
	},
	{
		id: 328,
		time: 327,
		velocity: 16.7725,
		power: 181.29928130304,
		road: 2404.59462962963,
		acceleration: -0.273518518518515
	},
	{
		id: 329,
		time: 328,
		velocity: 16.0163888888889,
		power: -2254.98219097367,
		road: 2420.95893518518,
		acceleration: -0.422500000000003
	},
	{
		id: 330,
		time: 329,
		velocity: 15.6702777777778,
		power: -4329.49507692865,
		road: 2436.83458333333,
		acceleration: -0.554814814814813
	},
	{
		id: 331,
		time: 330,
		velocity: 15.1080555555556,
		power: -3206.43956366582,
		road: 2452.19337962963,
		acceleration: -0.478888888888889
	},
	{
		id: 332,
		time: 331,
		velocity: 14.5797222222222,
		power: -2565.93343523538,
		road: 2467.0962962963,
		acceleration: -0.43287037037037
	},
	{
		id: 333,
		time: 332,
		velocity: 14.3716666666667,
		power: 979.781507097223,
		road: 2481.69412037037,
		acceleration: -0.177314814814814
	},
	{
		id: 334,
		time: 333,
		velocity: 14.5761111111111,
		power: 3940.24130227016,
		road: 2496.22203703704,
		acceleration: 0.0374999999999996
	},
	{
		id: 335,
		time: 334,
		velocity: 14.6922222222222,
		power: 4954.34161707403,
		road: 2510.8225,
		acceleration: 0.10759259259259
	},
	{
		id: 336,
		time: 335,
		velocity: 14.6944444444444,
		power: 3691.2034992559,
		road: 2525.48421296296,
		acceleration: 0.0149074074074065
	},
	{
		id: 337,
		time: 336,
		velocity: 14.6208333333333,
		power: 4287.01028765582,
		road: 2540.18143518518,
		acceleration: 0.0561111111111128
	},
	{
		id: 338,
		time: 337,
		velocity: 14.8605555555556,
		power: 4594.71183518047,
		road: 2554.94444444444,
		acceleration: 0.0754629629629644
	},
	{
		id: 339,
		time: 338,
		velocity: 14.9208333333333,
		power: 6246.86972797272,
		road: 2569.83842592593,
		acceleration: 0.186481481481481
	},
	{
		id: 340,
		time: 339,
		velocity: 15.1802777777778,
		power: 7079.54056376439,
		road: 2584.94287037037,
		acceleration: 0.234444444444444
	},
	{
		id: 341,
		time: 340,
		velocity: 15.5638888888889,
		power: 8549.59518450872,
		road: 2600.32490740741,
		acceleration: 0.320740740740742
	},
	{
		id: 342,
		time: 341,
		velocity: 15.8830555555556,
		power: 7757.5552772973,
		road: 2615.99337962963,
		acceleration: 0.252129629629628
	},
	{
		id: 343,
		time: 342,
		velocity: 15.9366666666667,
		power: 4471.55049418185,
		road: 2631.80171296296,
		acceleration: 0.0275925925925922
	},
	{
		id: 344,
		time: 343,
		velocity: 15.6466666666667,
		power: -401.792008201614,
		road: 2647.4774537037,
		acceleration: -0.292777777777777
	},
	{
		id: 345,
		time: 344,
		velocity: 15.0047222222222,
		power: -1969.02255975955,
		road: 2662.80962962963,
		acceleration: -0.394351851851852
	},
	{
		id: 346,
		time: 345,
		velocity: 14.7536111111111,
		power: -214.516283217792,
		road: 2677.81,
		acceleration: -0.269259259259259
	},
	{
		id: 347,
		time: 346,
		velocity: 14.8388888888889,
		power: 8373.26049130818,
		road: 2692.83986111111,
		acceleration: 0.328240740740741
	},
	{
		id: 348,
		time: 347,
		velocity: 15.9894444444444,
		power: 12949.3461464659,
		road: 2708.33944444444,
		acceleration: 0.611203703703701
	},
	{
		id: 349,
		time: 348,
		velocity: 16.5872222222222,
		power: 26301.9617156564,
		road: 2724.83759259259,
		acceleration: 1.38592592592593
	},
	{
		id: 350,
		time: 349,
		velocity: 18.9966666666667,
		power: 33686.4425528656,
		road: 2742.85032407407,
		acceleration: 1.64324074074074
	},
	{
		id: 351,
		time: 350,
		velocity: 20.9191666666667,
		power: 47280.2920590066,
		road: 2762.74902777778,
		acceleration: 2.12870370370371
	},
	{
		id: 352,
		time: 351,
		velocity: 22.9733333333333,
		power: 50336.0545387216,
		road: 2784.70722222222,
		acceleration: 1.99027777777778
	},
	{
		id: 353,
		time: 352,
		velocity: 24.9675,
		power: 53079.8145547923,
		road: 2808.59314814815,
		acceleration: 1.86518518518518
	},
	{
		id: 354,
		time: 353,
		velocity: 26.5147222222222,
		power: 51393.2417205612,
		road: 2834.20703703704,
		acceleration: 1.59074074074074
	},
	{
		id: 355,
		time: 354,
		velocity: 27.7455555555556,
		power: 45677.5360961578,
		road: 2861.22606481481,
		acceleration: 1.21953703703704
	},
	{
		id: 356,
		time: 355,
		velocity: 28.6261111111111,
		power: 26086.5841412309,
		road: 2889.06046296296,
		acceleration: 0.411203703703706
	},
	{
		id: 357,
		time: 356,
		velocity: 27.7483333333333,
		power: -3118.81074065861,
		road: 2916.76273148148,
		acceleration: -0.675462962962964
	},
	{
		id: 358,
		time: 357,
		velocity: 25.7191666666667,
		power: -28855.4731887895,
		road: 2943.30023148148,
		acceleration: -1.65407407407407
	},
	{
		id: 359,
		time: 358,
		velocity: 23.6638888888889,
		power: -43422.4622617426,
		road: 2967.85675925926,
		acceleration: -2.30787037037037
	},
	{
		id: 360,
		time: 359,
		velocity: 20.8247222222222,
		power: -39205.1694316418,
		road: 2990.13865740741,
		acceleration: -2.24138888888889
	},
	{
		id: 361,
		time: 360,
		velocity: 18.995,
		power: -27286.6265493773,
		road: 3010.41814814815,
		acceleration: -1.76342592592593
	},
	{
		id: 362,
		time: 361,
		velocity: 18.3736111111111,
		power: -15049.5437890226,
		road: 3029.23476851852,
		acceleration: -1.16231481481481
	},
	{
		id: 363,
		time: 362,
		velocity: 17.3377777777778,
		power: -7801.54480919902,
		road: 3047.08837962963,
		acceleration: -0.763703703703708
	},
	{
		id: 364,
		time: 363,
		velocity: 16.7038888888889,
		power: -8753.7832845389,
		road: 3064.14615740741,
		acceleration: -0.82796296296296
	},
	{
		id: 365,
		time: 364,
		velocity: 15.8897222222222,
		power: -4304.86623832736,
		road: 3080.51319444444,
		acceleration: -0.553518518518523
	},
	{
		id: 366,
		time: 365,
		velocity: 15.6772222222222,
		power: -2188.54058398378,
		road: 3096.39657407407,
		acceleration: -0.413796296296294
	},
	{
		id: 367,
		time: 366,
		velocity: 15.4625,
		power: 653.803796658344,
		road: 3111.96300925926,
		acceleration: -0.220092592592591
	},
	{
		id: 368,
		time: 367,
		velocity: 15.2294444444444,
		power: 5231.22461131479,
		road: 3127.46449074074,
		acceleration: 0.0901851851851827
	},
	{
		id: 369,
		time: 368,
		velocity: 15.9477777777778,
		power: 1073.36355819163,
		road: 3142.91638888889,
		acceleration: -0.189351851851852
	},
	{
		id: 370,
		time: 369,
		velocity: 14.8944444444444,
		power: 1764.43107286105,
		road: 3158.20439814815,
		acceleration: -0.138425925925926
	},
	{
		id: 371,
		time: 370,
		velocity: 14.8141666666667,
		power: -2593.77300873811,
		road: 3173.20555555555,
		acceleration: -0.435277777777776
	},
	{
		id: 372,
		time: 371,
		velocity: 14.6419444444444,
		power: 1405.97686701378,
		road: 3187.91439814815,
		acceleration: -0.149351851851854
	},
	{
		id: 373,
		time: 372,
		velocity: 14.4463888888889,
		power: 171.490824094811,
		road: 3202.43162037037,
		acceleration: -0.233888888888886
	},
	{
		id: 374,
		time: 373,
		velocity: 14.1125,
		power: 433.041970200725,
		road: 3216.72643518518,
		acceleration: -0.210925925925928
	},
	{
		id: 375,
		time: 374,
		velocity: 14.0091666666667,
		power: 535.836264038048,
		road: 3230.81601851852,
		acceleration: -0.199537037037038
	},
	{
		id: 376,
		time: 375,
		velocity: 13.8477777777778,
		power: 239.994191261337,
		road: 3244.69685185185,
		acceleration: -0.217962962962963
	},
	{
		id: 377,
		time: 376,
		velocity: 13.4586111111111,
		power: 87.3522128026666,
		road: 3258.35574074074,
		acceleration: -0.225925925925925
	},
	{
		id: 378,
		time: 377,
		velocity: 13.3313888888889,
		power: 277.510196707333,
		road: 3271.79782407407,
		acceleration: -0.207685185185188
	},
	{
		id: 379,
		time: 378,
		velocity: 13.2247222222222,
		power: 1045.81256152166,
		road: 3285.06402777778,
		acceleration: -0.144074074074073
	},
	{
		id: 380,
		time: 379,
		velocity: 13.0263888888889,
		power: 73.8943074173884,
		road: 3298.14916666667,
		acceleration: -0.218055555555557
	},
	{
		id: 381,
		time: 380,
		velocity: 12.6772222222222,
		power: -624.84876402343,
		road: 3310.98958333333,
		acceleration: -0.271388888888888
	},
	{
		id: 382,
		time: 381,
		velocity: 12.4105555555556,
		power: 3408.37065810829,
		road: 3323.72509259259,
		acceleration: 0.0615740740740751
	},
	{
		id: 383,
		time: 382,
		velocity: 13.2111111111111,
		power: 8140.63000450615,
		road: 3336.70861111111,
		acceleration: 0.434444444444445
	},
	{
		id: 384,
		time: 383,
		velocity: 13.9805555555556,
		power: 11987.4808242154,
		road: 3350.25722222222,
		acceleration: 0.695740740740741
	},
	{
		id: 385,
		time: 384,
		velocity: 14.4977777777778,
		power: 13709.6301907061,
		road: 3364.53513888889,
		acceleration: 0.76287037037037
	},
	{
		id: 386,
		time: 385,
		velocity: 15.4997222222222,
		power: 22867.1096550815,
		road: 3379.84597222222,
		acceleration: 1.30296296296296
	},
	{
		id: 387,
		time: 386,
		velocity: 17.8894444444444,
		power: 30247.9311509232,
		road: 3396.60851851852,
		acceleration: 1.60046296296296
	},
	{
		id: 388,
		time: 387,
		velocity: 19.2991666666667,
		power: 41334.5008394501,
		road: 3415.17296296296,
		acceleration: 2.00333333333333
	},
	{
		id: 389,
		time: 388,
		velocity: 21.5097222222222,
		power: 38184.8694222312,
		road: 3435.53666666667,
		acceleration: 1.59518518518519
	},
	{
		id: 390,
		time: 389,
		velocity: 22.675,
		power: 45860.6882095302,
		road: 3457.58152777778,
		acceleration: 1.76712962962963
	},
	{
		id: 391,
		time: 390,
		velocity: 24.6005555555556,
		power: 42804.1202418532,
		road: 3481.23125,
		acceleration: 1.44259259259259
	},
	{
		id: 392,
		time: 391,
		velocity: 25.8375,
		power: 42733.550680345,
		road: 3506.25115740741,
		acceleration: 1.29777777777778
	},
	{
		id: 393,
		time: 392,
		velocity: 26.5683333333333,
		power: 42567.8869410389,
		road: 3532.50550925926,
		acceleration: 1.17111111111111
	},
	{
		id: 394,
		time: 393,
		velocity: 28.1138888888889,
		power: 43480.2301791732,
		road: 3559.89578703704,
		acceleration: 1.10074074074074
	},
	{
		id: 395,
		time: 394,
		velocity: 29.1397222222222,
		power: 49902.6130952399,
		road: 3588.4512037037,
		acceleration: 1.22953703703704
	},
	{
		id: 396,
		time: 395,
		velocity: 30.2569444444444,
		power: 35634.4668823274,
		road: 3617.94078703704,
		acceleration: 0.638796296296295
	},
	{
		id: 397,
		time: 396,
		velocity: 30.0302777777778,
		power: 33360.8797349437,
		road: 3648.00810185185,
		acceleration: 0.516666666666666
	},
	{
		id: 398,
		time: 397,
		velocity: 30.6897222222222,
		power: 31137.028859813,
		road: 3678.53763888889,
		acceleration: 0.407777777777781
	},
	{
		id: 399,
		time: 398,
		velocity: 31.4802777777778,
		power: 39671.601174218,
		road: 3709.59907407407,
		acceleration: 0.656018518518511
	},
	{
		id: 400,
		time: 399,
		velocity: 31.9983333333333,
		power: 44154.2779770532,
		road: 3741.3625,
		acceleration: 0.747962962962962
	},
	{
		id: 401,
		time: 400,
		velocity: 32.9336111111111,
		power: 41446.5048230655,
		road: 3773.80324074074,
		acceleration: 0.606666666666669
	},
	{
		id: 402,
		time: 401,
		velocity: 33.3002777777778,
		power: 26536.4874757503,
		road: 3806.60134259259,
		acceleration: 0.108055555555552
	},
	{
		id: 403,
		time: 402,
		velocity: 32.3225,
		power: 3959.88307072242,
		road: 3839.15578703704,
		acceleration: -0.595370370370361
	},
	{
		id: 404,
		time: 403,
		velocity: 31.1475,
		power: -9884.99379733013,
		road: 3870.90481481481,
		acceleration: -1.01546296296296
	},
	{
		id: 405,
		time: 404,
		velocity: 30.2538888888889,
		power: -5274.99205517169,
		road: 3901.72717592592,
		acceleration: -0.837870370370371
	},
	{
		id: 406,
		time: 405,
		velocity: 29.8088888888889,
		power: 3460.40765164051,
		road: 3931.87152777778,
		acceleration: -0.51814814814815
	},
	{
		id: 407,
		time: 406,
		velocity: 29.5930555555556,
		power: 16860.6363616802,
		road: 3961.73583333333,
		acceleration: -0.041944444444443
	},
	{
		id: 408,
		time: 407,
		velocity: 30.1280555555556,
		power: 26630.5464822993,
		road: 3991.72393518518,
		acceleration: 0.289537037037036
	},
	{
		id: 409,
		time: 408,
		velocity: 30.6775,
		power: 28994.0414309482,
		road: 4022.03171296296,
		acceleration: 0.349814814814817
	},
	{
		id: 410,
		time: 409,
		velocity: 30.6425,
		power: 19112.4292716205,
		road: 4052.51527777778,
		acceleration: 0.00175925925925924
	},
	{
		id: 411,
		time: 410,
		velocity: 30.1333333333333,
		power: 5772.36358956299,
		road: 4082.77805555555,
		acceleration: -0.443333333333335
	},
	{
		id: 412,
		time: 411,
		velocity: 29.3475,
		power: -4923.96468500915,
		road: 4112.42263888889,
		acceleration: -0.793055555555554
	},
	{
		id: 413,
		time: 412,
		velocity: 28.2633333333333,
		power: -3610.59679277771,
		road: 4141.30787037037,
		acceleration: -0.725648148148146
	},
	{
		id: 414,
		time: 413,
		velocity: 27.9563888888889,
		power: -1960.40939154776,
		road: 4169.50717592592,
		acceleration: -0.646203703703705
	},
	{
		id: 415,
		time: 414,
		velocity: 27.4088888888889,
		power: 1456.55589719583,
		road: 4197.13273148148,
		acceleration: -0.501296296296296
	},
	{
		id: 416,
		time: 415,
		velocity: 26.7594444444444,
		power: 13839.0134717725,
		road: 4224.49643518518,
		acceleration: -0.0224074074074068
	},
	{
		id: 417,
		time: 416,
		velocity: 27.8891666666667,
		power: 25449.8226547234,
		road: 4252.05185185185,
		acceleration: 0.405833333333327
	},
	{
		id: 418,
		time: 417,
		velocity: 28.6263888888889,
		power: 43498.8557226154,
		road: 4280.3212037037,
		acceleration: 1.02203703703704
	},
	{
		id: 419,
		time: 418,
		velocity: 29.8255555555556,
		power: 34876.8245993286,
		road: 4309.42231481481,
		acceleration: 0.641481481481485
	},
	{
		id: 420,
		time: 419,
		velocity: 29.8136111111111,
		power: 24659.8410776075,
		road: 4338.96865740741,
		acceleration: 0.248981481481479
	},
	{
		id: 421,
		time: 420,
		velocity: 29.3733333333333,
		power: 8777.95846114508,
		road: 4368.48564814815,
		acceleration: -0.307685185185186
	},
	{
		id: 422,
		time: 421,
		velocity: 28.9025,
		power: 5127.1873644304,
		road: 4397.63787037037,
		acceleration: -0.421851851851848
	},
	{
		id: 423,
		time: 422,
		velocity: 28.5480555555556,
		power: 7415.07946415026,
		road: 4426.41675925926,
		acceleration: -0.324814814814815
	},
	{
		id: 424,
		time: 423,
		velocity: 28.3988888888889,
		power: 342.779697923079,
		road: 4454.75041666666,
		acceleration: -0.565648148148149
	},
	{
		id: 425,
		time: 424,
		velocity: 27.2055555555556,
		power: -59.7243772764942,
		road: 4482.51986111111,
		acceleration: -0.562777777777775
	},
	{
		id: 426,
		time: 425,
		velocity: 26.8597222222222,
		power: -4258.57955995674,
		road: 4509.65578703704,
		acceleration: -0.70425925925926
	},
	{
		id: 427,
		time: 426,
		velocity: 26.2861111111111,
		power: -2252.91197133115,
		road: 4536.13462962963,
		acceleration: -0.609907407407412
	},
	{
		id: 428,
		time: 427,
		velocity: 25.3758333333333,
		power: -3945.82575651259,
		road: 4561.97768518518,
		acceleration: -0.661666666666665
	},
	{
		id: 429,
		time: 428,
		velocity: 24.8747222222222,
		power: -3881.37003340714,
		road: 4587.16763888889,
		acceleration: -0.644537037037036
	},
	{
		id: 430,
		time: 429,
		velocity: 24.3525,
		power: -2058.6207441594,
		road: 4611.75800925926,
		acceleration: -0.55462962962963
	},
	{
		id: 431,
		time: 430,
		velocity: 23.7119444444444,
		power: -3329.62370797537,
		road: 4635.77296296296,
		acceleration: -0.596203703703701
	},
	{
		id: 432,
		time: 431,
		velocity: 23.0861111111111,
		power: -5590.43991753953,
		road: 4659.14782407407,
		acceleration: -0.683981481481482
	},
	{
		id: 433,
		time: 432,
		velocity: 22.3005555555556,
		power: -7225.78995033804,
		road: 4681.80634259259,
		acceleration: -0.748703703703708
	},
	{
		id: 434,
		time: 433,
		velocity: 21.4658333333333,
		power: -11726.4812756865,
		road: 4703.61259259259,
		acceleration: -0.955833333333331
	},
	{
		id: 435,
		time: 434,
		velocity: 20.2186111111111,
		power: -11715.3208029416,
		road: 4724.46171296296,
		acceleration: -0.958425925925926
	},
	{
		id: 436,
		time: 435,
		velocity: 19.4252777777778,
		power: -14130.1717573336,
		road: 4744.28518518518,
		acceleration: -1.09287037037037
	},
	{
		id: 437,
		time: 436,
		velocity: 18.1872222222222,
		power: -8829.65634514968,
		road: 4763.15384259259,
		acceleration: -0.816759259259261
	},
	{
		id: 438,
		time: 437,
		velocity: 17.7683333333333,
		power: -6150.91840913498,
		road: 4781.28055555555,
		acceleration: -0.667129629629628
	},
	{
		id: 439,
		time: 438,
		velocity: 17.4238888888889,
		power: -2387.23434770534,
		road: 4798.85199074074,
		acceleration: -0.443425925925926
	},
	{
		id: 440,
		time: 439,
		velocity: 16.8569444444444,
		power: -7758.17643883846,
		road: 4815.81782407407,
		acceleration: -0.767777777777781
	},
	{
		id: 441,
		time: 440,
		velocity: 15.465,
		power: -12633.7235124808,
		road: 4831.85143518518,
		acceleration: -1.09666666666667
	},
	{
		id: 442,
		time: 441,
		velocity: 14.1338888888889,
		power: -10960.485961284,
		road: 4846.82671296296,
		acceleration: -1.02
	},
	{
		id: 443,
		time: 442,
		velocity: 13.7969444444444,
		power: -3016.99328460002,
		road: 4861.06018518518,
		acceleration: -0.46361111111111
	},
	{
		id: 444,
		time: 443,
		velocity: 14.0741666666667,
		power: 4557.23923460881,
		road: 4875.11226851852,
		acceleration: 0.100833333333332
	},
	{
		id: 445,
		time: 444,
		velocity: 14.4363888888889,
		power: 11149.4153656149,
		road: 4889.49837962963,
		acceleration: 0.567222222222222
	},
	{
		id: 446,
		time: 445,
		velocity: 15.4986111111111,
		power: 11796.6537302286,
		road: 4904.45407407407,
		acceleration: 0.571944444444444
	},
	{
		id: 447,
		time: 446,
		velocity: 15.79,
		power: 11588.7759623363,
		road: 4919.95537037037,
		acceleration: 0.519259259259259
	},
	{
		id: 448,
		time: 447,
		velocity: 15.9941666666667,
		power: 7000.05329665702,
		road: 4935.81259259259,
		acceleration: 0.192592592592595
	},
	{
		id: 449,
		time: 448,
		velocity: 16.0763888888889,
		power: 6092.41463055381,
		road: 4951.82902777778,
		acceleration: 0.125833333333331
	},
	{
		id: 450,
		time: 449,
		velocity: 16.1675,
		power: 4132.89467902426,
		road: 4967.90625,
		acceleration: -0.00425925925925696
	},
	{
		id: 451,
		time: 450,
		velocity: 15.9813888888889,
		power: 2908.80125130984,
		road: 4983.94004629629,
		acceleration: -0.0825925925925937
	},
	{
		id: 452,
		time: 451,
		velocity: 15.8286111111111,
		power: 1578.94148966857,
		road: 4999.84939814815,
		acceleration: -0.166296296296297
	},
	{
		id: 453,
		time: 452,
		velocity: 15.6686111111111,
		power: 3790.50198077432,
		road: 5015.66671296296,
		acceleration: -0.0177777777777788
	},
	{
		id: 454,
		time: 453,
		velocity: 15.9280555555556,
		power: 5126.19258548414,
		road: 5031.50990740741,
		acceleration: 0.0695370370370387
	},
	{
		id: 455,
		time: 454,
		velocity: 16.0372222222222,
		power: 4649.81735645973,
		road: 5047.40592592592,
		acceleration: 0.0361111111111114
	},
	{
		id: 456,
		time: 455,
		velocity: 15.7769444444444,
		power: 2791.23652472858,
		road: 5063.27726851852,
		acceleration: -0.0854629629629642
	},
	{
		id: 457,
		time: 456,
		velocity: 15.6716666666667,
		power: -917.27781967262,
		road: 5078.94236111111,
		acceleration: -0.327037037037037
	},
	{
		id: 458,
		time: 457,
		velocity: 15.0561111111111,
		power: -522.252250664828,
		road: 5094.29597222222,
		acceleration: -0.295925925925925
	},
	{
		id: 459,
		time: 458,
		velocity: 14.8891666666667,
		power: -3346.84208395157,
		road: 5109.25773148148,
		acceleration: -0.487777777777779
	},
	{
		id: 460,
		time: 459,
		velocity: 14.2083333333333,
		power: -2051.96061091088,
		road: 5123.77847222222,
		acceleration: -0.394259259259258
	},
	{
		id: 461,
		time: 460,
		velocity: 13.8733333333333,
		power: -3103.10355793755,
		road: 5137.86708333333,
		acceleration: -0.469999999999999
	},
	{
		id: 462,
		time: 461,
		velocity: 13.4791666666667,
		power: -753.085157041055,
		road: 5151.57523148148,
		acceleration: -0.290925925925926
	},
	{
		id: 463,
		time: 462,
		velocity: 13.3355555555556,
		power: 2766.49162092986,
		road: 5165.12930555555,
		acceleration: -0.0172222222222231
	},
	{
		id: 464,
		time: 463,
		velocity: 13.8216666666667,
		power: 8181.65007371548,
		road: 5178.86962962963,
		acceleration: 0.389722222222222
	},
	{
		id: 465,
		time: 464,
		velocity: 14.6483333333333,
		power: 17655.51483832,
		road: 5193.32162037037,
		acceleration: 1.03361111111111
	},
	{
		id: 466,
		time: 465,
		velocity: 16.4363888888889,
		power: 29194.965177054,
		road: 5209.1225462963,
		acceleration: 1.66425925925926
	},
	{
		id: 467,
		time: 466,
		velocity: 18.8144444444444,
		power: 35393.9412831643,
		road: 5226.65958333333,
		acceleration: 1.80796296296296
	},
	{
		id: 468,
		time: 467,
		velocity: 20.0722222222222,
		power: 43912.4254739882,
		road: 5246.10856481481,
		acceleration: 2.01592592592593
	},
	{
		id: 469,
		time: 468,
		velocity: 22.4841666666667,
		power: 46167.8232779191,
		road: 5267.49814814815,
		acceleration: 1.86527777777778
	},
	{
		id: 470,
		time: 469,
		velocity: 24.4102777777778,
		power: 53865.0579311894,
		road: 5290.80712962963,
		acceleration: 1.97351851851852
	},
	{
		id: 471,
		time: 470,
		velocity: 25.9927777777778,
		power: 45389.2312826622,
		road: 5315.80796296296,
		acceleration: 1.41018518518518
	},
	{
		id: 472,
		time: 471,
		velocity: 26.7147222222222,
		power: 27660.2849191595,
		road: 5341.81324074074,
		acceleration: 0.598703703703706
	},
	{
		id: 473,
		time: 472,
		velocity: 26.2063888888889,
		power: 9127.36094011776,
		road: 5368.04180555555,
		acceleration: -0.152129629629631
	},
	{
		id: 474,
		time: 473,
		velocity: 25.5363888888889,
		power: 1712.84169334093,
		road: 5393.97592592593,
		acceleration: -0.436759259259262
	},
	{
		id: 475,
		time: 474,
		velocity: 25.4044444444444,
		power: -4062.69931834907,
		road: 5419.36351851852,
		acceleration: -0.656296296296297
	},
	{
		id: 476,
		time: 475,
		velocity: 24.2375,
		power: -2559.78134164022,
		road: 5444.13296296296,
		acceleration: -0.579999999999998
	},
	{
		id: 477,
		time: 476,
		velocity: 23.7963888888889,
		power: -8359.62432176828,
		road: 5468.20490740741,
		acceleration: -0.815000000000001
	},
	{
		id: 478,
		time: 477,
		velocity: 22.9594444444444,
		power: -4360.03247548904,
		road: 5491.555,
		acceleration: -0.628703703703703
	},
	{
		id: 479,
		time: 478,
		velocity: 22.3513888888889,
		power: -3135.84774503251,
		road: 5514.30953703704,
		acceleration: -0.562407407407406
	},
	{
		id: 480,
		time: 479,
		velocity: 22.1091666666667,
		power: -2260.08842445107,
		road: 5536.52722222222,
		acceleration: -0.511296296296294
	},
	{
		id: 481,
		time: 480,
		velocity: 21.4255555555556,
		power: -1682.91603872119,
		road: 5558.25231481481,
		acceleration: -0.473888888888894
	},
	{
		id: 482,
		time: 481,
		velocity: 20.9297222222222,
		power: -5786.57545764849,
		road: 5579.40810185185,
		acceleration: -0.664722222222217
	},
	{
		id: 483,
		time: 482,
		velocity: 20.115,
		power: -5239.04216108149,
		road: 5599.91611111111,
		acceleration: -0.630833333333335
	},
	{
		id: 484,
		time: 483,
		velocity: 19.5330555555556,
		power: -5675.8514164955,
		road: 5619.78472222222,
		acceleration: -0.647962962962964
	},
	{
		id: 485,
		time: 484,
		velocity: 18.9858333333333,
		power: -2495.74911863273,
		road: 5639.09314814815,
		acceleration: -0.472407407407406
	},
	{
		id: 486,
		time: 485,
		velocity: 18.6977777777778,
		power: -2404.78382691517,
		road: 5657.935,
		acceleration: -0.460740740740743
	},
	{
		id: 487,
		time: 486,
		velocity: 18.1508333333333,
		power: -3982.29768800127,
		road: 5676.27458333333,
		acceleration: -0.543796296296296
	},
	{
		id: 488,
		time: 487,
		velocity: 17.3544444444444,
		power: -4255.18507049558,
		road: 5694.06439814815,
		acceleration: -0.555740740740742
	},
	{
		id: 489,
		time: 488,
		velocity: 17.0305555555556,
		power: -2422.93798747861,
		road: 5711.35513888889,
		acceleration: -0.442407407407405
	},
	{
		id: 490,
		time: 489,
		velocity: 16.8236111111111,
		power: 3953.66300220364,
		road: 5728.40032407407,
		acceleration: -0.0487037037037048
	},
	{
		id: 491,
		time: 490,
		velocity: 17.2083333333333,
		power: 8157.32051867109,
		road: 5745.5237962963,
		acceleration: 0.205277777777777
	},
	{
		id: 492,
		time: 491,
		velocity: 17.6463888888889,
		power: 11562.1628522825,
		road: 5762.94740740741,
		acceleration: 0.395
	},
	{
		id: 493,
		time: 492,
		velocity: 18.0086111111111,
		power: 9099.82622983993,
		road: 5780.68421296296,
		acceleration: 0.231388888888894
	},
	{
		id: 494,
		time: 493,
		velocity: 17.9025,
		power: 5186.10169885652,
		road: 5798.535,
		acceleration: -0.00342592592593149
	},
	{
		id: 495,
		time: 494,
		velocity: 17.6361111111111,
		power: 1577.33079934347,
		road: 5816.27810185185,
		acceleration: -0.211944444444441
	},
	{
		id: 496,
		time: 495,
		velocity: 17.3727777777778,
		power: -541.488528847915,
		road: 5833.74930555556,
		acceleration: -0.331851851851852
	},
	{
		id: 497,
		time: 496,
		velocity: 16.9069444444444,
		power: -681.440472023796,
		road: 5850.88731481481,
		acceleration: -0.334537037037038
	},
	{
		id: 498,
		time: 497,
		velocity: 16.6325,
		power: -979.110556093186,
		road: 5867.68435185185,
		acceleration: -0.347407407407406
	},
	{
		id: 499,
		time: 498,
		velocity: 16.3305555555556,
		power: 1699.84040842636,
		road: 5884.22064814815,
		acceleration: -0.174074074074074
	},
	{
		id: 500,
		time: 499,
		velocity: 16.3847222222222,
		power: 2051.15939280989,
		road: 5900.59611111111,
		acceleration: -0.147592592592595
	},
	{
		id: 501,
		time: 500,
		velocity: 16.1897222222222,
		power: 3551.24695687867,
		road: 5916.87347222222,
		acceleration: -0.0486111111111072
	},
	{
		id: 502,
		time: 501,
		velocity: 16.1847222222222,
		power: 1667.64658282181,
		road: 5933.04305555555,
		acceleration: -0.166944444444447
	},
	{
		id: 503,
		time: 502,
		velocity: 15.8838888888889,
		power: 2165.34540902268,
		road: 5949.0637962963,
		acceleration: -0.130740740740743
	},
	{
		id: 504,
		time: 503,
		velocity: 15.7975,
		power: 1302.20489229503,
		road: 5964.9274537037,
		acceleration: -0.183425925925926
	},
	{
		id: 505,
		time: 504,
		velocity: 15.6344444444444,
		power: 1821.43717720531,
		road: 5980.62689814815,
		acceleration: -0.145000000000001
	},
	{
		id: 506,
		time: 505,
		velocity: 15.4488888888889,
		power: 1667.57992384041,
		road: 5996.17805555555,
		acceleration: -0.151574074074071
	},
	{
		id: 507,
		time: 506,
		velocity: 15.3427777777778,
		power: 2624.28072280166,
		road: 6011.61152777778,
		acceleration: -0.0837962962962973
	},
	{
		id: 508,
		time: 507,
		velocity: 15.3830555555556,
		power: 4302.25791731709,
		road: 6027.01856481481,
		acceleration: 0.030925925925926
	},
	{
		id: 509,
		time: 508,
		velocity: 15.5416666666667,
		power: 7604.25148282483,
		road: 6042.56509259259,
		acceleration: 0.248055555555558
	},
	{
		id: 510,
		time: 509,
		velocity: 16.0869444444444,
		power: 9881.79732882859,
		road: 6058.42685185185,
		acceleration: 0.382407407407408
	},
	{
		id: 511,
		time: 510,
		velocity: 16.5302777777778,
		power: 12047.0630256979,
		road: 6074.72763888889,
		acceleration: 0.495648148148145
	},
	{
		id: 512,
		time: 511,
		velocity: 17.0286111111111,
		power: 12758.8944764179,
		road: 6091.52990740741,
		acceleration: 0.507314814814816
	},
	{
		id: 513,
		time: 512,
		velocity: 17.6088888888889,
		power: 14185.1173858141,
		road: 6108.8650462963,
		acceleration: 0.558425925925928
	},
	{
		id: 514,
		time: 513,
		velocity: 18.2055555555556,
		power: 15020.0068770565,
		road: 6126.7637962963,
		acceleration: 0.568796296296295
	},
	{
		id: 515,
		time: 514,
		velocity: 18.735,
		power: 12388.56956064,
		road: 6145.14018518518,
		acceleration: 0.386481481481479
	},
	{
		id: 516,
		time: 515,
		velocity: 18.7683333333333,
		power: 7337.7552482589,
		road: 6163.75435185185,
		acceleration: 0.0890740740740767
	},
	{
		id: 517,
		time: 516,
		velocity: 18.4727777777778,
		power: 2980.19286063309,
		road: 6182.33583333333,
		acceleration: -0.154444444444447
	},
	{
		id: 518,
		time: 517,
		velocity: 18.2716666666667,
		power: 1398.4472662919,
		road: 6200.72087962963,
		acceleration: -0.238425925925924
	},
	{
		id: 519,
		time: 518,
		velocity: 18.0530555555556,
		power: 2373.02232794718,
		road: 6218.89810185185,
		acceleration: -0.177222222222223
	},
	{
		id: 520,
		time: 519,
		velocity: 17.9411111111111,
		power: 2196.99051246927,
		road: 6236.89550925926,
		acceleration: -0.182407407407403
	},
	{
		id: 521,
		time: 520,
		velocity: 17.7244444444444,
		power: 2667.56013789073,
		road: 6254.72657407407,
		acceleration: -0.150277777777781
	},
	{
		id: 522,
		time: 521,
		velocity: 17.6022222222222,
		power: 2896.1406372244,
		road: 6272.41615740741,
		acceleration: -0.132685185185185
	},
	{
		id: 523,
		time: 522,
		velocity: 17.5430555555556,
		power: 3006.84632990407,
		road: 6289.97824074074,
		acceleration: -0.122314814814814
	},
	{
		id: 524,
		time: 523,
		velocity: 17.3575,
		power: 2333.03475941172,
		road: 6307.39990740741,
		acceleration: -0.15851851851852
	},
	{
		id: 525,
		time: 524,
		velocity: 17.1266666666667,
		power: 2383.00686740225,
		road: 6324.66671296296,
		acceleration: -0.151203703703704
	},
	{
		id: 526,
		time: 525,
		velocity: 17.0894444444444,
		power: 2464.70851524903,
		road: 6341.78685185185,
		acceleration: -0.142129629629629
	},
	{
		id: 527,
		time: 526,
		velocity: 16.9311111111111,
		power: 3027.12912234626,
		road: 6358.78388888889,
		acceleration: -0.104074074074077
	},
	{
		id: 528,
		time: 527,
		velocity: 16.8144444444444,
		power: 1820.11403102928,
		road: 6375.64152777778,
		acceleration: -0.174722222222218
	},
	{
		id: 529,
		time: 528,
		velocity: 16.5652777777778,
		power: 1886.12803095944,
		road: 6392.3287037037,
		acceleration: -0.166203703703705
	},
	{
		id: 530,
		time: 529,
		velocity: 16.4325,
		power: 1240.90401339827,
		road: 6408.83162037037,
		acceleration: -0.202314814814812
	},
	{
		id: 531,
		time: 530,
		velocity: 16.2075,
		power: 1384.9198363494,
		road: 6425.13912037037,
		acceleration: -0.188518518518521
	},
	{
		id: 532,
		time: 531,
		velocity: 15.9997222222222,
		power: 1464.47393859037,
		road: 6441.26287037037,
		acceleration: -0.178981481481483
	},
	{
		id: 533,
		time: 532,
		velocity: 15.8955555555556,
		power: 304.483057955827,
		road: 6457.17208333333,
		acceleration: -0.250092592592594
	},
	{
		id: 534,
		time: 533,
		velocity: 15.4572222222222,
		power: -2014.89835404498,
		road: 6472.75643518518,
		acceleration: -0.399629629629628
	},
	{
		id: 535,
		time: 534,
		velocity: 14.8008333333333,
		power: -3555.02062702337,
		road: 6487.88976851852,
		acceleration: -0.502407407407407
	},
	{
		id: 536,
		time: 535,
		velocity: 14.3883333333333,
		power: -3326.01269709843,
		road: 6502.52884259259,
		acceleration: -0.486111111111112
	},
	{
		id: 537,
		time: 536,
		velocity: 13.9988888888889,
		power: -2952.05042779619,
		road: 6516.69546296296,
		acceleration: -0.458796296296294
	},
	{
		id: 538,
		time: 537,
		velocity: 13.4244444444444,
		power: -1801.86419683438,
		road: 6530.44703703704,
		acceleration: -0.371296296296299
	},
	{
		id: 539,
		time: 538,
		velocity: 13.2744444444444,
		power: -2866.24833909995,
		road: 6543.78652777778,
		acceleration: -0.45287037037037
	},
	{
		id: 540,
		time: 539,
		velocity: 12.6402777777778,
		power: -1145.53283700363,
		road: 6556.74222222222,
		acceleration: -0.314722222222223
	},
	{
		id: 541,
		time: 540,
		velocity: 12.4802777777778,
		power: -1413.00701878164,
		road: 6569.37324074074,
		acceleration: -0.33462962962963
	},
	{
		id: 542,
		time: 541,
		velocity: 12.2705555555556,
		power: -1191.98281876889,
		road: 6581.67976851852,
		acceleration: -0.31435185185185
	},
	{
		id: 543,
		time: 542,
		velocity: 11.6972222222222,
		power: -2569.91105525546,
		road: 6593.61236111111,
		acceleration: -0.433518518518518
	},
	{
		id: 544,
		time: 543,
		velocity: 11.1797222222222,
		power: -2986.06517369056,
		road: 6605.09092592592,
		acceleration: -0.474537037037038
	},
	{
		id: 545,
		time: 544,
		velocity: 10.8469444444444,
		power: -2657.41718880702,
		road: 6616.10777777778,
		acceleration: -0.448888888888888
	},
	{
		id: 546,
		time: 545,
		velocity: 10.3505555555556,
		power: -5200.66933594775,
		road: 6626.54430555555,
		acceleration: -0.71175925925926
	},
	{
		id: 547,
		time: 546,
		velocity: 9.04444444444444,
		power: -5995.86437661409,
		road: 6636.20925925926,
		acceleration: -0.831388888888888
	},
	{
		id: 548,
		time: 547,
		velocity: 8.35277777777778,
		power: -8768.22561241454,
		road: 6644.84018518518,
		acceleration: -1.23666666666667
	},
	{
		id: 549,
		time: 548,
		velocity: 6.64055555555556,
		power: -7715.4328474553,
		road: 6652.22449074074,
		acceleration: -1.25657407407407
	},
	{
		id: 550,
		time: 549,
		velocity: 5.27472222222222,
		power: -7113.04671031964,
		road: 6658.28949074074,
		acceleration: -1.38203703703704
	},
	{
		id: 551,
		time: 550,
		velocity: 4.20666666666667,
		power: -4100.2332186105,
		road: 6663.14898148148,
		acceleration: -1.02898148148148
	},
	{
		id: 552,
		time: 551,
		velocity: 3.55361111111111,
		power: -3124.02886109435,
		road: 6666.9987037037,
		acceleration: -0.990555555555555
	},
	{
		id: 553,
		time: 552,
		velocity: 2.30305555555556,
		power: -1623.20330323092,
		road: 6670.00212962963,
		acceleration: -0.702037037037037
	},
	{
		id: 554,
		time: 553,
		velocity: 2.10055555555556,
		power: -957.619605727366,
		road: 6672.37666666666,
		acceleration: -0.55574074074074
	},
	{
		id: 555,
		time: 554,
		velocity: 1.88638888888889,
		power: 91.8452601324574,
		road: 6674.43199074074,
		acceleration: -0.0826851851851851
	},
	{
		id: 556,
		time: 555,
		velocity: 2.055,
		power: 223.160337515317,
		road: 6676.43972222222,
		acceleration: -0.0125000000000002
	},
	{
		id: 557,
		time: 556,
		velocity: 2.06305555555556,
		power: 367.282616232844,
		road: 6678.47162037037,
		acceleration: 0.0608333333333335
	},
	{
		id: 558,
		time: 557,
		velocity: 2.06888888888889,
		power: 314.194515272419,
		road: 6680.54875,
		acceleration: 0.0296296296296292
	},
	{
		id: 559,
		time: 558,
		velocity: 2.14388888888889,
		power: 294.850592442828,
		road: 6682.64972222222,
		acceleration: 0.0180555555555557
	},
	{
		id: 560,
		time: 559,
		velocity: 2.11722222222222,
		power: 315.828417161939,
		road: 6684.77314814815,
		acceleration: 0.0268518518518519
	},
	{
		id: 561,
		time: 560,
		velocity: 2.14944444444444,
		power: 326.821243494126,
		road: 6686.92504629629,
		acceleration: 0.030092592592593
	},
	{
		id: 562,
		time: 561,
		velocity: 2.23416666666667,
		power: 256.743005121048,
		road: 6689.08949074074,
		acceleration: -0.00500000000000034
	},
	{
		id: 563,
		time: 562,
		velocity: 2.10222222222222,
		power: 197.490764605006,
		road: 6691.2349537037,
		acceleration: -0.0329629629629631
	},
	{
		id: 564,
		time: 563,
		velocity: 2.05055555555556,
		power: 67.5300775106753,
		road: 6693.31611111111,
		acceleration: -0.0956481481481481
	},
	{
		id: 565,
		time: 564,
		velocity: 1.94722222222222,
		power: 18.1727748913746,
		road: 6695.28949074074,
		acceleration: -0.119907407407408
	},
	{
		id: 566,
		time: 565,
		velocity: 1.7425,
		power: -56.8517155459933,
		road: 6697.12189814815,
		acceleration: -0.162037037037037
	},
	{
		id: 567,
		time: 566,
		velocity: 1.56444444444444,
		power: -5.53680238478755,
		road: 6698.80703703704,
		acceleration: -0.1325
	},
	{
		id: 568,
		time: 567,
		velocity: 1.54972222222222,
		power: 371.989779449315,
		road: 6700.47875,
		acceleration: 0.105648148148148
	},
	{
		id: 569,
		time: 568,
		velocity: 2.05944444444444,
		power: 528.421773197845,
		road: 6702.29226851852,
		acceleration: 0.177962962962963
	},
	{
		id: 570,
		time: 569,
		velocity: 2.09833333333333,
		power: 535.636809453555,
		road: 6704.2725462963,
		acceleration: 0.155555555555555
	},
	{
		id: 571,
		time: 570,
		velocity: 2.01638888888889,
		power: 263.473557188896,
		road: 6706.33310185185,
		acceleration: 0.00500000000000034
	},
	{
		id: 572,
		time: 571,
		velocity: 2.07444444444444,
		power: 223.818048394989,
		road: 6708.38865740741,
		acceleration: -0.0150000000000001
	},
	{
		id: 573,
		time: 572,
		velocity: 2.05333333333333,
		power: 427.438036852967,
		road: 6710.47953703704,
		acceleration: 0.0856481481481479
	},
	{
		id: 574,
		time: 573,
		velocity: 2.27333333333333,
		power: 277.585979872203,
		road: 6712.61671296296,
		acceleration: 0.00694444444444464
	},
	{
		id: 575,
		time: 574,
		velocity: 2.09527777777778,
		power: 323.290318823729,
		road: 6714.77143518518,
		acceleration: 0.0281481481481483
	},
	{
		id: 576,
		time: 575,
		velocity: 2.13777777777778,
		power: 132.829218281541,
		road: 6716.90800925926,
		acceleration: -0.0644444444444443
	},
	{
		id: 577,
		time: 576,
		velocity: 2.08,
		power: 266.174211671949,
		road: 6719.01402777778,
		acceleration: 0.00333333333333297
	},
	{
		id: 578,
		time: 577,
		velocity: 2.10527777777778,
		power: 240.095559254942,
		road: 6721.11694444444,
		acceleration: -0.0095370370370369
	},
	{
		id: 579,
		time: 578,
		velocity: 2.10916666666667,
		power: 219.878173900645,
		road: 6723.20564814815,
		acceleration: -0.0188888888888887
	},
	{
		id: 580,
		time: 579,
		velocity: 2.02333333333333,
		power: 282.565632121826,
		road: 6725.29138888889,
		acceleration: 0.0129629629629631
	},
	{
		id: 581,
		time: 580,
		velocity: 2.14416666666667,
		power: 244.303627809251,
		road: 6727.38032407407,
		acceleration: -0.00657407407407407
	},
	{
		id: 582,
		time: 581,
		velocity: 2.08944444444444,
		power: 391.272434173517,
		road: 6729.49837962963,
		acceleration: 0.0648148148148149
	},
	{
		id: 583,
		time: 582,
		velocity: 2.21777777777778,
		power: 224.675772837427,
		road: 6731.63916666667,
		acceleration: -0.0193518518518525
	},
	{
		id: 584,
		time: 583,
		velocity: 2.08611111111111,
		power: 278.592598771029,
		road: 6733.77407407407,
		acceleration: 0.0075925925925926
	},
	{
		id: 585,
		time: 584,
		velocity: 2.11222222222222,
		power: 147.776045798445,
		road: 6735.88472222222,
		acceleration: -0.056111111111111
	},
	{
		id: 586,
		time: 585,
		velocity: 2.04944444444444,
		power: 270.702590635566,
		road: 6737.97078703704,
		acceleration: 0.00694444444444464
	},
	{
		id: 587,
		time: 586,
		velocity: 2.10694444444444,
		power: 287.629513042257,
		road: 6740.06768518519,
		acceleration: 0.0147222222222227
	},
	{
		id: 588,
		time: 587,
		velocity: 2.15638888888889,
		power: 332.164698097478,
		road: 6742.18949074074,
		acceleration: 0.035092592592592
	},
	{
		id: 589,
		time: 588,
		velocity: 2.15472222222222,
		power: 370.835847107965,
		road: 6744.35412037037,
		acceleration: 0.0505555555555555
	},
	{
		id: 590,
		time: 589,
		velocity: 2.25861111111111,
		power: 311.102931238074,
		road: 6746.55351851852,
		acceleration: 0.0189814814814819
	},
	{
		id: 591,
		time: 590,
		velocity: 2.21333333333333,
		power: 238.84741225772,
		road: 6748.75453703704,
		acceleration: -0.0157407407407408
	},
	{
		id: 592,
		time: 591,
		velocity: 2.1075,
		power: 183.333580057644,
		road: 6750.92712962963,
		acceleration: -0.0411111111111113
	},
	{
		id: 593,
		time: 592,
		velocity: 2.13527777777778,
		power: 323.280771731059,
		road: 6753.09282407408,
		acceleration: 0.0273148148148148
	},
	{
		id: 594,
		time: 593,
		velocity: 2.29527777777778,
		power: 286.917645612666,
		road: 6755.27638888889,
		acceleration: 0.00842592592592606
	},
	{
		id: 595,
		time: 594,
		velocity: 2.13277777777778,
		power: 177.993767066037,
		road: 6757.4424537037,
		acceleration: -0.0434259259259258
	},
	{
		id: 596,
		time: 595,
		velocity: 2.005,
		power: 182.409513705508,
		road: 6759.56708333333,
		acceleration: -0.0394444444444453
	},
	{
		id: 597,
		time: 596,
		velocity: 2.17694444444444,
		power: 311.377440633342,
		road: 6761.68453703704,
		acceleration: 0.0250925925925931
	},
	{
		id: 598,
		time: 597,
		velocity: 2.20805555555556,
		power: 357.943817767515,
		road: 6763.83717592593,
		acceleration: 0.0452777777777778
	},
	{
		id: 599,
		time: 598,
		velocity: 2.14083333333333,
		power: 288.141278114762,
		road: 6766.01708333333,
		acceleration: 0.00925925925925952
	},
	{
		id: 600,
		time: 599,
		velocity: 2.20472222222222,
		power: 236.668642953118,
		road: 6768.19388888889,
		acceleration: -0.0154629629629635
	},
	{
		id: 601,
		time: 600,
		velocity: 2.16166666666667,
		power: 318.162864121605,
		road: 6770.37481481482,
		acceleration: 0.023703703703704
	},
	{
		id: 602,
		time: 601,
		velocity: 2.21194444444444,
		power: 231.892201334837,
		road: 6772.55851851852,
		acceleration: -0.018148148148148
	},
	{
		id: 603,
		time: 602,
		velocity: 2.15027777777778,
		power: 324.578450647691,
		road: 6774.7462962963,
		acceleration: 0.0262962962962963
	},
	{
		id: 604,
		time: 603,
		velocity: 2.24055555555556,
		power: 228.255561500525,
		road: 6776.93708333333,
		acceleration: -0.0202777777777778
	},
	{
		id: 605,
		time: 604,
		velocity: 2.15111111111111,
		power: 231.913443609154,
		road: 6779.10898148148,
		acceleration: -0.0175000000000001
	},
	{
		id: 606,
		time: 605,
		velocity: 2.09777777777778,
		power: 94.5637373429835,
		road: 6781.23064814815,
		acceleration: -0.0829629629629629
	},
	{
		id: 607,
		time: 606,
		velocity: 1.99166666666667,
		power: 226.511693855086,
		road: 6783.30351851852,
		acceleration: -0.01462962962963
	},
	{
		id: 608,
		time: 607,
		velocity: 2.10722222222222,
		power: 182.274861896335,
		road: 6785.35111111111,
		acceleration: -0.0359259259259255
	},
	{
		id: 609,
		time: 608,
		velocity: 1.99,
		power: 348.310911790115,
		road: 6787.40523148148,
		acceleration: 0.0489814814814813
	},
	{
		id: 610,
		time: 609,
		velocity: 2.13861111111111,
		power: 264.876018454411,
		road: 6789.48601851852,
		acceleration: 0.00435185185185194
	},
	{
		id: 611,
		time: 610,
		velocity: 2.12027777777778,
		power: 415.930280879555,
		road: 6791.60736111111,
		acceleration: 0.076759259259259
	},
	{
		id: 612,
		time: 611,
		velocity: 2.22027777777778,
		power: 262.100005577828,
		road: 6793.76606481482,
		acceleration: -0.00203703703703662
	},
	{
		id: 613,
		time: 612,
		velocity: 2.1325,
		power: 242.550706920865,
		road: 6795.91814814815,
		acceleration: -0.0112037037037038
	},
	{
		id: 614,
		time: 613,
		velocity: 2.08666666666667,
		power: 273.461155187281,
		road: 6798.06671296296,
		acceleration: 0.00416666666666687
	},
	{
		id: 615,
		time: 614,
		velocity: 2.23277777777778,
		power: 274.988516199308,
		road: 6800.21967592593,
		acceleration: 0.00462962962962932
	},
	{
		id: 616,
		time: 615,
		velocity: 2.14638888888889,
		power: 264.46474830632,
		road: 6802.37462962963,
		acceleration: -0.000648148148147953
	},
	{
		id: 617,
		time: 616,
		velocity: 2.08472222222222,
		power: 188.13728867204,
		road: 6804.51069444445,
		acceleration: -0.0371296296296295
	},
	{
		id: 618,
		time: 617,
		velocity: 2.12138888888889,
		power: 363.644575859628,
		road: 6806.65268518519,
		acceleration: 0.0489814814814813
	},
	{
		id: 619,
		time: 618,
		velocity: 2.29333333333333,
		power: 325.313076060806,
		road: 6808.83277777778,
		acceleration: 0.0272222222222225
	},
	{
		id: 620,
		time: 619,
		velocity: 2.16638888888889,
		power: 258.668520284329,
		road: 6811.02365740741,
		acceleration: -0.00564814814814829
	},
	{
		id: 621,
		time: 620,
		velocity: 2.10444444444444,
		power: 132.005407956575,
		road: 6813.17898148148,
		acceleration: -0.0654629629629628
	},
	{
		id: 622,
		time: 621,
		velocity: 2.09694444444444,
		power: 222.759433480534,
		road: 6815.29217592593,
		acceleration: -0.0187962962962964
	},
	{
		id: 623,
		time: 622,
		velocity: 2.11,
		power: 276.809386326284,
		road: 6817.40023148148,
		acceleration: 0.00851851851851837
	},
	{
		id: 624,
		time: 623,
		velocity: 2.13,
		power: 276.239958251154,
		road: 6819.51638888889,
		acceleration: 0.00768518518518491
	},
	{
		id: 625,
		time: 624,
		velocity: 2.12,
		power: 245.005495264742,
		road: 6821.63245370371,
		acceleration: -0.00787037037037042
	},
	{
		id: 626,
		time: 625,
		velocity: 2.08638888888889,
		power: 245.200812797066,
		road: 6823.74092592593,
		acceleration: -0.00731481481481477
	},
	{
		id: 627,
		time: 626,
		velocity: 2.10805555555556,
		power: 263.751647109843,
		road: 6825.84680555556,
		acceleration: 0.00212962962963026
	},
	{
		id: 628,
		time: 627,
		velocity: 2.12638888888889,
		power: 301.459759837079,
		road: 6827.96384259259,
		acceleration: 0.0201851851851851
	},
	{
		id: 629,
		time: 628,
		velocity: 2.14694444444444,
		power: 287.153186583848,
		road: 6830.09694444445,
		acceleration: 0.0119444444444445
	},
	{
		id: 630,
		time: 629,
		velocity: 2.14388888888889,
		power: 174.514212545909,
		road: 6832.21449074074,
		acceleration: -0.0430555555555561
	},
	{
		id: 631,
		time: 630,
		velocity: 1.99722222222222,
		power: 269.650623858244,
		road: 6834.31328703704,
		acceleration: 0.00555555555555554
	},
	{
		id: 632,
		time: 631,
		velocity: 2.16361111111111,
		power: 164.531796090831,
		road: 6836.39166666667,
		acceleration: -0.046388888888889
	},
	{
		id: 633,
		time: 632,
		velocity: 2.00472222222222,
		power: 274.318660778497,
		road: 6838.45212962963,
		acceleration: 0.0105555555555559
	},
	{
		id: 634,
		time: 633,
		velocity: 2.02888888888889,
		power: 242.3911807447,
		road: 6840.51490740741,
		acceleration: -0.00592592592592611
	},
	{
		id: 635,
		time: 634,
		velocity: 2.14583333333333,
		power: 460.906005313957,
		road: 6842.62490740741,
		acceleration: 0.100370370370371
	},
	{
		id: 636,
		time: 635,
		velocity: 2.30583333333333,
		power: 446.276060365858,
		road: 6844.82685185185,
		acceleration: 0.0835185185185185
	},
	{
		id: 637,
		time: 636,
		velocity: 2.27944444444444,
		power: 453.166590115704,
		road: 6847.11,
		acceleration: 0.0788888888888888
	},
	{
		id: 638,
		time: 637,
		velocity: 2.3825,
		power: 419.401580979123,
		road: 6849.4613425926,
		acceleration: 0.0575000000000001
	},
	{
		id: 639,
		time: 638,
		velocity: 2.47833333333333,
		power: 416.605547338723,
		road: 6851.86736111111,
		acceleration: 0.0518518518518523
	},
	{
		id: 640,
		time: 639,
		velocity: 2.435,
		power: 341.523892825699,
		road: 6854.30768518519,
		acceleration: 0.0167592592592589
	},
	{
		id: 641,
		time: 640,
		velocity: 2.43277777777778,
		power: 217.320708548846,
		road: 6856.73814814815,
		acceleration: -0.0364814814814816
	},
	{
		id: 642,
		time: 641,
		velocity: 2.36888888888889,
		power: 366.787629699924,
		road: 6859.16467592593,
		acceleration: 0.0286111111111111
	},
	{
		id: 643,
		time: 642,
		velocity: 2.52083333333333,
		power: 379.33201847538,
		road: 6861.62148148148,
		acceleration: 0.0319444444444441
	},
	{
		id: 644,
		time: 643,
		velocity: 2.52861111111111,
		power: 567.500119583787,
		road: 6864.14717592593,
		acceleration: 0.105833333333333
	},
	{
		id: 645,
		time: 644,
		velocity: 2.68638888888889,
		power: 454.060872057646,
		road: 6866.75203703704,
		acceleration: 0.0524999999999998
	},
	{
		id: 646,
		time: 645,
		velocity: 2.67833333333333,
		power: 169.482604697794,
		road: 6869.35189814815,
		acceleration: -0.0625
	},
	{
		id: 647,
		time: 646,
		velocity: 2.34111111111111,
		power: 12.585804660073,
		road: 6871.85768518519,
		acceleration: -0.125648148148148
	},
	{
		id: 648,
		time: 647,
		velocity: 2.30944444444444,
		power: -0.331357516298696,
		road: 6874.23527777778,
		acceleration: -0.130740740740741
	},
	{
		id: 649,
		time: 648,
		velocity: 2.28611111111111,
		power: 398.247688433891,
		road: 6876.57208333334,
		acceleration: 0.0491666666666668
	},
	{
		id: 650,
		time: 649,
		velocity: 2.48861111111111,
		power: 671.404869903908,
		road: 6879.01305555556,
		acceleration: 0.159166666666667
	},
	{
		id: 651,
		time: 650,
		velocity: 2.78694444444444,
		power: 807.748822959042,
		road: 6881.63060185186,
		acceleration: 0.193981481481481
	},
	{
		id: 652,
		time: 651,
		velocity: 2.86805555555556,
		power: 643.617378008503,
		road: 6884.40166666667,
		acceleration: 0.113055555555556
	},
	{
		id: 653,
		time: 652,
		velocity: 2.82777777777778,
		power: 230.358697534413,
		road: 6887.20662037037,
		acceleration: -0.0452777777777778
	},
	{
		id: 654,
		time: 653,
		velocity: 2.65111111111111,
		power: 58.035333485437,
		road: 6889.93435185186,
		acceleration: -0.109166666666667
	},
	{
		id: 655,
		time: 654,
		velocity: 2.54055555555556,
		power: -79.7180635598894,
		road: 6892.52569444445,
		acceleration: -0.163611111111111
	},
	{
		id: 656,
		time: 655,
		velocity: 2.33694444444444,
		power: -65.3297622064794,
		road: 6894.95569444445,
		acceleration: -0.159074074074074
	},
	{
		id: 657,
		time: 656,
		velocity: 2.17388888888889,
		power: -13.8823245953444,
		road: 6897.23777777778,
		acceleration: -0.136759259259259
	},
	{
		id: 658,
		time: 657,
		velocity: 2.13027777777778,
		power: 188.520251406264,
		road: 6899.43171296297,
		acceleration: -0.0395370370370367
	},
	{
		id: 659,
		time: 658,
		velocity: 2.21833333333333,
		power: 216.559317151114,
		road: 6901.59365740741,
		acceleration: -0.0244444444444447
	},
	{
		id: 660,
		time: 659,
		velocity: 2.10055555555556,
		power: 233.289447260189,
		road: 6903.73578703704,
		acceleration: -0.0151851851851852
	},
	{
		id: 661,
		time: 660,
		velocity: 2.08472222222222,
		power: 157.842146913685,
		road: 6905.84481481482,
		acceleration: -0.0510185185185184
	},
	{
		id: 662,
		time: 661,
		velocity: 2.06527777777778,
		power: 170.418949280898,
		road: 6907.90699074074,
		acceleration: -0.0426851851851855
	},
	{
		id: 663,
		time: 662,
		velocity: 1.9725,
		power: 273.458852781685,
		road: 6909.95337962963,
		acceleration: 0.0111111111111115
	},
	{
		id: 664,
		time: 663,
		velocity: 2.11805555555556,
		power: 295.248758136085,
		road: 6912.01587962963,
		acceleration: 0.0211111111111109
	},
	{
		id: 665,
		time: 664,
		velocity: 2.12861111111111,
		power: 493.084293984877,
		road: 6914.14597222223,
		acceleration: 0.114074074074074
	},
	{
		id: 666,
		time: 665,
		velocity: 2.31472222222222,
		power: 248.188127780566,
		road: 6916.32800925926,
		acceleration: -0.0101851851851849
	},
	{
		id: 667,
		time: 666,
		velocity: 2.0875,
		power: 191.765284245154,
		road: 6918.48675925926,
		acceleration: -0.0363888888888888
	},
	{
		id: 668,
		time: 667,
		velocity: 2.01944444444444,
		power: 87.2960795074056,
		road: 6920.58430555556,
		acceleration: -0.0860185185185189
	},
	{
		id: 669,
		time: 668,
		velocity: 2.05666666666667,
		power: 236.009122654921,
		road: 6922.63462962963,
		acceleration: -0.00842592592592561
	},
	{
		id: 670,
		time: 669,
		velocity: 2.06222222222222,
		power: 243.328065846364,
		road: 6924.67861111112,
		acceleration: -0.00425925925925918
	},
	{
		id: 671,
		time: 670,
		velocity: 2.00666666666667,
		power: 331.526959399024,
		road: 6926.74032407408,
		acceleration: 0.0397222222222222
	},
	{
		id: 672,
		time: 671,
		velocity: 2.17583333333333,
		power: 513.622070963039,
		road: 6928.88324074074,
		acceleration: 0.122685185185185
	},
	{
		id: 673,
		time: 672,
		velocity: 2.43027777777778,
		power: 664.953002263644,
		road: 6931.17523148148,
		acceleration: 0.175462962962963
	},
	{
		id: 674,
		time: 673,
		velocity: 2.53305555555556,
		power: 748.445918346218,
		road: 6933.64898148149,
		acceleration: 0.188055555555556
	},
	{
		id: 675,
		time: 674,
		velocity: 2.74,
		power: 695.410382838343,
		road: 6936.28986111112,
		acceleration: 0.146203703703704
	},
	{
		id: 676,
		time: 675,
		velocity: 2.86888888888889,
		power: 705.584837311665,
		road: 6939.07162037037,
		acceleration: 0.135555555555555
	},
	{
		id: 677,
		time: 676,
		velocity: 2.93972222222222,
		power: 536.86652193772,
		road: 6941.95328703704,
		acceleration: 0.0642592592592592
	},
	{
		id: 678,
		time: 677,
		velocity: 2.93277777777778,
		power: 654.829982879041,
		road: 6944.91731481482,
		acceleration: 0.100462962962963
	},
	{
		id: 679,
		time: 678,
		velocity: 3.17027777777778,
		power: 781.649710705956,
		road: 6947.9988425926,
		acceleration: 0.134537037037037
	},
	{
		id: 680,
		time: 679,
		velocity: 3.34333333333333,
		power: 834.111485533772,
		road: 6951.2175462963,
		acceleration: 0.139814814814815
	},
	{
		id: 681,
		time: 680,
		velocity: 3.35222222222222,
		power: 649.855384888455,
		road: 6954.54231481482,
		acceleration: 0.0723148148148147
	},
	{
		id: 682,
		time: 681,
		velocity: 3.38722222222222,
		power: 384.794325298383,
		road: 6957.89680555556,
		acceleration: -0.0128703703703703
	},
	{
		id: 683,
		time: 682,
		velocity: 3.30472222222222,
		power: 426.382484523526,
		road: 6961.2450925926,
		acceleration: 0.000462962962962887
	},
	{
		id: 684,
		time: 683,
		velocity: 3.35361111111111,
		power: 401.811201908718,
		road: 6964.5900462963,
		acceleration: -0.00712962962962926
	},
	{
		id: 685,
		time: 684,
		velocity: 3.36583333333333,
		power: 664.198780331641,
		road: 6967.96810185186,
		acceleration: 0.0733333333333333
	},
	{
		id: 686,
		time: 685,
		velocity: 3.52472222222222,
		power: 984.857732439284,
		road: 6971.46407407408,
		acceleration: 0.162499999999999
	},
	{
		id: 687,
		time: 686,
		velocity: 3.84111111111111,
		power: 1321.3601175656,
		road: 6975.16194444445,
		acceleration: 0.241296296296296
	},
	{
		id: 688,
		time: 687,
		velocity: 4.08972222222222,
		power: 1290.97695995759,
		road: 6979.08569444445,
		acceleration: 0.210462962962963
	},
	{
		id: 689,
		time: 688,
		velocity: 4.15611111111111,
		power: 880.045601040958,
		road: 6983.1600462963,
		acceleration: 0.090740740740741
	},
	{
		id: 690,
		time: 689,
		velocity: 4.11333333333333,
		power: 252.462105052559,
		road: 6987.24393518519,
		acceleration: -0.0716666666666663
	},
	{
		id: 691,
		time: 690,
		velocity: 3.87472222222222,
		power: 442.234868370495,
		road: 6991.28138888889,
		acceleration: -0.0212037037037041
	},
	{
		id: 692,
		time: 691,
		velocity: 4.0925,
		power: 731.546843354775,
		road: 6995.33495370371,
		acceleration: 0.053425925925926
	},
	{
		id: 693,
		time: 692,
		velocity: 4.27361111111111,
		power: 939.221583573833,
		road: 6999.46643518519,
		acceleration: 0.102407407407408
	},
	{
		id: 694,
		time: 693,
		velocity: 4.18194444444444,
		power: 797.573457033047,
		road: 7003.6800925926,
		acceleration: 0.0619444444444435
	},
	{
		id: 695,
		time: 694,
		velocity: 4.27833333333333,
		power: 528.888678800736,
		road: 7007.92162037038,
		acceleration: -0.0062037037037026
	},
	{
		id: 696,
		time: 695,
		velocity: 4.255,
		power: 886.031336235275,
		road: 7012.20023148149,
		acceleration: 0.0803703703703693
	},
	{
		id: 697,
		time: 696,
		velocity: 4.42305555555556,
		power: 819.441874891725,
		road: 7016.54921296297,
		acceleration: 0.0603703703703706
	},
	{
		id: 698,
		time: 697,
		velocity: 4.45944444444444,
		power: 910.955144395349,
		road: 7020.96773148149,
		acceleration: 0.0787037037037033
	},
	{
		id: 699,
		time: 698,
		velocity: 4.49111111111111,
		power: 696.754699230152,
		road: 7025.43833333334,
		acceleration: 0.0254629629629637
	},
	{
		id: 700,
		time: 699,
		velocity: 4.49944444444444,
		power: 695.228185221858,
		road: 7029.93370370371,
		acceleration: 0.0240740740740737
	},
	{
		id: 701,
		time: 700,
		velocity: 4.53166666666667,
		power: 752.697985774406,
		road: 7034.45921296297,
		acceleration: 0.0362037037037037
	},
	{
		id: 702,
		time: 701,
		velocity: 4.59972222222222,
		power: 609.635222209962,
		road: 7039.00393518519,
		acceleration: 0.00222222222222257
	},
	{
		id: 703,
		time: 702,
		velocity: 4.50611111111111,
		power: 82.0797642183593,
		road: 7043.4900462963,
		acceleration: -0.119444444444444
	},
	{
		id: 704,
		time: 703,
		velocity: 4.17333333333333,
		power: -122.823701824858,
		road: 7047.8325462963,
		acceleration: -0.167777777777778
	},
	{
		id: 705,
		time: 704,
		velocity: 4.09638888888889,
		power: -80.5126151660104,
		road: 7052.01240740741,
		acceleration: -0.157500000000001
	},
	{
		id: 706,
		time: 705,
		velocity: 4.03361111111111,
		power: 585.562904604233,
		road: 7056.12013888889,
		acceleration: 0.0132407407407413
	},
	{
		id: 707,
		time: 706,
		velocity: 4.21305555555556,
		power: 701.609859761119,
		road: 7060.25532407408,
		acceleration: 0.041666666666667
	},
	{
		id: 708,
		time: 707,
		velocity: 4.22138888888889,
		power: 1033.57742854024,
		road: 7064.47171296297,
		acceleration: 0.12074074074074
	},
	{
		id: 709,
		time: 708,
		velocity: 4.39583333333333,
		power: 811.44129365535,
		road: 7068.77875,
		acceleration: 0.0605555555555561
	},
	{
		id: 710,
		time: 709,
		velocity: 4.39472222222222,
		power: 889.981192857936,
		road: 7073.15407407408,
		acceleration: 0.0760185185185183
	},
	{
		id: 711,
		time: 710,
		velocity: 4.44944444444444,
		power: 638.054650046764,
		road: 7077.57421296297,
		acceleration: 0.0136111111111115
	},
	{
		id: 712,
		time: 711,
		velocity: 4.43666666666667,
		power: 545.520095422427,
		road: 7081.99689814815,
		acceleration: -0.00851851851851837
	},
	{
		id: 713,
		time: 712,
		velocity: 4.36916666666667,
		power: 394.437956712688,
		road: 7086.39342592593,
		acceleration: -0.0437962962962963
	},
	{
		id: 714,
		time: 713,
		velocity: 4.31805555555556,
		power: 323.404097226258,
		road: 7090.73824074075,
		acceleration: -0.0596296296296304
	},
	{
		id: 715,
		time: 714,
		velocity: 4.25777777777778,
		power: 345.241511235604,
		road: 7095.02675925926,
		acceleration: -0.0529629629629627
	},
	{
		id: 716,
		time: 715,
		velocity: 4.21027777777778,
		power: 416.27718085459,
		road: 7099.27166666667,
		acceleration: -0.0342592592592599
	},
	{
		id: 717,
		time: 716,
		velocity: 4.21527777777778,
		power: 677.612759970345,
		road: 7103.51476851852,
		acceleration: 0.0306481481481491
	},
	{
		id: 718,
		time: 717,
		velocity: 4.34972222222222,
		power: 854.478543448665,
		road: 7107.80907407408,
		acceleration: 0.0717592592592595
	},
	{
		id: 719,
		time: 718,
		velocity: 4.42555555555556,
		power: 850.151479025827,
		road: 7112.17277777778,
		acceleration: 0.0670370370370366
	},
	{
		id: 720,
		time: 719,
		velocity: 4.41638888888889,
		power: 551.843427565315,
		road: 7116.56699074075,
		acceleration: -0.00601851851851887
	},
	{
		id: 721,
		time: 720,
		velocity: 4.33166666666667,
		power: -151.25702101796,
		road: 7120.87078703704,
		acceleration: -0.174814814814814
	},
	{
		id: 722,
		time: 721,
		velocity: 3.90111111111111,
		power: -641.439642742658,
		road: 7124.93574074075,
		acceleration: -0.30287037037037
	},
	{
		id: 723,
		time: 722,
		velocity: 3.50777777777778,
		power: -1148.13231007729,
		road: 7128.6175,
		acceleration: -0.463518518518519
	},
	{
		id: 724,
		time: 723,
		velocity: 2.94111111111111,
		power: -915.320258751921,
		road: 7131.85175925926,
		acceleration: -0.431481481481481
	},
	{
		id: 725,
		time: 724,
		velocity: 2.60666666666667,
		power: -688.885479449558,
		road: 7134.67578703704,
		acceleration: -0.388981481481482
	},
	{
		id: 726,
		time: 725,
		velocity: 2.34083333333333,
		power: -43.1660421499201,
		road: 7137.23087962963,
		acceleration: -0.148888888888889
	},
	{
		id: 727,
		time: 726,
		velocity: 2.49444444444444,
		power: 411.199689869477,
		road: 7139.73268518519,
		acceleration: 0.0423148148148145
	},
	{
		id: 728,
		time: 727,
		velocity: 2.73361111111111,
		power: 829.104286481477,
		road: 7142.35652777778,
		acceleration: 0.201759259259259
	},
	{
		id: 729,
		time: 728,
		velocity: 2.94611111111111,
		power: 978.499257899522,
		road: 7145.19680555556,
		acceleration: 0.231111111111111
	},
	{
		id: 730,
		time: 729,
		velocity: 3.18777777777778,
		power: 901.108917714627,
		road: 7148.24222222223,
		acceleration: 0.179166666666667
	},
	{
		id: 731,
		time: 730,
		velocity: 3.27111111111111,
		power: 259.766041583818,
		road: 7151.35476851852,
		acceleration: -0.0449074074074072
	},
	{
		id: 732,
		time: 731,
		velocity: 2.81138888888889,
		power: -258.069340282076,
		road: 7154.33300925926,
		acceleration: -0.223703703703704
	},
	{
		id: 733,
		time: 732,
		velocity: 2.51666666666667,
		power: -330.497689864126,
		road: 7157.06995370371,
		acceleration: -0.258888888888889
	},
	{
		id: 734,
		time: 733,
		velocity: 2.49444444444444,
		power: 76.5637445040921,
		road: 7159.62768518519,
		acceleration: -0.0995370370370372
	},
	{
		id: 735,
		time: 734,
		velocity: 2.51277777777778,
		power: 287.394508825322,
		road: 7162.13069444445,
		acceleration: -0.00990740740740748
	},
	{
		id: 736,
		time: 735,
		velocity: 2.48694444444444,
		power: 414.869084141159,
		road: 7164.6500462963,
		acceleration: 0.0425925925925932
	},
	{
		id: 737,
		time: 736,
		velocity: 2.62222222222222,
		power: 418.691981572715,
		road: 7167.2112962963,
		acceleration: 0.0412037037037036
	},
	{
		id: 738,
		time: 737,
		velocity: 2.63638888888889,
		power: 416.511563551817,
		road: 7169.81194444445,
		acceleration: 0.037592592592592
	},
	{
		id: 739,
		time: 738,
		velocity: 2.59972222222222,
		power: 285.721978500665,
		road: 7172.42342592593,
		acceleration: -0.0159259259259255
	},
	{
		id: 740,
		time: 739,
		velocity: 2.57444444444444,
		power: 273.600519462396,
		road: 7175.01694444445,
		acceleration: -0.02
	},
	{
		id: 741,
		time: 740,
		velocity: 2.57638888888889,
		power: 198.209093890309,
		road: 7177.57574074075,
		acceleration: -0.0494444444444446
	},
	{
		id: 742,
		time: 741,
		velocity: 2.45138888888889,
		power: 402.71834317639,
		road: 7180.12745370371,
		acceleration: 0.035277777777778
	},
	{
		id: 743,
		time: 742,
		velocity: 2.68027777777778,
		power: 521.527551522914,
		road: 7182.73652777778,
		acceleration: 0.0794444444444444
	},
	{
		id: 744,
		time: 743,
		velocity: 2.81472222222222,
		power: 833.553741842649,
		road: 7185.47962962964,
		acceleration: 0.18861111111111
	},
	{
		id: 745,
		time: 744,
		velocity: 3.01722222222222,
		power: 795.450690260921,
		road: 7188.39472222223,
		acceleration: 0.155370370370371
	},
	{
		id: 746,
		time: 745,
		velocity: 3.14638888888889,
		power: 487.482740056056,
		road: 7191.40652777778,
		acceleration: 0.0380555555555557
	},
	{
		id: 747,
		time: 746,
		velocity: 2.92888888888889,
		power: -125.247042120933,
		road: 7194.3487962963,
		acceleration: -0.177129629629631
	},
	{
		id: 748,
		time: 747,
		velocity: 2.48583333333333,
		power: -371.532289834686,
		road: 7197.06462962964,
		acceleration: -0.275740740740741
	},
	{
		id: 749,
		time: 748,
		velocity: 2.31916666666667,
		power: -107.44927468691,
		road: 7199.55439814815,
		acceleration: -0.176388888888888
	},
	{
		id: 750,
		time: 749,
		velocity: 2.39972222222222,
		power: 324.423178924439,
		road: 7201.96166666667,
		acceleration: 0.011388888888888
	},
	{
		id: 751,
		time: 750,
		velocity: 2.52,
		power: 505.41734616898,
		road: 7204.41768518519,
		acceleration: 0.0861111111111112
	},
	{
		id: 752,
		time: 751,
		velocity: 2.5775,
		power: 598.310488085798,
		road: 7206.97453703704,
		acceleration: 0.115555555555556
	},
	{
		id: 753,
		time: 752,
		velocity: 2.74638888888889,
		power: 570.858601255735,
		road: 7209.63648148149,
		acceleration: 0.0946296296296296
	},
	{
		id: 754,
		time: 753,
		velocity: 2.80388888888889,
		power: 625.337078531647,
		road: 7212.39916666667,
		acceleration: 0.106851851851852
	},
	{
		id: 755,
		time: 754,
		velocity: 2.89805555555556,
		power: 445.732063770157,
		road: 7215.23222222223,
		acceleration: 0.0338888888888884
	},
	{
		id: 756,
		time: 755,
		velocity: 2.84805555555556,
		power: 676.851407240192,
		road: 7218.1388425926,
		acceleration: 0.113240740740741
	},
	{
		id: 757,
		time: 756,
		velocity: 3.14361111111111,
		power: 909.028511541434,
		road: 7221.1925925926,
		acceleration: 0.181018518518519
	},
	{
		id: 758,
		time: 757,
		velocity: 3.44111111111111,
		power: 990.384217302731,
		road: 7224.4312962963,
		acceleration: 0.188888888888889
	},
	{
		id: 759,
		time: 758,
		velocity: 3.41472222222222,
		power: 539.276349348876,
		road: 7227.78236111112,
		acceleration: 0.0358333333333336
	},
	{
		id: 760,
		time: 759,
		velocity: 3.25111111111111,
		power: 248.928753246125,
		road: 7231.12375000001,
		acceleration: -0.0551851851851852
	},
	{
		id: 761,
		time: 760,
		velocity: 3.27555555555556,
		power: 310.5477276338,
		road: 7234.42041666667,
		acceleration: -0.034259259259259
	},
	{
		id: 762,
		time: 761,
		velocity: 3.31194444444444,
		power: 454.928263829553,
		road: 7237.70615740741,
		acceleration: 0.012407407407407
	},
	{
		id: 763,
		time: 762,
		velocity: 3.28833333333333,
		power: 605.623287923488,
		road: 7241.02736111112,
		acceleration: 0.0585185185185186
	},
	{
		id: 764,
		time: 763,
		velocity: 3.45111111111111,
		power: 421.904312799235,
		road: 7244.37731481482,
		acceleration: -0.00101851851851897
	},
	{
		id: 765,
		time: 764,
		velocity: 3.30888888888889,
		power: 494.173137549889,
		road: 7247.73736111112,
		acceleration: 0.0212037037037045
	},
	{
		id: 766,
		time: 765,
		velocity: 3.35194444444444,
		power: 551.285300026802,
		road: 7251.12675925926,
		acceleration: 0.0374999999999996
	},
	{
		id: 767,
		time: 766,
		velocity: 3.56361111111111,
		power: 826.28015685631,
		road: 7254.59337962963,
		acceleration: 0.116944444444445
	},
	{
		id: 768,
		time: 767,
		velocity: 3.65972222222222,
		power: 728.988426419309,
		road: 7258.15888888889,
		acceleration: 0.0808333333333331
	},
	{
		id: 769,
		time: 768,
		velocity: 3.59444444444444,
		power: 566.369564018335,
		road: 7261.77981481482,
		acceleration: 0.0300000000000002
	},
	{
		id: 770,
		time: 769,
		velocity: 3.65361111111111,
		power: 594.865131168323,
		road: 7265.43402777778,
		acceleration: 0.0365740740740734
	},
	{
		id: 771,
		time: 770,
		velocity: 3.76944444444444,
		power: 556.52372438221,
		road: 7269.11856481482,
		acceleration: 0.0240740740740741
	},
	{
		id: 772,
		time: 771,
		velocity: 3.66666666666667,
		power: 596.941533368875,
		road: 7272.83222222223,
		acceleration: 0.0341666666666671
	},
	{
		id: 773,
		time: 772,
		velocity: 3.75611111111111,
		power: 498.349962566489,
		road: 7276.56564814815,
		acceleration: 0.00537037037037003
	},
	{
		id: 774,
		time: 773,
		velocity: 3.78555555555556,
		power: 644.871058520574,
		road: 7280.32444444445,
		acceleration: 0.0453703703703709
	},
	{
		id: 775,
		time: 774,
		velocity: 3.80277777777778,
		power: 679.124402871369,
		road: 7284.13208333334,
		acceleration: 0.0523148148148143
	},
	{
		id: 776,
		time: 775,
		velocity: 3.91305555555556,
		power: 683.728634253118,
		road: 7287.9912962963,
		acceleration: 0.0508333333333333
	},
	{
		id: 777,
		time: 776,
		velocity: 3.93805555555556,
		power: 891.329127756695,
		road: 7291.92712962964,
		acceleration: 0.102407407407408
	},
	{
		id: 778,
		time: 777,
		velocity: 4.11,
		power: 641.680376365102,
		road: 7295.93037037038,
		acceleration: 0.032407407407407
	},
	{
		id: 779,
		time: 778,
		velocity: 4.01027777777778,
		power: 487.769367683983,
		road: 7299.94555555556,
		acceleration: -0.00851851851851837
	},
	{
		id: 780,
		time: 779,
		velocity: 3.9125,
		power: -237.33145737408,
		road: 7303.85652777778,
		acceleration: -0.199907407407407
	},
	{
		id: 781,
		time: 780,
		velocity: 3.51027777777778,
		power: -890.600200527218,
		road: 7307.47037037038,
		acceleration: -0.394351851851852
	},
	{
		id: 782,
		time: 781,
		velocity: 2.82722222222222,
		power: -747.409029650807,
		road: 7310.69842592593,
		acceleration: -0.377222222222223
	},
	{
		id: 783,
		time: 782,
		velocity: 2.78083333333333,
		power: -286.028725523565,
		road: 7313.62018518519,
		acceleration: -0.23537037037037
	},
	{
		id: 784,
		time: 783,
		velocity: 2.80416666666667,
		power: 301.910073971308,
		road: 7316.41527777778,
		acceleration: -0.017962962962963
	},
	{
		id: 785,
		time: 784,
		velocity: 2.77333333333333,
		power: 127.987711052189,
		road: 7319.16013888889,
		acceleration: -0.0825
	},
	{
		id: 786,
		time: 785,
		velocity: 2.53333333333333,
		power: -128.167327078709,
		road: 7321.77226851852,
		acceleration: -0.182962962962963
	},
	{
		id: 787,
		time: 786,
		velocity: 2.25527777777778,
		power: -191.12741878164,
		road: 7324.18583333334,
		acceleration: -0.214166666666666
	},
	{
		id: 788,
		time: 787,
		velocity: 2.13083333333333,
		power: -100.687932302826,
		road: 7326.40328703704,
		acceleration: -0.178055555555556
	},
	{
		id: 789,
		time: 788,
		velocity: 1.99916666666667,
		power: 52.2293945380467,
		road: 7328.4800462963,
		acceleration: -0.103333333333333
	},
	{
		id: 790,
		time: 789,
		velocity: 1.94527777777778,
		power: 206.765861565135,
		road: 7330.49439814815,
		acceleration: -0.0214814814814814
	},
	{
		id: 791,
		time: 790,
		velocity: 2.06638888888889,
		power: 366.179815714497,
		road: 7332.52805555556,
		acceleration: 0.0600925925925924
	},
	{
		id: 792,
		time: 791,
		velocity: 2.17944444444444,
		power: 402.760824476902,
		road: 7334.62791666667,
		acceleration: 0.0723148148148152
	},
	{
		id: 793,
		time: 792,
		velocity: 2.16222222222222,
		power: 347.926534247402,
		road: 7336.78398148149,
		acceleration: 0.0400925925925919
	},
	{
		id: 794,
		time: 793,
		velocity: 2.18666666666667,
		power: 199.642511706212,
		road: 7338.9437962963,
		acceleration: -0.0325925925925921
	},
	{
		id: 795,
		time: 794,
		velocity: 2.08166666666667,
		power: 179.664618404819,
		road: 7341.06694444445,
		acceleration: -0.0407407407407412
	},
	{
		id: 796,
		time: 795,
		velocity: 2.04,
		power: 180.703107307121,
		road: 7343.15050925927,
		acceleration: -0.0384259259259254
	},
	{
		id: 797,
		time: 796,
		velocity: 2.07138888888889,
		power: 200.237998302247,
		road: 7345.20143518519,
		acceleration: -0.0268518518518519
	},
	{
		id: 798,
		time: 797,
		velocity: 2.00111111111111,
		power: 355.739460808449,
		road: 7347.26490740741,
		acceleration: 0.0519444444444441
	},
	{
		id: 799,
		time: 798,
		velocity: 2.19583333333333,
		power: 363.395204618863,
		road: 7349.37995370371,
		acceleration: 0.0512037037037039
	},
	{
		id: 800,
		time: 799,
		velocity: 2.225,
		power: 312.891166369063,
		road: 7351.53222222223,
		acceleration: 0.0232407407407411
	},
	{
		id: 801,
		time: 800,
		velocity: 2.07083333333333,
		power: 222.267774979579,
		road: 7353.68550925927,
		acceleration: -0.0212037037037041
	},
	{
		id: 802,
		time: 801,
		velocity: 2.13222222222222,
		power: 148.073863051233,
		road: 7355.80013888889,
		acceleration: -0.0561111111111114
	},
	{
		id: 803,
		time: 802,
		velocity: 2.05666666666667,
		power: 282.830247880881,
		road: 7357.89300925926,
		acceleration: 0.0125925925925929
	},
	{
		id: 804,
		time: 803,
		velocity: 2.10861111111111,
		power: 309.542871402712,
		road: 7360.00449074075,
		acceleration: 0.0246296296296298
	},
	{
		id: 805,
		time: 804,
		velocity: 2.20611111111111,
		power: 316.390703765554,
		road: 7362.1413425926,
		acceleration: 0.0261111111111112
	},
	{
		id: 806,
		time: 805,
		velocity: 2.135,
		power: 221.333930943133,
		road: 7364.28078703704,
		acceleration: -0.0209259259259262
	},
	{
		id: 807,
		time: 806,
		velocity: 2.04583333333333,
		power: 158.907573911731,
		road: 7366.38462962964,
		acceleration: -0.0502777777777776
	},
	{
		id: 808,
		time: 807,
		velocity: 2.05527777777778,
		power: 147.524402576772,
		road: 7368.4363425926,
		acceleration: -0.0539814814814816
	},
	{
		id: 809,
		time: 808,
		velocity: 1.97305555555556,
		power: -165.500335128987,
		road: 7370.35074074075,
		acceleration: -0.220648148148148
	},
	{
		id: 810,
		time: 809,
		velocity: 1.38388888888889,
		power: -59.0505471378485,
		road: 7372.07217592593,
		acceleration: -0.165277777777778
	},
	{
		id: 811,
		time: 810,
		velocity: 1.55944444444444,
		power: -64.6002473773747,
		road: 7373.62462962964,
		acceleration: -0.172685185185185
	},
	{
		id: 812,
		time: 811,
		velocity: 1.455,
		power: -389.97578485758,
		road: 7374.8600925926,
		acceleration: -0.461296296296296
	},
	{
		id: 813,
		time: 812,
		velocity: 0,
		power: -56.6735409445878,
		road: 7375.76800925927,
		acceleration: -0.193796296296296
	},
	{
		id: 814,
		time: 813,
		velocity: 0.978055555555555,
		power: -38.1183939034475,
		road: 7376.48717592593,
		acceleration: -0.183703703703704
	},
	{
		id: 815,
		time: 814,
		velocity: 0.903888888888889,
		power: 292.844485966359,
		road: 7377.25245370371,
		acceleration: 0.275925925925926
	},
	{
		id: 816,
		time: 815,
		velocity: 0.827777777777778,
		power: 321.735308792201,
		road: 7378.2600925926,
		acceleration: 0.208796296296296
	},
	{
		id: 817,
		time: 816,
		velocity: 1.60444444444444,
		power: 713.384906853415,
		road: 7379.59064814815,
		acceleration: 0.437037037037037
	},
	{
		id: 818,
		time: 817,
		velocity: 2.215,
		power: 725.808374765453,
		road: 7381.29916666667,
		acceleration: 0.318888888888889
	},
	{
		id: 819,
		time: 818,
		velocity: 1.78444444444444,
		power: 362.806272682555,
		road: 7383.20287037038,
		acceleration: 0.0714814814814815
	},
	{
		id: 820,
		time: 819,
		velocity: 1.81888888888889,
		power: 51.0758749257355,
		road: 7385.09185185186,
		acceleration: -0.100925925925926
	},
	{
		id: 821,
		time: 820,
		velocity: 1.91222222222222,
		power: 402.13687103358,
		road: 7386.97805555556,
		acceleration: 0.0953703703703706
	},
	{
		id: 822,
		time: 821,
		velocity: 2.07055555555556,
		power: 466.180621037638,
		road: 7388.97046296297,
		acceleration: 0.117037037037037
	},
	{
		id: 823,
		time: 822,
		velocity: 2.17,
		power: 448.603735038901,
		road: 7391.06912037038,
		acceleration: 0.0954629629629631
	},
	{
		id: 824,
		time: 823,
		velocity: 2.19861111111111,
		power: 325.953623603911,
		road: 7393.23000000001,
		acceleration: 0.0289814814814813
	},
	{
		id: 825,
		time: 824,
		velocity: 2.1575,
		power: 383.445813525636,
		road: 7395.43208333334,
		acceleration: 0.053425925925926
	},
	{
		id: 826,
		time: 825,
		velocity: 2.33027777777778,
		power: 336.99090156515,
		road: 7397.67495370371,
		acceleration: 0.0281481481481483
	},
	{
		id: 827,
		time: 826,
		velocity: 2.28305555555556,
		power: 604.388183426335,
		road: 7400.00347222223,
		acceleration: 0.143148148148148
	},
	{
		id: 828,
		time: 827,
		velocity: 2.58694444444444,
		power: 545.250145516271,
		road: 7402.45537037038,
		acceleration: 0.103611111111111
	},
	{
		id: 829,
		time: 828,
		velocity: 2.64111111111111,
		power: 763.625023653327,
		road: 7405.04865740741,
		acceleration: 0.179166666666667
	},
	{
		id: 830,
		time: 829,
		velocity: 2.82055555555556,
		power: 558.077872395645,
		road: 7407.77365740741,
		acceleration: 0.0842592592592593
	},
	{
		id: 831,
		time: 830,
		velocity: 2.83972222222222,
		power: 503.98471377858,
		road: 7410.56986111112,
		acceleration: 0.0581481481481485
	},
	{
		id: 832,
		time: 831,
		velocity: 2.81555555555556,
		power: 339.097774600075,
		road: 7413.39250000001,
		acceleration: -0.00527777777777816
	},
	{
		id: 833,
		time: 832,
		velocity: 2.80472222222222,
		power: 141.943081379871,
		road: 7416.17351851852,
		acceleration: -0.077962962962963
	},
	{
		id: 834,
		time: 833,
		velocity: 2.60583333333333,
		power: -94.8097921679707,
		road: 7418.83106481482,
		acceleration: -0.168981481481481
	},
	{
		id: 835,
		time: 834,
		velocity: 2.30861111111111,
		power: -87.5275276643825,
		road: 7421.32013888889,
		acceleration: -0.167962962962963
	},
	{
		id: 836,
		time: 835,
		velocity: 2.30083333333333,
		power: -83.8183964909941,
		road: 7423.64097222223,
		acceleration: -0.168518518518518
	},
	{
		id: 837,
		time: 836,
		velocity: 2.10027777777778,
		power: 152.167556542868,
		road: 7425.8487962963,
		acceleration: -0.0574999999999997
	},
	{
		id: 838,
		time: 837,
		velocity: 2.13611111111111,
		power: 155.227957088282,
		road: 7428.00087962963,
		acceleration: -0.0539814814814816
	},
	{
		id: 839,
		time: 838,
		velocity: 2.13888888888889,
		power: 299.277631170213,
		road: 7430.13490740741,
		acceleration: 0.0178703703703702
	},
	{
		id: 840,
		time: 839,
		velocity: 2.15388888888889,
		power: 161.548681807614,
		road: 7432.25310185186,
		acceleration: -0.0495370370370369
	},
	{
		id: 841,
		time: 840,
		velocity: 1.9875,
		power: 149.575300190414,
		road: 7434.31976851852,
		acceleration: -0.0535185185185187
	},
	{
		id: 842,
		time: 841,
		velocity: 1.97833333333333,
		power: 248.660446179458,
		road: 7436.35907407408,
		acceleration: -0.0012037037037036
	},
	{
		id: 843,
		time: 842,
		velocity: 2.15027777777778,
		power: 248.877948107705,
		road: 7438.39726851852,
		acceleration: -0.00101851851851853
	},
	{
		id: 844,
		time: 843,
		velocity: 1.98444444444444,
		power: 341.209756280326,
		road: 7440.45736111112,
		acceleration: 0.0448148148148149
	},
	{
		id: 845,
		time: 844,
		velocity: 2.11277777777778,
		power: 216.84808973101,
		road: 7442.5300925926,
		acceleration: -0.0195370370370371
	},
	{
		id: 846,
		time: 845,
		velocity: 2.09166666666667,
		power: 450.454164578749,
		road: 7444.64060185186,
		acceleration: 0.095092592592593
	},
	{
		id: 847,
		time: 846,
		velocity: 2.26972222222222,
		power: 342.053282095884,
		road: 7446.81648148149,
		acceleration: 0.0356481481481481
	},
	{
		id: 848,
		time: 847,
		velocity: 2.21972222222222,
		power: 199.596734878491,
		road: 7448.99347222223,
		acceleration: -0.0334259259259264
	},
	{
		id: 849,
		time: 848,
		velocity: 1.99138888888889,
		power: 294.932314659114,
		road: 7451.16046296297,
		acceleration: 0.013425925925926
	},
	{
		id: 850,
		time: 849,
		velocity: 2.31,
		power: 297.621167444632,
		road: 7453.34106481482,
		acceleration: 0.0137962962962965
	},
	{
		id: 851,
		time: 850,
		velocity: 2.26111111111111,
		power: 508.144112265759,
		road: 7455.58291666667,
		acceleration: 0.108703703703704
	},
	{
		id: 852,
		time: 851,
		velocity: 2.3175,
		power: 613.060344490051,
		road: 7457.95032407408,
		acceleration: 0.142407407407407
	},
	{
		id: 853,
		time: 852,
		velocity: 2.73722222222222,
		power: 1007.12387803157,
		road: 7460.52916666667,
		acceleration: 0.280462962962963
	},
	{
		id: 854,
		time: 853,
		velocity: 3.1025,
		power: 1027.61747565364,
		road: 7463.37268518519,
		acceleration: 0.248888888888889
	},
	{
		id: 855,
		time: 854,
		velocity: 3.06416666666667,
		power: 324.308651305039,
		road: 7466.33222222223,
		acceleration: -0.0168518518518512
	},
	{
		id: 856,
		time: 855,
		velocity: 2.68666666666667,
		power: -263.430006428472,
		road: 7469.16842592593,
		acceleration: -0.229814814814815
	},
	{
		id: 857,
		time: 856,
		velocity: 2.41305555555556,
		power: -231.169388129134,
		road: 7471.77740740741,
		acceleration: -0.22462962962963
	},
	{
		id: 858,
		time: 857,
		velocity: 2.39027777777778,
		power: -16.6626809561378,
		road: 7474.2050925926,
		acceleration: -0.137962962962963
	},
	{
		id: 859,
		time: 858,
		velocity: 2.27277777777778,
		power: 188.820774007458,
		road: 7476.54115740741,
		acceleration: -0.0452777777777773
	},
	{
		id: 860,
		time: 859,
		velocity: 2.27722222222222,
		power: 168.717307917453,
		road: 7478.82828703704,
		acceleration: -0.0525925925925925
	},
	{
		id: 861,
		time: 860,
		velocity: 2.2325,
		power: 137.0416667255,
		road: 7481.05643518519,
		acceleration: -0.0653703703703705
	},
	{
		id: 862,
		time: 861,
		velocity: 2.07666666666667,
		power: 249.970807009626,
		road: 7483.24699074074,
		acceleration: -0.00981481481481472
	},
	{
		id: 863,
		time: 862,
		velocity: 2.24777777777778,
		power: 108.847218459585,
		road: 7485.39435185186,
		acceleration: -0.0765740740740744
	},
	{
		id: 864,
		time: 863,
		velocity: 2.00277777777778,
		power: 224.682131748559,
		road: 7487.49486111112,
		acceleration: -0.0171296296296299
	},
	{
		id: 865,
		time: 864,
		velocity: 2.02527777777778,
		power: 184.745367273745,
		road: 7489.5688425926,
		acceleration: -0.0359259259259255
	},
	{
		id: 866,
		time: 865,
		velocity: 2.14,
		power: 398.935934168519,
		road: 7491.66046296297,
		acceleration: 0.0712037037037034
	},
	{
		id: 867,
		time: 866,
		velocity: 2.21638888888889,
		power: 380.54365325972,
		road: 7493.81574074075,
		acceleration: 0.056111111111111
	},
	{
		id: 868,
		time: 867,
		velocity: 2.19361111111111,
		power: 323.715655868047,
		road: 7496.01171296297,
		acceleration: 0.0252777777777782
	},
	{
		id: 869,
		time: 868,
		velocity: 2.21583333333333,
		power: 18.7118118101947,
		road: 7498.15990740741,
		acceleration: -0.120833333333334
	},
	{
		id: 870,
		time: 869,
		velocity: 1.85388888888889,
		power: 274.24938117383,
		road: 7500.25185185186,
		acceleration: 0.0083333333333333
	},
	{
		id: 871,
		time: 870,
		velocity: 2.21861111111111,
		power: 343.999415498169,
		road: 7502.36865740741,
		acceleration: 0.0413888888888887
	},
	{
		id: 872,
		time: 871,
		velocity: 2.34,
		power: 549.251083342985,
		road: 7504.57245370371,
		acceleration: 0.132592592592593
	},
	{
		id: 873,
		time: 872,
		velocity: 2.25166666666667,
		power: 247.455464968567,
		road: 7506.8350462963,
		acceleration: -0.0149999999999997
	},
	{
		id: 874,
		time: 873,
		velocity: 2.17361111111111,
		power: 133.684869275402,
		road: 7509.05675925926,
		acceleration: -0.0667592592592596
	},
	{
		id: 875,
		time: 874,
		velocity: 2.13972222222222,
		power: 241.826236853621,
		road: 7511.23847222222,
		acceleration: -0.0132407407407404
	},
	{
		id: 876,
		time: 875,
		velocity: 2.21194444444444,
		power: 283.568560293299,
		road: 7513.41712962963,
		acceleration: 0.00712962962962971
	},
	{
		id: 877,
		time: 876,
		velocity: 2.195,
		power: 303.801529541938,
		road: 7515.60740740741,
		acceleration: 0.0161111111111114
	},
	{
		id: 878,
		time: 877,
		velocity: 2.18805555555556,
		power: 246.153498256921,
		road: 7517.79986111111,
		acceleration: -0.0117592592592599
	},
	{
		id: 879,
		time: 878,
		velocity: 2.17666666666667,
		power: 310.21456302054,
		road: 7519.99583333334,
		acceleration: 0.0187962962962969
	},
	{
		id: 880,
		time: 879,
		velocity: 2.25138888888889,
		power: 223.006936224783,
		road: 7522.18972222222,
		acceleration: -0.0229629629629633
	},
	{
		id: 881,
		time: 880,
		velocity: 2.11916666666667,
		power: 157.60706933588,
		road: 7524.34564814815,
		acceleration: -0.0529629629629627
	},
	{
		id: 882,
		time: 881,
		velocity: 2.01777777777778,
		power: 323.116697029764,
		road: 7526.48953703704,
		acceleration: 0.0288888888888885
	},
	{
		id: 883,
		time: 882,
		velocity: 2.33805555555556,
		power: 640.826402805034,
		road: 7528.73328703704,
		acceleration: 0.170833333333333
	},
	{
		id: 884,
		time: 883,
		velocity: 2.63166666666667,
		power: 1008.93379014706,
		road: 7531.21157407408,
		acceleration: 0.298240740740741
	},
	{
		id: 885,
		time: 884,
		velocity: 2.9125,
		power: 1188.8417264205,
		road: 7533.99791666667,
		acceleration: 0.31787037037037
	},
	{
		id: 886,
		time: 885,
		velocity: 3.29166666666667,
		power: 718.695107257244,
		road: 7537.00296296296,
		acceleration: 0.119537037037037
	},
	{
		id: 887,
		time: 886,
		velocity: 2.99027777777778,
		power: 171.265629666442,
		road: 7540.0312962963,
		acceleration: -0.0729629629629631
	},
	{
		id: 888,
		time: 887,
		velocity: 2.69361111111111,
		power: -371.4049749586,
		road: 7542.88865740741,
		acceleration: -0.268981481481481
	},
	{
		id: 889,
		time: 888,
		velocity: 2.48472222222222,
		power: -343.715814308043,
		road: 7545.47592592593,
		acceleration: -0.271203703703705
	},
	{
		id: 890,
		time: 889,
		velocity: 2.17666666666667,
		power: -51.0390044038344,
		road: 7547.85097222222,
		acceleration: -0.15324074074074
	},
	{
		id: 891,
		time: 890,
		velocity: 2.23388888888889,
		power: 147.44703720484,
		road: 7550.11851851852,
		acceleration: -0.0617592592592593
	},
	{
		id: 892,
		time: 891,
		velocity: 2.29944444444444,
		power: 321.033877818873,
		road: 7552.36537037037,
		acceleration: 0.0203703703703702
	},
	{
		id: 893,
		time: 892,
		velocity: 2.23777777777778,
		power: 240.277995859329,
		road: 7554.61361111111,
		acceleration: -0.0175925925925924
	},
	{
		id: 894,
		time: 893,
		velocity: 2.18111111111111,
		power: 247.764189438916,
		road: 7556.84643518519,
		acceleration: -0.0132407407407409
	},
	{
		id: 895,
		time: 894,
		velocity: 2.25972222222222,
		power: 240.217891033204,
		road: 7559.06462962963,
		acceleration: -0.0160185185185187
	},
	{
		id: 896,
		time: 895,
		velocity: 2.18972222222222,
		power: 243.765923441758,
		road: 7561.26805555556,
		acceleration: -0.0135185185185183
	},
	{
		id: 897,
		time: 896,
		velocity: 2.14055555555556,
		power: 403.379124994578,
		road: 7563.4950925926,
		acceleration: 0.0607407407407408
	},
	{
		id: 898,
		time: 897,
		velocity: 2.44194444444444,
		power: 392.97108194043,
		road: 7565.77805555556,
		acceleration: 0.0511111111111111
	},
	{
		id: 899,
		time: 898,
		velocity: 2.34305555555556,
		power: 424.295723806093,
		road: 7568.11694444445,
		acceleration: 0.0607407407407408
	},
	{
		id: 900,
		time: 899,
		velocity: 2.32277777777778,
		power: 106.956030426507,
		road: 7570.44518518519,
		acceleration: -0.0820370370370371
	},
	{
		id: 901,
		time: 900,
		velocity: 2.19583333333333,
		power: 311.460790049213,
		road: 7572.7387962963,
		acceleration: 0.0127777777777776
	},
	{
		id: 902,
		time: 901,
		velocity: 2.38138888888889,
		power: 182.316358940755,
		road: 7575.01583333334,
		acceleration: -0.0459259259259257
	},
	{
		id: 903,
		time: 902,
		velocity: 2.185,
		power: 339.072537939262,
		road: 7577.28356481482,
		acceleration: 0.0273148148148148
	},
	{
		id: 904,
		time: 903,
		velocity: 2.27777777777778,
		power: 343.730446705887,
		road: 7579.57870370371,
		acceleration: 0.0274999999999999
	},
	{
		id: 905,
		time: 904,
		velocity: 2.46388888888889,
		power: 713.71335220969,
		road: 7581.97898148148,
		acceleration: 0.182777777777778
	},
	{
		id: 906,
		time: 905,
		velocity: 2.73333333333333,
		power: 752.049987419577,
		road: 7584.55870370371,
		acceleration: 0.176111111111111
	},
	{
		id: 907,
		time: 906,
		velocity: 2.80611111111111,
		power: 781.831451560276,
		road: 7587.31037037037,
		acceleration: 0.167777777777778
	},
	{
		id: 908,
		time: 907,
		velocity: 2.96722222222222,
		power: 354.962720235221,
		road: 7590.14592592593,
		acceleration: 0
	},
	{
		id: 909,
		time: 908,
		velocity: 2.73333333333333,
		power: 150.075516562317,
		road: 7592.94384259259,
		acceleration: -0.075277777777778
	},
	{
		id: 910,
		time: 909,
		velocity: 2.58027777777778,
		power: 34.2452320805772,
		road: 7595.6450462963,
		acceleration: -0.118148148148149
	},
	{
		id: 911,
		time: 910,
		velocity: 2.61277777777778,
		power: -57.4564739614927,
		road: 7598.20981481482,
		acceleration: -0.154722222222222
	},
	{
		id: 912,
		time: 911,
		velocity: 2.26916666666667,
		power: 86.0177281631661,
		road: 7600.65041666667,
		acceleration: -0.0936111111111106
	},
	{
		id: 913,
		time: 912,
		velocity: 2.29944444444444,
		power: -3.67941542027578,
		road: 7602.97814814815,
		acceleration: -0.13212962962963
	},
	{
		id: 914,
		time: 913,
		velocity: 2.21638888888889,
		power: 264.614037130795,
		road: 7605.23643518519,
		acceleration: -0.00675925925925958
	},
	{
		id: 915,
		time: 914,
		velocity: 2.24888888888889,
		power: 205.332949771266,
		road: 7607.47458333334,
		acceleration: -0.0335185185185183
	},
	{
		id: 916,
		time: 915,
		velocity: 2.19888888888889,
		power: 302.76658582115,
		road: 7609.7025,
		acceleration: 0.0130555555555554
	},
	{
		id: 917,
		time: 916,
		velocity: 2.25555555555556,
		power: 282.290029076798,
		road: 7611.93837962963,
		acceleration: 0.00287037037037052
	},
	{
		id: 918,
		time: 917,
		velocity: 2.2575,
		power: 186.098709144586,
		road: 7614.15486111111,
		acceleration: -0.0416666666666665
	},
	{
		id: 919,
		time: 918,
		velocity: 2.07388888888889,
		power: 247.420440816903,
		road: 7616.345,
		acceleration: -0.0110185185185188
	},
	{
		id: 920,
		time: 919,
		velocity: 2.2225,
		power: 303.351128632783,
		road: 7618.5375,
		acceleration: 0.0157407407407408
	},
	{
		id: 921,
		time: 920,
		velocity: 2.30472222222222,
		power: 280.999726246148,
		road: 7620.7400462963,
		acceleration: 0.0043518518518515
	},
	{
		id: 922,
		time: 921,
		velocity: 2.08694444444444,
		power: 150.782245970592,
		road: 7622.91625,
		acceleration: -0.0570370370370363
	},
	{
		id: 923,
		time: 922,
		velocity: 2.05138888888889,
		power: 283.181956518735,
		road: 7625.06828703704,
		acceleration: 0.00870370370370344
	},
	{
		id: 924,
		time: 923,
		velocity: 2.33083333333333,
		power: 406.316622889156,
		road: 7627.25745370371,
		acceleration: 0.0655555555555556
	},
	{
		id: 925,
		time: 924,
		velocity: 2.28361111111111,
		power: 622.52202982378,
		road: 7629.55689814815,
		acceleration: 0.155
	},
	{
		id: 926,
		time: 925,
		velocity: 2.51638888888889,
		power: 582.196618338012,
		road: 7631.99435185185,
		acceleration: 0.121018518518519
	},
	{
		id: 927,
		time: 926,
		velocity: 2.69388888888889,
		power: 644.869715373641,
		road: 7634.55925925926,
		acceleration: 0.133888888888889
	},
	{
		id: 928,
		time: 927,
		velocity: 2.68527777777778,
		power: 643.798761721459,
		road: 7637.25138888889,
		acceleration: 0.120555555555555
	},
	{
		id: 929,
		time: 928,
		velocity: 2.87805555555556,
		power: 799.53475961168,
		road: 7640.08643518519,
		acceleration: 0.165277777777778
	},
	{
		id: 930,
		time: 929,
		velocity: 3.18972222222222,
		power: 1105.67524590118,
		road: 7643.12925925926,
		acceleration: 0.250277777777777
	},
	{
		id: 931,
		time: 930,
		velocity: 3.43611111111111,
		power: 853.570945459603,
		road: 7646.36935185185,
		acceleration: 0.14425925925926
	},
	{
		id: 932,
		time: 931,
		velocity: 3.31083333333333,
		power: 653.848199574671,
		road: 7649.71759259259,
		acceleration: 0.0720370370370369
	},
	{
		id: 933,
		time: 932,
		velocity: 3.40583333333333,
		power: 543.844271569642,
		road: 7653.11912037037,
		acceleration: 0.0345370370370368
	},
	{
		id: 934,
		time: 933,
		velocity: 3.53972222222222,
		power: 964.22971403052,
		road: 7656.61601851852,
		acceleration: 0.156203703703703
	},
	{
		id: 935,
		time: 934,
		velocity: 3.77944444444444,
		power: 1155.89567316254,
		road: 7660.28925925926,
		acceleration: 0.196481481481482
	},
	{
		id: 936,
		time: 935,
		velocity: 3.99527777777778,
		power: 577.762970029833,
		road: 7664.07342592593,
		acceleration: 0.02537037037037
	},
	{
		id: 937,
		time: 936,
		velocity: 3.61583333333333,
		power: 696.266553675934,
		road: 7667.89833333334,
		acceleration: 0.0561111111111114
	},
	{
		id: 938,
		time: 937,
		velocity: 3.94777777777778,
		power: 1342.64059653785,
		road: 7671.86157407408,
		acceleration: 0.220555555555555
	},
	{
		id: 939,
		time: 938,
		velocity: 4.65694444444444,
		power: 3803.24396630945,
		road: 7676.31532407408,
		acceleration: 0.760462962962963
	},
	{
		id: 940,
		time: 939,
		velocity: 5.89722222222222,
		power: 5574.90068398093,
		road: 7681.62958333334,
		acceleration: 0.960555555555557
	},
	{
		id: 941,
		time: 940,
		velocity: 6.82944444444444,
		power: 5522.33825230323,
		road: 7687.81884259259,
		acceleration: 0.789444444444444
	},
	{
		id: 942,
		time: 941,
		velocity: 7.02527777777778,
		power: 3676.89262329217,
		road: 7694.61074074074,
		acceleration: 0.415833333333334
	},
	{
		id: 943,
		time: 942,
		velocity: 7.14472222222222,
		power: 2228.05511503467,
		road: 7701.69796296297,
		acceleration: 0.174814814814814
	},
	{
		id: 944,
		time: 943,
		velocity: 7.35388888888889,
		power: 2443.03417298408,
		road: 7708.97055555556,
		acceleration: 0.195925925925926
	},
	{
		id: 945,
		time: 944,
		velocity: 7.61305555555556,
		power: 2544.39626381951,
		road: 7716.44069444445,
		acceleration: 0.199166666666668
	},
	{
		id: 946,
		time: 945,
		velocity: 7.74222222222222,
		power: 1977.75864477549,
		road: 7724.06662037037,
		acceleration: 0.112407407407407
	},
	{
		id: 947,
		time: 946,
		velocity: 7.69111111111111,
		power: 1166.01349938284,
		road: 7731.74819444445,
		acceleration: -0.00111111111111128
	},
	{
		id: 948,
		time: 947,
		velocity: 7.60972222222222,
		power: 969.720717736906,
		road: 7739.41541666667,
		acceleration: -0.0275925925925922
	},
	{
		id: 949,
		time: 948,
		velocity: 7.65944444444444,
		power: 779.326410255939,
		road: 7747.04245370371,
		acceleration: -0.0527777777777771
	},
	{
		id: 950,
		time: 949,
		velocity: 7.53277777777778,
		power: 1138.31686060146,
		road: 7754.64185185185,
		acceleration: -0.00250000000000039
	},
	{
		id: 951,
		time: 950,
		velocity: 7.60222222222222,
		power: 300.680188279313,
		road: 7762.18125,
		acceleration: -0.1175
	},
	{
		id: 952,
		time: 951,
		velocity: 7.30694444444444,
		power: -331.42830672031,
		road: 7769.55925925926,
		acceleration: -0.205277777777778
	},
	{
		id: 953,
		time: 952,
		velocity: 6.91694444444444,
		power: -2516.08829472456,
		road: 7776.56837962963,
		acceleration: -0.532500000000001
	},
	{
		id: 954,
		time: 953,
		velocity: 6.00472222222222,
		power: -4336.65848415385,
		road: 7782.87467592593,
		acceleration: -0.873148148148148
	},
	{
		id: 955,
		time: 954,
		velocity: 4.6875,
		power: -4149.31749254608,
		road: 7788.26763888889,
		acceleration: -0.953518518518519
	},
	{
		id: 956,
		time: 955,
		velocity: 4.05638888888889,
		power: -3276.42374251425,
		road: 7792.72777777778,
		acceleration: -0.91212962962963
	},
	{
		id: 957,
		time: 956,
		velocity: 3.26833333333333,
		power: -1846.37686118043,
		road: 7796.39949074074,
		acceleration: -0.664722222222222
	},
	{
		id: 958,
		time: 957,
		velocity: 2.69333333333333,
		power: -1637.50287406767,
		road: 7799.38347222222,
		acceleration: -0.710740740740741
	},
	{
		id: 959,
		time: 958,
		velocity: 1.92416666666667,
		power: -744.849728411192,
		road: 7801.78314814815,
		acceleration: -0.45787037037037
	},
	{
		id: 960,
		time: 959,
		velocity: 1.89472222222222,
		power: -384.88987783239,
		road: 7803.78782407408,
		acceleration: -0.33212962962963
	},
	{
		id: 961,
		time: 960,
		velocity: 1.69694444444444,
		power: 108.945606282906,
		road: 7805.59361111111,
		acceleration: -0.0656481481481479
	},
	{
		id: 962,
		time: 961,
		velocity: 1.72722222222222,
		power: 212.712014149247,
		road: 7807.36527777778,
		acceleration: -0.0025925925925927
	},
	{
		id: 963,
		time: 962,
		velocity: 1.88694444444444,
		power: 528.705796139713,
		road: 7809.22115740741,
		acceleration: 0.171018518518518
	},
	{
		id: 964,
		time: 963,
		velocity: 2.21,
		power: 1300.84492194517,
		road: 7811.41064814815,
		acceleration: 0.496203703703704
	},
	{
		id: 965,
		time: 964,
		velocity: 3.21583333333333,
		power: 2795.77840055466,
		road: 7814.29319444445,
		acceleration: 0.889907407407407
	},
	{
		id: 966,
		time: 965,
		velocity: 4.55666666666667,
		power: 3719.91163044785,
		road: 7818.07143518519,
		acceleration: 0.90148148148148
	},
	{
		id: 967,
		time: 966,
		velocity: 4.91444444444444,
		power: 2726.45071571707,
		road: 7822.55143518519,
		acceleration: 0.502037037037038
	},
	{
		id: 968,
		time: 967,
		velocity: 4.72194444444444,
		power: 539.921177899981,
		road: 7827.27268518518,
		acceleration: -0.0195370370370371
	},
	{
		id: 969,
		time: 968,
		velocity: 4.49805555555556,
		power: 69.1606077129923,
		road: 7831.92222222222,
		acceleration: -0.123888888888889
	},
	{
		id: 970,
		time: 969,
		velocity: 4.54277777777778,
		power: 1075.9623020138,
		road: 7836.56212962963,
		acceleration: 0.10462962962963
	},
	{
		id: 971,
		time: 970,
		velocity: 5.03583333333333,
		power: 2880.80822752047,
		road: 7841.49138888889,
		acceleration: 0.474074074074073
	},
	{
		id: 972,
		time: 971,
		velocity: 5.92027777777778,
		power: 4044.56110370006,
		road: 7846.97365740741,
		acceleration: 0.631944444444445
	},
	{
		id: 973,
		time: 972,
		velocity: 6.43861111111111,
		power: 6082.1850938582,
		road: 7853.21009259259,
		acceleration: 0.876388888888888
	},
	{
		id: 974,
		time: 973,
		velocity: 7.665,
		power: 7724.78789843261,
		road: 7860.37333333333,
		acceleration: 0.977222222222223
	},
	{
		id: 975,
		time: 974,
		velocity: 8.85194444444445,
		power: 9301.64990351656,
		road: 7868.54087962963,
		acceleration: 1.03138888888889
	},
	{
		id: 976,
		time: 975,
		velocity: 9.53277777777778,
		power: 10055.3604473887,
		road: 7877.71226851852,
		acceleration: 0.976296296296296
	},
	{
		id: 977,
		time: 976,
		velocity: 10.5938888888889,
		power: 9025.66711891194,
		road: 7887.75143518518,
		acceleration: 0.75925925925926
	},
	{
		id: 978,
		time: 977,
		velocity: 11.1297222222222,
		power: 9586.29363835921,
		road: 7898.53976851852,
		acceleration: 0.739074074074074
	},
	{
		id: 979,
		time: 978,
		velocity: 11.75,
		power: 6141.97493367695,
		road: 7909.88162037037,
		acceleration: 0.367962962962963
	},
	{
		id: 980,
		time: 979,
		velocity: 11.6977777777778,
		power: 3591.86163217736,
		road: 7921.46842592592,
		acceleration: 0.121944444444445
	},
	{
		id: 981,
		time: 980,
		velocity: 11.4955555555556,
		power: 877.291448700248,
		road: 7933.05435185185,
		acceleration: -0.123703703703704
	},
	{
		id: 982,
		time: 981,
		velocity: 11.3788888888889,
		power: 450.283463547806,
		road: 7944.49842592592,
		acceleration: -0.16
	},
	{
		id: 983,
		time: 982,
		velocity: 11.2177777777778,
		power: 847.420780916465,
		road: 7955.80208333333,
		acceleration: -0.120833333333334
	},
	{
		id: 984,
		time: 983,
		velocity: 11.1330555555556,
		power: 1207.3627094265,
		road: 7967.00277777778,
		acceleration: -0.0850925925925914
	},
	{
		id: 985,
		time: 984,
		velocity: 11.1236111111111,
		power: 1231.4401239503,
		road: 7978.12046296296,
		acceleration: -0.080925925925925
	},
	{
		id: 986,
		time: 985,
		velocity: 10.975,
		power: 1388.29736186077,
		road: 7989.16550925926,
		acceleration: -0.0643518518518533
	},
	{
		id: 987,
		time: 986,
		velocity: 10.94,
		power: 969.259968975785,
		road: 8000.12717592592,
		acceleration: -0.102407407407409
	},
	{
		id: 988,
		time: 987,
		velocity: 10.8163888888889,
		power: 1058.51460293561,
		road: 8010.99175925926,
		acceleration: -0.0917592592592573
	},
	{
		id: 989,
		time: 988,
		velocity: 10.6997222222222,
		power: 601.590480661389,
		road: 8021.74351851852,
		acceleration: -0.13388888888889
	},
	{
		id: 990,
		time: 989,
		velocity: 10.5383333333333,
		power: -70.6002092348387,
		road: 8032.32953703704,
		acceleration: -0.197592592592592
	},
	{
		id: 991,
		time: 990,
		velocity: 10.2236111111111,
		power: 326.828291202782,
		road: 8042.73898148148,
		acceleration: -0.155555555555555
	},
	{
		id: 992,
		time: 991,
		velocity: 10.2330555555556,
		power: 2117.48794711346,
		road: 8053.08416666666,
		acceleration: 0.0270370370370365
	},
	{
		id: 993,
		time: 992,
		velocity: 10.6194444444444,
		power: 3830.96090709016,
		road: 8063.54055555555,
		acceleration: 0.195370370370371
	},
	{
		id: 994,
		time: 993,
		velocity: 10.8097222222222,
		power: 5675.3407297835,
		road: 8074.27574074074,
		acceleration: 0.36222222222222
	},
	{
		id: 995,
		time: 994,
		velocity: 11.3197222222222,
		power: 4895.68682315352,
		road: 8085.32625,
		acceleration: 0.268425925925927
	},
	{
		id: 996,
		time: 995,
		velocity: 11.4247222222222,
		power: 4489.60675846199,
		road: 8096.61976851852,
		acceleration: 0.217592592592593
	},
	{
		id: 997,
		time: 996,
		velocity: 11.4625,
		power: 2860.85961699396,
		road: 8108.05273148148,
		acceleration: 0.0612962962962964
	},
	{
		id: 998,
		time: 997,
		velocity: 11.5036111111111,
		power: 2652.7301230699,
		road: 8119.53657407407,
		acceleration: 0.0404629629629625
	},
	{
		id: 999,
		time: 998,
		velocity: 11.5461111111111,
		power: 2650.31782762699,
		road: 8131.06009259259,
		acceleration: 0.0388888888888879
	},
	{
		id: 1000,
		time: 999,
		velocity: 11.5791666666667,
		power: 2650.37026823633,
		road: 8142.62185185185,
		acceleration: 0.0375925925925937
	},
	{
		id: 1001,
		time: 1000,
		velocity: 11.6163888888889,
		power: 3433.40674377472,
		road: 8154.25527777778,
		acceleration: 0.105740740740741
	},
	{
		id: 1002,
		time: 1001,
		velocity: 11.8633333333333,
		power: 3257.51122119246,
		road: 8165.98467592592,
		acceleration: 0.0862037037037044
	},
	{
		id: 1003,
		time: 1002,
		velocity: 11.8377777777778,
		power: 3471.16637227259,
		road: 8177.80796296296,
		acceleration: 0.101574074074072
	},
	{
		id: 1004,
		time: 1003,
		velocity: 11.9211111111111,
		power: 2336.0034928464,
		road: 8189.68171296296,
		acceleration: -0.00064814814814973
	},
	{
		id: 1005,
		time: 1004,
		velocity: 11.8613888888889,
		power: 2661.98993921396,
		road: 8201.56898148148,
		acceleration: 0.027685185185188
	},
	{
		id: 1006,
		time: 1005,
		velocity: 11.9208333333333,
		power: 3194.46055810767,
		road: 8213.50648148148,
		acceleration: 0.0727777777777767
	},
	{
		id: 1007,
		time: 1006,
		velocity: 12.1394444444444,
		power: 3878.39952664891,
		road: 8225.54467592592,
		acceleration: 0.128611111111111
	},
	{
		id: 1008,
		time: 1007,
		velocity: 12.2472222222222,
		power: 3817.66950069001,
		road: 8237.70629629629,
		acceleration: 0.118240740740744
	},
	{
		id: 1009,
		time: 1008,
		velocity: 12.2755555555556,
		power: 3635.224120172,
		road: 8249.97615740741,
		acceleration: 0.0982407407407386
	},
	{
		id: 1010,
		time: 1009,
		velocity: 12.4341666666667,
		power: 3780.95279990255,
		road: 8262.34842592592,
		acceleration: 0.106574074074073
	},
	{
		id: 1011,
		time: 1010,
		velocity: 12.5669444444444,
		power: 3190.77190715009,
		road: 8274.80083333333,
		acceleration: 0.0537037037037038
	},
	{
		id: 1012,
		time: 1011,
		velocity: 12.4366666666667,
		power: 2155.67609769367,
		road: 8287.26324074074,
		acceleration: -0.0337037037037025
	},
	{
		id: 1013,
		time: 1012,
		velocity: 12.3330555555556,
		power: 914.200720033081,
		road: 8299.64060185185,
		acceleration: -0.136388888888888
	},
	{
		id: 1014,
		time: 1013,
		velocity: 12.1577777777778,
		power: 916.888965302744,
		road: 8311.88305555555,
		acceleration: -0.133425925925927
	},
	{
		id: 1015,
		time: 1014,
		velocity: 12.0363888888889,
		power: -417.467362319526,
		road: 8323.93597222222,
		acceleration: -0.245648148148149
	},
	{
		id: 1016,
		time: 1015,
		velocity: 11.5961111111111,
		power: -1941.21259844761,
		road: 8335.67680555555,
		acceleration: -0.378518518518518
	},
	{
		id: 1017,
		time: 1016,
		velocity: 11.0222222222222,
		power: -2583.94585880909,
		road: 8347.00888888889,
		acceleration: -0.438981481481481
	},
	{
		id: 1018,
		time: 1017,
		velocity: 10.7194444444444,
		power: -1996.77268662611,
		road: 8357.92824074074,
		acceleration: -0.386481481481482
	},
	{
		id: 1019,
		time: 1018,
		velocity: 10.4366666666667,
		power: -343.745390951731,
		road: 8368.54189814815,
		acceleration: -0.224907407407407
	},
	{
		id: 1020,
		time: 1019,
		velocity: 10.3475,
		power: 708.593419217981,
		road: 8378.98425925926,
		acceleration: -0.117685185185184
	},
	{
		id: 1021,
		time: 1020,
		velocity: 10.3663888888889,
		power: 1698.72453532298,
		road: 8389.35962962963,
		acceleration: -0.0162962962962965
	},
	{
		id: 1022,
		time: 1021,
		velocity: 10.3877777777778,
		power: 2060.05754959597,
		road: 8399.73694444444,
		acceleration: 0.020185185185186
	},
	{
		id: 1023,
		time: 1022,
		velocity: 10.4080555555556,
		power: 1476.2006950427,
		road: 8410.10504629629,
		acceleration: -0.0386111111111109
	},
	{
		id: 1024,
		time: 1023,
		velocity: 10.2505555555556,
		power: 1047.87545117575,
		road: 8420.41351851852,
		acceleration: -0.0806481481481498
	},
	{
		id: 1025,
		time: 1024,
		velocity: 10.1458333333333,
		power: 375.803663898475,
		road: 8430.60800925926,
		acceleration: -0.147314814814816
	},
	{
		id: 1026,
		time: 1025,
		velocity: 9.96611111111111,
		power: -459.42394785013,
		road: 8440.61282407407,
		acceleration: -0.232037037037037
	},
	{
		id: 1027,
		time: 1026,
		velocity: 9.55444444444444,
		power: -370.760227413732,
		road: 8450.39106481481,
		acceleration: -0.22111111111111
	},
	{
		id: 1028,
		time: 1027,
		velocity: 9.4825,
		power: -763.965437480069,
		road: 8459.92736111111,
		acceleration: -0.262777777777778
	},
	{
		id: 1029,
		time: 1028,
		velocity: 9.17777777777778,
		power: -80.5713943409603,
		road: 8469.23958333333,
		acceleration: -0.18537037037037
	},
	{
		id: 1030,
		time: 1029,
		velocity: 8.99833333333333,
		power: -411.99031387939,
		road: 8478.34828703704,
		acceleration: -0.221666666666668
	},
	{
		id: 1031,
		time: 1030,
		velocity: 8.8175,
		power: 1769.70014078116,
		road: 8487.36263888889,
		acceleration: 0.032962962962964
	},
	{
		id: 1032,
		time: 1031,
		velocity: 9.27666666666667,
		power: 6475.44891280865,
		road: 8496.67055555555,
		acceleration: 0.554166666666665
	},
	{
		id: 1033,
		time: 1032,
		velocity: 10.6608333333333,
		power: 9732.9520134378,
		road: 8506.67416666667,
		acceleration: 0.837222222222223
	},
	{
		id: 1034,
		time: 1033,
		velocity: 11.3291666666667,
		power: 8998.31236238397,
		road: 8517.43847222222,
		acceleration: 0.684166666666664
	},
	{
		id: 1035,
		time: 1034,
		velocity: 11.3291666666667,
		power: 9528.15480420271,
		road: 8528.88083333333,
		acceleration: 0.671944444444447
	},
	{
		id: 1036,
		time: 1035,
		velocity: 12.6766666666667,
		power: 12135.8925106298,
		road: 8541.07509259259,
		acceleration: 0.831851851851852
	},
	{
		id: 1037,
		time: 1036,
		velocity: 13.8247222222222,
		power: 13834.1547505907,
		road: 8554.12865740741,
		acceleration: 0.886759259259259
	},
	{
		id: 1038,
		time: 1037,
		velocity: 13.9894444444444,
		power: 7500.03730208089,
		road: 8567.79657407407,
		acceleration: 0.341944444444444
	},
	{
		id: 1039,
		time: 1038,
		velocity: 13.7025,
		power: 1053.03766971575,
		road: 8581.55842592592,
		acceleration: -0.154074074074074
	},
	{
		id: 1040,
		time: 1039,
		velocity: 13.3625,
		power: 1111.19643536989,
		road: 8595.17004629629,
		acceleration: -0.146388888888888
	},
	{
		id: 1041,
		time: 1040,
		velocity: 13.5502777777778,
		power: 2852.70078414192,
		road: 8608.70351851852,
		acceleration: -0.00990740740740925
	},
	{
		id: 1042,
		time: 1041,
		velocity: 13.6727777777778,
		power: 6057.90446514095,
		road: 8622.34833333333,
		acceleration: 0.232592592592594
	},
	{
		id: 1043,
		time: 1042,
		velocity: 14.0602777777778,
		power: 6380.90783851646,
		road: 8636.23203703703,
		acceleration: 0.245185185185186
	},
	{
		id: 1044,
		time: 1043,
		velocity: 14.2858333333333,
		power: 6894.21416769874,
		road: 8650.37347222222,
		acceleration: 0.270277777777775
	},
	{
		id: 1045,
		time: 1044,
		velocity: 14.4836111111111,
		power: 5619.40948005583,
		road: 8664.73305555555,
		acceleration: 0.166018518518518
	},
	{
		id: 1046,
		time: 1045,
		velocity: 14.5583333333333,
		power: 5242.14132985671,
		road: 8679.24171296296,
		acceleration: 0.132129629629631
	},
	{
		id: 1047,
		time: 1046,
		velocity: 14.6822222222222,
		power: 3589.78484597138,
		road: 8693.82166666666,
		acceleration: 0.0104629629629631
	},
	{
		id: 1048,
		time: 1047,
		velocity: 14.515,
		power: 1884.97840712909,
		road: 8708.35152777777,
		acceleration: -0.110648148148146
	},
	{
		id: 1049,
		time: 1048,
		velocity: 14.2263888888889,
		power: 514.626118693497,
		road: 8722.72287037037,
		acceleration: -0.20638888888889
	},
	{
		id: 1050,
		time: 1049,
		velocity: 14.0630555555556,
		power: 385.180809047204,
		road: 8736.88499999999,
		acceleration: -0.212037037037035
	},
	{
		id: 1051,
		time: 1050,
		velocity: 13.8788888888889,
		power: 2538.67666510459,
		road: 8750.91662037036,
		acceleration: -0.0489814814814835
	},
	{
		id: 1052,
		time: 1051,
		velocity: 14.0794444444444,
		power: 3776.85538326696,
		road: 8764.94550925925,
		acceleration: 0.0435185185185176
	},
	{
		id: 1053,
		time: 1052,
		velocity: 14.1936111111111,
		power: 5276.95214830628,
		road: 8779.07175925925,
		acceleration: 0.151203703703704
	},
	{
		id: 1054,
		time: 1053,
		velocity: 14.3325,
		power: 4790.92390063221,
		road: 8793.32851851851,
		acceleration: 0.109814814814817
	},
	{
		id: 1055,
		time: 1054,
		velocity: 14.4088888888889,
		power: 5040.54417677832,
		road: 8807.70180555555,
		acceleration: 0.123240740740741
	},
	{
		id: 1056,
		time: 1055,
		velocity: 14.5633333333333,
		power: 6082.96444069545,
		road: 8822.23259259258,
		acceleration: 0.191759259259257
	},
	{
		id: 1057,
		time: 1056,
		velocity: 14.9077777777778,
		power: 7803.15876166019,
		road: 8837.01032407406,
		acceleration: 0.302129629629631
	},
	{
		id: 1058,
		time: 1057,
		velocity: 15.3152777777778,
		power: 8742.86762318219,
		road: 8852.11398148147,
		acceleration: 0.349722222222223
	},
	{
		id: 1059,
		time: 1058,
		velocity: 15.6125,
		power: 7197.35759059969,
		road: 8867.50662037036,
		acceleration: 0.228240740740741
	},
	{
		id: 1060,
		time: 1059,
		velocity: 15.5925,
		power: 6167.54550452378,
		road: 8883.08828703703,
		acceleration: 0.149814814814814
	},
	{
		id: 1061,
		time: 1060,
		velocity: 15.7647222222222,
		power: 6191.6721757233,
		road: 8898.81731481481,
		acceleration: 0.144907407407409
	},
	{
		id: 1062,
		time: 1061,
		velocity: 16.0472222222222,
		power: 8230.60975592244,
		road: 8914.7536111111,
		acceleration: 0.269629629629629
	},
	{
		id: 1063,
		time: 1062,
		velocity: 16.4013888888889,
		power: 7942.23826956205,
		road: 8930.9436574074,
		acceleration: 0.237870370370374
	},
	{
		id: 1064,
		time: 1063,
		velocity: 16.4783333333333,
		power: 6824.01685576941,
		road: 8947.33097222221,
		acceleration: 0.156666666666663
	},
	{
		id: 1065,
		time: 1064,
		velocity: 16.5172222222222,
		power: 5596.73665437588,
		road: 8963.83347222221,
		acceleration: 0.0737037037037034
	},
	{
		id: 1066,
		time: 1065,
		velocity: 16.6225,
		power: 6623.38601492607,
		road: 8980.4399074074,
		acceleration: 0.134166666666665
	},
	{
		id: 1067,
		time: 1066,
		velocity: 16.8808333333333,
		power: 8945.21629054719,
		road: 8997.24828703703,
		acceleration: 0.269722222222221
	},
	{
		id: 1068,
		time: 1067,
		velocity: 17.3263888888889,
		power: 8835.92704872129,
		road: 9014.31629629629,
		acceleration: 0.24953703703704
	},
	{
		id: 1069,
		time: 1068,
		velocity: 17.3711111111111,
		power: 6185.42722267547,
		road: 9031.54925925925,
		acceleration: 0.0803703703703711
	},
	{
		id: 1070,
		time: 1069,
		velocity: 17.1219444444444,
		power: 3657.8162009654,
		road: 9048.78587962962,
		acceleration: -0.0730555555555554
	},
	{
		id: 1071,
		time: 1070,
		velocity: 17.1072222222222,
		power: 4366.11284301016,
		road: 9065.97180555555,
		acceleration: -0.028333333333336
	},
	{
		id: 1072,
		time: 1071,
		velocity: 17.2861111111111,
		power: 5644.01184955464,
		road: 9083.16805555555,
		acceleration: 0.0489814814814835
	},
	{
		id: 1073,
		time: 1072,
		velocity: 17.2688888888889,
		power: 7093.97395297674,
		road: 9100.45532407407,
		acceleration: 0.133055555555554
	},
	{
		id: 1074,
		time: 1073,
		velocity: 17.5063888888889,
		power: 9338.13978154531,
		road: 9117.93837962962,
		acceleration: 0.258518518518521
	},
	{
		id: 1075,
		time: 1074,
		velocity: 18.0616666666667,
		power: 10977.6582488439,
		road: 9135.72041666666,
		acceleration: 0.339444444444446
	},
	{
		id: 1076,
		time: 1075,
		velocity: 18.2872222222222,
		power: 10111.2509035181,
		road: 9153.80828703703,
		acceleration: 0.272222222222219
	},
	{
		id: 1077,
		time: 1076,
		velocity: 18.3230555555556,
		power: 7621.06445946447,
		road: 9172.09208333333,
		acceleration: 0.119629629629632
	},
	{
		id: 1078,
		time: 1077,
		velocity: 18.4205555555556,
		power: 5625.05293772199,
		road: 9190.43731481481,
		acceleration: 0.00324074074073977
	},
	{
		id: 1079,
		time: 1078,
		velocity: 18.2969444444444,
		power: 2691.8201569576,
		road: 9208.70342592592,
		acceleration: -0.161481481481484
	},
	{
		id: 1080,
		time: 1079,
		velocity: 17.8386111111111,
		power: 51.1085469703015,
		road: 9226.7349537037,
		acceleration: -0.307685185185182
	},
	{
		id: 1081,
		time: 1080,
		velocity: 17.4975,
		power: -2379.97554038841,
		road: 9244.39064814814,
		acceleration: -0.443981481481483
	},
	{
		id: 1082,
		time: 1081,
		velocity: 16.965,
		power: -1830.7946530509,
		road: 9261.62143518518,
		acceleration: -0.405833333333334
	},
	{
		id: 1083,
		time: 1082,
		velocity: 16.6211111111111,
		power: -986.088129900333,
		road: 9278.4749537037,
		acceleration: -0.348703703703706
	},
	{
		id: 1084,
		time: 1083,
		velocity: 16.4513888888889,
		power: 1655.21105971655,
		road: 9295.065,
		acceleration: -0.178240740740737
	},
	{
		id: 1085,
		time: 1084,
		velocity: 16.4302777777778,
		power: 3319.58633610519,
		road: 9311.5312037037,
		acceleration: -0.0694444444444464
	},
	{
		id: 1086,
		time: 1085,
		velocity: 16.4127777777778,
		power: 5179.65503371985,
		road: 9327.98722222222,
		acceleration: 0.049074074074074
	},
	{
		id: 1087,
		time: 1086,
		velocity: 16.5986111111111,
		power: 4650.02664859839,
		road: 9344.4749074074,
		acceleration: 0.0142592592592585
	},
	{
		id: 1088,
		time: 1087,
		velocity: 16.4730555555556,
		power: 4594.28809710035,
		road: 9360.97486111111,
		acceleration: 0.0102777777777803
	},
	{
		id: 1089,
		time: 1088,
		velocity: 16.4436111111111,
		power: 2817.01067544544,
		road: 9377.42944444444,
		acceleration: -0.101018518518522
	},
	{
		id: 1090,
		time: 1089,
		velocity: 16.2955555555556,
		power: 2208.30991507863,
		road: 9393.76527777777,
		acceleration: -0.136481481481479
	},
	{
		id: 1091,
		time: 1090,
		velocity: 16.0636111111111,
		power: 1274.64864559473,
		road: 9409.93666666666,
		acceleration: -0.192407407407405
	},
	{
		id: 1092,
		time: 1091,
		velocity: 15.8663888888889,
		power: 2455.45394281695,
		road: 9425.95597222222,
		acceleration: -0.11175925925926
	},
	{
		id: 1093,
		time: 1092,
		velocity: 15.9602777777778,
		power: 3505.1828218157,
		road: 9441.89902777777,
		acceleration: -0.0407407407407412
	},
	{
		id: 1094,
		time: 1093,
		velocity: 15.9413888888889,
		power: 4584.69105514792,
		road: 9457.83685185185,
		acceleration: 0.0302777777777763
	},
	{
		id: 1095,
		time: 1094,
		velocity: 15.9572222222222,
		power: 2898.77770121971,
		road: 9473.74999999999,
		acceleration: -0.0796296296296291
	},
	{
		id: 1096,
		time: 1095,
		velocity: 15.7213888888889,
		power: 1110.92080863895,
		road: 9489.52629629629,
		acceleration: -0.194074074074074
	},
	{
		id: 1097,
		time: 1096,
		velocity: 15.3591666666667,
		power: -1582.24565908975,
		road: 9505.02074074074,
		acceleration: -0.369629629629628
	},
	{
		id: 1098,
		time: 1097,
		velocity: 14.8483333333333,
		power: -3376.98732304591,
		road: 9520.08537037037,
		acceleration: -0.490000000000002
	},
	{
		id: 1099,
		time: 1098,
		velocity: 14.2513888888889,
		power: -5802.15243761366,
		road: 9534.5724537037,
		acceleration: -0.665092592592593
	},
	{
		id: 1100,
		time: 1099,
		velocity: 13.3638888888889,
		power: -9911.25270471594,
		road: 9548.23060185185,
		acceleration: -0.992777777777778
	},
	{
		id: 1101,
		time: 1100,
		velocity: 11.87,
		power: -12014.7151313235,
		road: 9560.78254629629,
		acceleration: -1.21962962962963
	},
	{
		id: 1102,
		time: 1101,
		velocity: 10.5925,
		power: -11684.3682681235,
		road: 9572.08263888889,
		acceleration: -1.28407407407407
	},
	{
		id: 1103,
		time: 1102,
		velocity: 9.51166666666666,
		power: -10889.3133492226,
		road: 9582.07694444444,
		acceleration: -1.3275
	},
	{
		id: 1104,
		time: 1103,
		velocity: 7.8875,
		power: -6522.68816651121,
		road: 9590.9349537037,
		acceleration: -0.945092592592594
	},
	{
		id: 1105,
		time: 1104,
		velocity: 7.75722222222222,
		power: -4125.72282813984,
		road: 9598.9687037037,
		acceleration: -0.703425925925925
	},
	{
		id: 1106,
		time: 1105,
		velocity: 7.40138888888889,
		power: 276.11608976867,
		road: 9606.58972222222,
		acceleration: -0.122037037037037
	},
	{
		id: 1107,
		time: 1106,
		velocity: 7.52138888888889,
		power: 1079.72030328037,
		road: 9614.14504629629,
		acceleration: -0.00935185185185095
	},
	{
		id: 1108,
		time: 1107,
		velocity: 7.72916666666667,
		power: 1588.54717168917,
		road: 9621.72592592592,
		acceleration: 0.0604629629629621
	},
	{
		id: 1109,
		time: 1108,
		velocity: 7.58277777777778,
		power: 1254.94696171847,
		road: 9629.34356481481,
		acceleration: 0.0130555555555558
	},
	{
		id: 1110,
		time: 1109,
		velocity: 7.56055555555556,
		power: 1256.55745218086,
		road: 9636.97416666667,
		acceleration: 0.0128703703703703
	},
	{
		id: 1111,
		time: 1110,
		velocity: 7.76777777777778,
		power: 2107.04935902752,
		road: 9644.67458333333,
		acceleration: 0.126759259259259
	},
	{
		id: 1112,
		time: 1111,
		velocity: 7.96305555555556,
		power: 4620.95263509641,
		road: 9652.66069444444,
		acceleration: 0.44462962962963
	},
	{
		id: 1113,
		time: 1112,
		velocity: 8.89444444444445,
		power: 7776.18156369322,
		road: 9661.25949074074,
		acceleration: 0.780740740740741
	},
	{
		id: 1114,
		time: 1113,
		velocity: 10.11,
		power: 11238.3888847803,
		road: 9670.77902777777,
		acceleration: 1.06074074074074
	},
	{
		id: 1115,
		time: 1114,
		velocity: 11.1452777777778,
		power: 13587.7714209369,
		road: 9681.40416666666,
		acceleration: 1.15046296296296
	},
	{
		id: 1116,
		time: 1115,
		velocity: 12.3458333333333,
		power: 10174.9167720396,
		road: 9692.96458333333,
		acceleration: 0.720092592592593
	},
	{
		id: 1117,
		time: 1116,
		velocity: 12.2702777777778,
		power: 7434.61976249064,
		road: 9705.10087962963,
		acceleration: 0.431666666666667
	},
	{
		id: 1118,
		time: 1117,
		velocity: 12.4402777777778,
		power: 5540.55087550418,
		road: 9717.57810185185,
		acceleration: 0.250185185185183
	},
	{
		id: 1119,
		time: 1118,
		velocity: 13.0963888888889,
		power: 9532.7096621347,
		road: 9730.45768518518,
		acceleration: 0.554537037037036
	},
	{
		id: 1120,
		time: 1119,
		velocity: 13.9338888888889,
		power: 12086.6135216824,
		road: 9743.96787037037,
		acceleration: 0.706666666666669
	},
	{
		id: 1121,
		time: 1120,
		velocity: 14.5602777777778,
		power: 13415.5749996837,
		road: 9758.20384259259,
		acceleration: 0.744907407407409
	},
	{
		id: 1122,
		time: 1121,
		velocity: 15.3311111111111,
		power: 13294.240412961,
		road: 9773.15097222222,
		acceleration: 0.677407407407408
	},
	{
		id: 1123,
		time: 1122,
		velocity: 15.9661111111111,
		power: 13205.3655716163,
		road: 9788.7474074074,
		acceleration: 0.621203703703703
	},
	{
		id: 1124,
		time: 1123,
		velocity: 16.4238888888889,
		power: 12391.1107164922,
		road: 9804.91773148148,
		acceleration: 0.526574074074075
	},
	{
		id: 1125,
		time: 1124,
		velocity: 16.9108333333333,
		power: 12468.2021286718,
		road: 9821.5999074074,
		acceleration: 0.497129629629629
	},
	{
		id: 1126,
		time: 1125,
		velocity: 17.4575,
		power: 13645.8927958047,
		road: 9838.79824074074,
		acceleration: 0.535185185185185
	},
	{
		id: 1127,
		time: 1126,
		velocity: 18.0294444444444,
		power: 14188.2777284683,
		road: 9856.5299074074,
		acceleration: 0.531481481481482
	},
	{
		id: 1128,
		time: 1127,
		velocity: 18.5052777777778,
		power: 14314.354858381,
		road: 9874.77944444444,
		acceleration: 0.504259259259261
	},
	{
		id: 1129,
		time: 1128,
		velocity: 18.9702777777778,
		power: 13006.1437964318,
		road: 9893.48199074073,
		acceleration: 0.401759259259258
	},
	{
		id: 1130,
		time: 1129,
		velocity: 19.2347222222222,
		power: 10829.1356950314,
		road: 9912.51666666666,
		acceleration: 0.262500000000003
	},
	{
		id: 1131,
		time: 1130,
		velocity: 19.2927777777778,
		power: 7601.4541905269,
		road: 9931.72162037036,
		acceleration: 0.0780555555555509
	},
	{
		id: 1132,
		time: 1131,
		velocity: 19.2044444444444,
		power: 4404.22122894523,
		road: 9950.91787037037,
		acceleration: -0.0954629629629622
	},
	{
		id: 1133,
		time: 1132,
		velocity: 18.9483333333333,
		power: 1231.90313924726,
		road: 9969.93462962962,
		acceleration: -0.263518518518516
	},
	{
		id: 1134,
		time: 1133,
		velocity: 18.5022222222222,
		power: -1390.55768537875,
		road: 9988.6187037037,
		acceleration: -0.401851851851852
	},
	{
		id: 1135,
		time: 1134,
		velocity: 17.9988888888889,
		power: -2000.92841462444,
		road: 10006.8868981481,
		acceleration: -0.429907407407409
	},
	{
		id: 1136,
		time: 1135,
		velocity: 17.6586111111111,
		power: -1087.243631499,
		road: 10024.7546759259,
		acceleration: -0.370925925925928
	},
	{
		id: 1137,
		time: 1136,
		velocity: 17.3894444444444,
		power: 342.256707024195,
		road: 10042.2967592593,
		acceleration: -0.280462962962964
	},
	{
		id: 1138,
		time: 1137,
		velocity: 17.1575,
		power: 374.278390030208,
		road: 10059.5622222222,
		acceleration: -0.272777777777776
	},
	{
		id: 1139,
		time: 1138,
		velocity: 16.8402777777778,
		power: 530.743778752653,
		road: 10076.5624537037,
		acceleration: -0.257685185185185
	},
	{
		id: 1140,
		time: 1139,
		velocity: 16.6163888888889,
		power: -490.227623229677,
		road: 10093.2760648148,
		acceleration: -0.315555555555555
	},
	{
		id: 1141,
		time: 1140,
		velocity: 16.2108333333333,
		power: 397.63756038421,
		road: 10109.7047685185,
		acceleration: -0.254259259259261
	},
	{
		id: 1142,
		time: 1141,
		velocity: 16.0775,
		power: 481.247277762389,
		road: 10125.8843981481,
		acceleration: -0.243888888888886
	},
	{
		id: 1143,
		time: 1142,
		velocity: 15.8847222222222,
		power: 706.054720207258,
		road: 10141.8298611111,
		acceleration: -0.224444444444444
	},
	{
		id: 1144,
		time: 1143,
		velocity: 15.5375,
		power: -1088.00368318162,
		road: 10157.4938888889,
		acceleration: -0.338425925925927
	},
	{
		id: 1145,
		time: 1144,
		velocity: 15.0622222222222,
		power: -2232.70342975159,
		road: 10172.7826851852,
		acceleration: -0.412037037037036
	},
	{
		id: 1146,
		time: 1145,
		velocity: 14.6486111111111,
		power: -1565.54369093634,
		road: 10187.6841666667,
		acceleration: -0.362592592592597
	},
	{
		id: 1147,
		time: 1146,
		velocity: 14.4497222222222,
		power: -485.446837391874,
		road: 10202.263287037,
		acceleration: -0.282129629629626
	},
	{
		id: 1148,
		time: 1147,
		velocity: 14.2158333333333,
		power: 820.26042805859,
		road: 10216.609537037,
		acceleration: -0.183611111111111
	},
	{
		id: 1149,
		time: 1148,
		velocity: 14.0977777777778,
		power: 2598.63029045697,
		road: 10230.8387962963,
		acceleration: -0.0503703703703717
	},
	{
		id: 1150,
		time: 1149,
		velocity: 14.2986111111111,
		power: 5151.52510207813,
		road: 10245.1106944444,
		acceleration: 0.135648148148148
	},
	{
		id: 1151,
		time: 1150,
		velocity: 14.6227777777778,
		power: 7458.52970482962,
		road: 10259.597037037,
		acceleration: 0.293240740740742
	},
	{
		id: 1152,
		time: 1151,
		velocity: 14.9775,
		power: 7902.05760711915,
		road: 10274.3843055555,
		acceleration: 0.308611111111111
	},
	{
		id: 1153,
		time: 1152,
		velocity: 15.2244444444444,
		power: 8558.1553015777,
		road: 10289.4941666667,
		acceleration: 0.336574074074074
	},
	{
		id: 1154,
		time: 1153,
		velocity: 15.6325,
		power: 7599.43937308948,
		road: 10304.8997685185,
		acceleration: 0.254907407407407
	},
	{
		id: 1155,
		time: 1154,
		velocity: 15.7422222222222,
		power: 6348.79399725692,
		road: 10320.5131018518,
		acceleration: 0.160555555555556
	},
	{
		id: 1156,
		time: 1155,
		velocity: 15.7061111111111,
		power: 3953.37765534584,
		road: 10336.2053703704,
		acceleration: -0.0026851851851859
	},
	{
		id: 1157,
		time: 1156,
		velocity: 15.6244444444444,
		power: 3702.06994667639,
		road: 10351.8867592593,
		acceleration: -0.0190740740740711
	},
	{
		id: 1158,
		time: 1157,
		velocity: 15.685,
		power: 4432.19264665831,
		road: 10367.5733333333,
		acceleration: 0.0294444444444437
	},
	{
		id: 1159,
		time: 1158,
		velocity: 15.7944444444444,
		power: 6098.70152129211,
		road: 10383.3431018518,
		acceleration: 0.136944444444442
	},
	{
		id: 1160,
		time: 1159,
		velocity: 16.0352777777778,
		power: 7948.09190197819,
		road: 10399.3062037037,
		acceleration: 0.249722222222223
	},
	{
		id: 1161,
		time: 1160,
		velocity: 16.4341666666667,
		power: 8046.56093559374,
		road: 10415.5159722222,
		acceleration: 0.243611111111111
	},
	{
		id: 1162,
		time: 1161,
		velocity: 16.5252777777778,
		power: 7384.4846569125,
		road: 10431.9428240741,
		acceleration: 0.190555555555559
	},
	{
		id: 1163,
		time: 1162,
		velocity: 16.6069444444444,
		power: 5912.59988696077,
		road: 10448.510462963,
		acceleration: 0.0910185185185135
	},
	{
		id: 1164,
		time: 1163,
		velocity: 16.7072222222222,
		power: 5903.24599615292,
		road: 10465.1669907407,
		acceleration: 0.0867592592592601
	},
	{
		id: 1165,
		time: 1164,
		velocity: 16.7855555555556,
		power: 6134.50372185819,
		road: 10481.9156018518,
		acceleration: 0.0974074074074096
	},
	{
		id: 1166,
		time: 1165,
		velocity: 16.8991666666667,
		power: 6480.63765698766,
		road: 10498.7701388889,
		acceleration: 0.114444444444445
	},
	{
		id: 1167,
		time: 1166,
		velocity: 17.0505555555556,
		power: 5071.94893525159,
		road: 10515.6941203704,
		acceleration: 0.0244444444444447
	},
	{
		id: 1168,
		time: 1167,
		velocity: 16.8588888888889,
		power: 4032.02975553528,
		road: 10532.6105555555,
		acceleration: -0.0395370370370394
	},
	{
		id: 1169,
		time: 1168,
		velocity: 16.7805555555556,
		power: 1649.39412955576,
		road: 10549.4152314815,
		acceleration: -0.183981481481482
	},
	{
		id: 1170,
		time: 1169,
		velocity: 16.4986111111111,
		power: 2640.64128830405,
		road: 10566.0689351852,
		acceleration: -0.117962962962963
	},
	{
		id: 1171,
		time: 1170,
		velocity: 16.505,
		power: 4508.26117238487,
		road: 10582.6643518518,
		acceleration: 0.00138888888888999
	},
	{
		id: 1172,
		time: 1171,
		velocity: 16.7847222222222,
		power: 6441.5574361638,
		road: 10599.3207407407,
		acceleration: 0.120555555555555
	},
	{
		id: 1173,
		time: 1172,
		velocity: 16.8602777777778,
		power: 8647.87162380917,
		road: 10616.1621759259,
		acceleration: 0.249537037037037
	},
	{
		id: 1174,
		time: 1173,
		velocity: 17.2536111111111,
		power: 9659.05840233863,
		road: 10633.2770833333,
		acceleration: 0.297407407407412
	},
	{
		id: 1175,
		time: 1174,
		velocity: 17.6769444444444,
		power: 9098.83569126714,
		road: 10650.6652314815,
		acceleration: 0.249074074074073
	},
	{
		id: 1176,
		time: 1175,
		velocity: 17.6075,
		power: 7765.48659858893,
		road: 10668.2576388889,
		acceleration: 0.159444444444439
	},
	{
		id: 1177,
		time: 1176,
		velocity: 17.7319444444444,
		power: 5033.9743498454,
		road: 10685.9269444444,
		acceleration: -0.00564814814814341
	},
	{
		id: 1178,
		time: 1177,
		velocity: 17.66,
		power: 4557.40962197342,
		road: 10703.5768518518,
		acceleration: -0.0331481481481504
	},
	{
		id: 1179,
		time: 1178,
		velocity: 17.5080555555556,
		power: 3059.35762612371,
		road: 10721.1504166667,
		acceleration: -0.119537037037038
	},
	{
		id: 1180,
		time: 1179,
		velocity: 17.3733333333333,
		power: 2807.58914817154,
		road: 10738.5987962963,
		acceleration: -0.130833333333332
	},
	{
		id: 1181,
		time: 1180,
		velocity: 17.2675,
		power: 1312.47462817956,
		road: 10755.8736574074,
		acceleration: -0.216203703703702
	},
	{
		id: 1182,
		time: 1181,
		velocity: 16.8594444444444,
		power: 270.856211787938,
		road: 10772.903287037,
		acceleration: -0.274259259259264
	},
	{
		id: 1183,
		time: 1182,
		velocity: 16.5505555555556,
		power: 63.0617806636183,
		road: 10789.6549537037,
		acceleration: -0.281666666666666
	},
	{
		id: 1184,
		time: 1183,
		velocity: 16.4225,
		power: 749.904807867307,
		road: 10806.1491666667,
		acceleration: -0.23324074074074
	},
	{
		id: 1185,
		time: 1184,
		velocity: 16.1597222222222,
		power: 1329.26894445749,
		road: 10822.4310185185,
		acceleration: -0.191481481481482
	},
	{
		id: 1186,
		time: 1185,
		velocity: 15.9761111111111,
		power: 925.741074466886,
		road: 10838.5106481481,
		acceleration: -0.212962962962962
	},
	{
		id: 1187,
		time: 1186,
		velocity: 15.7836111111111,
		power: 1024.24359984009,
		road: 10854.3828240741,
		acceleration: -0.201944444444445
	},
	{
		id: 1188,
		time: 1187,
		velocity: 15.5538888888889,
		power: 1169.15992094545,
		road: 10870.0600462963,
		acceleration: -0.187962962962963
	},
	{
		id: 1189,
		time: 1188,
		velocity: 15.4122222222222,
		power: 1046.5905027688,
		road: 10885.5473148148,
		acceleration: -0.191944444444445
	},
	{
		id: 1190,
		time: 1189,
		velocity: 15.2077777777778,
		power: 2259.4081330631,
		road: 10900.8856481481,
		acceleration: -0.105925925925927
	},
	{
		id: 1191,
		time: 1190,
		velocity: 15.2361111111111,
		power: 3071.23983041627,
		road: 10916.1469444444,
		acceleration: -0.0481481481481456
	},
	{
		id: 1192,
		time: 1191,
		velocity: 15.2677777777778,
		power: 4866.72287908383,
		road: 10931.4213888889,
		acceleration: 0.0744444444444454
	},
	{
		id: 1193,
		time: 1192,
		velocity: 15.4311111111111,
		power: 5657.19026705939,
		road: 10946.7952314815,
		acceleration: 0.12435185185185
	},
	{
		id: 1194,
		time: 1193,
		velocity: 15.6091666666667,
		power: 5613.53454326324,
		road: 10962.2893981481,
		acceleration: 0.116296296296296
	},
	{
		id: 1195,
		time: 1194,
		velocity: 15.6166666666667,
		power: 5225.55900073381,
		road: 10977.8847222222,
		acceleration: 0.0860185185185198
	},
	{
		id: 1196,
		time: 1195,
		velocity: 15.6891666666667,
		power: 5038.1760890336,
		road: 10993.5582407407,
		acceleration: 0.0703703703703713
	},
	{
		id: 1197,
		time: 1196,
		velocity: 15.8202777777778,
		power: 5929.54429048827,
		road: 11009.3297685185,
		acceleration: 0.125648148148146
	},
	{
		id: 1198,
		time: 1197,
		velocity: 15.9936111111111,
		power: 5656.45383512521,
		road: 11025.2155092593,
		acceleration: 0.102777777777778
	},
	{
		id: 1199,
		time: 1198,
		velocity: 15.9975,
		power: 6329.15611031471,
		road: 11041.2234722222,
		acceleration: 0.141666666666664
	},
	{
		id: 1200,
		time: 1199,
		velocity: 16.2452777777778,
		power: 6210.55070196036,
		road: 11057.3662962963,
		acceleration: 0.128055555555559
	},
	{
		id: 1201,
		time: 1200,
		velocity: 16.3777777777778,
		power: 6970.16611729416,
		road: 11073.6583333333,
		acceleration: 0.170370370370367
	},
	{
		id: 1202,
		time: 1201,
		velocity: 16.5086111111111,
		power: 6491.34654860687,
		road: 11090.102037037,
		acceleration: 0.132962962962964
	},
	{
		id: 1203,
		time: 1202,
		velocity: 16.6441666666667,
		power: 5539.90178443045,
		road: 11106.6464351852,
		acceleration: 0.0684259259259292
	},
	{
		id: 1204,
		time: 1203,
		velocity: 16.5830555555556,
		power: 5837.04443079581,
		road: 11123.2670833333,
		acceleration: 0.0840740740740706
	},
	{
		id: 1205,
		time: 1204,
		velocity: 16.7608333333333,
		power: 5049.39358796359,
		road: 11139.9459259259,
		acceleration: 0.0323148148148178
	},
	{
		id: 1206,
		time: 1205,
		velocity: 16.7411111111111,
		power: 4791.63889337349,
		road: 11156.6485648148,
		acceleration: 0.0152777777777757
	},
	{
		id: 1207,
		time: 1206,
		velocity: 16.6288888888889,
		power: 3703.64800565893,
		road: 11173.3327314815,
		acceleration: -0.0522222222222233
	},
	{
		id: 1208,
		time: 1207,
		velocity: 16.6041666666667,
		power: 3257.55791476166,
		road: 11189.951712963,
		acceleration: -0.078148148148145
	},
	{
		id: 1209,
		time: 1208,
		velocity: 16.5066666666667,
		power: 3777.39312968156,
		road: 11206.5099074074,
		acceleration: -0.0434259259259271
	},
	{
		id: 1210,
		time: 1209,
		velocity: 16.4986111111111,
		power: 3842.34947917603,
		road: 11223.0274074074,
		acceleration: -0.0379629629629612
	},
	{
		id: 1211,
		time: 1210,
		velocity: 16.4902777777778,
		power: 5192.27865144734,
		road: 11239.5495833333,
		acceleration: 0.0473148148148113
	},
	{
		id: 1212,
		time: 1211,
		velocity: 16.6486111111111,
		power: 6205.74165140355,
		road: 11256.1494907407,
		acceleration: 0.10814814814815
	},
	{
		id: 1213,
		time: 1212,
		velocity: 16.8230555555556,
		power: 7928.09856901199,
		road: 11272.9079166667,
		acceleration: 0.20888888888889
	},
	{
		id: 1214,
		time: 1213,
		velocity: 17.1169444444444,
		power: 7722.96154158784,
		road: 11289.8641203704,
		acceleration: 0.186666666666667
	},
	{
		id: 1215,
		time: 1214,
		velocity: 17.2086111111111,
		power: 8672.45717705863,
		road: 11307.0309259259,
		acceleration: 0.234537037037036
	},
	{
		id: 1216,
		time: 1215,
		velocity: 17.5266666666667,
		power: 8626.30682435981,
		road: 11324.4251851852,
		acceleration: 0.220370370370372
	},
	{
		id: 1217,
		time: 1216,
		velocity: 17.7780555555556,
		power: 10487.7270920649,
		road: 11342.0882407407,
		acceleration: 0.31722222222222
	},
	{
		id: 1218,
		time: 1217,
		velocity: 18.1602777777778,
		power: 11653.3583553825,
		road: 11360.092962963,
		acceleration: 0.366111111111113
	},
	{
		id: 1219,
		time: 1218,
		velocity: 18.625,
		power: 11732.9678673672,
		road: 11378.4557407407,
		acceleration: 0.349999999999998
	},
	{
		id: 1220,
		time: 1219,
		velocity: 18.8280555555556,
		power: 10023.7176238712,
		road: 11397.1123148148,
		acceleration: 0.237592592592595
	},
	{
		id: 1221,
		time: 1220,
		velocity: 18.8730555555556,
		power: 6466.05562212241,
		road: 11415.9041666667,
		acceleration: 0.0329629629629622
	},
	{
		id: 1222,
		time: 1221,
		velocity: 18.7238888888889,
		power: 1894.52226761162,
		road: 11434.6031944444,
		acceleration: -0.218611111111112
	},
	{
		id: 1223,
		time: 1222,
		velocity: 18.1722222222222,
		power: -962.015840942853,
		road: 11453.0064814815,
		acceleration: -0.372870370370368
	},
	{
		id: 1224,
		time: 1223,
		velocity: 17.7544444444444,
		power: -2093.93900802302,
		road: 11471.0075462963,
		acceleration: -0.431574074074078
	},
	{
		id: 1225,
		time: 1224,
		velocity: 17.4291666666667,
		power: -2236.92490011505,
		road: 11488.5756018518,
		acceleration: -0.434444444444445
	},
	{
		id: 1226,
		time: 1225,
		velocity: 16.8688888888889,
		power: -958.758477135833,
		road: 11505.7504166667,
		acceleration: -0.352037037037036
	},
	{
		id: 1227,
		time: 1226,
		velocity: 16.6983333333333,
		power: -2470.8705608009,
		road: 11522.5291666667,
		acceleration: -0.440092592592592
	},
	{
		id: 1228,
		time: 1227,
		velocity: 16.1088888888889,
		power: -1545.42058075392,
		road: 11538.8992592592,
		acceleration: -0.377222222222223
	},
	{
		id: 1229,
		time: 1228,
		velocity: 15.7372222222222,
		power: -1793.87348117075,
		road: 11554.8862962963,
		acceleration: -0.388888888888888
	},
	{
		id: 1230,
		time: 1229,
		velocity: 15.5316666666667,
		power: 693.560106716584,
		road: 11570.5689814815,
		acceleration: -0.219814814814814
	},
	{
		id: 1231,
		time: 1230,
		velocity: 15.4494444444444,
		power: 2878.26441210065,
		road: 11586.1068981481,
		acceleration: -0.0697222222222234
	},
	{
		id: 1232,
		time: 1231,
		velocity: 15.5280555555556,
		power: 3432.91099859817,
		road: 11601.5945833333,
		acceleration: -0.0307407407407396
	},
	{
		id: 1233,
		time: 1232,
		velocity: 15.4394444444444,
		power: 4267.6517301277,
		road: 11617.0797685185,
		acceleration: 0.0257407407407388
	},
	{
		id: 1234,
		time: 1233,
		velocity: 15.5266666666667,
		power: 5132.76122795064,
		road: 11632.6188425926,
		acceleration: 0.082037037037038
	},
	{
		id: 1235,
		time: 1234,
		velocity: 15.7741666666667,
		power: 6400.3345527016,
		road: 11648.2798611111,
		acceleration: 0.16185185185185
	},
	{
		id: 1236,
		time: 1235,
		velocity: 15.925,
		power: 7160.18006684971,
		road: 11664.1237037037,
		acceleration: 0.203796296296298
	},
	{
		id: 1237,
		time: 1236,
		velocity: 16.1380555555556,
		power: 6767.19792307157,
		road: 11680.1540740741,
		acceleration: 0.169259259259258
	},
	{
		id: 1238,
		time: 1237,
		velocity: 16.2819444444444,
		power: 2498.6038666934,
		road: 11696.2140277778,
		acceleration: -0.110092592592594
	},
	{
		id: 1239,
		time: 1238,
		velocity: 15.5947222222222,
		power: 190.959065001445,
		road: 11712.090462963,
		acceleration: -0.256944444444441
	},
	{
		id: 1240,
		time: 1239,
		velocity: 15.3672222222222,
		power: -3093.51833275843,
		road: 11727.6025462963,
		acceleration: -0.471759259259258
	},
	{
		id: 1241,
		time: 1240,
		velocity: 14.8666666666667,
		power: -123.783810987239,
		road: 11742.7461111111,
		acceleration: -0.265277777777779
	},
	{
		id: 1242,
		time: 1241,
		velocity: 14.7988888888889,
		power: 765.286578203619,
		road: 11757.6575,
		acceleration: -0.199074074074076
	},
	{
		id: 1243,
		time: 1242,
		velocity: 14.77,
		power: 3704.26167461998,
		road: 11772.4745833333,
		acceleration: 0.0104629629629649
	},
	{
		id: 1244,
		time: 1243,
		velocity: 14.8980555555556,
		power: 4411.06829735554,
		road: 11787.3264351852,
		acceleration: 0.0590740740740721
	},
	{
		id: 1245,
		time: 1244,
		velocity: 14.9761111111111,
		power: 4734.75589584363,
		road: 11802.2474074074,
		acceleration: 0.0791666666666693
	},
	{
		id: 1246,
		time: 1245,
		velocity: 15.0075,
		power: 4754.86054496333,
		road: 11817.246712963,
		acceleration: 0.077499999999997
	},
	{
		id: 1247,
		time: 1246,
		velocity: 15.1305555555556,
		power: 5196.08400946839,
		road: 11832.337037037,
		acceleration: 0.104537037037039
	},
	{
		id: 1248,
		time: 1247,
		velocity: 15.2897222222222,
		power: 5298.08200073091,
		road: 11847.5332407407,
		acceleration: 0.107222222222221
	},
	{
		id: 1249,
		time: 1248,
		velocity: 15.3291666666667,
		power: 5607.5193891085,
		road: 11862.8448611111,
		acceleration: 0.123611111111114
	},
	{
		id: 1250,
		time: 1249,
		velocity: 15.5013888888889,
		power: 5839.66810616844,
		road: 11878.2852314815,
		acceleration: 0.133888888888887
	},
	{
		id: 1251,
		time: 1250,
		velocity: 15.6913888888889,
		power: 6052.34429673798,
		road: 11893.8636574074,
		acceleration: 0.142222222222221
	},
	{
		id: 1252,
		time: 1251,
		velocity: 15.7558333333333,
		power: 6054.76643329089,
		road: 11909.5813425926,
		acceleration: 0.136296296296296
	},
	{
		id: 1253,
		time: 1252,
		velocity: 15.9102777777778,
		power: 4967.11789159945,
		road: 11925.3972222222,
		acceleration: 0.0600925925925946
	},
	{
		id: 1254,
		time: 1253,
		velocity: 15.8716666666667,
		power: 5846.26138573234,
		road: 11941.3004166667,
		acceleration: 0.114537037037037
	},
	{
		id: 1255,
		time: 1254,
		velocity: 16.0994444444444,
		power: 6178.16137967484,
		road: 11957.3263888889,
		acceleration: 0.131018518518518
	},
	{
		id: 1256,
		time: 1255,
		velocity: 16.3033333333333,
		power: 6731.01455271442,
		road: 11973.4981018518,
		acceleration: 0.160462962962963
	},
	{
		id: 1257,
		time: 1256,
		velocity: 16.3530555555556,
		power: 6219.41313470282,
		road: 11989.8106944444,
		acceleration: 0.1212962962963
	},
	{
		id: 1258,
		time: 1257,
		velocity: 16.4633333333333,
		power: 5628.67458461239,
		road: 12006.2236111111,
		acceleration: 0.0793518518518503
	},
	{
		id: 1259,
		time: 1258,
		velocity: 16.5413888888889,
		power: 5533.33780853951,
		road: 12022.7113425926,
		acceleration: 0.0702777777777754
	},
	{
		id: 1260,
		time: 1259,
		velocity: 16.5638888888889,
		power: 4254.49706687967,
		road: 12039.228287037,
		acceleration: -0.0118518518518513
	},
	{
		id: 1261,
		time: 1260,
		velocity: 16.4277777777778,
		power: 3066.87173367882,
		road: 12055.6965277778,
		acceleration: -0.0855555555555547
	},
	{
		id: 1262,
		time: 1261,
		velocity: 16.2847222222222,
		power: 1118.8251197114,
		road: 12072.0190277778,
		acceleration: -0.205925925925929
	},
	{
		id: 1263,
		time: 1262,
		velocity: 15.9461111111111,
		power: 1406.12587272276,
		road: 12088.1471296296,
		acceleration: -0.18287037037037
	},
	{
		id: 1264,
		time: 1263,
		velocity: 15.8791666666667,
		power: 20.9746585154149,
		road: 12104.0494907407,
		acceleration: -0.268611111111111
	},
	{
		id: 1265,
		time: 1264,
		velocity: 15.4788888888889,
		power: 586.603374591583,
		road: 12119.7043518518,
		acceleration: -0.226388888888888
	},
	{
		id: 1266,
		time: 1265,
		velocity: 15.2669444444444,
		power: 87.1170009605195,
		road: 12135.118287037,
		acceleration: -0.255462962962962
	},
	{
		id: 1267,
		time: 1266,
		velocity: 15.1127777777778,
		power: 1177.45900135135,
		road: 12150.3162037037,
		acceleration: -0.176574074074075
	},
	{
		id: 1268,
		time: 1267,
		velocity: 14.9491666666667,
		power: 429.646650467556,
		road: 12165.3137037037,
		acceleration: -0.224259259259259
	},
	{
		id: 1269,
		time: 1268,
		velocity: 14.5941666666667,
		power: 229.014279116303,
		road: 12180.0819907407,
		acceleration: -0.234166666666663
	},
	{
		id: 1270,
		time: 1269,
		velocity: 14.4102777777778,
		power: 840.504648729715,
		road: 12194.6399537037,
		acceleration: -0.186481481481485
	},
	{
		id: 1271,
		time: 1270,
		velocity: 14.3897222222222,
		power: 1647.38577518948,
		road: 12209.0423611111,
		acceleration: -0.124629629629629
	},
	{
		id: 1272,
		time: 1271,
		velocity: 14.2202777777778,
		power: 1470.11969811392,
		road: 12223.3152314815,
		acceleration: -0.134444444444444
	},
	{
		id: 1273,
		time: 1272,
		velocity: 14.0069444444444,
		power: 1082.09129186571,
		road: 12237.4410185185,
		acceleration: -0.159722222222225
	},
	{
		id: 1274,
		time: 1273,
		velocity: 13.9105555555556,
		power: 822.741153196124,
		road: 12251.3991666667,
		acceleration: -0.175555555555555
	},
	{
		id: 1275,
		time: 1274,
		velocity: 13.6936111111111,
		power: 1291.30063678278,
		road: 12265.2011111111,
		acceleration: -0.136851851851851
	},
	{
		id: 1276,
		time: 1275,
		velocity: 13.5963888888889,
		power: 250.632904137247,
		road: 12278.8281944444,
		acceleration: -0.21287037037037
	},
	{
		id: 1277,
		time: 1276,
		velocity: 13.2719444444444,
		power: -1316.86362188703,
		road: 12292.1831944444,
		acceleration: -0.331296296296294
	},
	{
		id: 1278,
		time: 1277,
		velocity: 12.6997222222222,
		power: -9019.96830664828,
		road: 12304.8913425926,
		acceleration: -0.96240740740741
	},
	{
		id: 1279,
		time: 1278,
		velocity: 10.7091666666667,
		power: -14929.5973655878,
		road: 12316.3333333333,
		acceleration: -1.56990740740741
	},
	{
		id: 1280,
		time: 1279,
		velocity: 8.56222222222222,
		power: -15854.1992936115,
		road: 12326.0428240741,
		acceleration: -1.89509259259259
	},
	{
		id: 1281,
		time: 1280,
		velocity: 7.01444444444444,
		power: -9849.68743470516,
		road: 12334.0787962963,
		acceleration: -1.45194444444445
	},
	{
		id: 1282,
		time: 1281,
		velocity: 6.35333333333333,
		power: -3040.10168658669,
		road: 12341.0831018518,
		acceleration: -0.611388888888889
	},
	{
		id: 1283,
		time: 1282,
		velocity: 6.72805555555556,
		power: 1833.58134986866,
		road: 12347.8476388889,
		acceleration: 0.131851851851852
	},
	{
		id: 1284,
		time: 1283,
		velocity: 7.41,
		power: 5665.59127379299,
		road: 12355.0153703704,
		acceleration: 0.674537037037037
	},
	{
		id: 1285,
		time: 1284,
		velocity: 8.37694444444444,
		power: 6970.2968434334,
		road: 12362.9033796296,
		acceleration: 0.766018518518519
	},
	{
		id: 1286,
		time: 1285,
		velocity: 9.02611111111111,
		power: 8305.77044002323,
		road: 12371.5914351852,
		acceleration: 0.834074074074074
	},
	{
		id: 1287,
		time: 1286,
		velocity: 9.91222222222222,
		power: 8587.65314054391,
		road: 12381.0823611111,
		acceleration: 0.771666666666667
	},
	{
		id: 1288,
		time: 1287,
		velocity: 10.6919444444444,
		power: 9540.15471807959,
		road: 12391.3530092592,
		acceleration: 0.787777777777777
	},
	{
		id: 1289,
		time: 1288,
		velocity: 11.3894444444444,
		power: 7340.36008691605,
		road: 12402.2727777778,
		acceleration: 0.510462962962963
	},
	{
		id: 1290,
		time: 1289,
		velocity: 11.4436111111111,
		power: 4407.8230038122,
		road: 12413.5531018518,
		acceleration: 0.210648148148149
	},
	{
		id: 1291,
		time: 1290,
		velocity: 11.3238888888889,
		power: 2248.30695204934,
		road: 12424.9419907407,
		acceleration: 0.00648148148148131
	},
	{
		id: 1292,
		time: 1291,
		velocity: 11.4088888888889,
		power: 3105.57615201924,
		road: 12436.3759722222,
		acceleration: 0.0837037037037032
	},
	{
		id: 1293,
		time: 1292,
		velocity: 11.6947222222222,
		power: 5959.32350157952,
		road: 12448.0182407407,
		acceleration: 0.332870370370372
	},
	{
		id: 1294,
		time: 1293,
		velocity: 12.3225,
		power: 8321.99673490031,
		road: 12460.0837037037,
		acceleration: 0.513518518518518
	},
	{
		id: 1295,
		time: 1294,
		velocity: 12.9494444444444,
		power: 8091.97465123839,
		road: 12472.6356018518,
		acceleration: 0.459351851851849
	},
	{
		id: 1296,
		time: 1295,
		velocity: 13.0727777777778,
		power: 5964.050809079,
		road: 12485.5484259259,
		acceleration: 0.262500000000003
	},
	{
		id: 1297,
		time: 1296,
		velocity: 13.11,
		power: 3655.67554304918,
		road: 12498.6269907407,
		acceleration: 0.0689814814814795
	},
	{
		id: 1298,
		time: 1297,
		velocity: 13.1563888888889,
		power: 3219.81305185481,
		road: 12511.7562037037,
		acceleration: 0.032314814814816
	},
	{
		id: 1299,
		time: 1298,
		velocity: 13.1697222222222,
		power: 2685.65073114349,
		road: 12524.89625,
		acceleration: -0.0106481481481477
	},
	{
		id: 1300,
		time: 1299,
		velocity: 13.0780555555556,
		power: 2106.20521465242,
		road: 12538.0030092592,
		acceleration: -0.0559259259259264
	},
	{
		id: 1301,
		time: 1300,
		velocity: 12.9886111111111,
		power: 1750.46460724248,
		road: 12551.0405092592,
		acceleration: -0.0825925925925919
	},
	{
		id: 1302,
		time: 1301,
		velocity: 12.9219444444444,
		power: 4703.20488174049,
		road: 12564.113287037,
		acceleration: 0.153148148148148
	},
	{
		id: 1303,
		time: 1302,
		velocity: 13.5375,
		power: 6378.80596807487,
		road: 12577.4006018518,
		acceleration: 0.275925925925925
	},
	{
		id: 1304,
		time: 1303,
		velocity: 13.8163888888889,
		power: 7710.98112608141,
		road: 12591.0067592592,
		acceleration: 0.361759259259259
	},
	{
		id: 1305,
		time: 1304,
		velocity: 14.0072222222222,
		power: 5096.35138961093,
		road: 12604.8684259259,
		acceleration: 0.14925925925926
	},
	{
		id: 1306,
		time: 1305,
		velocity: 13.9852777777778,
		power: 3240.84265323006,
		road: 12618.8079629629,
		acceleration: 0.00648148148148131
	},
	{
		id: 1307,
		time: 1306,
		velocity: 13.8358333333333,
		power: 1703.08295215752,
		road: 12632.6968518518,
		acceleration: -0.107777777777779
	},
	{
		id: 1308,
		time: 1307,
		velocity: 13.6838888888889,
		power: 415.066333065813,
		road: 12646.430787037,
		acceleration: -0.20212962962963
	},
	{
		id: 1309,
		time: 1308,
		velocity: 13.3788888888889,
		power: 1107.52687493942,
		road: 12659.9908796296,
		acceleration: -0.145555555555555
	},
	{
		id: 1310,
		time: 1309,
		velocity: 13.3991666666667,
		power: 2537.88260567896,
		road: 12673.4619907407,
		acceleration: -0.0324074074074083
	},
	{
		id: 1311,
		time: 1310,
		velocity: 13.5866666666667,
		power: 5270.39052625148,
		road: 12687.0052777778,
		acceleration: 0.17675925925926
	},
	{
		id: 1312,
		time: 1311,
		velocity: 13.9091666666667,
		power: 6590.21358269037,
		road: 12700.7705092592,
		acceleration: 0.267129629629631
	},
	{
		id: 1313,
		time: 1312,
		velocity: 14.2005555555556,
		power: 7056.80578571577,
		road: 12714.8131018518,
		acceleration: 0.28759259259259
	},
	{
		id: 1314,
		time: 1313,
		velocity: 14.4494444444444,
		power: 3694.15243189129,
		road: 12729.0150925926,
		acceleration: 0.031203703703703
	},
	{
		id: 1315,
		time: 1314,
		velocity: 14.0027777777778,
		power: 1346.08009315432,
		road: 12743.1623611111,
		acceleration: -0.140648148148147
	},
	{
		id: 1316,
		time: 1315,
		velocity: 13.7786111111111,
		power: -1201.7247208167,
		road: 12757.075787037,
		acceleration: -0.327037037037037
	},
	{
		id: 1317,
		time: 1316,
		velocity: 13.4683333333333,
		power: -1747.16608234531,
		road: 12770.6426388889,
		acceleration: -0.36611111111111
	},
	{
		id: 1318,
		time: 1317,
		velocity: 12.9044444444444,
		power: -1026.43936949925,
		road: 12783.8727314815,
		acceleration: -0.307407407407409
	},
	{
		id: 1319,
		time: 1318,
		velocity: 12.8563888888889,
		power: -400.086491047994,
		road: 12796.8219444444,
		acceleration: -0.254351851851851
	},
	{
		id: 1320,
		time: 1319,
		velocity: 12.7052777777778,
		power: 2687.74897880347,
		road: 12809.64375,
		acceleration: -0.000462962962963331
	},
	{
		id: 1321,
		time: 1320,
		velocity: 12.9030555555556,
		power: 3554.26013522401,
		road: 12822.4998611111,
		acceleration: 0.0690740740740772
	},
	{
		id: 1322,
		time: 1321,
		velocity: 13.0636111111111,
		power: 4721.90294472505,
		road: 12835.4700925926,
		acceleration: 0.159166666666664
	},
	{
		id: 1323,
		time: 1322,
		velocity: 13.1827777777778,
		power: 4222.36415569564,
		road: 12848.5765277778,
		acceleration: 0.113240740740739
	},
	{
		id: 1324,
		time: 1323,
		velocity: 13.2427777777778,
		power: 2802.46068258996,
		road: 12861.7385648148,
		acceleration: -0.00203703703703617
	},
	{
		id: 1325,
		time: 1324,
		velocity: 13.0575,
		power: 2177.72108971163,
		road: 12874.8740740741,
		acceleration: -0.0510185185185179
	},
	{
		id: 1326,
		time: 1325,
		velocity: 13.0297222222222,
		power: 2351.02131311656,
		road: 12887.9661111111,
		acceleration: -0.035925925925925
	},
	{
		id: 1327,
		time: 1326,
		velocity: 13.135,
		power: 2066.54899626368,
		road: 12901.0114814815,
		acceleration: -0.0574074074074087
	},
	{
		id: 1328,
		time: 1327,
		velocity: 12.8852777777778,
		power: 1850.42659901496,
		road: 12913.9916203703,
		acceleration: -0.0730555555555554
	},
	{
		id: 1329,
		time: 1328,
		velocity: 12.8105555555556,
		power: 792.572317491922,
		road: 12926.8571296296,
		acceleration: -0.156203703703705
	},
	{
		id: 1330,
		time: 1329,
		velocity: 12.6663888888889,
		power: 1200.62429285441,
		road: 12939.5845833333,
		acceleration: -0.119907407407407
	},
	{
		id: 1331,
		time: 1330,
		velocity: 12.5255555555556,
		power: 372.300706757287,
		road: 12952.1593055555,
		acceleration: -0.185555555555554
	},
	{
		id: 1332,
		time: 1331,
		velocity: 12.2538888888889,
		power: -1195.14865704109,
		road: 12964.4838888889,
		acceleration: -0.314722222222223
	},
	{
		id: 1333,
		time: 1332,
		velocity: 11.7222222222222,
		power: -2451.01074497395,
		road: 12976.4396296296,
		acceleration: -0.422962962962963
	},
	{
		id: 1334,
		time: 1333,
		velocity: 11.2566666666667,
		power: -3313.36994998896,
		road: 12987.9317592592,
		acceleration: -0.504259259259259
	},
	{
		id: 1335,
		time: 1334,
		velocity: 10.7411111111111,
		power: -3693.00823214142,
		road: 12998.8975,
		acceleration: -0.548518518518518
	},
	{
		id: 1336,
		time: 1335,
		velocity: 10.0766666666667,
		power: -4685.91423833308,
		road: 13009.2576851852,
		acceleration: -0.662592592592594
	},
	{
		id: 1337,
		time: 1336,
		velocity: 9.26888888888889,
		power: -4807.83618256834,
		road: 13018.9356944444,
		acceleration: -0.701759259259259
	},
	{
		id: 1338,
		time: 1337,
		velocity: 8.63583333333333,
		power: -4640.14602214291,
		road: 13027.9047685185,
		acceleration: -0.716111111111111
	},
	{
		id: 1339,
		time: 1338,
		velocity: 7.92833333333333,
		power: -6564.05000184561,
		road: 13036.0079629629,
		acceleration: -1.01564814814815
	},
	{
		id: 1340,
		time: 1339,
		velocity: 6.22194444444444,
		power: -6134.92427360317,
		road: 13043.0687962963,
		acceleration: -1.06907407407407
	},
	{
		id: 1341,
		time: 1340,
		velocity: 5.42861111111111,
		power: -5003.01046921483,
		road: 13049.0836574074,
		acceleration: -1.02287037037037
	},
	{
		id: 1342,
		time: 1341,
		velocity: 4.85972222222222,
		power: -2992.84007399654,
		road: 13054.2086574074,
		acceleration: -0.756851851851852
	},
	{
		id: 1343,
		time: 1342,
		velocity: 3.95138888888889,
		power: -2731.64162406768,
		road: 13058.5553240741,
		acceleration: -0.799814814814815
	},
	{
		id: 1344,
		time: 1343,
		velocity: 3.02916666666667,
		power: -2449.88372988662,
		road: 13062.0674537037,
		acceleration: -0.869259259259259
	},
	{
		id: 1345,
		time: 1344,
		velocity: 2.25194444444444,
		power: -1830.48464930978,
		road: 13064.7148611111,
		acceleration: -0.860185185185185
	},
	{
		id: 1346,
		time: 1345,
		velocity: 1.37083333333333,
		power: -862.26563776524,
		road: 13066.6300462963,
		acceleration: -0.60425925925926
	},
	{
		id: 1347,
		time: 1346,
		velocity: 1.21638888888889,
		power: -360.364939338168,
		road: 13068.0444444444,
		acceleration: -0.397314814814815
	},
	{
		id: 1348,
		time: 1347,
		velocity: 1.06,
		power: 5.43758651453169,
		road: 13069.1985648148,
		acceleration: -0.123240740740741
	},
	{
		id: 1349,
		time: 1348,
		velocity: 1.00111111111111,
		power: 8.35086591751525,
		road: 13070.2312962963,
		acceleration: -0.119537037037037
	},
	{
		id: 1350,
		time: 1349,
		velocity: 0.857777777777778,
		power: 215.465490540163,
		road: 13071.2516203703,
		acceleration: 0.0947222222222223
	},
	{
		id: 1351,
		time: 1350,
		velocity: 1.34416666666667,
		power: 422.125058280516,
		road: 13072.4421759259,
		acceleration: 0.245740740740741
	},
	{
		id: 1352,
		time: 1351,
		velocity: 1.73833333333333,
		power: 570.875196676429,
		road: 13073.8980555555,
		acceleration: 0.284907407407407
	},
	{
		id: 1353,
		time: 1352,
		velocity: 1.7125,
		power: 306.445023842644,
		road: 13075.5308796296,
		acceleration: 0.0689814814814813
	},
	{
		id: 1354,
		time: 1353,
		velocity: 1.55111111111111,
		power: 54.5395428911402,
		road: 13077.1514814815,
		acceleration: -0.093425925925926
	},
	{
		id: 1355,
		time: 1354,
		velocity: 1.45805555555556,
		power: 10.5367046432048,
		road: 13078.6646759259,
		acceleration: -0.121388888888889
	},
	{
		id: 1356,
		time: 1355,
		velocity: 1.34833333333333,
		power: 9.31355616994698,
		road: 13080.0564351852,
		acceleration: -0.121481481481482
	},
	{
		id: 1357,
		time: 1356,
		velocity: 1.18666666666667,
		power: 51.966947311556,
		road: 13081.344537037,
		acceleration: -0.085833333333333
	},
	{
		id: 1358,
		time: 1357,
		velocity: 1.20055555555556,
		power: 21.0198811445503,
		road: 13082.5349074074,
		acceleration: -0.10962962962963
	},
	{
		id: 1359,
		time: 1358,
		velocity: 1.01944444444444,
		power: 272.597013600962,
		road: 13083.7269444444,
		acceleration: 0.112962962962963
	},
	{
		id: 1360,
		time: 1359,
		velocity: 1.52555555555556,
		power: 535.324103625995,
		road: 13085.1146296296,
		acceleration: 0.278333333333333
	},
	{
		id: 1361,
		time: 1360,
		velocity: 2.03555555555556,
		power: 803.841927644854,
		road: 13086.8247685185,
		acceleration: 0.366574074074074
	},
	{
		id: 1362,
		time: 1361,
		velocity: 2.11916666666667,
		power: 583.616796206306,
		road: 13088.8084722222,
		acceleration: 0.180555555555556
	},
	{
		id: 1363,
		time: 1362,
		velocity: 2.06722222222222,
		power: 327.661041704264,
		road: 13090.9000925926,
		acceleration: 0.0352777777777775
	},
	{
		id: 1364,
		time: 1363,
		velocity: 2.14138888888889,
		power: 248.856145878297,
		road: 13093.0066666666,
		acceleration: -0.00537037037037047
	},
	{
		id: 1365,
		time: 1364,
		velocity: 2.10305555555556,
		power: 400.432904585612,
		road: 13095.1443055555,
		acceleration: 0.0674999999999999
	},
	{
		id: 1366,
		time: 1365,
		velocity: 2.26972222222222,
		power: 330.252954536835,
		road: 13097.3302777778,
		acceleration: 0.0291666666666668
	},
	{
		id: 1367,
		time: 1366,
		velocity: 2.22888888888889,
		power: 250.776875068167,
		road: 13099.5259722222,
		acceleration: -0.00972222222222197
	},
	{
		id: 1368,
		time: 1367,
		velocity: 2.07388888888889,
		power: 284.70455523708,
		road: 13101.7201388889,
		acceleration: 0.00666666666666638
	},
	{
		id: 1369,
		time: 1368,
		velocity: 2.28972222222222,
		power: 228.275257661949,
		road: 13103.9075925926,
		acceleration: -0.0200925925925923
	},
	{
		id: 1370,
		time: 1369,
		velocity: 2.16861111111111,
		power: 347.000390836901,
		road: 13106.1032407407,
		acceleration: 0.0364814814814816
	},
	{
		id: 1371,
		time: 1370,
		velocity: 2.18333333333333,
		power: 191.688959661907,
		road: 13108.2981018518,
		acceleration: -0.0380555555555557
	},
	{
		id: 1372,
		time: 1371,
		velocity: 2.17555555555556,
		power: 177.338971110202,
		road: 13110.4523148148,
		acceleration: -0.0432407407407407
	},
	{
		id: 1373,
		time: 1372,
		velocity: 2.03888888888889,
		power: 237.100467624095,
		road: 13112.5787037037,
		acceleration: -0.0124074074074074
	},
	{
		id: 1374,
		time: 1373,
		velocity: 2.14611111111111,
		power: 214.881281463399,
		road: 13114.6876388889,
		acceleration: -0.0225
	},
	{
		id: 1375,
		time: 1374,
		velocity: 2.10805555555556,
		power: 238.149081090667,
		road: 13116.7803703703,
		acceleration: -0.00990740740740748
	},
	{
		id: 1376,
		time: 1375,
		velocity: 2.00916666666667,
		power: 68.9040465438028,
		road: 13118.8210648148,
		acceleration: -0.0941666666666667
	},
	{
		id: 1377,
		time: 1376,
		velocity: 1.86361111111111,
		power: 75.3659204514787,
		road: 13120.7702777778,
		acceleration: -0.0887962962962963
	},
	{
		id: 1378,
		time: 1377,
		velocity: 1.84166666666667,
		power: 311.918726080078,
		road: 13122.6957407407,
		acceleration: 0.0412962962962962
	},
	{
		id: 1379,
		time: 1378,
		velocity: 2.13305555555556,
		power: 349.207665355038,
		road: 13124.6702777778,
		acceleration: 0.0568518518518519
	},
	{
		id: 1380,
		time: 1379,
		velocity: 2.03416666666667,
		power: 443.453694574907,
		road: 13126.7222685185,
		acceleration: 0.0980555555555558
	},
	{
		id: 1381,
		time: 1380,
		velocity: 2.13583333333333,
		power: 280.256841704842,
		road: 13128.8284722222,
		acceleration: 0.0103703703703704
	},
	{
		id: 1382,
		time: 1381,
		velocity: 2.16416666666667,
		power: 361.793778608038,
		road: 13130.9641666666,
		acceleration: 0.0486111111111107
	},
	{
		id: 1383,
		time: 1382,
		velocity: 2.18,
		power: 275.446229221306,
		road: 13133.1262962963,
		acceleration: 0.00425925925925963
	},
	{
		id: 1384,
		time: 1383,
		velocity: 2.14861111111111,
		power: 280.726610945019,
		road: 13135.2937962963,
		acceleration: 0.00648148148148131
	},
	{
		id: 1385,
		time: 1384,
		velocity: 2.18361111111111,
		power: 265.851623847617,
		road: 13137.4640740741,
		acceleration: -0.000925925925925775
	},
	{
		id: 1386,
		time: 1385,
		velocity: 2.17722222222222,
		power: 246.913028152581,
		road: 13139.6289814815,
		acceleration: -0.00981481481481472
	},
	{
		id: 1387,
		time: 1386,
		velocity: 2.11916666666667,
		power: 173.655923033763,
		road: 13141.7668055555,
		acceleration: -0.044351851851852
	},
	{
		id: 1388,
		time: 1387,
		velocity: 2.05055555555556,
		power: 188.477314625274,
		road: 13143.8648611111,
		acceleration: -0.0351851851851852
	},
	{
		id: 1389,
		time: 1388,
		velocity: 2.07166666666667,
		power: 274.581561865828,
		road: 13145.9498148148,
		acceleration: 0.0089814814814817
	},
	{
		id: 1390,
		time: 1389,
		velocity: 2.14611111111111,
		power: 320.733797494692,
		road: 13148.0546296296,
		acceleration: 0.0307407407407405
	},
	{
		id: 1391,
		time: 1390,
		velocity: 2.14277777777778,
		power: 264.21321611921,
		road: 13150.1755092592,
		acceleration: 0.00138888888888866
	},
	{
		id: 1392,
		time: 1391,
		velocity: 2.07583333333333,
		power: 216.742313946916,
		road: 13152.28625,
		acceleration: -0.0216666666666665
	},
	{
		id: 1393,
		time: 1392,
		velocity: 2.08111111111111,
		power: 288.143921292768,
		road: 13154.393287037,
		acceleration: 0.014259259259259
	},
	{
		id: 1394,
		time: 1393,
		velocity: 2.18555555555556,
		power: 247.166538418382,
		road: 13156.5042129629,
		acceleration: -0.00648148148148131
	},
	{
		id: 1395,
		time: 1394,
		velocity: 2.05638888888889,
		power: 361.733856829964,
		road: 13158.6363425926,
		acceleration: 0.048888888888889
	},
	{
		id: 1396,
		time: 1395,
		velocity: 2.22777777777778,
		power: 296.964978205025,
		road: 13160.8002314815,
		acceleration: 0.0146296296296295
	},
	{
		id: 1397,
		time: 1396,
		velocity: 2.22944444444444,
		power: 297.069173118507,
		road: 13162.978287037,
		acceleration: 0.0137037037037042
	},
	{
		id: 1398,
		time: 1397,
		velocity: 2.0975,
		power: 288.27238464729,
		road: 13165.1675462963,
		acceleration: 0.00870370370370388
	},
	{
		id: 1399,
		time: 1398,
		velocity: 2.25388888888889,
		power: 211.931394313373,
		road: 13167.3473611111,
		acceleration: -0.0275925925925931
	},
	{
		id: 1400,
		time: 1399,
		velocity: 2.14666666666667,
		power: 279.783072793482,
		road: 13169.5163425926,
		acceleration: 0.00592592592592567
	},
	{
		id: 1401,
		time: 1400,
		velocity: 2.11527777777778,
		power: 151.9919897307,
		road: 13171.6606481481,
		acceleration: -0.0552777777777775
	},
	{
		id: 1402,
		time: 1401,
		velocity: 2.08805555555556,
		power: 261.838636714962,
		road: 13173.7775462963,
		acceleration: 0.000462962962962443
	},
	{
		id: 1403,
		time: 1402,
		velocity: 2.14805555555556,
		power: 313.429207006747,
		road: 13175.9072685185,
		acceleration: 0.0251851851851859
	},
	{
		id: 1404,
		time: 1403,
		velocity: 2.19083333333333,
		power: 293.959254869865,
		road: 13178.0566666666,
		acceleration: 0.0141666666666662
	},
	{
		id: 1405,
		time: 1404,
		velocity: 2.13055555555556,
		power: 266.376447036538,
		road: 13180.2132407407,
		acceleration: 0.00018518518518551
	},
	{
		id: 1406,
		time: 1405,
		velocity: 2.14861111111111,
		power: 186.598276632809,
		road: 13182.3509259259,
		acceleration: -0.037962962962963
	},
	{
		id: 1407,
		time: 1406,
		velocity: 2.07694444444444,
		power: 168.411916521132,
		road: 13184.447037037,
		acceleration: -0.0451851851851854
	},
	{
		id: 1408,
		time: 1407,
		velocity: 1.995,
		power: 95.0485504784592,
		road: 13186.4803240741,
		acceleration: -0.080462962962963
	},
	{
		id: 1409,
		time: 1408,
		velocity: 1.90722222222222,
		power: -1.72586962882524,
		road: 13188.4081481481,
		acceleration: -0.130462962962963
	},
	{
		id: 1410,
		time: 1409,
		velocity: 1.68555555555556,
		power: -320.507673297018,
		road: 13190.1067129629,
		acceleration: -0.328055555555556
	},
	{
		id: 1411,
		time: 1410,
		velocity: 1.01083333333333,
		power: -224.735287577561,
		road: 13191.4913888889,
		acceleration: -0.299722222222222
	},
	{
		id: 1412,
		time: 1411,
		velocity: 1.00805555555556,
		power: -172.780526736799,
		road: 13192.578287037,
		acceleration: -0.295833333333333
	},
	{
		id: 1413,
		time: 1412,
		velocity: 0.798055555555556,
		power: 104.518197824145,
		road: 13193.5123148148,
		acceleration: -0.00990740740740736
	},
	{
		id: 1414,
		time: 1413,
		velocity: 0.981111111111111,
		power: 321.597869825601,
		road: 13194.5420833333,
		acceleration: 0.201388888888889
	},
	{
		id: 1415,
		time: 1414,
		velocity: 1.61222222222222,
		power: 760.566234303944,
		road: 13195.9030092592,
		acceleration: 0.460925925925926
	},
	{
		id: 1416,
		time: 1415,
		velocity: 2.18083333333333,
		power: 898.226910732527,
		road: 13197.6941666666,
		acceleration: 0.399537037037037
	},
	{
		id: 1417,
		time: 1416,
		velocity: 2.17972222222222,
		power: 667.553887122599,
		road: 13199.7881944444,
		acceleration: 0.206203703703704
	},
	{
		id: 1418,
		time: 1417,
		velocity: 2.23083333333333,
		power: 201.496507243672,
		road: 13201.9689814815,
		acceleration: -0.0326851851851848
	},
	{
		id: 1419,
		time: 1418,
		velocity: 2.08277777777778,
		power: 217.476042682821,
		road: 13204.1216666666,
		acceleration: -0.0235185185185189
	},
	{
		id: 1420,
		time: 1419,
		velocity: 2.10916666666667,
		power: 230.939863254607,
		road: 13206.2546759259,
		acceleration: -0.0158333333333331
	},
	{
		id: 1421,
		time: 1420,
		velocity: 2.18333333333333,
		power: 314.445987690581,
		road: 13208.3923148148,
		acceleration: 0.0250925925925922
	},
	{
		id: 1422,
		time: 1421,
		velocity: 2.15805555555556,
		power: 281.372939931715,
		road: 13210.5463425926,
		acceleration: 0.00768518518518535
	},
	{
		id: 1423,
		time: 1422,
		velocity: 2.13222222222222,
		power: 228.203497311985,
		road: 13212.6951851852,
		acceleration: -0.0180555555555557
	},
	{
		id: 1424,
		time: 1423,
		velocity: 2.12916666666667,
		power: 236.97751655034,
		road: 13214.8285648148,
		acceleration: -0.0128703703703703
	},
	{
		id: 1425,
		time: 1424,
		velocity: 2.11944444444444,
		power: 275.708002838999,
		road: 13216.95875,
		acceleration: 0.00648148148148131
	},
	{
		id: 1426,
		time: 1425,
		velocity: 2.15166666666667,
		power: 308.680219952187,
		road: 13219.1030555555,
		acceleration: 0.0217592592592597
	},
	{
		id: 1427,
		time: 1426,
		velocity: 2.19444444444444,
		power: 318.236255939796,
		road: 13221.2706018518,
		acceleration: 0.0247222222222221
	},
	{
		id: 1428,
		time: 1427,
		velocity: 2.19361111111111,
		power: 213.650165334807,
		road: 13223.4374537037,
		acceleration: -0.0261111111111112
	},
	{
		id: 1429,
		time: 1428,
		velocity: 2.07333333333333,
		power: 296.17684803041,
		road: 13225.5984722222,
		acceleration: 0.0144444444444445
	},
	{
		id: 1430,
		time: 1429,
		velocity: 2.23777777777778,
		power: 400.09593941336,
		road: 13227.7975462963,
		acceleration: 0.0616666666666665
	},
	{
		id: 1431,
		time: 1430,
		velocity: 2.37861111111111,
		power: 654.757591702637,
		road: 13230.1113888889,
		acceleration: 0.167870370370371
	},
	{
		id: 1432,
		time: 1431,
		velocity: 2.57694444444444,
		power: 593.908016437866,
		road: 13232.5710185185,
		acceleration: 0.123703703703704
	},
	{
		id: 1433,
		time: 1432,
		velocity: 2.60888888888889,
		power: 532.755028568947,
		road: 13235.1363888889,
		acceleration: 0.0877777777777773
	},
	{
		id: 1434,
		time: 1433,
		velocity: 2.64194444444444,
		power: 381.129284756454,
		road: 13237.7566666666,
		acceleration: 0.0220370370370375
	},
	{
		id: 1435,
		time: 1434,
		velocity: 2.64305555555556,
		power: 612.488534431847,
		road: 13240.4424074074,
		acceleration: 0.108888888888889
	},
	{
		id: 1436,
		time: 1435,
		velocity: 2.93555555555556,
		power: 679.133676482625,
		road: 13243.2443981481,
		acceleration: 0.123611111111111
	},
	{
		id: 1437,
		time: 1436,
		velocity: 3.01277777777778,
		power: 562.610207276238,
		road: 13246.1443518518,
		acceleration: 0.0723148148148152
	},
	{
		id: 1438,
		time: 1437,
		velocity: 2.86,
		power: 12.8879758525995,
		road: 13249.0168055555,
		acceleration: -0.127314814814815
	},
	{
		id: 1439,
		time: 1438,
		velocity: 2.55361111111111,
		power: -192.418564944538,
		road: 13251.7223611111,
		acceleration: -0.206481481481482
	},
	{
		id: 1440,
		time: 1439,
		velocity: 2.39333333333333,
		power: -122.130317797863,
		road: 13254.2335648148,
		acceleration: -0.182222222222222
	},
	{
		id: 1441,
		time: 1440,
		velocity: 2.31333333333333,
		power: 14.6764399747546,
		road: 13256.5916666666,
		acceleration: -0.123981481481481
	},
	{
		id: 1442,
		time: 1441,
		velocity: 2.18166666666667,
		power: 40.4604244942238,
		road: 13258.8321759259,
		acceleration: -0.111203703703704
	},
	{
		id: 1443,
		time: 1442,
		velocity: 2.05972222222222,
		power: 162.441053946626,
		road: 13260.9917129629,
		acceleration: -0.0507407407407401
	},
	{
		id: 1444,
		time: 1443,
		velocity: 2.16111111111111,
		power: 288.329961063647,
		road: 13263.1318981481,
		acceleration: 0.0120370370370368
	},
	{
		id: 1445,
		time: 1444,
		velocity: 2.21777777777778,
		power: 290.989737422369,
		road: 13265.2843518518,
		acceleration: 0.0125000000000002
	},
	{
		id: 1446,
		time: 1445,
		velocity: 2.09722222222222,
		power: 219.77037575375,
		road: 13267.4319907407,
		acceleration: -0.0221296296296294
	},
	{
		id: 1447,
		time: 1446,
		velocity: 2.09472222222222,
		power: 538.352022967111,
		road: 13269.6324537037,
		acceleration: 0.127777777777778
	},
	{
		id: 1448,
		time: 1447,
		velocity: 2.60111111111111,
		power: 1089.07342334022,
		road: 13272.0671759259,
		acceleration: 0.340740740740741
	},
	{
		id: 1449,
		time: 1448,
		velocity: 3.11944444444444,
		power: 1748.09475631737,
		road: 13274.9281944444,
		acceleration: 0.511851851851852
	},
	{
		id: 1450,
		time: 1449,
		velocity: 3.63027777777778,
		power: 1616.07166564208,
		road: 13278.2357407407,
		acceleration: 0.381203703703703
	},
	{
		id: 1451,
		time: 1450,
		velocity: 3.74472222222222,
		power: 1445.01296012623,
		road: 13281.8755555555,
		acceleration: 0.283333333333334
	},
	{
		id: 1452,
		time: 1451,
		velocity: 3.96944444444444,
		power: 1113.41642237717,
		road: 13285.7408333333,
		acceleration: 0.167592592592592
	},
	{
		id: 1453,
		time: 1452,
		velocity: 4.13305555555556,
		power: 962.323547489714,
		road: 13289.7481481481,
		acceleration: 0.116481481481481
	},
	{
		id: 1454,
		time: 1453,
		velocity: 4.09416666666667,
		power: 540.062834789033,
		road: 13293.8152777778,
		acceleration: 0.00314814814814834
	},
	{
		id: 1455,
		time: 1454,
		velocity: 3.97888888888889,
		power: -150.462472847098,
		road: 13297.7959259259,
		acceleration: -0.176111111111111
	},
	{
		id: 1456,
		time: 1455,
		velocity: 3.60472222222222,
		power: -885.679309346794,
		road: 13301.4948611111,
		acceleration: -0.387314814814815
	},
	{
		id: 1457,
		time: 1456,
		velocity: 2.93222222222222,
		power: -805.399273825236,
		road: 13304.8051851852,
		acceleration: -0.389907407407408
	},
	{
		id: 1458,
		time: 1457,
		velocity: 2.80916666666667,
		power: -459.323864297593,
		road: 13307.7728240741,
		acceleration: -0.295462962962962
	},
	{
		id: 1459,
		time: 1458,
		velocity: 2.71833333333333,
		power: 100.053730072321,
		road: 13310.5458796296,
		acceleration: -0.0937037037037038
	},
	{
		id: 1460,
		time: 1459,
		velocity: 2.65111111111111,
		power: 249.065379109232,
		road: 13313.2547685185,
		acceleration: -0.0346296296296296
	},
	{
		id: 1461,
		time: 1460,
		velocity: 2.70527777777778,
		power: 334.805546321917,
		road: 13315.9461574074,
		acceleration: -0.000370370370370576
	},
	{
		id: 1462,
		time: 1461,
		velocity: 2.71722222222222,
		power: 615.808830072206,
		road: 13318.6898148148,
		acceleration: 0.104907407407407
	},
	{
		id: 1463,
		time: 1462,
		velocity: 2.96583333333333,
		power: 822.125484290682,
		road: 13321.5702777778,
		acceleration: 0.168703703703704
	},
	{
		id: 1464,
		time: 1463,
		velocity: 3.21138888888889,
		power: 1132.84284854494,
		road: 13324.6617592592,
		acceleration: 0.253333333333333
	},
	{
		id: 1465,
		time: 1464,
		velocity: 3.47722222222222,
		power: 1159.1631367551,
		road: 13327.9962037037,
		acceleration: 0.232592592592593
	},
	{
		id: 1466,
		time: 1465,
		velocity: 3.66361111111111,
		power: 971.212546935898,
		road: 13331.5247222222,
		acceleration: 0.155555555555555
	},
	{
		id: 1467,
		time: 1466,
		velocity: 3.67805555555556,
		power: 690.988807390775,
		road: 13335.1636111111,
		acceleration: 0.0651851851851846
	},
	{
		id: 1468,
		time: 1467,
		velocity: 3.67277777777778,
		power: 624.146513442692,
		road: 13338.8565740741,
		acceleration: 0.0429629629629633
	},
	{
		id: 1469,
		time: 1468,
		velocity: 3.7925,
		power: 644.868989421304,
		road: 13342.5942592592,
		acceleration: 0.0464814814814818
	},
	{
		id: 1470,
		time: 1469,
		velocity: 3.8175,
		power: 1022.18744205889,
		road: 13346.4277777778,
		acceleration: 0.145185185185185
	},
	{
		id: 1471,
		time: 1470,
		velocity: 4.10833333333333,
		power: 994.211548140707,
		road: 13350.3976388889,
		acceleration: 0.1275
	},
	{
		id: 1472,
		time: 1471,
		velocity: 4.175,
		power: 901.834812483452,
		road: 13354.4792129629,
		acceleration: 0.0959259259259255
	},
	{
		id: 1473,
		time: 1472,
		velocity: 4.10527777777778,
		power: 933.933166376482,
		road: 13358.6578240741,
		acceleration: 0.0981481481481481
	},
	{
		id: 1474,
		time: 1473,
		velocity: 4.40277777777778,
		power: 1893.66606475879,
		road: 13363.0437037037,
		acceleration: 0.31638888888889
	},
	{
		id: 1475,
		time: 1474,
		velocity: 5.12416666666667,
		power: 2557.14124500718,
		road: 13367.8006481481,
		acceleration: 0.42574074074074
	},
	{
		id: 1476,
		time: 1475,
		velocity: 5.3825,
		power: 1754.17650651993,
		road: 13372.8812037037,
		acceleration: 0.221481481481482
	},
	{
		id: 1477,
		time: 1476,
		velocity: 5.06722222222222,
		power: 330.633177684581,
		road: 13378.0350925926,
		acceleration: -0.0748148148148147
	},
	{
		id: 1478,
		time: 1477,
		velocity: 4.89972222222222,
		power: -624.014697311854,
		road: 13383.0149537037,
		acceleration: -0.273240740740741
	},
	{
		id: 1479,
		time: 1478,
		velocity: 4.56277777777778,
		power: -526.26141887705,
		road: 13387.7294907407,
		acceleration: -0.257407407407408
	},
	{
		id: 1480,
		time: 1479,
		velocity: 4.295,
		power: -583.542467510292,
		road: 13392.1769907407,
		acceleration: -0.276666666666666
	},
	{
		id: 1481,
		time: 1480,
		velocity: 4.06972222222222,
		power: -379.502199955281,
		road: 13396.3698611111,
		acceleration: -0.232592592592593
	},
	{
		id: 1482,
		time: 1481,
		velocity: 3.865,
		power: -359.99764599852,
		road: 13400.3304629629,
		acceleration: -0.231944444444444
	},
	{
		id: 1483,
		time: 1482,
		velocity: 3.59916666666667,
		power: -144.131417162986,
		road: 13404.0872222222,
		acceleration: -0.175740740740741
	},
	{
		id: 1484,
		time: 1483,
		velocity: 3.5425,
		power: 996.545006189659,
		road: 13407.82875,
		acceleration: 0.145277777777778
	},
	{
		id: 1485,
		time: 1484,
		velocity: 4.30083333333333,
		power: 2247.54422839163,
		road: 13411.8676388889,
		acceleration: 0.449444444444445
	},
	{
		id: 1486,
		time: 1485,
		velocity: 4.9475,
		power: 4822.02499989573,
		road: 13416.5977777778,
		acceleration: 0.933055555555555
	},
	{
		id: 1487,
		time: 1486,
		velocity: 6.34166666666667,
		power: 5218.41775039259,
		road: 13422.2109259259,
		acceleration: 0.832962962962963
	},
	{
		id: 1488,
		time: 1487,
		velocity: 6.79972222222222,
		power: 5522.44403216965,
		road: 13428.6184722222,
		acceleration: 0.755833333333333
	},
	{
		id: 1489,
		time: 1488,
		velocity: 7.215,
		power: 4073.06957180814,
		road: 13435.6316666666,
		acceleration: 0.455462962962963
	},
	{
		id: 1490,
		time: 1489,
		velocity: 7.70805555555556,
		power: 5449.88475381498,
		road: 13443.1726388889,
		acceleration: 0.600092592592592
	},
	{
		id: 1491,
		time: 1490,
		velocity: 8.6,
		power: 6461.66591041345,
		road: 13451.3463888889,
		acceleration: 0.665462962962963
	},
	{
		id: 1492,
		time: 1491,
		velocity: 9.21138888888889,
		power: 4399.40338634605,
		road: 13460.0338425926,
		acceleration: 0.361944444444445
	},
	{
		id: 1493,
		time: 1492,
		velocity: 8.79388888888889,
		power: -1139.1058434994,
		road: 13468.7485185185,
		acceleration: -0.307499999999999
	},
	{
		id: 1494,
		time: 1493,
		velocity: 7.6775,
		power: -3907.73942129502,
		road: 13476.9771759259,
		acceleration: -0.664537037037038
	},
	{
		id: 1495,
		time: 1494,
		velocity: 7.21777777777778,
		power: -3423.44664159162,
		road: 13484.5562962963,
		acceleration: -0.634537037037037
	},
	{
		id: 1496,
		time: 1495,
		velocity: 6.89027777777778,
		power: -1340.10847404947,
		road: 13491.6408796296,
		acceleration: -0.354537037037036
	},
	{
		id: 1497,
		time: 1496,
		velocity: 6.61388888888889,
		power: 429.317421045799,
		road: 13498.5041203704,
		acceleration: -0.0881481481481501
	},
	{
		id: 1498,
		time: 1497,
		velocity: 6.95333333333333,
		power: 3567.34193099235,
		road: 13505.513287037,
		acceleration: 0.380000000000002
	},
	{
		id: 1499,
		time: 1498,
		velocity: 8.03027777777778,
		power: 6668.21570100738,
		road: 13513.0947222222,
		acceleration: 0.764537037037037
	},
	{
		id: 1500,
		time: 1499,
		velocity: 8.9075,
		power: 11033.7071506236,
		road: 13521.65125,
		acceleration: 1.18564814814815
	},
	{
		id: 1501,
		time: 1500,
		velocity: 10.5102777777778,
		power: 12722.7187906309,
		road: 13531.3953240741,
		acceleration: 1.18944444444444
	},
	{
		id: 1502,
		time: 1501,
		velocity: 11.5986111111111,
		power: 11290.4755686959,
		road: 13542.1863425926,
		acceleration: 0.904444444444444
	},
	{
		id: 1503,
		time: 1502,
		velocity: 11.6208333333333,
		power: 5883.38827649489,
		road: 13553.5994444444,
		acceleration: 0.339722222222221
	},
	{
		id: 1504,
		time: 1503,
		velocity: 11.5294444444444,
		power: 172.516343117162,
		road: 13565.0893518518,
		acceleration: -0.186111111111112
	},
	{
		id: 1505,
		time: 1504,
		velocity: 11.0402777777778,
		power: -238.355963420854,
		road: 13576.3755092592,
		acceleration: -0.221388888888887
	},
	{
		id: 1506,
		time: 1505,
		velocity: 10.9566666666667,
		power: 1322.21522251458,
		road: 13587.514537037,
		acceleration: -0.0728703703703708
	},
	{
		id: 1507,
		time: 1506,
		velocity: 11.3108333333333,
		power: 5504.23637343782,
		road: 13598.7740277778,
		acceleration: 0.313796296296298
	},
	{
		id: 1508,
		time: 1507,
		velocity: 11.9816666666667,
		power: 7488.22786941814,
		road: 13610.4253703704,
		acceleration: 0.469907407407407
	},
	{
		id: 1509,
		time: 1508,
		velocity: 12.3663888888889,
		power: 10341.8355753026,
		road: 13622.6492129629,
		acceleration: 0.675092592592595
	},
	{
		id: 1510,
		time: 1509,
		velocity: 13.3361111111111,
		power: 11030.1909753342,
		road: 13635.5479629629,
		acceleration: 0.674722222222218
	},
	{
		id: 1511,
		time: 1510,
		velocity: 14.0058333333333,
		power: 14815.0497699381,
		road: 13649.2343518518,
		acceleration: 0.900555555555556
	},
	{
		id: 1512,
		time: 1511,
		velocity: 15.0680555555556,
		power: 15144.8908702869,
		road: 13663.792037037,
		acceleration: 0.842037037037038
	},
	{
		id: 1513,
		time: 1512,
		velocity: 15.8622222222222,
		power: 12410.5165646454,
		road: 13679.0663425926,
		acceleration: 0.591203703703703
	},
	{
		id: 1514,
		time: 1513,
		velocity: 15.7794444444444,
		power: 6708.1287248983,
		road: 13694.7274537037,
		acceleration: 0.182407407407409
	},
	{
		id: 1515,
		time: 1514,
		velocity: 15.6152777777778,
		power: 2730.62626269755,
		road: 13710.4374074074,
		acceleration: -0.0847222222222221
	},
	{
		id: 1516,
		time: 1515,
		velocity: 15.6080555555556,
		power: 834.768603546593,
		road: 13726.0010648148,
		acceleration: -0.207870370370369
	},
	{
		id: 1517,
		time: 1516,
		velocity: 15.1558333333333,
		power: -646.013330967762,
		road: 13741.3089351852,
		acceleration: -0.303703703703707
	},
	{
		id: 1518,
		time: 1517,
		velocity: 14.7041666666667,
		power: -688.720065962442,
		road: 13756.31375,
		acceleration: -0.302407407407404
	},
	{
		id: 1519,
		time: 1518,
		velocity: 14.7008333333333,
		power: 869.096209604506,
		road: 13771.0730555555,
		acceleration: -0.188611111111113
	},
	{
		id: 1520,
		time: 1519,
		velocity: 14.59,
		power: 577.702238040145,
		road: 13785.6353240741,
		acceleration: -0.205462962962962
	},
	{
		id: 1521,
		time: 1520,
		velocity: 14.0877777777778,
		power: -840.42682222214,
		road: 13799.9426851852,
		acceleration: -0.304351851851854
	},
	{
		id: 1522,
		time: 1521,
		velocity: 13.7877777777778,
		power: -2158.9631510629,
		road: 13813.8982407407,
		acceleration: -0.399259259259257
	},
	{
		id: 1523,
		time: 1522,
		velocity: 13.3922222222222,
		power: -2072.43635563358,
		road: 13827.4585648148,
		acceleration: -0.391203703703706
	},
	{
		id: 1524,
		time: 1523,
		velocity: 12.9141666666667,
		power: -1699.0600952585,
		road: 13840.6430555555,
		acceleration: -0.360462962962963
	},
	{
		id: 1525,
		time: 1524,
		velocity: 12.7063888888889,
		power: -805.140581221894,
		road: 13853.5041666667,
		acceleration: -0.286296296296296
	},
	{
		id: 1526,
		time: 1525,
		velocity: 12.5333333333333,
		power: -851.988951663517,
		road: 13866.0783333333,
		acceleration: -0.287592592592594
	},
	{
		id: 1527,
		time: 1526,
		velocity: 12.0513888888889,
		power: -2142.10803244741,
		road: 13878.3110185185,
		acceleration: -0.395370370370371
	},
	{
		id: 1528,
		time: 1527,
		velocity: 11.5202777777778,
		power: -9228.94186263382,
		road: 13889.8245833333,
		acceleration: -1.04287037037037
	},
	{
		id: 1529,
		time: 1528,
		velocity: 9.40472222222222,
		power: -9271.74497493553,
		road: 13900.2559259259,
		acceleration: -1.12157407407407
	},
	{
		id: 1530,
		time: 1529,
		velocity: 8.68666666666667,
		power: -3211.70713938072,
		road: 13909.86125,
		acceleration: -0.530462962962963
	},
	{
		id: 1531,
		time: 1530,
		velocity: 9.92888888888889,
		power: 8293.45748785465,
		road: 13919.5599074074,
		acceleration: 0.71712962962963
	},
	{
		id: 1532,
		time: 1531,
		velocity: 11.5561111111111,
		power: 12547.6123938368,
		road: 13930.1437037037,
		acceleration: 1.05314814814815
	},
	{
		id: 1533,
		time: 1532,
		velocity: 11.8461111111111,
		power: 7908.12430446829,
		road: 13941.5184259259,
		acceleration: 0.528703703703703
	},
	{
		id: 1534,
		time: 1533,
		velocity: 11.515,
		power: -346.316732291345,
		road: 13953.0406018518,
		acceleration: -0.233796296296296
	},
	{
		id: 1535,
		time: 1534,
		velocity: 10.8547222222222,
		power: -1337.05183128392,
		road: 13964.2841666667,
		acceleration: -0.323425925925926
	},
	{
		id: 1536,
		time: 1535,
		velocity: 10.8758333333333,
		power: -1928.89670249506,
		road: 13975.1759722222,
		acceleration: -0.380092592592595
	},
	{
		id: 1537,
		time: 1536,
		velocity: 10.3747222222222,
		power: -1761.92075393606,
		road: 13985.6949537037,
		acceleration: -0.365555555555554
	},
	{
		id: 1538,
		time: 1537,
		velocity: 9.75805555555555,
		power: -4544.18160662515,
		road: 13995.7008796296,
		acceleration: -0.660555555555556
	},
	{
		id: 1539,
		time: 1538,
		velocity: 8.89416666666667,
		power: -6199.83688462124,
		road: 14004.9362962963,
		acceleration: -0.880462962962962
	},
	{
		id: 1540,
		time: 1539,
		velocity: 7.73333333333333,
		power: -7695.54573654505,
		road: 14013.1568981481,
		acceleration: -1.14916666666667
	},
	{
		id: 1541,
		time: 1540,
		velocity: 6.31055555555556,
		power: -8355.7289752514,
		road: 14020.0921759259,
		acceleration: -1.42148148148148
	},
	{
		id: 1542,
		time: 1541,
		velocity: 4.62972222222222,
		power: -6110.20945965485,
		road: 14025.6675925926,
		acceleration: -1.29824074074074
	},
	{
		id: 1543,
		time: 1542,
		velocity: 3.83861111111111,
		power: -4688.44892642363,
		road: 14029.948287037,
		acceleration: -1.2912037037037
	},
	{
		id: 1544,
		time: 1543,
		velocity: 2.43694444444444,
		power: -2383.25982065211,
		road: 14033.1210648148,
		acceleration: -0.924629629629631
	},
	{
		id: 1545,
		time: 1544,
		velocity: 1.85583333333333,
		power: -1535.00358846384,
		road: 14035.4133333333,
		acceleration: -0.836388888888889
	},
	{
		id: 1546,
		time: 1545,
		velocity: 1.32944444444444,
		power: -548.788422661007,
		road: 14037.0456481481,
		acceleration: -0.483518518518519
	},
	{
		id: 1547,
		time: 1546,
		velocity: 0.986388888888889,
		power: -502.685279611187,
		road: 14038.1268981481,
		acceleration: -0.618611111111111
	},
	{
		id: 1548,
		time: 1547,
		velocity: 0,
		power: -164.532109681683,
		road: 14038.6772685185,
		acceleration: -0.443148148148148
	},
	{
		id: 1549,
		time: 1548,
		velocity: 0,
		power: -31.3463429012346,
		road: 14038.8416666667,
		acceleration: -0.328796296296296
	},
	{
		id: 1550,
		time: 1549,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1551,
		time: 1550,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1552,
		time: 1551,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1553,
		time: 1552,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1554,
		time: 1553,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1555,
		time: 1554,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1556,
		time: 1555,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1557,
		time: 1556,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1558,
		time: 1557,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1559,
		time: 1558,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1560,
		time: 1559,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1561,
		time: 1560,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1562,
		time: 1561,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1563,
		time: 1562,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1564,
		time: 1563,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1565,
		time: 1564,
		velocity: 0,
		power: 0,
		road: 14038.8416666667,
		acceleration: 0
	},
	{
		id: 1566,
		time: 1565,
		velocity: 0,
		power: 1.20222382314442,
		road: 14038.8504166667,
		acceleration: 0.0175
	},
	{
		id: 1567,
		time: 1566,
		velocity: 0.0525,
		power: 2.11431606734146,
		road: 14038.8679166667,
		acceleration: 0
	},
	{
		id: 1568,
		time: 1567,
		velocity: 0,
		power: 2.11431606734146,
		road: 14038.8854166667,
		acceleration: 0
	},
	{
		id: 1569,
		time: 1568,
		velocity: 0,
		power: 0.912090789473684,
		road: 14038.8941666667,
		acceleration: -0.0175
	},
	{
		id: 1570,
		time: 1569,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1571,
		time: 1570,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1572,
		time: 1571,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1573,
		time: 1572,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1574,
		time: 1573,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1575,
		time: 1574,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1576,
		time: 1575,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1577,
		time: 1576,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1578,
		time: 1577,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1579,
		time: 1578,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1580,
		time: 1579,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1581,
		time: 1580,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1582,
		time: 1581,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1583,
		time: 1582,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1584,
		time: 1583,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1585,
		time: 1584,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1586,
		time: 1585,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1587,
		time: 1586,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1588,
		time: 1587,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1589,
		time: 1588,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1590,
		time: 1589,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1591,
		time: 1590,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1592,
		time: 1591,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1593,
		time: 1592,
		velocity: 0,
		power: 0,
		road: 14038.8941666667,
		acceleration: 0
	},
	{
		id: 1594,
		time: 1593,
		velocity: 0,
		power: 3.90895986939967,
		road: 14038.9177777778,
		acceleration: 0.0472222222222222
	},
	{
		id: 1595,
		time: 1594,
		velocity: 0.141666666666667,
		power: 188.242195927233,
		road: 14039.2247685185,
		acceleration: 0.519537037037037
	},
	{
		id: 1596,
		time: 1595,
		velocity: 1.55861111111111,
		power: 1634.12749820614,
		road: 14040.4377777778,
		acceleration: 1.2925
	},
	{
		id: 1597,
		time: 1596,
		velocity: 3.8775,
		power: 4040.6317546965,
		road: 14043.0472685185,
		acceleration: 1.50046296296296
	},
	{
		id: 1598,
		time: 1597,
		velocity: 4.64305555555556,
		power: 5565.23532738316,
		road: 14047.0675462963,
		acceleration: 1.32111111111111
	},
	{
		id: 1599,
		time: 1598,
		velocity: 5.52194444444444,
		power: 4972.31916637602,
		road: 14052.1882407407,
		acceleration: 0.879722222222222
	},
	{
		id: 1600,
		time: 1599,
		velocity: 6.51666666666667,
		power: 3685.81446668499,
		road: 14058.0086574074,
		acceleration: 0.519722222222222
	},
	{
		id: 1601,
		time: 1600,
		velocity: 6.20222222222222,
		power: 2836.58953844124,
		road: 14064.2531481481,
		acceleration: 0.328425925925926
	},
	{
		id: 1602,
		time: 1601,
		velocity: 6.50722222222222,
		power: 1100.35907655302,
		road: 14070.6766203704,
		acceleration: 0.0295370370370378
	},
	{
		id: 1603,
		time: 1602,
		velocity: 6.60527777777778,
		power: 2292.70487070408,
		road: 14077.2232407407,
		acceleration: 0.216759259259259
	},
	{
		id: 1604,
		time: 1603,
		velocity: 6.8525,
		power: 1768.05589133323,
		road: 14083.9402314815,
		acceleration: 0.12398148148148
	},
	{
		id: 1605,
		time: 1604,
		velocity: 6.87916666666667,
		power: 1827.97501676962,
		road: 14090.7827777778,
		acceleration: 0.127129629629631
	},
	{
		id: 1606,
		time: 1605,
		velocity: 6.98666666666667,
		power: 873.259845332058,
		road: 14097.6783796296,
		acceleration: -0.0210185185185194
	},
	{
		id: 1607,
		time: 1606,
		velocity: 6.78944444444444,
		power: -509.312773127056,
		road: 14104.4473148148,
		acceleration: -0.232314814814814
	},
	{
		id: 1608,
		time: 1607,
		velocity: 6.18222222222222,
		power: -2067.7161601452,
		road: 14110.8551388889,
		acceleration: -0.489907407407408
	},
	{
		id: 1609,
		time: 1608,
		velocity: 5.51694444444444,
		power: -2719.19294029215,
		road: 14116.6999537037,
		acceleration: -0.636111111111112
	},
	{
		id: 1610,
		time: 1609,
		velocity: 4.88111111111111,
		power: -2349.00200911661,
		road: 14121.9184722222,
		acceleration: -0.616481481481481
	},
	{
		id: 1611,
		time: 1610,
		velocity: 4.33277777777778,
		power: -1540.65635862456,
		road: 14126.5851388889,
		acceleration: -0.487222222222222
	},
	{
		id: 1612,
		time: 1611,
		velocity: 4.05527777777778,
		power: -1240.82278233401,
		road: 14130.7839351852,
		acceleration: -0.448518518518519
	},
	{
		id: 1613,
		time: 1612,
		velocity: 3.53555555555556,
		power: -832.756573642226,
		road: 14134.5750462963,
		acceleration: -0.366851851851852
	},
	{
		id: 1614,
		time: 1613,
		velocity: 3.23222222222222,
		power: -599.55489969579,
		road: 14138.0241203704,
		acceleration: -0.317222222222222
	},
	{
		id: 1615,
		time: 1614,
		velocity: 3.10361111111111,
		power: -367.779712841193,
		road: 14141.1868055555,
		acceleration: -0.255555555555556
	},
	{
		id: 1616,
		time: 1615,
		velocity: 2.76888888888889,
		power: -1146.09043183918,
		road: 14143.9362037037,
		acceleration: -0.571018518518519
	},
	{
		id: 1617,
		time: 1616,
		velocity: 1.51916666666667,
		power: -1670.51312831618,
		road: 14145.8828240741,
		acceleration: -1.03453703703704
	},
	{
		id: 1618,
		time: 1617,
		velocity: 0,
		power: -729.221471755754,
		road: 14146.8506944444,
		acceleration: -0.922962962962963
	},
	{
		id: 1619,
		time: 1618,
		velocity: 0,
		power: -88.5465925286211,
		road: 14147.1193981481,
		acceleration: -0.47537037037037
	},
	{
		id: 1620,
		time: 1619,
		velocity: 0.0930555555555556,
		power: 3.74760830691958,
		road: 14147.1504166667,
		acceleration: 0
	},
	{
		id: 1621,
		time: 1620,
		velocity: 0,
		power: 3.74760830691958,
		road: 14147.1814351852,
		acceleration: 0
	},
	{
		id: 1622,
		time: 1621,
		velocity: 0,
		power: 1.41804150422352,
		road: 14147.1969444444,
		acceleration: -0.0310185185185185
	},
	{
		id: 1623,
		time: 1622,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1624,
		time: 1623,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1625,
		time: 1624,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1626,
		time: 1625,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1627,
		time: 1626,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1628,
		time: 1627,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1629,
		time: 1628,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1630,
		time: 1629,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1631,
		time: 1630,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1632,
		time: 1631,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1633,
		time: 1632,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1634,
		time: 1633,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1635,
		time: 1634,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1636,
		time: 1635,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1637,
		time: 1636,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1638,
		time: 1637,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1639,
		time: 1638,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1640,
		time: 1639,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1641,
		time: 1640,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1642,
		time: 1641,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1643,
		time: 1642,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1644,
		time: 1643,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1645,
		time: 1644,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1646,
		time: 1645,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1647,
		time: 1646,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1648,
		time: 1647,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1649,
		time: 1648,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1650,
		time: 1649,
		velocity: 0,
		power: 0,
		road: 14147.1969444444,
		acceleration: 0
	},
	{
		id: 1651,
		time: 1650,
		velocity: 0,
		power: 38.0581822751149,
		road: 14147.3103240741,
		acceleration: 0.226759259259259
	},
	{
		id: 1652,
		time: 1651,
		velocity: 0.680277777777778,
		power: 327.042006486968,
		road: 14147.8150925926,
		acceleration: 0.556018518518518
	},
	{
		id: 1653,
		time: 1652,
		velocity: 1.66805555555556,
		power: 1787.42615896683,
		road: 14149.2094907407,
		acceleration: 1.22324074074074
	},
	{
		id: 1654,
		time: 1653,
		velocity: 3.66972222222222,
		power: 2963.26005088385,
		road: 14151.7617592592,
		acceleration: 1.0925
	},
	{
		id: 1655,
		time: 1654,
		velocity: 3.95777777777778,
		power: 3294.30108805293,
		road: 14155.285462963,
		acceleration: 0.850370370370371
	},
	{
		id: 1656,
		time: 1655,
		velocity: 4.21916666666667,
		power: 1191.53810194587,
		road: 14159.3215277778,
		acceleration: 0.174351851851851
	},
	{
		id: 1657,
		time: 1656,
		velocity: 4.19277777777778,
		power: 580.975874801144,
		road: 14163.4503703704,
		acceleration: 0.0112037037037034
	},
	{
		id: 1658,
		time: 1657,
		velocity: 3.99138888888889,
		power: 1204.43049633291,
		road: 14167.6665277778,
		acceleration: 0.163425925925926
	},
	{
		id: 1659,
		time: 1658,
		velocity: 4.70944444444444,
		power: 2137.61603371304,
		road: 14172.14625,
		acceleration: 0.363703703703703
	},
	{
		id: 1660,
		time: 1659,
		velocity: 5.28388888888889,
		power: 3753.7954836031,
		road: 14177.1332407407,
		acceleration: 0.650833333333333
	},
	{
		id: 1661,
		time: 1660,
		velocity: 5.94388888888889,
		power: 3945.74403375549,
		road: 14182.7431018518,
		acceleration: 0.594907407407407
	},
	{
		id: 1662,
		time: 1661,
		velocity: 6.49416666666667,
		power: 5076.32349199134,
		road: 14189.0021759259,
		acceleration: 0.703518518518519
	},
	{
		id: 1663,
		time: 1662,
		velocity: 7.39444444444444,
		power: 3659.5497029199,
		road: 14195.8184722222,
		acceleration: 0.410925925925927
	},
	{
		id: 1664,
		time: 1663,
		velocity: 7.17666666666667,
		power: 1970.54531718987,
		road: 14202.9084722222,
		acceleration: 0.136481481481481
	},
	{
		id: 1665,
		time: 1664,
		velocity: 6.90361111111111,
		power: -625.162038195857,
		road: 14209.9423611111,
		acceleration: -0.248703703703704
	},
	{
		id: 1666,
		time: 1665,
		velocity: 6.64833333333333,
		power: -349.65938316009,
		road: 14216.7481481481,
		acceleration: -0.2075
	},
	{
		id: 1667,
		time: 1666,
		velocity: 6.55416666666667,
		power: -317.439756020206,
		road: 14223.3489351852,
		acceleration: -0.202500000000001
	},
	{
		id: 1668,
		time: 1667,
		velocity: 6.29611111111111,
		power: 105.598030121285,
		road: 14229.7817592593,
		acceleration: -0.133425925925926
	},
	{
		id: 1669,
		time: 1668,
		velocity: 6.24805555555556,
		power: -602.302901859328,
		road: 14236.0224537037,
		acceleration: -0.250833333333333
	},
	{
		id: 1670,
		time: 1669,
		velocity: 5.80166666666667,
		power: -822.497693111394,
		road: 14241.9915277778,
		acceleration: -0.292407407407406
	},
	{
		id: 1671,
		time: 1670,
		velocity: 5.41888888888889,
		power: -2458.40997313708,
		road: 14247.5076388889,
		acceleration: -0.613518518518519
	},
	{
		id: 1672,
		time: 1671,
		velocity: 4.4075,
		power: -2363.31190036687,
		road: 14252.3918981481,
		acceleration: -0.650185185185185
	},
	{
		id: 1673,
		time: 1672,
		velocity: 3.85111111111111,
		power: -1574.88780006046,
		road: 14256.689212963,
		acceleration: -0.523703703703704
	},
	{
		id: 1674,
		time: 1673,
		velocity: 3.84777777777778,
		power: -51.1526687107108,
		road: 14260.6497685185,
		acceleration: -0.149814814814815
	},
	{
		id: 1675,
		time: 1674,
		velocity: 3.95805555555556,
		power: -385.505578244828,
		road: 14264.4137962963,
		acceleration: -0.24324074074074
	},
	{
		id: 1676,
		time: 1675,
		velocity: 3.12138888888889,
		power: -1818.73121676828,
		road: 14267.6976851852,
		acceleration: -0.717037037037037
	},
	{
		id: 1677,
		time: 1676,
		velocity: 1.69666666666667,
		power: -1658.76762117985,
		road: 14270.2094907407,
		acceleration: -0.82712962962963
	},
	{
		id: 1678,
		time: 1677,
		velocity: 1.47666666666667,
		power: -1363.83623303522,
		road: 14271.7875,
		acceleration: -1.04046296296296
	},
	{
		id: 1679,
		time: 1678,
		velocity: 0,
		power: -321.501039371387,
		road: 14272.5625,
		acceleration: -0.565555555555556
	},
	{
		id: 1680,
		time: 1679,
		velocity: 0,
		power: -85.0308707602339,
		road: 14272.8086111111,
		acceleration: -0.492222222222222
	},
	{
		id: 1681,
		time: 1680,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1682,
		time: 1681,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1683,
		time: 1682,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1684,
		time: 1683,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1685,
		time: 1684,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1686,
		time: 1685,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1687,
		time: 1686,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1688,
		time: 1687,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1689,
		time: 1688,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1690,
		time: 1689,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1691,
		time: 1690,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1692,
		time: 1691,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1693,
		time: 1692,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1694,
		time: 1693,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1695,
		time: 1694,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1696,
		time: 1695,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1697,
		time: 1696,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1698,
		time: 1697,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1699,
		time: 1698,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1700,
		time: 1699,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1701,
		time: 1700,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1702,
		time: 1701,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1703,
		time: 1702,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1704,
		time: 1703,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1705,
		time: 1704,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1706,
		time: 1705,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1707,
		time: 1706,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1708,
		time: 1707,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1709,
		time: 1708,
		velocity: 0,
		power: 0,
		road: 14272.8086111111,
		acceleration: 0
	},
	{
		id: 1710,
		time: 1709,
		velocity: 0,
		power: 259.673052037621,
		road: 14273.1482407407,
		acceleration: 0.679259259259259
	},
	{
		id: 1711,
		time: 1710,
		velocity: 2.03777777777778,
		power: 1379.91540472285,
		road: 14274.3625925926,
		acceleration: 1.07018518518518
	},
	{
		id: 1712,
		time: 1711,
		velocity: 3.21055555555556,
		power: 3872.52344486322,
		road: 14276.8627314815,
		acceleration: 1.50138888888889
	},
	{
		id: 1713,
		time: 1712,
		velocity: 4.50416666666667,
		power: 3550.81068053624,
		road: 14280.5527777778,
		acceleration: 0.878425925925927
	},
	{
		id: 1714,
		time: 1713,
		velocity: 4.67305555555556,
		power: 4934.10399602801,
		road: 14285.1742592593,
		acceleration: 0.984444444444444
	},
	{
		id: 1715,
		time: 1714,
		velocity: 6.16388888888889,
		power: 6633.06339536282,
		road: 14290.8319444444,
		acceleration: 1.08796296296296
	},
	{
		id: 1716,
		time: 1715,
		velocity: 7.76805555555556,
		power: 9270.12381006405,
		road: 14297.6693981481,
		acceleration: 1.27157407407407
	},
	{
		id: 1717,
		time: 1716,
		velocity: 8.48777777777778,
		power: 6762.72812224166,
		road: 14305.5144907407,
		acceleration: 0.743703703703702
	},
	{
		id: 1718,
		time: 1717,
		velocity: 8.395,
		power: 2901.29723494004,
		road: 14313.8314814815,
		acceleration: 0.200092592592595
	},
	{
		id: 1719,
		time: 1718,
		velocity: 8.36833333333333,
		power: 3198.34681685024,
		road: 14322.36125,
		acceleration: 0.225462962962961
	},
	{
		id: 1720,
		time: 1719,
		velocity: 9.16416666666667,
		power: 3121.00210552816,
		road: 14331.1059259259,
		acceleration: 0.204351851851852
	},
	{
		id: 1721,
		time: 1720,
		velocity: 9.00805555555556,
		power: 2426.92970097847,
		road: 14340.0098611111,
		acceleration: 0.114166666666668
	},
	{
		id: 1722,
		time: 1721,
		velocity: 8.71083333333333,
		power: 756.664697224454,
		road: 14348.9293055555,
		acceleration: -0.0831481481481475
	},
	{
		id: 1723,
		time: 1722,
		velocity: 8.91472222222222,
		power: -594.643937938489,
		road: 14357.6862037037,
		acceleration: -0.241944444444446
	},
	{
		id: 1724,
		time: 1723,
		velocity: 8.28222222222222,
		power: -7079.36167569394,
		road: 14365.7804166667,
		acceleration: -1.08342592592593
	},
	{
		id: 1725,
		time: 1724,
		velocity: 5.46055555555556,
		power: -10593.5486918268,
		road: 14372.4173611111,
		acceleration: -1.83111111111111
	},
	{
		id: 1726,
		time: 1725,
		velocity: 3.42138888888889,
		power: -10808.537936595,
		road: 14376.7583796296,
		acceleration: -2.76074074074074
	},
	{
		id: 1727,
		time: 1726,
		velocity: 0,
		power: -3286.75754298998,
		road: 14378.8089351852,
		acceleration: -1.82018518518519
	},
	{
		id: 1728,
		time: 1727,
		velocity: 0,
		power: -547.205934421702,
		road: 14379.3791666667,
		acceleration: -1.14046296296296
	},
	{
		id: 1729,
		time: 1728,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1730,
		time: 1729,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1731,
		time: 1730,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1732,
		time: 1731,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1733,
		time: 1732,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1734,
		time: 1733,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1735,
		time: 1734,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1736,
		time: 1735,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1737,
		time: 1736,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1738,
		time: 1737,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1739,
		time: 1738,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1740,
		time: 1739,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1741,
		time: 1740,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1742,
		time: 1741,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1743,
		time: 1742,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1744,
		time: 1743,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1745,
		time: 1744,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1746,
		time: 1745,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1747,
		time: 1746,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1748,
		time: 1747,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1749,
		time: 1748,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1750,
		time: 1749,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1751,
		time: 1750,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1752,
		time: 1751,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1753,
		time: 1752,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1754,
		time: 1753,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1755,
		time: 1754,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1756,
		time: 1755,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1757,
		time: 1756,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1758,
		time: 1757,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1759,
		time: 1758,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1760,
		time: 1759,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1761,
		time: 1760,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1762,
		time: 1761,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1763,
		time: 1762,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1764,
		time: 1763,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1765,
		time: 1764,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1766,
		time: 1765,
		velocity: 0,
		power: 0,
		road: 14379.3791666667,
		acceleration: 0
	},
	{
		id: 1767,
		time: 1766,
		velocity: 0,
		power: 172.542781614453,
		road: 14379.6506944444,
		acceleration: 0.543055555555556
	},
	{
		id: 1768,
		time: 1767,
		velocity: 1.62916666666667,
		power: 1841.84061066125,
		road: 14380.9043055555,
		acceleration: 1.42111111111111
	},
	{
		id: 1769,
		time: 1768,
		velocity: 4.26333333333333,
		power: 6454.50068523676,
		road: 14383.9269444444,
		acceleration: 2.11694444444444
	},
	{
		id: 1770,
		time: 1769,
		velocity: 6.35083333333333,
		power: 10389.4522325527,
		road: 14389.0119907407,
		acceleration: 2.00787037037037
	},
	{
		id: 1771,
		time: 1770,
		velocity: 7.65277777777778,
		power: 7996.25062714057,
		road: 14395.6574074074,
		acceleration: 1.11287037037037
	},
	{
		id: 1772,
		time: 1771,
		velocity: 7.60194444444444,
		power: 4252.37133021986,
		road: 14403.0810648148,
		acceleration: 0.443611111111111
	},
	{
		id: 1773,
		time: 1772,
		velocity: 7.68166666666667,
		power: 1018.85472894282,
		road: 14410.7165277778,
		acceleration: -0.0199999999999996
	},
	{
		id: 1774,
		time: 1773,
		velocity: 7.59277777777778,
		power: 1217.2557868537,
		road: 14418.3457407407,
		acceleration: 0.00750000000000028
	},
	{
		id: 1775,
		time: 1774,
		velocity: 7.62444444444444,
		power: 1941.47471714771,
		road: 14426.0311111111,
		acceleration: 0.104814814814813
	},
	{
		id: 1776,
		time: 1775,
		velocity: 7.99611111111111,
		power: 5378.25378675395,
		road: 14434.0399074074,
		acceleration: 0.542037037037038
	},
	{
		id: 1777,
		time: 1776,
		velocity: 9.21888888888889,
		power: 6216.50937667745,
		road: 14442.6159722222,
		acceleration: 0.592500000000001
	},
	{
		id: 1778,
		time: 1777,
		velocity: 9.40194444444444,
		power: 5406.64470664499,
		road: 14451.713287037,
		acceleration: 0.449999999999999
	},
	{
		id: 1779,
		time: 1778,
		velocity: 9.34611111111111,
		power: 179.543253823548,
		road: 14460.9580092593,
		acceleration: -0.155185185185186
	},
	{
		id: 1780,
		time: 1779,
		velocity: 8.75333333333333,
		power: -2978.08380577754,
		road: 14469.8634722222,
		acceleration: -0.523333333333333
	},
	{
		id: 1781,
		time: 1780,
		velocity: 7.83194444444444,
		power: -3141.57811634575,
		road: 14478.2265277778,
		acceleration: -0.561481481481481
	},
	{
		id: 1782,
		time: 1781,
		velocity: 7.66166666666667,
		power: -1790.35943539256,
		road: 14486.108287037,
		acceleration: -0.401111111111111
	},
	{
		id: 1783,
		time: 1782,
		velocity: 7.55,
		power: 105.29423051928,
		road: 14493.7167592593,
		acceleration: -0.145462962962965
	},
	{
		id: 1784,
		time: 1783,
		velocity: 7.39555555555556,
		power: -20.3660828182288,
		road: 14501.171712963,
		acceleration: -0.161574074074073
	},
	{
		id: 1785,
		time: 1784,
		velocity: 7.17694444444444,
		power: 179.468388659462,
		road: 14508.4800462963,
		acceleration: -0.131666666666667
	},
	{
		id: 1786,
		time: 1785,
		velocity: 7.155,
		power: 404.026685247837,
		road: 14515.6737962963,
		acceleration: -0.0975000000000001
	},
	{
		id: 1787,
		time: 1786,
		velocity: 7.10305555555556,
		power: 167.556318866916,
		road: 14522.7534259259,
		acceleration: -0.13074074074074
	},
	{
		id: 1788,
		time: 1787,
		velocity: 6.78472222222222,
		power: -2404.27450976783,
		road: 14529.5038888889,
		acceleration: -0.527592592592592
	},
	{
		id: 1789,
		time: 1788,
		velocity: 5.57222222222222,
		power: -3740.79165851058,
		road: 14535.593287037,
		acceleration: -0.794537037037037
	},
	{
		id: 1790,
		time: 1789,
		velocity: 4.71944444444444,
		power: -4059.92316134888,
		road: 14540.8040277778,
		acceleration: -0.962777777777778
	},
	{
		id: 1791,
		time: 1790,
		velocity: 3.89638888888889,
		power: -1909.36193275114,
		road: 14545.2374074074,
		acceleration: -0.591944444444444
	},
	{
		id: 1792,
		time: 1791,
		velocity: 3.79638888888889,
		power: -841.920429872992,
		road: 14549.1946759259,
		acceleration: -0.360277777777779
	},
	{
		id: 1793,
		time: 1792,
		velocity: 3.63861111111111,
		power: 3.07587961101435,
		road: 14552.9046759259,
		acceleration: -0.13425925925926
	},
	{
		id: 1794,
		time: 1793,
		velocity: 3.49361111111111,
		power: 355.077337695412,
		road: 14556.531712963,
		acceleration: -0.0316666666666663
	},
	{
		id: 1795,
		time: 1794,
		velocity: 3.70138888888889,
		power: 1539.91391914457,
		road: 14560.2909722222,
		acceleration: 0.296111111111111
	},
	{
		id: 1796,
		time: 1795,
		velocity: 4.52694444444444,
		power: 1831.7994832242,
		road: 14564.3665740741,
		acceleration: 0.336574074074075
	},
	{
		id: 1797,
		time: 1796,
		velocity: 4.50333333333333,
		power: 1682.61296159962,
		road: 14568.74375,
		acceleration: 0.266574074074074
	},
	{
		id: 1798,
		time: 1797,
		velocity: 4.50111111111111,
		power: 503.709302759947,
		road: 14573.24375,
		acceleration: -0.0209259259259253
	},
	{
		id: 1799,
		time: 1798,
		velocity: 4.46416666666667,
		power: 609.234767841314,
		road: 14577.7353240741,
		acceleration: 0.00407407407407323
	},
	{
		id: 1800,
		time: 1799,
		velocity: 4.51555555555556,
		power: 709.181490664109,
		road: 14582.2423611111,
		acceleration: 0.0268518518518519
	},
	{
		id: 1801,
		time: 1800,
		velocity: 4.58166666666667,
		power: 709.935601926604,
		road: 14586.775787037,
		acceleration: 0.0259259259259252
	},
	{
		id: 1802,
		time: 1801,
		velocity: 4.54194444444444,
		power: 652.054704145222,
		road: 14591.3280555555,
		acceleration: 0.0117592592592599
	},
	{
		id: 1803,
		time: 1802,
		velocity: 4.55083333333333,
		power: 792.332436142167,
		road: 14595.9076851852,
		acceleration: 0.042962962962962
	},
	{
		id: 1804,
		time: 1803,
		velocity: 4.71055555555555,
		power: 1829.84813993852,
		road: 14600.6422222222,
		acceleration: 0.266851851851853
	},
	{
		id: 1805,
		time: 1804,
		velocity: 5.3425,
		power: 2908.50486563882,
		road: 14605.7394444444,
		acceleration: 0.458518518518518
	},
	{
		id: 1806,
		time: 1805,
		velocity: 5.92638888888889,
		power: 2827.06906132633,
		road: 14611.2629166667,
		acceleration: 0.393981481481482
	},
	{
		id: 1807,
		time: 1806,
		velocity: 5.8925,
		power: 1686.28777443808,
		road: 14617.0631481481,
		acceleration: 0.159537037037037
	},
	{
		id: 1808,
		time: 1807,
		velocity: 5.82111111111111,
		power: -712.858887138412,
		road: 14622.8048611111,
		acceleration: -0.276574074074074
	},
	{
		id: 1809,
		time: 1808,
		velocity: 5.09666666666667,
		power: -2252.89705250995,
		road: 14628.1133333333,
		acceleration: -0.589907407407407
	},
	{
		id: 1810,
		time: 1809,
		velocity: 4.12277777777778,
		power: -2740.71869704553,
		road: 14632.7456481481,
		acceleration: -0.762407407407408
	},
	{
		id: 1811,
		time: 1810,
		velocity: 3.53388888888889,
		power: -2422.0427341373,
		road: 14636.5977314815,
		acceleration: -0.798055555555556
	},
	{
		id: 1812,
		time: 1811,
		velocity: 2.7025,
		power: -1901.46457048876,
		road: 14639.6569444444,
		acceleration: -0.787685185185186
	},
	{
		id: 1813,
		time: 1812,
		velocity: 1.75972222222222,
		power: -1606.04420043272,
		road: 14641.8756018518,
		acceleration: -0.893425925925926
	},
	{
		id: 1814,
		time: 1813,
		velocity: 0.853611111111111,
		power: -967.611021303139,
		road: 14643.1971296296,
		acceleration: -0.900833333333333
	},
	{
		id: 1815,
		time: 1814,
		velocity: 0,
		power: -251.260967286694,
		road: 14643.7749537037,
		acceleration: -0.586574074074074
	},
	{
		id: 1816,
		time: 1815,
		velocity: 0,
		power: -21.1615186322287,
		road: 14643.9172222222,
		acceleration: -0.284537037037037
	},
	{
		id: 1817,
		time: 1816,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1818,
		time: 1817,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1819,
		time: 1818,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1820,
		time: 1819,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1821,
		time: 1820,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1822,
		time: 1821,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1823,
		time: 1822,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1824,
		time: 1823,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1825,
		time: 1824,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1826,
		time: 1825,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1827,
		time: 1826,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1828,
		time: 1827,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1829,
		time: 1828,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1830,
		time: 1829,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1831,
		time: 1830,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1832,
		time: 1831,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1833,
		time: 1832,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1834,
		time: 1833,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1835,
		time: 1834,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1836,
		time: 1835,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1837,
		time: 1836,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1838,
		time: 1837,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1839,
		time: 1838,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1840,
		time: 1839,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1841,
		time: 1840,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1842,
		time: 1841,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1843,
		time: 1842,
		velocity: 0,
		power: 0,
		road: 14643.9172222222,
		acceleration: 0
	},
	{
		id: 1844,
		time: 1843,
		velocity: 0,
		power: 307.881525460075,
		road: 14644.2896296296,
		acceleration: 0.744814814814815
	},
	{
		id: 1845,
		time: 1844,
		velocity: 2.23444444444444,
		power: 2481.22469028902,
		road: 14645.8231018518,
		acceleration: 1.57731481481481
	},
	{
		id: 1846,
		time: 1845,
		velocity: 4.73194444444444,
		power: 5389.01547492695,
		road: 14648.9783333333,
		acceleration: 1.6662037037037
	},
	{
		id: 1847,
		time: 1846,
		velocity: 4.99861111111111,
		power: 5341.06672657489,
		road: 14653.5166203704,
		acceleration: 1.09990740740741
	},
	{
		id: 1848,
		time: 1847,
		velocity: 5.53416666666667,
		power: 3635.52529935516,
		road: 14658.8890740741,
		acceleration: 0.568425925925926
	},
	{
		id: 1849,
		time: 1848,
		velocity: 6.43722222222222,
		power: 6276.70633507673,
		road: 14665.0106944444,
		acceleration: 0.929907407407408
	},
	{
		id: 1850,
		time: 1849,
		velocity: 7.78833333333333,
		power: 7310.60952406699,
		road: 14672.0643055555,
		acceleration: 0.934074074074073
	},
	{
		id: 1851,
		time: 1850,
		velocity: 8.33638888888889,
		power: 8197.70389185692,
		road: 14680.0430555555,
		acceleration: 0.916203703703705
	},
	{
		id: 1852,
		time: 1851,
		velocity: 9.18583333333333,
		power: 7838.77916224005,
		road: 14688.8610648148,
		acceleration: 0.762314814814816
	},
	{
		id: 1853,
		time: 1852,
		velocity: 10.0752777777778,
		power: 8108.66111236305,
		road: 14698.4162037037,
		acceleration: 0.711944444444443
	},
	{
		id: 1854,
		time: 1853,
		velocity: 10.4722222222222,
		power: 5197.3346176815,
		road: 14708.5052314815,
		acceleration: 0.355833333333333
	},
	{
		id: 1855,
		time: 1854,
		velocity: 10.2533333333333,
		power: 1918.09279746061,
		road: 14718.776712963,
		acceleration: 0.00907407407407312
	},
	{
		id: 1856,
		time: 1855,
		velocity: 10.1025,
		power: 1480.58350417567,
		road: 14729.0350925926,
		acceleration: -0.0352777777777771
	},
	{
		id: 1857,
		time: 1856,
		velocity: 10.3663888888889,
		power: 4172.75057945421,
		road: 14739.3932407407,
		acceleration: 0.234814814814817
	},
	{
		id: 1858,
		time: 1857,
		velocity: 10.9577777777778,
		power: 6361.2583151847,
		road: 14750.0849537037,
		acceleration: 0.432314814814813
	},
	{
		id: 1859,
		time: 1858,
		velocity: 11.3994444444444,
		power: 6780.2581660981,
		road: 14761.2137037037,
		acceleration: 0.441759259259261
	},
	{
		id: 1860,
		time: 1859,
		velocity: 11.6916666666667,
		power: 6487.14722831913,
		road: 14772.756712963,
		acceleration: 0.386759259259259
	},
	{
		id: 1861,
		time: 1860,
		velocity: 12.1180555555556,
		power: 5391.25364300084,
		road: 14784.6277314815,
		acceleration: 0.269259259259259
	},
	{
		id: 1862,
		time: 1861,
		velocity: 12.2072222222222,
		power: 4493.57241486174,
		road: 14796.7231481481,
		acceleration: 0.179537037037036
	},
	{
		id: 1863,
		time: 1862,
		velocity: 12.2302777777778,
		power: 7445.56319111505,
		road: 14809.1161574074,
		acceleration: 0.415648148148147
	},
	{
		id: 1864,
		time: 1863,
		velocity: 13.365,
		power: 9616.39104294084,
		road: 14821.9975925926,
		acceleration: 0.561203703703706
	},
	{
		id: 1865,
		time: 1864,
		velocity: 13.8908333333333,
		power: 11316.5439727441,
		road: 14835.4840740741,
		acceleration: 0.648888888888886
	},
	{
		id: 1866,
		time: 1865,
		velocity: 14.1769444444444,
		power: 5882.03552834745,
		road: 14849.3980555555,
		acceleration: 0.206111111111113
	},
	{
		id: 1867,
		time: 1866,
		velocity: 13.9833333333333,
		power: 1892.89103865003,
		road: 14863.3673148148,
		acceleration: -0.0955555555555563
	},
	{
		id: 1868,
		time: 1867,
		velocity: 13.6041666666667,
		power: 393.515678913298,
		road: 14877.1861574074,
		acceleration: -0.205277777777777
	},
	{
		id: 1869,
		time: 1868,
		velocity: 13.5611111111111,
		power: 2762.49616019812,
		road: 14890.89125,
		acceleration: -0.0222222222222239
	},
	{
		id: 1870,
		time: 1869,
		velocity: 13.9166666666667,
		power: 6179.47830277178,
		road: 14904.702037037,
		acceleration: 0.233611111111111
	},
	{
		id: 1871,
		time: 1870,
		velocity: 14.305,
		power: 9082.27383481127,
		road: 14918.8456944444,
		acceleration: 0.432129629629632
	},
	{
		id: 1872,
		time: 1871,
		velocity: 14.8575,
		power: 10017.5746519185,
		road: 14933.4408796296,
		acceleration: 0.470925925925926
	},
	{
		id: 1873,
		time: 1872,
		velocity: 15.3294444444444,
		power: 10663.4889215097,
		road: 14948.5139351852,
		acceleration: 0.484814814814815
	},
	{
		id: 1874,
		time: 1873,
		velocity: 15.7594444444444,
		power: 7952.99294238698,
		road: 14963.9675925926,
		acceleration: 0.276388888888889
	},
	{
		id: 1875,
		time: 1874,
		velocity: 15.6866666666667,
		power: 3687.55036664563,
		road: 14979.5510648148,
		acceleration: -0.0167592592592598
	},
	{
		id: 1876,
		time: 1875,
		velocity: 15.2791666666667,
		power: -117.77603624104,
		road: 14994.99125,
		acceleration: -0.269814814814815
	},
	{
		id: 1877,
		time: 1876,
		velocity: 14.95,
		power: -882.918754491203,
		road: 15010.1376388889,
		acceleration: -0.317777777777778
	},
	{
		id: 1878,
		time: 1877,
		velocity: 14.7333333333333,
		power: 694.627841511066,
		road: 15025.0233796296,
		acceleration: -0.203518518518518
	},
	{
		id: 1879,
		time: 1878,
		velocity: 14.6686111111111,
		power: 1681.43121003711,
		road: 15039.7423611111,
		acceleration: -0.130000000000001
	},
	{
		id: 1880,
		time: 1879,
		velocity: 14.56,
		power: 2906.21692517393,
		road: 15054.3762037037,
		acceleration: -0.0402777777777761
	},
	{
		id: 1881,
		time: 1880,
		velocity: 14.6125,
		power: 1657.96510116024,
		road: 15068.9261574074,
		acceleration: -0.127500000000001
	},
	{
		id: 1882,
		time: 1881,
		velocity: 14.2861111111111,
		power: 1046.06941989036,
		road: 15083.3281944444,
		acceleration: -0.168333333333333
	},
	{
		id: 1883,
		time: 1882,
		velocity: 14.055,
		power: -1344.65931234124,
		road: 15097.4761574074,
		acceleration: -0.339814814814813
	},
	{
		id: 1884,
		time: 1883,
		velocity: 13.5930555555556,
		power: -414.384085445842,
		road: 15111.320787037,
		acceleration: -0.266851851851854
	},
	{
		id: 1885,
		time: 1884,
		velocity: 13.4855555555556,
		power: 2809.67452332959,
		road: 15125.0227314815,
		acceleration: -0.018518518518519
	},
	{
		id: 1886,
		time: 1885,
		velocity: 13.9994444444444,
		power: 8363.69815661411,
		road: 15138.9125925926,
		acceleration: 0.394351851851853
	},
	{
		id: 1887,
		time: 1886,
		velocity: 14.7761111111111,
		power: 11019.2924987691,
		road: 15153.279212963,
		acceleration: 0.559166666666666
	},
	{
		id: 1888,
		time: 1887,
		velocity: 15.1630555555556,
		power: 12092.9150143501,
		road: 15168.2222222222,
		acceleration: 0.593611111111111
	},
	{
		id: 1889,
		time: 1888,
		velocity: 15.7802777777778,
		power: 10249.9232835648,
		road: 15183.6779166667,
		acceleration: 0.431759259259259
	},
	{
		id: 1890,
		time: 1889,
		velocity: 16.0713888888889,
		power: 10889.6587695509,
		road: 15199.5728703704,
		acceleration: 0.44675925925926
	},
	{
		id: 1891,
		time: 1890,
		velocity: 16.5033333333333,
		power: 7421.04657911802,
		road: 15215.7925925926,
		acceleration: 0.202777777777779
	},
	{
		id: 1892,
		time: 1891,
		velocity: 16.3886111111111,
		power: 5700.54285664347,
		road: 15232.1566666667,
		acceleration: 0.0859259259259275
	},
	{
		id: 1893,
		time: 1892,
		velocity: 16.3291666666667,
		power: 5041.71071662349,
		road: 15248.5843981481,
		acceleration: 0.0413888888888856
	},
	{
		id: 1894,
		time: 1893,
		velocity: 16.6275,
		power: 9015.03205712538,
		road: 15265.1756018518,
		acceleration: 0.285555555555558
	},
	{
		id: 1895,
		time: 1894,
		velocity: 17.2452777777778,
		power: 14508.0318156866,
		road: 15282.2092592593,
		acceleration: 0.59935185185185
	},
	{
		id: 1896,
		time: 1895,
		velocity: 18.1272222222222,
		power: 16448.4784942198,
		road: 15299.8773148148,
		acceleration: 0.669444444444444
	},
	{
		id: 1897,
		time: 1896,
		velocity: 18.6358333333333,
		power: 17989.788475817,
		road: 15318.233287037,
		acceleration: 0.706388888888888
	},
	{
		id: 1898,
		time: 1897,
		velocity: 19.3644444444444,
		power: 15461.7328279205,
		road: 15337.2029166667,
		acceleration: 0.52092592592593
	},
	{
		id: 1899,
		time: 1898,
		velocity: 19.69,
		power: 10826.9972463607,
		road: 15356.5558333333,
		acceleration: 0.245648148148149
	},
	{
		id: 1900,
		time: 1899,
		velocity: 19.3727777777778,
		power: 4047.08536202711,
		road: 15375.9705092593,
		acceleration: -0.122129629629633
	},
	{
		id: 1901,
		time: 1900,
		velocity: 18.9980555555556,
		power: 535.022755817706,
		road: 15395.1711111111,
		acceleration: -0.30601851851852
	},
	{
		id: 1902,
		time: 1901,
		velocity: 18.7719444444444,
		power: 25.246278097517,
		road: 15414.0552314815,
		acceleration: -0.326944444444443
	},
	{
		id: 1903,
		time: 1902,
		velocity: 18.3919444444444,
		power: 1657.23482852438,
		road: 15432.6611574074,
		acceleration: -0.229444444444443
	},
	{
		id: 1904,
		time: 1903,
		velocity: 18.3097222222222,
		power: 1504.83703656336,
		road: 15451.0362962963,
		acceleration: -0.232129629629629
	},
	{
		id: 1905,
		time: 1904,
		velocity: 18.0755555555556,
		power: 2698.12576913164,
		road: 15469.2160648148,
		acceleration: -0.15861111111111
	},
	{
		id: 1906,
		time: 1905,
		velocity: 17.9161111111111,
		power: 1414.07839640799,
		road: 15487.2027314815,
		acceleration: -0.227592592592597
	},
	{
		id: 1907,
		time: 1906,
		velocity: 17.6269444444444,
		power: 1901.76553879045,
		road: 15504.97875,
		acceleration: -0.193703703703704
	},
	{
		id: 1908,
		time: 1907,
		velocity: 17.4944444444444,
		power: 2432.08813592677,
		road: 15522.5791203704,
		acceleration: -0.157592592592593
	},
	{
		id: 1909,
		time: 1908,
		velocity: 17.4433333333333,
		power: 3419.32320201914,
		road: 15540.0531944444,
		acceleration: -0.0949999999999989
	},
	{
		id: 1910,
		time: 1909,
		velocity: 17.3419444444444,
		power: 4871.51622020892,
		road: 15557.4766203704,
		acceleration: -0.00629629629629846
	},
	{
		id: 1911,
		time: 1910,
		velocity: 17.4755555555556,
		power: 6297.85769147136,
		road: 15574.935787037,
		acceleration: 0.0777777777777828
	},
	{
		id: 1912,
		time: 1911,
		velocity: 17.6766666666667,
		power: 7488.09412670417,
		road: 15592.5058333333,
		acceleration: 0.143981481481482
	},
	{
		id: 1913,
		time: 1912,
		velocity: 17.7738888888889,
		power: 8304.09294810253,
		road: 15610.2401851852,
		acceleration: 0.184629629629629
	},
	{
		id: 1914,
		time: 1913,
		velocity: 18.0294444444444,
		power: 9137.00247104772,
		road: 15628.1785648148,
		acceleration: 0.223425925925927
	},
	{
		id: 1915,
		time: 1914,
		velocity: 18.3469444444444,
		power: 7982.0714971186,
		road: 15646.3024537037,
		acceleration: 0.147592592592595
	},
	{
		id: 1916,
		time: 1915,
		velocity: 18.2166666666667,
		power: 6335.73875599109,
		road: 15664.5244907407,
		acceleration: 0.0487037037036977
	},
	{
		id: 1917,
		time: 1916,
		velocity: 18.1755555555556,
		power: 4135.75174899843,
		road: 15682.7324074074,
		acceleration: -0.0769444444444396
	},
	{
		id: 1918,
		time: 1917,
		velocity: 18.1161111111111,
		power: 3578.19680646205,
		road: 15700.8488425926,
		acceleration: -0.106018518518521
	},
	{
		id: 1919,
		time: 1918,
		velocity: 17.8986111111111,
		power: 3064.63257218159,
		road: 15718.84625,
		acceleration: -0.132037037037037
	},
	{
		id: 1920,
		time: 1919,
		velocity: 17.7794444444444,
		power: 4345.91958989492,
		road: 15736.7504166667,
		acceleration: -0.0544444444444459
	},
	{
		id: 1921,
		time: 1920,
		velocity: 17.9527777777778,
		power: 6676.79124059038,
		road: 15754.6678703704,
		acceleration: 0.081018518518519
	},
	{
		id: 1922,
		time: 1921,
		velocity: 18.1416666666667,
		power: 7583.58635070117,
		road: 15772.6904166667,
		acceleration: 0.129166666666663
	},
	{
		id: 1923,
		time: 1922,
		velocity: 18.1669444444444,
		power: 6621.5389487358,
		road: 15790.8121759259,
		acceleration: 0.0692592592592618
	},
	{
		id: 1924,
		time: 1923,
		velocity: 18.1605555555556,
		power: 8054.0599486607,
		road: 15809.0419907407,
		acceleration: 0.146851851851849
	},
	{
		id: 1925,
		time: 1924,
		velocity: 18.5822222222222,
		power: 7393.71290682198,
		road: 15827.3969907407,
		acceleration: 0.10351851851852
	},
	{
		id: 1926,
		time: 1925,
		velocity: 18.4775,
		power: 7034.194457888,
		road: 15845.8433333333,
		acceleration: 0.0791666666666693
	},
	{
		id: 1927,
		time: 1926,
		velocity: 18.3980555555556,
		power: 4897.15617148678,
		road: 15864.3080092593,
		acceleration: -0.0425000000000004
	},
	{
		id: 1928,
		time: 1927,
		velocity: 18.4547222222222,
		power: 4259.0716318306,
		road: 15882.7131481481,
		acceleration: -0.0765740740740775
	},
	{
		id: 1929,
		time: 1928,
		velocity: 18.2477777777778,
		power: 2468.5796665109,
		road: 15900.9926851852,
		acceleration: -0.174629629629631
	},
	{
		id: 1930,
		time: 1929,
		velocity: 17.8741666666667,
		power: -203.132113108889,
		road: 15919.0237037037,
		acceleration: -0.322407407407404
	},
	{
		id: 1931,
		time: 1930,
		velocity: 17.4875,
		power: -2450.73474918897,
		road: 15936.6694907407,
		acceleration: -0.448055555555552
	},
	{
		id: 1932,
		time: 1931,
		velocity: 16.9036111111111,
		power: -5495.93110587212,
		road: 15953.7771296296,
		acceleration: -0.62824074074074
	},
	{
		id: 1933,
		time: 1932,
		velocity: 15.9894444444444,
		power: -8426.79090229854,
		road: 15970.1623148148,
		acceleration: -0.816666666666668
	},
	{
		id: 1934,
		time: 1933,
		velocity: 15.0375,
		power: -8794.23163107613,
		road: 15985.7113888889,
		acceleration: -0.855555555555558
	},
	{
		id: 1935,
		time: 1934,
		velocity: 14.3369444444444,
		power: -4369.66826872614,
		road: 16000.5527777778,
		acceleration: -0.559814814814814
	},
	{
		id: 1936,
		time: 1935,
		velocity: 14.31,
		power: 2687.74145551033,
		road: 16015.0877777778,
		acceleration: -0.0529629629629618
	},
	{
		id: 1937,
		time: 1936,
		velocity: 14.8786111111111,
		power: 7804.74560994453,
		road: 16029.7505555556,
		acceleration: 0.308518518518515
	},
	{
		id: 1938,
		time: 1937,
		velocity: 15.2625,
		power: 11137.0263916489,
		road: 16044.8263425926,
		acceleration: 0.517500000000004
	},
	{
		id: 1939,
		time: 1938,
		velocity: 15.8625,
		power: 9758.00947577121,
		road: 16060.3578240741,
		acceleration: 0.393888888888887
	},
	{
		id: 1940,
		time: 1939,
		velocity: 16.0602777777778,
		power: 9938.28989153958,
		road: 16076.2775925926,
		acceleration: 0.382685185185187
	},
	{
		id: 1941,
		time: 1940,
		velocity: 16.4105555555556,
		power: 5780.48903134031,
		road: 16092.4384259259,
		acceleration: 0.099444444444444
	},
	{
		id: 1942,
		time: 1941,
		velocity: 16.1608333333333,
		power: 4654.03380927906,
		road: 16108.6611111111,
		acceleration: 0.0242592592592601
	},
	{
		id: 1943,
		time: 1942,
		velocity: 16.1330555555556,
		power: 10928.3992644291,
		road: 16125.1035185185,
		acceleration: 0.41518518518518
	},
	{
		id: 1944,
		time: 1943,
		velocity: 17.6561111111111,
		power: 9791.77191446711,
		road: 16141.9146296296,
		acceleration: 0.322222222222223
	},
	{
		id: 1945,
		time: 1944,
		velocity: 17.1275,
		power: 7440.87982805078,
		road: 16158.969212963,
		acceleration: 0.164722222222224
	},
	{
		id: 1946,
		time: 1945,
		velocity: 16.6272222222222,
		power: -2820.65675729763,
		road: 16175.8746759259,
		acceleration: -0.462962962962962
	},
	{
		id: 1947,
		time: 1946,
		velocity: 16.2672222222222,
		power: -2146.28283485072,
		road: 16192.3403703704,
		acceleration: -0.416574074074074
	},
	{
		id: 1948,
		time: 1947,
		velocity: 15.8777777777778,
		power: -1340.73843555513,
		road: 16208.4175925926,
		acceleration: -0.360370370370369
	},
	{
		id: 1949,
		time: 1948,
		velocity: 15.5461111111111,
		power: -1187.57850818958,
		road: 16224.141712963,
		acceleration: -0.345833333333337
	},
	{
		id: 1950,
		time: 1949,
		velocity: 15.2297222222222,
		power: -1065.4050318615,
		road: 16239.52625,
		acceleration: -0.33333333333333
	},
	{
		id: 1951,
		time: 1950,
		velocity: 14.8777777777778,
		power: -1250.16990764321,
		road: 16254.5731018518,
		acceleration: -0.342037037037038
	},
	{
		id: 1952,
		time: 1951,
		velocity: 14.52,
		power: -2498.60022567713,
		road: 16269.2354166667,
		acceleration: -0.427037037037037
	},
	{
		id: 1953,
		time: 1952,
		velocity: 13.9486111111111,
		power: -3089.96727411831,
		road: 16283.4497222222,
		acceleration: -0.46898148148148
	},
	{
		id: 1954,
		time: 1953,
		velocity: 13.4708333333333,
		power: -3806.28527564509,
		road: 16297.1674537037,
		acceleration: -0.524166666666668
	},
	{
		id: 1955,
		time: 1954,
		velocity: 12.9475,
		power: -5032.26543355048,
		road: 16310.3100925926,
		acceleration: -0.626018518518519
	},
	{
		id: 1956,
		time: 1955,
		velocity: 12.0705555555556,
		power: -5706.11100946764,
		road: 16322.7925,
		acceleration: -0.694444444444445
	},
	{
		id: 1957,
		time: 1956,
		velocity: 11.3875,
		power: -4705.05535774582,
		road: 16334.6159259259,
		acceleration: -0.623518518518516
	},
	{
		id: 1958,
		time: 1957,
		velocity: 11.0769444444444,
		power: -957.414034838248,
		road: 16345.983287037,
		acceleration: -0.288611111111113
	},
	{
		id: 1959,
		time: 1958,
		velocity: 11.2047222222222,
		power: 2698.07633454505,
		road: 16357.2327314815,
		acceleration: 0.0527777777777771
	},
	{
		id: 1960,
		time: 1959,
		velocity: 11.5458333333333,
		power: 2827.76939026615,
		road: 16368.5399537037,
		acceleration: 0.0627777777777787
	},
	{
		id: 1961,
		time: 1960,
		velocity: 11.2652777777778,
		power: 945.61991814712,
		road: 16379.8229166667,
		acceleration: -0.111296296296299
	},
	{
		id: 1962,
		time: 1961,
		velocity: 10.8708333333333,
		power: -1587.33143981399,
		road: 16390.8767592593,
		acceleration: -0.346944444444443
	},
	{
		id: 1963,
		time: 1962,
		velocity: 10.505,
		power: -2903.97914318154,
		road: 16401.5183333333,
		acceleration: -0.477592592592593
	},
	{
		id: 1964,
		time: 1963,
		velocity: 9.8325,
		power: -3175.00496910086,
		road: 16411.6641666667,
		acceleration: -0.513888888888888
	},
	{
		id: 1965,
		time: 1964,
		velocity: 9.32916666666667,
		power: -2652.03882295842,
		road: 16421.3188888889,
		acceleration: -0.468333333333334
	},
	{
		id: 1966,
		time: 1965,
		velocity: 9.1,
		power: -5002.3068690089,
		road: 16430.3622222222,
		acceleration: -0.754444444444443
	},
	{
		id: 1967,
		time: 1966,
		velocity: 7.56916666666667,
		power: -7456.73492432704,
		road: 16438.4624537037,
		acceleration: -1.13175925925926
	},
	{
		id: 1968,
		time: 1967,
		velocity: 5.93388888888889,
		power: -7948.48645080115,
		road: 16445.3094907407,
		acceleration: -1.37462962962963
	},
	{
		id: 1969,
		time: 1968,
		velocity: 4.97611111111111,
		power: -4381.08516990248,
		road: 16450.9906944444,
		acceleration: -0.957037037037036
	},
	{
		id: 1970,
		time: 1969,
		velocity: 4.69805555555556,
		power: -1610.1484242199,
		road: 16455.9519444444,
		acceleration: -0.48287037037037
	},
	{
		id: 1971,
		time: 1970,
		velocity: 4.48527777777778,
		power: 1671.82717509604,
		road: 16460.7836111111,
		acceleration: 0.223703703703703
	},
	{
		id: 1972,
		time: 1971,
		velocity: 5.64722222222222,
		power: 3269.57957152579,
		road: 16465.9864814815,
		acceleration: 0.518703703703705
	},
	{
		id: 1973,
		time: 1972,
		velocity: 6.25416666666667,
		power: 3942.68054655433,
		road: 16471.7363888889,
		acceleration: 0.575370370370369
	},
	{
		id: 1974,
		time: 1973,
		velocity: 6.21138888888889,
		power: 1067.13025010901,
		road: 16477.7926388889,
		acceleration: 0.0373148148148159
	},
	{
		id: 1975,
		time: 1974,
		velocity: 5.75916666666667,
		power: -434.568787400765,
		road: 16483.7555092593,
		acceleration: -0.224074074074074
	},
	{
		id: 1976,
		time: 1975,
		velocity: 5.58194444444444,
		power: 696.035302212258,
		road: 16489.5957407407,
		acceleration: -0.0212037037037049
	},
	{
		id: 1977,
		time: 1976,
		velocity: 6.14777777777778,
		power: 3003.15163679956,
		road: 16495.6139351852,
		acceleration: 0.377129629629631
	},
	{
		id: 1978,
		time: 1977,
		velocity: 6.89055555555556,
		power: 4429.91990938377,
		road: 16502.1040277778,
		acceleration: 0.566666666666665
	},
	{
		id: 1979,
		time: 1978,
		velocity: 7.28194444444444,
		power: 3718.36373427521,
		road: 16509.0802314815,
		acceleration: 0.405555555555557
	},
	{
		id: 1980,
		time: 1979,
		velocity: 7.36444444444444,
		power: 1404.3524910665,
		road: 16516.2833796296,
		acceleration: 0.0483333333333329
	},
	{
		id: 1981,
		time: 1980,
		velocity: 7.03555555555555,
		power: -197.623881319074,
		road: 16523.4181018519,
		acceleration: -0.185185185185184
	},
	{
		id: 1982,
		time: 1981,
		velocity: 6.72638888888889,
		power: -275.099792895959,
		road: 16530.3621296296,
		acceleration: -0.196203703703705
	},
	{
		id: 1983,
		time: 1982,
		velocity: 6.77583333333333,
		power: -36.1655953190645,
		road: 16537.1286574074,
		acceleration: -0.158796296296296
	},
	{
		id: 1984,
		time: 1983,
		velocity: 6.55916666666667,
		power: 543.755973164294,
		road: 16543.7825925926,
		acceleration: -0.0663888888888895
	},
	{
		id: 1985,
		time: 1984,
		velocity: 6.52722222222222,
		power: 3917.43466003938,
		road: 16550.6273148148,
		acceleration: 0.447962962962963
	},
	{
		id: 1986,
		time: 1985,
		velocity: 8.11972222222222,
		power: 6494.04395478248,
		road: 16558.0749074074,
		acceleration: 0.757777777777777
	},
	{
		id: 1987,
		time: 1986,
		velocity: 8.8325,
		power: 10055.6728040701,
		road: 16566.4486111111,
		acceleration: 1.09444444444445
	},
	{
		id: 1988,
		time: 1987,
		velocity: 9.81055555555555,
		power: 6755.53324980928,
		road: 16575.6666203704,
		acceleration: 0.594166666666666
	},
	{
		id: 1989,
		time: 1988,
		velocity: 9.90222222222222,
		power: 6190.1793462029,
		road: 16585.4241203704,
		acceleration: 0.484814814814813
	},
	{
		id: 1990,
		time: 1989,
		velocity: 10.2869444444444,
		power: 4610.74175482108,
		road: 16595.5697685185,
		acceleration: 0.291481481481483
	},
	{
		id: 1991,
		time: 1990,
		velocity: 10.685,
		power: 5406.97066486375,
		road: 16606.0375462963,
		acceleration: 0.352777777777776
	},
	{
		id: 1992,
		time: 1991,
		velocity: 10.9605555555556,
		power: 5509.96482665913,
		road: 16616.8522685185,
		acceleration: 0.341111111111111
	},
	{
		id: 1993,
		time: 1992,
		velocity: 11.3102777777778,
		power: 5451.02134740193,
		road: 16627.9953703704,
		acceleration: 0.315648148148147
	},
	{
		id: 1994,
		time: 1993,
		velocity: 11.6319444444444,
		power: 5494.31942412417,
		road: 16639.4471759259,
		acceleration: 0.301759259259262
	},
	{
		id: 1995,
		time: 1994,
		velocity: 11.8658333333333,
		power: 5611.78920824979,
		road: 16651.1975925926,
		acceleration: 0.295462962962961
	},
	{
		id: 1996,
		time: 1995,
		velocity: 12.1966666666667,
		power: 4767.44368464071,
		road: 16663.1996296296,
		acceleration: 0.207777777777778
	},
	{
		id: 1997,
		time: 1996,
		velocity: 12.2552777777778,
		power: 3647.23561782537,
		road: 16675.3574074074,
		acceleration: 0.103703703703705
	},
	{
		id: 1998,
		time: 1997,
		velocity: 12.1769444444444,
		power: 1074.55157673925,
		road: 16687.5080555556,
		acceleration: -0.117962962962963
	},
	{
		id: 1999,
		time: 1998,
		velocity: 11.8427777777778,
		power: -177.626107712311,
		road: 16699.4877777778,
		acceleration: -0.22388888888889
	},
	{
		id: 2000,
		time: 1999,
		velocity: 11.5836111111111,
		power: -1381.43776636891,
		road: 16711.1913425926,
		acceleration: -0.328425925925924
	},
	{
		id: 2001,
		time: 2000,
		velocity: 11.1916666666667,
		power: -428.287931502953,
		road: 16722.6105555556,
		acceleration: -0.240277777777777
	},
	{
		id: 2002,
		time: 2001,
		velocity: 11.1219444444444,
		power: -1460.49908437252,
		road: 16733.7421759259,
		acceleration: -0.33490740740741
	},
	{
		id: 2003,
		time: 2002,
		velocity: 10.5788888888889,
		power: -706.85998583078,
		road: 16744.5753240741,
		acceleration: -0.262037037037036
	},
	{
		id: 2004,
		time: 2003,
		velocity: 10.4055555555556,
		power: -1689.69146469682,
		road: 16755.098287037,
		acceleration: -0.358333333333333
	},
	{
		id: 2005,
		time: 2004,
		velocity: 10.0469444444444,
		power: -1155.00165252224,
		road: 16765.2896296296,
		acceleration: -0.304907407407409
	},
	{
		id: 2006,
		time: 2005,
		velocity: 9.66416666666666,
		power: -1518.97205113886,
		road: 16775.1565740741,
		acceleration: -0.343888888888889
	},
	{
		id: 2007,
		time: 2006,
		velocity: 9.37388888888889,
		power: -2216.65844503002,
		road: 16784.6398148148,
		acceleration: -0.423518518518518
	},
	{
		id: 2008,
		time: 2007,
		velocity: 8.77638888888889,
		power: -541.56972045553,
		road: 16793.7929166667,
		acceleration: -0.236759259259257
	},
	{
		id: 2009,
		time: 2008,
		velocity: 8.95388888888889,
		power: 486.324280565782,
		road: 16802.7696759259,
		acceleration: -0.115925925925927
	},
	{
		id: 2010,
		time: 2009,
		velocity: 9.02611111111111,
		power: 2742.70017377095,
		road: 16811.7621296296,
		acceleration: 0.147314814814816
	},
	{
		id: 2011,
		time: 2010,
		velocity: 9.21833333333333,
		power: 2919.79016818841,
		road: 16820.9085648148,
		acceleration: 0.160648148148148
	},
	{
		id: 2012,
		time: 2011,
		velocity: 9.43583333333333,
		power: 4603.24109772046,
		road: 16830.3039351852,
		acceleration: 0.337222222222222
	},
	{
		id: 2013,
		time: 2012,
		velocity: 10.0377777777778,
		power: 3508.42307091887,
		road: 16839.9684259259,
		acceleration: 0.201018518518518
	},
	{
		id: 2014,
		time: 2013,
		velocity: 9.82138888888889,
		power: 3059.95144024917,
		road: 16849.8056944444,
		acceleration: 0.144537037037038
	},
	{
		id: 2015,
		time: 2014,
		velocity: 9.86944444444445,
		power: 1267.95217376609,
		road: 16859.6912962963,
		acceleration: -0.0478703703703722
	},
	{
		id: 2016,
		time: 2015,
		velocity: 9.89416666666667,
		power: 1476.68635823,
		road: 16869.5406018518,
		acceleration: -0.0247222222222199
	},
	{
		id: 2017,
		time: 2016,
		velocity: 9.74722222222222,
		power: 2008.65737570322,
		road: 16879.3934722222,
		acceleration: 0.0318518518518509
	},
	{
		id: 2018,
		time: 2017,
		velocity: 9.965,
		power: 2594.64386736387,
		road: 16889.3081944444,
		acceleration: 0.0918518518518514
	},
	{
		id: 2019,
		time: 2018,
		velocity: 10.1697222222222,
		power: 4843.98954984961,
		road: 16899.4274537037,
		acceleration: 0.31722222222222
	},
	{
		id: 2020,
		time: 2019,
		velocity: 10.6988888888889,
		power: 4239.1924114274,
		road: 16909.8250462963,
		acceleration: 0.239444444444446
	},
	{
		id: 2021,
		time: 2020,
		velocity: 10.6833333333333,
		power: 4526.30019358462,
		road: 16920.4697685185,
		acceleration: 0.254814814814814
	},
	{
		id: 2022,
		time: 2021,
		velocity: 10.9341666666667,
		power: 3349.10931039993,
		road: 16931.3071759259,
		acceleration: 0.130555555555556
	},
	{
		id: 2023,
		time: 2022,
		velocity: 11.0905555555556,
		power: 4034.9826565729,
		road: 16942.3044907407,
		acceleration: 0.189259259259257
	},
	{
		id: 2024,
		time: 2023,
		velocity: 11.2511111111111,
		power: 3295.83716502456,
		road: 16953.4527314815,
		acceleration: 0.112592592592593
	},
	{
		id: 2025,
		time: 2024,
		velocity: 11.2719444444444,
		power: 3838.58587003775,
		road: 16964.7360648148,
		acceleration: 0.157592592592591
	},
	{
		id: 2026,
		time: 2025,
		velocity: 11.5633333333333,
		power: 3716.47456053201,
		road: 16976.1681018519,
		acceleration: 0.139814814814818
	},
	{
		id: 2027,
		time: 2026,
		velocity: 11.6705555555556,
		power: 3889.80540030165,
		road: 16987.7447222222,
		acceleration: 0.149351851851851
	},
	{
		id: 2028,
		time: 2027,
		velocity: 11.72,
		power: 3168.91525364464,
		road: 16999.4358796296,
		acceleration: 0.0797222222222231
	},
	{
		id: 2029,
		time: 2028,
		velocity: 11.8025,
		power: 2099.25593724343,
		road: 17011.1583333333,
		acceleration: -0.0171296296296308
	},
	{
		id: 2030,
		time: 2029,
		velocity: 11.6191666666667,
		power: 2868.89122574298,
		road: 17022.8977777778,
		acceleration: 0.051111111111112
	},
	{
		id: 2031,
		time: 2030,
		velocity: 11.8733333333333,
		power: 781.345118629616,
		road: 17034.5955092593,
		acceleration: -0.134537037037036
	},
	{
		id: 2032,
		time: 2031,
		velocity: 11.3988888888889,
		power: 412.125084367638,
		road: 17046.1433796296,
		acceleration: -0.165185185185186
	},
	{
		id: 2033,
		time: 2032,
		velocity: 11.1236111111111,
		power: -295.841650767838,
		road: 17057.4949537037,
		acceleration: -0.227407407407407
	},
	{
		id: 2034,
		time: 2033,
		velocity: 11.1911111111111,
		power: 1796.25657344154,
		road: 17068.7175462963,
		acceleration: -0.0305555555555568
	},
	{
		id: 2035,
		time: 2034,
		velocity: 11.3072222222222,
		power: 3283.95941864286,
		road: 17079.9783333333,
		acceleration: 0.106944444444444
	},
	{
		id: 2036,
		time: 2035,
		velocity: 11.4444444444444,
		power: 3288.18411955504,
		road: 17091.3441666667,
		acceleration: 0.103148148148149
	},
	{
		id: 2037,
		time: 2036,
		velocity: 11.5005555555556,
		power: 2739.25382005135,
		road: 17102.7864814815,
		acceleration: 0.0498148148148125
	},
	{
		id: 2038,
		time: 2037,
		velocity: 11.4566666666667,
		power: 3200.9957128805,
		road: 17114.2984259259,
		acceleration: 0.089444444444446
	},
	{
		id: 2039,
		time: 2038,
		velocity: 11.7127777777778,
		power: 3257.73371731458,
		road: 17125.9006481482,
		acceleration: 0.0911111111111094
	},
	{
		id: 2040,
		time: 2039,
		velocity: 11.7738888888889,
		power: 3082.28903871057,
		road: 17137.584537037,
		acceleration: 0.0722222222222229
	},
	{
		id: 2041,
		time: 2040,
		velocity: 11.6733333333333,
		power: 1581.40118522514,
		road: 17149.2732407407,
		acceleration: -0.0625925925925923
	},
	{
		id: 2042,
		time: 2041,
		velocity: 11.525,
		power: 8.57162464891427,
		road: 17160.8296759259,
		acceleration: -0.201944444444445
	},
	{
		id: 2043,
		time: 2042,
		velocity: 11.1680555555556,
		power: 482.543145279688,
		road: 17172.2071759259,
		acceleration: -0.155925925925926
	},
	{
		id: 2044,
		time: 2043,
		velocity: 11.2055555555556,
		power: 586.525311550289,
		road: 17183.4348611111,
		acceleration: -0.143703703703702
	},
	{
		id: 2045,
		time: 2044,
		velocity: 11.0938888888889,
		power: 2681.81982025871,
		road: 17194.6175,
		acceleration: 0.0536111111111097
	},
	{
		id: 2046,
		time: 2045,
		velocity: 11.3288888888889,
		power: 2788.87285987903,
		road: 17205.8577314815,
		acceleration: 0.0615740740740751
	},
	{
		id: 2047,
		time: 2046,
		velocity: 11.3902777777778,
		power: 4846.47190043838,
		road: 17217.2514814815,
		acceleration: 0.245462962962964
	},
	{
		id: 2048,
		time: 2047,
		velocity: 11.8302777777778,
		power: 3559.42357147384,
		road: 17228.8276851852,
		acceleration: 0.119444444444442
	},
	{
		id: 2049,
		time: 2048,
		velocity: 11.6872222222222,
		power: 3889.28357150718,
		road: 17240.5354166667,
		acceleration: 0.143611111111111
	},
	{
		id: 2050,
		time: 2049,
		velocity: 11.8211111111111,
		power: 3265.63322189952,
		road: 17252.3566666667,
		acceleration: 0.0834259259259262
	},
	{
		id: 2051,
		time: 2050,
		velocity: 12.0805555555556,
		power: 3769.44305533116,
		road: 17264.2815277778,
		acceleration: 0.123796296296296
	},
	{
		id: 2052,
		time: 2051,
		velocity: 12.0586111111111,
		power: 2518.72477139506,
		road: 17276.2741203704,
		acceleration: 0.0116666666666685
	},
	{
		id: 2053,
		time: 2052,
		velocity: 11.8561111111111,
		power: 215.651527701899,
		road: 17288.1783796296,
		acceleration: -0.188333333333333
	},
	{
		id: 2054,
		time: 2053,
		velocity: 11.5155555555556,
		power: -626.151727102773,
		road: 17299.8581944444,
		acceleration: -0.260555555555559
	},
	{
		id: 2055,
		time: 2054,
		velocity: 11.2769444444444,
		power: 497.785032389925,
		road: 17311.3296759259,
		acceleration: -0.156111111111109
	},
	{
		id: 2056,
		time: 2055,
		velocity: 11.3877777777778,
		power: 1964.11072248344,
		road: 17322.7133333333,
		acceleration: -0.0195370370370362
	},
	{
		id: 2057,
		time: 2056,
		velocity: 11.4569444444444,
		power: 3732.30067647582,
		road: 17334.1575925926,
		acceleration: 0.140740740740739
	},
	{
		id: 2058,
		time: 2057,
		velocity: 11.6991666666667,
		power: 2209.64383478542,
		road: 17345.6717592593,
		acceleration: -0.000925925925926663
	},
	{
		id: 2059,
		time: 2058,
		velocity: 11.385,
		power: 2063.88361448482,
		road: 17357.1784722222,
		acceleration: -0.0139814814814798
	},
	{
		id: 2060,
		time: 2059,
		velocity: 11.415,
		power: 634.103281902011,
		road: 17368.6067592593,
		acceleration: -0.142870370370371
	},
	{
		id: 2061,
		time: 2060,
		velocity: 11.2705555555556,
		power: 1685.69588520209,
		road: 17379.9416666667,
		acceleration: -0.0438888888888886
	},
	{
		id: 2062,
		time: 2061,
		velocity: 11.2533333333333,
		power: 1618.56102415149,
		road: 17391.2301851852,
		acceleration: -0.0488888888888894
	},
	{
		id: 2063,
		time: 2062,
		velocity: 11.2683333333333,
		power: 2898.34654694782,
		road: 17402.5290740741,
		acceleration: 0.069629629629631
	},
	{
		id: 2064,
		time: 2063,
		velocity: 11.4794444444444,
		power: 3097.82341701831,
		road: 17413.9053703704,
		acceleration: 0.0851851851851837
	},
	{
		id: 2065,
		time: 2064,
		velocity: 11.5088888888889,
		power: 3560.84405007465,
		road: 17425.3860185185,
		acceleration: 0.123518518518519
	},
	{
		id: 2066,
		time: 2065,
		velocity: 11.6388888888889,
		power: 3303.8035541205,
		road: 17436.9762962963,
		acceleration: 0.0957407407407391
	},
	{
		id: 2067,
		time: 2066,
		velocity: 11.7666666666667,
		power: 3214.89040341136,
		road: 17448.6565740741,
		acceleration: 0.0842592592592606
	},
	{
		id: 2068,
		time: 2067,
		velocity: 11.7616666666667,
		power: 3375.73234022299,
		road: 17460.4265740741,
		acceleration: 0.0951851851851835
	},
	{
		id: 2069,
		time: 2068,
		velocity: 11.9244444444444,
		power: 3721.32964332139,
		road: 17472.3049074074,
		acceleration: 0.121481481481482
	},
	{
		id: 2070,
		time: 2069,
		velocity: 12.1311111111111,
		power: 3425.09745145727,
		road: 17484.2895833333,
		acceleration: 0.0912037037037052
	},
	{
		id: 2071,
		time: 2070,
		velocity: 12.0352777777778,
		power: 2280.02478852646,
		road: 17496.3147685185,
		acceleration: -0.0101851851851862
	},
	{
		id: 2072,
		time: 2071,
		velocity: 11.8938888888889,
		power: 2028.95604204323,
		road: 17508.3191203704,
		acceleration: -0.0314814814814799
	},
	{
		id: 2073,
		time: 2072,
		velocity: 12.0366666666667,
		power: 2859.94284329065,
		road: 17520.3281944445,
		acceleration: 0.040925925925924
	},
	{
		id: 2074,
		time: 2073,
		velocity: 12.1580555555556,
		power: 4030.66522175012,
		road: 17532.4273611111,
		acceleration: 0.139259259259262
	},
	{
		id: 2075,
		time: 2074,
		velocity: 12.3116666666667,
		power: 4079.82977180048,
		road: 17544.6649537037,
		acceleration: 0.137592592592592
	},
	{
		id: 2076,
		time: 2075,
		velocity: 12.4494444444444,
		power: 4292.68312892199,
		road: 17557.0461111111,
		acceleration: 0.149537037037035
	},
	{
		id: 2077,
		time: 2076,
		velocity: 12.6066666666667,
		power: 5628.5314590964,
		road: 17569.6281018519,
		acceleration: 0.25212962962963
	},
	{
		id: 2078,
		time: 2077,
		velocity: 13.0680555555556,
		power: 5158.23847382777,
		road: 17582.4371759259,
		acceleration: 0.202037037037035
	},
	{
		id: 2079,
		time: 2078,
		velocity: 13.0555555555556,
		power: 5314.41075112362,
		road: 17595.4497685185,
		acceleration: 0.205000000000004
	},
	{
		id: 2080,
		time: 2079,
		velocity: 13.2216666666667,
		power: 3460.52355013906,
		road: 17608.5904166667,
		acceleration: 0.0511111111111084
	},
	{
		id: 2081,
		time: 2080,
		velocity: 13.2213888888889,
		power: 3210.86879143371,
		road: 17621.7715277778,
		acceleration: 0.0298148148148165
	},
	{
		id: 2082,
		time: 2081,
		velocity: 13.145,
		power: 3586.42387006853,
		road: 17634.9965740741,
		acceleration: 0.0580555555555549
	},
	{
		id: 2083,
		time: 2082,
		velocity: 13.3958333333333,
		power: 3938.7897331033,
		road: 17648.2922685185,
		acceleration: 0.0832407407407416
	},
	{
		id: 2084,
		time: 2083,
		velocity: 13.4711111111111,
		power: 5488.06972388288,
		road: 17661.7288888889,
		acceleration: 0.198611111111109
	},
	{
		id: 2085,
		time: 2084,
		velocity: 13.7408333333333,
		power: 5563.63218144001,
		road: 17675.3624074074,
		acceleration: 0.195185185185187
	},
	{
		id: 2086,
		time: 2085,
		velocity: 13.9813888888889,
		power: 3990.85100636802,
		road: 17689.1281944445,
		acceleration: 0.0693518518518506
	},
	{
		id: 2087,
		time: 2086,
		velocity: 13.6791666666667,
		power: 941.756794593338,
		road: 17702.8478240741,
		acceleration: -0.161666666666669
	},
	{
		id: 2088,
		time: 2087,
		velocity: 13.2558333333333,
		power: -2489.7018089591,
		road: 17716.2749537037,
		acceleration: -0.423333333333332
	},
	{
		id: 2089,
		time: 2088,
		velocity: 12.7113888888889,
		power: -2156.64992146932,
		road: 17729.2921296296,
		acceleration: -0.396574074074074
	},
	{
		id: 2090,
		time: 2089,
		velocity: 12.4894444444444,
		power: -2097.08067160018,
		road: 17741.9153240741,
		acceleration: -0.391388888888887
	},
	{
		id: 2091,
		time: 2090,
		velocity: 12.0816666666667,
		power: -1034.48045247976,
		road: 17754.1924537037,
		acceleration: -0.300740740740741
	},
	{
		id: 2092,
		time: 2091,
		velocity: 11.8091666666667,
		power: -1971.42713874379,
		road: 17766.12875,
		acceleration: -0.380925925925927
	},
	{
		id: 2093,
		time: 2092,
		velocity: 11.3466666666667,
		power: -1165.38443471687,
		road: 17777.7202777778,
		acceleration: -0.308611111111109
	},
	{
		id: 2094,
		time: 2093,
		velocity: 11.1558333333333,
		power: -1936.68984526121,
		road: 17788.9678240741,
		acceleration: -0.379351851851855
	},
	{
		id: 2095,
		time: 2094,
		velocity: 10.6711111111111,
		power: -1908.53748058559,
		road: 17799.8365740741,
		acceleration: -0.37824074074074
	},
	{
		id: 2096,
		time: 2095,
		velocity: 10.2119444444444,
		power: -7057.47815370362,
		road: 17810.0607870371,
		acceleration: -0.910833333333333
	},
	{
		id: 2097,
		time: 2096,
		velocity: 8.42333333333333,
		power: -9621.99088161302,
		road: 17819.1888425926,
		acceleration: -1.28148148148148
	},
	{
		id: 2098,
		time: 2097,
		velocity: 6.82666666666667,
		power: -9402.35305506723,
		road: 17826.9595370371,
		acceleration: -1.43324074074074
	},
	{
		id: 2099,
		time: 2098,
		velocity: 5.91222222222222,
		power: -6132.25106270446,
		road: 17833.440462963,
		acceleration: -1.1462962962963
	},
	{
		id: 2100,
		time: 2099,
		velocity: 4.98444444444444,
		power: -4053.87933070529,
		road: 17838.8843518519,
		acceleration: -0.927777777777777
	},
	{
		id: 2101,
		time: 2100,
		velocity: 4.04333333333333,
		power: -2978.19791114623,
		road: 17843.4514814815,
		acceleration: -0.82574074074074
	},
	{
		id: 2102,
		time: 2101,
		velocity: 3.435,
		power: -1888.3969187761,
		road: 17847.2780092593,
		acceleration: -0.655462962962963
	},
	{
		id: 2103,
		time: 2102,
		velocity: 3.01805555555556,
		power: -1190.2358157468,
		road: 17850.5165277778,
		acceleration: -0.520555555555555
	},
	{
		id: 2104,
		time: 2103,
		velocity: 2.48166666666667,
		power: -862.746163678379,
		road: 17853.2634259259,
		acceleration: -0.462685185185185
	},
	{
		id: 2105,
		time: 2104,
		velocity: 2.04694444444444,
		power: -502.029897860807,
		road: 17855.6005092593,
		acceleration: -0.356944444444445
	},
	{
		id: 2106,
		time: 2105,
		velocity: 1.94722222222222,
		power: -299.480872344461,
		road: 17857.6159259259,
		acceleration: -0.286388888888889
	},
	{
		id: 2107,
		time: 2106,
		velocity: 1.6225,
		power: -140.12227487819,
		road: 17859.381712963,
		acceleration: -0.21287037037037
	},
	{
		id: 2108,
		time: 2107,
		velocity: 1.40833333333333,
		power: -658.784902931391,
		road: 17860.7165277778,
		acceleration: -0.649074074074074
	},
	{
		id: 2109,
		time: 2108,
		velocity: 0,
		power: -226.609267975563,
		road: 17861.5127314815,
		acceleration: -0.428148148148148
	},
	{
		id: 2110,
		time: 2109,
		velocity: 0.338055555555556,
		power: -112.529446916868,
		road: 17861.8601388889,
		acceleration: -0.469444444444445
	},
	{
		id: 2111,
		time: 2110,
		velocity: 0,
		power: 13.6151636197739,
		road: 17861.9728240741,
		acceleration: 0
	},
	{
		id: 2112,
		time: 2111,
		velocity: 0,
		power: 0.792374545159195,
		road: 17862.0291666667,
		acceleration: -0.112685185185185
	},
	{
		id: 2113,
		time: 2112,
		velocity: 0,
		power: 133.948620616287,
		road: 17862.2650462963,
		acceleration: 0.471759259259259
	},
	{
		id: 2114,
		time: 2113,
		velocity: 1.41527777777778,
		power: 1022.76626089702,
		road: 17863.2311574074,
		acceleration: 0.988703703703704
	},
	{
		id: 2115,
		time: 2114,
		velocity: 2.96611111111111,
		power: 2604.55441689725,
		road: 17865.2926388889,
		acceleration: 1.20203703703704
	},
	{
		id: 2116,
		time: 2115,
		velocity: 3.60611111111111,
		power: 3112.16383926751,
		road: 17868.4139351852,
		acceleration: 0.917592592592592
	},
	{
		id: 2117,
		time: 2116,
		velocity: 4.16805555555556,
		power: 2053.82278191759,
		road: 17872.2111111111,
		acceleration: 0.434166666666667
	},
	{
		id: 2118,
		time: 2117,
		velocity: 4.26861111111111,
		power: 1599.76646727342,
		road: 17876.3598611111,
		acceleration: 0.268981481481481
	},
	{
		id: 2119,
		time: 2118,
		velocity: 4.41305555555556,
		power: 1168.97867222578,
		road: 17880.7153703704,
		acceleration: 0.144537037037037
	},
	{
		id: 2120,
		time: 2119,
		velocity: 4.60166666666667,
		power: 1632.67123477744,
		road: 17885.2626388889,
		acceleration: 0.238981481481481
	},
	{
		id: 2121,
		time: 2120,
		velocity: 4.98555555555556,
		power: 1512.09103312495,
		road: 17890.0263888889,
		acceleration: 0.193981481481481
	},
	{
		id: 2122,
		time: 2121,
		velocity: 4.995,
		power: 1082.89918783986,
		road: 17894.9328240741,
		acceleration: 0.0913888888888899
	},
	{
		id: 2123,
		time: 2122,
		velocity: 4.87583333333333,
		power: 183.994171759178,
		road: 17899.8342592593,
		acceleration: -0.10138888888889
	},
	{
		id: 2124,
		time: 2123,
		velocity: 4.68138888888889,
		power: -23.3263382967746,
		road: 17904.6123148148,
		acceleration: -0.145370370370371
	},
	{
		id: 2125,
		time: 2124,
		velocity: 4.55888888888889,
		power: -81.4487564574289,
		road: 17909.2387037037,
		acceleration: -0.157962962962962
	},
	{
		id: 2126,
		time: 2125,
		velocity: 4.40194444444444,
		power: -27.767893721755,
		road: 17913.7135185185,
		acceleration: -0.145185185185186
	},
	{
		id: 2127,
		time: 2126,
		velocity: 4.24583333333333,
		power: 22.4967374607454,
		road: 17918.0494907408,
		acceleration: -0.1325
	},
	{
		id: 2128,
		time: 2127,
		velocity: 4.16138888888889,
		power: -5.550193168169,
		road: 17922.2498611111,
		acceleration: -0.138703703703703
	},
	{
		id: 2129,
		time: 2128,
		velocity: 3.98583333333333,
		power: 125.105852987613,
		road: 17926.3286574074,
		acceleration: -0.104444444444445
	},
	{
		id: 2130,
		time: 2129,
		velocity: 3.9325,
		power: 115.541082427043,
		road: 17930.3024074074,
		acceleration: -0.105648148148148
	},
	{
		id: 2131,
		time: 2130,
		velocity: 3.84444444444444,
		power: 91.2656612787952,
		road: 17934.1678703704,
		acceleration: -0.110925925925926
	},
	{
		id: 2132,
		time: 2131,
		velocity: 3.65305555555556,
		power: -1426.90572247959,
		road: 17937.6977314815,
		acceleration: -0.560277777777777
	},
	{
		id: 2133,
		time: 2132,
		velocity: 2.25166666666667,
		power: -1760.21688621399,
		road: 17940.557037037,
		acceleration: -0.780833333333334
	},
	{
		id: 2134,
		time: 2133,
		velocity: 1.50194444444444,
		power: -1446.30624186081,
		road: 17942.5850462963,
		acceleration: -0.88175925925926
	},
	{
		id: 2135,
		time: 2134,
		velocity: 1.00777777777778,
		power: -714.816638427732,
		road: 17943.7968981482,
		acceleration: -0.750555555555555
	},
	{
		id: 2136,
		time: 2135,
		velocity: 0,
		power: -10.6537576442131,
		road: 17944.5622222222,
		acceleration: -0.1425
	},
	{
		id: 2137,
		time: 2136,
		velocity: 1.07444444444444,
		power: 475.265952842858,
		road: 17945.4688425926,
		acceleration: 0.425092592592593
	},
	{
		id: 2138,
		time: 2137,
		velocity: 2.28305555555556,
		power: 2142.47498132079,
		road: 17947.1825462963,
		acceleration: 1.18907407407407
	},
	{
		id: 2139,
		time: 2138,
		velocity: 3.56722222222222,
		power: 3288.84739176222,
		road: 17950.0327314815,
		acceleration: 1.08388888888889
	},
	{
		id: 2140,
		time: 2139,
		velocity: 4.32611111111111,
		power: 3405.94793969078,
		road: 17953.8294907407,
		acceleration: 0.809259259259259
	},
	{
		id: 2141,
		time: 2140,
		velocity: 4.71083333333333,
		power: 2340.21687008905,
		road: 17958.2409722222,
		acceleration: 0.420185185185185
	},
	{
		id: 2142,
		time: 2141,
		velocity: 4.82777777777778,
		power: 1098.61642366156,
		road: 17962.9163888889,
		acceleration: 0.107685185185185
	},
	{
		id: 2143,
		time: 2142,
		velocity: 4.64916666666667,
		power: -428.627478137663,
		road: 17967.527037037,
		acceleration: -0.237222222222222
	},
	{
		id: 2144,
		time: 2143,
		velocity: 3.99916666666667,
		power: -1126.72668306323,
		road: 17971.8117592593,
		acceleration: -0.41462962962963
	},
	{
		id: 2145,
		time: 2144,
		velocity: 3.58388888888889,
		power: -820.432780772553,
		road: 17975.7103703704,
		acceleration: -0.357592592592593
	},
	{
		id: 2146,
		time: 2145,
		velocity: 3.57638888888889,
		power: 195.101119303988,
		road: 17979.3906018519,
		acceleration: -0.0791666666666657
	},
	{
		id: 2147,
		time: 2146,
		velocity: 3.76166666666667,
		power: 632.03576515262,
		road: 17983.0546296296,
		acceleration: 0.0467592592592592
	},
	{
		id: 2148,
		time: 2147,
		velocity: 3.72416666666667,
		power: 525.417211359515,
		road: 17986.7493981482,
		acceleration: 0.0147222222222219
	},
	{
		id: 2149,
		time: 2148,
		velocity: 3.62055555555556,
		power: 170.849464977156,
		road: 17990.4086574074,
		acceleration: -0.0857407407407407
	},
	{
		id: 2150,
		time: 2149,
		velocity: 3.50444444444444,
		power: 317.099748808858,
		road: 17994.0041666667,
		acceleration: -0.0417592592592588
	},
	{
		id: 2151,
		time: 2150,
		velocity: 3.59888888888889,
		power: 591.145887871008,
		road: 17997.5981018519,
		acceleration: 0.0386111111111105
	},
	{
		id: 2152,
		time: 2151,
		velocity: 3.73638888888889,
		power: 1546.38801872667,
		road: 18001.3601388889,
		acceleration: 0.297592592592593
	},
	{
		id: 2153,
		time: 2152,
		velocity: 4.39722222222222,
		power: 2557.09530007913,
		road: 18005.5256018519,
		acceleration: 0.509259259259259
	},
	{
		id: 2154,
		time: 2153,
		velocity: 5.12666666666667,
		power: 3459.6798786839,
		road: 18010.2602777778,
		acceleration: 0.629166666666666
	},
	{
		id: 2155,
		time: 2154,
		velocity: 5.62388888888889,
		power: 2943.81209841596,
		road: 18015.5318518519,
		acceleration: 0.44462962962963
	},
	{
		id: 2156,
		time: 2155,
		velocity: 5.73111111111111,
		power: 2716.21286612865,
		road: 18021.2048611111,
		acceleration: 0.358240740740741
	},
	{
		id: 2157,
		time: 2156,
		velocity: 6.20138888888889,
		power: 1271.52794598942,
		road: 18027.097037037,
		acceleration: 0.0800925925925924
	},
	{
		id: 2158,
		time: 2157,
		velocity: 5.86416666666667,
		power: 477.063482667769,
		road: 18032.998287037,
		acceleration: -0.0619444444444452
	},
	{
		id: 2159,
		time: 2158,
		velocity: 5.54527777777778,
		power: -966.558941438638,
		road: 18038.7066203704,
		acceleration: -0.323888888888888
	},
	{
		id: 2160,
		time: 2159,
		velocity: 5.22972222222222,
		power: -954.597365718697,
		road: 18044.0878240741,
		acceleration: -0.330370370370371
	},
	{
		id: 2161,
		time: 2160,
		velocity: 4.87305555555556,
		power: -923.935786976085,
		road: 18049.1366666667,
		acceleration: -0.334351851851851
	},
	{
		id: 2162,
		time: 2161,
		velocity: 4.54222222222222,
		power: -480.316142830284,
		road: 18053.8951388889,
		acceleration: -0.246388888888889
	},
	{
		id: 2163,
		time: 2162,
		velocity: 4.49055555555556,
		power: -401.841057699125,
		road: 18058.4141666667,
		acceleration: -0.2325
	},
	{
		id: 2164,
		time: 2163,
		velocity: 4.17555555555556,
		power: 117.622819908695,
		road: 18062.7621759259,
		acceleration: -0.109537037037037
	},
	{
		id: 2165,
		time: 2164,
		velocity: 4.21361111111111,
		power: 15.7229855533501,
		road: 18066.9886574074,
		acceleration: -0.133518518518519
	},
	{
		id: 2166,
		time: 2165,
		velocity: 4.09,
		power: -411.491934182306,
		road: 18071.0264351852,
		acceleration: -0.243888888888888
	},
	{
		id: 2167,
		time: 2166,
		velocity: 3.44388888888889,
		power: 185.890268070549,
		road: 18074.8996296296,
		acceleration: -0.0852777777777778
	},
	{
		id: 2168,
		time: 2167,
		velocity: 3.95777777777778,
		power: -390.319308664456,
		road: 18078.6071759259,
		acceleration: -0.246018518518518
	},
	{
		id: 2169,
		time: 2168,
		velocity: 3.35194444444444,
		power: -352.915477201419,
		road: 18082.0709722222,
		acceleration: -0.241481481481482
	},
	{
		id: 2170,
		time: 2169,
		velocity: 2.71944444444444,
		power: -1637.12869297301,
		road: 18085.0591203704,
		acceleration: -0.709814814814814
	},
	{
		id: 2171,
		time: 2170,
		velocity: 1.82833333333333,
		power: -1374.32551156509,
		road: 18087.3045833333,
		acceleration: -0.775555555555555
	},
	{
		id: 2172,
		time: 2171,
		velocity: 1.02527777777778,
		power: -672.257998868468,
		road: 18088.8716203704,
		acceleration: -0.581296296296296
	},
	{
		id: 2173,
		time: 2172,
		velocity: 0.975555555555556,
		power: -246.884647259591,
		road: 18089.9648148148,
		acceleration: -0.366388888888889
	},
	{
		id: 2174,
		time: 2173,
		velocity: 0.729166666666667,
		power: 513.793465098537,
		road: 18091.0585185185,
		acceleration: 0.367407407407407
	},
	{
		id: 2175,
		time: 2174,
		velocity: 2.1275,
		power: 1081.87610945301,
		road: 18092.6336574074,
		acceleration: 0.595462962962963
	},
	{
		id: 2176,
		time: 2175,
		velocity: 2.76194444444444,
		power: 1788.82665314932,
		road: 18094.8641203704,
		acceleration: 0.715185185185185
	},
	{
		id: 2177,
		time: 2176,
		velocity: 2.87472222222222,
		power: 809.349364315641,
		road: 18097.5455092593,
		acceleration: 0.186666666666667
	},
	{
		id: 2178,
		time: 2177,
		velocity: 2.6875,
		power: 2602.80614562001,
		road: 18100.6898148148,
		acceleration: 0.739166666666667
	},
	{
		id: 2179,
		time: 2178,
		velocity: 4.97944444444444,
		power: 4784.1022711754,
		road: 18104.7549537037,
		acceleration: 1.1025
	},
	{
		id: 2180,
		time: 2179,
		velocity: 6.18222222222222,
		power: 9588.80206764157,
		road: 18110.2218518519,
		acceleration: 1.70101851851852
	},
	{
		id: 2181,
		time: 2180,
		velocity: 7.79055555555555,
		power: 10583.7030664052,
		road: 18117.2527777778,
		acceleration: 1.42703703703704
	},
	{
		id: 2182,
		time: 2181,
		velocity: 9.26055555555556,
		power: 13866.6995379037,
		road: 18125.7681944445,
		acceleration: 1.54194444444444
	},
	{
		id: 2183,
		time: 2182,
		velocity: 10.8080555555556,
		power: 15655.8178622878,
		road: 18135.7828240741,
		acceleration: 1.45648148148148
	},
	{
		id: 2184,
		time: 2183,
		velocity: 12.16,
		power: 12102.8637406004,
		road: 18146.9926851852,
		acceleration: 0.933981481481482
	},
	{
		id: 2185,
		time: 2184,
		velocity: 12.0625,
		power: 6305.33353859446,
		road: 18158.8450925926,
		acceleration: 0.351111111111114
	},
	{
		id: 2186,
		time: 2185,
		velocity: 11.8613888888889,
		power: 1478.47346679806,
		road: 18170.8334722222,
		acceleration: -0.0791666666666693
	},
	{
		id: 2187,
		time: 2186,
		velocity: 11.9225,
		power: 5692.54004034135,
		road: 18182.9241203704,
		acceleration: 0.283703703703704
	},
	{
		id: 2188,
		time: 2187,
		velocity: 12.9136111111111,
		power: 11369.311202658,
		road: 18195.5210648148,
		acceleration: 0.728888888888889
	},
	{
		id: 2189,
		time: 2188,
		velocity: 14.0480555555556,
		power: 15407.8411689288,
		road: 18208.9678240741,
		acceleration: 0.970740740740739
	},
	{
		id: 2190,
		time: 2189,
		velocity: 14.8347222222222,
		power: 15677.88896957,
		road: 18223.3485648148,
		acceleration: 0.897222222222224
	},
	{
		id: 2191,
		time: 2190,
		velocity: 15.6052777777778,
		power: 11907.2723012472,
		road: 18238.462037037,
		acceleration: 0.568240740740741
	},
	{
		id: 2192,
		time: 2191,
		velocity: 15.7527777777778,
		power: 10033.5138703074,
		road: 18254.0637037037,
		acceleration: 0.408148148148147
	},
	{
		id: 2193,
		time: 2192,
		velocity: 16.0591666666667,
		power: 6778.71847459602,
		road: 18269.9575925926,
		acceleration: 0.176296296296297
	},
	{
		id: 2194,
		time: 2193,
		velocity: 16.1341666666667,
		power: 6827.65808948088,
		road: 18286.0253703704,
		acceleration: 0.171481481481484
	},
	{
		id: 2195,
		time: 2194,
		velocity: 16.2672222222222,
		power: 4528.17972541289,
		road: 18302.1880555556,
		acceleration: 0.0183333333333309
	},
	{
		id: 2196,
		time: 2195,
		velocity: 16.1141666666667,
		power: 2631.95008903969,
		road: 18318.3083333333,
		acceleration: -0.103148148148147
	},
	{
		id: 2197,
		time: 2196,
		velocity: 15.8247222222222,
		power: 1070.90860689448,
		road: 18334.2765277778,
		acceleration: -0.201018518518518
	},
	{
		id: 2198,
		time: 2197,
		velocity: 15.6641666666667,
		power: 1148.53215004631,
		road: 18350.0484722222,
		acceleration: -0.191481481481482
	},
	{
		id: 2199,
		time: 2198,
		velocity: 15.5397222222222,
		power: 2249.79438831311,
		road: 18365.6675462963,
		acceleration: -0.11425925925926
	},
	{
		id: 2200,
		time: 2199,
		velocity: 15.4819444444444,
		power: 1078.39727171761,
		road: 18381.1348148148,
		acceleration: -0.189351851851853
	},
	{
		id: 2201,
		time: 2200,
		velocity: 15.0961111111111,
		power: 611.707599396084,
		road: 18396.3989814815,
		acceleration: -0.216851851851851
	},
	{
		id: 2202,
		time: 2201,
		velocity: 14.8891666666667,
		power: 353.539477009075,
		road: 18411.439537037,
		acceleration: -0.230370370370371
	},
	{
		id: 2203,
		time: 2202,
		velocity: 14.7908333333333,
		power: 752.479162250338,
		road: 18426.265787037,
		acceleration: -0.198240740740738
	},
	{
		id: 2204,
		time: 2203,
		velocity: 14.5013888888889,
		power: 1150.30157513342,
		road: 18440.9098611111,
		acceleration: -0.166111111111112
	},
	{
		id: 2205,
		time: 2204,
		velocity: 14.3908333333333,
		power: 324.281085388738,
		road: 18455.3600462963,
		acceleration: -0.221666666666666
	},
	{
		id: 2206,
		time: 2205,
		velocity: 14.1258333333333,
		power: 1025.96073309069,
		road: 18469.6160648148,
		acceleration: -0.166666666666666
	},
	{
		id: 2207,
		time: 2206,
		velocity: 14.0013888888889,
		power: 1105.24229596974,
		road: 18483.7100925926,
		acceleration: -0.157314814814816
	},
	{
		id: 2208,
		time: 2207,
		velocity: 13.9188888888889,
		power: 1261.60408545969,
		road: 18497.6543055556,
		acceleration: -0.142314814814815
	},
	{
		id: 2209,
		time: 2208,
		velocity: 13.6988888888889,
		power: 1990.83779193817,
		road: 18511.4850925926,
		acceleration: -0.0845370370370375
	},
	{
		id: 2210,
		time: 2209,
		velocity: 13.7477777777778,
		power: 3100.19593547308,
		road: 18525.2740277778,
		acceleration: 0.000833333333336128
	},
	{
		id: 2211,
		time: 2210,
		velocity: 13.9213888888889,
		power: 4226.66943847873,
		road: 18539.1057407407,
		acceleration: 0.0847222222222204
	},
	{
		id: 2212,
		time: 2211,
		velocity: 13.9530555555556,
		power: 4698.63682926444,
		road: 18553.037962963,
		acceleration: 0.116296296296298
	},
	{
		id: 2213,
		time: 2212,
		velocity: 14.0966666666667,
		power: 5069.86617089188,
		road: 18567.0976851852,
		acceleration: 0.138703703703703
	},
	{
		id: 2214,
		time: 2213,
		velocity: 14.3375,
		power: 4936.1584795541,
		road: 18581.2883796296,
		acceleration: 0.123240740740741
	},
	{
		id: 2215,
		time: 2214,
		velocity: 14.3227777777778,
		power: 4411.49750712512,
		road: 18595.5809722222,
		acceleration: 0.0805555555555557
	},
	{
		id: 2216,
		time: 2215,
		velocity: 14.3383333333333,
		power: 3036.5046012096,
		road: 18609.903287037,
		acceleration: -0.0211111111111144
	},
	{
		id: 2217,
		time: 2216,
		velocity: 14.2741666666667,
		power: 3701.7474650755,
		road: 18624.22875,
		acceleration: 0.0274074074074093
	},
	{
		id: 2218,
		time: 2217,
		velocity: 14.405,
		power: 4078.26893786032,
		road: 18638.5946296296,
		acceleration: 0.0534259259259269
	},
	{
		id: 2219,
		time: 2218,
		velocity: 14.4986111111111,
		power: 4715.63792474495,
		road: 18653.0356481482,
		acceleration: 0.0968518518518522
	},
	{
		id: 2220,
		time: 2219,
		velocity: 14.5647222222222,
		power: 4724.89413936795,
		road: 18667.5719444444,
		acceleration: 0.093703703703703
	},
	{
		id: 2221,
		time: 2220,
		velocity: 14.6861111111111,
		power: 4435.51940541753,
		road: 18682.1899537037,
		acceleration: 0.0697222222222234
	},
	{
		id: 2222,
		time: 2221,
		velocity: 14.7077777777778,
		power: 4812.28534513046,
		road: 18696.889537037,
		acceleration: 0.0934259259259242
	},
	{
		id: 2223,
		time: 2222,
		velocity: 14.845,
		power: 4263.87022339095,
		road: 18711.6616666667,
		acceleration: 0.0516666666666676
	},
	{
		id: 2224,
		time: 2223,
		velocity: 14.8411111111111,
		power: 2810.8213485812,
		road: 18726.4339814815,
		acceleration: -0.0512962962962984
	},
	{
		id: 2225,
		time: 2224,
		velocity: 14.5538888888889,
		power: 17.2998657456668,
		road: 18741.0572685185,
		acceleration: -0.246759259259258
	},
	{
		id: 2226,
		time: 2225,
		velocity: 14.1047222222222,
		power: -2968.85337176232,
		road: 18755.3271296296,
		acceleration: -0.460092592592593
	},
	{
		id: 2227,
		time: 2226,
		velocity: 13.4608333333333,
		power: -3983.20251474477,
		road: 18769.098287037,
		acceleration: -0.537314814814813
	},
	{
		id: 2228,
		time: 2227,
		velocity: 12.9419444444444,
		power: -5339.64120040383,
		road: 18782.2758333333,
		acceleration: -0.649907407407406
	},
	{
		id: 2229,
		time: 2228,
		velocity: 12.155,
		power: -5073.09964467717,
		road: 18794.808287037,
		acceleration: -0.640277777777778
	},
	{
		id: 2230,
		time: 2229,
		velocity: 11.54,
		power: -5585.09480841694,
		road: 18806.6703703704,
		acceleration: -0.700462962962964
	},
	{
		id: 2231,
		time: 2230,
		velocity: 10.8405555555556,
		power: -5555.28602911416,
		road: 18817.8221759259,
		acceleration: -0.720092592592593
	},
	{
		id: 2232,
		time: 2231,
		velocity: 9.99472222222222,
		power: -4815.92310726131,
		road: 18828.2777314815,
		acceleration: -0.672407407407407
	},
	{
		id: 2233,
		time: 2232,
		velocity: 9.52277777777778,
		power: -2982.48964777698,
		road: 18838.1473148148,
		acceleration: -0.499537037037037
	},
	{
		id: 2234,
		time: 2233,
		velocity: 9.34194444444444,
		power: -850.976532359337,
		road: 18847.6309722222,
		acceleration: -0.272314814814816
	},
	{
		id: 2235,
		time: 2234,
		velocity: 9.17777777777778,
		power: -23.0382957373316,
		road: 18856.8893055556,
		acceleration: -0.178333333333331
	},
	{
		id: 2236,
		time: 2235,
		velocity: 8.98777777777778,
		power: -701.046140236413,
		road: 18865.9310185185,
		acceleration: -0.254907407407408
	},
	{
		id: 2237,
		time: 2236,
		velocity: 8.57722222222222,
		power: -1332.83135518938,
		road: 18874.68,
		acceleration: -0.330555555555556
	},
	{
		id: 2238,
		time: 2237,
		velocity: 8.18611111111111,
		power: -842.655548757005,
		road: 18883.1275,
		acceleration: -0.272407407407407
	},
	{
		id: 2239,
		time: 2238,
		velocity: 8.17055555555556,
		power: -1064.9021380284,
		road: 18891.2877777778,
		acceleration: -0.302037037037037
	},
	{
		id: 2240,
		time: 2239,
		velocity: 7.67111111111111,
		power: -645.277500584776,
		road: 18899.1728240741,
		acceleration: -0.248425925925926
	},
	{
		id: 2241,
		time: 2240,
		velocity: 7.44083333333333,
		power: -164.353661422141,
		road: 18906.8421296296,
		acceleration: -0.183055555555555
	},
	{
		id: 2242,
		time: 2241,
		velocity: 7.62138888888889,
		power: 2933.56841489864,
		road: 18914.5397685185,
		acceleration: 0.239722222222222
	},
	{
		id: 2243,
		time: 2242,
		velocity: 8.39027777777778,
		power: 5505.6668224842,
		road: 18922.6325,
		acceleration: 0.550462962962963
	},
	{
		id: 2244,
		time: 2243,
		velocity: 9.09222222222222,
		power: 8286.01529599458,
		road: 18931.4106944445,
		acceleration: 0.820462962962964
	},
	{
		id: 2245,
		time: 2244,
		velocity: 10.0827777777778,
		power: 8721.57693047771,
		road: 18940.9875462963,
		acceleration: 0.77685185185185
	},
	{
		id: 2246,
		time: 2245,
		velocity: 10.7208333333333,
		power: 7740.81280096643,
		road: 18951.2549537037,
		acceleration: 0.60425925925926
	},
	{
		id: 2247,
		time: 2246,
		velocity: 10.905,
		power: 4990.30836414892,
		road: 18961.9726388889,
		acceleration: 0.296296296296296
	},
	{
		id: 2248,
		time: 2247,
		velocity: 10.9716666666667,
		power: 2590.95809691059,
		road: 18972.8660648148,
		acceleration: 0.0551851851851843
	},
	{
		id: 2249,
		time: 2248,
		velocity: 10.8863888888889,
		power: 1404.90531003716,
		road: 18983.7575925926,
		acceleration: -0.0589814814814815
	},
	{
		id: 2250,
		time: 2249,
		velocity: 10.7280555555556,
		power: 454.808447352952,
		road: 18994.5452314815,
		acceleration: -0.148796296296293
	},
	{
		id: 2251,
		time: 2250,
		velocity: 10.5252777777778,
		power: -169.980212920961,
		road: 19005.1546296296,
		acceleration: -0.207685185185188
	},
	{
		id: 2252,
		time: 2251,
		velocity: 10.2633333333333,
		power: 500.264723222249,
		road: 19015.5909259259,
		acceleration: -0.138518518518516
	},
	{
		id: 2253,
		time: 2252,
		velocity: 10.3125,
		power: 1508.86714023547,
		road: 19025.9405555556,
		acceleration: -0.0348148148148155
	},
	{
		id: 2254,
		time: 2253,
		velocity: 10.4208333333333,
		power: 4626.35931484675,
		road: 19036.41,
		acceleration: 0.274444444444445
	},
	{
		id: 2255,
		time: 2254,
		velocity: 11.0866666666667,
		power: 5238.17136480921,
		road: 19047.1755092593,
		acceleration: 0.317685185185184
	},
	{
		id: 2256,
		time: 2255,
		velocity: 11.2655555555556,
		power: 5795.30355160381,
		road: 19058.2752314815,
		acceleration: 0.35074074074074
	},
	{
		id: 2257,
		time: 2256,
		velocity: 11.4730555555556,
		power: 4673.34619847088,
		road: 19069.6651851852,
		acceleration: 0.229722222222222
	},
	{
		id: 2258,
		time: 2257,
		velocity: 11.7758333333333,
		power: 6371.78863860413,
		road: 19081.3535648148,
		acceleration: 0.36712962962963
	},
	{
		id: 2259,
		time: 2258,
		velocity: 12.3669444444444,
		power: 6310.75227865719,
		road: 19093.3956018518,
		acceleration: 0.340185185185186
	},
	{
		id: 2260,
		time: 2259,
		velocity: 12.4936111111111,
		power: 4930.27872727002,
		road: 19105.7110648148,
		acceleration: 0.206666666666665
	},
	{
		id: 2261,
		time: 2260,
		velocity: 12.3958333333333,
		power: 2670.61204206611,
		road: 19118.1352777778,
		acceleration: 0.0108333333333341
	},
	{
		id: 2262,
		time: 2261,
		velocity: 12.3994444444444,
		power: 2736.16882790102,
		road: 19130.5728703704,
		acceleration: 0.0159259259259237
	},
	{
		id: 2263,
		time: 2262,
		velocity: 12.5413888888889,
		power: 3557.50064805813,
		road: 19143.0600462963,
		acceleration: 0.0832407407407416
	},
	{
		id: 2264,
		time: 2263,
		velocity: 12.6455555555556,
		power: 4306.80794509112,
		road: 19155.6594907407,
		acceleration: 0.1412962962963
	},
	{
		id: 2265,
		time: 2264,
		velocity: 12.8233333333333,
		power: 4732.28048846191,
		road: 19168.4143981481,
		acceleration: 0.169629629629627
	},
	{
		id: 2266,
		time: 2265,
		velocity: 13.0502777777778,
		power: 4965.02283150745,
		road: 19181.3444444444,
		acceleration: 0.180648148148151
	},
	{
		id: 2267,
		time: 2266,
		velocity: 13.1875,
		power: 5310.09107168716,
		road: 19194.4645833333,
		acceleration: 0.199537037037034
	},
	{
		id: 2268,
		time: 2267,
		velocity: 13.4219444444444,
		power: 4923.82573905548,
		road: 19207.7648148148,
		acceleration: 0.160648148148148
	},
	{
		id: 2269,
		time: 2268,
		velocity: 13.5322222222222,
		power: 4544.80221606978,
		road: 19221.2077777778,
		acceleration: 0.124814814814817
	},
	{
		id: 2270,
		time: 2269,
		velocity: 13.5619444444444,
		power: 5021.3911192023,
		road: 19234.7910185185,
		acceleration: 0.15574074074074
	},
	{
		id: 2271,
		time: 2270,
		velocity: 13.8891666666667,
		power: 7180.77365265744,
		road: 19248.6067592593,
		acceleration: 0.309259259259258
	},
	{
		id: 2272,
		time: 2271,
		velocity: 14.46,
		power: 10200.6350197098,
		road: 19262.8317592593,
		acceleration: 0.50925925925926
	},
	{
		id: 2273,
		time: 2272,
		velocity: 15.0897222222222,
		power: 10105.5408789643,
		road: 19277.5460648148,
		acceleration: 0.469351851851853
	},
	{
		id: 2274,
		time: 2273,
		velocity: 15.2972222222222,
		power: 7532.18349450846,
		road: 19292.6285185185,
		acceleration: 0.266944444444443
	},
	{
		id: 2275,
		time: 2274,
		velocity: 15.2608333333333,
		power: 4064.62914883232,
		road: 19307.8550462963,
		acceleration: 0.0212037037037049
	},
	{
		id: 2276,
		time: 2275,
		velocity: 15.1533333333333,
		power: 2651.97192293617,
		road: 19323.0546296296,
		acceleration: -0.0750925925925934
	},
	{
		id: 2277,
		time: 2276,
		velocity: 15.0719444444444,
		power: 928.350095519303,
		road: 19338.1212037037,
		acceleration: -0.190925925925928
	},
	{
		id: 2278,
		time: 2277,
		velocity: 14.6880555555556,
		power: 897.309971816713,
		road: 19352.9977777778,
		acceleration: -0.189074074074071
	},
	{
		id: 2279,
		time: 2278,
		velocity: 14.5861111111111,
		power: 1871.94243112245,
		road: 19367.7215277778,
		acceleration: -0.116574074074075
	},
	{
		id: 2280,
		time: 2279,
		velocity: 14.7222222222222,
		power: 3091.45651148318,
		road: 19382.3731944444,
		acceleration: -0.0275925925925922
	},
	{
		id: 2281,
		time: 2280,
		velocity: 14.6052777777778,
		power: 2366.94057541437,
		road: 19396.9721296296,
		acceleration: -0.0778703703703698
	},
	{
		id: 2282,
		time: 2281,
		velocity: 14.3525,
		power: -2700.52184535218,
		road: 19411.3118518519,
		acceleration: -0.440555555555555
	},
	{
		id: 2283,
		time: 2282,
		velocity: 13.4005555555556,
		power: -10916.6603363908,
		road: 19424.8945833333,
		acceleration: -1.07342592592593
	},
	{
		id: 2284,
		time: 2283,
		velocity: 11.385,
		power: -12120.8172915716,
		road: 19437.3221296296,
		acceleration: -1.23694444444444
	},
	{
		id: 2285,
		time: 2284,
		velocity: 10.6416666666667,
		power: -8031.74819076183,
		road: 19448.6597222222,
		acceleration: -0.942962962962962
	},
	{
		id: 2286,
		time: 2285,
		velocity: 10.5716666666667,
		power: -2223.9337974616,
		road: 19459.3206481482,
		acceleration: -0.410370370370369
	},
	{
		id: 2287,
		time: 2286,
		velocity: 10.1538888888889,
		power: -3335.22769918792,
		road: 19469.5116666667,
		acceleration: -0.529444444444447
	},
	{
		id: 2288,
		time: 2287,
		velocity: 9.05333333333333,
		power: -11870.4333063924,
		road: 19478.6699537037,
		acceleration: -1.53601851851852
	},
	{
		id: 2289,
		time: 2288,
		velocity: 5.96361111111111,
		power: -14478.7161704667,
		road: 19485.9336574074,
		acceleration: -2.25314814814815
	},
	{
		id: 2290,
		time: 2289,
		velocity: 3.39444444444444,
		power: -11160.8564717121,
		road: 19490.7905555556,
		acceleration: -2.56046296296296
	},
	{
		id: 2291,
		time: 2290,
		velocity: 1.37194444444444,
		power: -4548.33829862809,
		road: 19493.373287037,
		acceleration: -1.98787037037037
	},
	{
		id: 2292,
		time: 2291,
		velocity: 0,
		power: -972.9241919412,
		road: 19494.3963425926,
		acceleration: -1.13148148148148
	},
	{
		id: 2293,
		time: 2292,
		velocity: 0,
		power: -71.4389122969461,
		road: 19494.625,
		acceleration: -0.457314814814815
	},
	{
		id: 2294,
		time: 2293,
		velocity: 0,
		power: 210.692664067014,
		road: 19494.9280555556,
		acceleration: 0.606111111111111
	},
	{
		id: 2295,
		time: 2294,
		velocity: 1.81833333333333,
		power: 3067.27093735955,
		road: 19496.4990277778,
		acceleration: 1.92972222222222
	},
	{
		id: 2296,
		time: 2295,
		velocity: 5.78916666666667,
		power: 8641.63154052335,
		road: 19500.1975,
		acceleration: 2.32527777777778
	},
	{
		id: 2297,
		time: 2296,
		velocity: 6.97583333333333,
		power: 13304.508713356,
		road: 19506.1585185185,
		acceleration: 2.19981481481481
	},
	{
		id: 2298,
		time: 2297,
		velocity: 8.41777777777778,
		power: 10288.845417628,
		road: 19513.8425925926,
		acceleration: 1.2462962962963
	},
	{
		id: 2299,
		time: 2298,
		velocity: 9.52805555555556,
		power: 11784.0004522463,
		road: 19522.7576851852,
		acceleration: 1.21574074074074
	},
	{
		id: 2300,
		time: 2299,
		velocity: 10.6230555555556,
		power: 9301.79564164225,
		road: 19532.6810648148,
		acceleration: 0.800833333333332
	},
	{
		id: 2301,
		time: 2300,
		velocity: 10.8202777777778,
		power: 7606.16721004185,
		road: 19543.2856944445,
		acceleration: 0.561666666666666
	},
	{
		id: 2302,
		time: 2301,
		velocity: 11.2130555555556,
		power: 6335.9200976359,
		road: 19554.3725,
		acceleration: 0.402685185185186
	},
	{
		id: 2303,
		time: 2302,
		velocity: 11.8311111111111,
		power: 5512.32513648621,
		road: 19565.8126851852,
		acceleration: 0.304074074074073
	},
	{
		id: 2304,
		time: 2303,
		velocity: 11.7325,
		power: 5495.45707065253,
		road: 19577.5478703704,
		acceleration: 0.285925925925929
	},
	{
		id: 2305,
		time: 2304,
		velocity: 12.0708333333333,
		power: 4819.23764977787,
		road: 19589.5325925926,
		acceleration: 0.213148148148148
	},
	{
		id: 2306,
		time: 2305,
		velocity: 12.4705555555556,
		power: 5797.35246091307,
		road: 19601.7663425926,
		acceleration: 0.284907407407406
	},
	{
		id: 2307,
		time: 2306,
		velocity: 12.5872222222222,
		power: 3426.59295311673,
		road: 19614.1800462963,
		acceleration: 0.0749999999999993
	},
	{
		id: 2308,
		time: 2307,
		velocity: 12.2958333333333,
		power: 507.281055287814,
		road: 19626.5459259259,
		acceleration: -0.170648148148146
	},
	{
		id: 2309,
		time: 2308,
		velocity: 11.9586111111111,
		power: -1174.32579319007,
		road: 19638.6705555556,
		acceleration: -0.311851851851854
	},
	{
		id: 2310,
		time: 2309,
		velocity: 11.6516666666667,
		power: 530.039665241381,
		road: 19650.5590740741,
		acceleration: -0.160370370370371
	},
	{
		id: 2311,
		time: 2310,
		velocity: 11.8147222222222,
		power: 4647.72698883421,
		road: 19662.468287037,
		acceleration: 0.20175925925926
	},
	{
		id: 2312,
		time: 2311,
		velocity: 12.5638888888889,
		power: 6068.06391671228,
		road: 19674.6343518519,
		acceleration: 0.311944444444444
	},
	{
		id: 2313,
		time: 2312,
		velocity: 12.5875,
		power: 7078.85974626021,
		road: 19687.1450462963,
		acceleration: 0.377314814814815
	},
	{
		id: 2314,
		time: 2313,
		velocity: 12.9466666666667,
		power: 7426.1517286167,
		road: 19700.0356481482,
		acceleration: 0.3825
	},
	{
		id: 2315,
		time: 2314,
		velocity: 13.7113888888889,
		power: 11085.2533249647,
		road: 19713.4363888889,
		acceleration: 0.637777777777778
	},
	{
		id: 2316,
		time: 2315,
		velocity: 14.5008333333333,
		power: 9008.19318653727,
		road: 19727.3758796296,
		acceleration: 0.439722222222223
	},
	{
		id: 2317,
		time: 2316,
		velocity: 14.2658333333333,
		power: 5167.90516577159,
		road: 19741.6045833333,
		acceleration: 0.138703703703703
	},
	{
		id: 2318,
		time: 2317,
		velocity: 14.1275,
		power: 164.681826481726,
		road: 19755.788287037,
		acceleration: -0.228703703703705
	},
	{
		id: 2319,
		time: 2318,
		velocity: 13.8147222222222,
		power: 1172.77577305407,
		road: 19769.7825925926,
		acceleration: -0.150092592592593
	},
	{
		id: 2320,
		time: 2319,
		velocity: 13.8155555555556,
		power: 219.3171393001,
		road: 19783.5926851852,
		acceleration: -0.218333333333334
	},
	{
		id: 2321,
		time: 2320,
		velocity: 13.4725,
		power: -203.033279526574,
		road: 19797.1700925926,
		acceleration: -0.247037037037035
	},
	{
		id: 2322,
		time: 2321,
		velocity: 13.0736111111111,
		power: -2387.29014300356,
		road: 19810.4163888889,
		acceleration: -0.415185185185186
	},
	{
		id: 2323,
		time: 2322,
		velocity: 12.57,
		power: -3742.75784933248,
		road: 19823.1918518519,
		acceleration: -0.526481481481483
	},
	{
		id: 2324,
		time: 2323,
		velocity: 11.8930555555556,
		power: -5827.33104980213,
		road: 19835.3474074074,
		acceleration: -0.713333333333333
	},
	{
		id: 2325,
		time: 2324,
		velocity: 10.9336111111111,
		power: -10949.7091186694,
		road: 19846.5338888889,
		acceleration: -1.22481481481481
	},
	{
		id: 2326,
		time: 2325,
		velocity: 8.89555555555556,
		power: -10572.362441758,
		road: 19856.4572685185,
		acceleration: -1.30138888888889
	},
	{
		id: 2327,
		time: 2326,
		velocity: 7.98888888888889,
		power: -10691.2673057116,
		road: 19864.9873148148,
		acceleration: -1.48527777777778
	},
	{
		id: 2328,
		time: 2327,
		velocity: 6.47777777777778,
		power: -6268.92214897396,
		road: 19872.2419444444,
		acceleration: -1.06555555555556
	},
	{
		id: 2329,
		time: 2328,
		velocity: 5.69888888888889,
		power: -6783.02444012535,
		road: 19878.3008333333,
		acceleration: -1.32592592592593
	},
	{
		id: 2330,
		time: 2329,
		velocity: 4.01111111111111,
		power: -4267.88914743566,
		road: 19883.1644907407,
		acceleration: -1.06453703703704
	},
	{
		id: 2331,
		time: 2330,
		velocity: 3.28416666666667,
		power: -3944.12131075657,
		road: 19886.8672222222,
		acceleration: -1.25731481481481
	},
	{
		id: 2332,
		time: 2331,
		velocity: 1.92694444444444,
		power: -2217.23414327224,
		road: 19889.4175,
		acceleration: -1.04759259259259
	},
	{
		id: 2333,
		time: 2332,
		velocity: 0.868333333333333,
		power: -1354.6021922688,
		road: 19890.8966203704,
		acceleration: -1.09472222222222
	},
	{
		id: 2334,
		time: 2333,
		velocity: 0,
		power: -297.757182173814,
		road: 19891.5072222222,
		acceleration: -0.642314814814815
	},
	{
		id: 2335,
		time: 2334,
		velocity: 0,
		power: -22.1993225146199,
		road: 19891.6519444444,
		acceleration: -0.289444444444444
	},
	{
		id: 2336,
		time: 2335,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2337,
		time: 2336,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2338,
		time: 2337,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2339,
		time: 2338,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2340,
		time: 2339,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2341,
		time: 2340,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2342,
		time: 2341,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2343,
		time: 2342,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2344,
		time: 2343,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2345,
		time: 2344,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2346,
		time: 2345,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2347,
		time: 2346,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2348,
		time: 2347,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2349,
		time: 2348,
		velocity: 0,
		power: 0,
		road: 19891.6519444444,
		acceleration: 0
	},
	{
		id: 2350,
		time: 2349,
		velocity: 0,
		power: 65.0123160362138,
		road: 19891.8080092593,
		acceleration: 0.31212962962963
	},
	{
		id: 2351,
		time: 2350,
		velocity: 0.936388888888889,
		power: 534.202403387896,
		road: 19892.4773148148,
		acceleration: 0.714351851851852
	},
	{
		id: 2352,
		time: 2351,
		velocity: 2.14305555555556,
		power: 2998.43994578964,
		road: 19894.3042592593,
		acceleration: 1.60092592592593
	},
	{
		id: 2353,
		time: 2352,
		velocity: 4.80277777777778,
		power: 6866.8685995489,
		road: 19897.8765740741,
		acceleration: 1.88981481481482
	},
	{
		id: 2354,
		time: 2353,
		velocity: 6.60583333333333,
		power: 9233.94341892227,
		road: 19903.2294907407,
		acceleration: 1.67138888888889
	},
	{
		id: 2355,
		time: 2354,
		velocity: 7.15722222222222,
		power: 8099.89971287835,
		road: 19909.9730092593,
		acceleration: 1.10981481481482
	},
	{
		id: 2356,
		time: 2355,
		velocity: 8.13222222222222,
		power: 9711.23771018865,
		road: 19917.8388888889,
		acceleration: 1.13490740740741
	},
	{
		id: 2357,
		time: 2356,
		velocity: 10.0105555555556,
		power: 11551.8001854612,
		road: 19926.8580092593,
		acceleration: 1.17157407407407
	},
	{
		id: 2358,
		time: 2357,
		velocity: 10.6719444444444,
		power: 11912.3768569702,
		road: 19936.9873148148,
		acceleration: 1.0487962962963
	},
	{
		id: 2359,
		time: 2358,
		velocity: 11.2786111111111,
		power: 7015.97113954219,
		road: 19947.8816203704,
		acceleration: 0.481203703703702
	},
	{
		id: 2360,
		time: 2359,
		velocity: 11.4541666666667,
		power: 5377.90776188372,
		road: 19959.1668055556,
		acceleration: 0.300555555555558
	},
	{
		id: 2361,
		time: 2360,
		velocity: 11.5736111111111,
		power: 3686.56196664017,
		road: 19970.6693055556,
		acceleration: 0.134074074074073
	},
	{
		id: 2362,
		time: 2361,
		velocity: 11.6808333333333,
		power: 2460.68329937853,
		road: 19982.24875,
		acceleration: 0.019814814814815
	},
	{
		id: 2363,
		time: 2362,
		velocity: 11.5136111111111,
		power: -3486.5801528918,
		road: 19993.5768055555,
		acceleration: -0.522592592592593
	},
	{
		id: 2364,
		time: 2363,
		velocity: 10.0058333333333,
		power: -7874.4630943381,
		road: 20004.1578240741,
		acceleration: -0.971481481481481
	},
	{
		id: 2365,
		time: 2364,
		velocity: 8.76638888888889,
		power: -7802.40991510691,
		road: 20013.7358796296,
		acceleration: -1.03444444444444
	},
	{
		id: 2366,
		time: 2365,
		velocity: 8.41027777777778,
		power: -4285.11126857859,
		road: 20022.4534259259,
		acceleration: -0.686574074074073
	},
	{
		id: 2367,
		time: 2366,
		velocity: 7.94611111111111,
		power: -1399.12228360147,
		road: 20030.6554166667,
		acceleration: -0.344537037037039
	},
	{
		id: 2368,
		time: 2367,
		velocity: 7.73277777777778,
		power: -1795.47819875379,
		road: 20038.4836574074,
		acceleration: -0.402962962962961
	},
	{
		id: 2369,
		time: 2368,
		velocity: 7.20138888888889,
		power: -1909.65418539366,
		road: 20045.8958333333,
		acceleration: -0.429166666666667
	},
	{
		id: 2370,
		time: 2369,
		velocity: 6.65861111111111,
		power: -4647.10321181024,
		road: 20052.6553703704,
		acceleration: -0.87611111111111
	},
	{
		id: 2371,
		time: 2370,
		velocity: 5.10444444444444,
		power: -4047.48986150127,
		road: 20058.5416666667,
		acceleration: -0.870370370370371
	},
	{
		id: 2372,
		time: 2371,
		velocity: 4.59027777777778,
		power: -3013.60967339369,
		road: 20063.6088425926,
		acceleration: -0.767870370370369
	},
	{
		id: 2373,
		time: 2372,
		velocity: 4.355,
		power: -1308.74753210257,
		road: 20068.068287037,
		acceleration: -0.447592592592593
	},
	{
		id: 2374,
		time: 2373,
		velocity: 3.76166666666667,
		power: -1813.38492780025,
		road: 20071.9925462963,
		acceleration: -0.622777777777778
	},
	{
		id: 2375,
		time: 2374,
		velocity: 2.72194444444444,
		power: -2493.40154499915,
		road: 20075.1187037037,
		acceleration: -0.973425925925926
	},
	{
		id: 2376,
		time: 2375,
		velocity: 1.43472222222222,
		power: -1498.74717446166,
		road: 20077.3368518518,
		acceleration: -0.842592592592593
	},
	{
		id: 2377,
		time: 2376,
		velocity: 1.23388888888889,
		power: -661.389953952846,
		road: 20078.8368055556,
		acceleration: -0.593796296296296
	},
	{
		id: 2378,
		time: 2377,
		velocity: 0.940555555555556,
		power: -72.3134867884684,
		road: 20079.94125,
		acceleration: -0.197222222222222
	},
	{
		id: 2379,
		time: 2378,
		velocity: 0.843055555555556,
		power: -80.1890791051875,
		road: 20080.8358333333,
		acceleration: -0.2225
	},
	{
		id: 2380,
		time: 2379,
		velocity: 0.566388888888889,
		power: -110.327052375528,
		road: 20081.4624074074,
		acceleration: -0.313518518518518
	},
	{
		id: 2381,
		time: 2380,
		velocity: 0,
		power: -47.87800651095,
		road: 20081.791712963,
		acceleration: -0.281018518518519
	},
	{
		id: 2382,
		time: 2381,
		velocity: 0,
		power: -5.47903412930475,
		road: 20081.8861111111,
		acceleration: -0.188796296296296
	},
	{
		id: 2383,
		time: 2382,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2384,
		time: 2383,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2385,
		time: 2384,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2386,
		time: 2385,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2387,
		time: 2386,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2388,
		time: 2387,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2389,
		time: 2388,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2390,
		time: 2389,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2391,
		time: 2390,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2392,
		time: 2391,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2393,
		time: 2392,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2394,
		time: 2393,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2395,
		time: 2394,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2396,
		time: 2395,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2397,
		time: 2396,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2398,
		time: 2397,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2399,
		time: 2398,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2400,
		time: 2399,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2401,
		time: 2400,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2402,
		time: 2401,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2403,
		time: 2402,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2404,
		time: 2403,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2405,
		time: 2404,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2406,
		time: 2405,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2407,
		time: 2406,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2408,
		time: 2407,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2409,
		time: 2408,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2410,
		time: 2409,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2411,
		time: 2410,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2412,
		time: 2411,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2413,
		time: 2412,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2414,
		time: 2413,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2415,
		time: 2414,
		velocity: 0,
		power: 0,
		road: 20081.8861111111,
		acceleration: 0
	},
	{
		id: 2416,
		time: 2415,
		velocity: 0,
		power: 165.115047309778,
		road: 20082.1511111111,
		acceleration: 0.53
	},
	{
		id: 2417,
		time: 2416,
		velocity: 1.59,
		power: 1084.79694073914,
		road: 20083.1755555556,
		acceleration: 0.988888888888889
	},
	{
		id: 2418,
		time: 2417,
		velocity: 2.96666666666667,
		power: 4274.27456189968,
		road: 20085.5698148148,
		acceleration: 1.75074074074074
	},
	{
		id: 2419,
		time: 2418,
		velocity: 5.25222222222222,
		power: 5116.08917465407,
		road: 20089.4633333333,
		acceleration: 1.24777777777778
	},
	{
		id: 2420,
		time: 2419,
		velocity: 5.33333333333333,
		power: 5462.17490692942,
		road: 20094.4825925926,
		acceleration: 1.0037037037037
	},
	{
		id: 2421,
		time: 2420,
		velocity: 5.97777777777778,
		power: 5846.51985798919,
		road: 20100.4456481481,
		acceleration: 0.883888888888889
	},
	{
		id: 2422,
		time: 2421,
		velocity: 7.90388888888889,
		power: 7544.99345884558,
		road: 20107.3481018518,
		acceleration: 0.994907407407407
	},
	{
		id: 2423,
		time: 2422,
		velocity: 8.31805555555555,
		power: 7415.51777893664,
		road: 20115.165462963,
		acceleration: 0.834907407407408
	},
	{
		id: 2424,
		time: 2423,
		velocity: 8.4825,
		power: 1948.6149090544,
		road: 20123.4409722222,
		acceleration: 0.0813888888888865
	},
	{
		id: 2425,
		time: 2424,
		velocity: 8.14805555555555,
		power: 394.453671908136,
		road: 20131.6993518518,
		acceleration: -0.115648148148146
	},
	{
		id: 2426,
		time: 2425,
		velocity: 7.97111111111111,
		power: -597.237895900632,
		road: 20139.7789814815,
		acceleration: -0.241851851851851
	},
	{
		id: 2427,
		time: 2426,
		velocity: 7.75694444444444,
		power: 1194.66874491018,
		road: 20147.7350462963,
		acceleration: -0.00527777777777771
	},
	{
		id: 2428,
		time: 2427,
		velocity: 8.13222222222222,
		power: 5865.10992296721,
		road: 20155.9793055555,
		acceleration: 0.581666666666666
	},
	{
		id: 2429,
		time: 2428,
		velocity: 9.71611111111111,
		power: 10345.6645407012,
		road: 20165.0278703704,
		acceleration: 1.02694444444444
	},
	{
		id: 2430,
		time: 2429,
		velocity: 10.8377777777778,
		power: 13279.7107464403,
		road: 20175.1831944444,
		acceleration: 1.18657407407408
	},
	{
		id: 2431,
		time: 2430,
		velocity: 11.6919444444444,
		power: 8810.35931683247,
		road: 20186.2510185185,
		acceleration: 0.638425925925926
	},
	{
		id: 2432,
		time: 2431,
		velocity: 11.6313888888889,
		power: 4707.32351555647,
		road: 20197.7516666667,
		acceleration: 0.22722222222222
	},
	{
		id: 2433,
		time: 2432,
		velocity: 11.5194444444444,
		power: 431.806661620953,
		road: 20209.2843518518,
		acceleration: -0.163148148148148
	},
	{
		id: 2434,
		time: 2433,
		velocity: 11.2025,
		power: -258.187176894599,
		road: 20220.6235648148,
		acceleration: -0.223796296296294
	},
	{
		id: 2435,
		time: 2434,
		velocity: 10.96,
		power: -814.421409514242,
		road: 20231.7139814815,
		acceleration: -0.273796296296297
	},
	{
		id: 2436,
		time: 2435,
		velocity: 10.6980555555556,
		power: 1618.47679187214,
		road: 20242.6477314815,
		acceleration: -0.0395370370370394
	},
	{
		id: 2437,
		time: 2436,
		velocity: 11.0838888888889,
		power: 4821.22188838505,
		road: 20253.6925462963,
		acceleration: 0.261666666666668
	},
	{
		id: 2438,
		time: 2437,
		velocity: 11.745,
		power: 7303.63307009439,
		road: 20265.1033796296,
		acceleration: 0.470370370370368
	},
	{
		id: 2439,
		time: 2438,
		velocity: 12.1091666666667,
		power: 6085.2874203518,
		road: 20276.9163888889,
		acceleration: 0.333981481481482
	},
	{
		id: 2440,
		time: 2439,
		velocity: 12.0858333333333,
		power: 3043.19549781331,
		road: 20288.9248611111,
		acceleration: 0.0569444444444454
	},
	{
		id: 2441,
		time: 2440,
		velocity: 11.9158333333333,
		power: 1508.86835844912,
		road: 20300.9234259259,
		acceleration: -0.0767592592592568
	},
	{
		id: 2442,
		time: 2441,
		velocity: 11.8788888888889,
		power: -981.646911720111,
		road: 20312.7369907407,
		acceleration: -0.293240740740742
	},
	{
		id: 2443,
		time: 2442,
		velocity: 11.2061111111111,
		power: -6392.27509025566,
		road: 20324.0069444444,
		acceleration: -0.793981481481481
	},
	{
		id: 2444,
		time: 2443,
		velocity: 9.53388888888889,
		power: -12911.6698913856,
		road: 20334.1170833333,
		acceleration: -1.52564814814815
	},
	{
		id: 2445,
		time: 2444,
		velocity: 7.30194444444444,
		power: -13258.6961955947,
		road: 20342.5550925926,
		acceleration: -1.81861111111111
	},
	{
		id: 2446,
		time: 2445,
		velocity: 5.75027777777778,
		power: -12654.8353223126,
		road: 20348.9709722222,
		acceleration: -2.22564814814815
	},
	{
		id: 2447,
		time: 2446,
		velocity: 2.85694444444444,
		power: -7236.53772637653,
		road: 20353.3309259259,
		acceleration: -1.8862037037037
	},
	{
		id: 2448,
		time: 2447,
		velocity: 1.64333333333333,
		power: -4164.25290676803,
		road: 20355.7893981481,
		acceleration: -1.91675925925926
	},
	{
		id: 2449,
		time: 2448,
		velocity: 0,
		power: -799.910652246727,
		road: 20356.8133333333,
		acceleration: -0.952314814814815
	},
	{
		id: 2450,
		time: 2449,
		velocity: 0,
		power: -109.043239181287,
		road: 20357.0872222222,
		acceleration: -0.547777777777778
	},
	{
		id: 2451,
		time: 2450,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2452,
		time: 2451,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2453,
		time: 2452,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2454,
		time: 2453,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2455,
		time: 2454,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2456,
		time: 2455,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2457,
		time: 2456,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2458,
		time: 2457,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2459,
		time: 2458,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2460,
		time: 2459,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2461,
		time: 2460,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2462,
		time: 2461,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2463,
		time: 2462,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2464,
		time: 2463,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2465,
		time: 2464,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2466,
		time: 2465,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2467,
		time: 2466,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2468,
		time: 2467,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2469,
		time: 2468,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2470,
		time: 2469,
		velocity: 0,
		power: 0,
		road: 20357.0872222222,
		acceleration: 0
	},
	{
		id: 2471,
		time: 2470,
		velocity: 0,
		power: 474.060429158628,
		road: 20357.5564351852,
		acceleration: 0.938425925925926
	},
	{
		id: 2472,
		time: 2471,
		velocity: 2.81527777777778,
		power: 2756.02254863043,
		road: 20359.2754629629,
		acceleration: 1.5612037037037
	},
	{
		id: 2473,
		time: 2472,
		velocity: 4.68361111111111,
		power: 6755.06748490604,
		road: 20362.7359722222,
		acceleration: 1.92175925925926
	},
	{
		id: 2474,
		time: 2473,
		velocity: 5.76527777777778,
		power: 5864.11224872614,
		road: 20367.7074074074,
		acceleration: 1.10009259259259
	},
	{
		id: 2475,
		time: 2474,
		velocity: 6.11555555555556,
		power: 5473.78305867704,
		road: 20373.6405092592,
		acceleration: 0.82324074074074
	},
	{
		id: 2476,
		time: 2475,
		velocity: 7.15333333333333,
		power: 6383.74299950988,
		road: 20380.4047685185,
		acceleration: 0.839074074074075
	},
	{
		id: 2477,
		time: 2476,
		velocity: 8.2825,
		power: 8965.77701086739,
		road: 20388.11875,
		acceleration: 1.06037037037037
	},
	{
		id: 2478,
		time: 2477,
		velocity: 9.29666666666667,
		power: 9774.98965163018,
		road: 20396.864537037,
		acceleration: 1.00324074074074
	},
	{
		id: 2479,
		time: 2478,
		velocity: 10.1630555555556,
		power: 8907.1408655704,
		road: 20406.5068518518,
		acceleration: 0.789814814814815
	},
	{
		id: 2480,
		time: 2479,
		velocity: 10.6519444444444,
		power: 9211.53244486505,
		road: 20416.9141666667,
		acceleration: 0.740185185185187
	},
	{
		id: 2481,
		time: 2480,
		velocity: 11.5172222222222,
		power: 6890.46123035412,
		road: 20427.9219907407,
		acceleration: 0.460833333333332
	},
	{
		id: 2482,
		time: 2481,
		velocity: 11.5455555555556,
		power: 3077.81816969791,
		road: 20439.2037037037,
		acceleration: 0.0869444444444447
	},
	{
		id: 2483,
		time: 2482,
		velocity: 10.9127777777778,
		power: -1604.17624725648,
		road: 20450.3546759259,
		acceleration: -0.348425925925927
	},
	{
		id: 2484,
		time: 2483,
		velocity: 10.4719444444444,
		power: -3928.68621883253,
		road: 20461.0426851852,
		acceleration: -0.577499999999999
	},
	{
		id: 2485,
		time: 2484,
		velocity: 9.81305555555556,
		power: -3347.6396274218,
		road: 20471.1759259259,
		acceleration: -0.532037037037037
	},
	{
		id: 2486,
		time: 2485,
		velocity: 9.31666666666667,
		power: -1127.1402752805,
		road: 20480.8919444444,
		acceleration: -0.302407407407408
	},
	{
		id: 2487,
		time: 2486,
		velocity: 9.56472222222222,
		power: 1733.42898874058,
		road: 20490.4623148148,
		acceleration: 0.0111111111111111
	},
	{
		id: 2488,
		time: 2487,
		velocity: 9.84638888888889,
		power: 3740.04610037072,
		road: 20500.1506944444,
		acceleration: 0.224907407407407
	},
	{
		id: 2489,
		time: 2488,
		velocity: 9.99138888888889,
		power: 1333.53941136899,
		road: 20509.9324074074,
		acceleration: -0.0382407407407417
	},
	{
		id: 2490,
		time: 2489,
		velocity: 9.45,
		power: -1228.63948440943,
		road: 20519.5381481481,
		acceleration: -0.313703703703702
	},
	{
		id: 2491,
		time: 2490,
		velocity: 8.90527777777778,
		power: -1620.78371586782,
		road: 20528.8073148148,
		acceleration: -0.359444444444444
	},
	{
		id: 2492,
		time: 2491,
		velocity: 8.91305555555556,
		power: 1173.89281973381,
		road: 20537.8778240741,
		acceleration: -0.0378703703703724
	},
	{
		id: 2493,
		time: 2492,
		velocity: 9.33638888888889,
		power: 3371.74073408072,
		road: 20547.0353703704,
		acceleration: 0.211944444444445
	},
	{
		id: 2494,
		time: 2493,
		velocity: 9.54111111111111,
		power: 4484.92172643122,
		road: 20556.4599537037,
		acceleration: 0.322129629629629
	},
	{
		id: 2495,
		time: 2494,
		velocity: 9.87944444444445,
		power: 5203.2752324461,
		road: 20566.2343518518,
		acceleration: 0.377500000000001
	},
	{
		id: 2496,
		time: 2495,
		velocity: 10.4688888888889,
		power: 7226.35719756631,
		road: 20576.4744907407,
		acceleration: 0.553981481481483
	},
	{
		id: 2497,
		time: 2496,
		velocity: 11.2030555555556,
		power: 7922.67575180186,
		road: 20587.2795833333,
		acceleration: 0.575925925925926
	},
	{
		id: 2498,
		time: 2497,
		velocity: 11.6072222222222,
		power: 5400.13612620598,
		road: 20598.5250925926,
		acceleration: 0.304907407407407
	},
	{
		id: 2499,
		time: 2498,
		velocity: 11.3836111111111,
		power: 3417.14983558028,
		road: 20609.9787962963,
		acceleration: 0.111481481481482
	},
	{
		id: 2500,
		time: 2499,
		velocity: 11.5375,
		power: 2837.78882418649,
		road: 20621.5159722222,
		acceleration: 0.055462962962963
	},
	{
		id: 2501,
		time: 2500,
		velocity: 11.7736111111111,
		power: 4080.00167134995,
		road: 20633.1625925926,
		acceleration: 0.163425925925925
	},
	{
		id: 2502,
		time: 2501,
		velocity: 11.8738888888889,
		power: 3830.7530881499,
		road: 20644.9582407407,
		acceleration: 0.134629629629629
	},
	{
		id: 2503,
		time: 2502,
		velocity: 11.9413888888889,
		power: 2510.96040729036,
		road: 20656.8286574074,
		acceleration: 0.0149074074074065
	},
	{
		id: 2504,
		time: 2503,
		velocity: 11.8183333333333,
		power: 3399.17180822084,
		road: 20668.7521759259,
		acceleration: 0.0912962962962975
	},
	{
		id: 2505,
		time: 2504,
		velocity: 12.1477777777778,
		power: 4567.95328171948,
		road: 20680.8150925926,
		acceleration: 0.1875
	},
	{
		id: 2506,
		time: 2505,
		velocity: 12.5038888888889,
		power: 6211.33124085915,
		road: 20693.1296296296,
		acceleration: 0.315740740740742
	},
	{
		id: 2507,
		time: 2506,
		velocity: 12.7655555555556,
		power: 4666.5795424155,
		road: 20705.6885648148,
		acceleration: 0.173055555555553
	},
	{
		id: 2508,
		time: 2507,
		velocity: 12.6669444444444,
		power: 4082.81867675622,
		road: 20718.3931944444,
		acceleration: 0.118333333333336
	},
	{
		id: 2509,
		time: 2508,
		velocity: 12.8588888888889,
		power: 4192.86138396983,
		road: 20731.2181944444,
		acceleration: 0.122407407407408
	},
	{
		id: 2510,
		time: 2509,
		velocity: 13.1327777777778,
		power: 5154.561484028,
		road: 20744.2011574074,
		acceleration: 0.193518518518516
	},
	{
		id: 2511,
		time: 2510,
		velocity: 13.2475,
		power: 4417.7865460727,
		road: 20757.344537037,
		acceleration: 0.127314814814817
	},
	{
		id: 2512,
		time: 2511,
		velocity: 13.2408333333333,
		power: 3546.24924864474,
		road: 20770.5788425926,
		acceleration: 0.0545370370370364
	},
	{
		id: 2513,
		time: 2512,
		velocity: 13.2963888888889,
		power: 2954.14772923834,
		road: 20783.84375,
		acceleration: 0.00666666666666416
	},
	{
		id: 2514,
		time: 2513,
		velocity: 13.2675,
		power: 3362.73892431401,
		road: 20797.1310648148,
		acceleration: 0.0381481481481512
	},
	{
		id: 2515,
		time: 2514,
		velocity: 13.3552777777778,
		power: 3523.63067220118,
		road: 20810.4620833333,
		acceleration: 0.0492592592592587
	},
	{
		id: 2516,
		time: 2515,
		velocity: 13.4441666666667,
		power: 2873.29284491961,
		road: 20823.8164351852,
		acceleration: -0.00259259259259359
	},
	{
		id: 2517,
		time: 2516,
		velocity: 13.2597222222222,
		power: 1370.40595595465,
		road: 20837.1099537037,
		acceleration: -0.119074074074074
	},
	{
		id: 2518,
		time: 2517,
		velocity: 12.9980555555556,
		power: 607.115233378519,
		road: 20850.2556944444,
		acceleration: -0.176481481481481
	},
	{
		id: 2519,
		time: 2518,
		velocity: 12.9147222222222,
		power: 101.355950532403,
		road: 20863.2062962963,
		acceleration: -0.213796296296296
	},
	{
		id: 2520,
		time: 2519,
		velocity: 12.6183333333333,
		power: -393.030847933878,
		road: 20875.9244907407,
		acceleration: -0.251018518518519
	},
	{
		id: 2521,
		time: 2520,
		velocity: 12.245,
		power: -1397.8937029225,
		road: 20888.3509722222,
		acceleration: -0.332407407407405
	},
	{
		id: 2522,
		time: 2521,
		velocity: 11.9175,
		power: -995.363681745792,
		road: 20900.4631018518,
		acceleration: -0.296296296296298
	},
	{
		id: 2523,
		time: 2522,
		velocity: 11.7294444444444,
		power: -960.45970352521,
		road: 20912.2813888889,
		acceleration: -0.291388888888889
	},
	{
		id: 2524,
		time: 2523,
		velocity: 11.3708333333333,
		power: -1184.7635991546,
		road: 20923.7989351852,
		acceleration: -0.310092592592593
	},
	{
		id: 2525,
		time: 2524,
		velocity: 10.9872222222222,
		power: -2073.5132518825,
		road: 20934.9651851852,
		acceleration: -0.3925
	},
	{
		id: 2526,
		time: 2525,
		velocity: 10.5519444444444,
		power: -965.250679405465,
		road: 20945.7916666667,
		acceleration: -0.287037037037036
	},
	{
		id: 2527,
		time: 2526,
		velocity: 10.5097222222222,
		power: -1978.86386688056,
		road: 20956.2809259259,
		acceleration: -0.387407407407407
	},
	{
		id: 2528,
		time: 2527,
		velocity: 9.825,
		power: -3395.25378592244,
		road: 20966.3067129629,
		acceleration: -0.539537037037038
	},
	{
		id: 2529,
		time: 2528,
		velocity: 8.93333333333333,
		power: -4863.04781037818,
		road: 20975.7024074074,
		acceleration: -0.720648148148149
	},
	{
		id: 2530,
		time: 2529,
		velocity: 8.34777777777778,
		power: -4379.10186387873,
		road: 20984.3880092592,
		acceleration: -0.699537037037036
	},
	{
		id: 2531,
		time: 2530,
		velocity: 7.72638888888889,
		power: -3400.8085103965,
		road: 20992.4194907407,
		acceleration: -0.608703703703704
	},
	{
		id: 2532,
		time: 2531,
		velocity: 7.10722222222222,
		power: -3619.92024561639,
		road: 20999.8100925926,
		acceleration: -0.673055555555557
	},
	{
		id: 2533,
		time: 2532,
		velocity: 6.32861111111111,
		power: -4102.34162347232,
		road: 21006.4637962963,
		acceleration: -0.80074074074074
	},
	{
		id: 2534,
		time: 2533,
		velocity: 5.32416666666667,
		power: -3789.26505693424,
		road: 21012.3024074074,
		acceleration: -0.829444444444444
	},
	{
		id: 2535,
		time: 2534,
		velocity: 4.61888888888889,
		power: -4487.99496699973,
		road: 21017.1706481481,
		acceleration: -1.1112962962963
	},
	{
		id: 2536,
		time: 2535,
		velocity: 2.99472222222222,
		power: -4180.78602985599,
		road: 21020.810787037,
		acceleration: -1.34490740740741
	},
	{
		id: 2537,
		time: 2536,
		velocity: 1.28944444444444,
		power: -2937.830679192,
		road: 21023.0086574074,
		acceleration: -1.53962962962963
	},
	{
		id: 2538,
		time: 2537,
		velocity: 0,
		power: -766.170477710798,
		road: 21023.9375925926,
		acceleration: -0.998240740740741
	},
	{
		id: 2539,
		time: 2538,
		velocity: 0,
		power: -61.5441276478233,
		road: 21024.1525,
		acceleration: -0.429814814814815
	},
	{
		id: 2540,
		time: 2539,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2541,
		time: 2540,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2542,
		time: 2541,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2543,
		time: 2542,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2544,
		time: 2543,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2545,
		time: 2544,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2546,
		time: 2545,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2547,
		time: 2546,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2548,
		time: 2547,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2549,
		time: 2548,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2550,
		time: 2549,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2551,
		time: 2550,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2552,
		time: 2551,
		velocity: 0,
		power: 0,
		road: 21024.1525,
		acceleration: 0
	},
	{
		id: 2553,
		time: 2552,
		velocity: 0,
		power: 114.580301991312,
		road: 21024.3685648148,
		acceleration: 0.43212962962963
	},
	{
		id: 2554,
		time: 2553,
		velocity: 1.29638888888889,
		power: 1270.071250763,
		road: 21025.3914814815,
		acceleration: 1.18157407407407
	},
	{
		id: 2555,
		time: 2554,
		velocity: 3.54472222222222,
		power: 4151.91860944849,
		road: 21027.8350925926,
		acceleration: 1.65981481481481
	},
	{
		id: 2556,
		time: 2555,
		velocity: 4.97944444444444,
		power: 7516.80568424159,
		road: 21031.9919444444,
		acceleration: 1.76666666666667
	},
	{
		id: 2557,
		time: 2556,
		velocity: 6.59638888888889,
		power: 6406.40511819086,
		road: 21037.5644444444,
		acceleration: 1.06462962962963
	},
	{
		id: 2558,
		time: 2557,
		velocity: 6.73861111111111,
		power: 5767.14181048569,
		road: 21044.0604629629,
		acceleration: 0.782407407407408
	},
	{
		id: 2559,
		time: 2558,
		velocity: 7.32666666666667,
		power: 4475.00621353712,
		road: 21051.1991203703,
		acceleration: 0.502870370370371
	},
	{
		id: 2560,
		time: 2559,
		velocity: 8.105,
		power: 2997.06391173774,
		road: 21058.7190277778,
		acceleration: 0.259629629629629
	},
	{
		id: 2561,
		time: 2560,
		velocity: 7.5175,
		power: 270.244451692757,
		road: 21066.3075462963,
		acceleration: -0.122407407407408
	},
	{
		id: 2562,
		time: 2561,
		velocity: 6.95944444444444,
		power: -3236.48446686963,
		road: 21073.5206481481,
		acceleration: -0.628425925925926
	},
	{
		id: 2563,
		time: 2562,
		velocity: 6.21972222222222,
		power: -2683.46826449339,
		road: 21080.1300462963,
		acceleration: -0.578981481481482
	},
	{
		id: 2564,
		time: 2563,
		velocity: 5.78055555555556,
		power: -2010.98930469859,
		road: 21086.2016666666,
		acceleration: -0.496574074074073
	},
	{
		id: 2565,
		time: 2564,
		velocity: 5.46972222222222,
		power: -1305.42879790073,
		road: 21091.8303703703,
		acceleration: -0.38925925925926
	},
	{
		id: 2566,
		time: 2565,
		velocity: 5.05194444444444,
		power: -1290.32073261064,
		road: 21097.063287037,
		acceleration: -0.402314814814813
	},
	{
		id: 2567,
		time: 2566,
		velocity: 4.57361111111111,
		power: -703.973462533614,
		road: 21101.9487962963,
		acceleration: -0.2925
	},
	{
		id: 2568,
		time: 2567,
		velocity: 4.59222222222222,
		power: -93.9729690893246,
		road: 21106.6076388889,
		acceleration: -0.160833333333334
	},
	{
		id: 2569,
		time: 2568,
		velocity: 4.56944444444444,
		power: -185.424099844962,
		road: 21111.0949537037,
		acceleration: -0.182222222222221
	},
	{
		id: 2570,
		time: 2569,
		velocity: 4.02694444444444,
		power: -819.476443671344,
		road: 21115.3203240741,
		acceleration: -0.341666666666668
	},
	{
		id: 2571,
		time: 2570,
		velocity: 3.56722222222222,
		power: -1071.21198546476,
		road: 21119.1600925926,
		acceleration: -0.429537037037036
	},
	{
		id: 2572,
		time: 2571,
		velocity: 3.28083333333333,
		power: -458.159579661678,
		road: 21122.6487962963,
		acceleration: -0.272592592592593
	},
	{
		id: 2573,
		time: 2572,
		velocity: 3.20916666666667,
		power: -362.335424148666,
		road: 21125.8754166666,
		acceleration: -0.251574074074074
	},
	{
		id: 2574,
		time: 2573,
		velocity: 2.8125,
		power: -251.643455541942,
		road: 21128.8656944444,
		acceleration: -0.221111111111111
	},
	{
		id: 2575,
		time: 2574,
		velocity: 2.6175,
		power: -106.187981511916,
		road: 21131.6594907407,
		acceleration: -0.171851851851852
	},
	{
		id: 2576,
		time: 2575,
		velocity: 2.69361111111111,
		power: 139.142971837737,
		road: 21134.3291203703,
		acceleration: -0.0764814814814816
	},
	{
		id: 2577,
		time: 2576,
		velocity: 2.58305555555556,
		power: 411.841419841513,
		road: 21136.9768055555,
		acceleration: 0.032592592592593
	},
	{
		id: 2578,
		time: 2577,
		velocity: 2.71527777777778,
		power: -114.614973165066,
		road: 21139.5517592592,
		acceleration: -0.178055555555556
	},
	{
		id: 2579,
		time: 2578,
		velocity: 2.15944444444444,
		power: -30.0117256579123,
		road: 21141.965787037,
		acceleration: -0.143796296296296
	},
	{
		id: 2580,
		time: 2579,
		velocity: 2.15166666666667,
		power: 345.058592021084,
		road: 21144.3199074074,
		acceleration: 0.0239814814814814
	},
	{
		id: 2581,
		time: 2580,
		velocity: 2.78722222222222,
		power: 1298.09650684301,
		road: 21146.8869444444,
		acceleration: 0.401851851851852
	},
	{
		id: 2582,
		time: 2581,
		velocity: 3.365,
		power: 2059.32051952533,
		road: 21149.9435185185,
		acceleration: 0.577222222222222
	},
	{
		id: 2583,
		time: 2582,
		velocity: 3.88333333333333,
		power: 1114.16935683888,
		road: 21153.3918518518,
		acceleration: 0.206296296296296
	},
	{
		id: 2584,
		time: 2583,
		velocity: 3.40611111111111,
		power: 958.566440342434,
		road: 21157.0152777778,
		acceleration: 0.143888888888889
	},
	{
		id: 2585,
		time: 2584,
		velocity: 3.79666666666667,
		power: -858.531844644861,
		road: 21160.5142592592,
		acceleration: -0.392777777777777
	},
	{
		id: 2586,
		time: 2585,
		velocity: 2.705,
		power: -1351.04504753207,
		road: 21163.5132407407,
		acceleration: -0.607222222222223
	},
	{
		id: 2587,
		time: 2586,
		velocity: 1.58444444444444,
		power: -1817.18826239505,
		road: 21165.7068055555,
		acceleration: -1.00361111111111
	},
	{
		id: 2588,
		time: 2587,
		velocity: 0.785833333333333,
		power: -909.66539932613,
		road: 21166.9477314815,
		acceleration: -0.901666666666667
	},
	{
		id: 2589,
		time: 2588,
		velocity: 0,
		power: -199.621783495372,
		road: 21167.47375,
		acceleration: -0.528148148148148
	},
	{
		id: 2590,
		time: 2589,
		velocity: 0,
		power: -16.6780027777778,
		road: 21167.6047222222,
		acceleration: -0.261944444444444
	},
	{
		id: 2591,
		time: 2590,
		velocity: 0,
		power: 0,
		road: 21167.6047222222,
		acceleration: 0
	},
	{
		id: 2592,
		time: 2591,
		velocity: 0,
		power: 0,
		road: 21167.6047222222,
		acceleration: 0
	},
	{
		id: 2593,
		time: 2592,
		velocity: 0,
		power: 0,
		road: 21167.6047222222,
		acceleration: 0
	},
	{
		id: 2594,
		time: 2593,
		velocity: 0,
		power: 0,
		road: 21167.6047222222,
		acceleration: 0
	},
	{
		id: 2595,
		time: 2594,
		velocity: 0,
		power: 54.8746077380026,
		road: 21167.7459722222,
		acceleration: 0.2825
	},
	{
		id: 2596,
		time: 2595,
		velocity: 0.8475,
		power: 516.135051451513,
		road: 21168.3884259259,
		acceleration: 0.719907407407407
	},
	{
		id: 2597,
		time: 2596,
		velocity: 2.15972222222222,
		power: 1451.42988696325,
		road: 21169.8500925926,
		acceleration: 0.918518518518519
	},
	{
		id: 2598,
		time: 2597,
		velocity: 2.75555555555556,
		power: 1639.9724111797,
		road: 21172.0915277777,
		acceleration: 0.641018518518519
	},
	{
		id: 2599,
		time: 2598,
		velocity: 2.77055555555556,
		power: 724.15434821748,
		road: 21174.7323148148,
		acceleration: 0.157685185185185
	},
	{
		id: 2600,
		time: 2599,
		velocity: 2.63277777777778,
		power: 41.405198433336,
		road: 21177.3944444444,
		acceleration: -0.115
	},
	{
		id: 2601,
		time: 2600,
		velocity: 2.41055555555556,
		power: -5.53889769985356,
		road: 21179.9324074074,
		acceleration: -0.133333333333333
	},
	{
		id: 2602,
		time: 2601,
		velocity: 2.37055555555556,
		power: 90.5082863450377,
		road: 21182.3580092592,
		acceleration: -0.0913888888888894
	},
	{
		id: 2603,
		time: 2602,
		velocity: 2.35861111111111,
		power: 201.436242772139,
		road: 21184.7176388889,
		acceleration: -0.0405555555555557
	},
	{
		id: 2604,
		time: 2603,
		velocity: 2.28888888888889,
		power: 330.980836589692,
		road: 21187.0660185185,
		acceleration: 0.0180555555555557
	},
	{
		id: 2605,
		time: 2604,
		velocity: 2.42472222222222,
		power: 454.256571612739,
		road: 21189.4581944444,
		acceleration: 0.069537037037037
	},
	{
		id: 2606,
		time: 2605,
		velocity: 2.56722222222222,
		power: 878.878460716544,
		road: 21192.0017129629,
		acceleration: 0.233148148148148
	},
	{
		id: 2607,
		time: 2606,
		velocity: 2.98833333333333,
		power: 1034.62503676634,
		road: 21194.7913425926,
		acceleration: 0.259074074074074
	},
	{
		id: 2608,
		time: 2607,
		velocity: 3.20194444444444,
		power: 1335.84256176881,
		road: 21197.8725462963,
		acceleration: 0.324074074074074
	},
	{
		id: 2609,
		time: 2608,
		velocity: 3.53944444444444,
		power: 854.253131291217,
		road: 21201.1848611111,
		acceleration: 0.138148148148149
	},
	{
		id: 2610,
		time: 2609,
		velocity: 3.40277777777778,
		power: 273.930491014717,
		road: 21204.5423611111,
		acceleration: -0.0477777777777781
	},
	{
		id: 2611,
		time: 2610,
		velocity: 3.05861111111111,
		power: 142.122807815802,
		road: 21207.8319907407,
		acceleration: -0.0879629629629632
	},
	{
		id: 2612,
		time: 2611,
		velocity: 3.27555555555556,
		power: 1421.75464489486,
		road: 21211.2310185185,
		acceleration: 0.30675925925926
	},
	{
		id: 2613,
		time: 2612,
		velocity: 4.32305555555556,
		power: 2145.56510975315,
		road: 21215.0143518518,
		acceleration: 0.461851851851852
	},
	{
		id: 2614,
		time: 2613,
		velocity: 4.44416666666667,
		power: 1967.46149131632,
		road: 21219.207037037,
		acceleration: 0.356851851851852
	},
	{
		id: 2615,
		time: 2614,
		velocity: 4.34611111111111,
		power: 1270.59863964624,
		road: 21223.6591203703,
		acceleration: 0.161944444444445
	},
	{
		id: 2616,
		time: 2615,
		velocity: 4.80888888888889,
		power: 2180.80689687025,
		road: 21228.3661111111,
		acceleration: 0.347870370370369
	},
	{
		id: 2617,
		time: 2616,
		velocity: 5.48777777777778,
		power: 2236.84836667396,
		road: 21233.4095833333,
		acceleration: 0.325092592592592
	},
	{
		id: 2618,
		time: 2617,
		velocity: 5.32138888888889,
		power: 1807.5633368483,
		road: 21238.7229629629,
		acceleration: 0.214722222222222
	},
	{
		id: 2619,
		time: 2618,
		velocity: 5.45305555555556,
		power: 288.847340969414,
		road: 21244.1001388889,
		acceleration: -0.0871296296296293
	},
	{
		id: 2620,
		time: 2619,
		velocity: 5.22638888888889,
		power: 327.079723768825,
		road: 21249.3946759259,
		acceleration: -0.0781481481481485
	},
	{
		id: 2621,
		time: 2620,
		velocity: 5.08694444444444,
		power: -14.1251442012851,
		road: 21254.5774537037,
		acceleration: -0.14537037037037
	},
	{
		id: 2622,
		time: 2621,
		velocity: 5.01694444444444,
		power: -137.818496698364,
		road: 21259.6023148148,
		acceleration: -0.170462962962963
	},
	{
		id: 2623,
		time: 2622,
		velocity: 4.715,
		power: -215.904205020519,
		road: 21264.4481944444,
		acceleration: -0.1875
	},
	{
		id: 2624,
		time: 2623,
		velocity: 4.52444444444444,
		power: -268.692575415661,
		road: 21269.1001388889,
		acceleration: -0.20037037037037
	},
	{
		id: 2625,
		time: 2624,
		velocity: 4.41583333333333,
		power: -399.263169602091,
		road: 21273.5352777778,
		acceleration: -0.23324074074074
	},
	{
		id: 2626,
		time: 2625,
		velocity: 4.01527777777778,
		power: -1257.14425163115,
		road: 21277.6234722222,
		acceleration: -0.460648148148148
	},
	{
		id: 2627,
		time: 2626,
		velocity: 3.1425,
		power: -2275.9948131132,
		road: 21281.0660185185,
		acceleration: -0.830648148148148
	},
	{
		id: 2628,
		time: 2627,
		velocity: 1.92388888888889,
		power: -1561.30912690515,
		road: 21283.7171759259,
		acceleration: -0.75212962962963
	},
	{
		id: 2629,
		time: 2628,
		velocity: 1.75888888888889,
		power: -894.627970743543,
		road: 21285.6881481481,
		acceleration: -0.608240740740741
	},
	{
		id: 2630,
		time: 2629,
		velocity: 1.31777777777778,
		power: -176.300599245356,
		road: 21287.2303240741,
		acceleration: -0.249351851851852
	},
	{
		id: 2631,
		time: 2630,
		velocity: 1.17583333333333,
		power: 136.288653667382,
		road: 21288.6347222222,
		acceleration: -0.0262037037037035
	},
	{
		id: 2632,
		time: 2631,
		velocity: 1.68027777777778,
		power: 601.92773839508,
		road: 21290.1685648148,
		acceleration: 0.285092592592593
	},
	{
		id: 2633,
		time: 2632,
		velocity: 2.17305555555556,
		power: 726.246740876324,
		road: 21291.9904629629,
		acceleration: 0.291018518518518
	},
	{
		id: 2634,
		time: 2633,
		velocity: 2.04888888888889,
		power: 785.968906439923,
		road: 21294.0902314815,
		acceleration: 0.264722222222222
	},
	{
		id: 2635,
		time: 2634,
		velocity: 2.47444444444444,
		power: 1015.98983340056,
		road: 21296.4810185185,
		acceleration: 0.317314814814815
	},
	{
		id: 2636,
		time: 2635,
		velocity: 3.125,
		power: 2001.95871311108,
		road: 21299.3341666666,
		acceleration: 0.607407407407407
	},
	{
		id: 2637,
		time: 2636,
		velocity: 3.87111111111111,
		power: 2277.35262705096,
		road: 21302.7728240741,
		acceleration: 0.563611111111111
	},
	{
		id: 2638,
		time: 2637,
		velocity: 4.16527777777778,
		power: 1689.39029492495,
		road: 21306.654537037,
		acceleration: 0.3225
	},
	{
		id: 2639,
		time: 2638,
		velocity: 4.0925,
		power: 210.468825789404,
		road: 21310.6569907407,
		acceleration: -0.0810185185185182
	},
	{
		id: 2640,
		time: 2639,
		velocity: 3.62805555555556,
		power: -820.81644448844,
		road: 21314.4368518518,
		acceleration: -0.364166666666667
	},
	{
		id: 2641,
		time: 2640,
		velocity: 3.07277777777778,
		power: -1147.42537893458,
		road: 21317.7873611111,
		acceleration: -0.494537037037037
	},
	{
		id: 2642,
		time: 2641,
		velocity: 2.60888888888889,
		power: -428.669746127254,
		road: 21320.7481481481,
		acceleration: -0.284907407407408
	},
	{
		id: 2643,
		time: 2642,
		velocity: 2.77333333333333,
		power: 272.358346827868,
		road: 21323.5517592592,
		acceleration: -0.0294444444444446
	},
	{
		id: 2644,
		time: 2643,
		velocity: 2.98444444444444,
		power: 599.094089651517,
		road: 21326.3860648148,
		acceleration: 0.0908333333333338
	},
	{
		id: 2645,
		time: 2644,
		velocity: 2.88138888888889,
		power: 160.658156696457,
		road: 21329.2295833333,
		acceleration: -0.0724074074074075
	},
	{
		id: 2646,
		time: 2645,
		velocity: 2.55611111111111,
		power: -172.823431602909,
		road: 21331.9375,
		acceleration: -0.198796296296296
	},
	{
		id: 2647,
		time: 2646,
		velocity: 2.38805555555556,
		power: -446.949265865103,
		road: 21334.3843518518,
		acceleration: -0.323333333333333
	},
	{
		id: 2648,
		time: 2647,
		velocity: 1.91138888888889,
		power: -142.976578254973,
		road: 21336.57,
		acceleration: -0.199074074074074
	},
	{
		id: 2649,
		time: 2648,
		velocity: 1.95888888888889,
		power: -228.122192909641,
		road: 21338.5299537037,
		acceleration: -0.252314814814815
	},
	{
		id: 2650,
		time: 2649,
		velocity: 1.63111111111111,
		power: 283.469635383181,
		road: 21340.3798611111,
		acceleration: 0.0322222222222222
	},
	{
		id: 2651,
		time: 2650,
		velocity: 2.00805555555556,
		power: 708.135651193718,
		road: 21342.36875,
		acceleration: 0.245740740740741
	},
	{
		id: 2652,
		time: 2651,
		velocity: 2.69611111111111,
		power: 1387.66631138899,
		road: 21344.7255555555,
		acceleration: 0.490092592592593
	},
	{
		id: 2653,
		time: 2652,
		velocity: 3.10138888888889,
		power: 1327.73031737835,
		road: 21347.5125462963,
		acceleration: 0.370277777777778
	},
	{
		id: 2654,
		time: 2653,
		velocity: 3.11888888888889,
		power: 792.338684020428,
		road: 21350.5555555555,
		acceleration: 0.141759259259259
	},
	{
		id: 2655,
		time: 2654,
		velocity: 3.12138888888889,
		power: -113.707836776683,
		road: 21353.5833796296,
		acceleration: -0.172129629629629
	},
	{
		id: 2656,
		time: 2655,
		velocity: 2.585,
		power: -47.6387185849026,
		road: 21356.4503703703,
		acceleration: -0.149537037037037
	},
	{
		id: 2657,
		time: 2656,
		velocity: 2.67027777777778,
		power: -294.595464086895,
		road: 21359.1187037037,
		acceleration: -0.247777777777778
	},
	{
		id: 2658,
		time: 2657,
		velocity: 2.37805555555556,
		power: 3.1355421926849,
		road: 21361.5983796296,
		acceleration: -0.129537037037037
	},
	{
		id: 2659,
		time: 2658,
		velocity: 2.19638888888889,
		power: 279.373204907557,
		road: 21364.0090277778,
		acceleration: -0.00851851851851837
	},
	{
		id: 2660,
		time: 2659,
		velocity: 2.64472222222222,
		power: 627.631513595012,
		road: 21366.4836574074,
		acceleration: 0.136481481481481
	},
	{
		id: 2661,
		time: 2660,
		velocity: 2.7875,
		power: 640.35073670529,
		road: 21369.0903703703,
		acceleration: 0.127685185185185
	},
	{
		id: 2662,
		time: 2661,
		velocity: 2.57944444444444,
		power: 110.762776591441,
		road: 21371.7175,
		acceleration: -0.086851851851852
	},
	{
		id: 2663,
		time: 2662,
		velocity: 2.38416666666667,
		power: -301.162404566523,
		road: 21374.1711111111,
		acceleration: -0.260185185185185
	},
	{
		id: 2664,
		time: 2663,
		velocity: 2.00694444444444,
		power: -309.70717174867,
		road: 21376.3548148148,
		acceleration: -0.27962962962963
	},
	{
		id: 2665,
		time: 2664,
		velocity: 1.74055555555556,
		power: -563.051269548562,
		road: 21378.1705555555,
		acceleration: -0.456296296296296
	},
	{
		id: 2666,
		time: 2665,
		velocity: 1.01527777777778,
		power: -333.954627238651,
		road: 21379.567824074,
		acceleration: -0.380648148148148
	},
	{
		id: 2667,
		time: 2666,
		velocity: 0.865,
		power: -392.979182995488,
		road: 21380.4846759259,
		acceleration: -0.580185185185185
	},
	{
		id: 2668,
		time: 2667,
		velocity: 0,
		power: -91.3953339863271,
		road: 21380.9422222222,
		acceleration: -0.338425925925926
	},
	{
		id: 2669,
		time: 2668,
		velocity: 0,
		power: -21.96235,
		road: 21381.0863888889,
		acceleration: -0.288333333333333
	},
	{
		id: 2670,
		time: 2669,
		velocity: 0,
		power: 0,
		road: 21381.0863888889,
		acceleration: 0
	},
	{
		id: 2671,
		time: 2670,
		velocity: 0,
		power: 0,
		road: 21381.0863888889,
		acceleration: 0
	},
	{
		id: 2672,
		time: 2671,
		velocity: 0,
		power: 0,
		road: 21381.0863888889,
		acceleration: 0
	},
	{
		id: 2673,
		time: 2672,
		velocity: 0,
		power: 0,
		road: 21381.0863888889,
		acceleration: 0
	},
	{
		id: 2674,
		time: 2673,
		velocity: 0,
		power: 0.8626766796299,
		road: 21381.0928703703,
		acceleration: 0.012962962962963
	},
	{
		id: 2675,
		time: 2674,
		velocity: 0.0388888888888889,
		power: 1.5661590772585,
		road: 21381.1058333333,
		acceleration: 0
	},
	{
		id: 2676,
		time: 2675,
		velocity: 0,
		power: 11.3239888778826,
		road: 21381.1618055555,
		acceleration: 0.0860185185185185
	},
	{
		id: 2677,
		time: 2676,
		velocity: 0.258055555555556,
		power: 10.0400619048533,
		road: 21381.2543055555,
		acceleration: -0.012962962962963
	},
	{
		id: 2678,
		time: 2677,
		velocity: 0,
		power: 10.3929218349525,
		road: 21381.340324074,
		acceleration: 0
	},
	{
		id: 2679,
		time: 2678,
		velocity: 0,
		power: 5.7080112215734,
		road: 21381.4074537037,
		acceleration: -0.0377777777777778
	},
	{
		id: 2680,
		time: 2679,
		velocity: 0.144722222222222,
		power: 5.82840568185411,
		road: 21381.4556944444,
		acceleration: 0
	},
	{
		id: 2681,
		time: 2680,
		velocity: 0,
		power: 5.82840568185411,
		road: 21381.5039351852,
		acceleration: 0
	},
	{
		id: 2682,
		time: 2681,
		velocity: 0,
		power: 1.81182912605588,
		road: 21381.5280555555,
		acceleration: -0.0482407407407407
	},
	{
		id: 2683,
		time: 2682,
		velocity: 0,
		power: 0,
		road: 21381.5280555555,
		acceleration: 0
	},
	{
		id: 2684,
		time: 2683,
		velocity: 0,
		power: 0,
		road: 21381.5280555555,
		acceleration: 0
	},
	{
		id: 2685,
		time: 2684,
		velocity: 0,
		power: 0,
		road: 21381.5280555555,
		acceleration: 0
	},
	{
		id: 2686,
		time: 2685,
		velocity: 0,
		power: 0,
		road: 21381.5280555555,
		acceleration: 0
	},
	{
		id: 2687,
		time: 2686,
		velocity: 0,
		power: 0,
		road: 21381.5280555555,
		acceleration: 0
	}
];
export default test7;
