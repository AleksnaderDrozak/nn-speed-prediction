export const test1 = [
	{
		id: 1,
		time: 0,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 3,
		time: 2,
		velocity: 0,
		power: 12.9464929958995,
		road: 0.056712962962963,
		acceleration: 0.113425925925926
	},
	{
		id: 4,
		time: 3,
		velocity: 0.340277777777778,
		power: 13.7046737760745,
		road: 0.170138888888889,
		acceleration: 0
	},
	{
		id: 5,
		time: 4,
		velocity: 0,
		power: 13.7046737760745,
		road: 0.283564814814815,
		acceleration: 0
	},
	{
		id: 6,
		time: 5,
		velocity: 0,
		power: 0.757784681611436,
		road: 0.340277777777778,
		acceleration: -0.113425925925926
	},
	{
		id: 7,
		time: 6,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 8,
		time: 7,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 9,
		time: 8,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 10,
		time: 9,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 11,
		time: 10,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 12,
		time: 11,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 13,
		time: 12,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 14,
		time: 13,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 15,
		time: 14,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 16,
		time: 15,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 17,
		time: 16,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 18,
		time: 17,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 19,
		time: 18,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 20,
		time: 19,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 21,
		time: 20,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 22,
		time: 21,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 23,
		time: 22,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 24,
		time: 23,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 25,
		time: 24,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 26,
		time: 25,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 27,
		time: 26,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 28,
		time: 27,
		velocity: 0,
		power: 0,
		road: 0.340277777777778,
		acceleration: 0
	},
	{
		id: 29,
		time: 28,
		velocity: 0,
		power: 45.6523883820375,
		road: 0.466851851851852,
		acceleration: 0.253148148148148
	},
	{
		id: 30,
		time: 29,
		velocity: 0.759444444444444,
		power: 133.712448240281,
		road: 0.843518518518519,
		acceleration: 0.247037037037037
	},
	{
		id: 31,
		time: 30,
		velocity: 0.741111111111111,
		power: 385.974870135865,
		road: 1.56287037037037,
		acceleration: 0.438333333333333
	},
	{
		id: 32,
		time: 31,
		velocity: 1.315,
		power: 455.92134310302,
		road: 2.65708333333333,
		acceleration: 0.311388888888889
	},
	{
		id: 33,
		time: 32,
		velocity: 1.69361111111111,
		power: 322.926153554666,
		road: 3.97226851851852,
		acceleration: 0.130555555555556
	},
	{
		id: 34,
		time: 33,
		velocity: 1.13277777777778,
		power: 111.058337783092,
		road: 5.33157407407407,
		acceleration: -0.0423148148148147
	},
	{
		id: 35,
		time: 34,
		velocity: 1.18805555555556,
		power: -26.9107633155346,
		road: 6.59430555555556,
		acceleration: -0.150833333333334
	},
	{
		id: 36,
		time: 35,
		velocity: 1.24111111111111,
		power: 217.872383364909,
		road: 7.81185185185185,
		acceleration: 0.0604629629629629
	},
	{
		id: 37,
		time: 36,
		velocity: 1.31416666666667,
		power: 14.1176145178197,
		road: 9.00175925925926,
		acceleration: -0.11574074074074
	},
	{
		id: 38,
		time: 37,
		velocity: 0.840833333333333,
		power: -82.1028367948182,
		road: 10.0275462962963,
		acceleration: -0.2125
	},
	{
		id: 39,
		time: 38,
		velocity: 0.603611111111111,
		power: -205.989129104326,
		road: 10.7280555555556,
		acceleration: -0.438055555555556
	},
	{
		id: 40,
		time: 39,
		velocity: 0,
		power: -5.26125851672242,
		road: 11.1389814814815,
		acceleration: -0.141111111111111
	},
	{
		id: 41,
		time: 40,
		velocity: 0.4175,
		power: 39.8375962160852,
		road: 11.4776388888889,
		acceleration: -0.00342592592592594
	},
	{
		id: 42,
		time: 41,
		velocity: 0.593333333333333,
		power: 85.0792936981215,
		road: 11.8662962962963,
		acceleration: 0.103425925925926
	},
	{
		id: 43,
		time: 42,
		velocity: 0.310277777777778,
		power: -4.0693726475767,
		road: 12.2370833333333,
		acceleration: -0.139166666666667
	},
	{
		id: 44,
		time: 43,
		velocity: 0,
		power: 24.1015852846249,
		road: 12.519537037037,
		acceleration: -0.0375
	},
	{
		id: 45,
		time: 44,
		velocity: 0.480833333333333,
		power: 53.4253982594357,
		road: 12.8149074074074,
		acceleration: 0.0633333333333333
	},
	{
		id: 46,
		time: 45,
		velocity: 0.500277777777778,
		power: 130.502721220432,
		road: 13.2400925925926,
		acceleration: 0.196296296296296
	},
	{
		id: 47,
		time: 46,
		velocity: 0.588888888888889,
		power: 175.297957000789,
		road: 13.8509722222222,
		acceleration: 0.175092592592593
	},
	{
		id: 48,
		time: 47,
		velocity: 1.00611111111111,
		power: -22.7635452007044,
		road: 14.4660185185185,
		acceleration: -0.166759259259259
	},
	{
		id: 49,
		time: 48,
		velocity: 0,
		power: -28.2159685707039,
		road: 14.899537037037,
		acceleration: -0.196296296296296
	},
	{
		id: 50,
		time: 49,
		velocity: 0,
		power: -33.0174483105913,
		road: 15.0672222222222,
		acceleration: -0.33537037037037
	},
	{
		id: 51,
		time: 50,
		velocity: 0,
		power: 22.3456962247617,
		road: 15.1485185185185,
		acceleration: 0.162592592592593
	},
	{
		id: 52,
		time: 51,
		velocity: 0.487777777777778,
		power: 19.6464281870153,
		road: 15.3111111111111,
		acceleration: 0
	},
	{
		id: 53,
		time: 52,
		velocity: 0,
		power: 577.475903382017,
		road: 15.9146296296296,
		acceleration: 0.881851851851852
	},
	{
		id: 54,
		time: 53,
		velocity: 2.64555555555556,
		power: 1711.29810379941,
		road: 17.4734722222222,
		acceleration: 1.0287962962963
	},
	{
		id: 55,
		time: 54,
		velocity: 3.57416666666667,
		power: 3851.17681245365,
		road: 20.2197222222222,
		acceleration: 1.34601851851852
	},
	{
		id: 56,
		time: 55,
		velocity: 4.03805555555556,
		power: 2440.933726239,
		road: 23.9189351851852,
		acceleration: 0.559907407407407
	},
	{
		id: 57,
		time: 56,
		velocity: 4.32527777777778,
		power: 1172.77645159144,
		road: 27.9817592592593,
		acceleration: 0.167314814814814
	},
	{
		id: 58,
		time: 57,
		velocity: 4.07611111111111,
		power: 137.453079668301,
		road: 32.0775,
		acceleration: -0.101481481481481
	},
	{
		id: 59,
		time: 58,
		velocity: 3.73361111111111,
		power: -192.912430177578,
		road: 36.0287037037037,
		acceleration: -0.187592592592593
	},
	{
		id: 60,
		time: 59,
		velocity: 3.7625,
		power: 437.152004759374,
		road: 39.8780555555556,
		acceleration: -0.016111111111111
	},
	{
		id: 61,
		time: 60,
		velocity: 4.02777777777778,
		power: 577.76030402003,
		road: 43.730462962963,
		acceleration: 0.0222222222222221
	},
	{
		id: 62,
		time: 61,
		velocity: 3.80027777777778,
		power: 104.503103308658,
		road: 47.5406481481481,
		acceleration: -0.106666666666666
	},
	{
		id: 63,
		time: 62,
		velocity: 3.4425,
		power: 513.035648504877,
		road: 51.3016666666667,
		acceleration: 0.0083333333333333
	},
	{
		id: 64,
		time: 63,
		velocity: 4.05277777777778,
		power: 2458.79866921947,
		road: 55.3207407407407,
		acceleration: 0.507777777777778
	},
	{
		id: 65,
		time: 64,
		velocity: 5.32361111111111,
		power: 4328.92055382359,
		road: 60.0097222222222,
		acceleration: 0.832037037037037
	},
	{
		id: 66,
		time: 65,
		velocity: 5.93861111111111,
		power: 4487.59880318078,
		road: 65.4746296296296,
		acceleration: 0.719814814814814
	},
	{
		id: 67,
		time: 66,
		velocity: 6.21222222222222,
		power: 4045.30380373126,
		road: 71.5740740740741,
		acceleration: 0.54925925925926
	},
	{
		id: 68,
		time: 67,
		velocity: 6.97138888888889,
		power: 4510.8756041825,
		road: 78.2283796296296,
		acceleration: 0.560462962962962
	},
	{
		id: 69,
		time: 68,
		velocity: 7.62,
		power: 5567.35620913818,
		road: 85.4874537037037,
		acceleration: 0.649074074074075
	},
	{
		id: 70,
		time: 69,
		velocity: 8.15944444444444,
		power: 6825.56819395516,
		road: 93.4404166666667,
		acceleration: 0.738703703703704
	},
	{
		id: 71,
		time: 70,
		velocity: 9.1875,
		power: 6294.71809157427,
		road: 102.061527777778,
		acceleration: 0.597592592592592
	},
	{
		id: 72,
		time: 71,
		velocity: 9.41277777777778,
		power: 5435.15446268634,
		road: 111.206203703704,
		acceleration: 0.449537037037038
	},
	{
		id: 73,
		time: 72,
		velocity: 9.50805555555556,
		power: 1838.84889885433,
		road: 120.59,
		acceleration: 0.0287037037037017
	},
	{
		id: 74,
		time: 73,
		velocity: 9.27361111111111,
		power: -412.735592012679,
		road: 129.876805555556,
		acceleration: -0.222685185185185
	},
	{
		id: 75,
		time: 74,
		velocity: 8.74472222222222,
		power: -3304.60082671397,
		road: 138.771157407407,
		acceleration: -0.562222222222221
	},
	{
		id: 76,
		time: 75,
		velocity: 7.82138888888889,
		power: -4169.42865917275,
		road: 147.036435185185,
		acceleration: -0.695925925925926
	},
	{
		id: 77,
		time: 76,
		velocity: 7.18583333333333,
		power: -3783.45109568567,
		road: 154.611388888889,
		acceleration: -0.684722222222223
	},
	{
		id: 78,
		time: 77,
		velocity: 6.69055555555556,
		power: -3695.76156298271,
		road: 161.484259259259,
		acceleration: -0.719444444444445
	},
	{
		id: 79,
		time: 78,
		velocity: 5.66305555555556,
		power: -3902.53610409611,
		road: 167.586851851852,
		acceleration: -0.821111111111111
	},
	{
		id: 80,
		time: 79,
		velocity: 4.7225,
		power: -4249.5878660497,
		road: 172.776666666667,
		acceleration: -1.00444444444444
	},
	{
		id: 81,
		time: 80,
		velocity: 3.67722222222222,
		power: -195.576478694726,
		road: 177.372222222222,
		acceleration: -0.184074074074074
	},
	{
		id: 82,
		time: 81,
		velocity: 5.11083333333333,
		power: 1926.34235903334,
		road: 182.023935185185,
		acceleration: 0.296388888888889
	},
	{
		id: 83,
		time: 82,
		velocity: 5.61166666666667,
		power: 6852.31162994473,
		road: 187.42,
		acceleration: 1.19231481481481
	},
	{
		id: 84,
		time: 83,
		velocity: 7.25416666666667,
		power: 10020.6035783891,
		road: 194.121851851852,
		acceleration: 1.41925925925926
	},
	{
		id: 85,
		time: 84,
		velocity: 9.36861111111111,
		power: 15344.8344023025,
		road: 202.421203703704,
		acceleration: 1.77574074074074
	},
	{
		id: 86,
		time: 85,
		velocity: 10.9388888888889,
		power: 14673.1197712203,
		road: 212.296851851852,
		acceleration: 1.37685185185185
	},
	{
		id: 87,
		time: 86,
		velocity: 11.3847222222222,
		power: 10375.4452873862,
		road: 223.259675925926,
		acceleration: 0.797500000000003
	},
	{
		id: 88,
		time: 87,
		velocity: 11.7611111111111,
		power: 6821.47665686472,
		road: 234.828935185185,
		acceleration: 0.415370370370368
	},
	{
		id: 89,
		time: 88,
		velocity: 12.185,
		power: 7626.28719947953,
		road: 246.834490740741,
		acceleration: 0.457222222222223
	},
	{
		id: 90,
		time: 89,
		velocity: 12.7563888888889,
		power: 7617.3410701365,
		road: 259.281944444444,
		acceleration: 0.426574074074074
	},
	{
		id: 91,
		time: 90,
		velocity: 13.0408333333333,
		power: 7863.53725723265,
		road: 272.152361111111,
		acceleration: 0.419351851851852
	},
	{
		id: 92,
		time: 91,
		velocity: 13.4430555555556,
		power: 6889.67199521956,
		road: 285.391898148148,
		acceleration: 0.318888888888887
	},
	{
		id: 93,
		time: 92,
		velocity: 13.7130555555556,
		power: 5887.43819484195,
		road: 298.903888888889,
		acceleration: 0.22601851851852
	},
	{
		id: 94,
		time: 93,
		velocity: 13.7188888888889,
		power: 3805.62244166563,
		road: 312.558564814815,
		acceleration: 0.0593518518518508
	},
	{
		id: 95,
		time: 94,
		velocity: 13.6211111111111,
		power: 1271.68595658187,
		road: 326.175833333333,
		acceleration: -0.134166666666665
	},
	{
		id: 96,
		time: 95,
		velocity: 13.3105555555556,
		power: -2118.37186778459,
		road: 339.528935185185,
		acceleration: -0.394166666666669
	},
	{
		id: 97,
		time: 96,
		velocity: 12.5363888888889,
		power: -4248.9064089399,
		road: 352.401574074074,
		acceleration: -0.566759259259259
	},
	{
		id: 98,
		time: 97,
		velocity: 11.9208333333333,
		power: -6404.01626066632,
		road: 364.610138888889,
		acceleration: -0.761388888888888
	},
	{
		id: 99,
		time: 98,
		velocity: 11.0263888888889,
		power: -4354.87030778059,
		road: 376.13875,
		acceleration: -0.598518518518517
	},
	{
		id: 100,
		time: 99,
		velocity: 10.7408333333333,
		power: -3774.01520619741,
		road: 387.089814814815,
		acceleration: -0.556574074074076
	},
	{
		id: 101,
		time: 100,
		velocity: 10.2511111111111,
		power: -1762.31992957865,
		road: 397.579722222222,
		acceleration: -0.36574074074074
	},
	{
		id: 102,
		time: 101,
		velocity: 9.92916666666667,
		power: -1823.44400243519,
		road: 407.69962962963,
		acceleration: -0.374259259259258
	},
	{
		id: 103,
		time: 102,
		velocity: 9.61805555555556,
		power: 942.99521358821,
		road: 417.591157407407,
		acceleration: -0.0825000000000014
	},
	{
		id: 104,
		time: 103,
		velocity: 10.0036111111111,
		power: 4076.80573469559,
		road: 427.564212962963,
		acceleration: 0.245555555555557
	},
	{
		id: 105,
		time: 104,
		velocity: 10.6658333333333,
		power: 9317.19899131591,
		road: 438.032361111111,
		acceleration: 0.744629629629632
	},
	{
		id: 106,
		time: 105,
		velocity: 11.8519444444444,
		power: 12673.6565264932,
		road: 449.359583333333,
		acceleration: 0.973518518518517
	},
	{
		id: 107,
		time: 106,
		velocity: 12.9241666666667,
		power: 14771.7544743785,
		road: 461.694490740741,
		acceleration: 1.04185185185185
	},
	{
		id: 108,
		time: 107,
		velocity: 13.7913888888889,
		power: 14831.5530489113,
		road: 475.019490740741,
		acceleration: 0.938333333333336
	},
	{
		id: 109,
		time: 108,
		velocity: 14.6669444444444,
		power: 12993.8402586081,
		road: 489.174027777778,
		acceleration: 0.720740740740739
	},
	{
		id: 110,
		time: 109,
		velocity: 15.0863888888889,
		power: 11552.6849057217,
		road: 503.972037037037,
		acceleration: 0.566203703703705
	},
	{
		id: 111,
		time: 110,
		velocity: 15.49,
		power: 9931.89704240818,
		road: 519.263333333333,
		acceleration: 0.420370370370371
	},
	{
		id: 112,
		time: 111,
		velocity: 15.9280555555556,
		power: 9139.03467445434,
		road: 534.936851851852,
		acceleration: 0.344074074074074
	},
	{
		id: 113,
		time: 112,
		velocity: 16.1186111111111,
		power: 6945.30119671908,
		road: 550.875,
		acceleration: 0.185185185185183
	},
	{
		id: 114,
		time: 113,
		velocity: 16.0455555555556,
		power: 5023.8214060246,
		road: 566.932962962963,
		acceleration: 0.0544444444444459
	},
	{
		id: 115,
		time: 114,
		velocity: 16.0913888888889,
		power: 4117.19956394218,
		road: 583.015416666667,
		acceleration: -0.00546296296296589
	},
	{
		id: 116,
		time: 115,
		velocity: 16.1022222222222,
		power: 2664.62983911129,
		road: 599.045925925926,
		acceleration: -0.0984259259259215
	},
	{
		id: 117,
		time: 116,
		velocity: 15.7502777777778,
		power: -395.653373306885,
		road: 614.879768518519,
		acceleration: -0.294907407407408
	},
	{
		id: 118,
		time: 117,
		velocity: 15.2066666666667,
		power: -3418.62415815304,
		road: 630.319398148148,
		acceleration: -0.49351851851852
	},
	{
		id: 119,
		time: 118,
		velocity: 14.6216666666667,
		power: -4615.92119962107,
		road: 645.223842592593,
		acceleration: -0.576851851851853
	},
	{
		id: 120,
		time: 119,
		velocity: 14.0197222222222,
		power: -3599.01344628365,
		road: 659.586805555556,
		acceleration: -0.506111111111112
	},
	{
		id: 121,
		time: 120,
		velocity: 13.6883333333333,
		power: -7202.75479335719,
		road: 673.304953703704,
		acceleration: -0.783518518518518
	},
	{
		id: 122,
		time: 121,
		velocity: 12.2711111111111,
		power: -6593.22562860833,
		road: 686.253564814815,
		acceleration: -0.755555555555555
	},
	{
		id: 123,
		time: 122,
		velocity: 11.7530555555556,
		power: -7769.61215894842,
		road: 698.383425925926,
		acceleration: -0.881944444444445
	},
	{
		id: 124,
		time: 123,
		velocity: 11.0425,
		power: -5128.08827351185,
		road: 709.73537037037,
		acceleration: -0.673888888888888
	},
	{
		id: 125,
		time: 124,
		velocity: 10.2494444444444,
		power: -10226.0667780631,
		road: 720.140416666667,
		acceleration: -1.21990740740741
	},
	{
		id: 126,
		time: 125,
		velocity: 8.09333333333333,
		power: -10535.6820742683,
		road: 729.240462962963,
		acceleration: -1.39009259259259
	},
	{
		id: 127,
		time: 126,
		velocity: 6.87222222222222,
		power: -8113.27253764964,
		road: 737.016388888889,
		acceleration: -1.25814814814815
	},
	{
		id: 128,
		time: 127,
		velocity: 6.475,
		power: -3228.4397278067,
		road: 743.837592592593,
		acceleration: -0.651296296296297
	},
	{
		id: 129,
		time: 128,
		velocity: 6.13944444444444,
		power: -1187.69589679821,
		road: 750.159398148148,
		acceleration: -0.347499999999999
	},
	{
		id: 130,
		time: 129,
		velocity: 5.82972222222222,
		power: 5066.78303243546,
		road: 756.642824074074,
		acceleration: 0.67074074074074
	},
	{
		id: 131,
		time: 130,
		velocity: 8.48722222222222,
		power: 10133.3992568317,
		road: 764.09662037037,
		acceleration: 1.27
	},
	{
		id: 132,
		time: 131,
		velocity: 9.94944444444444,
		power: 16705.0242091099,
		road: 773.075648148148,
		acceleration: 1.78046296296296
	},
	{
		id: 133,
		time: 132,
		velocity: 11.1711111111111,
		power: 12440.3768695293,
		road: 783.478055555556,
		acceleration: 1.0662962962963
	},
	{
		id: 134,
		time: 133,
		velocity: 11.6861111111111,
		power: 7859.67484087931,
		road: 794.682361111111,
		acceleration: 0.537500000000001
	},
	{
		id: 135,
		time: 134,
		velocity: 11.5619444444444,
		power: 2121.68630469626,
		road: 806.15162037037,
		acceleration: -0.00759259259259437
	},
	{
		id: 136,
		time: 135,
		velocity: 11.1483333333333,
		power: 573.613363893876,
		road: 817.543194444444,
		acceleration: -0.147777777777778
	},
	{
		id: 137,
		time: 136,
		velocity: 11.2427777777778,
		power: 3147.43704260259,
		road: 828.906018518519,
		acceleration: 0.0902777777777786
	},
	{
		id: 138,
		time: 137,
		velocity: 11.8327777777778,
		power: 3456.68243966241,
		road: 840.371296296296,
		acceleration: 0.114629629629631
	},
	{
		id: 139,
		time: 138,
		velocity: 11.4922222222222,
		power: 6134.92625104651,
		road: 852.06662037037,
		acceleration: 0.345462962962962
	},
	{
		id: 140,
		time: 139,
		velocity: 12.2791666666667,
		power: 6291.66939227305,
		road: 864.104074074074,
		acceleration: 0.338796296296296
	},
	{
		id: 141,
		time: 140,
		velocity: 12.8491666666667,
		power: 6269.78474583557,
		road: 876.469814814815,
		acceleration: 0.317777777777778
	},
	{
		id: 142,
		time: 141,
		velocity: 12.4455555555556,
		power: -581.876762207821,
		road: 888.862824074074,
		acceleration: -0.263240740740741
	},
	{
		id: 143,
		time: 142,
		velocity: 11.4894444444444,
		power: -6210.17382392192,
		road: 900.746712962963,
		acceleration: -0.754999999999997
	},
	{
		id: 144,
		time: 143,
		velocity: 10.5841666666667,
		power: -4128.0233857193,
		road: 911.96087962963,
		acceleration: -0.584444444444447
	},
	{
		id: 145,
		time: 144,
		velocity: 10.6922222222222,
		power: -1997.78851229603,
		road: 922.688981481482,
		acceleration: -0.387685185185186
	},
	{
		id: 146,
		time: 145,
		velocity: 10.3263888888889,
		power: -2357.89170839244,
		road: 933.00962962963,
		acceleration: -0.427222222222222
	},
	{
		id: 147,
		time: 146,
		velocity: 9.3025,
		power: -4540.29388840493,
		road: 942.782175925926,
		acceleration: -0.668981481481481
	},
	{
		id: 148,
		time: 147,
		velocity: 8.68527777777778,
		power: -6316.56903727726,
		road: 951.764490740741,
		acceleration: -0.911481481481482
	},
	{
		id: 149,
		time: 148,
		velocity: 7.59194444444444,
		power: -4480.9713745733,
		road: 959.919953703704,
		acceleration: -0.742222222222222
	},
	{
		id: 150,
		time: 149,
		velocity: 7.07583333333333,
		power: -2348.34896672584,
		road: 967.460925925926,
		acceleration: -0.486759259259259
	},
	{
		id: 151,
		time: 150,
		velocity: 7.225,
		power: -2016.12962981154,
		road: 974.530833333333,
		acceleration: -0.45537037037037
	},
	{
		id: 152,
		time: 151,
		velocity: 6.22583333333333,
		power: 380.341199229379,
		road: 981.325787037037,
		acceleration: -0.0945370370370364
	},
	{
		id: 153,
		time: 152,
		velocity: 6.79222222222222,
		power: 1097.39249137784,
		road: 988.082314814815,
		acceleration: 0.0176851851851847
	},
	{
		id: 154,
		time: 153,
		velocity: 7.27805555555556,
		power: 5141.03596944325,
		road: 995.15212962963,
		acceleration: 0.608888888888888
	},
	{
		id: 155,
		time: 154,
		velocity: 8.0525,
		power: 3942.89565602095,
		road: 1002.72032407407,
		acceleration: 0.38787037037037
	},
	{
		id: 156,
		time: 155,
		velocity: 7.95583333333333,
		power: 5225.81936930242,
		road: 1010.74282407407,
		acceleration: 0.520740740740741
	},
	{
		id: 157,
		time: 156,
		velocity: 8.84027777777778,
		power: 4019.64769821511,
		road: 1019.19175925926,
		acceleration: 0.33212962962963
	},
	{
		id: 158,
		time: 157,
		velocity: 9.04888888888889,
		power: 3033.97968316084,
		road: 1027.90453703704,
		acceleration: 0.195555555555554
	},
	{
		id: 159,
		time: 158,
		velocity: 8.5425,
		power: 34.2784374054018,
		road: 1036.63199074074,
		acceleration: -0.166203703703703
	},
	{
		id: 160,
		time: 159,
		velocity: 8.34166666666667,
		power: -1034.73149964675,
		road: 1045.12833333333,
		acceleration: -0.296018518518519
	},
	{
		id: 161,
		time: 160,
		velocity: 8.16083333333333,
		power: 632.195120836967,
		road: 1053.43351851852,
		acceleration: -0.086296296296295
	},
	{
		id: 162,
		time: 161,
		velocity: 8.28361111111111,
		power: 1244.54374113132,
		road: 1061.69180555556,
		acceleration: -0.00750000000000028
	},
	{
		id: 163,
		time: 162,
		velocity: 8.31916666666667,
		power: 2683.28662203009,
		road: 1070.03203703704,
		acceleration: 0.171388888888888
	},
	{
		id: 164,
		time: 163,
		velocity: 8.675,
		power: 3386.01433206566,
		road: 1078.58166666667,
		acceleration: 0.247407407407408
	},
	{
		id: 165,
		time: 164,
		velocity: 9.02583333333333,
		power: 3295.19763554861,
		road: 1087.36652777778,
		acceleration: 0.223055555555554
	},
	{
		id: 166,
		time: 165,
		velocity: 8.98833333333333,
		power: 4597.73006477645,
		road: 1096.44199074074,
		acceleration: 0.35814814814815
	},
	{
		id: 167,
		time: 166,
		velocity: 9.74944444444444,
		power: 3574.29045738291,
		road: 1105.80842592593,
		acceleration: 0.223796296296294
	},
	{
		id: 168,
		time: 167,
		velocity: 9.69722222222222,
		power: 775.853116070886,
		road: 1115.24115740741,
		acceleration: -0.0912037037037035
	},
	{
		id: 169,
		time: 168,
		velocity: 8.71472222222222,
		power: -708.796778279394,
		road: 1124.50023148148,
		acceleration: -0.25611111111111
	},
	{
		id: 170,
		time: 169,
		velocity: 8.98111111111111,
		power: 705.205588994891,
		road: 1133.5850462963,
		acceleration: -0.0924074074074071
	},
	{
		id: 171,
		time: 170,
		velocity: 9.42,
		power: 3946.36529335721,
		road: 1142.76199074074,
		acceleration: 0.276666666666667
	},
	{
		id: 172,
		time: 171,
		velocity: 9.54472222222222,
		power: 3372.33814998563,
		road: 1152.17662037037,
		acceleration: 0.198703703703703
	},
	{
		id: 173,
		time: 172,
		velocity: 9.57722222222222,
		power: 1623.77337362162,
		road: 1161.69097222222,
		acceleration: 0.000740740740738488
	},
	{
		id: 174,
		time: 173,
		velocity: 9.42222222222222,
		power: 1089.07313960785,
		road: 1171.17689814815,
		acceleration: -0.0575925925925915
	},
	{
		id: 175,
		time: 174,
		velocity: 9.37194444444444,
		power: -366.861125955103,
		road: 1180.52509259259,
		acceleration: -0.21787037037037
	},
	{
		id: 176,
		time: 175,
		velocity: 8.92361111111111,
		power: 146.82080620102,
		road: 1189.68541666667,
		acceleration: -0.15787037037037
	},
	{
		id: 177,
		time: 176,
		velocity: 8.94861111111111,
		power: 391.930598623267,
		road: 1198.70300925926,
		acceleration: -0.127592592592594
	},
	{
		id: 178,
		time: 177,
		velocity: 8.98916666666667,
		power: -441.336693146507,
		road: 1207.54486111111,
		acceleration: -0.223888888888887
	},
	{
		id: 179,
		time: 178,
		velocity: 8.25194444444444,
		power: -1073.91273179299,
		road: 1216.12458333333,
		acceleration: -0.300370370370372
	},
	{
		id: 180,
		time: 179,
		velocity: 8.0475,
		power: 339.252547608742,
		road: 1224.49199074074,
		acceleration: -0.12425925925926
	},
	{
		id: 181,
		time: 180,
		velocity: 8.61638888888889,
		power: 2349.62595178551,
		road: 1232.8612962963,
		acceleration: 0.128055555555555
	},
	{
		id: 182,
		time: 181,
		velocity: 8.63611111111111,
		power: 1435.83535915632,
		road: 1241.30023148148,
		acceleration: 0.0112037037037052
	},
	{
		id: 183,
		time: 182,
		velocity: 8.08111111111111,
		power: -178.650485613843,
		road: 1249.65018518518,
		acceleration: -0.189166666666667
	},
	{
		id: 184,
		time: 183,
		velocity: 8.04888888888889,
		power: -853.472299328275,
		road: 1257.76805555555,
		acceleration: -0.274999999999999
	},
	{
		id: 185,
		time: 184,
		velocity: 7.81111111111111,
		power: 676.065789604969,
		road: 1265.71166666667,
		acceleration: -0.0735185185185188
	},
	{
		id: 186,
		time: 185,
		velocity: 7.86055555555556,
		power: -385.479979080154,
		road: 1273.51171296296,
		acceleration: -0.213611111111112
	},
	{
		id: 187,
		time: 186,
		velocity: 7.40805555555556,
		power: -61.5803597426335,
		road: 1281.12069444444,
		acceleration: -0.168518518518519
	},
	{
		id: 188,
		time: 187,
		velocity: 7.30555555555556,
		power: -672.618383818482,
		road: 1288.51851851852,
		acceleration: -0.253796296296295
	},
	{
		id: 189,
		time: 188,
		velocity: 7.09916666666667,
		power: 499.015423467856,
		road: 1295.74731481481,
		acceleration: -0.0842592592592588
	},
	{
		id: 190,
		time: 189,
		velocity: 7.15527777777778,
		power: 766.803941884897,
		road: 1302.91208333333,
		acceleration: -0.0437962962962972
	},
	{
		id: 191,
		time: 190,
		velocity: 7.17416666666667,
		power: 507.76134781642,
		road: 1310.01462962963,
		acceleration: -0.080648148148148
	},
	{
		id: 192,
		time: 191,
		velocity: 6.85722222222222,
		power: -617.935580343759,
		road: 1316.95277777778,
		acceleration: -0.248148148148148
	},
	{
		id: 193,
		time: 192,
		velocity: 6.41083333333333,
		power: -2076.3709692644,
		road: 1323.52486111111,
		acceleration: -0.483981481481482
	},
	{
		id: 194,
		time: 193,
		velocity: 5.72222222222222,
		power: -2451.37657204187,
		road: 1329.56759259259,
		acceleration: -0.574722222222221
	},
	{
		id: 195,
		time: 194,
		velocity: 5.13305555555556,
		power: -2126.78969646218,
		road: 1335.04657407407,
		acceleration: -0.552777777777779
	},
	{
		id: 196,
		time: 195,
		velocity: 4.7525,
		power: -1103.67063315259,
		road: 1340.06259259259,
		acceleration: -0.373148148148148
	},
	{
		id: 197,
		time: 196,
		velocity: 4.60277777777778,
		power: -1381.44412912924,
		road: 1344.66435185185,
		acceleration: -0.45537037037037
	},
	{
		id: 198,
		time: 197,
		velocity: 3.76694444444444,
		power: -2694.95587978095,
		road: 1348.61069444444,
		acceleration: -0.855462962962963
	},
	{
		id: 199,
		time: 198,
		velocity: 2.18611111111111,
		power: -2568.68467245055,
		road: 1351.61208333333,
		acceleration: -1.03444444444444
	},
	{
		id: 200,
		time: 199,
		velocity: 1.49944444444444,
		power: -1366.38681258394,
		road: 1353.68356481481,
		acceleration: -0.82537037037037
	},
	{
		id: 201,
		time: 200,
		velocity: 1.29083333333333,
		power: -229.950603128958,
		road: 1355.19791666667,
		acceleration: -0.288888888888889
	},
	{
		id: 202,
		time: 201,
		velocity: 1.31944444444444,
		power: 39.4520760800959,
		road: 1356.51935185185,
		acceleration: -0.0969444444444445
	},
	{
		id: 203,
		time: 202,
		velocity: 1.20861111111111,
		power: 731.359869371974,
		road: 1357.99018518518,
		acceleration: 0.395740740740741
	},
	{
		id: 204,
		time: 203,
		velocity: 2.47805555555556,
		power: 1672.19144345001,
		road: 1360.02680555555,
		acceleration: 0.735833333333333
	},
	{
		id: 205,
		time: 204,
		velocity: 3.52694444444444,
		power: 2783.21595954013,
		road: 1362.87939814815,
		acceleration: 0.896111111111112
	},
	{
		id: 206,
		time: 205,
		velocity: 3.89694444444444,
		power: 2748.90499497974,
		road: 1366.51125,
		acceleration: 0.662407407407407
	},
	{
		id: 207,
		time: 206,
		velocity: 4.46527777777778,
		power: 2107.56388124272,
		road: 1370.67240740741,
		acceleration: 0.396203703703703
	},
	{
		id: 208,
		time: 207,
		velocity: 4.71555555555556,
		power: 1620.9358106667,
		road: 1375.15277777778,
		acceleration: 0.242222222222224
	},
	{
		id: 209,
		time: 208,
		velocity: 4.62361111111111,
		power: 717.030879233574,
		road: 1379.76638888889,
		acceleration: 0.0242592592592583
	},
	{
		id: 210,
		time: 209,
		velocity: 4.53805555555556,
		power: 136.616877422882,
		road: 1384.33828703704,
		acceleration: -0.107685185185185
	},
	{
		id: 211,
		time: 210,
		velocity: 4.3925,
		power: 187.232622272211,
		road: 1388.80907407407,
		acceleration: -0.0945370370370373
	},
	{
		id: 212,
		time: 211,
		velocity: 4.34,
		power: -707.517244516996,
		road: 1393.07648148148,
		acceleration: -0.312222222222222
	},
	{
		id: 213,
		time: 212,
		velocity: 3.60138888888889,
		power: -1355.58635543935,
		road: 1396.93486111111,
		acceleration: -0.505833333333334
	},
	{
		id: 214,
		time: 213,
		velocity: 2.875,
		power: -1449.79380388557,
		road: 1400.24263888889,
		acceleration: -0.59537037037037
	},
	{
		id: 215,
		time: 214,
		velocity: 2.55388888888889,
		power: -920.154546019448,
		road: 1403.01175925926,
		acceleration: -0.481944444444444
	},
	{
		id: 216,
		time: 215,
		velocity: 2.15555555555556,
		power: -77.4782195369829,
		road: 1405.45782407407,
		acceleration: -0.164166666666667
	},
	{
		id: 217,
		time: 216,
		velocity: 2.3825,
		power: 2155.30947253781,
		road: 1408.17412037037,
		acceleration: 0.704629629629629
	},
	{
		id: 218,
		time: 217,
		velocity: 4.66777777777778,
		power: 4741.31778690971,
		road: 1411.85375,
		acceleration: 1.22203703703704
	},
	{
		id: 219,
		time: 218,
		velocity: 5.82166666666667,
		power: 5828.79357241725,
		road: 1416.7062037037,
		acceleration: 1.12361111111111
	},
	{
		id: 220,
		time: 219,
		velocity: 5.75333333333333,
		power: 2741.39584574864,
		road: 1422.30550925926,
		acceleration: 0.370092592592593
	},
	{
		id: 221,
		time: 220,
		velocity: 5.77805555555555,
		power: 918.420349035138,
		road: 1428.10009259259,
		acceleration: 0.0204629629629629
	},
	{
		id: 222,
		time: 221,
		velocity: 5.88305555555556,
		power: 709.561887099211,
		road: 1433.89615740741,
		acceleration: -0.0175000000000001
	},
	{
		id: 223,
		time: 222,
		velocity: 5.70083333333333,
		power: -1942.17449757007,
		road: 1439.42638888889,
		acceleration: -0.514166666666666
	},
	{
		id: 224,
		time: 223,
		velocity: 4.23555555555556,
		power: -3862.28436274595,
		road: 1444.20384259259,
		acceleration: -0.991388888888888
	},
	{
		id: 225,
		time: 224,
		velocity: 2.90888888888889,
		power: -4203.9833650023,
		road: 1447.80291666667,
		acceleration: -1.36537037037037
	},
	{
		id: 226,
		time: 225,
		velocity: 1.60472222222222,
		power: -2421.59945778195,
		road: 1450.09782407407,
		acceleration: -1.24296296296296
	},
	{
		id: 227,
		time: 226,
		velocity: 0.506666666666667,
		power: -947.928886898616,
		road: 1451.28643518518,
		acceleration: -0.96962962962963
	},
	{
		id: 228,
		time: 227,
		velocity: 0,
		power: -168.39377259247,
		road: 1451.72277777778,
		acceleration: -0.534907407407408
	},
	{
		id: 229,
		time: 228,
		velocity: 0,
		power: -3.30871111111111,
		road: 1451.80722222222,
		acceleration: -0.168888888888889
	},
	{
		id: 230,
		time: 229,
		velocity: 0,
		power: 0,
		road: 1451.80722222222,
		acceleration: 0
	},
	{
		id: 231,
		time: 230,
		velocity: 0,
		power: 0,
		road: 1451.80722222222,
		acceleration: 0
	},
	{
		id: 232,
		time: 231,
		velocity: 0,
		power: 0,
		road: 1451.80722222222,
		acceleration: 0
	},
	{
		id: 233,
		time: 232,
		velocity: 0,
		power: 0,
		road: 1451.80722222222,
		acceleration: 0
	},
	{
		id: 234,
		time: 233,
		velocity: 0,
		power: 0,
		road: 1451.80722222222,
		acceleration: 0
	},
	{
		id: 235,
		time: 234,
		velocity: 0,
		power: 0,
		road: 1451.80722222222,
		acceleration: 0
	},
	{
		id: 236,
		time: 235,
		velocity: 0,
		power: 0,
		road: 1451.80722222222,
		acceleration: 0
	},
	{
		id: 237,
		time: 236,
		velocity: 0,
		power: 219.002771366435,
		road: 1452.11675925926,
		acceleration: 0.619074074074074
	},
	{
		id: 238,
		time: 237,
		velocity: 1.85722222222222,
		power: 770.878510354243,
		road: 1453.08972222222,
		acceleration: 0.707777777777778
	},
	{
		id: 239,
		time: 238,
		velocity: 2.12333333333333,
		power: 2806.77588496098,
		road: 1455.09097222222,
		acceleration: 1.3487962962963
	},
	{
		id: 240,
		time: 239,
		velocity: 4.04638888888889,
		power: 4380.83513531016,
		road: 1458.39763888889,
		acceleration: 1.26203703703704
	},
	{
		id: 241,
		time: 240,
		velocity: 5.64333333333333,
		power: 5268.95259929707,
		road: 1462.88412037037,
		acceleration: 1.09759259259259
	},
	{
		id: 242,
		time: 241,
		velocity: 5.41611111111111,
		power: 1267.44484148599,
		road: 1467.97930555555,
		acceleration: 0.119814814814815
	},
	{
		id: 243,
		time: 242,
		velocity: 4.40583333333333,
		power: -4102.66954104214,
		road: 1472.59694444444,
		acceleration: -1.07490740740741
	},
	{
		id: 244,
		time: 243,
		velocity: 2.41861111111111,
		power: -3312.22354286538,
		road: 1476.1137962963,
		acceleration: -1.12666666666667
	},
	{
		id: 245,
		time: 244,
		velocity: 2.03611111111111,
		power: -766.137732541083,
		road: 1478.85416666667,
		acceleration: -0.426296296296297
	},
	{
		id: 246,
		time: 245,
		velocity: 3.12694444444444,
		power: 1983.05305243778,
		road: 1481.68458333333,
		acceleration: 0.606388888888889
	},
	{
		id: 247,
		time: 246,
		velocity: 4.23777777777778,
		power: 2739.28903776406,
		road: 1485.16555555555,
		acceleration: 0.694722222222222
	},
	{
		id: 248,
		time: 247,
		velocity: 4.12027777777778,
		power: 3467.00819867289,
		road: 1489.36037037037,
		acceleration: 0.732962962962963
	},
	{
		id: 249,
		time: 248,
		velocity: 5.32583333333333,
		power: 2838.21999591345,
		road: 1494.1625462963,
		acceleration: 0.481759259259259
	},
	{
		id: 250,
		time: 249,
		velocity: 5.68305555555556,
		power: 6150.44086569748,
		road: 1499.71583333333,
		acceleration: 1.02046296296296
	},
	{
		id: 251,
		time: 250,
		velocity: 7.18166666666667,
		power: 7467.32664623954,
		road: 1506.29972222222,
		acceleration: 1.04074074074074
	},
	{
		id: 252,
		time: 251,
		velocity: 8.44805555555556,
		power: 7049.58451933657,
		road: 1513.81712962963,
		acceleration: 0.826296296296295
	},
	{
		id: 253,
		time: 252,
		velocity: 8.16194444444444,
		power: 1884.13721408117,
		road: 1521.79023148148,
		acceleration: 0.0850925925925923
	},
	{
		id: 254,
		time: 253,
		velocity: 7.43694444444444,
		power: -1771.36988702228,
		road: 1529.60587962963,
		acceleration: -0.399999999999999
	},
	{
		id: 255,
		time: 254,
		velocity: 7.24805555555556,
		power: -1099.65111239752,
		road: 1537.06467592593,
		acceleration: -0.313703703703703
	},
	{
		id: 256,
		time: 255,
		velocity: 7.22083333333333,
		power: 1308.12779669724,
		road: 1544.38180555555,
		acceleration: 0.0303703703703704
	},
	{
		id: 257,
		time: 256,
		velocity: 7.52805555555556,
		power: 1253.49899817806,
		road: 1551.7249537037,
		acceleration: 0.0216666666666656
	},
	{
		id: 258,
		time: 257,
		velocity: 7.31305555555556,
		power: 1298.71281158382,
		road: 1559.09259259259,
		acceleration: 0.0273148148148152
	},
	{
		id: 259,
		time: 258,
		velocity: 7.30277777777778,
		power: 1469.66846062894,
		road: 1566.49902777778,
		acceleration: 0.0502777777777776
	},
	{
		id: 260,
		time: 259,
		velocity: 7.67888888888889,
		power: 2493.98006042639,
		road: 1574.02509259259,
		acceleration: 0.188981481481481
	},
	{
		id: 261,
		time: 260,
		velocity: 7.88,
		power: 2700.93077551924,
		road: 1581.74888888889,
		acceleration: 0.206481481481481
	},
	{
		id: 262,
		time: 261,
		velocity: 7.92222222222222,
		power: 2409.11673428081,
		road: 1589.65472222222,
		acceleration: 0.157592592592593
	},
	{
		id: 263,
		time: 262,
		velocity: 8.15166666666667,
		power: 2729.45077023317,
		road: 1597.73472222222,
		acceleration: 0.190740740740741
	},
	{
		id: 264,
		time: 263,
		velocity: 8.45222222222222,
		power: 2528.81025466142,
		road: 1605.98814814815,
		acceleration: 0.156111111111111
	},
	{
		id: 265,
		time: 264,
		velocity: 8.39055555555555,
		power: 2643.30581910778,
		road: 1614.40101851852,
		acceleration: 0.162777777777777
	},
	{
		id: 266,
		time: 265,
		velocity: 8.64,
		power: 2880.32906483751,
		road: 1622.98699074074,
		acceleration: 0.183425925925928
	},
	{
		id: 267,
		time: 266,
		velocity: 9.0025,
		power: 3393.60234846154,
		road: 1631.78180555555,
		acceleration: 0.234259259259259
	},
	{
		id: 268,
		time: 267,
		velocity: 9.09333333333333,
		power: 1797.37991235287,
		road: 1640.71324074074,
		acceleration: 0.0389814814814802
	},
	{
		id: 269,
		time: 268,
		velocity: 8.75694444444444,
		power: 1006.06656731693,
		road: 1649.63722222222,
		acceleration: -0.0538888888888867
	},
	{
		id: 270,
		time: 269,
		velocity: 8.84083333333333,
		power: -405.959520803887,
		road: 1658.42453703704,
		acceleration: -0.219444444444443
	},
	{
		id: 271,
		time: 270,
		velocity: 8.435,
		power: 9.40986830194044,
		road: 1667.01819444444,
		acceleration: -0.167870370370371
	},
	{
		id: 272,
		time: 271,
		velocity: 8.25333333333333,
		power: 1845.36949093116,
		road: 1675.55717592593,
		acceleration: 0.0585185185185182
	},
	{
		id: 273,
		time: 272,
		velocity: 9.01638888888889,
		power: 5731.08774597396,
		road: 1684.38083333333,
		acceleration: 0.510833333333334
	},
	{
		id: 274,
		time: 273,
		velocity: 9.9675,
		power: 9023.63633979357,
		road: 1693.8699537037,
		acceleration: 0.820092592592593
	},
	{
		id: 275,
		time: 274,
		velocity: 10.7136111111111,
		power: 8114.87199902767,
		road: 1704.0924537037,
		acceleration: 0.646666666666667
	},
	{
		id: 276,
		time: 275,
		velocity: 10.9563888888889,
		power: 4507.16539562174,
		road: 1714.76402777778,
		acceleration: 0.251481481481479
	},
	{
		id: 277,
		time: 276,
		velocity: 10.7219444444444,
		power: 1644.00078004814,
		road: 1725.54486111111,
		acceleration: -0.0329629629629622
	},
	{
		id: 278,
		time: 277,
		velocity: 10.6147222222222,
		power: 205.847960392345,
		road: 1736.22347222222,
		acceleration: -0.171481481481482
	},
	{
		id: 279,
		time: 278,
		velocity: 10.4419444444444,
		power: 2012.40538748423,
		road: 1746.82060185185,
		acceleration: 0.00851851851852103
	},
	{
		id: 280,
		time: 279,
		velocity: 10.7475,
		power: 3258.61234754019,
		road: 1757.48648148148,
		acceleration: 0.128981481481482
	},
	{
		id: 281,
		time: 280,
		velocity: 11.0016666666667,
		power: 4780.12114952924,
		road: 1768.35064814815,
		acceleration: 0.267592592592592
	},
	{
		id: 282,
		time: 281,
		velocity: 11.2447222222222,
		power: 6019.91762041487,
		road: 1779.53199074074,
		acceleration: 0.366759259259258
	},
	{
		id: 283,
		time: 282,
		velocity: 11.8477777777778,
		power: 8488.99013840055,
		road: 1791.17694444444,
		acceleration: 0.560462962962964
	},
	{
		id: 284,
		time: 283,
		velocity: 12.6830555555556,
		power: 10291.9751567982,
		road: 1803.43601851852,
		acceleration: 0.667777777777777
	},
	{
		id: 285,
		time: 284,
		velocity: 13.2480555555556,
		power: 11262.0950134083,
		road: 1816.37407407407,
		acceleration: 0.690185185185186
	},
	{
		id: 286,
		time: 285,
		velocity: 13.9183333333333,
		power: 9902.3628176574,
		road: 1829.92444444444,
		acceleration: 0.534444444444443
	},
	{
		id: 287,
		time: 286,
		velocity: 14.2863888888889,
		power: 8708.65236101373,
		road: 1843.94800925926,
		acceleration: 0.411944444444448
	},
	{
		id: 288,
		time: 287,
		velocity: 14.4838888888889,
		power: 6674.11381776739,
		road: 1858.29921296296,
		acceleration: 0.243333333333332
	},
	{
		id: 289,
		time: 288,
		velocity: 14.6483333333333,
		power: 8083.38411926389,
		road: 1872.93699074074,
		acceleration: 0.329814814814814
	},
	{
		id: 290,
		time: 289,
		velocity: 15.2758333333333,
		power: 8367.52578151212,
		road: 1887.90532407407,
		acceleration: 0.331296296296298
	},
	{
		id: 291,
		time: 290,
		velocity: 15.4777777777778,
		power: 8856.96416900489,
		road: 1903.21226851852,
		acceleration: 0.345925925925926
	},
	{
		id: 292,
		time: 291,
		velocity: 15.6861111111111,
		power: 2450.2709871185,
		road: 1918.64439814815,
		acceleration: -0.0955555555555563
	},
	{
		id: 293,
		time: 292,
		velocity: 14.9891666666667,
		power: 2982.52783808587,
		road: 1934.00018518518,
		acceleration: -0.0571296296296282
	},
	{
		id: 294,
		time: 293,
		velocity: 15.3063888888889,
		power: 1974.98343783268,
		road: 1949.26569444444,
		acceleration: -0.123425925925927
	},
	{
		id: 295,
		time: 294,
		velocity: 15.3158333333333,
		power: 4671.67314317166,
		road: 1964.50078703704,
		acceleration: 0.0625925925925923
	},
	{
		id: 296,
		time: 295,
		velocity: 15.1769444444444,
		power: -271.870189860379,
		road: 1979.62953703704,
		acceleration: -0.275277777777779
	},
	{
		id: 297,
		time: 296,
		velocity: 14.4805555555556,
		power: -1310.23088960475,
		road: 1994.44875,
		acceleration: -0.343796296296295
	},
	{
		id: 298,
		time: 297,
		velocity: 14.2844444444444,
		power: -336.71176499628,
		road: 2008.96083333333,
		acceleration: -0.270462962962963
	},
	{
		id: 299,
		time: 298,
		velocity: 14.3655555555556,
		power: 2903.40867276626,
		road: 2023.32166666667,
		acceleration: -0.0320370370370355
	},
	{
		id: 300,
		time: 299,
		velocity: 14.3844444444444,
		power: 4090.20354226998,
		road: 2037.69351851852,
		acceleration: 0.0540740740740748
	},
	{
		id: 301,
		time: 300,
		velocity: 14.4466666666667,
		power: 4728.24988993622,
		road: 2052.14115740741,
		acceleration: 0.0974999999999966
	},
	{
		id: 302,
		time: 301,
		velocity: 14.6580555555556,
		power: 5715.19233237202,
		road: 2066.71912037037,
		acceleration: 0.163148148148148
	},
	{
		id: 303,
		time: 302,
		velocity: 14.8738888888889,
		power: 5367.46370657059,
		road: 2081.44458333333,
		acceleration: 0.131851851851854
	},
	{
		id: 304,
		time: 303,
		velocity: 14.8422222222222,
		power: 3852.78763829501,
		road: 2096.24671296296,
		acceleration: 0.0214814814814819
	},
	{
		id: 305,
		time: 304,
		velocity: 14.7225,
		power: 4258.88092156323,
		road: 2111.08402777778,
		acceleration: 0.0488888888888876
	},
	{
		id: 306,
		time: 305,
		velocity: 15.0205555555556,
		power: 4661.95478097784,
		road: 2125.98324074074,
		acceleration: 0.074907407407407
	},
	{
		id: 307,
		time: 306,
		velocity: 15.0669444444444,
		power: 5580.62090840716,
		road: 2140.98736111111,
		acceleration: 0.134907407407407
	},
	{
		id: 308,
		time: 307,
		velocity: 15.1272222222222,
		power: 3306.81764579688,
		road: 2156.0462037037,
		acceleration: -0.0254629629629637
	},
	{
		id: 309,
		time: 308,
		velocity: 14.9441666666667,
		power: 2454.58303918485,
		road: 2171.05074074074,
		acceleration: -0.0831481481481475
	},
	{
		id: 310,
		time: 309,
		velocity: 14.8175,
		power: 2970.46438325767,
		road: 2185.99111111111,
		acceleration: -0.0451851851851846
	},
	{
		id: 311,
		time: 310,
		velocity: 14.9916666666667,
		power: 4364.20556235907,
		road: 2200.93504629629,
		acceleration: 0.0523148148148156
	},
	{
		id: 312,
		time: 311,
		velocity: 15.1011111111111,
		power: 5468.1300227934,
		road: 2215.96805555555,
		acceleration: 0.125833333333333
	},
	{
		id: 313,
		time: 312,
		velocity: 15.195,
		power: 5877.04660977452,
		road: 2231.13810185185,
		acceleration: 0.148240740740739
	},
	{
		id: 314,
		time: 313,
		velocity: 15.4363888888889,
		power: 6642.40334887222,
		road: 2246.47875,
		acceleration: 0.192962962962964
	},
	{
		id: 315,
		time: 314,
		velocity: 15.68,
		power: 3963.63220566528,
		road: 2261.91925925926,
		acceleration: 0.00675925925925824
	},
	{
		id: 316,
		time: 315,
		velocity: 15.2152777777778,
		power: 2421.48437728684,
		road: 2277.31490740741,
		acceleration: -0.0964814814814794
	},
	{
		id: 317,
		time: 316,
		velocity: 15.1469444444444,
		power: -232.866951716482,
		road: 2292.52537037037,
		acceleration: -0.273888888888889
	},
	{
		id: 318,
		time: 317,
		velocity: 14.8583333333333,
		power: 3418.78534359474,
		road: 2307.5899537037,
		acceleration: -0.0178703703703693
	},
	{
		id: 319,
		time: 318,
		velocity: 15.1616666666667,
		power: 4320.17701382945,
		road: 2322.66773148148,
		acceleration: 0.0442592592592579
	},
	{
		id: 320,
		time: 319,
		velocity: 15.2797222222222,
		power: 7818.67151196556,
		road: 2337.90689814815,
		acceleration: 0.278518518518517
	},
	{
		id: 321,
		time: 320,
		velocity: 15.6938888888889,
		power: 6764.95124586477,
		road: 2353.38277777778,
		acceleration: 0.194907407407406
	},
	{
		id: 322,
		time: 321,
		velocity: 15.7463888888889,
		power: 7244.55021231083,
		road: 2369.06472222222,
		acceleration: 0.217222222222224
	},
	{
		id: 323,
		time: 322,
		velocity: 15.9313888888889,
		power: 6983.86818389132,
		road: 2384.95037037037,
		acceleration: 0.190185185185186
	},
	{
		id: 324,
		time: 323,
		velocity: 16.2644444444444,
		power: 10840.9032297776,
		road: 2401.14356481481,
		acceleration: 0.424907407407408
	},
	{
		id: 325,
		time: 324,
		velocity: 17.0211111111111,
		power: 4150.07487401718,
		road: 2417.54203703704,
		acceleration: -0.0143518518518526
	},
	{
		id: 326,
		time: 325,
		velocity: 15.8883333333333,
		power: 7571.04562351723,
		road: 2434.03300925926,
		acceleration: 0.199351851851851
	},
	{
		id: 327,
		time: 326,
		velocity: 16.8625,
		power: 3896.42167050707,
		road: 2450.60546296296,
		acceleration: -0.0363888888888901
	},
	{
		id: 328,
		time: 327,
		velocity: 16.9119444444444,
		power: 8548.14720005002,
		road: 2467.28550925926,
		acceleration: 0.251574074074078
	},
	{
		id: 329,
		time: 328,
		velocity: 16.6430555555556,
		power: 4184.78469088538,
		road: 2484.07842592592,
		acceleration: -0.0258333333333347
	},
	{
		id: 330,
		time: 329,
		velocity: 16.785,
		power: 2937.65079825558,
		road: 2500.80763888889,
		acceleration: -0.101574074074076
	},
	{
		id: 331,
		time: 330,
		velocity: 16.6072222222222,
		power: 3802.42703467055,
		road: 2517.46351851852,
		acceleration: -0.0450925925925958
	},
	{
		id: 332,
		time: 331,
		velocity: 16.5077777777778,
		power: 3152.82549853052,
		road: 2534.05490740741,
		acceleration: -0.083888888888886
	},
	{
		id: 333,
		time: 332,
		velocity: 16.5333333333333,
		power: 4798.93707555714,
		road: 2550.61486111111,
		acceleration: 0.0210185185185203
	},
	{
		id: 334,
		time: 333,
		velocity: 16.6702777777778,
		power: 5767.18634676162,
		road: 2567.22537037037,
		acceleration: 0.0800925925925924
	},
	{
		id: 335,
		time: 334,
		velocity: 16.7480555555556,
		power: 5685.01402659143,
		road: 2583.91185185185,
		acceleration: 0.0718518518518501
	},
	{
		id: 336,
		time: 335,
		velocity: 16.7488888888889,
		power: 5109.79677608147,
		road: 2600.65115740741,
		acceleration: 0.0337962962962983
	},
	{
		id: 337,
		time: 336,
		velocity: 16.7716666666667,
		power: 6886.02239565616,
		road: 2617.47777777778,
		acceleration: 0.140833333333333
	},
	{
		id: 338,
		time: 337,
		velocity: 17.1705555555556,
		power: 7318.22062489516,
		road: 2634.45518518518,
		acceleration: 0.160740740740739
	},
	{
		id: 339,
		time: 338,
		velocity: 17.2311111111111,
		power: 7594.12206443611,
		road: 2651.59796296296,
		acceleration: 0.169999999999998
	},
	{
		id: 340,
		time: 339,
		velocity: 17.2816666666667,
		power: 4017.66555702348,
		road: 2668.80069444444,
		acceleration: -0.0500925925925912
	},
	{
		id: 341,
		time: 340,
		velocity: 17.0202777777778,
		power: 3401.96267979945,
		road: 2685.93569444444,
		acceleration: -0.0853703703703701
	},
	{
		id: 342,
		time: 341,
		velocity: 16.975,
		power: 3949.22711199217,
		road: 2703.00314814815,
		acceleration: -0.0497222222222184
	},
	{
		id: 343,
		time: 342,
		velocity: 17.1325,
		power: 4345.38591466876,
		road: 2720.03365740741,
		acceleration: -0.0241666666666696
	},
	{
		id: 344,
		time: 343,
		velocity: 16.9477777777778,
		power: 5868.48167237092,
		road: 2737.0862962963,
		acceleration: 0.0684259259259257
	},
	{
		id: 345,
		time: 344,
		velocity: 17.1802777777778,
		power: 4658.58757048109,
		road: 2754.16972222222,
		acceleration: -0.00685185185185233
	},
	{
		id: 346,
		time: 345,
		velocity: 17.1119444444444,
		power: 7735.61259814242,
		road: 2771.33842592592,
		acceleration: 0.177407407407408
	},
	{
		id: 347,
		time: 346,
		velocity: 17.48,
		power: 7191.61332724634,
		road: 2788.66444444444,
		acceleration: 0.137222222222224
	},
	{
		id: 348,
		time: 347,
		velocity: 17.5919444444444,
		power: 9416.66431578967,
		road: 2806.18958333333,
		acceleration: 0.261018518518519
	},
	{
		id: 349,
		time: 348,
		velocity: 17.895,
		power: 7816.33818398502,
		road: 2823.92319444444,
		acceleration: 0.155925925925924
	},
	{
		id: 350,
		time: 349,
		velocity: 17.9477777777778,
		power: 6313.28726184083,
		road: 2841.7662037037,
		acceleration: 0.0628703703703728
	},
	{
		id: 351,
		time: 350,
		velocity: 17.7805555555556,
		power: 4172.4772846146,
		road: 2859.60939814815,
		acceleration: -0.0625
	},
	{
		id: 352,
		time: 351,
		velocity: 17.7075,
		power: 567.08413655695,
		road: 2877.28634259259,
		acceleration: -0.270000000000003
	},
	{
		id: 353,
		time: 352,
		velocity: 17.1377777777778,
		power: 1634.51299648958,
		road: 2894.72782407407,
		acceleration: -0.200925925925922
	},
	{
		id: 354,
		time: 353,
		velocity: 17.1777777777778,
		power: 505.996334157124,
		road: 2911.93703703704,
		acceleration: -0.263611111111114
	},
	{
		id: 355,
		time: 354,
		velocity: 16.9166666666667,
		power: 953.33463109835,
		road: 2928.89902777778,
		acceleration: -0.230833333333333
	},
	{
		id: 356,
		time: 355,
		velocity: 16.4452777777778,
		power: -1466.81266562655,
		road: 2945.55763888889,
		acceleration: -0.375925925925927
	},
	{
		id: 357,
		time: 356,
		velocity: 16.05,
		power: -204.114137949601,
		road: 2961.88291666667,
		acceleration: -0.290740740740738
	},
	{
		id: 358,
		time: 357,
		velocity: 16.0444444444444,
		power: -224.379313699531,
		road: 2977.91930555555,
		acceleration: -0.28703703703704
	},
	{
		id: 359,
		time: 358,
		velocity: 15.5841666666667,
		power: 1148.65910499517,
		road: 2993.71615740741,
		acceleration: -0.192037037037036
	},
	{
		id: 360,
		time: 359,
		velocity: 15.4738888888889,
		power: -1064.84291149308,
		road: 3009.24939814815,
		acceleration: -0.335185185185185
	},
	{
		id: 361,
		time: 360,
		velocity: 15.0388888888889,
		power: 111.230660730247,
		road: 3024.48967592593,
		acceleration: -0.25074074074074
	},
	{
		id: 362,
		time: 361,
		velocity: 14.8319444444444,
		power: -660.753520071618,
		road: 3039.45462962963,
		acceleration: -0.299907407407408
	},
	{
		id: 363,
		time: 362,
		velocity: 14.5741666666667,
		power: 204.952138693839,
		road: 3054.15231481481,
		acceleration: -0.23462962962963
	},
	{
		id: 364,
		time: 363,
		velocity: 14.335,
		power: 241.295748474891,
		road: 3068.6187037037,
		acceleration: -0.227962962962962
	},
	{
		id: 365,
		time: 364,
		velocity: 14.1480555555556,
		power: 1539.30594149322,
		road: 3082.90625,
		acceleration: -0.129722222222222
	},
	{
		id: 366,
		time: 365,
		velocity: 14.185,
		power: 2536.20244491125,
		road: 3097.10194444444,
		acceleration: -0.0539814814814807
	},
	{
		id: 367,
		time: 366,
		velocity: 14.1730555555556,
		power: 4822.53496501514,
		road: 3111.32736111111,
		acceleration: 0.113425925925926
	},
	{
		id: 368,
		time: 367,
		velocity: 14.4883333333333,
		power: 5503.35478192823,
		road: 3125.68824074074,
		acceleration: 0.157500000000001
	},
	{
		id: 369,
		time: 368,
		velocity: 14.6575,
		power: 7116.23873776394,
		road: 3140.25990740741,
		acceleration: 0.264074074074074
	},
	{
		id: 370,
		time: 369,
		velocity: 14.9652777777778,
		power: 6048.97607719292,
		road: 3155.05222222222,
		acceleration: 0.17722222222222
	},
	{
		id: 371,
		time: 370,
		velocity: 15.02,
		power: 6681.3705000108,
		road: 3170.03939814815,
		acceleration: 0.212500000000002
	},
	{
		id: 372,
		time: 371,
		velocity: 15.295,
		power: 3834.20945538272,
		road: 3185.13773148148,
		acceleration: 0.00981481481481339
	},
	{
		id: 373,
		time: 372,
		velocity: 14.9947222222222,
		power: 2783.60139223876,
		road: 3200.20986111111,
		acceleration: -0.0622222222222213
	},
	{
		id: 374,
		time: 373,
		velocity: 14.8333333333333,
		power: 1192.44476902093,
		road: 3215.16583333333,
		acceleration: -0.170092592592592
	},
	{
		id: 375,
		time: 374,
		velocity: 14.7847222222222,
		power: 2610.24056548781,
		road: 3230.00305555556,
		acceleration: -0.0674074074074085
	},
	{
		id: 376,
		time: 375,
		velocity: 14.7925,
		power: 4375.58145918825,
		road: 3244.83523148148,
		acceleration: 0.0573148148148146
	},
	{
		id: 377,
		time: 376,
		velocity: 15.0052777777778,
		power: 3823.25524101325,
		road: 3259.70458333333,
		acceleration: 0.0170370370370367
	},
	{
		id: 378,
		time: 377,
		velocity: 14.8358333333333,
		power: 2397.38552895101,
		road: 3274.54125,
		acceleration: -0.0824074074074055
	},
	{
		id: 379,
		time: 378,
		velocity: 14.5452777777778,
		power: -3363.29172837951,
		road: 3289.09231481481,
		acceleration: -0.488796296296297
	},
	{
		id: 380,
		time: 379,
		velocity: 13.5388888888889,
		power: -8186.19306043443,
		road: 3302.97208333333,
		acceleration: -0.853796296296297
	},
	{
		id: 381,
		time: 380,
		velocity: 12.2744444444444,
		power: -9634.76909544479,
		road: 3315.92416666667,
		acceleration: -1.00157407407407
	},
	{
		id: 382,
		time: 381,
		velocity: 11.5405555555556,
		power: -6647.24817269526,
		road: 3327.98175925926,
		acceleration: -0.787407407407411
	},
	{
		id: 383,
		time: 382,
		velocity: 11.1766666666667,
		power: -3598.11421255319,
		road: 3339.37976851852,
		acceleration: -0.531759259259257
	},
	{
		id: 384,
		time: 383,
		velocity: 10.6791666666667,
		power: -2785.60073375817,
		road: 3350.28064814815,
		acceleration: -0.462499999999999
	},
	{
		id: 385,
		time: 384,
		velocity: 10.1530555555556,
		power: -2302.51388021275,
		road: 3360.74023148148,
		acceleration: -0.420092592592594
	},
	{
		id: 386,
		time: 385,
		velocity: 9.91638888888889,
		power: -599.139872809788,
		road: 3370.86611111111,
		acceleration: -0.247314814814814
	},
	{
		id: 387,
		time: 386,
		velocity: 9.93722222222222,
		power: 1656.79797112002,
		road: 3380.86342592593,
		acceleration: -0.00981481481481516
	},
	{
		id: 388,
		time: 387,
		velocity: 10.1236111111111,
		power: 4515.36276901959,
		road: 3390.99699074074,
		acceleration: 0.282314814814816
	},
	{
		id: 389,
		time: 388,
		velocity: 10.7633333333333,
		power: 5706.13594394884,
		road: 3401.46314814815,
		acceleration: 0.38287037037037
	},
	{
		id: 390,
		time: 389,
		velocity: 11.0858333333333,
		power: 7855.22363705096,
		road: 3412.4,
		acceleration: 0.558518518518516
	},
	{
		id: 391,
		time: 390,
		velocity: 11.7991666666667,
		power: 7243.24356820992,
		road: 3423.84722222222,
		acceleration: 0.462222222222223
	},
	{
		id: 392,
		time: 391,
		velocity: 12.15,
		power: 7651.54316410653,
		road: 3435.75856481481,
		acceleration: 0.466018518518519
	},
	{
		id: 393,
		time: 392,
		velocity: 12.4838888888889,
		power: 6068.98180569003,
		road: 3448.05523148148,
		acceleration: 0.30462962962963
	},
	{
		id: 394,
		time: 393,
		velocity: 12.7130555555556,
		power: 7450.58559881364,
		road: 3460.70402777778,
		acceleration: 0.399629629629629
	},
	{
		id: 395,
		time: 394,
		velocity: 13.3488888888889,
		power: 8807.47166313346,
		road: 3473.79310185185,
		acceleration: 0.480925925925925
	},
	{
		id: 396,
		time: 395,
		velocity: 13.9266666666667,
		power: 9885.96639463779,
		road: 3487.38763888889,
		acceleration: 0.529999999999999
	},
	{
		id: 397,
		time: 396,
		velocity: 14.3030555555556,
		power: 9944.30917488813,
		road: 3501.49634259259,
		acceleration: 0.498333333333335
	},
	{
		id: 398,
		time: 397,
		velocity: 14.8438888888889,
		power: 7968.04556626577,
		road: 3516.01824074074,
		acceleration: 0.328055555555553
	},
	{
		id: 399,
		time: 398,
		velocity: 14.9108333333333,
		power: 8764.83244470567,
		road: 3530.88662037037,
		acceleration: 0.364907407407411
	},
	{
		id: 400,
		time: 399,
		velocity: 15.3977777777778,
		power: 6632.04056098902,
		road: 3546.03805555555,
		acceleration: 0.201203703703701
	},
	{
		id: 401,
		time: 400,
		velocity: 15.4475,
		power: 8140.21258653324,
		road: 3561.43611111111,
		acceleration: 0.292037037037037
	},
	{
		id: 402,
		time: 401,
		velocity: 15.7869444444444,
		power: 6828.97391156205,
		road: 3577.07592592592,
		acceleration: 0.191481481481482
	},
	{
		id: 403,
		time: 402,
		velocity: 15.9722222222222,
		power: 7258.92245168948,
		road: 3592.91671296296,
		acceleration: 0.210462962962964
	},
	{
		id: 404,
		time: 403,
		velocity: 16.0788888888889,
		power: 5996.27373984547,
		road: 3608.92273148148,
		acceleration: 0.119999999999996
	},
	{
		id: 405,
		time: 404,
		velocity: 16.1469444444444,
		power: 5904.55336794205,
		road: 3625.04333333333,
		acceleration: 0.10916666666667
	},
	{
		id: 406,
		time: 405,
		velocity: 16.2997222222222,
		power: 5007.72737781693,
		road: 3641.2425,
		acceleration: 0.0479629629629628
	},
	{
		id: 407,
		time: 406,
		velocity: 16.2227777777778,
		power: 6019.01647099804,
		road: 3657.52060185185,
		acceleration: 0.109907407407409
	},
	{
		id: 408,
		time: 407,
		velocity: 16.4766666666667,
		power: 7363.25043150205,
		road: 3673.94824074074,
		acceleration: 0.189166666666665
	},
	{
		id: 409,
		time: 408,
		velocity: 16.8672222222222,
		power: 6228.82133073857,
		road: 3690.52574074074,
		acceleration: 0.110555555555557
	},
	{
		id: 410,
		time: 409,
		velocity: 16.5544444444444,
		power: 6258.5105471079,
		road: 3707.21240740741,
		acceleration: 0.107777777777777
	},
	{
		id: 411,
		time: 410,
		velocity: 16.8,
		power: 4656.97575074867,
		road: 3723.95564814815,
		acceleration: 0.00537037037036825
	},
	{
		id: 412,
		time: 411,
		velocity: 16.8833333333333,
		power: 6860.37592182841,
		road: 3740.77143518518,
		acceleration: 0.139722222222222
	},
	{
		id: 413,
		time: 412,
		velocity: 16.9736111111111,
		power: 5374.89411084703,
		road: 3757.67898148148,
		acceleration: 0.0437962962962999
	},
	{
		id: 414,
		time: 413,
		velocity: 16.9313888888889,
		power: 3929.34211736794,
		road: 3774.58564814815,
		acceleration: -0.0455555555555556
	},
	{
		id: 415,
		time: 414,
		velocity: 16.7466666666667,
		power: 4517.25591469261,
		road: 3791.46541666667,
		acceleration: -0.00824074074074233
	},
	{
		id: 416,
		time: 415,
		velocity: 16.9488888888889,
		power: 5100.21904271646,
		road: 3808.35481481481,
		acceleration: 0.0274999999999999
	},
	{
		id: 417,
		time: 416,
		velocity: 17.0138888888889,
		power: 8091.72842522219,
		road: 3825.36143518518,
		acceleration: 0.206944444444446
	},
	{
		id: 418,
		time: 417,
		velocity: 17.3675,
		power: 5851.79668286632,
		road: 3842.50342592592,
		acceleration: 0.0637962962962924
	},
	{
		id: 419,
		time: 418,
		velocity: 17.1402777777778,
		power: 5842.77154413907,
		road: 3859.70768518518,
		acceleration: 0.0607407407407443
	},
	{
		id: 420,
		time: 419,
		velocity: 17.1961111111111,
		power: 5334.36647372673,
		road: 3876.95638888889,
		acceleration: 0.0281481481481478
	},
	{
		id: 421,
		time: 420,
		velocity: 17.4519444444444,
		power: 5541.00214715007,
		road: 3894.23884259259,
		acceleration: 0.0393518518518512
	},
	{
		id: 422,
		time: 421,
		velocity: 17.2583333333333,
		power: 5391.26394766295,
		road: 3911.55546296296,
		acceleration: 0.0289814814814804
	},
	{
		id: 423,
		time: 422,
		velocity: 17.2830555555556,
		power: 3555.00227414332,
		road: 3928.84606481481,
		acceleration: -0.081018518518519
	},
	{
		id: 424,
		time: 423,
		velocity: 17.2088888888889,
		power: 6024.55869772966,
		road: 3946.13041666667,
		acceleration: 0.0685185185185233
	},
	{
		id: 425,
		time: 424,
		velocity: 17.4638888888889,
		power: 5199.94517096174,
		road: 3963.45754629629,
		acceleration: 0.017037037037035
	},
	{
		id: 426,
		time: 425,
		velocity: 17.3341666666667,
		power: 5484.72899640458,
		road: 3980.80981481481,
		acceleration: 0.0332407407407409
	},
	{
		id: 427,
		time: 426,
		velocity: 17.3086111111111,
		power: 2474.44525615116,
		road: 3998.10546296296,
		acceleration: -0.146481481481484
	},
	{
		id: 428,
		time: 427,
		velocity: 17.0244444444444,
		power: 1911.19276068173,
		road: 4015.23972222222,
		acceleration: -0.176296296296293
	},
	{
		id: 429,
		time: 428,
		velocity: 16.8052777777778,
		power: 584.270342830534,
		road: 4032.15949074074,
		acceleration: -0.252685185185186
	},
	{
		id: 430,
		time: 429,
		velocity: 16.5505555555556,
		power: 1358.12418827839,
		road: 4048.85319444444,
		acceleration: -0.199444444444445
	},
	{
		id: 431,
		time: 430,
		velocity: 16.4261111111111,
		power: 2537.57894924408,
		road: 4065.38666666667,
		acceleration: -0.121018518518518
	},
	{
		id: 432,
		time: 431,
		velocity: 16.4422222222222,
		power: 2992.53061255112,
		road: 4081.81509259259,
		acceleration: -0.0890740740740767
	},
	{
		id: 433,
		time: 432,
		velocity: 16.2833333333333,
		power: 3865.46223487651,
		road: 4098.18324074074,
		acceleration: -0.0314814814814817
	},
	{
		id: 434,
		time: 433,
		velocity: 16.3316666666667,
		power: 2451.10382542383,
		road: 4114.47578703704,
		acceleration: -0.119722222222219
	},
	{
		id: 435,
		time: 434,
		velocity: 16.0830555555556,
		power: 635.686475553043,
		road: 4130.59217592593,
		acceleration: -0.232592592592592
	},
	{
		id: 436,
		time: 435,
		velocity: 15.5855555555556,
		power: -1173.36984318023,
		road: 4146.41916666667,
		acceleration: -0.346203703703706
	},
	{
		id: 437,
		time: 436,
		velocity: 15.2930555555556,
		power: -638.905422659891,
		road: 4161.9200462963,
		acceleration: -0.30601851851852
	},
	{
		id: 438,
		time: 437,
		velocity: 15.165,
		power: 653.553022645933,
		road: 4177.16115740741,
		acceleration: -0.213518518518516
	},
	{
		id: 439,
		time: 438,
		velocity: 14.945,
		power: 2704.81925462062,
		road: 4192.26125,
		acceleration: -0.0685185185185198
	},
	{
		id: 440,
		time: 439,
		velocity: 15.0875,
		power: 5659.41884161228,
		road: 4207.39449074074,
		acceleration: 0.134814814814815
	},
	{
		id: 441,
		time: 440,
		velocity: 15.5694444444444,
		power: 8793.88079270627,
		road: 4222.76416666667,
		acceleration: 0.338055555555556
	},
	{
		id: 442,
		time: 441,
		velocity: 15.9591666666667,
		power: 9375.42224238486,
		road: 4238.48152777778,
		acceleration: 0.357314814814814
	},
	{
		id: 443,
		time: 442,
		velocity: 16.1594444444444,
		power: 10147.5220775555,
		road: 4254.57069444444,
		acceleration: 0.386296296296297
	},
	{
		id: 444,
		time: 443,
		velocity: 16.7283333333333,
		power: 8624.71420307249,
		road: 4270.98800925926,
		acceleration: 0.270000000000003
	},
	{
		id: 445,
		time: 444,
		velocity: 16.7691666666667,
		power: 6990.69556258708,
		road: 4287.61842592593,
		acceleration: 0.156203703703703
	},
	{
		id: 446,
		time: 445,
		velocity: 16.6280555555556,
		power: 3904.01400502872,
		road: 4304.30703703704,
		acceleration: -0.0398148148148181
	},
	{
		id: 447,
		time: 446,
		velocity: 16.6088888888889,
		power: 3935.18972172752,
		road: 4320.9574537037,
		acceleration: -0.0365740740740748
	},
	{
		id: 448,
		time: 447,
		velocity: 16.6594444444444,
		power: 5260.87473134028,
		road: 4337.61282407407,
		acceleration: 0.0464814814814822
	},
	{
		id: 449,
		time: 448,
		velocity: 16.7675,
		power: 4909.93170835008,
		road: 4354.30300925926,
		acceleration: 0.0231481481481488
	},
	{
		id: 450,
		time: 449,
		velocity: 16.6783333333333,
		power: 3552.13509225128,
		road: 4370.97412037037,
		acceleration: -0.0612962962962982
	},
	{
		id: 451,
		time: 450,
		velocity: 16.4755555555556,
		power: 1815.03933571498,
		road: 4387.53092592593,
		acceleration: -0.167314814814812
	},
	{
		id: 452,
		time: 451,
		velocity: 16.2655555555556,
		power: 2595.71814582311,
		road: 4403.94708333333,
		acceleration: -0.113981481481481
	},
	{
		id: 453,
		time: 452,
		velocity: 16.3363888888889,
		power: 4173.41537778213,
		road: 4420.30060185185,
		acceleration: -0.0112962962962975
	},
	{
		id: 454,
		time: 453,
		velocity: 16.4416666666667,
		power: 6284.0494518829,
		road: 4436.70912037037,
		acceleration: 0.121296296296297
	},
	{
		id: 455,
		time: 454,
		velocity: 16.6294444444444,
		power: 6018.29118885777,
		road: 4453.22814814815,
		acceleration: 0.0997222222222227
	},
	{
		id: 456,
		time: 455,
		velocity: 16.6355555555556,
		power: 4964.44794635557,
		road: 4469.81231481481,
		acceleration: 0.030555555555555
	},
	{
		id: 457,
		time: 456,
		velocity: 16.5333333333333,
		power: 3215.225132669,
		road: 4486.37226851852,
		acceleration: -0.0789814814814811
	},
	{
		id: 458,
		time: 457,
		velocity: 16.3925,
		power: 3068.44419343854,
		road: 4502.84986111111,
		acceleration: -0.0857407407407429
	},
	{
		id: 459,
		time: 458,
		velocity: 16.3783333333333,
		power: 30.5777637924296,
		road: 4519.14699074074,
		acceleration: -0.275185185185183
	},
	{
		id: 460,
		time: 459,
		velocity: 15.7077777777778,
		power: -349.794383005423,
		road: 4535.15912037037,
		acceleration: -0.294814814814815
	},
	{
		id: 461,
		time: 460,
		velocity: 15.5080555555556,
		power: -5942.8843796177,
		road: 4550.69194444444,
		acceleration: -0.663796296296297
	},
	{
		id: 462,
		time: 461,
		velocity: 14.3869444444444,
		power: -7980.4990571986,
		road: 4565.48509259259,
		acceleration: -0.815555555555555
	},
	{
		id: 463,
		time: 462,
		velocity: 13.2611111111111,
		power: -10901.7476055948,
		road: 4579.34055555555,
		acceleration: -1.05981481481481
	},
	{
		id: 464,
		time: 463,
		velocity: 12.3286111111111,
		power: -8725.45899061713,
		road: 4592.20023148148,
		acceleration: -0.931759259259259
	},
	{
		id: 465,
		time: 464,
		velocity: 11.5916666666667,
		power: -11315.9959941051,
		road: 4603.98777777778,
		acceleration: -1.2125
	},
	{
		id: 466,
		time: 465,
		velocity: 9.62361111111111,
		power: -9300.90550622057,
		road: 4614.61430555555,
		acceleration: -1.10953703703704
	},
	{
		id: 467,
		time: 466,
		velocity: 9,
		power: -6757.80212487757,
		road: 4624.22726851852,
		acceleration: -0.917592592592591
	},
	{
		id: 468,
		time: 467,
		velocity: 8.83888888888889,
		power: -1555.57143686641,
		road: 4633.20402777778,
		acceleration: -0.354814814814816
	},
	{
		id: 469,
		time: 468,
		velocity: 8.55916666666667,
		power: 3629.11445106948,
		road: 4642.13069444444,
		acceleration: 0.25462962962963
	},
	{
		id: 470,
		time: 469,
		velocity: 9.76388888888889,
		power: 7501.90956305006,
		road: 4651.51574074074,
		acceleration: 0.662129629629629
	},
	{
		id: 471,
		time: 470,
		velocity: 10.8252777777778,
		power: 11749.3430648254,
		road: 4661.74148148148,
		acceleration: 1.01925925925926
	},
	{
		id: 472,
		time: 471,
		velocity: 11.6169444444444,
		power: 12848.8272625092,
		road: 4672.97717592592,
		acceleration: 1.00064814814815
	},
	{
		id: 473,
		time: 472,
		velocity: 12.7658333333333,
		power: 11234.6111292553,
		road: 4685.09402777778,
		acceleration: 0.761666666666667
	},
	{
		id: 474,
		time: 473,
		velocity: 13.1102777777778,
		power: 11357.0197902031,
		road: 4697.94444444444,
		acceleration: 0.705462962962962
	},
	{
		id: 475,
		time: 474,
		velocity: 13.7333333333333,
		power: 8860.44951557841,
		road: 4711.37842592592,
		acceleration: 0.461666666666666
	},
	{
		id: 476,
		time: 475,
		velocity: 14.1508333333333,
		power: 8048.1265856049,
		road: 4725.22967592592,
		acceleration: 0.372870370370372
	},
	{
		id: 477,
		time: 476,
		velocity: 14.2288888888889,
		power: 11378.7793380039,
		road: 4739.56134259259,
		acceleration: 0.587962962962962
	},
	{
		id: 478,
		time: 477,
		velocity: 15.4972222222222,
		power: 13527.3542508111,
		road: 4754.53287037037,
		acceleration: 0.691759259259262
	},
	{
		id: 479,
		time: 478,
		velocity: 16.2261111111111,
		power: 20180.5551343375,
		road: 4770.38166666666,
		acceleration: 1.06277777777778
	},
	{
		id: 480,
		time: 479,
		velocity: 17.4172222222222,
		power: 17801.056440366,
		road: 4787.17282407407,
		acceleration: 0.821944444444444
	},
	{
		id: 481,
		time: 480,
		velocity: 17.9630555555556,
		power: 18042.8834049802,
		road: 4804.76009259259,
		acceleration: 0.770277777777778
	},
	{
		id: 482,
		time: 481,
		velocity: 18.5369444444444,
		power: 13237.6076010863,
		road: 4822.95550925926,
		acceleration: 0.446018518518521
	},
	{
		id: 483,
		time: 482,
		velocity: 18.7552777777778,
		power: 13285.7496316461,
		road: 4841.58486111111,
		acceleration: 0.421851851851848
	},
	{
		id: 484,
		time: 483,
		velocity: 19.2286111111111,
		power: 11300.6022454312,
		road: 4860.57064814815,
		acceleration: 0.29101851851852
	},
	{
		id: 485,
		time: 484,
		velocity: 19.41,
		power: 7185.03219053518,
		road: 4879.73060185185,
		acceleration: 0.0573148148148164
	},
	{
		id: 486,
		time: 485,
		velocity: 18.9272222222222,
		power: 242.198914992703,
		road: 4898.76013888889,
		acceleration: -0.318148148148147
	},
	{
		id: 487,
		time: 486,
		velocity: 18.2741666666667,
		power: -9298.26255597943,
		road: 4917.20773148148,
		acceleration: -0.845740740740741
	},
	{
		id: 488,
		time: 487,
		velocity: 16.8727777777778,
		power: -13653.5643999917,
		road: 4934.67421296296,
		acceleration: -1.11648148148148
	},
	{
		id: 489,
		time: 488,
		velocity: 15.5777777777778,
		power: -12992.4127541523,
		road: 4951.02787037037,
		acceleration: -1.10916666666667
	},
	{
		id: 490,
		time: 489,
		velocity: 14.9466666666667,
		power: -7328.0319334754,
		road: 4966.44754629629,
		acceleration: -0.758796296296296
	},
	{
		id: 491,
		time: 490,
		velocity: 14.5963888888889,
		power: -1614.72728924219,
		road: 4981.305,
		acceleration: -0.365648148148148
	},
	{
		id: 492,
		time: 491,
		velocity: 14.4808333333333,
		power: 674.001625608334,
		road: 4995.88023148148,
		acceleration: -0.198796296296294
	},
	{
		id: 493,
		time: 492,
		velocity: 14.3502777777778,
		power: 112.501874547589,
		road: 5010.23833333333,
		acceleration: -0.235462962962963
	},
	{
		id: 494,
		time: 493,
		velocity: 13.89,
		power: -2123.11800332182,
		road: 5024.28023148148,
		acceleration: -0.396944444444445
	},
	{
		id: 495,
		time: 494,
		velocity: 13.29,
		power: -4733.30202160767,
		road: 5037.82518518518,
		acceleration: -0.596944444444445
	},
	{
		id: 496,
		time: 495,
		velocity: 12.5594444444444,
		power: -1308.17844657825,
		road: 5050.90731481481,
		acceleration: -0.328703703703704
	},
	{
		id: 497,
		time: 496,
		velocity: 12.9038888888889,
		power: -3020.1050935509,
		road: 5063.59125,
		acceleration: -0.467685185185186
	},
	{
		id: 498,
		time: 497,
		velocity: 11.8869444444444,
		power: -274.210440426298,
		road: 5075.92310185185,
		acceleration: -0.23648148148148
	},
	{
		id: 499,
		time: 498,
		velocity: 11.85,
		power: -3165.37397425645,
		road: 5087.89398148148,
		acceleration: -0.485462962962963
	},
	{
		id: 500,
		time: 499,
		velocity: 11.4475,
		power: 1070.07506556736,
		road: 5099.56805555555,
		acceleration: -0.10814814814815
	},
	{
		id: 501,
		time: 500,
		velocity: 11.5625,
		power: -1629.07158979934,
		road: 5111.01277777778,
		acceleration: -0.350555555555555
	},
	{
		id: 502,
		time: 501,
		velocity: 10.7983333333333,
		power: 2508.77461246205,
		road: 5122.29916666666,
		acceleration: 0.0338888888888889
	},
	{
		id: 503,
		time: 502,
		velocity: 11.5491666666667,
		power: 1288.44234290763,
		road: 5133.56300925926,
		acceleration: -0.0789814814814811
	},
	{
		id: 504,
		time: 503,
		velocity: 11.3255555555556,
		power: 5061.84939001514,
		road: 5144.92097222222,
		acceleration: 0.267222222222221
	},
	{
		id: 505,
		time: 504,
		velocity: 11.6,
		power: 2458.67342830467,
		road: 5156.42361111111,
		acceleration: 0.0221296296296298
	},
	{
		id: 506,
		time: 505,
		velocity: 11.6155555555556,
		power: 5581.35309208001,
		road: 5168.08615740741,
		acceleration: 0.297685185185188
	},
	{
		id: 507,
		time: 506,
		velocity: 12.2186111111111,
		power: 3670.28299603592,
		road: 5179.9562037037,
		acceleration: 0.117314814814813
	},
	{
		id: 508,
		time: 507,
		velocity: 11.9519444444444,
		power: 3768.08423867968,
		road: 5191.94541666667,
		acceleration: 0.121018518518518
	},
	{
		id: 509,
		time: 508,
		velocity: 11.9786111111111,
		power: 2329.97375326279,
		road: 5203.99189814815,
		acceleration: -0.00648148148148131
	},
	{
		id: 510,
		time: 509,
		velocity: 12.1991666666667,
		power: 3368.01989513139,
		road: 5216.07634259259,
		acceleration: 0.082407407407409
	},
	{
		id: 511,
		time: 510,
		velocity: 12.1991666666667,
		power: 3690.35177603646,
		road: 5228.25527777778,
		acceleration: 0.106574074074075
	},
	{
		id: 512,
		time: 511,
		velocity: 12.2983333333333,
		power: 4450.30639130947,
		road: 5240.57041666667,
		acceleration: 0.165833333333328
	},
	{
		id: 513,
		time: 512,
		velocity: 12.6966666666667,
		power: 7190.14724675459,
		road: 5253.15935185185,
		acceleration: 0.38175925925926
	},
	{
		id: 514,
		time: 513,
		velocity: 13.3444444444444,
		power: 8606.62794991742,
		road: 5266.17412037037,
		acceleration: 0.469907407407407
	},
	{
		id: 515,
		time: 514,
		velocity: 13.7080555555556,
		power: 7964.40973876003,
		road: 5279.61939814815,
		acceleration: 0.391111111111114
	},
	{
		id: 516,
		time: 515,
		velocity: 13.87,
		power: 5613.09455159757,
		road: 5293.35726851852,
		acceleration: 0.194074074074074
	},
	{
		id: 517,
		time: 516,
		velocity: 13.9266666666667,
		power: 4559.35748240149,
		road: 5307.24597222222,
		acceleration: 0.107592592592594
	},
	{
		id: 518,
		time: 517,
		velocity: 14.0308333333333,
		power: 5301.95735257139,
		road: 5321.26731481481,
		acceleration: 0.157685185185183
	},
	{
		id: 519,
		time: 518,
		velocity: 14.3430555555556,
		power: 5347.3043615051,
		road: 5335.44458333333,
		acceleration: 0.154166666666667
	},
	{
		id: 520,
		time: 519,
		velocity: 14.3891666666667,
		power: 4395.82565138843,
		road: 5349.73861111111,
		acceleration: 0.0793518518518539
	},
	{
		id: 521,
		time: 520,
		velocity: 14.2688888888889,
		power: 1182.31379669234,
		road: 5363.99472222222,
		acceleration: -0.155185185185188
	},
	{
		id: 522,
		time: 521,
		velocity: 13.8775,
		power: -978.646966076205,
		road: 5378.0175462963,
		acceleration: -0.311388888888887
	},
	{
		id: 523,
		time: 522,
		velocity: 13.455,
		power: -2375.7551582616,
		road: 5391.67726851852,
		acceleration: -0.414814814814816
	},
	{
		id: 524,
		time: 523,
		velocity: 13.0244444444444,
		power: -1852.53222268342,
		road: 5404.94310185185,
		acceleration: -0.372962962962962
	},
	{
		id: 525,
		time: 524,
		velocity: 12.7586111111111,
		power: -4784.10571200769,
		road: 5417.71648148148,
		acceleration: -0.611944444444445
	},
	{
		id: 526,
		time: 525,
		velocity: 11.6191666666667,
		power: -2912.77304706852,
		road: 5429.95319444444,
		acceleration: -0.461388888888889
	},
	{
		id: 527,
		time: 526,
		velocity: 11.6402777777778,
		power: -1548.76300798286,
		road: 5441.78740740741,
		acceleration: -0.343611111111111
	},
	{
		id: 528,
		time: 527,
		velocity: 11.7277777777778,
		power: 4491.95208451232,
		road: 5453.54736111111,
		acceleration: 0.195092592592593
	},
	{
		id: 529,
		time: 528,
		velocity: 12.2044444444444,
		power: 3231.70785545813,
		road: 5465.44365740741,
		acceleration: 0.0775925925925911
	},
	{
		id: 530,
		time: 529,
		velocity: 11.8730555555556,
		power: 3667.32503314927,
		road: 5477.43481481481,
		acceleration: 0.112129629629631
	},
	{
		id: 531,
		time: 530,
		velocity: 12.0641666666667,
		power: -119.08781681451,
		road: 5489.37291666667,
		acceleration: -0.21824074074074
	},
	{
		id: 532,
		time: 531,
		velocity: 11.5497222222222,
		power: -2679.32656745146,
		road: 5500.97916666667,
		acceleration: -0.445462962962964
	},
	{
		id: 533,
		time: 532,
		velocity: 10.5366666666667,
		power: -4604.87534430022,
		road: 5512.0462037037,
		acceleration: -0.632962962962964
	},
	{
		id: 534,
		time: 533,
		velocity: 10.1652777777778,
		power: -2594.50539661073,
		road: 5522.5725,
		acceleration: -0.448518518518519
	},
	{
		id: 535,
		time: 534,
		velocity: 10.2041666666667,
		power: 1288.91038776309,
		road: 5532.84689814815,
		acceleration: -0.0552777777777784
	},
	{
		id: 536,
		time: 535,
		velocity: 10.3708333333333,
		power: -7068.0393560238,
		road: 5542.62351851852,
		acceleration: -0.940277777777776
	},
	{
		id: 537,
		time: 536,
		velocity: 7.34444444444445,
		power: -8864.61048204953,
		road: 5551.30893518518,
		acceleration: -1.24212962962963
	},
	{
		id: 538,
		time: 537,
		velocity: 6.47777777777778,
		power: -9789.06528434374,
		road: 5558.58763888889,
		acceleration: -1.5712962962963
	},
	{
		id: 539,
		time: 538,
		velocity: 5.65694444444444,
		power: -3592.19809875455,
		road: 5564.69722222222,
		acceleration: -0.766944444444444
	},
	{
		id: 540,
		time: 539,
		velocity: 5.04361111111111,
		power: -1392.28583274041,
		road: 5570.21837962963,
		acceleration: -0.409907407407409
	},
	{
		id: 541,
		time: 540,
		velocity: 5.24805555555556,
		power: 203.471971220894,
		road: 5575.48342592592,
		acceleration: -0.102314814814815
	},
	{
		id: 542,
		time: 541,
		velocity: 5.35,
		power: 1800.29516990362,
		road: 5580.8037037037,
		acceleration: 0.212777777777778
	},
	{
		id: 543,
		time: 542,
		velocity: 5.68194444444444,
		power: 1026.24118410557,
		road: 5586.25731481481,
		acceleration: 0.0538888888888884
	},
	{
		id: 544,
		time: 543,
		velocity: 5.40972222222222,
		power: -72.660410436235,
		road: 5591.65888888889,
		acceleration: -0.157962962962963
	},
	{
		id: 545,
		time: 544,
		velocity: 4.87611111111111,
		power: -1166.19020244391,
		road: 5596.79078703704,
		acceleration: -0.381388888888889
	},
	{
		id: 546,
		time: 545,
		velocity: 4.53777777777778,
		power: -587.168768067441,
		road: 5601.5975,
		acceleration: -0.268981481481481
	},
	{
		id: 547,
		time: 546,
		velocity: 4.60277777777778,
		power: 731.383549845656,
		road: 5606.28203703704,
		acceleration: 0.0246296296296293
	},
	{
		id: 548,
		time: 547,
		velocity: 4.95,
		power: 1398.46728945607,
		road: 5611.06273148148,
		acceleration: 0.167685185185186
	},
	{
		id: 549,
		time: 548,
		velocity: 5.04083333333333,
		power: 1321.77740948874,
		road: 5615.99768518518,
		acceleration: 0.140833333333334
	},
	{
		id: 550,
		time: 549,
		velocity: 5.02527777777778,
		power: 841.773074917401,
		road: 5621.02046296296,
		acceleration: 0.0348148148148137
	},
	{
		id: 551,
		time: 550,
		velocity: 5.05444444444445,
		power: 637.004019090225,
		road: 5626.05638888889,
		acceleration: -0.00851851851851837
	},
	{
		id: 552,
		time: 551,
		velocity: 5.01527777777778,
		power: 67.637958655203,
		road: 5631.02458333333,
		acceleration: -0.126944444444444
	},
	{
		id: 553,
		time: 552,
		velocity: 4.64444444444444,
		power: -169.987100610682,
		road: 5635.84050925926,
		acceleration: -0.177592592592593
	},
	{
		id: 554,
		time: 553,
		velocity: 4.52166666666667,
		power: -64.5530293219777,
		road: 5640.49055555555,
		acceleration: -0.154166666666666
	},
	{
		id: 555,
		time: 554,
		velocity: 4.55277777777778,
		power: -265.750592538002,
		road: 5644.96291666667,
		acceleration: -0.201203703703704
	},
	{
		id: 556,
		time: 555,
		velocity: 4.04083333333333,
		power: -514.389133859158,
		road: 5649.20203703704,
		acceleration: -0.265277777777778
	},
	{
		id: 557,
		time: 556,
		velocity: 3.72583333333333,
		power: -1714.22300625343,
		road: 5653.00324074074,
		acceleration: -0.610555555555556
	},
	{
		id: 558,
		time: 557,
		velocity: 2.72111111111111,
		power: -1096.88077105704,
		road: 5656.25476851852,
		acceleration: -0.488796296296296
	},
	{
		id: 559,
		time: 558,
		velocity: 2.57444444444444,
		power: -933.992820722569,
		road: 5659.01791666666,
		acceleration: -0.487962962962963
	},
	{
		id: 560,
		time: 559,
		velocity: 2.26194444444444,
		power: -175.151069631038,
		road: 5661.43351851852,
		acceleration: -0.207129629629629
	},
	{
		id: 561,
		time: 560,
		velocity: 2.09972222222222,
		power: 1106.67725560711,
		road: 5663.91513888889,
		acceleration: 0.339166666666666
	},
	{
		id: 562,
		time: 561,
		velocity: 3.59194444444444,
		power: 2161.39609931385,
		road: 5666.88375,
		acceleration: 0.634814814814815
	},
	{
		id: 563,
		time: 562,
		velocity: 4.16638888888889,
		power: 1884.43485042663,
		road: 5670.38601851852,
		acceleration: 0.4325
	},
	{
		id: 564,
		time: 563,
		velocity: 3.39722222222222,
		power: 556.11464983559,
		road: 5674.11546296296,
		acceleration: 0.0218518518518516
	},
	{
		id: 565,
		time: 564,
		velocity: 3.6575,
		power: -225.47950641668,
		road: 5677.75578703703,
		acceleration: -0.200092592592592
	},
	{
		id: 566,
		time: 565,
		velocity: 3.56611111111111,
		power: 463.896564630857,
		road: 5681.29782407407,
		acceleration: 0.00351851851851848
	},
	{
		id: 567,
		time: 566,
		velocity: 3.40777777777778,
		power: 137.137959229078,
		road: 5684.79513888889,
		acceleration: -0.0929629629629631
	},
	{
		id: 568,
		time: 567,
		velocity: 3.37861111111111,
		power: 214.958159168995,
		road: 5688.21212962963,
		acceleration: -0.067685185185185
	},
	{
		id: 569,
		time: 568,
		velocity: 3.36305555555556,
		power: 335.379179754684,
		road: 5691.58083333333,
		acceleration: -0.0288888888888894
	},
	{
		id: 570,
		time: 569,
		velocity: 3.32111111111111,
		power: 222.494878146589,
		road: 5694.90356481481,
		acceleration: -0.0630555555555556
	},
	{
		id: 571,
		time: 570,
		velocity: 3.18944444444444,
		power: 401.331537253852,
		road: 5698.19231481481,
		acceleration: -0.00490740740740714
	},
	{
		id: 572,
		time: 571,
		velocity: 3.34833333333333,
		power: 623.165455995596,
		road: 5701.51074074074,
		acceleration: 0.0642592592592592
	},
	{
		id: 573,
		time: 572,
		velocity: 3.51388888888889,
		power: 969.086796824971,
		road: 5704.94300925926,
		acceleration: 0.163425925925926
	},
	{
		id: 574,
		time: 573,
		velocity: 3.67972222222222,
		power: 1081.66703944917,
		road: 5708.54768518518,
		acceleration: 0.181388888888889
	},
	{
		id: 575,
		time: 574,
		velocity: 3.8925,
		power: 1201.25616714815,
		road: 5712.34203703703,
		acceleration: 0.197962962962963
	},
	{
		id: 576,
		time: 575,
		velocity: 4.10777777777778,
		power: 1305.15245447976,
		road: 5716.33912037037,
		acceleration: 0.2075
	},
	{
		id: 577,
		time: 576,
		velocity: 4.30222222222222,
		power: 1586.41996297844,
		road: 5720.5687037037,
		acceleration: 0.2575
	},
	{
		id: 578,
		time: 577,
		velocity: 4.665,
		power: 1428.77765203323,
		road: 5725.02648148148,
		acceleration: 0.198888888888889
	},
	{
		id: 579,
		time: 578,
		velocity: 4.70444444444444,
		power: 1532.13289285968,
		road: 5729.68694444444,
		acceleration: 0.206481481481481
	},
	{
		id: 580,
		time: 579,
		velocity: 4.92166666666667,
		power: 1212.85888790093,
		road: 5734.51268518518,
		acceleration: 0.124074074074074
	},
	{
		id: 581,
		time: 580,
		velocity: 5.03722222222222,
		power: 1377.71144434365,
		road: 5739.47592592592,
		acceleration: 0.150925925925925
	},
	{
		id: 582,
		time: 581,
		velocity: 5.15722222222222,
		power: 1003.98703928918,
		road: 5744.54787037037,
		acceleration: 0.0664814814814818
	},
	{
		id: 583,
		time: 582,
		velocity: 5.12111111111111,
		power: -236.960412932231,
		road: 5749.55740740741,
		acceleration: -0.191296296296296
	},
	{
		id: 584,
		time: 583,
		velocity: 4.46333333333333,
		power: -1408.49009210104,
		road: 5754.24319444444,
		acceleration: -0.456203703703704
	},
	{
		id: 585,
		time: 584,
		velocity: 3.78861111111111,
		power: -1727.07508862157,
		road: 5758.41425925926,
		acceleration: -0.573240740740741
	},
	{
		id: 586,
		time: 585,
		velocity: 3.40138888888889,
		power: -1282.34649604432,
		road: 5762.04527777778,
		acceleration: -0.506851851851852
	},
	{
		id: 587,
		time: 586,
		velocity: 2.94277777777778,
		power: -603.858270746707,
		road: 5765.25722222222,
		acceleration: -0.331296296296296
	},
	{
		id: 588,
		time: 587,
		velocity: 2.79472222222222,
		power: -77.0517575869259,
		road: 5768.22365740741,
		acceleration: -0.159722222222222
	},
	{
		id: 589,
		time: 588,
		velocity: 2.92222222222222,
		power: 505.695942343192,
		road: 5771.13564814815,
		acceleration: 0.0508333333333333
	},
	{
		id: 590,
		time: 589,
		velocity: 3.09527777777778,
		power: 431.529854950482,
		road: 5774.08402777778,
		acceleration: 0.0219444444444443
	},
	{
		id: 591,
		time: 590,
		velocity: 2.86055555555556,
		power: 306.353412458814,
		road: 5777.03199074074,
		acceleration: -0.0227777777777778
	},
	{
		id: 592,
		time: 591,
		velocity: 2.85388888888889,
		power: 138.888155484038,
		road: 5779.92777777778,
		acceleration: -0.0815740740740738
	},
	{
		id: 593,
		time: 592,
		velocity: 2.85055555555556,
		power: 195.733895322661,
		road: 5782.75333333333,
		acceleration: -0.0588888888888888
	},
	{
		id: 594,
		time: 593,
		velocity: 2.68388888888889,
		power: -1052.70544415044,
		road: 5785.26287037037,
		acceleration: -0.573148148148148
	},
	{
		id: 595,
		time: 594,
		velocity: 1.13444444444444,
		power: -773.014561420446,
		road: 5787.21194444444,
		acceleration: -0.547777777777778
	},
	{
		id: 596,
		time: 595,
		velocity: 1.20722222222222,
		power: -591.031177186689,
		road: 5788.59796296296,
		acceleration: -0.578333333333333
	},
	{
		id: 597,
		time: 596,
		velocity: 0.948888888888889,
		power: 163.459414105528,
		road: 5789.70837962963,
		acceleration: 0.0271296296296295
	},
	{
		id: 598,
		time: 597,
		velocity: 1.21583333333333,
		power: 96.9978130748262,
		road: 5790.81453703703,
		acceleration: -0.0356481481481483
	},
	{
		id: 599,
		time: 598,
		velocity: 1.10027777777778,
		power: 167.592025275989,
		road: 5791.91884259259,
		acceleration: 0.0319444444444446
	},
	{
		id: 600,
		time: 599,
		velocity: 1.04472222222222,
		power: -37.7559667125871,
		road: 5792.95587962963,
		acceleration: -0.166481481481481
	},
	{
		id: 601,
		time: 600,
		velocity: 0.716388888888889,
		power: -174.461752206008,
		road: 5793.72629629629,
		acceleration: -0.366759259259259
	},
	{
		id: 602,
		time: 601,
		velocity: 0,
		power: -86.3257744287939,
		road: 5794.13921296296,
		acceleration: -0.348240740740741
	},
	{
		id: 603,
		time: 602,
		velocity: 0,
		power: -12.5857797433398,
		road: 5794.25861111111,
		acceleration: -0.238796296296296
	},
	{
		id: 604,
		time: 603,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 605,
		time: 604,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 606,
		time: 605,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 607,
		time: 606,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 608,
		time: 607,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 609,
		time: 608,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 610,
		time: 609,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 611,
		time: 610,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 612,
		time: 611,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 613,
		time: 612,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 614,
		time: 613,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 615,
		time: 614,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 616,
		time: 615,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 617,
		time: 616,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 618,
		time: 617,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 619,
		time: 618,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 620,
		time: 619,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 621,
		time: 620,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 622,
		time: 621,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 623,
		time: 622,
		velocity: 0,
		power: 0,
		road: 5794.25861111111,
		acceleration: 0
	},
	{
		id: 624,
		time: 623,
		velocity: 0,
		power: 3.4509946399533,
		road: 5794.28,
		acceleration: 0.0427777777777778
	},
	{
		id: 625,
		time: 624,
		velocity: 0.128333333333333,
		power: 6.26607283454979,
		road: 5794.32796296296,
		acceleration: 0.0103703703703704
	},
	{
		id: 626,
		time: 625,
		velocity: 0.0311111111111111,
		power: 6.42132886899632,
		road: 5794.38111111111,
		acceleration: 0
	},
	{
		id: 627,
		time: 626,
		velocity: 0,
		power: 2.55000292631872,
		road: 5794.41287037037,
		acceleration: -0.0427777777777778
	},
	{
		id: 628,
		time: 627,
		velocity: 0,
		power: 0.575520987654321,
		road: 5794.41805555555,
		acceleration: -0.0103703703703704
	},
	{
		id: 629,
		time: 628,
		velocity: 0,
		power: 0,
		road: 5794.41805555555,
		acceleration: 0
	},
	{
		id: 630,
		time: 629,
		velocity: 0,
		power: 49.1049763309254,
		road: 5794.55027777778,
		acceleration: 0.264444444444444
	},
	{
		id: 631,
		time: 630,
		velocity: 0.793333333333333,
		power: 306.801745708908,
		road: 5795.06523148148,
		acceleration: 0.501018518518519
	},
	{
		id: 632,
		time: 631,
		velocity: 1.50305555555556,
		power: 1021.509351217,
		road: 5796.22935185185,
		acceleration: 0.797314814814815
	},
	{
		id: 633,
		time: 632,
		velocity: 2.39194444444444,
		power: 1527.65423649542,
		road: 5798.14726851852,
		acceleration: 0.710277777777778
	},
	{
		id: 634,
		time: 633,
		velocity: 2.92416666666667,
		power: 2448.57354122764,
		road: 5800.83467592592,
		acceleration: 0.828703703703704
	},
	{
		id: 635,
		time: 634,
		velocity: 3.98916666666667,
		power: 3100.03825192898,
		road: 5804.33564814815,
		acceleration: 0.798425925925926
	},
	{
		id: 636,
		time: 635,
		velocity: 4.78722222222222,
		power: 3747.94299410615,
		road: 5808.62675925926,
		acceleration: 0.781851851851852
	},
	{
		id: 637,
		time: 636,
		velocity: 5.26972222222222,
		power: 3331.8077525481,
		road: 5813.59134259259,
		acceleration: 0.565092592592593
	},
	{
		id: 638,
		time: 637,
		velocity: 5.68444444444444,
		power: 2810.5302628096,
		road: 5819.03791666666,
		acceleration: 0.398888888888888
	},
	{
		id: 639,
		time: 638,
		velocity: 5.98388888888889,
		power: 4210.76297972379,
		road: 5824.98282407407,
		acceleration: 0.597777777777779
	},
	{
		id: 640,
		time: 639,
		velocity: 7.06305555555556,
		power: 4817.51404770032,
		road: 5831.53726851852,
		acceleration: 0.621296296296296
	},
	{
		id: 641,
		time: 640,
		velocity: 7.54833333333333,
		power: 6470.60938415003,
		road: 5838.79254629629,
		acceleration: 0.780370370370369
	},
	{
		id: 642,
		time: 641,
		velocity: 8.325,
		power: 6234.89704140222,
		road: 5846.76712962963,
		acceleration: 0.658240740740742
	},
	{
		id: 643,
		time: 642,
		velocity: 9.03777777777778,
		power: 7418.39344778507,
		road: 5855.43537037037,
		acceleration: 0.729074074074074
	},
	{
		id: 644,
		time: 643,
		velocity: 9.73555555555556,
		power: 7775.69124464041,
		road: 5864.81481481481,
		acceleration: 0.693333333333333
	},
	{
		id: 645,
		time: 644,
		velocity: 10.405,
		power: 4331.04432928575,
		road: 5874.68018518518,
		acceleration: 0.278518518518517
	},
	{
		id: 646,
		time: 645,
		velocity: 9.87333333333333,
		power: 2227.3883467861,
		road: 5884.70930555555,
		acceleration: 0.0489814814814817
	},
	{
		id: 647,
		time: 646,
		velocity: 9.8825,
		power: 644.888225144368,
		road: 5894.7049074074,
		acceleration: -0.116018518518517
	},
	{
		id: 648,
		time: 647,
		velocity: 10.0569444444444,
		power: 954.552401337976,
		road: 5904.60180555555,
		acceleration: -0.0813888888888883
	},
	{
		id: 649,
		time: 648,
		velocity: 9.62916666666667,
		power: 457.280215661045,
		road: 5914.39180555555,
		acceleration: -0.132407407407408
	},
	{
		id: 650,
		time: 649,
		velocity: 9.48527777777778,
		power: 896.482969117932,
		road: 5924.07407407407,
		acceleration: -0.0830555555555552
	},
	{
		id: 651,
		time: 650,
		velocity: 9.80777777777778,
		power: 2997.88518238801,
		road: 5933.78652777777,
		acceleration: 0.143425925925925
	},
	{
		id: 652,
		time: 651,
		velocity: 10.0594444444444,
		power: 4830.57236565159,
		road: 5943.73393518518,
		acceleration: 0.326481481481483
	},
	{
		id: 653,
		time: 652,
		velocity: 10.4647222222222,
		power: 5447.96510555622,
		road: 5954.0286574074,
		acceleration: 0.368148148148146
	},
	{
		id: 654,
		time: 653,
		velocity: 10.9122222222222,
		power: 3902.98410640641,
		road: 5964.60578703703,
		acceleration: 0.196666666666667
	},
	{
		id: 655,
		time: 654,
		velocity: 10.6494444444444,
		power: 2586.15686896821,
		road: 5975.31194444444,
		acceleration: 0.0613888888888887
	},
	{
		id: 656,
		time: 655,
		velocity: 10.6488888888889,
		power: 183.025170274215,
		road: 5985.96212962963,
		acceleration: -0.173333333333332
	},
	{
		id: 657,
		time: 656,
		velocity: 10.3922222222222,
		power: 2602.48292463503,
		road: 5996.55912037037,
		acceleration: 0.0669444444444451
	},
	{
		id: 658,
		time: 657,
		velocity: 10.8502777777778,
		power: 4288.75915905748,
		road: 6007.30273148148,
		acceleration: 0.226296296296294
	},
	{
		id: 659,
		time: 658,
		velocity: 11.3277777777778,
		power: 4851.41642319458,
		road: 6018.29324074074,
		acceleration: 0.267500000000002
	},
	{
		id: 660,
		time: 659,
		velocity: 11.1947222222222,
		power: 3282.60395733933,
		road: 6029.47254629629,
		acceleration: 0.110092592592594
	},
	{
		id: 661,
		time: 660,
		velocity: 11.1805555555556,
		power: 1776.13535036583,
		road: 6040.69074074074,
		acceleration: -0.032314814814816
	},
	{
		id: 662,
		time: 661,
		velocity: 11.2308333333333,
		power: 3409.47298270381,
		road: 6051.95208333333,
		acceleration: 0.118611111111113
	},
	{
		id: 663,
		time: 662,
		velocity: 11.5505555555556,
		power: 4667.49933539046,
		road: 6063.3862037037,
		acceleration: 0.226944444444444
	},
	{
		id: 664,
		time: 663,
		velocity: 11.8613888888889,
		power: 4963.72732604322,
		road: 6075.05472222222,
		acceleration: 0.24185185185185
	},
	{
		id: 665,
		time: 664,
		velocity: 11.9563888888889,
		power: 4210.08027527591,
		road: 6086.92662037037,
		acceleration: 0.164907407407409
	},
	{
		id: 666,
		time: 665,
		velocity: 12.0452777777778,
		power: 4203.21964615217,
		road: 6098.95953703704,
		acceleration: 0.15712962962963
	},
	{
		id: 667,
		time: 666,
		velocity: 12.3327777777778,
		power: 5730.19260109894,
		road: 6111.21013888889,
		acceleration: 0.278240740740742
	},
	{
		id: 668,
		time: 667,
		velocity: 12.7911111111111,
		power: 6411.0450805657,
		road: 6123.75944444444,
		acceleration: 0.319166666666664
	},
	{
		id: 669,
		time: 668,
		velocity: 13.0027777777778,
		power: 6124.69580592676,
		road: 6136.60782407407,
		acceleration: 0.27898148148148
	},
	{
		id: 670,
		time: 669,
		velocity: 13.1697222222222,
		power: 5743.77840507289,
		road: 6149.71314814815,
		acceleration: 0.234907407407411
	},
	{
		id: 671,
		time: 670,
		velocity: 13.4958333333333,
		power: 4652.80968619689,
		road: 6163.00574074074,
		acceleration: 0.13962962962963
	},
	{
		id: 672,
		time: 671,
		velocity: 13.4216666666667,
		power: 4699.93222335274,
		road: 6176.43685185185,
		acceleration: 0.137407407407409
	},
	{
		id: 673,
		time: 672,
		velocity: 13.5819444444444,
		power: 4337.42618070467,
		road: 6189.98879629629,
		acceleration: 0.104259259259258
	},
	{
		id: 674,
		time: 673,
		velocity: 13.8086111111111,
		power: 5815.05161376785,
		road: 6203.69828703703,
		acceleration: 0.210833333333333
	},
	{
		id: 675,
		time: 674,
		velocity: 14.0541666666667,
		power: 6862.36435182544,
		road: 6217.65208333333,
		acceleration: 0.277777777777775
	},
	{
		id: 676,
		time: 675,
		velocity: 14.4152777777778,
		power: 5527.3409170756,
		road: 6231.82851851852,
		acceleration: 0.167500000000002
	},
	{
		id: 677,
		time: 676,
		velocity: 14.3111111111111,
		power: 5800.07366008488,
		road: 6246.17851851852,
		acceleration: 0.179629629629629
	},
	{
		id: 678,
		time: 677,
		velocity: 14.5930555555556,
		power: 5220.84474576049,
		road: 6260.6837037037,
		acceleration: 0.130740740740741
	},
	{
		id: 679,
		time: 678,
		velocity: 14.8075,
		power: 3579.92946400119,
		road: 6275.25921296296,
		acceleration: 0.00990740740740748
	},
	{
		id: 680,
		time: 679,
		velocity: 14.3408333333333,
		power: 2469.70786601414,
		road: 6289.80518518518,
		acceleration: -0.0689814814814813
	},
	{
		id: 681,
		time: 680,
		velocity: 14.3861111111111,
		power: 159.237953711206,
		road: 6304.20032407407,
		acceleration: -0.232685185185185
	},
	{
		id: 682,
		time: 681,
		velocity: 14.1094444444444,
		power: -802.210970891936,
		road: 6318.32939814815,
		acceleration: -0.299444444444445
	},
	{
		id: 683,
		time: 682,
		velocity: 13.4425,
		power: 1892.33765318843,
		road: 6332.26143518518,
		acceleration: -0.0946296296296314
	},
	{
		id: 684,
		time: 683,
		velocity: 14.1022222222222,
		power: 3660.47534610465,
		road: 6346.16578703703,
		acceleration: 0.0392592592592607
	},
	{
		id: 685,
		time: 684,
		velocity: 14.2272222222222,
		power: 7919.83290466976,
		road: 6360.26412037037,
		acceleration: 0.348703703703702
	},
	{
		id: 686,
		time: 685,
		velocity: 14.4886111111111,
		power: 8265.75052399672,
		road: 6374.7136574074,
		acceleration: 0.353703703703706
	},
	{
		id: 687,
		time: 686,
		velocity: 15.1633333333333,
		power: 9614.02354377159,
		road: 6389.55328703703,
		acceleration: 0.426481481481481
	},
	{
		id: 688,
		time: 687,
		velocity: 15.5066666666667,
		power: 8636.93152024366,
		road: 6404.77402777777,
		acceleration: 0.335740740740739
	},
	{
		id: 689,
		time: 688,
		velocity: 15.4958333333333,
		power: 4117.5185052938,
		road: 6420.17199074074,
		acceleration: 0.0187037037037019
	},
	{
		id: 690,
		time: 689,
		velocity: 15.2194444444444,
		power: 1384.71413814644,
		road: 6435.49666666666,
		acceleration: -0.165277777777776
	},
	{
		id: 691,
		time: 690,
		velocity: 15.0108333333333,
		power: 2017.52198447847,
		road: 6450.67953703703,
		acceleration: -0.118333333333332
	},
	{
		id: 692,
		time: 691,
		velocity: 15.1408333333333,
		power: 3386.86809897772,
		road: 6465.7924074074,
		acceleration: -0.0216666666666683
	},
	{
		id: 693,
		time: 692,
		velocity: 15.1544444444444,
		power: 5312.10675263849,
		road: 6480.94935185185,
		acceleration: 0.109814814814817
	},
	{
		id: 694,
		time: 693,
		velocity: 15.3402777777778,
		power: 3220.95872069107,
		road: 6496.14333333333,
		acceleration: -0.0357407407407404
	},
	{
		id: 695,
		time: 694,
		velocity: 15.0336111111111,
		power: 3031.49225080372,
		road: 6511.29569444444,
		acceleration: -0.0474999999999994
	},
	{
		id: 696,
		time: 695,
		velocity: 15.0119444444444,
		power: 2782.34564867746,
		road: 6526.39277777778,
		acceleration: -0.0630555555555574
	},
	{
		id: 697,
		time: 696,
		velocity: 15.1511111111111,
		power: 4177.31286155277,
		road: 6541.47541666666,
		acceleration: 0.0341666666666658
	},
	{
		id: 698,
		time: 697,
		velocity: 15.1361111111111,
		power: 6119.74114099047,
		road: 6556.65736111111,
		acceleration: 0.164444444444447
	},
	{
		id: 699,
		time: 698,
		velocity: 15.5052777777778,
		power: 6497.65570727271,
		road: 6572.01273148148,
		acceleration: 0.182407407407405
	},
	{
		id: 700,
		time: 699,
		velocity: 15.6983333333333,
		power: 7550.3536170265,
		road: 6587.58097222222,
		acceleration: 0.243333333333336
	},
	{
		id: 701,
		time: 700,
		velocity: 15.8661111111111,
		power: 6162.48658959331,
		road: 6603.34166666666,
		acceleration: 0.141574074074072
	},
	{
		id: 702,
		time: 701,
		velocity: 15.93,
		power: 5142.82751161312,
		road: 6619.20800925926,
		acceleration: 0.0697222222222216
	},
	{
		id: 703,
		time: 702,
		velocity: 15.9075,
		power: 3323.07900676264,
		road: 6635.08393518518,
		acceleration: -0.0505555555555546
	},
	{
		id: 704,
		time: 703,
		velocity: 15.7144444444444,
		power: 1865.83510209145,
		road: 6650.86254629629,
		acceleration: -0.144074074074073
	},
	{
		id: 705,
		time: 704,
		velocity: 15.4977777777778,
		power: 2583.93960446032,
		road: 6666.52259259259,
		acceleration: -0.0930555555555568
	},
	{
		id: 706,
		time: 705,
		velocity: 15.6283333333333,
		power: 4173.53713432469,
		road: 6682.14337962963,
		acceleration: 0.0145370370370372
	},
	{
		id: 707,
		time: 706,
		velocity: 15.7580555555556,
		power: 3932.10897798423,
		road: 6697.77050925926,
		acceleration: -0.00185185185185155
	},
	{
		id: 708,
		time: 707,
		velocity: 15.4922222222222,
		power: 2889.60064045381,
		road: 6713.36143518518,
		acceleration: -0.0705555555555577
	},
	{
		id: 709,
		time: 708,
		velocity: 15.4166666666667,
		power: 2437.42970347189,
		road: 6728.86782407407,
		acceleration: -0.0985185185185173
	},
	{
		id: 710,
		time: 709,
		velocity: 15.4625,
		power: 3237.31202837184,
		road: 6744.30379629629,
		acceleration: -0.0423148148148158
	},
	{
		id: 711,
		time: 710,
		velocity: 15.3652777777778,
		power: 2194.4727216359,
		road: 6759.66314814815,
		acceleration: -0.110925925925926
	},
	{
		id: 712,
		time: 711,
		velocity: 15.0838888888889,
		power: 2588.61946678675,
		road: 6774.92638888889,
		acceleration: -0.081296296296296
	},
	{
		id: 713,
		time: 712,
		velocity: 15.2186111111111,
		power: 2795.80753255363,
		road: 6790.11652777778,
		acceleration: -0.0649074074074054
	},
	{
		id: 714,
		time: 713,
		velocity: 15.1705555555556,
		power: 4767.88485737963,
		road: 6805.30962962963,
		acceleration: 0.0708333333333329
	},
	{
		id: 715,
		time: 714,
		velocity: 15.2963888888889,
		power: 4029.99559616719,
		road: 6820.54736111111,
		acceleration: 0.018425925925925
	},
	{
		id: 716,
		time: 715,
		velocity: 15.2738888888889,
		power: 2817.32313652302,
		road: 6835.76222222222,
		acceleration: -0.0641666666666652
	},
	{
		id: 717,
		time: 716,
		velocity: 14.9780555555556,
		power: 2268.97157367039,
		road: 6850.89518518518,
		acceleration: -0.0996296296296322
	},
	{
		id: 718,
		time: 717,
		velocity: 14.9975,
		power: 3060.33356400941,
		road: 6865.95699074074,
		acceleration: -0.0426851851851833
	},
	{
		id: 719,
		time: 718,
		velocity: 15.1458333333333,
		power: 3725.12196745411,
		road: 6880.99953703703,
		acceleration: 0.00416666666666821
	},
	{
		id: 720,
		time: 719,
		velocity: 14.9905555555556,
		power: 3186.20264524151,
		road: 6896.02773148148,
		acceleration: -0.0328703703703717
	},
	{
		id: 721,
		time: 720,
		velocity: 14.8988888888889,
		power: 3264.22348132844,
		road: 6911.02625,
		acceleration: -0.0264814814814827
	},
	{
		id: 722,
		time: 721,
		velocity: 15.0663888888889,
		power: 3876.07081673501,
		road: 6926.01972222222,
		acceleration: 0.0163888888888906
	},
	{
		id: 723,
		time: 722,
		velocity: 15.0397222222222,
		power: 4645.07230089305,
		road: 6941.05560185185,
		acceleration: 0.0684259259259257
	},
	{
		id: 724,
		time: 723,
		velocity: 15.1041666666667,
		power: 2232.54383878439,
		road: 6956.0761574074,
		acceleration: -0.0990740740740748
	},
	{
		id: 725,
		time: 724,
		velocity: 14.7691666666667,
		power: 3049.23285363726,
		road: 6971.02717592592,
		acceleration: -0.0399999999999991
	},
	{
		id: 726,
		time: 725,
		velocity: 14.9197222222222,
		power: 3628.42512778386,
		road: 6985.95879629629,
		acceleration: 0.00120370370370182
	},
	{
		id: 727,
		time: 726,
		velocity: 15.1077777777778,
		power: 4756.51657866144,
		road: 7000.93037037037,
		acceleration: 0.0787037037037042
	},
	{
		id: 728,
		time: 727,
		velocity: 15.0052777777778,
		power: 4351.90563951111,
		road: 7015.96532407407,
		acceleration: 0.0480555555555569
	},
	{
		id: 729,
		time: 728,
		velocity: 15.0638888888889,
		power: 3555.42269496482,
		road: 7031.02027777777,
		acceleration: -0.00805555555555593
	},
	{
		id: 730,
		time: 729,
		velocity: 15.0836111111111,
		power: 4497.03043000826,
		road: 7046.09944444444,
		acceleration: 0.056481481481482
	},
	{
		id: 731,
		time: 730,
		velocity: 15.1747222222222,
		power: 4869.33901825507,
		road: 7061.24666666666,
		acceleration: 0.0796296296296291
	},
	{
		id: 732,
		time: 731,
		velocity: 15.3027777777778,
		power: 4508.64659802596,
		road: 7076.45981481481,
		acceleration: 0.0522222222222215
	},
	{
		id: 733,
		time: 732,
		velocity: 15.2402777777778,
		power: 3548.58254078519,
		road: 7091.69185185185,
		acceleration: -0.0144444444444431
	},
	{
		id: 734,
		time: 733,
		velocity: 15.1313888888889,
		power: 2274.84644585849,
		road: 7106.86648148148,
		acceleration: -0.100370370370371
	},
	{
		id: 735,
		time: 734,
		velocity: 15.0016666666667,
		power: 1676.05540796402,
		road: 7121.92157407407,
		acceleration: -0.138703703703705
	},
	{
		id: 736,
		time: 735,
		velocity: 14.8241666666667,
		power: 2310.78110026772,
		road: 7136.86162037037,
		acceleration: -0.0913888888888881
	},
	{
		id: 737,
		time: 736,
		velocity: 14.8572222222222,
		power: 3052.57905371482,
		road: 7151.73726851852,
		acceleration: -0.0374074074074091
	},
	{
		id: 738,
		time: 737,
		velocity: 14.8894444444444,
		power: 3099.96278726971,
		road: 7166.57773148148,
		acceleration: -0.0329629629629622
	},
	{
		id: 739,
		time: 738,
		velocity: 14.7252777777778,
		power: 2840.54773941747,
		road: 7181.37671296296,
		acceleration: -0.0500000000000007
	},
	{
		id: 740,
		time: 739,
		velocity: 14.7072222222222,
		power: 3935.97441906977,
		road: 7196.16462962963,
		acceleration: 0.0278703703703709
	},
	{
		id: 741,
		time: 740,
		velocity: 14.9730555555556,
		power: 4088.82257138409,
		road: 7210.98523148148,
		acceleration: 0.0374999999999996
	},
	{
		id: 742,
		time: 741,
		velocity: 14.8377777777778,
		power: 10724.2361574323,
		road: 7226.06875,
		acceleration: 0.488333333333333
	},
	{
		id: 743,
		time: 742,
		velocity: 16.1722222222222,
		power: 13210.4767629391,
		road: 7241.70569444444,
		acceleration: 0.618518518518519
	},
	{
		id: 744,
		time: 743,
		velocity: 16.8286111111111,
		power: 15591.9774010605,
		road: 7258.01314814815,
		acceleration: 0.7225
	},
	{
		id: 745,
		time: 744,
		velocity: 17.0052777777778,
		power: 8079.61881538254,
		road: 7274.79055555555,
		acceleration: 0.217407407407407
	},
	{
		id: 746,
		time: 745,
		velocity: 16.8244444444444,
		power: 4270.5783809314,
		road: 7291.665,
		acceleration: -0.0233333333333334
	},
	{
		id: 747,
		time: 746,
		velocity: 16.7586111111111,
		power: -673.003142114781,
		road: 7308.36439814815,
		acceleration: -0.326759259259259
	},
	{
		id: 748,
		time: 747,
		velocity: 16.025,
		power: -1836.4877944366,
		road: 7324.70268518518,
		acceleration: -0.395462962962963
	},
	{
		id: 749,
		time: 748,
		velocity: 15.6380555555556,
		power: -4248.01282411646,
		road: 7340.56851851852,
		acceleration: -0.549444444444445
	},
	{
		id: 750,
		time: 749,
		velocity: 15.1102777777778,
		power: -7985.48045134597,
		road: 7355.75574074074,
		acceleration: -0.807777777777778
	},
	{
		id: 751,
		time: 750,
		velocity: 13.6016666666667,
		power: -10408.3665684075,
		road: 7370.03625,
		acceleration: -1.00564814814815
	},
	{
		id: 752,
		time: 751,
		velocity: 12.6211111111111,
		power: -9021.28528254459,
		road: 7383.34518518518,
		acceleration: -0.9375
	},
	{
		id: 753,
		time: 752,
		velocity: 12.2977777777778,
		power: -5488.85350075807,
		road: 7395.8475,
		acceleration: -0.675740740740741
	},
	{
		id: 754,
		time: 753,
		velocity: 11.5744444444444,
		power: -1474.21832816625,
		road: 7407.84324074074,
		acceleration: -0.337407407407406
	},
	{
		id: 755,
		time: 754,
		velocity: 11.6088888888889,
		power: -1617.43379468397,
		road: 7419.49550925926,
		acceleration: -0.349537037037038
	},
	{
		id: 756,
		time: 755,
		velocity: 11.2491666666667,
		power: -1299.77554139357,
		road: 7430.81296296296,
		acceleration: -0.320092592592593
	},
	{
		id: 757,
		time: 756,
		velocity: 10.6141666666667,
		power: -1781.85299376958,
		road: 7441.78754629629,
		acceleration: -0.365648148148148
	},
	{
		id: 758,
		time: 757,
		velocity: 10.5119444444444,
		power: -2044.20378831516,
		road: 7452.38273148148,
		acceleration: -0.393148148148148
	},
	{
		id: 759,
		time: 758,
		velocity: 10.0697222222222,
		power: -2844.28238407987,
		road: 7462.54162037037,
		acceleration: -0.479444444444443
	},
	{
		id: 760,
		time: 759,
		velocity: 9.17583333333333,
		power: -5054.65495658721,
		road: 7472.09356481481,
		acceleration: -0.734444444444446
	},
	{
		id: 761,
		time: 760,
		velocity: 8.30861111111111,
		power: -4470.43937631816,
		road: 7480.92680555555,
		acceleration: -0.702962962962964
	},
	{
		id: 762,
		time: 761,
		velocity: 7.96083333333333,
		power: -3238.72754610268,
		road: 7489.11824074074,
		acceleration: -0.580648148148147
	},
	{
		id: 763,
		time: 762,
		velocity: 7.43388888888889,
		power: -1351.38053906324,
		road: 7496.84694444444,
		acceleration: -0.344814814814814
	},
	{
		id: 764,
		time: 763,
		velocity: 7.27416666666667,
		power: -593.256974415115,
		road: 7504.28203703704,
		acceleration: -0.242407407407407
	},
	{
		id: 765,
		time: 764,
		velocity: 7.23361111111111,
		power: 413.370126908784,
		road: 7511.54726851852,
		acceleration: -0.0973148148148164
	},
	{
		id: 766,
		time: 765,
		velocity: 7.14194444444444,
		power: 1052.69835646438,
		road: 7518.76217592593,
		acceleration: -0.00333333333333385
	},
	{
		id: 767,
		time: 766,
		velocity: 7.26416666666667,
		power: 150.113801962198,
		road: 7525.90837962963,
		acceleration: -0.134074074074074
	},
	{
		id: 768,
		time: 767,
		velocity: 6.83138888888889,
		power: -658.253356257571,
		road: 7532.86046296296,
		acceleration: -0.254166666666666
	},
	{
		id: 769,
		time: 768,
		velocity: 6.37944444444444,
		power: -1233.41250336742,
		road: 7539.51180555555,
		acceleration: -0.347314814814816
	},
	{
		id: 770,
		time: 769,
		velocity: 6.22222222222222,
		power: -113.491619606412,
		road: 7545.9049537037,
		acceleration: -0.169074074074073
	},
	{
		id: 771,
		time: 770,
		velocity: 6.32416666666667,
		power: 1471.17332343823,
		road: 7552.26023148148,
		acceleration: 0.0933333333333319
	},
	{
		id: 772,
		time: 771,
		velocity: 6.65944444444444,
		power: 1146.07152631474,
		road: 7558.68074074074,
		acceleration: 0.0371296296296304
	},
	{
		id: 773,
		time: 772,
		velocity: 6.33361111111111,
		power: -321.33479213474,
		road: 7565.01814814815,
		acceleration: -0.203333333333333
	},
	{
		id: 774,
		time: 773,
		velocity: 5.71416666666667,
		power: -1399.41012695112,
		road: 7571.05805555555,
		acceleration: -0.391666666666666
	},
	{
		id: 775,
		time: 774,
		velocity: 5.48444444444444,
		power: -756.909117449815,
		road: 7576.75944444444,
		acceleration: -0.285370370370371
	},
	{
		id: 776,
		time: 775,
		velocity: 5.4775,
		power: 4.7215920224004,
		road: 7582.24643518518,
		acceleration: -0.143425925925926
	},
	{
		id: 777,
		time: 776,
		velocity: 5.28388888888889,
		power: 103.148670921116,
		road: 7587.60009259259,
		acceleration: -0.123240740740741
	},
	{
		id: 778,
		time: 777,
		velocity: 5.11472222222222,
		power: -457.714372223378,
		road: 7592.77435185185,
		acceleration: -0.235555555555556
	},
	{
		id: 779,
		time: 778,
		velocity: 4.77083333333333,
		power: -576.044010648994,
		road: 7597.69875,
		acceleration: -0.264166666666666
	},
	{
		id: 780,
		time: 779,
		velocity: 4.49138888888889,
		power: -373.821144874331,
		road: 7602.37916666667,
		acceleration: -0.223796296296296
	},
	{
		id: 781,
		time: 780,
		velocity: 4.44333333333333,
		power: -308.555617520052,
		road: 7606.84199074074,
		acceleration: -0.211388888888889
	},
	{
		id: 782,
		time: 781,
		velocity: 4.13666666666667,
		power: -257.551051895696,
		road: 7611.09847222222,
		acceleration: -0.201296296296297
	},
	{
		id: 783,
		time: 782,
		velocity: 3.8875,
		power: -322.168439208379,
		road: 7615.14407407407,
		acceleration: -0.220462962962963
	},
	{
		id: 784,
		time: 783,
		velocity: 3.78194444444444,
		power: -127.984369215728,
		road: 7618.99407407407,
		acceleration: -0.170740740740741
	},
	{
		id: 785,
		time: 784,
		velocity: 3.62444444444444,
		power: -187.229002772487,
		road: 7622.66435185185,
		acceleration: -0.188703703703704
	},
	{
		id: 786,
		time: 785,
		velocity: 3.32138888888889,
		power: -150.541128868537,
		road: 7626.15041666667,
		acceleration: -0.179722222222222
	},
	{
		id: 787,
		time: 786,
		velocity: 3.24277777777778,
		power: -435.78891142123,
		road: 7629.40949074074,
		acceleration: -0.274259259259259
	},
	{
		id: 788,
		time: 787,
		velocity: 2.80166666666667,
		power: 11.2025227477382,
		road: 7632.46703703704,
		acceleration: -0.128796296296297
	},
	{
		id: 789,
		time: 788,
		velocity: 2.935,
		power: 590.084707166408,
		road: 7635.49652777778,
		acceleration: 0.0726851851851857
	},
	{
		id: 790,
		time: 789,
		velocity: 3.46083333333333,
		power: 1044.46591244362,
		road: 7638.66925925926,
		acceleration: 0.213796296296296
	},
	{
		id: 791,
		time: 790,
		velocity: 3.44305555555556,
		power: 1502.55139903619,
		road: 7642.11175925926,
		acceleration: 0.32574074074074
	},
	{
		id: 792,
		time: 791,
		velocity: 3.91222222222222,
		power: 1197.99285301689,
		road: 7645.81972222222,
		acceleration: 0.205185185185186
	},
	{
		id: 793,
		time: 792,
		velocity: 4.07638888888889,
		power: 1613.60762757188,
		road: 7649.77689814815,
		acceleration: 0.29324074074074
	},
	{
		id: 794,
		time: 793,
		velocity: 4.32277777777778,
		power: 1219.17140347521,
		road: 7653.96532407407,
		acceleration: 0.16925925925926
	},
	{
		id: 795,
		time: 794,
		velocity: 4.42,
		power: 1236.96237265927,
		road: 7658.31893518519,
		acceleration: 0.161111111111111
	},
	{
		id: 796,
		time: 795,
		velocity: 4.55972222222222,
		power: 799.851061500305,
		road: 7662.77824074074,
		acceleration: 0.0502777777777785
	},
	{
		id: 797,
		time: 796,
		velocity: 4.47361111111111,
		power: 155.65086341916,
		road: 7667.21194444444,
		acceleration: -0.101481481481482
	},
	{
		id: 798,
		time: 797,
		velocity: 4.11555555555556,
		power: 602.742695125718,
		road: 7671.59814814815,
		acceleration: 0.00648148148148131
	},
	{
		id: 799,
		time: 798,
		velocity: 4.57916666666667,
		power: -198.738171487572,
		road: 7675.89435185185,
		acceleration: -0.186481481481481
	},
	{
		id: 800,
		time: 799,
		velocity: 3.91416666666667,
		power: -61.9551420237177,
		road: 7680.02092592593,
		acceleration: -0.152777777777778
	},
	{
		id: 801,
		time: 800,
		velocity: 3.65722222222222,
		power: -898.022592206439,
		road: 7683.88069444444,
		acceleration: -0.380833333333333
	},
	{
		id: 802,
		time: 801,
		velocity: 3.43666666666667,
		power: -871.590917348162,
		road: 7687.35064814815,
		acceleration: -0.398796296296296
	},
	{
		id: 803,
		time: 802,
		velocity: 2.71777777777778,
		power: -952.494899127393,
		road: 7690.38976851852,
		acceleration: -0.46287037037037
	},
	{
		id: 804,
		time: 803,
		velocity: 2.26861111111111,
		power: -1194.5411010378,
		road: 7692.87907407407,
		acceleration: -0.636759259259259
	},
	{
		id: 805,
		time: 804,
		velocity: 1.52638888888889,
		power: -167.946592707101,
		road: 7694.94217592593,
		acceleration: -0.215648148148148
	},
	{
		id: 806,
		time: 805,
		velocity: 2.07083333333333,
		power: 1573.49207876532,
		road: 7697.19967592593,
		acceleration: 0.604444444444445
	},
	{
		id: 807,
		time: 806,
		velocity: 4.08194444444444,
		power: 4777.30536917448,
		road: 7700.46361111111,
		acceleration: 1.40842592592593
	},
	{
		id: 808,
		time: 807,
		velocity: 5.75166666666667,
		power: 6788.19441417866,
		road: 7705.12787037037,
		acceleration: 1.39222222222222
	},
	{
		id: 809,
		time: 808,
		velocity: 6.2475,
		power: 4014.24299269177,
		road: 7710.78856481481,
		acceleration: 0.600648148148148
	},
	{
		id: 810,
		time: 809,
		velocity: 5.88388888888889,
		power: 2678.56619283944,
		road: 7716.90564814815,
		acceleration: 0.31212962962963
	},
	{
		id: 811,
		time: 810,
		velocity: 6.68805555555556,
		power: 4470.50584161739,
		road: 7723.46152777778,
		acceleration: 0.565462962962963
	},
	{
		id: 812,
		time: 811,
		velocity: 7.94388888888889,
		power: 5613.45023884738,
		road: 7730.63333333333,
		acceleration: 0.666388888888889
	},
	{
		id: 813,
		time: 812,
		velocity: 7.88305555555556,
		power: 2411.62448308468,
		road: 7738.22532407407,
		acceleration: 0.173981481481482
	},
	{
		id: 814,
		time: 813,
		velocity: 7.21,
		power: -1649.64242854118,
		road: 7745.70898148148,
		acceleration: -0.390648148148148
	},
	{
		id: 815,
		time: 814,
		velocity: 6.77194444444444,
		power: -2810.90545903238,
		road: 7752.7087037037,
		acceleration: -0.577222222222223
	},
	{
		id: 816,
		time: 815,
		velocity: 6.15138888888889,
		power: -2520.93301616915,
		road: 7759.13828703704,
		acceleration: -0.563055555555557
	},
	{
		id: 817,
		time: 816,
		velocity: 5.52083333333333,
		power: -2511.4485122258,
		road: 7764.98712962963,
		acceleration: -0.598425925925926
	},
	{
		id: 818,
		time: 817,
		velocity: 4.97666666666667,
		power: -1904.91373879714,
		road: 7770.27564814815,
		acceleration: -0.522222222222222
	},
	{
		id: 819,
		time: 818,
		velocity: 4.58472222222222,
		power: -1773.61888637611,
		road: 7775.03689814815,
		acceleration: -0.532314814814814
	},
	{
		id: 820,
		time: 819,
		velocity: 3.92388888888889,
		power: -1111.00708183112,
		road: 7779.32675925926,
		acceleration: -0.410462962962963
	},
	{
		id: 821,
		time: 820,
		velocity: 3.74527777777778,
		power: -614.33120613784,
		road: 7783.26111111111,
		acceleration: -0.300555555555555
	},
	{
		id: 822,
		time: 821,
		velocity: 3.68305555555556,
		power: -371.149226362503,
		road: 7786.92435185185,
		acceleration: -0.241666666666666
	},
	{
		id: 823,
		time: 822,
		velocity: 3.19888888888889,
		power: -329.255222717448,
		road: 7790.34912037037,
		acceleration: -0.235277777777778
	},
	{
		id: 824,
		time: 823,
		velocity: 3.03944444444444,
		power: -498.729602496369,
		road: 7793.50652777778,
		acceleration: -0.299444444444445
	},
	{
		id: 825,
		time: 824,
		velocity: 2.78472222222222,
		power: 260.964794464255,
		road: 7796.49402777778,
		acceleration: -0.0403703703703702
	},
	{
		id: 826,
		time: 825,
		velocity: 3.07777777777778,
		power: 550.138529119457,
		road: 7799.49180555555,
		acceleration: 0.0609259259259258
	},
	{
		id: 827,
		time: 826,
		velocity: 3.22222222222222,
		power: 567.773073357131,
		road: 7802.55148148148,
		acceleration: 0.0628703703703706
	},
	{
		id: 828,
		time: 827,
		velocity: 2.97333333333333,
		power: 589.465173224437,
		road: 7805.67555555555,
		acceleration: 0.0659259259259257
	},
	{
		id: 829,
		time: 828,
		velocity: 3.27555555555556,
		power: 927.939406513615,
		road: 7808.91675925926,
		acceleration: 0.168333333333333
	},
	{
		id: 830,
		time: 829,
		velocity: 3.72722222222222,
		power: 2713.44162751343,
		road: 7812.56625,
		acceleration: 0.648240740740741
	},
	{
		id: 831,
		time: 830,
		velocity: 4.91805555555556,
		power: 3054.32168520833,
		road: 7816.84666666667,
		acceleration: 0.613611111111112
	},
	{
		id: 832,
		time: 831,
		velocity: 5.11638888888889,
		power: 1889.80487281988,
		road: 7821.57430555555,
		acceleration: 0.280833333333334
	},
	{
		id: 833,
		time: 832,
		velocity: 4.56972222222222,
		power: -497.876974775585,
		road: 7826.31708333333,
		acceleration: -0.250555555555557
	},
	{
		id: 834,
		time: 833,
		velocity: 4.16638888888889,
		power: -462.587760578015,
		road: 7830.81101851852,
		acceleration: -0.247129629629629
	},
	{
		id: 835,
		time: 834,
		velocity: 4.375,
		power: 215.988144650062,
		road: 7835.1387037037,
		acceleration: -0.0853703703703701
	},
	{
		id: 836,
		time: 835,
		velocity: 4.31361111111111,
		power: 2169.49123491605,
		road: 7839.60981481481,
		acceleration: 0.372222222222223
	},
	{
		id: 837,
		time: 836,
		velocity: 5.28305555555556,
		power: 1400.13745973998,
		road: 7844.35240740741,
		acceleration: 0.17074074074074
	},
	{
		id: 838,
		time: 837,
		velocity: 4.88722222222222,
		power: 2514.10496615465,
		road: 7849.37310185185,
		acceleration: 0.385462962962963
	},
	{
		id: 839,
		time: 838,
		velocity: 5.47,
		power: 1707.5973009031,
		road: 7854.68407407407,
		acceleration: 0.195092592592593
	},
	{
		id: 840,
		time: 839,
		velocity: 5.86833333333333,
		power: 1894.21170391738,
		road: 7860.20097222222,
		acceleration: 0.216759259259259
	},
	{
		id: 841,
		time: 840,
		velocity: 5.5375,
		power: 545.251925519056,
		road: 7865.80490740741,
		acceleration: -0.042685185185185
	},
	{
		id: 842,
		time: 841,
		velocity: 5.34194444444444,
		power: 919.060992558791,
		road: 7871.40138888889,
		acceleration: 0.0277777777777786
	},
	{
		id: 843,
		time: 842,
		velocity: 5.95166666666667,
		power: 762.665402438243,
		road: 7877.01074074074,
		acceleration: -0.00203703703703795
	},
	{
		id: 844,
		time: 843,
		velocity: 5.53138888888889,
		power: 1136.56607119896,
		road: 7882.65240740741,
		acceleration: 0.0666666666666664
	},
	{
		id: 845,
		time: 844,
		velocity: 5.54194444444444,
		power: 2.2637312679597,
		road: 7888.25509259259,
		acceleration: -0.144629629629629
	},
	{
		id: 846,
		time: 845,
		velocity: 5.51777777777778,
		power: 789.68050752685,
		road: 7893.78824074074,
		acceleration: 0.00555555555555554
	},
	{
		id: 847,
		time: 846,
		velocity: 5.54805555555556,
		power: 1948.57087920051,
		road: 7899.43310185185,
		acceleration: 0.21787037037037
	},
	{
		id: 848,
		time: 847,
		velocity: 6.19555555555556,
		power: 2142.05466706331,
		road: 7905.30537037037,
		acceleration: 0.236944444444444
	},
	{
		id: 849,
		time: 848,
		velocity: 6.22861111111111,
		power: 2058.15521328346,
		road: 7911.39958333333,
		acceleration: 0.206944444444445
	},
	{
		id: 850,
		time: 849,
		velocity: 6.16888888888889,
		power: 725.416168984721,
		road: 7917.58449074074,
		acceleration: -0.025555555555556
	},
	{
		id: 851,
		time: 850,
		velocity: 6.11888888888889,
		power: 996.603206316741,
		road: 7923.76694444444,
		acceleration: 0.0206481481481484
	},
	{
		id: 852,
		time: 851,
		velocity: 6.29055555555556,
		power: 1258.86257488114,
		road: 7929.99148148148,
		acceleration: 0.0635185185185181
	},
	{
		id: 853,
		time: 852,
		velocity: 6.35944444444444,
		power: 1265.3971852396,
		road: 7936.27879629629,
		acceleration: 0.0620370370370367
	},
	{
		id: 854,
		time: 853,
		velocity: 6.305,
		power: 543.280903455837,
		road: 7942.56773148148,
		acceleration: -0.0587962962962951
	},
	{
		id: 855,
		time: 854,
		velocity: 6.11416666666667,
		power: 622.861164775991,
		road: 7948.80513888889,
		acceleration: -0.0442592592592597
	},
	{
		id: 856,
		time: 855,
		velocity: 6.22666666666667,
		power: -343.80344537854,
		road: 7954.91662037037,
		acceleration: -0.207592592592593
	},
	{
		id: 857,
		time: 856,
		velocity: 5.68222222222222,
		power: 182.795761737176,
		road: 7960.86680555555,
		acceleration: -0.114999999999999
	},
	{
		id: 858,
		time: 857,
		velocity: 5.76916666666667,
		power: -18.1484646405117,
		road: 7966.68462962963,
		acceleration: -0.149722222222223
	},
	{
		id: 859,
		time: 858,
		velocity: 5.7775,
		power: 888.728556714489,
		road: 7972.43587962963,
		acceleration: 0.0165740740740743
	},
	{
		id: 860,
		time: 859,
		velocity: 5.73194444444445,
		power: 1880.03327116268,
		road: 7978.29097222222,
		acceleration: 0.191111111111111
	},
	{
		id: 861,
		time: 860,
		velocity: 6.3425,
		power: 2184.7204545331,
		road: 7984.35699074074,
		acceleration: 0.230740740740741
	},
	{
		id: 862,
		time: 861,
		velocity: 6.46972222222222,
		power: 880.5686389317,
		road: 7990.53884259259,
		acceleration: 0.000925925925924886
	},
	{
		id: 863,
		time: 862,
		velocity: 5.73472222222222,
		power: 134.941020243645,
		road: 7996.65851851852,
		acceleration: -0.125277777777777
	},
	{
		id: 864,
		time: 863,
		velocity: 5.96666666666667,
		power: 225.020733318527,
		road: 8002.66143518518,
		acceleration: -0.10824074074074
	},
	{
		id: 865,
		time: 864,
		velocity: 6.145,
		power: 1880.86415630289,
		road: 8008.70009259259,
		acceleration: 0.179722222222222
	},
	{
		id: 866,
		time: 865,
		velocity: 6.27388888888889,
		power: 1978.23606975682,
		road: 8014.92125,
		acceleration: 0.185277777777777
	},
	{
		id: 867,
		time: 866,
		velocity: 6.5225,
		power: 1783.66726773963,
		road: 8021.30675925926,
		acceleration: 0.143425925925926
	},
	{
		id: 868,
		time: 867,
		velocity: 6.57527777777778,
		power: 1352.236024301,
		road: 8027.79796296296,
		acceleration: 0.0679629629629641
	},
	{
		id: 869,
		time: 868,
		velocity: 6.47777777777778,
		power: 478.620141386902,
		road: 8034.28638888889,
		acceleration: -0.0735185185185188
	},
	{
		id: 870,
		time: 869,
		velocity: 6.30194444444444,
		power: -21.3675421226631,
		road: 8040.66115740741,
		acceleration: -0.153796296296296
	},
	{
		id: 871,
		time: 870,
		velocity: 6.11388888888889,
		power: -26.2156627549085,
		road: 8046.88222222222,
		acceleration: -0.153611111111111
	},
	{
		id: 872,
		time: 871,
		velocity: 6.01694444444444,
		power: 340.388371381971,
		road: 8052.98166666667,
		acceleration: -0.0896296296296297
	},
	{
		id: 873,
		time: 872,
		velocity: 6.03305555555556,
		power: -565.501662365383,
		road: 8058.9125462963,
		acceleration: -0.247500000000001
	},
	{
		id: 874,
		time: 873,
		velocity: 5.37138888888889,
		power: -935.922490491207,
		road: 8064.55981481481,
		acceleration: -0.319722222222222
	},
	{
		id: 875,
		time: 874,
		velocity: 5.05777777777778,
		power: -1167.33859964155,
		road: 8069.85972222222,
		acceleration: -0.375
	},
	{
		id: 876,
		time: 875,
		velocity: 4.90805555555556,
		power: 451.694222897341,
		road: 8074.94787037037,
		acceleration: -0.0485185185185175
	},
	{
		id: 877,
		time: 876,
		velocity: 5.22583333333333,
		power: 1293.93731575664,
		road: 8080.07351851852,
		acceleration: 0.123518518518518
	},
	{
		id: 878,
		time: 877,
		velocity: 5.42833333333333,
		power: 1995.99927032185,
		road: 8085.38694444444,
		acceleration: 0.252037037037037
	},
	{
		id: 879,
		time: 878,
		velocity: 5.66416666666667,
		power: 1622.46960374234,
		road: 8090.9087037037,
		acceleration: 0.16462962962963
	},
	{
		id: 880,
		time: 879,
		velocity: 5.71972222222222,
		power: 1099.07392105362,
		road: 8096.54277777778,
		acceleration: 0.0599999999999996
	},
	{
		id: 881,
		time: 880,
		velocity: 5.60833333333333,
		power: 540.808801413204,
		road: 8102.18462962963,
		acceleration: -0.0444444444444425
	},
	{
		id: 882,
		time: 881,
		velocity: 5.53083333333333,
		power: 1062.55570596331,
		road: 8107.83060185185,
		acceleration: 0.0526851851851839
	},
	{
		id: 883,
		time: 882,
		velocity: 5.87777777777778,
		power: 1686.69584498405,
		road: 8113.58412037037,
		acceleration: 0.162407407407407
	},
	{
		id: 884,
		time: 883,
		velocity: 6.09555555555556,
		power: 2334.11014724424,
		road: 8119.55087962963,
		acceleration: 0.264074074074074
	},
	{
		id: 885,
		time: 884,
		velocity: 6.32305555555556,
		power: 2112.72732671391,
		road: 8125.75425925926,
		acceleration: 0.209166666666667
	},
	{
		id: 886,
		time: 885,
		velocity: 6.50527777777778,
		power: 2112.99112792243,
		road: 8132.16041666667,
		acceleration: 0.196388888888889
	},
	{
		id: 887,
		time: 886,
		velocity: 6.68472222222222,
		power: 1936.22956384594,
		road: 8138.74351851852,
		acceleration: 0.157500000000001
	},
	{
		id: 888,
		time: 887,
		velocity: 6.79555555555555,
		power: 2174.17819021412,
		road: 8145.49805555556,
		acceleration: 0.18537037037037
	},
	{
		id: 889,
		time: 888,
		velocity: 7.06138888888889,
		power: 1401.78116337309,
		road: 8152.37541666667,
		acceleration: 0.0602777777777774
	},
	{
		id: 890,
		time: 889,
		velocity: 6.86555555555556,
		power: 1615.08544378165,
		road: 8159.32773148148,
		acceleration: 0.0896296296296297
	},
	{
		id: 891,
		time: 890,
		velocity: 7.06444444444444,
		power: 975.08988553844,
		road: 8166.32069444445,
		acceleration: -0.00833333333333375
	},
	{
		id: 892,
		time: 891,
		velocity: 7.03638888888889,
		power: 1944.29565984327,
		road: 8173.37662037037,
		acceleration: 0.13425925925926
	},
	{
		id: 893,
		time: 892,
		velocity: 7.26833333333333,
		power: 286.665875887731,
		road: 8180.44324074074,
		acceleration: -0.11287037037037
	},
	{
		id: 894,
		time: 893,
		velocity: 6.72583333333333,
		power: -131.467632628181,
		road: 8187.36625,
		acceleration: -0.174351851851852
	},
	{
		id: 895,
		time: 894,
		velocity: 6.51333333333333,
		power: -1723.35639566065,
		road: 8193.98921296296,
		acceleration: -0.42574074074074
	},
	{
		id: 896,
		time: 895,
		velocity: 5.99111111111111,
		power: -600.850528652425,
		road: 8200.27421296296,
		acceleration: -0.250185185185186
	},
	{
		id: 897,
		time: 896,
		velocity: 5.97527777777778,
		power: -332.219307504811,
		road: 8206.33125,
		acceleration: -0.205740740740741
	},
	{
		id: 898,
		time: 897,
		velocity: 5.89611111111111,
		power: 1162.37423690128,
		road: 8212.31384259259,
		acceleration: 0.0568518518518522
	},
	{
		id: 899,
		time: 898,
		velocity: 6.16166666666667,
		power: 1243.0014715437,
		road: 8218.35902777778,
		acceleration: 0.0683333333333334
	},
	{
		id: 900,
		time: 899,
		velocity: 6.18027777777778,
		power: 1425.62540652208,
		road: 8224.48648148148,
		acceleration: 0.0962037037037033
	},
	{
		id: 901,
		time: 900,
		velocity: 6.18472222222222,
		power: 781.337792484932,
		road: 8230.65425925926,
		acceleration: -0.0155555555555544
	},
	{
		id: 902,
		time: 901,
		velocity: 6.115,
		power: 84.4891046660966,
		road: 8236.74740740741,
		acceleration: -0.133703703703703
	},
	{
		id: 903,
		time: 902,
		velocity: 5.77916666666667,
		power: 55.8839201091358,
		road: 8242.70495370371,
		acceleration: -0.137500000000001
	},
	{
		id: 904,
		time: 903,
		velocity: 5.77222222222222,
		power: 204.194450700692,
		road: 8248.53888888889,
		acceleration: -0.109722222222222
	},
	{
		id: 905,
		time: 904,
		velocity: 5.78583333333333,
		power: 1329.08550835061,
		road: 8254.36472222222,
		acceleration: 0.0935185185185183
	},
	{
		id: 906,
		time: 905,
		velocity: 6.05972222222222,
		power: 1509.54080218763,
		road: 8260.2975462963,
		acceleration: 0.120462962962963
	},
	{
		id: 907,
		time: 906,
		velocity: 6.13361111111111,
		power: 1931.22709803378,
		road: 8266.38337962963,
		acceleration: 0.185555555555554
	},
	{
		id: 908,
		time: 907,
		velocity: 6.3425,
		power: 1459.06744744177,
		road: 8272.61060185185,
		acceleration: 0.0972222222222214
	},
	{
		id: 909,
		time: 908,
		velocity: 6.35138888888889,
		power: 1894.36442149409,
		road: 8278.96805555556,
		acceleration: 0.163240740740742
	},
	{
		id: 910,
		time: 909,
		velocity: 6.62333333333333,
		power: 2034.42906291588,
		road: 8285.49532407408,
		acceleration: 0.176388888888889
	},
	{
		id: 911,
		time: 910,
		velocity: 6.87166666666667,
		power: 2344.56717726468,
		road: 8292.21773148148,
		acceleration: 0.213888888888889
	},
	{
		id: 912,
		time: 911,
		velocity: 6.99305555555556,
		power: 1847.15359766883,
		road: 8299.11087962963,
		acceleration: 0.127592592592594
	},
	{
		id: 913,
		time: 912,
		velocity: 7.00611111111111,
		power: 1541.10301969952,
		road: 8306.10615740741,
		acceleration: 0.0766666666666662
	},
	{
		id: 914,
		time: 913,
		velocity: 7.10166666666667,
		power: 721.572722723677,
		road: 8313.1163425926,
		acceleration: -0.0468518518518524
	},
	{
		id: 915,
		time: 914,
		velocity: 6.8525,
		power: 590.363486901524,
		road: 8320.07041666667,
		acceleration: -0.0653703703703696
	},
	{
		id: 916,
		time: 915,
		velocity: 6.81,
		power: 1404.5611887589,
		road: 8327.02074074074,
		acceleration: 0.0578703703703694
	},
	{
		id: 917,
		time: 916,
		velocity: 7.27527777777778,
		power: 1766.58868551059,
		road: 8334.05439814815,
		acceleration: 0.108796296296297
	},
	{
		id: 918,
		time: 917,
		velocity: 7.17888888888889,
		power: 1714.44567516393,
		road: 8341.19069444445,
		acceleration: 0.0964814814814812
	},
	{
		id: 919,
		time: 918,
		velocity: 7.09944444444444,
		power: 274.178698983576,
		road: 8348.31745370371,
		acceleration: -0.115555555555554
	},
	{
		id: 920,
		time: 919,
		velocity: 6.92861111111111,
		power: 1012.34109844602,
		road: 8355.38398148148,
		acceleration: -0.00490740740740847
	},
	{
		id: 921,
		time: 920,
		velocity: 7.16416666666667,
		power: 2467.14643044627,
		road: 8362.55083333334,
		acceleration: 0.205555555555556
	},
	{
		id: 922,
		time: 921,
		velocity: 7.71611111111111,
		power: 2653.7170148034,
		road: 8369.93041666667,
		acceleration: 0.219907407407407
	},
	{
		id: 923,
		time: 922,
		velocity: 7.58833333333333,
		power: 2298.95474998035,
		road: 8377.49972222222,
		acceleration: 0.159537037037039
	},
	{
		id: 924,
		time: 923,
		velocity: 7.64277777777778,
		power: 955.481118637006,
		road: 8385.13444444445,
		acceleration: -0.0287037037037043
	},
	{
		id: 925,
		time: 924,
		velocity: 7.63,
		power: 781.325262314341,
		road: 8392.72893518519,
		acceleration: -0.0517592592592599
	},
	{
		id: 926,
		time: 925,
		velocity: 7.43305555555556,
		power: 193.209356160082,
		road: 8400.23152777778,
		acceleration: -0.132037037037036
	},
	{
		id: 927,
		time: 926,
		velocity: 7.24666666666667,
		power: 289.239922717335,
		road: 8407.60967592593,
		acceleration: -0.116851851851853
	},
	{
		id: 928,
		time: 927,
		velocity: 7.27944444444444,
		power: 938.478457903601,
		road: 8414.91814814815,
		acceleration: -0.0225
	},
	{
		id: 929,
		time: 928,
		velocity: 7.36555555555556,
		power: 1731.26675733286,
		road: 8422.26041666667,
		acceleration: 0.0900925925925931
	},
	{
		id: 930,
		time: 929,
		velocity: 7.51694444444444,
		power: 1269.46078415877,
		road: 8429.6587962963,
		acceleration: 0.0221296296296289
	},
	{
		id: 931,
		time: 930,
		velocity: 7.34583333333333,
		power: 203.203416916002,
		road: 8437.00388888889,
		acceleration: -0.128703703703703
	},
	{
		id: 932,
		time: 931,
		velocity: 6.97944444444444,
		power: -1462.66408308441,
		road: 8444.09837962963,
		acceleration: -0.3725
	},
	{
		id: 933,
		time: 932,
		velocity: 6.39944444444444,
		power: -2255.43660598638,
		road: 8450.75222222222,
		acceleration: -0.508796296296297
	},
	{
		id: 934,
		time: 933,
		velocity: 5.81944444444444,
		power: -1877.59484084628,
		road: 8456.91708333333,
		acceleration: -0.469166666666666
	},
	{
		id: 935,
		time: 934,
		velocity: 5.57194444444445,
		power: -1537.45441081197,
		road: 8462.63296296296,
		acceleration: -0.428796296296296
	},
	{
		id: 936,
		time: 935,
		velocity: 5.11305555555556,
		power: -1093.76800907017,
		road: 8467.95462962963,
		acceleration: -0.35962962962963
	},
	{
		id: 937,
		time: 936,
		velocity: 4.74055555555556,
		power: -787.174800206581,
		road: 8472.94273148148,
		acceleration: -0.307499999999999
	},
	{
		id: 938,
		time: 937,
		velocity: 4.64944444444444,
		power: -490.206553520548,
		road: 8477.65236111111,
		acceleration: -0.249444444444445
	},
	{
		id: 939,
		time: 938,
		velocity: 4.36472222222222,
		power: -1.72823216763132,
		road: 8482.16763888889,
		acceleration: -0.139259259259259
	},
	{
		id: 940,
		time: 939,
		velocity: 4.32277777777778,
		power: 624.657640072368,
		road: 8486.61791666667,
		acceleration: 0.00925925925925863
	},
	{
		id: 941,
		time: 940,
		velocity: 4.67722222222222,
		power: 2355.54025705971,
		road: 8491.26958333333,
		acceleration: 0.393518518518519
	},
	{
		id: 942,
		time: 941,
		velocity: 5.54527777777778,
		power: 3617.34188915044,
		road: 8496.41666666667,
		acceleration: 0.597314814814816
	},
	{
		id: 943,
		time: 942,
		velocity: 6.11472222222222,
		power: 3942.60181933943,
		road: 8502.15111111111,
		acceleration: 0.577407407407406
	},
	{
		id: 944,
		time: 943,
		velocity: 6.40944444444444,
		power: 2642.76830619173,
		road: 8508.3249537037,
		acceleration: 0.301388888888889
	},
	{
		id: 945,
		time: 944,
		velocity: 6.44944444444444,
		power: 2178.12388896949,
		road: 8514.75236111111,
		acceleration: 0.205740740740742
	},
	{
		id: 946,
		time: 945,
		velocity: 6.73194444444444,
		power: 2193.29594137843,
		road: 8521.38055555556,
		acceleration: 0.195833333333333
	},
	{
		id: 947,
		time: 946,
		velocity: 6.99694444444444,
		power: 2312.56958290513,
		road: 8528.20791666667,
		acceleration: 0.202499999999999
	},
	{
		id: 948,
		time: 947,
		velocity: 7.05694444444444,
		power: 2138.97563215277,
		road: 8535.21935185185,
		acceleration: 0.165648148148149
	},
	{
		id: 949,
		time: 948,
		velocity: 7.22888888888889,
		power: 2108.64417916898,
		road: 8542.39,
		acceleration: 0.152777777777777
	},
	{
		id: 950,
		time: 949,
		velocity: 7.45527777777778,
		power: 2030.32769449212,
		road: 8549.70416666667,
		acceleration: 0.13425925925926
	},
	{
		id: 951,
		time: 950,
		velocity: 7.45972222222222,
		power: 2241.22109303655,
		road: 8557.16398148148,
		acceleration: 0.157037037037037
	},
	{
		id: 952,
		time: 951,
		velocity: 7.7,
		power: 1372.95353204412,
		road: 8564.71805555556,
		acceleration: 0.0314814814814817
	},
	{
		id: 953,
		time: 952,
		velocity: 7.54972222222222,
		power: 1620.38119102099,
		road: 8572.31990740741,
		acceleration: 0.0640740740740728
	},
	{
		id: 954,
		time: 953,
		velocity: 7.65194444444444,
		power: 1575.52840000808,
		road: 8579.98162037037,
		acceleration: 0.0556481481481494
	},
	{
		id: 955,
		time: 954,
		velocity: 7.86694444444445,
		power: 2493.60205610674,
		road: 8587.75888888889,
		acceleration: 0.175462962962962
	},
	{
		id: 956,
		time: 955,
		velocity: 8.07611111111111,
		power: 2670.56482957387,
		road: 8595.71861111111,
		acceleration: 0.189444444444445
	},
	{
		id: 957,
		time: 956,
		velocity: 8.22027777777778,
		power: 3219.80473257502,
		road: 8603.89731481481,
		acceleration: 0.248518518518519
	},
	{
		id: 958,
		time: 957,
		velocity: 8.6125,
		power: 3525.19731585851,
		road: 8612.33592592593,
		acceleration: 0.271296296296295
	},
	{
		id: 959,
		time: 958,
		velocity: 8.89,
		power: 3413.6266605261,
		road: 8621.03134259259,
		acceleration: 0.242314814814815
	},
	{
		id: 960,
		time: 959,
		velocity: 8.94722222222222,
		power: 2957.25567007371,
		road: 8629.93625,
		acceleration: 0.176666666666666
	},
	{
		id: 961,
		time: 960,
		velocity: 9.1425,
		power: 3071.13674646622,
		road: 8639.0200462963,
		acceleration: 0.181111111111111
	},
	{
		id: 962,
		time: 961,
		velocity: 9.43333333333333,
		power: 4060.75896065505,
		road: 8648.33509259259,
		acceleration: 0.281388888888889
	},
	{
		id: 963,
		time: 962,
		velocity: 9.79138888888889,
		power: 3187.24281946472,
		road: 8657.87680555556,
		acceleration: 0.171944444444444
	},
	{
		id: 964,
		time: 963,
		velocity: 9.65833333333333,
		power: 2097.77274913121,
		road: 8667.52861111111,
		acceleration: 0.0482407407407415
	},
	{
		id: 965,
		time: 964,
		velocity: 9.57805555555556,
		power: 1621.84122934851,
		road: 8677.2024537037,
		acceleration: -0.00416666666666643
	},
	{
		id: 966,
		time: 965,
		velocity: 9.77888888888889,
		power: 3702.94279997468,
		road: 8686.98226851852,
		acceleration: 0.216111111111111
	},
	{
		id: 967,
		time: 966,
		velocity: 10.3066666666667,
		power: 6084.64712361447,
		road: 8697.09337962963,
		acceleration: 0.446481481481481
	},
	{
		id: 968,
		time: 967,
		velocity: 10.9175,
		power: 5760.32888427758,
		road: 8707.61986111111,
		acceleration: 0.38425925925926
	},
	{
		id: 969,
		time: 968,
		velocity: 10.9316666666667,
		power: 4058.07066704241,
		road: 8718.43851851852,
		acceleration: 0.200092592592593
	},
	{
		id: 970,
		time: 969,
		velocity: 10.9069444444444,
		power: 1168.10340739347,
		road: 8729.31648148148,
		acceleration: -0.0814814814814824
	},
	{
		id: 971,
		time: 970,
		velocity: 10.6730555555556,
		power: 1364.14493109942,
		road: 8740.12328703704,
		acceleration: -0.0608333333333331
	},
	{
		id: 972,
		time: 971,
		velocity: 10.7491666666667,
		power: 946.365926226586,
		road: 8750.84981481481,
		acceleration: -0.0997222222222209
	},
	{
		id: 973,
		time: 972,
		velocity: 10.6077777777778,
		power: 1844.51421887776,
		road: 8761.52134259259,
		acceleration: -0.0102777777777785
	},
	{
		id: 974,
		time: 973,
		velocity: 10.6422222222222,
		power: 1389.91656352387,
		road: 8772.16064814815,
		acceleration: -0.0541666666666671
	},
	{
		id: 975,
		time: 974,
		velocity: 10.5866666666667,
		power: 1709.67912926632,
		road: 8782.76208333333,
		acceleration: -0.0215740740740742
	},
	{
		id: 976,
		time: 975,
		velocity: 10.5430555555556,
		power: 2146.61929363483,
		road: 8793.36356481481,
		acceleration: 0.0216666666666665
	},
	{
		id: 977,
		time: 976,
		velocity: 10.7072222222222,
		power: 893.123030193149,
		road: 8803.92509259259,
		acceleration: -0.101574074074072
	},
	{
		id: 978,
		time: 977,
		velocity: 10.2819444444444,
		power: 2327.59645351989,
		road: 8814.45680555556,
		acceleration: 0.041944444444443
	},
	{
		id: 979,
		time: 978,
		velocity: 10.6688888888889,
		power: 596.753113447607,
		road: 8824.94462962963,
		acceleration: -0.12972222222222
	},
	{
		id: 980,
		time: 979,
		velocity: 10.3180555555556,
		power: 471.982057292192,
		road: 8835.29759259259,
		acceleration: -0.140000000000002
	},
	{
		id: 981,
		time: 980,
		velocity: 9.86194444444445,
		power: -2118.50430758252,
		road: 8845.37791666667,
		acceleration: -0.405277777777778
	},
	{
		id: 982,
		time: 981,
		velocity: 9.45305555555555,
		power: -1734.66561303118,
		road: 8855.07148148148,
		acceleration: -0.368240740740738
	},
	{
		id: 983,
		time: 982,
		velocity: 9.21333333333333,
		power: -884.572390763997,
		road: 8864.44291666667,
		acceleration: -0.276018518518521
	},
	{
		id: 984,
		time: 983,
		velocity: 9.03388888888889,
		power: -387.990538903575,
		road: 8873.56685185185,
		acceleration: -0.218981481481482
	},
	{
		id: 985,
		time: 984,
		velocity: 8.79611111111111,
		power: -1491.90782943898,
		road: 8882.40694444444,
		acceleration: -0.348703703703704
	},
	{
		id: 986,
		time: 985,
		velocity: 8.16722222222222,
		power: -1394.85332120655,
		road: 8890.90240740741,
		acceleration: -0.340555555555556
	},
	{
		id: 987,
		time: 986,
		velocity: 8.01222222222222,
		power: -2425.44390532854,
		road: 8898.98787037037,
		acceleration: -0.479444444444443
	},
	{
		id: 988,
		time: 987,
		velocity: 7.35777777777778,
		power: -887.137888361023,
		road: 8906.69268518519,
		acceleration: -0.281851851851852
	},
	{
		id: 989,
		time: 988,
		velocity: 7.32166666666667,
		power: 551.498736214963,
		road: 8914.21546296296,
		acceleration: -0.0822222222222226
	},
	{
		id: 990,
		time: 989,
		velocity: 7.76555555555556,
		power: 3880.04909380804,
		road: 8921.88277777778,
		acceleration: 0.371296296296296
	},
	{
		id: 991,
		time: 990,
		velocity: 8.47166666666667,
		power: 7557.27155559701,
		road: 8930.13393518519,
		acceleration: 0.79638888888889
	},
	{
		id: 992,
		time: 991,
		velocity: 9.71083333333333,
		power: 10312.5432361998,
		road: 8939.28740740741,
		acceleration: 1.00824074074074
	},
	{
		id: 993,
		time: 992,
		velocity: 10.7902777777778,
		power: 10794.5049078241,
		road: 8949.41180555556,
		acceleration: 0.933611111111111
	},
	{
		id: 994,
		time: 993,
		velocity: 11.2725,
		power: 7007.24584607149,
		road: 8960.24546296296,
		acceleration: 0.484907407407409
	},
	{
		id: 995,
		time: 994,
		velocity: 11.1655555555556,
		power: 2587.15162386762,
		road: 8971.34537037037,
		acceleration: 0.0475925925925917
	},
	{
		id: 996,
		time: 995,
		velocity: 10.9330555555556,
		power: 3511.12572752514,
		road: 8982.53462962963,
		acceleration: 0.13111111111111
	},
	{
		id: 997,
		time: 996,
		velocity: 11.6658333333333,
		power: 6031.40711198256,
		road: 8993.96555555556,
		acceleration: 0.352222222222222
	},
	{
		id: 998,
		time: 997,
		velocity: 12.2222222222222,
		power: 8559.34502978726,
		road: 9005.84671296296,
		acceleration: 0.54824074074074
	},
	{
		id: 999,
		time: 998,
		velocity: 12.5777777777778,
		power: 6329.0482455383,
		road: 9018.16476851852,
		acceleration: 0.325555555555555
	},
	{
		id: 1000,
		time: 999,
		velocity: 12.6425,
		power: 3613.03605988323,
		road: 9030.68884259259,
		acceleration: 0.0864814814814832
	},
	{
		id: 1001,
		time: 1000,
		velocity: 12.4816666666667,
		power: 1476.6918246577,
		road: 9043.2100462963,
		acceleration: -0.0922222222222242
	},
	{
		id: 1002,
		time: 1001,
		velocity: 12.3011111111111,
		power: 940.108634340356,
		road: 9055.61773148148,
		acceleration: -0.134814814814813
	},
	{
		id: 1003,
		time: 1002,
		velocity: 12.2380555555556,
		power: 358.092028815442,
		road: 9067.86731481482,
		acceleration: -0.181388888888888
	},
	{
		id: 1004,
		time: 1003,
		velocity: 11.9375,
		power: 76.1456378631009,
		road: 9079.92481481482,
		acceleration: -0.202777777777778
	},
	{
		id: 1005,
		time: 1004,
		velocity: 11.6927777777778,
		power: -1544.45563989642,
		road: 9091.70935185186,
		acceleration: -0.343148148148149
	},
	{
		id: 1006,
		time: 1005,
		velocity: 11.2086111111111,
		power: -727.15475717685,
		road: 9103.18824074074,
		acceleration: -0.268148148148146
	},
	{
		id: 1007,
		time: 1006,
		velocity: 11.1330555555556,
		power: -489.34482428226,
		road: 9114.41097222223,
		acceleration: -0.244166666666668
	},
	{
		id: 1008,
		time: 1007,
		velocity: 10.9602777777778,
		power: 941.257440138511,
		road: 9125.45819444445,
		acceleration: -0.10685185185185
	},
	{
		id: 1009,
		time: 1008,
		velocity: 10.8880555555556,
		power: 998.289876641821,
		road: 9136.40236111111,
		acceleration: -0.0992592592592594
	},
	{
		id: 1010,
		time: 1009,
		velocity: 10.8352777777778,
		power: 1898.22566424313,
		road: 9147.29120370371,
		acceleration: -0.0113888888888916
	},
	{
		id: 1011,
		time: 1010,
		velocity: 10.9261111111111,
		power: 2713.02812974201,
		road: 9158.20740740741,
		acceleration: 0.0661111111111126
	},
	{
		id: 1012,
		time: 1011,
		velocity: 11.0863888888889,
		power: 3112.81209341425,
		road: 9169.20726851852,
		acceleration: 0.101203703703703
	},
	{
		id: 1013,
		time: 1012,
		velocity: 11.1388888888889,
		power: 4775.80415213859,
		road: 9180.38291666667,
		acceleration: 0.250370370370373
	},
	{
		id: 1014,
		time: 1013,
		velocity: 11.6772222222222,
		power: 5227.22979597446,
		road: 9191.82273148148,
		acceleration: 0.27796296296296
	},
	{
		id: 1015,
		time: 1014,
		velocity: 11.9202777777778,
		power: 5190.64409546321,
		road: 9203.53157407408,
		acceleration: 0.260092592592594
	},
	{
		id: 1016,
		time: 1015,
		velocity: 11.9191666666667,
		power: 2883.29988419691,
		road: 9215.39449074074,
		acceleration: 0.0480555555555551
	},
	{
		id: 1017,
		time: 1016,
		velocity: 11.8213888888889,
		power: 2312.79551097045,
		road: 9227.27990740741,
		acceleration: -0.00305555555555337
	},
	{
		id: 1018,
		time: 1017,
		velocity: 11.9111111111111,
		power: 2270.18391152721,
		road: 9239.16046296297,
		acceleration: -0.00666666666666771
	},
	{
		id: 1019,
		time: 1018,
		velocity: 11.8991666666667,
		power: 3396.73980812744,
		road: 9251.08324074074,
		acceleration: 0.0911111111111111
	},
	{
		id: 1020,
		time: 1019,
		velocity: 12.0947222222222,
		power: 3612.31320076721,
		road: 9263.10462962963,
		acceleration: 0.10611111111111
	},
	{
		id: 1021,
		time: 1020,
		velocity: 12.2294444444444,
		power: 4564.31490395343,
		road: 9275.27027777778,
		acceleration: 0.182407407407407
	},
	{
		id: 1022,
		time: 1021,
		velocity: 12.4463888888889,
		power: 4023.68972479162,
		road: 9287.59175925926,
		acceleration: 0.129259259259261
	},
	{
		id: 1023,
		time: 1022,
		velocity: 12.4825,
		power: 3432.11050091678,
		road: 9300.01541666667,
		acceleration: 0.0750925925925916
	},
	{
		id: 1024,
		time: 1023,
		velocity: 12.4547222222222,
		power: 1845.39324318418,
		road: 9312.44712962963,
		acceleration: -0.0589814814814815
	},
	{
		id: 1025,
		time: 1024,
		velocity: 12.2694444444444,
		power: 1881.11288894841,
		road: 9324.82212962963,
		acceleration: -0.0544444444444423
	},
	{
		id: 1026,
		time: 1025,
		velocity: 12.3191666666667,
		power: 1721.64874158898,
		road: 9337.13671296297,
		acceleration: -0.0663888888888895
	},
	{
		id: 1027,
		time: 1026,
		velocity: 12.2555555555556,
		power: 2636.06426668765,
		road: 9349.42425925926,
		acceleration: 0.0123148148148147
	},
	{
		id: 1028,
		time: 1027,
		velocity: 12.3063888888889,
		power: 3030.99361485099,
		road: 9361.74046296297,
		acceleration: 0.0449999999999982
	},
	{
		id: 1029,
		time: 1028,
		velocity: 12.4541666666667,
		power: 3432.25531861701,
		road: 9374.11759259259,
		acceleration: 0.0768518518518526
	},
	{
		id: 1030,
		time: 1029,
		velocity: 12.4861111111111,
		power: 3835.76378421606,
		road: 9386.58680555556,
		acceleration: 0.107314814814815
	},
	{
		id: 1031,
		time: 1030,
		velocity: 12.6283333333333,
		power: 2534.16609325788,
		road: 9399.10782407408,
		acceleration: -0.00370370370370132
	},
	{
		id: 1032,
		time: 1031,
		velocity: 12.4430555555556,
		power: 343.077433427939,
		road: 9411.53421296297,
		acceleration: -0.185555555555558
	},
	{
		id: 1033,
		time: 1032,
		velocity: 11.9294444444444,
		power: -587.451734457625,
		road: 9423.73689814815,
		acceleration: -0.261851851851851
	},
	{
		id: 1034,
		time: 1033,
		velocity: 11.8427777777778,
		power: -1084.85974031505,
		road: 9435.65717592593,
		acceleration: -0.302962962962964
	},
	{
		id: 1035,
		time: 1034,
		velocity: 11.5341666666667,
		power: 1445.24401872485,
		road: 9447.38805555556,
		acceleration: -0.0758333333333319
	},
	{
		id: 1036,
		time: 1035,
		velocity: 11.7019444444444,
		power: 2166.38569908585,
		road: 9459.07597222222,
		acceleration: -0.0100925925925921
	},
	{
		id: 1037,
		time: 1036,
		velocity: 11.8125,
		power: 3690.48871465957,
		road: 9470.82097222223,
		acceleration: 0.124259259259258
	},
	{
		id: 1038,
		time: 1037,
		velocity: 11.9069444444444,
		power: 2342.32110835167,
		road: 9482.62907407408,
		acceleration: 0.00194444444444564
	},
	{
		id: 1039,
		time: 1038,
		velocity: 11.7077777777778,
		power: -120.953239619877,
		road: 9494.33041666667,
		acceleration: -0.215462962962965
	},
	{
		id: 1040,
		time: 1039,
		velocity: 11.1661111111111,
		power: -1126.02567692609,
		road: 9505.77180555556,
		acceleration: -0.304444444444444
	},
	{
		id: 1041,
		time: 1040,
		velocity: 10.9936111111111,
		power: -4277.38097104977,
		road: 9516.75902777778,
		acceleration: -0.603888888888889
	},
	{
		id: 1042,
		time: 1041,
		velocity: 9.89611111111111,
		power: -3334.22667435227,
		road: 9527.18212962963,
		acceleration: -0.524351851851852
	},
	{
		id: 1043,
		time: 1042,
		velocity: 9.59305555555556,
		power: -5929.11665785793,
		road: 9536.93337962963,
		acceleration: -0.819351851851851
	},
	{
		id: 1044,
		time: 1043,
		velocity: 8.53555555555556,
		power: -5447.72436752861,
		road: 9545.86856481481,
		acceleration: -0.812777777777779
	},
	{
		id: 1045,
		time: 1044,
		velocity: 7.45777777777778,
		power: -6020.17663268917,
		road: 9553.92263888889,
		acceleration: -0.949444444444445
	},
	{
		id: 1046,
		time: 1045,
		velocity: 6.74472222222222,
		power: -4183.17355006384,
		road: 9561.11810185185,
		acceleration: -0.767777777777778
	},
	{
		id: 1047,
		time: 1046,
		velocity: 6.23222222222222,
		power: -2355.33220003553,
		road: 9567.66472222222,
		acceleration: -0.529907407407407
	},
	{
		id: 1048,
		time: 1047,
		velocity: 5.86805555555556,
		power: -623.455843376184,
		road: 9573.81875,
		acceleration: -0.255277777777778
	},
	{
		id: 1049,
		time: 1048,
		velocity: 5.97888888888889,
		power: 414.25692483389,
		road: 9579.80773148148,
		acceleration: -0.0748148148148147
	},
	{
		id: 1050,
		time: 1049,
		velocity: 6.00777777777778,
		power: 803.714932944787,
		road: 9585.75671296296,
		acceleration: -0.00518518518518452
	},
	{
		id: 1051,
		time: 1050,
		velocity: 5.8525,
		power: 569.361760680085,
		road: 9591.68009259259,
		acceleration: -0.0460185185185189
	},
	{
		id: 1052,
		time: 1051,
		velocity: 5.84083333333333,
		power: 298.360154410083,
		road: 9597.53393518519,
		acceleration: -0.093055555555555
	},
	{
		id: 1053,
		time: 1052,
		velocity: 5.72861111111111,
		power: -981.068830227039,
		road: 9603.17712962963,
		acceleration: -0.328240740740742
	},
	{
		id: 1054,
		time: 1053,
		velocity: 4.86777777777778,
		power: -1206.36048493396,
		road: 9608.46458333334,
		acceleration: -0.38324074074074
	},
	{
		id: 1055,
		time: 1054,
		velocity: 4.69111111111111,
		power: -1258.47333446238,
		road: 9613.35453703704,
		acceleration: -0.411759259259259
	},
	{
		id: 1056,
		time: 1055,
		velocity: 4.49333333333333,
		power: -41.3988190092807,
		road: 9617.96421296296,
		acceleration: -0.148796296296297
	},
	{
		id: 1057,
		time: 1056,
		velocity: 4.42138888888889,
		power: 311.1854636282,
		road: 9622.46648148148,
		acceleration: -0.0660185185185185
	},
	{
		id: 1058,
		time: 1057,
		velocity: 4.49305555555556,
		power: 1844.06241952869,
		road: 9627.07662037037,
		acceleration: 0.281759259259259
	},
	{
		id: 1059,
		time: 1058,
		velocity: 5.33861111111111,
		power: 3624.5341755236,
		road: 9632.13388888889,
		acceleration: 0.6125
	},
	{
		id: 1060,
		time: 1059,
		velocity: 6.25888888888889,
		power: 6006.94563252786,
		road: 9637.96587962963,
		acceleration: 0.936944444444445
	},
	{
		id: 1061,
		time: 1060,
		velocity: 7.30388888888889,
		power: 7164.32312748883,
		road: 9644.74523148148,
		acceleration: 0.957777777777778
	},
	{
		id: 1062,
		time: 1061,
		velocity: 8.21194444444444,
		power: 7547.71411553002,
		road: 9652.43856481481,
		acceleration: 0.870185185185185
	},
	{
		id: 1063,
		time: 1062,
		velocity: 8.86944444444444,
		power: 6364.14438100504,
		road: 9660.87921296296,
		acceleration: 0.624444444444444
	},
	{
		id: 1064,
		time: 1063,
		velocity: 9.17722222222222,
		power: 4634.59552443251,
		road: 9669.81810185185,
		acceleration: 0.372037037037037
	},
	{
		id: 1065,
		time: 1064,
		velocity: 9.32805555555556,
		power: 3153.64289911942,
		road: 9679.035,
		acceleration: 0.183981481481482
	},
	{
		id: 1066,
		time: 1065,
		velocity: 9.42138888888889,
		power: 2451.97495084791,
		road: 9688.39305555556,
		acceleration: 0.0983333333333345
	},
	{
		id: 1067,
		time: 1066,
		velocity: 9.47222222222222,
		power: 5514.77158765765,
		road: 9698.01143518519,
		acceleration: 0.422314814814813
	},
	{
		id: 1068,
		time: 1067,
		velocity: 10.595,
		power: 8275.95233550325,
		road: 9708.17537037037,
		acceleration: 0.668796296296296
	},
	{
		id: 1069,
		time: 1068,
		velocity: 11.4277777777778,
		power: 12414.0908726854,
		road: 9719.16814814815,
		acceleration: 0.988888888888889
	},
	{
		id: 1070,
		time: 1069,
		velocity: 12.4388888888889,
		power: 8863.05913189955,
		road: 9730.94699074074,
		acceleration: 0.583240740740742
	},
	{
		id: 1071,
		time: 1070,
		velocity: 12.3447222222222,
		power: 5922.67962644666,
		road: 9743.16569444444,
		acceleration: 0.29648148148148
	},
	{
		id: 1072,
		time: 1071,
		velocity: 12.3172222222222,
		power: 3075.68906656356,
		road: 9755.55574074074,
		acceleration: 0.0462037037037035
	},
	{
		id: 1073,
		time: 1072,
		velocity: 12.5775,
		power: 4945.32723716529,
		road: 9768.06814814815,
		acceleration: 0.198518518518519
	},
	{
		id: 1074,
		time: 1073,
		velocity: 12.9402777777778,
		power: 5356.68812659644,
		road: 9780.79106481481,
		acceleration: 0.2225
	},
	{
		id: 1075,
		time: 1074,
		velocity: 12.9847222222222,
		power: 5453.34517775818,
		road: 9793.735,
		acceleration: 0.219537037037037
	},
	{
		id: 1076,
		time: 1075,
		velocity: 13.2361111111111,
		power: 4650.51588390863,
		road: 9806.86199074074,
		acceleration: 0.146574074074072
	},
	{
		id: 1077,
		time: 1076,
		velocity: 13.38,
		power: 4731.68027578978,
		road: 9820.13560185185,
		acceleration: 0.146666666666668
	},
	{
		id: 1078,
		time: 1077,
		velocity: 13.4247222222222,
		power: 4864.76784699254,
		road: 9833.55787037037,
		acceleration: 0.150648148148147
	},
	{
		id: 1079,
		time: 1078,
		velocity: 13.6880555555556,
		power: 4420.60238604104,
		road: 9847.11078703704,
		acceleration: 0.110648148148149
	},
	{
		id: 1080,
		time: 1079,
		velocity: 13.7119444444444,
		power: 4282.01579853575,
		road: 9860.76694444444,
		acceleration: 0.0958333333333332
	},
	{
		id: 1081,
		time: 1080,
		velocity: 13.7122222222222,
		power: 2741.6027422988,
		road: 9874.45930555555,
		acceleration: -0.0234259259259275
	},
	{
		id: 1082,
		time: 1081,
		velocity: 13.6177777777778,
		power: 2729.70594257868,
		road: 9888.12814814815,
		acceleration: -0.0236111111111086
	},
	{
		id: 1083,
		time: 1082,
		velocity: 13.6411111111111,
		power: 3679.40465072312,
		road: 9901.80953703704,
		acceleration: 0.0487037037037048
	},
	{
		id: 1084,
		time: 1083,
		velocity: 13.8583333333333,
		power: 5071.97090893668,
		road: 9915.59074074074,
		acceleration: 0.150925925925925
	},
	{
		id: 1085,
		time: 1084,
		velocity: 14.0705555555556,
		power: 5054.86597726652,
		road: 9929.51902777778,
		acceleration: 0.14324074074074
	},
	{
		id: 1086,
		time: 1085,
		velocity: 14.0708333333333,
		power: 2922.01758242427,
		road: 9943.50939814815,
		acceleration: -0.0190740740740729
	},
	{
		id: 1087,
		time: 1086,
		velocity: 13.8011111111111,
		power: 228.082299512432,
		road: 9957.38087962963,
		acceleration: -0.218703703703707
	},
	{
		id: 1088,
		time: 1087,
		velocity: 13.4144444444444,
		power: -2492.0739867438,
		road: 9970.9312037037,
		acceleration: -0.423611111111109
	},
	{
		id: 1089,
		time: 1088,
		velocity: 12.8,
		power: -3036.40428038082,
		road: 9984.0362037037,
		acceleration: -0.467037037037038
	},
	{
		id: 1090,
		time: 1089,
		velocity: 12.4,
		power: -2187.50889092736,
		road: 9996.70824074074,
		acceleration: -0.398888888888889
	},
	{
		id: 1091,
		time: 1090,
		velocity: 12.2177777777778,
		power: -238.08054528629,
		road: 10009.0639814815,
		acceleration: -0.233703703703704
	},
	{
		id: 1092,
		time: 1091,
		velocity: 12.0988888888889,
		power: -432.309045592642,
		road: 10021.1790740741,
		acceleration: -0.247592592592593
	},
	{
		id: 1093,
		time: 1092,
		velocity: 11.6572222222222,
		power: -403.394510940918,
		road: 10033.0491203704,
		acceleration: -0.2425
	},
	{
		id: 1094,
		time: 1093,
		velocity: 11.4902777777778,
		power: -2747.16881256881,
		road: 10044.5717592593,
		acceleration: -0.452314814814816
	},
	{
		id: 1095,
		time: 1094,
		velocity: 10.7419444444444,
		power: -3256.95844345003,
		road: 10055.615462963,
		acceleration: -0.505555555555555
	},
	{
		id: 1096,
		time: 1095,
		velocity: 10.1405555555556,
		power: -2824.5682677951,
		road: 10066.1708796296,
		acceleration: -0.47101851851852
	},
	{
		id: 1097,
		time: 1096,
		velocity: 10.0772222222222,
		power: -1634.46173917242,
		road: 10076.3135185185,
		acceleration: -0.354537037037035
	},
	{
		id: 1098,
		time: 1097,
		velocity: 9.67833333333333,
		power: 75.9062189238288,
		road: 10086.191712963,
		acceleration: -0.174351851851853
	},
	{
		id: 1099,
		time: 1098,
		velocity: 9.6175,
		power: 241.156537872301,
		road: 10095.905462963,
		acceleration: -0.154537037037036
	},
	{
		id: 1100,
		time: 1099,
		velocity: 9.61361111111111,
		power: 1311.70469927663,
		road: 10105.52375,
		acceleration: -0.0363888888888884
	},
	{
		id: 1101,
		time: 1100,
		velocity: 9.56916666666667,
		power: 3214.33031282806,
		road: 10115.2079166667,
		acceleration: 0.168148148148148
	},
	{
		id: 1102,
		time: 1101,
		velocity: 10.1219444444444,
		power: 5342.12500426114,
		road: 10125.1660185185,
		acceleration: 0.379722222222222
	},
	{
		id: 1103,
		time: 1102,
		velocity: 10.7527777777778,
		power: 6161.80830229865,
		road: 10135.5318518519,
		acceleration: 0.435740740740739
	},
	{
		id: 1104,
		time: 1103,
		velocity: 10.8763888888889,
		power: 6560.95876116537,
		road: 10146.3374074074,
		acceleration: 0.443703703703704
	},
	{
		id: 1105,
		time: 1104,
		velocity: 11.4530555555556,
		power: 4724.39879513861,
		road: 10157.4882407407,
		acceleration: 0.246851851851853
	},
	{
		id: 1106,
		time: 1105,
		velocity: 11.4933333333333,
		power: 3603.90975588071,
		road: 10168.8291666667,
		acceleration: 0.133333333333333
	},
	{
		id: 1107,
		time: 1106,
		velocity: 11.2763888888889,
		power: 854.699606568154,
		road: 10180.17625,
		acceleration: -0.12101851851852
	},
	{
		id: 1108,
		time: 1107,
		velocity: 11.09,
		power: 198.206840721516,
		road: 10191.3730555556,
		acceleration: -0.179537037037036
	},
	{
		id: 1109,
		time: 1108,
		velocity: 10.9547222222222,
		power: -46.7978578467947,
		road: 10202.38,
		acceleration: -0.200185185185187
	},
	{
		id: 1110,
		time: 1109,
		velocity: 10.6758333333333,
		power: 927.926321563831,
		road: 10213.2347685185,
		acceleration: -0.104166666666664
	},
	{
		id: 1111,
		time: 1110,
		velocity: 10.7775,
		power: 969.229298054798,
		road: 10223.9884259259,
		acceleration: -0.098055555555554
	},
	{
		id: 1112,
		time: 1111,
		velocity: 10.6605555555556,
		power: 1712.07020166154,
		road: 10234.6811111111,
		acceleration: -0.0238888888888908
	},
	{
		id: 1113,
		time: 1112,
		velocity: 10.6041666666667,
		power: 612.901966789827,
		road: 10245.2966666667,
		acceleration: -0.130370370370372
	},
	{
		id: 1114,
		time: 1113,
		velocity: 10.3863888888889,
		power: 2275.71510011242,
		road: 10255.8648148148,
		acceleration: 0.0355555555555576
	},
	{
		id: 1115,
		time: 1114,
		velocity: 10.7672222222222,
		power: 4944.46081527607,
		road: 10266.59625,
		acceleration: 0.291018518518516
	},
	{
		id: 1116,
		time: 1115,
		velocity: 11.4772222222222,
		power: 7900.49281532152,
		road: 10277.7460185185,
		acceleration: 0.545648148148151
	},
	{
		id: 1117,
		time: 1116,
		velocity: 12.0233333333333,
		power: 9860.25339305554,
		road: 10289.505462963,
		acceleration: 0.673703703703703
	},
	{
		id: 1118,
		time: 1117,
		velocity: 12.7883333333333,
		power: 7157.39137652444,
		road: 10301.8005092593,
		acceleration: 0.397499999999999
	},
	{
		id: 1119,
		time: 1118,
		velocity: 12.6697222222222,
		power: 5288.73564492801,
		road: 10314.4056481481,
		acceleration: 0.222685185185185
	},
	{
		id: 1120,
		time: 1119,
		velocity: 12.6913888888889,
		power: 2157.00411020925,
		road: 10327.1019907407,
		acceleration: -0.0402777777777761
	},
	{
		id: 1121,
		time: 1120,
		velocity: 12.6675,
		power: 2194.19942491403,
		road: 10339.7601388889,
		acceleration: -0.0361111111111114
	},
	{
		id: 1122,
		time: 1121,
		velocity: 12.5613888888889,
		power: 1681.39550886043,
		road: 10352.3616666667,
		acceleration: -0.0771296296296313
	},
	{
		id: 1123,
		time: 1122,
		velocity: 12.46,
		power: 856.730991502322,
		road: 10364.8528703704,
		acceleration: -0.143518518518517
	},
	{
		id: 1124,
		time: 1123,
		velocity: 12.2369444444444,
		power: -234.792610680454,
		road: 10377.1559259259,
		acceleration: -0.232777777777777
	},
	{
		id: 1125,
		time: 1124,
		velocity: 11.8630555555556,
		power: 1573.19208782898,
		road: 10389.3051388889,
		acceleration: -0.0749074074074088
	},
	{
		id: 1126,
		time: 1125,
		velocity: 12.2352777777778,
		power: 7636.58093820671,
		road: 10401.6349074074,
		acceleration: 0.436018518518518
	},
	{
		id: 1127,
		time: 1126,
		velocity: 13.545,
		power: 16328.3217144629,
		road: 10414.7240740741,
		acceleration: 1.08277777777778
	},
	{
		id: 1128,
		time: 1127,
		velocity: 15.1113888888889,
		power: 25812.6981980547,
		road: 10429.1674074074,
		acceleration: 1.62555555555556
	},
	{
		id: 1129,
		time: 1128,
		velocity: 17.1119444444444,
		power: 29634.7768882049,
		road: 10445.2503240741,
		acceleration: 1.65361111111111
	},
	{
		id: 1130,
		time: 1129,
		velocity: 18.5058333333333,
		power: 30431.6676040423,
		road: 10462.90875,
		acceleration: 1.4974074074074
	},
	{
		id: 1131,
		time: 1130,
		velocity: 19.6036111111111,
		power: 19202.3345840264,
		road: 10481.6867592593,
		acceleration: 0.741759259259261
	},
	{
		id: 1132,
		time: 1131,
		velocity: 19.3372222222222,
		power: 8806.94407989736,
		road: 10500.9070833333,
		acceleration: 0.142870370370375
	},
	{
		id: 1133,
		time: 1132,
		velocity: 18.9344444444444,
		power: -1114.66822241615,
		road: 10520.0019444444,
		acceleration: -0.393796296296298
	},
	{
		id: 1134,
		time: 1133,
		velocity: 18.4222222222222,
		power: -2914.12828323801,
		road: 10538.6565740741,
		acceleration: -0.486666666666668
	},
	{
		id: 1135,
		time: 1134,
		velocity: 17.8772222222222,
		power: -4657.62127142364,
		road: 10556.7773611111,
		acceleration: -0.581018518518519
	},
	{
		id: 1136,
		time: 1135,
		velocity: 17.1913888888889,
		power: -4376.84375153929,
		road: 10574.3268055556,
		acceleration: -0.561666666666667
	},
	{
		id: 1137,
		time: 1136,
		velocity: 16.7372222222222,
		power: -7934.57590060354,
		road: 10591.2056481481,
		acceleration: -0.779537037037038
	},
	{
		id: 1138,
		time: 1137,
		velocity: 15.5386111111111,
		power: -6530.27644682753,
		road: 10607.3459722222,
		acceleration: -0.6975
	},
	{
		id: 1139,
		time: 1138,
		velocity: 15.0988888888889,
		power: -9058.31567944878,
		road: 10622.6986574074,
		acceleration: -0.877777777777776
	},
	{
		id: 1140,
		time: 1139,
		velocity: 14.1038888888889,
		power: -6506.91111158143,
		road: 10637.255,
		acceleration: -0.714907407407408
	},
	{
		id: 1141,
		time: 1140,
		velocity: 13.3938888888889,
		power: -6389.35383478312,
		road: 10651.0943981481,
		acceleration: -0.71898148148148
	},
	{
		id: 1142,
		time: 1141,
		velocity: 12.9419444444444,
		power: -6784.24978609593,
		road: 10664.1908333333,
		acceleration: -0.766944444444444
	},
	{
		id: 1143,
		time: 1142,
		velocity: 11.8030555555556,
		power: -8589.95354551311,
		road: 10676.4299537037,
		acceleration: -0.947685185185186
	},
	{
		id: 1144,
		time: 1143,
		velocity: 10.5508333333333,
		power: -12753.2244109175,
		road: 10687.4922685185,
		acceleration: -1.40592592592593
	},
	{
		id: 1145,
		time: 1144,
		velocity: 8.72416666666667,
		power: -8968.60867049495,
		road: 10697.2799074074,
		acceleration: -1.14342592592593
	},
	{
		id: 1146,
		time: 1145,
		velocity: 8.37277777777778,
		power: -5277.44698754696,
		road: 10706.0958333333,
		acceleration: -0.799999999999999
	},
	{
		id: 1147,
		time: 1146,
		velocity: 8.15083333333333,
		power: -1761.00814920609,
		road: 10714.3164814815,
		acceleration: -0.390555555555556
	},
	{
		id: 1148,
		time: 1147,
		velocity: 7.5525,
		power: -2858.82710730949,
		road: 10722.0674074074,
		acceleration: -0.548888888888889
	},
	{
		id: 1149,
		time: 1148,
		velocity: 6.72611111111111,
		power: -4126.92376583355,
		road: 10729.1601388889,
		acceleration: -0.7675
	},
	{
		id: 1150,
		time: 1149,
		velocity: 5.84833333333333,
		power: -3459.79215070171,
		road: 10735.5074074074,
		acceleration: -0.723425925925927
	},
	{
		id: 1151,
		time: 1150,
		velocity: 5.38222222222222,
		power: -2157.47704831484,
		road: 10741.2214351852,
		acceleration: -0.543055555555554
	},
	{
		id: 1152,
		time: 1151,
		velocity: 5.09694444444444,
		power: -1470.55995321387,
		road: 10746.4443981481,
		acceleration: -0.439074074074075
	},
	{
		id: 1153,
		time: 1152,
		velocity: 4.53111111111111,
		power: -1105.77397400815,
		road: 10751.2566666667,
		acceleration: -0.382314814814814
	},
	{
		id: 1154,
		time: 1153,
		velocity: 4.23527777777778,
		power: -1310.8230914928,
		road: 10755.6516203704,
		acceleration: -0.452314814814815
	},
	{
		id: 1155,
		time: 1154,
		velocity: 3.74,
		power: -2385.05514513938,
		road: 10759.4193055556,
		acceleration: -0.802222222222222
	},
	{
		id: 1156,
		time: 1155,
		velocity: 2.12444444444444,
		power: -3231.58308577371,
		road: 10762.08,
		acceleration: -1.41175925925926
	},
	{
		id: 1157,
		time: 1156,
		velocity: 0,
		power: -1411.3204418074,
		road: 10763.4114814815,
		acceleration: -1.24666666666667
	},
	{
		id: 1158,
		time: 1157,
		velocity: 0,
		power: -194.761736712151,
		road: 10763.7655555556,
		acceleration: -0.708148148148148
	},
	{
		id: 1159,
		time: 1158,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1160,
		time: 1159,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1161,
		time: 1160,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1162,
		time: 1161,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1163,
		time: 1162,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1164,
		time: 1163,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1165,
		time: 1164,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1166,
		time: 1165,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1167,
		time: 1166,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1168,
		time: 1167,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1169,
		time: 1168,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1170,
		time: 1169,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1171,
		time: 1170,
		velocity: 0,
		power: 0,
		road: 10763.7655555556,
		acceleration: 0
	},
	{
		id: 1172,
		time: 1171,
		velocity: 0,
		power: 44.766813343348,
		road: 10763.8906481481,
		acceleration: 0.250185185185185
	},
	{
		id: 1173,
		time: 1172,
		velocity: 0.750555555555556,
		power: 1049.17374192511,
		road: 10764.7334722222,
		acceleration: 1.18527777777778
	},
	{
		id: 1174,
		time: 1173,
		velocity: 3.55583333333333,
		power: 3395.77607865509,
		road: 10766.921712963,
		acceleration: 1.50555555555556
	},
	{
		id: 1175,
		time: 1174,
		velocity: 4.51666666666667,
		power: 4622.6030266331,
		road: 10770.4796759259,
		acceleration: 1.23388888888889
	},
	{
		id: 1176,
		time: 1175,
		velocity: 4.45222222222222,
		power: 1688.87717093116,
		road: 10774.7918518519,
		acceleration: 0.274537037037038
	},
	{
		id: 1177,
		time: 1176,
		velocity: 4.37944444444444,
		power: 1197.12832222574,
		road: 10779.3112962963,
		acceleration: 0.14
	},
	{
		id: 1178,
		time: 1177,
		velocity: 4.93666666666667,
		power: 4404.24619198082,
		road: 10784.2950925926,
		acceleration: 0.788703703703704
	},
	{
		id: 1179,
		time: 1178,
		velocity: 6.81833333333333,
		power: 7304.88165993248,
		road: 10790.2452314815,
		acceleration: 1.14398148148148
	},
	{
		id: 1180,
		time: 1179,
		velocity: 7.81138888888889,
		power: 9146.39042316512,
		road: 10797.3645833333,
		acceleration: 1.19444444444445
	},
	{
		id: 1181,
		time: 1180,
		velocity: 8.52,
		power: 7200.14925965205,
		road: 10805.4658333333,
		acceleration: 0.76935185185185
	},
	{
		id: 1182,
		time: 1181,
		velocity: 9.12638888888889,
		power: 6224.55262301026,
		road: 10814.2389351852,
		acceleration: 0.574351851851853
	},
	{
		id: 1183,
		time: 1182,
		velocity: 9.53444444444444,
		power: 5423.93967270824,
		road: 10823.5181018519,
		acceleration: 0.437777777777777
	},
	{
		id: 1184,
		time: 1183,
		velocity: 9.83333333333333,
		power: 3842.27216151513,
		road: 10833.1360648148,
		acceleration: 0.239814814814817
	},
	{
		id: 1185,
		time: 1184,
		velocity: 9.84583333333333,
		power: 1178.80503360918,
		road: 10842.8473611111,
		acceleration: -0.0531481481481499
	},
	{
		id: 1186,
		time: 1185,
		velocity: 9.375,
		power: 293.601987484624,
		road: 10852.4583796296,
		acceleration: -0.147407407407409
	},
	{
		id: 1187,
		time: 1186,
		velocity: 9.39111111111111,
		power: 71.6267606352283,
		road: 10861.910787037,
		acceleration: -0.169814814814814
	},
	{
		id: 1188,
		time: 1187,
		velocity: 9.33638888888889,
		power: 1425.56163175128,
		road: 10871.2698611111,
		acceleration: -0.0168518518518539
	},
	{
		id: 1189,
		time: 1188,
		velocity: 9.32444444444444,
		power: 3083.04491403412,
		road: 10880.703287037,
		acceleration: 0.165555555555557
	},
	{
		id: 1190,
		time: 1189,
		velocity: 9.88777777777778,
		power: 3305.49104188591,
		road: 10890.3103703704,
		acceleration: 0.181759259259259
	},
	{
		id: 1191,
		time: 1190,
		velocity: 9.88166666666667,
		power: 2908.807205803,
		road: 10900.0741203704,
		acceleration: 0.131574074074075
	},
	{
		id: 1192,
		time: 1191,
		velocity: 9.71916666666667,
		power: 1203.80176929576,
		road: 10909.8773148148,
		acceleration: -0.0526851851851848
	},
	{
		id: 1193,
		time: 1192,
		velocity: 9.72972222222222,
		power: 997.978994285649,
		road: 10919.6175,
		acceleration: -0.0733333333333341
	},
	{
		id: 1194,
		time: 1193,
		velocity: 9.66166666666667,
		power: 1070.99571491174,
		road: 10929.2890740741,
		acceleration: -0.0638888888888882
	},
	{
		id: 1195,
		time: 1194,
		velocity: 9.5275,
		power: 1018.09887215883,
		road: 10938.8946296296,
		acceleration: -0.0681481481481487
	},
	{
		id: 1196,
		time: 1195,
		velocity: 9.52527777777778,
		power: 1236.66872420364,
		road: 10948.4446759259,
		acceleration: -0.0428703703703697
	},
	{
		id: 1197,
		time: 1196,
		velocity: 9.53305555555556,
		power: 2502.86121382592,
		road: 10958.0209259259,
		acceleration: 0.0952777777777776
	},
	{
		id: 1198,
		time: 1197,
		velocity: 9.81333333333333,
		power: 3215.91705906422,
		road: 10967.7284259259,
		acceleration: 0.167222222222223
	},
	{
		id: 1199,
		time: 1198,
		velocity: 10.0269444444444,
		power: 4567.85454146268,
		road: 10977.6691203704,
		acceleration: 0.299166666666666
	},
	{
		id: 1200,
		time: 1199,
		velocity: 10.4305555555556,
		power: 3579.25338644736,
		road: 10987.8509259259,
		acceleration: 0.183055555555555
	},
	{
		id: 1201,
		time: 1200,
		velocity: 10.3625,
		power: 2977.97036736346,
		road: 10998.181712963,
		acceleration: 0.114907407407408
	},
	{
		id: 1202,
		time: 1201,
		velocity: 10.3716666666667,
		power: 1623.90441311407,
		road: 11008.5580092593,
		acceleration: -0.0238888888888908
	},
	{
		id: 1203,
		time: 1202,
		velocity: 10.3588888888889,
		power: 1977.33897438167,
		road: 11018.9283796296,
		acceleration: 0.0120370370370377
	},
	{
		id: 1204,
		time: 1203,
		velocity: 10.3986111111111,
		power: 2440.74764045507,
		road: 11029.3336111111,
		acceleration: 0.0576851851851874
	},
	{
		id: 1205,
		time: 1204,
		velocity: 10.5447222222222,
		power: 2195.5259408832,
		road: 11039.7834259259,
		acceleration: 0.0314814814814799
	},
	{
		id: 1206,
		time: 1205,
		velocity: 10.4533333333333,
		power: 2796.10038237651,
		road: 11050.2937037037,
		acceleration: 0.0894444444444442
	},
	{
		id: 1207,
		time: 1206,
		velocity: 10.6669444444444,
		power: 2960.56073006342,
		road: 11060.8997222222,
		acceleration: 0.102037037037038
	},
	{
		id: 1208,
		time: 1207,
		velocity: 10.8508333333333,
		power: 4183.85311750104,
		road: 11071.6642592593,
		acceleration: 0.215
	},
	{
		id: 1209,
		time: 1208,
		velocity: 11.0983333333333,
		power: 3849.34042683612,
		road: 11082.622962963,
		acceleration: 0.173333333333334
	},
	{
		id: 1210,
		time: 1209,
		velocity: 11.1869444444444,
		power: 4047.21118565999,
		road: 11093.7602314815,
		acceleration: 0.183796296296295
	},
	{
		id: 1211,
		time: 1210,
		velocity: 11.4022222222222,
		power: 2746.9084884655,
		road: 11105.0179166667,
		acceleration: 0.0570370370370377
	},
	{
		id: 1212,
		time: 1211,
		velocity: 11.2694444444444,
		power: 2091.423201514,
		road: 11116.301712963,
		acceleration: -0.00481481481481438
	},
	{
		id: 1213,
		time: 1212,
		velocity: 11.1725,
		power: -402.062489663016,
		road: 11127.4653703704,
		acceleration: -0.235462962962963
	},
	{
		id: 1214,
		time: 1213,
		velocity: 10.6958333333333,
		power: -668.926588414179,
		road: 11138.3818518518,
		acceleration: -0.258888888888889
	},
	{
		id: 1215,
		time: 1214,
		velocity: 10.4927777777778,
		power: -405.443640147836,
		road: 11149.0531481481,
		acceleration: -0.231481481481483
	},
	{
		id: 1216,
		time: 1215,
		velocity: 10.4780555555556,
		power: 742.106647005839,
		road: 11159.5510185185,
		acceleration: -0.115370370370371
	},
	{
		id: 1217,
		time: 1216,
		velocity: 10.3497222222222,
		power: 3384.02597462167,
		road: 11170.0651851852,
		acceleration: 0.147962962962964
	},
	{
		id: 1218,
		time: 1217,
		velocity: 10.9366666666667,
		power: 3273.08782863493,
		road: 11180.7187962963,
		acceleration: 0.130925925925924
	},
	{
		id: 1219,
		time: 1218,
		velocity: 10.8708333333333,
		power: 3485.48279781283,
		road: 11191.5107407407,
		acceleration: 0.145740740740742
	},
	{
		id: 1220,
		time: 1219,
		velocity: 10.7869444444444,
		power: 1666.76119392203,
		road: 11202.3592592593,
		acceleration: -0.032592592592593
	},
	{
		id: 1221,
		time: 1220,
		velocity: 10.8388888888889,
		power: 1612.14577476704,
		road: 11213.1730092593,
		acceleration: -0.0369444444444422
	},
	{
		id: 1222,
		time: 1221,
		velocity: 10.76,
		power: 2087.85961464633,
		road: 11223.9731018518,
		acceleration: 0.00962962962962877
	},
	{
		id: 1223,
		time: 1222,
		velocity: 10.8158333333333,
		power: 1724.31667927208,
		road: 11234.7652777778,
		acceleration: -0.0254629629629637
	},
	{
		id: 1224,
		time: 1223,
		velocity: 10.7625,
		power: 1431.25948036673,
		road: 11245.5182407407,
		acceleration: -0.0529629629629635
	},
	{
		id: 1225,
		time: 1224,
		velocity: 10.6011111111111,
		power: 317.805242457954,
		road: 11256.1647222222,
		acceleration: -0.16
	},
	{
		id: 1226,
		time: 1225,
		velocity: 10.3358333333333,
		power: -720.853408868783,
		road: 11266.6005555556,
		acceleration: -0.261296296296294
	},
	{
		id: 1227,
		time: 1226,
		velocity: 9.97861111111111,
		power: -1896.20766356288,
		road: 11276.7148148148,
		acceleration: -0.381851851851852
	},
	{
		id: 1228,
		time: 1227,
		velocity: 9.45555555555556,
		power: -1373.93439385391,
		road: 11286.4737037037,
		acceleration: -0.328888888888891
	},
	{
		id: 1229,
		time: 1228,
		velocity: 9.34916666666667,
		power: 345.426766207097,
		road: 11295.9979166667,
		acceleration: -0.140462962962962
	},
	{
		id: 1230,
		time: 1229,
		velocity: 9.55722222222222,
		power: 2082.85829687147,
		road: 11305.4781944444,
		acceleration: 0.0525925925925907
	},
	{
		id: 1231,
		time: 1230,
		velocity: 9.61333333333333,
		power: 4371.45277229976,
		road: 11315.1324537037,
		acceleration: 0.295370370370373
	},
	{
		id: 1232,
		time: 1231,
		velocity: 10.2352777777778,
		power: 4856.56834079631,
		road: 11325.0984259259,
		acceleration: 0.328055555555553
	},
	{
		id: 1233,
		time: 1232,
		velocity: 10.5413888888889,
		power: 5807.01329272838,
		road: 11335.429537037,
		acceleration: 0.402222222222225
	},
	{
		id: 1234,
		time: 1233,
		velocity: 10.82,
		power: 3758.35685070235,
		road: 11346.0518518518,
		acceleration: 0.180185185185184
	},
	{
		id: 1235,
		time: 1234,
		velocity: 10.7758333333333,
		power: 1500.95382060305,
		road: 11356.7419907407,
		acceleration: -0.0445370370370348
	},
	{
		id: 1236,
		time: 1235,
		velocity: 10.4077777777778,
		power: 204.523337649644,
		road: 11367.3247222222,
		acceleration: -0.17027777777778
	},
	{
		id: 1237,
		time: 1236,
		velocity: 10.3091666666667,
		power: 24.0602434195622,
		road: 11377.7293055556,
		acceleration: -0.186018518518516
	},
	{
		id: 1238,
		time: 1237,
		velocity: 10.2177777777778,
		power: 1432.74848406869,
		road: 11388.0203703704,
		acceleration: -0.0410185185185199
	},
	{
		id: 1239,
		time: 1238,
		velocity: 10.2847222222222,
		power: 1783.94432375849,
		road: 11398.2886574074,
		acceleration: -0.00453703703703567
	},
	{
		id: 1240,
		time: 1239,
		velocity: 10.2955555555556,
		power: 3810.40707142839,
		road: 11408.6535648148,
		acceleration: 0.197777777777777
	},
	{
		id: 1241,
		time: 1240,
		velocity: 10.8111111111111,
		power: 3838.33513395036,
		road: 11419.2129166667,
		acceleration: 0.191111111111113
	},
	{
		id: 1242,
		time: 1241,
		velocity: 10.8580555555556,
		power: 4486.99323235004,
		road: 11429.9897685185,
		acceleration: 0.243888888888888
	},
	{
		id: 1243,
		time: 1242,
		velocity: 11.0272222222222,
		power: 3272.43187749057,
		road: 11440.9476388889,
		acceleration: 0.118148148148146
	},
	{
		id: 1244,
		time: 1243,
		velocity: 11.1655555555556,
		power: 3906.62657581952,
		road: 11452.0506481481,
		acceleration: 0.17212962962963
	},
	{
		id: 1245,
		time: 1244,
		velocity: 11.3744444444444,
		power: 3280.17261074723,
		road: 11463.2933796296,
		acceleration: 0.107314814814817
	},
	{
		id: 1246,
		time: 1245,
		velocity: 11.3491666666667,
		power: 2401.77162325243,
		road: 11474.6013888889,
		acceleration: 0.0232407407407393
	},
	{
		id: 1247,
		time: 1246,
		velocity: 11.2352777777778,
		power: 1633.57199339019,
		road: 11485.8971759259,
		acceleration: -0.0476851851851841
	},
	{
		id: 1248,
		time: 1247,
		velocity: 11.2313888888889,
		power: 1826.5379420401,
		road: 11497.1547685185,
		acceleration: -0.0287037037037052
	},
	{
		id: 1249,
		time: 1248,
		velocity: 11.2630555555556,
		power: 1569.93454117137,
		road: 11508.3722222222,
		acceleration: -0.0515740740740753
	},
	{
		id: 1250,
		time: 1249,
		velocity: 11.0805555555556,
		power: 2686.24309206576,
		road: 11519.5902777778,
		acceleration: 0.0527777777777807
	},
	{
		id: 1251,
		time: 1250,
		velocity: 11.3897222222222,
		power: 3193.16161036956,
		road: 11530.8833333333,
		acceleration: 0.0972222222222214
	},
	{
		id: 1252,
		time: 1251,
		velocity: 11.5547222222222,
		power: 3841.80873881435,
		road: 11542.3009722222,
		acceleration: 0.151944444444446
	},
	{
		id: 1253,
		time: 1252,
		velocity: 11.5363888888889,
		power: 3927.54708296593,
		road: 11553.8711111111,
		acceleration: 0.153055555555554
	},
	{
		id: 1254,
		time: 1253,
		velocity: 11.8488888888889,
		power: 5832.40341858573,
		road: 11565.6738425926,
		acceleration: 0.312129629629631
	},
	{
		id: 1255,
		time: 1254,
		velocity: 12.4911111111111,
		power: 6866.48581852543,
		road: 11577.8235185185,
		acceleration: 0.381759259259258
	},
	{
		id: 1256,
		time: 1255,
		velocity: 12.6816666666667,
		power: 6436.54830120078,
		road: 11590.3260648148,
		acceleration: 0.32398148148148
	},
	{
		id: 1257,
		time: 1256,
		velocity: 12.8208333333333,
		power: 2158.42087031517,
		road: 11602.97125,
		acceleration: -0.0387037037037032
	},
	{
		id: 1258,
		time: 1257,
		velocity: 12.375,
		power: -62.970386281574,
		road: 11615.4865740741,
		acceleration: -0.22101851851852
	},
	{
		id: 1259,
		time: 1258,
		velocity: 12.0186111111111,
		power: -1437.45514198337,
		road: 11627.7238888889,
		acceleration: -0.334999999999999
	},
	{
		id: 1260,
		time: 1259,
		velocity: 11.8158333333333,
		power: -142.411384034173,
		road: 11639.6834259259,
		acceleration: -0.220555555555556
	},
	{
		id: 1261,
		time: 1260,
		velocity: 11.7133333333333,
		power: 860.499109507979,
		road: 11651.4681018518,
		acceleration: -0.129166666666666
	},
	{
		id: 1262,
		time: 1261,
		velocity: 11.6311111111111,
		power: 1565.74891992888,
		road: 11663.1562037037,
		acceleration: -0.0639814814814823
	},
	{
		id: 1263,
		time: 1262,
		velocity: 11.6238888888889,
		power: 2483.46161747902,
		road: 11674.8218518518,
		acceleration: 0.0190740740740765
	},
	{
		id: 1264,
		time: 1263,
		velocity: 11.7705555555556,
		power: 2932.54092950633,
		road: 11686.5260648148,
		acceleration: 0.0580555555555549
	},
	{
		id: 1265,
		time: 1264,
		velocity: 11.8052777777778,
		power: 4034.38582113913,
		road: 11698.3353703704,
		acceleration: 0.152129629629629
	},
	{
		id: 1266,
		time: 1265,
		velocity: 12.0802777777778,
		power: 3634.68391869257,
		road: 11710.2763888889,
		acceleration: 0.111296296296295
	},
	{
		id: 1267,
		time: 1266,
		velocity: 12.1044444444444,
		power: 3967.68089767139,
		road: 11722.3406944444,
		acceleration: 0.135277777777778
	},
	{
		id: 1268,
		time: 1267,
		velocity: 12.2111111111111,
		power: 2438.38628210397,
		road: 11734.4727777778,
		acceleration: 0.000277777777780486
	},
	{
		id: 1269,
		time: 1268,
		velocity: 12.0811111111111,
		power: 1968.01562268523,
		road: 11746.5850925926,
		acceleration: -0.0398148148148163
	},
	{
		id: 1270,
		time: 1269,
		velocity: 11.985,
		power: 1639.91575223686,
		road: 11758.6440740741,
		acceleration: -0.0668518518518511
	},
	{
		id: 1271,
		time: 1270,
		velocity: 12.0105555555556,
		power: 2752.3127511279,
		road: 11770.6848611111,
		acceleration: 0.0304629629629627
	},
	{
		id: 1272,
		time: 1271,
		velocity: 12.1725,
		power: 4013.10668898343,
		road: 11782.809212963,
		acceleration: 0.136666666666665
	},
	{
		id: 1273,
		time: 1272,
		velocity: 12.395,
		power: 4024.24784528924,
		road: 11795.0678703704,
		acceleration: 0.131944444444445
	},
	{
		id: 1274,
		time: 1273,
		velocity: 12.4063888888889,
		power: 4371.47973486211,
		road: 11807.4701388889,
		acceleration: 0.155277777777776
	},
	{
		id: 1275,
		time: 1274,
		velocity: 12.6383333333333,
		power: 3757.02449831428,
		road: 11819.999212963,
		acceleration: 0.0983333333333345
	},
	{
		id: 1276,
		time: 1275,
		velocity: 12.69,
		power: 2619.19671847999,
		road: 11832.5782407407,
		acceleration: 0.00157407407407639
	},
	{
		id: 1277,
		time: 1276,
		velocity: 12.4111111111111,
		power: 1310.04682196681,
		road: 11845.1049074074,
		acceleration: -0.106296296296298
	},
	{
		id: 1278,
		time: 1277,
		velocity: 12.3194444444444,
		power: -183.024338718134,
		road: 11857.4638888889,
		acceleration: -0.229074074074072
	},
	{
		id: 1279,
		time: 1278,
		velocity: 12.0027777777778,
		power: 1499.38382544183,
		road: 11869.667037037,
		acceleration: -0.0825925925925937
	},
	{
		id: 1280,
		time: 1279,
		velocity: 12.1633333333333,
		power: 367.644410473652,
		road: 11881.7400462963,
		acceleration: -0.177685185185185
	},
	{
		id: 1281,
		time: 1280,
		velocity: 11.7863888888889,
		power: 699.681197969032,
		road: 11893.6512962963,
		acceleration: -0.145833333333334
	},
	{
		id: 1282,
		time: 1281,
		velocity: 11.5652777777778,
		power: 60.3724547969481,
		road: 11905.3897685185,
		acceleration: -0.199722222222222
	},
	{
		id: 1283,
		time: 1282,
		velocity: 11.5641666666667,
		power: 275.821232576436,
		road: 11916.9395833333,
		acceleration: -0.177592592592591
	},
	{
		id: 1284,
		time: 1283,
		velocity: 11.2536111111111,
		power: 1238.82421021425,
		road: 11928.357037037,
		acceleration: -0.0871296296296293
	},
	{
		id: 1285,
		time: 1284,
		velocity: 11.3038888888889,
		power: 773.884727588357,
		road: 11939.667037037,
		acceleration: -0.127777777777778
	},
	{
		id: 1286,
		time: 1285,
		velocity: 11.1808333333333,
		power: 1511.54466800601,
		road: 11950.8846296296,
		acceleration: -0.0570370370370394
	},
	{
		id: 1287,
		time: 1286,
		velocity: 11.0825,
		power: 828.568987481603,
		road: 11962.0141203704,
		acceleration: -0.119166666666667
	},
	{
		id: 1288,
		time: 1287,
		velocity: 10.9463888888889,
		power: 776.538687159494,
		road: 11973.0231481481,
		acceleration: -0.121759259259257
	},
	{
		id: 1289,
		time: 1288,
		velocity: 10.8155555555556,
		power: 703.787112758124,
		road: 11983.9081018518,
		acceleration: -0.12638888888889
	},
	{
		id: 1290,
		time: 1289,
		velocity: 10.7033333333333,
		power: 466.254166506334,
		road: 11994.6563425926,
		acceleration: -0.147037037037038
	},
	{
		id: 1291,
		time: 1290,
		velocity: 10.5052777777778,
		power: 328.824360174997,
		road: 12005.2519907407,
		acceleration: -0.158148148148149
	},
	{
		id: 1292,
		time: 1291,
		velocity: 10.3411111111111,
		power: 462.126078357427,
		road: 12015.6973148148,
		acceleration: -0.1425
	},
	{
		id: 1293,
		time: 1292,
		velocity: 10.2758333333333,
		power: 1205.58443305146,
		road: 12026.0387037037,
		acceleration: -0.0653703703703687
	},
	{
		id: 1294,
		time: 1293,
		velocity: 10.3091666666667,
		power: 2574.917937051,
		road: 12036.3841203704,
		acceleration: 0.0734259259259247
	},
	{
		id: 1295,
		time: 1294,
		velocity: 10.5613888888889,
		power: 3688.04414958034,
		road: 12046.8563888889,
		acceleration: 0.180277777777778
	},
	{
		id: 1296,
		time: 1295,
		velocity: 10.8166666666667,
		power: 4423.28322439815,
		road: 12057.5400925926,
		acceleration: 0.242592592592592
	},
	{
		id: 1297,
		time: 1296,
		velocity: 11.0369444444444,
		power: 4086.89746607397,
		road: 12068.4444444444,
		acceleration: 0.198703703703702
	},
	{
		id: 1298,
		time: 1297,
		velocity: 11.1575,
		power: 3362.56814775928,
		road: 12079.5093055556,
		acceleration: 0.122314814814818
	},
	{
		id: 1299,
		time: 1298,
		velocity: 11.1836111111111,
		power: 2171.6977640744,
		road: 12090.6390277778,
		acceleration: 0.00740740740740797
	},
	{
		id: 1300,
		time: 1299,
		velocity: 11.0591666666667,
		power: 418.942493614072,
		road: 12101.6941666667,
		acceleration: -0.156574074074074
	},
	{
		id: 1301,
		time: 1300,
		velocity: 10.6877777777778,
		power: -941.052452810889,
		road: 12112.5286574074,
		acceleration: -0.284722222222221
	},
	{
		id: 1302,
		time: 1301,
		velocity: 10.3294444444444,
		power: -897.574546557497,
		road: 12123.0810648148,
		acceleration: -0.279444444444447
	},
	{
		id: 1303,
		time: 1302,
		velocity: 10.2208333333333,
		power: -922.158766764708,
		road: 12133.3531944444,
		acceleration: -0.281111111111111
	},
	{
		id: 1304,
		time: 1303,
		velocity: 9.84444444444444,
		power: -2380.06788541648,
		road: 12143.2673611111,
		acceleration: -0.434814814814814
	},
	{
		id: 1305,
		time: 1304,
		velocity: 9.025,
		power: -3238.13270060647,
		road: 12152.6950462963,
		acceleration: -0.538148148148148
	},
	{
		id: 1306,
		time: 1305,
		velocity: 8.60638888888889,
		power: -3600.62407659811,
		road: 12161.5543981481,
		acceleration: -0.598518518518521
	},
	{
		id: 1307,
		time: 1306,
		velocity: 8.04888888888889,
		power: -2485.51684301377,
		road: 12169.8743518518,
		acceleration: -0.480277777777776
	},
	{
		id: 1308,
		time: 1307,
		velocity: 7.58416666666667,
		power: -2687.88372527235,
		road: 12177.6925925926,
		acceleration: -0.52314814814815
	},
	{
		id: 1309,
		time: 1308,
		velocity: 7.03694444444444,
		power: -3810.91163622733,
		road: 12184.8927314815,
		acceleration: -0.713055555555554
	},
	{
		id: 1310,
		time: 1309,
		velocity: 5.90972222222222,
		power: -3700.54748598402,
		road: 12191.3599537037,
		acceleration: -0.752777777777779
	},
	{
		id: 1311,
		time: 1310,
		velocity: 5.32583333333333,
		power: -3156.19956043065,
		road: 12197.087962963,
		acceleration: -0.725648148148148
	},
	{
		id: 1312,
		time: 1311,
		velocity: 4.86,
		power: -1496.30164148061,
		road: 12202.2288425926,
		acceleration: -0.44861111111111
	},
	{
		id: 1313,
		time: 1312,
		velocity: 4.56388888888889,
		power: -498.039148297275,
		road: 12207.0205555555,
		acceleration: -0.249722222222222
	},
	{
		id: 1314,
		time: 1313,
		velocity: 4.57666666666667,
		power: -4.38293845092806,
		road: 12211.6172685185,
		acceleration: -0.140277777777778
	},
	{
		id: 1315,
		time: 1314,
		velocity: 4.43916666666667,
		power: 374.00027542609,
		road: 12216.1181944444,
		acceleration: -0.0512962962962966
	},
	{
		id: 1316,
		time: 1315,
		velocity: 4.41,
		power: 263.160133799856,
		road: 12220.555462963,
		acceleration: -0.0760185185185183
	},
	{
		id: 1317,
		time: 1316,
		velocity: 4.34861111111111,
		power: 372.439150943867,
		road: 12224.930462963,
		acceleration: -0.0485185185185184
	},
	{
		id: 1318,
		time: 1317,
		velocity: 4.29361111111111,
		power: 197.050154972483,
		road: 12229.2363888889,
		acceleration: -0.0896296296296306
	},
	{
		id: 1319,
		time: 1318,
		velocity: 4.14111111111111,
		power: -129.346836283854,
		road: 12233.4125925926,
		acceleration: -0.169814814814814
	},
	{
		id: 1320,
		time: 1319,
		velocity: 3.83916666666667,
		power: -805.372904086231,
		road: 12237.3275462963,
		acceleration: -0.352685185185185
	},
	{
		id: 1321,
		time: 1320,
		velocity: 3.23555555555556,
		power: -859.568345542519,
		road: 12240.8711574074,
		acceleration: -0.389999999999999
	},
	{
		id: 1322,
		time: 1321,
		velocity: 2.97111111111111,
		power: -1012.24542891321,
		road: 12243.9818981481,
		acceleration: -0.475740740740741
	},
	{
		id: 1323,
		time: 1322,
		velocity: 2.41194444444444,
		power: -364.159447521002,
		road: 12246.7188425926,
		acceleration: -0.271851851851852
	},
	{
		id: 1324,
		time: 1323,
		velocity: 2.42,
		power: 16.5455175945874,
		road: 12249.2577777778,
		acceleration: -0.124166666666667
	},
	{
		id: 1325,
		time: 1324,
		velocity: 2.59861111111111,
		power: 1486.6265894555,
		road: 12251.9588888889,
		acceleration: 0.448518518518518
	},
	{
		id: 1326,
		time: 1325,
		velocity: 3.7575,
		power: 3088.09031352499,
		road: 12255.3037037037,
		acceleration: 0.838888888888889
	},
	{
		id: 1327,
		time: 1326,
		velocity: 4.93666666666667,
		power: 5350.43979379034,
		road: 12259.6473611111,
		acceleration: 1.1587962962963
	},
	{
		id: 1328,
		time: 1327,
		velocity: 6.075,
		power: 6282.53634070825,
		road: 12265.1040277778,
		acceleration: 1.06722222222222
	},
	{
		id: 1329,
		time: 1328,
		velocity: 6.95916666666667,
		power: 9233.78989716228,
		road: 12271.7487037037,
		acceleration: 1.3087962962963
	},
	{
		id: 1330,
		time: 1329,
		velocity: 8.86305555555555,
		power: 9676.66166385982,
		road: 12279.6130555555,
		acceleration: 1.13055555555556
	},
	{
		id: 1331,
		time: 1330,
		velocity: 9.46666666666667,
		power: 10212.8081855603,
		road: 12288.5560185185,
		acceleration: 1.02666666666667
	},
	{
		id: 1332,
		time: 1331,
		velocity: 10.0391666666667,
		power: 10806.6903168594,
		road: 12298.4915277778,
		acceleration: 0.958425925925926
	},
	{
		id: 1333,
		time: 1332,
		velocity: 11.7383333333333,
		power: 12802.779992771,
		road: 12309.4230555555,
		acceleration: 1.03361111111111
	},
	{
		id: 1334,
		time: 1333,
		velocity: 12.5675,
		power: 12391.3736317476,
		road: 12321.3140277778,
		acceleration: 0.885277777777779
	},
	{
		id: 1335,
		time: 1334,
		velocity: 12.695,
		power: 6052.87726381494,
		road: 12333.7941666667,
		acceleration: 0.293055555555556
	},
	{
		id: 1336,
		time: 1335,
		velocity: 12.6175,
		power: 2129.60606062402,
		road: 12346.4008333333,
		acceleration: -0.0400000000000009
	},
	{
		id: 1337,
		time: 1336,
		velocity: 12.4475,
		power: 1222.35326138744,
		road: 12358.9306481481,
		acceleration: -0.113703703703704
	},
	{
		id: 1338,
		time: 1337,
		velocity: 12.3538888888889,
		power: 940.700437255977,
		road: 12371.33625,
		acceleration: -0.134722222222223
	},
	{
		id: 1339,
		time: 1338,
		velocity: 12.2133333333333,
		power: 773.853137945758,
		road: 12383.6014351852,
		acceleration: -0.146111111111111
	},
	{
		id: 1340,
		time: 1339,
		velocity: 12.0091666666667,
		power: 390.58560870833,
		road: 12395.705462963,
		acceleration: -0.176203703703701
	},
	{
		id: 1341,
		time: 1340,
		velocity: 11.8252777777778,
		power: -1367.35134576679,
		road: 12407.5575925926,
		acceleration: -0.327592592592595
	},
	{
		id: 1342,
		time: 1341,
		velocity: 11.2305555555556,
		power: -1957.03408121889,
		road: 12419.0556944444,
		acceleration: -0.380462962962961
	},
	{
		id: 1343,
		time: 1342,
		velocity: 10.8677777777778,
		power: -3183.30392350071,
		road: 12430.1143981481,
		acceleration: -0.498333333333335
	},
	{
		id: 1344,
		time: 1343,
		velocity: 10.3302777777778,
		power: -2549.11998175819,
		road: 12440.7023148148,
		acceleration: -0.443240740740741
	},
	{
		id: 1345,
		time: 1344,
		velocity: 9.90083333333333,
		power: -3370.2485462567,
		road: 12450.8010185185,
		acceleration: -0.535185185185185
	},
	{
		id: 1346,
		time: 1345,
		velocity: 9.26222222222222,
		power: -2513.49161408925,
		road: 12460.4050462963,
		acceleration: -0.454166666666666
	},
	{
		id: 1347,
		time: 1346,
		velocity: 8.96777777777778,
		power: -1103.85050870605,
		road: 12469.6314814815,
		acceleration: -0.301018518518518
	},
	{
		id: 1348,
		time: 1347,
		velocity: 8.99777777777778,
		power: 1638.12030895743,
		road: 12478.7151388889,
		acceleration: 0.0154629629629603
	},
	{
		id: 1349,
		time: 1348,
		velocity: 9.30861111111111,
		power: 2856.53875224407,
		road: 12487.8827314815,
		acceleration: 0.152407407407408
	},
	{
		id: 1350,
		time: 1349,
		velocity: 9.425,
		power: 3426.93871964981,
		road: 12497.2306481481,
		acceleration: 0.208240740740742
	},
	{
		id: 1351,
		time: 1350,
		velocity: 9.6225,
		power: 2691.72757297879,
		road: 12506.742037037,
		acceleration: 0.118703703703702
	},
	{
		id: 1352,
		time: 1351,
		velocity: 9.66472222222222,
		power: 2135.65350541387,
		road: 12516.3399074074,
		acceleration: 0.0542592592592595
	},
	{
		id: 1353,
		time: 1352,
		velocity: 9.58777777777778,
		power: 2016.42804304797,
		road: 12525.9847222222,
		acceleration: 0.0396296296296317
	},
	{
		id: 1354,
		time: 1353,
		velocity: 9.74138888888889,
		power: 3126.33389774472,
		road: 12535.7273148148,
		acceleration: 0.155925925925926
	},
	{
		id: 1355,
		time: 1354,
		velocity: 10.1325,
		power: 4554.71174996907,
		road: 12545.6959259259,
		acceleration: 0.296111111111111
	},
	{
		id: 1356,
		time: 1355,
		velocity: 10.4761111111111,
		power: 5720.73099573923,
		road: 12556.0099074074,
		acceleration: 0.394629629629629
	},
	{
		id: 1357,
		time: 1356,
		velocity: 10.9252777777778,
		power: 6487.11237775013,
		road: 12566.7420833333,
		acceleration: 0.441759259259261
	},
	{
		id: 1358,
		time: 1357,
		velocity: 11.4577777777778,
		power: 6092.08350808166,
		road: 12577.8831944444,
		acceleration: 0.37611111111111
	},
	{
		id: 1359,
		time: 1358,
		velocity: 11.6044444444444,
		power: 5401.48130782685,
		road: 12589.3583333333,
		acceleration: 0.291944444444447
	},
	{
		id: 1360,
		time: 1359,
		velocity: 11.8011111111111,
		power: 3180.38845950004,
		road: 12601.0203703704,
		acceleration: 0.0818518518518516
	},
	{
		id: 1361,
		time: 1360,
		velocity: 11.7033333333333,
		power: 2673.95054390992,
		road: 12612.7405092593,
		acceleration: 0.0343518518518504
	},
	{
		id: 1362,
		time: 1361,
		velocity: 11.7075,
		power: 1848.28582716324,
		road: 12624.4581018518,
		acceleration: -0.0394444444444453
	},
	{
		id: 1363,
		time: 1362,
		velocity: 11.6827777777778,
		power: 3029.14005452646,
		road: 12636.1888425926,
		acceleration: 0.0657407407407415
	},
	{
		id: 1364,
		time: 1363,
		velocity: 11.9005555555556,
		power: 2882.18919776475,
		road: 12647.9777314815,
		acceleration: 0.0505555555555546
	},
	{
		id: 1365,
		time: 1364,
		velocity: 11.8591666666667,
		power: 1893.0781828732,
		road: 12659.7731018518,
		acceleration: -0.0375925925925902
	},
	{
		id: 1366,
		time: 1365,
		velocity: 11.57,
		power: 458.528381670703,
		road: 12671.467962963,
		acceleration: -0.163425925925926
	},
	{
		id: 1367,
		time: 1366,
		velocity: 11.4102777777778,
		power: 428.52615847953,
		road: 12682.9993981481,
		acceleration: -0.163425925925928
	},
	{
		id: 1368,
		time: 1367,
		velocity: 11.3688888888889,
		power: 1115.87302531974,
		road: 12694.4000925926,
		acceleration: -0.098055555555554
	},
	{
		id: 1369,
		time: 1368,
		velocity: 11.2758333333333,
		power: 1735.90761990416,
		road: 12705.7321759259,
		acceleration: -0.0391666666666666
	},
	{
		id: 1370,
		time: 1369,
		velocity: 11.2927777777778,
		power: 1336.06932266982,
		road: 12717.0072685185,
		acceleration: -0.0748148148148147
	},
	{
		id: 1371,
		time: 1370,
		velocity: 11.1444444444444,
		power: 1463.17487038483,
		road: 12728.2143055555,
		acceleration: -0.0612962962962964
	},
	{
		id: 1372,
		time: 1371,
		velocity: 11.0919444444444,
		power: 1731.1487124272,
		road: 12739.3732407407,
		acceleration: -0.0349074074074061
	},
	{
		id: 1373,
		time: 1372,
		velocity: 11.1880555555556,
		power: 1838.55564935558,
		road: 12750.5027314815,
		acceleration: -0.0239814814814832
	},
	{
		id: 1374,
		time: 1373,
		velocity: 11.0725,
		power: 1894.03303973236,
		road: 12761.6111574074,
		acceleration: -0.018148148148148
	},
	{
		id: 1375,
		time: 1374,
		velocity: 11.0375,
		power: 1796.19573974592,
		road: 12772.6971296296,
		acceleration: -0.0267592592592596
	},
	{
		id: 1376,
		time: 1375,
		velocity: 11.1077777777778,
		power: 2064.80546517157,
		road: 12783.7692592592,
		acceleration: -0.000925925925926663
	},
	{
		id: 1377,
		time: 1376,
		velocity: 11.0697222222222,
		power: 2464.01200849599,
		road: 12794.8590740741,
		acceleration: 0.0362962962962978
	},
	{
		id: 1378,
		time: 1377,
		velocity: 11.1463888888889,
		power: 2296.31826617713,
		road: 12805.9768055555,
		acceleration: 0.0195370370370345
	},
	{
		id: 1379,
		time: 1378,
		velocity: 11.1663888888889,
		power: 2387.93040135371,
		road: 12817.1180092592,
		acceleration: 0.0274074074074093
	},
	{
		id: 1380,
		time: 1379,
		velocity: 11.1519444444444,
		power: 1978.32749701475,
		road: 12828.2672222222,
		acceleration: -0.011388888888888
	},
	{
		id: 1381,
		time: 1380,
		velocity: 11.1122222222222,
		power: 1628.08606142779,
		road: 12839.3889351852,
		acceleration: -0.0436111111111117
	},
	{
		id: 1382,
		time: 1381,
		velocity: 11.0355555555556,
		power: 972.625460307491,
		road: 12850.4368981481,
		acceleration: -0.103888888888889
	},
	{
		id: 1383,
		time: 1382,
		velocity: 10.8402777777778,
		power: 670.197808365371,
		road: 12861.3676851852,
		acceleration: -0.130462962962964
	},
	{
		id: 1384,
		time: 1383,
		velocity: 10.7208333333333,
		power: 698.58853410012,
		road: 12872.1705555555,
		acceleration: -0.125370370370369
	},
	{
		id: 1385,
		time: 1384,
		velocity: 10.6594444444444,
		power: 1252.76196033818,
		road: 12882.8761111111,
		acceleration: -0.0692592592592582
	},
	{
		id: 1386,
		time: 1385,
		velocity: 10.6325,
		power: 1267.49555023763,
		road: 12893.5139351852,
		acceleration: -0.0662037037037049
	},
	{
		id: 1387,
		time: 1386,
		velocity: 10.5222222222222,
		power: 1498.24572077225,
		road: 12904.0976388889,
		acceleration: -0.0420370370370353
	},
	{
		id: 1388,
		time: 1387,
		velocity: 10.5333333333333,
		power: 1563.58368161158,
		road: 12914.6430555555,
		acceleration: -0.0345370370370368
	},
	{
		id: 1389,
		time: 1388,
		velocity: 10.5288888888889,
		power: 2087.65930358712,
		road: 12925.1801388889,
		acceleration: 0.0178703703703675
	},
	{
		id: 1390,
		time: 1389,
		velocity: 10.5758333333333,
		power: 1707.03505548809,
		road: 12935.7161574074,
		acceleration: -0.0199999999999996
	},
	{
		id: 1391,
		time: 1390,
		velocity: 10.4733333333333,
		power: 1884.11449352705,
		road: 12946.2411574074,
		acceleration: -0.00203703703703439
	},
	{
		id: 1392,
		time: 1391,
		velocity: 10.5227777777778,
		power: 2524.4213669762,
		road: 12956.7955092592,
		acceleration: 0.060740740740739
	},
	{
		id: 1393,
		time: 1392,
		velocity: 10.7580555555556,
		power: 4006.45302445902,
		road: 12967.4810185185,
		acceleration: 0.201574074074076
	},
	{
		id: 1394,
		time: 1393,
		velocity: 11.0780555555556,
		power: 4347.45244401289,
		road: 12978.3793518518,
		acceleration: 0.224074074074075
	},
	{
		id: 1395,
		time: 1394,
		velocity: 11.195,
		power: 2671.15464463418,
		road: 12989.4185648148,
		acceleration: 0.0576851851851821
	},
	{
		id: 1396,
		time: 1395,
		velocity: 10.9311111111111,
		power: 163.741832874324,
		road: 13000.3967592592,
		acceleration: -0.179722222222221
	},
	{
		id: 1397,
		time: 1396,
		velocity: 10.5388888888889,
		power: -1140.83585380972,
		road: 13011.1331481481,
		acceleration: -0.30388888888889
	},
	{
		id: 1398,
		time: 1397,
		velocity: 10.2833333333333,
		power: -1661.56755658026,
		road: 13021.5395833333,
		acceleration: -0.356018518518519
	},
	{
		id: 1399,
		time: 1398,
		velocity: 9.86305555555555,
		power: -702.041032020939,
		road: 13031.6390740741,
		acceleration: -0.25787037037037
	},
	{
		id: 1400,
		time: 1399,
		velocity: 9.76527777777778,
		power: 105.438323221733,
		road: 13041.5239814815,
		acceleration: -0.171296296296296
	},
	{
		id: 1401,
		time: 1400,
		velocity: 9.76944444444444,
		power: 1428.45680492516,
		road: 13051.3091666667,
		acceleration: -0.0281481481481478
	},
	{
		id: 1402,
		time: 1401,
		velocity: 9.77861111111111,
		power: 2363.83192802679,
		road: 13061.1159722222,
		acceleration: 0.0713888888888903
	},
	{
		id: 1403,
		time: 1402,
		velocity: 9.97944444444444,
		power: 1628.3829468653,
		road: 13070.9543518518,
		acceleration: -0.00824074074074055
	},
	{
		id: 1404,
		time: 1403,
		velocity: 9.74472222222222,
		power: 1919.54230399169,
		road: 13080.7999074074,
		acceleration: 0.0225925925925914
	},
	{
		id: 1405,
		time: 1404,
		velocity: 9.84638888888889,
		power: 924.317118046442,
		road: 13090.6153240741,
		acceleration: -0.0828703703703706
	},
	{
		id: 1406,
		time: 1405,
		velocity: 9.73083333333333,
		power: 788.349951282537,
		road: 13100.3414814815,
		acceleration: -0.0956481481481486
	},
	{
		id: 1407,
		time: 1406,
		velocity: 9.45777777777778,
		power: -597.590312755293,
		road: 13109.8975462963,
		acceleration: -0.244537037037036
	},
	{
		id: 1408,
		time: 1407,
		velocity: 9.11277777777778,
		power: -1336.40862266151,
		road: 13119.1677314815,
		acceleration: -0.327222222222224
	},
	{
		id: 1409,
		time: 1408,
		velocity: 8.74916666666667,
		power: 250.762429594608,
		road: 13128.2021759259,
		acceleration: -0.144259259259258
	},
	{
		id: 1410,
		time: 1409,
		velocity: 9.025,
		power: 1241.42062716589,
		road: 13137.1510648148,
		acceleration: -0.0268518518518519
	},
	{
		id: 1411,
		time: 1410,
		velocity: 9.03222222222222,
		power: 3012.98951074613,
		road: 13146.1751851852,
		acceleration: 0.177314814814814
	},
	{
		id: 1412,
		time: 1411,
		velocity: 9.28111111111111,
		power: 2796.39598070623,
		road: 13155.3603240741,
		acceleration: 0.144722222222223
	},
	{
		id: 1413,
		time: 1412,
		velocity: 9.45916666666667,
		power: 1896.18024688115,
		road: 13164.6371759259,
		acceleration: 0.0387037037037032
	},
	{
		id: 1414,
		time: 1413,
		velocity: 9.14833333333333,
		power: 158.649572471947,
		road: 13173.8547685185,
		acceleration: -0.157222222222224
	},
	{
		id: 1415,
		time: 1414,
		velocity: 8.80944444444444,
		power: -454.507329720995,
		road: 13182.8806481481,
		acceleration: -0.226203703703703
	},
	{
		id: 1416,
		time: 1415,
		velocity: 8.78055555555556,
		power: 1508.24160818712,
		road: 13191.7961574074,
		acceleration: 0.00546296296296411
	},
	{
		id: 1417,
		time: 1416,
		velocity: 9.16472222222222,
		power: 3049.66136650842,
		road: 13200.8055555555,
		acceleration: 0.182314814814815
	},
	{
		id: 1418,
		time: 1417,
		velocity: 9.35638888888889,
		power: 3240.3083790884,
		road: 13210.0035185185,
		acceleration: 0.194814814814816
	},
	{
		id: 1419,
		time: 1418,
		velocity: 9.365,
		power: 3373.47365642432,
		road: 13219.3987962963,
		acceleration: 0.199814814814815
	},
	{
		id: 1420,
		time: 1419,
		velocity: 9.76416666666667,
		power: 2443.5997052,
		road: 13228.9390740741,
		acceleration: 0.0901851851851845
	},
	{
		id: 1421,
		time: 1420,
		velocity: 9.62694444444444,
		power: 2737.70546410065,
		road: 13238.5835185185,
		acceleration: 0.118148148148149
	},
	{
		id: 1422,
		time: 1421,
		velocity: 9.71944444444445,
		power: 1519.95011588356,
		road: 13248.2791203704,
		acceleration: -0.0158333333333349
	},
	{
		id: 1423,
		time: 1422,
		velocity: 9.71666666666667,
		power: 2080.35801423331,
		road: 13257.9889814815,
		acceleration: 0.044351851851852
	},
	{
		id: 1424,
		time: 1423,
		velocity: 9.76,
		power: 1402.18229424034,
		road: 13267.7064351852,
		acceleration: -0.0291666666666686
	},
	{
		id: 1425,
		time: 1424,
		velocity: 9.63194444444444,
		power: 411.513772532689,
		road: 13277.3418518518,
		acceleration: -0.134907407407407
	},
	{
		id: 1426,
		time: 1425,
		velocity: 9.31194444444444,
		power: 165.714786484889,
		road: 13286.8299074074,
		acceleration: -0.159814814814812
	},
	{
		id: 1427,
		time: 1426,
		velocity: 9.28055555555556,
		power: -656.033803102033,
		road: 13296.1129629629,
		acceleration: -0.250185185185186
	},
	{
		id: 1428,
		time: 1427,
		velocity: 8.88138888888889,
		power: -192.015890223252,
		road: 13305.1729629629,
		acceleration: -0.195925925925925
	},
	{
		id: 1429,
		time: 1428,
		velocity: 8.72416666666667,
		power: -482.479717879255,
		road: 13314.0206018518,
		acceleration: -0.228796296296295
	},
	{
		id: 1430,
		time: 1429,
		velocity: 8.59416666666667,
		power: 420.42879745978,
		road: 13322.6943981481,
		acceleration: -0.11888888888889
	},
	{
		id: 1431,
		time: 1430,
		velocity: 8.52472222222222,
		power: 1777.21149297192,
		road: 13331.3320833333,
		acceleration: 0.0466666666666669
	},
	{
		id: 1432,
		time: 1431,
		velocity: 8.86416666666667,
		power: 223.418360537552,
		road: 13339.9222685185,
		acceleration: -0.141666666666667
	},
	{
		id: 1433,
		time: 1432,
		velocity: 8.16916666666667,
		power: -391.795526354954,
		road: 13348.3335185185,
		acceleration: -0.216203703703703
	},
	{
		id: 1434,
		time: 1433,
		velocity: 7.87611111111111,
		power: -2546.16837472312,
		road: 13356.3886111111,
		acceleration: -0.496111111111111
	},
	{
		id: 1435,
		time: 1434,
		velocity: 7.37583333333333,
		power: -2609.38305437978,
		road: 13363.9341666666,
		acceleration: -0.522962962962963
	},
	{
		id: 1436,
		time: 1435,
		velocity: 6.60027777777778,
		power: -2545.92543439992,
		road: 13370.9499074074,
		acceleration: -0.536666666666666
	},
	{
		id: 1437,
		time: 1436,
		velocity: 6.26611111111111,
		power: -2026.8949858599,
		road: 13377.4579166666,
		acceleration: -0.478796296296297
	},
	{
		id: 1438,
		time: 1437,
		velocity: 5.93944444444444,
		power: -2677.79299166172,
		road: 13383.4164351852,
		acceleration: -0.620185185185184
	},
	{
		id: 1439,
		time: 1438,
		velocity: 4.73972222222222,
		power: -4731.31872652266,
		road: 13388.5044444444,
		acceleration: -1.12083333333333
	},
	{
		id: 1440,
		time: 1439,
		velocity: 2.90361111111111,
		power: -5278.51240779869,
		road: 13392.2150925926,
		acceleration: -1.63388888888889
	},
	{
		id: 1441,
		time: 1440,
		velocity: 1.03777777777778,
		power: -2892.65512184929,
		road: 13394.3188425926,
		acceleration: -1.57990740740741
	},
	{
		id: 1442,
		time: 1441,
		velocity: 0,
		power: -660.60842083066,
		road: 13395.1487037037,
		acceleration: -0.96787037037037
	},
	{
		id: 1443,
		time: 1442,
		velocity: 0,
		power: -35.7862797920728,
		road: 13395.3216666666,
		acceleration: -0.345925925925926
	},
	{
		id: 1444,
		time: 1443,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1445,
		time: 1444,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1446,
		time: 1445,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1447,
		time: 1446,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1448,
		time: 1447,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1449,
		time: 1448,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1450,
		time: 1449,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1451,
		time: 1450,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1452,
		time: 1451,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1453,
		time: 1452,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1454,
		time: 1453,
		velocity: 0,
		power: 0,
		road: 13395.3216666666,
		acceleration: 0
	},
	{
		id: 1455,
		time: 1454,
		velocity: 0,
		power: 4.72421392369325,
		road: 13395.3490277778,
		acceleration: 0.0547222222222222
	},
	{
		id: 1456,
		time: 1455,
		velocity: 0.164166666666667,
		power: 6.61151264270813,
		road: 13395.40375,
		acceleration: 0
	},
	{
		id: 1457,
		time: 1456,
		velocity: 0,
		power: 6.61151264270813,
		road: 13395.4584722222,
		acceleration: 0
	},
	{
		id: 1458,
		time: 1457,
		velocity: 0,
		power: 7.09823062920802,
		road: 13395.5153240741,
		acceleration: 0.00425925925925925
	},
	{
		id: 1459,
		time: 1458,
		velocity: 0.176944444444444,
		power: 7.12612981030628,
		road: 13395.5743055555,
		acceleration: 0
	},
	{
		id: 1460,
		time: 1459,
		velocity: 0,
		power: 7.12612981030628,
		road: 13395.633287037,
		acceleration: 0
	},
	{
		id: 1461,
		time: 1460,
		velocity: 0,
		power: 1.91514939896036,
		road: 13395.6627777778,
		acceleration: -0.0589814814814815
	},
	{
		id: 1462,
		time: 1461,
		velocity: 0,
		power: 0,
		road: 13395.6627777778,
		acceleration: 0
	},
	{
		id: 1463,
		time: 1462,
		velocity: 0,
		power: 0,
		road: 13395.6627777778,
		acceleration: 0
	},
	{
		id: 1464,
		time: 1463,
		velocity: 0,
		power: 0,
		road: 13395.6627777778,
		acceleration: 0
	},
	{
		id: 1465,
		time: 1464,
		velocity: 0,
		power: 139.310681870817,
		road: 13395.9038888889,
		acceleration: 0.482222222222222
	},
	{
		id: 1466,
		time: 1465,
		velocity: 1.44666666666667,
		power: 1041.96296718803,
		road: 13396.8832407407,
		acceleration: 0.994259259259259
	},
	{
		id: 1467,
		time: 1466,
		velocity: 2.98277777777778,
		power: 3925.27261080272,
		road: 13399.1908796296,
		acceleration: 1.66231481481482
	},
	{
		id: 1468,
		time: 1467,
		velocity: 4.98694444444444,
		power: 5620.11870402555,
		road: 13403.0321759259,
		acceleration: 1.405
	},
	{
		id: 1469,
		time: 1468,
		velocity: 5.66166666666667,
		power: 5329.01801242653,
		road: 13408.0625925926,
		acceleration: 0.973240740740741
	},
	{
		id: 1470,
		time: 1469,
		velocity: 5.9025,
		power: 5451.9422241485,
		road: 13413.9898148148,
		acceleration: 0.82037037037037
	},
	{
		id: 1471,
		time: 1470,
		velocity: 7.44805555555556,
		power: 7434.01109597898,
		road: 13420.8223148148,
		acceleration: 0.990185185185185
	},
	{
		id: 1472,
		time: 1471,
		velocity: 8.63222222222222,
		power: 10344.9468242485,
		road: 13428.7536574074,
		acceleration: 1.2075
	},
	{
		id: 1473,
		time: 1472,
		velocity: 9.525,
		power: 7050.59680439165,
		road: 13437.6204166667,
		acceleration: 0.663333333333334
	},
	{
		id: 1474,
		time: 1473,
		velocity: 9.43805555555555,
		power: 2819.62104085431,
		road: 13446.8906018518,
		acceleration: 0.143518518518517
	},
	{
		id: 1475,
		time: 1474,
		velocity: 9.06277777777778,
		power: -202.928869487159,
		road: 13456.1332407407,
		acceleration: -0.198611111111111
	},
	{
		id: 1476,
		time: 1475,
		velocity: 8.92916666666667,
		power: -435.992396335472,
		road: 13465.164537037,
		acceleration: -0.224074074074075
	},
	{
		id: 1477,
		time: 1476,
		velocity: 8.76583333333333,
		power: 2352.05435284239,
		road: 13474.1350925926,
		acceleration: 0.102592592592593
	},
	{
		id: 1478,
		time: 1477,
		velocity: 9.37055555555556,
		power: 2990.41623249489,
		road: 13483.2422685185,
		acceleration: 0.17064814814815
	},
	{
		id: 1479,
		time: 1478,
		velocity: 9.44111111111111,
		power: 3765.7473842477,
		road: 13492.5587962963,
		acceleration: 0.248055555555554
	},
	{
		id: 1480,
		time: 1479,
		velocity: 9.51,
		power: 3020.78289197861,
		road: 13502.0767129629,
		acceleration: 0.154722222222224
	},
	{
		id: 1481,
		time: 1480,
		velocity: 9.83472222222222,
		power: 5221.0270104538,
		road: 13511.8613425926,
		acceleration: 0.378703703703701
	},
	{
		id: 1482,
		time: 1481,
		velocity: 10.5772222222222,
		power: 6485.61662306635,
		road: 13522.0753703704,
		acceleration: 0.480092592592595
	},
	{
		id: 1483,
		time: 1482,
		velocity: 10.9502777777778,
		power: 6726.72501362222,
		road: 13532.7636574074,
		acceleration: 0.468425925925926
	},
	{
		id: 1484,
		time: 1483,
		velocity: 11.24,
		power: 5148.22900515142,
		road: 13543.8318518518,
		acceleration: 0.291388888888887
	},
	{
		id: 1485,
		time: 1484,
		velocity: 11.4513888888889,
		power: 5126.73476263182,
		road: 13555.1825462963,
		acceleration: 0.27361111111111
	},
	{
		id: 1486,
		time: 1485,
		velocity: 11.7711111111111,
		power: 5587.09851257251,
		road: 13566.8198611111,
		acceleration: 0.299629629629631
	},
	{
		id: 1487,
		time: 1486,
		velocity: 12.1388888888889,
		power: 4767.40330795868,
		road: 13578.7135185185,
		acceleration: 0.213055555555556
	},
	{
		id: 1488,
		time: 1487,
		velocity: 12.0905555555556,
		power: 4198.17316978486,
		road: 13590.7910648148,
		acceleration: 0.154722222222221
	},
	{
		id: 1489,
		time: 1488,
		velocity: 12.2352777777778,
		power: 2201.55199865534,
		road: 13602.9356944444,
		acceleration: -0.0205555555555552
	},
	{
		id: 1490,
		time: 1489,
		velocity: 12.0772222222222,
		power: 1652.25850066196,
		road: 13615.0366203704,
		acceleration: -0.0668518518518511
	},
	{
		id: 1491,
		time: 1490,
		velocity: 11.89,
		power: 1065.43270298856,
		road: 13627.04625,
		acceleration: -0.115740740740744
	},
	{
		id: 1492,
		time: 1491,
		velocity: 11.8880555555556,
		power: 1996.1294164511,
		road: 13638.9818055555,
		acceleration: -0.0324074074074048
	},
	{
		id: 1493,
		time: 1492,
		velocity: 11.98,
		power: 2832.67927968246,
		road: 13650.9216203704,
		acceleration: 0.0409259259259258
	},
	{
		id: 1494,
		time: 1493,
		velocity: 12.0127777777778,
		power: 3926.09083377184,
		road: 13662.9485185185,
		acceleration: 0.133240740740739
	},
	{
		id: 1495,
		time: 1494,
		velocity: 12.2877777777778,
		power: 4500.80115109797,
		road: 13675.1301388889,
		acceleration: 0.176203703703704
	},
	{
		id: 1496,
		time: 1495,
		velocity: 12.5086111111111,
		power: 6618.70740926182,
		road: 13687.5712962963,
		acceleration: 0.34287037037037
	},
	{
		id: 1497,
		time: 1496,
		velocity: 13.0413888888889,
		power: 6530.86640165545,
		road: 13700.3421296296,
		acceleration: 0.316481481481482
	},
	{
		id: 1498,
		time: 1497,
		velocity: 13.2372222222222,
		power: 5012.30385764205,
		road: 13713.3613888889,
		acceleration: 0.180370370370371
	},
	{
		id: 1499,
		time: 1498,
		velocity: 13.0497222222222,
		power: 2535.22294101696,
		road: 13726.4601388889,
		acceleration: -0.0213888888888878
	},
	{
		id: 1500,
		time: 1499,
		velocity: 12.9772222222222,
		power: 871.835968230496,
		road: 13739.4718518518,
		acceleration: -0.152685185185186
	},
	{
		id: 1501,
		time: 1500,
		velocity: 12.7791666666667,
		power: 1578.38441057969,
		road: 13752.3608333333,
		acceleration: -0.0927777777777763
	},
	{
		id: 1502,
		time: 1501,
		velocity: 12.7713888888889,
		power: 1924.07508555479,
		road: 13765.1721296296,
		acceleration: -0.0625925925925923
	},
	{
		id: 1503,
		time: 1502,
		velocity: 12.7894444444444,
		power: 6078.99375620433,
		road: 13778.0879629629,
		acceleration: 0.271666666666667
	},
	{
		id: 1504,
		time: 1503,
		velocity: 13.5941666666667,
		power: 11515.9420453128,
		road: 13791.4758796296,
		acceleration: 0.672499999999999
	},
	{
		id: 1505,
		time: 1504,
		velocity: 14.7888888888889,
		power: 14044.2408607866,
		road: 13805.6006018518,
		acceleration: 0.801111111111112
	},
	{
		id: 1506,
		time: 1505,
		velocity: 15.1927777777778,
		power: 10533.9474867087,
		road: 13820.3737962963,
		acceleration: 0.495833333333332
	},
	{
		id: 1507,
		time: 1506,
		velocity: 15.0816666666667,
		power: 3210.53580023609,
		road: 13835.3796759259,
		acceleration: -0.0304629629629627
	},
	{
		id: 1508,
		time: 1507,
		velocity: 14.6975,
		power: -1203.54454327766,
		road: 13850.2021759259,
		acceleration: -0.336296296296299
	},
	{
		id: 1509,
		time: 1508,
		velocity: 14.1838888888889,
		power: -2987.26039161386,
		road: 13864.6257407407,
		acceleration: -0.461574074074072
	},
	{
		id: 1510,
		time: 1509,
		velocity: 13.6969444444444,
		power: -7091.0398521717,
		road: 13878.4320833333,
		acceleration: -0.77287037037037
	},
	{
		id: 1511,
		time: 1510,
		velocity: 12.3788888888889,
		power: -12671.1893179607,
		road: 13891.2230092592,
		acceleration: -1.25796296296296
	},
	{
		id: 1512,
		time: 1511,
		velocity: 10.41,
		power: -12155.8016314107,
		road: 13902.7299074074,
		acceleration: -1.31009259259259
	},
	{
		id: 1513,
		time: 1512,
		velocity: 9.76666666666667,
		power: -10062.6390029143,
		road: 13912.9729166666,
		acceleration: -1.21768518518519
	},
	{
		id: 1514,
		time: 1513,
		velocity: 8.72583333333333,
		power: -5911.90637882281,
		road: 13922.1824074074,
		acceleration: -0.849351851851853
	},
	{
		id: 1515,
		time: 1514,
		velocity: 7.86194444444444,
		power: -4565.32617658176,
		road: 13930.5986111111,
		acceleration: -0.737222222222222
	},
	{
		id: 1516,
		time: 1515,
		velocity: 7.555,
		power: -2848.17957546372,
		road: 13938.3729629629,
		acceleration: -0.546481481481481
	},
	{
		id: 1517,
		time: 1516,
		velocity: 7.08638888888889,
		power: -1599.6053691296,
		road: 13945.6802777778,
		acceleration: -0.387592592592592
	},
	{
		id: 1518,
		time: 1517,
		velocity: 6.69916666666667,
		power: -606.335481611551,
		road: 13952.6707407407,
		acceleration: -0.246111111111111
	},
	{
		id: 1519,
		time: 1518,
		velocity: 6.81666666666667,
		power: -761.990013813687,
		road: 13959.4021759259,
		acceleration: -0.271944444444443
	},
	{
		id: 1520,
		time: 1519,
		velocity: 6.27055555555556,
		power: -1518.14658635618,
		road: 13965.7975925926,
		acceleration: -0.400092592592593
	},
	{
		id: 1521,
		time: 1520,
		velocity: 5.49888888888889,
		power: -2800.22516252204,
		road: 13971.6686574074,
		acceleration: -0.64861111111111
	},
	{
		id: 1522,
		time: 1521,
		velocity: 4.87083333333333,
		power: -1542.98398462488,
		road: 13976.9912037037,
		acceleration: -0.448425925925926
	},
	{
		id: 1523,
		time: 1522,
		velocity: 4.92527777777778,
		power: 1348.04330497057,
		road: 13982.1556944444,
		acceleration: 0.132314814814815
	},
	{
		id: 1524,
		time: 1523,
		velocity: 5.89583333333333,
		power: 4317.2072975465,
		road: 13987.7219444444,
		acceleration: 0.671203703703704
	},
	{
		id: 1525,
		time: 1524,
		velocity: 6.88444444444444,
		power: 6754.23843269098,
		road: 13994.105,
		acceleration: 0.962407407407407
	},
	{
		id: 1526,
		time: 1525,
		velocity: 7.8125,
		power: 7159.24009081769,
		road: 14001.4058796296,
		acceleration: 0.873240740740741
	},
	{
		id: 1527,
		time: 1526,
		velocity: 8.51555555555555,
		power: 6695.22554225283,
		road: 14009.4959722222,
		acceleration: 0.705185185185186
	},
	{
		id: 1528,
		time: 1527,
		velocity: 9,
		power: 5384.60057280145,
		road: 14018.1793518518,
		acceleration: 0.481388888888889
	},
	{
		id: 1529,
		time: 1528,
		velocity: 9.25666666666667,
		power: 3871.03826568931,
		road: 14027.2408796296,
		acceleration: 0.274907407407406
	},
	{
		id: 1530,
		time: 1529,
		velocity: 9.34027777777778,
		power: 2807.3030159991,
		road: 14036.5109259259,
		acceleration: 0.142129629629631
	},
	{
		id: 1531,
		time: 1530,
		velocity: 9.42638888888889,
		power: 3906.4522564552,
		road: 14045.9796296296,
		acceleration: 0.255185185185185
	},
	{
		id: 1532,
		time: 1531,
		velocity: 10.0222222222222,
		power: 5137.7818412124,
		road: 14055.7609259259,
		acceleration: 0.370000000000001
	},
	{
		id: 1533,
		time: 1532,
		velocity: 10.4502777777778,
		power: 7319.42124699251,
		road: 14066.0086574074,
		acceleration: 0.562870370370369
	},
	{
		id: 1534,
		time: 1533,
		velocity: 11.115,
		power: 5224.68634725454,
		road: 14076.698287037,
		acceleration: 0.320925925925927
	},
	{
		id: 1535,
		time: 1534,
		velocity: 10.985,
		power: 2014.99986718364,
		road: 14087.5488888889,
		acceleration: 0.0010185185185172
	},
	{
		id: 1536,
		time: 1535,
		velocity: 10.4533333333333,
		power: -1406.64579799938,
		road: 14098.2350462963,
		acceleration: -0.329907407407408
	},
	{
		id: 1537,
		time: 1536,
		velocity: 10.1252777777778,
		power: -3429.00281375536,
		road: 14108.4874074074,
		acceleration: -0.537685185185186
	},
	{
		id: 1538,
		time: 1537,
		velocity: 9.37194444444444,
		power: -5539.13491959651,
		road: 14118.0781018518,
		acceleration: -0.785648148148148
	},
	{
		id: 1539,
		time: 1538,
		velocity: 8.09638888888889,
		power: -7469.18324967788,
		road: 14126.7381018518,
		acceleration: -1.07574074074074
	},
	{
		id: 1540,
		time: 1539,
		velocity: 6.89805555555555,
		power: -5699.39104477266,
		road: 14134.3885185185,
		acceleration: -0.943425925925924
	},
	{
		id: 1541,
		time: 1540,
		velocity: 6.54166666666667,
		power: -3126.81484829747,
		road: 14141.2506944444,
		acceleration: -0.633055555555555
	},
	{
		id: 1542,
		time: 1541,
		velocity: 6.19722222222222,
		power: -1174.45561331928,
		road: 14147.6243055555,
		acceleration: -0.344074074074075
	},
	{
		id: 1543,
		time: 1542,
		velocity: 5.86583333333333,
		power: 627.881309577634,
		road: 14153.8048611111,
		acceleration: -0.0420370370370371
	},
	{
		id: 1544,
		time: 1543,
		velocity: 6.41555555555556,
		power: 3890.46754353913,
		road: 14160.2086111111,
		acceleration: 0.488425925925926
	},
	{
		id: 1545,
		time: 1544,
		velocity: 7.6625,
		power: 6527.98705986097,
		road: 14167.2650925926,
		acceleration: 0.817037037037037
	},
	{
		id: 1546,
		time: 1545,
		velocity: 8.31694444444444,
		power: 6375.31198364324,
		road: 14175.0779166666,
		acceleration: 0.695648148148148
	},
	{
		id: 1547,
		time: 1546,
		velocity: 8.5025,
		power: 2105.7776968818,
		road: 14183.2905555555,
		acceleration: 0.103981481481481
	},
	{
		id: 1548,
		time: 1547,
		velocity: 7.97444444444444,
		power: 137.287035878384,
		road: 14191.4813888889,
		acceleration: -0.147592592592593
	},
	{
		id: 1549,
		time: 1548,
		velocity: 7.87416666666667,
		power: -1020.68648530331,
		road: 14199.449537037,
		acceleration: -0.297777777777777
	},
	{
		id: 1550,
		time: 1549,
		velocity: 7.60916666666667,
		power: -653.405400073043,
		road: 14207.1437962963,
		acceleration: -0.25
	},
	{
		id: 1551,
		time: 1550,
		velocity: 7.22444444444444,
		power: -485.314221759079,
		road: 14214.5994907407,
		acceleration: -0.22712962962963
	},
	{
		id: 1552,
		time: 1551,
		velocity: 7.19277777777778,
		power: 142.696675058296,
		road: 14221.8733333333,
		acceleration: -0.136574074074074
	},
	{
		id: 1553,
		time: 1552,
		velocity: 7.19944444444444,
		power: 488.955552315401,
		road: 14229.0366203704,
		acceleration: -0.0845370370370366
	},
	{
		id: 1554,
		time: 1553,
		velocity: 6.97083333333333,
		power: -270.987593890933,
		road: 14236.0597685185,
		acceleration: -0.195740740740741
	},
	{
		id: 1555,
		time: 1554,
		velocity: 6.60555555555556,
		power: -1115.25458010999,
		road: 14242.8217592592,
		acceleration: -0.326574074074074
	},
	{
		id: 1556,
		time: 1555,
		velocity: 6.21972222222222,
		power: -1874.57586743461,
		road: 14249.1905555555,
		acceleration: -0.459814814814814
	},
	{
		id: 1557,
		time: 1556,
		velocity: 5.59138888888889,
		power: -3363.53943315009,
		road: 14254.9491203704,
		acceleration: -0.760648148148148
	},
	{
		id: 1558,
		time: 1557,
		velocity: 4.32361111111111,
		power: -5078.1798648101,
		road: 14259.6938888889,
		acceleration: -1.26694444444444
	},
	{
		id: 1559,
		time: 1558,
		velocity: 2.41888888888889,
		power: -3479.51744770129,
		road: 14263.2178240741,
		acceleration: -1.17472222222222
	},
	{
		id: 1560,
		time: 1559,
		velocity: 2.06722222222222,
		power: -1512.24539831196,
		road: 14265.7774537037,
		acceleration: -0.753888888888889
	},
	{
		id: 1561,
		time: 1560,
		velocity: 2.06194444444444,
		power: 143.370084716944,
		road: 14267.9302314815,
		acceleration: -0.059814814814815
	},
	{
		id: 1562,
		time: 1561,
		velocity: 2.23944444444444,
		power: 450.517219846279,
		road: 14270.0976388889,
		acceleration: 0.0890740740740741
	},
	{
		id: 1563,
		time: 1562,
		velocity: 2.33444444444444,
		power: 674.567238717679,
		road: 14272.3988888889,
		acceleration: 0.178611111111111
	},
	{
		id: 1564,
		time: 1563,
		velocity: 2.59777777777778,
		power: 655.744648161907,
		road: 14274.8642129629,
		acceleration: 0.149537037037037
	},
	{
		id: 1565,
		time: 1564,
		velocity: 2.68805555555556,
		power: 131.684949178599,
		road: 14277.3665740741,
		acceleration: -0.0754629629629626
	},
	{
		id: 1566,
		time: 1565,
		velocity: 2.10805555555556,
		power: -646.402759840391,
		road: 14279.6144907407,
		acceleration: -0.433425925925926
	},
	{
		id: 1567,
		time: 1566,
		velocity: 1.2975,
		power: -450.747381426808,
		road: 14281.4516666666,
		acceleration: -0.388055555555555
	},
	{
		id: 1568,
		time: 1567,
		velocity: 1.52388888888889,
		power: -94.2437933420634,
		road: 14282.998287037,
		acceleration: -0.193055555555556
	},
	{
		id: 1569,
		time: 1568,
		velocity: 1.52888888888889,
		power: 401.93206514672,
		road: 14284.5230092592,
		acceleration: 0.149259259259259
	},
	{
		id: 1570,
		time: 1569,
		velocity: 1.74527777777778,
		power: 428.85169993624,
		road: 14286.1932407407,
		acceleration: 0.141759259259259
	},
	{
		id: 1571,
		time: 1570,
		velocity: 1.94916666666667,
		power: 192.817825338616,
		road: 14287.9283796296,
		acceleration: -0.0119444444444445
	},
	{
		id: 1572,
		time: 1571,
		velocity: 1.49305555555556,
		power: -617.899248252653,
		road: 14289.3666666666,
		acceleration: -0.581759259259259
	},
	{
		id: 1573,
		time: 1572,
		velocity: 0,
		power: -406.809976704003,
		road: 14290.1892129629,
		acceleration: -0.649722222222222
	},
	{
		id: 1574,
		time: 1573,
		velocity: 0,
		power: -87.2624614197531,
		road: 14290.4380555555,
		acceleration: -0.497685185185185
	},
	{
		id: 1575,
		time: 1574,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1576,
		time: 1575,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1577,
		time: 1576,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1578,
		time: 1577,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1579,
		time: 1578,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1580,
		time: 1579,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1581,
		time: 1580,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1582,
		time: 1581,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1583,
		time: 1582,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1584,
		time: 1583,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1585,
		time: 1584,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1586,
		time: 1585,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1587,
		time: 1586,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1588,
		time: 1587,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1589,
		time: 1588,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1590,
		time: 1589,
		velocity: 0,
		power: 0,
		road: 14290.4380555555,
		acceleration: 0
	},
	{
		id: 1591,
		time: 1590,
		velocity: 0,
		power: 425.317913420497,
		road: 14290.8809259259,
		acceleration: 0.885740740740741
	},
	{
		id: 1592,
		time: 1591,
		velocity: 2.65722222222222,
		power: 3443.40366049069,
		road: 14292.6998611111,
		acceleration: 1.86638888888889
	},
	{
		id: 1593,
		time: 1592,
		velocity: 5.59916666666667,
		power: 7364.87343282336,
		road: 14296.4252777778,
		acceleration: 1.94657407407407
	},
	{
		id: 1594,
		time: 1593,
		velocity: 5.83972222222222,
		power: 7448.63159214425,
		road: 14301.7835185185,
		acceleration: 1.31907407407407
	},
	{
		id: 1595,
		time: 1594,
		velocity: 6.61444444444444,
		power: 5269.6176809988,
		road: 14308.1606481481,
		acceleration: 0.718703703703704
	},
	{
		id: 1596,
		time: 1595,
		velocity: 7.75527777777778,
		power: 7570.24617811398,
		road: 14315.3706018518,
		acceleration: 0.946944444444446
	},
	{
		id: 1597,
		time: 1596,
		velocity: 8.68055555555556,
		power: 7317.26572663596,
		road: 14323.4478240741,
		acceleration: 0.787592592592591
	},
	{
		id: 1598,
		time: 1597,
		velocity: 8.97722222222222,
		power: 5076.95574394263,
		road: 14332.1405555555,
		acceleration: 0.443425925925926
	},
	{
		id: 1599,
		time: 1598,
		velocity: 9.08555555555555,
		power: 4149.36860658047,
		road: 14341.2083796296,
		acceleration: 0.306759259259259
	},
	{
		id: 1600,
		time: 1599,
		velocity: 9.60083333333333,
		power: 3587.96160758541,
		road: 14350.5431018518,
		acceleration: 0.227037037037038
	},
	{
		id: 1601,
		time: 1600,
		velocity: 9.65833333333333,
		power: 4008.3082558393,
		road: 14360.1214351852,
		acceleration: 0.260185185185186
	},
	{
		id: 1602,
		time: 1601,
		velocity: 9.86611111111111,
		power: 1702.73717969876,
		road: 14369.8316203704,
		acceleration: 0.0035185185185167
	},
	{
		id: 1603,
		time: 1602,
		velocity: 9.61138888888889,
		power: 417.954677032368,
		road: 14379.4763888889,
		acceleration: -0.13435185185185
	},
	{
		id: 1604,
		time: 1603,
		velocity: 9.25527777777778,
		power: -508.220658402462,
		road: 14388.9368518518,
		acceleration: -0.234259259259259
	},
	{
		id: 1605,
		time: 1604,
		velocity: 9.16333333333333,
		power: 728.040247752066,
		road: 14398.2332407407,
		acceleration: -0.0938888888888894
	},
	{
		id: 1606,
		time: 1605,
		velocity: 9.32972222222222,
		power: -593.835961913605,
		road: 14407.3613425926,
		acceleration: -0.242685185185184
	},
	{
		id: 1607,
		time: 1606,
		velocity: 8.52722222222222,
		power: -3030.0329113484,
		road: 14416.100787037,
		acceleration: -0.534629629629631
	},
	{
		id: 1608,
		time: 1607,
		velocity: 7.55944444444444,
		power: -4485.18777080761,
		road: 14424.1997685185,
		acceleration: -0.746296296296295
	},
	{
		id: 1609,
		time: 1608,
		velocity: 7.09083333333333,
		power: -2707.01945381024,
		road: 14431.6554166667,
		acceleration: -0.540370370370371
	},
	{
		id: 1610,
		time: 1609,
		velocity: 6.90611111111111,
		power: -668.292161047182,
		road: 14438.7133796296,
		acceleration: -0.255
	},
	{
		id: 1611,
		time: 1610,
		velocity: 6.79444444444444,
		power: 897.376062496943,
		road: 14445.6348148148,
		acceleration: -0.0180555555555557
	},
	{
		id: 1612,
		time: 1611,
		velocity: 7.03666666666667,
		power: 1574.56570496893,
		road: 14452.5889351852,
		acceleration: 0.0834259259259262
	},
	{
		id: 1613,
		time: 1612,
		velocity: 7.15638888888889,
		power: 803.368004313032,
		road: 14459.5678703704,
		acceleration: -0.0337962962962957
	},
	{
		id: 1614,
		time: 1613,
		velocity: 6.69305555555555,
		power: 776.653599652545,
		road: 14466.5114351852,
		acceleration: -0.0369444444444458
	},
	{
		id: 1615,
		time: 1614,
		velocity: 6.92583333333333,
		power: 1554.30802617015,
		road: 14473.4764814815,
		acceleration: 0.0799074074074078
	},
	{
		id: 1616,
		time: 1615,
		velocity: 7.39611111111111,
		power: 4209.24227729818,
		road: 14480.7089351852,
		acceleration: 0.454907407407407
	},
	{
		id: 1617,
		time: 1616,
		velocity: 8.05777777777778,
		power: 4462.86560911321,
		road: 14488.3936574074,
		acceleration: 0.44962962962963
	},
	{
		id: 1618,
		time: 1617,
		velocity: 8.27472222222222,
		power: 2670.53122967969,
		road: 14496.3967592592,
		acceleration: 0.187129629629629
	},
	{
		id: 1619,
		time: 1618,
		velocity: 7.9575,
		power: -1014.56734172957,
		road: 14504.3448611111,
		acceleration: -0.297129629629629
	},
	{
		id: 1620,
		time: 1619,
		velocity: 7.16638888888889,
		power: -3475.64292909112,
		road: 14511.8206018518,
		acceleration: -0.647592592592593
	},
	{
		id: 1621,
		time: 1620,
		velocity: 6.33194444444444,
		power: -5293.19837177611,
		road: 14518.478287037,
		acceleration: -0.988518518518518
	},
	{
		id: 1622,
		time: 1621,
		velocity: 4.99194444444444,
		power: -5688.12319328959,
		road: 14524.0302314815,
		acceleration: -1.22296296296296
	},
	{
		id: 1623,
		time: 1622,
		velocity: 3.4975,
		power: -3905.64561513858,
		road: 14528.4346296296,
		acceleration: -1.07212962962963
	},
	{
		id: 1624,
		time: 1623,
		velocity: 3.11555555555556,
		power: -2076.60215038151,
		road: 14531.9221759259,
		acceleration: -0.761574074074074
	},
	{
		id: 1625,
		time: 1624,
		velocity: 2.70722222222222,
		power: -634.071733091777,
		road: 14534.8486574074,
		acceleration: -0.360555555555556
	},
	{
		id: 1626,
		time: 1625,
		velocity: 2.41583333333333,
		power: -294.805728606624,
		road: 14537.4699537037,
		acceleration: -0.249814814814814
	},
	{
		id: 1627,
		time: 1626,
		velocity: 2.36611111111111,
		power: -177.361898699752,
		road: 14539.8619444444,
		acceleration: -0.208796296296296
	},
	{
		id: 1628,
		time: 1627,
		velocity: 2.08083333333333,
		power: 262.642244117251,
		road: 14542.145,
		acceleration: -0.00907407407407401
	},
	{
		id: 1629,
		time: 1628,
		velocity: 2.38861111111111,
		power: 432.015344649861,
		road: 14544.4568055555,
		acceleration: 0.0665740740740741
	},
	{
		id: 1630,
		time: 1629,
		velocity: 2.56583333333333,
		power: 546.834108635283,
		road: 14546.8566666667,
		acceleration: 0.109537037037037
	},
	{
		id: 1631,
		time: 1630,
		velocity: 2.40944444444444,
		power: 245.917063549875,
		road: 14549.2989814815,
		acceleration: -0.0246296296296298
	},
	{
		id: 1632,
		time: 1631,
		velocity: 2.31472222222222,
		power: -79.0472660000358,
		road: 14551.6459722222,
		acceleration: -0.166018518518518
	},
	{
		id: 1633,
		time: 1632,
		velocity: 2.06777777777778,
		power: 52.2116344598624,
		road: 14553.8573148148,
		acceleration: -0.105277777777778
	},
	{
		id: 1634,
		time: 1633,
		velocity: 2.09361111111111,
		power: -9.96618660527064,
		road: 14555.9485648148,
		acceleration: -0.134907407407407
	},
	{
		id: 1635,
		time: 1634,
		velocity: 1.91,
		power: -257.210011800134,
		road: 14557.835787037,
		acceleration: -0.273148148148148
	},
	{
		id: 1636,
		time: 1635,
		velocity: 1.24833333333333,
		power: -435.176348945655,
		road: 14559.3727314815,
		acceleration: -0.427407407407407
	},
	{
		id: 1637,
		time: 1636,
		velocity: 0.811388888888889,
		power: -484.449882623063,
		road: 14560.3776388889,
		acceleration: -0.636666666666667
	},
	{
		id: 1638,
		time: 1637,
		velocity: 0,
		power: -130.804434542519,
		road: 14560.8561574074,
		acceleration: -0.416111111111111
	},
	{
		id: 1639,
		time: 1638,
		velocity: 0,
		power: -18.3117186322287,
		road: 14560.9913888889,
		acceleration: -0.270462962962963
	},
	{
		id: 1640,
		time: 1639,
		velocity: 0,
		power: 0,
		road: 14560.9913888889,
		acceleration: 0
	},
	{
		id: 1641,
		time: 1640,
		velocity: 0,
		power: 0,
		road: 14560.9913888889,
		acceleration: 0
	},
	{
		id: 1642,
		time: 1641,
		velocity: 0,
		power: 0,
		road: 14560.9913888889,
		acceleration: 0
	},
	{
		id: 1643,
		time: 1642,
		velocity: 0,
		power: 0,
		road: 14560.9913888889,
		acceleration: 0
	},
	{
		id: 1644,
		time: 1643,
		velocity: 0,
		power: 0,
		road: 14560.9913888889,
		acceleration: 0
	},
	{
		id: 1645,
		time: 1644,
		velocity: 0,
		power: 1.35371398380939,
		road: 14561.0011111111,
		acceleration: 0.0194444444444444
	},
	{
		id: 1646,
		time: 1645,
		velocity: 0.0583333333333333,
		power: 2.34924083311586,
		road: 14561.0205555555,
		acceleration: 0
	},
	{
		id: 1647,
		time: 1646,
		velocity: 0,
		power: 302.455325850145,
		road: 14561.3984722222,
		acceleration: 0.716944444444444
	},
	{
		id: 1648,
		time: 1647,
		velocity: 2.15083333333333,
		power: 1723.38360428218,
		road: 14562.7453240741,
		acceleration: 1.22092592592593
	},
	{
		id: 1649,
		time: 1648,
		velocity: 3.72111111111111,
		power: 4719.2414631404,
		road: 14565.5296759259,
		acceleration: 1.65407407407407
	},
	{
		id: 1650,
		time: 1649,
		velocity: 4.96222222222222,
		power: 4470.15529661866,
		road: 14569.644537037,
		acceleration: 1.00694444444444
	},
	{
		id: 1651,
		time: 1650,
		velocity: 5.17166666666667,
		power: 4581.830193163,
		road: 14574.6716666667,
		acceleration: 0.817592592592592
	},
	{
		id: 1652,
		time: 1651,
		velocity: 6.17388888888889,
		power: 4592.37278067901,
		road: 14580.4523611111,
		acceleration: 0.689537037037037
	},
	{
		id: 1653,
		time: 1652,
		velocity: 7.03083333333333,
		power: 6047.7039213497,
		road: 14586.9885648148,
		acceleration: 0.821481481481482
	},
	{
		id: 1654,
		time: 1653,
		velocity: 7.63611111111111,
		power: 3990.46770099569,
		road: 14594.1502314815,
		acceleration: 0.429444444444445
	},
	{
		id: 1655,
		time: 1654,
		velocity: 7.46222222222222,
		power: 1008.93900395468,
		road: 14601.5195833333,
		acceleration: -0.0140740740740757
	},
	{
		id: 1656,
		time: 1655,
		velocity: 6.98861111111111,
		power: -1296.60370302628,
		road: 14608.7088425926,
		acceleration: -0.34611111111111
	},
	{
		id: 1657,
		time: 1656,
		velocity: 6.59777777777778,
		power: -231.47984536542,
		road: 14615.6302777778,
		acceleration: -0.189537037037038
	},
	{
		id: 1658,
		time: 1657,
		velocity: 6.89361111111111,
		power: 2079.36808675556,
		road: 14622.5380555555,
		acceleration: 0.162222222222224
	},
	{
		id: 1659,
		time: 1658,
		velocity: 7.47527777777778,
		power: 3374.99815462197,
		road: 14629.6966203704,
		acceleration: 0.339351851851852
	},
	{
		id: 1660,
		time: 1659,
		velocity: 7.61583333333333,
		power: 1937.08994129758,
		road: 14637.0836111111,
		acceleration: 0.1175
	},
	{
		id: 1661,
		time: 1660,
		velocity: 7.24611111111111,
		power: 748.207059078627,
		road: 14644.5031481481,
		acceleration: -0.0524074074074079
	},
	{
		id: 1662,
		time: 1661,
		velocity: 7.31805555555555,
		power: 430.315746972647,
		road: 14651.8483796296,
		acceleration: -0.0962037037037042
	},
	{
		id: 1663,
		time: 1662,
		velocity: 7.32722222222222,
		power: 1385.65740201826,
		road: 14659.16625,
		acceleration: 0.0414814814814815
	},
	{
		id: 1664,
		time: 1663,
		velocity: 7.37055555555555,
		power: 1878.47370542115,
		road: 14666.5593055555,
		acceleration: 0.10888888888889
	},
	{
		id: 1665,
		time: 1664,
		velocity: 7.64472222222222,
		power: 2185.04065427531,
		road: 14674.0798611111,
		acceleration: 0.146111111111111
	},
	{
		id: 1666,
		time: 1665,
		velocity: 7.76555555555556,
		power: 3066.83608161341,
		road: 14681.8016666667,
		acceleration: 0.256388888888887
	},
	{
		id: 1667,
		time: 1666,
		velocity: 8.13972222222222,
		power: 4009.99299085071,
		road: 14689.8321296296,
		acceleration: 0.360925925925927
	},
	{
		id: 1668,
		time: 1667,
		velocity: 8.7275,
		power: 3154.21482507783,
		road: 14698.1587962963,
		acceleration: 0.231481481481481
	},
	{
		id: 1669,
		time: 1668,
		velocity: 8.46,
		power: 2737.75167528905,
		road: 14706.6856481481,
		acceleration: 0.168888888888889
	},
	{
		id: 1670,
		time: 1669,
		velocity: 8.64638888888889,
		power: 3175.89544819196,
		road: 14715.4031481481,
		acceleration: 0.212407407407406
	},
	{
		id: 1671,
		time: 1670,
		velocity: 9.36472222222222,
		power: 4708.11340704965,
		road: 14724.4145833333,
		acceleration: 0.375462962962963
	},
	{
		id: 1672,
		time: 1671,
		velocity: 9.58638888888889,
		power: 3622.95020804118,
		road: 14733.7297685185,
		acceleration: 0.232037037037038
	},
	{
		id: 1673,
		time: 1672,
		velocity: 9.3425,
		power: 1663.25986742743,
		road: 14743.1647222222,
		acceleration: 0.00750000000000028
	},
	{
		id: 1674,
		time: 1673,
		velocity: 9.38722222222222,
		power: 631.721313013554,
		road: 14752.5502314815,
		acceleration: -0.106388888888889
	},
	{
		id: 1675,
		time: 1674,
		velocity: 9.26722222222222,
		power: 2625.93849755216,
		road: 14761.940787037,
		acceleration: 0.116481481481481
	},
	{
		id: 1676,
		time: 1675,
		velocity: 9.69194444444444,
		power: 6038.08608312537,
		road: 14771.6266203704,
		acceleration: 0.474074074074075
	},
	{
		id: 1677,
		time: 1676,
		velocity: 10.8094444444444,
		power: 10402.8951393081,
		road: 14781.9825462963,
		acceleration: 0.86611111111111
	},
	{
		id: 1678,
		time: 1677,
		velocity: 11.8655555555556,
		power: 14865.4870635889,
		road: 14793.3565740741,
		acceleration: 1.17009259259259
	},
	{
		id: 1679,
		time: 1678,
		velocity: 13.2022222222222,
		power: 18031.1194951391,
		road: 14805.9568981481,
		acceleration: 1.2825
	},
	{
		id: 1680,
		time: 1679,
		velocity: 14.6569444444444,
		power: 18267.8312051294,
		road: 14819.7731944444,
		acceleration: 1.14944444444444
	},
	{
		id: 1681,
		time: 1680,
		velocity: 15.3138888888889,
		power: 17003.0670221984,
		road: 14834.6368055555,
		acceleration: 0.945185185185183
	},
	{
		id: 1682,
		time: 1681,
		velocity: 16.0377777777778,
		power: 9501.88810086034,
		road: 14850.1615277778,
		acceleration: 0.377037037037036
	},
	{
		id: 1683,
		time: 1682,
		velocity: 15.7880555555556,
		power: 4141.90349487277,
		road: 14865.8792592592,
		acceleration: 0.00898148148148259
	},
	{
		id: 1684,
		time: 1683,
		velocity: 15.3408333333333,
		power: -123.730885975834,
		road: 14881.4651388889,
		acceleration: -0.272685185185184
	},
	{
		id: 1685,
		time: 1684,
		velocity: 15.2197222222222,
		power: -2134.49423991406,
		road: 14896.7121759259,
		acceleration: -0.405000000000001
	},
	{
		id: 1686,
		time: 1685,
		velocity: 14.5730555555556,
		power: -2560.18058875583,
		road: 14911.5406481481,
		acceleration: -0.432129629629628
	},
	{
		id: 1687,
		time: 1686,
		velocity: 14.0444444444444,
		power: -2968.76837731442,
		road: 14925.9229629629,
		acceleration: -0.460185185185187
	},
	{
		id: 1688,
		time: 1687,
		velocity: 13.8391666666667,
		power: -1422.19589948784,
		road: 14939.9031018518,
		acceleration: -0.344166666666666
	},
	{
		id: 1689,
		time: 1688,
		velocity: 13.5405555555556,
		power: -181.916781828666,
		road: 14953.5876851852,
		acceleration: -0.246944444444445
	},
	{
		id: 1690,
		time: 1689,
		velocity: 13.3036111111111,
		power: -1332.09172618638,
		road: 14966.9824074074,
		acceleration: -0.332777777777777
	},
	{
		id: 1691,
		time: 1690,
		velocity: 12.8408333333333,
		power: -901.459608924393,
		road: 14980.0626851852,
		acceleration: -0.296111111111111
	},
	{
		id: 1692,
		time: 1691,
		velocity: 12.6522222222222,
		power: -1664.59577909467,
		road: 14992.8169444444,
		acceleration: -0.355925925925925
	},
	{
		id: 1693,
		time: 1692,
		velocity: 12.2358333333333,
		power: -1168.95287180526,
		road: 15005.2367129629,
		acceleration: -0.313055555555557
	},
	{
		id: 1694,
		time: 1693,
		velocity: 11.9016666666667,
		power: -680.247135410935,
		road: 15017.3653703704,
		acceleration: -0.269166666666667
	},
	{
		id: 1695,
		time: 1694,
		velocity: 11.8447222222222,
		power: 1351.5449451239,
		road: 15029.3147685185,
		acceleration: -0.0893518518518519
	},
	{
		id: 1696,
		time: 1695,
		velocity: 11.9677777777778,
		power: 2437.5203071338,
		road: 15041.2231018518,
		acceleration: 0.00722222222222335
	},
	{
		id: 1697,
		time: 1696,
		velocity: 11.9233333333333,
		power: 4030.26416708021,
		road: 15053.2071296296,
		acceleration: 0.144166666666665
	},
	{
		id: 1698,
		time: 1697,
		velocity: 12.2772222222222,
		power: 3748.71318982128,
		road: 15065.3203703704,
		acceleration: 0.11425925925926
	},
	{
		id: 1699,
		time: 1698,
		velocity: 12.3105555555556,
		power: 4443.23568993099,
		road: 15077.5747222222,
		acceleration: 0.167962962962964
	},
	{
		id: 1700,
		time: 1699,
		velocity: 12.4272222222222,
		power: 3182.77192226882,
		road: 15089.9411111111,
		acceleration: 0.056111111111111
	},
	{
		id: 1701,
		time: 1700,
		velocity: 12.4455555555556,
		power: 3995.18840612558,
		road: 15102.3962037037,
		acceleration: 0.121296296296295
	},
	{
		id: 1702,
		time: 1701,
		velocity: 12.6744444444444,
		power: 4149.08492232249,
		road: 15114.9764351852,
		acceleration: 0.128981481481484
	},
	{
		id: 1703,
		time: 1702,
		velocity: 12.8141666666667,
		power: 4805.80816451102,
		road: 15127.7094907407,
		acceleration: 0.176666666666668
	},
	{
		id: 1704,
		time: 1703,
		velocity: 12.9755555555556,
		power: 4595.52300535546,
		road: 15140.6069444444,
		acceleration: 0.152129629629627
	},
	{
		id: 1705,
		time: 1704,
		velocity: 13.1308333333333,
		power: 5427.90037727183,
		road: 15153.6859259259,
		acceleration: 0.210925925925926
	},
	{
		id: 1706,
		time: 1705,
		velocity: 13.4469444444444,
		power: 4867.92132973078,
		road: 15166.9493055555,
		acceleration: 0.157870370370372
	},
	{
		id: 1707,
		time: 1706,
		velocity: 13.4491666666667,
		power: 4471.15240621857,
		road: 15180.3519907407,
		acceleration: 0.120740740740741
	},
	{
		id: 1708,
		time: 1707,
		velocity: 13.4930555555556,
		power: 3491.50600584433,
		road: 15193.8356944444,
		acceleration: 0.0412962962962951
	},
	{
		id: 1709,
		time: 1708,
		velocity: 13.5708333333333,
		power: 3975.84417910307,
		road: 15207.3783796296,
		acceleration: 0.076666666666668
	},
	{
		id: 1710,
		time: 1709,
		velocity: 13.6791666666667,
		power: 3020.2097057338,
		road: 15220.9601388889,
		acceleration: 0.00148148148148231
	},
	{
		id: 1711,
		time: 1710,
		velocity: 13.4975,
		power: 1545.49258288823,
		road: 15234.4871759259,
		acceleration: -0.110925925925926
	},
	{
		id: 1712,
		time: 1711,
		velocity: 13.2380555555556,
		power: 667.160679487678,
		road: 15247.8706481481,
		acceleration: -0.176203703703704
	},
	{
		id: 1713,
		time: 1712,
		velocity: 13.1505555555556,
		power: 1148.92601298173,
		road: 15261.0984722222,
		acceleration: -0.135092592592594
	},
	{
		id: 1714,
		time: 1713,
		velocity: 13.0922222222222,
		power: 4103.77006408061,
		road: 15274.3085648148,
		acceleration: 0.0996296296296286
	},
	{
		id: 1715,
		time: 1714,
		velocity: 13.5369444444444,
		power: 5823.69559223096,
		road: 15287.6824074074,
		acceleration: 0.227870370370372
	},
	{
		id: 1716,
		time: 1715,
		velocity: 13.8341666666667,
		power: 6833.28940879258,
		road: 15301.3165277778,
		acceleration: 0.292685185185185
	},
	{
		id: 1717,
		time: 1716,
		velocity: 13.9702777777778,
		power: 4879.87533752385,
		road: 15315.16375,
		acceleration: 0.133518518518517
	},
	{
		id: 1718,
		time: 1717,
		velocity: 13.9375,
		power: 4336.37014113831,
		road: 15329.1217592592,
		acceleration: 0.088055555555556
	},
	{
		id: 1719,
		time: 1718,
		velocity: 14.0983333333333,
		power: 3864.04798761722,
		road: 15343.1488425926,
		acceleration: 0.0500925925925948
	},
	{
		id: 1720,
		time: 1719,
		velocity: 14.1205555555556,
		power: 4523.46601285875,
		road: 15357.2491666666,
		acceleration: 0.0963888888888889
	},
	{
		id: 1721,
		time: 1720,
		velocity: 14.2266666666667,
		power: 4030.71577788089,
		road: 15371.4261574074,
		acceleration: 0.0569444444444436
	},
	{
		id: 1722,
		time: 1721,
		velocity: 14.2691666666667,
		power: 3812.12331302323,
		road: 15385.6511574074,
		acceleration: 0.0390740740740725
	},
	{
		id: 1723,
		time: 1722,
		velocity: 14.2377777777778,
		power: 2667.39757014216,
		road: 15399.8731481481,
		acceleration: -0.0450925925925922
	},
	{
		id: 1724,
		time: 1723,
		velocity: 14.0913888888889,
		power: 940.246752211521,
		road: 15413.9875925926,
		acceleration: -0.17
	},
	{
		id: 1725,
		time: 1724,
		velocity: 13.7591666666667,
		power: -288.97302820319,
		road: 15427.8879629629,
		acceleration: -0.258148148148146
	},
	{
		id: 1726,
		time: 1725,
		velocity: 13.4633333333333,
		power: -49.013754671065,
		road: 15441.5411111111,
		acceleration: -0.236296296296295
	},
	{
		id: 1727,
		time: 1726,
		velocity: 13.3825,
		power: 428.459672967027,
		road: 15454.9781944444,
		acceleration: -0.195833333333336
	},
	{
		id: 1728,
		time: 1727,
		velocity: 13.1716666666667,
		power: 1451.83215242441,
		road: 15468.2611574074,
		acceleration: -0.112407407407407
	},
	{
		id: 1729,
		time: 1728,
		velocity: 13.1261111111111,
		power: 1272.40982908531,
		road: 15481.4259722222,
		acceleration: -0.12388888888889
	},
	{
		id: 1730,
		time: 1729,
		velocity: 13.0108333333333,
		power: 2252.23488336738,
		road: 15494.5070833333,
		acceleration: -0.0435185185185176
	},
	{
		id: 1731,
		time: 1730,
		velocity: 13.0411111111111,
		power: 2582.99768602409,
		road: 15507.5583796296,
		acceleration: -0.0161111111111101
	},
	{
		id: 1732,
		time: 1731,
		velocity: 13.0777777777778,
		power: 3247.25121134904,
		road: 15520.6200462963,
		acceleration: 0.0368518518518517
	},
	{
		id: 1733,
		time: 1732,
		velocity: 13.1213888888889,
		power: 5169.18739710235,
		road: 15533.7930555555,
		acceleration: 0.185833333333333
	},
	{
		id: 1734,
		time: 1733,
		velocity: 13.5986111111111,
		power: 6501.43789010254,
		road: 15547.1986111111,
		acceleration: 0.279259259259259
	},
	{
		id: 1735,
		time: 1734,
		velocity: 13.9155555555556,
		power: 6851.46794827168,
		road: 15560.8893055555,
		acceleration: 0.291018518518518
	},
	{
		id: 1736,
		time: 1735,
		velocity: 13.9944444444444,
		power: 3925.81568358381,
		road: 15574.7558333333,
		acceleration: 0.0606481481481485
	},
	{
		id: 1737,
		time: 1736,
		velocity: 13.7805555555556,
		power: 1052.11172545963,
		road: 15588.575,
		acceleration: -0.15537037037037
	},
	{
		id: 1738,
		time: 1737,
		velocity: 13.4494444444444,
		power: -213.744695543645,
		road: 15602.1922685185,
		acceleration: -0.248425925925924
	},
	{
		id: 1739,
		time: 1738,
		velocity: 13.2491666666667,
		power: -150.953307326147,
		road: 15615.5652777778,
		acceleration: -0.240092592592594
	},
	{
		id: 1740,
		time: 1739,
		velocity: 13.0602777777778,
		power: 952.423464529441,
		road: 15628.7434259259,
		acceleration: -0.149629629629629
	},
	{
		id: 1741,
		time: 1740,
		velocity: 13.0005555555556,
		power: 1260.99845873619,
		road: 15641.7857407407,
		acceleration: -0.122037037037039
	},
	{
		id: 1742,
		time: 1741,
		velocity: 12.8830555555556,
		power: 2168.41805786078,
		road: 15654.7436574074,
		acceleration: -0.0467592592592574
	},
	{
		id: 1743,
		time: 1742,
		velocity: 12.92,
		power: 2923.38612441501,
		road: 15667.6856018518,
		acceleration: 0.0148148148148142
	},
	{
		id: 1744,
		time: 1743,
		velocity: 13.045,
		power: 3441.19606491762,
		road: 15680.6626851852,
		acceleration: 0.055462962962963
	},
	{
		id: 1745,
		time: 1744,
		velocity: 13.0494444444444,
		power: 3344.22308722972,
		road: 15693.6904166667,
		acceleration: 0.0458333333333343
	},
	{
		id: 1746,
		time: 1745,
		velocity: 13.0575,
		power: 2519.99186378837,
		road: 15706.7306481481,
		acceleration: -0.0208333333333339
	},
	{
		id: 1747,
		time: 1746,
		velocity: 12.9825,
		power: 3149.9407734916,
		road: 15719.7752777778,
		acceleration: 0.0296296296296301
	},
	{
		id: 1748,
		time: 1747,
		velocity: 13.1383333333333,
		power: 2728.31819387338,
		road: 15732.8324074074,
		acceleration: -0.00462962962962798
	},
	{
		id: 1749,
		time: 1748,
		velocity: 13.0436111111111,
		power: 3123.02417681501,
		road: 15745.9005555555,
		acceleration: 0.0266666666666655
	},
	{
		id: 1750,
		time: 1749,
		velocity: 13.0625,
		power: 2221.18320578123,
		road: 15758.9593518518,
		acceleration: -0.0453703703703709
	},
	{
		id: 1751,
		time: 1750,
		velocity: 13.0022222222222,
		power: 2496.97746734496,
		road: 15771.9843518518,
		acceleration: -0.0222222222222239
	},
	{
		id: 1752,
		time: 1751,
		velocity: 12.9769444444444,
		power: 2496.80328898038,
		road: 15784.9874537037,
		acceleration: -0.0215740740740742
	},
	{
		id: 1753,
		time: 1752,
		velocity: 12.9977777777778,
		power: 2727.61845833759,
		road: 15797.9784722222,
		acceleration: -0.00259259259259181
	},
	{
		id: 1754,
		time: 1753,
		velocity: 12.9944444444444,
		power: 2822.24037043421,
		road: 15810.9706944444,
		acceleration: 0.00500000000000078
	},
	{
		id: 1755,
		time: 1754,
		velocity: 12.9919444444444,
		power: 2588.64928335813,
		road: 15823.9585648148,
		acceleration: -0.0137037037037047
	},
	{
		id: 1756,
		time: 1755,
		velocity: 12.9566666666667,
		power: 3988.84425723432,
		road: 15836.9883796296,
		acceleration: 0.0975925925925925
	},
	{
		id: 1757,
		time: 1756,
		velocity: 13.2872222222222,
		power: 3039.02709645054,
		road: 15850.0766203704,
		acceleration: 0.0192592592592611
	},
	{
		id: 1758,
		time: 1757,
		velocity: 13.0497222222222,
		power: 3543.6985385448,
		road: 15863.2036111111,
		acceleration: 0.0582407407407395
	},
	{
		id: 1759,
		time: 1758,
		velocity: 13.1313888888889,
		power: 1815.11368751797,
		road: 15876.32,
		acceleration: -0.0794444444444444
	},
	{
		id: 1760,
		time: 1759,
		velocity: 13.0488888888889,
		power: 2753.89141349035,
		road: 15889.3950925926,
		acceleration: -0.00314814814814923
	},
	{
		id: 1761,
		time: 1760,
		velocity: 13.0402777777778,
		power: 2514.57847626695,
		road: 15902.4576388889,
		acceleration: -0.0219444444444434
	},
	{
		id: 1762,
		time: 1761,
		velocity: 13.0655555555556,
		power: 2748.64917705624,
		road: 15915.5078240741,
		acceleration: -0.00277777777777644
	},
	{
		id: 1763,
		time: 1762,
		velocity: 13.0405555555556,
		power: 2720.6113834187,
		road: 15928.5541666667,
		acceleration: -0.00490740740740847
	},
	{
		id: 1764,
		time: 1763,
		velocity: 13.0255555555556,
		power: 2425.09162536071,
		road: 15941.5839814815,
		acceleration: -0.0281481481481478
	},
	{
		id: 1765,
		time: 1764,
		velocity: 12.9811111111111,
		power: 2716.97944439708,
		road: 15954.5976388889,
		acceleration: -0.00416666666666643
	},
	{
		id: 1766,
		time: 1765,
		velocity: 13.0280555555556,
		power: 2611.43761025932,
		road: 15967.6030092592,
		acceleration: -0.012407407407407
	},
	{
		id: 1767,
		time: 1766,
		velocity: 12.9883333333333,
		power: 1422.87555879974,
		road: 15980.5487962963,
		acceleration: -0.106759259259261
	},
	{
		id: 1768,
		time: 1767,
		velocity: 12.6608333333333,
		power: 289.485301832185,
		road: 15993.3431944444,
		acceleration: -0.196018518518516
	},
	{
		id: 1769,
		time: 1768,
		velocity: 12.44,
		power: -2220.91385433513,
		road: 16005.8387037037,
		acceleration: -0.40175925925926
	},
	{
		id: 1770,
		time: 1769,
		velocity: 11.7830555555556,
		power: -4188.45324836537,
		road: 16017.8461111111,
		acceleration: -0.574444444444445
	},
	{
		id: 1771,
		time: 1770,
		velocity: 10.9375,
		power: -7245.85224863561,
		road: 16029.1299074074,
		acceleration: -0.872777777777777
	},
	{
		id: 1772,
		time: 1771,
		velocity: 9.82166666666667,
		power: -9921.49850232732,
		road: 16039.3758333333,
		acceleration: -1.20296296296296
	},
	{
		id: 1773,
		time: 1772,
		velocity: 8.17416666666667,
		power: -9060.34687344939,
		road: 16048.4067129629,
		acceleration: -1.22712962962963
	},
	{
		id: 1774,
		time: 1773,
		velocity: 7.25611111111111,
		power: -8778.16534339587,
		road: 16056.1474537037,
		acceleration: -1.35314814814815
	},
	{
		id: 1775,
		time: 1774,
		velocity: 5.76222222222222,
		power: -6961.94332265545,
		road: 16062.5658333333,
		acceleration: -1.29157407407408
	},
	{
		id: 1776,
		time: 1775,
		velocity: 4.29944444444444,
		power: -7136.24168560533,
		road: 16067.5076851852,
		acceleration: -1.66148148148148
	},
	{
		id: 1777,
		time: 1776,
		velocity: 2.27166666666667,
		power: -4438.85305191168,
		road: 16070.8527777778,
		acceleration: -1.53203703703704
	},
	{
		id: 1778,
		time: 1777,
		velocity: 1.16611111111111,
		power: -2302.40114667032,
		road: 16072.7152777778,
		acceleration: -1.43314814814815
	},
	{
		id: 1779,
		time: 1778,
		velocity: 0,
		power: -457.679119458612,
		road: 16073.4825925926,
		acceleration: -0.757222222222222
	},
	{
		id: 1780,
		time: 1779,
		velocity: 0,
		power: -48.0880354450942,
		road: 16073.6769444444,
		acceleration: -0.388703703703704
	},
	{
		id: 1781,
		time: 1780,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1782,
		time: 1781,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1783,
		time: 1782,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1784,
		time: 1783,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1785,
		time: 1784,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1786,
		time: 1785,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1787,
		time: 1786,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1788,
		time: 1787,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1789,
		time: 1788,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1790,
		time: 1789,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1791,
		time: 1790,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1792,
		time: 1791,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1793,
		time: 1792,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1794,
		time: 1793,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1795,
		time: 1794,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1796,
		time: 1795,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1797,
		time: 1796,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1798,
		time: 1797,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1799,
		time: 1798,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1800,
		time: 1799,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1801,
		time: 1800,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1802,
		time: 1801,
		velocity: 0,
		power: 0,
		road: 16073.6769444444,
		acceleration: 0
	},
	{
		id: 1803,
		time: 1802,
		velocity: 0,
		power: 341.61228592442,
		road: 16074.070787037,
		acceleration: 0.787685185185185
	},
	{
		id: 1804,
		time: 1803,
		velocity: 2.36305555555556,
		power: 2242.61419562103,
		road: 16075.5783796296,
		acceleration: 1.43981481481481
	},
	{
		id: 1805,
		time: 1804,
		velocity: 4.31944444444444,
		power: 6119.86187483133,
		road: 16078.7542592592,
		acceleration: 1.89675925925926
	},
	{
		id: 1806,
		time: 1805,
		velocity: 5.69027777777778,
		power: 5652.57389064669,
		road: 16083.4431018518,
		acceleration: 1.12916666666667
	},
	{
		id: 1807,
		time: 1806,
		velocity: 5.75055555555556,
		power: 4484.27624967823,
		road: 16089.0450925926,
		acceleration: 0.69712962962963
	},
	{
		id: 1808,
		time: 1807,
		velocity: 6.41083333333333,
		power: 3464.17546995559,
		road: 16095.2164351852,
		acceleration: 0.441574074074073
	},
	{
		id: 1809,
		time: 1808,
		velocity: 7.015,
		power: 4331.8018023784,
		road: 16101.8744444444,
		acceleration: 0.53175925925926
	},
	{
		id: 1810,
		time: 1809,
		velocity: 7.34583333333333,
		power: 4859.76753586725,
		road: 16109.0747685185,
		acceleration: 0.55287037037037
	},
	{
		id: 1811,
		time: 1810,
		velocity: 8.06944444444444,
		power: 5102.69099001174,
		road: 16116.8172222222,
		acceleration: 0.531388888888887
	},
	{
		id: 1812,
		time: 1811,
		velocity: 8.60916666666667,
		power: 5526.00761005867,
		road: 16125.0931018518,
		acceleration: 0.535462962962965
	},
	{
		id: 1813,
		time: 1812,
		velocity: 8.95222222222222,
		power: 5610.16498428261,
		road: 16133.88625,
		acceleration: 0.499074074074073
	},
	{
		id: 1814,
		time: 1813,
		velocity: 9.56666666666667,
		power: 6398.15508260628,
		road: 16143.2013425926,
		acceleration: 0.544814814814815
	},
	{
		id: 1815,
		time: 1814,
		velocity: 10.2436111111111,
		power: 5616.55417459933,
		road: 16152.9989351852,
		acceleration: 0.420185185185185
	},
	{
		id: 1816,
		time: 1815,
		velocity: 10.2127777777778,
		power: 3702.59094270873,
		road: 16163.1063425926,
		acceleration: 0.199444444444445
	},
	{
		id: 1817,
		time: 1816,
		velocity: 10.165,
		power: 3665.4715405788,
		road: 16173.4065740741,
		acceleration: 0.186203703703702
	},
	{
		id: 1818,
		time: 1817,
		velocity: 10.8022222222222,
		power: 7055.08961588536,
		road: 16184.0518981481,
		acceleration: 0.503981481481482
	},
	{
		id: 1819,
		time: 1818,
		velocity: 11.7247222222222,
		power: 10262.9365125369,
		road: 16195.3269444444,
		acceleration: 0.755462962962962
	},
	{
		id: 1820,
		time: 1819,
		velocity: 12.4313888888889,
		power: 10191.5989015226,
		road: 16207.3208333333,
		acceleration: 0.682222222222222
	},
	{
		id: 1821,
		time: 1820,
		velocity: 12.8488888888889,
		power: 7562.57553735321,
		road: 16219.8637037037,
		acceleration: 0.415740740740741
	},
	{
		id: 1822,
		time: 1821,
		velocity: 12.9719444444444,
		power: 5099.43344821562,
		road: 16232.7121296296,
		acceleration: 0.19537037037037
	},
	{
		id: 1823,
		time: 1822,
		velocity: 13.0175,
		power: 4305.82678466054,
		road: 16245.7202314815,
		acceleration: 0.123981481481483
	},
	{
		id: 1824,
		time: 1823,
		velocity: 13.2208333333333,
		power: 5587.66364860411,
		road: 16258.8997222222,
		acceleration: 0.218796296296297
	},
	{
		id: 1825,
		time: 1824,
		velocity: 13.6283333333333,
		power: 6909.9769585594,
		road: 16272.3431481481,
		acceleration: 0.309074074074072
	},
	{
		id: 1826,
		time: 1825,
		velocity: 13.9447222222222,
		power: 5967.5992903133,
		road: 16286.0523611111,
		acceleration: 0.2225
	},
	{
		id: 1827,
		time: 1826,
		velocity: 13.8883333333333,
		power: 2174.97242144181,
		road: 16299.8381481481,
		acceleration: -0.0693518518518523
	},
	{
		id: 1828,
		time: 1827,
		velocity: 13.4202777777778,
		power: -1414.88136582823,
		road: 16313.4189814815,
		acceleration: -0.340555555555554
	},
	{
		id: 1829,
		time: 1828,
		velocity: 12.9230555555556,
		power: -5087.48615805438,
		road: 16326.5139351852,
		acceleration: -0.631203703703704
	},
	{
		id: 1830,
		time: 1829,
		velocity: 11.9947222222222,
		power: -15337.2101059763,
		road: 16338.5191203704,
		acceleration: -1.54833333333333
	},
	{
		id: 1831,
		time: 1830,
		velocity: 8.77527777777778,
		power: -17180.5420943844,
		road: 16348.7777777778,
		acceleration: -1.94472222222222
	},
	{
		id: 1832,
		time: 1831,
		velocity: 7.08888888888889,
		power: -14654.3706932226,
		road: 16357.0502777778,
		acceleration: -2.02759259259259
	},
	{
		id: 1833,
		time: 1832,
		velocity: 5.91194444444444,
		power: -6735.55026552273,
		road: 16363.7001851852,
		acceleration: -1.21759259259259
	},
	{
		id: 1834,
		time: 1833,
		velocity: 5.1225,
		power: -4289.66145818167,
		road: 16369.2631481481,
		acceleration: -0.956296296296296
	},
	{
		id: 1835,
		time: 1834,
		velocity: 4.22,
		power: -2562.79320447873,
		road: 16373.9927314815,
		acceleration: -0.710462962962963
	},
	{
		id: 1836,
		time: 1835,
		velocity: 3.78055555555556,
		power: -1645.49382213389,
		road: 16378.087037037,
		acceleration: -0.560092592592593
	},
	{
		id: 1837,
		time: 1836,
		velocity: 3.44222222222222,
		power: 44.6584864408433,
		road: 16381.8399074074,
		acceleration: -0.122777777777778
	},
	{
		id: 1838,
		time: 1837,
		velocity: 3.85166666666667,
		power: 629.836667267861,
		road: 16385.5531481481,
		acceleration: 0.0435185185185185
	},
	{
		id: 1839,
		time: 1838,
		velocity: 3.91111111111111,
		power: 497.305953597268,
		road: 16389.2906018518,
		acceleration: 0.00490740740740758
	},
	{
		id: 1840,
		time: 1839,
		velocity: 3.45694444444444,
		power: -627.266135023214,
		road: 16392.8709259259,
		acceleration: -0.319166666666667
	},
	{
		id: 1841,
		time: 1840,
		velocity: 2.89416666666667,
		power: -201.180206659461,
		road: 16396.1929629629,
		acceleration: -0.197407407407407
	},
	{
		id: 1842,
		time: 1841,
		velocity: 3.31888888888889,
		power: 134.17097715417,
		road: 16399.3719907407,
		acceleration: -0.0886111111111108
	},
	{
		id: 1843,
		time: 1842,
		velocity: 3.19111111111111,
		power: 752.347022137004,
		road: 16402.5643055555,
		acceleration: 0.115185185185185
	},
	{
		id: 1844,
		time: 1843,
		velocity: 3.23972222222222,
		power: 27.0655806970878,
		road: 16405.7521296296,
		acceleration: -0.124166666666667
	},
	{
		id: 1845,
		time: 1844,
		velocity: 2.94638888888889,
		power: 938.460972864923,
		road: 16408.9651388889,
		acceleration: 0.174537037037037
	},
	{
		id: 1846,
		time: 1845,
		velocity: 3.71472222222222,
		power: 2467.90647821349,
		road: 16412.5596759259,
		acceleration: 0.588518518518518
	},
	{
		id: 1847,
		time: 1846,
		velocity: 5.00527777777778,
		power: 5412.71765849751,
		road: 16417.0181944444,
		acceleration: 1.13944444444445
	},
	{
		id: 1848,
		time: 1847,
		velocity: 6.36472222222222,
		power: 5443.13595988303,
		road: 16422.4969444444,
		acceleration: 0.901018518518518
	},
	{
		id: 1849,
		time: 1848,
		velocity: 6.41777777777778,
		power: 3348.95562440031,
		road: 16428.6386574074,
		acceleration: 0.424907407407408
	},
	{
		id: 1850,
		time: 1849,
		velocity: 6.28,
		power: 545.107135037946,
		road: 16434.9631944444,
		acceleration: -0.0592592592592593
	},
	{
		id: 1851,
		time: 1850,
		velocity: 6.18694444444444,
		power: 23.6752906388607,
		road: 16441.1855092592,
		acceleration: -0.145185185185185
	},
	{
		id: 1852,
		time: 1851,
		velocity: 5.98222222222222,
		power: -336.310183331052,
		road: 16447.2319907407,
		acceleration: -0.206481481481481
	},
	{
		id: 1853,
		time: 1852,
		velocity: 5.66055555555556,
		power: -322.722759932523,
		road: 16453.0728703703,
		acceleration: -0.204722222222222
	},
	{
		id: 1854,
		time: 1853,
		velocity: 5.57277777777778,
		power: 1148.7961092029,
		road: 16458.8430555555,
		acceleration: 0.0633333333333326
	},
	{
		id: 1855,
		time: 1854,
		velocity: 6.17222222222222,
		power: 3279.25859334714,
		road: 16464.8577777778,
		acceleration: 0.425740740740741
	},
	{
		id: 1856,
		time: 1855,
		velocity: 6.93777777777778,
		power: 3387.47967954511,
		road: 16471.2870833333,
		acceleration: 0.403425925925925
	},
	{
		id: 1857,
		time: 1856,
		velocity: 6.78305555555556,
		power: 1526.29856944497,
		road: 16477.9620833333,
		acceleration: 0.0879629629629637
	},
	{
		id: 1858,
		time: 1857,
		velocity: 6.43611111111111,
		power: -707.119607535013,
		road: 16484.5487037037,
		acceleration: -0.264722222222221
	},
	{
		id: 1859,
		time: 1858,
		velocity: 6.14361111111111,
		power: -872.370899933207,
		road: 16490.8553240741,
		acceleration: -0.295277777777778
	},
	{
		id: 1860,
		time: 1859,
		velocity: 5.89722222222222,
		power: -173.206974413856,
		road: 16496.9252314815,
		acceleration: -0.178148148148148
	},
	{
		id: 1861,
		time: 1860,
		velocity: 5.90166666666667,
		power: 440.004910018957,
		road: 16502.8713425926,
		acceleration: -0.0694444444444446
	},
	{
		id: 1862,
		time: 1861,
		velocity: 5.93527777777778,
		power: 2208.00107839802,
		road: 16508.9013888889,
		acceleration: 0.237314814814814
	},
	{
		id: 1863,
		time: 1862,
		velocity: 6.60916666666667,
		power: 2453.13726328056,
		road: 16515.1807407407,
		acceleration: 0.261296296296297
	},
	{
		id: 1864,
		time: 1863,
		velocity: 6.68555555555556,
		power: 3939.1257752187,
		road: 16521.82625,
		acceleration: 0.471018518518519
	},
	{
		id: 1865,
		time: 1864,
		velocity: 7.34833333333333,
		power: 4048.11005948749,
		road: 16528.9289351852,
		acceleration: 0.443333333333332
	},
	{
		id: 1866,
		time: 1865,
		velocity: 7.93916666666667,
		power: 9224.60238854279,
		road: 16536.78875,
		acceleration: 1.07092592592593
	},
	{
		id: 1867,
		time: 1866,
		velocity: 9.89833333333333,
		power: 11250.8350710966,
		road: 16545.7563425926,
		acceleration: 1.14462962962963
	},
	{
		id: 1868,
		time: 1867,
		velocity: 10.7822222222222,
		power: 12016.2496752059,
		road: 16555.8298148148,
		acceleration: 1.06712962962963
	},
	{
		id: 1869,
		time: 1868,
		velocity: 11.1405555555556,
		power: 8868.2259269189,
		road: 16566.7647685185,
		acceleration: 0.655833333333334
	},
	{
		id: 1870,
		time: 1869,
		velocity: 11.8658333333333,
		power: 8204.69257236238,
		road: 16578.2993518518,
		acceleration: 0.543425925925925
	},
	{
		id: 1871,
		time: 1870,
		velocity: 12.4125,
		power: 9308.95980479886,
		road: 16590.4037037037,
		acceleration: 0.596111111111112
	},
	{
		id: 1872,
		time: 1871,
		velocity: 12.9288888888889,
		power: 6402.60158372719,
		road: 16602.965,
		acceleration: 0.317777777777778
	},
	{
		id: 1873,
		time: 1872,
		velocity: 12.8191666666667,
		power: 3769.00571733489,
		road: 16615.7302314815,
		acceleration: 0.0900925925925922
	},
	{
		id: 1874,
		time: 1873,
		velocity: 12.6827777777778,
		power: 1963.85517714426,
		road: 16628.51125,
		acceleration: -0.0585185185185182
	},
	{
		id: 1875,
		time: 1874,
		velocity: 12.7533333333333,
		power: 2920.60682181433,
		road: 16641.273287037,
		acceleration: 0.0205555555555552
	},
	{
		id: 1876,
		time: 1875,
		velocity: 12.8808333333333,
		power: 5092.11333848211,
		road: 16654.1425,
		acceleration: 0.193796296296297
	},
	{
		id: 1877,
		time: 1876,
		velocity: 13.2641666666667,
		power: 7718.91328144471,
		road: 16667.303287037,
		acceleration: 0.389351851851851
	},
	{
		id: 1878,
		time: 1877,
		velocity: 13.9213888888889,
		power: 7743.58290263207,
		road: 16680.8428703704,
		acceleration: 0.368240740740742
	},
	{
		id: 1879,
		time: 1878,
		velocity: 13.9855555555556,
		power: 6241.65331298621,
		road: 16694.6849537037,
		acceleration: 0.23675925925926
	},
	{
		id: 1880,
		time: 1879,
		velocity: 13.9744444444444,
		power: 4247.11787360434,
		road: 16708.6852777778,
		acceleration: 0.0797222222222214
	},
	{
		id: 1881,
		time: 1880,
		velocity: 14.1605555555556,
		power: 5736.93888458625,
		road: 16722.8179629629,
		acceleration: 0.184999999999999
	},
	{
		id: 1882,
		time: 1881,
		velocity: 14.5405555555556,
		power: 7932.99377669523,
		road: 16737.2095833333,
		acceleration: 0.332870370370371
	},
	{
		id: 1883,
		time: 1882,
		velocity: 14.9730555555556,
		power: 6702.60194090362,
		road: 16751.8823148148,
		acceleration: 0.229351851851852
	},
	{
		id: 1884,
		time: 1883,
		velocity: 14.8486111111111,
		power: 3479.457669773,
		road: 16766.6675462963,
		acceleration: -0.00435185185185283
	},
	{
		id: 1885,
		time: 1884,
		velocity: 14.5275,
		power: -1104.64490357356,
		road: 16781.2870833333,
		acceleration: -0.327037037037035
	},
	{
		id: 1886,
		time: 1885,
		velocity: 13.9919444444444,
		power: -1675.11805663009,
		road: 16795.5605092592,
		acceleration: -0.365185185185188
	},
	{
		id: 1887,
		time: 1886,
		velocity: 13.7530555555556,
		power: -2073.71418193507,
		road: 16809.4550462963,
		acceleration: -0.392592592592592
	},
	{
		id: 1888,
		time: 1887,
		velocity: 13.3497222222222,
		power: -1816.49841445084,
		road: 16822.9676851852,
		acceleration: -0.371203703703701
	},
	{
		id: 1889,
		time: 1888,
		velocity: 12.8783333333333,
		power: -1810.60311109581,
		road: 16836.1101388889,
		acceleration: -0.369166666666667
	},
	{
		id: 1890,
		time: 1889,
		velocity: 12.6455555555556,
		power: 1115.81422037576,
		road: 16849.0027777778,
		acceleration: -0.130462962962964
	},
	{
		id: 1891,
		time: 1890,
		velocity: 12.9583333333333,
		power: 4485.77459029328,
		road: 16861.9017592592,
		acceleration: 0.143148148148148
	},
	{
		id: 1892,
		time: 1891,
		velocity: 13.3077777777778,
		power: 7607.23756905283,
		road: 16875.0625462963,
		acceleration: 0.380462962962966
	},
	{
		id: 1893,
		time: 1892,
		velocity: 13.7869444444444,
		power: 7388.0107921595,
		road: 16888.5844444444,
		acceleration: 0.341759259259257
	},
	{
		id: 1894,
		time: 1893,
		velocity: 13.9836111111111,
		power: 6182.44264884294,
		road: 16902.3941666666,
		acceleration: 0.23388888888889
	},
	{
		id: 1895,
		time: 1894,
		velocity: 14.0094444444444,
		power: 1897.79823532112,
		road: 16916.2743981481,
		acceleration: -0.0928703703703704
	},
	{
		id: 1896,
		time: 1895,
		velocity: 13.5083333333333,
		power: 114.950059917234,
		road: 16929.995787037,
		acceleration: -0.224814814814817
	},
	{
		id: 1897,
		time: 1896,
		velocity: 13.3091666666667,
		power: -941.964930127253,
		road: 16943.4533333333,
		acceleration: -0.302870370370371
	},
	{
		id: 1898,
		time: 1897,
		velocity: 13.1008333333333,
		power: 934.391474368329,
		road: 16956.6833796296,
		acceleration: -0.152129629629629
	},
	{
		id: 1899,
		time: 1898,
		velocity: 13.0519444444444,
		power: 699.383703762716,
		road: 16969.7535185185,
		acceleration: -0.167685185185185
	},
	{
		id: 1900,
		time: 1899,
		velocity: 12.8061111111111,
		power: 1724.68469050043,
		road: 16982.6986574074,
		acceleration: -0.082314814814815
	},
	{
		id: 1901,
		time: 1900,
		velocity: 12.8538888888889,
		power: 728.166230390773,
		road: 16995.5223148148,
		acceleration: -0.160648148148148
	},
	{
		id: 1902,
		time: 1901,
		velocity: 12.57,
		power: 787.098880502853,
		road: 17008.1892592592,
		acceleration: -0.152777777777777
	},
	{
		id: 1903,
		time: 1902,
		velocity: 12.3477777777778,
		power: -33.1897261021023,
		road: 17020.670787037,
		acceleration: -0.218055555555555
	},
	{
		id: 1904,
		time: 1903,
		velocity: 12.1997222222222,
		power: 1078.08353070157,
		road: 17032.9827314815,
		acceleration: -0.121111111111112
	},
	{
		id: 1905,
		time: 1904,
		velocity: 12.2066666666667,
		power: 1611.66038079219,
		road: 17045.1975,
		acceleration: -0.0732407407407401
	},
	{
		id: 1906,
		time: 1905,
		velocity: 12.1280555555556,
		power: 1526.2213773394,
		road: 17057.3362962963,
		acceleration: -0.0787037037037042
	},
	{
		id: 1907,
		time: 1906,
		velocity: 11.9636111111111,
		power: 598.969090622641,
		road: 17069.3574074074,
		acceleration: -0.156666666666666
	},
	{
		id: 1908,
		time: 1907,
		velocity: 11.7366666666667,
		power: -498.20559668533,
		road: 17081.175,
		acceleration: -0.250370370370369
	},
	{
		id: 1909,
		time: 1908,
		velocity: 11.3769444444444,
		power: -1082.90873038117,
		road: 17092.7169444444,
		acceleration: -0.300925925925929
	},
	{
		id: 1910,
		time: 1909,
		velocity: 11.0608333333333,
		power: -1241.99902660596,
		road: 17103.9511574074,
		acceleration: -0.314537037037034
	},
	{
		id: 1911,
		time: 1910,
		velocity: 10.7930555555556,
		power: -10.6076807961255,
		road: 17114.9299074074,
		acceleration: -0.19638888888889
	},
	{
		id: 1912,
		time: 1911,
		velocity: 10.7877777777778,
		power: 885.658103952233,
		road: 17125.7566203703,
		acceleration: -0.107685185185185
	},
	{
		id: 1913,
		time: 1912,
		velocity: 10.7377777777778,
		power: 1728.65053544576,
		road: 17136.5174074074,
		acceleration: -0.024166666666666
	},
	{
		id: 1914,
		time: 1913,
		velocity: 10.7205555555556,
		power: 1307.45080666012,
		road: 17147.2340277778,
		acceleration: -0.0641666666666669
	},
	{
		id: 1915,
		time: 1914,
		velocity: 10.5952777777778,
		power: 4738.3478387984,
		road: 17158.0516203703,
		acceleration: 0.26611111111111
	},
	{
		id: 1916,
		time: 1915,
		velocity: 11.5361111111111,
		power: 6919.47010110048,
		road: 17169.2280092592,
		acceleration: 0.451481481481482
	},
	{
		id: 1917,
		time: 1916,
		velocity: 12.075,
		power: 11743.2193722629,
		road: 17181.0478240741,
		acceleration: 0.83537037037037
	},
	{
		id: 1918,
		time: 1917,
		velocity: 13.1013888888889,
		power: 11420.0994670142,
		road: 17193.6515740741,
		acceleration: 0.7325
	},
	{
		id: 1919,
		time: 1918,
		velocity: 13.7336111111111,
		power: 10533.7050225371,
		road: 17206.9238888889,
		acceleration: 0.604629629629633
	},
	{
		id: 1920,
		time: 1919,
		velocity: 13.8888888888889,
		power: 6388.28263751668,
		road: 17220.6260185185,
		acceleration: 0.254999999999997
	},
	{
		id: 1921,
		time: 1920,
		velocity: 13.8663888888889,
		power: 2934.11727371513,
		road: 17234.4492129629,
		acceleration: -0.0128703703703703
	},
	{
		id: 1922,
		time: 1921,
		velocity: 13.695,
		power: 2413.43268345467,
		road: 17248.2402777778,
		acceleration: -0.0513888888888872
	},
	{
		id: 1923,
		time: 1922,
		velocity: 13.7347222222222,
		power: 2533.20376741888,
		road: 17261.9851851852,
		acceleration: -0.0409259259259276
	},
	{
		id: 1924,
		time: 1923,
		velocity: 13.7436111111111,
		power: 4369.68835061924,
		road: 17275.7585648148,
		acceleration: 0.0978703703703712
	},
	{
		id: 1925,
		time: 1924,
		velocity: 13.9886111111111,
		power: 7900.61909267733,
		road: 17289.7574074074,
		acceleration: 0.353055555555553
	},
	{
		id: 1926,
		time: 1925,
		velocity: 14.7938888888889,
		power: 9856.43219817219,
		road: 17304.1685185185,
		acceleration: 0.471481481481483
	},
	{
		id: 1927,
		time: 1926,
		velocity: 15.1580555555556,
		power: 13390.3869617702,
		road: 17319.1558333333,
		acceleration: 0.680925925925926
	},
	{
		id: 1928,
		time: 1927,
		velocity: 16.0313888888889,
		power: 12605.4006972424,
		road: 17334.7733333333,
		acceleration: 0.579444444444444
	},
	{
		id: 1929,
		time: 1928,
		velocity: 16.5322222222222,
		power: 11595.9315639167,
		road: 17350.9189351852,
		acceleration: 0.476759259259259
	},
	{
		id: 1930,
		time: 1929,
		velocity: 16.5883333333333,
		power: 6565.98293729676,
		road: 17367.371574074,
		acceleration: 0.137314814814815
	},
	{
		id: 1931,
		time: 1930,
		velocity: 16.4433333333333,
		power: 3590.79301795065,
		road: 17383.8662962963,
		acceleration: -0.0531481481481464
	},
	{
		id: 1932,
		time: 1931,
		velocity: 16.3727777777778,
		power: 2999.10409853873,
		road: 17400.2901851852,
		acceleration: -0.0885185185185193
	},
	{
		id: 1933,
		time: 1932,
		velocity: 16.3227777777778,
		power: 2167.23256469945,
		road: 17416.6006018518,
		acceleration: -0.13842592592593
	},
	{
		id: 1934,
		time: 1933,
		velocity: 16.0280555555556,
		power: 2000.74486608183,
		road: 17432.7691203703,
		acceleration: -0.145370370370369
	},
	{
		id: 1935,
		time: 1934,
		velocity: 15.9366666666667,
		power: 865.537608641681,
		road: 17448.7575,
		acceleration: -0.214907407407408
	},
	{
		id: 1936,
		time: 1935,
		velocity: 15.6780555555556,
		power: 1264.70413553305,
		road: 17464.5463425926,
		acceleration: -0.184166666666664
	},
	{
		id: 1937,
		time: 1936,
		velocity: 15.4755555555556,
		power: 1942.71014679056,
		road: 17480.1755555555,
		acceleration: -0.135092592592592
	},
	{
		id: 1938,
		time: 1937,
		velocity: 15.5313888888889,
		power: 1463.35088221556,
		road: 17495.6554166666,
		acceleration: -0.163611111111114
	},
	{
		id: 1939,
		time: 1938,
		velocity: 15.1872222222222,
		power: 1165.00452458883,
		road: 17510.9635185185,
		acceleration: -0.179907407407407
	},
	{
		id: 1940,
		time: 1939,
		velocity: 14.9358333333333,
		power: 123.63848535478,
		road: 17526.0580092592,
		acceleration: -0.247314814814814
	},
	{
		id: 1941,
		time: 1940,
		velocity: 14.7894444444444,
		power: 800.171158287963,
		road: 17540.9309259259,
		acceleration: -0.195833333333333
	},
	{
		id: 1942,
		time: 1941,
		velocity: 14.5997222222222,
		power: 1603.23189548804,
		road: 17555.638287037,
		acceleration: -0.135277777777778
	},
	{
		id: 1943,
		time: 1942,
		velocity: 14.53,
		power: 5083.36427393658,
		road: 17570.3344444444,
		acceleration: 0.112870370370372
	},
	{
		id: 1944,
		time: 1943,
		velocity: 15.1280555555556,
		power: 6632.08877613296,
		road: 17585.1946296296,
		acceleration: 0.215185185185186
	},
	{
		id: 1945,
		time: 1944,
		velocity: 15.2452777777778,
		power: 7511.99185332381,
		road: 17600.2947222222,
		acceleration: 0.26462962962963
	},
	{
		id: 1946,
		time: 1945,
		velocity: 15.3238888888889,
		power: 4335.5552745181,
		road: 17615.5465740741,
		acceleration: 0.0388888888888896
	},
	{
		id: 1947,
		time: 1946,
		velocity: 15.2447222222222,
		power: 2734.90514653862,
		road: 17630.7826388889,
		acceleration: -0.0704629629629636
	},
	{
		id: 1948,
		time: 1947,
		velocity: 15.0338888888889,
		power: 2066.9760276334,
		road: 17645.9265277778,
		acceleration: -0.113888888888891
	},
	{
		id: 1949,
		time: 1948,
		velocity: 14.9822222222222,
		power: 2529.31793804217,
		road: 17660.9738888889,
		acceleration: -0.0791666666666657
	},
	{
		id: 1950,
		time: 1949,
		velocity: 15.0072222222222,
		power: 3744.14506505856,
		road: 17675.9849537037,
		acceleration: 0.00657407407407362
	},
	{
		id: 1951,
		time: 1950,
		velocity: 15.0536111111111,
		power: 4131.57843578884,
		road: 17691.0157407407,
		acceleration: 0.0328703703703717
	},
	{
		id: 1952,
		time: 1951,
		velocity: 15.0808333333333,
		power: 3965.5987667236,
		road: 17706.0731481481,
		acceleration: 0.0203703703703688
	},
	{
		id: 1953,
		time: 1952,
		velocity: 15.0683333333333,
		power: 3883.68813633091,
		road: 17721.1477777778,
		acceleration: 0.0140740740740739
	},
	{
		id: 1954,
		time: 1953,
		velocity: 15.0958333333333,
		power: 3322.35804613905,
		road: 17736.2170833333,
		acceleration: -0.0247222222222199
	},
	{
		id: 1955,
		time: 1954,
		velocity: 15.0066666666667,
		power: 2627.47985353034,
		road: 17751.2382407407,
		acceleration: -0.0715740740740767
	},
	{
		id: 1956,
		time: 1955,
		velocity: 14.8536111111111,
		power: 1116.00617300125,
		road: 17766.1365277778,
		acceleration: -0.174166666666665
	},
	{
		id: 1957,
		time: 1956,
		velocity: 14.5733333333333,
		power: 129.296453042877,
		road: 17780.8277777778,
		acceleration: -0.239907407407408
	},
	{
		id: 1958,
		time: 1957,
		velocity: 14.2869444444444,
		power: 605.741762137879,
		road: 17795.2982407407,
		acceleration: -0.201666666666668
	},
	{
		id: 1959,
		time: 1958,
		velocity: 14.2486111111111,
		power: 1828.11533864675,
		road: 17809.6132407407,
		acceleration: -0.109259259259259
	},
	{
		id: 1960,
		time: 1959,
		velocity: 14.2455555555556,
		power: 2701.96203404843,
		road: 17823.8520833333,
		acceleration: -0.0430555555555543
	},
	{
		id: 1961,
		time: 1960,
		velocity: 14.1577777777778,
		power: 2546.80365598693,
		road: 17838.0428703704,
		acceleration: -0.0530555555555559
	},
	{
		id: 1962,
		time: 1961,
		velocity: 14.0894444444444,
		power: -39.4776094494437,
		road: 17852.0863425926,
		acceleration: -0.241574074074077
	},
	{
		id: 1963,
		time: 1962,
		velocity: 13.5208333333333,
		power: -3464.41440631985,
		road: 17865.7599074074,
		acceleration: -0.498240740740737
	},
	{
		id: 1964,
		time: 1963,
		velocity: 12.6630555555556,
		power: -6624.48757784957,
		road: 17878.8066203703,
		acceleration: -0.755462962962964
	},
	{
		id: 1965,
		time: 1964,
		velocity: 11.8230555555556,
		power: -6083.40081317113,
		road: 17891.1100462963,
		acceleration: -0.73111111111111
	},
	{
		id: 1966,
		time: 1965,
		velocity: 11.3275,
		power: -4270.87671323165,
		road: 17902.7536574074,
		acceleration: -0.588518518518519
	},
	{
		id: 1967,
		time: 1966,
		velocity: 10.8975,
		power: -1760.11276935728,
		road: 17913.9214814815,
		acceleration: -0.363055555555555
	},
	{
		id: 1968,
		time: 1967,
		velocity: 10.7338888888889,
		power: -860.108651564683,
		road: 17924.7693055555,
		acceleration: -0.276944444444446
	},
	{
		id: 1969,
		time: 1968,
		velocity: 10.4966666666667,
		power: 886.581436515419,
		road: 17935.4265740741,
		acceleration: -0.104166666666666
	},
	{
		id: 1970,
		time: 1969,
		velocity: 10.585,
		power: 3542.34921283493,
		road: 17946.1098148148,
		acceleration: 0.156111111111114
	},
	{
		id: 1971,
		time: 1970,
		velocity: 11.2022222222222,
		power: 5254.19157935628,
		road: 17957.02625,
		acceleration: 0.310277777777774
	},
	{
		id: 1972,
		time: 1971,
		velocity: 11.4275,
		power: 4436.12515625754,
		road: 17968.2069444444,
		acceleration: 0.218240740740743
	},
	{
		id: 1973,
		time: 1972,
		velocity: 11.2397222222222,
		power: 87.2256076480021,
		road: 17979.4018055555,
		acceleration: -0.189907407407409
	},
	{
		id: 1974,
		time: 1973,
		velocity: 10.6325,
		power: -4781.55878201528,
		road: 17990.1724074074,
		acceleration: -0.658611111111112
	},
	{
		id: 1975,
		time: 1974,
		velocity: 9.45166666666667,
		power: -10605.782704929,
		road: 17999.9538425926,
		acceleration: -1.31972222222222
	},
	{
		id: 1976,
		time: 1975,
		velocity: 7.28055555555556,
		power: -13382.1642629809,
		road: 18008.1331481481,
		acceleration: -1.88453703703704
	},
	{
		id: 1977,
		time: 1976,
		velocity: 4.97888888888889,
		power: -10458.4294075614,
		road: 18014.4203240741,
		acceleration: -1.89972222222222
	},
	{
		id: 1978,
		time: 1977,
		velocity: 3.7525,
		power: -4786.62897025793,
		road: 18019.1554629629,
		acceleration: -1.20435185185185
	},
	{
		id: 1979,
		time: 1978,
		velocity: 3.6675,
		power: -1370.02774510768,
		road: 18023.0344907407,
		acceleration: -0.50787037037037
	},
	{
		id: 1980,
		time: 1979,
		velocity: 3.45527777777778,
		power: -12.299038034137,
		road: 18026.5905092592,
		acceleration: -0.138148148148148
	},
	{
		id: 1981,
		time: 1980,
		velocity: 3.33805555555556,
		power: 373.462737055017,
		road: 18030.0669444444,
		acceleration: -0.0210185185185185
	},
	{
		id: 1982,
		time: 1981,
		velocity: 3.60444444444444,
		power: 1428.38396796541,
		road: 18033.6740740741,
		acceleration: 0.282407407407407
	},
	{
		id: 1983,
		time: 1982,
		velocity: 4.3025,
		power: 1234.46069141284,
		road: 18037.5234259259,
		acceleration: 0.202037037037037
	},
	{
		id: 1984,
		time: 1983,
		velocity: 3.94416666666667,
		power: 2263.49028915353,
		road: 18041.6911574074,
		acceleration: 0.434722222222223
	},
	{
		id: 1985,
		time: 1984,
		velocity: 4.90861111111111,
		power: 2554.04248561042,
		road: 18046.2983796296,
		acceleration: 0.444259259259258
	},
	{
		id: 1986,
		time: 1985,
		velocity: 5.63527777777778,
		power: 6255.81535812451,
		road: 18051.66875,
		acceleration: 1.08203703703704
	},
	{
		id: 1987,
		time: 1986,
		velocity: 7.19027777777778,
		power: 9156.8585887571,
		road: 18058.2371759259,
		acceleration: 1.31407407407407
	},
	{
		id: 1988,
		time: 1987,
		velocity: 8.85083333333333,
		power: 18589.3871388491,
		road: 18066.553287037,
		acceleration: 2.1812962962963
	},
	{
		id: 1989,
		time: 1988,
		velocity: 12.1791666666667,
		power: 17382.6346917294,
		road: 18076.7603703703,
		acceleration: 1.60064814814815
	},
	{
		id: 1990,
		time: 1989,
		velocity: 11.9922222222222,
		power: 13513.8730615152,
		road: 18088.281574074,
		acceleration: 1.02759259259259
	},
	{
		id: 1991,
		time: 1990,
		velocity: 11.9336111111111,
		power: 2393.87896965654,
		road: 18100.3162962963,
		acceleration: -0.000555555555557419
	},
	{
		id: 1992,
		time: 1991,
		velocity: 12.1775,
		power: 4647.0108899577,
		road: 18112.4463425926,
		acceleration: 0.191203703703707
	},
	{
		id: 1993,
		time: 1992,
		velocity: 12.5658333333333,
		power: 8437.08914518312,
		road: 18124.9188888889,
		acceleration: 0.493796296296296
	},
	{
		id: 1994,
		time: 1993,
		velocity: 13.415,
		power: 11099.0024520043,
		road: 18137.9719907407,
		acceleration: 0.667314814814816
	},
	{
		id: 1995,
		time: 1994,
		velocity: 14.1794444444444,
		power: 11377.7375195496,
		road: 18151.6768055555,
		acceleration: 0.636111111111111
	},
	{
		id: 1996,
		time: 1995,
		velocity: 14.4741666666667,
		power: 11072.229248813,
		road: 18165.9833796296,
		acceleration: 0.567407407407407
	},
	{
		id: 1997,
		time: 1996,
		velocity: 15.1172222222222,
		power: 8898.09130652782,
		road: 18180.7634259259,
		acceleration: 0.379537037037039
	},
	{
		id: 1998,
		time: 1997,
		velocity: 15.3180555555556,
		power: 5802.35999744679,
		road: 18195.8075462963,
		acceleration: 0.148611111111109
	},
	{
		id: 1999,
		time: 1998,
		velocity: 14.92,
		power: 1702.21988432782,
		road: 18210.8575925926,
		acceleration: -0.136759259259257
	},
	{
		id: 2000,
		time: 1999,
		velocity: 14.7069444444444,
		power: -1921.70899563847,
		road: 18225.6458796296,
		acceleration: -0.386759259259261
	},
	{
		id: 2001,
		time: 2000,
		velocity: 14.1577777777778,
		power: -1267.43457806591,
		road: 18240.0724074074,
		acceleration: -0.336759259259257
	},
	{
		id: 2002,
		time: 2001,
		velocity: 13.9097222222222,
		power: -1930.07729660543,
		road: 18254.1392129629,
		acceleration: -0.382685185185188
	},
	{
		id: 2003,
		time: 2002,
		velocity: 13.5588888888889,
		power: 811.989059818596,
		road: 18267.9281944444,
		acceleration: -0.172962962962961
	},
	{
		id: 2004,
		time: 2003,
		velocity: 13.6388888888889,
		power: 3021.40685972262,
		road: 18281.629537037,
		acceleration: -0.00231481481481666
	},
	{
		id: 2005,
		time: 2004,
		velocity: 13.9027777777778,
		power: 5464.98388387263,
		road: 18295.4199074074,
		acceleration: 0.180370370370371
	},
	{
		id: 2006,
		time: 2005,
		velocity: 14.1,
		power: 6952.03605748871,
		road: 18309.4409259259,
		acceleration: 0.280925925925926
	},
	{
		id: 2007,
		time: 2006,
		velocity: 14.4816666666667,
		power: 6891.59492615159,
		road: 18323.7335185185,
		acceleration: 0.262222222222222
	},
	{
		id: 2008,
		time: 2007,
		velocity: 14.6894444444444,
		power: 6462.47475170297,
		road: 18338.2667129629,
		acceleration: 0.218981481481483
	},
	{
		id: 2009,
		time: 2008,
		velocity: 14.7569444444444,
		power: 5006.79563603037,
		road: 18352.9631018518,
		acceleration: 0.107407407407408
	},
	{
		id: 2010,
		time: 2009,
		velocity: 14.8038888888889,
		power: 2473.37128839182,
		road: 18367.6764351852,
		acceleration: -0.0735185185185205
	},
	{
		id: 2011,
		time: 2010,
		velocity: 14.4688888888889,
		power: 931.205470065487,
		road: 18382.2627314815,
		acceleration: -0.180555555555555
	},
	{
		id: 2012,
		time: 2011,
		velocity: 14.2152777777778,
		power: 147.926803005146,
		road: 18396.6421296296,
		acceleration: -0.23324074074074
	},
	{
		id: 2013,
		time: 2012,
		velocity: 14.1041666666667,
		power: 1983.27703964664,
		road: 18410.8572685185,
		acceleration: -0.0952777777777793
	},
	{
		id: 2014,
		time: 2013,
		velocity: 14.1830555555556,
		power: 3012.33305302633,
		road: 18425.0159259259,
		acceleration: -0.0176851851851829
	},
	{
		id: 2015,
		time: 2014,
		velocity: 14.1622222222222,
		power: 3182.66386885557,
		road: 18439.1633796296,
		acceleration: -0.00472222222222385
	},
	{
		id: 2016,
		time: 2015,
		velocity: 14.09,
		power: 609.980885802852,
		road: 18453.2118518518,
		acceleration: -0.193240740740741
	},
	{
		id: 2017,
		time: 2016,
		velocity: 13.6033333333333,
		power: -308.273631565173,
		road: 18467.0344444444,
		acceleration: -0.258518518518517
	},
	{
		id: 2018,
		time: 2017,
		velocity: 13.3866666666667,
		power: -2269.76570822772,
		road: 18480.5246296296,
		acceleration: -0.406296296296297
	},
	{
		id: 2019,
		time: 2018,
		velocity: 12.8711111111111,
		power: -2432.98592834461,
		road: 18493.6022685185,
		acceleration: -0.418796296296296
	},
	{
		id: 2020,
		time: 2019,
		velocity: 12.3469444444444,
		power: -3160.25801124174,
		road: 18506.2306944444,
		acceleration: -0.479629629629629
	},
	{
		id: 2021,
		time: 2020,
		velocity: 11.9477777777778,
		power: -2789.78469158713,
		road: 18518.3936574074,
		acceleration: -0.451296296296293
	},
	{
		id: 2022,
		time: 2021,
		velocity: 11.5172222222222,
		power: -2061.52387087651,
		road: 18530.1363425926,
		acceleration: -0.38925925925926
	},
	{
		id: 2023,
		time: 2022,
		velocity: 11.1791666666667,
		power: -2161.64416994397,
		road: 18541.4844907407,
		acceleration: -0.399814814814816
	},
	{
		id: 2024,
		time: 2023,
		velocity: 10.7483333333333,
		power: -1425.40069427911,
		road: 18552.4669444444,
		acceleration: -0.331574074074076
	},
	{
		id: 2025,
		time: 2024,
		velocity: 10.5225,
		power: -1690.57603756328,
		road: 18563.1046296296,
		acceleration: -0.357962962962961
	},
	{
		id: 2026,
		time: 2025,
		velocity: 10.1052777777778,
		power: -559.439089314089,
		road: 18573.4411111111,
		acceleration: -0.244444444444444
	},
	{
		id: 2027,
		time: 2026,
		velocity: 10.015,
		power: 360.61674418549,
		road: 18583.5813425926,
		acceleration: -0.148055555555555
	},
	{
		id: 2028,
		time: 2027,
		velocity: 10.0783333333333,
		power: 2199.87472804725,
		road: 18593.6695833333,
		acceleration: 0.0440740740740715
	},
	{
		id: 2029,
		time: 2028,
		velocity: 10.2375,
		power: 2698.3877055716,
		road: 18603.8264814814,
		acceleration: 0.0932407407407396
	},
	{
		id: 2030,
		time: 2029,
		velocity: 10.2947222222222,
		power: 2446.50694070299,
		road: 18614.0621759259,
		acceleration: 0.0643518518518533
	},
	{
		id: 2031,
		time: 2030,
		velocity: 10.2713888888889,
		power: 1520.64080740598,
		road: 18624.314537037,
		acceleration: -0.0310185185185183
	},
	{
		id: 2032,
		time: 2031,
		velocity: 10.1444444444444,
		power: -2006.29841914514,
		road: 18634.3543981481,
		acceleration: -0.393981481481481
	},
	{
		id: 2033,
		time: 2032,
		velocity: 9.11277777777778,
		power: -6631.55787119744,
		road: 18643.7376851851,
		acceleration: -0.919166666666667
	},
	{
		id: 2034,
		time: 2033,
		velocity: 7.51388888888889,
		power: -10354.6181041532,
		road: 18651.9133796296,
		acceleration: -1.49601851851852
	},
	{
		id: 2035,
		time: 2034,
		velocity: 5.65638888888889,
		power: -8185.6154439448,
		road: 18658.6231481481,
		acceleration: -1.43583333333333
	},
	{
		id: 2036,
		time: 2035,
		velocity: 4.80527777777778,
		power: -7561.0564362315,
		road: 18663.7706944444,
		acceleration: -1.68861111111111
	},
	{
		id: 2037,
		time: 2036,
		velocity: 2.44805555555556,
		power: -3950.32364605176,
		road: 18667.4392129629,
		acceleration: -1.26944444444444
	},
	{
		id: 2038,
		time: 2037,
		velocity: 1.84805555555556,
		power: -2932.30304620297,
		road: 18669.7342129629,
		acceleration: -1.47759259259259
	},
	{
		id: 2039,
		time: 2038,
		velocity: 0.3725,
		power: -748.570943683404,
		road: 18670.8824074074,
		acceleration: -0.816018518518519
	},
	{
		id: 2040,
		time: 2039,
		velocity: 0,
		power: -199.998151284295,
		road: 18671.3145833333,
		acceleration: -0.616018518518519
	},
	{
		id: 2041,
		time: 2040,
		velocity: 0,
		power: 0.197817105263158,
		road: 18671.3766666666,
		acceleration: -0.124166666666667
	},
	{
		id: 2042,
		time: 2041,
		velocity: 0,
		power: 0,
		road: 18671.3766666666,
		acceleration: 0
	},
	{
		id: 2043,
		time: 2042,
		velocity: 0,
		power: 0,
		road: 18671.3766666666,
		acceleration: 0
	},
	{
		id: 2044,
		time: 2043,
		velocity: 0,
		power: 0,
		road: 18671.3766666666,
		acceleration: 0
	},
	{
		id: 2045,
		time: 2044,
		velocity: 0,
		power: 0,
		road: 18671.3766666666,
		acceleration: 0
	},
	{
		id: 2046,
		time: 2045,
		velocity: 0,
		power: 0,
		road: 18671.3766666666,
		acceleration: 0
	},
	{
		id: 2047,
		time: 2046,
		velocity: 0,
		power: 0,
		road: 18671.3766666666,
		acceleration: 0
	},
	{
		id: 2048,
		time: 2047,
		velocity: 0,
		power: 0,
		road: 18671.3766666666,
		acceleration: 0
	},
	{
		id: 2049,
		time: 2048,
		velocity: 0,
		power: 63.764922132792,
		road: 18671.5309722222,
		acceleration: 0.308611111111111
	},
	{
		id: 2050,
		time: 2049,
		velocity: 0.925833333333333,
		power: 1091.40780808881,
		road: 18672.4218055555,
		acceleration: 1.16444444444444
	},
	{
		id: 2051,
		time: 2050,
		velocity: 3.49333333333333,
		power: 3918.01036031102,
		road: 18674.725787037,
		acceleration: 1.66185185185185
	},
	{
		id: 2052,
		time: 2051,
		velocity: 4.98555555555556,
		power: 5567.70804817067,
		road: 18678.5578703703,
		acceleration: 1.39435185185185
	},
	{
		id: 2053,
		time: 2052,
		velocity: 5.10888888888889,
		power: 3041.76394770508,
		road: 18683.3509722222,
		acceleration: 0.527685185185185
	},
	{
		id: 2054,
		time: 2053,
		velocity: 5.07638888888889,
		power: 1870.02822230558,
		road: 18688.5268055555,
		acceleration: 0.237777777777778
	},
	{
		id: 2055,
		time: 2054,
		velocity: 5.69888888888889,
		power: 3323.43696104382,
		road: 18694.0649074074,
		acceleration: 0.48675925925926
	},
	{
		id: 2056,
		time: 2055,
		velocity: 6.56916666666667,
		power: 5168.62008367968,
		road: 18700.214074074,
		acceleration: 0.73537037037037
	},
	{
		id: 2057,
		time: 2056,
		velocity: 7.2825,
		power: 5497.047795549,
		road: 18707.0751388889,
		acceleration: 0.688425925925927
	},
	{
		id: 2058,
		time: 2057,
		velocity: 7.76416666666667,
		power: 3769.14557409006,
		road: 18714.4692129629,
		acceleration: 0.377592592592592
	},
	{
		id: 2059,
		time: 2058,
		velocity: 7.70194444444444,
		power: 2637.96006506877,
		road: 18722.1521759259,
		acceleration: 0.200185185185187
	},
	{
		id: 2060,
		time: 2059,
		velocity: 7.88305555555556,
		power: 1583.09120223024,
		road: 18729.9608796296,
		acceleration: 0.0512962962962957
	},
	{
		id: 2061,
		time: 2060,
		velocity: 7.91805555555556,
		power: 1779.83255250973,
		road: 18737.8328703703,
		acceleration: 0.075277777777778
	},
	{
		id: 2062,
		time: 2061,
		velocity: 7.92777777777778,
		power: 1352.41468506478,
		road: 18745.7508796296,
		acceleration: 0.016759259259258
	},
	{
		id: 2063,
		time: 2062,
		velocity: 7.93333333333333,
		power: 700.841276397216,
		road: 18753.6426851851,
		acceleration: -0.0691666666666659
	},
	{
		id: 2064,
		time: 2063,
		velocity: 7.71055555555555,
		power: -460.683289124514,
		road: 18761.3880555555,
		acceleration: -0.223703703703705
	},
	{
		id: 2065,
		time: 2064,
		velocity: 7.25666666666667,
		power: -1344.79062909676,
		road: 18768.8474537037,
		acceleration: -0.348240740740741
	},
	{
		id: 2066,
		time: 2065,
		velocity: 6.88861111111111,
		power: -1925.8446506793,
		road: 18775.9116666666,
		acceleration: -0.442129629629628
	},
	{
		id: 2067,
		time: 2066,
		velocity: 6.38416666666667,
		power: -1909.27003517895,
		road: 18782.527037037,
		acceleration: -0.455555555555556
	},
	{
		id: 2068,
		time: 2067,
		velocity: 5.89,
		power: -1942.13655849749,
		road: 18788.6741203703,
		acceleration: -0.481018518518519
	},
	{
		id: 2069,
		time: 2068,
		velocity: 5.44555555555556,
		power: -1458.29190468219,
		road: 18794.3732407407,
		acceleration: -0.414907407407407
	},
	{
		id: 2070,
		time: 2069,
		velocity: 5.13944444444444,
		power: -1584.50770221157,
		road: 18799.6349537037,
		acceleration: -0.459907407407407
	},
	{
		id: 2071,
		time: 2070,
		velocity: 4.51027777777778,
		power: -2510.42078836622,
		road: 18804.3144444444,
		acceleration: -0.704537037037038
	},
	{
		id: 2072,
		time: 2071,
		velocity: 3.33194444444444,
		power: -3273.82332482331,
		road: 18808.1208796296,
		acceleration: -1.04157407407407
	},
	{
		id: 2073,
		time: 2072,
		velocity: 2.01472222222222,
		power: -3298.56538796916,
		road: 18810.6548148148,
		acceleration: -1.50342592592593
	},
	{
		id: 2074,
		time: 2073,
		velocity: 0,
		power: -1142.40197541037,
		road: 18811.8817129629,
		acceleration: -1.11064814814815
	},
	{
		id: 2075,
		time: 2074,
		velocity: 0,
		power: -173.068055669266,
		road: 18812.2175,
		acceleration: -0.671574074074074
	},
	{
		id: 2076,
		time: 2075,
		velocity: 0,
		power: 0,
		road: 18812.2175,
		acceleration: 0
	},
	{
		id: 2077,
		time: 2076,
		velocity: 0,
		power: 0,
		road: 18812.2175,
		acceleration: 0
	},
	{
		id: 2078,
		time: 2077,
		velocity: 0,
		power: 0,
		road: 18812.2175,
		acceleration: 0
	},
	{
		id: 2079,
		time: 2078,
		velocity: 0,
		power: 0,
		road: 18812.2175,
		acceleration: 0
	},
	{
		id: 2080,
		time: 2079,
		velocity: 0,
		power: 0,
		road: 18812.2175,
		acceleration: 0
	},
	{
		id: 2081,
		time: 2080,
		velocity: 0,
		power: 41.0430843336552,
		road: 18812.3362037037,
		acceleration: 0.237407407407407
	},
	{
		id: 2082,
		time: 2081,
		velocity: 0.712222222222222,
		power: 946.102933129799,
		road: 18813.1346759259,
		acceleration: 1.12212962962963
	},
	{
		id: 2083,
		time: 2082,
		velocity: 3.36638888888889,
		power: 2175.85928853144,
		road: 18815.0335185185,
		acceleration: 1.07861111111111
	},
	{
		id: 2084,
		time: 2083,
		velocity: 3.23583333333333,
		power: 2366.54038001356,
		road: 18817.8486574074,
		acceleration: 0.753981481481481
	},
	{
		id: 2085,
		time: 2084,
		velocity: 2.97416666666667,
		power: -61.6337216715294,
		road: 18820.9639351851,
		acceleration: -0.153703703703703
	},
	{
		id: 2086,
		time: 2085,
		velocity: 2.90527777777778,
		power: -322.966654637871,
		road: 18823.8778703703,
		acceleration: -0.248981481481481
	},
	{
		id: 2087,
		time: 2086,
		velocity: 2.48888888888889,
		power: -220.25404723575,
		road: 18826.558287037,
		acceleration: -0.218055555555556
	},
	{
		id: 2088,
		time: 2087,
		velocity: 2.32,
		power: -182.242363517344,
		road: 18829.025324074,
		acceleration: -0.208703703703704
	},
	{
		id: 2089,
		time: 2088,
		velocity: 2.27916666666667,
		power: -20.3325270048179,
		road: 18831.3181481481,
		acceleration: -0.139722222222222
	},
	{
		id: 2090,
		time: 2089,
		velocity: 2.06972222222222,
		power: 51.4203593139915,
		road: 18833.4885648148,
		acceleration: -0.105092592592593
	},
	{
		id: 2091,
		time: 2090,
		velocity: 2.00472222222222,
		power: -235.931612920157,
		road: 18835.4791203703,
		acceleration: -0.25462962962963
	},
	{
		id: 2092,
		time: 2091,
		velocity: 1.51527777777778,
		power: -807.776105544709,
		road: 18836.9974074074,
		acceleration: -0.689907407407407
	},
	{
		id: 2093,
		time: 2092,
		velocity: 0,
		power: -429.772526357572,
		road: 18837.8366203703,
		acceleration: -0.668240740740741
	},
	{
		id: 2094,
		time: 2093,
		velocity: 0,
		power: -90.3335062540611,
		road: 18838.0891666666,
		acceleration: -0.505092592592593
	},
	{
		id: 2095,
		time: 2094,
		velocity: 0,
		power: 0,
		road: 18838.0891666666,
		acceleration: 0
	},
	{
		id: 2096,
		time: 2095,
		velocity: 0,
		power: 0,
		road: 18838.0891666666,
		acceleration: 0
	},
	{
		id: 2097,
		time: 2096,
		velocity: 0,
		power: 0,
		road: 18838.0891666666,
		acceleration: 0
	},
	{
		id: 2098,
		time: 2097,
		velocity: 0,
		power: 0,
		road: 18838.0891666666,
		acceleration: 0
	},
	{
		id: 2099,
		time: 2098,
		velocity: 0,
		power: 201.160646787464,
		road: 18838.3846296296,
		acceleration: 0.590925925925926
	},
	{
		id: 2100,
		time: 2099,
		velocity: 1.77277777777778,
		power: 1618.47271324267,
		road: 18839.6086574074,
		acceleration: 1.2662037037037
	},
	{
		id: 2101,
		time: 2100,
		velocity: 3.79861111111111,
		power: 3248.63010286125,
		road: 18842.0901851851,
		acceleration: 1.2487962962963
	},
	{
		id: 2102,
		time: 2101,
		velocity: 3.74638888888889,
		power: 2584.86975506292,
		road: 18845.5254166666,
		acceleration: 0.658611111111111
	},
	{
		id: 2103,
		time: 2102,
		velocity: 3.74861111111111,
		power: 256.777790146517,
		road: 18849.2585648148,
		acceleration: -0.0627777777777778
	},
	{
		id: 2104,
		time: 2103,
		velocity: 3.61027777777778,
		power: 688.279003330533,
		road: 18852.9898611111,
		acceleration: 0.0590740740740738
	},
	{
		id: 2105,
		time: 2104,
		velocity: 3.92361111111111,
		power: 1807.46213756612,
		road: 18856.924537037,
		acceleration: 0.347685185185186
	},
	{
		id: 2106,
		time: 2105,
		velocity: 4.79166666666667,
		power: 3370.97747977198,
		road: 18861.3635648148,
		acceleration: 0.661018518518518
	},
	{
		id: 2107,
		time: 2106,
		velocity: 5.59333333333333,
		power: 4514.8131266197,
		road: 18866.5224074074,
		acceleration: 0.778611111111111
	},
	{
		id: 2108,
		time: 2107,
		velocity: 6.25944444444444,
		power: 3685.61805054811,
		road: 18872.3311111111,
		acceleration: 0.521111111111111
	},
	{
		id: 2109,
		time: 2108,
		velocity: 6.355,
		power: 2511.03857276126,
		road: 18878.5385648148,
		acceleration: 0.276388888888889
	},
	{
		id: 2110,
		time: 2109,
		velocity: 6.4225,
		power: 1099.30788981058,
		road: 18884.9,
		acceleration: 0.031574074074074
	},
	{
		id: 2111,
		time: 2110,
		velocity: 6.35416666666667,
		power: 869.113216120162,
		road: 18891.2737962963,
		acceleration: -0.00685185185185233
	},
	{
		id: 2112,
		time: 2111,
		velocity: 6.33444444444444,
		power: -84.5505424995613,
		road: 18897.5622685185,
		acceleration: -0.163796296296296
	},
	{
		id: 2113,
		time: 2112,
		velocity: 5.93111111111111,
		power: -1025.0903001937,
		road: 18903.6056481481,
		acceleration: -0.326388888888889
	},
	{
		id: 2114,
		time: 2113,
		velocity: 5.375,
		power: -4497.27557592129,
		road: 18908.9731018518,
		acceleration: -1.02546296296296
	},
	{
		id: 2115,
		time: 2114,
		velocity: 3.25805555555556,
		power: -5348.7506492294,
		road: 18913.0721759259,
		acceleration: -1.5112962962963
	},
	{
		id: 2116,
		time: 2115,
		velocity: 1.39722222222222,
		power: -3855.55387554466,
		road: 18915.5197685185,
		acceleration: -1.79166666666667
	},
	{
		id: 2117,
		time: 2116,
		velocity: 0,
		power: -915.868332912964,
		road: 18916.5285185185,
		acceleration: -1.08601851851852
	},
	{
		id: 2118,
		time: 2117,
		velocity: 0,
		power: -74.6140362248213,
		road: 18916.7613888889,
		acceleration: -0.465740740740741
	},
	{
		id: 2119,
		time: 2118,
		velocity: 0,
		power: 0,
		road: 18916.7613888889,
		acceleration: 0
	},
	{
		id: 2120,
		time: 2119,
		velocity: 0,
		power: 0,
		road: 18916.7613888889,
		acceleration: 0
	},
	{
		id: 2121,
		time: 2120,
		velocity: 0,
		power: 0,
		road: 18916.7613888889,
		acceleration: 0
	},
	{
		id: 2122,
		time: 2121,
		velocity: 0,
		power: 0,
		road: 18916.7613888889,
		acceleration: 0
	},
	{
		id: 2123,
		time: 2122,
		velocity: 0,
		power: 0,
		road: 18916.7613888889,
		acceleration: 0
	},
	{
		id: 2124,
		time: 2123,
		velocity: 0,
		power: 0,
		road: 18916.7613888889,
		acceleration: 0
	},
	{
		id: 2125,
		time: 2124,
		velocity: 0,
		power: 0,
		road: 18916.7613888889,
		acceleration: 0
	},
	{
		id: 2126,
		time: 2125,
		velocity: 0,
		power: 0,
		road: 18916.7613888889,
		acceleration: 0
	},
	{
		id: 2127,
		time: 2126,
		velocity: 0,
		power: 319.846901077544,
		road: 18917.1415277777,
		acceleration: 0.760277777777778
	},
	{
		id: 2128,
		time: 2127,
		velocity: 2.28083333333333,
		power: 2246.13321816728,
		road: 18918.6319907407,
		acceleration: 1.46037037037037
	},
	{
		id: 2129,
		time: 2128,
		velocity: 4.38111111111111,
		power: 6537.27250122051,
		road: 18921.8544444444,
		acceleration: 2.00361111111111
	},
	{
		id: 2130,
		time: 2129,
		velocity: 6.01083333333333,
		power: 6754.24916054558,
		road: 18926.7363425926,
		acceleration: 1.31527777777778
	},
	{
		id: 2131,
		time: 2130,
		velocity: 6.22666666666667,
		power: 6923.00032779751,
		road: 18932.802037037,
		acceleration: 1.05231481481482
	},
	{
		id: 2132,
		time: 2131,
		velocity: 7.53805555555556,
		power: 7190.76582402958,
		road: 18939.8522685185,
		acceleration: 0.916759259259258
	},
	{
		id: 2133,
		time: 2132,
		velocity: 8.76111111111111,
		power: 11841.0004125105,
		road: 18948.0380555555,
		acceleration: 1.35435185185185
	},
	{
		id: 2134,
		time: 2133,
		velocity: 10.2897222222222,
		power: 19138.0391116344,
		road: 18957.8352314814,
		acceleration: 1.86842592592593
	},
	{
		id: 2135,
		time: 2134,
		velocity: 13.1433333333333,
		power: 19181.2423205312,
		road: 18969.3396296296,
		acceleration: 1.54601851851852
	},
	{
		id: 2136,
		time: 2135,
		velocity: 13.3991666666667,
		power: 20904.0494212627,
		road: 18982.3473611111,
		acceleration: 1.46064814814815
	},
	{
		id: 2137,
		time: 2136,
		velocity: 14.6716666666667,
		power: 20418.9898274899,
		road: 18996.7077777777,
		acceleration: 1.24472222222222
	},
	{
		id: 2138,
		time: 2137,
		velocity: 16.8775,
		power: 30749.4063303415,
		road: 19012.5696296296,
		acceleration: 1.75814814814815
	},
	{
		id: 2139,
		time: 2138,
		velocity: 18.6736111111111,
		power: 28230.8737529491,
		road: 19030.007037037,
		acceleration: 1.39296296296297
	},
	{
		id: 2140,
		time: 2139,
		velocity: 18.8505555555556,
		power: 23639.0374620289,
		road: 19048.6417592592,
		acceleration: 1.00166666666667
	},
	{
		id: 2141,
		time: 2140,
		velocity: 19.8825,
		power: 21916.960515206,
		road: 19068.1910648148,
		acceleration: 0.827500000000001
	},
	{
		id: 2142,
		time: 2141,
		velocity: 21.1561111111111,
		power: 30173.3373974989,
		road: 19088.7375,
		acceleration: 1.16675925925926
	},
	{
		id: 2143,
		time: 2142,
		velocity: 22.3508333333333,
		power: 27844.5963347595,
		road: 19110.3439814814,
		acceleration: 0.953333333333337
	},
	{
		id: 2144,
		time: 2143,
		velocity: 22.7425,
		power: 16075.0505065,
		road: 19132.6004166666,
		acceleration: 0.34657407407407
	},
	{
		id: 2145,
		time: 2144,
		velocity: 22.1958333333333,
		power: 4749.90657747253,
		road: 19154.9368518518,
		acceleration: -0.18657407407407
	},
	{
		id: 2146,
		time: 2145,
		velocity: 21.7911111111111,
		power: -2650.42904088988,
		road: 19176.9174537037,
		acceleration: -0.525092592592596
	},
	{
		id: 2147,
		time: 2146,
		velocity: 21.1672222222222,
		power: -2164.92046085457,
		road: 19198.3894444444,
		acceleration: -0.49212962962963
	},
	{
		id: 2148,
		time: 2147,
		velocity: 20.7194444444444,
		power: -1709.29339188715,
		road: 19219.3850925926,
		acceleration: -0.460555555555551
	},
	{
		id: 2149,
		time: 2148,
		velocity: 20.4094444444444,
		power: -923.390811872273,
		road: 19239.9443055555,
		acceleration: -0.412314814814817
	},
	{
		id: 2150,
		time: 2149,
		velocity: 19.9302777777778,
		power: -566.339964107823,
		road: 19260.104537037,
		acceleration: -0.38564814814815
	},
	{
		id: 2151,
		time: 2150,
		velocity: 19.5625,
		power: -1213.25547348816,
		road: 19279.8662037037,
		acceleration: -0.411481481481484
	},
	{
		id: 2152,
		time: 2151,
		velocity: 19.175,
		power: -1929.62597178821,
		road: 19299.2010185185,
		acceleration: -0.44222222222222
	},
	{
		id: 2153,
		time: 2152,
		velocity: 18.6036111111111,
		power: -7164.85501511135,
		road: 19317.9524074074,
		acceleration: -0.724629629629629
	},
	{
		id: 2154,
		time: 2153,
		velocity: 17.3886111111111,
		power: -7661.56055942237,
		road: 19335.9641203703,
		acceleration: -0.754722222222224
	},
	{
		id: 2155,
		time: 2154,
		velocity: 16.9108333333333,
		power: -3808.52609249095,
		road: 19353.3351388889,
		acceleration: -0.526666666666667
	},
	{
		id: 2156,
		time: 2155,
		velocity: 17.0236111111111,
		power: 8772.29135900978,
		road: 19370.5616203703,
		acceleration: 0.237592592592595
	},
	{
		id: 2157,
		time: 2156,
		velocity: 18.1013888888889,
		power: 15573.9446371421,
		road: 19388.21625,
		acceleration: 0.618703703703705
	},
	{
		id: 2158,
		time: 2157,
		velocity: 18.7669444444444,
		power: 18472.3873692051,
		road: 19406.5481018518,
		acceleration: 0.735740740740741
	},
	{
		id: 2159,
		time: 2158,
		velocity: 19.2308333333333,
		power: 10221.0474455643,
		road: 19425.3679166666,
		acceleration: 0.240185185185183
	},
	{
		id: 2160,
		time: 2159,
		velocity: 18.8219444444444,
		power: 3005.34927277863,
		road: 19444.2271296296,
		acceleration: -0.161388888888887
	},
	{
		id: 2161,
		time: 2160,
		velocity: 18.2827777777778,
		power: -2340.77313952379,
		road: 19462.779074074,
		acceleration: -0.453148148148152
	},
	{
		id: 2162,
		time: 2161,
		velocity: 17.8713888888889,
		power: -3996.61043874878,
		road: 19480.8332407407,
		acceleration: -0.542407407407403
	},
	{
		id: 2163,
		time: 2162,
		velocity: 17.1947222222222,
		power: -3927.98037109769,
		road: 19498.3488425926,
		acceleration: -0.534722222222225
	},
	{
		id: 2164,
		time: 2163,
		velocity: 16.6786111111111,
		power: -3746.27458330678,
		road: 19515.3367592592,
		acceleration: -0.520648148148151
	},
	{
		id: 2165,
		time: 2164,
		velocity: 16.3094444444444,
		power: -2807.12916859112,
		road: 19531.8349537037,
		acceleration: -0.458796296296292
	},
	{
		id: 2166,
		time: 2165,
		velocity: 15.8183333333333,
		power: -1855.34676432182,
		road: 19547.9068518518,
		acceleration: -0.393796296296298
	},
	{
		id: 2167,
		time: 2166,
		velocity: 15.4972222222222,
		power: -2137.91761718538,
		road: 19563.5775462963,
		acceleration: -0.408611111111114
	},
	{
		id: 2168,
		time: 2167,
		velocity: 15.0836111111111,
		power: -1874.93733958289,
		road: 19578.8502314814,
		acceleration: -0.387407407407407
	},
	{
		id: 2169,
		time: 2168,
		velocity: 14.6561111111111,
		power: -2756.87004864954,
		road: 19593.7061574074,
		acceleration: -0.44611111111111
	},
	{
		id: 2170,
		time: 2169,
		velocity: 14.1588888888889,
		power: -2789.96488524532,
		road: 19608.1154166666,
		acceleration: -0.447222222222223
	},
	{
		id: 2171,
		time: 2170,
		velocity: 13.7419444444444,
		power: -1769.06080733125,
		road: 19622.1159259259,
		acceleration: -0.370277777777778
	},
	{
		id: 2172,
		time: 2171,
		velocity: 13.5452777777778,
		power: -2683.74953306688,
		road: 19635.7120833333,
		acceleration: -0.438425925925927
	},
	{
		id: 2173,
		time: 2172,
		velocity: 12.8436111111111,
		power: -3324.32795768639,
		road: 19648.844074074,
		acceleration: -0.489907407407404
	},
	{
		id: 2174,
		time: 2173,
		velocity: 12.2722222222222,
		power: -4737.96550001924,
		road: 19661.4254166666,
		acceleration: -0.611388888888889
	},
	{
		id: 2175,
		time: 2174,
		velocity: 11.7111111111111,
		power: -3932.59114035985,
		road: 19673.4249537037,
		acceleration: -0.552222222222223
	},
	{
		id: 2176,
		time: 2175,
		velocity: 11.1869444444444,
		power: -3846.20363663944,
		road: 19684.8715277777,
		acceleration: -0.553703703703704
	},
	{
		id: 2177,
		time: 2176,
		velocity: 10.6111111111111,
		power: -3002.27674573334,
		road: 19695.7997685185,
		acceleration: -0.482962962962961
	},
	{
		id: 2178,
		time: 2177,
		velocity: 10.2622222222222,
		power: -4084.48757575863,
		road: 19706.1860648148,
		acceleration: -0.600925925925926
	},
	{
		id: 2179,
		time: 2178,
		velocity: 9.38416666666667,
		power: -3780.61944143871,
		road: 19715.9785185185,
		acceleration: -0.58675925925926
	},
	{
		id: 2180,
		time: 2179,
		velocity: 8.85083333333333,
		power: -3656.71412501828,
		road: 19725.1813888888,
		acceleration: -0.592407407407407
	},
	{
		id: 2181,
		time: 2180,
		velocity: 8.485,
		power: -1424.30559546491,
		road: 19733.9172222222,
		acceleration: -0.341666666666667
	},
	{
		id: 2182,
		time: 2181,
		velocity: 8.35916666666667,
		power: 2755.0738179712,
		road: 19742.5647685185,
		acceleration: 0.165092592592593
	},
	{
		id: 2183,
		time: 2182,
		velocity: 9.34611111111111,
		power: 7331.96457706729,
		road: 19751.6325,
		acceleration: 0.675277777777778
	},
	{
		id: 2184,
		time: 2183,
		velocity: 10.5108333333333,
		power: 10089.0509772895,
		road: 19761.4842129629,
		acceleration: 0.892685185185186
	},
	{
		id: 2185,
		time: 2184,
		velocity: 11.0372222222222,
		power: 6236.61161924032,
		road: 19771.9985648148,
		acceleration: 0.432592592592592
	},
	{
		id: 2186,
		time: 2185,
		velocity: 10.6438888888889,
		power: 617.147111491642,
		road: 19782.6637962962,
		acceleration: -0.130833333333333
	},
	{
		id: 2187,
		time: 2186,
		velocity: 10.1183333333333,
		power: -2574.71715229352,
		road: 19793.0393518518,
		acceleration: -0.448518518518519
	},
	{
		id: 2188,
		time: 2187,
		velocity: 9.69166666666667,
		power: -2770.79483404133,
		road: 19802.9525462962,
		acceleration: -0.476203703703705
	},
	{
		id: 2189,
		time: 2188,
		velocity: 9.21527777777778,
		power: -2379.35120573672,
		road: 19812.4066203703,
		acceleration: -0.442037037037036
	},
	{
		id: 2190,
		time: 2189,
		velocity: 8.79222222222222,
		power: -2549.75750271547,
		road: 19821.4043518518,
		acceleration: -0.470648148148149
	},
	{
		id: 2191,
		time: 2190,
		velocity: 8.27972222222222,
		power: -1993.54281206345,
		road: 19829.9600462962,
		acceleration: -0.413425925925926
	},
	{
		id: 2192,
		time: 2191,
		velocity: 7.975,
		power: -2570.14118349646,
		road: 19838.0601388888,
		acceleration: -0.497777777777777
	},
	{
		id: 2193,
		time: 2192,
		velocity: 7.29888888888889,
		power: -3425.07100821208,
		road: 19845.5926851851,
		acceleration: -0.637314814814815
	},
	{
		id: 2194,
		time: 2193,
		velocity: 6.36777777777778,
		power: -6159.67273631934,
		road: 19852.2433796296,
		acceleration: -1.12638888888889
	},
	{
		id: 2195,
		time: 2194,
		velocity: 4.59583333333333,
		power: -6500.77814787351,
		road: 19857.6231018518,
		acceleration: -1.41555555555555
	},
	{
		id: 2196,
		time: 2195,
		velocity: 3.05222222222222,
		power: -6020.65937683267,
		road: 19861.384074074,
		acceleration: -1.82194444444444
	},
	{
		id: 2197,
		time: 2196,
		velocity: 0.901944444444444,
		power: -2770.82909737898,
		road: 19863.4681018518,
		acceleration: -1.53194444444444
	},
	{
		id: 2198,
		time: 2197,
		velocity: 0,
		power: -682.277690454834,
		road: 19864.2774537037,
		acceleration: -1.01740740740741
	},
	{
		id: 2199,
		time: 2198,
		velocity: 0,
		power: -24.654150308642,
		road: 19864.4277777777,
		acceleration: -0.300648148148148
	},
	{
		id: 2200,
		time: 2199,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2201,
		time: 2200,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2202,
		time: 2201,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2203,
		time: 2202,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2204,
		time: 2203,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2205,
		time: 2204,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2206,
		time: 2205,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2207,
		time: 2206,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2208,
		time: 2207,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2209,
		time: 2208,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2210,
		time: 2209,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2211,
		time: 2210,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2212,
		time: 2211,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2213,
		time: 2212,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2214,
		time: 2213,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2215,
		time: 2214,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2216,
		time: 2215,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2217,
		time: 2216,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2218,
		time: 2217,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2219,
		time: 2218,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2220,
		time: 2219,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2221,
		time: 2220,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2222,
		time: 2221,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2223,
		time: 2222,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2224,
		time: 2223,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2225,
		time: 2224,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2226,
		time: 2225,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2227,
		time: 2226,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2228,
		time: 2227,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2229,
		time: 2228,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2230,
		time: 2229,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2231,
		time: 2230,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2232,
		time: 2231,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2233,
		time: 2232,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2234,
		time: 2233,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2235,
		time: 2234,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2236,
		time: 2235,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2237,
		time: 2236,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2238,
		time: 2237,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2239,
		time: 2238,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2240,
		time: 2239,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2241,
		time: 2240,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2242,
		time: 2241,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2243,
		time: 2242,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2244,
		time: 2243,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2245,
		time: 2244,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2246,
		time: 2245,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2247,
		time: 2246,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2248,
		time: 2247,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2249,
		time: 2248,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2250,
		time: 2249,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2251,
		time: 2250,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2252,
		time: 2251,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2253,
		time: 2252,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2254,
		time: 2253,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2255,
		time: 2254,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2256,
		time: 2255,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2257,
		time: 2256,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2258,
		time: 2257,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2259,
		time: 2258,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2260,
		time: 2259,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2261,
		time: 2260,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2262,
		time: 2261,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2263,
		time: 2262,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2264,
		time: 2263,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2265,
		time: 2264,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2266,
		time: 2265,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2267,
		time: 2266,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2268,
		time: 2267,
		velocity: 0,
		power: 0,
		road: 19864.4277777777,
		acceleration: 0
	},
	{
		id: 2269,
		time: 2268,
		velocity: 0,
		power: 99.6857711890518,
		road: 19864.6274537037,
		acceleration: 0.399351851851852
	},
	{
		id: 2270,
		time: 2269,
		velocity: 1.19805555555556,
		power: 1117.57319290308,
		road: 19865.5809722222,
		acceleration: 1.10833333333333
	},
	{
		id: 2271,
		time: 2270,
		velocity: 3.325,
		power: 3527.35816588311,
		road: 19867.8446759259,
		acceleration: 1.51203703703704
	},
	{
		id: 2272,
		time: 2271,
		velocity: 4.53611111111111,
		power: 5694.37303585838,
		road: 19871.5960185185,
		acceleration: 1.46324074074074
	},
	{
		id: 2273,
		time: 2272,
		velocity: 5.58777777777778,
		power: 4319.40120990264,
		road: 19876.474537037,
		acceleration: 0.791111111111112
	},
	{
		id: 2274,
		time: 2273,
		velocity: 5.69833333333333,
		power: 3824.6966865684,
		road: 19882.0378703703,
		acceleration: 0.578518518518518
	},
	{
		id: 2275,
		time: 2274,
		velocity: 6.27166666666667,
		power: 3548.24205482005,
		road: 19888.1230092592,
		acceleration: 0.465092592592592
	},
	{
		id: 2276,
		time: 2275,
		velocity: 6.98305555555556,
		power: 4465.55779263444,
		road: 19894.7206018518,
		acceleration: 0.559814814814816
	},
	{
		id: 2277,
		time: 2276,
		velocity: 7.37777777777778,
		power: 5472.50163279324,
		road: 19901.9193518518,
		acceleration: 0.642499999999999
	},
	{
		id: 2278,
		time: 2277,
		velocity: 8.19916666666667,
		power: 6649.17111109389,
		road: 19909.8013425925,
		acceleration: 0.723981481481481
	},
	{
		id: 2279,
		time: 2278,
		velocity: 9.155,
		power: 7473.07984240184,
		road: 19918.41625,
		acceleration: 0.741851851851854
	},
	{
		id: 2280,
		time: 2279,
		velocity: 9.60333333333333,
		power: 6834.20941334155,
		road: 19927.7005092592,
		acceleration: 0.59685185185185
	},
	{
		id: 2281,
		time: 2280,
		velocity: 9.98972222222222,
		power: 6089.01081737795,
		road: 19937.517824074,
		acceleration: 0.46925925925926
	},
	{
		id: 2282,
		time: 2281,
		velocity: 10.5627777777778,
		power: 5417.54673154183,
		road: 19947.7542129629,
		acceleration: 0.368888888888888
	},
	{
		id: 2283,
		time: 2282,
		velocity: 10.71,
		power: 3089.01513370719,
		road: 19958.235,
		acceleration: 0.119907407407409
	},
	{
		id: 2284,
		time: 2283,
		velocity: 10.3494444444444,
		power: -863.760963081182,
		road: 19968.6379629629,
		acceleration: -0.275555555555554
	},
	{
		id: 2285,
		time: 2284,
		velocity: 9.73611111111111,
		power: -2774.7815213271,
		road: 19978.6658796296,
		acceleration: -0.474537037037038
	},
	{
		id: 2286,
		time: 2285,
		velocity: 9.28638888888889,
		power: -5022.95423075791,
		road: 19988.0879166666,
		acceleration: -0.737222222222224
	},
	{
		id: 2287,
		time: 2286,
		velocity: 8.13777777777778,
		power: -5263.012445766,
		road: 19996.7369444444,
		acceleration: -0.808796296296295
	},
	{
		id: 2288,
		time: 2287,
		velocity: 7.30972222222222,
		power: -5499.60128829185,
		road: 20004.5299074074,
		acceleration: -0.903333333333333
	},
	{
		id: 2289,
		time: 2288,
		velocity: 6.57638888888889,
		power: -3544.00098481258,
		road: 20011.5274537037,
		acceleration: -0.6875
	},
	{
		id: 2290,
		time: 2289,
		velocity: 6.07527777777778,
		power: -2795.98694949909,
		road: 20017.874537037,
		acceleration: -0.613425925925926
	},
	{
		id: 2291,
		time: 2290,
		velocity: 5.46944444444445,
		power: -2252.42003272786,
		road: 20023.6362037037,
		acceleration: -0.557407407407409
	},
	{
		id: 2292,
		time: 2291,
		velocity: 4.90416666666667,
		power: -2321.77718966618,
		road: 20028.8118518518,
		acceleration: -0.614629629629629
	},
	{
		id: 2293,
		time: 2292,
		velocity: 4.23138888888889,
		power: -2329.5996592806,
		road: 20033.3398611111,
		acceleration: -0.680648148148148
	},
	{
		id: 2294,
		time: 2293,
		velocity: 3.4275,
		power: -2461.04399514357,
		road: 20037.1166203703,
		acceleration: -0.821851851851852
	},
	{
		id: 2295,
		time: 2294,
		velocity: 2.43861111111111,
		power: -2423.65241220975,
		road: 20039.9686111111,
		acceleration: -1.02768518518518
	},
	{
		id: 2296,
		time: 2295,
		velocity: 1.14833333333333,
		power: -1697.59060083562,
		road: 20041.7355092592,
		acceleration: -1.1425
	},
	{
		id: 2297,
		time: 2296,
		velocity: 0,
		power: -512.349387411342,
		road: 20042.5247222222,
		acceleration: -0.81287037037037
	},
	{
		id: 2298,
		time: 2297,
		velocity: 0,
		power: -46.2804523391813,
		road: 20042.7161111111,
		acceleration: -0.382777777777778
	},
	{
		id: 2299,
		time: 2298,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2300,
		time: 2299,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2301,
		time: 2300,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2302,
		time: 2301,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2303,
		time: 2302,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2304,
		time: 2303,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2305,
		time: 2304,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2306,
		time: 2305,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2307,
		time: 2306,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2308,
		time: 2307,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2309,
		time: 2308,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2310,
		time: 2309,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2311,
		time: 2310,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2312,
		time: 2311,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2313,
		time: 2312,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2314,
		time: 2313,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2315,
		time: 2314,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2316,
		time: 2315,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2317,
		time: 2316,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2318,
		time: 2317,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2319,
		time: 2318,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2320,
		time: 2319,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2321,
		time: 2320,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2322,
		time: 2321,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2323,
		time: 2322,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2324,
		time: 2323,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2325,
		time: 2324,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2326,
		time: 2325,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2327,
		time: 2326,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2328,
		time: 2327,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2329,
		time: 2328,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2330,
		time: 2329,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2331,
		time: 2330,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2332,
		time: 2331,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2333,
		time: 2332,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2334,
		time: 2333,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2335,
		time: 2334,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2336,
		time: 2335,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2337,
		time: 2336,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2338,
		time: 2337,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2339,
		time: 2338,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2340,
		time: 2339,
		velocity: 0,
		power: 0,
		road: 20042.7161111111,
		acceleration: 0
	},
	{
		id: 2341,
		time: 2340,
		velocity: 0,
		power: 153.395971031796,
		road: 20042.9705092592,
		acceleration: 0.508796296296296
	},
	{
		id: 2342,
		time: 2341,
		velocity: 1.52638888888889,
		power: 1441.28271443417,
		road: 20044.0925925925,
		acceleration: 1.22657407407407
	},
	{
		id: 2343,
		time: 2342,
		velocity: 3.67972222222222,
		power: 4266.95207217386,
		road: 20046.6437037037,
		acceleration: 1.63148148148148
	},
	{
		id: 2344,
		time: 2343,
		velocity: 4.89444444444444,
		power: 4954.76191795645,
		road: 20050.601574074,
		acceleration: 1.18203703703704
	},
	{
		id: 2345,
		time: 2344,
		velocity: 5.0725,
		power: 2875.59661847085,
		road: 20055.3959722222,
		acceleration: 0.491018518518519
	},
	{
		id: 2346,
		time: 2345,
		velocity: 5.15277777777778,
		power: 2684.42574115951,
		road: 20060.6341203703,
		acceleration: 0.396481481481481
	},
	{
		id: 2347,
		time: 2346,
		velocity: 6.08388888888889,
		power: 4285.30099938225,
		road: 20066.3891666666,
		acceleration: 0.637314814814815
	},
	{
		id: 2348,
		time: 2347,
		velocity: 6.98444444444444,
		power: 4684.5021155307,
		road: 20072.7735185185,
		acceleration: 0.621296296296297
	},
	{
		id: 2349,
		time: 2348,
		velocity: 7.01666666666667,
		power: 2448.03460558937,
		road: 20079.5808333333,
		acceleration: 0.22462962962963
	},
	{
		id: 2350,
		time: 2349,
		velocity: 6.75777777777778,
		power: 607.563908540255,
		road: 20086.4697685185,
		acceleration: -0.0613888888888896
	},
	{
		id: 2351,
		time: 2350,
		velocity: 6.80027777777778,
		power: 905.72660995382,
		road: 20093.3206018518,
		acceleration: -0.0148148148148151
	},
	{
		id: 2352,
		time: 2351,
		velocity: 6.97222222222222,
		power: 3665.67518829074,
		road: 20100.3600925925,
		acceleration: 0.392129629629629
	},
	{
		id: 2353,
		time: 2352,
		velocity: 7.93416666666667,
		power: 2217.88119128438,
		road: 20107.6762037037,
		acceleration: 0.161111111111111
	},
	{
		id: 2354,
		time: 2353,
		velocity: 7.28361111111111,
		power: 2816.27310367908,
		road: 20115.1902314814,
		acceleration: 0.234722222222223
	},
	{
		id: 2355,
		time: 2354,
		velocity: 7.67638888888889,
		power: 2189.82408405788,
		road: 20122.8906481481,
		acceleration: 0.138055555555556
	},
	{
		id: 2356,
		time: 2355,
		velocity: 8.34833333333333,
		power: 7058.23719140099,
		road: 20131.0330555555,
		acceleration: 0.745925925925925
	},
	{
		id: 2357,
		time: 2356,
		velocity: 9.52138888888889,
		power: 7815.22946172448,
		road: 20139.9239814814,
		acceleration: 0.751111111111111
	},
	{
		id: 2358,
		time: 2357,
		velocity: 9.92972222222222,
		power: 5228.52132922875,
		road: 20149.3913888888,
		acceleration: 0.401851851851852
	},
	{
		id: 2359,
		time: 2358,
		velocity: 9.55388888888889,
		power: 1982.98933207692,
		road: 20159.077037037,
		acceleration: 0.0346296296296309
	},
	{
		id: 2360,
		time: 2359,
		velocity: 9.62527777777778,
		power: 236.980712996536,
		road: 20168.7031018518,
		acceleration: -0.153796296296298
	},
	{
		id: 2361,
		time: 2360,
		velocity: 9.46833333333333,
		power: 788.566060416638,
		road: 20178.2066666666,
		acceleration: -0.0912037037037035
	},
	{
		id: 2362,
		time: 2361,
		velocity: 9.28027777777778,
		power: 1065.30012796886,
		road: 20187.6351851851,
		acceleration: -0.0588888888888892
	},
	{
		id: 2363,
		time: 2362,
		velocity: 9.44861111111111,
		power: 4613.48878586247,
		road: 20197.1980092592,
		acceleration: 0.327499999999999
	},
	{
		id: 2364,
		time: 2363,
		velocity: 10.4508333333333,
		power: 5697.77483875405,
		road: 20207.1339814814,
		acceleration: 0.418796296296298
	},
	{
		id: 2365,
		time: 2364,
		velocity: 10.5366666666667,
		power: 5775.91887104825,
		road: 20217.4784259259,
		acceleration: 0.398148148148147
	},
	{
		id: 2366,
		time: 2365,
		velocity: 10.6430555555556,
		power: 1523.61417424751,
		road: 20228.0029629629,
		acceleration: -0.037962962962963
	},
	{
		id: 2367,
		time: 2366,
		velocity: 10.3369444444444,
		power: 408.377968821622,
		road: 20238.4346759259,
		acceleration: -0.147685185185184
	},
	{
		id: 2368,
		time: 2367,
		velocity: 10.0936111111111,
		power: -1895.83152858444,
		road: 20248.6018518518,
		acceleration: -0.381388888888889
	},
	{
		id: 2369,
		time: 2368,
		velocity: 9.49888888888889,
		power: -1170.1668013473,
		road: 20258.4249074074,
		acceleration: -0.306851851851851
	},
	{
		id: 2370,
		time: 2369,
		velocity: 9.41638888888889,
		power: 636.665543556922,
		road: 20268.039537037,
		acceleration: -0.110000000000001
	},
	{
		id: 2371,
		time: 2370,
		velocity: 9.76361111111111,
		power: 3742.58958845179,
		road: 20277.7121759259,
		acceleration: 0.226018518518519
	},
	{
		id: 2372,
		time: 2371,
		velocity: 10.1769444444444,
		power: 5071.65150130881,
		road: 20287.6733333333,
		acceleration: 0.351018518518517
	},
	{
		id: 2373,
		time: 2372,
		velocity: 10.4694444444444,
		power: 3167.44674006707,
		road: 20297.8797685185,
		acceleration: 0.139537037037037
	},
	{
		id: 2374,
		time: 2373,
		velocity: 10.1822222222222,
		power: -516.013584897365,
		road: 20308.0365277777,
		acceleration: -0.238888888888889
	},
	{
		id: 2375,
		time: 2374,
		velocity: 9.46027777777778,
		power: -2999.34577895823,
		road: 20317.8222685185,
		acceleration: -0.503148148148147
	},
	{
		id: 2376,
		time: 2375,
		velocity: 8.96,
		power: -1907.31121956764,
		road: 20327.1609259259,
		acceleration: -0.391018518518518
	},
	{
		id: 2377,
		time: 2376,
		velocity: 9.00916666666667,
		power: 1704.70318232941,
		road: 20336.314537037,
		acceleration: 0.0209259259259262
	},
	{
		id: 2378,
		time: 2377,
		velocity: 9.52305555555556,
		power: 3355.78349633442,
		road: 20345.5808333333,
		acceleration: 0.204444444444444
	},
	{
		id: 2379,
		time: 2378,
		velocity: 9.57333333333333,
		power: -751.468996136003,
		road: 20354.8188888888,
		acceleration: -0.260925925925925
	},
	{
		id: 2380,
		time: 2379,
		velocity: 8.22638888888889,
		power: -5910.35343372809,
		road: 20363.4833333333,
		acceleration: -0.886296296296299
	},
	{
		id: 2381,
		time: 2380,
		velocity: 6.86416666666667,
		power: -9642.31944552693,
		road: 20370.9460648148,
		acceleration: -1.51712962962963
	},
	{
		id: 2382,
		time: 2381,
		velocity: 5.02194444444444,
		power: -6068.56796891101,
		road: 20377.0533333333,
		acceleration: -1.1937962962963
	},
	{
		id: 2383,
		time: 2382,
		velocity: 4.645,
		power: -1179.98557826073,
		road: 20382.3753703703,
		acceleration: -0.376666666666667
	},
	{
		id: 2384,
		time: 2383,
		velocity: 5.73416666666667,
		power: 5877.8631884903,
		road: 20387.9874537037,
		acceleration: 0.95675925925926
	},
	{
		id: 2385,
		time: 2384,
		velocity: 7.89222222222222,
		power: 10320.0277805721,
		road: 20394.7976851851,
		acceleration: 1.43953703703704
	},
	{
		id: 2386,
		time: 2385,
		velocity: 8.96361111111111,
		power: 19475.5450271227,
		road: 20403.427824074,
		acceleration: 2.20027777777778
	},
	{
		id: 2387,
		time: 2386,
		velocity: 12.335,
		power: 20647.6304929836,
		road: 20414.079074074,
		acceleration: 1.84194444444445
	},
	{
		id: 2388,
		time: 2387,
		velocity: 13.4180555555556,
		power: 19930.2369820313,
		road: 20426.3929166666,
		acceleration: 1.48324074074074
	},
	{
		id: 2389,
		time: 2388,
		velocity: 13.4133333333333,
		power: 5923.52102739144,
		road: 20439.5711574074,
		acceleration: 0.245555555555555
	},
	{
		id: 2390,
		time: 2389,
		velocity: 13.0716666666667,
		power: -2615.68857416932,
		road: 20452.6554629629,
		acceleration: -0.433425925925926
	},
	{
		id: 2391,
		time: 2390,
		velocity: 12.1177777777778,
		power: -7008.05931128911,
		road: 20465.1208796296,
		acceleration: -0.804351851851852
	},
	{
		id: 2392,
		time: 2391,
		velocity: 11.0002777777778,
		power: -11707.4549975968,
		road: 20476.5462037037,
		acceleration: -1.27583333333333
	},
	{
		id: 2393,
		time: 2392,
		velocity: 9.24416666666667,
		power: -13725.0817890152,
		road: 20486.5194907407,
		acceleration: -1.62824074074074
	},
	{
		id: 2394,
		time: 2393,
		velocity: 7.23305555555556,
		power: -13974.9017473404,
		road: 20494.6982407407,
		acceleration: -1.96083333333333
	},
	{
		id: 2395,
		time: 2394,
		velocity: 5.11777777777778,
		power: -11703.2916904463,
		road: 20500.8158796296,
		acceleration: -2.16138888888889
	},
	{
		id: 2396,
		time: 2395,
		velocity: 2.76,
		power: -8274.22012885594,
		road: 20504.6473148148,
		acceleration: -2.41101851851852
	},
	{
		id: 2397,
		time: 2396,
		velocity: 0,
		power: -2650.33668144895,
		road: 20506.4202777777,
		acceleration: -1.70592592592593
	},
	{
		id: 2398,
		time: 2397,
		velocity: 0,
		power: -344.253809776461,
		road: 20506.8883333333,
		acceleration: -0.903888888888889
	},
	{
		id: 2399,
		time: 2398,
		velocity: 0.0483333333333333,
		power: 1.94651279656086,
		road: 20506.9044444444,
		acceleration: 0
	},
	{
		id: 2400,
		time: 2399,
		velocity: 0,
		power: 1.94651279656086,
		road: 20506.9205555555,
		acceleration: 0
	},
	{
		id: 2401,
		time: 2400,
		velocity: 0,
		power: 0.850302046783626,
		road: 20506.9286111111,
		acceleration: -0.0161111111111111
	},
	{
		id: 2402,
		time: 2401,
		velocity: 0,
		power: 0,
		road: 20506.9286111111,
		acceleration: 0
	},
	{
		id: 2403,
		time: 2402,
		velocity: 0,
		power: 0,
		road: 20506.9286111111,
		acceleration: 0
	},
	{
		id: 2404,
		time: 2403,
		velocity: 0,
		power: 0,
		road: 20506.9286111111,
		acceleration: 0
	},
	{
		id: 2405,
		time: 2404,
		velocity: 0,
		power: 0,
		road: 20506.9286111111,
		acceleration: 0
	},
	{
		id: 2406,
		time: 2405,
		velocity: 0,
		power: 0,
		road: 20506.9286111111,
		acceleration: 0
	},
	{
		id: 2407,
		time: 2406,
		velocity: 0,
		power: 0,
		road: 20506.9286111111,
		acceleration: 0
	},
	{
		id: 2408,
		time: 2407,
		velocity: 0,
		power: 0,
		road: 20506.9286111111,
		acceleration: 0
	},
	{
		id: 2409,
		time: 2408,
		velocity: 0,
		power: 0,
		road: 20506.9286111111,
		acceleration: 0
	},
	{
		id: 2410,
		time: 2409,
		velocity: 0,
		power: 0,
		road: 20506.9286111111,
		acceleration: 0
	},
	{
		id: 2411,
		time: 2410,
		velocity: 0,
		power: 29.9982425196577,
		road: 20507.0265277777,
		acceleration: 0.195833333333333
	},
	{
		id: 2412,
		time: 2411,
		velocity: 0.5875,
		power: 399.007817630174,
		road: 20507.5560648148,
		acceleration: 0.667407407407407
	},
	{
		id: 2413,
		time: 2412,
		velocity: 2.00222222222222,
		power: 2979.25521684777,
		road: 20509.2706944444,
		acceleration: 1.70277777777778
	},
	{
		id: 2414,
		time: 2413,
		velocity: 5.10833333333333,
		power: 6125.67128067919,
		road: 20512.7081481481,
		acceleration: 1.74287037037037
	},
	{
		id: 2415,
		time: 2414,
		velocity: 5.81611111111111,
		power: 7988.4468582325,
		road: 20517.7755555555,
		acceleration: 1.51703703703704
	},
	{
		id: 2416,
		time: 2415,
		velocity: 6.55333333333333,
		power: 7319.12549141972,
		road: 20524.1318518518,
		acceleration: 1.06074074074074
	},
	{
		id: 2417,
		time: 2416,
		velocity: 8.29055555555555,
		power: 8345.2108411588,
		road: 20531.5319907407,
		acceleration: 1.02694444444445
	},
	{
		id: 2418,
		time: 2417,
		velocity: 8.89694444444445,
		power: 14774.0139709102,
		road: 20540.2502314814,
		acceleration: 1.60925925925926
	},
	{
		id: 2419,
		time: 2418,
		velocity: 11.3811111111111,
		power: 16631.5845188408,
		road: 20550.5284722222,
		acceleration: 1.51074074074074
	},
	{
		id: 2420,
		time: 2419,
		velocity: 12.8227777777778,
		power: 21078.0787897915,
		road: 20562.3900462962,
		acceleration: 1.65592592592592
	},
	{
		id: 2421,
		time: 2420,
		velocity: 13.8647222222222,
		power: 12658.1761561435,
		road: 20575.4743518518,
		acceleration: 0.789537037037036
	},
	{
		id: 2422,
		time: 2421,
		velocity: 13.7497222222222,
		power: 8170.78733983387,
		road: 20589.1498611111,
		acceleration: 0.392870370370373
	},
	{
		id: 2423,
		time: 2422,
		velocity: 14.0013888888889,
		power: 12593.9825779887,
		road: 20603.3649074074,
		acceleration: 0.686203703703704
	},
	{
		id: 2424,
		time: 2423,
		velocity: 15.9233333333333,
		power: 17685.9366540691,
		road: 20618.4105555555,
		acceleration: 0.975
	},
	{
		id: 2425,
		time: 2424,
		velocity: 16.6747222222222,
		power: 25670.6934908459,
		road: 20634.6331481481,
		acceleration: 1.37888888888889
	},
	{
		id: 2426,
		time: 2425,
		velocity: 18.1380555555556,
		power: 26993.5832354142,
		road: 20652.1974537037,
		acceleration: 1.30453703703704
	},
	{
		id: 2427,
		time: 2426,
		velocity: 19.8369444444444,
		power: 28426.1587703134,
		road: 20671.0380092592,
		acceleration: 1.24796296296297
	},
	{
		id: 2428,
		time: 2427,
		velocity: 20.4186111111111,
		power: 17379.6294496401,
		road: 20690.7881944444,
		acceleration: 0.571296296296296
	},
	{
		id: 2429,
		time: 2428,
		velocity: 19.8519444444444,
		power: 2848.25571783536,
		road: 20710.7229629629,
		acceleration: -0.202129629629628
	},
	{
		id: 2430,
		time: 2429,
		velocity: 19.2305555555556,
		power: -8408.89095475766,
		road: 20730.1606944444,
		acceleration: -0.791944444444443
	},
	{
		id: 2431,
		time: 2430,
		velocity: 18.0427777777778,
		power: -9998.54680005787,
		road: 20748.7605092592,
		acceleration: -0.88388888888889
	},
	{
		id: 2432,
		time: 2431,
		velocity: 17.2002777777778,
		power: -9104.94795636229,
		road: 20766.4977777777,
		acceleration: -0.841203703703705
	},
	{
		id: 2433,
		time: 2432,
		velocity: 16.7069444444444,
		power: -4528.19976009352,
		road: 20783.53,
		acceleration: -0.568888888888889
	},
	{
		id: 2434,
		time: 2433,
		velocity: 16.3361111111111,
		power: -3180.24574091958,
		road: 20800.0365277777,
		acceleration: -0.482499999999998
	},
	{
		id: 2435,
		time: 2434,
		velocity: 15.7527777777778,
		power: -3916.01919511897,
		road: 20816.0379166666,
		acceleration: -0.527777777777777
	},
	{
		id: 2436,
		time: 2435,
		velocity: 15.1236111111111,
		power: -7483.99703116602,
		road: 20831.3901851851,
		acceleration: -0.770462962962965
	},
	{
		id: 2437,
		time: 2436,
		velocity: 14.0247222222222,
		power: -9074.40754092346,
		road: 20845.9068518518,
		acceleration: -0.900740740740742
	},
	{
		id: 2438,
		time: 2437,
		velocity: 13.0505555555556,
		power: -10985.6266167714,
		road: 20859.4324074074,
		acceleration: -1.08148148148148
	},
	{
		id: 2439,
		time: 2438,
		velocity: 11.8791666666667,
		power: -12449.7679544009,
		road: 20871.7820833333,
		acceleration: -1.27027777777777
	},
	{
		id: 2440,
		time: 2439,
		velocity: 10.2138888888889,
		power: -11457.3164690335,
		road: 20882.8556018518,
		acceleration: -1.28203703703704
	},
	{
		id: 2441,
		time: 2440,
		velocity: 9.20444444444444,
		power: -8086.51892289666,
		road: 20892.7685185185,
		acceleration: -1.03916666666667
	},
	{
		id: 2442,
		time: 2441,
		velocity: 8.76166666666667,
		power: -4080.14229719871,
		road: 20901.83875,
		acceleration: -0.646203703703703
	},
	{
		id: 2443,
		time: 2442,
		velocity: 8.27527777777778,
		power: 1315.35216979079,
		road: 20910.5796759259,
		acceleration: -0.012407407407407
	},
	{
		id: 2444,
		time: 2443,
		velocity: 9.16722222222222,
		power: 4727.93119423033,
		road: 20919.5063425925,
		acceleration: 0.383888888888889
	},
	{
		id: 2445,
		time: 2444,
		velocity: 9.91333333333333,
		power: 5422.63436490854,
		road: 20928.8416203703,
		acceleration: 0.433333333333334
	},
	{
		id: 2446,
		time: 2445,
		velocity: 9.57527777777778,
		power: 2708.39451158509,
		road: 20938.4517592592,
		acceleration: 0.116388888888888
	},
	{
		id: 2447,
		time: 2446,
		velocity: 9.51638888888889,
		power: 2339.79048097005,
		road: 20948.1563888888,
		acceleration: 0.0725925925925939
	},
	{
		id: 2448,
		time: 2447,
		velocity: 10.1311111111111,
		power: 2354.28566351478,
		road: 20957.9330555555,
		acceleration: 0.0714814814814808
	},
	{
		id: 2449,
		time: 2448,
		velocity: 9.78972222222222,
		power: 675.454653326409,
		road: 20967.69125,
		acceleration: -0.108425925925925
	},
	{
		id: 2450,
		time: 2449,
		velocity: 9.19111111111111,
		power: -3716.01458547609,
		road: 20977.0992129629,
		acceleration: -0.592037037037038
	},
	{
		id: 2451,
		time: 2450,
		velocity: 8.355,
		power: -4047.83887595818,
		road: 20985.8837037037,
		acceleration: -0.654907407407407
	},
	{
		id: 2452,
		time: 2451,
		velocity: 7.825,
		power: -5776.79389099538,
		road: 20993.8793981481,
		acceleration: -0.922685185185186
	},
	{
		id: 2453,
		time: 2452,
		velocity: 6.42305555555556,
		power: -6692.16991905686,
		road: 21000.8302314814,
		acceleration: -1.16703703703704
	},
	{
		id: 2454,
		time: 2453,
		velocity: 4.85388888888889,
		power: -7075.00636248866,
		road: 21006.4641203703,
		acceleration: -1.46685185185185
	},
	{
		id: 2455,
		time: 2454,
		velocity: 3.42444444444444,
		power: -6271.20248617906,
		road: 21010.4722685185,
		acceleration: -1.78462962962963
	},
	{
		id: 2456,
		time: 2455,
		velocity: 1.06916666666667,
		power: -3254.43999378006,
		road: 21012.7791203703,
		acceleration: -1.61796296296296
	},
	{
		id: 2457,
		time: 2456,
		velocity: 0,
		power: -890.523457463209,
		road: 21013.70625,
		acceleration: -1.14148148148148
	},
	{
		id: 2458,
		time: 2457,
		velocity: 0,
		power: -38.634994005848,
		road: 21013.8844444444,
		acceleration: -0.356388888888889
	},
	{
		id: 2459,
		time: 2458,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2460,
		time: 2459,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2461,
		time: 2460,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2462,
		time: 2461,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2463,
		time: 2462,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2464,
		time: 2463,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2465,
		time: 2464,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2466,
		time: 2465,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2467,
		time: 2466,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2468,
		time: 2467,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2469,
		time: 2468,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2470,
		time: 2469,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2471,
		time: 2470,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2472,
		time: 2471,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2473,
		time: 2472,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2474,
		time: 2473,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2475,
		time: 2474,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2476,
		time: 2475,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2477,
		time: 2476,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2478,
		time: 2477,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2479,
		time: 2478,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2480,
		time: 2479,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2481,
		time: 2480,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2482,
		time: 2481,
		velocity: 0,
		power: 0,
		road: 21013.8844444444,
		acceleration: 0
	},
	{
		id: 2483,
		time: 2482,
		velocity: 0,
		power: 73.3714288210784,
		road: 21014.0518981481,
		acceleration: 0.334907407407407
	},
	{
		id: 2484,
		time: 2483,
		velocity: 1.00472222222222,
		power: 943.948541964835,
		road: 21014.9058796296,
		acceleration: 1.03814814814815
	},
	{
		id: 2485,
		time: 2484,
		velocity: 3.11444444444444,
		power: 4478.61468821359,
		road: 21017.2293981481,
		acceleration: 1.90092592592593
	},
	{
		id: 2486,
		time: 2485,
		velocity: 5.70277777777778,
		power: 6620.04664572165,
		road: 21021.2927314814,
		acceleration: 1.5787037037037
	},
	{
		id: 2487,
		time: 2486,
		velocity: 5.74083333333333,
		power: 7657.82774683694,
		road: 21026.804074074,
		acceleration: 1.31731481481482
	},
	{
		id: 2488,
		time: 2487,
		velocity: 7.06638888888889,
		power: 5836.30330095546,
		road: 21033.3658796296,
		acceleration: 0.78361111111111
	},
	{
		id: 2489,
		time: 2488,
		velocity: 8.05361111111111,
		power: 11864.7756494022,
		road: 21041.050324074,
		acceleration: 1.46166666666667
	},
	{
		id: 2490,
		time: 2489,
		velocity: 10.1258333333333,
		power: 14094.5595487994,
		road: 21050.1880555555,
		acceleration: 1.44490740740741
	},
	{
		id: 2491,
		time: 2490,
		velocity: 11.4011111111111,
		power: 13272.9704228024,
		road: 21060.6212037037,
		acceleration: 1.14592592592592
	},
	{
		id: 2492,
		time: 2491,
		velocity: 11.4913888888889,
		power: 6128.41743029729,
		road: 21071.8153703703,
		acceleration: 0.37611111111111
	},
	{
		id: 2493,
		time: 2492,
		velocity: 11.2541666666667,
		power: 4664.07124603011,
		road: 21083.3093981481,
		acceleration: 0.223611111111111
	},
	{
		id: 2494,
		time: 2493,
		velocity: 12.0719444444444,
		power: 6107.7025401541,
		road: 21095.0843518518,
		acceleration: 0.338240740740742
	},
	{
		id: 2495,
		time: 2494,
		velocity: 12.5061111111111,
		power: 6005.78934426783,
		road: 21107.1836111111,
		acceleration: 0.310370370370372
	},
	{
		id: 2496,
		time: 2495,
		velocity: 12.1852777777778,
		power: 1197.89176530112,
		road: 21119.3838425926,
		acceleration: -0.108425925925928
	},
	{
		id: 2497,
		time: 2496,
		velocity: 11.7466666666667,
		power: -537.361144784957,
		road: 21131.4019907407,
		acceleration: -0.255740740740739
	},
	{
		id: 2498,
		time: 2497,
		velocity: 11.7388888888889,
		power: 2300.09706727063,
		road: 21143.2901388888,
		acceleration: -0.00425925925926052
	},
	{
		id: 2499,
		time: 2498,
		velocity: 12.1725,
		power: 3791.09119085559,
		road: 21155.2385185185,
		acceleration: 0.124722222222221
	},
	{
		id: 2500,
		time: 2499,
		velocity: 12.1208333333333,
		power: 2686.12646613794,
		road: 21167.2618981481,
		acceleration: 0.0252777777777791
	},
	{
		id: 2501,
		time: 2500,
		velocity: 11.8147222222222,
		power: -3467.66057049527,
		road: 21179.0407407407,
		acceleration: -0.514351851851851
	},
	{
		id: 2502,
		time: 2501,
		velocity: 10.6294444444444,
		power: -6559.34810237394,
		road: 21190.1543055555,
		acceleration: -0.816203703703703
	},
	{
		id: 2503,
		time: 2502,
		velocity: 9.67222222222222,
		power: -5845.94660422564,
		road: 21200.4686574074,
		acceleration: -0.782222222222224
	},
	{
		id: 2504,
		time: 2503,
		velocity: 9.46805555555556,
		power: -2938.62084786666,
		road: 21210.1423611111,
		acceleration: -0.499074074074073
	},
	{
		id: 2505,
		time: 2504,
		velocity: 9.13222222222222,
		power: -607.228801127705,
		road: 21219.4441666666,
		acceleration: -0.244722222222222
	},
	{
		id: 2506,
		time: 2505,
		velocity: 8.93805555555556,
		power: -1794.28986499124,
		road: 21228.4323148148,
		acceleration: -0.382592592592593
	},
	{
		id: 2507,
		time: 2506,
		velocity: 8.32027777777778,
		power: -3027.45745913278,
		road: 21236.9584722222,
		acceleration: -0.541388888888887
	},
	{
		id: 2508,
		time: 2507,
		velocity: 7.50805555555556,
		power: -4727.46907076004,
		road: 21244.8167129629,
		acceleration: -0.794444444444445
	},
	{
		id: 2509,
		time: 2508,
		velocity: 6.55472222222222,
		power: -4321.18521498183,
		road: 21251.878287037,
		acceleration: -0.798888888888889
	},
	{
		id: 2510,
		time: 2509,
		velocity: 5.92361111111111,
		power: -3866.34152829449,
		road: 21258.1409722222,
		acceleration: -0.798888888888889
	},
	{
		id: 2511,
		time: 2510,
		velocity: 5.11138888888889,
		power: -2763.8456120568,
		road: 21263.6688425925,
		acceleration: -0.67074074074074
	},
	{
		id: 2512,
		time: 2511,
		velocity: 4.5425,
		power: -2079.52641749298,
		road: 21268.5674537037,
		acceleration: -0.587777777777777
	},
	{
		id: 2513,
		time: 2512,
		velocity: 4.16027777777778,
		power: -735.420405671762,
		road: 21273.0158796296,
		acceleration: -0.312592592592592
	},
	{
		id: 2514,
		time: 2513,
		velocity: 4.17361111111111,
		power: -7.38685976118397,
		road: 21277.2383796296,
		acceleration: -0.13925925925926
	},
	{
		id: 2515,
		time: 2514,
		velocity: 4.12472222222222,
		power: 674.340880191639,
		road: 21281.407824074,
		acceleration: 0.0331481481481486
	},
	{
		id: 2516,
		time: 2515,
		velocity: 4.25972222222222,
		power: 413.333128209092,
		road: 21285.5774537037,
		acceleration: -0.0327777777777776
	},
	{
		id: 2517,
		time: 2516,
		velocity: 4.07527777777778,
		power: 881.906832908348,
		road: 21289.7727314814,
		acceleration: 0.0840740740740742
	},
	{
		id: 2518,
		time: 2517,
		velocity: 4.37694444444444,
		power: 1022.00293271176,
		road: 21294.0664814814,
		acceleration: 0.11287037037037
	},
	{
		id: 2519,
		time: 2518,
		velocity: 4.59833333333333,
		power: 1402.38360704728,
		road: 21298.5134259259,
		acceleration: 0.193518518518518
	},
	{
		id: 2520,
		time: 2519,
		velocity: 4.65583333333333,
		power: 827.722632195117,
		road: 21303.0829166666,
		acceleration: 0.0515740740740744
	},
	{
		id: 2521,
		time: 2520,
		velocity: 4.53166666666667,
		power: 371.485629648773,
		road: 21307.6514351851,
		acceleration: -0.0535185185185183
	},
	{
		id: 2522,
		time: 2521,
		velocity: 4.43777777777778,
		power: -5.39810824680897,
		road: 21312.1232407407,
		acceleration: -0.139907407407407
	},
	{
		id: 2523,
		time: 2522,
		velocity: 4.23611111111111,
		power: 657.515792202718,
		road: 21316.5343981481,
		acceleration: 0.0186111111111105
	},
	{
		id: 2524,
		time: 2523,
		velocity: 4.5875,
		power: 1078.88609607567,
		road: 21321.0123611111,
		acceleration: 0.115
	},
	{
		id: 2525,
		time: 2524,
		velocity: 4.78277777777778,
		power: 1667.49692895241,
		road: 21325.6666203703,
		acceleration: 0.237592592592593
	},
	{
		id: 2526,
		time: 2525,
		velocity: 4.94888888888889,
		power: 1723.85704718932,
		road: 21330.5548611111,
		acceleration: 0.23037037037037
	},
	{
		id: 2527,
		time: 2526,
		velocity: 5.27861111111111,
		power: 468.345954228974,
		road: 21335.5370833333,
		acceleration: -0.0424074074074072
	},
	{
		id: 2528,
		time: 2527,
		velocity: 4.65555555555556,
		power: -206.062897580258,
		road: 21340.4054629629,
		acceleration: -0.185277777777777
	},
	{
		id: 2529,
		time: 2528,
		velocity: 4.39305555555556,
		power: -548.172283505006,
		road: 21345.0493055555,
		acceleration: -0.263796296296296
	},
	{
		id: 2530,
		time: 2529,
		velocity: 4.48722222222222,
		power: 299.117347412069,
		road: 21349.5270833333,
		acceleration: -0.0683333333333334
	},
	{
		id: 2531,
		time: 2530,
		velocity: 4.45055555555555,
		power: 372.029722664601,
		road: 21353.9458333333,
		acceleration: -0.0497222222222229
	},
	{
		id: 2532,
		time: 2531,
		velocity: 4.24388888888889,
		power: 224.972615940826,
		road: 21358.2979166666,
		acceleration: -0.0836111111111109
	},
	{
		id: 2533,
		time: 2532,
		velocity: 4.23638888888889,
		power: -42.5341665076667,
		road: 21362.5341666666,
		acceleration: -0.148055555555555
	},
	{
		id: 2534,
		time: 2533,
		velocity: 4.00638888888889,
		power: -176.644085677343,
		road: 21366.6051851851,
		acceleration: -0.182407407407408
	},
	{
		id: 2535,
		time: 2534,
		velocity: 3.69666666666667,
		power: -67.4763141448789,
		road: 21370.5079166666,
		acceleration: -0.154166666666666
	},
	{
		id: 2536,
		time: 2535,
		velocity: 3.77388888888889,
		power: -428.044163349782,
		road: 21374.2050462963,
		acceleration: -0.257037037037037
	},
	{
		id: 2537,
		time: 2536,
		velocity: 3.23527777777778,
		power: 130.330180456406,
		road: 21377.7259722222,
		acceleration: -0.0953703703703708
	},
	{
		id: 2538,
		time: 2537,
		velocity: 3.41055555555556,
		power: 25.0238838802796,
		road: 21381.1361111111,
		acceleration: -0.126203703703704
	},
	{
		id: 2539,
		time: 2538,
		velocity: 3.39527777777778,
		power: 379.439847756108,
		road: 21384.4761574074,
		acceleration: -0.0139814814814816
	},
	{
		id: 2540,
		time: 2539,
		velocity: 3.19333333333333,
		power: -222.469655226328,
		road: 21387.7062962963,
		acceleration: -0.205833333333333
	},
	{
		id: 2541,
		time: 2540,
		velocity: 2.79305555555556,
		power: -400.889024150469,
		road: 21390.6966666666,
		acceleration: -0.273703703703704
	},
	{
		id: 2542,
		time: 2541,
		velocity: 2.57416666666667,
		power: -319.673121934486,
		road: 21393.4225925925,
		acceleration: -0.255185185185185
	},
	{
		id: 2543,
		time: 2542,
		velocity: 2.42777777777778,
		power: -132.213437421813,
		road: 21395.9276388888,
		acceleration: -0.186574074074074
	},
	{
		id: 2544,
		time: 2543,
		velocity: 2.23333333333333,
		power: -278.280680862275,
		road: 21398.2099537037,
		acceleration: -0.258888888888889
	},
	{
		id: 2545,
		time: 2544,
		velocity: 1.7975,
		power: -727.632416363632,
		road: 21400.094537037,
		acceleration: -0.536574074074074
	},
	{
		id: 2546,
		time: 2545,
		velocity: 0.818055555555556,
		power: -443.016347146991,
		road: 21401.4776388888,
		acceleration: -0.466388888888889
	},
	{
		id: 2547,
		time: 2546,
		velocity: 0.834166666666667,
		power: -379.796417122215,
		road: 21402.3279629629,
		acceleration: -0.599166666666667
	},
	{
		id: 2548,
		time: 2547,
		velocity: 0,
		power: -56.9687499869969,
		road: 21402.7423611111,
		acceleration: -0.272685185185185
	},
	{
		id: 2549,
		time: 2548,
		velocity: 0,
		power: -19.8258001461988,
		road: 21402.8813888888,
		acceleration: -0.278055555555556
	},
	{
		id: 2550,
		time: 2549,
		velocity: 0,
		power: 45.8193600293889,
		road: 21403.0082407407,
		acceleration: 0.253703703703704
	},
	{
		id: 2551,
		time: 2550,
		velocity: 0.761111111111111,
		power: 30.6608123264842,
		road: 21403.2619444444,
		acceleration: 0
	},
	{
		id: 2552,
		time: 2551,
		velocity: 0,
		power: 30.6608123264842,
		road: 21403.5156481481,
		acceleration: 0
	},
	{
		id: 2553,
		time: 2552,
		velocity: 0,
		power: -15.1629801819363,
		road: 21403.6425,
		acceleration: -0.253703703703704
	},
	{
		id: 2554,
		time: 2553,
		velocity: 0,
		power: 182.259864288138,
		road: 21403.9223611111,
		acceleration: 0.559722222222222
	},
	{
		id: 2555,
		time: 2554,
		velocity: 1.67916666666667,
		power: 868.94853469944,
		road: 21404.8911574074,
		acceleration: 0.818148148148148
	},
	{
		id: 2556,
		time: 2555,
		velocity: 2.45444444444444,
		power: 2397.96405176346,
		road: 21406.8496296296,
		acceleration: 1.1612037037037
	},
	{
		id: 2557,
		time: 2556,
		velocity: 3.48361111111111,
		power: 2903.01109263473,
		road: 21409.8348148148,
		acceleration: 0.892222222222222
	},
	{
		id: 2558,
		time: 2557,
		velocity: 4.35583333333333,
		power: 3019.10119313638,
		road: 21413.6185648148,
		acceleration: 0.704907407407408
	},
	{
		id: 2559,
		time: 2558,
		velocity: 4.56916666666667,
		power: 1952.95464036494,
		road: 21417.9246296296,
		acceleration: 0.339722222222221
	},
	{
		id: 2560,
		time: 2559,
		velocity: 4.50277777777778,
		power: 1086.51005557039,
		road: 21422.4572685185,
		acceleration: 0.113425925925926
	},
	{
		id: 2561,
		time: 2560,
		velocity: 4.69611111111111,
		power: 1171.13998728879,
		road: 21427.1093518518,
		acceleration: 0.125462962962962
	},
	{
		id: 2562,
		time: 2561,
		velocity: 4.94555555555556,
		power: 603.335332315467,
		road: 21431.8216203703,
		acceleration: -0.0050925925925922
	},
	{
		id: 2563,
		time: 2562,
		velocity: 4.4875,
		power: -173.31354358097,
		road: 21436.4418981481,
		acceleration: -0.178888888888889
	},
	{
		id: 2564,
		time: 2563,
		velocity: 4.15944444444444,
		power: -238.517203726607,
		road: 21440.8751851851,
		acceleration: -0.195092592592593
	},
	{
		id: 2565,
		time: 2564,
		velocity: 4.36027777777778,
		power: 971.969423848263,
		road: 21445.2585648148,
		acceleration: 0.0952777777777785
	},
	{
		id: 2566,
		time: 2565,
		velocity: 4.77333333333333,
		power: 1739.48779182634,
		road: 21449.8207407407,
		acceleration: 0.262314814814814
	},
	{
		id: 2567,
		time: 2566,
		velocity: 4.94638888888889,
		power: 1706.45435797269,
		road: 21454.6306018518,
		acceleration: 0.233055555555556
	},
	{
		id: 2568,
		time: 2567,
		velocity: 5.05944444444444,
		power: 796.695519792413,
		road: 21459.5712962963,
		acceleration: 0.0286111111111111
	},
	{
		id: 2569,
		time: 2568,
		velocity: 4.85916666666667,
		power: 261.181830983108,
		road: 21464.4837962963,
		acceleration: -0.0849999999999991
	},
	{
		id: 2570,
		time: 2569,
		velocity: 4.69138888888889,
		power: -454.567078241371,
		road: 21469.2333796296,
		acceleration: -0.240833333333334
	},
	{
		id: 2571,
		time: 2570,
		velocity: 4.33694444444444,
		power: -1075.64638319849,
		road: 21473.6655555555,
		acceleration: -0.393981481481481
	},
	{
		id: 2572,
		time: 2571,
		velocity: 3.67722222222222,
		power: -943.192439632634,
		road: 21477.7096296296,
		acceleration: -0.382222222222222
	},
	{
		id: 2573,
		time: 2572,
		velocity: 3.54472222222222,
		power: -1582.17961441976,
		road: 21481.2606481481,
		acceleration: -0.603888888888889
	},
	{
		id: 2574,
		time: 2573,
		velocity: 2.52527777777778,
		power: -721.621917295032,
		road: 21484.319074074,
		acceleration: -0.381296296296297
	},
	{
		id: 2575,
		time: 2574,
		velocity: 2.53333333333333,
		power: -595.657803616837,
		road: 21487.0042129629,
		acceleration: -0.365277777777777
	},
	{
		id: 2576,
		time: 2575,
		velocity: 2.44888888888889,
		power: 126.611170713275,
		road: 21489.4683796296,
		acceleration: -0.0766666666666667
	},
	{
		id: 2577,
		time: 2576,
		velocity: 2.29527777777778,
		power: -56.7990804344537,
		road: 21491.8162037037,
		acceleration: -0.156018518518519
	},
	{
		id: 2578,
		time: 2577,
		velocity: 2.06527777777778,
		power: -269.865609740426,
		road: 21493.9544907407,
		acceleration: -0.263055555555555
	},
	{
		id: 2579,
		time: 2578,
		velocity: 1.65972222222222,
		power: -470.88713180288,
		road: 21495.7590277777,
		acceleration: -0.404444444444445
	},
	{
		id: 2580,
		time: 2579,
		velocity: 1.08194444444444,
		power: -414.205877541382,
		road: 21497.13875,
		acceleration: -0.445185185185185
	},
	{
		id: 2581,
		time: 2580,
		velocity: 0.729722222222222,
		power: -354.939390751401,
		road: 21498.0192592592,
		acceleration: -0.553240740740741
	},
	{
		id: 2582,
		time: 2581,
		velocity: 0,
		power: -93.5301644906443,
		road: 21498.442824074,
		acceleration: -0.360648148148148
	},
	{
		id: 2583,
		time: 2582,
		velocity: 0,
		power: -13.3321103476283,
		road: 21498.5644444444,
		acceleration: -0.243240740740741
	},
	{
		id: 2584,
		time: 2583,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2585,
		time: 2584,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2586,
		time: 2585,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2587,
		time: 2586,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2588,
		time: 2587,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2589,
		time: 2588,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2590,
		time: 2589,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2591,
		time: 2590,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2592,
		time: 2591,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2593,
		time: 2592,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2594,
		time: 2593,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2595,
		time: 2594,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2596,
		time: 2595,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2597,
		time: 2596,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2598,
		time: 2597,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2599,
		time: 2598,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2600,
		time: 2599,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2601,
		time: 2600,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2602,
		time: 2601,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2603,
		time: 2602,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2604,
		time: 2603,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2605,
		time: 2604,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2606,
		time: 2605,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2607,
		time: 2606,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2608,
		time: 2607,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2609,
		time: 2608,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2610,
		time: 2609,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2611,
		time: 2610,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2612,
		time: 2611,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2613,
		time: 2612,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2614,
		time: 2613,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2615,
		time: 2614,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2616,
		time: 2615,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2617,
		time: 2616,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2618,
		time: 2617,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2619,
		time: 2618,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2620,
		time: 2619,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2621,
		time: 2620,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2622,
		time: 2621,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2623,
		time: 2622,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2624,
		time: 2623,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2625,
		time: 2624,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2626,
		time: 2625,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2627,
		time: 2626,
		velocity: 0,
		power: 0,
		road: 21498.5644444444,
		acceleration: 0
	},
	{
		id: 2628,
		time: 2627,
		velocity: 0,
		power: 2.60835719711175,
		road: 21498.5814814814,
		acceleration: 0.0340740740740741
	},
	{
		id: 2629,
		time: 2628,
		velocity: 0.102222222222222,
		power: 12.8382960277046,
		road: 21498.6502777777,
		acceleration: 0.0694444444444444
	},
	{
		id: 2630,
		time: 2629,
		velocity: 0.208333333333333,
		power: 12.5074916870742,
		road: 21498.7537962963,
		acceleration: 0
	},
	{
		id: 2631,
		time: 2630,
		velocity: 0,
		power: 7.65705402470659,
		road: 21498.8402777777,
		acceleration: -0.0340740740740741
	},
	{
		id: 2632,
		time: 2631,
		velocity: 0,
		power: 1.91070906432749,
		road: 21498.875,
		acceleration: -0.0694444444444444
	},
	{
		id: 2633,
		time: 2632,
		velocity: 0,
		power: 0,
		road: 21498.875,
		acceleration: 0
	},
	{
		id: 2634,
		time: 2633,
		velocity: 0,
		power: 0,
		road: 21498.875,
		acceleration: 0
	},
	{
		id: 2635,
		time: 2634,
		velocity: 0,
		power: 0,
		road: 21498.875,
		acceleration: 0
	},
	{
		id: 2636,
		time: 2635,
		velocity: 0,
		power: 0,
		road: 21498.875,
		acceleration: 0
	},
	{
		id: 2637,
		time: 2636,
		velocity: 0,
		power: 0,
		road: 21498.875,
		acceleration: 0
	},
	{
		id: 2638,
		time: 2637,
		velocity: 0,
		power: 0,
		road: 21498.875,
		acceleration: 0
	}
]
export default test1;