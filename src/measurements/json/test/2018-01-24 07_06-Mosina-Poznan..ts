export const test8 = [
	{
		id: 1,
		time: 0,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 3,
		time: 2,
		velocity: 0,
		power: 69.8116261366708,
		road: 0.162685185185185,
		acceleration: 0.32537037037037
	},
	{
		id: 4,
		time: 3,
		velocity: 0.976111111111111,
		power: 531.199174263223,
		road: 0.838703703703704,
		acceleration: 0.701296296296296
	},
	{
		id: 5,
		time: 4,
		velocity: 2.10388888888889,
		power: 1747.41753158714,
		road: 2.39351851851852,
		acceleration: 1.0562962962963
	},
	{
		id: 6,
		time: 5,
		velocity: 3.16888888888889,
		power: 1883.35945451037,
		road: 4.82013888888889,
		acceleration: 0.687314814814815
	},
	{
		id: 7,
		time: 6,
		velocity: 3.03805555555556,
		power: 1414.98515120268,
		road: 7.77643518518519,
		acceleration: 0.372037037037037
	},
	{
		id: 8,
		time: 7,
		velocity: 3.22,
		power: 743.692001832804,
		road: 10.9746759259259,
		acceleration: 0.111851851851851
	},
	{
		id: 9,
		time: 8,
		velocity: 3.50444444444444,
		power: 953.862758585974,
		road: 14.3125462962963,
		acceleration: 0.167407407407408
	},
	{
		id: 10,
		time: 9,
		velocity: 3.54027777777778,
		power: -284.043184572748,
		road: 17.6221296296296,
		acceleration: -0.223981481481482
	},
	{
		id: 11,
		time: 10,
		velocity: 2.54805555555556,
		power: -586.210241636304,
		road: 20.6514814814815,
		acceleration: -0.336481481481481
	},
	{
		id: 12,
		time: 11,
		velocity: 2.495,
		power: -834.57785232854,
		road: 23.2795833333333,
		acceleration: -0.466018518518518
	},
	{
		id: 13,
		time: 12,
		velocity: 2.14222222222222,
		power: 355.119949482032,
		road: 25.6870833333333,
		acceleration: 0.0248148148148148
	},
	{
		id: 14,
		time: 13,
		velocity: 2.6225,
		power: 769.124734297612,
		road: 28.2026388888889,
		acceleration: 0.191296296296297
	},
	{
		id: 15,
		time: 14,
		velocity: 3.06888888888889,
		power: 2064.14769075813,
		road: 31.120462962963,
		acceleration: 0.613240740740741
	},
	{
		id: 16,
		time: 15,
		velocity: 3.98194444444444,
		power: 2789.67495050841,
		road: 34.6893055555556,
		acceleration: 0.688796296296296
	},
	{
		id: 17,
		time: 16,
		velocity: 4.68888888888889,
		power: 3133.11177825819,
		road: 38.9233796296296,
		acceleration: 0.641666666666666
	},
	{
		id: 18,
		time: 17,
		velocity: 4.99388888888889,
		power: 1870.03188746406,
		road: 43.6180555555556,
		acceleration: 0.279537037037038
	},
	{
		id: 19,
		time: 18,
		velocity: 4.82055555555556,
		power: 1740.82509773483,
		road: 48.567037037037,
		acceleration: 0.229074074074074
	},
	{
		id: 20,
		time: 19,
		velocity: 5.37611111111111,
		power: 2765.68629648074,
		road: 53.8352777777778,
		acceleration: 0.409444444444445
	},
	{
		id: 21,
		time: 20,
		velocity: 6.22222222222222,
		power: 5068.67559788042,
		road: 59.6902314814815,
		acceleration: 0.763981481481482
	},
	{
		id: 22,
		time: 21,
		velocity: 7.1125,
		power: 6104.16657826387,
		road: 66.3340740740741,
		acceleration: 0.813796296296296
	},
	{
		id: 23,
		time: 22,
		velocity: 7.8175,
		power: 7706.98219431866,
		road: 73.8444444444444,
		acceleration: 0.91925925925926
	},
	{
		id: 24,
		time: 23,
		velocity: 8.98,
		power: 8571.93894570358,
		road: 82.2653703703704,
		acceleration: 0.901851851851853
	},
	{
		id: 25,
		time: 24,
		velocity: 9.81805555555555,
		power: 8459.91938345317,
		road: 91.52875,
		acceleration: 0.783055555555555
	},
	{
		id: 26,
		time: 25,
		velocity: 10.1666666666667,
		power: 3288.33593342364,
		road: 101.27037037037,
		acceleration: 0.173425925925926
	},
	{
		id: 27,
		time: 26,
		velocity: 9.50027777777778,
		power: -2044.34468952119,
		road: 110.897407407407,
		acceleration: -0.402592592592592
	},
	{
		id: 28,
		time: 27,
		velocity: 8.61027777777778,
		power: -3022.07174479231,
		road: 120.062638888889,
		acceleration: -0.52101851851852
	},
	{
		id: 29,
		time: 28,
		velocity: 8.60361111111111,
		power: -3414.81270685803,
		road: 128.674490740741,
		acceleration: -0.585740740740739
	},
	{
		id: 30,
		time: 29,
		velocity: 7.74305555555556,
		power: -3026.74610469201,
		road: 136.71375,
		acceleration: -0.559444444444444
	},
	{
		id: 31,
		time: 30,
		velocity: 6.93194444444444,
		power: -6942.4020614028,
		road: 143.88625,
		acceleration: -1.17407407407408
	},
	{
		id: 32,
		time: 31,
		velocity: 5.08138888888889,
		power: -6143.81988605896,
		road: 149.85662037037,
		acceleration: -1.23018518518519
	},
	{
		id: 33,
		time: 32,
		velocity: 4.0525,
		power: -3316.95033327423,
		road: 154.787268518519,
		acceleration: -0.849259259259258
	},
	{
		id: 34,
		time: 33,
		velocity: 4.38416666666667,
		power: 1005.70503350098,
		road: 159.340046296296,
		acceleration: 0.0935185185185183
	},
	{
		id: 35,
		time: 34,
		velocity: 5.36194444444444,
		power: 5415.18621292753,
		road: 164.428564814815,
		acceleration: 0.977962962962962
	},
	{
		id: 36,
		time: 35,
		velocity: 6.98638888888889,
		power: 8320.98296808251,
		road: 170.636342592593,
		acceleration: 1.26055555555556
	},
	{
		id: 37,
		time: 36,
		velocity: 8.16583333333333,
		power: 10572.2785247236,
		road: 178.135601851852,
		acceleration: 1.32240740740741
	},
	{
		id: 38,
		time: 37,
		velocity: 9.32916666666667,
		power: 8736.41408479704,
		road: 186.744398148148,
		acceleration: 0.896666666666667
	},
	{
		id: 39,
		time: 38,
		velocity: 9.67638888888889,
		power: 8365.42621194207,
		road: 196.178194444444,
		acceleration: 0.753333333333334
	},
	{
		id: 40,
		time: 39,
		velocity: 10.4258333333333,
		power: 6170.90212053131,
		road: 206.219027777778,
		acceleration: 0.460740740740739
	},
	{
		id: 41,
		time: 40,
		velocity: 10.7113888888889,
		power: 8011.06240479163,
		road: 216.792453703704,
		acceleration: 0.604444444444447
	},
	{
		id: 42,
		time: 41,
		velocity: 11.4897222222222,
		power: 4486.41929949204,
		road: 227.784398148148,
		acceleration: 0.23259259259259
	},
	{
		id: 43,
		time: 42,
		velocity: 11.1236111111111,
		power: 1885.25323454147,
		road: 238.883287037037,
		acceleration: -0.0187037037037019
	},
	{
		id: 44,
		time: 43,
		velocity: 10.6552777777778,
		power: -2144.03366854235,
		road: 249.772407407407,
		acceleration: -0.400833333333335
	},
	{
		id: 45,
		time: 44,
		velocity: 10.2872222222222,
		power: -0.589038750077466,
		road: 260.365740740741,
		acceleration: -0.19074074074074
	},
	{
		id: 46,
		time: 45,
		velocity: 10.5513888888889,
		power: 2753.65291071793,
		road: 270.905740740741,
		acceleration: 0.0840740740740742
	},
	{
		id: 47,
		time: 46,
		velocity: 10.9075,
		power: 3071.97804536469,
		road: 281.543657407407,
		acceleration: 0.111759259259259
	},
	{
		id: 48,
		time: 47,
		velocity: 10.6225,
		power: 3826.20129783417,
		road: 292.327083333333,
		acceleration: 0.179259259259258
	},
	{
		id: 49,
		time: 48,
		velocity: 11.0891666666667,
		power: 2723.91536692963,
		road: 303.233888888889,
		acceleration: 0.0675000000000008
	},
	{
		id: 50,
		time: 49,
		velocity: 11.11,
		power: 4744.07553609488,
		road: 314.301018518519,
		acceleration: 0.253148148148149
	},
	{
		id: 51,
		time: 50,
		velocity: 11.3819444444444,
		power: 2097.75733217607,
		road: 325.493981481482,
		acceleration: -0.00148148148148231
	},
	{
		id: 52,
		time: 51,
		velocity: 11.0847222222222,
		power: 2770.13307539802,
		road: 336.716435185185,
		acceleration: 0.0604629629629638
	},
	{
		id: 53,
		time: 52,
		velocity: 11.2913888888889,
		power: -879.81620039706,
		road: 347.829074074074,
		acceleration: -0.280092592592593
	},
	{
		id: 54,
		time: 53,
		velocity: 10.5416666666667,
		power: -1136.40198463161,
		road: 358.649861111111,
		acceleration: -0.303611111111112
	},
	{
		id: 55,
		time: 54,
		velocity: 10.1738888888889,
		power: -1334.85286267145,
		road: 369.157361111111,
		acceleration: -0.322962962962963
	},
	{
		id: 56,
		time: 55,
		velocity: 10.3225,
		power: -90.8719799638089,
		road: 379.405416666667,
		acceleration: -0.195925925925925
	},
	{
		id: 57,
		time: 56,
		velocity: 9.95388888888889,
		power: -64.2698647505313,
		road: 389.459953703704,
		acceleration: -0.191111111111113
	},
	{
		id: 58,
		time: 57,
		velocity: 9.60055555555556,
		power: -291.132719780168,
		road: 399.312361111111,
		acceleration: -0.213148148148145
	},
	{
		id: 59,
		time: 58,
		velocity: 9.68305555555556,
		power: 2355.97892970997,
		road: 409.093935185185,
		acceleration: 0.0714814814814808
	},
	{
		id: 60,
		time: 59,
		velocity: 10.1683333333333,
		power: 6404.34986232552,
		road: 419.153101851852,
		acceleration: 0.483703703703702
	},
	{
		id: 61,
		time: 60,
		velocity: 11.0516666666667,
		power: 8986.85763901397,
		road: 429.801157407407,
		acceleration: 0.694074074074075
	},
	{
		id: 62,
		time: 61,
		velocity: 11.7652777777778,
		power: 8338.45018998743,
		road: 441.084166666667,
		acceleration: 0.575833333333334
	},
	{
		id: 63,
		time: 62,
		velocity: 11.8958333333333,
		power: 9666.86190848006,
		road: 452.977546296296,
		acceleration: 0.644907407407407
	},
	{
		id: 64,
		time: 63,
		velocity: 12.9863888888889,
		power: 10124.6374291392,
		road: 465.50875,
		acceleration: 0.630740740740741
	},
	{
		id: 65,
		time: 64,
		velocity: 13.6575,
		power: 11249.7737176323,
		road: 478.689675925926,
		acceleration: 0.668703703703702
	},
	{
		id: 66,
		time: 65,
		velocity: 13.9019444444444,
		power: 3671.36423261019,
		road: 492.231527777778,
		acceleration: 0.0531481481481464
	},
	{
		id: 67,
		time: 66,
		velocity: 13.1458333333333,
		power: 2427.05424727652,
		road: 505.778333333333,
		acceleration: -0.0432407407407389
	},
	{
		id: 68,
		time: 67,
		velocity: 13.5277777777778,
		power: -3273.26228498514,
		road: 519.060972222222,
		acceleration: -0.485092592592592
	},
	{
		id: 69,
		time: 68,
		velocity: 12.4466666666667,
		power: -2763.63091240864,
		road: 531.878055555556,
		acceleration: -0.446018518518519
	},
	{
		id: 70,
		time: 69,
		velocity: 11.8077777777778,
		power: -4621.28264357524,
		road: 544.16875,
		acceleration: -0.60675925925926
	},
	{
		id: 71,
		time: 70,
		velocity: 11.7075,
		power: 1017.20293385757,
		road: 556.096944444444,
		acceleration: -0.118240740740742
	},
	{
		id: 72,
		time: 71,
		velocity: 12.0919444444444,
		power: 2895.27556919064,
		road: 567.990046296296,
		acceleration: 0.0480555555555551
	},
	{
		id: 73,
		time: 72,
		velocity: 11.9519444444444,
		power: 3567.34319191643,
		road: 579.959305555555,
		acceleration: 0.10425925925926
	},
	{
		id: 74,
		time: 73,
		velocity: 12.0202777777778,
		power: 1737.17113334102,
		road: 591.952361111111,
		acceleration: -0.0566666666666666
	},
	{
		id: 75,
		time: 74,
		velocity: 11.9219444444444,
		power: 1783.22440162611,
		road: 603.891481481481,
		acceleration: -0.0512037037037043
	},
	{
		id: 76,
		time: 75,
		velocity: 11.7983333333333,
		power: 257.03293784134,
		road: 615.713287037037,
		acceleration: -0.183425925925926
	},
	{
		id: 77,
		time: 76,
		velocity: 11.47,
		power: 15.0374691704556,
		road: 627.342222222222,
		acceleration: -0.202314814814814
	},
	{
		id: 78,
		time: 77,
		velocity: 11.315,
		power: -345.458691621215,
		road: 638.753703703704,
		acceleration: -0.232592592592592
	},
	{
		id: 79,
		time: 78,
		velocity: 11.1005555555556,
		power: -528.510277163214,
		road: 649.925185185185,
		acceleration: -0.247407407407408
	},
	{
		id: 80,
		time: 79,
		velocity: 10.7277777777778,
		power: -795.376710417806,
		road: 660.837453703704,
		acceleration: -0.271018518518517
	},
	{
		id: 81,
		time: 80,
		velocity: 10.5019444444444,
		power: -594.141796068155,
		road: 671.489259259259,
		acceleration: -0.249907407407408
	},
	{
		id: 82,
		time: 81,
		velocity: 10.3508333333333,
		power: -447.427178199177,
		road: 681.899305555555,
		acceleration: -0.233611111111111
	},
	{
		id: 83,
		time: 82,
		velocity: 10.0269444444444,
		power: -2432.47898192559,
		road: 691.973518518518,
		acceleration: -0.438055555555556
	},
	{
		id: 84,
		time: 83,
		velocity: 9.18777777777778,
		power: -4009.70907181568,
		road: 701.518796296296,
		acceleration: -0.619814814814815
	},
	{
		id: 85,
		time: 84,
		velocity: 8.49138888888889,
		power: -6854.37361173042,
		road: 710.256944444444,
		acceleration: -0.994444444444444
	},
	{
		id: 86,
		time: 85,
		velocity: 7.04361111111111,
		power: -6520.06315925969,
		road: 717.973333333333,
		acceleration: -1.04907407407408
	},
	{
		id: 87,
		time: 86,
		velocity: 6.04055555555555,
		power: -5108.88906880111,
		road: 724.68875,
		acceleration: -0.95287037037037
	},
	{
		id: 88,
		time: 87,
		velocity: 5.63277777777778,
		power: -1589.8368976405,
		road: 730.715046296296,
		acceleration: -0.425370370370371
	},
	{
		id: 89,
		time: 88,
		velocity: 5.7675,
		power: 2760.63538744674,
		road: 736.697592592593,
		acceleration: 0.33787037037037
	},
	{
		id: 90,
		time: 89,
		velocity: 7.05416666666667,
		power: 7215.62927988726,
		road: 743.343703703704,
		acceleration: 0.989259259259262
	},
	{
		id: 91,
		time: 90,
		velocity: 8.60055555555556,
		power: 11455.0914300301,
		road: 751.172175925926,
		acceleration: 1.37546296296296
	},
	{
		id: 92,
		time: 91,
		velocity: 9.89388888888889,
		power: 15829.7225637194,
		road: 760.491712962963,
		acceleration: 1.60666666666667
	},
	{
		id: 93,
		time: 92,
		velocity: 11.8741666666667,
		power: 12393.8875707623,
		road: 771.130046296296,
		acceleration: 1.03092592592592
	},
	{
		id: 94,
		time: 93,
		velocity: 11.6933333333333,
		power: 5052.23722663578,
		road: 782.418888888889,
		acceleration: 0.270092592592594
	},
	{
		id: 95,
		time: 94,
		velocity: 10.7041666666667,
		power: -4052.14236581195,
		road: 793.553240740741,
		acceleration: -0.579074074074072
	},
	{
		id: 96,
		time: 95,
		velocity: 10.1369444444444,
		power: -4262.14302319469,
		road: 804.090833333333,
		acceleration: -0.614444444444445
	},
	{
		id: 97,
		time: 96,
		velocity: 9.85,
		power: -2314.34281055966,
		road: 814.107962962963,
		acceleration: -0.426481481481481
	},
	{
		id: 98,
		time: 97,
		velocity: 9.42472222222222,
		power: -1060.60409191707,
		road: 823.764212962963,
		acceleration: -0.29527777777778
	},
	{
		id: 99,
		time: 98,
		velocity: 9.25111111111111,
		power: 458.218163621492,
		road: 833.209444444444,
		acceleration: -0.126759259259259
	},
	{
		id: 100,
		time: 99,
		velocity: 9.46972222222222,
		power: 2211.30249745752,
		road: 842.625879629629,
		acceleration: 0.0691666666666677
	},
	{
		id: 101,
		time: 100,
		velocity: 9.63222222222222,
		power: 4278.72344600891,
		road: 852.221296296296,
		acceleration: 0.288796296296297
	},
	{
		id: 102,
		time: 101,
		velocity: 10.1175,
		power: 2120.56975115531,
		road: 861.984537037037,
		acceleration: 0.0468518518518515
	},
	{
		id: 103,
		time: 102,
		velocity: 9.61027777777778,
		power: 1668.49117839934,
		road: 871.77,
		acceleration: -0.00240740740740719
	},
	{
		id: 104,
		time: 103,
		velocity: 9.625,
		power: 560.688384930361,
		road: 881.494166666666,
		acceleration: -0.120185185185186
	},
	{
		id: 105,
		time: 104,
		velocity: 9.75694444444444,
		power: 2134.60785858398,
		road: 891.183703703703,
		acceleration: 0.0509259259259256
	},
	{
		id: 106,
		time: 105,
		velocity: 9.76305555555555,
		power: 2161.56396939826,
		road: 900.924722222222,
		acceleration: 0.0520370370370369
	},
	{
		id: 107,
		time: 106,
		velocity: 9.78111111111111,
		power: 2287.5901854602,
		road: 910.723518518518,
		acceleration: 0.063518518518519
	},
	{
		id: 108,
		time: 107,
		velocity: 9.9475,
		power: 1788.85216621085,
		road: 920.558564814814,
		acceleration: 0.00898148148148081
	},
	{
		id: 109,
		time: 108,
		velocity: 9.79,
		power: -88.4484963530683,
		road: 930.30287037037,
		acceleration: -0.190462962962963
	},
	{
		id: 110,
		time: 109,
		velocity: 9.20972222222222,
		power: -2207.79838611683,
		road: 939.740324074074,
		acceleration: -0.423240740740741
	},
	{
		id: 111,
		time: 110,
		velocity: 8.67777777777778,
		power: -1738.58123830206,
		road: 948.778425925925,
		acceleration: -0.375462962962963
	},
	{
		id: 112,
		time: 111,
		velocity: 8.66361111111111,
		power: -588.092268918813,
		road: 957.50824074074,
		acceleration: -0.24111111111111
	},
	{
		id: 113,
		time: 112,
		velocity: 8.48638888888889,
		power: 1424.31025882268,
		road: 966.119768518518,
		acceleration: 0.00453703703703567
	},
	{
		id: 114,
		time: 113,
		velocity: 8.69138888888889,
		power: 753.278729012641,
		road: 974.695277777777,
		acceleration: -0.0765740740740739
	},
	{
		id: 115,
		time: 114,
		velocity: 8.43388888888889,
		power: 2315.54383821527,
		road: 983.28949074074,
		acceleration: 0.113981481481481
	},
	{
		id: 116,
		time: 115,
		velocity: 8.82833333333333,
		power: 2826.78505200181,
		road: 992.025416666666,
		acceleration: 0.169444444444444
	},
	{
		id: 117,
		time: 116,
		velocity: 9.19972222222222,
		power: 3462.03965652195,
		road: 1000.96324074074,
		acceleration: 0.234351851851853
	},
	{
		id: 118,
		time: 117,
		velocity: 9.13694444444444,
		power: 1807.15806072586,
		road: 1010.03592592593,
		acceleration: 0.0353703703703712
	},
	{
		id: 119,
		time: 118,
		velocity: 8.93444444444444,
		power: -7.62927544120461,
		road: 1019.03930555555,
		acceleration: -0.173981481481482
	},
	{
		id: 120,
		time: 119,
		velocity: 8.67777777777778,
		power: -673.192022252432,
		road: 1027.83,
		acceleration: -0.25138888888889
	},
	{
		id: 121,
		time: 120,
		velocity: 8.38277777777778,
		power: -236.682053119617,
		road: 1036.39611111111,
		acceleration: -0.197777777777778
	},
	{
		id: 122,
		time: 121,
		velocity: 8.34111111111111,
		power: -118.17030133694,
		road: 1044.7724537037,
		acceleration: -0.181759259259259
	},
	{
		id: 123,
		time: 122,
		velocity: 8.1325,
		power: -12.0625879921484,
		road: 1052.97449074074,
		acceleration: -0.166851851851851
	},
	{
		id: 124,
		time: 123,
		velocity: 7.88222222222222,
		power: -334.902030501845,
		road: 1060.98935185185,
		acceleration: -0.2075
	},
	{
		id: 125,
		time: 124,
		velocity: 7.71861111111111,
		power: 197.796275613399,
		road: 1068.83268518518,
		acceleration: -0.135555555555556
	},
	{
		id: 126,
		time: 125,
		velocity: 7.72583333333333,
		power: 1558.68268959221,
		road: 1076.63240740741,
		acceleration: 0.0483333333333329
	},
	{
		id: 127,
		time: 126,
		velocity: 8.02722222222222,
		power: 3655.68782016536,
		road: 1084.61523148148,
		acceleration: 0.317870370370372
	},
	{
		id: 128,
		time: 127,
		velocity: 8.67222222222222,
		power: 4045.53340308373,
		road: 1092.92939814815,
		acceleration: 0.344814814814812
	},
	{
		id: 129,
		time: 128,
		velocity: 8.76027777777778,
		power: 4573.88377874217,
		road: 1101.60782407407,
		acceleration: 0.383703703703706
	},
	{
		id: 130,
		time: 129,
		velocity: 9.17833333333333,
		power: 4526.83170569868,
		road: 1110.65407407407,
		acceleration: 0.351944444444444
	},
	{
		id: 131,
		time: 130,
		velocity: 9.72805555555555,
		power: 4716.13895464499,
		road: 1120.05115740741,
		acceleration: 0.349722222222224
	},
	{
		id: 132,
		time: 131,
		velocity: 9.80944444444444,
		power: 3047.05534574901,
		road: 1129.69893518518,
		acceleration: 0.151666666666666
	},
	{
		id: 133,
		time: 132,
		velocity: 9.63333333333333,
		power: 787.77801398624,
		road: 1139.37518518518,
		acceleration: -0.0947222222222237
	},
	{
		id: 134,
		time: 133,
		velocity: 9.44388888888889,
		power: -543.844518577439,
		road: 1148.88486111111,
		acceleration: -0.238425925925926
	},
	{
		id: 135,
		time: 134,
		velocity: 9.09416666666667,
		power: -511.498317696057,
		road: 1158.15842592593,
		acceleration: -0.233796296296296
	},
	{
		id: 136,
		time: 135,
		velocity: 8.93194444444445,
		power: 1451.00038611704,
		road: 1167.31101851852,
		acceleration: -0.00814814814814824
	},
	{
		id: 137,
		time: 136,
		velocity: 9.41944444444444,
		power: 3221.57070063965,
		road: 1176.55472222222,
		acceleration: 0.190370370370371
	},
	{
		id: 138,
		time: 137,
		velocity: 9.66527777777778,
		power: 4869.57584587509,
		road: 1186.07291666667,
		acceleration: 0.358611111111111
	},
	{
		id: 139,
		time: 138,
		velocity: 10.0077777777778,
		power: 4372.60303659939,
		road: 1195.91263888889,
		acceleration: 0.284444444444445
	},
	{
		id: 140,
		time: 139,
		velocity: 10.2727777777778,
		power: 4092.20736931304,
		road: 1206.01467592592,
		acceleration: 0.240185185185185
	},
	{
		id: 141,
		time: 140,
		velocity: 10.3858333333333,
		power: 3732.22957076471,
		road: 1216.33287037037,
		acceleration: 0.19212962962963
	},
	{
		id: 142,
		time: 141,
		velocity: 10.5841666666667,
		power: 2322.88988894765,
		road: 1226.76949074074,
		acceleration: 0.044722222222223
	},
	{
		id: 143,
		time: 142,
		velocity: 10.4069444444444,
		power: 350.345890518069,
		road: 1237.15208333333,
		acceleration: -0.152777777777779
	},
	{
		id: 144,
		time: 143,
		velocity: 9.9275,
		power: -1388.3684843763,
		road: 1247.29375,
		acceleration: -0.329074074074075
	},
	{
		id: 145,
		time: 144,
		velocity: 9.59694444444444,
		power: -1271.41471368013,
		road: 1257.11203703704,
		acceleration: -0.317685185185184
	},
	{
		id: 146,
		time: 145,
		velocity: 9.45388888888889,
		power: -114.860580056813,
		road: 1266.67569444444,
		acceleration: -0.191574074074074
	},
	{
		id: 147,
		time: 146,
		velocity: 9.35277777777778,
		power: 403.036983817918,
		road: 1276.0774537037,
		acceleration: -0.132222222222223
	},
	{
		id: 148,
		time: 147,
		velocity: 9.20027777777778,
		power: -648.579587419352,
		road: 1285.28851851852,
		acceleration: -0.249166666666666
	},
	{
		id: 149,
		time: 148,
		velocity: 8.70638888888889,
		power: -1810.06183430009,
		road: 1294.18212962963,
		acceleration: -0.38574074074074
	},
	{
		id: 150,
		time: 149,
		velocity: 8.19555555555556,
		power: -2877.80803584097,
		road: 1302.6199537037,
		acceleration: -0.525833333333333
	},
	{
		id: 151,
		time: 150,
		velocity: 7.62277777777778,
		power: -1059.35556240051,
		road: 1310.64365740741,
		acceleration: -0.302407407407409
	},
	{
		id: 152,
		time: 151,
		velocity: 7.79916666666667,
		power: 22.2709939534246,
		road: 1318.43685185185,
		acceleration: -0.158611111111111
	},
	{
		id: 153,
		time: 152,
		velocity: 7.71972222222222,
		power: 1937.17288640924,
		road: 1326.20115740741,
		acceleration: 0.100833333333334
	},
	{
		id: 154,
		time: 153,
		velocity: 7.92527777777778,
		power: 2749.07305502964,
		road: 1334.11699074074,
		acceleration: 0.202222222222224
	},
	{
		id: 155,
		time: 154,
		velocity: 8.40583333333333,
		power: 2966.20226982718,
		road: 1342.24337962963,
		acceleration: 0.218888888888888
	},
	{
		id: 156,
		time: 155,
		velocity: 8.37638888888889,
		power: 1200.40957888558,
		road: 1350.47305555555,
		acceleration: -0.0123148148148147
	},
	{
		id: 157,
		time: 156,
		velocity: 7.88833333333333,
		power: -1412.07014464573,
		road: 1358.5224537037,
		acceleration: -0.348240740740741
	},
	{
		id: 158,
		time: 157,
		velocity: 7.36111111111111,
		power: -3434.5903208236,
		road: 1366.07907407407,
		acceleration: -0.637314814814815
	},
	{
		id: 159,
		time: 158,
		velocity: 6.46444444444444,
		power: -4873.88259022624,
		road: 1372.86259259259,
		acceleration: -0.908888888888889
	},
	{
		id: 160,
		time: 159,
		velocity: 5.16166666666667,
		power: -5694.02413813853,
		road: 1378.5962037037,
		acceleration: -1.19092592592593
	},
	{
		id: 161,
		time: 160,
		velocity: 3.78833333333333,
		power: -6328.08605605679,
		road: 1382.88921296296,
		acceleration: -1.69027777777778
	},
	{
		id: 162,
		time: 161,
		velocity: 1.39361111111111,
		power: -3900.95726759436,
		road: 1385.47680555555,
		acceleration: -1.72055555555556
	},
	{
		id: 163,
		time: 162,
		velocity: 0,
		power: -1178.53763997678,
		road: 1386.57273148148,
		acceleration: -1.26277777777778
	},
	{
		id: 164,
		time: 163,
		velocity: 0,
		power: -74.1563291585445,
		road: 1386.805,
		acceleration: -0.464537037037037
	},
	{
		id: 165,
		time: 164,
		velocity: 0,
		power: 0,
		road: 1386.805,
		acceleration: 0
	},
	{
		id: 166,
		time: 165,
		velocity: 0,
		power: 261.502185586887,
		road: 1387.14592592592,
		acceleration: 0.681851851851852
	},
	{
		id: 167,
		time: 166,
		velocity: 2.04555555555556,
		power: 1723.41731942813,
		road: 1388.45680555555,
		acceleration: 1.25805555555556
	},
	{
		id: 168,
		time: 167,
		velocity: 3.77416666666667,
		power: 4221.68313094693,
		road: 1391.15523148148,
		acceleration: 1.51703703703704
	},
	{
		id: 169,
		time: 168,
		velocity: 4.55111111111111,
		power: 5548.16396722625,
		road: 1395.25601851852,
		acceleration: 1.28768518518518
	},
	{
		id: 170,
		time: 169,
		velocity: 5.90861111111111,
		power: 5424.76171073088,
		road: 1400.47606481481,
		acceleration: 0.950833333333335
	},
	{
		id: 171,
		time: 170,
		velocity: 6.62666666666667,
		power: 6215.32121630689,
		road: 1406.62842592592,
		acceleration: 0.913796296296295
	},
	{
		id: 172,
		time: 171,
		velocity: 7.2925,
		power: 4066.30823121671,
		road: 1413.47310185185,
		acceleration: 0.470833333333334
	},
	{
		id: 173,
		time: 172,
		velocity: 7.32111111111111,
		power: 1290.46188306495,
		road: 1420.57087962963,
		acceleration: 0.0353703703703703
	},
	{
		id: 174,
		time: 173,
		velocity: 6.73277777777778,
		power: -2890.06065709759,
		road: 1427.38662037037,
		acceleration: -0.599444444444444
	},
	{
		id: 175,
		time: 174,
		velocity: 5.49416666666667,
		power: -2009.31542414068,
		road: 1433.65939814815,
		acceleration: -0.486481481481482
	},
	{
		id: 176,
		time: 175,
		velocity: 5.86166666666667,
		power: 706.522905755826,
		road: 1439.67680555555,
		acceleration: -0.0242592592592592
	},
	{
		id: 177,
		time: 176,
		velocity: 6.66,
		power: 5681.93216312224,
		road: 1446.07388888889,
		acceleration: 0.783611111111111
	},
	{
		id: 178,
		time: 177,
		velocity: 7.845,
		power: 8573.49421525844,
		road: 1453.39902777778,
		acceleration: 1.0725
	},
	{
		id: 179,
		time: 178,
		velocity: 9.07916666666667,
		power: 8880.21455230384,
		road: 1461.73652777778,
		acceleration: 0.95222222222222
	},
	{
		id: 180,
		time: 179,
		velocity: 9.51666666666667,
		power: 6472.63616478708,
		road: 1470.83652777778,
		acceleration: 0.572777777777779
	},
	{
		id: 181,
		time: 180,
		velocity: 9.56333333333333,
		power: 3585.13368175989,
		road: 1480.33199074074,
		acceleration: 0.218148148148147
	},
	{
		id: 182,
		time: 181,
		velocity: 9.73361111111111,
		power: 3221.51103197757,
		road: 1490.02087962963,
		acceleration: 0.168703703703704
	},
	{
		id: 183,
		time: 182,
		velocity: 10.0227777777778,
		power: 4579.34601660003,
		road: 1499.94481481481,
		acceleration: 0.301388888888889
	},
	{
		id: 184,
		time: 183,
		velocity: 10.4675,
		power: 4461.29367550319,
		road: 1510.15560185185,
		acceleration: 0.272314814814816
	},
	{
		id: 185,
		time: 184,
		velocity: 10.5505555555556,
		power: 7892.74886894772,
		road: 1520.79601851852,
		acceleration: 0.586944444444443
	},
	{
		id: 186,
		time: 185,
		velocity: 11.7836111111111,
		power: 6668.24881186742,
		road: 1531.94481481481,
		acceleration: 0.429814814814817
	},
	{
		id: 187,
		time: 186,
		velocity: 11.7569444444444,
		power: 5429.03100460942,
		road: 1543.45476851852,
		acceleration: 0.292499999999999
	},
	{
		id: 188,
		time: 187,
		velocity: 11.4280555555556,
		power: 448.698669270048,
		road: 1555.02981481481,
		acceleration: -0.162314814814815
	},
	{
		id: 189,
		time: 188,
		velocity: 11.2966666666667,
		power: 1236.819904869,
		road: 1566.47967592592,
		acceleration: -0.0880555555555542
	},
	{
		id: 190,
		time: 189,
		velocity: 11.4927777777778,
		power: 6337.9415802236,
		road: 1578.07060185185,
		acceleration: 0.370185185185186
	},
	{
		id: 191,
		time: 190,
		velocity: 12.5386111111111,
		power: 9531.77045683232,
		road: 1590.15513888889,
		acceleration: 0.617037037037036
	},
	{
		id: 192,
		time: 191,
		velocity: 13.1477777777778,
		power: 9585.63387104214,
		road: 1602.83523148148,
		acceleration: 0.574074074074074
	},
	{
		id: 193,
		time: 192,
		velocity: 13.215,
		power: 3840.15416085732,
		road: 1615.84555555555,
		acceleration: 0.0863888888888873
	},
	{
		id: 194,
		time: 193,
		velocity: 12.7977777777778,
		power: -5497.47728206348,
		road: 1628.56324074074,
		acceleration: -0.671666666666663
	},
	{
		id: 195,
		time: 194,
		velocity: 11.1327777777778,
		power: -9418.44530150882,
		road: 1640.42541666667,
		acceleration: -1.03935185185185
	},
	{
		id: 196,
		time: 195,
		velocity: 10.0969444444444,
		power: -13295.0079370632,
		road: 1651.01375,
		acceleration: -1.50833333333333
	},
	{
		id: 197,
		time: 196,
		velocity: 8.27277777777778,
		power: -11123.4692274335,
		road: 1660.11930555555,
		acceleration: -1.45722222222222
	},
	{
		id: 198,
		time: 197,
		velocity: 6.76111111111111,
		power: -10804.9549672418,
		road: 1667.6637037037,
		acceleration: -1.66509259259259
	},
	{
		id: 199,
		time: 198,
		velocity: 5.10166666666667,
		power: -3817.19349614645,
		road: 1673.98291666667,
		acceleration: -0.785277777777779
	},
	{
		id: 200,
		time: 199,
		velocity: 5.91694444444444,
		power: -434.267110002892,
		road: 1679.79699074074,
		acceleration: -0.224999999999999
	},
	{
		id: 201,
		time: 200,
		velocity: 6.08611111111111,
		power: 2567.18913061284,
		road: 1685.65569444444,
		acceleration: 0.314259259259259
	},
	{
		id: 202,
		time: 201,
		velocity: 6.04444444444444,
		power: 1656.64282975728,
		road: 1691.74060185185,
		acceleration: 0.138148148148148
	},
	{
		id: 203,
		time: 202,
		velocity: 6.33138888888889,
		power: 6107.30518563099,
		road: 1698.30768518518,
		acceleration: 0.826203703703704
	},
	{
		id: 204,
		time: 203,
		velocity: 8.56472222222222,
		power: 10849.1696770163,
		road: 1705.95324074074,
		acceleration: 1.33074074074074
	},
	{
		id: 205,
		time: 204,
		velocity: 10.0366666666667,
		power: 11848.0514930313,
		road: 1714.87523148148,
		acceleration: 1.22212962962963
	},
	{
		id: 206,
		time: 205,
		velocity: 9.99777777777778,
		power: 3611.73944431364,
		road: 1724.51504629629,
		acceleration: 0.213518518518519
	},
	{
		id: 207,
		time: 206,
		velocity: 9.20527777777778,
		power: -5159.80066186475,
		road: 1733.88402777778,
		acceleration: -0.755185185185184
	},
	{
		id: 208,
		time: 207,
		velocity: 7.77111111111111,
		power: -9200.07051122498,
		road: 1742.21175925926,
		acceleration: -1.32731481481482
	},
	{
		id: 209,
		time: 208,
		velocity: 6.01583333333333,
		power: -10923.1645422594,
		road: 1748.94638888889,
		acceleration: -1.85888888888889
	},
	{
		id: 210,
		time: 209,
		velocity: 3.62861111111111,
		power: -10497.5057997169,
		road: 1753.45638888889,
		acceleration: -2.59037037037037
	},
	{
		id: 211,
		time: 210,
		velocity: 0,
		power: -3933.52474031303,
		road: 1755.66856481481,
		acceleration: -2.00527777777778
	},
	{
		id: 212,
		time: 211,
		velocity: 0,
		power: -619.923593193632,
		road: 1756.27333333333,
		acceleration: -1.20953703703704
	},
	{
		id: 213,
		time: 212,
		velocity: 0,
		power: 0,
		road: 1756.27333333333,
		acceleration: 0
	},
	{
		id: 214,
		time: 213,
		velocity: 0,
		power: 0,
		road: 1756.27333333333,
		acceleration: 0
	},
	{
		id: 215,
		time: 214,
		velocity: 0,
		power: 0,
		road: 1756.27333333333,
		acceleration: 0
	},
	{
		id: 216,
		time: 215,
		velocity: 0,
		power: 0,
		road: 1756.27333333333,
		acceleration: 0
	},
	{
		id: 217,
		time: 216,
		velocity: 0,
		power: 2.93146826133738,
		road: 1756.29208333333,
		acceleration: 0.0375
	},
	{
		id: 218,
		time: 217,
		velocity: 0.1125,
		power: 4.53069968056949,
		road: 1756.32958333333,
		acceleration: 0
	},
	{
		id: 219,
		time: 218,
		velocity: 0,
		power: 6.77791096033518,
		road: 1756.37768518518,
		acceleration: 0.0212037037037037
	},
	{
		id: 220,
		time: 219,
		velocity: 0.0636111111111111,
		power: 3.40772422530404,
		road: 1756.41763888889,
		acceleration: -0.0375
	},
	{
		id: 221,
		time: 220,
		velocity: 0,
		power: 2.56179201735657,
		road: 1756.43884259259,
		acceleration: 0
	},
	{
		id: 222,
		time: 221,
		velocity: 0,
		power: 1.06792639701105,
		road: 1756.44944444444,
		acceleration: -0.0212037037037037
	},
	{
		id: 223,
		time: 222,
		velocity: 0,
		power: 0,
		road: 1756.44944444444,
		acceleration: 0
	},
	{
		id: 224,
		time: 223,
		velocity: 0,
		power: 0,
		road: 1756.44944444444,
		acceleration: 0
	},
	{
		id: 225,
		time: 224,
		velocity: 0,
		power: 0,
		road: 1756.44944444444,
		acceleration: 0
	},
	{
		id: 226,
		time: 225,
		velocity: 0,
		power: 0,
		road: 1756.44944444444,
		acceleration: 0
	},
	{
		id: 227,
		time: 226,
		velocity: 0,
		power: 0,
		road: 1756.44944444444,
		acceleration: 0
	},
	{
		id: 228,
		time: 227,
		velocity: 0,
		power: 11.2711512651474,
		road: 1756.50101851852,
		acceleration: 0.103148148148148
	},
	{
		id: 229,
		time: 228,
		velocity: 0.309444444444444,
		power: 632.23371424725,
		road: 1757.09851851852,
		acceleration: 0.988703703703704
	},
	{
		id: 230,
		time: 229,
		velocity: 2.96611111111111,
		power: 3045.95665047041,
		road: 1758.97930555555,
		acceleration: 1.57787037037037
	},
	{
		id: 231,
		time: 230,
		velocity: 4.73361111111111,
		power: 5363.36992739763,
		road: 1762.40625,
		acceleration: 1.51444444444444
	},
	{
		id: 232,
		time: 231,
		velocity: 4.85277777777778,
		power: 3148.09170562825,
		road: 1766.89060185185,
		acceleration: 0.600370370370371
	},
	{
		id: 233,
		time: 232,
		velocity: 4.76722222222222,
		power: 301.827278019993,
		road: 1771.63856481481,
		acceleration: -0.0731481481481486
	},
	{
		id: 234,
		time: 233,
		velocity: 4.51416666666667,
		power: 2863.80832627804,
		road: 1776.58412037037,
		acceleration: 0.468333333333334
	},
	{
		id: 235,
		time: 234,
		velocity: 6.25777777777778,
		power: 5895.45292133336,
		road: 1782.23949074074,
		acceleration: 0.951296296296297
	},
	{
		id: 236,
		time: 235,
		velocity: 7.62111111111111,
		power: 11264.4741685985,
		road: 1789.15009259259,
		acceleration: 1.55916666666667
	},
	{
		id: 237,
		time: 236,
		velocity: 9.19166666666667,
		power: 13448.3309547123,
		road: 1797.59296296296,
		acceleration: 1.50537037037037
	},
	{
		id: 238,
		time: 237,
		velocity: 10.7738888888889,
		power: 11528.9588715702,
		road: 1807.32013888889,
		acceleration: 1.06324074074074
	},
	{
		id: 239,
		time: 238,
		velocity: 10.8108333333333,
		power: 9468.99124782913,
		road: 1817.95060185185,
		acceleration: 0.743333333333334
	},
	{
		id: 240,
		time: 239,
		velocity: 11.4216666666667,
		power: 7152.67646354108,
		road: 1829.18722222222,
		acceleration: 0.468981481481482
	},
	{
		id: 241,
		time: 240,
		velocity: 12.1808333333333,
		power: 9731.07071527069,
		road: 1840.98763888889,
		acceleration: 0.65861111111111
	},
	{
		id: 242,
		time: 241,
		velocity: 12.7866666666667,
		power: 10050.4713647576,
		road: 1853.43314814815,
		acceleration: 0.631574074074072
	},
	{
		id: 243,
		time: 242,
		velocity: 13.3163888888889,
		power: 12168.054027819,
		road: 1866.56736111111,
		acceleration: 0.745833333333335
	},
	{
		id: 244,
		time: 243,
		velocity: 14.4183333333333,
		power: 14782.8314032807,
		road: 1880.51101851852,
		acceleration: 0.873055555555554
	},
	{
		id: 245,
		time: 244,
		velocity: 15.4058333333333,
		power: 12981.730513437,
		road: 1895.22805555555,
		acceleration: 0.673703703703703
	},
	{
		id: 246,
		time: 245,
		velocity: 15.3375,
		power: 7426.30060270147,
		road: 1910.40921296296,
		acceleration: 0.254537037037039
	},
	{
		id: 247,
		time: 246,
		velocity: 15.1819444444444,
		power: 4415.215539718,
		road: 1925.73837962963,
		acceleration: 0.0414814814814815
	},
	{
		id: 248,
		time: 247,
		velocity: 15.5302777777778,
		power: 5883.22198726745,
		road: 1941.15717592592,
		acceleration: 0.137777777777778
	},
	{
		id: 249,
		time: 248,
		velocity: 15.7508333333333,
		power: 7489.18892567581,
		road: 1956.76351851852,
		acceleration: 0.237314814814814
	},
	{
		id: 250,
		time: 249,
		velocity: 15.8938888888889,
		power: 9246.99690158958,
		road: 1972.65787037037,
		acceleration: 0.338703703703706
	},
	{
		id: 251,
		time: 250,
		velocity: 16.5463888888889,
		power: 10913.1289744764,
		road: 1988.9337962963,
		acceleration: 0.424444444444443
	},
	{
		id: 252,
		time: 251,
		velocity: 17.0241666666667,
		power: 11932.236256253,
		road: 2005.6525462963,
		acceleration: 0.461203703703699
	},
	{
		id: 253,
		time: 252,
		velocity: 17.2775,
		power: 9356.51035519889,
		road: 2022.74203703704,
		acceleration: 0.280277777777783
	},
	{
		id: 254,
		time: 253,
		velocity: 17.3872222222222,
		power: 8024.59538146126,
		road: 2040.06546296296,
		acceleration: 0.187592592592591
	},
	{
		id: 255,
		time: 254,
		velocity: 17.5869444444444,
		power: 6104.78901986892,
		road: 2057.51597222222,
		acceleration: 0.0665740740740723
	},
	{
		id: 256,
		time: 255,
		velocity: 17.4772222222222,
		power: 5203.10948808246,
		road: 2075.00532407407,
		acceleration: 0.0111111111111128
	},
	{
		id: 257,
		time: 256,
		velocity: 17.4205555555556,
		power: 3340.94453491611,
		road: 2092.45083333333,
		acceleration: -0.098796296296296
	},
	{
		id: 258,
		time: 257,
		velocity: 17.2905555555556,
		power: 4550.32217308396,
		road: 2109.83486111111,
		acceleration: -0.0241666666666696
	},
	{
		id: 259,
		time: 258,
		velocity: 17.4047222222222,
		power: 6347.91603025095,
		road: 2127.24814814815,
		acceleration: 0.0826851851851913
	},
	{
		id: 260,
		time: 259,
		velocity: 17.6686111111111,
		power: 10070.9117218768,
		road: 2144.85069444444,
		acceleration: 0.295833333333327
	},
	{
		id: 261,
		time: 260,
		velocity: 18.1780555555556,
		power: 10855.0669175521,
		road: 2162.76361111111,
		acceleration: 0.324907407407409
	},
	{
		id: 262,
		time: 261,
		velocity: 18.3794444444444,
		power: 11057.9658542293,
		road: 2180.99833333333,
		acceleration: 0.318703703703704
	},
	{
		id: 263,
		time: 262,
		velocity: 18.6247222222222,
		power: 10466.9995139085,
		road: 2199.52699074074,
		acceleration: 0.269166666666671
	},
	{
		id: 264,
		time: 263,
		velocity: 18.9855555555556,
		power: 11030.4597807373,
		road: 2218.33314814815,
		acceleration: 0.285833333333329
	},
	{
		id: 265,
		time: 264,
		velocity: 19.2369444444444,
		power: 11264.2792954295,
		road: 2237.42388888889,
		acceleration: 0.283333333333331
	},
	{
		id: 266,
		time: 265,
		velocity: 19.4747222222222,
		power: 10593.2573810803,
		road: 2256.77291666667,
		acceleration: 0.233240740740744
	},
	{
		id: 267,
		time: 266,
		velocity: 19.6852777777778,
		power: 6764.59564918888,
		road: 2276.24930555555,
		acceleration: 0.0214814814814801
	},
	{
		id: 268,
		time: 267,
		velocity: 19.3013888888889,
		power: 2130.82643032668,
		road: 2295.62439814815,
		acceleration: -0.224074074074071
	},
	{
		id: 269,
		time: 268,
		velocity: 18.8025,
		power: -2846.25211708994,
		road: 2314.6437037037,
		acceleration: -0.487500000000004
	},
	{
		id: 270,
		time: 269,
		velocity: 18.2227777777778,
		power: -385.526454679486,
		road: 2333.24722222222,
		acceleration: -0.344074074074072
	},
	{
		id: 271,
		time: 270,
		velocity: 18.2691666666667,
		power: 4610.27073611996,
		road: 2351.65041666667,
		acceleration: -0.0565740740740708
	},
	{
		id: 272,
		time: 271,
		velocity: 18.6327777777778,
		power: 10253.6457736148,
		road: 2370.15453703704,
		acceleration: 0.258425925925927
	},
	{
		id: 273,
		time: 272,
		velocity: 18.9980555555556,
		power: 10521.2329723037,
		road: 2388.91777777778,
		acceleration: 0.25981481481481
	},
	{
		id: 274,
		time: 273,
		velocity: 19.0486111111111,
		power: 14556.8762120996,
		road: 2408.04157407407,
		acceleration: 0.461296296296297
	},
	{
		id: 275,
		time: 274,
		velocity: 20.0166666666667,
		power: 17925.1275451977,
		road: 2427.69935185185,
		acceleration: 0.606666666666666
	},
	{
		id: 276,
		time: 275,
		velocity: 20.8180555555556,
		power: 22214.2812552728,
		road: 2448.04958333333,
		acceleration: 0.778240740740742
	},
	{
		id: 277,
		time: 276,
		velocity: 21.3833333333333,
		power: 20877.7263740981,
		road: 2469.11703703704,
		acceleration: 0.656203703703707
	},
	{
		id: 278,
		time: 277,
		velocity: 21.9852777777778,
		power: 15218.851379947,
		road: 2490.68578703704,
		acceleration: 0.346388888888885
	},
	{
		id: 279,
		time: 278,
		velocity: 21.8572222222222,
		power: 13931.5098547419,
		road: 2512.5612962963,
		acceleration: 0.267129629629633
	},
	{
		id: 280,
		time: 279,
		velocity: 22.1847222222222,
		power: 18549.0342301257,
		road: 2534.80208333333,
		acceleration: 0.463425925925922
	},
	{
		id: 281,
		time: 280,
		velocity: 23.3755555555556,
		power: 18085.8333683286,
		road: 2557.48162037037,
		acceleration: 0.414074074074076
	},
	{
		id: 282,
		time: 281,
		velocity: 23.0994444444444,
		power: -255.121867415502,
		road: 2580.15398148148,
		acceleration: -0.428425925925929
	},
	{
		id: 283,
		time: 282,
		velocity: 20.8994444444444,
		power: -11578.9281111448,
		road: 2602.13782407407,
		acceleration: -0.948611111111109
	},
	{
		id: 284,
		time: 283,
		velocity: 20.5297222222222,
		power: -13666.6194511667,
		road: 2623.1199537037,
		acceleration: -1.05481481481482
	},
	{
		id: 285,
		time: 284,
		velocity: 19.935,
		power: -1861.27512765551,
		road: 2643.34773148148,
		acceleration: -0.453888888888887
	},
	{
		id: 286,
		time: 285,
		velocity: 19.5377777777778,
		power: -1190.61095776906,
		road: 2663.14310185185,
		acceleration: -0.410925925925927
	},
	{
		id: 287,
		time: 286,
		velocity: 19.2969444444444,
		power: 1515.58087258137,
		road: 2682.60328703704,
		acceleration: -0.259444444444444
	},
	{
		id: 288,
		time: 287,
		velocity: 19.1566666666667,
		power: 1636.48143101397,
		road: 2701.81060185185,
		acceleration: -0.246296296296297
	},
	{
		id: 289,
		time: 288,
		velocity: 18.7988888888889,
		power: -1712.60126115308,
		road: 2720.68333333333,
		acceleration: -0.422870370370369
	},
	{
		id: 290,
		time: 289,
		velocity: 18.0283333333333,
		power: -2996.56632793618,
		road: 2739.10037037037,
		acceleration: -0.488518518518518
	},
	{
		id: 291,
		time: 290,
		velocity: 17.6911111111111,
		power: -4614.80130261737,
		road: 2756.98449074074,
		acceleration: -0.577314814814812
	},
	{
		id: 292,
		time: 291,
		velocity: 17.0669444444444,
		power: 480.775622214761,
		road: 2774.44472222222,
		acceleration: -0.270462962962966
	},
	{
		id: 293,
		time: 292,
		velocity: 17.2169444444444,
		power: 3170.88442548293,
		road: 2791.71787037037,
		acceleration: -0.103703703703701
	},
	{
		id: 294,
		time: 293,
		velocity: 17.38,
		power: 10925.399306709,
		road: 2809.11824074074,
		acceleration: 0.358148148148146
	},
	{
		id: 295,
		time: 294,
		velocity: 18.1413888888889,
		power: 10091.5465038828,
		road: 2826.84291666667,
		acceleration: 0.290462962962962
	},
	{
		id: 296,
		time: 295,
		velocity: 18.0883333333333,
		power: 8976.67224893651,
		road: 2844.81893518518,
		acceleration: 0.212222222222223
	},
	{
		id: 297,
		time: 296,
		velocity: 18.0166666666667,
		power: 3515.54407568413,
		road: 2862.84763888889,
		acceleration: -0.106851851851854
	},
	{
		id: 298,
		time: 297,
		velocity: 17.8208333333333,
		power: 67.590685049358,
		road: 2880.67166666667,
		acceleration: -0.302499999999998
	},
	{
		id: 299,
		time: 298,
		velocity: 17.1808333333333,
		power: 629.413007085044,
		road: 2898.21277777778,
		acceleration: -0.263333333333335
	},
	{
		id: 300,
		time: 299,
		velocity: 17.2266666666667,
		power: 33.8901311163728,
		road: 2915.47555555556,
		acceleration: -0.293333333333329
	},
	{
		id: 301,
		time: 300,
		velocity: 16.9408333333333,
		power: 4433.68182546163,
		road: 2932.58097222222,
		acceleration: -0.0213888888888896
	},
	{
		id: 302,
		time: 301,
		velocity: 17.1166666666667,
		power: 5711.66891883181,
		road: 2949.7037037037,
		acceleration: 0.0560185185185169
	},
	{
		id: 303,
		time: 302,
		velocity: 17.3947222222222,
		power: 1023.08659251243,
		road: 2966.74032407407,
		acceleration: -0.228240740740741
	},
	{
		id: 304,
		time: 303,
		velocity: 16.2561111111111,
		power: -892.213946811779,
		road: 2983.49217592593,
		acceleration: -0.341296296296296
	},
	{
		id: 305,
		time: 304,
		velocity: 16.0927777777778,
		power: -7076.50129117812,
		road: 2999.70731481481,
		acceleration: -0.732129629629631
	},
	{
		id: 306,
		time: 305,
		velocity: 15.1983333333333,
		power: -1352.93079727388,
		road: 3015.37828703704,
		acceleration: -0.356203703703704
	},
	{
		id: 307,
		time: 306,
		velocity: 15.1875,
		power: -1626.68504863106,
		road: 3030.68578703704,
		acceleration: -0.370740740740741
	},
	{
		id: 308,
		time: 307,
		velocity: 14.9805555555556,
		power: 832.875260947899,
		road: 3045.70958333333,
		acceleration: -0.196666666666665
	},
	{
		id: 309,
		time: 308,
		velocity: 14.6083333333333,
		power: -689.231899394238,
		road: 3060.48537037037,
		acceleration: -0.299351851851853
	},
	{
		id: 310,
		time: 309,
		velocity: 14.2894444444444,
		power: -1708.55566034398,
		road: 3074.92703703704,
		acceleration: -0.368888888888886
	},
	{
		id: 311,
		time: 310,
		velocity: 13.8738888888889,
		power: 789.505997673576,
		road: 3089.09314814815,
		acceleration: -0.182222222222226
	},
	{
		id: 312,
		time: 311,
		velocity: 14.0616666666667,
		power: 622.078780480072,
		road: 3103.07263888889,
		acceleration: -0.191018518518518
	},
	{
		id: 313,
		time: 312,
		velocity: 13.7163888888889,
		power: 1946.31953633057,
		road: 3116.9125462963,
		acceleration: -0.0881481481481465
	},
	{
		id: 314,
		time: 313,
		velocity: 13.6094444444444,
		power: 200.880543756506,
		road: 3130.59953703704,
		acceleration: -0.217685185185186
	},
	{
		id: 315,
		time: 314,
		velocity: 13.4086111111111,
		power: 1001.78079238047,
		road: 3144.10143518518,
		acceleration: -0.1525
	},
	{
		id: 316,
		time: 315,
		velocity: 13.2588888888889,
		power: 896.595694606443,
		road: 3157.44833333333,
		acceleration: -0.157499999999999
	},
	{
		id: 317,
		time: 316,
		velocity: 13.1369444444444,
		power: 1858.85698856311,
		road: 3170.67703703704,
		acceleration: -0.0788888888888888
	},
	{
		id: 318,
		time: 317,
		velocity: 13.1719444444444,
		power: 3130.65506791986,
		road: 3183.87768518518,
		acceleration: 0.0227777777777778
	},
	{
		id: 319,
		time: 318,
		velocity: 13.3272222222222,
		power: 7319.87075627111,
		road: 3197.26194444444,
		acceleration: 0.344444444444445
	},
	{
		id: 320,
		time: 319,
		velocity: 14.1702777777778,
		power: 8517.85227905095,
		road: 3211.02537037037,
		acceleration: 0.413888888888888
	},
	{
		id: 321,
		time: 320,
		velocity: 14.4136111111111,
		power: 9605.33057103181,
		road: 3225.22916666667,
		acceleration: 0.466851851851853
	},
	{
		id: 322,
		time: 321,
		velocity: 14.7277777777778,
		power: 5568.08928544219,
		road: 3239.74407407407,
		acceleration: 0.15537037037037
	},
	{
		id: 323,
		time: 322,
		velocity: 14.6363888888889,
		power: 2603.90084736094,
		road: 3254.30675925926,
		acceleration: -0.0598148148148177
	},
	{
		id: 324,
		time: 323,
		velocity: 14.2341666666667,
		power: -207.024165996756,
		road: 3268.70981481481,
		acceleration: -0.259444444444442
	},
	{
		id: 325,
		time: 324,
		velocity: 13.9494444444444,
		power: -191.606271295883,
		road: 3282.85592592593,
		acceleration: -0.254444444444445
	},
	{
		id: 326,
		time: 325,
		velocity: 13.8730555555556,
		power: -820.696751066593,
		road: 3296.72587962963,
		acceleration: -0.297870370370369
	},
	{
		id: 327,
		time: 326,
		velocity: 13.3405555555556,
		power: -161.810255305193,
		road: 3310.32481481481,
		acceleration: -0.244166666666667
	},
	{
		id: 328,
		time: 327,
		velocity: 13.2169444444444,
		power: -2314.36510618984,
		road: 3323.59694444444,
		acceleration: -0.409444444444445
	},
	{
		id: 329,
		time: 328,
		velocity: 12.6447222222222,
		power: -373.815830768078,
		road: 3336.53828703704,
		acceleration: -0.25212962962963
	},
	{
		id: 330,
		time: 329,
		velocity: 12.5841666666667,
		power: -1689.54597132168,
		road: 3349.17476851852,
		acceleration: -0.357592592592594
	},
	{
		id: 331,
		time: 330,
		velocity: 12.1441666666667,
		power: 954.700281483214,
		road: 3361.56583333333,
		acceleration: -0.133240740740741
	},
	{
		id: 332,
		time: 331,
		velocity: 12.245,
		power: 2045.97947729685,
		road: 3373.87101851852,
		acceleration: -0.0385185185185186
	},
	{
		id: 333,
		time: 332,
		velocity: 12.4686111111111,
		power: -351.988501254781,
		road: 3386.03634259259,
		acceleration: -0.241203703703704
	},
	{
		id: 334,
		time: 333,
		velocity: 11.4205555555556,
		power: -2977.8698027917,
		road: 3397.84583333333,
		acceleration: -0.470462962962964
	},
	{
		id: 335,
		time: 334,
		velocity: 10.8336111111111,
		power: -7172.11880119499,
		road: 3408.98365740741,
		acceleration: -0.87287037037037
	},
	{
		id: 336,
		time: 335,
		velocity: 9.85,
		power: -7613.96018819218,
		road: 3419.20083333333,
		acceleration: -0.968425925925924
	},
	{
		id: 337,
		time: 336,
		velocity: 8.51527777777778,
		power: -6564.48482162832,
		road: 3428.47416666667,
		acceleration: -0.91925925925926
	},
	{
		id: 338,
		time: 337,
		velocity: 8.07583333333333,
		power: -5629.23155138996,
		road: 3436.85138888889,
		acceleration: -0.872962962962963
	},
	{
		id: 339,
		time: 338,
		velocity: 7.23111111111111,
		power: -4094.62131696447,
		road: 3444.42824074074,
		acceleration: -0.727777777777778
	},
	{
		id: 340,
		time: 339,
		velocity: 6.33194444444444,
		power: -4694.91052452931,
		road: 3451.2000462963,
		acceleration: -0.882314814814814
	},
	{
		id: 341,
		time: 340,
		velocity: 5.42888888888889,
		power: -2252.24375559067,
		road: 3457.2612037037,
		acceleration: -0.538981481481482
	},
	{
		id: 342,
		time: 341,
		velocity: 5.61416666666667,
		power: -4644.25153998858,
		road: 3462.5162962963,
		acceleration: -1.07314814814815
	},
	{
		id: 343,
		time: 342,
		velocity: 3.1125,
		power: -3696.90021540683,
		road: 3466.70097222222,
		acceleration: -1.06768518518519
	},
	{
		id: 344,
		time: 343,
		velocity: 2.22583333333333,
		power: -3914.28796639404,
		road: 3469.56564814815,
		acceleration: -1.57231481481481
	},
	{
		id: 345,
		time: 344,
		velocity: 0.897222222222222,
		power: -1100.1136646695,
		road: 3471.23138888889,
		acceleration: -0.825555555555555
	},
	{
		id: 346,
		time: 345,
		velocity: 0.635833333333333,
		power: -513.261349569844,
		road: 3472.11337962963,
		acceleration: -0.741944444444444
	},
	{
		id: 347,
		time: 346,
		velocity: 0,
		power: -58.7375064247666,
		road: 3472.47486111111,
		acceleration: -0.299074074074074
	},
	{
		id: 348,
		time: 347,
		velocity: 0,
		power: -8.47476593567252,
		road: 3472.58083333333,
		acceleration: -0.211944444444444
	},
	{
		id: 349,
		time: 348,
		velocity: 0,
		power: 0,
		road: 3472.58083333333,
		acceleration: 0
	},
	{
		id: 350,
		time: 349,
		velocity: 0,
		power: 0,
		road: 3472.58083333333,
		acceleration: 0
	},
	{
		id: 351,
		time: 350,
		velocity: 0,
		power: 0,
		road: 3472.58083333333,
		acceleration: 0
	},
	{
		id: 352,
		time: 351,
		velocity: 0,
		power: 0,
		road: 3472.58083333333,
		acceleration: 0
	},
	{
		id: 353,
		time: 352,
		velocity: 0,
		power: 0,
		road: 3472.58083333333,
		acceleration: 0
	},
	{
		id: 354,
		time: 353,
		velocity: 0,
		power: 0,
		road: 3472.58083333333,
		acceleration: 0
	},
	{
		id: 355,
		time: 354,
		velocity: 0,
		power: 165.845325607588,
		road: 3472.84648148148,
		acceleration: 0.531296296296296
	},
	{
		id: 356,
		time: 355,
		velocity: 1.59388888888889,
		power: 1830.73940814708,
		road: 3474.0899537037,
		acceleration: 1.42435185185185
	},
	{
		id: 357,
		time: 356,
		velocity: 4.27305555555556,
		power: 5444.98712292141,
		road: 3476.97407407407,
		acceleration: 1.85694444444444
	},
	{
		id: 358,
		time: 357,
		velocity: 5.57083333333333,
		power: 7178.04399109894,
		road: 3481.5437962963,
		acceleration: 1.51425925925926
	},
	{
		id: 359,
		time: 358,
		velocity: 6.13666666666667,
		power: 5753.97431292322,
		road: 3487.32138888889,
		acceleration: 0.901481481481481
	},
	{
		id: 360,
		time: 359,
		velocity: 6.9775,
		power: 8507.64642161141,
		road: 3494.12981481481,
		acceleration: 1.16018518518519
	},
	{
		id: 361,
		time: 360,
		velocity: 9.05138888888889,
		power: 12465.8042127087,
		road: 3502.24310185185,
		acceleration: 1.44953703703704
	},
	{
		id: 362,
		time: 361,
		velocity: 10.4852777777778,
		power: 14247.6032552531,
		road: 3511.7762037037,
		acceleration: 1.39009259259259
	},
	{
		id: 363,
		time: 362,
		velocity: 11.1477777777778,
		power: 9503.67739979401,
		road: 3522.37912037037,
		acceleration: 0.749537037037037
	},
	{
		id: 364,
		time: 363,
		velocity: 11.3,
		power: 8516.90799841477,
		road: 3533.65337962963,
		acceleration: 0.593148148148147
	},
	{
		id: 365,
		time: 364,
		velocity: 12.2647222222222,
		power: 8501.53171023981,
		road: 3545.49722222222,
		acceleration: 0.546018518518517
	},
	{
		id: 366,
		time: 365,
		velocity: 12.7858333333333,
		power: 11363.4812618557,
		road: 3557.98328703704,
		acceleration: 0.738425925925927
	},
	{
		id: 367,
		time: 366,
		velocity: 13.5152777777778,
		power: 10951.9027310688,
		road: 3571.1612037037,
		acceleration: 0.645277777777778
	},
	{
		id: 368,
		time: 367,
		velocity: 14.2005555555556,
		power: 13085.5678247546,
		road: 3585.0375,
		acceleration: 0.751481481481481
	},
	{
		id: 369,
		time: 368,
		velocity: 15.0402777777778,
		power: 16253.1290426421,
		road: 3599.74324074074,
		acceleration: 0.907407407407408
	},
	{
		id: 370,
		time: 369,
		velocity: 16.2375,
		power: 19531.2318971544,
		road: 3615.42125,
		acceleration: 1.03712962962963
	},
	{
		id: 371,
		time: 370,
		velocity: 17.3119444444444,
		power: 13276.6520026565,
		road: 3631.89884259259,
		acceleration: 0.562037037037033
	},
	{
		id: 372,
		time: 371,
		velocity: 16.7263888888889,
		power: 7530.00565195756,
		road: 3648.74736111111,
		acceleration: 0.179814814814815
	},
	{
		id: 373,
		time: 372,
		velocity: 16.7769444444444,
		power: 1818.89908052278,
		road: 3665.59847222222,
		acceleration: -0.174629629629628
	},
	{
		id: 374,
		time: 373,
		velocity: 16.7880555555556,
		power: 5793.16757902197,
		road: 3682.39925925926,
		acceleration: 0.0739814814814821
	},
	{
		id: 375,
		time: 374,
		velocity: 16.9483333333333,
		power: 6849.86124163068,
		road: 3699.30458333333,
		acceleration: 0.135092592592596
	},
	{
		id: 376,
		time: 375,
		velocity: 17.1822222222222,
		power: 7150.93405065523,
		road: 3716.35111111111,
		acceleration: 0.147314814814816
	},
	{
		id: 377,
		time: 376,
		velocity: 17.23,
		power: 4679.81204913366,
		road: 3733.46791666666,
		acceleration: -0.0067592592592618
	},
	{
		id: 378,
		time: 377,
		velocity: 16.9280555555556,
		power: 2404.54185516061,
		road: 3750.50953703704,
		acceleration: -0.143611111111113
	},
	{
		id: 379,
		time: 378,
		velocity: 16.7513888888889,
		power: 1801.6540996727,
		road: 3767.39111111111,
		acceleration: -0.176481481481478
	},
	{
		id: 380,
		time: 379,
		velocity: 16.7005555555556,
		power: 6339.17510570201,
		road: 3784.2374537037,
		acceleration: 0.106018518518514
	},
	{
		id: 381,
		time: 380,
		velocity: 17.2461111111111,
		power: 10619.2721568786,
		road: 3801.31587962963,
		acceleration: 0.358148148148146
	},
	{
		id: 382,
		time: 381,
		velocity: 17.8258333333333,
		power: 12944.0723627265,
		road: 3818.81004629629,
		acceleration: 0.473333333333336
	},
	{
		id: 383,
		time: 382,
		velocity: 18.1205555555556,
		power: 9937.75351753583,
		road: 3836.67777777778,
		acceleration: 0.273796296296297
	},
	{
		id: 384,
		time: 383,
		velocity: 18.0675,
		power: 8211.48592146884,
		road: 3854.7637037037,
		acceleration: 0.162592592592592
	},
	{
		id: 385,
		time: 384,
		velocity: 18.3136111111111,
		power: 5855.5141148882,
		road: 3872.94236111111,
		acceleration: 0.0228703703703736
	},
	{
		id: 386,
		time: 385,
		velocity: 18.1891666666667,
		power: 6384.7233919816,
		road: 3891.15833333333,
		acceleration: 0.0517592592592564
	},
	{
		id: 387,
		time: 386,
		velocity: 18.2227777777778,
		power: 5003.76738900897,
		road: 3909.38625,
		acceleration: -0.0278703703703691
	},
	{
		id: 388,
		time: 387,
		velocity: 18.23,
		power: 8165.15274015673,
		road: 3927.67546296296,
		acceleration: 0.150462962962962
	},
	{
		id: 389,
		time: 388,
		velocity: 18.6405555555556,
		power: 8774.97209395808,
		road: 3946.12861111111,
		acceleration: 0.177407407407404
	},
	{
		id: 390,
		time: 389,
		velocity: 18.755,
		power: 8966.79000764162,
		road: 3964.76027777778,
		acceleration: 0.17962962962963
	},
	{
		id: 391,
		time: 390,
		velocity: 18.7688888888889,
		power: 7008.7673947716,
		road: 3983.51412037037,
		acceleration: 0.0647222222222226
	},
	{
		id: 392,
		time: 391,
		velocity: 18.8347222222222,
		power: 6065.97888046761,
		road: 4002.30569444444,
		acceleration: 0.0107407407407436
	},
	{
		id: 393,
		time: 392,
		velocity: 18.7872222222222,
		power: 5075.53245528778,
		road: 4021.08078703704,
		acceleration: -0.0437037037037058
	},
	{
		id: 394,
		time: 393,
		velocity: 18.6377777777778,
		power: 4683.74056583037,
		road: 4039.80222222222,
		acceleration: -0.0636111111111077
	},
	{
		id: 395,
		time: 394,
		velocity: 18.6438888888889,
		power: 3427.43105349065,
		road: 4058.42652777778,
		acceleration: -0.130648148148151
	},
	{
		id: 396,
		time: 395,
		velocity: 18.3952777777778,
		power: 4251.82032116965,
		road: 4076.94509259259,
		acceleration: -0.0808333333333344
	},
	{
		id: 397,
		time: 396,
		velocity: 18.3952777777778,
		power: 2441.5018553437,
		road: 4095.33361111111,
		acceleration: -0.179259259259258
	},
	{
		id: 398,
		time: 397,
		velocity: 18.1061111111111,
		power: 5889.44864910335,
		road: 4113.64236111111,
		acceleration: 0.0197222222222209
	},
	{
		id: 399,
		time: 398,
		velocity: 18.4544444444444,
		power: 7878.5014506399,
		road: 4132.02587962963,
		acceleration: 0.129814814814814
	},
	{
		id: 400,
		time: 399,
		velocity: 18.7847222222222,
		power: 10007.8251700333,
		road: 4150.59490740741,
		acceleration: 0.241203703703707
	},
	{
		id: 401,
		time: 400,
		velocity: 18.8297222222222,
		power: 10441.000764851,
		road: 4169.41083333333,
		acceleration: 0.252592592592592
	},
	{
		id: 402,
		time: 401,
		velocity: 19.2122222222222,
		power: 8518.25124958533,
		road: 4188.42148148148,
		acceleration: 0.136851851851851
	},
	{
		id: 403,
		time: 402,
		velocity: 19.1952777777778,
		power: 9524.74339674952,
		road: 4207.59268518518,
		acceleration: 0.184259259259257
	},
	{
		id: 404,
		time: 403,
		velocity: 19.3825,
		power: 7924.3556282901,
		road: 4226.90152777778,
		acceleration: 0.0910185185185171
	},
	{
		id: 405,
		time: 404,
		velocity: 19.4852777777778,
		power: 9378.60809328381,
		road: 4246.33763888889,
		acceleration: 0.163518518518522
	},
	{
		id: 406,
		time: 405,
		velocity: 19.6858333333333,
		power: 8185.59297933241,
		road: 4265.90236111111,
		acceleration: 0.093703703703703
	},
	{
		id: 407,
		time: 406,
		velocity: 19.6636111111111,
		power: 4246.01458840825,
		road: 4285.45587962963,
		acceleration: -0.11611111111111
	},
	{
		id: 408,
		time: 407,
		velocity: 19.1369444444444,
		power: 1393.39348871289,
		road: 4304.81958333333,
		acceleration: -0.263518518518524
	},
	{
		id: 409,
		time: 408,
		velocity: 18.8952777777778,
		power: 280.223463191192,
		road: 4323.89300925926,
		acceleration: -0.317037037037036
	},
	{
		id: 410,
		time: 409,
		velocity: 18.7125,
		power: 1879.99390811341,
		road: 4342.69680555555,
		acceleration: -0.222222222222221
	},
	{
		id: 411,
		time: 410,
		velocity: 18.4702777777778,
		power: 2862.70959056583,
		road: 4361.30851851852,
		acceleration: -0.161944444444444
	},
	{
		id: 412,
		time: 411,
		velocity: 18.4094444444444,
		power: 2919.03229181762,
		road: 4379.76222222222,
		acceleration: -0.154074074074074
	},
	{
		id: 413,
		time: 412,
		velocity: 18.2502777777778,
		power: 4442.48850508884,
		road: 4398.10685185185,
		acceleration: -0.0640740740740711
	},
	{
		id: 414,
		time: 413,
		velocity: 18.2780555555556,
		power: 4664.2523118532,
		road: 4416.39472222222,
		acceleration: -0.0494444444444468
	},
	{
		id: 415,
		time: 414,
		velocity: 18.2611111111111,
		power: 6756.27698180892,
		road: 4434.69268518518,
		acceleration: 0.069629629629631
	},
	{
		id: 416,
		time: 415,
		velocity: 18.4591666666667,
		power: 6408.77599992817,
		road: 4453.04916666666,
		acceleration: 0.0474074074074053
	},
	{
		id: 417,
		time: 416,
		velocity: 18.4202777777778,
		power: 6622.59563005377,
		road: 4471.45805555555,
		acceleration: 0.0574074074074069
	},
	{
		id: 418,
		time: 417,
		velocity: 18.4333333333333,
		power: 4238.99575391776,
		road: 4489.85689814815,
		acceleration: -0.0775000000000006
	},
	{
		id: 419,
		time: 418,
		velocity: 18.2266666666667,
		power: 4893.85137442447,
		road: 4508.19787037037,
		acceleration: -0.0382407407407399
	},
	{
		id: 420,
		time: 419,
		velocity: 18.3055555555556,
		power: 4988.66635132718,
		road: 4526.50393518518,
		acceleration: -0.0315740740740722
	},
	{
		id: 421,
		time: 420,
		velocity: 18.3386111111111,
		power: 6151.5791810524,
		road: 4544.81157407407,
		acceleration: 0.034722222222225
	},
	{
		id: 422,
		time: 421,
		velocity: 18.3308333333333,
		power: 4866.76562752969,
		road: 4563.11731481481,
		acceleration: -0.0385185185185222
	},
	{
		id: 423,
		time: 422,
		velocity: 18.19,
		power: 3932.42445025846,
		road: 4581.35893518518,
		acceleration: -0.0897222222222247
	},
	{
		id: 424,
		time: 423,
		velocity: 18.0694444444444,
		power: 3147.62745983171,
		road: 4599.49004629629,
		acceleration: -0.131296296296295
	},
	{
		id: 425,
		time: 424,
		velocity: 17.9369444444444,
		power: 3298.95078784124,
		road: 4617.4961574074,
		acceleration: -0.118703703703705
	},
	{
		id: 426,
		time: 425,
		velocity: 17.8338888888889,
		power: 1042.07297205508,
		road: 4635.32023148148,
		acceleration: -0.245370370370367
	},
	{
		id: 427,
		time: 426,
		velocity: 17.3333333333333,
		power: -123.866813854874,
		road: 4652.8674537037,
		acceleration: -0.30833333333333
	},
	{
		id: 428,
		time: 427,
		velocity: 17.0119444444444,
		power: 663.31527001388,
		road: 4670.13287037037,
		acceleration: -0.255277777777785
	},
	{
		id: 429,
		time: 428,
		velocity: 17.0680555555556,
		power: 3341.40774568677,
		road: 4687.22675925926,
		acceleration: -0.0877777777777737
	},
	{
		id: 430,
		time: 429,
		velocity: 17.07,
		power: 6250.88541168431,
		road: 4704.32180555555,
		acceleration: 0.0900925925925939
	},
	{
		id: 431,
		time: 430,
		velocity: 17.2822222222222,
		power: 8249.59739960749,
		road: 4721.56444444444,
		acceleration: 0.205092592592589
	},
	{
		id: 432,
		time: 431,
		velocity: 17.6833333333333,
		power: 9128.99336485514,
		road: 4739.03300925926,
		acceleration: 0.24675925925926
	},
	{
		id: 433,
		time: 432,
		velocity: 17.8102777777778,
		power: 8107.91991038354,
		road: 4756.71277777778,
		acceleration: 0.175648148148149
	},
	{
		id: 434,
		time: 433,
		velocity: 17.8091666666667,
		power: 5076.15269204655,
		road: 4774.47703703703,
		acceleration: -0.00666666666666771
	},
	{
		id: 435,
		time: 434,
		velocity: 17.6633333333333,
		power: 3299.8775658289,
		road: 4792.18328703703,
		acceleration: -0.109351851851848
	},
	{
		id: 436,
		time: 435,
		velocity: 17.4822222222222,
		power: 41.6447682677043,
		road: 4809.68606481481,
		acceleration: -0.297592592592597
	},
	{
		id: 437,
		time: 436,
		velocity: 16.9163888888889,
		power: -971.73825429649,
		road: 4826.86361111111,
		acceleration: -0.352870370370368
	},
	{
		id: 438,
		time: 437,
		velocity: 16.6047222222222,
		power: -2586.95278599472,
		road: 4843.64106481481,
		acceleration: -0.447314814814817
	},
	{
		id: 439,
		time: 438,
		velocity: 16.1402777777778,
		power: -772.560284342625,
		road: 4860.03078703703,
		acceleration: -0.328148148148145
	},
	{
		id: 440,
		time: 439,
		velocity: 15.9319444444444,
		power: -2294.01123169526,
		road: 4876.04550925926,
		acceleration: -0.421851851851853
	},
	{
		id: 441,
		time: 440,
		velocity: 15.3391666666667,
		power: -2031.73335090941,
		road: 4891.64884259259,
		acceleration: -0.400925925925925
	},
	{
		id: 442,
		time: 441,
		velocity: 14.9375,
		power: -4743.60156677636,
		road: 4906.75935185185,
		acceleration: -0.584722222222224
	},
	{
		id: 443,
		time: 442,
		velocity: 14.1777777777778,
		power: -4106.8677036119,
		road: 4921.30634259259,
		acceleration: -0.542314814814814
	},
	{
		id: 444,
		time: 443,
		velocity: 13.7122222222222,
		power: -3882.76287738828,
		road: 4935.31800925926,
		acceleration: -0.528333333333332
	},
	{
		id: 445,
		time: 444,
		velocity: 13.3525,
		power: -4934.30173539751,
		road: 4948.75856481481,
		acceleration: -0.613888888888891
	},
	{
		id: 446,
		time: 445,
		velocity: 12.3361111111111,
		power: -6198.00283692457,
		road: 4961.52814814815,
		acceleration: -0.728055555555553
	},
	{
		id: 447,
		time: 446,
		velocity: 11.5280555555556,
		power: -8320.71585395934,
		road: 4973.46435185185,
		acceleration: -0.938703703703705
	},
	{
		id: 448,
		time: 447,
		velocity: 10.5363888888889,
		power: -7114.75210536063,
		road: 4984.49486111111,
		acceleration: -0.872685185185185
	},
	{
		id: 449,
		time: 448,
		velocity: 9.71805555555556,
		power: -8852.14515809898,
		road: 4994.53412037037,
		acceleration: -1.10981481481482
	},
	{
		id: 450,
		time: 449,
		velocity: 8.19861111111111,
		power: -8992.34970485245,
		road: 5003.3999074074,
		acceleration: -1.23712962962963
	},
	{
		id: 451,
		time: 450,
		velocity: 6.825,
		power: -8708.99705442701,
		road: 5010.96199074074,
		acceleration: -1.37027777777778
	},
	{
		id: 452,
		time: 451,
		velocity: 5.60722222222222,
		power: -6206.18561270668,
		road: 5017.24453703703,
		acceleration: -1.1887962962963
	},
	{
		id: 453,
		time: 452,
		velocity: 4.63222222222222,
		power: -3755.28944018555,
		road: 5022.48407407407,
		acceleration: -0.897222222222222
	},
	{
		id: 454,
		time: 453,
		velocity: 4.13333333333333,
		power: -2521.43254886411,
		road: 5026.90555555555,
		acceleration: -0.738888888888889
	},
	{
		id: 455,
		time: 454,
		velocity: 3.39055555555556,
		power: -1747.16910325738,
		road: 5030.64379629629,
		acceleration: -0.627592592592592
	},
	{
		id: 456,
		time: 455,
		velocity: 2.74944444444444,
		power: -2825.92179704809,
		road: 5033.47652777777,
		acceleration: -1.18342592592593
	},
	{
		id: 457,
		time: 456,
		velocity: 0.583055555555556,
		power: -1590.81232534906,
		road: 5035.1524537037,
		acceleration: -1.13018518518519
	},
	{
		id: 458,
		time: 457,
		velocity: 0,
		power: -487.752411274363,
		road: 5035.80504629629,
		acceleration: -0.916481481481481
	},
	{
		id: 459,
		time: 458,
		velocity: 0,
		power: -6.15171346653671,
		road: 5035.90222222222,
		acceleration: -0.194351851851852
	},
	{
		id: 460,
		time: 459,
		velocity: 0,
		power: 0,
		road: 5035.90222222222,
		acceleration: 0
	},
	{
		id: 461,
		time: 460,
		velocity: 0,
		power: 0,
		road: 5035.90222222222,
		acceleration: 0
	},
	{
		id: 462,
		time: 461,
		velocity: 0,
		power: 52.7679068067641,
		road: 5036.04023148148,
		acceleration: 0.276018518518519
	},
	{
		id: 463,
		time: 462,
		velocity: 0.828055555555556,
		power: 810.198019108471,
		road: 5036.80856481481,
		acceleration: 0.98462962962963
	},
	{
		id: 464,
		time: 463,
		velocity: 2.95388888888889,
		power: 2772.84194501692,
		road: 5038.75523148148,
		acceleration: 1.37203703703704
	},
	{
		id: 465,
		time: 464,
		velocity: 4.11611111111111,
		power: 5372.16236294379,
		road: 5042.15351851851,
		acceleration: 1.5312037037037
	},
	{
		id: 466,
		time: 465,
		velocity: 5.42166666666667,
		power: 4742.86225966785,
		road: 5046.78648148148,
		acceleration: 0.938148148148148
	},
	{
		id: 467,
		time: 466,
		velocity: 5.76833333333333,
		power: 5779.1643825023,
		road: 5052.36138888888,
		acceleration: 0.945740740740741
	},
	{
		id: 468,
		time: 467,
		velocity: 6.95333333333333,
		power: 6674.66641256632,
		road: 5058.87249999999,
		acceleration: 0.926666666666667
	},
	{
		id: 469,
		time: 468,
		velocity: 8.20166666666667,
		power: 8756.69818729081,
		road: 5066.38023148148,
		acceleration: 1.06657407407407
	},
	{
		id: 470,
		time: 469,
		velocity: 8.96805555555556,
		power: 10603.861872561,
		road: 5074.98388888888,
		acceleration: 1.12527777777778
	},
	{
		id: 471,
		time: 470,
		velocity: 10.3291666666667,
		power: 8280.94710622574,
		road: 5084.51680555555,
		acceleration: 0.73324074074074
	},
	{
		id: 472,
		time: 471,
		velocity: 10.4013888888889,
		power: 7136.66422777824,
		road: 5094.69148148148,
		acceleration: 0.550277777777778
	},
	{
		id: 473,
		time: 472,
		velocity: 10.6188888888889,
		power: 4297.66865753212,
		road: 5105.25944444444,
		acceleration: 0.236296296296297
	},
	{
		id: 474,
		time: 473,
		velocity: 11.0380555555556,
		power: 8310.49768848897,
		road: 5116.24458333333,
		acceleration: 0.598055555555554
	},
	{
		id: 475,
		time: 474,
		velocity: 12.1955555555556,
		power: 11321.1809752017,
		road: 5127.93421296296,
		acceleration: 0.810925925925927
	},
	{
		id: 476,
		time: 475,
		velocity: 13.0516666666667,
		power: 12960.272731154,
		road: 5140.46333333333,
		acceleration: 0.868055555555555
	},
	{
		id: 477,
		time: 476,
		velocity: 13.6422222222222,
		power: 14533.6679447764,
		road: 5153.87935185185,
		acceleration: 0.905740740740741
	},
	{
		id: 478,
		time: 477,
		velocity: 14.9127777777778,
		power: 16056.9095049703,
		road: 5168.21296296296,
		acceleration: 0.929444444444442
	},
	{
		id: 479,
		time: 478,
		velocity: 15.84,
		power: 21145.6275802708,
		road: 5183.59972222222,
		acceleration: 1.17685185185185
	},
	{
		id: 480,
		time: 479,
		velocity: 17.1727777777778,
		power: 30406.8351438276,
		road: 5200.37898148148,
		acceleration: 1.60814814814815
	},
	{
		id: 481,
		time: 480,
		velocity: 19.7372222222222,
		power: 41682.3185813169,
		road: 5218.97166666666,
		acceleration: 2.0187037037037
	},
	{
		id: 482,
		time: 481,
		velocity: 21.8961111111111,
		power: 47378.1439263209,
		road: 5239.5887037037,
		acceleration: 2.03
	},
	{
		id: 483,
		time: 482,
		velocity: 23.2627777777778,
		power: 50371.8963108305,
		road: 5262.17523148148,
		acceleration: 1.90898148148148
	},
	{
		id: 484,
		time: 483,
		velocity: 25.4641666666667,
		power: 44936.6984112247,
		road: 5286.45027777777,
		acceleration: 1.46805555555556
	},
	{
		id: 485,
		time: 484,
		velocity: 26.3002777777778,
		power: 49442.5192157039,
		road: 5312.2074074074,
		acceleration: 1.49611111111111
	},
	{
		id: 486,
		time: 485,
		velocity: 27.7511111111111,
		power: 41665.4083864085,
		road: 5339.24439814814,
		acceleration: 1.06361111111111
	},
	{
		id: 487,
		time: 486,
		velocity: 28.655,
		power: 38328.0005137162,
		road: 5367.24050925926,
		acceleration: 0.854629629629631
	},
	{
		id: 488,
		time: 487,
		velocity: 28.8641666666667,
		power: 21990.2919467094,
		road: 5395.77194444444,
		acceleration: 0.216018518518521
	},
	{
		id: 489,
		time: 488,
		velocity: 28.3991666666667,
		power: 12283.0680992817,
		road: 5424.34180555555,
		acceleration: -0.139166666666668
	},
	{
		id: 490,
		time: 489,
		velocity: 28.2375,
		power: 3578.03203363835,
		road: 5452.61958333333,
		acceleration: -0.445000000000004
	},
	{
		id: 491,
		time: 490,
		velocity: 27.5291666666667,
		power: 3705.44981096475,
		road: 5480.46259259259,
		acceleration: -0.424537037037037
	},
	{
		id: 492,
		time: 491,
		velocity: 27.1255555555556,
		power: -947.848483942852,
		road: 5507.80166666666,
		acceleration: -0.583333333333332
	},
	{
		id: 493,
		time: 492,
		velocity: 26.4875,
		power: -600.270440572942,
		road: 5534.57236111111,
		acceleration: -0.553425925925925
	},
	{
		id: 494,
		time: 493,
		velocity: 25.8688888888889,
		power: 1120.47179270815,
		road: 5560.83106481481,
		acceleration: -0.470555555555556
	},
	{
		id: 495,
		time: 494,
		velocity: 25.7138888888889,
		power: 732.672721712514,
		road: 5586.6186574074,
		acceleration: -0.471666666666668
	},
	{
		id: 496,
		time: 495,
		velocity: 25.0725,
		power: 8407.30757922742,
		road: 5612.09592592592,
		acceleration: -0.148981481481481
	},
	{
		id: 497,
		time: 496,
		velocity: 25.4219444444444,
		power: 14673.2147184768,
		road: 5637.55268518518,
		acceleration: 0.107962962962969
	},
	{
		id: 498,
		time: 497,
		velocity: 26.0377777777778,
		power: 15405.4682691059,
		road: 5663.12916666666,
		acceleration: 0.131481481481476
	},
	{
		id: 499,
		time: 498,
		velocity: 25.4669444444444,
		power: -4822.75188326654,
		road: 5688.42856481481,
		acceleration: -0.685648148148147
	},
	{
		id: 500,
		time: 499,
		velocity: 23.365,
		power: -27927.2990708755,
		road: 5712.5549537037,
		acceleration: -1.66037037037037
	},
	{
		id: 501,
		time: 500,
		velocity: 21.0566666666667,
		power: -37872.4321145005,
		road: 5734.75949074074,
		acceleration: -2.18333333333334
	},
	{
		id: 502,
		time: 501,
		velocity: 18.9169444444444,
		power: -38511.5234122385,
		road: 5754.68800925925,
		acceleration: -2.3687037037037
	},
	{
		id: 503,
		time: 502,
		velocity: 16.2588888888889,
		power: -38562.3498073005,
		road: 5772.12675925925,
		acceleration: -2.61083333333334
	},
	{
		id: 504,
		time: 503,
		velocity: 13.2241666666667,
		power: -30569.8114011248,
		road: 5787.06199074074,
		acceleration: -2.3962037037037
	},
	{
		id: 505,
		time: 504,
		velocity: 11.7283333333333,
		power: -19714.8649296753,
		road: 5799.88319444444,
		acceleration: -1.83185185185185
	},
	{
		id: 506,
		time: 505,
		velocity: 10.7633333333333,
		power: -7827.11082643031,
		road: 5811.32921296296,
		acceleration: -0.918518518518519
	},
	{
		id: 507,
		time: 506,
		velocity: 10.4686111111111,
		power: -364.782172936622,
		road: 5822.20134259259,
		acceleration: -0.22925925925926
	},
	{
		id: 508,
		time: 507,
		velocity: 11.0405555555556,
		power: 3991.30936660262,
		road: 5833.05481481481,
		acceleration: 0.191944444444445
	},
	{
		id: 509,
		time: 508,
		velocity: 11.3391666666667,
		power: 5797.64904719155,
		road: 5844.17898148148,
		acceleration: 0.349444444444446
	},
	{
		id: 510,
		time: 509,
		velocity: 11.5169444444444,
		power: 3291.7971029974,
		road: 5855.5299074074,
		acceleration: 0.104074074074072
	},
	{
		id: 511,
		time: 510,
		velocity: 11.3527777777778,
		power: 1563.65994039613,
		road: 5866.90476851852,
		acceleration: -0.0562037037037033
	},
	{
		id: 512,
		time: 511,
		velocity: 11.1705555555556,
		power: 1280.21694765436,
		road: 5878.2111574074,
		acceleration: -0.0807407407407421
	},
	{
		id: 513,
		time: 512,
		velocity: 11.2747222222222,
		power: 1886.21790377298,
		road: 5889.46564814815,
		acceleration: -0.0230555555555529
	},
	{
		id: 514,
		time: 513,
		velocity: 11.2836111111111,
		power: 3568.17465731326,
		road: 5900.77430555555,
		acceleration: 0.131388888888891
	},
	{
		id: 515,
		time: 514,
		velocity: 11.5647222222222,
		power: 3953.61468489545,
		road: 5912.22893518518,
		acceleration: 0.160555555555554
	},
	{
		id: 516,
		time: 515,
		velocity: 11.7563888888889,
		power: 6014.40933698309,
		road: 5923.93097222222,
		acceleration: 0.334259259259259
	},
	{
		id: 517,
		time: 516,
		velocity: 12.2863888888889,
		power: 5812.53217636722,
		road: 5935.94921296296,
		acceleration: 0.298148148148149
	},
	{
		id: 518,
		time: 517,
		velocity: 12.4591666666667,
		power: 6779.51524166509,
		road: 5948.29754629629,
		acceleration: 0.362037037037036
	},
	{
		id: 519,
		time: 518,
		velocity: 12.8425,
		power: 4646.33254947831,
		road: 5960.91134259259,
		acceleration: 0.16888888888889
	},
	{
		id: 520,
		time: 519,
		velocity: 12.7930555555556,
		power: 1391.41274864069,
		road: 5973.55842592592,
		acceleration: -0.102314814814816
	},
	{
		id: 521,
		time: 520,
		velocity: 12.1522222222222,
		power: 309.094145152027,
		road: 5986.05953703703,
		acceleration: -0.18962962962963
	},
	{
		id: 522,
		time: 521,
		velocity: 12.2736111111111,
		power: 3693.2952597456,
		road: 5998.51379629629,
		acceleration: 0.0959259259259273
	},
	{
		id: 523,
		time: 522,
		velocity: 13.0808333333333,
		power: 6444.5385343146,
		road: 6011.17384259259,
		acceleration: 0.315648148148149
	},
	{
		id: 524,
		time: 523,
		velocity: 13.0991666666667,
		power: 7026.48668743912,
		road: 6024.16379629629,
		acceleration: 0.344166666666665
	},
	{
		id: 525,
		time: 524,
		velocity: 13.3061111111111,
		power: 3886.8508156978,
		road: 6037.36717592592,
		acceleration: 0.082685185185186
	},
	{
		id: 526,
		time: 525,
		velocity: 13.3288888888889,
		power: 4130.63785302423,
		road: 6050.66111111111,
		acceleration: 0.098425925925925
	},
	{
		id: 527,
		time: 526,
		velocity: 13.3944444444444,
		power: 3662.0148777627,
		road: 6064.03356481481,
		acceleration: 0.0586111111111105
	},
	{
		id: 528,
		time: 527,
		velocity: 13.4819444444444,
		power: 3550.55891314448,
		road: 6077.45930555555,
		acceleration: 0.0479629629629645
	},
	{
		id: 529,
		time: 528,
		velocity: 13.4727777777778,
		power: 3605.4364991791,
		road: 6090.93425925926,
		acceleration: 0.050462962962964
	},
	{
		id: 530,
		time: 529,
		velocity: 13.5458333333333,
		power: 2345.75879618416,
		road: 6104.41069444444,
		acceleration: -0.047500000000003
	},
	{
		id: 531,
		time: 530,
		velocity: 13.3394444444444,
		power: 1310.12300943792,
		road: 6117.80037037037,
		acceleration: -0.126018518518517
	},
	{
		id: 532,
		time: 531,
		velocity: 13.0947222222222,
		power: -3102.78521051205,
		road: 6130.89083333333,
		acceleration: -0.472407407407408
	},
	{
		id: 533,
		time: 532,
		velocity: 12.1286111111111,
		power: -4428.62688152128,
		road: 6143.45212962963,
		acceleration: -0.585925925925926
	},
	{
		id: 534,
		time: 533,
		velocity: 11.5816666666667,
		power: -8466.40392582522,
		road: 6155.24101851852,
		acceleration: -0.95888888888889
	},
	{
		id: 535,
		time: 534,
		velocity: 10.2180555555556,
		power: -7983.31177058593,
		road: 6166.06685185185,
		acceleration: -0.967222222222221
	},
	{
		id: 536,
		time: 535,
		velocity: 9.22694444444444,
		power: -9596.97342198298,
		road: 6175.80111111111,
		acceleration: -1.21592592592593
	},
	{
		id: 537,
		time: 536,
		velocity: 7.93388888888889,
		power: -12026.9582399014,
		road: 6184.0812037037,
		acceleration: -1.69240740740741
	},
	{
		id: 538,
		time: 537,
		velocity: 5.14083333333333,
		power: -10139.9918827678,
		road: 6190.62425925926,
		acceleration: -1.78166666666667
	},
	{
		id: 539,
		time: 538,
		velocity: 3.88194444444444,
		power: -6861.53216220322,
		road: 6195.45907407407,
		acceleration: -1.63481481481481
	},
	{
		id: 540,
		time: 539,
		velocity: 3.02944444444444,
		power: -2216.32879971557,
		road: 6199.08731481481,
		acceleration: -0.778333333333334
	},
	{
		id: 541,
		time: 540,
		velocity: 2.80583333333333,
		power: -417.989501239336,
		road: 6202.18898148148,
		acceleration: -0.274814814814815
	},
	{
		id: 542,
		time: 541,
		velocity: 3.0575,
		power: 2037.06191328438,
		road: 6205.41884259259,
		acceleration: 0.531203703703704
	},
	{
		id: 543,
		time: 542,
		velocity: 4.62305555555556,
		power: 4799.92118714243,
		road: 6209.46981481481,
		acceleration: 1.11101851851852
	},
	{
		id: 544,
		time: 543,
		velocity: 6.13888888888889,
		power: 6136.22927143632,
		road: 6214.63069444444,
		acceleration: 1.1087962962963
	},
	{
		id: 545,
		time: 544,
		velocity: 6.38388888888889,
		power: 7145.44361554159,
		road: 6220.87319444444,
		acceleration: 1.05444444444444
	},
	{
		id: 546,
		time: 545,
		velocity: 7.78638888888889,
		power: 13940.8858825343,
		road: 6228.52046296296,
		acceleration: 1.75509259259259
	},
	{
		id: 547,
		time: 546,
		velocity: 11.4041666666667,
		power: 23735.4721447389,
		road: 6238.23671296296,
		acceleration: 2.38287037037037
	},
	{
		id: 548,
		time: 547,
		velocity: 13.5325,
		power: 29140.8172238686,
		road: 6250.30490740741,
		acceleration: 2.32101851851852
	},
	{
		id: 549,
		time: 548,
		velocity: 14.7494444444444,
		power: 20476.1208852866,
		road: 6264.18773148148,
		acceleration: 1.30824074074074
	},
	{
		id: 550,
		time: 549,
		velocity: 15.3288888888889,
		power: 19702.4237268198,
		road: 6279.27976851852,
		acceleration: 1.11018518518519
	},
	{
		id: 551,
		time: 550,
		velocity: 16.8630555555556,
		power: 17849.3603885667,
		road: 6295.37027777778,
		acceleration: 0.886759259259255
	},
	{
		id: 552,
		time: 551,
		velocity: 17.4097222222222,
		power: 15426.3116312333,
		road: 6312.23828703703,
		acceleration: 0.668240740740742
	},
	{
		id: 553,
		time: 552,
		velocity: 17.3336111111111,
		power: 6977.08735792267,
		road: 6329.50388888889,
		acceleration: 0.126944444444447
	},
	{
		id: 554,
		time: 553,
		velocity: 17.2438888888889,
		power: 4183.25136398007,
		road: 6346.81115740741,
		acceleration: -0.0436111111111117
	},
	{
		id: 555,
		time: 554,
		velocity: 17.2788888888889,
		power: 4307.95871721978,
		road: 6364.07925925926,
		acceleration: -0.0347222222222214
	},
	{
		id: 556,
		time: 555,
		velocity: 17.2294444444444,
		power: 4849.29998391224,
		road: 6381.32935185185,
		acceleration: -0.00129629629629591
	},
	{
		id: 557,
		time: 556,
		velocity: 17.24,
		power: 5111.59717975314,
		road: 6398.58597222222,
		acceleration: 0.0143518518518491
	},
	{
		id: 558,
		time: 557,
		velocity: 17.3219444444444,
		power: 5374.68041725189,
		road: 6415.86449074074,
		acceleration: 0.0294444444444437
	},
	{
		id: 559,
		time: 558,
		velocity: 17.3177777777778,
		power: 5268.41846358015,
		road: 6433.16875,
		acceleration: 0.0220370370370375
	},
	{
		id: 560,
		time: 559,
		velocity: 17.3061111111111,
		power: 4821.24968217599,
		road: 6450.48138888889,
		acceleration: -0.00527777777777771
	},
	{
		id: 561,
		time: 560,
		velocity: 17.3061111111111,
		power: 5472.9419530426,
		road: 6467.80814814814,
		acceleration: 0.0335185185185196
	},
	{
		id: 562,
		time: 561,
		velocity: 17.4183333333333,
		power: 7203.69806211776,
		road: 6485.21875,
		acceleration: 0.134166666666665
	},
	{
		id: 563,
		time: 562,
		velocity: 17.7086111111111,
		power: 9933.17406916536,
		road: 6502.83976851851,
		acceleration: 0.286666666666669
	},
	{
		id: 564,
		time: 563,
		velocity: 18.1661111111111,
		power: 11499.4151039668,
		road: 6520.78444444444,
		acceleration: 0.360648148148147
	},
	{
		id: 565,
		time: 564,
		velocity: 18.5002777777778,
		power: 12175.7994547809,
		road: 6539.09847222222,
		acceleration: 0.378055555555559
	},
	{
		id: 566,
		time: 565,
		velocity: 18.8427777777778,
		power: 11726.9933347381,
		road: 6557.76763888888,
		acceleration: 0.332222222222221
	},
	{
		id: 567,
		time: 566,
		velocity: 19.1627777777778,
		power: 11472.0601691731,
		road: 6576.75314814814,
		acceleration: 0.300462962962964
	},
	{
		id: 568,
		time: 567,
		velocity: 19.4016666666667,
		power: 11249.7812598422,
		road: 6596.02527777777,
		acceleration: 0.272777777777776
	},
	{
		id: 569,
		time: 568,
		velocity: 19.6611111111111,
		power: 10164.3757971572,
		road: 6615.53481481481,
		acceleration: 0.202037037037041
	},
	{
		id: 570,
		time: 569,
		velocity: 19.7688888888889,
		power: 9815.65937383028,
		road: 6635.2324537037,
		acceleration: 0.174166666666665
	},
	{
		id: 571,
		time: 570,
		velocity: 19.9241666666667,
		power: 11203.7177754252,
		road: 6655.13564814814,
		acceleration: 0.236944444444447
	},
	{
		id: 572,
		time: 571,
		velocity: 20.3719444444444,
		power: 10695.7144616219,
		road: 6675.25694444444,
		acceleration: 0.199259259259254
	},
	{
		id: 573,
		time: 572,
		velocity: 20.3666666666667,
		power: 11775.7760626224,
		road: 6695.59962962963,
		acceleration: 0.243518518518517
	},
	{
		id: 574,
		time: 573,
		velocity: 20.6547222222222,
		power: 8800.25442407227,
		road: 6716.10583333333,
		acceleration: 0.0835185185185168
	},
	{
		id: 575,
		time: 574,
		velocity: 20.6225,
		power: 9048.37177336966,
		road: 6736.69986111111,
		acceleration: 0.0921296296296319
	},
	{
		id: 576,
		time: 575,
		velocity: 20.6430555555556,
		power: 7001.72311687604,
		road: 6757.33337962963,
		acceleration: -0.0131481481481472
	},
	{
		id: 577,
		time: 576,
		velocity: 20.6152777777778,
		power: 6946.38038060507,
		road: 6777.95263888889,
		acceleration: -0.0153703703703698
	},
	{
		id: 578,
		time: 577,
		velocity: 20.5763888888889,
		power: 6162.48780108229,
		road: 6798.53736111111,
		acceleration: -0.0537037037037038
	},
	{
		id: 579,
		time: 578,
		velocity: 20.4819444444444,
		power: 8433.03206092223,
		road: 6819.12583333333,
		acceleration: 0.0612037037037041
	},
	{
		id: 580,
		time: 579,
		velocity: 20.7988888888889,
		power: 9018.80609895976,
		road: 6839.7886574074,
		acceleration: 0.0874999999999986
	},
	{
		id: 581,
		time: 580,
		velocity: 20.8388888888889,
		power: 10125.8817223377,
		road: 6860.56421296296,
		acceleration: 0.137962962962963
	},
	{
		id: 582,
		time: 581,
		velocity: 20.8958333333333,
		power: 8916.51663888493,
		road: 6881.445,
		acceleration: 0.0725000000000016
	},
	{
		id: 583,
		time: 582,
		velocity: 21.0163888888889,
		power: 8062.43169594779,
		road: 6902.37587962963,
		acceleration: 0.0276851851851845
	},
	{
		id: 584,
		time: 583,
		velocity: 20.9219444444444,
		power: 9037.6244565031,
		road: 6923.35759259259,
		acceleration: 0.0739814814814821
	},
	{
		id: 585,
		time: 584,
		velocity: 21.1177777777778,
		power: 6855.27247993774,
		road: 6944.3586574074,
		acceleration: -0.0352777777777753
	},
	{
		id: 586,
		time: 585,
		velocity: 20.9105555555556,
		power: 7773.34547707489,
		road: 6965.3475,
		acceleration: 0.0108333333333341
	},
	{
		id: 587,
		time: 586,
		velocity: 20.9544444444444,
		power: 3803.79850719968,
		road: 6986.2499537037,
		acceleration: -0.183611111111116
	},
	{
		id: 588,
		time: 587,
		velocity: 20.5669444444444,
		power: 6685.52235663913,
		road: 7007.04287037037,
		acceleration: -0.0354629629629635
	},
	{
		id: 589,
		time: 588,
		velocity: 20.8041666666667,
		power: 9052.55721959466,
		road: 7027.85916666666,
		acceleration: 0.0822222222222244
	},
	{
		id: 590,
		time: 589,
		velocity: 21.2011111111111,
		power: 12363.2995319841,
		road: 7048.83634259259,
		acceleration: 0.239537037037035
	},
	{
		id: 591,
		time: 590,
		velocity: 21.2855555555556,
		power: 9211.41645768211,
		road: 7069.97111111111,
		acceleration: 0.0756481481481472
	},
	{
		id: 592,
		time: 591,
		velocity: 21.0311111111111,
		power: 6271.63666500782,
		road: 7091.10893518518,
		acceleration: -0.069537037037037
	},
	{
		id: 593,
		time: 592,
		velocity: 20.9925,
		power: 5058.13037023008,
		road: 7112.14902777777,
		acceleration: -0.125925925925923
	},
	{
		id: 594,
		time: 593,
		velocity: 20.9077777777778,
		power: 5609.05462884344,
		road: 7133.07888888888,
		acceleration: -0.0945370370370391
	},
	{
		id: 595,
		time: 594,
		velocity: 20.7475,
		power: 4940.63484444628,
		road: 7153.89949074074,
		acceleration: -0.123981481481479
	},
	{
		id: 596,
		time: 595,
		velocity: 20.6205555555556,
		power: 4121.44801968263,
		road: 7174.57796296296,
		acceleration: -0.160277777777779
	},
	{
		id: 597,
		time: 596,
		velocity: 20.4269444444444,
		power: 3981.07240280451,
		road: 7195.09527777777,
		acceleration: -0.162037037037038
	},
	{
		id: 598,
		time: 597,
		velocity: 20.2613888888889,
		power: 4508.77459017532,
		road: 7215.46648148148,
		acceleration: -0.130185185185184
	},
	{
		id: 599,
		time: 598,
		velocity: 20.23,
		power: 4433.01107387036,
		road: 7235.70777777777,
		acceleration: -0.12962962962963
	},
	{
		id: 600,
		time: 599,
		velocity: 20.0380555555556,
		power: 2158.09922860913,
		road: 7255.76342592592,
		acceleration: -0.241666666666667
	},
	{
		id: 601,
		time: 600,
		velocity: 19.5363888888889,
		power: -795.393681770938,
		road: 7275.50375,
		acceleration: -0.38898148148148
	},
	{
		id: 602,
		time: 601,
		velocity: 19.0630555555556,
		power: -1808.25350054319,
		road: 7294.83180555555,
		acceleration: -0.435555555555556
	},
	{
		id: 603,
		time: 602,
		velocity: 18.7313888888889,
		power: -1493.75899891058,
		road: 7313.73643518518,
		acceleration: -0.411296296296296
	},
	{
		id: 604,
		time: 603,
		velocity: 18.3025,
		power: 719.012794185232,
		road: 7332.2949074074,
		acceleration: -0.281018518518522
	},
	{
		id: 605,
		time: 604,
		velocity: 18.22,
		power: -1073.34690429875,
		road: 7350.52476851851,
		acceleration: -0.376203703703705
	},
	{
		id: 606,
		time: 605,
		velocity: 17.6027777777778,
		power: -946.874254957006,
		road: 7368.38523148148,
		acceleration: -0.362592592592591
	},
	{
		id: 607,
		time: 606,
		velocity: 17.2147222222222,
		power: -3184.11015176143,
		road: 7385.81958333333,
		acceleration: -0.489629629629626
	},
	{
		id: 608,
		time: 607,
		velocity: 16.7511111111111,
		power: -5102.24225215952,
		road: 7402.70708333333,
		acceleration: -0.604074074074077
	},
	{
		id: 609,
		time: 608,
		velocity: 15.7905555555556,
		power: -5290.84378765035,
		road: 7418.98416666666,
		acceleration: -0.616759259259258
	},
	{
		id: 610,
		time: 609,
		velocity: 15.3644444444444,
		power: -8188.1039013448,
		road: 7434.54560185185,
		acceleration: -0.814537037037036
	},
	{
		id: 611,
		time: 610,
		velocity: 14.3075,
		power: -4554.03011861798,
		road: 7449.41342592592,
		acceleration: -0.572685185185186
	},
	{
		id: 612,
		time: 611,
		velocity: 14.0725,
		power: -4092.11202830204,
		road: 7463.72374999999,
		acceleration: -0.542314814814814
	},
	{
		id: 613,
		time: 612,
		velocity: 13.7375,
		power: -3263.44019880506,
		road: 7477.52166666666,
		acceleration: -0.482500000000002
	},
	{
		id: 614,
		time: 613,
		velocity: 12.86,
		power: -9858.08952423735,
		road: 7490.57078703703,
		acceleration: -1.01509259259259
	},
	{
		id: 615,
		time: 614,
		velocity: 11.0272222222222,
		power: -11631.2633259029,
		road: 7502.49722222222,
		acceleration: -1.23027777777778
	},
	{
		id: 616,
		time: 615,
		velocity: 10.0466666666667,
		power: -10479.4185429823,
		road: 7513.19875,
		acceleration: -1.21953703703704
	},
	{
		id: 617,
		time: 616,
		velocity: 9.20138888888889,
		power: -6076.259763694,
		road: 7522.87064814814,
		acceleration: -0.839722222222225
	},
	{
		id: 618,
		time: 617,
		velocity: 8.50805555555556,
		power: -4166.17788391355,
		road: 7531.79129629629,
		acceleration: -0.662777777777777
	},
	{
		id: 619,
		time: 618,
		velocity: 8.05833333333333,
		power: -3705.43141757568,
		road: 7540.06222222222,
		acceleration: -0.636666666666668
	},
	{
		id: 620,
		time: 619,
		velocity: 7.29138888888889,
		power: -4985.44465623719,
		road: 7547.58694444444,
		acceleration: -0.85574074074074
	},
	{
		id: 621,
		time: 620,
		velocity: 5.94083333333333,
		power: -9209.63027002245,
		road: 7553.83356481481,
		acceleration: -1.70046296296296
	},
	{
		id: 622,
		time: 621,
		velocity: 2.95694444444444,
		power: -7643.89551440671,
		road: 7558.24916666666,
		acceleration: -1.96157407407407
	},
	{
		id: 623,
		time: 622,
		velocity: 1.40666666666667,
		power: -4288.17229931326,
		road: 7560.69384259259,
		acceleration: -1.98027777777778
	},
	{
		id: 624,
		time: 623,
		velocity: 0,
		power: -781.713649767804,
		road: 7561.65555555555,
		acceleration: -0.985648148148148
	},
	{
		id: 625,
		time: 624,
		velocity: 0,
		power: -75.8176058479532,
		road: 7561.89,
		acceleration: -0.468888888888889
	},
	{
		id: 626,
		time: 625,
		velocity: 0,
		power: 0,
		road: 7561.89,
		acceleration: 0
	},
	{
		id: 627,
		time: 626,
		velocity: 0,
		power: 0,
		road: 7561.89,
		acceleration: 0
	},
	{
		id: 628,
		time: 627,
		velocity: 0,
		power: 0,
		road: 7561.89,
		acceleration: 0
	},
	{
		id: 629,
		time: 628,
		velocity: 0,
		power: 0,
		road: 7561.89,
		acceleration: 0
	},
	{
		id: 630,
		time: 629,
		velocity: 0,
		power: 0,
		road: 7561.89,
		acceleration: 0
	},
	{
		id: 631,
		time: 630,
		velocity: 0,
		power: 0,
		road: 7561.89,
		acceleration: 0
	},
	{
		id: 632,
		time: 631,
		velocity: 0,
		power: 0,
		road: 7561.89,
		acceleration: 0
	},
	{
		id: 633,
		time: 632,
		velocity: 0,
		power: 215.897556999955,
		road: 7562.19712962963,
		acceleration: 0.614259259259259
	},
	{
		id: 634,
		time: 633,
		velocity: 1.84277777777778,
		power: 1109.80876288594,
		road: 7563.28523148148,
		acceleration: 0.947685185185185
	},
	{
		id: 635,
		time: 634,
		velocity: 2.84305555555556,
		power: 3036.61399927195,
		road: 7565.50351851851,
		acceleration: 1.31268518518519
	},
	{
		id: 636,
		time: 635,
		velocity: 3.93805555555556,
		power: 3396.34964426628,
		road: 7568.84643518518,
		acceleration: 0.936574074074074
	},
	{
		id: 637,
		time: 636,
		velocity: 4.6525,
		power: 2926.45142142754,
		road: 7572.96342592592,
		acceleration: 0.611574074074074
	},
	{
		id: 638,
		time: 637,
		velocity: 4.67777777777778,
		power: 2372.55786464526,
		road: 7577.58662037037,
		acceleration: 0.400833333333334
	},
	{
		id: 639,
		time: 638,
		velocity: 5.14055555555556,
		power: 1371.99087698746,
		road: 7582.48712962963,
		acceleration: 0.153796296296297
	},
	{
		id: 640,
		time: 639,
		velocity: 5.11388888888889,
		power: 574.523036714682,
		road: 7587.45476851852,
		acceleration: -0.019537037037038
	},
	{
		id: 641,
		time: 640,
		velocity: 4.61916666666667,
		power: -916.164390299971,
		road: 7592.24175925926,
		acceleration: -0.341759259259259
	},
	{
		id: 642,
		time: 641,
		velocity: 4.11527777777778,
		power: -2825.16012672307,
		road: 7596.43439814814,
		acceleration: -0.846944444444444
	},
	{
		id: 643,
		time: 642,
		velocity: 2.57305555555556,
		power: -3008.4886411021,
		road: 7599.64291666666,
		acceleration: -1.1212962962963
	},
	{
		id: 644,
		time: 643,
		velocity: 1.25527777777778,
		power: -2310.94936632164,
		road: 7601.6049074074,
		acceleration: -1.37175925925926
	},
	{
		id: 645,
		time: 644,
		velocity: 0,
		power: -585.997104104288,
		road: 7602.45217592592,
		acceleration: -0.857685185185185
	},
	{
		id: 646,
		time: 645,
		velocity: 0,
		power: -57.6560828622482,
		road: 7602.66138888889,
		acceleration: -0.418425925925926
	},
	{
		id: 647,
		time: 646,
		velocity: 0,
		power: 0,
		road: 7602.66138888889,
		acceleration: 0
	},
	{
		id: 648,
		time: 647,
		velocity: 0,
		power: 0,
		road: 7602.66138888889,
		acceleration: 0
	},
	{
		id: 649,
		time: 648,
		velocity: 0,
		power: 0,
		road: 7602.66138888889,
		acceleration: 0
	},
	{
		id: 650,
		time: 649,
		velocity: 0,
		power: 0,
		road: 7602.66138888889,
		acceleration: 0
	},
	{
		id: 651,
		time: 650,
		velocity: 0,
		power: 0,
		road: 7602.66138888889,
		acceleration: 0
	},
	{
		id: 652,
		time: 651,
		velocity: 0,
		power: 0,
		road: 7602.66138888889,
		acceleration: 0
	},
	{
		id: 653,
		time: 652,
		velocity: 0,
		power: 0,
		road: 7602.66138888889,
		acceleration: 0
	},
	{
		id: 654,
		time: 653,
		velocity: 0,
		power: 0,
		road: 7602.66138888889,
		acceleration: 0
	},
	{
		id: 655,
		time: 654,
		velocity: 0,
		power: 0,
		road: 7602.66138888889,
		acceleration: 0
	},
	{
		id: 656,
		time: 655,
		velocity: 0,
		power: 214.412491320259,
		road: 7602.96736111111,
		acceleration: 0.611944444444444
	},
	{
		id: 657,
		time: 656,
		velocity: 1.83583333333333,
		power: 1151.90648180508,
		road: 7604.0674537037,
		acceleration: 0.976296296296296
	},
	{
		id: 658,
		time: 657,
		velocity: 2.92888888888889,
		power: 2840.76361812721,
		road: 7606.27027777777,
		acceleration: 1.22916666666667
	},
	{
		id: 659,
		time: 658,
		velocity: 3.6875,
		power: 2172.39117488242,
		road: 7609.38828703703,
		acceleration: 0.601203703703704
	},
	{
		id: 660,
		time: 659,
		velocity: 3.63944444444444,
		power: 205.01103161519,
		road: 7612.77189814814,
		acceleration: -0.0699999999999998
	},
	{
		id: 661,
		time: 660,
		velocity: 2.71888888888889,
		power: -673.59514064337,
		road: 7615.94203703703,
		acceleration: -0.356944444444445
	},
	{
		id: 662,
		time: 661,
		velocity: 2.61666666666667,
		power: -2448.92525937604,
		road: 7618.32712962963,
		acceleration: -1.21314814814815
	},
	{
		id: 663,
		time: 662,
		velocity: 0,
		power: -977.282513081678,
		road: 7619.6525,
		acceleration: -0.906296296296296
	},
	{
		id: 664,
		time: 663,
		velocity: 0,
		power: -307.675470760234,
		road: 7620.08861111111,
		acceleration: -0.872222222222222
	},
	{
		id: 665,
		time: 664,
		velocity: 0,
		power: 0,
		road: 7620.08861111111,
		acceleration: 0
	},
	{
		id: 666,
		time: 665,
		velocity: 0,
		power: 117.999089518492,
		road: 7620.30828703703,
		acceleration: 0.439351851851852
	},
	{
		id: 667,
		time: 666,
		velocity: 1.31805555555556,
		power: 1420.85009262965,
		road: 7621.38166666666,
		acceleration: 1.26805555555556
	},
	{
		id: 668,
		time: 667,
		velocity: 3.80416666666667,
		power: 4090.36997369773,
		road: 7623.88467592592,
		acceleration: 1.5912037037037
	},
	{
		id: 669,
		time: 668,
		velocity: 4.77361111111111,
		power: 5686.6297964394,
		road: 7627.86694444444,
		acceleration: 1.36731481481482
	},
	{
		id: 670,
		time: 669,
		velocity: 5.42,
		power: 3336.91793809916,
		road: 7632.81703703703,
		acceleration: 0.568333333333333
	},
	{
		id: 671,
		time: 670,
		velocity: 5.50916666666667,
		power: 1605.43543246022,
		road: 7638.13837962963,
		acceleration: 0.174166666666666
	},
	{
		id: 672,
		time: 671,
		velocity: 5.29611111111111,
		power: -82.3492462594047,
		road: 7643.46699074074,
		acceleration: -0.15962962962963
	},
	{
		id: 673,
		time: 672,
		velocity: 4.94111111111111,
		power: -1048.80830570535,
		road: 7648.53597222222,
		acceleration: -0.35962962962963
	},
	{
		id: 674,
		time: 673,
		velocity: 4.43027777777778,
		power: -2845.5646615303,
		road: 7653.02180555555,
		acceleration: -0.806666666666666
	},
	{
		id: 675,
		time: 674,
		velocity: 2.87611111111111,
		power: -3207.80479590304,
		road: 7656.55939814814,
		acceleration: -1.08981481481481
	},
	{
		id: 676,
		time: 675,
		velocity: 1.67166666666667,
		power: -2528.46446413266,
		road: 7658.92282407407,
		acceleration: -1.25851851851852
	},
	{
		id: 677,
		time: 676,
		velocity: 0.654722222222222,
		power: -987.666331861176,
		road: 7660.17763888888,
		acceleration: -0.958703703703704
	},
	{
		id: 678,
		time: 677,
		velocity: 0,
		power: -149.038871324861,
		road: 7660.7524537037,
		acceleration: -0.401296296296296
	},
	{
		id: 679,
		time: 678,
		velocity: 0.467777777777778,
		power: -22.7736504358977,
		road: 7661.0175,
		acceleration: -0.218240740740741
	},
	{
		id: 680,
		time: 679,
		velocity: 0,
		power: 223.137799938268,
		road: 7661.40976851852,
		acceleration: 0.472685185185185
	},
	{
		id: 681,
		time: 680,
		velocity: 1.41805555555556,
		power: 766.678027385194,
		road: 7662.38782407407,
		acceleration: 0.698888888888889
	},
	{
		id: 682,
		time: 681,
		velocity: 2.56444444444444,
		power: 1596.23145707468,
		road: 7664.13296296296,
		acceleration: 0.835277777777778
	},
	{
		id: 683,
		time: 682,
		velocity: 2.50583333333333,
		power: 1003.34133873046,
		road: 7666.45796296296,
		acceleration: 0.324444444444444
	},
	{
		id: 684,
		time: 683,
		velocity: 2.39138888888889,
		power: 331.800314870123,
		road: 7668.9499074074,
		acceleration: 0.00944444444444459
	},
	{
		id: 685,
		time: 684,
		velocity: 2.59277777777778,
		power: 463.39204977536,
		road: 7671.47768518518,
		acceleration: 0.0622222222222222
	},
	{
		id: 686,
		time: 685,
		velocity: 2.6925,
		power: 1024.45892718543,
		road: 7674.17124999999,
		acceleration: 0.269351851851852
	},
	{
		id: 687,
		time: 686,
		velocity: 3.19944444444444,
		power: 1038.01868054196,
		road: 7677.11888888888,
		acceleration: 0.238796296296296
	},
	{
		id: 688,
		time: 687,
		velocity: 3.30916666666667,
		power: 513.954934274886,
		road: 7680.20722222222,
		acceleration: 0.0425925925925927
	},
	{
		id: 689,
		time: 688,
		velocity: 2.82027777777778,
		power: -295.615888851098,
		road: 7683.19856481481,
		acceleration: -0.236574074074074
	},
	{
		id: 690,
		time: 689,
		velocity: 2.48972222222222,
		power: -435.140179274203,
		road: 7685.92162037036,
		acceleration: -0.3
	},
	{
		id: 691,
		time: 690,
		velocity: 2.40916666666667,
		power: -23.2843128022537,
		road: 7688.42430555555,
		acceleration: -0.140740740740741
	},
	{
		id: 692,
		time: 691,
		velocity: 2.39805555555556,
		power: 95.1681040734445,
		road: 7690.81231481481,
		acceleration: -0.0886111111111112
	},
	{
		id: 693,
		time: 692,
		velocity: 2.22388888888889,
		power: -28.2434380393507,
		road: 7693.08430555555,
		acceleration: -0.143425925925926
	},
	{
		id: 694,
		time: 693,
		velocity: 1.97888888888889,
		power: 106.198531687775,
		road: 7695.24546296296,
		acceleration: -0.0782407407407404
	},
	{
		id: 695,
		time: 694,
		velocity: 2.16333333333333,
		power: 424.786696635438,
		road: 7697.40611111111,
		acceleration: 0.0772222222222219
	},
	{
		id: 696,
		time: 695,
		velocity: 2.45555555555556,
		power: 579.769543404649,
		road: 7699.6749074074,
		acceleration: 0.139074074074074
	},
	{
		id: 697,
		time: 696,
		velocity: 2.39611111111111,
		power: 242.724380425745,
		road: 7702.00296296296,
		acceleration: -0.0205555555555557
	},
	{
		id: 698,
		time: 697,
		velocity: 2.10166666666667,
		power: 39.1708829902098,
		road: 7704.26472222222,
		acceleration: -0.112037037037037
	},
	{
		id: 699,
		time: 698,
		velocity: 2.11944444444444,
		power: 8.85386987600133,
		road: 7706.40763888888,
		acceleration: -0.125648148148148
	},
	{
		id: 700,
		time: 699,
		velocity: 2.01916666666667,
		power: 43.4484215208015,
		road: 7708.43416666666,
		acceleration: -0.10712962962963
	},
	{
		id: 701,
		time: 700,
		velocity: 1.78027777777778,
		power: 63.8791383682033,
		road: 7710.35986111111,
		acceleration: -0.0945370370370373
	},
	{
		id: 702,
		time: 701,
		velocity: 1.83583333333333,
		power: 949.241598799816,
		road: 7712.41666666666,
		acceleration: 0.35675925925926
	},
	{
		id: 703,
		time: 702,
		velocity: 3.08944444444444,
		power: 1425.95366933921,
		road: 7714.89023148148,
		acceleration: 0.476759259259259
	},
	{
		id: 704,
		time: 703,
		velocity: 3.21055555555556,
		power: 1187.63224304544,
		road: 7717.75462962963,
		acceleration: 0.304907407407407
	},
	{
		id: 705,
		time: 704,
		velocity: 2.75055555555556,
		power: -205.687339073405,
		road: 7720.66819444444,
		acceleration: -0.206574074074074
	},
	{
		id: 706,
		time: 705,
		velocity: 2.46972222222222,
		power: -392.623034975201,
		road: 7723.33518518518,
		acceleration: -0.286574074074073
	},
	{
		id: 707,
		time: 706,
		velocity: 2.35083333333333,
		power: 277.522114563227,
		road: 7725.85152777777,
		acceleration: -0.0147222222222227
	},
	{
		id: 708,
		time: 707,
		velocity: 2.70638888888889,
		power: 850.027273253042,
		road: 7728.4662037037,
		acceleration: 0.211388888888889
	},
	{
		id: 709,
		time: 708,
		velocity: 3.10388888888889,
		power: 1725.49268176134,
		road: 7731.4274074074,
		acceleration: 0.481666666666666
	},
	{
		id: 710,
		time: 709,
		velocity: 3.79583333333333,
		power: 2331.90703728835,
		road: 7734.91453703703,
		acceleration: 0.570185185185185
	},
	{
		id: 711,
		time: 710,
		velocity: 4.41694444444444,
		power: 2268.85704552285,
		road: 7738.91703703703,
		acceleration: 0.460555555555556
	},
	{
		id: 712,
		time: 711,
		velocity: 4.48555555555556,
		power: 1356.54335206225,
		road: 7743.24583333333,
		acceleration: 0.192037037037037
	},
	{
		id: 713,
		time: 712,
		velocity: 4.37194444444444,
		power: 477.711864698853,
		road: 7747.65847222222,
		acceleration: -0.0243518518518524
	},
	{
		id: 714,
		time: 713,
		velocity: 4.34388888888889,
		power: -329.15923219061,
		road: 7751.94967592592,
		acceleration: -0.218518518518518
	},
	{
		id: 715,
		time: 714,
		velocity: 3.83,
		power: -756.09944438463,
		road: 7755.96421296296,
		acceleration: -0.334814814814815
	},
	{
		id: 716,
		time: 715,
		velocity: 3.3675,
		power: -835.590373528286,
		road: 7759.62361111111,
		acceleration: -0.375462962962963
	},
	{
		id: 717,
		time: 716,
		velocity: 3.2175,
		power: -241.540614618063,
		road: 7762.99060185185,
		acceleration: -0.209351851851852
	},
	{
		id: 718,
		time: 717,
		velocity: 3.20194444444444,
		power: 736.729373249313,
		road: 7766.30328703703,
		acceleration: 0.10074074074074
	},
	{
		id: 719,
		time: 718,
		velocity: 3.66972222222222,
		power: 1392.74290511476,
		road: 7769.80847222222,
		acceleration: 0.28425925925926
	},
	{
		id: 720,
		time: 719,
		velocity: 4.07027777777778,
		power: 1893.2767096214,
		road: 7773.64763888889,
		acceleration: 0.383703703703703
	},
	{
		id: 721,
		time: 720,
		velocity: 4.35305555555556,
		power: 2131.47114865911,
		road: 7777.87537037037,
		acceleration: 0.393425925925926
	},
	{
		id: 722,
		time: 721,
		velocity: 4.85,
		power: 1176.06124660544,
		road: 7782.36824074074,
		acceleration: 0.136851851851853
	},
	{
		id: 723,
		time: 722,
		velocity: 4.48083333333333,
		power: 966.767674603948,
		road: 7786.97046296296,
		acceleration: 0.0818518518518516
	},
	{
		id: 724,
		time: 723,
		velocity: 4.59861111111111,
		power: -64.4697419681974,
		road: 7791.53662037037,
		acceleration: -0.153981481481482
	},
	{
		id: 725,
		time: 724,
		velocity: 4.38805555555556,
		power: 413.047484108894,
		road: 7796.00513888888,
		acceleration: -0.0412962962962959
	},
	{
		id: 726,
		time: 725,
		velocity: 4.35694444444444,
		power: 690.574281934464,
		road: 7800.46523148148,
		acceleration: 0.0244444444444438
	},
	{
		id: 727,
		time: 726,
		velocity: 4.67194444444444,
		power: 986.061602024661,
		road: 7804.98300925925,
		acceleration: 0.0909259259259265
	},
	{
		id: 728,
		time: 727,
		velocity: 4.66083333333333,
		power: 2037.48637395816,
		road: 7809.70347222222,
		acceleration: 0.314444444444444
	},
	{
		id: 729,
		time: 728,
		velocity: 5.30027777777778,
		power: 1390.8362372768,
		road: 7814.65828703703,
		acceleration: 0.154259259259259
	},
	{
		id: 730,
		time: 729,
		velocity: 5.13472222222222,
		power: 1579.4370754894,
		road: 7819.78138888889,
		acceleration: 0.182314814814815
	},
	{
		id: 731,
		time: 730,
		velocity: 5.20777777777778,
		power: 1671.15210467053,
		road: 7825.08967592592,
		acceleration: 0.188055555555556
	},
	{
		id: 732,
		time: 731,
		velocity: 5.86444444444444,
		power: 1646.18553701535,
		road: 7830.57763888889,
		acceleration: 0.171296296296295
	},
	{
		id: 733,
		time: 732,
		velocity: 5.64861111111111,
		power: 972.928765241411,
		road: 7836.17027777777,
		acceleration: 0.0380555555555571
	},
	{
		id: 734,
		time: 733,
		velocity: 5.32194444444444,
		power: -1081.53832745801,
		road: 7841.60523148148,
		acceleration: -0.353425925925926
	},
	{
		id: 735,
		time: 734,
		velocity: 4.80416666666667,
		power: -742.502969948527,
		road: 7846.71597222222,
		acceleration: -0.295000000000001
	},
	{
		id: 736,
		time: 735,
		velocity: 4.76361111111111,
		power: 497.43038776154,
		road: 7851.66157407407,
		acceleration: -0.035277777777778
	},
	{
		id: 737,
		time: 736,
		velocity: 5.21611111111111,
		power: 2034.52680520032,
		road: 7856.72986111111,
		acceleration: 0.280648148148148
	},
	{
		id: 738,
		time: 737,
		velocity: 5.64611111111111,
		power: 3021.36314639111,
		road: 7862.15925925926,
		acceleration: 0.441574074074073
	},
	{
		id: 739,
		time: 738,
		velocity: 6.08833333333333,
		power: 3018.3470814359,
		road: 7868.00759259259,
		acceleration: 0.396296296296297
	},
	{
		id: 740,
		time: 739,
		velocity: 6.405,
		power: 3160.26877755076,
		road: 7874.24583333333,
		acceleration: 0.383518518518518
	},
	{
		id: 741,
		time: 740,
		velocity: 6.79666666666667,
		power: 4209.88727257034,
		road: 7880.93064814814,
		acceleration: 0.509629629629629
	},
	{
		id: 742,
		time: 741,
		velocity: 7.61722222222222,
		power: 4341.53160561123,
		road: 7888.1099074074,
		acceleration: 0.479259259259261
	},
	{
		id: 743,
		time: 742,
		velocity: 7.84277777777778,
		power: 3272.48929847253,
		road: 7895.67625,
		acceleration: 0.294907407407406
	},
	{
		id: 744,
		time: 743,
		velocity: 7.68138888888889,
		power: -2224.43144278469,
		road: 7903.15425925926,
		acceleration: -0.471574074074073
	},
	{
		id: 745,
		time: 744,
		velocity: 6.2025,
		power: -3493.44826649942,
		road: 7910.05314814814,
		acceleration: -0.686666666666667
	},
	{
		id: 746,
		time: 745,
		velocity: 5.78277777777778,
		power: -3796.96009481774,
		road: 7916.2099537037,
		acceleration: -0.797499999999999
	},
	{
		id: 747,
		time: 746,
		velocity: 5.28888888888889,
		power: -860.260912586631,
		road: 7921.81472222222,
		acceleration: -0.306574074074074
	},
	{
		id: 748,
		time: 747,
		velocity: 5.28277777777778,
		power: 167.951897570513,
		road: 7927.21069444444,
		acceleration: -0.111018518518518
	},
	{
		id: 749,
		time: 748,
		velocity: 5.44972222222222,
		power: 2156.16655432851,
		road: 7932.6862037037,
		acceleration: 0.270092592592593
	},
	{
		id: 750,
		time: 749,
		velocity: 6.09916666666667,
		power: 2895.05604103589,
		road: 7938.4861574074,
		acceleration: 0.378796296296297
	},
	{
		id: 751,
		time: 750,
		velocity: 6.41916666666667,
		power: 3619.46729979661,
		road: 7944.70689814814,
		acceleration: 0.462777777777777
	},
	{
		id: 752,
		time: 751,
		velocity: 6.83805555555556,
		power: 2061.63329775273,
		road: 7951.24898148148,
		acceleration: 0.179907407407407
	},
	{
		id: 753,
		time: 752,
		velocity: 6.63888888888889,
		power: 2638.46306417877,
		road: 7958.00962962963,
		acceleration: 0.257222222222222
	},
	{
		id: 754,
		time: 753,
		velocity: 7.19083333333333,
		power: 1862.60346462809,
		road: 7964.9624074074,
		acceleration: 0.127037037037037
	},
	{
		id: 755,
		time: 754,
		velocity: 7.21916666666667,
		power: 3131.73010937566,
		road: 7972.13018518518,
		acceleration: 0.302962962962963
	},
	{
		id: 756,
		time: 755,
		velocity: 7.54777777777778,
		power: 1471.48935497373,
		road: 7979.47583333333,
		acceleration: 0.0527777777777771
	},
	{
		id: 757,
		time: 756,
		velocity: 7.34916666666667,
		power: 1572.22723484175,
		road: 7986.88032407407,
		acceleration: 0.064907407407409
	},
	{
		id: 758,
		time: 757,
		velocity: 7.41388888888889,
		power: -670.314701005468,
		road: 7994.19032407407,
		acceleration: -0.25388888888889
	},
	{
		id: 759,
		time: 758,
		velocity: 6.78611111111111,
		power: 174.308658733151,
		road: 8001.30828703703,
		acceleration: -0.130185185185185
	},
	{
		id: 760,
		time: 759,
		velocity: 6.95861111111111,
		power: 821.765522927054,
		road: 8008.3449074074,
		acceleration: -0.0325000000000006
	},
	{
		id: 761,
		time: 760,
		velocity: 7.31638888888889,
		power: 1489.40532243633,
		road: 8015.39856481481,
		acceleration: 0.066574074074075
	},
	{
		id: 762,
		time: 761,
		velocity: 6.98583333333333,
		power: 613.635107625774,
		road: 8022.45351851852,
		acceleration: -0.0639814814814814
	},
	{
		id: 763,
		time: 762,
		velocity: 6.76666666666667,
		power: 302.053945756615,
		road: 8029.42189814815,
		acceleration: -0.109166666666666
	},
	{
		id: 764,
		time: 763,
		velocity: 6.98888888888889,
		power: 852.452034747611,
		road: 8036.32351851852,
		acceleration: -0.0243518518518533
	},
	{
		id: 765,
		time: 764,
		velocity: 6.91277777777778,
		power: 1909.09946468582,
		road: 8043.2799074074,
		acceleration: 0.133888888888889
	},
	{
		id: 766,
		time: 765,
		velocity: 7.16833333333333,
		power: 2283.74804845349,
		road: 8050.39402777777,
		acceleration: 0.181574074074073
	},
	{
		id: 767,
		time: 766,
		velocity: 7.53361111111111,
		power: 2855.42429586532,
		road: 8057.72481481481,
		acceleration: 0.251759259259259
	},
	{
		id: 768,
		time: 767,
		velocity: 7.66805555555556,
		power: 2727.13788311812,
		road: 8065.29106481481,
		acceleration: 0.219166666666666
	},
	{
		id: 769,
		time: 768,
		velocity: 7.82583333333333,
		power: 1650.49186986815,
		road: 8072.99898148148,
		acceleration: 0.0641666666666669
	},
	{
		id: 770,
		time: 769,
		velocity: 7.72611111111111,
		power: 1196.54110645698,
		road: 8080.73962962963,
		acceleration: 0.00129629629629591
	},
	{
		id: 771,
		time: 770,
		velocity: 7.67194444444444,
		power: 995.716246228028,
		road: 8088.46810185185,
		acceleration: -0.0256481481481474
	},
	{
		id: 772,
		time: 771,
		velocity: 7.74888888888889,
		power: 1113.97164085162,
		road: 8096.17921296296,
		acceleration: -0.00907407407407401
	},
	{
		id: 773,
		time: 772,
		velocity: 7.69888888888889,
		power: 804.564378139934,
		road: 8103.86050925926,
		acceleration: -0.0505555555555555
	},
	{
		id: 774,
		time: 773,
		velocity: 7.52027777777778,
		power: 334.890363216613,
		road: 8111.45972222222,
		acceleration: -0.113611111111111
	},
	{
		id: 775,
		time: 774,
		velocity: 7.40805555555556,
		power: 247.304311564583,
		road: 8118.94004629629,
		acceleration: -0.124166666666667
	},
	{
		id: 776,
		time: 775,
		velocity: 7.32638888888889,
		power: 23.7401917875485,
		road: 8126.28111111111,
		acceleration: -0.154351851851851
	},
	{
		id: 777,
		time: 776,
		velocity: 7.05722222222222,
		power: -914.633441551916,
		road: 8133.39949074074,
		acceleration: -0.291018518518518
	},
	{
		id: 778,
		time: 777,
		velocity: 6.535,
		power: -1432.21464445936,
		road: 8140.18472222222,
		acceleration: -0.375277777777779
	},
	{
		id: 779,
		time: 778,
		velocity: 6.20055555555555,
		power: -2052.24393484931,
		road: 8146.53736111111,
		acceleration: -0.489907407407406
	},
	{
		id: 780,
		time: 779,
		velocity: 5.5875,
		power: -1481.66543904299,
		road: 8152.43949074074,
		acceleration: -0.411111111111111
	},
	{
		id: 781,
		time: 780,
		velocity: 5.30166666666667,
		power: -191.001346856973,
		road: 8158.04560185185,
		acceleration: -0.180925925925926
	},
	{
		id: 782,
		time: 781,
		velocity: 5.65777777777778,
		power: 696.383015635552,
		road: 8163.55550925926,
		acceleration: -0.0114814814814812
	},
	{
		id: 783,
		time: 782,
		velocity: 5.55305555555556,
		power: 911.975018503962,
		road: 8169.07435185185,
		acceleration: 0.0293518518518523
	},
	{
		id: 784,
		time: 783,
		velocity: 5.38972222222222,
		power: -80.348272784417,
		road: 8174.52805555555,
		acceleration: -0.15962962962963
	},
	{
		id: 785,
		time: 784,
		velocity: 5.17888888888889,
		power: 533.331185711935,
		road: 8179.88259259259,
		acceleration: -0.0387037037037041
	},
	{
		id: 786,
		time: 785,
		velocity: 5.43694444444444,
		power: 447.400610155247,
		road: 8185.19050925926,
		acceleration: -0.0545370370370364
	},
	{
		id: 787,
		time: 786,
		velocity: 5.22611111111111,
		power: 654.248393798452,
		road: 8190.4649074074,
		acceleration: -0.0125000000000002
	},
	{
		id: 788,
		time: 787,
		velocity: 5.14138888888889,
		power: -165.950232364428,
		road: 8195.6449537037,
		acceleration: -0.176203703703703
	},
	{
		id: 789,
		time: 788,
		velocity: 4.90833333333333,
		power: -203.772974878221,
		road: 8200.64472222222,
		acceleration: -0.184351851851852
	},
	{
		id: 790,
		time: 789,
		velocity: 4.67305555555556,
		power: 57.6550134518908,
		road: 8205.48828703703,
		acceleration: -0.128055555555555
	},
	{
		id: 791,
		time: 790,
		velocity: 4.75722222222222,
		power: 906.088903445551,
		road: 8210.29680555555,
		acceleration: 0.0579629629629617
	},
	{
		id: 792,
		time: 791,
		velocity: 5.08222222222222,
		power: 1951.78003332349,
		road: 8215.27018518518,
		acceleration: 0.271759259259259
	},
	{
		id: 793,
		time: 792,
		velocity: 5.48833333333333,
		power: 2764.51753139774,
		road: 8220.58166666666,
		acceleration: 0.404444444444445
	},
	{
		id: 794,
		time: 793,
		velocity: 5.97055555555556,
		power: 2807.35077759137,
		road: 8226.28162037037,
		acceleration: 0.3725
	},
	{
		id: 795,
		time: 794,
		velocity: 6.19972222222222,
		power: 3267.30704888919,
		road: 8232.37564814815,
		acceleration: 0.415648148148148
	},
	{
		id: 796,
		time: 795,
		velocity: 6.73527777777778,
		power: 3493.67300845516,
		road: 8238.88412037037,
		acceleration: 0.41324074074074
	},
	{
		id: 797,
		time: 796,
		velocity: 7.21027777777778,
		power: 3763.12081568116,
		road: 8245.80773148148,
		acceleration: 0.417037037037039
	},
	{
		id: 798,
		time: 797,
		velocity: 7.45083333333333,
		power: 3587.35227305808,
		road: 8253.11898148148,
		acceleration: 0.358240740740741
	},
	{
		id: 799,
		time: 798,
		velocity: 7.81,
		power: 3037.87524509169,
		road: 8260.73879629629,
		acceleration: 0.258888888888888
	},
	{
		id: 800,
		time: 799,
		velocity: 7.98694444444444,
		power: 3405.33673281066,
		road: 8268.63342592592,
		acceleration: 0.29074074074074
	},
	{
		id: 801,
		time: 800,
		velocity: 8.32305555555556,
		power: 3495.30420822377,
		road: 8276.81527777777,
		acceleration: 0.283703703703704
	},
	{
		id: 802,
		time: 801,
		velocity: 8.66111111111111,
		power: 4330.10353832208,
		road: 8285.32222222222,
		acceleration: 0.366481481481483
	},
	{
		id: 803,
		time: 802,
		velocity: 9.08638888888889,
		power: 4293.98821679362,
		road: 8294.18111111111,
		acceleration: 0.337407407407406
	},
	{
		id: 804,
		time: 803,
		velocity: 9.33527777777778,
		power: 3570.96225745993,
		road: 8303.32643518518,
		acceleration: 0.235462962962963
	},
	{
		id: 805,
		time: 804,
		velocity: 9.3675,
		power: 2180.49397564706,
		road: 8312.62453703703,
		acceleration: 0.0700925925925926
	},
	{
		id: 806,
		time: 805,
		velocity: 9.29666666666667,
		power: 1191.44771593656,
		road: 8321.93671296296,
		acceleration: -0.0419444444444448
	},
	{
		id: 807,
		time: 806,
		velocity: 9.20944444444445,
		power: -74.5088458650465,
		road: 8331.13611111111,
		acceleration: -0.183611111111109
	},
	{
		id: 808,
		time: 807,
		velocity: 8.81666666666667,
		power: -1190.33137479974,
		road: 8340.08759259259,
		acceleration: -0.312222222222223
	},
	{
		id: 809,
		time: 808,
		velocity: 8.36,
		power: -1146.02947798496,
		road: 8348.72856481481,
		acceleration: -0.308796296296297
	},
	{
		id: 810,
		time: 809,
		velocity: 8.28305555555555,
		power: 221.117388798113,
		road: 8357.14527777777,
		acceleration: -0.139722222222222
	},
	{
		id: 811,
		time: 810,
		velocity: 8.3975,
		power: 997.956174631445,
		road: 8365.47185185185,
		acceleration: -0.0405555555555566
	},
	{
		id: 812,
		time: 811,
		velocity: 8.23833333333333,
		power: 2302.48333988262,
		road: 8373.83925925925,
		acceleration: 0.122222222222222
	},
	{
		id: 813,
		time: 812,
		velocity: 8.64972222222222,
		power: 2021.39035021001,
		road: 8382.30921296296,
		acceleration: 0.0828703703703724
	},
	{
		id: 814,
		time: 813,
		velocity: 8.64611111111111,
		power: 2147.75332366353,
		road: 8390.86805555555,
		acceleration: 0.0949074074074048
	},
	{
		id: 815,
		time: 814,
		velocity: 8.52305555555555,
		power: 712.36024249553,
		road: 8399.4336574074,
		acceleration: -0.0813888888888883
	},
	{
		id: 816,
		time: 815,
		velocity: 8.40555555555556,
		power: 693.188969551295,
		road: 8407.91749999999,
		acceleration: -0.0821296296296303
	},
	{
		id: 817,
		time: 816,
		velocity: 8.39972222222222,
		power: 815.671686217301,
		road: 8416.32759259258,
		acceleration: -0.0653703703703687
	},
	{
		id: 818,
		time: 817,
		velocity: 8.32694444444444,
		power: 1934.89230618791,
		road: 8424.74212962962,
		acceleration: 0.0742592592592608
	},
	{
		id: 819,
		time: 818,
		velocity: 8.62833333333333,
		power: 2121.16056617263,
		road: 8433.24083333333,
		acceleration: 0.0940740740740722
	},
	{
		id: 820,
		time: 819,
		velocity: 8.68194444444444,
		power: 2600.25037164366,
		road: 8441.86037037036,
		acceleration: 0.147592592592593
	},
	{
		id: 821,
		time: 820,
		velocity: 8.76972222222222,
		power: 2518.81621648856,
		road: 8450.61939814814,
		acceleration: 0.131388888888889
	},
	{
		id: 822,
		time: 821,
		velocity: 9.0225,
		power: 4265.47739240088,
		road: 8459.60685185184,
		acceleration: 0.325462962962963
	},
	{
		id: 823,
		time: 822,
		velocity: 9.65833333333333,
		power: 6341.29069309213,
		road: 8469.02189814814,
		acceleration: 0.529722222222222
	},
	{
		id: 824,
		time: 823,
		velocity: 10.3588888888889,
		power: 7470.40307352419,
		road: 8479.00277777777,
		acceleration: 0.601944444444445
	},
	{
		id: 825,
		time: 824,
		velocity: 10.8283333333333,
		power: 8407.25111622912,
		road: 8489.60518518518,
		acceleration: 0.64111111111111
	},
	{
		id: 826,
		time: 825,
		velocity: 11.5816666666667,
		power: 10841.6772185893,
		road: 8500.93023148147,
		acceleration: 0.804166666666667
	},
	{
		id: 827,
		time: 826,
		velocity: 12.7713888888889,
		power: 11855.794096253,
		road: 8513.06421296296,
		acceleration: 0.813703703703704
	},
	{
		id: 828,
		time: 827,
		velocity: 13.2694444444444,
		power: 10928.1592710104,
		road: 8525.93925925925,
		acceleration: 0.668425925925924
	},
	{
		id: 829,
		time: 828,
		velocity: 13.5869444444444,
		power: 8480.53044204508,
		road: 8539.36481481481,
		acceleration: 0.432592592592595
	},
	{
		id: 830,
		time: 829,
		velocity: 14.0691666666667,
		power: 13157.7843018854,
		road: 8553.37912037036,
		acceleration: 0.744907407407405
	},
	{
		id: 831,
		time: 830,
		velocity: 15.5041666666667,
		power: 18793.1772943218,
		road: 8568.29851851851,
		acceleration: 1.06527777777778
	},
	{
		id: 832,
		time: 831,
		velocity: 16.7827777777778,
		power: 29694.2256003526,
		road: 8584.56652777777,
		acceleration: 1.63194444444445
	},
	{
		id: 833,
		time: 832,
		velocity: 18.965,
		power: 31881.508593307,
		road: 8602.42916666666,
		acceleration: 1.55731481481482
	},
	{
		id: 834,
		time: 833,
		velocity: 20.1761111111111,
		power: 25027.5245504195,
		road: 8621.58537037037,
		acceleration: 1.02981481481481
	},
	{
		id: 835,
		time: 834,
		velocity: 19.8722222222222,
		power: 9503.63085075014,
		road: 8641.33407407407,
		acceleration: 0.155185185185182
	},
	{
		id: 836,
		time: 835,
		velocity: 19.4305555555556,
		power: 125.287354124081,
		road: 8660.9911574074,
		acceleration: -0.338425925925925
	},
	{
		id: 837,
		time: 836,
		velocity: 19.1608333333333,
		power: -1869.04038062236,
		road: 8680.26009259259,
		acceleration: -0.437870370370369
	},
	{
		id: 838,
		time: 837,
		velocity: 18.5586111111111,
		power: -2437.27379817948,
		road: 8699.07898148148,
		acceleration: -0.462222222222223
	},
	{
		id: 839,
		time: 838,
		velocity: 18.0438888888889,
		power: -2548.15590681294,
		road: 8717.43560185185,
		acceleration: -0.462314814814814
	},
	{
		id: 840,
		time: 839,
		velocity: 17.7738888888889,
		power: -710.64899831994,
		road: 8735.38587962963,
		acceleration: -0.350370370370374
	},
	{
		id: 841,
		time: 840,
		velocity: 17.5075,
		power: 2477.72565237114,
		road: 8753.08217592592,
		acceleration: -0.157592592592586
	},
	{
		id: 842,
		time: 841,
		velocity: 17.5711111111111,
		power: 6138.61651363794,
		road: 8770.7299537037,
		acceleration: 0.0605555555555526
	},
	{
		id: 843,
		time: 842,
		velocity: 17.9555555555556,
		power: 11338.9567333766,
		road: 8788.5862037037,
		acceleration: 0.35638888888889
	},
	{
		id: 844,
		time: 843,
		velocity: 18.5766666666667,
		power: 9656.99288196504,
		road: 8806.74189814815,
		acceleration: 0.2425
	},
	{
		id: 845,
		time: 844,
		velocity: 18.2986111111111,
		power: 5905.81423578049,
		road: 8825.02958333333,
		acceleration: 0.0214814814814801
	},
	{
		id: 846,
		time: 845,
		velocity: 18.02,
		power: -847.899456224848,
		road: 8843.14736111111,
		acceleration: -0.361296296296299
	},
	{
		id: 847,
		time: 846,
		velocity: 17.4927777777778,
		power: -689.613645035862,
		road: 8860.91157407407,
		acceleration: -0.345833333333335
	},
	{
		id: 848,
		time: 847,
		velocity: 17.2611111111111,
		power: -165.464923528078,
		road: 8878.34851851851,
		acceleration: -0.308703703703703
	},
	{
		id: 849,
		time: 848,
		velocity: 17.0938888888889,
		power: 4521.11992271991,
		road: 8895.62013888889,
		acceleration: -0.0219444444444434
	},
	{
		id: 850,
		time: 849,
		velocity: 17.4269444444444,
		power: 6974.35248190631,
		road: 8912.94291666666,
		acceleration: 0.124259259259262
	},
	{
		id: 851,
		time: 850,
		velocity: 17.6338888888889,
		power: 9567.45708009059,
		road: 8930.46296296296,
		acceleration: 0.270277777777778
	},
	{
		id: 852,
		time: 851,
		velocity: 17.9047222222222,
		power: 8285.78333138014,
		road: 8948.20962962963,
		acceleration: 0.182962962962961
	},
	{
		id: 853,
		time: 852,
		velocity: 17.9758333333333,
		power: 5963.66814496856,
		road: 8966.0686574074,
		acceleration: 0.0417592592592584
	},
	{
		id: 854,
		time: 853,
		velocity: 17.7591666666667,
		power: 4515.21680067244,
		road: 8983.92708333333,
		acceleration: -0.0429629629629638
	},
	{
		id: 855,
		time: 854,
		velocity: 17.7758333333333,
		power: 4438.15194464685,
		road: 9001.74106481481,
		acceleration: -0.0459259259259213
	},
	{
		id: 856,
		time: 855,
		velocity: 17.8380555555556,
		power: 7039.58070674718,
		road: 9019.58476851852,
		acceleration: 0.10537037037037
	},
	{
		id: 857,
		time: 856,
		velocity: 18.0752777777778,
		power: 6939.27411859953,
		road: 9037.52875,
		acceleration: 0.095185185185187
	},
	{
		id: 858,
		time: 857,
		velocity: 18.0613888888889,
		power: 7303.8223929952,
		road: 9055.57625,
		acceleration: 0.111851851851849
	},
	{
		id: 859,
		time: 858,
		velocity: 18.1736111111111,
		power: 6067.63810927976,
		road: 9073.69833333333,
		acceleration: 0.0373148148148132
	},
	{
		id: 860,
		time: 859,
		velocity: 18.1872222222222,
		power: 6854.48295657353,
		road: 9091.87916666667,
		acceleration: 0.0801851851851865
	},
	{
		id: 861,
		time: 860,
		velocity: 18.3019444444444,
		power: 6946.80677021445,
		road: 9110.14111111111,
		acceleration: 0.0820370370370362
	},
	{
		id: 862,
		time: 861,
		velocity: 18.4197222222222,
		power: 9309.50175026872,
		road: 9128.54902777778,
		acceleration: 0.209907407407407
	},
	{
		id: 863,
		time: 862,
		velocity: 18.8169444444444,
		power: 10133.0650314759,
		road: 9147.18430555555,
		acceleration: 0.244814814814816
	},
	{
		id: 864,
		time: 863,
		velocity: 19.0363888888889,
		power: 11418.8737871497,
		road: 9166.09287037037,
		acceleration: 0.30175925925926
	},
	{
		id: 865,
		time: 864,
		velocity: 19.325,
		power: 10184.6319679734,
		road: 9185.2624537037,
		acceleration: 0.220277777777778
	},
	{
		id: 866,
		time: 865,
		velocity: 19.4777777777778,
		power: 8955.23686544129,
		road: 9204.61453703704,
		acceleration: 0.144722222222221
	},
	{
		id: 867,
		time: 866,
		velocity: 19.4705555555556,
		power: 6387.02554812067,
		road: 9224.04060185185,
		acceleration: 0.00324074074073977
	},
	{
		id: 868,
		time: 867,
		velocity: 19.3347222222222,
		power: 5203.28540555939,
		road: 9243.43861111111,
		acceleration: -0.0593518518518543
	},
	{
		id: 869,
		time: 868,
		velocity: 19.2997222222222,
		power: 5795.97202122873,
		road: 9262.79402777778,
		acceleration: -0.0258333333333276
	},
	{
		id: 870,
		time: 869,
		velocity: 19.3930555555556,
		power: 7186.80165651547,
		road: 9282.16087962963,
		acceleration: 0.0487037037037013
	},
	{
		id: 871,
		time: 870,
		velocity: 19.4808333333333,
		power: 7145.6942107568,
		road: 9301.57435185185,
		acceleration: 0.0445370370370348
	},
	{
		id: 872,
		time: 871,
		velocity: 19.4333333333333,
		power: 4606.37122571877,
		road: 9320.96449074074,
		acceleration: -0.0912037037036981
	},
	{
		id: 873,
		time: 872,
		velocity: 19.1194444444444,
		power: 3232.20032662309,
		road: 9340.22837962963,
		acceleration: -0.1612962962963
	},
	{
		id: 874,
		time: 873,
		velocity: 18.9969444444444,
		power: 4200.51778154846,
		road: 9359.35944444445,
		acceleration: -0.104351851851852
	},
	{
		id: 875,
		time: 874,
		velocity: 19.1202777777778,
		power: 4624.09640225861,
		road: 9378.39930555556,
		acceleration: -0.0780555555555544
	},
	{
		id: 876,
		time: 875,
		velocity: 18.8852777777778,
		power: 3744.33830125996,
		road: 9397.33861111111,
		acceleration: -0.123055555555556
	},
	{
		id: 877,
		time: 876,
		velocity: 18.6277777777778,
		power: 1734.04321562105,
		road: 9416.10175925926,
		acceleration: -0.229259259259258
	},
	{
		id: 878,
		time: 877,
		velocity: 18.4325,
		power: 917.525341349552,
		road: 9434.61587962963,
		acceleration: -0.268796296296294
	},
	{
		id: 879,
		time: 878,
		velocity: 18.0788888888889,
		power: 417.986891830868,
		road: 9452.85018518519,
		acceleration: -0.290833333333335
	},
	{
		id: 880,
		time: 879,
		velocity: 17.7552777777778,
		power: 1434.9363688171,
		road: 9470.82601851852,
		acceleration: -0.226111111111113
	},
	{
		id: 881,
		time: 880,
		velocity: 17.7541666666667,
		power: 2798.12207459402,
		road: 9488.61805555556,
		acceleration: -0.141481481481478
	},
	{
		id: 882,
		time: 881,
		velocity: 17.6544444444444,
		power: 2897.83413425941,
		road: 9506.27356481482,
		acceleration: -0.131574074074077
	},
	{
		id: 883,
		time: 882,
		velocity: 17.3605555555556,
		power: -1014.51181907528,
		road: 9523.6837037037,
		acceleration: -0.359166666666667
	},
	{
		id: 884,
		time: 883,
		velocity: 16.6766666666667,
		power: -3396.4856800951,
		road: 9540.66472222222,
		acceleration: -0.499074074074073
	},
	{
		id: 885,
		time: 884,
		velocity: 16.1572222222222,
		power: -6127.32307936443,
		road: 9557.0612962963,
		acceleration: -0.669814814814814
	},
	{
		id: 886,
		time: 885,
		velocity: 15.3511111111111,
		power: -8961.35056458356,
		road: 9572.69041666666,
		acceleration: -0.865092592592594
	},
	{
		id: 887,
		time: 886,
		velocity: 14.0813888888889,
		power: -11724.6869170317,
		road: 9587.34388888889,
		acceleration: -1.0862037037037
	},
	{
		id: 888,
		time: 887,
		velocity: 12.8986111111111,
		power: -13085.5541270387,
		road: 9600.83101851852,
		acceleration: -1.24648148148148
	},
	{
		id: 889,
		time: 888,
		velocity: 11.6116666666667,
		power: -11245.5592300594,
		road: 9613.10856481481,
		acceleration: -1.17268518518519
	},
	{
		id: 890,
		time: 889,
		velocity: 10.5633333333333,
		power: -9166.33516770708,
		road: 9624.27018518518,
		acceleration: -1.05916666666666
	},
	{
		id: 891,
		time: 890,
		velocity: 9.72111111111111,
		power: -5666.72730946925,
		road: 9634.51875,
		acceleration: -0.766944444444444
	},
	{
		id: 892,
		time: 891,
		velocity: 9.31083333333333,
		power: -3331.4897358208,
		road: 9644.11189814815,
		acceleration: -0.543888888888889
	},
	{
		id: 893,
		time: 892,
		velocity: 8.93166666666667,
		power: -1010.12891771434,
		road: 9653.28787037037,
		acceleration: -0.290462962962962
	},
	{
		id: 894,
		time: 893,
		velocity: 8.84972222222222,
		power: -2138.26881011041,
		road: 9662.10564814814,
		acceleration: -0.425925925925926
	},
	{
		id: 895,
		time: 894,
		velocity: 8.03305555555556,
		power: -2555.87927601106,
		road: 9670.46648148148,
		acceleration: -0.487962962962962
	},
	{
		id: 896,
		time: 895,
		velocity: 7.46777777777778,
		power: -2513.86984686213,
		road: 9678.33430555555,
		acceleration: -0.498055555555557
	},
	{
		id: 897,
		time: 896,
		velocity: 7.35555555555556,
		power: -1028.67976661876,
		road: 9685.80129629629,
		acceleration: -0.303611111111111
	},
	{
		id: 898,
		time: 897,
		velocity: 7.12222222222222,
		power: 359.984403188715,
		road: 9693.06398148148,
		acceleration: -0.105000000000001
	},
	{
		id: 899,
		time: 898,
		velocity: 7.15277777777778,
		power: 667.589581318104,
		road: 9700.24481481481,
		acceleration: -0.0587037037037046
	},
	{
		id: 900,
		time: 899,
		velocity: 7.17944444444444,
		power: 1054.91200750997,
		road: 9707.39574074074,
		acceleration: -0.00111111111110951
	},
	{
		id: 901,
		time: 900,
		velocity: 7.11888888888889,
		power: 183.037806890214,
		road: 9714.48185185185,
		acceleration: -0.128518518518518
	},
	{
		id: 902,
		time: 901,
		velocity: 6.76722222222222,
		power: 1233.20654702076,
		road: 9721.51819444444,
		acceleration: 0.0289814814814813
	},
	{
		id: 903,
		time: 902,
		velocity: 7.26638888888889,
		power: 1987.94849802461,
		road: 9728.63782407407,
		acceleration: 0.137592592592592
	},
	{
		id: 904,
		time: 903,
		velocity: 7.53166666666667,
		power: 6746.27685015834,
		road: 9736.21425925926,
		acceleration: 0.776018518518518
	},
	{
		id: 905,
		time: 904,
		velocity: 9.09527777777778,
		power: 8375.79641025082,
		road: 9744.61851851852,
		acceleration: 0.879629629629631
	},
	{
		id: 906,
		time: 905,
		velocity: 9.90527777777778,
		power: 12154.9910014419,
		road: 9754.05023148148,
		acceleration: 1.17527777777778
	},
	{
		id: 907,
		time: 906,
		velocity: 11.0575,
		power: 9981.60900049494,
		road: 9764.47740740741,
		acceleration: 0.815648148148147
	},
	{
		id: 908,
		time: 907,
		velocity: 11.5422222222222,
		power: 8438.45662364475,
		road: 9775.6112037037,
		acceleration: 0.597592592592592
	},
	{
		id: 909,
		time: 908,
		velocity: 11.6980555555556,
		power: 4544.59039218723,
		road: 9787.14907407407,
		acceleration: 0.210555555555555
	},
	{
		id: 910,
		time: 909,
		velocity: 11.6891666666667,
		power: 4546.45580160691,
		road: 9798.89259259259,
		acceleration: 0.200740740740741
	},
	{
		id: 911,
		time: 910,
		velocity: 12.1444444444444,
		power: 6408.56485278422,
		road: 9810.91152777778,
		acceleration: 0.350092592592592
	},
	{
		id: 912,
		time: 911,
		velocity: 12.7483333333333,
		power: 8524.12316814586,
		road: 9823.35703703704,
		acceleration: 0.503055555555555
	},
	{
		id: 913,
		time: 912,
		velocity: 13.1983333333333,
		power: 9170.13697031298,
		road: 9836.3137962963,
		acceleration: 0.519444444444446
	},
	{
		id: 914,
		time: 913,
		velocity: 13.7027777777778,
		power: 7880.6883340956,
		road: 9849.72365740741,
		acceleration: 0.386759259259257
	},
	{
		id: 915,
		time: 914,
		velocity: 13.9086111111111,
		power: 6484.74436338687,
		road: 9863.45726851852,
		acceleration: 0.260740740740742
	},
	{
		id: 916,
		time: 915,
		velocity: 13.9805555555556,
		power: 5275.71706432647,
		road: 9877.40083333334,
		acceleration: 0.159166666666666
	},
	{
		id: 917,
		time: 916,
		velocity: 14.1802777777778,
		power: 3292.14455378464,
		road: 9891.42768518519,
		acceleration: 0.00740740740740975
	},
	{
		id: 918,
		time: 917,
		velocity: 13.9308333333333,
		power: 2637.97452560275,
		road: 9905.43777777778,
		acceleration: -0.0409259259259294
	},
	{
		id: 919,
		time: 918,
		velocity: 13.8577777777778,
		power: 2583.99210088025,
		road: 9919.40555555556,
		acceleration: -0.0437037037037022
	},
	{
		id: 920,
		time: 919,
		velocity: 14.0491666666667,
		power: 5429.52558976798,
		road: 9933.43490740741,
		acceleration: 0.166851851851852
	},
	{
		id: 921,
		time: 920,
		velocity: 14.4313888888889,
		power: 6787.49131820352,
		road: 9947.6762962963,
		acceleration: 0.257222222222222
	},
	{
		id: 922,
		time: 921,
		velocity: 14.6294444444444,
		power: 8927.40284169282,
		road: 9962.24351851852,
		acceleration: 0.394444444444446
	},
	{
		id: 923,
		time: 922,
		velocity: 15.2325,
		power: 9185.91066061928,
		road: 9977.2025,
		acceleration: 0.389074074074074
	},
	{
		id: 924,
		time: 923,
		velocity: 15.5986111111111,
		power: 9453.00063160846,
		road: 9992.54819444445,
		acceleration: 0.384351851851852
	},
	{
		id: 925,
		time: 924,
		velocity: 15.7825,
		power: 8848.52395260654,
		road: 10008.2476851852,
		acceleration: 0.323240740740738
	},
	{
		id: 926,
		time: 925,
		velocity: 16.2022222222222,
		power: 7153.04955742105,
		road: 10024.2076851852,
		acceleration: 0.197777777777778
	},
	{
		id: 927,
		time: 926,
		velocity: 16.1919444444444,
		power: 4918.63541982327,
		road: 10040.2899074074,
		acceleration: 0.0466666666666669
	},
	{
		id: 928,
		time: 927,
		velocity: 15.9225,
		power: 1467.81294773028,
		road: 10056.3073611111,
		acceleration: -0.176203703703704
	},
	{
		id: 929,
		time: 928,
		velocity: 15.6736111111111,
		power: -737.532295172999,
		road: 10072.0784259259,
		acceleration: -0.316574074074072
	},
	{
		id: 930,
		time: 929,
		velocity: 15.2422222222222,
		power: 143.169411525186,
		road: 10087.5647222222,
		acceleration: -0.252962962962961
	},
	{
		id: 931,
		time: 930,
		velocity: 15.1636111111111,
		power: 3078.97003387465,
		road: 10102.8995833333,
		acceleration: -0.0499074074074084
	},
	{
		id: 932,
		time: 931,
		velocity: 15.5238888888889,
		power: 6194.38418162935,
		road: 10118.2895833333,
		acceleration: 0.160185185185185
	},
	{
		id: 933,
		time: 932,
		velocity: 15.7227777777778,
		power: 9823.89458616109,
		road: 10133.9548148148,
		acceleration: 0.390277777777778
	},
	{
		id: 934,
		time: 933,
		velocity: 16.3344444444444,
		power: 9986.16908507103,
		road: 10150.0042592593,
		acceleration: 0.378148148148147
	},
	{
		id: 935,
		time: 934,
		velocity: 16.6583333333333,
		power: 11346.5453589041,
		road: 10166.4631481481,
		acceleration: 0.44074074074074
	},
	{
		id: 936,
		time: 935,
		velocity: 17.045,
		power: 7724.0885583263,
		road: 10183.2400462963,
		acceleration: 0.195277777777779
	},
	{
		id: 937,
		time: 936,
		velocity: 16.9202777777778,
		power: 2199.36292845292,
		road: 10200.0397685185,
		acceleration: -0.149629629629629
	},
	{
		id: 938,
		time: 937,
		velocity: 16.2094444444444,
		power: -604.783899544194,
		road: 10216.604537037,
		acceleration: -0.320277777777779
	},
	{
		id: 939,
		time: 938,
		velocity: 16.0841666666667,
		power: 736.562160904431,
		road: 10232.8943055556,
		acceleration: -0.229722222222222
	},
	{
		id: 940,
		time: 939,
		velocity: 16.2311111111111,
		power: 5651.51940647817,
		road: 10249.1135648148,
		acceleration: 0.0887037037037004
	},
	{
		id: 941,
		time: 940,
		velocity: 16.4755555555556,
		power: 8202.91318998251,
		road: 10265.499537037,
		acceleration: 0.244722222222226
	},
	{
		id: 942,
		time: 941,
		velocity: 16.8183333333333,
		power: 7784.94882551173,
		road: 10282.1113888889,
		acceleration: 0.207037037037036
	},
	{
		id: 943,
		time: 942,
		velocity: 16.8522222222222,
		power: 7367.87948047149,
		road: 10298.9127314815,
		acceleration: 0.171944444444446
	},
	{
		id: 944,
		time: 943,
		velocity: 16.9913888888889,
		power: 6417.62517436218,
		road: 10315.8534722222,
		acceleration: 0.106851851851854
	},
	{
		id: 945,
		time: 944,
		velocity: 17.1388888888889,
		power: 5927.88804877371,
		road: 10332.8841203704,
		acceleration: 0.0729629629629613
	},
	{
		id: 946,
		time: 945,
		velocity: 17.0711111111111,
		power: 5516.13564134236,
		road: 10349.9739351852,
		acceleration: 0.0453703703703674
	},
	{
		id: 947,
		time: 946,
		velocity: 17.1275,
		power: 4908.33836695562,
		road: 10367.0900462963,
		acceleration: 0.00722222222222513
	},
	{
		id: 948,
		time: 947,
		velocity: 17.1605555555556,
		power: 4685.71245477853,
		road: 10384.2065740741,
		acceleration: -0.006388888888889
	},
	{
		id: 949,
		time: 948,
		velocity: 17.0519444444444,
		power: 2161.26073003941,
		road: 10401.2407407407,
		acceleration: -0.158333333333331
	},
	{
		id: 950,
		time: 949,
		velocity: 16.6525,
		power: 2151.2878451769,
		road: 10418.1183796296,
		acceleration: -0.154722222222226
	},
	{
		id: 951,
		time: 950,
		velocity: 16.6963888888889,
		power: 1771.12997009565,
		road: 10434.8316203704,
		acceleration: -0.17407407407407
	},
	{
		id: 952,
		time: 951,
		velocity: 16.5297222222222,
		power: 1677.25775059363,
		road: 10451.3700462963,
		acceleration: -0.175555555555558
	},
	{
		id: 953,
		time: 952,
		velocity: 16.1258333333333,
		power: -2743.56713406937,
		road: 10467.5943518519,
		acceleration: -0.452685185185183
	},
	{
		id: 954,
		time: 953,
		velocity: 15.3383333333333,
		power: -2773.87463940065,
		road: 10483.3664814815,
		acceleration: -0.451666666666668
	},
	{
		id: 955,
		time: 954,
		velocity: 15.1747222222222,
		power: -3066.17893382197,
		road: 10498.6781944444,
		acceleration: -0.469166666666666
	},
	{
		id: 956,
		time: 955,
		velocity: 14.7183333333333,
		power: -394.088502243466,
		road: 10513.6149074074,
		acceleration: -0.280833333333334
	},
	{
		id: 957,
		time: 956,
		velocity: 14.4958333333333,
		power: -318.961625709714,
		road: 10528.2755092593,
		acceleration: -0.27138888888889
	},
	{
		id: 958,
		time: 957,
		velocity: 14.3605555555556,
		power: 200.94470351049,
		road: 10542.685462963,
		acceleration: -0.229907407407405
	},
	{
		id: 959,
		time: 958,
		velocity: 14.0286111111111,
		power: -885.181428109989,
		road: 10556.8275925926,
		acceleration: -0.305740740740744
	},
	{
		id: 960,
		time: 959,
		velocity: 13.5786111111111,
		power: -1204.82548535396,
		road: 10570.6536111111,
		acceleration: -0.326481481481482
	},
	{
		id: 961,
		time: 960,
		velocity: 13.3811111111111,
		power: -2246.73325105271,
		road: 10584.1141666667,
		acceleration: -0.404444444444444
	},
	{
		id: 962,
		time: 961,
		velocity: 12.8152777777778,
		power: -950.366982856242,
		road: 10597.2223611111,
		acceleration: -0.300277777777778
	},
	{
		id: 963,
		time: 962,
		velocity: 12.6777777777778,
		power: -4688.41150558351,
		road: 10609.8774074074,
		acceleration: -0.606018518518519
	},
	{
		id: 964,
		time: 963,
		velocity: 11.5630555555556,
		power: -4238.72743666142,
		road: 10621.9405092593,
		acceleration: -0.57787037037037
	},
	{
		id: 965,
		time: 964,
		velocity: 11.0816666666667,
		power: -5909.80737826885,
		road: 10633.3425,
		acceleration: -0.744351851851851
	},
	{
		id: 966,
		time: 965,
		velocity: 10.4447222222222,
		power: -3520.41241575128,
		road: 10644.1043518519,
		acceleration: -0.535925925925927
	},
	{
		id: 967,
		time: 966,
		velocity: 9.95527777777778,
		power: -2881.90895771289,
		road: 10654.3574074074,
		acceleration: -0.481666666666666
	},
	{
		id: 968,
		time: 967,
		velocity: 9.63666666666667,
		power: -3034.19000218451,
		road: 10664.1158796296,
		acceleration: -0.5075
	},
	{
		id: 969,
		time: 968,
		velocity: 8.92222222222222,
		power: -2053.14073819213,
		road: 10673.4166203704,
		acceleration: -0.407962962962962
	},
	{
		id: 970,
		time: 969,
		velocity: 8.73138888888889,
		power: -3067.67662024128,
		road: 10682.2452314815,
		acceleration: -0.536296296296298
	},
	{
		id: 971,
		time: 970,
		velocity: 8.02777777777778,
		power: -2271.29470362901,
		road: 10690.5792592593,
		acceleration: -0.452870370370372
	},
	{
		id: 972,
		time: 971,
		velocity: 7.56361111111111,
		power: -1892.21746818531,
		road: 10698.4797222222,
		acceleration: -0.414259259259258
	},
	{
		id: 973,
		time: 972,
		velocity: 7.48861111111111,
		power: -146.28812009337,
		road: 10706.082962963,
		acceleration: -0.180185185185184
	},
	{
		id: 974,
		time: 973,
		velocity: 7.48722222222222,
		power: 986.351749775123,
		road: 10713.5856481481,
		acceleration: -0.0209259259259262
	},
	{
		id: 975,
		time: 974,
		velocity: 7.50083333333333,
		power: 999.699497218318,
		road: 10721.0686111111,
		acceleration: -0.018518518518519
	},
	{
		id: 976,
		time: 975,
		velocity: 7.43305555555556,
		power: 687.716016220052,
		road: 10728.5115740741,
		acceleration: -0.061481481481481
	},
	{
		id: 977,
		time: 976,
		velocity: 7.30277777777778,
		power: 170.891358864265,
		road: 10735.8571296296,
		acceleration: -0.133333333333333
	},
	{
		id: 978,
		time: 977,
		velocity: 7.10083333333333,
		power: -71.1032542776934,
		road: 10743.0525462963,
		acceleration: -0.166944444444445
	},
	{
		id: 979,
		time: 978,
		velocity: 6.93222222222222,
		power: -642.484831102173,
		road: 10750.0387037037,
		acceleration: -0.251574074074075
	},
	{
		id: 980,
		time: 979,
		velocity: 6.54805555555556,
		power: -852.151156569892,
		road: 10756.7559722222,
		acceleration: -0.286203703703703
	},
	{
		id: 981,
		time: 980,
		velocity: 6.24222222222222,
		power: -1271.39562856999,
		road: 10763.1503703704,
		acceleration: -0.359537037037037
	},
	{
		id: 982,
		time: 981,
		velocity: 5.85361111111111,
		power: -649.148032267426,
		road: 10769.2347685185,
		acceleration: -0.260462962962963
	},
	{
		id: 983,
		time: 982,
		velocity: 5.76666666666667,
		power: 290.016016979277,
		road: 10775.14125,
		acceleration: -0.0953703703703699
	},
	{
		id: 984,
		time: 983,
		velocity: 5.95611111111111,
		power: 2730.73027968124,
		road: 10781.1645833333,
		acceleration: 0.329074074074073
	},
	{
		id: 985,
		time: 984,
		velocity: 6.84083333333333,
		power: 4445.71744810136,
		road: 10787.6380555556,
		acceleration: 0.571203703703704
	},
	{
		id: 986,
		time: 985,
		velocity: 7.48027777777778,
		power: 6429.80929017029,
		road: 10794.7914351852,
		acceleration: 0.788611111111111
	},
	{
		id: 987,
		time: 986,
		velocity: 8.32194444444444,
		power: 6224.21420934486,
		road: 10802.6728240741,
		acceleration: 0.667407407407407
	},
	{
		id: 988,
		time: 987,
		velocity: 8.84305555555556,
		power: 4930.11875208433,
		road: 10811.1110185185,
		acceleration: 0.446203703703704
	},
	{
		id: 989,
		time: 988,
		velocity: 8.81888888888889,
		power: 1733.87266855864,
		road: 10819.7922685185,
		acceleration: 0.0399074074074068
	},
	{
		id: 990,
		time: 989,
		velocity: 8.44166666666667,
		power: -1022.94518985105,
		road: 10828.3463425926,
		acceleration: -0.294259259259258
	},
	{
		id: 991,
		time: 990,
		velocity: 7.96027777777778,
		power: -1839.26450791819,
		road: 10836.5528703704,
		acceleration: -0.400833333333335
	},
	{
		id: 992,
		time: 991,
		velocity: 7.61638888888889,
		power: -1620.65735737213,
		road: 10844.3691203704,
		acceleration: -0.379722222222221
	},
	{
		id: 993,
		time: 992,
		velocity: 7.3025,
		power: -1894.13159624811,
		road: 10851.782037037,
		acceleration: -0.426944444444445
	},
	{
		id: 994,
		time: 993,
		velocity: 6.67944444444444,
		power: -2264.32353658016,
		road: 10858.7329166667,
		acceleration: -0.497129629629629
	},
	{
		id: 995,
		time: 994,
		velocity: 6.125,
		power: -3723.31733861649,
		road: 10865.0503240741,
		acceleration: -0.769814814814815
	},
	{
		id: 996,
		time: 995,
		velocity: 4.99305555555556,
		power: -3695.07800981689,
		road: 10870.5575462963,
		acceleration: -0.850555555555555
	},
	{
		id: 997,
		time: 996,
		velocity: 4.12777777777778,
		power: -2758.79594195701,
		road: 10875.260787037,
		acceleration: -0.757407407407408
	},
	{
		id: 998,
		time: 997,
		velocity: 3.85277777777778,
		power: -1632.7169807878,
		road: 10879.3043981481,
		acceleration: -0.561851851851852
	},
	{
		id: 999,
		time: 998,
		velocity: 3.3075,
		power: -1025.39796196873,
		road: 10882.8474074074,
		acceleration: -0.439351851851852
	},
	{
		id: 1000,
		time: 999,
		velocity: 2.80972222222222,
		power: -403.480633298958,
		road: 10886.0375462963,
		acceleration: -0.266388888888889
	},
	{
		id: 1001,
		time: 1000,
		velocity: 3.05361111111111,
		power: 16.1390675446161,
		road: 10889.0311111111,
		acceleration: -0.12675925925926
	},
	{
		id: 1002,
		time: 1001,
		velocity: 2.92722222222222,
		power: -455.908373278885,
		road: 10891.8089351852,
		acceleration: -0.304722222222222
	},
	{
		id: 1003,
		time: 1002,
		velocity: 1.89555555555556,
		power: -1782.28016854195,
		road: 10893.925462963,
		acceleration: -1.01787037037037
	},
	{
		id: 1004,
		time: 1003,
		velocity: 0,
		power: -899.530343511454,
		road: 10895.0451851852,
		acceleration: -0.975740740740741
	},
	{
		id: 1005,
		time: 1004,
		velocity: 0,
		power: -150.942645484081,
		road: 10895.3611111111,
		acceleration: -0.631851851851852
	},
	{
		id: 1006,
		time: 1005,
		velocity: 0,
		power: 58.8926919829991,
		road: 10895.5083796296,
		acceleration: 0.294537037037037
	},
	{
		id: 1007,
		time: 1006,
		velocity: 0.883611111111111,
		power: 536.18163556391,
		road: 10896.1679166667,
		acceleration: 0.73
	},
	{
		id: 1008,
		time: 1007,
		velocity: 2.19,
		power: 1464.48651126392,
		road: 10897.6493518519,
		acceleration: 0.913796296296296
	},
	{
		id: 1009,
		time: 1008,
		velocity: 2.74138888888889,
		power: 1581.3333539967,
		road: 10899.8938888889,
		acceleration: 0.612407407407407
	},
	{
		id: 1010,
		time: 1009,
		velocity: 2.72083333333333,
		power: 586.552635529208,
		road: 10902.4977314815,
		acceleration: 0.106203703703704
	},
	{
		id: 1011,
		time: 1010,
		velocity: 2.50861111111111,
		power: -1035.72982395157,
		road: 10904.8581018519,
		acceleration: -0.593148148148148
	},
	{
		id: 1012,
		time: 1011,
		velocity: 0.961944444444444,
		power: -1187.88148683991,
		road: 10906.4684259259,
		acceleration: -0.906944444444445
	},
	{
		id: 1013,
		time: 1012,
		velocity: 0,
		power: -495.937112454922,
		road: 10907.2071759259,
		acceleration: -0.836203703703704
	},
	{
		id: 1014,
		time: 1013,
		velocity: 0,
		power: -29.3319362735543,
		road: 10907.3675,
		acceleration: -0.320648148148148
	},
	{
		id: 1015,
		time: 1014,
		velocity: 0,
		power: 0,
		road: 10907.3675,
		acceleration: 0
	},
	{
		id: 1016,
		time: 1015,
		velocity: 0,
		power: 0,
		road: 10907.3675,
		acceleration: 0
	},
	{
		id: 1017,
		time: 1016,
		velocity: 0,
		power: 0,
		road: 10907.3675,
		acceleration: 0
	},
	{
		id: 1018,
		time: 1017,
		velocity: 0,
		power: 0,
		road: 10907.3675,
		acceleration: 0
	},
	{
		id: 1019,
		time: 1018,
		velocity: 0,
		power: 0,
		road: 10907.3675,
		acceleration: 0
	},
	{
		id: 1020,
		time: 1019,
		velocity: 0,
		power: 0,
		road: 10907.3675,
		acceleration: 0
	},
	{
		id: 1021,
		time: 1020,
		velocity: 0,
		power: 0,
		road: 10907.3675,
		acceleration: 0
	},
	{
		id: 1022,
		time: 1021,
		velocity: 0,
		power: 0,
		road: 10907.3675,
		acceleration: 0
	},
	{
		id: 1023,
		time: 1022,
		velocity: 0,
		power: 0,
		road: 10907.3675,
		acceleration: 0
	},
	{
		id: 1024,
		time: 1023,
		velocity: 0,
		power: 27.4765772796479,
		road: 10907.4601851852,
		acceleration: 0.18537037037037
	},
	{
		id: 1025,
		time: 1024,
		velocity: 0.556111111111111,
		power: 579.686537023066,
		road: 10908.0772685185,
		acceleration: 0.863425925925926
	},
	{
		id: 1026,
		time: 1025,
		velocity: 2.59027777777778,
		power: 2293.20703386811,
		road: 10909.7740277778,
		acceleration: 1.29592592592593
	},
	{
		id: 1027,
		time: 1026,
		velocity: 3.88777777777778,
		power: 4880.77660869708,
		road: 10912.8800462963,
		acceleration: 1.52259259259259
	},
	{
		id: 1028,
		time: 1027,
		velocity: 5.12388888888889,
		power: 4089.85213819842,
		road: 10917.1792592593,
		acceleration: 0.863796296296297
	},
	{
		id: 1029,
		time: 1028,
		velocity: 5.18166666666667,
		power: 2640.18038376797,
		road: 10922.1209722222,
		acceleration: 0.421203703703704
	},
	{
		id: 1030,
		time: 1029,
		velocity: 5.15138888888889,
		power: 1658.35330905146,
		road: 10927.3681481481,
		acceleration: 0.189722222222223
	},
	{
		id: 1031,
		time: 1030,
		velocity: 5.69305555555556,
		power: 2629.07742228685,
		road: 10932.8884722222,
		acceleration: 0.356574074074072
	},
	{
		id: 1032,
		time: 1031,
		velocity: 6.25138888888889,
		power: 3365.87758281869,
		road: 10938.8123611111,
		acceleration: 0.450555555555557
	},
	{
		id: 1033,
		time: 1032,
		velocity: 6.50305555555556,
		power: 3462.23690859856,
		road: 10945.1726851852,
		acceleration: 0.422314814814815
	},
	{
		id: 1034,
		time: 1033,
		velocity: 6.96,
		power: 3671.35565236954,
		road: 10951.9522222222,
		acceleration: 0.41611111111111
	},
	{
		id: 1035,
		time: 1034,
		velocity: 7.49972222222222,
		power: 4725.53611728443,
		road: 10959.2037962963,
		acceleration: 0.527962962962963
	},
	{
		id: 1036,
		time: 1035,
		velocity: 8.08694444444444,
		power: 4787.96823208109,
		road: 10966.9629166667,
		acceleration: 0.487129629629629
	},
	{
		id: 1037,
		time: 1036,
		velocity: 8.42138888888889,
		power: 5389.98187855515,
		road: 10975.2253240741,
		acceleration: 0.519444444444446
	},
	{
		id: 1038,
		time: 1037,
		velocity: 9.05805555555556,
		power: 5739.18448839268,
		road: 10984.0052777778,
		acceleration: 0.515648148148149
	},
	{
		id: 1039,
		time: 1038,
		velocity: 9.63388888888889,
		power: 6770.66736175429,
		road: 10993.3357407407,
		acceleration: 0.58537037037037
	},
	{
		id: 1040,
		time: 1039,
		velocity: 10.1775,
		power: 6477.79519888437,
		road: 11003.2118981481,
		acceleration: 0.50601851851852
	},
	{
		id: 1041,
		time: 1040,
		velocity: 10.5761111111111,
		power: 5258.9567064363,
		road: 11013.5152314815,
		acceleration: 0.348333333333333
	},
	{
		id: 1042,
		time: 1041,
		velocity: 10.6788888888889,
		power: 4112.89819684841,
		road: 11024.10125,
		acceleration: 0.217037037037036
	},
	{
		id: 1043,
		time: 1042,
		velocity: 10.8286111111111,
		power: 3365.7247170792,
		road: 11034.8634722222,
		acceleration: 0.135370370370373
	},
	{
		id: 1044,
		time: 1043,
		velocity: 10.9822222222222,
		power: 3043.95384819562,
		road: 11045.7430555556,
		acceleration: 0.0993518518518499
	},
	{
		id: 1045,
		time: 1044,
		velocity: 10.9769444444444,
		power: 2234.99876789622,
		road: 11056.682037037,
		acceleration: 0.0194444444444457
	},
	{
		id: 1046,
		time: 1045,
		velocity: 10.8869444444444,
		power: 1279.41886093671,
		road: 11067.5949537037,
		acceleration: -0.0715740740740749
	},
	{
		id: 1047,
		time: 1046,
		velocity: 10.7675,
		power: 41.9148374920788,
		road: 11078.3776388889,
		acceleration: -0.18888888888889
	},
	{
		id: 1048,
		time: 1047,
		velocity: 10.4102777777778,
		power: 769.470917315298,
		road: 11089.008287037,
		acceleration: -0.115185185185185
	},
	{
		id: 1049,
		time: 1048,
		velocity: 10.5413888888889,
		power: 1518.24007534038,
		road: 11099.561712963,
		acceleration: -0.0392592592592607
	},
	{
		id: 1050,
		time: 1049,
		velocity: 10.6497222222222,
		power: 3147.5304308031,
		road: 11110.1560185185,
		acceleration: 0.12101851851852
	},
	{
		id: 1051,
		time: 1050,
		velocity: 10.7733333333333,
		power: 2893.93020220744,
		road: 11120.856712963,
		acceleration: 0.0917592592592591
	},
	{
		id: 1052,
		time: 1051,
		velocity: 10.8166666666667,
		power: 2667.50494567092,
		road: 11131.6366203704,
		acceleration: 0.0666666666666664
	},
	{
		id: 1053,
		time: 1052,
		velocity: 10.8497222222222,
		power: 2205.18228795975,
		road: 11142.46,
		acceleration: 0.0202777777777765
	},
	{
		id: 1054,
		time: 1053,
		velocity: 10.8341666666667,
		power: 4064.74647794898,
		road: 11153.3911574074,
		acceleration: 0.195277777777777
	},
	{
		id: 1055,
		time: 1054,
		velocity: 11.4025,
		power: 6602.74560615286,
		road: 11164.62875,
		acceleration: 0.417592592592596
	},
	{
		id: 1056,
		time: 1055,
		velocity: 12.1025,
		power: 10532.0210373835,
		road: 11176.439537037,
		acceleration: 0.728796296296295
	},
	{
		id: 1057,
		time: 1056,
		velocity: 13.0205555555556,
		power: 12027.0456955176,
		road: 11189.0078703704,
		acceleration: 0.786296296296294
	},
	{
		id: 1058,
		time: 1057,
		velocity: 13.7613888888889,
		power: 11708.5338906438,
		road: 11202.3164814815,
		acceleration: 0.69425925925926
	},
	{
		id: 1059,
		time: 1058,
		velocity: 14.1852777777778,
		power: 8554.71569951833,
		road: 11216.1775,
		acceleration: 0.410555555555556
	},
	{
		id: 1060,
		time: 1059,
		velocity: 14.2522222222222,
		power: 3114.29383596331,
		road: 11230.2402777778,
		acceleration: -0.00703703703703695
	},
	{
		id: 1061,
		time: 1060,
		velocity: 13.7402777777778,
		power: 1092.30692982683,
		road: 11244.2216203704,
		acceleration: -0.155833333333334
	},
	{
		id: 1062,
		time: 1061,
		velocity: 13.7177777777778,
		power: -2415.99889606779,
		road: 11257.9160648148,
		acceleration: -0.417962962962962
	},
	{
		id: 1063,
		time: 1062,
		velocity: 12.9983333333333,
		power: -1655.28541235074,
		road: 11271.2227314815,
		acceleration: -0.357592592592594
	},
	{
		id: 1064,
		time: 1063,
		velocity: 12.6675,
		power: -2776.12238951212,
		road: 11284.1272222222,
		acceleration: -0.446759259259256
	},
	{
		id: 1065,
		time: 1064,
		velocity: 12.3775,
		power: 34.4976293375819,
		road: 11296.7014814815,
		acceleration: -0.213703703703706
	},
	{
		id: 1066,
		time: 1065,
		velocity: 12.3572222222222,
		power: 1682.65621811893,
		road: 11309.1325462963,
		acceleration: -0.0726851851851862
	},
	{
		id: 1067,
		time: 1066,
		velocity: 12.4494444444444,
		power: 3640.84239265049,
		road: 11321.573287037,
		acceleration: 0.0920370370370378
	},
	{
		id: 1068,
		time: 1067,
		velocity: 12.6536111111111,
		power: 6268.57768863122,
		road: 11334.2112037037,
		acceleration: 0.302314814814814
	},
	{
		id: 1069,
		time: 1068,
		velocity: 13.2641666666667,
		power: 8106.93106665836,
		road: 11347.215462963,
		acceleration: 0.430370370370371
	},
	{
		id: 1070,
		time: 1069,
		velocity: 13.7405555555556,
		power: 10237.4931942875,
		road: 11360.7168981482,
		acceleration: 0.563981481481482
	},
	{
		id: 1071,
		time: 1070,
		velocity: 14.3455555555556,
		power: 9933.11490188047,
		road: 11374.7516203704,
		acceleration: 0.502592592592592
	},
	{
		id: 1072,
		time: 1071,
		velocity: 14.7719444444444,
		power: 9971.03039355267,
		road: 11389.2738425926,
		acceleration: 0.47240740740741
	},
	{
		id: 1073,
		time: 1072,
		velocity: 15.1577777777778,
		power: 8981.84668143827,
		road: 11404.2200462963,
		acceleration: 0.375555555555556
	},
	{
		id: 1074,
		time: 1073,
		velocity: 15.4722222222222,
		power: 8524.19999884289,
		road: 11419.5159259259,
		acceleration: 0.323796296296296
	},
	{
		id: 1075,
		time: 1074,
		velocity: 15.7433333333333,
		power: 7579.96776227031,
		road: 11435.0960648148,
		acceleration: 0.244722222222222
	},
	{
		id: 1076,
		time: 1075,
		velocity: 15.8919444444444,
		power: 6270.72653670332,
		road: 11450.8725925926,
		acceleration: 0.148055555555556
	},
	{
		id: 1077,
		time: 1076,
		velocity: 15.9163888888889,
		power: 5957.11176047714,
		road: 11466.7838888889,
		acceleration: 0.121481481481478
	},
	{
		id: 1078,
		time: 1077,
		velocity: 16.1077777777778,
		power: 5927.79015693509,
		road: 11482.8131944444,
		acceleration: 0.11453703703704
	},
	{
		id: 1079,
		time: 1078,
		velocity: 16.2355555555556,
		power: 6176.31367394944,
		road: 11498.9625462963,
		acceleration: 0.125555555555554
	},
	{
		id: 1080,
		time: 1079,
		velocity: 16.2930555555556,
		power: 3563.00860786804,
		road: 11515.1521759259,
		acceleration: -0.0449999999999982
	},
	{
		id: 1081,
		time: 1080,
		velocity: 15.9727777777778,
		power: 680.898809836271,
		road: 11531.2051388889,
		acceleration: -0.228333333333335
	},
	{
		id: 1082,
		time: 1081,
		velocity: 15.5505555555556,
		power: -1106.57022412402,
		road: 11546.9734259259,
		acceleration: -0.341018518518517
	},
	{
		id: 1083,
		time: 1082,
		velocity: 15.27,
		power: -3551.85712109825,
		road: 11562.32,
		acceleration: -0.502407407407409
	},
	{
		id: 1084,
		time: 1083,
		velocity: 14.4655555555556,
		power: -3020.767557828,
		road: 11577.1830092593,
		acceleration: -0.464722222222221
	},
	{
		id: 1085,
		time: 1084,
		velocity: 14.1563888888889,
		power: -3594.3751425869,
		road: 11591.560787037,
		acceleration: -0.505740740740743
	},
	{
		id: 1086,
		time: 1085,
		velocity: 13.7527777777778,
		power: -1469.70701826005,
		road: 11605.5119444444,
		acceleration: -0.347499999999998
	},
	{
		id: 1087,
		time: 1086,
		velocity: 13.4230555555556,
		power: 422.42787039382,
		road: 11619.1890740741,
		acceleration: -0.200555555555557
	},
	{
		id: 1088,
		time: 1087,
		velocity: 13.5547222222222,
		power: 3279.68720334037,
		road: 11632.7765740741,
		acceleration: 0.0212962962962973
	},
	{
		id: 1089,
		time: 1088,
		velocity: 13.8166666666667,
		power: 6345.60615641438,
		road: 11646.5000462963,
		acceleration: 0.250648148148146
	},
	{
		id: 1090,
		time: 1089,
		velocity: 14.175,
		power: 5076.75344567647,
		road: 11660.4214351852,
		acceleration: 0.145185185185188
	},
	{
		id: 1091,
		time: 1090,
		velocity: 13.9902777777778,
		power: 6227.56164709802,
		road: 11674.5267592593,
		acceleration: 0.222685185185187
	},
	{
		id: 1092,
		time: 1091,
		velocity: 14.4847222222222,
		power: 4485.16331661034,
		road: 11688.787037037,
		acceleration: 0.0872222222222216
	},
	{
		id: 1093,
		time: 1092,
		velocity: 14.4366666666667,
		power: 5666.19369490351,
		road: 11703.175,
		acceleration: 0.168148148148148
	},
	{
		id: 1094,
		time: 1093,
		velocity: 14.4947222222222,
		power: 3213.12112992351,
		road: 11717.6406018519,
		acceleration: -0.0128703703703721
	},
	{
		id: 1095,
		time: 1094,
		velocity: 14.4461111111111,
		power: 1311.9563268068,
		road: 11732.025462963,
		acceleration: -0.148611111111112
	},
	{
		id: 1096,
		time: 1095,
		velocity: 13.9908333333333,
		power: -418.947472914681,
		road: 11746.2001851852,
		acceleration: -0.271666666666668
	},
	{
		id: 1097,
		time: 1096,
		velocity: 13.6797222222222,
		power: -1646.91470265098,
		road: 11760.0589814815,
		acceleration: -0.360185185185182
	},
	{
		id: 1098,
		time: 1097,
		velocity: 13.3655555555556,
		power: 30.7745881157021,
		road: 11773.623287037,
		acceleration: -0.228796296296295
	},
	{
		id: 1099,
		time: 1098,
		velocity: 13.3044444444444,
		power: 1181.3090241593,
		road: 11787.0052314815,
		acceleration: -0.135925925925926
	},
	{
		id: 1100,
		time: 1099,
		velocity: 13.2719444444444,
		power: 2628.10541086977,
		road: 11800.3090740741,
		acceleration: -0.0202777777777783
	},
	{
		id: 1101,
		time: 1100,
		velocity: 13.3047222222222,
		power: 3118.04090862136,
		road: 11813.6119444444,
		acceleration: 0.0183333333333326
	},
	{
		id: 1102,
		time: 1101,
		velocity: 13.3594444444444,
		power: 3904.64339351657,
		road: 11826.9631944444,
		acceleration: 0.0784259259259255
	},
	{
		id: 1103,
		time: 1102,
		velocity: 13.5072222222222,
		power: 4483.3487278914,
		road: 11840.4135185185,
		acceleration: 0.119722222222222
	},
	{
		id: 1104,
		time: 1103,
		velocity: 13.6638888888889,
		power: 6322.81186380312,
		road: 11854.0503703704,
		acceleration: 0.253333333333334
	},
	{
		id: 1105,
		time: 1104,
		velocity: 14.1194444444444,
		power: 8487.69007083488,
		road: 11868.0134722222,
		acceleration: 0.399166666666664
	},
	{
		id: 1106,
		time: 1105,
		velocity: 14.7047222222222,
		power: 9808.01216895724,
		road: 11882.4106018519,
		acceleration: 0.468888888888889
	},
	{
		id: 1107,
		time: 1106,
		velocity: 15.0705555555556,
		power: 7009.11011030637,
		road: 11897.1656944444,
		acceleration: 0.247037037037039
	},
	{
		id: 1108,
		time: 1107,
		velocity: 14.8605555555556,
		power: 2214.07287928044,
		road: 11911.996712963,
		acceleration: -0.0951851851851853
	},
	{
		id: 1109,
		time: 1108,
		velocity: 14.4191666666667,
		power: -1216.37385856792,
		road: 11926.6126388889,
		acceleration: -0.335000000000001
	},
	{
		id: 1110,
		time: 1109,
		velocity: 14.0655555555556,
		power: -3751.10856490041,
		road: 11940.8021759259,
		acceleration: -0.517777777777779
	},
	{
		id: 1111,
		time: 1110,
		velocity: 13.3072222222222,
		power: -3368.265966358,
		road: 11954.4874074074,
		acceleration: -0.490833333333331
	},
	{
		id: 1112,
		time: 1111,
		velocity: 12.9466666666667,
		power: -3252.99886527035,
		road: 11967.6852777778,
		acceleration: -0.48388888888889
	},
	{
		id: 1113,
		time: 1112,
		velocity: 12.6138888888889,
		power: -868.642232930049,
		road: 11980.4956944444,
		acceleration: -0.291018518518518
	},
	{
		id: 1114,
		time: 1113,
		velocity: 12.4341666666667,
		power: -551.567592757041,
		road: 11993.029537037,
		acceleration: -0.26212962962963
	},
	{
		id: 1115,
		time: 1114,
		velocity: 12.1602777777778,
		power: 879.790775491819,
		road: 12005.3631018519,
		acceleration: -0.138425925925926
	},
	{
		id: 1116,
		time: 1115,
		velocity: 12.1986111111111,
		power: 2903.0313070893,
		road: 12017.6450925926,
		acceleration: 0.0352777777777789
	},
	{
		id: 1117,
		time: 1116,
		velocity: 12.54,
		power: 4581.03229387755,
		road: 12030.0315740741,
		acceleration: 0.173703703703705
	},
	{
		id: 1118,
		time: 1117,
		velocity: 12.6813888888889,
		power: 4840.82283669476,
		road: 12042.5985185185,
		acceleration: 0.18722222222222
	},
	{
		id: 1119,
		time: 1118,
		velocity: 12.7602777777778,
		power: 4317.59947387871,
		road: 12055.3274074074,
		acceleration: 0.136666666666668
	},
	{
		id: 1120,
		time: 1119,
		velocity: 12.95,
		power: 4451.22778425238,
		road: 12068.195462963,
		acceleration: 0.141666666666666
	},
	{
		id: 1121,
		time: 1120,
		velocity: 13.1063888888889,
		power: 2940.28058436791,
		road: 12081.1423611111,
		acceleration: 0.0160185185185213
	},
	{
		id: 1122,
		time: 1121,
		velocity: 12.8083333333333,
		power: 680.433267593769,
		road: 12094.014537037,
		acceleration: -0.165462962962966
	},
	{
		id: 1123,
		time: 1122,
		velocity: 12.4536111111111,
		power: -1195.3958564316,
		road: 12106.6456944444,
		acceleration: -0.316574074074076
	},
	{
		id: 1124,
		time: 1123,
		velocity: 12.1566666666667,
		power: -2456.61241590175,
		road: 12118.9074537037,
		acceleration: -0.422222222222221
	},
	{
		id: 1125,
		time: 1124,
		velocity: 11.5416666666667,
		power: -2080.1770876061,
		road: 12130.7627777778,
		acceleration: -0.390648148148149
	},
	{
		id: 1126,
		time: 1125,
		velocity: 11.2816666666667,
		power: -2106.7772805782,
		road: 12142.2256481482,
		acceleration: -0.394259259259259
	},
	{
		id: 1127,
		time: 1126,
		velocity: 10.9738888888889,
		power: -559.681472764414,
		road: 12153.3663425926,
		acceleration: -0.250092592592592
	},
	{
		id: 1128,
		time: 1127,
		velocity: 10.7913888888889,
		power: -207.319722305975,
		road: 12164.2747685185,
		acceleration: -0.214444444444442
	},
	{
		id: 1129,
		time: 1128,
		velocity: 10.6383333333333,
		power: 521.049933039686,
		road: 12175.0052777778,
		acceleration: -0.141388888888889
	},
	{
		id: 1130,
		time: 1129,
		velocity: 10.5497222222222,
		power: 2327.15644577383,
		road: 12185.6835648148,
		acceleration: 0.036944444444444
	},
	{
		id: 1131,
		time: 1130,
		velocity: 10.9022222222222,
		power: 2636.38232396349,
		road: 12196.4130555556,
		acceleration: 0.0654629629629611
	},
	{
		id: 1132,
		time: 1131,
		velocity: 10.8347222222222,
		power: 4805.20386799083,
		road: 12207.3093981482,
		acceleration: 0.268240740740742
	},
	{
		id: 1133,
		time: 1132,
		velocity: 11.3544444444444,
		power: 3584.36210815824,
		road: 12218.4107407407,
		acceleration: 0.14175925925926
	},
	{
		id: 1134,
		time: 1133,
		velocity: 11.3275,
		power: 4540.07496552274,
		road: 12229.6943518519,
		acceleration: 0.222777777777779
	},
	{
		id: 1135,
		time: 1134,
		velocity: 11.5030555555556,
		power: 2018.6601483016,
		road: 12241.082037037,
		acceleration: -0.0146296296296295
	},
	{
		id: 1136,
		time: 1135,
		velocity: 11.3105555555556,
		power: 1594.96162551184,
		road: 12252.4360185185,
		acceleration: -0.0527777777777789
	},
	{
		id: 1137,
		time: 1136,
		velocity: 11.1691666666667,
		power: -318.935721581947,
		road: 12263.649537037,
		acceleration: -0.228148148148149
	},
	{
		id: 1138,
		time: 1137,
		velocity: 10.8186111111111,
		power: -839.065197996622,
		road: 12274.61125,
		acceleration: -0.275462962962962
	},
	{
		id: 1139,
		time: 1138,
		velocity: 10.4841666666667,
		power: -1609.24972444936,
		road: 12285.2602777778,
		acceleration: -0.349907407407407
	},
	{
		id: 1140,
		time: 1139,
		velocity: 10.1194444444444,
		power: -1327.91216885853,
		road: 12295.5731018519,
		acceleration: -0.3225
	},
	{
		id: 1141,
		time: 1140,
		velocity: 9.85111111111111,
		power: -346.427126893827,
		road: 12305.6144444444,
		acceleration: -0.220462962962964
	},
	{
		id: 1142,
		time: 1141,
		velocity: 9.82277777777778,
		power: 533.550595971819,
		road: 12315.4827777778,
		acceleration: -0.125555555555556
	},
	{
		id: 1143,
		time: 1142,
		velocity: 9.74277777777778,
		power: -812.530947471707,
		road: 12325.1541666667,
		acceleration: -0.268333333333334
	},
	{
		id: 1144,
		time: 1143,
		velocity: 9.04611111111111,
		power: -2227.08420359981,
		road: 12334.4777777778,
		acceleration: -0.427222222222222
	},
	{
		id: 1145,
		time: 1144,
		velocity: 8.54111111111111,
		power: -3258.88938032742,
		road: 12343.308287037,
		acceleration: -0.558981481481482
	},
	{
		id: 1146,
		time: 1145,
		velocity: 8.06583333333333,
		power: -2826.88811643885,
		road: 12351.5970833333,
		acceleration: -0.524444444444445
	},
	{
		id: 1147,
		time: 1146,
		velocity: 7.47277777777778,
		power: -2230.96535242649,
		road: 12359.3924537037,
		acceleration: -0.462407407407407
	},
	{
		id: 1148,
		time: 1147,
		velocity: 7.15388888888889,
		power: -1741.82425408336,
		road: 12366.753287037,
		acceleration: -0.406666666666666
	},
	{
		id: 1149,
		time: 1148,
		velocity: 6.84583333333333,
		power: -1627.69203988618,
		road: 12373.710462963,
		acceleration: -0.400648148148147
	},
	{
		id: 1150,
		time: 1149,
		velocity: 6.27083333333333,
		power: -949.026567758824,
		road: 12380.315787037,
		acceleration: -0.303055555555556
	},
	{
		id: 1151,
		time: 1150,
		velocity: 6.24472222222222,
		power: -565.43467350564,
		road: 12386.6476388889,
		acceleration: -0.24388888888889
	},
	{
		id: 1152,
		time: 1151,
		velocity: 6.11416666666667,
		power: -586.529333419965,
		road: 12392.7327314815,
		acceleration: -0.24962962962963
	},
	{
		id: 1153,
		time: 1152,
		velocity: 5.52194444444444,
		power: 891.587207037737,
		road: 12398.6979166667,
		acceleration: 0.00981481481481516
	},
	{
		id: 1154,
		time: 1153,
		velocity: 6.27416666666667,
		power: 2746.98601651279,
		road: 12404.8293518519,
		acceleration: 0.322685185185184
	},
	{
		id: 1155,
		time: 1154,
		velocity: 7.08222222222222,
		power: 7045.12750325288,
		road: 12411.5931018519,
		acceleration: 0.941944444444445
	},
	{
		id: 1156,
		time: 1155,
		velocity: 8.34777777777778,
		power: 4884.60516460241,
		road: 12419.0906481482,
		acceleration: 0.525648148148148
	},
	{
		id: 1157,
		time: 1156,
		velocity: 7.85111111111111,
		power: 2679.51979780246,
		road: 12426.9490740741,
		acceleration: 0.196111111111112
	},
	{
		id: 1158,
		time: 1157,
		velocity: 7.67055555555556,
		power: -134.864937890956,
		road: 12434.8154166667,
		acceleration: -0.180277777777778
	},
	{
		id: 1159,
		time: 1158,
		velocity: 7.80694444444444,
		power: 2658.2026707922,
		road: 12442.6878703704,
		acceleration: 0.192500000000001
	},
	{
		id: 1160,
		time: 1159,
		velocity: 8.42861111111111,
		power: 3464.07049422206,
		road: 12450.7987037037,
		acceleration: 0.284259259259259
	},
	{
		id: 1161,
		time: 1160,
		velocity: 8.52333333333333,
		power: 2178.98054943894,
		road: 12459.1062962963,
		acceleration: 0.109259259259259
	},
	{
		id: 1162,
		time: 1161,
		velocity: 8.13472222222222,
		power: -588.115809505605,
		road: 12467.3481944445,
		acceleration: -0.240648148148148
	},
	{
		id: 1163,
		time: 1162,
		velocity: 7.70666666666667,
		power: -383.637751524253,
		road: 12475.3628240741,
		acceleration: -0.213888888888889
	},
	{
		id: 1164,
		time: 1163,
		velocity: 7.88166666666667,
		power: 1912.89827312214,
		road: 12483.3153703704,
		acceleration: 0.0897222222222229
	},
	{
		id: 1165,
		time: 1164,
		velocity: 8.40388888888889,
		power: 3389.61962726831,
		road: 12491.4493518519,
		acceleration: 0.273148148148148
	},
	{
		id: 1166,
		time: 1165,
		velocity: 8.52611111111111,
		power: 3010.52916739206,
		road: 12499.8252314815,
		acceleration: 0.210648148148149
	},
	{
		id: 1167,
		time: 1166,
		velocity: 8.51361111111111,
		power: 1421.13474931022,
		road: 12508.3104166667,
		acceleration: 0.00796296296296184
	},
	{
		id: 1168,
		time: 1167,
		velocity: 8.42777777777778,
		power: 1349.40855754207,
		road: 12516.7990740741,
		acceleration: -0.00101851851851897
	},
	{
		id: 1169,
		time: 1168,
		velocity: 8.52305555555555,
		power: 2195.3667166354,
		road: 12525.3380092593,
		acceleration: 0.101574074074074
	},
	{
		id: 1170,
		time: 1169,
		velocity: 8.81833333333333,
		power: 2877.05597845143,
		road: 12534.0168981482,
		acceleration: 0.178333333333333
	},
	{
		id: 1171,
		time: 1170,
		velocity: 8.96277777777778,
		power: 3612.8519081774,
		road: 12542.9122222222,
		acceleration: 0.254537037037039
	},
	{
		id: 1172,
		time: 1171,
		velocity: 9.28666666666667,
		power: 3850.67383287022,
		road: 12552.068287037,
		acceleration: 0.266944444444444
	},
	{
		id: 1173,
		time: 1172,
		velocity: 9.61916666666667,
		power: 3271.51350026596,
		road: 12561.4523148148,
		acceleration: 0.188981481481482
	},
	{
		id: 1174,
		time: 1173,
		velocity: 9.52972222222222,
		power: 1515.89650695135,
		road: 12570.9258333333,
		acceleration: -0.0100000000000033
	},
	{
		id: 1175,
		time: 1174,
		velocity: 9.25666666666667,
		power: 369.709876911762,
		road: 12580.3263888889,
		acceleration: -0.135925925925925
	},
	{
		id: 1176,
		time: 1175,
		velocity: 9.21138888888889,
		power: 1101.57849532554,
		road: 12589.6330092593,
		acceleration: -0.0519444444444446
	},
	{
		id: 1177,
		time: 1176,
		velocity: 9.37388888888889,
		power: 2198.83466548887,
		road: 12598.9493981482,
		acceleration: 0.0714814814814826
	},
	{
		id: 1178,
		time: 1177,
		velocity: 9.47111111111111,
		power: 3215.79947806811,
		road: 12608.3914814815,
		acceleration: 0.179907407407407
	},
	{
		id: 1179,
		time: 1178,
		velocity: 9.75111111111111,
		power: 3822.592997878,
		road: 12618.0414814815,
		acceleration: 0.235925925925926
	},
	{
		id: 1180,
		time: 1179,
		velocity: 10.0816666666667,
		power: 4714.35858046462,
		road: 12627.9672222222,
		acceleration: 0.315555555555555
	},
	{
		id: 1181,
		time: 1180,
		velocity: 10.4177777777778,
		power: 4455.99818813955,
		road: 12638.1863888889,
		acceleration: 0.271296296296295
	},
	{
		id: 1182,
		time: 1181,
		velocity: 10.565,
		power: 3224.13818195965,
		road: 12648.6091666667,
		acceleration: 0.135925925925926
	},
	{
		id: 1183,
		time: 1182,
		velocity: 10.4894444444444,
		power: 1715.86261193074,
		road: 12659.0911111111,
		acceleration: -0.0175925925925924
	},
	{
		id: 1184,
		time: 1183,
		velocity: 10.365,
		power: 1048.07650816771,
		road: 12669.5225925926,
		acceleration: -0.0833333333333339
	},
	{
		id: 1185,
		time: 1184,
		velocity: 10.315,
		power: 1178.73581071049,
		road: 12679.8781944444,
		acceleration: -0.0684259259259239
	},
	{
		id: 1186,
		time: 1185,
		velocity: 10.2841666666667,
		power: 1941.95045858369,
		road: 12690.2044907407,
		acceleration: 0.00981481481481161
	},
	{
		id: 1187,
		time: 1186,
		velocity: 10.3944444444444,
		power: 2253.79810623633,
		road: 12700.5560185185,
		acceleration: 0.0406481481481507
	},
	{
		id: 1188,
		time: 1187,
		velocity: 10.4369444444444,
		power: 2564.3935270655,
		road: 12710.9629166667,
		acceleration: 0.0700925925925926
	},
	{
		id: 1189,
		time: 1188,
		velocity: 10.4944444444444,
		power: 2486.49849152222,
		road: 12721.4348148148,
		acceleration: 0.0599074074074064
	},
	{
		id: 1190,
		time: 1189,
		velocity: 10.5741666666667,
		power: 3081.82030466599,
		road: 12731.9946296296,
		acceleration: 0.115925925925929
	},
	{
		id: 1191,
		time: 1190,
		velocity: 10.7847222222222,
		power: 3336.92583372015,
		road: 12742.6803240741,
		acceleration: 0.135833333333332
	},
	{
		id: 1192,
		time: 1191,
		velocity: 10.9019444444444,
		power: 3440.52651834034,
		road: 12753.5039351852,
		acceleration: 0.139999999999999
	},
	{
		id: 1193,
		time: 1192,
		velocity: 10.9941666666667,
		power: 2072.61461149965,
		road: 12764.4001388889,
		acceleration: 0.0051851851851854
	},
	{
		id: 1194,
		time: 1193,
		velocity: 10.8002777777778,
		power: 777.364648610252,
		road: 12775.2397222222,
		acceleration: -0.118425925925926
	},
	{
		id: 1195,
		time: 1194,
		velocity: 10.5466666666667,
		power: -860.548330030391,
		road: 12785.882037037,
		acceleration: -0.276111111111112
	},
	{
		id: 1196,
		time: 1195,
		velocity: 10.1658333333333,
		power: -290.729526055277,
		road: 12796.2774537037,
		acceleration: -0.217685185185184
	},
	{
		id: 1197,
		time: 1196,
		velocity: 10.1472222222222,
		power: -171.069823275025,
		road: 12806.4622685185,
		acceleration: -0.20351851851852
	},
	{
		id: 1198,
		time: 1197,
		velocity: 9.93611111111111,
		power: 1280.31194624229,
		road: 12816.5199074074,
		acceleration: -0.0508333333333333
	},
	{
		id: 1199,
		time: 1198,
		velocity: 10.0133333333333,
		power: 1815.016518565,
		road: 12826.5549537037,
		acceleration: 0.00564814814814874
	},
	{
		id: 1200,
		time: 1199,
		velocity: 10.1641666666667,
		power: 3039.16302248063,
		road: 12836.6581944444,
		acceleration: 0.130740740740741
	},
	{
		id: 1201,
		time: 1200,
		velocity: 10.3283333333333,
		power: 2934.84438555174,
		road: 12846.884212963,
		acceleration: 0.114814814814814
	},
	{
		id: 1202,
		time: 1201,
		velocity: 10.3577777777778,
		power: 2387.75684464108,
		road: 12857.195462963,
		acceleration: 0.0556481481481494
	},
	{
		id: 1203,
		time: 1202,
		velocity: 10.3311111111111,
		power: 2676.53136700398,
		road: 12867.5757407407,
		acceleration: 0.0824074074074055
	},
	{
		id: 1204,
		time: 1203,
		velocity: 10.5755555555556,
		power: 2482.01986360256,
		road: 12878.0273148148,
		acceleration: 0.0601851851851869
	},
	{
		id: 1205,
		time: 1204,
		velocity: 10.5383333333333,
		power: 3365.2934859267,
		road: 12888.5811574074,
		acceleration: 0.144351851851852
	},
	{
		id: 1206,
		time: 1205,
		velocity: 10.7641666666667,
		power: 2588.19218957167,
		road: 12899.2388425926,
		acceleration: 0.0633333333333344
	},
	{
		id: 1207,
		time: 1206,
		velocity: 10.7655555555556,
		power: 2617.2562247803,
		road: 12909.9601388889,
		acceleration: 0.0638888888888882
	},
	{
		id: 1208,
		time: 1207,
		velocity: 10.73,
		power: 2761.15795506089,
		road: 12920.7510648148,
		acceleration: 0.0753703703703685
	},
	{
		id: 1209,
		time: 1208,
		velocity: 10.9902777777778,
		power: 3209.01721676742,
		road: 12931.6371759259,
		acceleration: 0.115
	},
	{
		id: 1210,
		time: 1209,
		velocity: 11.1105555555556,
		power: 3472.26783110542,
		road: 12942.648287037,
		acceleration: 0.135
	},
	{
		id: 1211,
		time: 1210,
		velocity: 11.135,
		power: 2124.32256647551,
		road: 12953.7291203704,
		acceleration: 0.00444444444444514
	},
	{
		id: 1212,
		time: 1211,
		velocity: 11.0036111111111,
		power: 1269.39597379678,
		road: 12964.7743518519,
		acceleration: -0.075648148148149
	},
	{
		id: 1213,
		time: 1212,
		velocity: 10.8836111111111,
		power: 195.788046372875,
		road: 12975.6938425926,
		acceleration: -0.175833333333332
	},
	{
		id: 1214,
		time: 1213,
		velocity: 10.6075,
		power: -491.835688461398,
		road: 12986.4052777778,
		acceleration: -0.240277777777781
	},
	{
		id: 1215,
		time: 1214,
		velocity: 10.2827777777778,
		power: -670.608158146103,
		road: 12996.8683796296,
		acceleration: -0.256388888888887
	},
	{
		id: 1216,
		time: 1215,
		velocity: 10.1144444444444,
		power: -307.893861518406,
		road: 13007.0943055556,
		acceleration: -0.217962962962963
	},
	{
		id: 1217,
		time: 1216,
		velocity: 9.95361111111111,
		power: 1524.7947761617,
		road: 13017.197962963,
		acceleration: -0.0265740740740732
	},
	{
		id: 1218,
		time: 1217,
		velocity: 10.2030555555556,
		power: 2214.18287692354,
		road: 13027.3106944444,
		acceleration: 0.0447222222222212
	},
	{
		id: 1219,
		time: 1218,
		velocity: 10.2486111111111,
		power: 3417.34580659515,
		road: 13037.5281481482,
		acceleration: 0.164722222222224
	},
	{
		id: 1220,
		time: 1219,
		velocity: 10.4477777777778,
		power: 2519.65872923959,
		road: 13047.8620833333,
		acceleration: 0.068240740740741
	},
	{
		id: 1221,
		time: 1220,
		velocity: 10.4077777777778,
		power: 2126.39939840365,
		road: 13058.2435185185,
		acceleration: 0.0267592592592578
	},
	{
		id: 1222,
		time: 1221,
		velocity: 10.3288888888889,
		power: 810.609700017652,
		road: 13068.5856018519,
		acceleration: -0.105462962962962
	},
	{
		id: 1223,
		time: 1222,
		velocity: 10.1313888888889,
		power: 1014.94916086948,
		road: 13078.8336111111,
		acceleration: -0.0826851851851842
	},
	{
		id: 1224,
		time: 1223,
		velocity: 10.1597222222222,
		power: 2098.70124255477,
		road: 13089.0548611111,
		acceleration: 0.029166666666665
	},
	{
		id: 1225,
		time: 1224,
		velocity: 10.4163888888889,
		power: 2840.52890037808,
		road: 13099.342037037,
		acceleration: 0.102685185185186
	},
	{
		id: 1226,
		time: 1225,
		velocity: 10.4394444444444,
		power: 3287.549403268,
		road: 13109.7519907407,
		acceleration: 0.142870370370369
	},
	{
		id: 1227,
		time: 1226,
		velocity: 10.5883333333333,
		power: 3134.13243009977,
		road: 13120.2943055556,
		acceleration: 0.121851851851851
	},
	{
		id: 1228,
		time: 1227,
		velocity: 10.7819444444444,
		power: 3611.84572138209,
		road: 13130.9789814815,
		acceleration: 0.162870370370371
	},
	{
		id: 1229,
		time: 1228,
		velocity: 10.9280555555556,
		power: 2534.17687461425,
		road: 13141.771712963,
		acceleration: 0.0532407407407405
	},
	{
		id: 1230,
		time: 1229,
		velocity: 10.7480555555556,
		power: 781.833622554363,
		road: 13152.5328240741,
		acceleration: -0.116481481481479
	},
	{
		id: 1231,
		time: 1230,
		velocity: 10.4325,
		power: -483.6842684124,
		road: 13163.1164351852,
		acceleration: -0.23851851851852
	},
	{
		id: 1232,
		time: 1231,
		velocity: 10.2125,
		power: -377.699018687855,
		road: 13173.4677314815,
		acceleration: -0.226111111111111
	},
	{
		id: 1233,
		time: 1232,
		velocity: 10.0697222222222,
		power: 96.144067185476,
		road: 13183.6181944444,
		acceleration: -0.175555555555555
	},
	{
		id: 1234,
		time: 1233,
		velocity: 9.90583333333333,
		power: 1757.1836540086,
		road: 13193.6802777778,
		acceleration: -0.00120370370370537
	},
	{
		id: 1235,
		time: 1234,
		velocity: 10.2088888888889,
		power: 528.773486506973,
		road: 13203.6776388889,
		acceleration: -0.12824074074074
	},
	{
		id: 1236,
		time: 1235,
		velocity: 9.685,
		power: 759.826883143206,
		road: 13213.56,
		acceleration: -0.101759259259261
	},
	{
		id: 1237,
		time: 1236,
		velocity: 9.60055555555556,
		power: -488.767233783519,
		road: 13223.2747685185,
		acceleration: -0.233425925925925
	},
	{
		id: 1238,
		time: 1237,
		velocity: 9.50861111111111,
		power: 485.69870783971,
		road: 13232.8102314815,
		acceleration: -0.125185185185185
	},
	{
		id: 1239,
		time: 1238,
		velocity: 9.30944444444444,
		power: 513.312046533718,
		road: 13242.2230555556,
		acceleration: -0.120092592592593
	},
	{
		id: 1240,
		time: 1239,
		velocity: 9.24027777777778,
		power: 760.32657925056,
		road: 13251.5306018519,
		acceleration: -0.0904629629629632
	},
	{
		id: 1241,
		time: 1240,
		velocity: 9.23722222222222,
		power: 952.508927869508,
		road: 13260.7593981482,
		acceleration: -0.0670370370370375
	},
	{
		id: 1242,
		time: 1241,
		velocity: 9.10833333333333,
		power: 905.181553477806,
		road: 13269.919212963,
		acceleration: -0.0709259259259269
	},
	{
		id: 1243,
		time: 1242,
		velocity: 9.0275,
		power: 712.176420756339,
		road: 13278.9978240741,
		acceleration: -0.0914814814814804
	},
	{
		id: 1244,
		time: 1243,
		velocity: 8.96277777777778,
		power: -7.78830814902235,
		road: 13287.9439814815,
		acceleration: -0.173425925925926
	},
	{
		id: 1245,
		time: 1244,
		velocity: 8.58805555555556,
		power: 465.947693395199,
		road: 13296.7456944445,
		acceleration: -0.115462962962962
	},
	{
		id: 1246,
		time: 1245,
		velocity: 8.68111111111111,
		power: 1784.77318287441,
		road: 13305.51125,
		acceleration: 0.0431481481481484
	},
	{
		id: 1247,
		time: 1246,
		velocity: 9.09222222222222,
		power: 3050.05099174104,
		road: 13314.3927777778,
		acceleration: 0.188796296296296
	},
	{
		id: 1248,
		time: 1247,
		velocity: 9.15444444444445,
		power: 3041.60257529992,
		road: 13323.4580092593,
		acceleration: 0.17861111111111
	},
	{
		id: 1249,
		time: 1248,
		velocity: 9.21694444444444,
		power: 1847.55804821016,
		road: 13332.6308796296,
		acceleration: 0.0366666666666688
	},
	{
		id: 1250,
		time: 1249,
		velocity: 9.20222222222222,
		power: 1559.94554620661,
		road: 13341.8236574074,
		acceleration: 0.00314814814814568
	},
	{
		id: 1251,
		time: 1250,
		velocity: 9.16388888888889,
		power: 1188.79136558336,
		road: 13350.9986111111,
		acceleration: -0.0387962962962956
	},
	{
		id: 1252,
		time: 1251,
		velocity: 9.10055555555556,
		power: 851.353575153462,
		road: 13360.1160648148,
		acceleration: -0.0762037037037029
	},
	{
		id: 1253,
		time: 1252,
		velocity: 8.97361111111111,
		power: 482.225276128269,
		road: 13369.1368518519,
		acceleration: -0.11712962962963
	},
	{
		id: 1254,
		time: 1253,
		velocity: 8.8125,
		power: 274.615255063471,
		road: 13378.0293055556,
		acceleration: -0.139537037037037
	},
	{
		id: 1255,
		time: 1254,
		velocity: 8.68194444444444,
		power: 1082.84291618489,
		road: 13386.8310648148,
		acceleration: -0.0418518518518507
	},
	{
		id: 1256,
		time: 1255,
		velocity: 8.84805555555556,
		power: 2411.23721521215,
		road: 13395.6694444445,
		acceleration: 0.115092592592593
	},
	{
		id: 1257,
		time: 1256,
		velocity: 9.15777777777778,
		power: 5113.20892093484,
		road: 13404.7731944445,
		acceleration: 0.415648148148147
	},
	{
		id: 1258,
		time: 1257,
		velocity: 9.92888888888889,
		power: 10635.2057603548,
		road: 13414.5640740741,
		acceleration: 0.958611111111113
	},
	{
		id: 1259,
		time: 1258,
		velocity: 11.7238888888889,
		power: 10830.9515467216,
		road: 13425.2689351852,
		acceleration: 0.869351851851851
	},
	{
		id: 1260,
		time: 1259,
		velocity: 11.7658333333333,
		power: 10848.9760759541,
		road: 13436.8005092593,
		acceleration: 0.784074074074075
	},
	{
		id: 1261,
		time: 1260,
		velocity: 12.2811111111111,
		power: 5986.85941509196,
		road: 13448.8790740741,
		acceleration: 0.309907407407405
	},
	{
		id: 1262,
		time: 1261,
		velocity: 12.6536111111111,
		power: 9347.46000984449,
		road: 13461.3960185185,
		acceleration: 0.566851851851851
	},
	{
		id: 1263,
		time: 1262,
		velocity: 13.4663888888889,
		power: 8980.70214999492,
		road: 13474.4451851852,
		acceleration: 0.497592592592595
	},
	{
		id: 1264,
		time: 1263,
		velocity: 13.7738888888889,
		power: 5284.93726147866,
		road: 13487.8356018519,
		acceleration: 0.184907407407406
	},
	{
		id: 1265,
		time: 1264,
		velocity: 13.2083333333333,
		power: -4991.50019886899,
		road: 13501.0073148148,
		acceleration: -0.622314814814814
	},
	{
		id: 1266,
		time: 1265,
		velocity: 11.5994444444444,
		power: -13288.6448426723,
		road: 13513.1905092593,
		acceleration: -1.35472222222222
	},
	{
		id: 1267,
		time: 1266,
		velocity: 9.70972222222222,
		power: -14759.1059575779,
		road: 13523.8756944444,
		acceleration: -1.6412962962963
	},
	{
		id: 1268,
		time: 1267,
		velocity: 8.28444444444444,
		power: -12381.863139757,
		road: 13532.9356481482,
		acceleration: -1.60916666666667
	},
	{
		id: 1269,
		time: 1268,
		velocity: 6.77194444444444,
		power: -9138.53986789552,
		road: 13540.4741203704,
		acceleration: -1.4337962962963
	},
	{
		id: 1270,
		time: 1269,
		velocity: 5.40833333333333,
		power: -7764.90239177712,
		road: 13546.549212963,
		acceleration: -1.49296296296296
	},
	{
		id: 1271,
		time: 1270,
		velocity: 3.80555555555556,
		power: -5029.01009791868,
		road: 13551.2439814815,
		acceleration: -1.26768518518518
	},
	{
		id: 1272,
		time: 1271,
		velocity: 2.96888888888889,
		power: -3587.91463832845,
		road: 13554.689212963,
		acceleration: -1.23138888888889
	},
	{
		id: 1273,
		time: 1272,
		velocity: 1.71416666666667,
		power: -2370.05182332384,
		road: 13556.8844907407,
		acceleration: -1.26851851851852
	},
	{
		id: 1274,
		time: 1273,
		velocity: 0,
		power: -870.607275581035,
		road: 13557.9506944444,
		acceleration: -0.98962962962963
	},
	{
		id: 1275,
		time: 1274,
		velocity: 0,
		power: -120.133912426901,
		road: 13558.2363888889,
		acceleration: -0.571388888888889
	},
	{
		id: 1276,
		time: 1275,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1277,
		time: 1276,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1278,
		time: 1277,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1279,
		time: 1278,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1280,
		time: 1279,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1281,
		time: 1280,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1282,
		time: 1281,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1283,
		time: 1282,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1284,
		time: 1283,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1285,
		time: 1284,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1286,
		time: 1285,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1287,
		time: 1286,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1288,
		time: 1287,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1289,
		time: 1288,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1290,
		time: 1289,
		velocity: 0,
		power: 0,
		road: 13558.2363888889,
		acceleration: 0
	},
	{
		id: 1291,
		time: 1290,
		velocity: 0,
		power: 1.56880344520821,
		road: 13558.2474537037,
		acceleration: 0.0221296296296296
	},
	{
		id: 1292,
		time: 1291,
		velocity: 0.0663888888888889,
		power: 2.67366114642681,
		road: 13558.2695833333,
		acceleration: 0
	},
	{
		id: 1293,
		time: 1292,
		velocity: 0,
		power: 2.67366114642681,
		road: 13558.291712963,
		acceleration: 0
	},
	{
		id: 1294,
		time: 1293,
		velocity: 0,
		power: 1.10485475958415,
		road: 13558.3027777778,
		acceleration: -0.0221296296296296
	},
	{
		id: 1295,
		time: 1294,
		velocity: 0,
		power: 0,
		road: 13558.3027777778,
		acceleration: 0
	},
	{
		id: 1296,
		time: 1295,
		velocity: 0,
		power: 0,
		road: 13558.3027777778,
		acceleration: 0
	},
	{
		id: 1297,
		time: 1296,
		velocity: 0,
		power: 0,
		road: 13558.3027777778,
		acceleration: 0
	},
	{
		id: 1298,
		time: 1297,
		velocity: 0,
		power: 0,
		road: 13558.3027777778,
		acceleration: 0
	},
	{
		id: 1299,
		time: 1298,
		velocity: 0,
		power: 0,
		road: 13558.3027777778,
		acceleration: 0
	},
	{
		id: 1300,
		time: 1299,
		velocity: 0,
		power: 0,
		road: 13558.3027777778,
		acceleration: 0
	},
	{
		id: 1301,
		time: 1300,
		velocity: 0,
		power: 160.869700816959,
		road: 13558.5639814815,
		acceleration: 0.522407407407407
	},
	{
		id: 1302,
		time: 1301,
		velocity: 1.56722222222222,
		power: 1351.59356238251,
		road: 13559.667962963,
		acceleration: 1.16314814814815
	},
	{
		id: 1303,
		time: 1302,
		velocity: 3.48944444444444,
		power: 4584.46536112351,
		road: 13562.2305555556,
		acceleration: 1.75407407407407
	},
	{
		id: 1304,
		time: 1303,
		velocity: 5.26222222222222,
		power: 5669.86787556456,
		road: 13566.3299074074,
		acceleration: 1.31944444444445
	},
	{
		id: 1305,
		time: 1304,
		velocity: 5.52555555555556,
		power: 5377.53772516628,
		road: 13571.5587037037,
		acceleration: 0.939444444444444
	},
	{
		id: 1306,
		time: 1305,
		velocity: 6.30777777777778,
		power: 7732.94724736902,
		road: 13577.8307407407,
		acceleration: 1.14703703703704
	},
	{
		id: 1307,
		time: 1306,
		velocity: 8.70333333333333,
		power: 12795.2367605542,
		road: 13585.475462963,
		acceleration: 1.59833333333333
	},
	{
		id: 1308,
		time: 1307,
		velocity: 10.3205555555556,
		power: 14199.9044884478,
		road: 13594.6448611111,
		acceleration: 1.45101851851852
	},
	{
		id: 1309,
		time: 1308,
		velocity: 10.6608333333333,
		power: 7696.47511644721,
		road: 13604.8427314815,
		acceleration: 0.605925925925925
	},
	{
		id: 1310,
		time: 1309,
		velocity: 10.5211111111111,
		power: 3113.73757230727,
		road: 13615.4031018519,
		acceleration: 0.119074074074074
	},
	{
		id: 1311,
		time: 1310,
		velocity: 10.6777777777778,
		power: 4023.10075631611,
		road: 13626.12375,
		acceleration: 0.201481481481482
	},
	{
		id: 1312,
		time: 1311,
		velocity: 11.2652777777778,
		power: 6341.97622339773,
		road: 13637.1488425926,
		acceleration: 0.407407407407407
	},
	{
		id: 1313,
		time: 1312,
		velocity: 11.7433333333333,
		power: 6112.60247694152,
		road: 13648.5581481481,
		acceleration: 0.36101851851852
	},
	{
		id: 1314,
		time: 1313,
		velocity: 11.7608333333333,
		power: 4704.00548765118,
		road: 13660.2564814815,
		acceleration: 0.217037037037036
	},
	{
		id: 1315,
		time: 1314,
		velocity: 11.9163888888889,
		power: 2942.94016414484,
		road: 13672.0905092593,
		acceleration: 0.0543518518518518
	},
	{
		id: 1316,
		time: 1315,
		velocity: 11.9063888888889,
		power: 2880.40152148498,
		road: 13683.9752314815,
		acceleration: 0.0470370370370397
	},
	{
		id: 1317,
		time: 1316,
		velocity: 11.9019444444444,
		power: 2326.6207398249,
		road: 13695.8822222222,
		acceleration: -0.00250000000000128
	},
	{
		id: 1318,
		time: 1317,
		velocity: 11.9088888888889,
		power: 1835.56013585112,
		road: 13707.7654166667,
		acceleration: -0.0450925925925922
	},
	{
		id: 1319,
		time: 1318,
		velocity: 11.7711111111111,
		power: 1184.30179531705,
		road: 13719.5756018518,
		acceleration: -0.100925925925926
	},
	{
		id: 1320,
		time: 1319,
		velocity: 11.5991666666667,
		power: -91.4318884816419,
		road: 13731.229212963,
		acceleration: -0.212222222222223
	},
	{
		id: 1321,
		time: 1320,
		velocity: 11.2722222222222,
		power: 32.5914284138533,
		road: 13742.6775462963,
		acceleration: -0.198333333333332
	},
	{
		id: 1322,
		time: 1321,
		velocity: 11.1761111111111,
		power: -1267.72854541959,
		road: 13753.868287037,
		acceleration: -0.316851851851851
	},
	{
		id: 1323,
		time: 1322,
		velocity: 10.6486111111111,
		power: -101.731140312256,
		road: 13764.7983333333,
		acceleration: -0.204537037037039
	},
	{
		id: 1324,
		time: 1323,
		velocity: 10.6586111111111,
		power: -326.007572097929,
		road: 13775.5140740741,
		acceleration: -0.224074074074075
	},
	{
		id: 1325,
		time: 1324,
		velocity: 10.5038888888889,
		power: 1000.54080851455,
		road: 13786.0723611111,
		acceleration: -0.0908333333333307
	},
	{
		id: 1326,
		time: 1325,
		velocity: 10.3761111111111,
		power: 2544.09880671488,
		road: 13796.6167592593,
		acceleration: 0.0630555555555521
	},
	{
		id: 1327,
		time: 1326,
		velocity: 10.8477777777778,
		power: 3376.25440047074,
		road: 13807.2633796296,
		acceleration: 0.141388888888889
	},
	{
		id: 1328,
		time: 1327,
		velocity: 10.9280555555556,
		power: 5039.70950862888,
		road: 13818.127037037,
		acceleration: 0.292685185185187
	},
	{
		id: 1329,
		time: 1328,
		velocity: 11.2541666666667,
		power: 5632.06380626936,
		road: 13829.3024074074,
		acceleration: 0.330740740740742
	},
	{
		id: 1330,
		time: 1329,
		velocity: 11.84,
		power: 5758.42641540713,
		road: 13840.8046296296,
		acceleration: 0.322962962962961
	},
	{
		id: 1331,
		time: 1330,
		velocity: 11.8969444444444,
		power: 6084.210832375,
		road: 13852.6347685185,
		acceleration: 0.332870370370372
	},
	{
		id: 1332,
		time: 1331,
		velocity: 12.2527777777778,
		power: 5872.64534635093,
		road: 13864.7794907407,
		acceleration: 0.296296296296292
	},
	{
		id: 1333,
		time: 1332,
		velocity: 12.7288888888889,
		power: 6781.3115439668,
		road: 13877.2497685185,
		acceleration: 0.354814814814816
	},
	{
		id: 1334,
		time: 1333,
		velocity: 12.9613888888889,
		power: 8468.98139304368,
		road: 13890.1313888889,
		acceleration: 0.46787037037037
	},
	{
		id: 1335,
		time: 1334,
		velocity: 13.6563888888889,
		power: 11857.6457344011,
		road: 13903.5934259259,
		acceleration: 0.692962962962964
	},
	{
		id: 1336,
		time: 1335,
		velocity: 14.8077777777778,
		power: 12655.2750305801,
		road: 13917.7497222222,
		acceleration: 0.695555555555556
	},
	{
		id: 1337,
		time: 1336,
		velocity: 15.0480555555556,
		power: 8788.18443970975,
		road: 13932.4422685185,
		acceleration: 0.376944444444444
	},
	{
		id: 1338,
		time: 1337,
		velocity: 14.7872222222222,
		power: 1090.06614991716,
		road: 13947.2364351852,
		acceleration: -0.173703703703703
	},
	{
		id: 1339,
		time: 1338,
		velocity: 14.2866666666667,
		power: -1603.12289664504,
		road: 13961.7627777778,
		acceleration: -0.361944444444445
	},
	{
		id: 1340,
		time: 1339,
		velocity: 13.9622222222222,
		power: -2562.42811193257,
		road: 13975.8931944444,
		acceleration: -0.429907407407406
	},
	{
		id: 1341,
		time: 1340,
		velocity: 13.4975,
		power: -2079.44932250445,
		road: 13989.6125,
		acceleration: -0.392314814814817
	},
	{
		id: 1342,
		time: 1341,
		velocity: 13.1097222222222,
		power: -2405.51737137366,
		road: 14002.9273148148,
		acceleration: -0.416666666666664
	},
	{
		id: 1343,
		time: 1342,
		velocity: 12.7122222222222,
		power: -1615.14770712656,
		road: 14015.8575,
		acceleration: -0.352592592592593
	},
	{
		id: 1344,
		time: 1343,
		velocity: 12.4397222222222,
		power: -2460.72502999042,
		road: 14028.4005092593,
		acceleration: -0.421759259259261
	},
	{
		id: 1345,
		time: 1344,
		velocity: 11.8444444444444,
		power: -5597.38526179481,
		road: 14040.3835648148,
		acceleration: -0.698148148148146
	},
	{
		id: 1346,
		time: 1345,
		velocity: 10.6177777777778,
		power: -7490.8204001581,
		road: 14051.5672685185,
		acceleration: -0.900555555555556
	},
	{
		id: 1347,
		time: 1346,
		velocity: 9.73805555555556,
		power: -10165.6194164701,
		road: 14061.6806018519,
		acceleration: -1.24018518518519
	},
	{
		id: 1348,
		time: 1347,
		velocity: 8.12388888888889,
		power: -10307.8382935751,
		road: 14070.4725462963,
		acceleration: -1.40259259259259
	},
	{
		id: 1349,
		time: 1348,
		velocity: 6.41,
		power: -12088.1688185234,
		road: 14077.5925462963,
		acceleration: -1.9412962962963
	},
	{
		id: 1350,
		time: 1349,
		velocity: 3.91416666666667,
		power: -9522.00344148003,
		road: 14082.6869907407,
		acceleration: -2.10981481481481
	},
	{
		id: 1351,
		time: 1350,
		velocity: 1.79444444444444,
		power: -4882.83472265538,
		road: 14085.8453240741,
		acceleration: -1.76240740740741
	},
	{
		id: 1352,
		time: 1351,
		velocity: 1.12277777777778,
		power: -766.094456373476,
		road: 14087.8568055556,
		acceleration: -0.531296296296296
	},
	{
		id: 1353,
		time: 1352,
		velocity: 2.32027777777778,
		power: 1259.0719546622,
		road: 14089.8678240741,
		acceleration: 0.53037037037037
	},
	{
		id: 1354,
		time: 1353,
		velocity: 3.38555555555556,
		power: 2171.64827689336,
		road: 14092.51125,
		acceleration: 0.734444444444444
	},
	{
		id: 1355,
		time: 1354,
		velocity: 3.32611111111111,
		power: 1006.5883150975,
		road: 14095.6257407407,
		acceleration: 0.207685185185186
	},
	{
		id: 1356,
		time: 1355,
		velocity: 2.94333333333333,
		power: 94.0331667803406,
		road: 14098.7931944444,
		acceleration: -0.101759259259259
	},
	{
		id: 1357,
		time: 1356,
		velocity: 3.08027777777778,
		power: 48.034759926384,
		road: 14101.851712963,
		acceleration: -0.116111111111111
	},
	{
		id: 1358,
		time: 1357,
		velocity: 2.97777777777778,
		power: 340.808791656761,
		road: 14104.8459259259,
		acceleration: -0.0125000000000006
	},
	{
		id: 1359,
		time: 1358,
		velocity: 2.90583333333333,
		power: -127.601073602876,
		road: 14107.7446296296,
		acceleration: -0.178518518518518
	},
	{
		id: 1360,
		time: 1359,
		velocity: 2.54472222222222,
		power: -336.006900196836,
		road: 14110.4222222222,
		acceleration: -0.263703703703704
	},
	{
		id: 1361,
		time: 1360,
		velocity: 2.18666666666667,
		power: -836.679976119206,
		road: 14112.71,
		acceleration: -0.515925925925926
	},
	{
		id: 1362,
		time: 1361,
		velocity: 1.35805555555556,
		power: -826.144896636594,
		road: 14114.4206018519,
		acceleration: -0.638425925925926
	},
	{
		id: 1363,
		time: 1362,
		velocity: 0.629444444444444,
		power: -584.81420865325,
		road: 14115.4475462963,
		acceleration: -0.728888888888889
	},
	{
		id: 1364,
		time: 1363,
		velocity: 0,
		power: -81.2910394168066,
		road: 14115.9630092593,
		acceleration: -0.294074074074074
	},
	{
		id: 1365,
		time: 1364,
		velocity: 0.475833333333333,
		power: 305.036852048219,
		road: 14116.5443981482,
		acceleration: 0.425925925925926
	},
	{
		id: 1366,
		time: 1365,
		velocity: 1.90722222222222,
		power: 1001.3571775233,
		road: 14117.7227777778,
		acceleration: 0.768055555555556
	},
	{
		id: 1367,
		time: 1366,
		velocity: 2.30416666666667,
		power: 1498.46032526965,
		road: 14119.6337962963,
		acceleration: 0.697222222222222
	},
	{
		id: 1368,
		time: 1367,
		velocity: 2.5675,
		power: 905.362059059814,
		road: 14122.0274537037,
		acceleration: 0.268055555555556
	},
	{
		id: 1369,
		time: 1368,
		velocity: 2.71138888888889,
		power: 697.333744383979,
		road: 14124.6306944444,
		acceleration: 0.151111111111111
	},
	{
		id: 1370,
		time: 1369,
		velocity: 2.7575,
		power: 451.178035142573,
		road: 14127.3317592593,
		acceleration: 0.044537037037037
	},
	{
		id: 1371,
		time: 1370,
		velocity: 2.70111111111111,
		power: 46.4331038526465,
		road: 14129.9985648148,
		acceleration: -0.113055555555555
	},
	{
		id: 1372,
		time: 1371,
		velocity: 2.37222222222222,
		power: -471.4109797883,
		road: 14132.4417592593,
		acceleration: -0.334166666666667
	},
	{
		id: 1373,
		time: 1372,
		velocity: 1.755,
		power: -791.689276199551,
		road: 14134.4446296296,
		acceleration: -0.546481481481482
	},
	{
		id: 1374,
		time: 1373,
		velocity: 1.06166666666667,
		power: -837.683140988059,
		road: 14135.7788888889,
		acceleration: -0.790740740740741
	},
	{
		id: 1375,
		time: 1374,
		velocity: 0,
		power: -280.096235108509,
		road: 14136.4252777778,
		acceleration: -0.585
	},
	{
		id: 1376,
		time: 1375,
		velocity: 0,
		power: -37.9448979532164,
		road: 14136.6022222222,
		acceleration: -0.353888888888889
	},
	{
		id: 1377,
		time: 1376,
		velocity: 0,
		power: 0,
		road: 14136.6022222222,
		acceleration: 0
	},
	{
		id: 1378,
		time: 1377,
		velocity: 0,
		power: 0,
		road: 14136.6022222222,
		acceleration: 0
	},
	{
		id: 1379,
		time: 1378,
		velocity: 0,
		power: 0,
		road: 14136.6022222222,
		acceleration: 0
	},
	{
		id: 1380,
		time: 1379,
		velocity: 0,
		power: 0,
		road: 14136.6022222222,
		acceleration: 0
	},
	{
		id: 1381,
		time: 1380,
		velocity: 0,
		power: 0,
		road: 14136.6022222222,
		acceleration: 0
	},
	{
		id: 1382,
		time: 1381,
		velocity: 0,
		power: 7.83922706331592,
		road: 14136.6421296296,
		acceleration: 0.0798148148148148
	},
	{
		id: 1383,
		time: 1382,
		velocity: 0.239444444444444,
		power: 9.64333391870462,
		road: 14136.7219444444,
		acceleration: 0
	},
	{
		id: 1384,
		time: 1383,
		velocity: 0,
		power: 9.64333391870462,
		road: 14136.8017592593,
		acceleration: 0
	},
	{
		id: 1385,
		time: 1384,
		velocity: 0,
		power: 1.80396884340481,
		road: 14136.8416666667,
		acceleration: -0.0798148148148148
	},
	{
		id: 1386,
		time: 1385,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1387,
		time: 1386,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1388,
		time: 1387,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1389,
		time: 1388,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1390,
		time: 1389,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1391,
		time: 1390,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1392,
		time: 1391,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1393,
		time: 1392,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1394,
		time: 1393,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1395,
		time: 1394,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1396,
		time: 1395,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1397,
		time: 1396,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1398,
		time: 1397,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1399,
		time: 1398,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1400,
		time: 1399,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1401,
		time: 1400,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1402,
		time: 1401,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1403,
		time: 1402,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1404,
		time: 1403,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1405,
		time: 1404,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1406,
		time: 1405,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1407,
		time: 1406,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1408,
		time: 1407,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1409,
		time: 1408,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1410,
		time: 1409,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1411,
		time: 1410,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1412,
		time: 1411,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1413,
		time: 1412,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1414,
		time: 1413,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1415,
		time: 1414,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1416,
		time: 1415,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1417,
		time: 1416,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1418,
		time: 1417,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1419,
		time: 1418,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1420,
		time: 1419,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1421,
		time: 1420,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1422,
		time: 1421,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1423,
		time: 1422,
		velocity: 0,
		power: 0,
		road: 14136.8416666667,
		acceleration: 0
	},
	{
		id: 1424,
		time: 1423,
		velocity: 0,
		power: 279.061733215848,
		road: 14137.1948148148,
		acceleration: 0.706296296296296
	},
	{
		id: 1425,
		time: 1424,
		velocity: 2.11888888888889,
		power: 2107.10868648342,
		road: 14138.6176388889,
		acceleration: 1.43305555555556
	},
	{
		id: 1426,
		time: 1425,
		velocity: 4.29916666666667,
		power: 5908.02157119912,
		road: 14141.7001388889,
		acceleration: 1.8862962962963
	},
	{
		id: 1427,
		time: 1426,
		velocity: 5.65888888888889,
		power: 5314.08930797927,
		road: 14146.2684722222,
		acceleration: 1.08537037037037
	},
	{
		id: 1428,
		time: 1427,
		velocity: 5.375,
		power: 2443.77393030237,
		road: 14151.5513425926,
		acceleration: 0.343703703703704
	},
	{
		id: 1429,
		time: 1428,
		velocity: 5.33027777777778,
		power: 911.997084266813,
		road: 14157.0216666667,
		acceleration: 0.0312037037037038
	},
	{
		id: 1430,
		time: 1429,
		velocity: 5.7525,
		power: 2471.63104584528,
		road: 14162.6653240741,
		acceleration: 0.315462962962962
	},
	{
		id: 1431,
		time: 1430,
		velocity: 6.32138888888889,
		power: 2429.99964559742,
		road: 14168.6081481481,
		acceleration: 0.282870370370372
	},
	{
		id: 1432,
		time: 1431,
		velocity: 6.17888888888889,
		power: 1306.16696273128,
		road: 14174.7303703704,
		acceleration: 0.0759259259259251
	},
	{
		id: 1433,
		time: 1432,
		velocity: 5.98027777777778,
		power: 508.387316167288,
		road: 14180.8599074074,
		acceleration: -0.0612962962962955
	},
	{
		id: 1434,
		time: 1433,
		velocity: 6.1375,
		power: 226.622441963328,
		road: 14186.904537037,
		acceleration: -0.108518518518521
	},
	{
		id: 1435,
		time: 1434,
		velocity: 5.85333333333333,
		power: 277.357362745987,
		road: 14192.8458333333,
		acceleration: -0.0981481481481463
	},
	{
		id: 1436,
		time: 1435,
		velocity: 5.68583333333333,
		power: -1269.81192793096,
		road: 14198.5480555556,
		acceleration: -0.380000000000001
	},
	{
		id: 1437,
		time: 1436,
		velocity: 4.9975,
		power: -716.738976840981,
		road: 14203.9182407407,
		acceleration: -0.284074074074073
	},
	{
		id: 1438,
		time: 1437,
		velocity: 5.00111111111111,
		power: -1182.79969746263,
		road: 14208.9518981482,
		acceleration: -0.388981481481482
	},
	{
		id: 1439,
		time: 1438,
		velocity: 4.51888888888889,
		power: -641.659888732891,
		road: 14213.6492592593,
		acceleration: -0.28361111111111
	},
	{
		id: 1440,
		time: 1439,
		velocity: 4.14666666666667,
		power: -1167.04104223219,
		road: 14217.9943981482,
		acceleration: -0.420833333333334
	},
	{
		id: 1441,
		time: 1440,
		velocity: 3.73861111111111,
		power: -849.515368911602,
		road: 14221.9478703704,
		acceleration: -0.3625
	},
	{
		id: 1442,
		time: 1441,
		velocity: 3.43138888888889,
		power: -300.198298754714,
		road: 14225.6094444444,
		acceleration: -0.221296296296296
	},
	{
		id: 1443,
		time: 1442,
		velocity: 3.48277777777778,
		power: -358.228846639805,
		road: 14229.0383333333,
		acceleration: -0.244074074074074
	},
	{
		id: 1444,
		time: 1443,
		velocity: 3.00638888888889,
		power: -362.099979578728,
		road: 14232.2186574074,
		acceleration: -0.253055555555555
	},
	{
		id: 1445,
		time: 1444,
		velocity: 2.67222222222222,
		power: -729.087883214137,
		road: 14235.0718055556,
		acceleration: -0.401296296296297
	},
	{
		id: 1446,
		time: 1445,
		velocity: 2.27888888888889,
		power: -1191.10084575485,
		road: 14237.3880092593,
		acceleration: -0.672592592592592
	},
	{
		id: 1447,
		time: 1446,
		velocity: 0.988611111111111,
		power: -1108.54611167303,
		road: 14238.9225462963,
		acceleration: -0.890740740740741
	},
	{
		id: 1448,
		time: 1447,
		velocity: 0,
		power: -424.740222272379,
		road: 14239.6318981482,
		acceleration: -0.75962962962963
	},
	{
		id: 1449,
		time: 1448,
		velocity: 0,
		power: -31.5325896848603,
		road: 14239.7966666667,
		acceleration: -0.329537037037037
	},
	{
		id: 1450,
		time: 1449,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1451,
		time: 1450,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1452,
		time: 1451,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1453,
		time: 1452,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1454,
		time: 1453,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1455,
		time: 1454,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1456,
		time: 1455,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1457,
		time: 1456,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1458,
		time: 1457,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1459,
		time: 1458,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1460,
		time: 1459,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1461,
		time: 1460,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1462,
		time: 1461,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1463,
		time: 1462,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1464,
		time: 1463,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1465,
		time: 1464,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1466,
		time: 1465,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1467,
		time: 1466,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1468,
		time: 1467,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1469,
		time: 1468,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1470,
		time: 1469,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1471,
		time: 1470,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1472,
		time: 1471,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1473,
		time: 1472,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1474,
		time: 1473,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1475,
		time: 1474,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1476,
		time: 1475,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1477,
		time: 1476,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1478,
		time: 1477,
		velocity: 0,
		power: 0,
		road: 14239.7966666667,
		acceleration: 0
	},
	{
		id: 1479,
		time: 1478,
		velocity: 0,
		power: 59.3020537283957,
		road: 14239.944537037,
		acceleration: 0.295740740740741
	},
	{
		id: 1480,
		time: 1479,
		velocity: 0.887222222222222,
		power: 741.079337564757,
		road: 14240.6963425926,
		acceleration: 0.91212962962963
	},
	{
		id: 1481,
		time: 1480,
		velocity: 2.73638888888889,
		power: 2331.12794144203,
		road: 14242.5151851852,
		acceleration: 1.22194444444444
	},
	{
		id: 1482,
		time: 1481,
		velocity: 3.66583333333333,
		power: 2408.48419440692,
		road: 14245.3299074074,
		acceleration: 0.769814814814815
	},
	{
		id: 1483,
		time: 1482,
		velocity: 3.19666666666667,
		power: 974.868012554147,
		road: 14248.6189351852,
		acceleration: 0.178796296296296
	},
	{
		id: 1484,
		time: 1483,
		velocity: 3.27277777777778,
		power: -33.0451993243945,
		road: 14251.9253240741,
		acceleration: -0.144074074074074
	},
	{
		id: 1485,
		time: 1484,
		velocity: 3.23361111111111,
		power: 549.87168435595,
		road: 14255.1819444444,
		acceleration: 0.044537037037037
	},
	{
		id: 1486,
		time: 1485,
		velocity: 3.33027777777778,
		power: 218.895591078408,
		road: 14258.4296759259,
		acceleration: -0.062314814814814
	},
	{
		id: 1487,
		time: 1486,
		velocity: 3.08583333333333,
		power: 663.032932380613,
		road: 14261.6868055556,
		acceleration: 0.0811111111111109
	},
	{
		id: 1488,
		time: 1487,
		velocity: 3.47694444444444,
		power: 1065.54292755325,
		road: 14265.0828240741,
		acceleration: 0.196666666666666
	},
	{
		id: 1489,
		time: 1488,
		velocity: 3.92027777777778,
		power: 2064.62890014091,
		road: 14268.8019444444,
		acceleration: 0.449537037037037
	},
	{
		id: 1490,
		time: 1489,
		velocity: 4.43444444444444,
		power: 1898.44814854516,
		road: 14272.9200925926,
		acceleration: 0.348518518518518
	},
	{
		id: 1491,
		time: 1490,
		velocity: 4.5225,
		power: 963.031910453821,
		road: 14277.2603240741,
		acceleration: 0.0956481481481486
	},
	{
		id: 1492,
		time: 1491,
		velocity: 4.20722222222222,
		power: 181.42540909979,
		road: 14281.6013888889,
		acceleration: -0.0939814814814817
	},
	{
		id: 1493,
		time: 1492,
		velocity: 4.1525,
		power: 355.305417132406,
		road: 14285.870462963,
		acceleration: -0.0500000000000007
	},
	{
		id: 1494,
		time: 1493,
		velocity: 4.3725,
		power: 637.326105508159,
		road: 14290.1246296296,
		acceleration: 0.020185185185186
	},
	{
		id: 1495,
		time: 1494,
		velocity: 4.26777777777778,
		power: 540.114661768853,
		road: 14294.3868055556,
		acceleration: -0.00416666666666732
	},
	{
		id: 1496,
		time: 1495,
		velocity: 4.14,
		power: -277.83161377372,
		road: 14298.5431481482,
		acceleration: -0.2075
	},
	{
		id: 1497,
		time: 1496,
		velocity: 3.75,
		power: -499.351878760667,
		road: 14302.4606018519,
		acceleration: -0.270277777777778
	},
	{
		id: 1498,
		time: 1497,
		velocity: 3.45694444444444,
		power: 183.080581136135,
		road: 14306.2010648148,
		acceleration: -0.0837037037037032
	},
	{
		id: 1499,
		time: 1498,
		velocity: 3.88888888888889,
		power: 1078.51422306914,
		road: 14309.9821759259,
		acceleration: 0.165
	},
	{
		id: 1500,
		time: 1499,
		velocity: 4.245,
		power: 1958.30191299033,
		road: 14314.0320833333,
		acceleration: 0.372592592592593
	},
	{
		id: 1501,
		time: 1500,
		velocity: 4.57472222222222,
		power: 1942.80927773718,
		road: 14318.4316203704,
		acceleration: 0.326666666666666
	},
	{
		id: 1502,
		time: 1501,
		velocity: 4.86888888888889,
		power: 1457.86643115335,
		road: 14323.0894444444,
		acceleration: 0.189907407407407
	},
	{
		id: 1503,
		time: 1502,
		velocity: 4.81472222222222,
		power: 575.156786521747,
		road: 14327.8359722222,
		acceleration: -0.0125000000000002
	},
	{
		id: 1504,
		time: 1503,
		velocity: 4.53722222222222,
		power: -128.787838794912,
		road: 14332.4918981482,
		acceleration: -0.168703703703703
	},
	{
		id: 1505,
		time: 1504,
		velocity: 4.36277777777778,
		power: -751.210630965245,
		road: 14336.9046759259,
		acceleration: -0.317592592592592
	},
	{
		id: 1506,
		time: 1505,
		velocity: 3.86194444444444,
		power: -800.016428538232,
		road: 14340.9870833333,
		acceleration: -0.343148148148149
	},
	{
		id: 1507,
		time: 1506,
		velocity: 3.50777777777778,
		power: -1147.64150229235,
		road: 14344.6661111111,
		acceleration: -0.463611111111111
	},
	{
		id: 1508,
		time: 1507,
		velocity: 2.97194444444444,
		power: -1566.85255222249,
		road: 14347.7819444445,
		acceleration: -0.662777777777778
	},
	{
		id: 1509,
		time: 1508,
		velocity: 1.87361111111111,
		power: -2167.88483339819,
		road: 14349.9817592593,
		acceleration: -1.16925925925926
	},
	{
		id: 1510,
		time: 1509,
		velocity: 0,
		power: -915.46308956269,
		road: 14351.1016203704,
		acceleration: -0.990648148148148
	},
	{
		id: 1511,
		time: 1510,
		velocity: 0,
		power: -147.03124845679,
		road: 14351.4138888889,
		acceleration: -0.624537037037037
	},
	{
		id: 1512,
		time: 1511,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1513,
		time: 1512,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1514,
		time: 1513,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1515,
		time: 1514,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1516,
		time: 1515,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1517,
		time: 1516,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1518,
		time: 1517,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1519,
		time: 1518,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1520,
		time: 1519,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1521,
		time: 1520,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1522,
		time: 1521,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1523,
		time: 1522,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1524,
		time: 1523,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1525,
		time: 1524,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1526,
		time: 1525,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1527,
		time: 1526,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1528,
		time: 1527,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1529,
		time: 1528,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1530,
		time: 1529,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1531,
		time: 1530,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1532,
		time: 1531,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1533,
		time: 1532,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1534,
		time: 1533,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1535,
		time: 1534,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1536,
		time: 1535,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1537,
		time: 1536,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1538,
		time: 1537,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1539,
		time: 1538,
		velocity: 0,
		power: 0,
		road: 14351.4138888889,
		acceleration: 0
	},
	{
		id: 1540,
		time: 1539,
		velocity: 0,
		power: 85.5833666613668,
		road: 14351.5968981482,
		acceleration: 0.366018518518519
	},
	{
		id: 1541,
		time: 1540,
		velocity: 1.09805555555556,
		power: 886.298963683087,
		road: 14352.4481481482,
		acceleration: 0.970462962962963
	},
	{
		id: 1542,
		time: 1541,
		velocity: 2.91138888888889,
		power: 4137.71790510836,
		road: 14354.6914814815,
		acceleration: 1.8137037037037
	},
	{
		id: 1543,
		time: 1542,
		velocity: 5.44111111111111,
		power: 5687.17750916668,
		road: 14358.5498611111,
		acceleration: 1.41638888888889
	},
	{
		id: 1544,
		time: 1543,
		velocity: 5.34722222222222,
		power: 5883.79659440095,
		road: 14363.6521759259,
		acceleration: 1.07148148148148
	},
	{
		id: 1545,
		time: 1544,
		velocity: 6.12583333333333,
		power: 4602.55818949211,
		road: 14369.6219907407,
		acceleration: 0.663518518518519
	},
	{
		id: 1546,
		time: 1545,
		velocity: 7.43166666666667,
		power: 9454.55931795585,
		road: 14376.5623148148,
		acceleration: 1.2775
	},
	{
		id: 1547,
		time: 1546,
		velocity: 9.17972222222222,
		power: 12075.6374898754,
		road: 14384.8259259259,
		acceleration: 1.36907407407407
	},
	{
		id: 1548,
		time: 1547,
		velocity: 10.2330555555556,
		power: 9256.51480566851,
		road: 14394.2037037037,
		acceleration: 0.859259259259259
	},
	{
		id: 1549,
		time: 1548,
		velocity: 10.0094444444444,
		power: 3745.51337576576,
		road: 14404.117962963,
		acceleration: 0.213703703703704
	},
	{
		id: 1550,
		time: 1549,
		velocity: 9.82083333333333,
		power: 631.322135363313,
		road: 14414.0806481482,
		acceleration: -0.116851851851852
	},
	{
		id: 1551,
		time: 1550,
		velocity: 9.8825,
		power: 3211.93402569981,
		road: 14424.0619907407,
		acceleration: 0.154166666666667
	},
	{
		id: 1552,
		time: 1551,
		velocity: 10.4719444444444,
		power: 5250.99316723226,
		road: 14434.2963888889,
		acceleration: 0.351944444444445
	},
	{
		id: 1553,
		time: 1552,
		velocity: 10.8766666666667,
		power: 5674.12149360853,
		road: 14444.8923148148,
		acceleration: 0.371111111111112
	},
	{
		id: 1554,
		time: 1553,
		velocity: 10.9958333333333,
		power: 3083.67765969928,
		road: 14455.7262962963,
		acceleration: 0.104999999999999
	},
	{
		id: 1555,
		time: 1554,
		velocity: 10.7869444444444,
		power: 693.908331649742,
		road: 14466.5496759259,
		acceleration: -0.126203703703704
	},
	{
		id: 1556,
		time: 1555,
		velocity: 10.4980555555556,
		power: -681.813536304441,
		road: 14477.1807407407,
		acceleration: -0.258425925925927
	},
	{
		id: 1557,
		time: 1556,
		velocity: 10.2205555555556,
		power: -2203.34004641468,
		road: 14487.476712963,
		acceleration: -0.411759259259259
	},
	{
		id: 1558,
		time: 1557,
		velocity: 9.55166666666667,
		power: -3671.15291579771,
		road: 14497.2794444444,
		acceleration: -0.574722222222222
	},
	{
		id: 1559,
		time: 1558,
		velocity: 8.77388888888889,
		power: -4959.47337722957,
		road: 14506.4227314815,
		acceleration: -0.744166666666665
	},
	{
		id: 1560,
		time: 1559,
		velocity: 7.98805555555556,
		power: -5279.22478727507,
		road: 14514.7786574074,
		acceleration: -0.830555555555557
	},
	{
		id: 1561,
		time: 1560,
		velocity: 7.06,
		power: -3351.17368806502,
		road: 14522.4083796296,
		acceleration: -0.621851851851853
	},
	{
		id: 1562,
		time: 1561,
		velocity: 6.90833333333333,
		power: -2420.6690603623,
		road: 14529.469212963,
		acceleration: -0.515925925925925
	},
	{
		id: 1563,
		time: 1562,
		velocity: 6.44027777777778,
		power: -2496.48455135556,
		road: 14535.9952314815,
		acceleration: -0.553703703703703
	},
	{
		id: 1564,
		time: 1563,
		velocity: 5.39888888888889,
		power: -3944.83456276009,
		road: 14541.814537037,
		acceleration: -0.859722222222223
	},
	{
		id: 1565,
		time: 1564,
		velocity: 4.32916666666667,
		power: -3586.8293034886,
		road: 14546.7509722222,
		acceleration: -0.906018518518518
	},
	{
		id: 1566,
		time: 1565,
		velocity: 3.72222222222222,
		power: -2353.78081735238,
		road: 14550.8646296296,
		acceleration: -0.739537037037037
	},
	{
		id: 1567,
		time: 1566,
		velocity: 3.18027777777778,
		power: -960.949150055789,
		road: 14554.3980555556,
		acceleration: -0.420925925925926
	},
	{
		id: 1568,
		time: 1567,
		velocity: 3.06638888888889,
		power: -503.785736936445,
		road: 14557.5708333333,
		acceleration: -0.30037037037037
	},
	{
		id: 1569,
		time: 1568,
		velocity: 2.82111111111111,
		power: -665.813652632676,
		road: 14560.4036111111,
		acceleration: -0.37962962962963
	},
	{
		id: 1570,
		time: 1569,
		velocity: 2.04138888888889,
		power: -873.447434071422,
		road: 14562.7881944444,
		acceleration: -0.516759259259259
	},
	{
		id: 1571,
		time: 1570,
		velocity: 1.51611111111111,
		power: -1068.01643040632,
		road: 14564.5256944444,
		acceleration: -0.777407407407408
	},
	{
		id: 1572,
		time: 1571,
		velocity: 0.488888888888889,
		power: -528.073185028635,
		road: 14565.5342592593,
		acceleration: -0.680462962962963
	},
	{
		id: 1573,
		time: 1572,
		velocity: 0,
		power: -148.776939418197,
		road: 14565.9499074074,
		acceleration: -0.50537037037037
	},
	{
		id: 1574,
		time: 1573,
		velocity: 0,
		power: -2.73517608836907,
		road: 14566.0313888889,
		acceleration: -0.162962962962963
	},
	{
		id: 1575,
		time: 1574,
		velocity: 0,
		power: 0,
		road: 14566.0313888889,
		acceleration: 0
	},
	{
		id: 1576,
		time: 1575,
		velocity: 0,
		power: 0,
		road: 14566.0313888889,
		acceleration: 0
	},
	{
		id: 1577,
		time: 1576,
		velocity: 0,
		power: 0,
		road: 14566.0313888889,
		acceleration: 0
	},
	{
		id: 1578,
		time: 1577,
		velocity: 0,
		power: 0,
		road: 14566.0313888889,
		acceleration: 0
	},
	{
		id: 1579,
		time: 1578,
		velocity: 0,
		power: 0,
		road: 14566.0313888889,
		acceleration: 0
	},
	{
		id: 1580,
		time: 1579,
		velocity: 0,
		power: 0,
		road: 14566.0313888889,
		acceleration: 0
	},
	{
		id: 1581,
		time: 1580,
		velocity: 0,
		power: 0,
		road: 14566.0313888889,
		acceleration: 0
	},
	{
		id: 1582,
		time: 1581,
		velocity: 0,
		power: 0,
		road: 14566.0313888889,
		acceleration: 0
	},
	{
		id: 1583,
		time: 1582,
		velocity: 0,
		power: 56.5886373485521,
		road: 14566.1752314815,
		acceleration: 0.287685185185185
	},
	{
		id: 1584,
		time: 1583,
		velocity: 0.863055555555556,
		power: 664.943380520149,
		road: 14566.8898611111,
		acceleration: 0.853888888888889
	},
	{
		id: 1585,
		time: 1584,
		velocity: 2.56166666666667,
		power: 3854.81739466,
		road: 14568.9518055556,
		acceleration: 1.84074074074074
	},
	{
		id: 1586,
		time: 1585,
		velocity: 5.52222222222222,
		power: 5193.90261092519,
		road: 14572.6135648148,
		acceleration: 1.35888888888889
	},
	{
		id: 1587,
		time: 1586,
		velocity: 4.93972222222222,
		power: 3155.46307463969,
		road: 14577.24375,
		acceleration: 0.577962962962962
	},
	{
		id: 1588,
		time: 1587,
		velocity: 4.29555555555556,
		power: -2092.60295303082,
		road: 14581.8543055556,
		acceleration: -0.617222222222222
	},
	{
		id: 1589,
		time: 1588,
		velocity: 3.67055555555556,
		power: -1268.85587657394,
		road: 14585.9237037037,
		acceleration: -0.465092592592593
	},
	{
		id: 1590,
		time: 1589,
		velocity: 3.54444444444444,
		power: -691.754394988276,
		road: 14589.5937962963,
		acceleration: -0.333518518518519
	},
	{
		id: 1591,
		time: 1590,
		velocity: 3.295,
		power: -243.13331316398,
		road: 14592.9925,
		acceleration: -0.20925925925926
	},
	{
		id: 1592,
		time: 1591,
		velocity: 3.04277777777778,
		power: -256.516027802024,
		road: 14596.1775925926,
		acceleration: -0.217962962962963
	},
	{
		id: 1593,
		time: 1592,
		velocity: 2.89055555555556,
		power: 20.1433958990318,
		road: 14599.1909722222,
		acceleration: -0.125462962962963
	},
	{
		id: 1594,
		time: 1593,
		velocity: 2.91861111111111,
		power: 451.531863937095,
		road: 14602.1556944444,
		acceleration: 0.0281481481481487
	},
	{
		id: 1595,
		time: 1594,
		velocity: 3.12722222222222,
		power: 775.086379857619,
		road: 14605.2022222222,
		acceleration: 0.135462962962962
	},
	{
		id: 1596,
		time: 1595,
		velocity: 3.29694444444444,
		power: 1315.22294360731,
		road: 14608.4623148148,
		acceleration: 0.291666666666667
	},
	{
		id: 1597,
		time: 1596,
		velocity: 3.79361111111111,
		power: 1618.49155718042,
		road: 14612.0392592593,
		acceleration: 0.342037037037037
	},
	{
		id: 1598,
		time: 1597,
		velocity: 4.15333333333333,
		power: 2199.72462270379,
		road: 14616.0107407407,
		acceleration: 0.447037037037038
	},
	{
		id: 1599,
		time: 1598,
		velocity: 4.63805555555555,
		power: 2237.38862802654,
		road: 14620.4046759259,
		acceleration: 0.39787037037037
	},
	{
		id: 1600,
		time: 1599,
		velocity: 4.98722222222222,
		power: 2645.31739599225,
		road: 14625.2166666667,
		acceleration: 0.43824074074074
	},
	{
		id: 1601,
		time: 1600,
		velocity: 5.46805555555556,
		power: 3027.25827272202,
		road: 14630.4789814815,
		acceleration: 0.462407407407407
	},
	{
		id: 1602,
		time: 1601,
		velocity: 6.02527777777778,
		power: 2690.39999809273,
		road: 14636.1493518519,
		acceleration: 0.353703703703704
	},
	{
		id: 1603,
		time: 1602,
		velocity: 6.04833333333333,
		power: 1738.72973000278,
		road: 14642.0772685185,
		acceleration: 0.16138888888889
	},
	{
		id: 1604,
		time: 1603,
		velocity: 5.95222222222222,
		power: 628.423686455096,
		road: 14648.0672685185,
		acceleration: -0.0372222222222227
	},
	{
		id: 1605,
		time: 1604,
		velocity: 5.91361111111111,
		power: 262.102031837547,
		road: 14653.9883796296,
		acceleration: -0.100555555555555
	},
	{
		id: 1606,
		time: 1605,
		velocity: 5.74666666666667,
		power: 110.316349077988,
		road: 14659.7960185185,
		acceleration: -0.126388888888889
	},
	{
		id: 1607,
		time: 1606,
		velocity: 5.57305555555556,
		power: -2043.79607017742,
		road: 14665.2719444444,
		acceleration: -0.537037037037037
	},
	{
		id: 1608,
		time: 1607,
		velocity: 4.3025,
		power: -2433.71131669353,
		road: 14670.1461574074,
		acceleration: -0.666388888888888
	},
	{
		id: 1609,
		time: 1608,
		velocity: 3.7475,
		power: -2430.3530203308,
		road: 14674.3113425926,
		acceleration: -0.751666666666667
	},
	{
		id: 1610,
		time: 1609,
		velocity: 3.31805555555556,
		power: -1212.4517171278,
		road: 14677.8531481482,
		acceleration: -0.495092592592592
	},
	{
		id: 1611,
		time: 1610,
		velocity: 2.81722222222222,
		power: -599.1687857924,
		road: 14680.98,
		acceleration: -0.334814814814814
	},
	{
		id: 1612,
		time: 1611,
		velocity: 2.74305555555556,
		power: -12.2647339734485,
		road: 14683.8711574074,
		acceleration: -0.136574074074074
	},
	{
		id: 1613,
		time: 1612,
		velocity: 2.90833333333333,
		power: 864.797932279416,
		road: 14686.7843518519,
		acceleration: 0.180648148148148
	},
	{
		id: 1614,
		time: 1613,
		velocity: 3.35916666666667,
		power: 1900.38757257457,
		road: 14690.0296759259,
		acceleration: 0.483611111111111
	},
	{
		id: 1615,
		time: 1614,
		velocity: 4.19388888888889,
		power: 3097.68534030235,
		road: 14693.8733333333,
		acceleration: 0.713055555555555
	},
	{
		id: 1616,
		time: 1615,
		velocity: 5.0475,
		power: 3401.48896146351,
		road: 14698.3996296296,
		acceleration: 0.652222222222223
	},
	{
		id: 1617,
		time: 1616,
		velocity: 5.31583333333333,
		power: 2079.78448770383,
		road: 14703.4001851852,
		acceleration: 0.296296296296296
	},
	{
		id: 1618,
		time: 1617,
		velocity: 5.08277777777778,
		power: 642.320824346076,
		road: 14708.5434722222,
		acceleration: -0.0108333333333341
	},
	{
		id: 1619,
		time: 1618,
		velocity: 5.015,
		power: 77.8138530840282,
		road: 14713.6184722222,
		acceleration: -0.125740740740739
	},
	{
		id: 1620,
		time: 1619,
		velocity: 4.93861111111111,
		power: 182.55161019452,
		road: 14718.5793518519,
		acceleration: -0.1025
	},
	{
		id: 1621,
		time: 1620,
		velocity: 4.77527777777778,
		power: 73.5696601779098,
		road: 14723.4266666667,
		acceleration: -0.12462962962963
	},
	{
		id: 1622,
		time: 1621,
		velocity: 4.64111111111111,
		power: -274.752068259029,
		road: 14728.1109259259,
		acceleration: -0.201481481481482
	},
	{
		id: 1623,
		time: 1622,
		velocity: 4.33416666666667,
		power: -981.946148765092,
		road: 14732.5077314815,
		acceleration: -0.373425925925925
	},
	{
		id: 1624,
		time: 1623,
		velocity: 3.655,
		power: -1840.54622209082,
		road: 14736.4008796296,
		acceleration: -0.633888888888889
	},
	{
		id: 1625,
		time: 1624,
		velocity: 2.73944444444444,
		power: -1901.09304647073,
		road: 14739.5971296296,
		acceleration: -0.759907407407407
	},
	{
		id: 1626,
		time: 1625,
		velocity: 2.05444444444444,
		power: -1575.73881648154,
		road: 14742.0028240741,
		acceleration: -0.821203703703703
	},
	{
		id: 1627,
		time: 1626,
		velocity: 1.19138888888889,
		power: -841.408671881387,
		road: 14743.6667592593,
		acceleration: -0.662314814814815
	},
	{
		id: 1628,
		time: 1627,
		velocity: 0.7525,
		power: -522.644300676825,
		road: 14744.6571296296,
		acceleration: -0.684814814814815
	},
	{
		id: 1629,
		time: 1628,
		velocity: 0,
		power: -114.765510225716,
		road: 14745.1065277778,
		acceleration: -0.39712962962963
	},
	{
		id: 1630,
		time: 1629,
		velocity: 0,
		power: -14.6503828947368,
		road: 14745.2319444445,
		acceleration: -0.250833333333333
	},
	{
		id: 1631,
		time: 1630,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1632,
		time: 1631,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1633,
		time: 1632,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1634,
		time: 1633,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1635,
		time: 1634,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1636,
		time: 1635,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1637,
		time: 1636,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1638,
		time: 1637,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1639,
		time: 1638,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1640,
		time: 1639,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1641,
		time: 1640,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1642,
		time: 1641,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1643,
		time: 1642,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1644,
		time: 1643,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1645,
		time: 1644,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1646,
		time: 1645,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1647,
		time: 1646,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1648,
		time: 1647,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1649,
		time: 1648,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1650,
		time: 1649,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1651,
		time: 1650,
		velocity: 0,
		power: 0,
		road: 14745.2319444445,
		acceleration: 0
	},
	{
		id: 1652,
		time: 1651,
		velocity: 0,
		power: 187.716538261648,
		road: 14745.5163888889,
		acceleration: 0.568888888888889
	},
	{
		id: 1653,
		time: 1652,
		velocity: 1.70666666666667,
		power: 1507.18908003014,
		road: 14746.6953240741,
		acceleration: 1.22009259259259
	},
	{
		id: 1654,
		time: 1653,
		velocity: 3.66027777777778,
		power: 3611.90777705186,
		road: 14749.1837037037,
		acceleration: 1.3987962962963
	},
	{
		id: 1655,
		time: 1654,
		velocity: 4.19638888888889,
		power: 3460.37252534595,
		road: 14752.807037037,
		acceleration: 0.871111111111111
	},
	{
		id: 1656,
		time: 1655,
		velocity: 4.32,
		power: 2360.49240418593,
		road: 14757.0874074074,
		acceleration: 0.442962962962963
	},
	{
		id: 1657,
		time: 1656,
		velocity: 4.98916666666667,
		power: 3445.08869174916,
		road: 14761.8961111111,
		acceleration: 0.613703703703703
	},
	{
		id: 1658,
		time: 1657,
		velocity: 6.0375,
		power: 5920.09448029448,
		road: 14767.4953240741,
		acceleration: 0.967314814814815
	},
	{
		id: 1659,
		time: 1658,
		velocity: 7.22194444444444,
		power: 7137.65671252392,
		road: 14774.0728240741,
		acceleration: 0.989259259259259
	},
	{
		id: 1660,
		time: 1659,
		velocity: 7.95694444444444,
		power: 8197.05438914345,
		road: 14781.6347222222,
		acceleration: 0.979537037037037
	},
	{
		id: 1661,
		time: 1660,
		velocity: 8.97611111111111,
		power: 7565.55902173541,
		road: 14790.0734722222,
		acceleration: 0.774166666666668
	},
	{
		id: 1662,
		time: 1661,
		velocity: 9.54444444444444,
		power: 5828.28099816041,
		road: 14799.149537037,
		acceleration: 0.500462962962963
	},
	{
		id: 1663,
		time: 1662,
		velocity: 9.45833333333333,
		power: 2683.6962104767,
		road: 14808.5373611111,
		acceleration: 0.123055555555554
	},
	{
		id: 1664,
		time: 1663,
		velocity: 9.34527777777778,
		power: 1480.52780250656,
		road: 14817.9801851852,
		acceleration: -0.0130555555555549
	},
	{
		id: 1665,
		time: 1664,
		velocity: 9.50527777777778,
		power: 4106.30788871169,
		road: 14827.5521296296,
		acceleration: 0.271296296296295
	},
	{
		id: 1666,
		time: 1665,
		velocity: 10.2722222222222,
		power: 6957.53523267122,
		road: 14837.5337037037,
		acceleration: 0.547962962962963
	},
	{
		id: 1667,
		time: 1666,
		velocity: 10.9891666666667,
		power: 9056.57040786287,
		road: 14848.1416666667,
		acceleration: 0.704814814814815
	},
	{
		id: 1668,
		time: 1667,
		velocity: 11.6197222222222,
		power: 7788.87713352716,
		road: 14859.3666666667,
		acceleration: 0.529259259259257
	},
	{
		id: 1669,
		time: 1668,
		velocity: 11.86,
		power: 5997.07200976533,
		road: 14871.0239814815,
		acceleration: 0.335370370370374
	},
	{
		id: 1670,
		time: 1669,
		velocity: 11.9952777777778,
		power: 4582.41965004337,
		road: 14882.9466666667,
		acceleration: 0.19537037037037
	},
	{
		id: 1671,
		time: 1670,
		velocity: 12.2058333333333,
		power: 5888.02070986473,
		road: 14895.1151851852,
		acceleration: 0.296296296296298
	},
	{
		id: 1672,
		time: 1671,
		velocity: 12.7488888888889,
		power: 6498.44099930806,
		road: 14907.597037037,
		acceleration: 0.330370370370369
	},
	{
		id: 1673,
		time: 1672,
		velocity: 12.9863888888889,
		power: 6872.67050846828,
		road: 14920.4149537037,
		acceleration: 0.341759259259257
	},
	{
		id: 1674,
		time: 1673,
		velocity: 13.2311111111111,
		power: 7224.34772434041,
		road: 14933.5786574074,
		acceleration: 0.349814814814817
	},
	{
		id: 1675,
		time: 1674,
		velocity: 13.7983333333333,
		power: 7271.85748242037,
		road: 14947.0841203704,
		acceleration: 0.333703703703701
	},
	{
		id: 1676,
		time: 1675,
		velocity: 13.9875,
		power: 7682.09844090859,
		road: 14960.929212963,
		acceleration: 0.345555555555556
	},
	{
		id: 1677,
		time: 1676,
		velocity: 14.2677777777778,
		power: 6025.82904208477,
		road: 14975.0505555556,
		acceleration: 0.206944444444444
	},
	{
		id: 1678,
		time: 1677,
		velocity: 14.4191666666667,
		power: 5682.03590707645,
		road: 14989.3617592593,
		acceleration: 0.17277777777778
	},
	{
		id: 1679,
		time: 1678,
		velocity: 14.5058333333333,
		power: 4924.44373509488,
		road: 15003.8150925926,
		acceleration: 0.11148148148148
	},
	{
		id: 1680,
		time: 1679,
		velocity: 14.6022222222222,
		power: 5443.55405051383,
		road: 15018.3959259259,
		acceleration: 0.143518518518519
	},
	{
		id: 1681,
		time: 1680,
		velocity: 14.8497222222222,
		power: 6461.93372561861,
		road: 15033.1525925926,
		acceleration: 0.208148148148148
	},
	{
		id: 1682,
		time: 1681,
		velocity: 15.1302777777778,
		power: 6953.76903743654,
		road: 15048.1293518519,
		acceleration: 0.232037037037038
	},
	{
		id: 1683,
		time: 1682,
		velocity: 15.2983333333333,
		power: 7239.80154280448,
		road: 15063.3421759259,
		acceleration: 0.240092592592593
	},
	{
		id: 1684,
		time: 1683,
		velocity: 15.57,
		power: 6339.31670474009,
		road: 15078.7594444444,
		acceleration: 0.168796296296295
	},
	{
		id: 1685,
		time: 1684,
		velocity: 15.6366666666667,
		power: 4115.7204977818,
		road: 15094.2684259259,
		acceleration: 0.0146296296296313
	},
	{
		id: 1686,
		time: 1685,
		velocity: 15.3422222222222,
		power: 2412.71458034217,
		road: 15109.7351851852,
		acceleration: -0.0990740740740748
	},
	{
		id: 1687,
		time: 1686,
		velocity: 15.2727777777778,
		power: 1984.72969652833,
		road: 15125.0898611111,
		acceleration: -0.125092592592592
	},
	{
		id: 1688,
		time: 1687,
		velocity: 15.2613888888889,
		power: 3611.41154311199,
		road: 15140.3760185185,
		acceleration: -0.0119444444444454
	},
	{
		id: 1689,
		time: 1688,
		velocity: 15.3063888888889,
		power: 5266.21444793112,
		road: 15155.7059722222,
		acceleration: 0.0995370370370381
	},
	{
		id: 1690,
		time: 1689,
		velocity: 15.5713888888889,
		power: 5380.48751365756,
		road: 15171.1372685185,
		acceleration: 0.103148148148149
	},
	{
		id: 1691,
		time: 1690,
		velocity: 15.5708333333333,
		power: 5877.47933718142,
		road: 15186.6860185185,
		acceleration: 0.13175925925926
	},
	{
		id: 1692,
		time: 1691,
		velocity: 15.7016666666667,
		power: 4371.22706156645,
		road: 15202.3143981482,
		acceleration: 0.0274999999999981
	},
	{
		id: 1693,
		time: 1692,
		velocity: 15.6538888888889,
		power: 4599.38739595376,
		road: 15217.9772685185,
		acceleration: 0.0414814814814815
	},
	{
		id: 1694,
		time: 1693,
		velocity: 15.6952777777778,
		power: 3888.89029947799,
		road: 15233.6575925926,
		acceleration: -0.00657407407407362
	},
	{
		id: 1695,
		time: 1694,
		velocity: 15.6819444444444,
		power: 3917.96294149973,
		road: 15249.3324074074,
		acceleration: -0.00444444444444514
	},
	{
		id: 1696,
		time: 1695,
		velocity: 15.6405555555556,
		power: 3838.08331533856,
		road: 15265.0002314815,
		acceleration: -0.00953703703703823
	},
	{
		id: 1697,
		time: 1696,
		velocity: 15.6666666666667,
		power: 4568.70411857906,
		road: 15280.6826388889,
		acceleration: 0.0387037037037032
	},
	{
		id: 1698,
		time: 1697,
		velocity: 15.7980555555556,
		power: 4574.77413638339,
		road: 15296.4032407407,
		acceleration: 0.0376851851851878
	},
	{
		id: 1699,
		time: 1698,
		velocity: 15.7536111111111,
		power: 4738.02352988905,
		road: 15312.1661574074,
		acceleration: 0.0469444444444438
	},
	{
		id: 1700,
		time: 1699,
		velocity: 15.8075,
		power: 3959.13540074854,
		road: 15327.9498148148,
		acceleration: -0.00546296296296234
	},
	{
		id: 1701,
		time: 1700,
		velocity: 15.7816666666667,
		power: 3906.56968401679,
		road: 15343.7263888889,
		acceleration: -0.00870370370370566
	},
	{
		id: 1702,
		time: 1701,
		velocity: 15.7275,
		power: 3679.29286899447,
		road: 15359.4869907407,
		acceleration: -0.0232407407407393
	},
	{
		id: 1703,
		time: 1702,
		velocity: 15.7377777777778,
		power: 3466.61096387402,
		road: 15375.2177777778,
		acceleration: -0.0363888888888884
	},
	{
		id: 1704,
		time: 1703,
		velocity: 15.6725,
		power: 4392.27518245625,
		road: 15390.9430555556,
		acceleration: 0.0253703703703696
	},
	{
		id: 1705,
		time: 1704,
		velocity: 15.8036111111111,
		power: 4720.12053046475,
		road: 15406.7039351852,
		acceleration: 0.0458333333333343
	},
	{
		id: 1706,
		time: 1705,
		velocity: 15.8752777777778,
		power: 5804.96778908767,
		road: 15422.5449537037,
		acceleration: 0.114444444444445
	},
	{
		id: 1707,
		time: 1706,
		velocity: 16.0158333333333,
		power: 4532.19323846728,
		road: 15438.4570833333,
		acceleration: 0.0277777777777768
	},
	{
		id: 1708,
		time: 1707,
		velocity: 15.8869444444444,
		power: 4372.51526574806,
		road: 15454.3913425926,
		acceleration: 0.0164814814814829
	},
	{
		id: 1709,
		time: 1708,
		velocity: 15.9247222222222,
		power: 3026.91596021988,
		road: 15470.2983333333,
		acceleration: -0.0710185185185193
	},
	{
		id: 1710,
		time: 1709,
		velocity: 15.8027777777778,
		power: 3253.19554382978,
		road: 15486.1427314815,
		acceleration: -0.0541666666666671
	},
	{
		id: 1711,
		time: 1710,
		velocity: 15.7244444444444,
		power: 3272.84183571355,
		road: 15501.9344444444,
		acceleration: -0.0512037037037043
	},
	{
		id: 1712,
		time: 1711,
		velocity: 15.7711111111111,
		power: 3142.34171232364,
		road: 15517.6714814815,
		acceleration: -0.0581481481481489
	},
	{
		id: 1713,
		time: 1712,
		velocity: 15.6283333333333,
		power: 4054.19405143163,
		road: 15533.3811574074,
		acceleration: 0.00342592592592794
	},
	{
		id: 1714,
		time: 1713,
		velocity: 15.7347222222222,
		power: 3644.55387095123,
		road: 15549.080787037,
		acceleration: -0.0235185185185181
	},
	{
		id: 1715,
		time: 1714,
		velocity: 15.7005555555556,
		power: 3775.32390165702,
		road: 15564.7615740741,
		acceleration: -0.014166666666668
	},
	{
		id: 1716,
		time: 1715,
		velocity: 15.5858333333333,
		power: 2960.14730972262,
		road: 15580.4016203704,
		acceleration: -0.0673148148148144
	},
	{
		id: 1717,
		time: 1716,
		velocity: 15.5327777777778,
		power: 3254.49231612445,
		road: 15595.9850925926,
		acceleration: -0.0458333333333343
	},
	{
		id: 1718,
		time: 1717,
		velocity: 15.5630555555556,
		power: 3406.1767336659,
		road: 15611.5284722222,
		acceleration: -0.0343518518518486
	},
	{
		id: 1719,
		time: 1718,
		velocity: 15.4827777777778,
		power: 3931.45979120219,
		road: 15627.055462963,
		acceleration: 0.00157407407407284
	},
	{
		id: 1720,
		time: 1719,
		velocity: 15.5375,
		power: 3619.30229033916,
		road: 15642.5736574074,
		acceleration: -0.0191666666666688
	},
	{
		id: 1721,
		time: 1720,
		velocity: 15.5055555555556,
		power: 3876.33242041206,
		road: 15658.0815277778,
		acceleration: -0.00148148148148053
	},
	{
		id: 1722,
		time: 1721,
		velocity: 15.4783333333333,
		power: 4322.710645352,
		road: 15673.6027314815,
		acceleration: 0.0281481481481478
	},
	{
		id: 1723,
		time: 1722,
		velocity: 15.6219444444444,
		power: 4967.93617305076,
		road: 15689.1728703704,
		acceleration: 0.0697222222222216
	},
	{
		id: 1724,
		time: 1723,
		velocity: 15.7147222222222,
		power: 5565.43952738378,
		road: 15704.8309722222,
		acceleration: 0.106203703703704
	},
	{
		id: 1725,
		time: 1724,
		velocity: 15.7969444444444,
		power: 5904.06578586513,
		road: 15720.6041203704,
		acceleration: 0.12388888888889
	},
	{
		id: 1726,
		time: 1725,
		velocity: 15.9936111111111,
		power: 5232.38477947239,
		road: 15736.4768981482,
		acceleration: 0.0753703703703685
	},
	{
		id: 1727,
		time: 1726,
		velocity: 15.9408333333333,
		power: 3607.68955834385,
		road: 15752.3711574074,
		acceleration: -0.0324074074074066
	},
	{
		id: 1728,
		time: 1727,
		velocity: 15.6997222222222,
		power: 545.398742850465,
		road: 15768.1335648148,
		acceleration: -0.231296296296295
	},
	{
		id: 1729,
		time: 1728,
		velocity: 15.2997222222222,
		power: -1197.72281008541,
		road: 15783.6086111111,
		acceleration: -0.343425925925928
	},
	{
		id: 1730,
		time: 1729,
		velocity: 14.9105555555556,
		power: -1249.78621572135,
		road: 15798.740462963,
		acceleration: -0.342962962962963
	},
	{
		id: 1731,
		time: 1730,
		velocity: 14.6708333333333,
		power: 163.162105558307,
		road: 15813.580787037,
		acceleration: -0.240092592592594
	},
	{
		id: 1732,
		time: 1731,
		velocity: 14.5794444444444,
		power: 1848.38896451377,
		road: 15828.2427314815,
		acceleration: -0.116666666666667
	},
	{
		id: 1733,
		time: 1732,
		velocity: 14.5605555555556,
		power: 2484.18463038333,
		road: 15842.812037037,
		acceleration: -0.0686111111111103
	},
	{
		id: 1734,
		time: 1733,
		velocity: 14.465,
		power: 2977.86275635975,
		road: 15857.33125,
		acceleration: -0.031574074074074
	},
	{
		id: 1735,
		time: 1734,
		velocity: 14.4847222222222,
		power: 2839.29631695368,
		road: 15871.8144444444,
		acceleration: -0.0404629629629625
	},
	{
		id: 1736,
		time: 1735,
		velocity: 14.4391666666667,
		power: 2181.26559840694,
		road: 15886.2342592593,
		acceleration: -0.086296296296295
	},
	{
		id: 1737,
		time: 1736,
		velocity: 14.2061111111111,
		power: 2016.66597224509,
		road: 15900.5630092593,
		acceleration: -0.0958333333333332
	},
	{
		id: 1738,
		time: 1737,
		velocity: 14.1972222222222,
		power: 1251.49073620021,
		road: 15914.7693518519,
		acceleration: -0.148981481481483
	},
	{
		id: 1739,
		time: 1738,
		velocity: 13.9922222222222,
		power: 2371.29706297402,
		road: 15928.8694907407,
		acceleration: -0.0634259259259267
	},
	{
		id: 1740,
		time: 1739,
		velocity: 14.0158333333333,
		power: 1479.23634060133,
		road: 15942.874212963,
		acceleration: -0.127407407407405
	},
	{
		id: 1741,
		time: 1740,
		velocity: 13.815,
		power: 1603.07448324249,
		road: 15956.7576388889,
		acceleration: -0.115185185185185
	},
	{
		id: 1742,
		time: 1741,
		velocity: 13.6466666666667,
		power: 1305.07493119799,
		road: 15970.5160648148,
		acceleration: -0.134814814814815
	},
	{
		id: 1743,
		time: 1742,
		velocity: 13.6113888888889,
		power: 2038.96364438108,
		road: 15984.1690277778,
		acceleration: -0.0761111111111106
	},
	{
		id: 1744,
		time: 1743,
		velocity: 13.5866666666667,
		power: 1961.28757372034,
		road: 15997.7439351852,
		acceleration: -0.0800000000000001
	},
	{
		id: 1745,
		time: 1744,
		velocity: 13.4066666666667,
		power: 1015.34892009366,
		road: 16011.2035648148,
		acceleration: -0.150555555555556
	},
	{
		id: 1746,
		time: 1745,
		velocity: 13.1597222222222,
		power: 1076.09475476399,
		road: 16024.5165740741,
		acceleration: -0.142685185185185
	},
	{
		id: 1747,
		time: 1746,
		velocity: 13.1586111111111,
		power: 723.920310534557,
		road: 16037.674537037,
		acceleration: -0.167407407407408
	},
	{
		id: 1748,
		time: 1747,
		velocity: 12.9044444444444,
		power: 1261.0014436869,
		road: 16050.6881018519,
		acceleration: -0.121388888888887
	},
	{
		id: 1749,
		time: 1748,
		velocity: 12.7955555555556,
		power: 94.4504199204699,
		road: 16063.5345833333,
		acceleration: -0.212777777777777
	},
	{
		id: 1750,
		time: 1749,
		velocity: 12.5202777777778,
		power: 950.238291156492,
		road: 16076.205,
		acceleration: -0.139351851851853
	},
	{
		id: 1751,
		time: 1750,
		velocity: 12.4863888888889,
		power: 423.671663155404,
		road: 16088.7156481482,
		acceleration: -0.180185185185184
	},
	{
		id: 1752,
		time: 1751,
		velocity: 12.255,
		power: 1032.83309933431,
		road: 16101.0732407407,
		acceleration: -0.125925925925927
	},
	{
		id: 1753,
		time: 1752,
		velocity: 12.1425,
		power: 19.4437017357513,
		road: 16113.2631018519,
		acceleration: -0.209537037037038
	},
	{
		id: 1754,
		time: 1753,
		velocity: 11.8577777777778,
		power: -28.4549769126839,
		road: 16125.2427777778,
		acceleration: -0.210833333333333
	},
	{
		id: 1755,
		time: 1754,
		velocity: 11.6225,
		power: 46.0208221466188,
		road: 16137.0162962963,
		acceleration: -0.201481481481483
	},
	{
		id: 1756,
		time: 1755,
		velocity: 11.5380555555556,
		power: -255.294364483273,
		road: 16148.5761111111,
		acceleration: -0.225925925925925
	},
	{
		id: 1757,
		time: 1756,
		velocity: 11.18,
		power: -72.4211010495745,
		road: 16159.9196296296,
		acceleration: -0.206666666666669
	},
	{
		id: 1758,
		time: 1757,
		velocity: 11.0025,
		power: -4866.18471233115,
		road: 16170.8285648148,
		acceleration: -0.6625
	},
	{
		id: 1759,
		time: 1758,
		velocity: 9.55055555555555,
		power: -6951.23403661607,
		road: 16180.9533333333,
		acceleration: -0.905833333333334
	},
	{
		id: 1760,
		time: 1759,
		velocity: 8.4625,
		power: -9152.80815733335,
		road: 16190.0075,
		acceleration: -1.23537037037037
	},
	{
		id: 1761,
		time: 1760,
		velocity: 7.29638888888889,
		power: -6175.08575757623,
		road: 16197.9541666667,
		acceleration: -0.979629629629629
	},
	{
		id: 1762,
		time: 1761,
		velocity: 6.61166666666667,
		power: -6444.69389535965,
		road: 16204.8419907407,
		acceleration: -1.13805555555556
	},
	{
		id: 1763,
		time: 1762,
		velocity: 5.04833333333333,
		power: -6191.73850689776,
		road: 16210.5136111111,
		acceleration: -1.29435185185185
	},
	{
		id: 1764,
		time: 1763,
		velocity: 3.41333333333333,
		power: -5090.49875287184,
		road: 16214.8510648148,
		acceleration: -1.37398148148148
	},
	{
		id: 1765,
		time: 1764,
		velocity: 2.48972222222222,
		power: -2811.76118245458,
		road: 16217.958287037,
		acceleration: -1.08648148148148
	},
	{
		id: 1766,
		time: 1765,
		velocity: 1.78888888888889,
		power: -1907.25387793749,
		road: 16219.9533796296,
		acceleration: -1.13777777777778
	},
	{
		id: 1767,
		time: 1766,
		velocity: 0,
		power: -672.700840268037,
		road: 16220.9646296296,
		acceleration: -0.829907407407407
	},
	{
		id: 1768,
		time: 1767,
		velocity: 0,
		power: -132.405918778428,
		road: 16221.2627777778,
		acceleration: -0.596296296296296
	},
	{
		id: 1769,
		time: 1768,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1770,
		time: 1769,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1771,
		time: 1770,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1772,
		time: 1771,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1773,
		time: 1772,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1774,
		time: 1773,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1775,
		time: 1774,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1776,
		time: 1775,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1777,
		time: 1776,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1778,
		time: 1777,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1779,
		time: 1778,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1780,
		time: 1779,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1781,
		time: 1780,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1782,
		time: 1781,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1783,
		time: 1782,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1784,
		time: 1783,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1785,
		time: 1784,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1786,
		time: 1785,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1787,
		time: 1786,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1788,
		time: 1787,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1789,
		time: 1788,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1790,
		time: 1789,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1791,
		time: 1790,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1792,
		time: 1791,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1793,
		time: 1792,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1794,
		time: 1793,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1795,
		time: 1794,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1796,
		time: 1795,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1797,
		time: 1796,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1798,
		time: 1797,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1799,
		time: 1798,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1800,
		time: 1799,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1801,
		time: 1800,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1802,
		time: 1801,
		velocity: 0,
		power: 0,
		road: 16221.2627777778,
		acceleration: 0
	},
	{
		id: 1803,
		time: 1802,
		velocity: 0,
		power: 134.607313531695,
		road: 16221.4993055556,
		acceleration: 0.473055555555556
	},
	{
		id: 1804,
		time: 1803,
		velocity: 1.41916666666667,
		power: 1307.7079655741,
		road: 16222.5590740741,
		acceleration: 1.17342592592593
	},
	{
		id: 1805,
		time: 1804,
		velocity: 3.52027777777778,
		power: 4651.67492690944,
		road: 16225.1033333333,
		acceleration: 1.79555555555556
	},
	{
		id: 1806,
		time: 1805,
		velocity: 5.38666666666667,
		power: 8225.13065617211,
		road: 16229.4681481482,
		acceleration: 1.84555555555556
	},
	{
		id: 1807,
		time: 1806,
		velocity: 6.95583333333333,
		power: 10294.8161833717,
		road: 16235.5688888889,
		acceleration: 1.6262962962963
	},
	{
		id: 1808,
		time: 1807,
		velocity: 8.39916666666667,
		power: 11321.1065239036,
		road: 16243.1838425926,
		acceleration: 1.40212962962963
	},
	{
		id: 1809,
		time: 1808,
		velocity: 9.59305555555556,
		power: 7799.73467062725,
		road: 16251.8855092593,
		acceleration: 0.771296296296297
	},
	{
		id: 1810,
		time: 1809,
		velocity: 9.26972222222222,
		power: 4380.89884232339,
		road: 16261.1337037037,
		acceleration: 0.321759259259258
	},
	{
		id: 1811,
		time: 1810,
		velocity: 9.36444444444444,
		power: 3691.64490309644,
		road: 16270.6569907407,
		acceleration: 0.228425925925928
	},
	{
		id: 1812,
		time: 1811,
		velocity: 10.2783333333333,
		power: 7587.16447152784,
		road: 16280.6031944444,
		acceleration: 0.617407407407407
	},
	{
		id: 1813,
		time: 1812,
		velocity: 11.1219444444444,
		power: 14163.5950436394,
		road: 16291.4462962963,
		acceleration: 1.17638888888889
	},
	{
		id: 1814,
		time: 1813,
		velocity: 12.8936111111111,
		power: 12489.1428433227,
		road: 16303.3251851852,
		acceleration: 0.895185185185184
	},
	{
		id: 1815,
		time: 1814,
		velocity: 12.9638888888889,
		power: 12375.699280573,
		road: 16316.0517592593,
		acceleration: 0.800185185185185
	},
	{
		id: 1816,
		time: 1815,
		velocity: 13.5225,
		power: 9417.17997866961,
		road: 16329.4328240741,
		acceleration: 0.508796296296294
	},
	{
		id: 1817,
		time: 1816,
		velocity: 14.42,
		power: 15150.7494004169,
		road: 16343.512037037,
		acceleration: 0.887500000000003
	},
	{
		id: 1818,
		time: 1817,
		velocity: 15.6263888888889,
		power: 20865.2375021493,
		road: 16358.6289814815,
		acceleration: 1.18796296296296
	},
	{
		id: 1819,
		time: 1818,
		velocity: 17.0863888888889,
		power: 23636.2575366402,
		road: 16374.9578240741,
		acceleration: 1.23583333333333
	},
	{
		id: 1820,
		time: 1819,
		velocity: 18.1275,
		power: 25008.2264682544,
		road: 16392.4990277778,
		acceleration: 1.18888888888889
	},
	{
		id: 1821,
		time: 1820,
		velocity: 19.1930555555556,
		power: 18590.8258927432,
		road: 16410.9993055556,
		acceleration: 0.729259259259255
	},
	{
		id: 1822,
		time: 1821,
		velocity: 19.2741666666667,
		power: 20873.0748223349,
		road: 16430.2616203704,
		acceleration: 0.794814814814821
	},
	{
		id: 1823,
		time: 1822,
		velocity: 20.5119444444444,
		power: 14812.1286120885,
		road: 16450.1353240741,
		acceleration: 0.427962962962962
	},
	{
		id: 1824,
		time: 1823,
		velocity: 20.4769444444444,
		power: 11309.65779306,
		road: 16470.3364814815,
		acceleration: 0.226944444444442
	},
	{
		id: 1825,
		time: 1824,
		velocity: 19.955,
		power: -475.553425644784,
		road: 16490.4610185185,
		acceleration: -0.380185185185187
	},
	{
		id: 1826,
		time: 1825,
		velocity: 19.3713888888889,
		power: -3749.3688620111,
		road: 16510.1233333333,
		acceleration: -0.544259259259256
	},
	{
		id: 1827,
		time: 1826,
		velocity: 18.8441666666667,
		power: -4766.83955033276,
		road: 16529.2167592593,
		acceleration: -0.593518518518518
	},
	{
		id: 1828,
		time: 1827,
		velocity: 18.1744444444444,
		power: -7237.75068104868,
		road: 16547.6488888889,
		acceleration: -0.729074074074074
	},
	{
		id: 1829,
		time: 1828,
		velocity: 17.1841666666667,
		power: -15888.3825073054,
		road: 16565.0909259259,
		acceleration: -1.25111111111111
	},
	{
		id: 1830,
		time: 1829,
		velocity: 15.0908333333333,
		power: -20204.5456767903,
		road: 16581.1116203704,
		acceleration: -1.59157407407407
	},
	{
		id: 1831,
		time: 1830,
		velocity: 13.3997222222222,
		power: -24967.6303741959,
		road: 16595.2941666667,
		acceleration: -2.08472222222222
	},
	{
		id: 1832,
		time: 1831,
		velocity: 10.93,
		power: -19268.8290196492,
		road: 16607.5010648148,
		acceleration: -1.86657407407407
	},
	{
		id: 1833,
		time: 1832,
		velocity: 9.49111111111111,
		power: -13700.5205624641,
		road: 16617.9948148148,
		acceleration: -1.55972222222222
	},
	{
		id: 1834,
		time: 1833,
		velocity: 8.72055555555555,
		power: -5832.9050411164,
		road: 16627.2912037037,
		acceleration: -0.835000000000001
	},
	{
		id: 1835,
		time: 1834,
		velocity: 8.425,
		power: 34.7541223973717,
		road: 16636.0866666667,
		acceleration: -0.166851851851851
	},
	{
		id: 1836,
		time: 1835,
		velocity: 8.99055555555556,
		power: 5529.30112143475,
		road: 16645.0368055556,
		acceleration: 0.476203703703703
	},
	{
		id: 1837,
		time: 1836,
		velocity: 10.1491666666667,
		power: 8642.94160861554,
		road: 16654.6093981481,
		acceleration: 0.768703703703704
	},
	{
		id: 1838,
		time: 1837,
		velocity: 10.7311111111111,
		power: 9389.75520130438,
		road: 16664.9489351852,
		acceleration: 0.765185185185185
	},
	{
		id: 1839,
		time: 1838,
		velocity: 11.2861111111111,
		power: 7893.06506098715,
		road: 16675.949537037,
		acceleration: 0.556944444444445
	},
	{
		id: 1840,
		time: 1839,
		velocity: 11.82,
		power: 10797.1911464328,
		road: 16687.6118981481,
		acceleration: 0.766574074074073
	},
	{
		id: 1841,
		time: 1840,
		velocity: 13.0308333333333,
		power: 22845.5659273263,
		road: 16700.4773148148,
		acceleration: 1.63953703703704
	},
	{
		id: 1842,
		time: 1841,
		velocity: 16.2047222222222,
		power: 33500.8646220922,
		road: 16715.2259259259,
		acceleration: 2.12685185185185
	},
	{
		id: 1843,
		time: 1842,
		velocity: 18.2005555555556,
		power: 40162.9708332516,
		road: 16732.1349074074,
		acceleration: 2.19388888888889
	},
	{
		id: 1844,
		time: 1843,
		velocity: 19.6125,
		power: 26421.7937912967,
		road: 16750.722037037,
		acceleration: 1.16240740740741
	},
	{
		id: 1845,
		time: 1844,
		velocity: 19.6919444444444,
		power: 19948.6458145973,
		road: 16770.2523148148,
		acceleration: 0.723888888888887
	},
	{
		id: 1846,
		time: 1845,
		velocity: 20.3722222222222,
		power: 13611.7342158212,
		road: 16790.3212962963,
		acceleration: 0.353518518518523
	},
	{
		id: 1847,
		time: 1846,
		velocity: 20.6730555555556,
		power: 15325.1444098522,
		road: 16810.7763425926,
		acceleration: 0.418611111111108
	},
	{
		id: 1848,
		time: 1847,
		velocity: 20.9477777777778,
		power: 12140.9847018823,
		road: 16831.5600462963,
		acceleration: 0.238703703703706
	},
	{
		id: 1849,
		time: 1848,
		velocity: 21.0883333333333,
		power: 11680.8698837666,
		road: 16852.5651851852,
		acceleration: 0.204166666666666
	},
	{
		id: 1850,
		time: 1849,
		velocity: 21.2855555555556,
		power: 14624.580492357,
		road: 16873.8397222222,
		acceleration: 0.334629629629628
	},
	{
		id: 1851,
		time: 1850,
		velocity: 21.9516666666667,
		power: 18517.5183434963,
		road: 16895.530462963,
		acceleration: 0.497777777777781
	},
	{
		id: 1852,
		time: 1851,
		velocity: 22.5816666666667,
		power: 27335.8717903182,
		road: 16917.9022222222,
		acceleration: 0.864259259259256
	},
	{
		id: 1853,
		time: 1852,
		velocity: 23.8783333333333,
		power: 28135.4305166826,
		road: 16941.1218055555,
		acceleration: 0.831388888888888
	},
	{
		id: 1854,
		time: 1853,
		velocity: 24.4458333333333,
		power: 28690.8210414919,
		road: 16965.1525,
		acceleration: 0.790833333333335
	},
	{
		id: 1855,
		time: 1854,
		velocity: 24.9541666666667,
		power: 16142.2574819227,
		road: 16989.6880555555,
		acceleration: 0.218888888888888
	},
	{
		id: 1856,
		time: 1855,
		velocity: 24.535,
		power: 7805.68491519882,
		road: 17014.2648148148,
		acceleration: -0.136481481481482
	},
	{
		id: 1857,
		time: 1856,
		velocity: 24.0363888888889,
		power: -2674.91796532856,
		road: 17038.4871296296,
		acceleration: -0.572407407407404
	},
	{
		id: 1858,
		time: 1857,
		velocity: 23.2369444444444,
		power: -1331.71329589116,
		road: 17062.1724537037,
		acceleration: -0.501574074074071
	},
	{
		id: 1859,
		time: 1858,
		velocity: 23.0302777777778,
		power: -3414.30031560501,
		road: 17085.3157407407,
		acceleration: -0.582500000000007
	},
	{
		id: 1860,
		time: 1859,
		velocity: 22.2888888888889,
		power: -479.400133908758,
		road: 17107.9488888889,
		acceleration: -0.437777777777779
	},
	{
		id: 1861,
		time: 1860,
		velocity: 21.9236111111111,
		power: -3751.87164700175,
		road: 17130.073287037,
		acceleration: -0.57972222222222
	},
	{
		id: 1862,
		time: 1861,
		velocity: 21.2911111111111,
		power: -2453.26767618574,
		road: 17151.65375,
		acceleration: -0.508148148148148
	},
	{
		id: 1863,
		time: 1862,
		velocity: 20.7644444444444,
		power: -5562.21884182243,
		road: 17172.6541203704,
		acceleration: -0.65203703703704
	},
	{
		id: 1864,
		time: 1863,
		velocity: 19.9675,
		power: -7193.5141743635,
		road: 17192.9637962963,
		acceleration: -0.729351851851849
	},
	{
		id: 1865,
		time: 1864,
		velocity: 19.1030555555556,
		power: -16879.06754546,
		road: 17212.2837962963,
		acceleration: -1.25
	},
	{
		id: 1866,
		time: 1865,
		velocity: 17.0144444444444,
		power: -23057.6719265382,
		road: 17230.1507407407,
		acceleration: -1.65611111111111
	},
	{
		id: 1867,
		time: 1866,
		velocity: 14.9991666666667,
		power: -22595.5665568711,
		road: 17246.3212962963,
		acceleration: -1.73666666666667
	},
	{
		id: 1868,
		time: 1867,
		velocity: 13.8930555555556,
		power: -11840.2213667207,
		road: 17261.0784259259,
		acceleration: -1.09018518518518
	},
	{
		id: 1869,
		time: 1868,
		velocity: 13.7438888888889,
		power: -4061.48411778713,
		road: 17275.0193981481,
		acceleration: -0.542129629629629
	},
	{
		id: 1870,
		time: 1869,
		velocity: 13.3727777777778,
		power: 3382.52458334075,
		road: 17288.7022685185,
		acceleration: 0.0259259259259252
	},
	{
		id: 1871,
		time: 1870,
		velocity: 13.9708333333333,
		power: 9708.09048806752,
		road: 17302.6441666667,
		acceleration: 0.49212962962963
	},
	{
		id: 1872,
		time: 1871,
		velocity: 15.2202777777778,
		power: 20190.6856598549,
		road: 17317.4218055555,
		acceleration: 1.17935185185185
	},
	{
		id: 1873,
		time: 1872,
		velocity: 16.9108333333333,
		power: 22562.160997838,
		road: 17333.3922685185,
		acceleration: 1.2062962962963
	},
	{
		id: 1874,
		time: 1873,
		velocity: 17.5897222222222,
		power: 17296.9785260924,
		road: 17350.3540740741,
		acceleration: 0.776388888888889
	},
	{
		id: 1875,
		time: 1874,
		velocity: 17.5494444444444,
		power: 9495.36885468569,
		road: 17367.8380092592,
		acceleration: 0.267870370370371
	},
	{
		id: 1876,
		time: 1875,
		velocity: 17.7144444444444,
		power: 7834.90419653916,
		road: 17385.5352314815,
		acceleration: 0.158703703703704
	},
	{
		id: 1877,
		time: 1876,
		velocity: 18.0658333333333,
		power: 8303.55247028632,
		road: 17403.4009722222,
		acceleration: 0.178333333333331
	},
	{
		id: 1878,
		time: 1877,
		velocity: 18.0844444444444,
		power: 7466.93754453357,
		road: 17421.4172222222,
		acceleration: 0.122685185185187
	},
	{
		id: 1879,
		time: 1878,
		velocity: 18.0825,
		power: 5255.77638649601,
		road: 17439.4909722222,
		acceleration: -0.00768518518518491
	},
	{
		id: 1880,
		time: 1879,
		velocity: 18.0427777777778,
		power: 4333.90392639936,
		road: 17457.5309722222,
		acceleration: -0.0598148148148177
	},
	{
		id: 1881,
		time: 1880,
		velocity: 17.905,
		power: 3629.90100423306,
		road: 17475.492037037,
		acceleration: -0.098055555555554
	},
	{
		id: 1882,
		time: 1881,
		velocity: 17.7883333333333,
		power: 1665.15737394981,
		road: 17493.2998611111,
		acceleration: -0.208425925925923
	},
	{
		id: 1883,
		time: 1882,
		velocity: 17.4175,
		power: 1170.07550390245,
		road: 17510.8873611111,
		acceleration: -0.232222222222227
	},
	{
		id: 1884,
		time: 1883,
		velocity: 17.2083333333333,
		power: 665.258163175797,
		road: 17528.2303240741,
		acceleration: -0.256851851851849
	},
	{
		id: 1885,
		time: 1884,
		velocity: 17.0177777777778,
		power: 2247.66137200608,
		road: 17545.3669444444,
		acceleration: -0.155833333333334
	},
	{
		id: 1886,
		time: 1885,
		velocity: 16.95,
		power: 2996.69196874367,
		road: 17562.3725462963,
		acceleration: -0.106203703703702
	},
	{
		id: 1887,
		time: 1886,
		velocity: 16.8897222222222,
		power: 3373.76508078817,
		road: 17579.285,
		acceleration: -0.0800925925925924
	},
	{
		id: 1888,
		time: 1887,
		velocity: 16.7775,
		power: 3207.50794122193,
		road: 17596.1135185185,
		acceleration: -0.0877777777777773
	},
	{
		id: 1889,
		time: 1888,
		velocity: 16.6866666666667,
		power: 2839.11241277967,
		road: 17612.8442592593,
		acceleration: -0.10777777777778
	},
	{
		id: 1890,
		time: 1889,
		velocity: 16.5663888888889,
		power: 2606.33050129311,
		road: 17629.4615740741,
		acceleration: -0.119074074074074
	},
	{
		id: 1891,
		time: 1890,
		velocity: 16.4202777777778,
		power: 2034.40608009357,
		road: 17645.9436111111,
		acceleration: -0.151481481481479
	},
	{
		id: 1892,
		time: 1891,
		velocity: 16.2322222222222,
		power: 2717.04250665552,
		road: 17662.2976851852,
		acceleration: -0.104444444444443
	},
	{
		id: 1893,
		time: 1892,
		velocity: 16.2530555555556,
		power: 3352.23932339097,
		road: 17678.5689351852,
		acceleration: -0.0612037037037041
	},
	{
		id: 1894,
		time: 1893,
		velocity: 16.2366666666667,
		power: 2969.63071681199,
		road: 17694.7677777778,
		acceleration: -0.0836111111111144
	},
	{
		id: 1895,
		time: 1894,
		velocity: 15.9813888888889,
		power: 4343.69255851331,
		road: 17710.9280555555,
		acceleration: 0.00648148148148309
	},
	{
		id: 1896,
		time: 1895,
		velocity: 16.2725,
		power: 3612.63039816616,
		road: 17727.0714351852,
		acceleration: -0.0402777777777779
	},
	{
		id: 1897,
		time: 1896,
		velocity: 16.1158333333333,
		power: 4060.71638140976,
		road: 17743.1894907407,
		acceleration: -0.0103703703703708
	},
	{
		id: 1898,
		time: 1897,
		velocity: 15.9502777777778,
		power: 1296.86462339016,
		road: 17759.2086574074,
		acceleration: -0.187407407407408
	},
	{
		id: 1899,
		time: 1898,
		velocity: 15.7102777777778,
		power: 2203.58490305527,
		road: 17775.0721296296,
		acceleration: -0.123981481481481
	},
	{
		id: 1900,
		time: 1899,
		velocity: 15.7438888888889,
		power: 3282.30825908785,
		road: 17790.8485648148,
		acceleration: -0.050092592592593
	},
	{
		id: 1901,
		time: 1900,
		velocity: 15.8,
		power: 4155.13253540291,
		road: 17806.604212963,
		acceleration: 0.00851851851852103
	},
	{
		id: 1902,
		time: 1901,
		velocity: 15.7358333333333,
		power: 4744.01963376447,
		road: 17822.3874074074,
		acceleration: 0.0465740740740728
	},
	{
		id: 1903,
		time: 1902,
		velocity: 15.8836111111111,
		power: 4164.65321857661,
		road: 17838.1975,
		acceleration: 0.0072222222222198
	},
	{
		id: 1904,
		time: 1903,
		velocity: 15.8216666666667,
		power: 3940.6504889433,
		road: 17854.0074074074,
		acceleration: -0.0075925925925926
	},
	{
		id: 1905,
		time: 1904,
		velocity: 15.7130555555556,
		power: 3466.21392662078,
		road: 17869.7943981481,
		acceleration: -0.0382407407407399
	},
	{
		id: 1906,
		time: 1905,
		velocity: 15.7688888888889,
		power: 3743.75385079051,
		road: 17885.5528240741,
		acceleration: -0.0188888888888883
	},
	{
		id: 1907,
		time: 1906,
		velocity: 15.765,
		power: 3840.44955690241,
		road: 17901.2958333333,
		acceleration: -0.0119444444444454
	},
	{
		id: 1908,
		time: 1907,
		velocity: 15.6772222222222,
		power: 3330.73839367878,
		road: 17917.0104166667,
		acceleration: -0.0449074074074041
	},
	{
		id: 1909,
		time: 1908,
		velocity: 15.6341666666667,
		power: 3310.65720653171,
		road: 17932.6801388889,
		acceleration: -0.0448148148148171
	},
	{
		id: 1910,
		time: 1909,
		velocity: 15.6305555555556,
		power: 3082.88618565976,
		road: 17948.2982407407,
		acceleration: -0.0584259259259259
	},
	{
		id: 1911,
		time: 1910,
		velocity: 15.5019444444444,
		power: 3403.02508469191,
		road: 17963.8693981481,
		acceleration: -0.0354629629629617
	},
	{
		id: 1912,
		time: 1911,
		velocity: 15.5277777777778,
		power: 3002.09529983271,
		road: 17979.3923611111,
		acceleration: -0.0609259259259272
	},
	{
		id: 1913,
		time: 1912,
		velocity: 15.4477777777778,
		power: 3253.8992583638,
		road: 17994.8637037037,
		acceleration: -0.042314814814814
	},
	{
		id: 1914,
		time: 1913,
		velocity: 15.375,
		power: 1689.99952755625,
		road: 18010.2410185185,
		acceleration: -0.145740740740742
	},
	{
		id: 1915,
		time: 1914,
		velocity: 15.0905555555556,
		power: 961.616639738095,
		road: 18025.4496296296,
		acceleration: -0.191666666666666
	},
	{
		id: 1916,
		time: 1915,
		velocity: 14.8727777777778,
		power: -148.771068163768,
		road: 18040.4302314815,
		acceleration: -0.264351851851853
	},
	{
		id: 1917,
		time: 1916,
		velocity: 14.5819444444444,
		power: -923.307187689269,
		road: 18055.1212037037,
		acceleration: -0.314907407407407
	},
	{
		id: 1918,
		time: 1917,
		velocity: 14.1458333333333,
		power: -635.088995690909,
		road: 18069.509537037,
		acceleration: -0.290370370370372
	},
	{
		id: 1919,
		time: 1918,
		velocity: 14.0016666666667,
		power: -423.879695259377,
		road: 18083.6171296296,
		acceleration: -0.271111111111111
	},
	{
		id: 1920,
		time: 1919,
		velocity: 13.7686111111111,
		power: 1854.891340092,
		road: 18097.5405555555,
		acceleration: -0.0972222222222214
	},
	{
		id: 1921,
		time: 1920,
		velocity: 13.8541666666667,
		power: 3318.47679220838,
		road: 18111.4225,
		acceleration: 0.0142592592592603
	},
	{
		id: 1922,
		time: 1921,
		velocity: 14.0444444444444,
		power: 5002.90155309924,
		road: 18125.3806018518,
		acceleration: 0.138055555555555
	},
	{
		id: 1923,
		time: 1922,
		velocity: 14.1827777777778,
		power: 5730.07092079349,
		road: 18139.5002777778,
		acceleration: 0.185092592592595
	},
	{
		id: 1924,
		time: 1923,
		velocity: 14.4094444444444,
		power: 6101.47325202049,
		road: 18153.8141666667,
		acceleration: 0.203333333333333
	},
	{
		id: 1925,
		time: 1924,
		velocity: 14.6544444444444,
		power: 2709.22454552162,
		road: 18168.2061574074,
		acceleration: -0.0471296296296302
	},
	{
		id: 1926,
		time: 1925,
		velocity: 14.0413888888889,
		power: 1882.82975317497,
		road: 18182.5219444444,
		acceleration: -0.105277777777779
	},
	{
		id: 1927,
		time: 1926,
		velocity: 14.0936111111111,
		power: 995.023561645525,
		road: 18196.7014351852,
		acceleration: -0.167314814814814
	},
	{
		id: 1928,
		time: 1927,
		velocity: 14.1525,
		power: 3826.25551788007,
		road: 18210.8192592592,
		acceleration: 0.043981481481481
	},
	{
		id: 1929,
		time: 1928,
		velocity: 14.1733333333333,
		power: 1880.25779649769,
		road: 18224.9092592592,
		acceleration: -0.0996296296296286
	},
	{
		id: 1930,
		time: 1929,
		velocity: 13.7947222222222,
		power: 1013.19058062776,
		road: 18238.8687962963,
		acceleration: -0.161296296296296
	},
	{
		id: 1931,
		time: 1930,
		velocity: 13.6686111111111,
		power: -45.9430407369371,
		road: 18252.6288425926,
		acceleration: -0.237685185185185
	},
	{
		id: 1932,
		time: 1931,
		velocity: 13.4602777777778,
		power: -3.21302241121721,
		road: 18266.1546296296,
		acceleration: -0.230833333333333
	},
	{
		id: 1933,
		time: 1932,
		velocity: 13.1022222222222,
		power: 418.37251704598,
		road: 18279.4677777778,
		acceleration: -0.194444444444446
	},
	{
		id: 1934,
		time: 1933,
		velocity: 13.0852777777778,
		power: 697.792573788094,
		road: 18292.5992129629,
		acceleration: -0.168981481481481
	},
	{
		id: 1935,
		time: 1934,
		velocity: 12.9533333333333,
		power: 2411.02802345648,
		road: 18305.6314814815,
		acceleration: -0.0293518518518514
	},
	{
		id: 1936,
		time: 1935,
		velocity: 13.0141666666667,
		power: 2799.51550903925,
		road: 18318.6502314815,
		acceleration: 0.00231481481481488
	},
	{
		id: 1937,
		time: 1936,
		velocity: 13.0922222222222,
		power: 3671.46545573941,
		road: 18331.7056944444,
		acceleration: 0.0711111111111098
	},
	{
		id: 1938,
		time: 1937,
		velocity: 13.1666666666667,
		power: 3741.40947899326,
		road: 18344.8337037037,
		acceleration: 0.0739814814814821
	},
	{
		id: 1939,
		time: 1938,
		velocity: 13.2361111111111,
		power: 3531.7613173821,
		road: 18358.0261574074,
		acceleration: 0.0549074074074074
	},
	{
		id: 1940,
		time: 1939,
		velocity: 13.2569444444444,
		power: 3291.27930240681,
		road: 18371.2631944444,
		acceleration: 0.0342592592592617
	},
	{
		id: 1941,
		time: 1940,
		velocity: 13.2694444444444,
		power: 2780.86804458041,
		road: 18384.5140740741,
		acceleration: -0.00657407407407717
	},
	{
		id: 1942,
		time: 1941,
		velocity: 13.2163888888889,
		power: 2478.84314151194,
		road: 18397.746712963,
		acceleration: -0.0299074074074071
	},
	{
		id: 1943,
		time: 1942,
		velocity: 13.1672222222222,
		power: 2416.66365571731,
		road: 18410.9474537037,
		acceleration: -0.0338888888888889
	},
	{
		id: 1944,
		time: 1943,
		velocity: 13.1677777777778,
		power: 2058.4518415177,
		road: 18424.1007407407,
		acceleration: -0.0610185185185177
	},
	{
		id: 1945,
		time: 1944,
		velocity: 13.0333333333333,
		power: 2111.45242770468,
		road: 18437.1959259259,
		acceleration: -0.0551851851851861
	},
	{
		id: 1946,
		time: 1945,
		velocity: 13.0016666666667,
		power: 2805.44036645979,
		road: 18450.2641203704,
		acceleration: 0.00120370370370537
	},
	{
		id: 1947,
		time: 1946,
		velocity: 13.1713888888889,
		power: 4078.23554067952,
		road: 18463.3835185185,
		acceleration: 0.101203703703703
	},
	{
		id: 1948,
		time: 1947,
		velocity: 13.3369444444444,
		power: 3944.27238424408,
		road: 18476.5969444444,
		acceleration: 0.0868518518518506
	},
	{
		id: 1949,
		time: 1948,
		velocity: 13.2622222222222,
		power: 3422.1618081753,
		road: 18489.8753703704,
		acceleration: 0.0431481481481484
	},
	{
		id: 1950,
		time: 1949,
		velocity: 13.3008333333333,
		power: 3246.36963176078,
		road: 18503.1893981481,
		acceleration: 0.0280555555555573
	},
	{
		id: 1951,
		time: 1950,
		velocity: 13.4211111111111,
		power: 3713.87487795491,
		road: 18516.5490277778,
		acceleration: 0.063148148148148
	},
	{
		id: 1952,
		time: 1951,
		velocity: 13.4516666666667,
		power: 3451.56956545488,
		road: 18529.9606018518,
		acceleration: 0.0407407407407412
	},
	{
		id: 1953,
		time: 1952,
		velocity: 13.4230555555556,
		power: 2649.06574008747,
		road: 18543.3814351852,
		acceleration: -0.0222222222222239
	},
	{
		id: 1954,
		time: 1953,
		velocity: 13.3544444444444,
		power: 3511.92383200475,
		road: 18556.8135185185,
		acceleration: 0.0447222222222248
	},
	{
		id: 1955,
		time: 1954,
		velocity: 13.5858333333333,
		power: 3737.48487398278,
		road: 18570.2981481481,
		acceleration: 0.060370370370368
	},
	{
		id: 1956,
		time: 1955,
		velocity: 13.6041666666667,
		power: 4028.63266634033,
		road: 18583.8531018518,
		acceleration: 0.0802777777777788
	},
	{
		id: 1957,
		time: 1956,
		velocity: 13.5952777777778,
		power: 2900.39204591237,
		road: 18597.4441666667,
		acceleration: -0.00805555555555593
	},
	{
		id: 1958,
		time: 1957,
		velocity: 13.5616666666667,
		power: 1819.50714667129,
		road: 18610.9861574074,
		acceleration: -0.0900925925925922
	},
	{
		id: 1959,
		time: 1958,
		velocity: 13.3338888888889,
		power: 1894.50604119796,
		road: 18624.4420833333,
		acceleration: -0.082037037037038
	},
	{
		id: 1960,
		time: 1959,
		velocity: 13.3491666666667,
		power: 1114.23711492626,
		road: 18637.7868055555,
		acceleration: -0.140370370370372
	},
	{
		id: 1961,
		time: 1960,
		velocity: 13.1405555555556,
		power: 1730.66212088748,
		road: 18651.0168055555,
		acceleration: -0.0890740740740714
	},
	{
		id: 1962,
		time: 1961,
		velocity: 13.0666666666667,
		power: 1095.42968068738,
		road: 18664.1337962963,
		acceleration: -0.136944444444444
	},
	{
		id: 1963,
		time: 1962,
		velocity: 12.9383333333333,
		power: 1560.50823205508,
		road: 18677.1338425926,
		acceleration: -0.0969444444444463
	},
	{
		id: 1964,
		time: 1963,
		velocity: 12.8497222222222,
		power: 1605.22139513394,
		road: 18690.0399074074,
		acceleration: -0.0910185185185171
	},
	{
		id: 1965,
		time: 1964,
		velocity: 12.7936111111111,
		power: 1555.89381919044,
		road: 18702.8540740741,
		acceleration: -0.0927777777777781
	},
	{
		id: 1966,
		time: 1965,
		velocity: 12.66,
		power: 1868.86777490269,
		road: 18715.5893055556,
		acceleration: -0.0650925925925954
	},
	{
		id: 1967,
		time: 1966,
		velocity: 12.6544444444444,
		power: 1528.43967494815,
		road: 18728.2463888889,
		acceleration: -0.0912037037037017
	},
	{
		id: 1968,
		time: 1967,
		velocity: 12.52,
		power: 2039.83494617756,
		road: 18740.8343981481,
		acceleration: -0.0469444444444456
	},
	{
		id: 1969,
		time: 1968,
		velocity: 12.5191666666667,
		power: 1431.12948141653,
		road: 18753.3509722222,
		acceleration: -0.0959259259259255
	},
	{
		id: 1970,
		time: 1969,
		velocity: 12.3666666666667,
		power: 1799.85915680274,
		road: 18765.7881018518,
		acceleration: -0.0629629629629598
	},
	{
		id: 1971,
		time: 1970,
		velocity: 12.3311111111111,
		power: 1206.62875568602,
		road: 18778.1382407407,
		acceleration: -0.11101851851852
	},
	{
		id: 1972,
		time: 1971,
		velocity: 12.1861111111111,
		power: 1523.54764191802,
		road: 18790.3919907407,
		acceleration: -0.0817592592592611
	},
	{
		id: 1973,
		time: 1972,
		velocity: 12.1213888888889,
		power: 1532.88224587878,
		road: 18802.5653703704,
		acceleration: -0.0789814814814811
	},
	{
		id: 1974,
		time: 1973,
		velocity: 12.0941666666667,
		power: 1686.76438509075,
		road: 18814.6673148148,
		acceleration: -0.0638888888888882
	},
	{
		id: 1975,
		time: 1974,
		velocity: 11.9944444444444,
		power: 2175.95943592818,
		road: 18826.7271759259,
		acceleration: -0.0202777777777783
	},
	{
		id: 1976,
		time: 1975,
		velocity: 12.0605555555556,
		power: 2230.62623125362,
		road: 18838.7693981481,
		acceleration: -0.0150000000000006
	},
	{
		id: 1977,
		time: 1976,
		velocity: 12.0491666666667,
		power: 2561.19059599666,
		road: 18850.8110185185,
		acceleration: 0.013796296296297
	},
	{
		id: 1978,
		time: 1977,
		velocity: 12.0358333333333,
		power: 2265.83768379255,
		road: 18862.8535648148,
		acceleration: -0.0119444444444454
	},
	{
		id: 1979,
		time: 1978,
		velocity: 12.0247222222222,
		power: 2686.23299022951,
		road: 18874.9023611111,
		acceleration: 0.0244444444444447
	},
	{
		id: 1980,
		time: 1979,
		velocity: 12.1225,
		power: 2554.69395719231,
		road: 18886.9695833333,
		acceleration: 0.012407407407407
	},
	{
		id: 1981,
		time: 1980,
		velocity: 12.0730555555556,
		power: 3349.63456880272,
		road: 18899.0828703704,
		acceleration: 0.0797222222222231
	},
	{
		id: 1982,
		time: 1981,
		velocity: 12.2638888888889,
		power: 3177.28730222916,
		road: 18911.2671296296,
		acceleration: 0.0622222222222231
	},
	{
		id: 1983,
		time: 1982,
		velocity: 12.3091666666667,
		power: 3289.58575893351,
		road: 18923.5172222222,
		acceleration: 0.0694444444444429
	},
	{
		id: 1984,
		time: 1983,
		velocity: 12.2813888888889,
		power: 1943.94946362724,
		road: 18935.7790277778,
		acceleration: -0.0460185185185189
	},
	{
		id: 1985,
		time: 1984,
		velocity: 12.1258333333333,
		power: 2619.97742001924,
		road: 18948.0239814815,
		acceleration: 0.0123148148148147
	},
	{
		id: 1986,
		time: 1985,
		velocity: 12.3461111111111,
		power: 4123.09077098948,
		road: 18960.3439814815,
		acceleration: 0.137777777777778
	},
	{
		id: 1987,
		time: 1986,
		velocity: 12.6947222222222,
		power: 8316.25115617798,
		road: 18972.9693518518,
		acceleration: 0.472962962962965
	},
	{
		id: 1988,
		time: 1987,
		velocity: 13.5447222222222,
		power: 9676.44257999354,
		road: 18986.1047222222,
		acceleration: 0.547037037037036
	},
	{
		id: 1989,
		time: 1988,
		velocity: 13.9872222222222,
		power: 68.2928644245846,
		road: 18999.4027314815,
		acceleration: -0.22175925925926
	},
	{
		id: 1990,
		time: 1989,
		velocity: 12.0294444444444,
		power: -11302.5353480977,
		road: 19012.0115277778,
		acceleration: -1.15666666666667
	},
	{
		id: 1991,
		time: 1990,
		velocity: 10.0747222222222,
		power: -20609.3838535513,
		road: 19022.9565740741,
		acceleration: -2.17083333333333
	},
	{
		id: 1992,
		time: 1991,
		velocity: 7.47472222222222,
		power: -17283.223563613,
		road: 19031.6915740741,
		acceleration: -2.24925925925926
	},
	{
		id: 1993,
		time: 1992,
		velocity: 5.28166666666667,
		power: -13624.8679820034,
		road: 19038.11,
		acceleration: -2.38388888888889
	},
	{
		id: 1994,
		time: 1993,
		velocity: 2.92305555555556,
		power: -8899.08200077246,
		road: 19042.0906944444,
		acceleration: -2.49157407407407
	},
	{
		id: 1995,
		time: 1994,
		velocity: 0,
		power: -2868.29871093281,
		road: 19043.9453240741,
		acceleration: -1.76055555555556
	},
	{
		id: 1996,
		time: 1995,
		velocity: 0,
		power: -390.837997677063,
		road: 19044.4325,
		acceleration: -0.974351851851852
	},
	{
		id: 1997,
		time: 1996,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 1998,
		time: 1997,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 1999,
		time: 1998,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2000,
		time: 1999,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2001,
		time: 2000,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2002,
		time: 2001,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2003,
		time: 2002,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2004,
		time: 2003,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2005,
		time: 2004,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2006,
		time: 2005,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2007,
		time: 2006,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2008,
		time: 2007,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2009,
		time: 2008,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2010,
		time: 2009,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2011,
		time: 2010,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2012,
		time: 2011,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2013,
		time: 2012,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2014,
		time: 2013,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2015,
		time: 2014,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2016,
		time: 2015,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2017,
		time: 2016,
		velocity: 0,
		power: 0,
		road: 19044.4325,
		acceleration: 0
	},
	{
		id: 2018,
		time: 2017,
		velocity: 0,
		power: 55.7896686923891,
		road: 19044.5751388889,
		acceleration: 0.285277777777778
	},
	{
		id: 2019,
		time: 2018,
		velocity: 0.855833333333333,
		power: 434.513143189529,
		road: 19045.1772685185,
		acceleration: 0.633703703703704
	},
	{
		id: 2020,
		time: 2019,
		velocity: 1.90111111111111,
		power: 2434.62601158794,
		road: 19046.8153240741,
		acceleration: 1.43814814814815
	},
	{
		id: 2021,
		time: 2020,
		velocity: 4.31444444444444,
		power: 5053.43297339615,
		road: 19049.9540277778,
		acceleration: 1.56314814814815
	},
	{
		id: 2022,
		time: 2021,
		velocity: 5.54527777777778,
		power: 5865.83510528601,
		road: 19054.4860648148,
		acceleration: 1.22351851851852
	},
	{
		id: 2023,
		time: 2022,
		velocity: 5.57166666666667,
		power: 4300.9144330207,
		road: 19059.9702777778,
		acceleration: 0.680833333333332
	},
	{
		id: 2024,
		time: 2023,
		velocity: 6.35694444444444,
		power: 3928.57093170939,
		road: 19066.0600462963,
		acceleration: 0.530277777777778
	},
	{
		id: 2025,
		time: 2024,
		velocity: 7.13611111111111,
		power: 5459.08909893188,
		road: 19072.7665277778,
		acceleration: 0.703148148148148
	},
	{
		id: 2026,
		time: 2025,
		velocity: 7.68111111111111,
		power: 5273.55553038702,
		road: 19080.1224074074,
		acceleration: 0.595648148148148
	},
	{
		id: 2027,
		time: 2026,
		velocity: 8.14388888888889,
		power: 3645.28395030625,
		road: 19087.9401851852,
		acceleration: 0.328148148148149
	},
	{
		id: 2028,
		time: 2027,
		velocity: 8.12055555555556,
		power: 2164.42592429172,
		road: 19095.9815277778,
		acceleration: 0.118981481481483
	},
	{
		id: 2029,
		time: 2028,
		velocity: 8.03805555555556,
		power: 315.411259053378,
		road: 19104.0210648148,
		acceleration: -0.122592592592594
	},
	{
		id: 2030,
		time: 2029,
		velocity: 7.77611111111111,
		power: -949.513777733081,
		road: 19111.8546296296,
		acceleration: -0.289351851851851
	},
	{
		id: 2031,
		time: 2030,
		velocity: 7.2525,
		power: -1131.76743068647,
		road: 19119.3848611111,
		acceleration: -0.317314814814814
	},
	{
		id: 2032,
		time: 2031,
		velocity: 7.08611111111111,
		power: 238.195009186375,
		road: 19126.6948148148,
		acceleration: -0.123240740740741
	},
	{
		id: 2033,
		time: 2032,
		velocity: 7.40638888888889,
		power: 1762.83225196963,
		road: 19133.9914351852,
		acceleration: 0.0965740740740735
	},
	{
		id: 2034,
		time: 2033,
		velocity: 7.54222222222222,
		power: 3393.85924370167,
		road: 19141.4944907407,
		acceleration: 0.316296296296295
	},
	{
		id: 2035,
		time: 2034,
		velocity: 8.035,
		power: 3550.08166999791,
		road: 19149.3133333333,
		acceleration: 0.315277777777778
	},
	{
		id: 2036,
		time: 2035,
		velocity: 8.35222222222222,
		power: 4305.70271011783,
		road: 19157.4841203704,
		acceleration: 0.388611111111111
	},
	{
		id: 2037,
		time: 2036,
		velocity: 8.70805555555556,
		power: 2979.86170992512,
		road: 19165.9501851852,
		acceleration: 0.201944444444445
	},
	{
		id: 2038,
		time: 2037,
		velocity: 8.64083333333333,
		power: 2495.47145041952,
		road: 19174.5843055555,
		acceleration: 0.134166666666665
	},
	{
		id: 2039,
		time: 2038,
		velocity: 8.75472222222222,
		power: 3341.56517080032,
		road: 19183.3989814815,
		acceleration: 0.226944444444445
	},
	{
		id: 2040,
		time: 2039,
		velocity: 9.38888888888889,
		power: 6519.88290719695,
		road: 19192.6110648148,
		acceleration: 0.56787037037037
	},
	{
		id: 2041,
		time: 2040,
		velocity: 10.3444444444444,
		power: 8095.984768048,
		road: 19202.4480092592,
		acceleration: 0.681851851851851
	},
	{
		id: 2042,
		time: 2041,
		velocity: 10.8002777777778,
		power: 5609.61232894574,
		road: 19212.8157407407,
		acceleration: 0.379722222222224
	},
	{
		id: 2043,
		time: 2042,
		velocity: 10.5280555555556,
		power: 2035.5028127929,
		road: 19223.3792592592,
		acceleration: 0.0118518518518513
	},
	{
		id: 2044,
		time: 2043,
		velocity: 10.38,
		power: 1443.73682534641,
		road: 19233.925462963,
		acceleration: -0.0464814814814822
	},
	{
		id: 2045,
		time: 2044,
		velocity: 10.6608333333333,
		power: 3909.36740320512,
		road: 19244.5460185185,
		acceleration: 0.195185185185185
	},
	{
		id: 2046,
		time: 2045,
		velocity: 11.1136111111111,
		power: 7450.98133530291,
		road: 19255.5225,
		acceleration: 0.516666666666669
	},
	{
		id: 2047,
		time: 2046,
		velocity: 11.93,
		power: 8151.04162714084,
		road: 19267.0277314815,
		acceleration: 0.540833333333332
	},
	{
		id: 2048,
		time: 2047,
		velocity: 12.2833333333333,
		power: 10567.6849389717,
		road: 19279.1549074074,
		acceleration: 0.703055555555554
	},
	{
		id: 2049,
		time: 2048,
		velocity: 13.2227777777778,
		power: 9185.00026633421,
		road: 19291.9016203704,
		acceleration: 0.536018518518517
	},
	{
		id: 2050,
		time: 2049,
		velocity: 13.5380555555556,
		power: 13607.3491166086,
		road: 19305.3323611111,
		acceleration: 0.83203703703704
	},
	{
		id: 2051,
		time: 2050,
		velocity: 14.7794444444444,
		power: 11955.5235130623,
		road: 19319.5005555555,
		acceleration: 0.642870370370368
	},
	{
		id: 2052,
		time: 2051,
		velocity: 15.1513888888889,
		power: 9272.78909226491,
		road: 19334.1958333333,
		acceleration: 0.411296296296298
	},
	{
		id: 2053,
		time: 2052,
		velocity: 14.7719444444444,
		power: 1867.39421598639,
		road: 19349.0368055555,
		acceleration: -0.119907407407405
	},
	{
		id: 2054,
		time: 2053,
		velocity: 14.4197222222222,
		power: 332.713464006247,
		road: 19363.7053240741,
		acceleration: -0.225000000000001
	},
	{
		id: 2055,
		time: 2054,
		velocity: 14.4763888888889,
		power: 1590.26740474288,
		road: 19378.1958796296,
		acceleration: -0.130925925925927
	},
	{
		id: 2056,
		time: 2055,
		velocity: 14.3791666666667,
		power: 1900.34504255715,
		road: 19392.5682407407,
		acceleration: -0.105462962962962
	},
	{
		id: 2057,
		time: 2056,
		velocity: 14.1033333333333,
		power: -972.100868894893,
		road: 19406.7316666667,
		acceleration: -0.312407407407409
	},
	{
		id: 2058,
		time: 2057,
		velocity: 13.5391666666667,
		power: -1818.80386996267,
		road: 19420.5524074074,
		acceleration: -0.37296296296296
	},
	{
		id: 2059,
		time: 2058,
		velocity: 13.2602777777778,
		power: -2319.95627572033,
		road: 19433.9816203704,
		acceleration: -0.410092592592592
	},
	{
		id: 2060,
		time: 2059,
		velocity: 12.8730555555556,
		power: -1303.08340962235,
		road: 19447.0417129629,
		acceleration: -0.328148148148149
	},
	{
		id: 2061,
		time: 2060,
		velocity: 12.5547222222222,
		power: -1525.38512979493,
		road: 19459.7655555555,
		acceleration: -0.344351851851851
	},
	{
		id: 2062,
		time: 2061,
		velocity: 12.2272222222222,
		power: -1225.59355964403,
		road: 19472.1583796296,
		acceleration: -0.317685185185187
	},
	{
		id: 2063,
		time: 2062,
		velocity: 11.92,
		power: -1191.0126627601,
		road: 19484.2358333333,
		acceleration: -0.313055555555554
	},
	{
		id: 2064,
		time: 2063,
		velocity: 11.6155555555556,
		power: -718.063623345655,
		road: 19496.0219444444,
		acceleration: -0.26962962962963
	},
	{
		id: 2065,
		time: 2064,
		velocity: 11.4183333333333,
		power: 703.516194971482,
		road: 19507.6035648148,
		acceleration: -0.139351851851853
	},
	{
		id: 2066,
		time: 2065,
		velocity: 11.5019444444444,
		power: 3012.07140355973,
		road: 19519.1509722222,
		acceleration: 0.0709259259259269
	},
	{
		id: 2067,
		time: 2066,
		velocity: 11.8283333333333,
		power: 3300.35387459422,
		road: 19530.780787037,
		acceleration: 0.0938888888888876
	},
	{
		id: 2068,
		time: 2067,
		velocity: 11.7,
		power: 3792.45150364802,
		road: 19542.5242592592,
		acceleration: 0.133425925925927
	},
	{
		id: 2069,
		time: 2068,
		velocity: 11.9022222222222,
		power: 2061.91731810637,
		road: 19554.3231018518,
		acceleration: -0.0226851851851855
	},
	{
		id: 2070,
		time: 2069,
		velocity: 11.7602777777778,
		power: 3329.74020977066,
		road: 19566.1549537037,
		acceleration: 0.088703703703704
	},
	{
		id: 2071,
		time: 2070,
		velocity: 11.9661111111111,
		power: 2901.31763681161,
		road: 19578.0553240741,
		acceleration: 0.048333333333332
	},
	{
		id: 2072,
		time: 2071,
		velocity: 12.0472222222222,
		power: 4299.76364816617,
		road: 19590.0631944444,
		acceleration: 0.166666666666668
	},
	{
		id: 2073,
		time: 2072,
		velocity: 12.2602777777778,
		power: 3068.36858915755,
		road: 19602.1819907407,
		acceleration: 0.0551851851851843
	},
	{
		id: 2074,
		time: 2073,
		velocity: 12.1316666666667,
		power: 2011.67624337721,
		road: 19614.3101388889,
		acceleration: -0.0364814814814824
	},
	{
		id: 2075,
		time: 2074,
		velocity: 11.9377777777778,
		power: 706.236438642833,
		road: 19626.34625,
		acceleration: -0.14759259259259
	},
	{
		id: 2076,
		time: 2075,
		velocity: 11.8175,
		power: 539.366731133641,
		road: 19638.2288425926,
		acceleration: -0.159444444444446
	},
	{
		id: 2077,
		time: 2076,
		velocity: 11.6533333333333,
		power: 507.293635285719,
		road: 19649.9519444444,
		acceleration: -0.159537037037039
	},
	{
		id: 2078,
		time: 2077,
		velocity: 11.4591666666667,
		power: 1851.89992451677,
		road: 19661.5769907407,
		acceleration: -0.036574074074073
	},
	{
		id: 2079,
		time: 2078,
		velocity: 11.7077777777778,
		power: 4096.68196646949,
		road: 19673.2652777778,
		acceleration: 0.163055555555557
	},
	{
		id: 2080,
		time: 2079,
		velocity: 12.1425,
		power: 5474.2650224911,
		road: 19685.1724074074,
		acceleration: 0.274629629629629
	},
	{
		id: 2081,
		time: 2080,
		velocity: 12.2830555555556,
		power: 5128.49388075054,
		road: 19697.3325,
		acceleration: 0.231296296296298
	},
	{
		id: 2082,
		time: 2081,
		velocity: 12.4016666666667,
		power: 4684.41647442537,
		road: 19709.6999074074,
		acceleration: 0.183333333333332
	},
	{
		id: 2083,
		time: 2082,
		velocity: 12.6925,
		power: 3055.77395699184,
		road: 19722.1796759259,
		acceleration: 0.0413888888888874
	},
	{
		id: 2084,
		time: 2083,
		velocity: 12.4072222222222,
		power: 2500.03701747114,
		road: 19734.6772222222,
		acceleration: -0.00583333333333336
	},
	{
		id: 2085,
		time: 2084,
		velocity: 12.3841666666667,
		power: -33.4102664333778,
		road: 19747.0634722222,
		acceleration: -0.216759259259257
	},
	{
		id: 2086,
		time: 2085,
		velocity: 12.0422222222222,
		power: -527.852770577704,
		road: 19759.2132407407,
		acceleration: -0.256203703703704
	},
	{
		id: 2087,
		time: 2086,
		velocity: 11.6386111111111,
		power: -597.217449161768,
		road: 19771.105,
		acceleration: -0.259814814814815
	},
	{
		id: 2088,
		time: 2087,
		velocity: 11.6047222222222,
		power: 241.721415435851,
		road: 19782.7756018518,
		acceleration: -0.182499999999999
	},
	{
		id: 2089,
		time: 2088,
		velocity: 11.4947222222222,
		power: 1460.39434510755,
		road: 19794.32,
		acceleration: -0.0699074074074062
	},
	{
		id: 2090,
		time: 2089,
		velocity: 11.4288888888889,
		power: -516.167807353355,
		road: 19805.7054166667,
		acceleration: -0.248055555555558
	},
	{
		id: 2091,
		time: 2090,
		velocity: 10.8605555555556,
		power: -942.321698503863,
		road: 19816.8237962963,
		acceleration: -0.286018518518517
	},
	{
		id: 2092,
		time: 2091,
		velocity: 10.6366666666667,
		power: -1886.2873941196,
		road: 19827.6109259259,
		acceleration: -0.376481481481482
	},
	{
		id: 2093,
		time: 2092,
		velocity: 10.2994444444444,
		power: -606.260612495164,
		road: 19838.0848148148,
		acceleration: -0.249999999999998
	},
	{
		id: 2094,
		time: 2093,
		velocity: 10.1105555555556,
		power: -3286.30446134541,
		road: 19848.1703240741,
		acceleration: -0.52675925925926
	},
	{
		id: 2095,
		time: 2094,
		velocity: 9.05638888888889,
		power: -5266.6168431526,
		road: 19857.6107407407,
		acceleration: -0.763425925925926
	},
	{
		id: 2096,
		time: 2095,
		velocity: 8.00916666666667,
		power: -10230.1452206852,
		road: 19865.9409722222,
		acceleration: -1.45694444444444
	},
	{
		id: 2097,
		time: 2096,
		velocity: 5.73972222222222,
		power: -7397.83765135083,
		road: 19872.9069907407,
		acceleration: -1.27148148148148
	},
	{
		id: 2098,
		time: 2097,
		velocity: 5.24194444444444,
		power: -2666.42155271659,
		road: 19878.9305092593,
		acceleration: -0.613518518518518
	},
	{
		id: 2099,
		time: 2098,
		velocity: 6.16861111111111,
		power: 1879.488577147,
		road: 19884.7441203704,
		acceleration: 0.193703703703703
	},
	{
		id: 2100,
		time: 2099,
		velocity: 6.32083333333333,
		power: 2700.54150611524,
		road: 19890.8144907407,
		acceleration: 0.319814814814816
	},
	{
		id: 2101,
		time: 2100,
		velocity: 6.20138888888889,
		power: -1175.69573609248,
		road: 19896.8686111111,
		acceleration: -0.352314814814815
	},
	{
		id: 2102,
		time: 2101,
		velocity: 5.11166666666667,
		power: -3400.92739648833,
		road: 19902.3478240741,
		acceleration: -0.797500000000001
	},
	{
		id: 2103,
		time: 2102,
		velocity: 3.92833333333333,
		power: -5078.52079865676,
		road: 19906.7519444444,
		acceleration: -1.35268518518518
	},
	{
		id: 2104,
		time: 2103,
		velocity: 2.14333333333333,
		power: -3583.90556062457,
		road: 19909.7922222222,
		acceleration: -1.375
	},
	{
		id: 2105,
		time: 2104,
		velocity: 0.986666666666667,
		power: -1900.32365823592,
		road: 19911.4902777778,
		acceleration: -1.30944444444444
	},
	{
		id: 2106,
		time: 2105,
		velocity: 0,
		power: -381.4540996448,
		road: 19912.1763888889,
		acceleration: -0.714444444444445
	},
	{
		id: 2107,
		time: 2106,
		velocity: 0,
		power: -31.3695953216374,
		road: 19912.3408333333,
		acceleration: -0.328888888888889
	},
	{
		id: 2108,
		time: 2107,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2109,
		time: 2108,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2110,
		time: 2109,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2111,
		time: 2110,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2112,
		time: 2111,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2113,
		time: 2112,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2114,
		time: 2113,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2115,
		time: 2114,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2116,
		time: 2115,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2117,
		time: 2116,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2118,
		time: 2117,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2119,
		time: 2118,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2120,
		time: 2119,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2121,
		time: 2120,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2122,
		time: 2121,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2123,
		time: 2122,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2124,
		time: 2123,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2125,
		time: 2124,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2126,
		time: 2125,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2127,
		time: 2126,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2128,
		time: 2127,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2129,
		time: 2128,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2130,
		time: 2129,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2131,
		time: 2130,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2132,
		time: 2131,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2133,
		time: 2132,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2134,
		time: 2133,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2135,
		time: 2134,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2136,
		time: 2135,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2137,
		time: 2136,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2138,
		time: 2137,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2139,
		time: 2138,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2140,
		time: 2139,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2141,
		time: 2140,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2142,
		time: 2141,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2143,
		time: 2142,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2144,
		time: 2143,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2145,
		time: 2144,
		velocity: 0,
		power: 0,
		road: 19912.3408333333,
		acceleration: 0
	},
	{
		id: 2146,
		time: 2145,
		velocity: 0,
		power: 150.395952024945,
		road: 19912.5924537037,
		acceleration: 0.503240740740741
	},
	{
		id: 2147,
		time: 2146,
		velocity: 1.50972222222222,
		power: 755.575658696438,
		road: 19913.4805092593,
		acceleration: 0.76962962962963
	},
	{
		id: 2148,
		time: 2147,
		velocity: 2.30888888888889,
		power: 1820.61861016595,
		road: 19915.2356481481,
		acceleration: 0.964537037037037
	},
	{
		id: 2149,
		time: 2148,
		velocity: 2.89361111111111,
		power: 1996.57001571625,
		road: 19917.8153240741,
		acceleration: 0.684537037037037
	},
	{
		id: 2150,
		time: 2149,
		velocity: 3.56333333333333,
		power: 1857.1579110523,
		road: 19920.9799074074,
		acceleration: 0.485277777777778
	},
	{
		id: 2151,
		time: 2150,
		velocity: 3.76472222222222,
		power: 2607.95537283285,
		road: 19924.6897685185,
		acceleration: 0.605277777777778
	},
	{
		id: 2152,
		time: 2151,
		velocity: 4.70944444444444,
		power: 2795.57222273457,
		road: 19928.976712963,
		acceleration: 0.548888888888889
	},
	{
		id: 2153,
		time: 2152,
		velocity: 5.21,
		power: 3516.67869821672,
		road: 19933.8476851852,
		acceleration: 0.619166666666666
	},
	{
		id: 2154,
		time: 2153,
		velocity: 5.62222222222222,
		power: 3357.14044662768,
		road: 19939.2812962963,
		acceleration: 0.506111111111112
	},
	{
		id: 2155,
		time: 2154,
		velocity: 6.22777777777778,
		power: 2349.37775550406,
		road: 19945.1068518518,
		acceleration: 0.277777777777777
	},
	{
		id: 2156,
		time: 2155,
		velocity: 6.04333333333333,
		power: 633.224566928323,
		road: 19951.0536574074,
		acceleration: -0.0352777777777762
	},
	{
		id: 2157,
		time: 2156,
		velocity: 5.51638888888889,
		power: -756.629561107901,
		road: 19956.8409259259,
		acceleration: -0.283796296296297
	},
	{
		id: 2158,
		time: 2157,
		velocity: 5.37638888888889,
		power: -2707.11567730357,
		road: 19962.1461574074,
		acceleration: -0.680277777777777
	},
	{
		id: 2159,
		time: 2158,
		velocity: 4.0025,
		power: -2414.90324019637,
		road: 19966.7663888889,
		acceleration: -0.689722222222223
	},
	{
		id: 2160,
		time: 2159,
		velocity: 3.44722222222222,
		power: -2815.15348523015,
		road: 19970.5857407407,
		acceleration: -0.912037037037037
	},
	{
		id: 2161,
		time: 2160,
		velocity: 2.64027777777778,
		power: -979.105079261665,
		road: 19973.7179166667,
		acceleration: -0.462314814814815
	},
	{
		id: 2162,
		time: 2161,
		velocity: 2.61555555555556,
		power: -914.776618482804,
		road: 19976.3715740741,
		acceleration: -0.494722222222221
	},
	{
		id: 2163,
		time: 2162,
		velocity: 1.96305555555556,
		power: -770.987648372045,
		road: 19978.5240277778,
		acceleration: -0.507685185185186
	},
	{
		id: 2164,
		time: 2163,
		velocity: 1.11722222222222,
		power: -1030.57098236895,
		road: 19979.986712963,
		acceleration: -0.871851851851852
	},
	{
		id: 2165,
		time: 2164,
		velocity: 0,
		power: -349.105443087226,
		road: 19980.6862962963,
		acceleration: -0.654351851851852
	},
	{
		id: 2166,
		time: 2165,
		velocity: 0,
		power: -43.1972338856401,
		road: 19980.8725,
		acceleration: -0.372407407407407
	},
	{
		id: 2167,
		time: 2166,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2168,
		time: 2167,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2169,
		time: 2168,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2170,
		time: 2169,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2171,
		time: 2170,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2172,
		time: 2171,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2173,
		time: 2172,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2174,
		time: 2173,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2175,
		time: 2174,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2176,
		time: 2175,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2177,
		time: 2176,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2178,
		time: 2177,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2179,
		time: 2178,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2180,
		time: 2179,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2181,
		time: 2180,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2182,
		time: 2181,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2183,
		time: 2182,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2184,
		time: 2183,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2185,
		time: 2184,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2186,
		time: 2185,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2187,
		time: 2186,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2188,
		time: 2187,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2189,
		time: 2188,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2190,
		time: 2189,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2191,
		time: 2190,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2192,
		time: 2191,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2193,
		time: 2192,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2194,
		time: 2193,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2195,
		time: 2194,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2196,
		time: 2195,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2197,
		time: 2196,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2198,
		time: 2197,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2199,
		time: 2198,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2200,
		time: 2199,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2201,
		time: 2200,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2202,
		time: 2201,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2203,
		time: 2202,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2204,
		time: 2203,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2205,
		time: 2204,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2206,
		time: 2205,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2207,
		time: 2206,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2208,
		time: 2207,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2209,
		time: 2208,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2210,
		time: 2209,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2211,
		time: 2210,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2212,
		time: 2211,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2213,
		time: 2212,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2214,
		time: 2213,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2215,
		time: 2214,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2216,
		time: 2215,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2217,
		time: 2216,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2218,
		time: 2217,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2219,
		time: 2218,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2220,
		time: 2219,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2221,
		time: 2220,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2222,
		time: 2221,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2223,
		time: 2222,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2224,
		time: 2223,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2225,
		time: 2224,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2226,
		time: 2225,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2227,
		time: 2226,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2228,
		time: 2227,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2229,
		time: 2228,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2230,
		time: 2229,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2231,
		time: 2230,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2232,
		time: 2231,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2233,
		time: 2232,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2234,
		time: 2233,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2235,
		time: 2234,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2236,
		time: 2235,
		velocity: 0,
		power: 0,
		road: 19980.8725,
		acceleration: 0
	},
	{
		id: 2237,
		time: 2236,
		velocity: 0,
		power: 62.7884264713336,
		road: 19981.0254166667,
		acceleration: 0.305833333333333
	},
	{
		id: 2238,
		time: 2237,
		velocity: 0.9175,
		power: 244.487111583444,
		road: 19981.5254166667,
		acceleration: 0.388333333333333
	},
	{
		id: 2239,
		time: 2238,
		velocity: 1.165,
		power: 462.183985260816,
		road: 19982.42625,
		acceleration: 0.413333333333333
	},
	{
		id: 2240,
		time: 2239,
		velocity: 1.24,
		power: 295.174364858239,
		road: 19983.602037037,
		acceleration: 0.136574074074074
	},
	{
		id: 2241,
		time: 2240,
		velocity: 1.32722222222222,
		power: 203.770599250708,
		road: 19984.8668981481,
		acceleration: 0.041574074074074
	},
	{
		id: 2242,
		time: 2241,
		velocity: 1.28972222222222,
		power: 132.579577973654,
		road: 19986.1431481481,
		acceleration: -0.0187962962962962
	},
	{
		id: 2243,
		time: 2242,
		velocity: 1.18361111111111,
		power: -8.91349786070915,
		road: 19987.3419444444,
		acceleration: -0.136111111111111
	},
	{
		id: 2244,
		time: 2243,
		velocity: 0.918888888888889,
		power: -262.094739544421,
		road: 19988.2577314815,
		acceleration: -0.429907407407408
	},
	{
		id: 2245,
		time: 2244,
		velocity: 0,
		power: -127.353104920656,
		road: 19988.7612962963,
		acceleration: -0.394537037037037
	},
	{
		id: 2246,
		time: 2245,
		velocity: 0,
		power: -25.936794217024,
		road: 19988.9144444444,
		acceleration: -0.306296296296296
	},
	{
		id: 2247,
		time: 2246,
		velocity: 0,
		power: 0,
		road: 19988.9144444444,
		acceleration: 0
	},
	{
		id: 2248,
		time: 2247,
		velocity: 0,
		power: 0,
		road: 19988.9144444444,
		acceleration: 0
	},
	{
		id: 2249,
		time: 2248,
		velocity: 0,
		power: 0,
		road: 19988.9144444444,
		acceleration: 0
	},
	{
		id: 2250,
		time: 2249,
		velocity: 0,
		power: 0,
		road: 19988.9144444444,
		acceleration: 0
	},
	{
		id: 2251,
		time: 2250,
		velocity: 0,
		power: 0,
		road: 19988.9144444444,
		acceleration: 0
	},
	{
		id: 2252,
		time: 2251,
		velocity: 0,
		power: 0,
		road: 19988.9144444444,
		acceleration: 0
	},
	{
		id: 2253,
		time: 2252,
		velocity: 0,
		power: 0,
		road: 19988.9144444444,
		acceleration: 0
	},
	{
		id: 2254,
		time: 2253,
		velocity: 0,
		power: 0,
		road: 19988.9144444444,
		acceleration: 0
	},
	{
		id: 2255,
		time: 2254,
		velocity: 0,
		power: 175.215549269622,
		road: 19989.188287037,
		acceleration: 0.547685185185185
	},
	{
		id: 2256,
		time: 2255,
		velocity: 1.64305555555556,
		power: 1728.19827123148,
		road: 19990.4148148148,
		acceleration: 1.35768518518519
	},
	{
		id: 2257,
		time: 2256,
		velocity: 4.07305555555556,
		power: 4408.01307802034,
		road: 19993.1146296296,
		acceleration: 1.58888888888889
	},
	{
		id: 2258,
		time: 2257,
		velocity: 4.76666666666667,
		power: 5074.66391801018,
		road: 19997.1952314815,
		acceleration: 1.17268518518518
	},
	{
		id: 2259,
		time: 2258,
		velocity: 5.16111111111111,
		power: 2061.76553489228,
		road: 20002.0169907407,
		acceleration: 0.309629629629629
	},
	{
		id: 2260,
		time: 2259,
		velocity: 5.00194444444444,
		power: 2335.2240189529,
		road: 20007.1612962963,
		acceleration: 0.335462962962963
	},
	{
		id: 2261,
		time: 2260,
		velocity: 5.77305555555556,
		power: 4018.93537985833,
		road: 20012.7772222222,
		acceleration: 0.607777777777778
	},
	{
		id: 2262,
		time: 2261,
		velocity: 6.98444444444444,
		power: 9951.78492822333,
		road: 20019.4097222222,
		acceleration: 1.42537037037037
	},
	{
		id: 2263,
		time: 2262,
		velocity: 9.27805555555556,
		power: 10855.5478726342,
		road: 20027.3880092593,
		acceleration: 1.2662037037037
	},
	{
		id: 2264,
		time: 2263,
		velocity: 9.57166666666667,
		power: 8517.74439788424,
		road: 20036.4085185185,
		acceleration: 0.818240740740741
	},
	{
		id: 2265,
		time: 2264,
		velocity: 9.43916666666667,
		power: 1861.54031300013,
		road: 20045.8527777778,
		acceleration: 0.0292592592592591
	},
	{
		id: 2266,
		time: 2265,
		velocity: 9.36583333333333,
		power: 741.375442866299,
		road: 20055.2643518518,
		acceleration: -0.0946296296296296
	},
	{
		id: 2267,
		time: 2266,
		velocity: 9.28777777777778,
		power: 1282.6701625558,
		road: 20064.6123148148,
		acceleration: -0.032592592592593
	},
	{
		id: 2268,
		time: 2267,
		velocity: 9.34138888888889,
		power: -273.90281663406,
		road: 20073.8406944444,
		acceleration: -0.206574074074073
	},
	{
		id: 2269,
		time: 2268,
		velocity: 8.74611111111111,
		power: -3247.12105935873,
		road: 20082.6872685185,
		acceleration: -0.557037037037038
	},
	{
		id: 2270,
		time: 2269,
		velocity: 7.61666666666667,
		power: -8631.68749004313,
		road: 20090.600787037,
		acceleration: -1.30907407407407
	},
	{
		id: 2271,
		time: 2270,
		velocity: 5.41416666666667,
		power: -8792.28180822448,
		road: 20097.0693981481,
		acceleration: -1.58074074074074
	},
	{
		id: 2272,
		time: 2271,
		velocity: 4.00388888888889,
		power: -8227.74381788095,
		road: 20101.7527777778,
		acceleration: -1.98972222222222
	},
	{
		id: 2273,
		time: 2272,
		velocity: 1.6475,
		power: -3846.52534511716,
		road: 20104.6834722222,
		acceleration: -1.51564814814815
	},
	{
		id: 2274,
		time: 2273,
		velocity: 0.867222222222222,
		power: -1721.13097210126,
		road: 20106.1890277778,
		acceleration: -1.33462962962963
	},
	{
		id: 2275,
		time: 2274,
		velocity: 0,
		power: -225.124711489233,
		road: 20106.7526851852,
		acceleration: -0.549166666666667
	},
	{
		id: 2276,
		time: 2275,
		velocity: 0,
		power: -22.1202017218973,
		road: 20106.8972222222,
		acceleration: -0.289074074074074
	},
	{
		id: 2277,
		time: 2276,
		velocity: 0,
		power: 0,
		road: 20106.8972222222,
		acceleration: 0
	},
	{
		id: 2278,
		time: 2277,
		velocity: 0,
		power: 0,
		road: 20106.8972222222,
		acceleration: 0
	},
	{
		id: 2279,
		time: 2278,
		velocity: 0,
		power: 0,
		road: 20106.8972222222,
		acceleration: 0
	},
	{
		id: 2280,
		time: 2279,
		velocity: 0,
		power: 0,
		road: 20106.8972222222,
		acceleration: 0
	},
	{
		id: 2281,
		time: 2280,
		velocity: 0,
		power: 0,
		road: 20106.8972222222,
		acceleration: 0
	},
	{
		id: 2282,
		time: 2281,
		velocity: 0,
		power: 0,
		road: 20106.8972222222,
		acceleration: 0
	},
	{
		id: 2283,
		time: 2282,
		velocity: 0,
		power: 0,
		road: 20106.8972222222,
		acceleration: 0
	},
	{
		id: 2284,
		time: 2283,
		velocity: 0,
		power: 0,
		road: 20106.8972222222,
		acceleration: 0
	},
	{
		id: 2285,
		time: 2284,
		velocity: 0,
		power: 0,
		road: 20106.8972222222,
		acceleration: 0
	},
	{
		id: 2286,
		time: 2285,
		velocity: 0,
		power: 0,
		road: 20106.8972222222,
		acceleration: 0
	},
	{
		id: 2287,
		time: 2286,
		velocity: 0,
		power: 5.49362522342195,
		road: 20106.9279166667,
		acceleration: 0.0613888888888889
	},
	{
		id: 2288,
		time: 2287,
		velocity: 0.184166666666667,
		power: 7.41700190883221,
		road: 20106.9893055556,
		acceleration: 0
	},
	{
		id: 2289,
		time: 2288,
		velocity: 0,
		power: 7.41700190883221,
		road: 20107.0506944444,
		acceleration: 0
	},
	{
		id: 2290,
		time: 2289,
		velocity: 0,
		power: 1.92331388888889,
		road: 20107.0813888889,
		acceleration: -0.0613888888888889
	},
	{
		id: 2291,
		time: 2290,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2292,
		time: 2291,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2293,
		time: 2292,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2294,
		time: 2293,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2295,
		time: 2294,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2296,
		time: 2295,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2297,
		time: 2296,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2298,
		time: 2297,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2299,
		time: 2298,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2300,
		time: 2299,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2301,
		time: 2300,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2302,
		time: 2301,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2303,
		time: 2302,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2304,
		time: 2303,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2305,
		time: 2304,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2306,
		time: 2305,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2307,
		time: 2306,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2308,
		time: 2307,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2309,
		time: 2308,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2310,
		time: 2309,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2311,
		time: 2310,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2312,
		time: 2311,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2313,
		time: 2312,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2314,
		time: 2313,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2315,
		time: 2314,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2316,
		time: 2315,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2317,
		time: 2316,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2318,
		time: 2317,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2319,
		time: 2318,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2320,
		time: 2319,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2321,
		time: 2320,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2322,
		time: 2321,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2323,
		time: 2322,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2324,
		time: 2323,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2325,
		time: 2324,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2326,
		time: 2325,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2327,
		time: 2326,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2328,
		time: 2327,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2329,
		time: 2328,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2330,
		time: 2329,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2331,
		time: 2330,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2332,
		time: 2331,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2333,
		time: 2332,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2334,
		time: 2333,
		velocity: 0,
		power: 0,
		road: 20107.0813888889,
		acceleration: 0
	},
	{
		id: 2335,
		time: 2334,
		velocity: 0,
		power: 14.6562092226068,
		road: 20107.1430555556,
		acceleration: 0.123333333333333
	},
	{
		id: 2336,
		time: 2335,
		velocity: 0.37,
		power: 240.420862699449,
		road: 20107.5302314815,
		acceleration: 0.527685185185185
	},
	{
		id: 2337,
		time: 2336,
		velocity: 1.58305555555556,
		power: 1739.05344284121,
		road: 20108.8251851852,
		acceleration: 1.28787037037037
	},
	{
		id: 2338,
		time: 2337,
		velocity: 3.86361111111111,
		power: 4725.87522501287,
		road: 20111.5965740741,
		acceleration: 1.665
	},
	{
		id: 2339,
		time: 2338,
		velocity: 5.365,
		power: 5884.45701517507,
		road: 20115.8584722222,
		acceleration: 1.31601851851852
	},
	{
		id: 2340,
		time: 2339,
		velocity: 5.53111111111111,
		power: 4086.00405980525,
		road: 20121.1158333333,
		acceleration: 0.674907407407408
	},
	{
		id: 2341,
		time: 2340,
		velocity: 5.88833333333333,
		power: 1453.92794055131,
		road: 20126.7731481481,
		acceleration: 0.125
	},
	{
		id: 2342,
		time: 2341,
		velocity: 5.74,
		power: 918.707850977967,
		road: 20132.5043518519,
		acceleration: 0.0227777777777787
	},
	{
		id: 2343,
		time: 2342,
		velocity: 5.59944444444444,
		power: 285.739300358718,
		road: 20138.2005092593,
		acceleration: -0.0928703703703704
	},
	{
		id: 2344,
		time: 2343,
		velocity: 5.60972222222222,
		power: 776.950227719991,
		road: 20143.8499074074,
		acceleration: -0.000648148148148842
	},
	{
		id: 2345,
		time: 2344,
		velocity: 5.73805555555556,
		power: 711.751917548166,
		road: 20149.4926851852,
		acceleration: -0.0125925925925925
	},
	{
		id: 2346,
		time: 2345,
		velocity: 5.56166666666667,
		power: 244.604793744577,
		road: 20155.0797222222,
		acceleration: -0.0988888888888901
	},
	{
		id: 2347,
		time: 2346,
		velocity: 5.31305555555555,
		power: -348.36163243981,
		road: 20160.5115740741,
		acceleration: -0.211481481481481
	},
	{
		id: 2348,
		time: 2347,
		velocity: 5.10361111111111,
		power: -941.931762927158,
		road: 20165.6704166667,
		acceleration: -0.334537037037038
	},
	{
		id: 2349,
		time: 2348,
		velocity: 4.55805555555555,
		power: -2479.74470036197,
		road: 20170.3109259259,
		acceleration: -0.702129629629629
	},
	{
		id: 2350,
		time: 2349,
		velocity: 3.20666666666667,
		power: -3484.50480717006,
		road: 20174.0406481481,
		acceleration: -1.11944444444444
	},
	{
		id: 2351,
		time: 2350,
		velocity: 1.74527777777778,
		power: -2664.74803826277,
		road: 20176.5952777778,
		acceleration: -1.23074074074074
	},
	{
		id: 2352,
		time: 2351,
		velocity: 0.865833333333333,
		power: -1093.92041248273,
		road: 20178.0821759259,
		acceleration: -0.904722222222222
	},
	{
		id: 2353,
		time: 2352,
		velocity: 0.4925,
		power: -319.929718481405,
		road: 20178.8258333333,
		acceleration: -0.581759259259259
	},
	{
		id: 2354,
		time: 2353,
		velocity: 0,
		power: -47.069322057754,
		road: 20179.1343055556,
		acceleration: -0.288611111111111
	},
	{
		id: 2355,
		time: 2354,
		velocity: 0,
		power: -2.84898289473684,
		road: 20179.2163888889,
		acceleration: -0.164166666666667
	},
	{
		id: 2356,
		time: 2355,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2357,
		time: 2356,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2358,
		time: 2357,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2359,
		time: 2358,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2360,
		time: 2359,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2361,
		time: 2360,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2362,
		time: 2361,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2363,
		time: 2362,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2364,
		time: 2363,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2365,
		time: 2364,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2366,
		time: 2365,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2367,
		time: 2366,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2368,
		time: 2367,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2369,
		time: 2368,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2370,
		time: 2369,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2371,
		time: 2370,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2372,
		time: 2371,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2373,
		time: 2372,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2374,
		time: 2373,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2375,
		time: 2374,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2376,
		time: 2375,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2377,
		time: 2376,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2378,
		time: 2377,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2379,
		time: 2378,
		velocity: 0,
		power: 0,
		road: 20179.2163888889,
		acceleration: 0
	},
	{
		id: 2380,
		time: 2379,
		velocity: 0,
		power: 1.29558108076848,
		road: 20179.2257407407,
		acceleration: 0.0187037037037037
	},
	{
		id: 2381,
		time: 2380,
		velocity: 0.0561111111111111,
		power: 2.25974565731344,
		road: 20179.2444444444,
		acceleration: 0
	},
	{
		id: 2382,
		time: 2381,
		velocity: 0,
		power: 2.25974565731344,
		road: 20179.2631481481,
		acceleration: 0
	},
	{
		id: 2383,
		time: 2382,
		velocity: 0,
		power: 0.964162800519818,
		road: 20179.2725,
		acceleration: -0.0187037037037037
	},
	{
		id: 2384,
		time: 2383,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2385,
		time: 2384,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2386,
		time: 2385,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2387,
		time: 2386,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2388,
		time: 2387,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2389,
		time: 2388,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2390,
		time: 2389,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2391,
		time: 2390,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2392,
		time: 2391,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2393,
		time: 2392,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2394,
		time: 2393,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2395,
		time: 2394,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2396,
		time: 2395,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2397,
		time: 2396,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2398,
		time: 2397,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2399,
		time: 2398,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2400,
		time: 2399,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2401,
		time: 2400,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2402,
		time: 2401,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2403,
		time: 2402,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2404,
		time: 2403,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2405,
		time: 2404,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2406,
		time: 2405,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2407,
		time: 2406,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2408,
		time: 2407,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2409,
		time: 2408,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2410,
		time: 2409,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2411,
		time: 2410,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2412,
		time: 2411,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2413,
		time: 2412,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2414,
		time: 2413,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2415,
		time: 2414,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2416,
		time: 2415,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2417,
		time: 2416,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2418,
		time: 2417,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2419,
		time: 2418,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2420,
		time: 2419,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2421,
		time: 2420,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2422,
		time: 2421,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2423,
		time: 2422,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2424,
		time: 2423,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2425,
		time: 2424,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2426,
		time: 2425,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2427,
		time: 2426,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2428,
		time: 2427,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2429,
		time: 2428,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2430,
		time: 2429,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2431,
		time: 2430,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2432,
		time: 2431,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2433,
		time: 2432,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2434,
		time: 2433,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2435,
		time: 2434,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2436,
		time: 2435,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2437,
		time: 2436,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2438,
		time: 2437,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2439,
		time: 2438,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2440,
		time: 2439,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2441,
		time: 2440,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2442,
		time: 2441,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2443,
		time: 2442,
		velocity: 0,
		power: 0,
		road: 20179.2725,
		acceleration: 0
	},
	{
		id: 2444,
		time: 2443,
		velocity: 0,
		power: 92.9343018582516,
		road: 20179.4643518519,
		acceleration: 0.383703703703704
	},
	{
		id: 2445,
		time: 2444,
		velocity: 1.15111111111111,
		power: 1295.12645318845,
		road: 20180.4659722222,
		acceleration: 1.23583333333333
	},
	{
		id: 2446,
		time: 2445,
		velocity: 3.7075,
		power: 3920.97488928534,
		road: 20182.8770833333,
		acceleration: 1.58314814814815
	},
	{
		id: 2447,
		time: 2446,
		velocity: 4.74944444444444,
		power: 6654.68296115166,
		road: 20186.8855555556,
		acceleration: 1.61157407407407
	},
	{
		id: 2448,
		time: 2447,
		velocity: 5.98583333333333,
		power: 4840.63391978254,
		road: 20192.1154166667,
		acceleration: 0.831203703703704
	},
	{
		id: 2449,
		time: 2448,
		velocity: 6.20111111111111,
		power: 3990.5966732516,
		road: 20198.0414814815,
		acceleration: 0.561203703703703
	},
	{
		id: 2450,
		time: 2449,
		velocity: 6.43305555555555,
		power: 2827.29454600139,
		road: 20204.4066203704,
		acceleration: 0.316944444444445
	},
	{
		id: 2451,
		time: 2450,
		velocity: 6.93666666666667,
		power: 3621.29213269279,
		road: 20211.1366666667,
		acceleration: 0.412870370370371
	},
	{
		id: 2452,
		time: 2451,
		velocity: 7.43972222222222,
		power: 2367.70015561744,
		road: 20218.1724074074,
		acceleration: 0.198518518518519
	},
	{
		id: 2453,
		time: 2452,
		velocity: 7.02861111111111,
		power: 2654.67537446275,
		road: 20225.4213888889,
		acceleration: 0.227962962962962
	},
	{
		id: 2454,
		time: 2453,
		velocity: 7.62055555555556,
		power: 3383.25216930431,
		road: 20232.9411574074,
		acceleration: 0.313611111111111
	},
	{
		id: 2455,
		time: 2454,
		velocity: 8.38055555555556,
		power: 7932.32588306798,
		road: 20241.0493981481,
		acceleration: 0.863333333333332
	},
	{
		id: 2456,
		time: 2455,
		velocity: 9.61861111111111,
		power: 11430.4501619237,
		road: 20250.160787037,
		acceleration: 1.14296296296296
	},
	{
		id: 2457,
		time: 2456,
		velocity: 11.0494444444444,
		power: 10499.8148905769,
		road: 20260.2946296296,
		acceleration: 0.901944444444444
	},
	{
		id: 2458,
		time: 2457,
		velocity: 11.0863888888889,
		power: 7497.20613966062,
		road: 20271.1449537037,
		acceleration: 0.531018518518518
	},
	{
		id: 2459,
		time: 2458,
		velocity: 11.2116666666667,
		power: 1612.39845679068,
		road: 20282.2386111111,
		acceleration: -0.0443518518518502
	},
	{
		id: 2460,
		time: 2459,
		velocity: 10.9163888888889,
		power: -1114.92158831576,
		road: 20293.159212963,
		acceleration: -0.30175925925926
	},
	{
		id: 2461,
		time: 2460,
		velocity: 10.1811111111111,
		power: -4043.78000348061,
		road: 20303.631712963,
		acceleration: -0.594444444444445
	},
	{
		id: 2462,
		time: 2461,
		velocity: 9.42833333333333,
		power: -4914.21545511296,
		road: 20313.4534722222,
		acceleration: -0.707037037037036
	},
	{
		id: 2463,
		time: 2462,
		velocity: 8.79527777777778,
		power: -4746.44771102014,
		road: 20322.5609722222,
		acceleration: -0.721481481481483
	},
	{
		id: 2464,
		time: 2463,
		velocity: 8.01666666666667,
		power: -4896.15008528251,
		road: 20330.9165277778,
		acceleration: -0.782407407407407
	},
	{
		id: 2465,
		time: 2464,
		velocity: 7.08111111111111,
		power: -3801.5158003932,
		road: 20338.5387037037,
		acceleration: -0.684351851851852
	},
	{
		id: 2466,
		time: 2465,
		velocity: 6.74222222222222,
		power: -295.033234855952,
		road: 20345.7188888889,
		acceleration: -0.19962962962963
	},
	{
		id: 2467,
		time: 2466,
		velocity: 7.41777777777778,
		power: 2959.22289633401,
		road: 20352.9363888889,
		acceleration: 0.274259259259258
	},
	{
		id: 2468,
		time: 2467,
		velocity: 7.90388888888889,
		power: 4251.37756170678,
		road: 20360.5062962963,
		acceleration: 0.430555555555556
	},
	{
		id: 2469,
		time: 2468,
		velocity: 8.03388888888889,
		power: 2191.44835226878,
		road: 20368.3570833333,
		acceleration: 0.131203703703704
	},
	{
		id: 2470,
		time: 2469,
		velocity: 7.81138888888889,
		power: 238.648549868077,
		road: 20376.2083796296,
		acceleration: -0.130185185185186
	},
	{
		id: 2471,
		time: 2470,
		velocity: 7.51333333333333,
		power: -27.8306217879962,
		road: 20383.9122685185,
		acceleration: -0.16462962962963
	},
	{
		id: 2472,
		time: 2471,
		velocity: 7.54,
		power: 1335.20387462251,
		road: 20391.5456481481,
		acceleration: 0.0236111111111112
	},
	{
		id: 2473,
		time: 2472,
		velocity: 7.88222222222222,
		power: 3471.30778575656,
		road: 20399.3438888889,
		acceleration: 0.306111111111112
	},
	{
		id: 2474,
		time: 2473,
		velocity: 8.43166666666667,
		power: 5789.46024438802,
		road: 20407.5815277778,
		acceleration: 0.572685185185185
	},
	{
		id: 2475,
		time: 2474,
		velocity: 9.25805555555556,
		power: 5322.82322126754,
		road: 20416.3393518519,
		acceleration: 0.467685185185186
	},
	{
		id: 2476,
		time: 2475,
		velocity: 9.28527777777778,
		power: 5113.66828015004,
		road: 20425.5354166667,
		acceleration: 0.408796296296295
	},
	{
		id: 2477,
		time: 2476,
		velocity: 9.65805555555555,
		power: 4232.01434648332,
		road: 20435.0792592593,
		acceleration: 0.286759259259261
	},
	{
		id: 2478,
		time: 2477,
		velocity: 10.1183333333333,
		power: 7114.40812474097,
		road: 20445.049212963,
		acceleration: 0.565462962962963
	},
	{
		id: 2479,
		time: 2478,
		velocity: 10.9816666666667,
		power: 6062.63096199402,
		road: 20455.5113425926,
		acceleration: 0.418888888888887
	},
	{
		id: 2480,
		time: 2479,
		velocity: 10.9147222222222,
		power: 7468.31743217156,
		road: 20466.4437962963,
		acceleration: 0.521759259259261
	},
	{
		id: 2481,
		time: 2480,
		velocity: 11.6836111111111,
		power: 7900.12209244363,
		road: 20477.8980555556,
		acceleration: 0.521851851851851
	},
	{
		id: 2482,
		time: 2481,
		velocity: 12.5472222222222,
		power: 8141.70469039583,
		road: 20489.8657407407,
		acceleration: 0.505000000000001
	},
	{
		id: 2483,
		time: 2482,
		velocity: 12.4297222222222,
		power: 4537.33950431855,
		road: 20502.1727314815,
		acceleration: 0.173611111111111
	},
	{
		id: 2484,
		time: 2483,
		velocity: 12.2044444444444,
		power: 910.667975803375,
		road: 20514.4987037037,
		acceleration: -0.13564814814815
	},
	{
		id: 2485,
		time: 2484,
		velocity: 12.1402777777778,
		power: -745.279317988117,
		road: 20526.6194907407,
		acceleration: -0.27472222222222
	},
	{
		id: 2486,
		time: 2485,
		velocity: 11.6055555555556,
		power: -710.377200201686,
		road: 20538.4681944444,
		acceleration: -0.269444444444444
	},
	{
		id: 2487,
		time: 2486,
		velocity: 11.3961111111111,
		power: -2281.48482557998,
		road: 20549.9771759259,
		acceleration: -0.410000000000002
	},
	{
		id: 2488,
		time: 2487,
		velocity: 10.9102777777778,
		power: -1822.68826858302,
		road: 20561.0966203704,
		acceleration: -0.369074074074073
	},
	{
		id: 2489,
		time: 2488,
		velocity: 10.4983333333333,
		power: -1411.12632834943,
		road: 20571.8663888889,
		acceleration: -0.330277777777779
	},
	{
		id: 2490,
		time: 2489,
		velocity: 10.4052777777778,
		power: 784.347276463358,
		road: 20582.4149537037,
		acceleration: -0.11212962962963
	},
	{
		id: 2491,
		time: 2490,
		velocity: 10.5738888888889,
		power: 3886.74668068833,
		road: 20593.0046759259,
		acceleration: 0.194444444444445
	},
	{
		id: 2492,
		time: 2491,
		velocity: 11.0816666666667,
		power: 6021.46718901082,
		road: 20603.8848148148,
		acceleration: 0.386388888888888
	},
	{
		id: 2493,
		time: 2492,
		velocity: 11.5644444444444,
		power: 8019.62445093194,
		road: 20615.2288425926,
		acceleration: 0.541388888888887
	},
	{
		id: 2494,
		time: 2493,
		velocity: 12.1980555555556,
		power: 7398.32645766661,
		road: 20627.0679166667,
		acceleration: 0.448703703703707
	},
	{
		id: 2495,
		time: 2494,
		velocity: 12.4277777777778,
		power: 3948.91191957314,
		road: 20639.1968055556,
		acceleration: 0.130925925925924
	},
	{
		id: 2496,
		time: 2495,
		velocity: 11.9572222222222,
		power: -524.481264081797,
		road: 20651.2636111111,
		acceleration: -0.255092592592591
	},
	{
		id: 2497,
		time: 2496,
		velocity: 11.4327777777778,
		power: -3268.71485682439,
		road: 20662.9540277778,
		acceleration: -0.497685185185185
	},
	{
		id: 2498,
		time: 2497,
		velocity: 10.9347222222222,
		power: -1940.81731700162,
		road: 20674.2057407407,
		acceleration: -0.379722222222224
	},
	{
		id: 2499,
		time: 2498,
		velocity: 10.8180555555556,
		power: 283.826935702096,
		road: 20685.1834722222,
		acceleration: -0.168240740740739
	},
	{
		id: 2500,
		time: 2499,
		velocity: 10.9280555555556,
		power: 3374.84224190333,
		road: 20696.1410648148,
		acceleration: 0.127962962962963
	},
	{
		id: 2501,
		time: 2500,
		velocity: 11.3186111111111,
		power: 3723.44090276878,
		road: 20707.2401388889,
		acceleration: 0.154999999999999
	},
	{
		id: 2502,
		time: 2501,
		velocity: 11.2830555555556,
		power: 1841.09352496592,
		road: 20718.4043518518,
		acceleration: -0.0247222222222234
	},
	{
		id: 2503,
		time: 2502,
		velocity: 10.8538888888889,
		power: -135.29185919163,
		road: 20729.4516666667,
		acceleration: -0.209074074074072
	},
	{
		id: 2504,
		time: 2503,
		velocity: 10.6913888888889,
		power: 305.127641571485,
		road: 20740.3122222222,
		acceleration: -0.164444444444444
	},
	{
		id: 2505,
		time: 2504,
		velocity: 10.7897222222222,
		power: 1773.5800733521,
		road: 20751.0805555556,
		acceleration: -0.0200000000000014
	},
	{
		id: 2506,
		time: 2505,
		velocity: 10.7938888888889,
		power: 2165.81538475787,
		road: 20761.8480092593,
		acceleration: 0.0182407407407421
	},
	{
		id: 2507,
		time: 2506,
		velocity: 10.7461111111111,
		power: 2750.65446803519,
		road: 20772.6613425926,
		acceleration: 0.0735185185185188
	},
	{
		id: 2508,
		time: 2507,
		velocity: 11.0102777777778,
		power: 1669.40944638675,
		road: 20783.495462963,
		acceleration: -0.031944444444445
	},
	{
		id: 2509,
		time: 2508,
		velocity: 10.6980555555556,
		power: 1637.24914502793,
		road: 20794.2965277778,
		acceleration: -0.0341666666666658
	},
	{
		id: 2510,
		time: 2509,
		velocity: 10.6436111111111,
		power: 182.02586631858,
		road: 20804.9934722222,
		acceleration: -0.174074074074076
	},
	{
		id: 2511,
		time: 2510,
		velocity: 10.4880555555556,
		power: 986.801703368465,
		road: 20815.5572222222,
		acceleration: -0.0923148148148147
	},
	{
		id: 2512,
		time: 2511,
		velocity: 10.4211111111111,
		power: 816.954228023003,
		road: 20826.0212037037,
		acceleration: -0.107222222222221
	},
	{
		id: 2513,
		time: 2512,
		velocity: 10.3219444444444,
		power: 842.245130642227,
		road: 20836.3802777778,
		acceleration: -0.102592592592593
	},
	{
		id: 2514,
		time: 2513,
		velocity: 10.1802777777778,
		power: 660.551681949915,
		road: 20846.6285648148,
		acceleration: -0.11898148148148
	},
	{
		id: 2515,
		time: 2514,
		velocity: 10.0641666666667,
		power: 1113.26686711595,
		road: 20856.7821296296,
		acceleration: -0.0704629629629636
	},
	{
		id: 2516,
		time: 2515,
		velocity: 10.1105555555556,
		power: 1255.33913978849,
		road: 20866.8733333333,
		acceleration: -0.0542592592592612
	},
	{
		id: 2517,
		time: 2516,
		velocity: 10.0175,
		power: 1676.52013072975,
		road: 20876.9326388889,
		acceleration: -0.00953703703703468
	},
	{
		id: 2518,
		time: 2517,
		velocity: 10.0355555555556,
		power: 1390.98028794551,
		road: 20886.9678240741,
		acceleration: -0.038703703703705
	},
	{
		id: 2519,
		time: 2518,
		velocity: 9.99444444444444,
		power: 1997.97901973168,
		road: 20896.9961574074,
		acceleration: 0.0249999999999986
	},
	{
		id: 2520,
		time: 2519,
		velocity: 10.0925,
		power: 1939.93186099771,
		road: 20907.0461111111,
		acceleration: 0.0182407407407421
	},
	{
		id: 2521,
		time: 2520,
		velocity: 10.0902777777778,
		power: 1613.9690722594,
		road: 20917.0972685185,
		acceleration: -0.0158333333333331
	},
	{
		id: 2522,
		time: 2521,
		velocity: 9.94694444444444,
		power: 1715.69762632424,
		road: 20927.1380555555,
		acceleration: -0.00490740740740669
	},
	{
		id: 2523,
		time: 2522,
		velocity: 10.0777777777778,
		power: 1080.35777943282,
		road: 20937.1411574074,
		acceleration: -0.0704629629629636
	},
	{
		id: 2524,
		time: 2523,
		velocity: 9.87888888888889,
		power: 1256.79818849601,
		road: 20947.0837962963,
		acceleration: -0.0504629629629623
	},
	{
		id: 2525,
		time: 2524,
		velocity: 9.79555555555556,
		power: 1393.58204290337,
		road: 20956.98375,
		acceleration: -0.0349074074074078
	},
	{
		id: 2526,
		time: 2525,
		velocity: 9.97305555555556,
		power: 2524.72042141524,
		road: 20966.908287037,
		acceleration: 0.0840740740740742
	},
	{
		id: 2527,
		time: 2526,
		velocity: 10.1311111111111,
		power: 3141.02472789447,
		road: 20976.9469444444,
		acceleration: 0.144166666666665
	},
	{
		id: 2528,
		time: 2527,
		velocity: 10.2280555555556,
		power: 3230.8347064846,
		road: 20987.1312037037,
		acceleration: 0.147037037037038
	},
	{
		id: 2529,
		time: 2528,
		velocity: 10.4141666666667,
		power: 2800.89386409042,
		road: 20997.4379166667,
		acceleration: 0.0978703703703712
	},
	{
		id: 2530,
		time: 2529,
		velocity: 10.4247222222222,
		power: 2192.17000898477,
		road: 21007.8104166667,
		acceleration: 0.0337037037037042
	},
	{
		id: 2531,
		time: 2530,
		velocity: 10.3291666666667,
		power: 2868.15897859882,
		road: 21018.2494907407,
		acceleration: 0.0994444444444422
	},
	{
		id: 2532,
		time: 2531,
		velocity: 10.7125,
		power: 5361.72588548882,
		road: 21028.9064814815,
		acceleration: 0.336388888888889
	},
	{
		id: 2533,
		time: 2532,
		velocity: 11.4338888888889,
		power: 7965.78701024278,
		road: 21040.0094444444,
		acceleration: 0.555555555555557
	},
	{
		id: 2534,
		time: 2533,
		velocity: 11.9958333333333,
		power: 9194.31589866026,
		road: 21051.7002314815,
		acceleration: 0.620092592592592
	},
	{
		id: 2535,
		time: 2534,
		velocity: 12.5727777777778,
		power: 5393.03496244466,
		road: 21063.8289351852,
		acceleration: 0.255740740740741
	},
	{
		id: 2536,
		time: 2535,
		velocity: 12.2011111111111,
		power: -800.236678688812,
		road: 21075.945787037,
		acceleration: -0.279444444444444
	},
	{
		id: 2537,
		time: 2536,
		velocity: 11.1575,
		power: -7846.50867602362,
		road: 21087.4645833333,
		acceleration: -0.916666666666666
	},
	{
		id: 2538,
		time: 2537,
		velocity: 9.82277777777778,
		power: -10558.7860302513,
		road: 21097.8996759259,
		acceleration: -1.25074074074074
	},
	{
		id: 2539,
		time: 2538,
		velocity: 8.44888888888889,
		power: -11013.3322426015,
		road: 21106.9858796296,
		acceleration: -1.44703703703704
	},
	{
		id: 2540,
		time: 2539,
		velocity: 6.81638888888889,
		power: -9264.98741180828,
		road: 21114.6314814815,
		acceleration: -1.43416666666667
	},
	{
		id: 2541,
		time: 2540,
		velocity: 5.52027777777778,
		power: -7418.47488767818,
		road: 21120.85875,
		acceleration: -1.4025
	},
	{
		id: 2542,
		time: 2541,
		velocity: 4.24138888888889,
		power: -4106.47246453251,
		road: 21125.8838425926,
		acceleration: -1.00185185185185
	},
	{
		id: 2543,
		time: 2542,
		velocity: 3.81083333333333,
		power: -421.161830094826,
		road: 21130.2885185185,
		acceleration: -0.238981481481482
	},
	{
		id: 2544,
		time: 2543,
		velocity: 4.80333333333333,
		power: 2777.39625262281,
		road: 21134.8263888889,
		acceleration: 0.505370370370371
	},
	{
		id: 2545,
		time: 2544,
		velocity: 5.7575,
		power: 5373.58222088079,
		road: 21140.083287037,
		acceleration: 0.932685185185186
	},
	{
		id: 2546,
		time: 2545,
		velocity: 6.60888888888889,
		power: 8482.12462295899,
		road: 21146.43375,
		acceleration: 1.25444444444444
	},
	{
		id: 2547,
		time: 2546,
		velocity: 8.56666666666667,
		power: 10913.05345098,
		road: 21154.0810185185,
		acceleration: 1.33916666666667
	},
	{
		id: 2548,
		time: 2547,
		velocity: 9.775,
		power: 14060.8334139269,
		road: 21163.1270833333,
		acceleration: 1.45842592592593
	},
	{
		id: 2549,
		time: 2548,
		velocity: 10.9841666666667,
		power: 9988.70303811526,
		road: 21173.3233333333,
		acceleration: 0.841944444444445
	},
	{
		id: 2550,
		time: 2549,
		velocity: 11.0925,
		power: 8749.6399861412,
		road: 21184.2625925926,
		acceleration: 0.644074074074073
	},
	{
		id: 2551,
		time: 2550,
		velocity: 11.7072222222222,
		power: 7849.28287069101,
		road: 21195.7800925926,
		acceleration: 0.512407407407407
	},
	{
		id: 2552,
		time: 2551,
		velocity: 12.5213888888889,
		power: 9852.22821663025,
		road: 21207.8757407407,
		acceleration: 0.643888888888888
	},
	{
		id: 2553,
		time: 2552,
		velocity: 13.0241666666667,
		power: 9906.1901271173,
		road: 21220.5921759259,
		acceleration: 0.597685185185185
	},
	{
		id: 2554,
		time: 2553,
		velocity: 13.5002777777778,
		power: 7722.2798580639,
		road: 21233.800787037,
		acceleration: 0.386666666666668
	},
	{
		id: 2555,
		time: 2554,
		velocity: 13.6813888888889,
		power: 6081.62624631997,
		road: 21247.3230092593,
		acceleration: 0.240555555555554
	},
	{
		id: 2556,
		time: 2555,
		velocity: 13.7458333333333,
		power: 2511.39309493833,
		road: 21260.9460185185,
		acceleration: -0.0389814814814802
	},
	{
		id: 2557,
		time: 2556,
		velocity: 13.3833333333333,
		power: 90.3410369037782,
		road: 21274.4380092593,
		acceleration: -0.223055555555558
	},
	{
		id: 2558,
		time: 2557,
		velocity: 13.0122222222222,
		power: -2275.90059400618,
		road: 21287.6153240741,
		acceleration: -0.406296296296293
	},
	{
		id: 2559,
		time: 2558,
		velocity: 12.5269444444444,
		power: -3105.72609031002,
		road: 21300.3522685185,
		acceleration: -0.474444444444444
	},
	{
		id: 2560,
		time: 2559,
		velocity: 11.96,
		power: -3424.1570018969,
		road: 21312.5994444444,
		acceleration: -0.505092592592593
	},
	{
		id: 2561,
		time: 2560,
		velocity: 11.4969444444444,
		power: -3368.85375500167,
		road: 21324.3410648148,
		acceleration: -0.506018518518518
	},
	{
		id: 2562,
		time: 2561,
		velocity: 11.0088888888889,
		power: -2916.37097830145,
		road: 21335.5943518519,
		acceleration: -0.470648148148149
	},
	{
		id: 2563,
		time: 2562,
		velocity: 10.5480555555556,
		power: -2655.17203961839,
		road: 21346.386712963,
		acceleration: -0.451203703703705
	},
	{
		id: 2564,
		time: 2563,
		velocity: 10.1433333333333,
		power: -6905.08398959814,
		road: 21356.5026851852,
		acceleration: -0.901574074074073
	},
	{
		id: 2565,
		time: 2564,
		velocity: 8.30416666666667,
		power: -6595.2176249299,
		road: 21365.7039351852,
		acceleration: -0.927870370370371
	},
	{
		id: 2566,
		time: 2565,
		velocity: 7.76444444444444,
		power: -8593.63333806333,
		road: 21373.8014351852,
		acceleration: -1.27962962962963
	},
	{
		id: 2567,
		time: 2566,
		velocity: 6.30444444444445,
		power: -8032.34618720351,
		road: 21380.5573611111,
		acceleration: -1.40351851851852
	},
	{
		id: 2568,
		time: 2567,
		velocity: 4.09361111111111,
		power: -6734.67562493976,
		road: 21385.8731018519,
		acceleration: -1.47685185185185
	},
	{
		id: 2569,
		time: 2568,
		velocity: 3.33388888888889,
		power: -3819.5852599711,
		road: 21389.8801851852,
		acceleration: -1.14046296296296
	},
	{
		id: 2570,
		time: 2569,
		velocity: 2.88305555555556,
		power: -865.919490231523,
		road: 21393.1091203704,
		acceleration: -0.415833333333334
	},
	{
		id: 2571,
		time: 2570,
		velocity: 2.84611111111111,
		power: -1250.48054008105,
		road: 21395.8213888889,
		acceleration: -0.6175
	},
	{
		id: 2572,
		time: 2571,
		velocity: 1.48138888888889,
		power: -1075.28751266463,
		road: 21397.8853240741,
		acceleration: -0.679166666666667
	},
	{
		id: 2573,
		time: 2572,
		velocity: 0.845555555555556,
		power: -972.034284832471,
		road: 21399.1353240741,
		acceleration: -0.948703703703703
	},
	{
		id: 2574,
		time: 2573,
		velocity: 0,
		power: -183.447695871036,
		road: 21399.6640740741,
		acceleration: -0.493796296296296
	},
	{
		id: 2575,
		time: 2574,
		velocity: 0,
		power: 126.250103083115,
		road: 21400.053287037,
		acceleration: 0.214722222222222
	},
	{
		id: 2576,
		time: 2575,
		velocity: 1.48972222222222,
		power: 952.815400449258,
		road: 21401.0107407407,
		acceleration: 0.92175925925926
	},
	{
		id: 2577,
		time: 2576,
		velocity: 2.76527777777778,
		power: 1388.14422614793,
		road: 21402.7784722222,
		acceleration: 0.698796296296296
	},
	{
		id: 2578,
		time: 2577,
		velocity: 2.09638888888889,
		power: 51.3458907320162,
		road: 21404.8437962963,
		acceleration: -0.103611111111111
	},
	{
		id: 2579,
		time: 2578,
		velocity: 1.17888888888889,
		power: -905.350349458342,
		road: 21406.5055092593,
		acceleration: -0.703611111111111
	},
	{
		id: 2580,
		time: 2579,
		velocity: 0.654444444444444,
		power: -519.632541706924,
		road: 21407.4660185185,
		acceleration: -0.698796296296296
	},
	{
		id: 2581,
		time: 2580,
		velocity: 0,
		power: -104.253218808296,
		road: 21407.8806481482,
		acceleration: -0.392962962962963
	},
	{
		id: 2582,
		time: 2581,
		velocity: 0,
		power: -9.36387530864197,
		road: 21407.9897222222,
		acceleration: -0.218148148148148
	},
	{
		id: 2583,
		time: 2582,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2584,
		time: 2583,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2585,
		time: 2584,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2586,
		time: 2585,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2587,
		time: 2586,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2588,
		time: 2587,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2589,
		time: 2588,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2590,
		time: 2589,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2591,
		time: 2590,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2592,
		time: 2591,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2593,
		time: 2592,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2594,
		time: 2593,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2595,
		time: 2594,
		velocity: 0,
		power: 0,
		road: 21407.9897222222,
		acceleration: 0
	},
	{
		id: 2596,
		time: 2595,
		velocity: 0,
		power: 112.414781774822,
		road: 21408.2034722222,
		acceleration: 0.4275
	},
	{
		id: 2597,
		time: 2596,
		velocity: 1.2825,
		power: 879.892367849276,
		road: 21409.0903240741,
		acceleration: 0.918703703703704
	},
	{
		id: 2598,
		time: 2597,
		velocity: 2.75611111111111,
		power: 2952.46740880607,
		road: 21411.1333333333,
		acceleration: 1.39361111111111
	},
	{
		id: 2599,
		time: 2598,
		velocity: 4.18083333333333,
		power: 4425.97513486198,
		road: 21414.4988888889,
		acceleration: 1.25148148148148
	},
	{
		id: 2600,
		time: 2599,
		velocity: 5.03694444444444,
		power: 4861.85473763733,
		road: 21418.9905555556,
		acceleration: 1.00074074074074
	},
	{
		id: 2601,
		time: 2600,
		velocity: 5.75833333333333,
		power: 5382.21318400627,
		road: 21424.4310185185,
		acceleration: 0.896851851851852
	},
	{
		id: 2602,
		time: 2601,
		velocity: 6.87138888888889,
		power: 8634.49295341863,
		road: 21430.9415277778,
		acceleration: 1.24324074074074
	},
	{
		id: 2603,
		time: 2602,
		velocity: 8.76666666666667,
		power: 10655.5472469249,
		road: 21438.7132407407,
		acceleration: 1.27916666666667
	},
	{
		id: 2604,
		time: 2603,
		velocity: 9.59583333333333,
		power: 8561.4417761704,
		road: 21447.5476851852,
		acceleration: 0.846296296296298
	},
	{
		id: 2605,
		time: 2604,
		velocity: 9.41027777777778,
		power: 3817.05533773836,
		road: 21456.9303240741,
		acceleration: 0.250092592592594
	},
	{
		id: 2606,
		time: 2605,
		velocity: 9.51694444444444,
		power: 1113.8043128992,
		road: 21466.4106481482,
		acceleration: -0.0547222222222228
	},
	{
		id: 2607,
		time: 2606,
		velocity: 9.43166666666667,
		power: 616.475618500982,
		road: 21475.8094444445,
		acceleration: -0.108333333333334
	},
	{
		id: 2608,
		time: 2607,
		velocity: 9.08527777777778,
		power: 109.701048098445,
		road: 21485.0724074074,
		acceleration: -0.163333333333334
	},
	{
		id: 2609,
		time: 2608,
		velocity: 9.02694444444444,
		power: -1680.25383304932,
		road: 21494.0691203704,
		acceleration: -0.369166666666667
	},
	{
		id: 2610,
		time: 2609,
		velocity: 8.32416666666667,
		power: -2408.75316601648,
		road: 21502.6493518519,
		acceleration: -0.463796296296294
	},
	{
		id: 2611,
		time: 2610,
		velocity: 7.69388888888889,
		power: -3838.37386343761,
		road: 21510.6642592593,
		acceleration: -0.666851851851853
	},
	{
		id: 2612,
		time: 2611,
		velocity: 7.02638888888889,
		power: -3511.03576813863,
		road: 21518.015787037,
		acceleration: -0.659907407407407
	},
	{
		id: 2613,
		time: 2612,
		velocity: 6.34444444444444,
		power: -2929.84638947253,
		road: 21524.7315740741,
		acceleration: -0.611574074074074
	},
	{
		id: 2614,
		time: 2613,
		velocity: 5.85916666666667,
		power: -2599.4953498713,
		road: 21530.8436574074,
		acceleration: -0.595833333333334
	},
	{
		id: 2615,
		time: 2614,
		velocity: 5.23888888888889,
		power: -1137.16419978185,
		road: 21536.4790277778,
		acceleration: -0.357592592592592
	},
	{
		id: 2616,
		time: 2615,
		velocity: 5.27166666666667,
		power: -722.085466244044,
		road: 21541.7924537037,
		acceleration: -0.286296296296296
	},
	{
		id: 2617,
		time: 2616,
		velocity: 5.00027777777778,
		power: 784.001114471249,
		road: 21546.9711574074,
		acceleration: 0.0168518518518521
	},
	{
		id: 2618,
		time: 2617,
		velocity: 5.28944444444444,
		power: 269.492436287669,
		road: 21552.1147222222,
		acceleration: -0.0871296296296293
	},
	{
		id: 2619,
		time: 2618,
		velocity: 5.01027777777778,
		power: 139.928436143596,
		road: 21557.1584722222,
		acceleration: -0.1125
	},
	{
		id: 2620,
		time: 2619,
		velocity: 4.66277777777778,
		power: -804.760048572441,
		road: 21561.9880092593,
		acceleration: -0.315925925925927
	},
	{
		id: 2621,
		time: 2620,
		velocity: 4.34166666666667,
		power: -1101.91440457744,
		road: 21566.4605555556,
		acceleration: -0.398055555555555
	},
	{
		id: 2622,
		time: 2621,
		velocity: 3.81611111111111,
		power: -507.134202888945,
		road: 21570.6010648148,
		acceleration: -0.266018518518519
	},
	{
		id: 2623,
		time: 2622,
		velocity: 3.86472222222222,
		power: 65.7751531469286,
		road: 21574.5492592593,
		acceleration: -0.118611111111111
	},
	{
		id: 2624,
		time: 2623,
		velocity: 3.98583333333333,
		power: 574.816950270407,
		road: 21578.4478240741,
		acceleration: 0.0193518518518521
	},
	{
		id: 2625,
		time: 2624,
		velocity: 3.87416666666667,
		power: 340.144393772864,
		road: 21582.334212963,
		acceleration: -0.043703703703704
	},
	{
		id: 2626,
		time: 2625,
		velocity: 3.73361111111111,
		power: -115.442678126333,
		road: 21586.1149537037,
		acceleration: -0.167592592592593
	},
	{
		id: 2627,
		time: 2626,
		velocity: 3.48305555555556,
		power: 23.6313625960582,
		road: 21589.7479166667,
		acceleration: -0.127962962962963
	},
	{
		id: 2628,
		time: 2627,
		velocity: 3.49027777777778,
		power: 141.579960185702,
		road: 21593.2708796296,
		acceleration: -0.0920370370370374
	},
	{
		id: 2629,
		time: 2628,
		velocity: 3.4575,
		power: 61.2699922821629,
		road: 21596.6902777778,
		acceleration: -0.115092592592593
	},
	{
		id: 2630,
		time: 2629,
		velocity: 3.13777777777778,
		power: -247.370494274251,
		road: 21599.9454166667,
		acceleration: -0.213425925925926
	},
	{
		id: 2631,
		time: 2630,
		velocity: 2.85,
		power: -660.289889490444,
		road: 21602.9103240741,
		acceleration: -0.367037037037037
	},
	{
		id: 2632,
		time: 2631,
		velocity: 2.35638888888889,
		power: -481.35503763657,
		road: 21605.529212963,
		acceleration: -0.325
	},
	{
		id: 2633,
		time: 2632,
		velocity: 2.16277777777778,
		power: -492.295881254966,
		road: 21607.8064814815,
		acceleration: -0.358240740740741
	},
	{
		id: 2634,
		time: 2633,
		velocity: 1.77527777777778,
		power: -359.780344717495,
		road: 21609.7418518519,
		acceleration: -0.325555555555556
	},
	{
		id: 2635,
		time: 2634,
		velocity: 1.37972222222222,
		power: -487.331193019382,
		road: 21611.2833333333,
		acceleration: -0.462222222222222
	},
	{
		id: 2636,
		time: 2635,
		velocity: 0.776111111111111,
		power: -445.88471048711,
		road: 21612.2978240741,
		acceleration: -0.591759259259259
	},
	{
		id: 2637,
		time: 2636,
		velocity: 0,
		power: -153.852576088846,
		road: 21612.7864814815,
		acceleration: -0.459907407407407
	},
	{
		id: 2638,
		time: 2637,
		velocity: 0,
		power: -16.0745319363223,
		road: 21612.9158333333,
		acceleration: -0.258703703703704
	},
	{
		id: 2639,
		time: 2638,
		velocity: 0,
		power: 0,
		road: 21612.9158333333,
		acceleration: 0
	},
	{
		id: 2640,
		time: 2639,
		velocity: 0,
		power: 0,
		road: 21612.9158333333,
		acceleration: 0
	},
	{
		id: 2641,
		time: 2640,
		velocity: 0,
		power: 0,
		road: 21612.9158333333,
		acceleration: 0
	},
	{
		id: 2642,
		time: 2641,
		velocity: 0,
		power: 0,
		road: 21612.9158333333,
		acceleration: 0
	},
	{
		id: 2643,
		time: 2642,
		velocity: 0,
		power: 0,
		road: 21612.9158333333,
		acceleration: 0
	},
	{
		id: 2644,
		time: 2643,
		velocity: 0,
		power: 0,
		road: 21612.9158333333,
		acceleration: 0
	},
	{
		id: 2645,
		time: 2644,
		velocity: 0,
		power: 0,
		road: 21612.9158333333,
		acceleration: 0
	},
	{
		id: 2646,
		time: 2645,
		velocity: 0,
		power: 38.3902188600803,
		road: 21613.0298148148,
		acceleration: 0.227962962962963
	},
	{
		id: 2647,
		time: 2646,
		velocity: 0.683888888888889,
		power: 130.548005836941,
		road: 21613.3868981482,
		acceleration: 0.258240740740741
	},
	{
		id: 2648,
		time: 2647,
		velocity: 0.774722222222222,
		power: 210.96804009839,
		road: 21613.9929166667,
		acceleration: 0.23962962962963
	},
	{
		id: 2649,
		time: 2648,
		velocity: 0.718888888888889,
		power: 48.0904319085686,
		road: 21614.6912037037,
		acceleration: -0.0550925925925925
	},
	{
		id: 2650,
		time: 2649,
		velocity: 0.518611111111111,
		power: 38.8184064704851,
		road: 21615.3301388889,
		acceleration: -0.0636111111111111
	},
	{
		id: 2651,
		time: 2650,
		velocity: 0.583888888888889,
		power: 92.1838404157195,
		road: 21615.9516666667,
		acceleration: 0.0287962962962962
	},
	{
		id: 2652,
		time: 2651,
		velocity: 0.805277777777778,
		power: 214.62020011914,
		road: 21616.6793055556,
		acceleration: 0.183425925925926
	},
	{
		id: 2653,
		time: 2652,
		velocity: 1.06888888888889,
		power: 184.516761238621,
		road: 21617.5468981482,
		acceleration: 0.0964814814814814
	},
	{
		id: 2654,
		time: 2653,
		velocity: 0.873333333333333,
		power: 4.89137615980348,
		road: 21618.4018055556,
		acceleration: -0.121851851851852
	},
	{
		id: 2655,
		time: 2654,
		velocity: 0.439722222222222,
		power: -133.40302869863,
		road: 21619.0176388889,
		acceleration: -0.356296296296296
	},
	{
		id: 2656,
		time: 2655,
		velocity: 0,
		power: -45.2683828507381,
		road: 21619.3097685185,
		acceleration: -0.291111111111111
	},
	{
		id: 2657,
		time: 2656,
		velocity: 0,
		power: -1.32222672189733,
		road: 21619.3830555556,
		acceleration: -0.146574074074074
	},
	{
		id: 2658,
		time: 2657,
		velocity: 0,
		power: 0,
		road: 21619.3830555556,
		acceleration: 0
	},
	{
		id: 2659,
		time: 2658,
		velocity: 0,
		power: 0,
		road: 21619.3830555556,
		acceleration: 0
	},
	{
		id: 2660,
		time: 2659,
		velocity: 0,
		power: 0,
		road: 21619.3830555556,
		acceleration: 0
	},
	{
		id: 2661,
		time: 2660,
		velocity: 0,
		power: 0,
		road: 21619.3830555556,
		acceleration: 0
	},
	{
		id: 2662,
		time: 2661,
		velocity: 0,
		power: 0,
		road: 21619.3830555556,
		acceleration: 0
	},
	{
		id: 2663,
		time: 2662,
		velocity: 0,
		power: 0,
		road: 21619.3830555556,
		acceleration: 0
	},
	{
		id: 2664,
		time: 2663,
		velocity: 0,
		power: 1.24514121759629,
		road: 21619.3920833333,
		acceleration: 0.0180555555555556
	},
	{
		id: 2665,
		time: 2664,
		velocity: 0.0541666666666667,
		power: 6.59553340045648,
		road: 21619.4325,
		acceleration: 0.0447222222222222
	},
	{
		id: 2666,
		time: 2665,
		velocity: 0.134166666666667,
		power: 7.58481325914034,
		road: 21619.4952777778,
		acceleration: 0
	},
	{
		id: 2667,
		time: 2666,
		velocity: 0,
		power: 5.57461230820149,
		road: 21619.5490277778,
		acceleration: -0.0180555555555556
	},
	{
		id: 2668,
		time: 2667,
		velocity: 0,
		power: 1.75421739766082,
		road: 21619.5713888889,
		acceleration: -0.0447222222222222
	},
	{
		id: 2669,
		time: 2668,
		velocity: 0,
		power: 0,
		road: 21619.5713888889,
		acceleration: 0
	},
	{
		id: 2670,
		time: 2669,
		velocity: 0,
		power: 0,
		road: 21619.5713888889,
		acceleration: 0
	},
	{
		id: 2671,
		time: 2670,
		velocity: 0,
		power: 0,
		road: 21619.5713888889,
		acceleration: 0
	}
];
export default test8;
