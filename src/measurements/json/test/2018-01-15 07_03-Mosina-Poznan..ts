
export const test4 = [
	{
		id: 1,
		time: 0,
		velocity: 0.6125,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0.821388888888889,
		power: 182.065781266276,
		road: 0.688194444444445,
		acceleration: 0.151388888888889
	},
	{
		id: 3,
		time: 2,
		velocity: 0.857777777777778,
		power: 31.3371454120158,
		road: 1.41106481481481,
		acceleration: -0.0820370370370371
	},
	{
		id: 4,
		time: 3,
		velocity: 0.366388888888889,
		power: 60.6392466331694,
		road: 2.07708333333333,
		acceleration: -0.0316666666666666
	},
	{
		id: 5,
		time: 4,
		velocity: 0.726388888888889,
		power: 67.5525324344051,
		road: 2.71893518518519,
		acceleration: -0.0166666666666667
	},
	{
		id: 6,
		time: 5,
		velocity: 0.807777777777778,
		power: 180.891579437807,
		road: 3.42393518518519,
		acceleration: 0.142962962962963
	},
	{
		id: 7,
		time: 6,
		velocity: 0.795277777777778,
		power: 125.695748848569,
		road: 4.21981481481482,
		acceleration: 0.0387962962962962
	},
	{
		id: 8,
		time: 7,
		velocity: 0.842777777777778,
		power: 116.131591585959,
		road: 5.04537037037037,
		acceleration: 0.0205555555555557
	},
	{
		id: 9,
		time: 8,
		velocity: 0.869444444444444,
		power: 103.119771534301,
		road: 5.88226851851852,
		acceleration: 0.00212962962962959
	},
	{
		id: 10,
		time: 9,
		velocity: 0.801666666666667,
		power: -1.86940263268952,
		road: 6.6550462962963,
		acceleration: -0.13037037037037
	},
	{
		id: 11,
		time: 10,
		velocity: 0.451666666666667,
		power: 20.2990374928693,
		road: 7.315,
		acceleration: -0.0952777777777777
	},
	{
		id: 12,
		time: 11,
		velocity: 0.583611111111111,
		power: -30.3095564526838,
		road: 7.83259259259259,
		acceleration: -0.189444444444444
	},
	{
		id: 13,
		time: 12,
		velocity: 0.233333333333333,
		power: 74.3927230763292,
		road: 8.27949074074074,
		acceleration: 0.0480555555555556
	},
	{
		id: 14,
		time: 13,
		velocity: 0.595833333333333,
		power: 9.03682513827333,
		road: 8.69800925925926,
		acceleration: -0.104814814814815
	},
	{
		id: 15,
		time: 14,
		velocity: 0.269166666666667,
		power: 119.161891350637,
		road: 9.14194444444444,
		acceleration: 0.155648148148148
	},
	{
		id: 16,
		time: 15,
		velocity: 0.700277777777778,
		power: 40.3808009799674,
		road: 9.6424537037037,
		acceleration: -0.0425
	},
	{
		id: 17,
		time: 16,
		velocity: 0.468333333333333,
		power: 105.63986533768,
		road: 10.1646296296296,
		acceleration: 0.0858333333333333
	},
	{
		id: 18,
		time: 17,
		velocity: 0.526666666666667,
		power: 81.111893737369,
		road: 10.7402314814815,
		acceleration: 0.0210185185185185
	},
	{
		id: 19,
		time: 18,
		velocity: 0.763333333333333,
		power: 27.0769499229308,
		road: 11.2885648148148,
		acceleration: -0.0755555555555556
	},
	{
		id: 20,
		time: 19,
		velocity: 0.241666666666667,
		power: 82.3035221657717,
		road: 11.8174074074074,
		acceleration: 0.0365740740740741
	},
	{
		id: 21,
		time: 20,
		velocity: 0.636388888888889,
		power: 3.48019245498737,
		road: 12.3044907407407,
		acceleration: -0.120092592592593
	},
	{
		id: 22,
		time: 21,
		velocity: 0.403055555555556,
		power: 118.44267250471,
		road: 12.7950925925926,
		acceleration: 0.12712962962963
	},
	{
		id: 23,
		time: 22,
		velocity: 0.623055555555555,
		power: -4.58130219538187,
		road: 13.280462962963,
		acceleration: -0.137592592592593
	},
	{
		id: 24,
		time: 23,
		velocity: 0.223611111111111,
		power: 27.3976656275199,
		road: 13.6703240740741,
		acceleration: -0.053425925925926
	},
	{
		id: 25,
		time: 24,
		velocity: 0.242777777777778,
		power: 17.4623902078564,
		road: 13.9978240740741,
		acceleration: -0.0712962962962962
	},
	{
		id: 26,
		time: 25,
		velocity: 0.409166666666667,
		power: 108.909662935717,
		road: 14.3773148148148,
		acceleration: 0.175277777777778
	},
	{
		id: 27,
		time: 26,
		velocity: 0.749444444444444,
		power: 143.069115319243,
		road: 14.9197685185185,
		acceleration: 0.150648148148148
	},
	{
		id: 28,
		time: 27,
		velocity: 0.694722222222222,
		power: 86.5271546223429,
		road: 15.5465277777778,
		acceleration: 0.0179629629629628
	},
	{
		id: 29,
		time: 28,
		velocity: 0.463055555555556,
		power: 9.16297292816399,
		road: 16.1267592592593,
		acceleration: -0.111018518518518
	},
	{
		id: 30,
		time: 29,
		velocity: 0.416388888888889,
		power: 4.46966587520601,
		road: 16.5927314814815,
		acceleration: -0.1175
	},
	{
		id: 31,
		time: 30,
		velocity: 0.342222222222222,
		power: 38.7247230845785,
		road: 16.9878703703704,
		acceleration: -0.0241666666666667
	},
	{
		id: 32,
		time: 31,
		velocity: 0.390555555555556,
		power: 38.8937286079611,
		road: 17.3619907407407,
		acceleration: -0.0178703703703703
	},
	{
		id: 33,
		time: 32,
		velocity: 0.362777777777778,
		power: 51.6817040056986,
		road: 17.73625,
		acceleration: 0.0181481481481481
	},
	{
		id: 34,
		time: 33,
		velocity: 0.396666666666667,
		power: 47.5643777909146,
		road: 18.1210185185185,
		acceleration: 0.00287037037037036
	},
	{
		id: 35,
		time: 34,
		velocity: 0.399166666666667,
		power: 60.9262564090482,
		road: 18.5233333333333,
		acceleration: 0.0322222222222223
	},
	{
		id: 36,
		time: 35,
		velocity: 0.459444444444444,
		power: 292.449914045917,
		road: 19.1315740740741,
		acceleration: 0.379629629629629
	},
	{
		id: 37,
		time: 36,
		velocity: 1.53555555555556,
		power: 542.507053927279,
		road: 20.1473148148148,
		acceleration: 0.435370370370371
	},
	{
		id: 38,
		time: 37,
		velocity: 1.70527777777778,
		power: 587.208944241739,
		road: 21.5389814814815,
		acceleration: 0.316481481481481
	},
	{
		id: 39,
		time: 38,
		velocity: 1.40888888888889,
		power: 210.647281922935,
		road: 23.0958333333333,
		acceleration: 0.0138888888888888
	},
	{
		id: 40,
		time: 39,
		velocity: 1.57722222222222,
		power: -236.879913202047,
		road: 24.5068055555556,
		acceleration: -0.305648148148148
	},
	{
		id: 41,
		time: 40,
		velocity: 0.788333333333333,
		power: -243.120049123722,
		road: 25.5815740740741,
		acceleration: -0.366759259259259
	},
	{
		id: 42,
		time: 41,
		velocity: 0.308611111111111,
		power: -237.064426064195,
		road: 26.2100925925926,
		acceleration: -0.525740740740741
	},
	{
		id: 43,
		time: 42,
		velocity: 0,
		power: -30.0141698158861,
		road: 26.4443518518518,
		acceleration: -0.262777777777778
	},
	{
		id: 44,
		time: 43,
		velocity: 0,
		power: 12.2253101637991,
		road: 26.5465740740741,
		acceleration: -0.0012962962962963
	},
	{
		id: 45,
		time: 44,
		velocity: 0.304722222222222,
		power: 12.2725347011925,
		road: 26.6481481481481,
		acceleration: 0
	},
	{
		id: 46,
		time: 45,
		velocity: 0,
		power: 12.2725347011925,
		road: 26.7497222222222,
		acceleration: 0
	},
	{
		id: 47,
		time: 46,
		velocity: 0,
		power: 75.9938221989257,
		road: 26.9697685185185,
		acceleration: 0.236944444444444
	},
	{
		id: 48,
		time: 47,
		velocity: 1.01555555555556,
		power: 514.348166775221,
		road: 27.645787037037,
		acceleration: 0.675
	},
	{
		id: 49,
		time: 48,
		velocity: 2.025,
		power: 1008.54198656626,
		road: 28.9905555555555,
		acceleration: 0.6625
	},
	{
		id: 50,
		time: 49,
		velocity: 1.9875,
		power: -299.713575369292,
		road: 30.4973148148148,
		acceleration: -0.338518518518518
	},
	{
		id: 51,
		time: 50,
		velocity: 0,
		power: -363.52533740221,
		road: 31.59625,
		acceleration: -0.47712962962963
	},
	{
		id: 52,
		time: 51,
		velocity: 0.593611111111111,
		power: -179.114058995729,
		road: 32.2476851851852,
		acceleration: -0.417870370370371
	},
	{
		id: 53,
		time: 52,
		velocity: 0.733888888888889,
		power: 253.463034404847,
		road: 32.8487962962963,
		acceleration: 0.317222222222222
	},
	{
		id: 54,
		time: 53,
		velocity: 0.951666666666667,
		power: 800.443570489012,
		road: 33.9336111111111,
		acceleration: 0.650185185185185
	},
	{
		id: 55,
		time: 54,
		velocity: 2.54416666666667,
		power: 1486.04671699895,
		road: 35.717962962963,
		acceleration: 0.748888888888889
	},
	{
		id: 56,
		time: 55,
		velocity: 2.98055555555556,
		power: 2580.86908668327,
		road: 38.3314814814815,
		acceleration: 0.909444444444445
	},
	{
		id: 57,
		time: 56,
		velocity: 3.68,
		power: 1376.04611605799,
		road: 41.5577777777778,
		acceleration: 0.316111111111111
	},
	{
		id: 58,
		time: 57,
		velocity: 3.4925,
		power: 1075.91538981283,
		road: 45.0378703703704,
		acceleration: 0.191481481481481
	},
	{
		id: 59,
		time: 58,
		velocity: 3.555,
		power: 235.188891658982,
		road: 48.5814351851852,
		acceleration: -0.0645370370370371
	},
	{
		id: 60,
		time: 59,
		velocity: 3.48638888888889,
		power: 395.840596925682,
		road: 52.0850925925926,
		acceleration: -0.0152777777777779
	},
	{
		id: 61,
		time: 60,
		velocity: 3.44666666666667,
		power: 211.609263055421,
		road: 55.54625,
		acceleration: -0.069722222222222
	},
	{
		id: 62,
		time: 61,
		velocity: 3.34583333333333,
		power: 325.27602224006,
		road: 58.9558333333333,
		acceleration: -0.0334259259259264
	},
	{
		id: 63,
		time: 62,
		velocity: 3.38611111111111,
		power: 82.3379186710764,
		road: 62.2948611111111,
		acceleration: -0.107685185185185
	},
	{
		id: 64,
		time: 63,
		velocity: 3.12361111111111,
		power: 1107.17414861562,
		road: 65.6851388888889,
		acceleration: 0.210185185185185
	},
	{
		id: 65,
		time: 64,
		velocity: 3.97638888888889,
		power: 1444.54632783644,
		road: 69.3222685185185,
		acceleration: 0.283518518518518
	},
	{
		id: 66,
		time: 65,
		velocity: 4.23666666666667,
		power: 3048.42848873662,
		road: 73.4240277777778,
		acceleration: 0.64574074074074
	},
	{
		id: 67,
		time: 66,
		velocity: 5.06083333333333,
		power: 1993.0083564387,
		road: 78.0079166666666,
		acceleration: 0.31851851851852
	},
	{
		id: 68,
		time: 67,
		velocity: 4.93194444444444,
		power: 1484.47964794096,
		road: 82.8424074074074,
		acceleration: 0.182685185185185
	},
	{
		id: 69,
		time: 68,
		velocity: 4.78472222222222,
		power: 618.61985367589,
		road: 87.7638888888889,
		acceleration: -0.00870370370370299
	},
	{
		id: 70,
		time: 69,
		velocity: 5.03472222222222,
		power: 2375.17356719446,
		road: 92.8555092592592,
		acceleration: 0.348981481481481
	},
	{
		id: 71,
		time: 70,
		velocity: 5.97888888888889,
		power: 3855.00676549337,
		road: 98.4140740740741,
		acceleration: 0.584907407407408
	},
	{
		id: 72,
		time: 71,
		velocity: 6.53944444444444,
		power: 5030.5124950225,
		road: 104.617037037037,
		acceleration: 0.70388888888889
	},
	{
		id: 73,
		time: 72,
		velocity: 7.14638888888889,
		power: 3362.79949186978,
		road: 111.357731481481,
		acceleration: 0.371574074074073
	},
	{
		id: 74,
		time: 73,
		velocity: 7.09361111111111,
		power: 5857.37018787538,
		road: 118.628981481481,
		acceleration: 0.689537037037037
	},
	{
		id: 75,
		time: 74,
		velocity: 8.60805555555556,
		power: 6825.85411622335,
		road: 126.6125,
		acceleration: 0.734999999999999
	},
	{
		id: 76,
		time: 75,
		velocity: 9.35138888888889,
		power: 7234.25241066514,
		road: 135.315,
		acceleration: 0.702962962962964
	},
	{
		id: 77,
		time: 76,
		velocity: 9.2025,
		power: 1943.61049173883,
		road: 144.394444444444,
		acceleration: 0.0509259259259256
	},
	{
		id: 78,
		time: 77,
		velocity: 8.76083333333333,
		power: -499.432081587703,
		road: 153.383703703704,
		acceleration: -0.231296296296295
	},
	{
		id: 79,
		time: 78,
		velocity: 8.6575,
		power: -2846.44796653117,
		road: 161.999166666667,
		acceleration: -0.516296296296296
	},
	{
		id: 80,
		time: 79,
		velocity: 7.65361111111111,
		power: -3702.26263235713,
		road: 170.032453703704,
		acceleration: -0.648055555555556
	},
	{
		id: 81,
		time: 80,
		velocity: 6.81666666666667,
		power: -6138.85068764028,
		road: 177.21412037037,
		acceleration: -1.05518518518519
	},
	{
		id: 82,
		time: 81,
		velocity: 5.49194444444445,
		power: -4053.35035197958,
		road: 183.451759259259,
		acceleration: -0.83287037037037
	},
	{
		id: 83,
		time: 82,
		velocity: 5.155,
		power: -3730.22806447665,
		road: 188.836574074074,
		acceleration: -0.872777777777779
	},
	{
		id: 84,
		time: 83,
		velocity: 4.19833333333333,
		power: -328.706864497736,
		road: 193.678981481481,
		acceleration: -0.212037037037036
	},
	{
		id: 85,
		time: 84,
		velocity: 4.85583333333333,
		power: -1090.4534456249,
		road: 198.219444444444,
		acceleration: -0.391851851851853
	},
	{
		id: 86,
		time: 85,
		velocity: 3.97944444444444,
		power: 3923.63419129801,
		road: 202.932222222222,
		acceleration: 0.736481481481483
	},
	{
		id: 87,
		time: 86,
		velocity: 6.40777777777778,
		power: 4096.95077306732,
		road: 208.339907407407,
		acceleration: 0.653333333333332
	},
	{
		id: 88,
		time: 87,
		velocity: 6.81583333333333,
		power: 8290.05235293334,
		road: 214.686064814815,
		acceleration: 1.22361111111111
	},
	{
		id: 89,
		time: 88,
		velocity: 7.65027777777778,
		power: 6451.34333996728,
		road: 222.026990740741,
		acceleration: 0.765925925925926
	},
	{
		id: 90,
		time: 89,
		velocity: 8.70555555555556,
		power: 8122.07003461436,
		road: 230.190972222222,
		acceleration: 0.880185185185185
	},
	{
		id: 91,
		time: 90,
		velocity: 9.45638888888889,
		power: 8532.08025792405,
		road: 239.20537037037,
		acceleration: 0.820648148148148
	},
	{
		id: 92,
		time: 91,
		velocity: 10.1122222222222,
		power: 8355.84265626506,
		road: 248.987685185185,
		acceleration: 0.715185185185186
	},
	{
		id: 93,
		time: 92,
		velocity: 10.8511111111111,
		power: 7861.81902513707,
		road: 259.428194444444,
		acceleration: 0.601203703703703
	},
	{
		id: 94,
		time: 93,
		velocity: 11.26,
		power: 6252.16445934396,
		road: 270.371481481481,
		acceleration: 0.40435185185185
	},
	{
		id: 95,
		time: 94,
		velocity: 11.3252777777778,
		power: 2714.06192238304,
		road: 281.545416666667,
		acceleration: 0.0569444444444454
	},
	{
		id: 96,
		time: 95,
		velocity: 11.0219444444444,
		power: 244.768153332324,
		road: 292.660833333333,
		acceleration: -0.173981481481482
	},
	{
		id: 97,
		time: 96,
		velocity: 10.7380555555556,
		power: -834.016800716552,
		road: 303.551944444444,
		acceleration: -0.274629629629629
	},
	{
		id: 98,
		time: 97,
		velocity: 10.5013888888889,
		power: 852.841764758271,
		road: 314.251574074074,
		acceleration: -0.108333333333334
	},
	{
		id: 99,
		time: 98,
		velocity: 10.6969444444444,
		power: 2393.09074679971,
		road: 324.918935185185,
		acceleration: 0.0437962962962981
	},
	{
		id: 100,
		time: 99,
		velocity: 10.8694444444444,
		power: 4016.97364788054,
		road: 335.706990740741,
		acceleration: 0.197592592592594
	},
	{
		id: 101,
		time: 100,
		velocity: 11.0941666666667,
		power: 3721.78789758886,
		road: 346.674212962963,
		acceleration: 0.16074074074074
	},
	{
		id: 102,
		time: 101,
		velocity: 11.1791666666667,
		power: 4256.85510379725,
		road: 357.823287037037,
		acceleration: 0.202962962962962
	},
	{
		id: 103,
		time: 102,
		velocity: 11.4783333333333,
		power: 3291.38797299024,
		road: 369.126805555556,
		acceleration: 0.105925925925925
	},
	{
		id: 104,
		time: 103,
		velocity: 11.4119444444444,
		power: 1281.65339323199,
		road: 380.44287037037,
		acceleration: -0.0808333333333326
	},
	{
		id: 105,
		time: 104,
		velocity: 10.9366666666667,
		power: -640.846094572002,
		road: 391.58962962963,
		acceleration: -0.257777777777779
	},
	{
		id: 106,
		time: 105,
		velocity: 10.705,
		power: -1985.62369953521,
		road: 402.414537037037,
		acceleration: -0.385925925925926
	},
	{
		id: 107,
		time: 106,
		velocity: 10.2541666666667,
		power: -3249.89240572203,
		road: 412.788055555556,
		acceleration: -0.516851851851852
	},
	{
		id: 108,
		time: 107,
		velocity: 9.38611111111111,
		power: -3253.27855751212,
		road: 422.63875,
		acceleration: -0.528796296296296
	},
	{
		id: 109,
		time: 108,
		velocity: 9.11861111111111,
		power: -2723.79246738102,
		road: 431.983703703704,
		acceleration: -0.482685185185183
	},
	{
		id: 110,
		time: 109,
		velocity: 8.80611111111111,
		power: 1332.89127891218,
		road: 441.077268518519,
		acceleration: -0.0200925925925937
	},
	{
		id: 111,
		time: 110,
		velocity: 9.32583333333333,
		power: 5390.35603908423,
		road: 450.376990740741,
		acceleration: 0.432407407407407
	},
	{
		id: 112,
		time: 111,
		velocity: 10.4158333333333,
		power: 9918.76585772565,
		road: 460.324537037037,
		acceleration: 0.863240740740743
	},
	{
		id: 113,
		time: 112,
		velocity: 11.3958333333333,
		power: 11140.0912243265,
		road: 471.146851851852,
		acceleration: 0.886296296296294
	},
	{
		id: 114,
		time: 113,
		velocity: 11.9847222222222,
		power: 11288.29254781,
		road: 482.817268518519,
		acceleration: 0.809907407407408
	},
	{
		id: 115,
		time: 114,
		velocity: 12.8455555555556,
		power: 11754.8981584979,
		road: 495.279398148148,
		acceleration: 0.773518518518518
	},
	{
		id: 116,
		time: 115,
		velocity: 13.7163888888889,
		power: 12244.2690817999,
		road: 508.50037037037,
		acceleration: 0.744166666666668
	},
	{
		id: 117,
		time: 116,
		velocity: 14.2172222222222,
		power: 10882.0540465684,
		road: 522.385694444444,
		acceleration: 0.584537037037034
	},
	{
		id: 118,
		time: 117,
		velocity: 14.5991666666667,
		power: 8490.47448003053,
		road: 536.750787037037,
		acceleration: 0.375000000000004
	},
	{
		id: 119,
		time: 118,
		velocity: 14.8413888888889,
		power: 7219.31343262703,
		road: 551.436157407407,
		acceleration: 0.265555555555554
	},
	{
		id: 120,
		time: 119,
		velocity: 15.0138888888889,
		power: 5003.90094554609,
		road: 566.304398148148,
		acceleration: 0.100185185185182
	},
	{
		id: 121,
		time: 120,
		velocity: 14.8997222222222,
		power: 3701.48575734966,
		road: 581.226064814815,
		acceleration: 0.00666666666666771
	},
	{
		id: 122,
		time: 121,
		velocity: 14.8613888888889,
		power: 2662.1285596348,
		road: 596.11837962963,
		acceleration: -0.0653703703703687
	},
	{
		id: 123,
		time: 122,
		velocity: 14.8177777777778,
		power: 2120.41505866665,
		road: 610.927407407407,
		acceleration: -0.101203703703705
	},
	{
		id: 124,
		time: 123,
		velocity: 14.5961111111111,
		power: 790.237190392988,
		road: 625.589722222222,
		acceleration: -0.192222222222224
	},
	{
		id: 125,
		time: 124,
		velocity: 14.2847222222222,
		power: -817.844197402849,
		road: 640.003935185185,
		acceleration: -0.303981481481481
	},
	{
		id: 126,
		time: 125,
		velocity: 13.9058333333333,
		power: -1967.19592628016,
		road: 654.073425925926,
		acceleration: -0.385462962962961
	},
	{
		id: 127,
		time: 126,
		velocity: 13.4397222222222,
		power: -2579.79599136185,
		road: 667.734953703704,
		acceleration: -0.430462962962963
	},
	{
		id: 128,
		time: 127,
		velocity: 12.9933333333333,
		power: -3040.22611999947,
		road: 680.947777777778,
		acceleration: -0.466944444444444
	},
	{
		id: 129,
		time: 128,
		velocity: 12.505,
		power: -2020.15509344185,
		road: 693.734537037037,
		acceleration: -0.385185185185186
	},
	{
		id: 130,
		time: 129,
		velocity: 12.2841666666667,
		power: -2859.25364041558,
		road: 706.100694444445,
		acceleration: -0.456018518518519
	},
	{
		id: 131,
		time: 130,
		velocity: 11.6252777777778,
		power: -6460.49062157458,
		road: 717.847870370371,
		acceleration: -0.781944444444443
	},
	{
		id: 132,
		time: 131,
		velocity: 10.1591666666667,
		power: -9999.00749424741,
		road: 728.620694444445,
		acceleration: -1.16675925925926
	},
	{
		id: 133,
		time: 132,
		velocity: 8.78388888888889,
		power: -13048.023207771,
		road: 737.990462962963,
		acceleration: -1.63935185185185
	},
	{
		id: 134,
		time: 133,
		velocity: 6.70722222222222,
		power: -10727.1792040177,
		road: 745.731666666667,
		acceleration: -1.61777777777778
	},
	{
		id: 135,
		time: 134,
		velocity: 5.30583333333333,
		power: -9392.90625536343,
		road: 751.771898148148,
		acceleration: -1.78416666666667
	},
	{
		id: 136,
		time: 135,
		velocity: 3.43138888888889,
		power: -3064.8080660114,
		road: 756.509490740741,
		acceleration: -0.821111111111111
	},
	{
		id: 137,
		time: 136,
		velocity: 4.24388888888889,
		power: -1805.87801381963,
		road: 760.531851851852,
		acceleration: -0.609351851851853
	},
	{
		id: 138,
		time: 137,
		velocity: 3.47777777777778,
		power: 984.70905427572,
		road: 764.31875,
		acceleration: 0.138425925925926
	},
	{
		id: 139,
		time: 138,
		velocity: 3.84666666666667,
		power: 885.454409105581,
		road: 768.226203703704,
		acceleration: 0.102685185185186
	},
	{
		id: 140,
		time: 139,
		velocity: 4.55194444444444,
		power: 5828.80959184205,
		road: 772.787962962963,
		acceleration: 1.20592592592593
	},
	{
		id: 141,
		time: 140,
		velocity: 7.09555555555556,
		power: 10408.9100897051,
		road: 778.790694444445,
		acceleration: 1.67601851851852
	},
	{
		id: 142,
		time: 141,
		velocity: 8.87472222222222,
		power: 13709.98459729,
		road: 786.486898148148,
		acceleration: 1.71092592592593
	},
	{
		id: 143,
		time: 142,
		velocity: 9.68472222222222,
		power: 10072.7614682594,
		road: 795.536157407408,
		acceleration: 0.995185185185186
	},
	{
		id: 144,
		time: 143,
		velocity: 10.0811111111111,
		power: 8652.85268644181,
		road: 805.44962962963,
		acceleration: 0.73324074074074
	},
	{
		id: 145,
		time: 144,
		velocity: 11.0744444444444,
		power: 8953.76594214322,
		road: 816.076157407408,
		acceleration: 0.692870370370372
	},
	{
		id: 146,
		time: 145,
		velocity: 11.7633333333333,
		power: 9954.3036575446,
		road: 827.409722222223,
		acceleration: 0.721203703703702
	},
	{
		id: 147,
		time: 146,
		velocity: 12.2447222222222,
		power: 8668.86926733923,
		road: 839.379398148148,
		acceleration: 0.551018518518518
	},
	{
		id: 148,
		time: 147,
		velocity: 12.7275,
		power: 5778.04998313753,
		road: 851.762175925926,
		acceleration: 0.275185185185185
	},
	{
		id: 149,
		time: 148,
		velocity: 12.5888888888889,
		power: 3545.27006440969,
		road: 864.322268518519,
		acceleration: 0.0794444444444444
	},
	{
		id: 150,
		time: 149,
		velocity: 12.4830555555556,
		power: 580.71595457211,
		road: 876.838518518519,
		acceleration: -0.167129629629629
	},
	{
		id: 151,
		time: 150,
		velocity: 12.2261111111111,
		power: -1385.22160601956,
		road: 889.10587962963,
		acceleration: -0.33064814814815
	},
	{
		id: 152,
		time: 151,
		velocity: 11.5969444444444,
		power: -1628.99872290683,
		road: 901.0325,
		acceleration: -0.350833333333332
	},
	{
		id: 153,
		time: 152,
		velocity: 11.4305555555556,
		power: -5347.14896280977,
		road: 912.437453703704,
		acceleration: -0.692499999999999
	},
	{
		id: 154,
		time: 153,
		velocity: 10.1486111111111,
		power: -5858.33519687396,
		road: 923.112407407408,
		acceleration: -0.767500000000002
	},
	{
		id: 155,
		time: 154,
		velocity: 9.29444444444444,
		power: -4763.55825833497,
		road: 933.060694444445,
		acceleration: -0.685833333333331
	},
	{
		id: 156,
		time: 155,
		velocity: 9.37305555555556,
		power: 897.603487822768,
		road: 942.625833333334,
		acceleration: -0.0804629629629652
	},
	{
		id: 157,
		time: 156,
		velocity: 9.90722222222222,
		power: 3129.70221372445,
		road: 952.232037037037,
		acceleration: 0.162592592592594
	},
	{
		id: 158,
		time: 157,
		velocity: 9.78222222222222,
		power: 4567.26287339969,
		road: 962.07212962963,
		acceleration: 0.305185185185186
	},
	{
		id: 159,
		time: 158,
		velocity: 10.2886111111111,
		power: 5486.13228920524,
		road: 972.254583333334,
		acceleration: 0.379537037037036
	},
	{
		id: 160,
		time: 159,
		velocity: 11.0458333333333,
		power: 6786.39930747833,
		road: 982.866805555556,
		acceleration: 0.48
	},
	{
		id: 161,
		time: 160,
		velocity: 11.2222222222222,
		power: 6397.17513575932,
		road: 993.92425925926,
		acceleration: 0.410462962962962
	},
	{
		id: 162,
		time: 161,
		velocity: 11.52,
		power: 3716.13468186685,
		road: 1005.25893518519,
		acceleration: 0.143981481481482
	},
	{
		id: 163,
		time: 162,
		velocity: 11.4777777777778,
		power: 3614.22899610044,
		road: 1016.73,
		acceleration: 0.128796296296295
	},
	{
		id: 164,
		time: 163,
		velocity: 11.6086111111111,
		power: 2726.44214445849,
		road: 1028.28777777778,
		acceleration: 0.0446296296296307
	},
	{
		id: 165,
		time: 164,
		velocity: 11.6538888888889,
		power: 4511.54181400597,
		road: 1039.96819444444,
		acceleration: 0.200648148148147
	},
	{
		id: 166,
		time: 165,
		velocity: 12.0797222222222,
		power: 3198.07343835058,
		road: 1051.78768518519,
		acceleration: 0.0775000000000006
	},
	{
		id: 167,
		time: 166,
		velocity: 11.8411111111111,
		power: 2515.62801749587,
		road: 1063.65365740741,
		acceleration: 0.0154629629629639
	},
	{
		id: 168,
		time: 167,
		velocity: 11.7002777777778,
		power: 1405.24851559671,
		road: 1075.48643518519,
		acceleration: -0.0818518518518516
	},
	{
		id: 169,
		time: 168,
		velocity: 11.8341666666667,
		power: 1373.80607920809,
		road: 1087.23694444444,
		acceleration: -0.082685185185186
	},
	{
		id: 170,
		time: 169,
		velocity: 11.5930555555556,
		power: 2689.98340875623,
		road: 1098.96388888889,
		acceleration: 0.0355555555555558
	},
	{
		id: 171,
		time: 170,
		velocity: 11.8069444444444,
		power: 1771.86015734832,
		road: 1110.68541666667,
		acceleration: -0.0463888888888899
	},
	{
		id: 172,
		time: 171,
		velocity: 11.695,
		power: 5729.18247357594,
		road: 1122.53393518518,
		acceleration: 0.300370370370372
	},
	{
		id: 173,
		time: 172,
		velocity: 12.4941666666667,
		power: 5743.93454366272,
		road: 1134.67532407407,
		acceleration: 0.285370370370371
	},
	{
		id: 174,
		time: 173,
		velocity: 12.6630555555556,
		power: 8517.70839344931,
		road: 1147.2075462963,
		acceleration: 0.496296296296295
	},
	{
		id: 175,
		time: 174,
		velocity: 13.1838888888889,
		power: 6697.90346691411,
		road: 1160.14814814815,
		acceleration: 0.320462962962962
	},
	{
		id: 176,
		time: 175,
		velocity: 13.4555555555556,
		power: 6076.08865912512,
		road: 1173.37652777778,
		acceleration: 0.255092592592593
	},
	{
		id: 177,
		time: 176,
		velocity: 13.4283333333333,
		power: 4127.6711887862,
		road: 1186.77939814815,
		acceleration: 0.0938888888888894
	},
	{
		id: 178,
		time: 177,
		velocity: 13.4655555555556,
		power: 3774.1253859107,
		road: 1200.26087962963,
		acceleration: 0.0633333333333326
	},
	{
		id: 179,
		time: 178,
		velocity: 13.6455555555556,
		power: 1910.82758799738,
		road: 1213.73342592593,
		acceleration: -0.0812037037037019
	},
	{
		id: 180,
		time: 179,
		velocity: 13.1847222222222,
		power: 153.510984844631,
		road: 1227.05763888889,
		acceleration: -0.215462962962965
	},
	{
		id: 181,
		time: 180,
		velocity: 12.8191666666667,
		power: -2634.74949694341,
		road: 1240.05657407407,
		acceleration: -0.435092592592591
	},
	{
		id: 182,
		time: 181,
		velocity: 12.3402777777778,
		power: -1763.24239218958,
		road: 1252.65615740741,
		acceleration: -0.363611111111112
	},
	{
		id: 183,
		time: 182,
		velocity: 12.0938888888889,
		power: -2488.78674282868,
		road: 1264.86134259259,
		acceleration: -0.425185185185185
	},
	{
		id: 184,
		time: 183,
		velocity: 11.5436111111111,
		power: -1588.70646474829,
		road: 1276.68037037037,
		acceleration: -0.347129629629631
	},
	{
		id: 185,
		time: 184,
		velocity: 11.2988888888889,
		power: -552.095737768595,
		road: 1288.19958333333,
		acceleration: -0.252499999999998
	},
	{
		id: 186,
		time: 185,
		velocity: 11.3363888888889,
		power: 2720.6990371614,
		road: 1299.61703703704,
		acceleration: 0.0489814814814817
	},
	{
		id: 187,
		time: 186,
		velocity: 11.6905555555556,
		power: 5113.93446308045,
		road: 1311.18921296296,
		acceleration: 0.260462962962961
	},
	{
		id: 188,
		time: 187,
		velocity: 12.0802777777778,
		power: 6777.63815376501,
		road: 1323.08662037037,
		acceleration: 0.390000000000001
	},
	{
		id: 189,
		time: 188,
		velocity: 12.5063888888889,
		power: 5895.80895421286,
		road: 1335.32555555556,
		acceleration: 0.293055555555554
	},
	{
		id: 190,
		time: 189,
		velocity: 12.5697222222222,
		power: 3861.7845769927,
		road: 1347.76634259259,
		acceleration: 0.110648148148149
	},
	{
		id: 191,
		time: 190,
		velocity: 12.4122222222222,
		power: 373.967385843847,
		road: 1360.17115740741,
		acceleration: -0.182592592592595
	},
	{
		id: 192,
		time: 191,
		velocity: 11.9586111111111,
		power: -3731.19448025514,
		road: 1372.21768518519,
		acceleration: -0.533981481481479
	},
	{
		id: 193,
		time: 192,
		velocity: 10.9677777777778,
		power: -6718.32376682194,
		road: 1383.58717592593,
		acceleration: -0.820092592592593
	},
	{
		id: 194,
		time: 193,
		velocity: 9.95194444444444,
		power: -9178.44636533956,
		road: 1393.98939814815,
		acceleration: -1.11444444444444
	},
	{
		id: 195,
		time: 194,
		velocity: 8.61527777777778,
		power: -10886.0760543997,
		road: 1403.12115740741,
		acceleration: -1.42648148148148
	},
	{
		id: 196,
		time: 195,
		velocity: 6.68833333333333,
		power: -11127.2822789676,
		road: 1410.68675925926,
		acceleration: -1.70583333333333
	},
	{
		id: 197,
		time: 196,
		velocity: 4.83444444444444,
		power: -10693.1939226554,
		road: 1416.32953703704,
		acceleration: -2.13981481481481
	},
	{
		id: 198,
		time: 197,
		velocity: 2.19583333333333,
		power: -5088.29014043433,
		road: 1420.12925925926,
		acceleration: -1.5462962962963
	},
	{
		id: 199,
		time: 198,
		velocity: 2.04944444444444,
		power: -1931.0048143919,
		road: 1422.69333333333,
		acceleration: -0.925
	},
	{
		id: 200,
		time: 199,
		velocity: 2.05944444444444,
		power: 1056.63172674211,
		road: 1424.97393518519,
		acceleration: 0.358055555555556
	},
	{
		id: 201,
		time: 200,
		velocity: 3.27,
		power: 2238.08604329479,
		road: 1427.78685185185,
		acceleration: 0.706574074074074
	},
	{
		id: 202,
		time: 201,
		velocity: 4.16916666666667,
		power: 5094.75821250792,
		road: 1431.59055555556,
		acceleration: 1.275
	},
	{
		id: 203,
		time: 202,
		velocity: 5.88444444444445,
		power: 4819.43048443275,
		road: 1436.4800462963,
		acceleration: 0.896574074074073
	},
	{
		id: 204,
		time: 203,
		velocity: 5.95972222222222,
		power: 3707.77785486859,
		road: 1442.09277777778,
		acceleration: 0.549907407407408
	},
	{
		id: 205,
		time: 204,
		velocity: 5.81888888888889,
		power: -1452.13902718578,
		road: 1447.77319444444,
		acceleration: -0.414537037037037
	},
	{
		id: 206,
		time: 205,
		velocity: 4.64083333333333,
		power: -3020.24517230439,
		road: 1452.86305555556,
		acceleration: -0.766574074074074
	},
	{
		id: 207,
		time: 206,
		velocity: 3.66,
		power: -4263.11839398418,
		road: 1456.95217592593,
		acceleration: -1.23490740740741
	},
	{
		id: 208,
		time: 207,
		velocity: 2.11416666666667,
		power: -3622.85846824547,
		road: 1459.65037037037,
		acceleration: -1.54694444444444
	},
	{
		id: 209,
		time: 208,
		velocity: 0,
		power: -1360.3456750537,
		road: 1460.96509259259,
		acceleration: -1.22
	},
	{
		id: 210,
		time: 209,
		velocity: 0,
		power: -192.675877339181,
		road: 1461.3174537037,
		acceleration: -0.704722222222222
	},
	{
		id: 211,
		time: 210,
		velocity: 0,
		power: 0,
		road: 1461.3174537037,
		acceleration: 0
	},
	{
		id: 212,
		time: 211,
		velocity: 0,
		power: 0,
		road: 1461.3174537037,
		acceleration: 0
	},
	{
		id: 213,
		time: 212,
		velocity: 0,
		power: 0,
		road: 1461.3174537037,
		acceleration: 0
	},
	{
		id: 214,
		time: 213,
		velocity: 0,
		power: 0,
		road: 1461.3174537037,
		acceleration: 0
	},
	{
		id: 215,
		time: 214,
		velocity: 0,
		power: 77.9154085484516,
		road: 1461.49083333333,
		acceleration: 0.346759259259259
	},
	{
		id: 216,
		time: 215,
		velocity: 1.04027777777778,
		power: 869.015541921625,
		road: 1462.32388888889,
		acceleration: 0.972592592592593
	},
	{
		id: 217,
		time: 216,
		velocity: 2.91777777777778,
		power: 2902.76040945652,
		road: 1464.33800925926,
		acceleration: 1.38953703703704
	},
	{
		id: 218,
		time: 217,
		velocity: 4.16861111111111,
		power: 3239.56381367349,
		road: 1467.51712962963,
		acceleration: 0.940462962962962
	},
	{
		id: 219,
		time: 218,
		velocity: 3.86166666666667,
		power: 1485.95720233458,
		road: 1471.30532407407,
		acceleration: 0.277685185185186
	},
	{
		id: 220,
		time: 219,
		velocity: 3.75083333333333,
		power: -570.623150520339,
		road: 1475.08513888889,
		acceleration: -0.294444444444444
	},
	{
		id: 221,
		time: 220,
		velocity: 3.28527777777778,
		power: -1172.48389672234,
		road: 1478.46824074074,
		acceleration: -0.498981481481481
	},
	{
		id: 222,
		time: 221,
		velocity: 2.36472222222222,
		power: -1959.26365415851,
		road: 1481.15125,
		acceleration: -0.901203703703704
	},
	{
		id: 223,
		time: 222,
		velocity: 1.04722222222222,
		power: -1543.22503215285,
		road: 1482.83611111111,
		acceleration: -1.09509259259259
	},
	{
		id: 224,
		time: 223,
		velocity: 0,
		power: -465.143360361635,
		road: 1483.57930555556,
		acceleration: -0.788240740740741
	},
	{
		id: 225,
		time: 224,
		velocity: 0,
		power: -36.6325069850552,
		road: 1483.75384259259,
		acceleration: -0.349074074074074
	},
	{
		id: 226,
		time: 225,
		velocity: 0,
		power: 0,
		road: 1483.75384259259,
		acceleration: 0
	},
	{
		id: 227,
		time: 226,
		velocity: 0,
		power: 0,
		road: 1483.75384259259,
		acceleration: 0
	},
	{
		id: 228,
		time: 227,
		velocity: 0,
		power: 0,
		road: 1483.75384259259,
		acceleration: 0
	},
	{
		id: 229,
		time: 228,
		velocity: 0,
		power: 9.00569455330046,
		road: 1483.79791666667,
		acceleration: 0.0881481481481481
	},
	{
		id: 230,
		time: 229,
		velocity: 0.264444444444444,
		power: 10.6502455068608,
		road: 1483.88606481481,
		acceleration: 0
	},
	{
		id: 231,
		time: 230,
		velocity: 0,
		power: 10.6502455068608,
		road: 1483.97421296296,
		acceleration: 0
	},
	{
		id: 232,
		time: 231,
		velocity: 0,
		power: 1.64436504223522,
		road: 1484.01828703704,
		acceleration: -0.0881481481481481
	},
	{
		id: 233,
		time: 232,
		velocity: 0,
		power: 0,
		road: 1484.01828703704,
		acceleration: 0
	},
	{
		id: 234,
		time: 233,
		velocity: 0,
		power: 0,
		road: 1484.01828703704,
		acceleration: 0
	},
	{
		id: 235,
		time: 234,
		velocity: 0,
		power: 0,
		road: 1484.01828703704,
		acceleration: 0
	},
	{
		id: 236,
		time: 235,
		velocity: 0,
		power: 0,
		road: 1484.01828703704,
		acceleration: 0
	},
	{
		id: 237,
		time: 236,
		velocity: 0,
		power: 0,
		road: 1484.01828703704,
		acceleration: 0
	},
	{
		id: 238,
		time: 237,
		velocity: 0,
		power: 0,
		road: 1484.01828703704,
		acceleration: 0
	},
	{
		id: 239,
		time: 238,
		velocity: 0,
		power: 0,
		road: 1484.01828703704,
		acceleration: 0
	},
	{
		id: 240,
		time: 239,
		velocity: 0,
		power: 0,
		road: 1484.01828703704,
		acceleration: 0
	},
	{
		id: 241,
		time: 240,
		velocity: 0,
		power: 297.676385521256,
		road: 1484.38398148148,
		acceleration: 0.731388888888889
	},
	{
		id: 242,
		time: 241,
		velocity: 2.19416666666667,
		power: 1726.55267588203,
		road: 1485.72833333333,
		acceleration: 1.22592592592593
	},
	{
		id: 243,
		time: 242,
		velocity: 3.67777777777778,
		power: 3860.84609744434,
		road: 1488.38550925926,
		acceleration: 1.39972222222222
	},
	{
		id: 244,
		time: 243,
		velocity: 4.19916666666667,
		power: 3565.57665297177,
		road: 1492.17083333333,
		acceleration: 0.856574074074074
	},
	{
		id: 245,
		time: 244,
		velocity: 4.76388888888889,
		power: 3263.09678410871,
		road: 1496.69467592593,
		acceleration: 0.620462962962963
	},
	{
		id: 246,
		time: 245,
		velocity: 5.53916666666667,
		power: 3370.83868095778,
		road: 1501.80481481482,
		acceleration: 0.552129629629629
	},
	{
		id: 247,
		time: 246,
		velocity: 5.85555555555556,
		power: 2734.40045986017,
		road: 1507.37675925926,
		acceleration: 0.371481481481482
	},
	{
		id: 248,
		time: 247,
		velocity: 5.87833333333333,
		power: 3346.36704668612,
		road: 1513.35509259259,
		acceleration: 0.441296296296296
	},
	{
		id: 249,
		time: 248,
		velocity: 6.86305555555556,
		power: 4924.64419123509,
		road: 1519.87550925926,
		acceleration: 0.642870370370371
	},
	{
		id: 250,
		time: 249,
		velocity: 7.78416666666667,
		power: 6591.72558758033,
		road: 1527.11726851852,
		acceleration: 0.799814814814814
	},
	{
		id: 251,
		time: 250,
		velocity: 8.27777777777778,
		power: 5464.41110680446,
		road: 1535.03990740741,
		acceleration: 0.561944444444445
	},
	{
		id: 252,
		time: 251,
		velocity: 8.54888888888889,
		power: 2314.17886450497,
		road: 1543.30759259259,
		acceleration: 0.128148148148147
	},
	{
		id: 253,
		time: 252,
		velocity: 8.16861111111111,
		power: 57.7328356915784,
		road: 1551.56013888889,
		acceleration: -0.158425925925926
	},
	{
		id: 254,
		time: 253,
		velocity: 7.8025,
		power: -1272.16690332062,
		road: 1559.56824074074,
		acceleration: -0.330462962962963
	},
	{
		id: 255,
		time: 254,
		velocity: 7.5575,
		power: -724.479245925036,
		road: 1567.2812962963,
		acceleration: -0.25962962962963
	},
	{
		id: 256,
		time: 255,
		velocity: 7.38972222222222,
		power: 337.238218058047,
		road: 1574.80842592593,
		acceleration: -0.112222222222222
	},
	{
		id: 257,
		time: 256,
		velocity: 7.46583333333333,
		power: 780.798222951261,
		road: 1582.25523148148,
		acceleration: -0.0484259259259261
	},
	{
		id: 258,
		time: 257,
		velocity: 7.41222222222222,
		power: 931.313095344163,
		road: 1589.66472222222,
		acceleration: -0.026203703703704
	},
	{
		id: 259,
		time: 258,
		velocity: 7.31111111111111,
		power: 388.917254630298,
		road: 1597.0100462963,
		acceleration: -0.102129629629629
	},
	{
		id: 260,
		time: 259,
		velocity: 7.15944444444444,
		power: -581.332266873535,
		road: 1604.18351851852,
		acceleration: -0.241574074074074
	},
	{
		id: 261,
		time: 260,
		velocity: 6.6875,
		power: -516.468961812218,
		road: 1611.11981481482,
		acceleration: -0.232777777777779
	},
	{
		id: 262,
		time: 261,
		velocity: 6.61277777777778,
		power: -1345.98124056945,
		road: 1617.75699074074,
		acceleration: -0.365462962962963
	},
	{
		id: 263,
		time: 262,
		velocity: 6.06305555555556,
		power: -2674.25353903415,
		road: 1623.90842592593,
		acceleration: -0.606018518518519
	},
	{
		id: 264,
		time: 263,
		velocity: 4.86944444444444,
		power: -4363.77455065909,
		road: 1629.25564814815,
		acceleration: -1.00240740740741
	},
	{
		id: 265,
		time: 264,
		velocity: 3.60555555555556,
		power: -5273.78855688784,
		road: 1633.35583333333,
		acceleration: -1.49166666666667
	},
	{
		id: 266,
		time: 265,
		velocity: 1.58805555555556,
		power: -2809.28543551007,
		road: 1636.10597222222,
		acceleration: -1.20842592592593
	},
	{
		id: 267,
		time: 266,
		velocity: 1.24416666666667,
		power: -189.449989386501,
		road: 1638.13787037037,
		acceleration: -0.228055555555556
	},
	{
		id: 268,
		time: 267,
		velocity: 2.92138888888889,
		power: 2043.34428495584,
		road: 1640.45523148148,
		acceleration: 0.798981481481482
	},
	{
		id: 269,
		time: 268,
		velocity: 3.985,
		power: 5625.18459969343,
		road: 1643.95208333333,
		acceleration: 1.56
	},
	{
		id: 270,
		time: 269,
		velocity: 5.92416666666667,
		power: 5498.39252044851,
		road: 1648.76050925926,
		acceleration: 1.06314814814815
	},
	{
		id: 271,
		time: 270,
		velocity: 6.11083333333333,
		power: 7406.18232327492,
		road: 1654.68444444444,
		acceleration: 1.16787037037037
	},
	{
		id: 272,
		time: 271,
		velocity: 7.48861111111111,
		power: 8801.93112735862,
		road: 1661.76759259259,
		acceleration: 1.15055555555556
	},
	{
		id: 273,
		time: 272,
		velocity: 9.37583333333333,
		power: 12175.4594179157,
		road: 1670.10925925926,
		acceleration: 1.36648148148148
	},
	{
		id: 274,
		time: 273,
		velocity: 10.2102777777778,
		power: 11332.1625755658,
		road: 1679.66699074074,
		acceleration: 1.06564814814815
	},
	{
		id: 275,
		time: 274,
		velocity: 10.6855555555556,
		power: 5609.33252543765,
		road: 1689.95023148148,
		acceleration: 0.385370370370371
	},
	{
		id: 276,
		time: 275,
		velocity: 10.5319444444444,
		power: 1811.95475042007,
		road: 1700.42231481482,
		acceleration: -0.00768518518518491
	},
	{
		id: 277,
		time: 276,
		velocity: 10.1872222222222,
		power: -2.43274865533128,
		road: 1710.79638888889,
		acceleration: -0.188333333333334
	},
	{
		id: 278,
		time: 277,
		velocity: 10.1205555555556,
		power: -234.844540933394,
		road: 1720.9712962963,
		acceleration: -0.209999999999999
	},
	{
		id: 279,
		time: 278,
		velocity: 9.90194444444444,
		power: 1463.97710022175,
		road: 1731.02541666667,
		acceleration: -0.0315740740740758
	},
	{
		id: 280,
		time: 279,
		velocity: 10.0925,
		power: 1040.20417975306,
		road: 1741.02643518519,
		acceleration: -0.07462962962963
	},
	{
		id: 281,
		time: 280,
		velocity: 9.89666666666667,
		power: 1589.24120464359,
		road: 1750.98226851852,
		acceleration: -0.0157407407407391
	},
	{
		id: 282,
		time: 281,
		velocity: 9.85472222222222,
		power: 307.87498942452,
		road: 1760.85541666667,
		acceleration: -0.149629629629629
	},
	{
		id: 283,
		time: 282,
		velocity: 9.64361111111111,
		power: 2152.62680114553,
		road: 1770.67787037037,
		acceleration: 0.0482407407407415
	},
	{
		id: 284,
		time: 283,
		velocity: 10.0413888888889,
		power: 3737.54328786787,
		road: 1780.62990740741,
		acceleration: 0.210925925925924
	},
	{
		id: 285,
		time: 284,
		velocity: 10.4875,
		power: 8050.74338236775,
		road: 1791.00064814815,
		acceleration: 0.626481481481482
	},
	{
		id: 286,
		time: 285,
		velocity: 11.5230555555556,
		power: 11180.0550817552,
		road: 1802.11365740741,
		acceleration: 0.858055555555557
	},
	{
		id: 287,
		time: 286,
		velocity: 12.6155555555556,
		power: 12771.8516918524,
		road: 1814.10944444444,
		acceleration: 0.907499999999999
	},
	{
		id: 288,
		time: 287,
		velocity: 13.21,
		power: 13243.8859731916,
		road: 1826.98726851852,
		acceleration: 0.856574074074071
	},
	{
		id: 289,
		time: 288,
		velocity: 14.0927777777778,
		power: 12369.3783235807,
		road: 1840.65106481482,
		acceleration: 0.715370370370373
	},
	{
		id: 290,
		time: 289,
		velocity: 14.7616666666667,
		power: 12562.3255319546,
		road: 1855.0087037037,
		acceleration: 0.672314814814815
	},
	{
		id: 291,
		time: 290,
		velocity: 15.2269444444444,
		power: 8312.60740337627,
		road: 1869.86925925926,
		acceleration: 0.333518518518519
	},
	{
		id: 292,
		time: 291,
		velocity: 15.0933333333333,
		power: 9556.69142893702,
		road: 1885.09587962963,
		acceleration: 0.398611111111109
	},
	{
		id: 293,
		time: 292,
		velocity: 15.9575,
		power: 9243.78054548585,
		road: 1900.69935185185,
		acceleration: 0.355092592592593
	},
	{
		id: 294,
		time: 293,
		velocity: 16.2922222222222,
		power: 9716.62747221313,
		road: 1916.66314814815,
		acceleration: 0.365555555555554
	},
	{
		id: 295,
		time: 294,
		velocity: 16.19,
		power: 6188.55630112103,
		road: 1932.87162037037,
		acceleration: 0.123796296296298
	},
	{
		id: 296,
		time: 295,
		velocity: 16.3288888888889,
		power: 4766.22407690423,
		road: 1949.15657407408,
		acceleration: 0.0291666666666686
	},
	{
		id: 297,
		time: 296,
		velocity: 16.3797222222222,
		power: 4536.10956784923,
		road: 1965.46291666667,
		acceleration: 0.013611111111107
	},
	{
		id: 298,
		time: 297,
		velocity: 16.2308333333333,
		power: 3774.05285336732,
		road: 1981.75861111111,
		acceleration: -0.0349074074074061
	},
	{
		id: 299,
		time: 298,
		velocity: 16.2241666666667,
		power: 3892.10118291691,
		road: 1998.0237037037,
		acceleration: -0.0262962962962945
	},
	{
		id: 300,
		time: 299,
		velocity: 16.3008333333333,
		power: 3359.80024314759,
		road: 2014.24606481482,
		acceleration: -0.0591666666666661
	},
	{
		id: 301,
		time: 300,
		velocity: 16.0533333333333,
		power: 4277.3701668826,
		road: 2030.43935185185,
		acceleration: 0.0010185185185172
	},
	{
		id: 302,
		time: 301,
		velocity: 16.2272222222222,
		power: 4028.53795392387,
		road: 2046.62574074074,
		acceleration: -0.0148148148148124
	},
	{
		id: 303,
		time: 302,
		velocity: 16.2563888888889,
		power: 5583.75917446722,
		road: 2062.84685185185,
		acceleration: 0.0842592592592588
	},
	{
		id: 304,
		time: 303,
		velocity: 16.3061111111111,
		power: 5377.95113645368,
		road: 2079.14407407408,
		acceleration: 0.0679629629629623
	},
	{
		id: 305,
		time: 304,
		velocity: 16.4311111111111,
		power: 5043.5614815798,
		road: 2095.4974537037,
		acceleration: 0.0443518518518502
	},
	{
		id: 306,
		time: 305,
		velocity: 16.3894444444444,
		power: 4629.58780609443,
		road: 2111.88138888889,
		acceleration: 0.0167592592592598
	},
	{
		id: 307,
		time: 306,
		velocity: 16.3563888888889,
		power: 3116.10530225726,
		road: 2128.23425925926,
		acceleration: -0.0788888888888906
	},
	{
		id: 308,
		time: 307,
		velocity: 16.1944444444444,
		power: 3116.42539091233,
		road: 2144.50944444445,
		acceleration: -0.0764814814814798
	},
	{
		id: 309,
		time: 308,
		velocity: 16.16,
		power: 4427.52151467962,
		road: 2160.75087962963,
		acceleration: 0.00898148148148437
	},
	{
		id: 310,
		time: 309,
		velocity: 16.3833333333333,
		power: 6144.58967094493,
		road: 2177.05523148148,
		acceleration: 0.116851851851848
	},
	{
		id: 311,
		time: 310,
		velocity: 16.545,
		power: 5813.24306891202,
		road: 2193.46365740741,
		acceleration: 0.0912962962962993
	},
	{
		id: 312,
		time: 311,
		velocity: 16.4338888888889,
		power: 2369.30056682635,
		road: 2209.85388888889,
		acceleration: -0.127685185185186
	},
	{
		id: 313,
		time: 312,
		velocity: 16.0002777777778,
		power: 4711.90263837632,
		road: 2226.19212962963,
		acceleration: 0.0237037037037027
	},
	{
		id: 314,
		time: 313,
		velocity: 16.6161111111111,
		power: 3094.4113880304,
		road: 2242.50273148148,
		acceleration: -0.0789814814814811
	},
	{
		id: 315,
		time: 314,
		velocity: 16.1969444444444,
		power: 5088.52523398775,
		road: 2258.79856481482,
		acceleration: 0.0494444444444433
	},
	{
		id: 316,
		time: 315,
		velocity: 16.1486111111111,
		power: 3029.1022556648,
		road: 2275.07800925926,
		acceleration: -0.0822222222222244
	},
	{
		id: 317,
		time: 316,
		velocity: 16.3694444444444,
		power: 5264.12471642638,
		road: 2291.34722222222,
		acceleration: 0.0617592592592615
	},
	{
		id: 318,
		time: 317,
		velocity: 16.3822222222222,
		power: 5402.27228375291,
		road: 2307.68134259259,
		acceleration: 0.0680555555555564
	},
	{
		id: 319,
		time: 318,
		velocity: 16.3527777777778,
		power: 4030.95522805167,
		road: 2324.03921296296,
		acceleration: -0.020555555555557
	},
	{
		id: 320,
		time: 319,
		velocity: 16.3077777777778,
		power: 4267.2311946191,
		road: 2340.38430555556,
		acceleration: -0.00499999999999901
	},
	{
		id: 321,
		time: 320,
		velocity: 16.3672222222222,
		power: 6824.09976174635,
		road: 2356.80449074074,
		acceleration: 0.155185185185182
	},
	{
		id: 322,
		time: 321,
		velocity: 16.8183333333333,
		power: 4358.03246143099,
		road: 2373.3,
		acceleration: -0.00453703703703212
	},
	{
		id: 323,
		time: 322,
		velocity: 16.2941666666667,
		power: 2426.20876373749,
		road: 2389.73064814815,
		acceleration: -0.125185185185188
	},
	{
		id: 324,
		time: 323,
		velocity: 15.9916666666667,
		power: 1598.48811819284,
		road: 2406.01162037037,
		acceleration: -0.174166666666665
	},
	{
		id: 325,
		time: 324,
		velocity: 16.2958333333333,
		power: 8755.71742909913,
		road: 2422.34685185185,
		acceleration: 0.282685185185187
	},
	{
		id: 326,
		time: 325,
		velocity: 17.1422222222222,
		power: 13981.8940278488,
		road: 2439.1162962963,
		acceleration: 0.585740740740739
	},
	{
		id: 327,
		time: 326,
		velocity: 17.7488888888889,
		power: 13973.1532560698,
		road: 2456.45143518519,
		acceleration: 0.54564814814815
	},
	{
		id: 328,
		time: 327,
		velocity: 17.9327777777778,
		power: 9307.63521961178,
		road: 2474.18138888889,
		acceleration: 0.243981481481477
	},
	{
		id: 329,
		time: 328,
		velocity: 17.8741666666667,
		power: 7032.97430669541,
		road: 2492.08453703704,
		acceleration: 0.102407407407409
	},
	{
		id: 330,
		time: 329,
		velocity: 18.0561111111111,
		power: 8409.72797638546,
		road: 2510.12694444445,
		acceleration: 0.176111111111112
	},
	{
		id: 331,
		time: 330,
		velocity: 18.4611111111111,
		power: 13832.3154949481,
		road: 2528.49203703704,
		acceleration: 0.46925925925926
	},
	{
		id: 332,
		time: 331,
		velocity: 19.2819444444444,
		power: 16251.9327399167,
		road: 2547.37689814815,
		acceleration: 0.570277777777775
	},
	{
		id: 333,
		time: 332,
		velocity: 19.7669444444444,
		power: 19512.1817451096,
		road: 2566.8975462963,
		acceleration: 0.701296296296299
	},
	{
		id: 334,
		time: 333,
		velocity: 20.565,
		power: 22150.1494705382,
		road: 2587.15981481482,
		acceleration: 0.781944444444445
	},
	{
		id: 335,
		time: 334,
		velocity: 21.6277777777778,
		power: 21904.9508871005,
		road: 2608.1687962963,
		acceleration: 0.711481481481481
	},
	{
		id: 336,
		time: 335,
		velocity: 21.9013888888889,
		power: 17403.853772636,
		road: 2629.75884259259,
		acceleration: 0.450648148148147
	},
	{
		id: 337,
		time: 336,
		velocity: 21.9169444444444,
		power: 11736.2510237706,
		road: 2651.65495370371,
		acceleration: 0.161481481481481
	},
	{
		id: 338,
		time: 337,
		velocity: 22.1122222222222,
		power: 11771.5181344416,
		road: 2673.70939814815,
		acceleration: 0.155185185185186
	},
	{
		id: 339,
		time: 338,
		velocity: 22.3669444444444,
		power: 6474.85415501411,
		road: 2695.79328703704,
		acceleration: -0.0962962962962983
	},
	{
		id: 340,
		time: 339,
		velocity: 21.6280555555556,
		power: 3817.35268036936,
		road: 2717.72074074074,
		acceleration: -0.216574074074074
	},
	{
		id: 341,
		time: 340,
		velocity: 21.4625,
		power: 6231.16613141002,
		road: 2739.49199074074,
		acceleration: -0.095833333333335
	},
	{
		id: 342,
		time: 341,
		velocity: 22.0794444444444,
		power: 11071.6820306157,
		road: 2761.28282407408,
		acceleration: 0.135000000000002
	},
	{
		id: 343,
		time: 342,
		velocity: 22.0330555555556,
		power: 11909.6754187053,
		road: 2783.22486111111,
		acceleration: 0.16740740740741
	},
	{
		id: 344,
		time: 343,
		velocity: 21.9647222222222,
		power: 10976.9156214578,
		road: 2805.30870370371,
		acceleration: 0.116203703703707
	},
	{
		id: 345,
		time: 344,
		velocity: 22.4280555555556,
		power: 12719.7934802259,
		road: 2827.54587962963,
		acceleration: 0.190462962962958
	},
	{
		id: 346,
		time: 345,
		velocity: 22.6044444444444,
		power: 12523.9640659877,
		road: 2849.96425925926,
		acceleration: 0.171944444444449
	},
	{
		id: 347,
		time: 346,
		velocity: 22.4805555555556,
		power: 9010.37122148701,
		road: 2872.47097222222,
		acceleration: 0.00472222222222385
	},
	{
		id: 348,
		time: 347,
		velocity: 22.4422222222222,
		power: 4830.08033547713,
		road: 2894.88722222222,
		acceleration: -0.18564814814815
	},
	{
		id: 349,
		time: 348,
		velocity: 22.0475,
		power: 6429.68227904134,
		road: 2917.15782407408,
		acceleration: -0.105648148148148
	},
	{
		id: 350,
		time: 349,
		velocity: 22.1636111111111,
		power: 5344.49189881671,
		road: 2939.29972222222,
		acceleration: -0.151759259259261
	},
	{
		id: 351,
		time: 350,
		velocity: 21.9869444444444,
		power: 9167.92686134597,
		road: 2961.38120370371,
		acceleration: 0.0309259259259278
	},
	{
		id: 352,
		time: 351,
		velocity: 22.1402777777778,
		power: 8587.10742584041,
		road: 2983.47953703704,
		acceleration: 0.00277777777777644
	},
	{
		id: 353,
		time: 352,
		velocity: 22.1719444444444,
		power: 9483.08408306969,
		road: 3005.60125,
		acceleration: 0.043981481481481
	},
	{
		id: 354,
		time: 353,
		velocity: 22.1188888888889,
		power: 8421.16653505969,
		road: 3027.74152777778,
		acceleration: -0.00685185185184878
	},
	{
		id: 355,
		time: 354,
		velocity: 22.1197222222222,
		power: 8545.47813800218,
		road: 3049.87796296297,
		acceleration: -0.000833333333332575
	},
	{
		id: 356,
		time: 355,
		velocity: 22.1694444444444,
		power: 9228.2149331431,
		road: 3072.02930555556,
		acceleration: 0.0306481481481455
	},
	{
		id: 357,
		time: 356,
		velocity: 22.2108333333333,
		power: 9466.92158503715,
		road: 3094.21611111111,
		acceleration: 0.0402777777777779
	},
	{
		id: 358,
		time: 357,
		velocity: 22.2405555555556,
		power: 5708.57109979431,
		road: 3116.35578703704,
		acceleration: -0.134537037037035
	},
	{
		id: 359,
		time: 358,
		velocity: 21.7658333333333,
		power: 4658.57934515179,
		road: 3138.33893518519,
		acceleration: -0.178518518518519
	},
	{
		id: 360,
		time: 359,
		velocity: 21.6752777777778,
		power: 1417.00952156168,
		road: 3160.07018518519,
		acceleration: -0.325277777777782
	},
	{
		id: 361,
		time: 360,
		velocity: 21.2647222222222,
		power: 2420.13571860755,
		road: 3181.50462962963,
		acceleration: -0.268333333333331
	},
	{
		id: 362,
		time: 361,
		velocity: 20.9608333333333,
		power: -1440.02998162143,
		road: 3202.58050925926,
		acceleration: -0.448796296296297
	},
	{
		id: 363,
		time: 362,
		velocity: 20.3288888888889,
		power: -4544.37798168533,
		road: 3223.13398148148,
		acceleration: -0.59601851851852
	},
	{
		id: 364,
		time: 363,
		velocity: 19.4766666666667,
		power: -5143.33044201071,
		road: 3243.07912037037,
		acceleration: -0.620648148148149
	},
	{
		id: 365,
		time: 364,
		velocity: 19.0988888888889,
		power: -5959.8122838554,
		road: 3262.38407407408,
		acceleration: -0.659722222222221
	},
	{
		id: 366,
		time: 365,
		velocity: 18.3497222222222,
		power: -2807.00056085175,
		road: 3281.11833333334,
		acceleration: -0.481666666666662
	},
	{
		id: 367,
		time: 366,
		velocity: 18.0316666666667,
		power: -4282.28686774194,
		road: 3299.33175925926,
		acceleration: -0.560000000000002
	},
	{
		id: 368,
		time: 367,
		velocity: 17.4188888888889,
		power: -4045.17242971466,
		road: 3316.99388888889,
		acceleration: -0.542592592592595
	},
	{
		id: 369,
		time: 368,
		velocity: 16.7219444444444,
		power: -2843.09026349011,
		road: 3334.15143518519,
		acceleration: -0.466574074074074
	},
	{
		id: 370,
		time: 369,
		velocity: 16.6319444444444,
		power: -3992.17108931174,
		road: 3350.80847222223,
		acceleration: -0.534444444444446
	},
	{
		id: 371,
		time: 370,
		velocity: 15.8155555555556,
		power: -1388.22776342295,
		road: 3367.01574074074,
		acceleration: -0.365092592592593
	},
	{
		id: 372,
		time: 371,
		velocity: 15.6266666666667,
		power: 2402.24188188784,
		road: 3382.98356481482,
		acceleration: -0.113796296296295
	},
	{
		id: 373,
		time: 372,
		velocity: 16.2905555555556,
		power: 10582.9004212154,
		road: 3399.10092592593,
		acceleration: 0.412870370370372
	},
	{
		id: 374,
		time: 373,
		velocity: 17.0541666666667,
		power: 20688.5022952159,
		road: 3415.92402777778,
		acceleration: 0.99861111111111
	},
	{
		id: 375,
		time: 374,
		velocity: 18.6225,
		power: 23924.1066806963,
		road: 3433.79217592593,
		acceleration: 1.09148148148148
	},
	{
		id: 376,
		time: 375,
		velocity: 19.565,
		power: 21454.0718813597,
		road: 3452.63671296297,
		acceleration: 0.861296296296295
	},
	{
		id: 377,
		time: 376,
		velocity: 19.6380555555556,
		power: 14115.9838430043,
		road: 3472.11944444445,
		acceleration: 0.415092592592593
	},
	{
		id: 378,
		time: 377,
		velocity: 19.8677777777778,
		power: 9526.40159733714,
		road: 3491.88745370371,
		acceleration: 0.155462962962964
	},
	{
		id: 379,
		time: 378,
		velocity: 20.0313888888889,
		power: 17781.9612149969,
		road: 3512.0162962963,
		acceleration: 0.566203703703703
	},
	{
		id: 380,
		time: 379,
		velocity: 21.3366666666667,
		power: 22532.2088392793,
		road: 3532.80800925926,
		acceleration: 0.759537037037035
	},
	{
		id: 381,
		time: 380,
		velocity: 22.1463888888889,
		power: 23786.0841043715,
		road: 3554.36037037037,
		acceleration: 0.761759259259261
	},
	{
		id: 382,
		time: 381,
		velocity: 22.3166666666667,
		power: 20006.4584940575,
		road: 3576.56083333334,
		acceleration: 0.534444444444446
	},
	{
		id: 383,
		time: 382,
		velocity: 22.94,
		power: 15215.5301442113,
		road: 3599.17157407408,
		acceleration: 0.286111111111111
	},
	{
		id: 384,
		time: 383,
		velocity: 23.0047222222222,
		power: 16291.4259376388,
		road: 3622.08435185186,
		acceleration: 0.317962962962959
	},
	{
		id: 385,
		time: 384,
		velocity: 23.2705555555556,
		power: 10753.8662036865,
		road: 3645.18476851852,
		acceleration: 0.0573148148148164
	},
	{
		id: 386,
		time: 385,
		velocity: 23.1119444444444,
		power: 10479.0185288701,
		road: 3668.33513888889,
		acceleration: 0.0425925925925945
	},
	{
		id: 387,
		time: 386,
		velocity: 23.1325,
		power: 6482.22169543324,
		road: 3691.43907407408,
		acceleration: -0.135462962962961
	},
	{
		id: 388,
		time: 387,
		velocity: 22.8641666666667,
		power: 6776.64666467291,
		road: 3714.41666666667,
		acceleration: -0.117222222222221
	},
	{
		id: 389,
		time: 388,
		velocity: 22.7602777777778,
		power: 6420.86747475782,
		road: 3737.2713425926,
		acceleration: -0.128611111111116
	},
	{
		id: 390,
		time: 389,
		velocity: 22.7466666666667,
		power: 8087.84922646964,
		road: 3760.03726851852,
		acceleration: -0.0488888888888859
	},
	{
		id: 391,
		time: 390,
		velocity: 22.7175,
		power: 10251.1141753112,
		road: 3782.8037962963,
		acceleration: 0.0500925925925912
	},
	{
		id: 392,
		time: 391,
		velocity: 22.9105555555556,
		power: 10810.5550771225,
		road: 3805.63175925926,
		acceleration: 0.0727777777777767
	},
	{
		id: 393,
		time: 392,
		velocity: 22.965,
		power: 11182.4253744809,
		road: 3828.53907407408,
		acceleration: 0.085925925925924
	},
	{
		id: 394,
		time: 393,
		velocity: 22.9752777777778,
		power: 8387.39086010168,
		road: 3851.46828703704,
		acceleration: -0.0421296296296276
	},
	{
		id: 395,
		time: 394,
		velocity: 22.7841666666667,
		power: 5605.38170136696,
		road: 3874.29407407408,
		acceleration: -0.164722222222224
	},
	{
		id: 396,
		time: 395,
		velocity: 22.4708333333333,
		power: 4365.0554958962,
		road: 3896.9300925926,
		acceleration: -0.214814814814815
	},
	{
		id: 397,
		time: 396,
		velocity: 22.3308333333333,
		power: 1787.6717579037,
		road: 3919.29587962963,
		acceleration: -0.325648148148147
	},
	{
		id: 398,
		time: 397,
		velocity: 21.8072222222222,
		power: 447.853736254594,
		road: 3941.30935185185,
		acceleration: -0.378981481481478
	},
	{
		id: 399,
		time: 398,
		velocity: 21.3338888888889,
		power: -4293.73625438424,
		road: 3962.83518518519,
		acceleration: -0.596296296296298
	},
	{
		id: 400,
		time: 399,
		velocity: 20.5419444444444,
		power: -3917.76167636984,
		road: 3983.77810185185,
		acceleration: -0.569537037037037
	},
	{
		id: 401,
		time: 400,
		velocity: 20.0986111111111,
		power: -3727.57976372951,
		road: 4004.16013888889,
		acceleration: -0.55222222222222
	},
	{
		id: 402,
		time: 401,
		velocity: 19.6772222222222,
		power: 946.031726489664,
		road: 4024.11495370371,
		acceleration: -0.302222222222223
	},
	{
		id: 403,
		time: 402,
		velocity: 19.6352777777778,
		power: 4256.2215472949,
		road: 4043.85768518519,
		acceleration: -0.121944444444445
	},
	{
		id: 404,
		time: 403,
		velocity: 19.7327777777778,
		power: 8729.69375136899,
		road: 4063.59680555556,
		acceleration: 0.11472222222222
	},
	{
		id: 405,
		time: 404,
		velocity: 20.0213888888889,
		power: 13025.4971939622,
		road: 4083.55787037037,
		acceleration: 0.329166666666669
	},
	{
		id: 406,
		time: 405,
		velocity: 20.6227777777778,
		power: 13787.6835129719,
		road: 4103.85810185186,
		acceleration: 0.349166666666669
	},
	{
		id: 407,
		time: 406,
		velocity: 20.7802777777778,
		power: 15187.4623296013,
		road: 4124.53203703704,
		acceleration: 0.398240740740739
	},
	{
		id: 408,
		time: 407,
		velocity: 21.2161111111111,
		power: 8551.44338997438,
		road: 4145.43180555556,
		acceleration: 0.0534259259259251
	},
	{
		id: 409,
		time: 408,
		velocity: 20.7830555555556,
		power: 8838.38819027614,
		road: 4166.39083333334,
		acceleration: 0.0650925925925918
	},
	{
		id: 410,
		time: 409,
		velocity: 20.9755555555556,
		power: 8385.95287473035,
		road: 4187.4025462963,
		acceleration: 0.0402777777777779
	},
	{
		id: 411,
		time: 410,
		velocity: 21.3369444444444,
		power: 4378.55940452948,
		road: 4208.35606481482,
		acceleration: -0.156666666666666
	},
	{
		id: 412,
		time: 411,
		velocity: 20.3130555555556,
		power: 3552.06793550932,
		road: 4229.13513888889,
		acceleration: -0.192222222222224
	},
	{
		id: 413,
		time: 412,
		velocity: 20.3988888888889,
		power: 2713.12582765542,
		road: 4249.70407407408,
		acceleration: -0.228055555555557
	},
	{
		id: 414,
		time: 413,
		velocity: 20.6527777777778,
		power: 8909.00675503112,
		road: 4270.20365740741,
		acceleration: 0.0893518518518555
	},
	{
		id: 415,
		time: 414,
		velocity: 20.5811111111111,
		power: 8527.58721789202,
		road: 4290.78115740741,
		acceleration: 0.0664814814814783
	},
	{
		id: 416,
		time: 415,
		velocity: 20.5983333333333,
		power: 6812.21232768817,
		road: 4311.38120370371,
		acceleration: -0.0213888888888896
	},
	{
		id: 417,
		time: 416,
		velocity: 20.5886111111111,
		power: 6661.19203184296,
		road: 4331.95652777778,
		acceleration: -0.0280555555555537
	},
	{
		id: 418,
		time: 417,
		velocity: 20.4969444444444,
		power: 5641.78076083997,
		road: 4352.47893518519,
		acceleration: -0.0777777777777757
	},
	{
		id: 419,
		time: 418,
		velocity: 20.365,
		power: 3809.44274988344,
		road: 4372.87898148148,
		acceleration: -0.166944444444447
	},
	{
		id: 420,
		time: 419,
		velocity: 20.0877777777778,
		power: 302.065687749247,
		road: 4393.02537037037,
		acceleration: -0.340370370370369
	},
	{
		id: 421,
		time: 420,
		velocity: 19.4758333333333,
		power: -3884.12877919737,
		road: 4412.72564814815,
		acceleration: -0.55185185185185
	},
	{
		id: 422,
		time: 421,
		velocity: 18.7094444444444,
		power: -7116.98662087106,
		road: 4431.78898148149,
		acceleration: -0.722037037037037
	},
	{
		id: 423,
		time: 422,
		velocity: 17.9216666666667,
		power: -5077.69494019531,
		road: 4450.18805555556,
		acceleration: -0.606481481481485
	},
	{
		id: 424,
		time: 423,
		velocity: 17.6563888888889,
		power: -3815.04787128007,
		road: 4468.0187962963,
		acceleration: -0.530185185185186
	},
	{
		id: 425,
		time: 424,
		velocity: 17.1188888888889,
		power: -3447.87277158067,
		road: 4485.33217592593,
		acceleration: -0.504537037037032
	},
	{
		id: 426,
		time: 425,
		velocity: 16.4080555555556,
		power: -5810.30663651184,
		road: 4502.06912037037,
		acceleration: -0.648333333333333
	},
	{
		id: 427,
		time: 426,
		velocity: 15.7113888888889,
		power: -7441.62631746287,
		road: 4518.10296296297,
		acceleration: -0.757870370370373
	},
	{
		id: 428,
		time: 427,
		velocity: 14.8452777777778,
		power: -7432.01780752101,
		road: 4533.37375,
		acceleration: -0.768240740740742
	},
	{
		id: 429,
		time: 428,
		velocity: 14.1033333333333,
		power: -9388.62697997437,
		road: 4547.79726851852,
		acceleration: -0.926296296296295
	},
	{
		id: 430,
		time: 429,
		velocity: 12.9325,
		power: -11151.9114944794,
		road: 4561.2075925926,
		acceleration: -1.10009259259259
	},
	{
		id: 431,
		time: 430,
		velocity: 11.545,
		power: -10168.8237411639,
		road: 4573.52875,
		acceleration: -1.07824074074074
	},
	{
		id: 432,
		time: 431,
		velocity: 10.8686111111111,
		power: -8269.51940174121,
		road: 4584.82722222223,
		acceleration: -0.967129629629628
	},
	{
		id: 433,
		time: 432,
		velocity: 10.0311111111111,
		power: -4795.7480287502,
		road: 4595.30736111112,
		acceleration: -0.669537037037038
	},
	{
		id: 434,
		time: 433,
		velocity: 9.53638888888889,
		power: -3784.69833429632,
		road: 4605.1600462963,
		acceleration: -0.58537037037037
	},
	{
		id: 435,
		time: 434,
		velocity: 9.1125,
		power: -843.830054536122,
		road: 4614.58430555556,
		acceleration: -0.271481481481482
	},
	{
		id: 436,
		time: 435,
		velocity: 9.21666666666667,
		power: 3054.20541514419,
		road: 4623.95546296297,
		acceleration: 0.165277777777778
	},
	{
		id: 437,
		time: 436,
		velocity: 10.0322222222222,
		power: 6448.6865311894,
		road: 4633.66745370371,
		acceleration: 0.516388888888889
	},
	{
		id: 438,
		time: 437,
		velocity: 10.6616666666667,
		power: 8687.55961916077,
		road: 4643.98564814815,
		acceleration: 0.696018518518517
	},
	{
		id: 439,
		time: 438,
		velocity: 11.3047222222222,
		power: 8169.69355054574,
		road: 4654.94523148149,
		acceleration: 0.58675925925926
	},
	{
		id: 440,
		time: 439,
		velocity: 11.7925,
		power: 5676.93933932948,
		road: 4666.35856481482,
		acceleration: 0.320740740740742
	},
	{
		id: 441,
		time: 440,
		velocity: 11.6238888888889,
		power: 2612.9837342616,
		road: 4677.94888888889,
		acceleration: 0.0332407407407374
	},
	{
		id: 442,
		time: 441,
		velocity: 11.4044444444444,
		power: 738.655907019148,
		road: 4689.48814814815,
		acceleration: -0.135370370370371
	},
	{
		id: 443,
		time: 442,
		velocity: 11.3863888888889,
		power: 2447.40854425223,
		road: 4700.97060185186,
		acceleration: 0.0217592592592606
	},
	{
		id: 444,
		time: 443,
		velocity: 11.6891666666667,
		power: 3257.73977584798,
		road: 4712.51069444445,
		acceleration: 0.0935185185185201
	},
	{
		id: 445,
		time: 444,
		velocity: 11.685,
		power: 4093.06003625435,
		road: 4724.17935185186,
		acceleration: 0.163611111111111
	},
	{
		id: 446,
		time: 445,
		velocity: 11.8772222222222,
		power: 2885.45597901137,
		road: 4735.95546296297,
		acceleration: 0.0512962962962948
	},
	{
		id: 447,
		time: 446,
		velocity: 11.8430555555556,
		power: 2801.68588484216,
		road: 4747.77833333334,
		acceleration: 0.0422222222222217
	},
	{
		id: 448,
		time: 447,
		velocity: 11.8116666666667,
		power: 1808.17856991566,
		road: 4759.59939814815,
		acceleration: -0.0458333333333307
	},
	{
		id: 449,
		time: 448,
		velocity: 11.7397222222222,
		power: 3548.79865369495,
		road: 4771.45120370371,
		acceleration: 0.107314814814814
	},
	{
		id: 450,
		time: 449,
		velocity: 12.165,
		power: 6259.01966352524,
		road: 4783.52361111112,
		acceleration: 0.333888888888888
	},
	{
		id: 451,
		time: 450,
		velocity: 12.8133333333333,
		power: 8374.47233851694,
		road: 4796.00685185186,
		acceleration: 0.487777777777779
	},
	{
		id: 452,
		time: 451,
		velocity: 13.2030555555556,
		power: 8787.24621674016,
		road: 4808.97773148149,
		acceleration: 0.487499999999999
	},
	{
		id: 453,
		time: 452,
		velocity: 13.6275,
		power: 8657.91601128082,
		road: 4822.41518518519,
		acceleration: 0.44564814814815
	},
	{
		id: 454,
		time: 453,
		velocity: 14.1502777777778,
		power: 12294.1939561671,
		road: 4836.41620370371,
		acceleration: 0.681481481481478
	},
	{
		id: 455,
		time: 454,
		velocity: 15.2475,
		power: 11733.2373175988,
		road: 4851.05337962963,
		acceleration: 0.590833333333334
	},
	{
		id: 456,
		time: 455,
		velocity: 15.4,
		power: 17111.6542455538,
		road: 4866.43740740741,
		acceleration: 0.902870370370373
	},
	{
		id: 457,
		time: 456,
		velocity: 16.8588888888889,
		power: 13832.579192093,
		road: 4882.58365740741,
		acceleration: 0.621574074074072
	},
	{
		id: 458,
		time: 457,
		velocity: 17.1122222222222,
		power: 12714.630418509,
		road: 4899.29597222223,
		acceleration: 0.510555555555555
	},
	{
		id: 459,
		time: 458,
		velocity: 16.9316666666667,
		power: 3986.69798072407,
		road: 4916.24189814815,
		acceleration: -0.043333333333333
	},
	{
		id: 460,
		time: 459,
		velocity: 16.7288888888889,
		power: 352.044786258494,
		road: 4933.03393518519,
		acceleration: -0.264444444444443
	},
	{
		id: 461,
		time: 460,
		velocity: 16.3188888888889,
		power: -3906.47250316212,
		road: 4949.42967592593,
		acceleration: -0.528148148148151
	},
	{
		id: 462,
		time: 461,
		velocity: 15.3472222222222,
		power: -4892.69072543649,
		road: 4965.26532407408,
		acceleration: -0.592037037037034
	},
	{
		id: 463,
		time: 462,
		velocity: 14.9527777777778,
		power: -3031.24303540747,
		road: 4980.57157407408,
		acceleration: -0.466759259259261
	},
	{
		id: 464,
		time: 463,
		velocity: 14.9186111111111,
		power: -1130.99717684904,
		road: 4995.47837962963,
		acceleration: -0.332129629629629
	},
	{
		id: 465,
		time: 464,
		velocity: 14.3508333333333,
		power: -2173.83662534188,
		road: 5010.0175462963,
		acceleration: -0.403148148148148
	},
	{
		id: 466,
		time: 465,
		velocity: 13.7433333333333,
		power: -1563.62642396437,
		road: 5024.17708333334,
		acceleration: -0.356111111111112
	},
	{
		id: 467,
		time: 466,
		velocity: 13.8502777777778,
		power: 25.5033910257289,
		road: 5038.04162037037,
		acceleration: -0.23388888888889
	},
	{
		id: 468,
		time: 467,
		velocity: 13.6491666666667,
		power: 2761.67582972914,
		road: 5051.7775925926,
		acceleration: -0.0232407407407411
	},
	{
		id: 469,
		time: 468,
		velocity: 13.6736111111111,
		power: 827.396164927443,
		road: 5065.4175462963,
		acceleration: -0.168796296296295
	},
	{
		id: 470,
		time: 469,
		velocity: 13.3438888888889,
		power: 1555.21595960431,
		road: 5078.91833333334,
		acceleration: -0.109537037037036
	},
	{
		id: 471,
		time: 470,
		velocity: 13.3205555555556,
		power: -409.502931603993,
		road: 5092.23453703704,
		acceleration: -0.259629629629629
	},
	{
		id: 472,
		time: 471,
		velocity: 12.8947222222222,
		power: -153.326651620968,
		road: 5105.30291666667,
		acceleration: -0.23601851851852
	},
	{
		id: 473,
		time: 472,
		velocity: 12.6358333333333,
		power: -791.949877503148,
		road: 5118.11092592593,
		acceleration: -0.284722222222225
	},
	{
		id: 474,
		time: 473,
		velocity: 12.4663888888889,
		power: -1876.78950790864,
		road: 5130.59013888889,
		acceleration: -0.372870370370368
	},
	{
		id: 475,
		time: 474,
		velocity: 11.7761111111111,
		power: -1659.59891037401,
		road: 5142.70601851852,
		acceleration: -0.353796296296299
	},
	{
		id: 476,
		time: 475,
		velocity: 11.5744444444444,
		power: -2546.40476806856,
		road: 5154.42865740741,
		acceleration: -0.432685185185182
	},
	{
		id: 477,
		time: 476,
		velocity: 11.1683333333333,
		power: -294.231664237592,
		road: 5165.82111111111,
		acceleration: -0.227685185185186
	},
	{
		id: 478,
		time: 477,
		velocity: 11.0930555555556,
		power: 1274.20167653356,
		road: 5177.05986111111,
		acceleration: -0.0797222222222214
	},
	{
		id: 479,
		time: 478,
		velocity: 11.3352777777778,
		power: 3688.37188766187,
		road: 5188.33083333334,
		acceleration: 0.144166666666667
	},
	{
		id: 480,
		time: 479,
		velocity: 11.6008333333333,
		power: 5618.80996137376,
		road: 5199.82912037037,
		acceleration: 0.31046296296296
	},
	{
		id: 481,
		time: 480,
		velocity: 12.0244444444444,
		power: 3921.04232144733,
		road: 5211.55546296297,
		acceleration: 0.145648148148149
	},
	{
		id: 482,
		time: 481,
		velocity: 11.7722222222222,
		power: 1654.47130908248,
		road: 5223.32555555556,
		acceleration: -0.0581481481481489
	},
	{
		id: 483,
		time: 482,
		velocity: 11.4263888888889,
		power: 260.007446287508,
		road: 5234.9762962963,
		acceleration: -0.180555555555554
	},
	{
		id: 484,
		time: 483,
		velocity: 11.4827777777778,
		power: 303.663021796384,
		road: 5246.44981481482,
		acceleration: -0.173888888888889
	},
	{
		id: 485,
		time: 484,
		velocity: 11.2505555555556,
		power: 1719.69206478639,
		road: 5257.81560185186,
		acceleration: -0.0415740740740755
	},
	{
		id: 486,
		time: 485,
		velocity: 11.3016666666667,
		power: 3772.31256168005,
		road: 5269.23337962963,
		acceleration: 0.145555555555555
	},
	{
		id: 487,
		time: 486,
		velocity: 11.9194444444444,
		power: 5113.11184649799,
		road: 5280.85287037037,
		acceleration: 0.25787037037037
	},
	{
		id: 488,
		time: 487,
		velocity: 12.0241666666667,
		power: 6237.4421735592,
		road: 5292.77185185186,
		acceleration: 0.341111111111111
	},
	{
		id: 489,
		time: 488,
		velocity: 12.325,
		power: 2681.46184303334,
		road: 5304.8725462963,
		acceleration: 0.022314814814818
	},
	{
		id: 490,
		time: 489,
		velocity: 11.9863888888889,
		power: -153.4741781302,
		road: 5316.87337962963,
		acceleration: -0.222037037037039
	},
	{
		id: 491,
		time: 490,
		velocity: 11.3580555555556,
		power: -3332.48807284552,
		road: 5328.51115740741,
		acceleration: -0.504074074074072
	},
	{
		id: 492,
		time: 491,
		velocity: 10.8127777777778,
		power: -936.106785383831,
		road: 5339.75388888889,
		acceleration: -0.286018518518521
	},
	{
		id: 493,
		time: 492,
		velocity: 11.1283333333333,
		power: -891.41654749088,
		road: 5350.71337962963,
		acceleration: -0.280462962962961
	},
	{
		id: 494,
		time: 493,
		velocity: 10.5166666666667,
		power: 1568.97251353176,
		road: 5361.51226851852,
		acceleration: -0.040740740740743
	},
	{
		id: 495,
		time: 494,
		velocity: 10.6905555555556,
		power: 1936.64827922382,
		road: 5372.28861111111,
		acceleration: -0.00435185185185105
	},
	{
		id: 496,
		time: 495,
		velocity: 11.1152777777778,
		power: 4352.86189321272,
		road: 5383.17537037037,
		acceleration: 0.225185185185184
	},
	{
		id: 497,
		time: 496,
		velocity: 11.1922222222222,
		power: 4595.86938766918,
		road: 5394.29296296297,
		acceleration: 0.236481481481482
	},
	{
		id: 498,
		time: 497,
		velocity: 11.4,
		power: 4020.17991141857,
		road: 5405.61513888889,
		acceleration: 0.172685185185188
	},
	{
		id: 499,
		time: 498,
		velocity: 11.6333333333333,
		power: 4545.14978745229,
		road: 5417.12953703704,
		acceleration: 0.211759259259258
	},
	{
		id: 500,
		time: 499,
		velocity: 11.8275,
		power: 4728.27128440914,
		road: 5428.85865740741,
		acceleration: 0.217685185185184
	},
	{
		id: 501,
		time: 500,
		velocity: 12.0530555555556,
		power: 4954.67774046097,
		road: 5440.80995370371,
		acceleration: 0.226666666666668
	},
	{
		id: 502,
		time: 501,
		velocity: 12.3133333333333,
		power: 5657.42354049517,
		road: 5453.01189814815,
		acceleration: 0.274629629629628
	},
	{
		id: 503,
		time: 502,
		velocity: 12.6513888888889,
		power: 5749.69622709217,
		road: 5465.48513888889,
		acceleration: 0.267962962962963
	},
	{
		id: 504,
		time: 503,
		velocity: 12.8569444444444,
		power: 6869.73198912116,
		road: 5478.26425925926,
		acceleration: 0.343796296296295
	},
	{
		id: 505,
		time: 504,
		velocity: 13.3447222222222,
		power: 5523.1201322608,
		road: 5491.325,
		acceleration: 0.219444444444447
	},
	{
		id: 506,
		time: 505,
		velocity: 13.3097222222222,
		power: 3782.6620118827,
		road: 5504.5325925926,
		acceleration: 0.074259259259259
	},
	{
		id: 507,
		time: 506,
		velocity: 13.0797222222222,
		power: 193.547617216889,
		road: 5517.67263888889,
		acceleration: -0.209351851851851
	},
	{
		id: 508,
		time: 507,
		velocity: 12.7166666666667,
		power: -347.648775186826,
		road: 5530.58319444445,
		acceleration: -0.249629629629631
	},
	{
		id: 509,
		time: 508,
		velocity: 12.5608333333333,
		power: -81.4033002106192,
		road: 5543.25657407408,
		acceleration: -0.224722222222223
	},
	{
		id: 510,
		time: 509,
		velocity: 12.4055555555556,
		power: 2586.72971060104,
		road: 5555.81731481482,
		acceleration: -0.000555555555553866
	},
	{
		id: 511,
		time: 510,
		velocity: 12.715,
		power: 8157.40447753506,
		road: 5568.60226851852,
		acceleration: 0.448981481481482
	},
	{
		id: 512,
		time: 511,
		velocity: 13.9077777777778,
		power: 8785.6657229432,
		road: 5581.84601851852,
		acceleration: 0.468611111111111
	},
	{
		id: 513,
		time: 512,
		velocity: 13.8113888888889,
		power: 7767.86877855748,
		road: 5595.50555555556,
		acceleration: 0.362962962962962
	},
	{
		id: 514,
		time: 513,
		velocity: 13.8038888888889,
		power: 1281.30172876596,
		road: 5609.27810185185,
		acceleration: -0.136944444444447
	},
	{
		id: 515,
		time: 514,
		velocity: 13.4969444444444,
		power: 1255.54380175598,
		road: 5622.91425925926,
		acceleration: -0.135833333333332
	},
	{
		id: 516,
		time: 515,
		velocity: 13.4038888888889,
		power: 3163.60006851918,
		road: 5636.48888888889,
		acceleration: 0.0127777777777798
	},
	{
		id: 517,
		time: 516,
		velocity: 13.8422222222222,
		power: 5858.79309444599,
		road: 5650.1775,
		acceleration: 0.215185185185184
	},
	{
		id: 518,
		time: 517,
		velocity: 14.1425,
		power: 8996.18783390992,
		road: 5664.19074074074,
		acceleration: 0.434074074074074
	},
	{
		id: 519,
		time: 518,
		velocity: 14.7061111111111,
		power: 8175.4226076538,
		road: 5678.59587962963,
		acceleration: 0.349722222222221
	},
	{
		id: 520,
		time: 519,
		velocity: 14.8913888888889,
		power: 6125.87701643984,
		road: 5693.26995370371,
		acceleration: 0.18814814814815
	},
	{
		id: 521,
		time: 520,
		velocity: 14.7069444444444,
		power: 3122.70093128196,
		road: 5708.0237962963,
		acceleration: -0.0286111111111111
	},
	{
		id: 522,
		time: 521,
		velocity: 14.6202777777778,
		power: 1434.85041706445,
		road: 5722.69018518519,
		acceleration: -0.146296296296295
	},
	{
		id: 523,
		time: 522,
		velocity: 14.4525,
		power: 1279.24418896055,
		road: 5737.20643518519,
		acceleration: -0.153981481481482
	},
	{
		id: 524,
		time: 523,
		velocity: 14.245,
		power: 2541.80549422258,
		road: 5751.61578703704,
		acceleration: -0.0598148148148141
	},
	{
		id: 525,
		time: 524,
		velocity: 14.4408333333333,
		power: 4183.15619137585,
		road: 5766.02495370371,
		acceleration: 0.0594444444444431
	},
	{
		id: 526,
		time: 525,
		velocity: 14.6308333333333,
		power: 4373.04453415509,
		road: 5780.49921296296,
		acceleration: 0.0707407407407405
	},
	{
		id: 527,
		time: 526,
		velocity: 14.4572222222222,
		power: 2606.55240698352,
		road: 5794.98023148148,
		acceleration: -0.0572222222222223
	},
	{
		id: 528,
		time: 527,
		velocity: 14.2691666666667,
		power: 944.907576846504,
		road: 5809.34518518519,
		acceleration: -0.174907407407408
	},
	{
		id: 529,
		time: 528,
		velocity: 14.1061111111111,
		power: 1115.88436094079,
		road: 5823.54328703704,
		acceleration: -0.158796296296297
	},
	{
		id: 530,
		time: 529,
		velocity: 13.9808333333333,
		power: 431.840643756645,
		road: 5837.55902777778,
		acceleration: -0.205925925925925
	},
	{
		id: 531,
		time: 530,
		velocity: 13.6513888888889,
		power: 1245.86164615166,
		road: 5851.40120370371,
		acceleration: -0.141203703703704
	},
	{
		id: 532,
		time: 531,
		velocity: 13.6825,
		power: 1803.01667497554,
		road: 5865.12476851852,
		acceleration: -0.0960185185185178
	},
	{
		id: 533,
		time: 532,
		velocity: 13.6927777777778,
		power: 3333.37402024437,
		road: 5878.81134259259,
		acceleration: 0.0220370370370375
	},
	{
		id: 534,
		time: 533,
		velocity: 13.7175,
		power: 4807.39371018453,
		road: 5892.57472222222,
		acceleration: 0.131574074074074
	},
	{
		id: 535,
		time: 534,
		velocity: 14.0772222222222,
		power: 5252.66673407144,
		road: 5906.48337962963,
		acceleration: 0.158981481481483
	},
	{
		id: 536,
		time: 535,
		velocity: 14.1697222222222,
		power: 5502.06508500969,
		road: 5920.55666666667,
		acceleration: 0.170277777777777
	},
	{
		id: 537,
		time: 536,
		velocity: 14.2283333333333,
		power: 3406.71124679851,
		road: 5934.72074074074,
		acceleration: 0.0112962962962992
	},
	{
		id: 538,
		time: 537,
		velocity: 14.1111111111111,
		power: 3557.27505712821,
		road: 5948.90138888889,
		acceleration: 0.0218518518518493
	},
	{
		id: 539,
		time: 538,
		velocity: 14.2352777777778,
		power: 2522.6015360049,
		road: 5963.06592592593,
		acceleration: -0.0540740740740766
	},
	{
		id: 540,
		time: 539,
		velocity: 14.0661111111111,
		power: 3626.19978146966,
		road: 5977.21740740741,
		acceleration: 0.027962962962965
	},
	{
		id: 541,
		time: 540,
		velocity: 14.195,
		power: 3161.58650913315,
		road: 5991.37949074074,
		acceleration: -0.00675925925926002
	},
	{
		id: 542,
		time: 541,
		velocity: 14.215,
		power: 3775.31695460814,
		road: 6005.55722222222,
		acceleration: 0.0380555555555571
	},
	{
		id: 543,
		time: 542,
		velocity: 14.1802777777778,
		power: 4319.18150540573,
		road: 6019.79199074074,
		acceleration: 0.0760185185185165
	},
	{
		id: 544,
		time: 543,
		velocity: 14.4230555555556,
		power: 4492.07326045038,
		road: 6034.1075462963,
		acceleration: 0.0855555555555565
	},
	{
		id: 545,
		time: 544,
		velocity: 14.4716666666667,
		power: 5545.93513813887,
		road: 6048.54449074074,
		acceleration: 0.157222222222224
	},
	{
		id: 546,
		time: 545,
		velocity: 14.6519444444444,
		power: 5260.35365533798,
		road: 6063.12523148148,
		acceleration: 0.130370370370368
	},
	{
		id: 547,
		time: 546,
		velocity: 14.8141666666667,
		power: 5104.22975216603,
		road: 6077.82819444445,
		acceleration: 0.114074074074075
	},
	{
		id: 548,
		time: 547,
		velocity: 14.8138888888889,
		power: 3560.78213482438,
		road: 6092.58930555556,
		acceleration: 0.00222222222222079
	},
	{
		id: 549,
		time: 548,
		velocity: 14.6586111111111,
		power: 2754.58100459345,
		road: 6107.32444444445,
		acceleration: -0.0541666666666654
	},
	{
		id: 550,
		time: 549,
		velocity: 14.6516666666667,
		power: 2303.85764292101,
		road: 6121.99037037037,
		acceleration: -0.0842592592592588
	},
	{
		id: 551,
		time: 550,
		velocity: 14.5611111111111,
		power: 2787.02275752359,
		road: 6136.59027777778,
		acceleration: -0.0477777777777781
	},
	{
		id: 552,
		time: 551,
		velocity: 14.5152777777778,
		power: 2981.30577691278,
		road: 6151.15,
		acceleration: -0.032592592592593
	},
	{
		id: 553,
		time: 552,
		velocity: 14.5538888888889,
		power: 4292.07900103681,
		road: 6165.72398148148,
		acceleration: 0.06111111111111
	},
	{
		id: 554,
		time: 553,
		velocity: 14.7444444444444,
		power: 4638.45633724662,
		road: 6180.3700925926,
		acceleration: 0.0831481481481475
	},
	{
		id: 555,
		time: 554,
		velocity: 14.7647222222222,
		power: 3974.75670978486,
		road: 6195.07458333334,
		acceleration: 0.0336111111111137
	},
	{
		id: 556,
		time: 555,
		velocity: 14.6547222222222,
		power: 2085.33646642761,
		road: 6209.74587962963,
		acceleration: -0.100000000000001
	},
	{
		id: 557,
		time: 556,
		velocity: 14.4444444444444,
		power: -51.4081118151372,
		road: 6224.24236111111,
		acceleration: -0.249629629629631
	},
	{
		id: 558,
		time: 557,
		velocity: 14.0158333333333,
		power: -250.215954550387,
		road: 6238.48393518519,
		acceleration: -0.260185185185183
	},
	{
		id: 559,
		time: 558,
		velocity: 13.8741666666667,
		power: -852.780408702735,
		road: 6252.44476851852,
		acceleration: -0.301296296296297
	},
	{
		id: 560,
		time: 559,
		velocity: 13.5405555555556,
		power: -367.869345225098,
		road: 6266.12439814815,
		acceleration: -0.261111111111109
	},
	{
		id: 561,
		time: 560,
		velocity: 13.2325,
		power: -199.305924850532,
		road: 6279.55115740741,
		acceleration: -0.244629629629632
	},
	{
		id: 562,
		time: 561,
		velocity: 13.1402777777778,
		power: 3043.671934619,
		road: 6292.86171296297,
		acceleration: 0.0122222222222241
	},
	{
		id: 563,
		time: 562,
		velocity: 13.5772222222222,
		power: 5710.01899671267,
		road: 6306.28662037037,
		acceleration: 0.21648148148148
	},
	{
		id: 564,
		time: 563,
		velocity: 13.8819444444444,
		power: 9346.35301411177,
		road: 6320.05796296297,
		acceleration: 0.476388888888888
	},
	{
		id: 565,
		time: 564,
		velocity: 14.5694444444444,
		power: 9559.89965178584,
		road: 6334.29805555556,
		acceleration: 0.461111111111112
	},
	{
		id: 566,
		time: 565,
		velocity: 14.9605555555556,
		power: 12724.1181137594,
		road: 6349.09333333334,
		acceleration: 0.64925925925926
	},
	{
		id: 567,
		time: 566,
		velocity: 15.8297222222222,
		power: 12090.298709016,
		road: 6364.49337962963,
		acceleration: 0.560277777777777
	},
	{
		id: 568,
		time: 567,
		velocity: 16.2502777777778,
		power: 12846.0883740192,
		road: 6380.4587962963,
		acceleration: 0.570462962962962
	},
	{
		id: 569,
		time: 568,
		velocity: 16.6719444444444,
		power: 9601.17035844166,
		road: 6396.87555555556,
		acceleration: 0.332222222222221
	},
	{
		id: 570,
		time: 569,
		velocity: 16.8263888888889,
		power: 9311.6598892369,
		road: 6413.60675925926,
		acceleration: 0.296666666666667
	},
	{
		id: 571,
		time: 570,
		velocity: 17.1402777777778,
		power: 7434.29625203288,
		road: 6430.57055555556,
		acceleration: 0.168518518518518
	},
	{
		id: 572,
		time: 571,
		velocity: 17.1775,
		power: 6385.61653148181,
		road: 6447.66773148149,
		acceleration: 0.0982407407407422
	},
	{
		id: 573,
		time: 572,
		velocity: 17.1211111111111,
		power: 5477.07226224094,
		road: 6464.83402777778,
		acceleration: 0.0399999999999991
	},
	{
		id: 574,
		time: 573,
		velocity: 17.2602777777778,
		power: 5108.26697356199,
		road: 6482.02856481482,
		acceleration: 0.0164814814814847
	},
	{
		id: 575,
		time: 574,
		velocity: 17.2269444444444,
		power: 6243.70699171592,
		road: 6499.27305555556,
		acceleration: 0.0834259259259227
	},
	{
		id: 576,
		time: 575,
		velocity: 17.3713888888889,
		power: 4998.65733887609,
		road: 6516.56240740741,
		acceleration: 0.00629629629629846
	},
	{
		id: 577,
		time: 576,
		velocity: 17.2791666666667,
		power: 5346.06586523651,
		road: 6533.86824074074,
		acceleration: 0.0266666666666637
	},
	{
		id: 578,
		time: 577,
		velocity: 17.3069444444444,
		power: 4413.07640961383,
		road: 6551.1725925926,
		acceleration: -0.0296296296296283
	},
	{
		id: 579,
		time: 578,
		velocity: 17.2825,
		power: 4905.53726587764,
		road: 6568.46245370371,
		acceleration: 0.000648148148147953
	},
	{
		id: 580,
		time: 579,
		velocity: 17.2811111111111,
		power: 4704.77433074641,
		road: 6585.74699074074,
		acceleration: -0.0112962962962939
	},
	{
		id: 581,
		time: 580,
		velocity: 17.2730555555556,
		power: 5173.55384198974,
		road: 6603.03435185185,
		acceleration: 0.0169444444444444
	},
	{
		id: 582,
		time: 581,
		velocity: 17.3333333333333,
		power: 5409.13609569377,
		road: 6620.34532407408,
		acceleration: 0.0302777777777798
	},
	{
		id: 583,
		time: 582,
		velocity: 17.3719444444444,
		power: 4512.30771173697,
		road: 6637.65944444445,
		acceleration: -0.0239814814814849
	},
	{
		id: 584,
		time: 583,
		velocity: 17.2011111111111,
		power: 3992.1239509714,
		road: 6654.93453703704,
		acceleration: -0.0540740740740731
	},
	{
		id: 585,
		time: 584,
		velocity: 17.1711111111111,
		power: 5221.32432377351,
		road: 6672.19305555556,
		acceleration: 0.0209259259259262
	},
	{
		id: 586,
		time: 585,
		velocity: 17.4347222222222,
		power: 5323.36125032132,
		road: 6689.47513888889,
		acceleration: 0.026203703703704
	},
	{
		id: 587,
		time: 586,
		velocity: 17.2797222222222,
		power: 5640.14868098052,
		road: 6706.79231481482,
		acceleration: 0.043981481481481
	},
	{
		id: 588,
		time: 587,
		velocity: 17.3030555555556,
		power: 4056.37041965264,
		road: 6724.10574074075,
		acceleration: -0.0514814814814812
	},
	{
		id: 589,
		time: 588,
		velocity: 17.2802777777778,
		power: 5780.71607770839,
		road: 6741.41972222223,
		acceleration: 0.0525925925925961
	},
	{
		id: 590,
		time: 589,
		velocity: 17.4375,
		power: 5251.11353956825,
		road: 6758.76962962963,
		acceleration: 0.0192592592592575
	},
	{
		id: 591,
		time: 590,
		velocity: 17.3608333333333,
		power: 5567.59920225825,
		road: 6776.14777777778,
		acceleration: 0.0372222222222192
	},
	{
		id: 592,
		time: 591,
		velocity: 17.3919444444444,
		power: 4260.87015966397,
		road: 6793.52388888889,
		acceleration: -0.0412962962962951
	},
	{
		id: 593,
		time: 592,
		velocity: 17.3136111111111,
		power: 4495.12978047109,
		road: 6810.8663425926,
		acceleration: -0.0260185185185193
	},
	{
		id: 594,
		time: 593,
		velocity: 17.2827777777778,
		power: 4657.93361815743,
		road: 6828.18805555556,
		acceleration: -0.0154629629629639
	},
	{
		id: 595,
		time: 594,
		velocity: 17.3455555555556,
		power: 5430.11495920392,
		road: 6845.51745370371,
		acceleration: 0.0308333333333337
	},
	{
		id: 596,
		time: 595,
		velocity: 17.4061111111111,
		power: 5086.38986699651,
		road: 6862.86694444445,
		acceleration: 0.00935185185185361
	},
	{
		id: 597,
		time: 596,
		velocity: 17.3108333333333,
		power: 5026.4019405138,
		road: 6880.2238425926,
		acceleration: 0.00546296296296234
	},
	{
		id: 598,
		time: 597,
		velocity: 17.3619444444444,
		power: 4531.97692641418,
		road: 6897.57148148148,
		acceleration: -0.0239814814814814
	},
	{
		id: 599,
		time: 598,
		velocity: 17.3341666666667,
		power: 3471.80247411665,
		road: 6914.86407407408,
		acceleration: -0.0861111111111086
	},
	{
		id: 600,
		time: 599,
		velocity: 17.0525,
		power: 129.847038403269,
		road: 6931.97138888889,
		acceleration: -0.284444444444446
	},
	{
		id: 601,
		time: 600,
		velocity: 16.5086111111111,
		power: -3271.89921488777,
		road: 6948.69166666667,
		acceleration: -0.489629629629633
	},
	{
		id: 602,
		time: 601,
		velocity: 15.8652777777778,
		power: -3951.94981619185,
		road: 6964.90185185186,
		acceleration: -0.530555555555553
	},
	{
		id: 603,
		time: 602,
		velocity: 15.4608333333333,
		power: -1637.34032036219,
		road: 6980.65870370371,
		acceleration: -0.376111111111111
	},
	{
		id: 604,
		time: 603,
		velocity: 15.3802777777778,
		power: 3006.29481082532,
		road: 6996.19694444445,
		acceleration: -0.06111111111111
	},
	{
		id: 605,
		time: 604,
		velocity: 15.6819444444444,
		power: 5901.8651677911,
		road: 7011.77078703704,
		acceleration: 0.132314814814814
	},
	{
		id: 606,
		time: 605,
		velocity: 15.8577777777778,
		power: 9759.11727992403,
		road: 7027.59893518519,
		acceleration: 0.376296296296292
	},
	{
		id: 607,
		time: 606,
		velocity: 16.5091666666667,
		power: 12135.760067034,
		road: 7043.86699074074,
		acceleration: 0.503518518518522
	},
	{
		id: 608,
		time: 607,
		velocity: 17.1925,
		power: 12413.5520485405,
		road: 7060.63097222223,
		acceleration: 0.488333333333333
	},
	{
		id: 609,
		time: 608,
		velocity: 17.3227777777778,
		power: 9896.11265800982,
		road: 7077.79375,
		acceleration: 0.309259259259257
	},
	{
		id: 610,
		time: 609,
		velocity: 17.4369444444444,
		power: 6470.29475555376,
		road: 7095.15722222222,
		acceleration: 0.0921296296296319
	},
	{
		id: 611,
		time: 610,
		velocity: 17.4688888888889,
		power: 5633.16196714069,
		road: 7112.5863425926,
		acceleration: 0.0391666666666666
	},
	{
		id: 612,
		time: 611,
		velocity: 17.4402777777778,
		power: 5617.77135964754,
		road: 7130.05342592593,
		acceleration: 0.0367592592592558
	},
	{
		id: 613,
		time: 612,
		velocity: 17.5472222222222,
		power: 3041.23864773208,
		road: 7147.48078703704,
		acceleration: -0.116203703703697
	},
	{
		id: 614,
		time: 613,
		velocity: 17.1202777777778,
		power: 349.256206281951,
		road: 7164.71324074074,
		acceleration: -0.273611111111116
	},
	{
		id: 615,
		time: 614,
		velocity: 16.6194444444444,
		power: -2581.78706224139,
		road: 7181.58495370371,
		acceleration: -0.447870370370367
	},
	{
		id: 616,
		time: 615,
		velocity: 16.2036111111111,
		power: -664.29480590439,
		road: 7198.0713425926,
		acceleration: -0.32277777777778
	},
	{
		id: 617,
		time: 616,
		velocity: 16.1519444444444,
		power: 311.862006971344,
		road: 7214.26875,
		acceleration: -0.255185185185184
	},
	{
		id: 618,
		time: 617,
		velocity: 15.8538888888889,
		power: 10.1465251399122,
		road: 7230.20361111111,
		acceleration: -0.269907407407407
	},
	{
		id: 619,
		time: 618,
		velocity: 15.3938888888889,
		power: -1688.13313851736,
		road: 7245.81453703704,
		acceleration: -0.377962962962965
	},
	{
		id: 620,
		time: 619,
		velocity: 15.0180555555556,
		power: -2327.96456450881,
		road: 7261.02745370371,
		acceleration: -0.418055555555556
	},
	{
		id: 621,
		time: 620,
		velocity: 14.5997222222222,
		power: -2725.16272007655,
		road: 7275.80953703704,
		acceleration: -0.44361111111111
	},
	{
		id: 622,
		time: 621,
		velocity: 14.0630555555556,
		power: -5718.23925039921,
		road: 7290.03861111111,
		acceleration: -0.662407407407407
	},
	{
		id: 623,
		time: 622,
		velocity: 13.0308333333333,
		power: -9101.09921813886,
		road: 7303.46689814815,
		acceleration: -0.939166666666665
	},
	{
		id: 624,
		time: 623,
		velocity: 11.7822222222222,
		power: -10850.2376294085,
		road: 7315.85967592593,
		acceleration: -1.13185185185185
	},
	{
		id: 625,
		time: 624,
		velocity: 10.6675,
		power: -8919.64040192876,
		road: 7327.17324074074,
		acceleration: -1.02657407407407
	},
	{
		id: 626,
		time: 625,
		velocity: 9.95111111111111,
		power: -6271.42312807003,
		road: 7337.56263888889,
		acceleration: -0.82175925925926
	},
	{
		id: 627,
		time: 626,
		velocity: 9.31694444444444,
		power: -3535.17469449203,
		road: 7347.25958333334,
		acceleration: -0.563148148148148
	},
	{
		id: 628,
		time: 627,
		velocity: 8.97805555555555,
		power: -2938.33719478784,
		road: 7356.41916666667,
		acceleration: -0.511574074074073
	},
	{
		id: 629,
		time: 628,
		velocity: 8.41638888888889,
		power: -2238.96737517375,
		road: 7365.10259259259,
		acceleration: -0.440740740740742
	},
	{
		id: 630,
		time: 629,
		velocity: 7.99472222222222,
		power: -2050.10871244363,
		road: 7373.35222222222,
		acceleration: -0.426851851851852
	},
	{
		id: 631,
		time: 630,
		velocity: 7.6975,
		power: -2852.76419336146,
		road: 7381.11462962963,
		acceleration: -0.547592592592593
	},
	{
		id: 632,
		time: 631,
		velocity: 6.77361111111111,
		power: -2523.91957065044,
		road: 7388.34125,
		acceleration: -0.523981481481481
	},
	{
		id: 633,
		time: 632,
		velocity: 6.42277777777778,
		power: -2893.42467556704,
		road: 7395.00125,
		acceleration: -0.609259259259259
	},
	{
		id: 634,
		time: 633,
		velocity: 5.86972222222222,
		power: -1904.83450092568,
		road: 7401.11861111111,
		acceleration: -0.476018518518519
	},
	{
		id: 635,
		time: 634,
		velocity: 5.34555555555556,
		power: -2094.11404970015,
		road: 7406.72902777778,
		acceleration: -0.537870370370371
	},
	{
		id: 636,
		time: 635,
		velocity: 4.80916666666667,
		power: -1919.98467186386,
		road: 7411.80032407407,
		acceleration: -0.54037037037037
	},
	{
		id: 637,
		time: 636,
		velocity: 4.24861111111111,
		power: -2772.41158534459,
		road: 7416.20055555556,
		acceleration: -0.80175925925926
	},
	{
		id: 638,
		time: 637,
		velocity: 2.94027777777778,
		power: -2578.11860140063,
		road: 7419.7500462963,
		acceleration: -0.899722222222222
	},
	{
		id: 639,
		time: 638,
		velocity: 2.11,
		power: -2188.53740480995,
		road: 7422.33842592593,
		acceleration: -1.0225
	},
	{
		id: 640,
		time: 639,
		velocity: 1.18111111111111,
		power: -1280.83582561957,
		road: 7423.92550925926,
		acceleration: -0.980092592592593
	},
	{
		id: 641,
		time: 640,
		velocity: 0,
		power: -406.535247642825,
		road: 7424.67087962963,
		acceleration: -0.703333333333333
	},
	{
		id: 642,
		time: 641,
		velocity: 0,
		power: -49.6390608836907,
		road: 7424.86773148148,
		acceleration: -0.393703703703704
	},
	{
		id: 643,
		time: 642,
		velocity: 0,
		power: 0,
		road: 7424.86773148148,
		acceleration: 0
	},
	{
		id: 644,
		time: 643,
		velocity: 0,
		power: 159.8938232058,
		road: 7425.12805555556,
		acceleration: 0.520648148148148
	},
	{
		id: 645,
		time: 644,
		velocity: 1.56194444444444,
		power: 712.303103747431,
		road: 7426.01050925926,
		acceleration: 0.723611111111111
	},
	{
		id: 646,
		time: 645,
		velocity: 2.17083333333333,
		power: 1969.02308432837,
		road: 7427.77759259259,
		acceleration: 1.04564814814815
	},
	{
		id: 647,
		time: 646,
		velocity: 3.13694444444444,
		power: 1829.90030181951,
		road: 7430.37337962963,
		acceleration: 0.611759259259259
	},
	{
		id: 648,
		time: 647,
		velocity: 3.39722222222222,
		power: 1499.5940790106,
		road: 7433.46425925926,
		acceleration: 0.378425925925926
	},
	{
		id: 649,
		time: 648,
		velocity: 3.30611111111111,
		power: -371.305691249993,
		road: 7436.61578703704,
		acceleration: -0.257129629629629
	},
	{
		id: 650,
		time: 649,
		velocity: 2.36555555555556,
		power: -571.336356255892,
		road: 7439.46717592593,
		acceleration: -0.343148148148148
	},
	{
		id: 651,
		time: 650,
		velocity: 2.36777777777778,
		power: -624.445446446466,
		road: 7441.94893518518,
		acceleration: -0.396111111111111
	},
	{
		id: 652,
		time: 651,
		velocity: 2.11777777777778,
		power: 85.2464451292918,
		road: 7444.18759259259,
		acceleration: -0.0900925925925931
	},
	{
		id: 653,
		time: 652,
		velocity: 2.09527777777778,
		power: 150.34364187606,
		road: 7446.35277777778,
		acceleration: -0.0568518518518513
	},
	{
		id: 654,
		time: 653,
		velocity: 2.19722222222222,
		power: 302.92435909442,
		road: 7448.49893518518,
		acceleration: 0.018796296296296
	},
	{
		id: 655,
		time: 654,
		velocity: 2.17416666666667,
		power: 474.983117137545,
		road: 7450.70300925926,
		acceleration: 0.0970370370370368
	},
	{
		id: 656,
		time: 655,
		velocity: 2.38638888888889,
		power: 323.6036492574,
		road: 7452.96583333333,
		acceleration: 0.0204629629629629
	},
	{
		id: 657,
		time: 656,
		velocity: 2.25861111111111,
		power: 259.250177929004,
		road: 7455.23398148148,
		acceleration: -0.00981481481481428
	},
	{
		id: 658,
		time: 657,
		velocity: 2.14472222222222,
		power: 230.01331208434,
		road: 7457.48592592592,
		acceleration: -0.0225925925925927
	},
	{
		id: 659,
		time: 658,
		velocity: 2.31861111111111,
		power: 422.843429322737,
		road: 7459.75944444444,
		acceleration: 0.0657407407407402
	},
	{
		id: 660,
		time: 659,
		velocity: 2.45583333333333,
		power: 716.774809373303,
		road: 7462.15800925926,
		acceleration: 0.184351851851852
	},
	{
		id: 661,
		time: 660,
		velocity: 2.69777777777778,
		power: 544.552770728329,
		road: 7464.69629629629,
		acceleration: 0.095092592592593
	},
	{
		id: 662,
		time: 661,
		velocity: 2.60388888888889,
		power: 365.094444748289,
		road: 7467.29069444444,
		acceleration: 0.0171296296296295
	},
	{
		id: 663,
		time: 662,
		velocity: 2.50722222222222,
		power: 94.275298665447,
		road: 7469.84754629629,
		acceleration: -0.0922222222222224
	},
	{
		id: 664,
		time: 663,
		velocity: 2.42111111111111,
		power: 143.160074460956,
		road: 7472.32333333333,
		acceleration: -0.0699074074074071
	},
	{
		id: 665,
		time: 664,
		velocity: 2.39416666666667,
		power: 59.3185312323606,
		road: 7474.71194444444,
		acceleration: -0.104444444444445
	},
	{
		id: 666,
		time: 665,
		velocity: 2.19388888888889,
		power: 101.181795239659,
		road: 7477.00638888889,
		acceleration: -0.0838888888888887
	},
	{
		id: 667,
		time: 666,
		velocity: 2.16944444444444,
		power: 118.693208391864,
		road: 7479.22203703703,
		acceleration: -0.0737037037037038
	},
	{
		id: 668,
		time: 667,
		velocity: 2.17305555555556,
		power: 262.936834545309,
		road: 7481.39944444444,
		acceleration: -0.00277777777777777
	},
	{
		id: 669,
		time: 668,
		velocity: 2.18555555555556,
		power: 565.959165849478,
		road: 7483.64328703703,
		acceleration: 0.135648148148149
	},
	{
		id: 670,
		time: 669,
		velocity: 2.57638888888889,
		power: 523.500945841555,
		road: 7486.00643518518,
		acceleration: 0.102962962962963
	},
	{
		id: 671,
		time: 670,
		velocity: 2.48194444444444,
		power: 218.146531436975,
		road: 7488.4037037037,
		acceleration: -0.0347222222222228
	},
	{
		id: 672,
		time: 671,
		velocity: 2.08138888888889,
		power: -101.233564065858,
		road: 7490.69513888889,
		acceleration: -0.176944444444445
	},
	{
		id: 673,
		time: 672,
		velocity: 2.04555555555556,
		power: 136.093089428562,
		road: 7492.86611111111,
		acceleration: -0.063981481481481
	},
	{
		id: 674,
		time: 673,
		velocity: 2.29,
		power: 422.592681527062,
		road: 7495.0424074074,
		acceleration: 0.0746296296296296
	},
	{
		id: 675,
		time: 674,
		velocity: 2.30527777777778,
		power: 445.987244608227,
		road: 7497.29523148148,
		acceleration: 0.0784259259259255
	},
	{
		id: 676,
		time: 675,
		velocity: 2.28083333333333,
		power: 121.626532278536,
		road: 7499.55055555555,
		acceleration: -0.0734259259259256
	},
	{
		id: 677,
		time: 676,
		velocity: 2.06972222222222,
		power: -38.7757295129977,
		road: 7501.69462962963,
		acceleration: -0.149074074074074
	},
	{
		id: 678,
		time: 677,
		velocity: 1.85805555555556,
		power: 211.714963855984,
		road: 7503.75347222222,
		acceleration: -0.0213888888888891
	},
	{
		id: 679,
		time: 678,
		velocity: 2.21666666666667,
		power: 537.596561065256,
		road: 7505.87050925926,
		acceleration: 0.137777777777778
	},
	{
		id: 680,
		time: 679,
		velocity: 2.48305555555556,
		power: 582.385547962044,
		road: 7508.12731481481,
		acceleration: 0.141759259259259
	},
	{
		id: 681,
		time: 680,
		velocity: 2.28333333333333,
		power: 210.554851770771,
		road: 7510.43782407407,
		acceleration: -0.0343518518518517
	},
	{
		id: 682,
		time: 681,
		velocity: 2.11361111111111,
		power: -89.6004461274926,
		road: 7512.64467592592,
		acceleration: -0.172962962962963
	},
	{
		id: 683,
		time: 682,
		velocity: 1.96416666666667,
		power: 82.3042730593668,
		road: 7514.72101851852,
		acceleration: -0.0880555555555556
	},
	{
		id: 684,
		time: 683,
		velocity: 2.01916666666667,
		power: 127.137700892025,
		road: 7516.72199074074,
		acceleration: -0.0626851851851851
	},
	{
		id: 685,
		time: 684,
		velocity: 1.92555555555556,
		power: 207.570184833932,
		road: 7518.68263888889,
		acceleration: -0.0179629629629627
	},
	{
		id: 686,
		time: 685,
		velocity: 1.91027777777778,
		power: 373.062954683613,
		road: 7520.66851851852,
		acceleration: 0.0684259259259254
	},
	{
		id: 687,
		time: 686,
		velocity: 2.22444444444444,
		power: 761.31870975654,
		road: 7522.81092592592,
		acceleration: 0.24462962962963
	},
	{
		id: 688,
		time: 687,
		velocity: 2.65944444444444,
		power: 683.087083291531,
		road: 7525.16342592592,
		acceleration: 0.175555555555555
	},
	{
		id: 689,
		time: 688,
		velocity: 2.43694444444444,
		power: 282.0364515009,
		road: 7527.59935185185,
		acceleration: -0.00870370370370388
	},
	{
		id: 690,
		time: 689,
		velocity: 2.19833333333333,
		power: -42.8864403479143,
		road: 7529.95606481481,
		acceleration: -0.149722222222222
	},
	{
		id: 691,
		time: 690,
		velocity: 2.21027777777778,
		power: 13.46502185535,
		road: 7532.17601851852,
		acceleration: -0.123796296296296
	},
	{
		id: 692,
		time: 691,
		velocity: 2.06555555555556,
		power: -152.2791674528,
		road: 7534.23009259259,
		acceleration: -0.207962962962963
	},
	{
		id: 693,
		time: 692,
		velocity: 1.57444444444444,
		power: -284.520016384635,
		road: 7536.03231481481,
		acceleration: -0.295740740740741
	},
	{
		id: 694,
		time: 693,
		velocity: 1.32305555555556,
		power: -217.693703631515,
		road: 7537.54648148148,
		acceleration: -0.28037037037037
	},
	{
		id: 695,
		time: 694,
		velocity: 1.22444444444444,
		power: 38.7566071082488,
		road: 7538.87166666667,
		acceleration: -0.0975925925925929
	},
	{
		id: 696,
		time: 695,
		velocity: 1.28166666666667,
		power: 385.016117877412,
		road: 7540.23296296296,
		acceleration: 0.169814814814815
	},
	{
		id: 697,
		time: 696,
		velocity: 1.8325,
		power: 654.405717229051,
		road: 7541.83069444444,
		acceleration: 0.303055555555555
	},
	{
		id: 698,
		time: 697,
		velocity: 2.13361111111111,
		power: 689.866701788456,
		road: 7543.70888888889,
		acceleration: 0.25787037037037
	},
	{
		id: 699,
		time: 698,
		velocity: 2.05527777777778,
		power: 331.545339465395,
		road: 7545.73731481481,
		acceleration: 0.0425925925925927
	},
	{
		id: 700,
		time: 699,
		velocity: 1.96027777777778,
		power: -41.6202237871192,
		road: 7547.71111111111,
		acceleration: -0.151851851851852
	},
	{
		id: 701,
		time: 700,
		velocity: 1.67805555555556,
		power: -33.0713975167879,
		road: 7549.53476851852,
		acceleration: -0.148425925925926
	},
	{
		id: 702,
		time: 701,
		velocity: 1.61,
		power: -17.3316716133666,
		road: 7551.21425925926,
		acceleration: -0.139907407407407
	},
	{
		id: 703,
		time: 702,
		velocity: 1.54055555555556,
		power: 401.956839870966,
		road: 7552.88606481481,
		acceleration: 0.124537037037037
	},
	{
		id: 704,
		time: 703,
		velocity: 2.05166666666667,
		power: 622.871828369334,
		road: 7554.73324074074,
		acceleration: 0.226203703703704
	},
	{
		id: 705,
		time: 704,
		velocity: 2.28861111111111,
		power: 509.926532695584,
		road: 7556.7612037037,
		acceleration: 0.13537037037037
	},
	{
		id: 706,
		time: 705,
		velocity: 1.94666666666667,
		power: 226.010462534165,
		road: 7558.84898148148,
		acceleration: -0.0157407407407408
	},
	{
		id: 707,
		time: 706,
		velocity: 2.00444444444444,
		power: 0.329769193575538,
		road: 7560.86412037037,
		acceleration: -0.129537037037037
	},
	{
		id: 708,
		time: 707,
		velocity: 1.9,
		power: 240.021582703562,
		road: 7562.81458333333,
		acceleration: 0.000185185185185288
	},
	{
		id: 709,
		time: 708,
		velocity: 1.94722222222222,
		power: 319.340523055738,
		road: 7564.78574074074,
		acceleration: 0.0412037037037039
	},
	{
		id: 710,
		time: 709,
		velocity: 2.12805555555556,
		power: 605.942311889381,
		road: 7566.86611111111,
		acceleration: 0.177222222222222
	},
	{
		id: 711,
		time: 710,
		velocity: 2.43166666666667,
		power: 351.418181033359,
		road: 7569.05467592593,
		acceleration: 0.0391666666666666
	},
	{
		id: 712,
		time: 711,
		velocity: 2.06472222222222,
		power: 185.461470535962,
		road: 7571.2424537037,
		acceleration: -0.0407407407407403
	},
	{
		id: 713,
		time: 712,
		velocity: 2.00583333333333,
		power: -114.29802971244,
		road: 7573.31587962963,
		acceleration: -0.187962962962964
	},
	{
		id: 714,
		time: 713,
		velocity: 1.86777777777778,
		power: 394.409525504191,
		road: 7575.33351851852,
		acceleration: 0.0763888888888891
	},
	{
		id: 715,
		time: 714,
		velocity: 2.29388888888889,
		power: 676.525512120148,
		road: 7577.48972222222,
		acceleration: 0.200740740740741
	},
	{
		id: 716,
		time: 715,
		velocity: 2.60805555555556,
		power: 1039.78861268186,
		road: 7579.90759259259,
		acceleration: 0.322592592592592
	},
	{
		id: 717,
		time: 716,
		velocity: 2.83555555555556,
		power: 1163.93004035086,
		road: 7582.645,
		acceleration: 0.316481481481482
	},
	{
		id: 718,
		time: 717,
		velocity: 3.24333333333333,
		power: 1084.15894777438,
		road: 7585.66361111111,
		acceleration: 0.245925925925926
	},
	{
		id: 719,
		time: 718,
		velocity: 3.34583333333333,
		power: 1167.12199185581,
		road: 7588.92689814815,
		acceleration: 0.243425925925926
	},
	{
		id: 720,
		time: 719,
		velocity: 3.56583333333333,
		power: 595.049547181076,
		road: 7592.33685185185,
		acceleration: 0.0499074074074071
	},
	{
		id: 721,
		time: 720,
		velocity: 3.39305555555556,
		power: 1084.43796794052,
		road: 7595.86638888889,
		acceleration: 0.189259259259259
	},
	{
		id: 722,
		time: 721,
		velocity: 3.91361111111111,
		power: 964.02472042058,
		road: 7599.56046296296,
		acceleration: 0.139814814814816
	},
	{
		id: 723,
		time: 722,
		velocity: 3.98527777777778,
		power: 1387.58759115677,
		road: 7603.44462962963,
		acceleration: 0.24037037037037
	},
	{
		id: 724,
		time: 723,
		velocity: 4.11416666666667,
		power: 965.893308531634,
		road: 7607.50587962963,
		acceleration: 0.113796296296297
	},
	{
		id: 725,
		time: 724,
		velocity: 4.255,
		power: 1089.36615674022,
		road: 7611.69240740741,
		acceleration: 0.136759259259259
	},
	{
		id: 726,
		time: 725,
		velocity: 4.39555555555556,
		power: 1283.68944736236,
		road: 7616.03398148148,
		acceleration: 0.173333333333333
	},
	{
		id: 727,
		time: 726,
		velocity: 4.63416666666667,
		power: 1193.08226443521,
		road: 7620.5324537037,
		acceleration: 0.140462962962963
	},
	{
		id: 728,
		time: 727,
		velocity: 4.67638888888889,
		power: 2003.31395820271,
		road: 7625.25449074074,
		acceleration: 0.306666666666667
	},
	{
		id: 729,
		time: 728,
		velocity: 5.31555555555556,
		power: 1945.6111781525,
		road: 7630.26351851852,
		acceleration: 0.267314814814815
	},
	{
		id: 730,
		time: 729,
		velocity: 5.43611111111111,
		power: 2976.52980837953,
		road: 7635.62643518518,
		acceleration: 0.440462962962963
	},
	{
		id: 731,
		time: 730,
		velocity: 5.99777777777778,
		power: 3660.65340939676,
		road: 7641.46601851852,
		acceleration: 0.512870370370371
	},
	{
		id: 732,
		time: 731,
		velocity: 6.85416666666667,
		power: 5557.74801383573,
		road: 7647.93805555555,
		acceleration: 0.752037037037036
	},
	{
		id: 733,
		time: 732,
		velocity: 7.69222222222222,
		power: 5316.6726737186,
		road: 7655.09824074074,
		acceleration: 0.62425925925926
	},
	{
		id: 734,
		time: 733,
		velocity: 7.87055555555555,
		power: 2724.14171913656,
		road: 7662.67949074074,
		acceleration: 0.21787037037037
	},
	{
		id: 735,
		time: 734,
		velocity: 7.50777777777778,
		power: -454.448093110399,
		road: 7670.25828703704,
		acceleration: -0.222777777777778
	},
	{
		id: 736,
		time: 735,
		velocity: 7.02388888888889,
		power: -1955.50822109854,
		road: 7677.50537037037,
		acceleration: -0.440648148148148
	},
	{
		id: 737,
		time: 736,
		velocity: 6.54861111111111,
		power: -2498.69374684609,
		road: 7684.26111111111,
		acceleration: -0.542037037037037
	},
	{
		id: 738,
		time: 737,
		velocity: 5.88166666666667,
		power: -2310.30592121612,
		road: 7690.47574074074,
		acceleration: -0.540185185185185
	},
	{
		id: 739,
		time: 738,
		velocity: 5.40333333333333,
		power: -2616.76778433037,
		road: 7696.10300925926,
		acceleration: -0.634537037037037
	},
	{
		id: 740,
		time: 739,
		velocity: 4.645,
		power: -1285.09774377114,
		road: 7701.20953703704,
		acceleration: -0.406944444444443
	},
	{
		id: 741,
		time: 740,
		velocity: 4.66083333333333,
		power: -1404.28374295661,
		road: 7705.88462962963,
		acceleration: -0.455925925925927
	},
	{
		id: 742,
		time: 741,
		velocity: 4.03555555555556,
		power: -618.837649839996,
		road: 7710.18712962963,
		acceleration: -0.289259259259259
	},
	{
		id: 743,
		time: 742,
		velocity: 3.77722222222222,
		power: -803.30067511476,
		road: 7714.17064814815,
		acceleration: -0.348703703703704
	},
	{
		id: 744,
		time: 743,
		velocity: 3.61472222222222,
		power: -261.41761222653,
		road: 7717.87509259259,
		acceleration: -0.209444444444444
	},
	{
		id: 745,
		time: 744,
		velocity: 3.40722222222222,
		power: 268.233114469801,
		road: 7721.44708333333,
		acceleration: -0.055462962962963
	},
	{
		id: 746,
		time: 745,
		velocity: 3.61083333333333,
		power: 1343.74567235694,
		road: 7725.11671296296,
		acceleration: 0.250740740740741
	},
	{
		id: 747,
		time: 746,
		velocity: 4.36694444444444,
		power: 2893.32717404456,
		road: 7729.215,
		acceleration: 0.606574074074074
	},
	{
		id: 748,
		time: 747,
		velocity: 5.22694444444444,
		power: 4603.16582034553,
		road: 7734.04759259259,
		acceleration: 0.862037037037037
	},
	{
		id: 749,
		time: 748,
		velocity: 6.19694444444444,
		power: 4066.09850686559,
		road: 7739.6224537037,
		acceleration: 0.622499999999999
	},
	{
		id: 750,
		time: 749,
		velocity: 6.23444444444444,
		power: 2781.46594188218,
		road: 7745.6762037037,
		acceleration: 0.335277777777779
	},
	{
		id: 751,
		time: 750,
		velocity: 6.23277777777778,
		power: 2665.95272313233,
		road: 7752.04268518518,
		acceleration: 0.290185185185185
	},
	{
		id: 752,
		time: 751,
		velocity: 7.0675,
		power: 3899.14282674362,
		road: 7758.78194444444,
		acceleration: 0.45537037037037
	},
	{
		id: 753,
		time: 752,
		velocity: 7.60055555555556,
		power: 4091.42704104076,
		road: 7765.96981481481,
		acceleration: 0.441851851851852
	},
	{
		id: 754,
		time: 753,
		velocity: 7.55833333333333,
		power: 2485.02000991692,
		road: 7773.47310185185,
		acceleration: 0.188981481481482
	},
	{
		id: 755,
		time: 754,
		velocity: 7.63444444444445,
		power: 1373.16898517541,
		road: 7781.08564814815,
		acceleration: 0.0295370370370378
	},
	{
		id: 756,
		time: 755,
		velocity: 7.68916666666667,
		power: 2481.42975383595,
		road: 7788.80148148148,
		acceleration: 0.177037037037036
	},
	{
		id: 757,
		time: 756,
		velocity: 8.08944444444444,
		power: 2629.60588152234,
		road: 7796.69949074074,
		acceleration: 0.187314814814815
	},
	{
		id: 758,
		time: 757,
		velocity: 8.19638888888889,
		power: 4957.05600177431,
		road: 7804.9249537037,
		acceleration: 0.467592592592593
	},
	{
		id: 759,
		time: 758,
		velocity: 9.09194444444445,
		power: 4287.19573693934,
		road: 7813.56023148148,
		acceleration: 0.352037037037036
	},
	{
		id: 760,
		time: 759,
		velocity: 9.14555555555556,
		power: 3481.06842173322,
		road: 7822.4900462963,
		acceleration: 0.237037037037037
	},
	{
		id: 761,
		time: 760,
		velocity: 8.9075,
		power: -566.59149753467,
		road: 7831.41888888889,
		acceleration: -0.238981481481483
	},
	{
		id: 762,
		time: 761,
		velocity: 8.375,
		power: -2540.2089486654,
		road: 7839.98814814815,
		acceleration: -0.480185185185185
	},
	{
		id: 763,
		time: 762,
		velocity: 7.705,
		power: -2207.61139210365,
		road: 7848.09199074074,
		acceleration: -0.450648148148147
	},
	{
		id: 764,
		time: 763,
		velocity: 7.55555555555556,
		power: -835.811914399805,
		road: 7855.83319444444,
		acceleration: -0.274629629629629
	},
	{
		id: 765,
		time: 764,
		velocity: 7.55111111111111,
		power: 1179.79910064153,
		road: 7863.43861111111,
		acceleration: 0.00305555555555515
	},
	{
		id: 766,
		time: 765,
		velocity: 7.71416666666667,
		power: 2014.64861719453,
		road: 7871.10342592593,
		acceleration: 0.11574074074074
	},
	{
		id: 767,
		time: 766,
		velocity: 7.90277777777778,
		power: 3003.51152567032,
		road: 7878.9462962963,
		acceleration: 0.240370370370371
	},
	{
		id: 768,
		time: 767,
		velocity: 8.27222222222222,
		power: 3034.17358999698,
		road: 7887.02458333333,
		acceleration: 0.230462962962962
	},
	{
		id: 769,
		time: 768,
		velocity: 8.40555555555556,
		power: 3772.99079226024,
		road: 7895.37217592593,
		acceleration: 0.308148148148149
	},
	{
		id: 770,
		time: 769,
		velocity: 8.82722222222222,
		power: 3852.28003887676,
		road: 7904.02291666667,
		acceleration: 0.298148148148147
	},
	{
		id: 771,
		time: 770,
		velocity: 9.16666666666667,
		power: 4268.58712380524,
		road: 7912.98643518518,
		acceleration: 0.327407407407406
	},
	{
		id: 772,
		time: 771,
		velocity: 9.38777777777778,
		power: 2913.37883510417,
		road: 7922.19222222222,
		acceleration: 0.157129629629631
	},
	{
		id: 773,
		time: 772,
		velocity: 9.29861111111111,
		power: 1643.46126402812,
		road: 7931.48143518518,
		acceleration: 0.00972222222222108
	},
	{
		id: 774,
		time: 773,
		velocity: 9.19583333333333,
		power: 36.6827068512236,
		road: 7940.69,
		acceleration: -0.171018518518517
	},
	{
		id: 775,
		time: 774,
		velocity: 8.87472222222222,
		power: 783.915624463078,
		road: 7949.77143518518,
		acceleration: -0.0832407407407398
	},
	{
		id: 776,
		time: 775,
		velocity: 9.04888888888889,
		power: 1196.11193592562,
		road: 7958.79421296296,
		acceleration: -0.0340740740740753
	},
	{
		id: 777,
		time: 776,
		velocity: 9.09361111111111,
		power: 2642.82913577356,
		road: 7967.86601851852,
		acceleration: 0.132129629629629
	},
	{
		id: 778,
		time: 777,
		velocity: 9.27111111111111,
		power: 2808.85333168196,
		road: 7977.07638888889,
		acceleration: 0.145
	},
	{
		id: 779,
		time: 778,
		velocity: 9.48388888888889,
		power: 2962.24354323553,
		road: 7986.43699074074,
		acceleration: 0.155462962962963
	},
	{
		id: 780,
		time: 779,
		velocity: 9.56,
		power: 3146.15818383168,
		road: 7995.95949074074,
		acceleration: 0.168333333333335
	},
	{
		id: 781,
		time: 780,
		velocity: 9.77611111111111,
		power: 2753.75636242732,
		road: 8005.62564814815,
		acceleration: 0.11898148148148
	},
	{
		id: 782,
		time: 781,
		velocity: 9.84083333333333,
		power: 3061.24368289123,
		road: 8015.42449074074,
		acceleration: 0.14638888888889
	},
	{
		id: 783,
		time: 782,
		velocity: 9.99916666666667,
		power: 2723.48750317255,
		road: 8025.34907407407,
		acceleration: 0.105092592592591
	},
	{
		id: 784,
		time: 783,
		velocity: 10.0913888888889,
		power: 2644.71840144048,
		road: 8035.37263888889,
		acceleration: 0.0928703703703722
	},
	{
		id: 785,
		time: 784,
		velocity: 10.1194444444444,
		power: 2849.85799203643,
		road: 8045.49773148148,
		acceleration: 0.110185185185182
	},
	{
		id: 786,
		time: 785,
		velocity: 10.3297222222222,
		power: 2163.43272188427,
		road: 8055.6962037037,
		acceleration: 0.0365740740740765
	},
	{
		id: 787,
		time: 786,
		velocity: 10.2011111111111,
		power: 1028.64721585927,
		road: 8065.87310185185,
		acceleration: -0.0797222222222214
	},
	{
		id: 788,
		time: 787,
		velocity: 9.88027777777778,
		power: -1086.02650552489,
		road: 8075.86125,
		acceleration: -0.29777777777778
	},
	{
		id: 789,
		time: 788,
		velocity: 9.43638888888889,
		power: -1906.93551220193,
		road: 8085.50680555556,
		acceleration: -0.387407407407407
	},
	{
		id: 790,
		time: 789,
		velocity: 9.03888888888889,
		power: -1671.46572929317,
		road: 8094.77606481482,
		acceleration: -0.365185185185185
	},
	{
		id: 791,
		time: 790,
		velocity: 8.78472222222222,
		power: 13.5859062197132,
		road: 8103.77699074074,
		acceleration: -0.171481481481482
	},
	{
		id: 792,
		time: 791,
		velocity: 8.92194444444444,
		power: 1268.20729920611,
		road: 8112.68092592593,
		acceleration: -0.0224999999999991
	},
	{
		id: 793,
		time: 792,
		velocity: 8.97138888888889,
		power: 2476.46878188374,
		road: 8121.63259259259,
		acceleration: 0.117962962962963
	},
	{
		id: 794,
		time: 793,
		velocity: 9.13861111111111,
		power: 2167.56907674764,
		road: 8130.68222222222,
		acceleration: 0.0779629629629639
	},
	{
		id: 795,
		time: 794,
		velocity: 9.15583333333333,
		power: 2480.87365566149,
		road: 8139.82601851852,
		acceleration: 0.110370370370369
	},
	{
		id: 796,
		time: 795,
		velocity: 9.3025,
		power: 2363.78477385039,
		road: 8149.07143518519,
		acceleration: 0.0928703703703704
	},
	{
		id: 797,
		time: 796,
		velocity: 9.41722222222222,
		power: 2307.34577638229,
		road: 8158.40481481481,
		acceleration: 0.083055555555557
	},
	{
		id: 798,
		time: 797,
		velocity: 9.405,
		power: 2651.32438060541,
		road: 8167.83847222222,
		acceleration: 0.1175
	},
	{
		id: 799,
		time: 798,
		velocity: 9.655,
		power: 2214.76660814486,
		road: 8177.36365740741,
		acceleration: 0.0655555555555551
	},
	{
		id: 800,
		time: 799,
		velocity: 9.61388888888889,
		power: 3129.90880494209,
		road: 8187.00217592593,
		acceleration: 0.161111111111111
	},
	{
		id: 801,
		time: 800,
		velocity: 9.88833333333333,
		power: 1922.66593678758,
		road: 8196.73453703704,
		acceleration: 0.026574074074075
	},
	{
		id: 802,
		time: 801,
		velocity: 9.73472222222222,
		power: 2229.45090879529,
		road: 8206.50925925926,
		acceleration: 0.0581481481481472
	},
	{
		id: 803,
		time: 802,
		velocity: 9.78833333333333,
		power: 1235.51640479711,
		road: 8216.2887037037,
		acceleration: -0.0487037037037048
	},
	{
		id: 804,
		time: 803,
		velocity: 9.74222222222222,
		power: 1860.86141869829,
		road: 8226.05324074074,
		acceleration: 0.0188888888888918
	},
	{
		id: 805,
		time: 804,
		velocity: 9.79138888888889,
		power: 1413.42880139523,
		road: 8235.81268518518,
		acceleration: -0.0290740740740745
	},
	{
		id: 806,
		time: 805,
		velocity: 9.70111111111111,
		power: 1044.271282467,
		road: 8245.52375,
		acceleration: -0.0676851851851872
	},
	{
		id: 807,
		time: 806,
		velocity: 9.53916666666667,
		power: 517.260026496118,
		road: 8255.13944444445,
		acceleration: -0.123055555555554
	},
	{
		id: 808,
		time: 807,
		velocity: 9.42222222222222,
		power: 1083.86897160035,
		road: 8264.66407407408,
		acceleration: -0.0590740740740738
	},
	{
		id: 809,
		time: 808,
		velocity: 9.52388888888889,
		power: 986.144553134689,
		road: 8274.1249537037,
		acceleration: -0.0684259259259257
	},
	{
		id: 810,
		time: 809,
		velocity: 9.33388888888889,
		power: 1039.48715659359,
		road: 8283.52111111111,
		acceleration: -0.0610185185185195
	},
	{
		id: 811,
		time: 810,
		velocity: 9.23916666666667,
		power: 736.857288068636,
		road: 8292.84009259259,
		acceleration: -0.0933333333333337
	},
	{
		id: 812,
		time: 811,
		velocity: 9.24388888888889,
		power: 1004.72829533468,
		road: 8302.08171296296,
		acceleration: -0.0613888888888887
	},
	{
		id: 813,
		time: 812,
		velocity: 9.14972222222222,
		power: 370.970303205969,
		road: 8311.22666666667,
		acceleration: -0.131944444444446
	},
	{
		id: 814,
		time: 813,
		velocity: 8.84333333333333,
		power: -1033.48649768503,
		road: 8320.1587037037,
		acceleration: -0.293888888888887
	},
	{
		id: 815,
		time: 814,
		velocity: 8.36222222222222,
		power: -1779.52395262911,
		road: 8328.75050925926,
		acceleration: -0.386574074074076
	},
	{
		id: 816,
		time: 815,
		velocity: 7.99,
		power: -1914.47063297946,
		road: 8336.94365740741,
		acceleration: -0.41074074074074
	},
	{
		id: 817,
		time: 816,
		velocity: 7.61111111111111,
		power: -1149.34884661935,
		road: 8344.77333333333,
		acceleration: -0.316203703703703
	},
	{
		id: 818,
		time: 817,
		velocity: 7.41361111111111,
		power: -689.736375795684,
		road: 8352.31712962963,
		acceleration: -0.255555555555555
	},
	{
		id: 819,
		time: 818,
		velocity: 7.22333333333333,
		power: -1026.90629148486,
		road: 8359.58027777778,
		acceleration: -0.305740740740742
	},
	{
		id: 820,
		time: 819,
		velocity: 6.69388888888889,
		power: -1995.36090821044,
		road: 8366.46106481482,
		acceleration: -0.458981481481482
	},
	{
		id: 821,
		time: 820,
		velocity: 6.03666666666667,
		power: -1573.65864405457,
		road: 8372.90861111111,
		acceleration: -0.4075
	},
	{
		id: 822,
		time: 821,
		velocity: 6.00083333333333,
		power: -1154.08005348955,
		road: 8378.97833333333,
		acceleration: -0.348148148148148
	},
	{
		id: 823,
		time: 822,
		velocity: 5.64944444444444,
		power: 230.046521018993,
		road: 8384.82138888889,
		acceleration: -0.105185185185185
	},
	{
		id: 824,
		time: 823,
		velocity: 5.72111111111111,
		power: 198.259908857024,
		road: 8390.55708333333,
		acceleration: -0.109537037037036
	},
	{
		id: 825,
		time: 824,
		velocity: 5.67222222222222,
		power: 1222.6992853367,
		road: 8396.2775462963,
		acceleration: 0.0790740740740743
	},
	{
		id: 826,
		time: 825,
		velocity: 5.88666666666667,
		power: 1633.17469661318,
		road: 8402.11152777778,
		acceleration: 0.147962962962962
	},
	{
		id: 827,
		time: 826,
		velocity: 6.165,
		power: 2153.36301914917,
		road: 8408.13365740741,
		acceleration: 0.228333333333334
	},
	{
		id: 828,
		time: 827,
		velocity: 6.35722222222222,
		power: 2737.00595976284,
		road: 8414.42393518518,
		acceleration: 0.307962962962963
	},
	{
		id: 829,
		time: 828,
		velocity: 6.81055555555556,
		power: 3690.00669490098,
		road: 8421.08333333333,
		acceleration: 0.430277777777778
	},
	{
		id: 830,
		time: 829,
		velocity: 7.45583333333333,
		power: 3236.76508226011,
		road: 8428.12194444444,
		acceleration: 0.328148148148149
	},
	{
		id: 831,
		time: 830,
		velocity: 7.34166666666667,
		power: 3272.52813492716,
		road: 8435.47944444444,
		acceleration: 0.309629629629629
	},
	{
		id: 832,
		time: 831,
		velocity: 7.73944444444444,
		power: 2099.18013394043,
		road: 8443.0574537037,
		acceleration: 0.131388888888889
	},
	{
		id: 833,
		time: 832,
		velocity: 7.85,
		power: 1821.00562906098,
		road: 8450.74527777778,
		acceleration: 0.0882407407407406
	},
	{
		id: 834,
		time: 833,
		velocity: 7.60638888888889,
		power: 603.855904714179,
		road: 8458.43810185185,
		acceleration: -0.0782407407407399
	},
	{
		id: 835,
		time: 834,
		velocity: 7.50472222222222,
		power: 838.530634041029,
		road: 8466.06944444444,
		acceleration: -0.044722222222223
	},
	{
		id: 836,
		time: 835,
		velocity: 7.71583333333333,
		power: 1418.25718062721,
		road: 8473.69606481481,
		acceleration: 0.0352777777777789
	},
	{
		id: 837,
		time: 836,
		velocity: 7.71222222222222,
		power: 1402.3525380124,
		road: 8481.3562962963,
		acceleration: 0.0319444444444432
	},
	{
		id: 838,
		time: 837,
		velocity: 7.60055555555556,
		power: 449.405620355427,
		road: 8488.98337962963,
		acceleration: -0.0982407407407404
	},
	{
		id: 839,
		time: 838,
		velocity: 7.42111111111111,
		power: -185.446245856635,
		road: 8496.46884259259,
		acceleration: -0.185
	},
	{
		id: 840,
		time: 839,
		velocity: 7.15722222222222,
		power: 218.6831813844,
		road: 8503.79865740741,
		acceleration: -0.126296296296297
	},
	{
		id: 841,
		time: 840,
		velocity: 7.22166666666667,
		power: 768.263716664564,
		road: 8511.04259259259,
		acceleration: -0.0454629629629624
	},
	{
		id: 842,
		time: 841,
		velocity: 7.28472222222222,
		power: 1242.41178483154,
		road: 8518.27564814815,
		acceleration: 0.0237037037037036
	},
	{
		id: 843,
		time: 842,
		velocity: 7.22833333333333,
		power: 2141.87697659692,
		road: 8525.59555555555,
		acceleration: 0.149999999999999
	},
	{
		id: 844,
		time: 843,
		velocity: 7.67166666666667,
		power: 4231.14164821661,
		road: 8533.20273148148,
		acceleration: 0.424537037037037
	},
	{
		id: 845,
		time: 844,
		velocity: 8.55833333333333,
		power: 6826.39771777655,
		road: 8541.37824074074,
		acceleration: 0.71212962962963
	},
	{
		id: 846,
		time: 845,
		velocity: 9.36472222222222,
		power: 11143.5444700579,
		road: 8550.46652777778,
		acceleration: 1.11342592592593
	},
	{
		id: 847,
		time: 846,
		velocity: 11.0119444444444,
		power: 11201.9018350975,
		road: 8560.59893518518,
		acceleration: 0.974814814814815
	},
	{
		id: 848,
		time: 847,
		velocity: 11.4827777777778,
		power: 11385.6666550051,
		road: 8571.66032407407,
		acceleration: 0.883148148148148
	},
	{
		id: 849,
		time: 848,
		velocity: 12.0141666666667,
		power: 6533.43825424734,
		road: 8583.35393518519,
		acceleration: 0.381296296296297
	},
	{
		id: 850,
		time: 849,
		velocity: 12.1558333333333,
		power: 4725.03830194791,
		road: 8595.34060185185,
		acceleration: 0.204814814814815
	},
	{
		id: 851,
		time: 850,
		velocity: 12.0972222222222,
		power: 2467.42369956769,
		road: 8607.43171296296,
		acceleration: 0.00407407407407234
	},
	{
		id: 852,
		time: 851,
		velocity: 12.0263888888889,
		power: 3097.50803264104,
		road: 8619.55365740741,
		acceleration: 0.0575925925925915
	},
	{
		id: 853,
		time: 852,
		velocity: 12.3286111111111,
		power: 4047.71578715441,
		road: 8631.77222222222,
		acceleration: 0.13564814814815
	},
	{
		id: 854,
		time: 853,
		velocity: 12.5041666666667,
		power: 4353.83679280244,
		road: 8644.13634259259,
		acceleration: 0.155462962962963
	},
	{
		id: 855,
		time: 854,
		velocity: 12.4927777777778,
		power: 3733.50461272808,
		road: 8656.62712962963,
		acceleration: 0.0978703703703712
	},
	{
		id: 856,
		time: 855,
		velocity: 12.6222222222222,
		power: 2680.01425240567,
		road: 8669.17074074074,
		acceleration: 0.00777777777777722
	},
	{
		id: 857,
		time: 856,
		velocity: 12.5275,
		power: 2675.11940762164,
		road: 8681.72180555556,
		acceleration: 0.00712962962962926
	},
	{
		id: 858,
		time: 857,
		velocity: 12.5141666666667,
		power: 1734.97314365351,
		road: 8694.24115740741,
		acceleration: -0.0705555555555559
	},
	{
		id: 859,
		time: 858,
		velocity: 12.4105555555556,
		power: 2245.60759851848,
		road: 8706.71203703704,
		acceleration: -0.0263888888888886
	},
	{
		id: 860,
		time: 859,
		velocity: 12.4483333333333,
		power: 2139.40228509218,
		road: 8719.1525,
		acceleration: -0.0344444444444445
	},
	{
		id: 861,
		time: 860,
		velocity: 12.4108333333333,
		power: 4155.72631704117,
		road: 8731.64240740741,
		acceleration: 0.133333333333333
	},
	{
		id: 862,
		time: 861,
		velocity: 12.8105555555556,
		power: 2455.52558220453,
		road: 8744.19337962963,
		acceleration: -0.0112037037037034
	},
	{
		id: 863,
		time: 862,
		velocity: 12.4147222222222,
		power: 3564.56638419197,
		road: 8756.77879629629,
		acceleration: 0.0800925925925924
	},
	{
		id: 864,
		time: 863,
		velocity: 12.6511111111111,
		power: 2840.17632054609,
		road: 8769.41333333333,
		acceleration: 0.0181481481481462
	},
	{
		id: 865,
		time: 864,
		velocity: 12.865,
		power: 6182.78910079195,
		road: 8782.20046296296,
		acceleration: 0.287037037037038
	},
	{
		id: 866,
		time: 865,
		velocity: 13.2758333333333,
		power: 6290.60549124397,
		road: 8795.27134259259,
		acceleration: 0.280462962962964
	},
	{
		id: 867,
		time: 866,
		velocity: 13.4925,
		power: 6928.78500367478,
		road: 8808.63981481481,
		acceleration: 0.314722222222223
	},
	{
		id: 868,
		time: 867,
		velocity: 13.8091666666667,
		power: 6485.59597602957,
		road: 8822.29800925926,
		acceleration: 0.264722222222224
	},
	{
		id: 869,
		time: 868,
		velocity: 14.07,
		power: 6180.06334120324,
		road: 8836.20305555555,
		acceleration: 0.228981481481478
	},
	{
		id: 870,
		time: 869,
		velocity: 14.1794444444444,
		power: 4305.60849449054,
		road: 8850.26347222222,
		acceleration: 0.0817592592592611
	},
	{
		id: 871,
		time: 870,
		velocity: 14.0544444444444,
		power: 2223.37400439692,
		road: 8864.32805555555,
		acceleration: -0.0734259259259247
	},
	{
		id: 872,
		time: 871,
		velocity: 13.8497222222222,
		power: 740.718473083665,
		road: 8878.26527777777,
		acceleration: -0.181296296296296
	},
	{
		id: 873,
		time: 872,
		velocity: 13.6355555555556,
		power: 158.046468413277,
		road: 8892.00097222222,
		acceleration: -0.221759259259258
	},
	{
		id: 874,
		time: 873,
		velocity: 13.3891666666667,
		power: 118.747602937258,
		road: 8905.51518518518,
		acceleration: -0.221203703703706
	},
	{
		id: 875,
		time: 874,
		velocity: 13.1861111111111,
		power: 196.353978898698,
		road: 8918.81296296296,
		acceleration: -0.211666666666666
	},
	{
		id: 876,
		time: 875,
		velocity: 13.0005555555556,
		power: -100.012228170462,
		road: 8931.88898148148,
		acceleration: -0.231851851851852
	},
	{
		id: 877,
		time: 876,
		velocity: 12.6936111111111,
		power: -209.451462834212,
		road: 8944.73032407407,
		acceleration: -0.237500000000001
	},
	{
		id: 878,
		time: 877,
		velocity: 12.4736111111111,
		power: 67.5305006160802,
		road: 8957.34712962962,
		acceleration: -0.211574074074074
	},
	{
		id: 879,
		time: 878,
		velocity: 12.3658333333333,
		power: 841.144346967001,
		road: 8969.78625,
		acceleration: -0.143796296296296
	},
	{
		id: 880,
		time: 879,
		velocity: 12.2622222222222,
		power: 1990.47751050317,
		road: 8982.13129629629,
		acceleration: -0.0443518518518538
	},
	{
		id: 881,
		time: 880,
		velocity: 12.3405555555556,
		power: 2133.65858544905,
		road: 8994.43861111111,
		acceleration: -0.0311111111111106
	},
	{
		id: 882,
		time: 881,
		velocity: 12.2725,
		power: 2802.3577739799,
		road: 9006.74333333333,
		acceleration: 0.0259259259259252
	},
	{
		id: 883,
		time: 882,
		velocity: 12.34,
		power: 2709.58109052716,
		road: 9019.06967592592,
		acceleration: 0.0173148148148172
	},
	{
		id: 884,
		time: 883,
		velocity: 12.3925,
		power: 4100.31227432073,
		road: 9031.47087962963,
		acceleration: 0.132407407407406
	},
	{
		id: 885,
		time: 884,
		velocity: 12.6697222222222,
		power: 4311.47358367104,
		road: 9044.01041666666,
		acceleration: 0.144259259259259
	},
	{
		id: 886,
		time: 885,
		velocity: 12.7727777777778,
		power: 5626.47399050191,
		road: 9056.74416666666,
		acceleration: 0.244166666666665
	},
	{
		id: 887,
		time: 886,
		velocity: 13.125,
		power: 5929.62240380067,
		road: 9069.72800925925,
		acceleration: 0.25601851851852
	},
	{
		id: 888,
		time: 887,
		velocity: 13.4377777777778,
		power: 6632.72927346065,
		road: 9082.98856481481,
		acceleration: 0.297407407407407
	},
	{
		id: 889,
		time: 888,
		velocity: 13.665,
		power: 6858.08380719523,
		road: 9096.54717592592,
		acceleration: 0.298703703703703
	},
	{
		id: 890,
		time: 889,
		velocity: 14.0211111111111,
		power: 6552.32289860778,
		road: 9110.38537037036,
		acceleration: 0.260462962962965
	},
	{
		id: 891,
		time: 890,
		velocity: 14.2191666666667,
		power: 5636.2976894622,
		road: 9124.44425925925,
		acceleration: 0.180925925925926
	},
	{
		id: 892,
		time: 891,
		velocity: 14.2077777777778,
		power: 6422.30559337675,
		road: 9138.70824074073,
		acceleration: 0.229259259259258
	},
	{
		id: 893,
		time: 892,
		velocity: 14.7088888888889,
		power: 6136.90774582633,
		road: 9153.18592592592,
		acceleration: 0.19814814814815
	},
	{
		id: 894,
		time: 893,
		velocity: 14.8136111111111,
		power: 5202.81826626081,
		road: 9167.82458333333,
		acceleration: 0.123796296296295
	},
	{
		id: 895,
		time: 894,
		velocity: 14.5791666666667,
		power: -125.866314538873,
		road: 9182.39703703703,
		acceleration: -0.256203703703701
	},
	{
		id: 896,
		time: 895,
		velocity: 13.9402777777778,
		power: -2203.39288921289,
		road: 9196.63949074074,
		acceleration: -0.403796296296298
	},
	{
		id: 897,
		time: 896,
		velocity: 13.6022222222222,
		power: -3652.92173204637,
		road: 9210.42398148148,
		acceleration: -0.512129629629632
	},
	{
		id: 898,
		time: 897,
		velocity: 13.0427777777778,
		power: -1966.81425207973,
		road: 9223.76129629629,
		acceleration: -0.38222222222222
	},
	{
		id: 899,
		time: 898,
		velocity: 12.7936111111111,
		power: -2475.5159613748,
		road: 9236.69634259259,
		acceleration: -0.422314814814817
	},
	{
		id: 900,
		time: 899,
		velocity: 12.3352777777778,
		power: -1787.07962258025,
		road: 9249.2375,
		acceleration: -0.365462962962962
	},
	{
		id: 901,
		time: 900,
		velocity: 11.9463888888889,
		power: -1912.12530065958,
		road: 9261.40810185185,
		acceleration: -0.37564814814815
	},
	{
		id: 902,
		time: 901,
		velocity: 11.6666666666667,
		power: -708.335031942819,
		road: 9273.25625,
		acceleration: -0.269259259259258
	},
	{
		id: 903,
		time: 902,
		velocity: 11.5275,
		power: 454.082783922108,
		road: 9284.88837962963,
		acceleration: -0.162777777777778
	},
	{
		id: 904,
		time: 903,
		velocity: 11.4580555555556,
		power: 2243.54333023669,
		road: 9296.43962962963,
		acceleration: 0.00101851851852075
	},
	{
		id: 905,
		time: 904,
		velocity: 11.6697222222222,
		power: 2680.68525644202,
		road: 9308.01138888889,
		acceleration: 0.0399999999999956
	},
	{
		id: 906,
		time: 905,
		velocity: 11.6475,
		power: 1275.88788270654,
		road: 9319.55976851852,
		acceleration: -0.0867592592592565
	},
	{
		id: 907,
		time: 906,
		velocity: 11.1977777777778,
		power: -952.747752857285,
		road: 9330.92069444444,
		acceleration: -0.288148148148149
	},
	{
		id: 908,
		time: 907,
		velocity: 10.8052777777778,
		power: -2203.11934385913,
		road: 9341.93472222222,
		acceleration: -0.405648148148149
	},
	{
		id: 909,
		time: 908,
		velocity: 10.4305555555556,
		power: -2171.54301494894,
		road: 9352.54310185185,
		acceleration: -0.405648148148147
	},
	{
		id: 910,
		time: 909,
		velocity: 9.98083333333333,
		power: -1684.64059707128,
		road: 9362.76902777778,
		acceleration: -0.359259259259259
	},
	{
		id: 911,
		time: 910,
		velocity: 9.7275,
		power: -3715.95257776705,
		road: 9372.52486111111,
		acceleration: -0.580925925925927
	},
	{
		id: 912,
		time: 911,
		velocity: 8.68777777777778,
		power: -5665.12894814841,
		road: 9381.57472222222,
		acceleration: -0.831018518518517
	},
	{
		id: 913,
		time: 912,
		velocity: 7.48777777777778,
		power: -6609.32320747711,
		road: 9389.69935185185,
		acceleration: -1.01944444444444
	},
	{
		id: 914,
		time: 913,
		velocity: 6.66916666666667,
		power: -4621.90503383774,
		road: 9396.89847222222,
		acceleration: -0.831574074074074
	},
	{
		id: 915,
		time: 914,
		velocity: 6.19305555555556,
		power: -3341.84921910846,
		road: 9403.33333333333,
		acceleration: -0.696944444444446
	},
	{
		id: 916,
		time: 915,
		velocity: 5.39694444444444,
		power: -5102.978655221,
		road: 9408.86171296296,
		acceleration: -1.11601851851852
	},
	{
		id: 917,
		time: 916,
		velocity: 3.32111111111111,
		power: -4767.63226693987,
		road: 9413.18203703704,
		acceleration: -1.30009259259259
	},
	{
		id: 918,
		time: 917,
		velocity: 2.29277777777778,
		power: -3367.63229685962,
		road: 9416.1975462963,
		acceleration: -1.30953703703704
	},
	{
		id: 919,
		time: 918,
		velocity: 1.46833333333333,
		power: -1122.91350157339,
		road: 9418.19740740741,
		acceleration: -0.721759259259259
	},
	{
		id: 920,
		time: 919,
		velocity: 1.15583333333333,
		power: -456.09771704287,
		road: 9419.60069444445,
		acceleration: -0.471388888888889
	},
	{
		id: 921,
		time: 920,
		velocity: 0.878611111111111,
		power: -125.046667698738,
		road: 9420.64083333333,
		acceleration: -0.254907407407407
	},
	{
		id: 922,
		time: 921,
		velocity: 0.703611111111111,
		power: 266.838944525378,
		road: 9421.63157407408,
		acceleration: 0.156111111111111
	},
	{
		id: 923,
		time: 922,
		velocity: 1.62416666666667,
		power: 1329.97701629578,
		road: 9423.11027777778,
		acceleration: 0.819814814814814
	},
	{
		id: 924,
		time: 923,
		velocity: 3.33805555555556,
		power: 2978.48168132722,
		road: 9425.57125,
		acceleration: 1.14472222222222
	},
	{
		id: 925,
		time: 924,
		velocity: 4.13777777777778,
		power: 3820.64137565032,
		road: 9429.10652777778,
		acceleration: 1.00388888888889
	},
	{
		id: 926,
		time: 925,
		velocity: 4.63583333333333,
		power: 3401.57649330313,
		road: 9433.48375,
		acceleration: 0.68
	},
	{
		id: 927,
		time: 926,
		velocity: 5.37805555555556,
		power: 3171.91221478063,
		road: 9438.46537037037,
		acceleration: 0.528796296296296
	},
	{
		id: 928,
		time: 927,
		velocity: 5.72416666666667,
		power: 2499.11610095833,
		road: 9443.88217592593,
		acceleration: 0.341574074074074
	},
	{
		id: 929,
		time: 928,
		velocity: 5.66055555555556,
		power: 1865.648885806,
		road: 9449.56953703704,
		acceleration: 0.199537037037036
	},
	{
		id: 930,
		time: 929,
		velocity: 5.97666666666667,
		power: 1925.23481828965,
		road: 9455.45527777778,
		acceleration: 0.197222222222223
	},
	{
		id: 931,
		time: 930,
		velocity: 6.31583333333333,
		power: 3544.37929388252,
		road: 9461.66523148148,
		acceleration: 0.451203703703704
	},
	{
		id: 932,
		time: 931,
		velocity: 7.01416666666667,
		power: 4885.68940566397,
		road: 9468.40537037037,
		acceleration: 0.609166666666667
	},
	{
		id: 933,
		time: 932,
		velocity: 7.80416666666667,
		power: 4990.68139844286,
		road: 9475.72939814815,
		acceleration: 0.558611111111111
	},
	{
		id: 934,
		time: 933,
		velocity: 7.99166666666667,
		power: 3849.31530402712,
		road: 9483.51185185185,
		acceleration: 0.358240740740742
	},
	{
		id: 935,
		time: 934,
		velocity: 8.08888888888889,
		power: 2092.54124055271,
		road: 9491.52875,
		acceleration: 0.110648148148148
	},
	{
		id: 936,
		time: 935,
		velocity: 8.13611111111111,
		power: 668.085691499943,
		road: 9499.56277777778,
		acceleration: -0.0763888888888893
	},
	{
		id: 937,
		time: 936,
		velocity: 7.7625,
		power: -703.929212119792,
		road: 9507.43046296296,
		acceleration: -0.256296296296298
	},
	{
		id: 938,
		time: 937,
		velocity: 7.32,
		power: -1654.10313994922,
		road: 9514.9750462963,
		acceleration: -0.389907407407406
	},
	{
		id: 939,
		time: 938,
		velocity: 6.96638888888889,
		power: -1463.16404326149,
		road: 9522.13916666667,
		acceleration: -0.371018518518518
	},
	{
		id: 940,
		time: 939,
		velocity: 6.64944444444444,
		power: -952.784477939398,
		road: 9528.96759259259,
		acceleration: -0.300370370370371
	},
	{
		id: 941,
		time: 940,
		velocity: 6.41888888888889,
		power: 487.07865885082,
		road: 9535.60828703704,
		acceleration: -0.0750925925925925
	},
	{
		id: 942,
		time: 941,
		velocity: 6.74111111111111,
		power: 1498.24545431171,
		road: 9542.25384259259,
		acceleration: 0.0848148148148153
	},
	{
		id: 943,
		time: 942,
		velocity: 6.90388888888889,
		power: 2993.95590987348,
		road: 9549.095,
		acceleration: 0.306388888888889
	},
	{
		id: 944,
		time: 943,
		velocity: 7.33805555555556,
		power: 2343.45267464126,
		road: 9556.18523148148,
		acceleration: 0.191759259259258
	},
	{
		id: 945,
		time: 944,
		velocity: 7.31638888888889,
		power: 2406.00254968176,
		road: 9563.46638888889,
		acceleration: 0.190092592592594
	},
	{
		id: 946,
		time: 945,
		velocity: 7.47416666666667,
		power: 1486.9477764814,
		road: 9570.86902777778,
		acceleration: 0.0528703703703686
	},
	{
		id: 947,
		time: 946,
		velocity: 7.49666666666667,
		power: 903.399893379043,
		road: 9578.28296296296,
		acceleration: -0.0302777777777772
	},
	{
		id: 948,
		time: 947,
		velocity: 7.22555555555556,
		power: -345.947284873464,
		road: 9585.57814814815,
		acceleration: -0.207222222222223
	},
	{
		id: 949,
		time: 948,
		velocity: 6.8525,
		power: -1339.66195423265,
		road: 9592.59175925926,
		acceleration: -0.355925925925925
	},
	{
		id: 950,
		time: 949,
		velocity: 6.42888888888889,
		power: -1724.46306048352,
		road: 9599.21444444444,
		acceleration: -0.425925925925926
	},
	{
		id: 951,
		time: 950,
		velocity: 5.94777777777778,
		power: 263.853769646611,
		road: 9605.57092592592,
		acceleration: -0.106481481481481
	},
	{
		id: 952,
		time: 951,
		velocity: 6.53305555555555,
		power: 2164.12355247978,
		road: 9611.97657407407,
		acceleration: 0.204814814814815
	},
	{
		id: 953,
		time: 952,
		velocity: 7.04333333333333,
		power: 3511.41932396118,
		road: 9618.68351851852,
		acceleration: 0.397777777777779
	},
	{
		id: 954,
		time: 953,
		velocity: 7.14111111111111,
		power: 2209.24947039843,
		road: 9625.67791666666,
		acceleration: 0.177129629629629
	},
	{
		id: 955,
		time: 954,
		velocity: 7.06444444444444,
		power: 1354.18167638959,
		road: 9632.78314814814,
		acceleration: 0.0445370370370375
	},
	{
		id: 956,
		time: 955,
		velocity: 7.17694444444444,
		power: 508.978300490711,
		road: 9639.87055555555,
		acceleration: -0.0801851851851856
	},
	{
		id: 957,
		time: 956,
		velocity: 6.90055555555556,
		power: 2929.38139512708,
		road: 9647.05398148148,
		acceleration: 0.272222222222222
	},
	{
		id: 958,
		time: 957,
		velocity: 7.88111111111111,
		power: 4768.1466966704,
		road: 9654.62462962963,
		acceleration: 0.502222222222223
	},
	{
		id: 959,
		time: 958,
		velocity: 8.68361111111111,
		power: 10356.361105455,
		road: 9663.01138888889,
		acceleration: 1.13
	},
	{
		id: 960,
		time: 959,
		velocity: 10.2905555555556,
		power: 12383.8762681971,
		road: 9672.55481481481,
		acceleration: 1.18333333333333
	},
	{
		id: 961,
		time: 960,
		velocity: 11.4311111111111,
		power: 17391.7116944858,
		road: 9683.43138888889,
		acceleration: 1.48296296296296
	},
	{
		id: 962,
		time: 961,
		velocity: 13.1325,
		power: 12866.3538230785,
		road: 9695.50324074074,
		acceleration: 0.907592592592593
	},
	{
		id: 963,
		time: 962,
		velocity: 13.0133333333333,
		power: 12688.9086393208,
		road: 9708.43217592592,
		acceleration: 0.806574074074073
	},
	{
		id: 964,
		time: 963,
		velocity: 13.8508333333333,
		power: 8873.80829931228,
		road: 9721.99157407407,
		acceleration: 0.454351851851852
	},
	{
		id: 965,
		time: 964,
		velocity: 14.4955555555556,
		power: 11229.7189312501,
		road: 9736.07592592592,
		acceleration: 0.595555555555556
	},
	{
		id: 966,
		time: 965,
		velocity: 14.8,
		power: 13539.8644761861,
		road: 9750.81388888889,
		acceleration: 0.711666666666666
	},
	{
		id: 967,
		time: 966,
		velocity: 15.9858333333333,
		power: 14581.6662143122,
		road: 9766.27013888889,
		acceleration: 0.724907407407409
	},
	{
		id: 968,
		time: 967,
		velocity: 16.6702777777778,
		power: 20349.8949514189,
		road: 9782.60143518518,
		acceleration: 1.02518518518518
	},
	{
		id: 969,
		time: 968,
		velocity: 17.8755555555556,
		power: 16444.3077695808,
		road: 9799.79800925926,
		acceleration: 0.705370370370375
	},
	{
		id: 970,
		time: 969,
		velocity: 18.1019444444444,
		power: 16396.637380654,
		road: 9817.67273148148,
		acceleration: 0.650925925925925
	},
	{
		id: 971,
		time: 970,
		velocity: 18.6230555555556,
		power: 9680.76396364981,
		road: 9835.99069444444,
		acceleration: 0.235555555555553
	},
	{
		id: 972,
		time: 971,
		velocity: 18.5822222222222,
		power: 7893.41584812755,
		road: 9854.48916666666,
		acceleration: 0.12546296296296
	},
	{
		id: 973,
		time: 972,
		velocity: 18.4783333333333,
		power: 3502.66154390188,
		road: 9872.98912037037,
		acceleration: -0.122499999999995
	},
	{
		id: 974,
		time: 973,
		velocity: 18.2555555555556,
		power: 2994.7382537096,
		road: 9891.35425925926,
		acceleration: -0.147129629629628
	},
	{
		id: 975,
		time: 974,
		velocity: 18.1408333333333,
		power: 4029.07804007134,
		road: 9909.60361111111,
		acceleration: -0.084444444444447
	},
	{
		id: 976,
		time: 975,
		velocity: 18.225,
		power: 5068.78360533744,
		road: 9927.79925925926,
		acceleration: -0.0229629629629642
	},
	{
		id: 977,
		time: 976,
		velocity: 18.1866666666667,
		power: 6301.46420298338,
		road: 9946.00708333333,
		acceleration: 0.0473148148148148
	},
	{
		id: 978,
		time: 977,
		velocity: 18.2827777777778,
		power: 5074.46616373364,
		road: 9964.22680555555,
		acceleration: -0.0235185185185181
	},
	{
		id: 979,
		time: 978,
		velocity: 18.1544444444444,
		power: 5481.81059796186,
		road: 9982.4349074074,
		acceleration: 0.000277777777775157
	},
	{
		id: 980,
		time: 979,
		velocity: 18.1875,
		power: 3848.27307582713,
		road: 10000.5971759259,
		acceleration: -0.0919444444444437
	},
	{
		id: 981,
		time: 980,
		velocity: 18.0069444444444,
		power: 4252.24974880942,
		road: 10018.680462963,
		acceleration: -0.0660185185185149
	},
	{
		id: 982,
		time: 981,
		velocity: 17.9563888888889,
		power: -143.35477576804,
		road: 10036.5726388889,
		acceleration: -0.316203703703707
	},
	{
		id: 983,
		time: 982,
		velocity: 17.2388888888889,
		power: -1864.55621707763,
		road: 10054.1008333333,
		acceleration: -0.411759259259259
	},
	{
		id: 984,
		time: 983,
		velocity: 16.7716666666667,
		power: -5839.27840069274,
		road: 10071.0984259259,
		acceleration: -0.649444444444441
	},
	{
		id: 985,
		time: 984,
		velocity: 16.0080555555556,
		power: -3589.90036942999,
		road: 10087.5172685185,
		acceleration: -0.508055555555558
	},
	{
		id: 986,
		time: 985,
		velocity: 15.7147222222222,
		power: -3712.92437071628,
		road: 10103.4249537037,
		acceleration: -0.514259259259259
	},
	{
		id: 987,
		time: 986,
		velocity: 15.2288888888889,
		power: -642.391608080169,
		road: 10118.9224074074,
		acceleration: -0.306203703703707
	},
	{
		id: 988,
		time: 987,
		velocity: 15.0894444444444,
		power: -978.281162060735,
		road: 10134.1043518519,
		acceleration: -0.324814814814811
	},
	{
		id: 989,
		time: 988,
		velocity: 14.7402777777778,
		power: 1505.10291424437,
		road: 10149.0499074074,
		acceleration: -0.147962962962964
	},
	{
		id: 990,
		time: 989,
		velocity: 14.785,
		power: 3464.5784254538,
		road: 10163.9174074074,
		acceleration: -0.00814814814814824
	},
	{
		id: 991,
		time: 990,
		velocity: 15.065,
		power: 6174.73613095065,
		road: 10178.8701851852,
		acceleration: 0.178703703703702
	},
	{
		id: 992,
		time: 991,
		velocity: 15.2763888888889,
		power: 6338.16187028378,
		road: 10194.0031944444,
		acceleration: 0.181759259259261
	},
	{
		id: 993,
		time: 992,
		velocity: 15.3302777777778,
		power: 4541.25241465252,
		road: 10209.2536111111,
		acceleration: 0.0530555555555559
	},
	{
		id: 994,
		time: 993,
		velocity: 15.2241666666667,
		power: 3061.93610192714,
		road: 10224.5062962963,
		acceleration: -0.0485185185185202
	},
	{
		id: 995,
		time: 994,
		velocity: 15.1308333333333,
		power: 3128.83664370473,
		road: 10239.7134722222,
		acceleration: -0.0425000000000004
	},
	{
		id: 996,
		time: 995,
		velocity: 15.2027777777778,
		power: 2560.94906802868,
		road: 10254.8594907407,
		acceleration: -0.0798148148148137
	},
	{
		id: 997,
		time: 996,
		velocity: 14.9847222222222,
		power: 3107.26156827324,
		road: 10269.9455092593,
		acceleration: -0.0401851851851855
	},
	{
		id: 998,
		time: 997,
		velocity: 15.0102777777778,
		power: 2509.85441784184,
		road: 10284.9714814815,
		acceleration: -0.079907407407406
	},
	{
		id: 999,
		time: 998,
		velocity: 14.9630555555556,
		power: 2791.18060155173,
		road: 10299.9283796296,
		acceleration: -0.0582407407407413
	},
	{
		id: 1000,
		time: 999,
		velocity: 14.81,
		power: 3370.46307508715,
		road: 10314.8479166667,
		acceleration: -0.0164814814814793
	},
	{
		id: 1001,
		time: 1000,
		velocity: 14.9608333333333,
		power: 3922.29558904209,
		road: 10329.7702777778,
		acceleration: 0.0221296296296281
	},
	{
		id: 1002,
		time: 1001,
		velocity: 15.0294444444444,
		power: 4577.62291829022,
		road: 10344.7368981481,
		acceleration: 0.0663888888888877
	},
	{
		id: 1003,
		time: 1002,
		velocity: 15.0091666666667,
		power: 4105.08208279718,
		road: 10359.7525,
		acceleration: 0.0315740740740758
	},
	{
		id: 1004,
		time: 1003,
		velocity: 15.0555555555556,
		power: 2696.21061647359,
		road: 10374.7508333333,
		acceleration: -0.0661111111111108
	},
	{
		id: 1005,
		time: 1004,
		velocity: 14.8311111111111,
		power: 1694.67538566459,
		road: 10389.6493518519,
		acceleration: -0.133518518518519
	},
	{
		id: 1006,
		time: 1005,
		velocity: 14.6086111111111,
		power: 738.9092976744,
		road: 10404.3824537037,
		acceleration: -0.197314814814813
	},
	{
		id: 1007,
		time: 1006,
		velocity: 14.4636111111111,
		power: -158.40245617562,
		road: 10418.8881481481,
		acceleration: -0.2575
	},
	{
		id: 1008,
		time: 1007,
		velocity: 14.0586111111111,
		power: 1746.02517782661,
		road: 10433.2074074074,
		acceleration: -0.115370370370371
	},
	{
		id: 1009,
		time: 1008,
		velocity: 14.2625,
		power: 4679.64241336857,
		road: 10447.5187037037,
		acceleration: 0.099444444444444
	},
	{
		id: 1010,
		time: 1009,
		velocity: 14.7619444444444,
		power: 7722.30604926717,
		road: 10462.0350462963,
		acceleration: 0.310648148148148
	},
	{
		id: 1011,
		time: 1010,
		velocity: 14.9905555555556,
		power: 8158.57995820875,
		road: 10476.8687962963,
		acceleration: 0.324166666666665
	},
	{
		id: 1012,
		time: 1011,
		velocity: 15.235,
		power: 7383.3358307423,
		road: 10491.9918981481,
		acceleration: 0.254537037037039
	},
	{
		id: 1013,
		time: 1012,
		velocity: 15.5255555555556,
		power: 7094.73130832246,
		road: 10507.3536574074,
		acceleration: 0.222777777777777
	},
	{
		id: 1014,
		time: 1013,
		velocity: 15.6588888888889,
		power: 5889.50884340652,
		road: 10522.893287037,
		acceleration: 0.132962962962962
	},
	{
		id: 1015,
		time: 1014,
		velocity: 15.6338888888889,
		power: 4394.74007778525,
		road: 10538.5140740741,
		acceleration: 0.0293518518518514
	},
	{
		id: 1016,
		time: 1015,
		velocity: 15.6136111111111,
		power: 2749.45382525815,
		road: 10554.1094907407,
		acceleration: -0.0800925925925924
	},
	{
		id: 1017,
		time: 1016,
		velocity: 15.4186111111111,
		power: 1443.11453582475,
		road: 10569.5824537037,
		acceleration: -0.164814814814813
	},
	{
		id: 1018,
		time: 1017,
		velocity: 15.1394444444444,
		power: -471.578893313919,
		road: 10584.8275925926,
		acceleration: -0.290833333333335
	},
	{
		id: 1019,
		time: 1018,
		velocity: 14.7411111111111,
		power: -945.837023420786,
		road: 10599.7675462963,
		acceleration: -0.319537037037037
	},
	{
		id: 1020,
		time: 1019,
		velocity: 14.46,
		power: -812.396177438982,
		road: 10614.3946296296,
		acceleration: -0.306203703703703
	},
	{
		id: 1021,
		time: 1020,
		velocity: 14.2208333333333,
		power: 2080.64933105334,
		road: 10628.821712963,
		acceleration: -0.0937962962962953
	},
	{
		id: 1022,
		time: 1021,
		velocity: 14.4597222222222,
		power: 3187.56071912648,
		road: 10643.1960185185,
		acceleration: -0.011759259259259
	},
	{
		id: 1023,
		time: 1022,
		velocity: 14.4247222222222,
		power: 2527.64503220138,
		road: 10657.5350462963,
		acceleration: -0.0587962962962951
	},
	{
		id: 1024,
		time: 1023,
		velocity: 14.0444444444444,
		power: 357.297333795098,
		road: 10671.7372685185,
		acceleration: -0.214814814814815
	},
	{
		id: 1025,
		time: 1024,
		velocity: 13.8152777777778,
		power: 594.656961897722,
		road: 10685.7353703704,
		acceleration: -0.193425925925927
	},
	{
		id: 1026,
		time: 1025,
		velocity: 13.8444444444444,
		power: 1674.53756906393,
		road: 10699.5823148148,
		acceleration: -0.10888888888889
	},
	{
		id: 1027,
		time: 1026,
		velocity: 13.7177777777778,
		power: 2856.66797687998,
		road: 10713.3660648148,
		acceleration: -0.0174999999999983
	},
	{
		id: 1028,
		time: 1027,
		velocity: 13.7627777777778,
		power: 2757.98536600489,
		road: 10727.1288888889,
		acceleration: -0.0243518518518542
	},
	{
		id: 1029,
		time: 1028,
		velocity: 13.7713888888889,
		power: 3323.76479511146,
		road: 10740.8889351852,
		acceleration: 0.0187962962962995
	},
	{
		id: 1030,
		time: 1029,
		velocity: 13.7741666666667,
		power: 1042.19514629474,
		road: 10754.5816666667,
		acceleration: -0.153425925925928
	},
	{
		id: 1031,
		time: 1030,
		velocity: 13.3025,
		power: 658.055573556487,
		road: 10768.1078703704,
		acceleration: -0.179629629629627
	},
	{
		id: 1032,
		time: 1031,
		velocity: 13.2325,
		power: -41.4000023508905,
		road: 10781.4288888889,
		acceleration: -0.230740740740742
	},
	{
		id: 1033,
		time: 1032,
		velocity: 13.0819444444444,
		power: 2178.15123445639,
		road: 10794.6084259259,
		acceleration: -0.0522222222222233
	},
	{
		id: 1034,
		time: 1033,
		velocity: 13.1458333333333,
		power: 2006.88510240254,
		road: 10807.7297222222,
		acceleration: -0.0642592592592592
	},
	{
		id: 1035,
		time: 1034,
		velocity: 13.0397222222222,
		power: 1428.26698232989,
		road: 10820.7646759259,
		acceleration: -0.108425925925925
	},
	{
		id: 1036,
		time: 1035,
		velocity: 12.7566666666667,
		power: 1177.2957439059,
		road: 10833.6824074074,
		acceleration: -0.126018518518519
	},
	{
		id: 1037,
		time: 1036,
		velocity: 12.7677777777778,
		power: 1459.09499455321,
		road: 10846.4868981482,
		acceleration: -0.100462962962963
	},
	{
		id: 1038,
		time: 1037,
		velocity: 12.7383333333333,
		power: 2493.34564291744,
		road: 10859.2341203704,
		acceleration: -0.0140740740740757
	},
	{
		id: 1039,
		time: 1038,
		velocity: 12.7144444444444,
		power: 1240.06373953056,
		road: 10871.9164814815,
		acceleration: -0.115648148148146
	},
	{
		id: 1040,
		time: 1039,
		velocity: 12.4208333333333,
		power: 479.88430460733,
		road: 10884.4530555556,
		acceleration: -0.175925925925926
	},
	{
		id: 1041,
		time: 1040,
		velocity: 12.2105555555556,
		power: 464.890440412192,
		road: 10896.8145833333,
		acceleration: -0.174166666666668
	},
	{
		id: 1042,
		time: 1041,
		velocity: 12.1919444444444,
		power: 1568.0926128735,
		road: 10909.0502777778,
		acceleration: -0.077499999999997
	},
	{
		id: 1043,
		time: 1042,
		velocity: 12.1883333333333,
		power: 2951.87231181494,
		road: 10921.2680555556,
		acceleration: 0.0416666666666643
	},
	{
		id: 1044,
		time: 1043,
		velocity: 12.3355555555556,
		power: 3602.51046559317,
		road: 10933.5540740741,
		acceleration: 0.0948148148148142
	},
	{
		id: 1045,
		time: 1044,
		velocity: 12.4763888888889,
		power: 3861.46971532867,
		road: 10945.9438425926,
		acceleration: 0.112685185185185
	},
	{
		id: 1046,
		time: 1045,
		velocity: 12.5263888888889,
		power: 2266.94909407559,
		road: 10958.3781944444,
		acceleration: -0.0235185185185163
	},
	{
		id: 1047,
		time: 1046,
		velocity: 12.265,
		power: 1798.84196117908,
		road: 10970.7698611111,
		acceleration: -0.0618518518518538
	},
	{
		id: 1048,
		time: 1047,
		velocity: 12.2908333333333,
		power: 2391.16092271157,
		road: 10983.1252777778,
		acceleration: -0.010648148148146
	},
	{
		id: 1049,
		time: 1048,
		velocity: 12.4944444444444,
		power: 3889.59037211528,
		road: 10995.5325462963,
		acceleration: 0.11435185185185
	},
	{
		id: 1050,
		time: 1049,
		velocity: 12.6080555555556,
		power: 4957.39885351035,
		road: 11008.0955555556,
		acceleration: 0.197129629629629
	},
	{
		id: 1051,
		time: 1050,
		velocity: 12.8822222222222,
		power: 5272.14878443687,
		road: 11020.8637962963,
		acceleration: 0.213333333333333
	},
	{
		id: 1052,
		time: 1051,
		velocity: 13.1344444444444,
		power: 5806.34544859468,
		road: 11033.8613888889,
		acceleration: 0.245370370370372
	},
	{
		id: 1053,
		time: 1052,
		velocity: 13.3441666666667,
		power: 4396.27565001806,
		road: 11047.0436574074,
		acceleration: 0.123981481481481
	},
	{
		id: 1054,
		time: 1053,
		velocity: 13.2541666666667,
		power: 3446.34071409439,
		road: 11060.3106481481,
		acceleration: 0.0454629629629633
	},
	{
		id: 1055,
		time: 1054,
		velocity: 13.2708333333333,
		power: 3246.67014119202,
		road: 11073.6145833333,
		acceleration: 0.0284259259259247
	},
	{
		id: 1056,
		time: 1055,
		velocity: 13.4294444444444,
		power: 3910.02413007743,
		road: 11086.972037037,
		acceleration: 0.0786111111111136
	},
	{
		id: 1057,
		time: 1056,
		velocity: 13.49,
		power: 4790.48389175108,
		road: 11100.4402314815,
		acceleration: 0.142870370370369
	},
	{
		id: 1058,
		time: 1057,
		velocity: 13.6994444444444,
		power: 3921.11597882501,
		road: 11114.015462963,
		acceleration: 0.0712037037037039
	},
	{
		id: 1059,
		time: 1058,
		velocity: 13.6430555555556,
		power: 3802.48907130201,
		road: 11127.6561111111,
		acceleration: 0.0596296296296295
	},
	{
		id: 1060,
		time: 1059,
		velocity: 13.6688888888889,
		power: 3129.45184111398,
		road: 11141.33,
		acceleration: 0.00685185185185233
	},
	{
		id: 1061,
		time: 1060,
		velocity: 13.72,
		power: 3821.00368842738,
		road: 11155.0366203704,
		acceleration: 0.0586111111111105
	},
	{
		id: 1062,
		time: 1061,
		velocity: 13.8188888888889,
		power: 3736.63974605222,
		road: 11168.7976388889,
		acceleration: 0.0501851851851853
	},
	{
		id: 1063,
		time: 1062,
		velocity: 13.8194444444444,
		power: 4170.16837322789,
		road: 11182.6240740741,
		acceleration: 0.080648148148148
	},
	{
		id: 1064,
		time: 1063,
		velocity: 13.9619444444444,
		power: 4550.16191618227,
		road: 11196.5436574074,
		acceleration: 0.105648148148148
	},
	{
		id: 1065,
		time: 1064,
		velocity: 14.1358333333333,
		power: 4852.89476042132,
		road: 11210.5778703704,
		acceleration: 0.12361111111111
	},
	{
		id: 1066,
		time: 1065,
		velocity: 14.1902777777778,
		power: 4833.08284248287,
		road: 11224.7324537037,
		acceleration: 0.117129629629632
	},
	{
		id: 1067,
		time: 1066,
		velocity: 14.3133333333333,
		power: 4505.13494244499,
		road: 11238.99,
		acceleration: 0.0887962962962945
	},
	{
		id: 1068,
		time: 1067,
		velocity: 14.4022222222222,
		power: 4581.50365387409,
		road: 11253.3373611111,
		acceleration: 0.0908333333333307
	},
	{
		id: 1069,
		time: 1068,
		velocity: 14.4627777777778,
		power: 4119.88036030803,
		road: 11267.7573611111,
		acceleration: 0.0544444444444476
	},
	{
		id: 1070,
		time: 1069,
		velocity: 14.4766666666667,
		power: 3755.67916874824,
		road: 11282.2178703704,
		acceleration: 0.0265740740740714
	},
	{
		id: 1071,
		time: 1070,
		velocity: 14.4819444444444,
		power: 2493.4908289418,
		road: 11296.659537037,
		acceleration: -0.0642592592592592
	},
	{
		id: 1072,
		time: 1071,
		velocity: 14.27,
		power: 1155.40191753942,
		road: 11310.9896759259,
		acceleration: -0.158796296296295
	},
	{
		id: 1073,
		time: 1072,
		velocity: 14.0002777777778,
		power: -152.113272365472,
		road: 11325.1148148148,
		acceleration: -0.251203703703704
	},
	{
		id: 1074,
		time: 1073,
		velocity: 13.7283333333333,
		power: -663.403299347892,
		road: 11338.9714351852,
		acceleration: -0.285833333333334
	},
	{
		id: 1075,
		time: 1074,
		velocity: 13.4125,
		power: 389.496935964227,
		road: 11352.5841666667,
		acceleration: -0.201944444444445
	},
	{
		id: 1076,
		time: 1075,
		velocity: 13.3944444444444,
		power: 1399.58211125355,
		road: 11366.0356944444,
		acceleration: -0.120462962962963
	},
	{
		id: 1077,
		time: 1076,
		velocity: 13.3669444444444,
		power: 2395.54260623811,
		road: 11379.406712963,
		acceleration: -0.0405555555555548
	},
	{
		id: 1078,
		time: 1077,
		velocity: 13.2908333333333,
		power: 2265.72800435437,
		road: 11392.7327314815,
		acceleration: -0.0494444444444451
	},
	{
		id: 1079,
		time: 1078,
		velocity: 13.2461111111111,
		power: 2212.15823860631,
		road: 11406.0079166667,
		acceleration: -0.0522222222222215
	},
	{
		id: 1080,
		time: 1079,
		velocity: 13.2102777777778,
		power: 3881.27594705588,
		road: 11419.2964814815,
		acceleration: 0.0789814814814829
	},
	{
		id: 1081,
		time: 1080,
		velocity: 13.5277777777778,
		power: 5140.16394759626,
		road: 11432.710787037,
		acceleration: 0.172499999999999
	},
	{
		id: 1082,
		time: 1081,
		velocity: 13.7636111111111,
		power: 5378.46617821088,
		road: 11446.3027777778,
		acceleration: 0.18287037037037
	},
	{
		id: 1083,
		time: 1082,
		velocity: 13.7588888888889,
		power: 4290.04429253447,
		road: 11460.032962963,
		acceleration: 0.0935185185185183
	},
	{
		id: 1084,
		time: 1083,
		velocity: 13.8083333333333,
		power: 3423.57798683546,
		road: 11473.8225925926,
		acceleration: 0.0253703703703696
	},
	{
		id: 1085,
		time: 1084,
		velocity: 13.8397222222222,
		power: 2822.33256751329,
		road: 11487.6147222222,
		acceleration: -0.0203703703703706
	},
	{
		id: 1086,
		time: 1085,
		velocity: 13.6977777777778,
		power: 2262.0633725888,
		road: 11501.365787037,
		acceleration: -0.061759259259258
	},
	{
		id: 1087,
		time: 1086,
		velocity: 13.6230555555556,
		power: 56.0931343547597,
		road: 11514.9722222222,
		acceleration: -0.227499999999999
	},
	{
		id: 1088,
		time: 1087,
		velocity: 13.1572222222222,
		power: -758.187326404322,
		road: 11528.3212037037,
		acceleration: -0.287407407407411
	},
	{
		id: 1089,
		time: 1088,
		velocity: 12.8355555555556,
		power: -2087.78835178783,
		road: 11541.3309722222,
		acceleration: -0.391018518518516
	},
	{
		id: 1090,
		time: 1089,
		velocity: 12.45,
		power: -945.38543374371,
		road: 11553.9971759259,
		acceleration: -0.296111111111111
	},
	{
		id: 1091,
		time: 1090,
		velocity: 12.2688888888889,
		power: -849.602055581967,
		road: 11566.3724537037,
		acceleration: -0.28574074074074
	},
	{
		id: 1092,
		time: 1091,
		velocity: 11.9783333333333,
		power: -2100.08773661969,
		road: 11578.4088425926,
		acceleration: -0.392037037037037
	},
	{
		id: 1093,
		time: 1092,
		velocity: 11.2738888888889,
		power: -3135.17698520915,
		road: 11590.0058333333,
		acceleration: -0.486759259259259
	},
	{
		id: 1094,
		time: 1093,
		velocity: 10.8086111111111,
		power: -2883.62316652811,
		road: 11601.1248611111,
		acceleration: -0.469166666666666
	},
	{
		id: 1095,
		time: 1094,
		velocity: 10.5708333333333,
		power: -521.075483828488,
		road: 11611.8875462963,
		acceleration: -0.243518518518519
	},
	{
		id: 1096,
		time: 1095,
		velocity: 10.5433333333333,
		power: -642.801988068282,
		road: 11622.4015277778,
		acceleration: -0.25388888888889
	},
	{
		id: 1097,
		time: 1096,
		velocity: 10.0469444444444,
		power: -29.0584978909096,
		road: 11632.6935185185,
		acceleration: -0.190092592592592
	},
	{
		id: 1098,
		time: 1097,
		velocity: 10.0005555555556,
		power: 954.696213460285,
		road: 11642.847037037,
		acceleration: -0.0868518518518542
	},
	{
		id: 1099,
		time: 1098,
		velocity: 10.2827777777778,
		power: 2979.20221847192,
		road: 11653.017962963,
		acceleration: 0.121666666666666
	},
	{
		id: 1100,
		time: 1099,
		velocity: 10.4119444444444,
		power: 4341.62899510942,
		road: 11663.3756944444,
		acceleration: 0.251944444444446
	},
	{
		id: 1101,
		time: 1100,
		velocity: 10.7563888888889,
		power: 4098.81954855137,
		road: 11673.9670833333,
		acceleration: 0.215370370370371
	},
	{
		id: 1102,
		time: 1101,
		velocity: 10.9288888888889,
		power: 4189.92035704607,
		road: 11684.7729166667,
		acceleration: 0.213518518518519
	},
	{
		id: 1103,
		time: 1102,
		velocity: 11.0525,
		power: 4048.88539746928,
		road: 11695.7805555556,
		acceleration: 0.190092592592592
	},
	{
		id: 1104,
		time: 1103,
		velocity: 11.3266666666667,
		power: 4411.15738920855,
		road: 11706.990462963,
		acceleration: 0.214444444444446
	},
	{
		id: 1105,
		time: 1104,
		velocity: 11.5722222222222,
		power: 5773.44191502463,
		road: 11718.4704166667,
		acceleration: 0.325648148148147
	},
	{
		id: 1106,
		time: 1105,
		velocity: 12.0294444444444,
		power: 5999.37278297757,
		road: 11730.2765740741,
		acceleration: 0.326759259259257
	},
	{
		id: 1107,
		time: 1106,
		velocity: 12.3069444444444,
		power: 7405.9844047119,
		road: 11742.4591666667,
		acceleration: 0.426111111111112
	},
	{
		id: 1108,
		time: 1107,
		velocity: 12.8505555555556,
		power: 6574.92679659791,
		road: 11755.0208796296,
		acceleration: 0.332129629629632
	},
	{
		id: 1109,
		time: 1108,
		velocity: 13.0258333333333,
		power: 5081.54344754002,
		road: 11767.8461574074,
		acceleration: 0.194999999999997
	},
	{
		id: 1110,
		time: 1109,
		velocity: 12.8919444444444,
		power: 2540.39036837815,
		road: 11780.76125,
		acceleration: -0.0153703703703698
	},
	{
		id: 1111,
		time: 1110,
		velocity: 12.8044444444444,
		power: 2065.38708554921,
		road: 11793.6421759259,
		acceleration: -0.0529629629629618
	},
	{
		id: 1112,
		time: 1111,
		velocity: 12.8669444444444,
		power: 2032.49373576431,
		road: 11806.469537037,
		acceleration: -0.0541666666666671
	},
	{
		id: 1113,
		time: 1112,
		velocity: 12.7294444444444,
		power: 2518.53952128791,
		road: 11819.2631018519,
		acceleration: -0.013425925925926
	},
	{
		id: 1114,
		time: 1113,
		velocity: 12.7641666666667,
		power: 2803.86826543548,
		road: 11832.0549537037,
		acceleration: 0.0100000000000016
	},
	{
		id: 1115,
		time: 1114,
		velocity: 12.8969444444444,
		power: 3627.32270135683,
		road: 11844.8897222222,
		acceleration: 0.0758333333333319
	},
	{
		id: 1116,
		time: 1115,
		velocity: 12.9569444444444,
		power: 4235.41527788377,
		road: 11857.8231018519,
		acceleration: 0.121388888888891
	},
	{
		id: 1117,
		time: 1116,
		velocity: 13.1283333333333,
		power: 4365.94476796125,
		road: 11870.8805555556,
		acceleration: 0.126759259259259
	},
	{
		id: 1118,
		time: 1117,
		velocity: 13.2772222222222,
		power: 5428.51196672731,
		road: 11884.1034259259,
		acceleration: 0.204074074074073
	},
	{
		id: 1119,
		time: 1118,
		velocity: 13.5691666666667,
		power: 5773.5445799864,
		road: 11897.5387962963,
		acceleration: 0.220925925925926
	},
	{
		id: 1120,
		time: 1119,
		velocity: 13.7911111111111,
		power: 6543.86732531321,
		road: 11911.2186574074,
		acceleration: 0.268055555555556
	},
	{
		id: 1121,
		time: 1120,
		velocity: 14.0813888888889,
		power: 6027.60715643605,
		road: 11925.1408796296,
		acceleration: 0.216666666666667
	},
	{
		id: 1122,
		time: 1121,
		velocity: 14.2191666666667,
		power: 6122.34002189033,
		road: 11939.2781018519,
		acceleration: 0.213333333333333
	},
	{
		id: 1123,
		time: 1122,
		velocity: 14.4311111111111,
		power: 4819.56608524218,
		road: 11953.5770833333,
		acceleration: 0.110185185185182
	},
	{
		id: 1124,
		time: 1123,
		velocity: 14.4119444444444,
		power: 4352.89456180298,
		road: 11967.9674074074,
		acceleration: 0.0725000000000016
	},
	{
		id: 1125,
		time: 1124,
		velocity: 14.4366666666667,
		power: 3576.75141739981,
		road: 11982.40125,
		acceleration: 0.014537037037039
	},
	{
		id: 1126,
		time: 1125,
		velocity: 14.4747222222222,
		power: 4596.18985007445,
		road: 11996.8856018519,
		acceleration: 0.0864814814814796
	},
	{
		id: 1127,
		time: 1126,
		velocity: 14.6713888888889,
		power: 4121.13892751779,
		road: 12011.4380092593,
		acceleration: 0.0496296296296315
	},
	{
		id: 1128,
		time: 1127,
		velocity: 14.5855555555556,
		power: 3581.79430399898,
		road: 12026.0201388889,
		acceleration: 0.00981481481481161
	},
	{
		id: 1129,
		time: 1128,
		velocity: 14.5041666666667,
		power: 1988.51532087393,
		road: 12040.5555092593,
		acceleration: -0.103333333333332
	},
	{
		id: 1130,
		time: 1129,
		velocity: 14.3613888888889,
		power: 2037.66667587821,
		road: 12054.9906481481,
		acceleration: -0.0971296296296273
	},
	{
		id: 1131,
		time: 1130,
		velocity: 14.2941666666667,
		power: 410.497117845127,
		road: 12069.2710648148,
		acceleration: -0.212314814814818
	},
	{
		id: 1132,
		time: 1131,
		velocity: 13.8672222222222,
		power: 180.650338508142,
		road: 12083.3325925926,
		acceleration: -0.225462962962963
	},
	{
		id: 1133,
		time: 1132,
		velocity: 13.685,
		power: -1205.8895511407,
		road: 12097.118287037,
		acceleration: -0.326203703703705
	},
	{
		id: 1134,
		time: 1133,
		velocity: 13.3155555555556,
		power: -954.624575058828,
		road: 12110.5888888889,
		acceleration: -0.303981481481481
	},
	{
		id: 1135,
		time: 1134,
		velocity: 12.9552777777778,
		power: -2031.78399648374,
		road: 12123.7141203704,
		acceleration: -0.386759259259259
	},
	{
		id: 1136,
		time: 1135,
		velocity: 12.5247222222222,
		power: -1279.19014439141,
		road: 12136.4837962963,
		acceleration: -0.324351851851851
	},
	{
		id: 1137,
		time: 1136,
		velocity: 12.3425,
		power: -529.857468604488,
		road: 12148.9614351852,
		acceleration: -0.259722222222221
	},
	{
		id: 1138,
		time: 1137,
		velocity: 12.1761111111111,
		power: 714.714235871686,
		road: 12161.2335648148,
		acceleration: -0.151296296296296
	},
	{
		id: 1139,
		time: 1138,
		velocity: 12.0708333333333,
		power: 1110.30688071154,
		road: 12173.3727314815,
		acceleration: -0.114629629629631
	},
	{
		id: 1140,
		time: 1139,
		velocity: 11.9986111111111,
		power: 1255.82981749567,
		road: 12185.4047685185,
		acceleration: -0.0996296296296304
	},
	{
		id: 1141,
		time: 1140,
		velocity: 11.8772222222222,
		power: 1781.05626916878,
		road: 12197.3610648148,
		acceleration: -0.0518518518518505
	},
	{
		id: 1142,
		time: 1141,
		velocity: 11.9152777777778,
		power: 1720.61740842287,
		road: 12209.2635648148,
		acceleration: -0.05574074074074
	},
	{
		id: 1143,
		time: 1142,
		velocity: 11.8313888888889,
		power: 2483.2319275059,
		road: 12221.1442592593,
		acceleration: 0.0121296296296283
	},
	{
		id: 1144,
		time: 1143,
		velocity: 11.9136111111111,
		power: 2347.90609728045,
		road: 12233.0310185185,
		acceleration: 0
	},
	{
		id: 1145,
		time: 1144,
		velocity: 11.9152777777778,
		power: 2458.8201453152,
		road: 12244.9225925926,
		acceleration: 0.00962962962962877
	},
	{
		id: 1146,
		time: 1145,
		velocity: 11.8602777777778,
		power: 2611.86430893182,
		road: 12256.8302777778,
		acceleration: 0.0225925925925932
	},
	{
		id: 1147,
		time: 1146,
		velocity: 11.9813888888889,
		power: 2604.24702537111,
		road: 12268.7598611111,
		acceleration: 0.0212037037037049
	},
	{
		id: 1148,
		time: 1147,
		velocity: 11.9788888888889,
		power: 1902.72826515979,
		road: 12280.6799537037,
		acceleration: -0.0401851851851873
	},
	{
		id: 1149,
		time: 1148,
		velocity: 11.7397222222222,
		power: -426.500192795929,
		road: 12292.4581481481,
		acceleration: -0.243611111111109
	},
	{
		id: 1150,
		time: 1149,
		velocity: 11.2505555555556,
		power: -1953.05485247845,
		road: 12303.9244444444,
		acceleration: -0.380185185185187
	},
	{
		id: 1151,
		time: 1150,
		velocity: 10.8383333333333,
		power: -2538.98128866914,
		road: 12314.982037037,
		acceleration: -0.437222222222221
	},
	{
		id: 1152,
		time: 1151,
		velocity: 10.4280555555556,
		power: 397.330642814577,
		road: 12325.7440277778,
		acceleration: -0.15398148148148
	},
	{
		id: 1153,
		time: 1152,
		velocity: 10.7886111111111,
		power: 2931.94897974772,
		road: 12336.4761574074,
		acceleration: 0.0942592592592568
	},
	{
		id: 1154,
		time: 1153,
		velocity: 11.1211111111111,
		power: 5765.05727641338,
		road: 12347.4337962963,
		acceleration: 0.356759259259261
	},
	{
		id: 1155,
		time: 1154,
		velocity: 11.4983333333333,
		power: 6052.82894092629,
		road: 12358.750462963,
		acceleration: 0.361296296296295
	},
	{
		id: 1156,
		time: 1155,
		velocity: 11.8725,
		power: 6236.8491596079,
		road: 12370.4256944444,
		acceleration: 0.355833333333335
	},
	{
		id: 1157,
		time: 1156,
		velocity: 12.1886111111111,
		power: 6012.74702607147,
		road: 12382.4368518519,
		acceleration: 0.316018518518518
	},
	{
		id: 1158,
		time: 1157,
		velocity: 12.4463888888889,
		power: 5038.16832457902,
		road: 12394.7148611111,
		acceleration: 0.217685185185184
	},
	{
		id: 1159,
		time: 1158,
		velocity: 12.5255555555556,
		power: 4373.56246659939,
		road: 12407.1781018519,
		acceleration: 0.152777777777779
	},
	{
		id: 1160,
		time: 1159,
		velocity: 12.6469444444444,
		power: 4092.89873182092,
		road: 12419.7794444444,
		acceleration: 0.123425925925927
	},
	{
		id: 1161,
		time: 1160,
		velocity: 12.8166666666667,
		power: 4824.05193802669,
		road: 12432.5311574074,
		acceleration: 0.177314814814814
	},
	{
		id: 1162,
		time: 1161,
		velocity: 13.0575,
		power: 5933.81752913452,
		road: 12445.5000925926,
		acceleration: 0.257129629629631
	},
	{
		id: 1163,
		time: 1162,
		velocity: 13.4183333333333,
		power: 6561.44574499287,
		road: 12458.7439351852,
		acceleration: 0.292685185185182
	},
	{
		id: 1164,
		time: 1163,
		velocity: 13.6947222222222,
		power: 5623.95262812946,
		road: 12472.2373611111,
		acceleration: 0.206481481481482
	},
	{
		id: 1165,
		time: 1164,
		velocity: 13.6769444444444,
		power: 2837.57404298738,
		road: 12485.8275925926,
		acceleration: -0.0128703703703721
	},
	{
		id: 1166,
		time: 1165,
		velocity: 13.3797222222222,
		power: 1449.09922468298,
		road: 12499.3522222222,
		acceleration: -0.118333333333329
	},
	{
		id: 1167,
		time: 1166,
		velocity: 13.3397222222222,
		power: 1748.13676602166,
		road: 12512.7714351852,
		acceleration: -0.0925000000000011
	},
	{
		id: 1168,
		time: 1167,
		velocity: 13.3994444444444,
		power: 2429.43070611791,
		road: 12526.1256944444,
		acceleration: -0.0374074074074073
	},
	{
		id: 1169,
		time: 1168,
		velocity: 13.2675,
		power: 456.145823778434,
		road: 12539.3661574074,
		acceleration: -0.190185185185186
	},
	{
		id: 1170,
		time: 1169,
		velocity: 12.7691666666667,
		power: -865.834733611669,
		road: 12552.3652777778,
		acceleration: -0.292500000000002
	},
	{
		id: 1171,
		time: 1170,
		velocity: 12.5219444444444,
		power: -1144.32594329364,
		road: 12565.0617592593,
		acceleration: -0.312777777777777
	},
	{
		id: 1172,
		time: 1171,
		velocity: 12.3291666666667,
		power: 2182.12992020377,
		road: 12577.5852314815,
		acceleration: -0.0332407407407409
	},
	{
		id: 1173,
		time: 1172,
		velocity: 12.6694444444444,
		power: 5282.78035278734,
		road: 12590.2028703704,
		acceleration: 0.221574074074075
	},
	{
		id: 1174,
		time: 1173,
		velocity: 13.1866666666667,
		power: 10090.218529211,
		road: 12603.2256481481,
		acceleration: 0.588703703703702
	},
	{
		id: 1175,
		time: 1174,
		velocity: 14.0952777777778,
		power: 9711.51945710581,
		road: 12616.801712963,
		acceleration: 0.517870370370373
	},
	{
		id: 1176,
		time: 1175,
		velocity: 14.2230555555556,
		power: 7361.68078556172,
		road: 12630.793287037,
		acceleration: 0.313148148148146
	},
	{
		id: 1177,
		time: 1176,
		velocity: 14.1261111111111,
		power: 2822.46051482607,
		road: 12644.9259722222,
		acceleration: -0.0309259259259242
	},
	{
		id: 1178,
		time: 1177,
		velocity: 14.0025,
		power: 1686.01833708883,
		road: 12658.9865277778,
		acceleration: -0.113333333333333
	},
	{
		id: 1179,
		time: 1178,
		velocity: 13.8830555555556,
		power: 2686.28654534985,
		road: 12672.9721296296,
		acceleration: -0.0365740740740765
	},
	{
		id: 1180,
		time: 1179,
		velocity: 14.0163888888889,
		power: 3602.8271290525,
		road: 12686.9555092593,
		acceleration: 0.0321296296296314
	},
	{
		id: 1181,
		time: 1180,
		velocity: 14.0988888888889,
		power: 2537.72102762094,
		road: 12700.93125,
		acceleration: -0.0474074074074071
	},
	{
		id: 1182,
		time: 1181,
		velocity: 13.7408333333333,
		power: 3566.74411788462,
		road: 12714.898287037,
		acceleration: 0.0299999999999994
	},
	{
		id: 1183,
		time: 1182,
		velocity: 14.1063888888889,
		power: 1404.65962875291,
		road: 12728.8148611111,
		acceleration: -0.130925925925926
	},
	{
		id: 1184,
		time: 1183,
		velocity: 13.7061111111111,
		power: 2831.2565592915,
		road: 12742.6553703704,
		acceleration: -0.0212037037037049
	},
	{
		id: 1185,
		time: 1184,
		velocity: 13.6772222222222,
		power: 156.919778065975,
		road: 12756.3744907407,
		acceleration: -0.221574074074072
	},
	{
		id: 1186,
		time: 1185,
		velocity: 13.4416666666667,
		power: 1541.44498959983,
		road: 12769.9268981481,
		acceleration: -0.111851851851853
	},
	{
		id: 1187,
		time: 1186,
		velocity: 13.3705555555556,
		power: 2123.44505142204,
		road: 12783.3911574074,
		acceleration: -0.0644444444444456
	},
	{
		id: 1188,
		time: 1187,
		velocity: 13.4838888888889,
		power: 2533.50817566886,
		road: 12796.8076388889,
		acceleration: -0.0311111111111106
	},
	{
		id: 1189,
		time: 1188,
		velocity: 13.3483333333333,
		power: 2968.88332218089,
		road: 12810.2102314815,
		acceleration: 0.00333333333333208
	},
	{
		id: 1190,
		time: 1189,
		velocity: 13.3805555555556,
		power: 1625.52772784868,
		road: 12823.5642592593,
		acceleration: -0.100462962962959
	},
	{
		id: 1191,
		time: 1190,
		velocity: 13.1825,
		power: 2727.96266894523,
		road: 12836.8619444444,
		acceleration: -0.0122222222222259
	},
	{
		id: 1192,
		time: 1191,
		velocity: 13.3116666666667,
		power: 1761.0247922072,
		road: 12850.1099537037,
		acceleration: -0.0871296296296293
	},
	{
		id: 1193,
		time: 1192,
		velocity: 13.1191666666667,
		power: 1483.52735129686,
		road: 12863.2610185185,
		acceleration: -0.106759259259258
	},
	{
		id: 1194,
		time: 1193,
		velocity: 12.8622222222222,
		power: -1693.41009468789,
		road: 12876.1792592593,
		acceleration: -0.35888888888889
	},
	{
		id: 1195,
		time: 1194,
		velocity: 12.235,
		power: -1919.88345165696,
		road: 12888.7297685185,
		acceleration: -0.376574074074073
	},
	{
		id: 1196,
		time: 1195,
		velocity: 11.9894444444444,
		power: -1329.14589490501,
		road: 12900.929212963,
		acceleration: -0.325555555555555
	},
	{
		id: 1197,
		time: 1196,
		velocity: 11.8855555555556,
		power: 873.061184153316,
		road: 12912.9,
		acceleration: -0.13175925925926
	},
	{
		id: 1198,
		time: 1197,
		velocity: 11.8397222222222,
		power: 1973.64634335957,
		road: 12924.7883796296,
		acceleration: -0.0330555555555563
	},
	{
		id: 1199,
		time: 1198,
		velocity: 11.8902777777778,
		power: 1414.26433419985,
		road: 12936.6197222222,
		acceleration: -0.081018518518519
	},
	{
		id: 1200,
		time: 1199,
		velocity: 11.6425,
		power: 1145.90319856757,
		road: 12948.3591666667,
		acceleration: -0.102777777777776
	},
	{
		id: 1201,
		time: 1200,
		velocity: 11.5313888888889,
		power: 986.828047147532,
		road: 12959.9898611111,
		acceleration: -0.114722222222223
	},
	{
		id: 1202,
		time: 1201,
		velocity: 11.5461111111111,
		power: 1686.89411771561,
		road: 12971.5384722222,
		acceleration: -0.0494444444444433
	},
	{
		id: 1203,
		time: 1202,
		velocity: 11.4941666666667,
		power: 2479.56353675692,
		road: 12983.0738425926,
		acceleration: 0.0229629629629642
	},
	{
		id: 1204,
		time: 1203,
		velocity: 11.6002777777778,
		power: 2944.00734315816,
		road: 12994.6525,
		acceleration: 0.0636111111111095
	},
	{
		id: 1205,
		time: 1204,
		velocity: 11.7369444444444,
		power: 3608.54369533418,
		road: 13006.322962963,
		acceleration: 0.119999999999999
	},
	{
		id: 1206,
		time: 1205,
		velocity: 11.8541666666667,
		power: 3319.88851772286,
		road: 13018.0984259259,
		acceleration: 0.0900000000000016
	},
	{
		id: 1207,
		time: 1206,
		velocity: 11.8702777777778,
		power: 2132.99591339689,
		road: 13029.9105092593,
		acceleration: -0.0167592592592598
	},
	{
		id: 1208,
		time: 1207,
		velocity: 11.6866666666667,
		power: 1092.43237355331,
		road: 13041.6603240741,
		acceleration: -0.107777777777779
	},
	{
		id: 1209,
		time: 1208,
		velocity: 11.5308333333333,
		power: 1334.90005081205,
		road: 13053.3143055556,
		acceleration: -0.0838888888888896
	},
	{
		id: 1210,
		time: 1209,
		velocity: 11.6186111111111,
		power: 1404.08585556211,
		road: 13064.8884722222,
		acceleration: -0.0757407407407413
	},
	{
		id: 1211,
		time: 1210,
		velocity: 11.4594444444444,
		power: 1958.64227165244,
		road: 13076.4127314815,
		acceleration: -0.0240740740740737
	},
	{
		id: 1212,
		time: 1211,
		velocity: 11.4586111111111,
		power: 1108.33510877682,
		road: 13087.8749074074,
		acceleration: -0.10009259259259
	},
	{
		id: 1213,
		time: 1212,
		velocity: 11.3183333333333,
		power: 1325.18258654509,
		road: 13099.247962963,
		acceleration: -0.0781481481481485
	},
	{
		id: 1214,
		time: 1213,
		velocity: 11.225,
		power: 357.552364044462,
		road: 13110.499212963,
		acceleration: -0.165462962962964
	},
	{
		id: 1215,
		time: 1214,
		velocity: 10.9622222222222,
		power: 764.897062154089,
		road: 13121.6053703704,
		acceleration: -0.124722222222223
	},
	{
		id: 1216,
		time: 1215,
		velocity: 10.9441666666667,
		power: 603.074673668743,
		road: 13132.5803240741,
		acceleration: -0.137685185185184
	},
	{
		id: 1217,
		time: 1216,
		velocity: 10.8119444444444,
		power: 895.834125933239,
		road: 13143.4328240741,
		acceleration: -0.107222222222221
	},
	{
		id: 1218,
		time: 1217,
		velocity: 10.6405555555556,
		power: 785.451989287998,
		road: 13154.1738425926,
		acceleration: -0.115740740740742
	},
	{
		id: 1219,
		time: 1218,
		velocity: 10.5969444444444,
		power: 992.134944890513,
		road: 13164.8103240741,
		acceleration: -0.0933333333333337
	},
	{
		id: 1220,
		time: 1219,
		velocity: 10.5319444444444,
		power: 738.30496329547,
		road: 13175.3419444444,
		acceleration: -0.116388888888888
	},
	{
		id: 1221,
		time: 1220,
		velocity: 10.2913888888889,
		power: -358.627100145672,
		road: 13185.7032407407,
		acceleration: -0.224259259259259
	},
	{
		id: 1222,
		time: 1221,
		velocity: 9.92416666666667,
		power: -3546.00560992814,
		road: 13195.6740277778,
		acceleration: -0.556759259259259
	},
	{
		id: 1223,
		time: 1222,
		velocity: 8.86166666666667,
		power: -3931.33195245324,
		road: 13205.057962963,
		acceleration: -0.616944444444446
	},
	{
		id: 1224,
		time: 1223,
		velocity: 8.44055555555556,
		power: -4291.13992601886,
		road: 13213.7901388889,
		acceleration: -0.686574074074073
	},
	{
		id: 1225,
		time: 1224,
		velocity: 7.86444444444444,
		power: -2413.22401336884,
		road: 13221.9410648148,
		acceleration: -0.475925925925925
	},
	{
		id: 1226,
		time: 1225,
		velocity: 7.43388888888889,
		power: -2641.54025029497,
		road: 13229.5924074074,
		acceleration: -0.52324074074074
	},
	{
		id: 1227,
		time: 1226,
		velocity: 6.87083333333333,
		power: -1848.58441505308,
		road: 13236.7685185185,
		acceleration: -0.427222222222222
	},
	{
		id: 1228,
		time: 1227,
		velocity: 6.58277777777778,
		power: -2032.24078643117,
		road: 13243.4957407407,
		acceleration: -0.470555555555555
	},
	{
		id: 1229,
		time: 1228,
		velocity: 6.02222222222222,
		power: -782.223235345424,
		road: 13249.8478703704,
		acceleration: -0.27962962962963
	},
	{
		id: 1230,
		time: 1229,
		velocity: 6.03194444444444,
		power: -59.6558885712072,
		road: 13255.980787037,
		acceleration: -0.158796296296297
	},
	{
		id: 1231,
		time: 1230,
		velocity: 6.10638888888889,
		power: 1239.77438719884,
		road: 13262.0673148148,
		acceleration: 0.0660185185185185
	},
	{
		id: 1232,
		time: 1231,
		velocity: 6.22027777777778,
		power: 991.456704119972,
		road: 13268.1976388889,
		acceleration: 0.0215740740740742
	},
	{
		id: 1233,
		time: 1232,
		velocity: 6.09666666666667,
		power: -673.053459239471,
		road: 13274.2059722222,
		acceleration: -0.265555555555556
	},
	{
		id: 1234,
		time: 1233,
		velocity: 5.30972222222222,
		power: -3511.96431666967,
		road: 13279.6712962963,
		acceleration: -0.820462962962962
	},
	{
		id: 1235,
		time: 1234,
		velocity: 3.75888888888889,
		power: -5625.11636350996,
		road: 13283.9681018519,
		acceleration: -1.51657407407407
	},
	{
		id: 1236,
		time: 1235,
		velocity: 1.54694444444444,
		power: -4124.27233266421,
		road: 13286.6216666667,
		acceleration: -1.76990740740741
	},
	{
		id: 1237,
		time: 1236,
		velocity: 0,
		power: -1217.57334911456,
		road: 13287.7637962963,
		acceleration: -1.25296296296296
	},
	{
		id: 1238,
		time: 1237,
		velocity: 0,
		power: -94.7995599577648,
		road: 13288.0216203704,
		acceleration: -0.515648148148148
	},
	{
		id: 1239,
		time: 1238,
		velocity: 0,
		power: 0,
		road: 13288.0216203704,
		acceleration: 0
	},
	{
		id: 1240,
		time: 1239,
		velocity: 0,
		power: 0,
		road: 13288.0216203704,
		acceleration: 0
	},
	{
		id: 1241,
		time: 1240,
		velocity: 0,
		power: 0,
		road: 13288.0216203704,
		acceleration: 0
	},
	{
		id: 1242,
		time: 1241,
		velocity: 0,
		power: 0,
		road: 13288.0216203704,
		acceleration: 0
	},
	{
		id: 1243,
		time: 1242,
		velocity: 0,
		power: 40.8056074782858,
		road: 13288.1399074074,
		acceleration: 0.236574074074074
	},
	{
		id: 1244,
		time: 1243,
		velocity: 0.709722222222222,
		power: 243.455012468342,
		road: 13288.5949537037,
		acceleration: 0.436944444444444
	},
	{
		id: 1245,
		time: 1244,
		velocity: 1.31083333333333,
		power: 1281.8529525984,
		road: 13289.7764814815,
		acceleration: 1.01601851851852
	},
	{
		id: 1246,
		time: 1245,
		velocity: 3.04805555555556,
		power: 3245.75852746063,
		road: 13292.1281018519,
		acceleration: 1.32416666666667
	},
	{
		id: 1247,
		time: 1246,
		velocity: 4.68222222222222,
		power: 4294.97100142528,
		road: 13295.7065740741,
		acceleration: 1.12953703703704
	},
	{
		id: 1248,
		time: 1247,
		velocity: 4.69944444444444,
		power: 2794.07761206524,
		road: 13300.1143518519,
		acceleration: 0.529074074074074
	},
	{
		id: 1249,
		time: 1248,
		velocity: 4.63527777777778,
		power: -680.22811191541,
		road: 13304.6380555556,
		acceleration: -0.297222222222222
	},
	{
		id: 1250,
		time: 1249,
		velocity: 3.79055555555556,
		power: -1335.19759629126,
		road: 13308.7746759259,
		acceleration: -0.476944444444446
	},
	{
		id: 1251,
		time: 1250,
		velocity: 3.26861111111111,
		power: -2584.75681936559,
		road: 13312.2093518519,
		acceleration: -0.926944444444444
	},
	{
		id: 1252,
		time: 1251,
		velocity: 1.85444444444444,
		power: -1404.7794550445,
		road: 13314.8326851852,
		acceleration: -0.695740740740741
	},
	{
		id: 1253,
		time: 1252,
		velocity: 1.70333333333333,
		power: -806.378460240487,
		road: 13316.8305092593,
		acceleration: -0.555277777777778
	},
	{
		id: 1254,
		time: 1253,
		velocity: 1.60277777777778,
		power: -11.32524978659,
		road: 13318.4825925926,
		acceleration: -0.136203703703704
	},
	{
		id: 1255,
		time: 1254,
		velocity: 1.44583333333333,
		power: 181.968515432702,
		road: 13320.0628703704,
		acceleration: -0.00740740740740731
	},
	{
		id: 1256,
		time: 1255,
		velocity: 1.68111111111111,
		power: 414.327517398307,
		road: 13321.7077777778,
		acceleration: 0.136666666666667
	},
	{
		id: 1257,
		time: 1256,
		velocity: 2.01277777777778,
		power: 607.143656787309,
		road: 13323.5318518519,
		acceleration: 0.221666666666667
	},
	{
		id: 1258,
		time: 1257,
		velocity: 2.11083333333333,
		power: 502.306130011235,
		road: 13325.5341666667,
		acceleration: 0.134814814814814
	},
	{
		id: 1259,
		time: 1258,
		velocity: 2.08555555555556,
		power: 230.54240155573,
		road: 13327.5978703704,
		acceleration: -0.0120370370370368
	},
	{
		id: 1260,
		time: 1259,
		velocity: 1.97666666666667,
		power: -120.584090052159,
		road: 13329.5583333333,
		acceleration: -0.194444444444444
	},
	{
		id: 1261,
		time: 1260,
		velocity: 1.5275,
		power: -813.960542628926,
		road: 13331.0739814815,
		acceleration: -0.695185185185185
	},
	{
		id: 1262,
		time: 1261,
		velocity: 0,
		power: -422.032628333989,
		road: 13331.9125925926,
		acceleration: -0.658888888888889
	},
	{
		id: 1263,
		time: 1262,
		velocity: 0,
		power: -92.0447381578947,
		road: 13332.1671759259,
		acceleration: -0.509166666666667
	},
	{
		id: 1264,
		time: 1263,
		velocity: 0,
		power: 0,
		road: 13332.1671759259,
		acceleration: 0
	},
	{
		id: 1265,
		time: 1264,
		velocity: 0,
		power: 0,
		road: 13332.1671759259,
		acceleration: 0
	},
	{
		id: 1266,
		time: 1265,
		velocity: 0,
		power: 0,
		road: 13332.1671759259,
		acceleration: 0
	},
	{
		id: 1267,
		time: 1266,
		velocity: 0,
		power: 0,
		road: 13332.1671759259,
		acceleration: 0
	},
	{
		id: 1268,
		time: 1267,
		velocity: 0,
		power: 0,
		road: 13332.1671759259,
		acceleration: 0
	},
	{
		id: 1269,
		time: 1268,
		velocity: 0,
		power: 370.287721901662,
		road: 13332.5784259259,
		acceleration: 0.8225
	},
	{
		id: 1270,
		time: 1269,
		velocity: 2.4675,
		power: 1332.21602802955,
		road: 13333.8775,
		acceleration: 0.953148148148148
	},
	{
		id: 1271,
		time: 1270,
		velocity: 2.85944444444444,
		power: 2042.90207580103,
		road: 13336.0775,
		acceleration: 0.848703703703704
	},
	{
		id: 1272,
		time: 1271,
		velocity: 2.54611111111111,
		power: 295.712155253168,
		road: 13338.6957407407,
		acceleration: -0.0122222222222224
	},
	{
		id: 1273,
		time: 1272,
		velocity: 2.43083333333333,
		power: -238.379744839096,
		road: 13341.1920833333,
		acceleration: -0.231574074074074
	},
	{
		id: 1274,
		time: 1273,
		velocity: 2.16472222222222,
		power: 105.462253696093,
		road: 13343.5311574074,
		acceleration: -0.0829629629629629
	},
	{
		id: 1275,
		time: 1274,
		velocity: 2.29722222222222,
		power: -230.190494915815,
		road: 13345.707962963,
		acceleration: -0.241574074074074
	},
	{
		id: 1276,
		time: 1275,
		velocity: 1.70611111111111,
		power: 875.291572356173,
		road: 13347.9085648148,
		acceleration: 0.289166666666667
	},
	{
		id: 1277,
		time: 1276,
		velocity: 3.03222222222222,
		power: 1414.61965040866,
		road: 13350.478287037,
		acceleration: 0.449074074074074
	},
	{
		id: 1278,
		time: 1277,
		velocity: 3.64444444444444,
		power: 2515.68565272734,
		road: 13353.6269444444,
		acceleration: 0.708796296296296
	},
	{
		id: 1279,
		time: 1278,
		velocity: 3.8325,
		power: 1486.12911445196,
		road: 13357.2769907407,
		acceleration: 0.293981481481481
	},
	{
		id: 1280,
		time: 1279,
		velocity: 3.91416666666667,
		power: 803.784488866998,
		road: 13361.1164351852,
		acceleration: 0.0848148148148153
	},
	{
		id: 1281,
		time: 1280,
		velocity: 3.89888888888889,
		power: 503.536507801093,
		road: 13364.9986574074,
		acceleration: 0.000740740740740709
	},
	{
		id: 1282,
		time: 1281,
		velocity: 3.83472222222222,
		power: 315.129928048635,
		road: 13368.8563888889,
		acceleration: -0.049722222222222
	},
	{
		id: 1283,
		time: 1282,
		velocity: 3.765,
		power: 555.293176960682,
		road: 13372.6975462963,
		acceleration: 0.0165740740740739
	},
	{
		id: 1284,
		time: 1283,
		velocity: 3.94861111111111,
		power: 873.209288977782,
		road: 13376.5969444444,
		acceleration: 0.0999074074074073
	},
	{
		id: 1285,
		time: 1284,
		velocity: 4.13444444444444,
		power: 1818.5193463041,
		road: 13380.7106018519,
		acceleration: 0.328611111111111
	},
	{
		id: 1286,
		time: 1285,
		velocity: 4.75083333333333,
		power: 3539.57619442052,
		road: 13385.3228240741,
		acceleration: 0.668518518518519
	},
	{
		id: 1287,
		time: 1286,
		velocity: 5.95416666666667,
		power: 5089.50263351983,
		road: 13390.6958333333,
		acceleration: 0.853055555555556
	},
	{
		id: 1288,
		time: 1287,
		velocity: 6.69361111111111,
		power: 6026.87691468434,
		road: 13396.9291666667,
		acceleration: 0.867592592592591
	},
	{
		id: 1289,
		time: 1288,
		velocity: 7.35361111111111,
		power: 10624.4917874339,
		road: 13404.2771759259,
		acceleration: 1.36175925925926
	},
	{
		id: 1290,
		time: 1289,
		velocity: 10.0394444444444,
		power: 16617.6936123529,
		road: 13413.1978703704,
		acceleration: 1.78361111111111
	},
	{
		id: 1291,
		time: 1290,
		velocity: 12.0444444444444,
		power: 19842.6315649792,
		road: 13423.8879166667,
		acceleration: 1.75509259259259
	},
	{
		id: 1292,
		time: 1291,
		velocity: 12.6188888888889,
		power: 10680.2687448226,
		road: 13435.820787037,
		acceleration: 0.730555555555554
	},
	{
		id: 1293,
		time: 1292,
		velocity: 12.2311111111111,
		power: 153.467648640904,
		road: 13448.0198611111,
		acceleration: -0.19814814814815
	},
	{
		id: 1294,
		time: 1293,
		velocity: 11.45,
		power: -3755.1561603951,
		road: 13459.8502777778,
		acceleration: -0.539166666666667
	},
	{
		id: 1295,
		time: 1294,
		velocity: 11.0013888888889,
		power: -2533.93259439316,
		road: 13471.1939814815,
		acceleration: -0.43425925925926
	},
	{
		id: 1296,
		time: 1295,
		velocity: 10.9283333333333,
		power: 454.160682939261,
		road: 13482.2439814815,
		acceleration: -0.153148148148148
	},
	{
		id: 1297,
		time: 1296,
		velocity: 10.9905555555556,
		power: 2565.44480416391,
		road: 13493.2419444445,
		acceleration: 0.0490740740740758
	},
	{
		id: 1298,
		time: 1297,
		velocity: 11.1486111111111,
		power: 3662.78872651562,
		road: 13504.3391203704,
		acceleration: 0.149351851851849
	},
	{
		id: 1299,
		time: 1298,
		velocity: 11.3763888888889,
		power: 2956.76862011482,
		road: 13515.5501388889,
		acceleration: 0.0783333333333331
	},
	{
		id: 1300,
		time: 1299,
		velocity: 11.2255555555556,
		power: 1673.27172669042,
		road: 13526.779212963,
		acceleration: -0.0422222222222182
	},
	{
		id: 1301,
		time: 1300,
		velocity: 11.0219444444444,
		power: 103.317734022487,
		road: 13537.8935185185,
		acceleration: -0.187314814814815
	},
	{
		id: 1302,
		time: 1301,
		velocity: 10.8144444444444,
		power: 1238.36407416243,
		road: 13548.8756018519,
		acceleration: -0.0771296296296313
	},
	{
		id: 1303,
		time: 1302,
		velocity: 10.9941666666667,
		power: 2695.4901870604,
		road: 13559.8502777778,
		acceleration: 0.0623148148148154
	},
	{
		id: 1304,
		time: 1303,
		velocity: 11.2088888888889,
		power: 3483.2288479227,
		road: 13570.9228240741,
		acceleration: 0.133425925925923
	},
	{
		id: 1305,
		time: 1304,
		velocity: 11.2147222222222,
		power: 2095.17581030971,
		road: 13582.062037037,
		acceleration: -9.25925925905347E-05
	},
	{
		id: 1306,
		time: 1305,
		velocity: 10.9938888888889,
		power: 455.354412791335,
		road: 13593.1245833333,
		acceleration: -0.15324074074074
	},
	{
		id: 1307,
		time: 1306,
		velocity: 10.7491666666667,
		power: 237.218141665986,
		road: 13604.0247222222,
		acceleration: -0.171574074074073
	},
	{
		id: 1308,
		time: 1307,
		velocity: 10.7,
		power: 1135.07917613693,
		road: 13614.7979166667,
		acceleration: -0.082314814814815
	},
	{
		id: 1309,
		time: 1308,
		velocity: 10.7469444444444,
		power: 2489.02857280319,
		road: 13625.555,
		acceleration: 0.0500925925925912
	},
	{
		id: 1310,
		time: 1309,
		velocity: 10.8994444444444,
		power: 3165.91167003602,
		road: 13636.3935185185,
		acceleration: 0.112777777777778
	},
	{
		id: 1311,
		time: 1310,
		velocity: 11.0383333333333,
		power: 4354.3369892122,
		road: 13647.3981018519,
		acceleration: 0.219351851851851
	},
	{
		id: 1312,
		time: 1311,
		velocity: 11.405,
		power: 5904.48319562056,
		road: 13658.6869907407,
		acceleration: 0.349259259259259
	},
	{
		id: 1313,
		time: 1312,
		velocity: 11.9472222222222,
		power: 6387.64585589489,
		road: 13670.3360185185,
		acceleration: 0.37101851851852
	},
	{
		id: 1314,
		time: 1313,
		velocity: 12.1513888888889,
		power: 6947.37288062439,
		road: 13682.3686574074,
		acceleration: 0.396203703703703
	},
	{
		id: 1315,
		time: 1314,
		velocity: 12.5936111111111,
		power: 8042.28944726985,
		road: 13694.8300925926,
		acceleration: 0.461388888888887
	},
	{
		id: 1316,
		time: 1315,
		velocity: 13.3313888888889,
		power: 9706.28999579776,
		road: 13707.8030092593,
		acceleration: 0.561574074074077
	},
	{
		id: 1317,
		time: 1316,
		velocity: 13.8361111111111,
		power: 10464.1591018925,
		road: 13721.3459259259,
		acceleration: 0.578425925925924
	},
	{
		id: 1318,
		time: 1317,
		velocity: 14.3288888888889,
		power: 8416.36632287657,
		road: 13735.3730092593,
		acceleration: 0.389907407407406
	},
	{
		id: 1319,
		time: 1318,
		velocity: 14.5011111111111,
		power: 6281.72856926506,
		road: 13749.7029166667,
		acceleration: 0.215740740740742
	},
	{
		id: 1320,
		time: 1319,
		velocity: 14.4833333333333,
		power: 2622.67647560603,
		road: 13764.1137037037,
		acceleration: -0.0539814814814807
	},
	{
		id: 1321,
		time: 1320,
		velocity: 14.1669444444444,
		power: 1188.56499437949,
		road: 13778.4195833333,
		acceleration: -0.155833333333334
	},
	{
		id: 1322,
		time: 1321,
		velocity: 14.0336111111111,
		power: 1342.97992139919,
		road: 13792.5769907407,
		acceleration: -0.14111111111111
	},
	{
		id: 1323,
		time: 1322,
		velocity: 14.06,
		power: 1630.69587148305,
		road: 13806.6055092593,
		acceleration: -0.116666666666671
	},
	{
		id: 1324,
		time: 1323,
		velocity: 13.8169444444444,
		power: 933.938818687102,
		road: 13820.4928240741,
		acceleration: -0.165740740740739
	},
	{
		id: 1325,
		time: 1324,
		velocity: 13.5363888888889,
		power: 492.30164566809,
		road: 13834.1993981482,
		acceleration: -0.195740740740739
	},
	{
		id: 1326,
		time: 1325,
		velocity: 13.4727777777778,
		power: 1192.70929031291,
		road: 13847.7388425926,
		acceleration: -0.138518518518518
	},
	{
		id: 1327,
		time: 1326,
		velocity: 13.4013888888889,
		power: 1312.835234882,
		road: 13861.1459259259,
		acceleration: -0.126203703703702
	},
	{
		id: 1328,
		time: 1327,
		velocity: 13.1577777777778,
		power: -1650.09694794209,
		road: 13874.3116666667,
		acceleration: -0.356481481481485
	},
	{
		id: 1329,
		time: 1328,
		velocity: 12.4033333333333,
		power: -4328.22539131939,
		road: 13887.0113888889,
		acceleration: -0.575555555555555
	},
	{
		id: 1330,
		time: 1329,
		velocity: 11.6747222222222,
		power: -5318.44225163013,
		road: 13899.0876388889,
		acceleration: -0.67138888888889
	},
	{
		id: 1331,
		time: 1330,
		velocity: 11.1436111111111,
		power: -4207.09255532881,
		road: 13910.5348148148,
		acceleration: -0.58675925925926
	},
	{
		id: 1332,
		time: 1331,
		velocity: 10.6430555555556,
		power: -4929.48962938546,
		road: 13921.3528703704,
		acceleration: -0.67148148148148
	},
	{
		id: 1333,
		time: 1332,
		velocity: 9.66027777777778,
		power: -5530.38287885725,
		road: 13931.4554166667,
		acceleration: -0.759537037037036
	},
	{
		id: 1334,
		time: 1333,
		velocity: 8.865,
		power: -4972.07517881914,
		road: 13940.810787037,
		acceleration: -0.734814814814815
	},
	{
		id: 1335,
		time: 1334,
		velocity: 8.43861111111111,
		power: -2030.97070879186,
		road: 13949.5918518519,
		acceleration: -0.413796296296294
	},
	{
		id: 1336,
		time: 1335,
		velocity: 8.41888888888889,
		power: -316.096724025724,
		road: 13958.0625,
		acceleration: -0.20703703703704
	},
	{
		id: 1337,
		time: 1336,
		velocity: 8.24388888888889,
		power: -2206.80526430396,
		road: 13966.2048611111,
		acceleration: -0.449537037037035
	},
	{
		id: 1338,
		time: 1337,
		velocity: 7.09,
		power: -5844.17358119372,
		road: 13973.6294907407,
		acceleration: -0.985925925925926
	},
	{
		id: 1339,
		time: 1338,
		velocity: 5.46111111111111,
		power: -8215.83596392956,
		road: 13979.7846296296,
		acceleration: -1.55305555555556
	},
	{
		id: 1340,
		time: 1339,
		velocity: 3.58472222222222,
		power: -7200.84936130946,
		road: 13984.2435648148,
		acceleration: -1.83935185185185
	},
	{
		id: 1341,
		time: 1340,
		velocity: 1.57194444444444,
		power: -4212.1435668584,
		road: 13986.8726388889,
		acceleration: -1.82037037037037
	},
	{
		id: 1342,
		time: 1341,
		velocity: 0,
		power: -1133.8277173368,
		road: 13987.9940740741,
		acceleration: -1.19490740740741
	},
	{
		id: 1343,
		time: 1342,
		velocity: 0,
		power: -98.3999532326186,
		road: 13988.2560648148,
		acceleration: -0.523981481481481
	},
	{
		id: 1344,
		time: 1343,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1345,
		time: 1344,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1346,
		time: 1345,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1347,
		time: 1346,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1348,
		time: 1347,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1349,
		time: 1348,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1350,
		time: 1349,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1351,
		time: 1350,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1352,
		time: 1351,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1353,
		time: 1352,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1354,
		time: 1353,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1355,
		time: 1354,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1356,
		time: 1355,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1357,
		time: 1356,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1358,
		time: 1357,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1359,
		time: 1358,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1360,
		time: 1359,
		velocity: 0,
		power: 0,
		road: 13988.2560648148,
		acceleration: 0
	},
	{
		id: 1361,
		time: 1360,
		velocity: 0,
		power: 79.1447848070734,
		road: 13988.4310185185,
		acceleration: 0.349907407407407
	},
	{
		id: 1362,
		time: 1361,
		velocity: 1.04972222222222,
		power: 983.634278165852,
		road: 13989.3083333333,
		acceleration: 1.05481481481481
	},
	{
		id: 1363,
		time: 1362,
		velocity: 3.16444444444444,
		power: 2992.70576282463,
		road: 13991.4016203704,
		acceleration: 1.37712962962963
	},
	{
		id: 1364,
		time: 1363,
		velocity: 4.13138888888889,
		power: 4248.12955557661,
		road: 13994.7790277778,
		acceleration: 1.19111111111111
	},
	{
		id: 1365,
		time: 1364,
		velocity: 4.62305555555556,
		power: 3264.64459653835,
		road: 13999.0824537037,
		acceleration: 0.660925925925926
	},
	{
		id: 1366,
		time: 1365,
		velocity: 5.14722222222222,
		power: 2505.73034998118,
		road: 14003.91875,
		acceleration: 0.404814814814814
	},
	{
		id: 1367,
		time: 1366,
		velocity: 5.34583333333333,
		power: 2691.0621132148,
		road: 14009.1563888889,
		acceleration: 0.397870370370371
	},
	{
		id: 1368,
		time: 1367,
		velocity: 5.81666666666667,
		power: 5191.21328236582,
		road: 14014.9879166667,
		acceleration: 0.789907407407407
	},
	{
		id: 1369,
		time: 1368,
		velocity: 7.51694444444444,
		power: 8548.14693691036,
		road: 14021.7975,
		acceleration: 1.1662037037037
	},
	{
		id: 1370,
		time: 1369,
		velocity: 8.84444444444445,
		power: 11494.8898551909,
		road: 14029.8573148148,
		acceleration: 1.33425925925926
	},
	{
		id: 1371,
		time: 1370,
		velocity: 9.81944444444444,
		power: 6978.25591971351,
		road: 14038.9025462963,
		acceleration: 0.636574074074074
	},
	{
		id: 1372,
		time: 1371,
		velocity: 9.42666666666667,
		power: 349.139997298943,
		road: 14048.1977314815,
		acceleration: -0.136666666666667
	},
	{
		id: 1373,
		time: 1372,
		velocity: 8.43444444444444,
		power: -4936.12264878501,
		road: 14057.0458333333,
		acceleration: -0.7575
	},
	{
		id: 1374,
		time: 1373,
		velocity: 7.54694444444444,
		power: -4787.69416906631,
		road: 14065.1216203704,
		acceleration: -0.78712962962963
	},
	{
		id: 1375,
		time: 1374,
		velocity: 7.06527777777778,
		power: -4869.34628241179,
		road: 14072.3723148148,
		acceleration: -0.863055555555555
	},
	{
		id: 1376,
		time: 1375,
		velocity: 5.84527777777778,
		power: -4022.42025653717,
		road: 14078.7863888889,
		acceleration: -0.810185185185186
	},
	{
		id: 1377,
		time: 1376,
		velocity: 5.11638888888889,
		power: -3909.22229993866,
		road: 14084.3534722222,
		acceleration: -0.883796296296295
	},
	{
		id: 1378,
		time: 1377,
		velocity: 4.41388888888889,
		power: -1785.89042287244,
		road: 14089.2149537037,
		acceleration: -0.527407407407408
	},
	{
		id: 1379,
		time: 1378,
		velocity: 4.26305555555555,
		power: -1365.1373127016,
		road: 14093.5789814815,
		acceleration: -0.4675
	},
	{
		id: 1380,
		time: 1379,
		velocity: 3.71388888888889,
		power: -974.453501459351,
		road: 14097.5106944445,
		acceleration: -0.39712962962963
	},
	{
		id: 1381,
		time: 1380,
		velocity: 3.2225,
		power: -1031.1233444534,
		road: 14101.0219907407,
		acceleration: -0.443703703703704
	},
	{
		id: 1382,
		time: 1381,
		velocity: 2.93194444444444,
		power: -707.158605278482,
		road: 14104.1249537037,
		acceleration: -0.372962962962963
	},
	{
		id: 1383,
		time: 1382,
		velocity: 2.595,
		power: -406.485049577897,
		road: 14106.8983333333,
		acceleration: -0.286203703703703
	},
	{
		id: 1384,
		time: 1383,
		velocity: 2.36388888888889,
		power: -216.101658571063,
		road: 14109.4179166667,
		acceleration: -0.221388888888889
	},
	{
		id: 1385,
		time: 1384,
		velocity: 2.26777777777778,
		power: -171.564388137638,
		road: 14111.7223611111,
		acceleration: -0.208888888888889
	},
	{
		id: 1386,
		time: 1385,
		velocity: 1.96833333333333,
		power: -671.81512420771,
		road: 14113.6762962963,
		acceleration: -0.49212962962963
	},
	{
		id: 1387,
		time: 1386,
		velocity: 0.8875,
		power: -791.069498518791,
		road: 14115.0062037037,
		acceleration: -0.755925925925926
	},
	{
		id: 1388,
		time: 1387,
		velocity: 0,
		power: -312.389615392267,
		road: 14115.6300925926,
		acceleration: -0.656111111111111
	},
	{
		id: 1389,
		time: 1388,
		velocity: 0,
		power: -23.5846118421053,
		road: 14115.7780092593,
		acceleration: -0.295833333333333
	},
	{
		id: 1390,
		time: 1389,
		velocity: 0,
		power: 0,
		road: 14115.7780092593,
		acceleration: 0
	},
	{
		id: 1391,
		time: 1390,
		velocity: 0,
		power: 0,
		road: 14115.7780092593,
		acceleration: 0
	},
	{
		id: 1392,
		time: 1391,
		velocity: 0,
		power: 0,
		road: 14115.7780092593,
		acceleration: 0
	},
	{
		id: 1393,
		time: 1392,
		velocity: 0,
		power: 0,
		road: 14115.7780092593,
		acceleration: 0
	},
	{
		id: 1394,
		time: 1393,
		velocity: 0,
		power: 43.4540520210552,
		road: 14115.9008796296,
		acceleration: 0.245740740740741
	},
	{
		id: 1395,
		time: 1394,
		velocity: 0.737222222222222,
		power: 29.6979351012396,
		road: 14116.1466203704,
		acceleration: 0
	},
	{
		id: 1396,
		time: 1395,
		velocity: 0,
		power: 29.6979351012396,
		road: 14116.3923611111,
		acceleration: 0
	},
	{
		id: 1397,
		time: 1396,
		velocity: 0,
		power: -13.7601449967511,
		road: 14116.5152314815,
		acceleration: -0.245740740740741
	},
	{
		id: 1398,
		time: 1397,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1399,
		time: 1398,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1400,
		time: 1399,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1401,
		time: 1400,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1402,
		time: 1401,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1403,
		time: 1402,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1404,
		time: 1403,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1405,
		time: 1404,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1406,
		time: 1405,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1407,
		time: 1406,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1408,
		time: 1407,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1409,
		time: 1408,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1410,
		time: 1409,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1411,
		time: 1410,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1412,
		time: 1411,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1413,
		time: 1412,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1414,
		time: 1413,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1415,
		time: 1414,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1416,
		time: 1415,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1417,
		time: 1416,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1418,
		time: 1417,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1419,
		time: 1418,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1420,
		time: 1419,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1421,
		time: 1420,
		velocity: 0,
		power: 0,
		road: 14116.5152314815,
		acceleration: 0
	},
	{
		id: 1422,
		time: 1421,
		velocity: 0,
		power: 232.939264628612,
		road: 14116.8353703704,
		acceleration: 0.640277777777778
	},
	{
		id: 1423,
		time: 1422,
		velocity: 1.92083333333333,
		power: 1179.12110236127,
		road: 14117.9630092593,
		acceleration: 0.974722222222222
	},
	{
		id: 1424,
		time: 1423,
		velocity: 2.92416666666667,
		power: 3111.99131447099,
		road: 14120.2347685185,
		acceleration: 1.31351851851852
	},
	{
		id: 1425,
		time: 1424,
		velocity: 3.94055555555556,
		power: 3612.39200725699,
		road: 14123.6529166667,
		acceleration: 0.97925925925926
	},
	{
		id: 1426,
		time: 1425,
		velocity: 4.85861111111111,
		power: 3512.78532541233,
		road: 14127.9247685185,
		acceleration: 0.728148148148148
	},
	{
		id: 1427,
		time: 1426,
		velocity: 5.10861111111111,
		power: 1951.89195038224,
		road: 14132.705462963,
		acceleration: 0.289537037037037
	},
	{
		id: 1428,
		time: 1427,
		velocity: 4.80916666666667,
		power: 257.984834478575,
		road: 14137.5883333333,
		acceleration: -0.0851851851851846
	},
	{
		id: 1429,
		time: 1428,
		velocity: 4.60305555555556,
		power: 261.571220660027,
		road: 14142.3871296296,
		acceleration: -0.0829629629629629
	},
	{
		id: 1430,
		time: 1429,
		velocity: 4.85972222222222,
		power: 213.247986671125,
		road: 14147.0983333333,
		acceleration: -0.0922222222222215
	},
	{
		id: 1431,
		time: 1430,
		velocity: 4.5325,
		power: -394.312861538541,
		road: 14151.648287037,
		acceleration: -0.230277777777778
	},
	{
		id: 1432,
		time: 1431,
		velocity: 3.91222222222222,
		power: -644.399160940853,
		road: 14155.9350925926,
		acceleration: -0.296018518518519
	},
	{
		id: 1433,
		time: 1432,
		velocity: 3.97166666666667,
		power: -224.546036972707,
		road: 14159.9763425926,
		acceleration: -0.195092592592593
	},
	{
		id: 1434,
		time: 1433,
		velocity: 3.94722222222222,
		power: 193.909940627959,
		road: 14163.8782407407,
		acceleration: -0.0836111111111109
	},
	{
		id: 1435,
		time: 1434,
		velocity: 3.66138888888889,
		power: -417.944604790592,
		road: 14167.6117592593,
		acceleration: -0.253148148148148
	},
	{
		id: 1436,
		time: 1435,
		velocity: 3.21222222222222,
		power: -1034.44483487598,
		road: 14170.9905092593,
		acceleration: -0.456388888888889
	},
	{
		id: 1437,
		time: 1436,
		velocity: 2.57805555555556,
		power: -1065.07585193508,
		road: 14173.8808333333,
		acceleration: -0.520462962962963
	},
	{
		id: 1438,
		time: 1437,
		velocity: 2.1,
		power: -816.776618035905,
		road: 14176.2650462963,
		acceleration: -0.491759259259259
	},
	{
		id: 1439,
		time: 1438,
		velocity: 1.73694444444444,
		power: -430.36027701748,
		road: 14178.2226851852,
		acceleration: -0.361388888888889
	},
	{
		id: 1440,
		time: 1439,
		velocity: 1.49388888888889,
		power: -165.823885613361,
		road: 14179.8824537037,
		acceleration: -0.234351851851852
	},
	{
		id: 1441,
		time: 1440,
		velocity: 1.39694444444444,
		power: 24.6532521049647,
		road: 14181.3694444445,
		acceleration: -0.111203703703704
	},
	{
		id: 1442,
		time: 1441,
		velocity: 1.40333333333333,
		power: 426.610529842515,
		road: 14182.8849074074,
		acceleration: 0.168148148148148
	},
	{
		id: 1443,
		time: 1442,
		velocity: 1.99833333333333,
		power: 628.085380676092,
		road: 14184.6116666667,
		acceleration: 0.254444444444444
	},
	{
		id: 1444,
		time: 1443,
		velocity: 2.16027777777778,
		power: 640.312744284611,
		road: 14186.572962963,
		acceleration: 0.214629629629629
	},
	{
		id: 1445,
		time: 1444,
		velocity: 2.04722222222222,
		power: 331.564115163113,
		road: 14188.6603703704,
		acceleration: 0.0375925925925928
	},
	{
		id: 1446,
		time: 1445,
		velocity: 2.11111111111111,
		power: 219.62909116391,
		road: 14190.7568518519,
		acceleration: -0.0194444444444439
	},
	{
		id: 1447,
		time: 1446,
		velocity: 2.10194444444444,
		power: 337.615624391176,
		road: 14192.8631481482,
		acceleration: 0.0390740740740734
	},
	{
		id: 1448,
		time: 1447,
		velocity: 2.16444444444444,
		power: 280.584913754157,
		road: 14194.9934259259,
		acceleration: 0.00888888888888895
	},
	{
		id: 1449,
		time: 1448,
		velocity: 2.13777777777778,
		power: 524.805701270827,
		road: 14197.1890740741,
		acceleration: 0.121851851851852
	},
	{
		id: 1450,
		time: 1449,
		velocity: 2.4675,
		power: 740.720231696092,
		road: 14199.5460185185,
		acceleration: 0.200740740740741
	},
	{
		id: 1451,
		time: 1450,
		velocity: 2.76666666666667,
		power: 842.471917322665,
		road: 14202.1108796296,
		acceleration: 0.215092592592592
	},
	{
		id: 1452,
		time: 1451,
		velocity: 2.78305555555556,
		power: 750.520706835737,
		road: 14204.86125,
		acceleration: 0.155925925925926
	},
	{
		id: 1453,
		time: 1452,
		velocity: 2.93527777777778,
		power: 302.713582261487,
		road: 14207.6802314815,
		acceleration: -0.0187037037037041
	},
	{
		id: 1454,
		time: 1453,
		velocity: 2.71055555555556,
		power: 30.6557455540738,
		road: 14210.4299074074,
		acceleration: -0.119907407407407
	},
	{
		id: 1455,
		time: 1454,
		velocity: 2.42333333333333,
		power: -131.656368494022,
		road: 14213.0273148148,
		acceleration: -0.184629629629629
	},
	{
		id: 1456,
		time: 1455,
		velocity: 2.38138888888889,
		power: -90.4192311519003,
		road: 14215.4473611111,
		acceleration: -0.170092592592592
	},
	{
		id: 1457,
		time: 1456,
		velocity: 2.20027777777778,
		power: 68.421972833531,
		road: 14217.732962963,
		acceleration: -0.0987962962962969
	},
	{
		id: 1458,
		time: 1457,
		velocity: 2.12694444444444,
		power: 85.1372578936622,
		road: 14219.9245833333,
		acceleration: -0.0891666666666664
	},
	{
		id: 1459,
		time: 1458,
		velocity: 2.11388888888889,
		power: 145.5569770604,
		road: 14222.0428703704,
		acceleration: -0.0575000000000001
	},
	{
		id: 1460,
		time: 1459,
		velocity: 2.02777777777778,
		power: 120.559386434809,
		road: 14224.0984259259,
		acceleration: -0.0679629629629628
	},
	{
		id: 1461,
		time: 1460,
		velocity: 1.92305555555556,
		power: -911.055732357145,
		road: 14225.7676851852,
		acceleration: -0.70462962962963
	},
	{
		id: 1462,
		time: 1461,
		velocity: 0,
		power: -508.394789330772,
		road: 14226.7466666667,
		acceleration: -0.675925925925926
	},
	{
		id: 1463,
		time: 1462,
		velocity: 0,
		power: -155.915833934373,
		road: 14227.0671759259,
		acceleration: -0.641018518518519
	},
	{
		id: 1464,
		time: 1463,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1465,
		time: 1464,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1466,
		time: 1465,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1467,
		time: 1466,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1468,
		time: 1467,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1469,
		time: 1468,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1470,
		time: 1469,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1471,
		time: 1470,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1472,
		time: 1471,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1473,
		time: 1472,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1474,
		time: 1473,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1475,
		time: 1474,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1476,
		time: 1475,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1477,
		time: 1476,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1478,
		time: 1477,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1479,
		time: 1478,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1480,
		time: 1479,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1481,
		time: 1480,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1482,
		time: 1481,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1483,
		time: 1482,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1484,
		time: 1483,
		velocity: 0,
		power: 0,
		road: 14227.0671759259,
		acceleration: 0
	},
	{
		id: 1485,
		time: 1484,
		velocity: 0,
		power: 310.370452505344,
		road: 14227.4412037037,
		acceleration: 0.748055555555556
	},
	{
		id: 1486,
		time: 1485,
		velocity: 2.24416666666667,
		power: 2030.0674084312,
		road: 14228.8726851852,
		acceleration: 1.36685185185185
	},
	{
		id: 1487,
		time: 1486,
		velocity: 4.10055555555556,
		power: 4811.38562832217,
		road: 14231.7902314815,
		acceleration: 1.60527777777778
	},
	{
		id: 1488,
		time: 1487,
		velocity: 4.81583333333333,
		power: 4196.01826586844,
		road: 14235.9702777778,
		acceleration: 0.919722222222222
	},
	{
		id: 1489,
		time: 1488,
		velocity: 5.00333333333333,
		power: 2964.32376881409,
		road: 14240.8588888889,
		acceleration: 0.497407407407407
	},
	{
		id: 1490,
		time: 1489,
		velocity: 5.59277777777778,
		power: 2515.63402028034,
		road: 14246.1736111111,
		acceleration: 0.354814814814815
	},
	{
		id: 1491,
		time: 1490,
		velocity: 5.88027777777778,
		power: 2407.1452712475,
		road: 14251.8174537037,
		acceleration: 0.303425925925927
	},
	{
		id: 1492,
		time: 1491,
		velocity: 5.91361111111111,
		power: 1436.81430172199,
		road: 14257.6688425926,
		acceleration: 0.111666666666666
	},
	{
		id: 1493,
		time: 1492,
		velocity: 5.92777777777778,
		power: 775.464638169753,
		road: 14263.5716666667,
		acceleration: -0.00879629629629619
	},
	{
		id: 1494,
		time: 1493,
		velocity: 5.85388888888889,
		power: 1173.89840485555,
		road: 14269.5006481482,
		acceleration: 0.0611111111111109
	},
	{
		id: 1495,
		time: 1494,
		velocity: 6.09694444444444,
		power: 1264.89426092676,
		road: 14275.4973148148,
		acceleration: 0.0742592592592581
	},
	{
		id: 1496,
		time: 1495,
		velocity: 6.15055555555556,
		power: 1866.97303121269,
		road: 14281.6173148148,
		acceleration: 0.172407407407407
	},
	{
		id: 1497,
		time: 1496,
		velocity: 6.37111111111111,
		power: 1586.35565565896,
		road: 14287.8819444445,
		acceleration: 0.116851851851853
	},
	{
		id: 1498,
		time: 1497,
		velocity: 6.4475,
		power: 1622.83902089485,
		road: 14294.2635648148,
		acceleration: 0.11712962962963
	},
	{
		id: 1499,
		time: 1498,
		velocity: 6.50194444444444,
		power: 1505.28862367073,
		road: 14300.7502314815,
		acceleration: 0.0929629629629627
	},
	{
		id: 1500,
		time: 1499,
		velocity: 6.65,
		power: -1107.32675180348,
		road: 14307.1168055556,
		acceleration: -0.333148148148148
	},
	{
		id: 1501,
		time: 1500,
		velocity: 5.44805555555556,
		power: -2684.84002802225,
		road: 14313.0034259259,
		acceleration: -0.626759259259259
	},
	{
		id: 1502,
		time: 1501,
		velocity: 4.62166666666667,
		power: -3754.53506449852,
		road: 14318.1193518519,
		acceleration: -0.91462962962963
	},
	{
		id: 1503,
		time: 1502,
		velocity: 3.90611111111111,
		power: -3187.22570422518,
		road: 14322.3087037037,
		acceleration: -0.938518518518518
	},
	{
		id: 1504,
		time: 1503,
		velocity: 2.6325,
		power: -2630.7091523147,
		road: 14325.5321759259,
		acceleration: -0.993240740740742
	},
	{
		id: 1505,
		time: 1504,
		velocity: 1.64194444444444,
		power: -2307.47307824343,
		road: 14327.6080092593,
		acceleration: -1.30203703703704
	},
	{
		id: 1506,
		time: 1505,
		velocity: 0,
		power: -700.436622352929,
		road: 14328.5940740741,
		acceleration: -0.8775
	},
	{
		id: 1507,
		time: 1506,
		velocity: 0,
		power: -108.831054402209,
		road: 14328.8677314815,
		acceleration: -0.547314814814815
	},
	{
		id: 1508,
		time: 1507,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1509,
		time: 1508,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1510,
		time: 1509,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1511,
		time: 1510,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1512,
		time: 1511,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1513,
		time: 1512,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1514,
		time: 1513,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1515,
		time: 1514,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1516,
		time: 1515,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1517,
		time: 1516,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1518,
		time: 1517,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1519,
		time: 1518,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1520,
		time: 1519,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1521,
		time: 1520,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1522,
		time: 1521,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1523,
		time: 1522,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1524,
		time: 1523,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1525,
		time: 1524,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1526,
		time: 1525,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1527,
		time: 1526,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1528,
		time: 1527,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1529,
		time: 1528,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1530,
		time: 1529,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1531,
		time: 1530,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1532,
		time: 1531,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1533,
		time: 1532,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1534,
		time: 1533,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1535,
		time: 1534,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1536,
		time: 1535,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1537,
		time: 1536,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1538,
		time: 1537,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1539,
		time: 1538,
		velocity: 0,
		power: 0,
		road: 14328.8677314815,
		acceleration: 0
	},
	{
		id: 1540,
		time: 1539,
		velocity: 0,
		power: 339.001528693595,
		road: 14329.2599537037,
		acceleration: 0.784444444444444
	},
	{
		id: 1541,
		time: 1540,
		velocity: 2.35333333333333,
		power: 1948.4234950329,
		road: 14330.6956481482,
		acceleration: 1.3025
	},
	{
		id: 1542,
		time: 1541,
		velocity: 3.9075,
		power: 4815.66779862219,
		road: 14333.5923148148,
		acceleration: 1.61944444444444
	},
	{
		id: 1543,
		time: 1542,
		velocity: 4.85833333333333,
		power: 5432.3619735314,
		road: 14337.8944907408,
		acceleration: 1.19157407407407
	},
	{
		id: 1544,
		time: 1543,
		velocity: 5.92805555555556,
		power: 4401.34393849665,
		road: 14343.1606944445,
		acceleration: 0.736481481481481
	},
	{
		id: 1545,
		time: 1544,
		velocity: 6.11694444444444,
		power: 3326.37780844866,
		road: 14349.0203703704,
		acceleration: 0.450462962962963
	},
	{
		id: 1546,
		time: 1545,
		velocity: 6.20972222222222,
		power: 597.838553049835,
		road: 14355.0831018519,
		acceleration: -0.0443518518518529
	},
	{
		id: 1547,
		time: 1546,
		velocity: 5.795,
		power: -691.35823635953,
		road: 14360.9885648148,
		acceleration: -0.270185185185184
	},
	{
		id: 1548,
		time: 1547,
		velocity: 5.30638888888889,
		power: -1955.56201866264,
		road: 14366.5,
		acceleration: -0.517870370370369
	},
	{
		id: 1549,
		time: 1548,
		velocity: 4.65611111111111,
		power: -1275.1994958857,
		road: 14371.5487037037,
		acceleration: -0.407592592592593
	},
	{
		id: 1550,
		time: 1549,
		velocity: 4.57222222222222,
		power: -525.586881225831,
		road: 14376.265,
		acceleration: -0.257222222222222
	},
	{
		id: 1551,
		time: 1550,
		velocity: 4.53472222222222,
		power: 506.733683850357,
		road: 14380.8413888889,
		acceleration: -0.0225925925925932
	},
	{
		id: 1552,
		time: 1551,
		velocity: 4.58833333333333,
		power: 1208.58184920068,
		road: 14385.4740740741,
		acceleration: 0.135185185185185
	},
	{
		id: 1553,
		time: 1552,
		velocity: 4.97777777777778,
		power: 1433.98969738734,
		road: 14390.2618518519,
		acceleration: 0.175
	},
	{
		id: 1554,
		time: 1553,
		velocity: 5.05972222222222,
		power: 1100.71603431381,
		road: 14395.1843055556,
		acceleration: 0.0943518518518518
	},
	{
		id: 1555,
		time: 1554,
		velocity: 4.87138888888889,
		power: 588.47923490747,
		road: 14400.1457407407,
		acceleration: -0.0163888888888897
	},
	{
		id: 1556,
		time: 1555,
		velocity: 4.92861111111111,
		power: 1031.92492308315,
		road: 14405.1370833333,
		acceleration: 0.0762037037037038
	},
	{
		id: 1557,
		time: 1556,
		velocity: 5.28833333333333,
		power: 1074.02155971539,
		road: 14410.2070833333,
		acceleration: 0.0811111111111122
	},
	{
		id: 1558,
		time: 1557,
		velocity: 5.11472222222222,
		power: 628.719957925964,
		road: 14415.3114351852,
		acceleration: -0.0124074074074079
	},
	{
		id: 1559,
		time: 1558,
		velocity: 4.89138888888889,
		power: 453.645492300663,
		road: 14420.3856944445,
		acceleration: -0.0477777777777773
	},
	{
		id: 1560,
		time: 1559,
		velocity: 5.145,
		power: 1191.2902216661,
		road: 14425.4879166667,
		acceleration: 0.103703703703704
	},
	{
		id: 1561,
		time: 1560,
		velocity: 5.42583333333333,
		power: 2207.63223622938,
		road: 14430.7894907407,
		acceleration: 0.294999999999999
	},
	{
		id: 1562,
		time: 1561,
		velocity: 5.77638888888889,
		power: 2306.63107728903,
		road: 14436.3830092593,
		acceleration: 0.28888888888889
	},
	{
		id: 1563,
		time: 1562,
		velocity: 6.01166666666667,
		power: 1694.13548014203,
		road: 14442.2009259259,
		acceleration: 0.159907407407407
	},
	{
		id: 1564,
		time: 1563,
		velocity: 5.90555555555556,
		power: -125.225117781056,
		road: 14448.0142592593,
		acceleration: -0.169074074074074
	},
	{
		id: 1565,
		time: 1564,
		velocity: 5.26916666666667,
		power: -3042.76107880053,
		road: 14453.3724537037,
		acceleration: -0.741203703703704
	},
	{
		id: 1566,
		time: 1565,
		velocity: 3.78805555555556,
		power: -4434.26199646755,
		road: 14457.7586111111,
		acceleration: -1.20287037037037
	},
	{
		id: 1567,
		time: 1566,
		velocity: 2.29694444444444,
		power: -3200.12930576102,
		road: 14460.9481018519,
		acceleration: -1.19046296296296
	},
	{
		id: 1568,
		time: 1567,
		velocity: 1.69777777777778,
		power: -1791.92834854168,
		road: 14463.0218518519,
		acceleration: -1.04101851851852
	},
	{
		id: 1569,
		time: 1568,
		velocity: 0.665,
		power: -557.562417722536,
		road: 14464.2765277778,
		acceleration: -0.597129629629629
	},
	{
		id: 1570,
		time: 1569,
		velocity: 0.505555555555556,
		power: -279.517907186793,
		road: 14464.9496759259,
		acceleration: -0.565925925925926
	},
	{
		id: 1571,
		time: 1570,
		velocity: 0,
		power: 82.7611650817064,
		road: 14465.3780092593,
		acceleration: 0.0762962962962963
	},
	{
		id: 1572,
		time: 1571,
		velocity: 0.893888888888889,
		power: 232.133235044311,
		road: 14465.9830555556,
		acceleration: 0.27712962962963
	},
	{
		id: 1573,
		time: 1572,
		velocity: 1.33694444444444,
		power: 652.596765380861,
		road: 14467.0008333333,
		acceleration: 0.548333333333333
	},
	{
		id: 1574,
		time: 1573,
		velocity: 1.645,
		power: 495.663663038481,
		road: 14468.4135185185,
		acceleration: 0.241481481481481
	},
	{
		id: 1575,
		time: 1574,
		velocity: 1.61833333333333,
		power: 412.798646282452,
		road: 14470.0181481482,
		acceleration: 0.142407407407408
	},
	{
		id: 1576,
		time: 1575,
		velocity: 1.76416666666667,
		power: 288.649689775708,
		road: 14471.7189351852,
		acceleration: 0.0499074074074075
	},
	{
		id: 1577,
		time: 1576,
		velocity: 1.79472222222222,
		power: 236.887932017078,
		road: 14473.4521759259,
		acceleration: 0.0149999999999999
	},
	{
		id: 1578,
		time: 1577,
		velocity: 1.66333333333333,
		power: 144.150348291494,
		road: 14475.1725462963,
		acceleration: -0.0407407407407407
	},
	{
		id: 1579,
		time: 1578,
		velocity: 1.64194444444444,
		power: 310.100601375357,
		road: 14476.9025,
		acceleration: 0.0599074074074073
	},
	{
		id: 1580,
		time: 1579,
		velocity: 1.97444444444444,
		power: 1167.05228206362,
		road: 14478.9048148148,
		acceleration: 0.484814814814815
	},
	{
		id: 1581,
		time: 1580,
		velocity: 3.11777777777778,
		power: 1883.75399813067,
		road: 14481.4708333333,
		acceleration: 0.642592592592593
	},
	{
		id: 1582,
		time: 1581,
		velocity: 3.56972222222222,
		power: 2766.88831310781,
		road: 14484.7375925926,
		acceleration: 0.758888888888889
	},
	{
		id: 1583,
		time: 1582,
		velocity: 4.25111111111111,
		power: 2654.63462278587,
		road: 14488.6711111111,
		acceleration: 0.57462962962963
	},
	{
		id: 1584,
		time: 1583,
		velocity: 4.84166666666667,
		power: 3629.42858781648,
		road: 14493.240462963,
		acceleration: 0.697037037037036
	},
	{
		id: 1585,
		time: 1584,
		velocity: 5.66083333333333,
		power: 3358.34437085568,
		road: 14498.4277314815,
		acceleration: 0.538796296296296
	},
	{
		id: 1586,
		time: 1585,
		velocity: 5.8675,
		power: 2613.99690384062,
		road: 14504.0561111111,
		acceleration: 0.343425925925925
	},
	{
		id: 1587,
		time: 1586,
		velocity: 5.87194444444444,
		power: 712.431534701875,
		road: 14509.8477777778,
		acceleration: -0.0168518518518512
	},
	{
		id: 1588,
		time: 1587,
		velocity: 5.61027777777778,
		power: -135.814351686403,
		road: 14515.5456481482,
		acceleration: -0.170740740740741
	},
	{
		id: 1589,
		time: 1588,
		velocity: 5.35527777777778,
		power: -42.8315356044445,
		road: 14521.0817592593,
		acceleration: -0.152777777777778
	},
	{
		id: 1590,
		time: 1589,
		velocity: 5.41361111111111,
		power: -726.584540499761,
		road: 14526.3979166667,
		acceleration: -0.28712962962963
	},
	{
		id: 1591,
		time: 1590,
		velocity: 4.74888888888889,
		power: -1504.77902123333,
		road: 14531.3396759259,
		acceleration: -0.461666666666667
	},
	{
		id: 1592,
		time: 1591,
		velocity: 3.97027777777778,
		power: -2644.86470832983,
		road: 14535.6592592593,
		acceleration: -0.782685185185185
	},
	{
		id: 1593,
		time: 1592,
		velocity: 3.06555555555556,
		power: -2604.63330807743,
		road: 14539.1244444445,
		acceleration: -0.926111111111111
	},
	{
		id: 1594,
		time: 1593,
		velocity: 1.97055555555556,
		power: -1770.27981238604,
		road: 14541.6985185185,
		acceleration: -0.856111111111111
	},
	{
		id: 1595,
		time: 1594,
		velocity: 1.40194444444444,
		power: -995.383165420552,
		road: 14543.4863425926,
		acceleration: -0.716388888888889
	},
	{
		id: 1596,
		time: 1595,
		velocity: 0.916388888888889,
		power: -328.545522598548,
		road: 14544.7102314815,
		acceleration: -0.411481481481482
	},
	{
		id: 1597,
		time: 1596,
		velocity: 0.736111111111111,
		power: -252.399452038525,
		road: 14545.4947222222,
		acceleration: -0.467314814814815
	},
	{
		id: 1598,
		time: 1597,
		velocity: 0,
		power: -67.094249180263,
		road: 14545.8928240741,
		acceleration: -0.305462962962963
	},
	{
		id: 1599,
		time: 1598,
		velocity: 0,
		power: -13.6963588369071,
		road: 14546.0155092593,
		acceleration: -0.24537037037037
	},
	{
		id: 1600,
		time: 1599,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1601,
		time: 1600,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1602,
		time: 1601,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1603,
		time: 1602,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1604,
		time: 1603,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1605,
		time: 1604,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1606,
		time: 1605,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1607,
		time: 1606,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1608,
		time: 1607,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1609,
		time: 1608,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1610,
		time: 1609,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1611,
		time: 1610,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1612,
		time: 1611,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1613,
		time: 1612,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1614,
		time: 1613,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1615,
		time: 1614,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1616,
		time: 1615,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1617,
		time: 1616,
		velocity: 0,
		power: 0,
		road: 14546.0155092593,
		acceleration: 0
	},
	{
		id: 1618,
		time: 1617,
		velocity: 0,
		power: 39.21338993547,
		road: 14546.1309722222,
		acceleration: 0.230925925925926
	},
	{
		id: 1619,
		time: 1618,
		velocity: 0.692777777777778,
		power: 734.551713119671,
		road: 14546.8425462963,
		acceleration: 0.961296296296296
	},
	{
		id: 1620,
		time: 1619,
		velocity: 2.88388888888889,
		power: 2473.61746402086,
		road: 14548.6798148148,
		acceleration: 1.29009259259259
	},
	{
		id: 1621,
		time: 1620,
		velocity: 3.87027777777778,
		power: 5296.64405982928,
		road: 14551.9487962963,
		acceleration: 1.57333333333333
	},
	{
		id: 1622,
		time: 1621,
		velocity: 5.41277777777778,
		power: 4055.80002788361,
		road: 14556.4133333333,
		acceleration: 0.817777777777779
	},
	{
		id: 1623,
		time: 1622,
		velocity: 5.33722222222222,
		power: 4322.44052507135,
		road: 14561.6496759259,
		acceleration: 0.725833333333332
	},
	{
		id: 1624,
		time: 1623,
		velocity: 6.04777777777778,
		power: 2483.14290558646,
		road: 14567.402962963,
		acceleration: 0.308055555555556
	},
	{
		id: 1625,
		time: 1624,
		velocity: 6.33694444444444,
		power: 3274.42468993787,
		road: 14573.5176851852,
		acceleration: 0.414814814814815
	},
	{
		id: 1626,
		time: 1625,
		velocity: 6.58166666666667,
		power: 1674.40083229517,
		road: 14579.9025462963,
		acceleration: 0.125462962962963
	},
	{
		id: 1627,
		time: 1626,
		velocity: 6.42416666666667,
		power: 929.68118096984,
		road: 14586.3505555556,
		acceleration: 0.000833333333333464
	},
	{
		id: 1628,
		time: 1627,
		velocity: 6.33944444444444,
		power: 1554.38691759359,
		road: 14592.8491666667,
		acceleration: 0.10037037037037
	},
	{
		id: 1629,
		time: 1628,
		velocity: 6.88277777777778,
		power: 3356.34884987318,
		road: 14599.5835185185,
		acceleration: 0.371111111111112
	},
	{
		id: 1630,
		time: 1629,
		velocity: 7.5375,
		power: 4847.49782419627,
		road: 14606.779212963,
		acceleration: 0.551574074074073
	},
	{
		id: 1631,
		time: 1630,
		velocity: 7.99416666666667,
		power: 4363.51883900841,
		road: 14614.4685185185,
		acceleration: 0.435648148148149
	},
	{
		id: 1632,
		time: 1631,
		velocity: 8.18972222222222,
		power: 2214.50446577612,
		road: 14622.44,
		acceleration: 0.128703703703705
	},
	{
		id: 1633,
		time: 1632,
		velocity: 7.92361111111111,
		power: 439.447098758357,
		road: 14630.4231018519,
		acceleration: -0.105462962962965
	},
	{
		id: 1634,
		time: 1633,
		velocity: 7.67777777777778,
		power: 694.433864579918,
		road: 14638.3184259259,
		acceleration: -0.0700925925925926
	},
	{
		id: 1635,
		time: 1634,
		velocity: 7.97944444444444,
		power: 1979.34320400083,
		road: 14646.2288425926,
		acceleration: 0.100277777777777
	},
	{
		id: 1636,
		time: 1635,
		velocity: 8.22444444444444,
		power: 2959.120768865,
		road: 14654.2999537037,
		acceleration: 0.221111111111112
	},
	{
		id: 1637,
		time: 1636,
		velocity: 8.34111111111111,
		power: 2703.67554254799,
		road: 14662.5703703704,
		acceleration: 0.1775
	},
	{
		id: 1638,
		time: 1637,
		velocity: 8.51194444444444,
		power: 2118.87753282422,
		road: 14670.978287037,
		acceleration: 0.0974999999999984
	},
	{
		id: 1639,
		time: 1638,
		velocity: 8.51694444444444,
		power: 2159.52334475951,
		road: 14679.484212963,
		acceleration: 0.0985185185185209
	},
	{
		id: 1640,
		time: 1639,
		velocity: 8.63666666666667,
		power: 2001.51537305161,
		road: 14688.0772222222,
		acceleration: 0.0756481481481472
	},
	{
		id: 1641,
		time: 1640,
		velocity: 8.73888888888889,
		power: 2869.68106661168,
		road: 14696.795787037,
		acceleration: 0.175462962962964
	},
	{
		id: 1642,
		time: 1641,
		velocity: 9.04333333333333,
		power: 3780.21658245135,
		road: 14705.7378240741,
		acceleration: 0.27148148148148
	},
	{
		id: 1643,
		time: 1642,
		velocity: 9.45111111111111,
		power: 3764.54945888467,
		road: 14714.9427314815,
		acceleration: 0.254259259259259
	},
	{
		id: 1644,
		time: 1643,
		velocity: 9.50166666666667,
		power: 2921.44477111229,
		road: 14724.3491666667,
		acceleration: 0.148796296296297
	},
	{
		id: 1645,
		time: 1644,
		velocity: 9.48972222222222,
		power: 1605.54087679977,
		road: 14733.8298611111,
		acceleration: -0.000277777777778709
	},
	{
		id: 1646,
		time: 1645,
		velocity: 9.45027777777778,
		power: 1626.68159821513,
		road: 14743.3114351852,
		acceleration: 0.00203703703703617
	},
	{
		id: 1647,
		time: 1646,
		velocity: 9.50777777777778,
		power: 1026.61535150252,
		road: 14752.7621759259,
		acceleration: -0.0637037037037018
	},
	{
		id: 1648,
		time: 1647,
		velocity: 9.29861111111111,
		power: 1704.62470679793,
		road: 14762.1872685185,
		acceleration: 0.012407407407407
	},
	{
		id: 1649,
		time: 1648,
		velocity: 9.4875,
		power: 2433.73894861108,
		road: 14771.6643518519,
		acceleration: 0.0915740740740745
	},
	{
		id: 1650,
		time: 1649,
		velocity: 9.7825,
		power: 4626.90279214904,
		road: 14781.3478703704,
		acceleration: 0.321296296296296
	},
	{
		id: 1651,
		time: 1650,
		velocity: 10.2625,
		power: 6468.87370209168,
		road: 14791.4361111111,
		acceleration: 0.488148148148149
	},
	{
		id: 1652,
		time: 1651,
		velocity: 10.9519444444444,
		power: 6786.91218637998,
		road: 14802.0099074074,
		acceleration: 0.482962962962963
	},
	{
		id: 1653,
		time: 1652,
		velocity: 11.2313888888889,
		power: 4641.08350374172,
		road: 14812.9502314815,
		acceleration: 0.250092592592591
	},
	{
		id: 1654,
		time: 1653,
		velocity: 11.0127777777778,
		power: 1681.37132305707,
		road: 14823.9973148148,
		acceleration: -0.0365740740740748
	},
	{
		id: 1655,
		time: 1654,
		velocity: 10.8422222222222,
		power: 1895.79335907568,
		road: 14835.0183796296,
		acceleration: -0.0154629629629603
	},
	{
		id: 1656,
		time: 1655,
		velocity: 11.185,
		power: 4015.18355195241,
		road: 14846.1228703704,
		acceleration: 0.182314814814813
	},
	{
		id: 1657,
		time: 1656,
		velocity: 11.5597222222222,
		power: 5707.38868527362,
		road: 14857.4818981482,
		acceleration: 0.32675925925926
	},
	{
		id: 1658,
		time: 1657,
		velocity: 11.8225,
		power: 5474.52714343665,
		road: 14869.1482407407,
		acceleration: 0.287870370370371
	},
	{
		id: 1659,
		time: 1658,
		velocity: 12.0486111111111,
		power: 4741.46575980161,
		road: 14881.0633796296,
		acceleration: 0.209722222222224
	},
	{
		id: 1660,
		time: 1659,
		velocity: 12.1888888888889,
		power: 3985.6155524978,
		road: 14893.1512962963,
		acceleration: 0.135833333333332
	},
	{
		id: 1661,
		time: 1660,
		velocity: 12.23,
		power: 3956.53449250181,
		road: 14905.3710185185,
		acceleration: 0.127777777777778
	},
	{
		id: 1662,
		time: 1661,
		velocity: 12.4319444444444,
		power: 4453.99336675028,
		road: 14917.7365740741,
		acceleration: 0.163888888888888
	},
	{
		id: 1663,
		time: 1662,
		velocity: 12.6805555555556,
		power: 4955.73539424351,
		road: 14930.282962963,
		acceleration: 0.197777777777778
	},
	{
		id: 1664,
		time: 1663,
		velocity: 12.8233333333333,
		power: 4424.4878688307,
		road: 14943.0012037037,
		acceleration: 0.145925925925924
	},
	{
		id: 1665,
		time: 1664,
		velocity: 12.8697222222222,
		power: 4356.36717289527,
		road: 14955.8595833333,
		acceleration: 0.134351851851854
	},
	{
		id: 1666,
		time: 1665,
		velocity: 13.0836111111111,
		power: 4226.12689751453,
		road: 14968.8443981482,
		acceleration: 0.118518518518519
	},
	{
		id: 1667,
		time: 1666,
		velocity: 13.1788888888889,
		power: 4226.52880243856,
		road: 14981.9453703704,
		acceleration: 0.113796296296295
	},
	{
		id: 1668,
		time: 1667,
		velocity: 13.2111111111111,
		power: 2464.03286220268,
		road: 14995.0890277778,
		acceleration: -0.0284259259259247
	},
	{
		id: 1669,
		time: 1668,
		velocity: 12.9983333333333,
		power: 1600.36826391641,
		road: 15008.1706018519,
		acceleration: -0.0957407407407427
	},
	{
		id: 1670,
		time: 1669,
		velocity: 12.8916666666667,
		power: 663.002325962518,
		road: 15021.1201388889,
		acceleration: -0.168333333333333
	},
	{
		id: 1671,
		time: 1670,
		velocity: 12.7061111111111,
		power: 1669.73689936907,
		road: 15033.9436574074,
		acceleration: -0.0837037037037032
	},
	{
		id: 1672,
		time: 1671,
		velocity: 12.7472222222222,
		power: 2432.51563700807,
		road: 15046.7154166667,
		acceleration: -0.0198148148148167
	},
	{
		id: 1673,
		time: 1672,
		velocity: 12.8322222222222,
		power: 3382.02271979963,
		road: 15059.5059722222,
		acceleration: 0.0574074074074105
	},
	{
		id: 1674,
		time: 1673,
		velocity: 12.8783333333333,
		power: 3979.53796921543,
		road: 15072.3768055556,
		acceleration: 0.103148148148145
	},
	{
		id: 1675,
		time: 1674,
		velocity: 13.0566666666667,
		power: 4022.09550816746,
		road: 15085.350462963,
		acceleration: 0.102500000000001
	},
	{
		id: 1676,
		time: 1675,
		velocity: 13.1397222222222,
		power: 5771.77157064559,
		road: 15098.4930092593,
		acceleration: 0.235277777777778
	},
	{
		id: 1677,
		time: 1676,
		velocity: 13.5841666666667,
		power: 6473.20214845406,
		road: 15111.8918981482,
		acceleration: 0.277407407407409
	},
	{
		id: 1678,
		time: 1677,
		velocity: 13.8888888888889,
		power: 9121.25903996803,
		road: 15125.6592592593,
		acceleration: 0.459537037037038
	},
	{
		id: 1679,
		time: 1678,
		velocity: 14.5183333333333,
		power: 8709.58687849595,
		road: 15139.8569907407,
		acceleration: 0.4012037037037
	},
	{
		id: 1680,
		time: 1679,
		velocity: 14.7877777777778,
		power: 11828.4955251063,
		road: 15154.5519907407,
		acceleration: 0.593333333333334
	},
	{
		id: 1681,
		time: 1680,
		velocity: 15.6688888888889,
		power: 12209.9067328296,
		road: 15169.8321759259,
		acceleration: 0.577037037037037
	},
	{
		id: 1682,
		time: 1681,
		velocity: 16.2494444444444,
		power: 12629.3929037536,
		road: 15185.6830555556,
		acceleration: 0.564351851851852
	},
	{
		id: 1683,
		time: 1682,
		velocity: 16.4808333333333,
		power: 9282.96172415669,
		road: 15201.9755092593,
		acceleration: 0.318796296296298
	},
	{
		id: 1684,
		time: 1683,
		velocity: 16.6252777777778,
		power: 6879.51148209409,
		road: 15218.5042592593,
		acceleration: 0.153796296296296
	},
	{
		id: 1685,
		time: 1684,
		velocity: 16.7108333333333,
		power: 5216.02252815622,
		road: 15235.1322685185,
		acceleration: 0.044722222222223
	},
	{
		id: 1686,
		time: 1685,
		velocity: 16.615,
		power: 3602.05558074838,
		road: 15251.7543518519,
		acceleration: -0.0565740740740743
	},
	{
		id: 1687,
		time: 1686,
		velocity: 16.4555555555556,
		power: 393.434948491041,
		road: 15268.2205092593,
		acceleration: -0.255277777777781
	},
	{
		id: 1688,
		time: 1687,
		velocity: 15.945,
		power: -567.895420332244,
		road: 15284.4031481482,
		acceleration: -0.311759259259254
	},
	{
		id: 1689,
		time: 1688,
		velocity: 15.6797222222222,
		power: -2172.95199186133,
		road: 15300.2237962963,
		acceleration: -0.412222222222224
	},
	{
		id: 1690,
		time: 1689,
		velocity: 15.2188888888889,
		power: -1870.61586357938,
		road: 15315.6441203704,
		acceleration: -0.388425925925926
	},
	{
		id: 1691,
		time: 1690,
		velocity: 14.7797222222222,
		power: -1587.42654181502,
		road: 15330.6875,
		acceleration: -0.365462962962962
	},
	{
		id: 1692,
		time: 1691,
		velocity: 14.5833333333333,
		power: -96.6233051339694,
		road: 15345.4198148148,
		acceleration: -0.256666666666668
	},
	{
		id: 1693,
		time: 1692,
		velocity: 14.4488888888889,
		power: 1966.02559777477,
		road: 15359.9711111111,
		acceleration: -0.10537037037037
	},
	{
		id: 1694,
		time: 1693,
		velocity: 14.4636111111111,
		power: 3251.12995782862,
		road: 15374.464212963,
		acceleration: -0.0110185185185205
	},
	{
		id: 1695,
		time: 1694,
		velocity: 14.5502777777778,
		power: 3608.37720917428,
		road: 15388.9591666667,
		acceleration: 0.0147222222222236
	},
	{
		id: 1696,
		time: 1695,
		velocity: 14.4930555555556,
		power: 3169.42058764375,
		road: 15403.4530092593,
		acceleration: -0.0169444444444444
	},
	{
		id: 1697,
		time: 1696,
		velocity: 14.4127777777778,
		power: 3083.81036181006,
		road: 15417.9271296296,
		acceleration: -0.0225000000000026
	},
	{
		id: 1698,
		time: 1697,
		velocity: 14.4827777777778,
		power: 2999.9129563652,
		road: 15432.3761111111,
		acceleration: -0.0277777777777768
	},
	{
		id: 1699,
		time: 1698,
		velocity: 14.4097222222222,
		power: 4801.94965950493,
		road: 15446.8618518519,
		acceleration: 0.101296296296297
	},
	{
		id: 1700,
		time: 1699,
		velocity: 14.7166666666667,
		power: 2961.50862352041,
		road: 15461.3818518519,
		acceleration: -0.0327777777777776
	},
	{
		id: 1701,
		time: 1700,
		velocity: 14.3844444444444,
		power: 3261.21183067175,
		road: 15475.8802314815,
		acceleration: -0.0104629629629649
	},
	{
		id: 1702,
		time: 1701,
		velocity: 14.3783333333333,
		power: 2076.08644758276,
		road: 15490.3260648148,
		acceleration: -0.0946296296296261
	},
	{
		id: 1703,
		time: 1702,
		velocity: 14.4327777777778,
		power: 3822.83435565947,
		road: 15504.7411111111,
		acceleration: 0.0330555555555527
	},
	{
		id: 1704,
		time: 1703,
		velocity: 14.4836111111111,
		power: 3549.24756152653,
		road: 15519.1788888889,
		acceleration: 0.0124074074074088
	},
	{
		id: 1705,
		time: 1704,
		velocity: 14.4155555555556,
		power: 3916.82120571292,
		road: 15533.6419444444,
		acceleration: 0.0381481481481458
	},
	{
		id: 1706,
		time: 1705,
		velocity: 14.5472222222222,
		power: 3609.6821037408,
		road: 15548.1315740741,
		acceleration: 0.0150000000000006
	},
	{
		id: 1707,
		time: 1706,
		velocity: 14.5286111111111,
		power: 4070.19992867825,
		road: 15562.6522685185,
		acceleration: 0.0471296296296302
	},
	{
		id: 1708,
		time: 1707,
		velocity: 14.5569444444444,
		power: 3530.77406103509,
		road: 15577.2001851852,
		acceleration: 0.00731481481481566
	},
	{
		id: 1709,
		time: 1708,
		velocity: 14.5691666666667,
		power: 3623.2850500399,
		road: 15591.7585648148,
		acceleration: 0.0136111111111124
	},
	{
		id: 1710,
		time: 1709,
		velocity: 14.5694444444444,
		power: 3422.7561892429,
		road: 15606.3232407407,
		acceleration: -0.00101851851851897
	},
	{
		id: 1711,
		time: 1710,
		velocity: 14.5538888888889,
		power: 3286.11732530446,
		road: 15620.8820833333,
		acceleration: -0.0106481481481495
	},
	{
		id: 1712,
		time: 1711,
		velocity: 14.5372222222222,
		power: 3862.78812269404,
		road: 15635.4508333333,
		acceleration: 0.0304629629629645
	},
	{
		id: 1713,
		time: 1712,
		velocity: 14.6608333333333,
		power: 4097.53305077081,
		road: 15650.0577777778,
		acceleration: 0.0459259259259248
	},
	{
		id: 1714,
		time: 1713,
		velocity: 14.6916666666667,
		power: 3725.50631370717,
		road: 15664.6967592593,
		acceleration: 0.0181481481481498
	},
	{
		id: 1715,
		time: 1714,
		velocity: 14.5916666666667,
		power: 2826.16405362481,
		road: 15679.3219444444,
		acceleration: -0.0457407407407437
	},
	{
		id: 1716,
		time: 1715,
		velocity: 14.5236111111111,
		power: 3281.2299732435,
		road: 15693.9181481482,
		acceleration: -0.0122222222222206
	},
	{
		id: 1717,
		time: 1716,
		velocity: 14.655,
		power: 3670.67178862113,
		road: 15708.5160648148,
		acceleration: 0.0156481481481485
	},
	{
		id: 1718,
		time: 1717,
		velocity: 14.6386111111111,
		power: 2983.76106638405,
		road: 15723.1051388889,
		acceleration: -0.033333333333335
	},
	{
		id: 1719,
		time: 1718,
		velocity: 14.4236111111111,
		power: 2658.31936692619,
		road: 15737.6498611111,
		acceleration: -0.055370370370369
	},
	{
		id: 1720,
		time: 1719,
		velocity: 14.4888888888889,
		power: 2452.29357379044,
		road: 15752.1326851852,
		acceleration: -0.0684259259259257
	},
	{
		id: 1721,
		time: 1720,
		velocity: 14.4333333333333,
		power: 3566.40295384537,
		road: 15766.5878240741,
		acceleration: 0.0130555555555549
	},
	{
		id: 1722,
		time: 1721,
		velocity: 14.4627777777778,
		power: 3113.64035947251,
		road: 15781.0396759259,
		acceleration: -0.0196296296296286
	},
	{
		id: 1723,
		time: 1722,
		velocity: 14.43,
		power: 3783.08134066523,
		road: 15795.4960648148,
		acceleration: 0.0287037037037052
	},
	{
		id: 1724,
		time: 1723,
		velocity: 14.5194444444444,
		power: 3914.51423654954,
		road: 15809.9853240741,
		acceleration: 0.0370370370370345
	},
	{
		id: 1725,
		time: 1724,
		velocity: 14.5738888888889,
		power: 3991.87483666123,
		road: 15824.5137037037,
		acceleration: 0.0412037037037045
	},
	{
		id: 1726,
		time: 1725,
		velocity: 14.5536111111111,
		power: 4316.02415099909,
		road: 15839.0939814815,
		acceleration: 0.0625925925925905
	},
	{
		id: 1727,
		time: 1726,
		velocity: 14.7072222222222,
		power: 4293.69929722257,
		road: 15853.7349074074,
		acceleration: 0.0587037037037046
	},
	{
		id: 1728,
		time: 1727,
		velocity: 14.75,
		power: 4855.29024912683,
		road: 15868.4530555556,
		acceleration: 0.0957407407407391
	},
	{
		id: 1729,
		time: 1728,
		velocity: 14.8408333333333,
		power: 4444.93868313967,
		road: 15883.2508333333,
		acceleration: 0.0635185185185208
	},
	{
		id: 1730,
		time: 1729,
		velocity: 14.8977777777778,
		power: 5499.94400485665,
		road: 15898.1473148148,
		acceleration: 0.133888888888887
	},
	{
		id: 1731,
		time: 1730,
		velocity: 15.1516666666667,
		power: 7401.88070375077,
		road: 15913.2394444444,
		acceleration: 0.257407407407408
	},
	{
		id: 1732,
		time: 1731,
		velocity: 15.6130555555556,
		power: 12785.1664355542,
		road: 15928.7595833333,
		acceleration: 0.598611111111111
	},
	{
		id: 1733,
		time: 1732,
		velocity: 16.6936111111111,
		power: 19137.8412183055,
		road: 15945.054537037,
		acceleration: 0.95101851851852
	},
	{
		id: 1734,
		time: 1733,
		velocity: 18.0047222222222,
		power: 21452.0842435834,
		road: 15962.32625,
		acceleration: 1.0025
	},
	{
		id: 1735,
		time: 1734,
		velocity: 18.6205555555556,
		power: 25308.4054542208,
		road: 15980.6616203704,
		acceleration: 1.12481481481482
	},
	{
		id: 1736,
		time: 1735,
		velocity: 20.0680555555556,
		power: 26411.7110247463,
		road: 16000.0984722222,
		acceleration: 1.07814814814814
	},
	{
		id: 1737,
		time: 1736,
		velocity: 21.2391666666667,
		power: 33248.3690666581,
		road: 16020.73125,
		acceleration: 1.31370370370371
	},
	{
		id: 1738,
		time: 1737,
		velocity: 22.5616666666667,
		power: 27390.2572132461,
		road: 16042.480462963,
		acceleration: 0.919166666666666
	},
	{
		id: 1739,
		time: 1738,
		velocity: 22.8255555555556,
		power: 23698.8680859866,
		road: 16065.0300462963,
		acceleration: 0.681574074074071
	},
	{
		id: 1740,
		time: 1739,
		velocity: 23.2838888888889,
		power: 9748.90779857043,
		road: 16087.9309259259,
		acceleration: 0.0210185185185239
	},
	{
		id: 1741,
		time: 1740,
		velocity: 22.6247222222222,
		power: 860.105089928421,
		road: 16110.6530555556,
		acceleration: -0.378518518518518
	},
	{
		id: 1742,
		time: 1741,
		velocity: 21.69,
		power: -24443.9204797423,
		road: 16132.4030555556,
		acceleration: -1.56574074074074
	},
	{
		id: 1743,
		time: 1742,
		velocity: 18.5866666666667,
		power: -35350.9612064641,
		road: 16152.2662962963,
		acceleration: -2.20777777777778
	},
	{
		id: 1744,
		time: 1743,
		velocity: 16.0013888888889,
		power: -51909.2792381599,
		road: 16169.2852314815,
		acceleration: -3.48083333333333
	},
	{
		id: 1745,
		time: 1744,
		velocity: 11.2475,
		power: -38130.0656761295,
		road: 16182.9893518519,
		acceleration: -3.1487962962963
	},
	{
		id: 1746,
		time: 1745,
		velocity: 9.14027777777778,
		power: -24318.9629879323,
		road: 16193.8468518519,
		acceleration: -2.54444444444444
	},
	{
		id: 1747,
		time: 1746,
		velocity: 8.36805555555556,
		power: -8761.51405167423,
		road: 16202.8336574074,
		acceleration: -1.19694444444444
	},
	{
		id: 1748,
		time: 1747,
		velocity: 7.65666666666667,
		power: -6850.37466361068,
		road: 16210.6822685185,
		acceleration: -1.07944444444445
	},
	{
		id: 1749,
		time: 1748,
		velocity: 5.90194444444444,
		power: -8085.64924101111,
		road: 16217.2697222222,
		acceleration: -1.44287037037037
	},
	{
		id: 1750,
		time: 1749,
		velocity: 4.03944444444444,
		power: -6874.83824186233,
		road: 16222.3528703704,
		acceleration: -1.56574074074074
	},
	{
		id: 1751,
		time: 1750,
		velocity: 2.95944444444444,
		power: -2986.14200687074,
		road: 16226.1737037037,
		acceleration: -0.958888888888889
	},
	{
		id: 1752,
		time: 1751,
		velocity: 3.02527777777778,
		power: -820.981579369505,
		road: 16229.3107407407,
		acceleration: -0.408703703703703
	},
	{
		id: 1753,
		time: 1752,
		velocity: 2.81333333333333,
		power: 137.639967763693,
		road: 16232.2024537037,
		acceleration: -0.0819444444444453
	},
	{
		id: 1754,
		time: 1753,
		velocity: 2.71361111111111,
		power: 770.869287571448,
		road: 16235.1260185185,
		acceleration: 0.145648148148148
	},
	{
		id: 1755,
		time: 1754,
		velocity: 3.46222222222222,
		power: 2719.42180101927,
		road: 16238.4823148148,
		acceleration: 0.719814814814815
	},
	{
		id: 1756,
		time: 1755,
		velocity: 4.97277777777778,
		power: 4966.36999608806,
		road: 16242.743287037,
		acceleration: 1.08953703703704
	},
	{
		id: 1757,
		time: 1756,
		velocity: 5.98222222222222,
		power: 5067.13454653151,
		road: 16247.9861111111,
		acceleration: 0.874166666666667
	},
	{
		id: 1758,
		time: 1757,
		velocity: 6.08472222222222,
		power: 5533.80624356754,
		road: 16254.0702314815,
		acceleration: 0.808425925925925
	},
	{
		id: 1759,
		time: 1758,
		velocity: 7.39805555555555,
		power: 5685.41909932624,
		road: 16260.9181018519,
		acceleration: 0.719074074074073
	},
	{
		id: 1760,
		time: 1759,
		velocity: 8.13944444444444,
		power: 8363.11942536822,
		road: 16268.6159259259,
		acceleration: 0.980833333333334
	},
	{
		id: 1761,
		time: 1760,
		velocity: 9.02722222222222,
		power: 6669.87784102843,
		road: 16277.1313888889,
		acceleration: 0.654444444444444
	},
	{
		id: 1762,
		time: 1761,
		velocity: 9.36138888888889,
		power: 7112.32456259435,
		road: 16286.294212963,
		acceleration: 0.640277777777779
	},
	{
		id: 1763,
		time: 1762,
		velocity: 10.0602777777778,
		power: 6858.56314837607,
		road: 16296.0553703704,
		acceleration: 0.55638888888889
	},
	{
		id: 1764,
		time: 1763,
		velocity: 10.6963888888889,
		power: 9134.54462119564,
		road: 16306.4610185185,
		acceleration: 0.73259259259259
	},
	{
		id: 1765,
		time: 1764,
		velocity: 11.5591666666667,
		power: 9092.11953424503,
		road: 16317.5639351852,
		acceleration: 0.661944444444446
	},
	{
		id: 1766,
		time: 1765,
		velocity: 12.0461111111111,
		power: 7182.29415875502,
		road: 16329.2188888889,
		acceleration: 0.442129629629626
	},
	{
		id: 1767,
		time: 1766,
		velocity: 12.0227777777778,
		power: 1611.22463717122,
		road: 16341.062962963,
		acceleration: -0.0638888888888864
	},
	{
		id: 1768,
		time: 1767,
		velocity: 11.3675,
		power: -2317.56551904762,
		road: 16352.6687037037,
		acceleration: -0.412777777777777
	},
	{
		id: 1769,
		time: 1768,
		velocity: 10.8077777777778,
		power: -3793.99757713793,
		road: 16363.7905555556,
		acceleration: -0.555
	},
	{
		id: 1770,
		time: 1769,
		velocity: 10.3577777777778,
		power: -3829.45347712518,
		road: 16374.3494907407,
		acceleration: -0.570833333333333
	},
	{
		id: 1771,
		time: 1770,
		velocity: 9.655,
		power: -3672.96197582969,
		road: 16384.3381944444,
		acceleration: -0.569629629629631
	},
	{
		id: 1772,
		time: 1771,
		velocity: 9.09888888888889,
		power: -1337.73933977057,
		road: 16393.8791203704,
		acceleration: -0.325925925925926
	},
	{
		id: 1773,
		time: 1772,
		velocity: 9.38,
		power: 2839.63786630884,
		road: 16403.3260185185,
		acceleration: 0.13787037037037
	},
	{
		id: 1774,
		time: 1773,
		velocity: 10.0686111111111,
		power: 7148.79201904198,
		road: 16413.1335648148,
		acceleration: 0.583425925925928
	},
	{
		id: 1775,
		time: 1774,
		velocity: 10.8491666666667,
		power: 7597.49478483694,
		road: 16423.5223611111,
		acceleration: 0.579074074074072
	},
	{
		id: 1776,
		time: 1775,
		velocity: 11.1172222222222,
		power: 4358.92033865145,
		road: 16434.3159722222,
		acceleration: 0.230555555555556
	},
	{
		id: 1777,
		time: 1776,
		velocity: 10.7602777777778,
		power: 375.003305380529,
		road: 16445.14625,
		acceleration: -0.157222222222224
	},
	{
		id: 1778,
		time: 1777,
		velocity: 10.3775,
		power: -2709.95226530977,
		road: 16455.6678703704,
		acceleration: -0.46009259259259
	},
	{
		id: 1779,
		time: 1778,
		velocity: 9.73694444444444,
		power: -2658.74708170174,
		road: 16465.7285185185,
		acceleration: -0.461851851851854
	},
	{
		id: 1780,
		time: 1779,
		velocity: 9.37472222222222,
		power: -3013.66394683041,
		road: 16475.3034722222,
		acceleration: -0.509537037037036
	},
	{
		id: 1781,
		time: 1780,
		velocity: 8.84888888888889,
		power: -2642.86604705202,
		road: 16484.3838888889,
		acceleration: -0.479537037037037
	},
	{
		id: 1782,
		time: 1781,
		velocity: 8.29833333333333,
		power: -2253.21380064855,
		road: 16493.0025925926,
		acceleration: -0.443888888888891
	},
	{
		id: 1783,
		time: 1782,
		velocity: 8.04305555555555,
		power: -2217.01519161046,
		road: 16501.1743055556,
		acceleration: -0.450092592592592
	},
	{
		id: 1784,
		time: 1783,
		velocity: 7.49861111111111,
		power: -2092.52162078095,
		road: 16508.8981018519,
		acceleration: -0.44574074074074
	},
	{
		id: 1785,
		time: 1784,
		velocity: 6.96111111111111,
		power: -2401.97382118709,
		road: 16516.1463425926,
		acceleration: -0.505370370370371
	},
	{
		id: 1786,
		time: 1785,
		velocity: 6.52694444444444,
		power: -840.419149813306,
		road: 16523.0005092593,
		acceleration: -0.282777777777778
	},
	{
		id: 1787,
		time: 1786,
		velocity: 6.65027777777778,
		power: 1570.93895725137,
		road: 16529.7589351852,
		acceleration: 0.0912962962962958
	},
	{
		id: 1788,
		time: 1787,
		velocity: 7.235,
		power: 4281.88949383143,
		road: 16536.8047685185,
		acceleration: 0.483518518518519
	},
	{
		id: 1789,
		time: 1788,
		velocity: 7.9775,
		power: 7291.85393927478,
		road: 16544.509212963,
		acceleration: 0.833703703703703
	},
	{
		id: 1790,
		time: 1789,
		velocity: 9.15138888888889,
		power: 8454.22137668879,
		road: 16553.0650925926,
		acceleration: 0.869166666666668
	},
	{
		id: 1791,
		time: 1790,
		velocity: 9.8425,
		power: 11117.8072161647,
		road: 16562.5796296296,
		acceleration: 1.04814814814815
	},
	{
		id: 1792,
		time: 1791,
		velocity: 11.1219444444444,
		power: 10561.7597467099,
		road: 16573.0526388889,
		acceleration: 0.868796296296296
	},
	{
		id: 1793,
		time: 1792,
		velocity: 11.7577777777778,
		power: 11899.5650200561,
		road: 16584.4093518519,
		acceleration: 0.898611111111112
	},
	{
		id: 1794,
		time: 1793,
		velocity: 12.5383333333333,
		power: 7746.23556243266,
		road: 16596.4480555556,
		acceleration: 0.465370370370369
	},
	{
		id: 1795,
		time: 1794,
		velocity: 12.5180555555556,
		power: 5440.49632322903,
		road: 16608.8424537037,
		acceleration: 0.246018518518518
	},
	{
		id: 1796,
		time: 1795,
		velocity: 12.4958333333333,
		power: 6467.61894854799,
		road: 16621.5181944445,
		acceleration: 0.316666666666668
	},
	{
		id: 1797,
		time: 1796,
		velocity: 13.4883333333333,
		power: 7862.59235536931,
		road: 16634.5565277778,
		acceleration: 0.408518518518518
	},
	{
		id: 1798,
		time: 1797,
		velocity: 13.7436111111111,
		power: 10501.7442497303,
		road: 16648.0901388889,
		acceleration: 0.582037037037036
	},
	{
		id: 1799,
		time: 1798,
		velocity: 14.2419444444444,
		power: 7890.95639646983,
		road: 16662.0908796296,
		acceleration: 0.352222222222222
	},
	{
		id: 1800,
		time: 1799,
		velocity: 14.545,
		power: 7604.696801022,
		road: 16676.4238425926,
		acceleration: 0.312222222222223
	},
	{
		id: 1801,
		time: 1800,
		velocity: 14.6802777777778,
		power: 6681.43095914641,
		road: 16691.0285185185,
		acceleration: 0.231203703703706
	},
	{
		id: 1802,
		time: 1801,
		velocity: 14.9355555555556,
		power: 6509.14244370175,
		road: 16705.8529166667,
		acceleration: 0.208240740740738
	},
	{
		id: 1803,
		time: 1802,
		velocity: 15.1697222222222,
		power: 6409.36529838861,
		road: 16720.8773148148,
		acceleration: 0.191759259259261
	},
	{
		id: 1804,
		time: 1803,
		velocity: 15.2555555555556,
		power: 4897.21440149953,
		road: 16736.0381018519,
		acceleration: 0.081018518518519
	},
	{
		id: 1805,
		time: 1804,
		velocity: 15.1786111111111,
		power: 1683.24971485271,
		road: 16751.1693518519,
		acceleration: -0.140092592592593
	},
	{
		id: 1806,
		time: 1805,
		velocity: 14.7494444444444,
		power: -28.8129107823306,
		road: 16766.102962963,
		acceleration: -0.255185185185185
	},
	{
		id: 1807,
		time: 1806,
		velocity: 14.49,
		power: 171.286159721554,
		road: 16780.7905555556,
		acceleration: -0.236851851851853
	},
	{
		id: 1808,
		time: 1807,
		velocity: 14.4680555555556,
		power: 3047.62781380674,
		road: 16795.3458796296,
		acceleration: -0.0276851851851845
	},
	{
		id: 1809,
		time: 1808,
		velocity: 14.6663888888889,
		power: 6328.75783313144,
		road: 16809.9893981482,
		acceleration: 0.204074074074073
	},
	{
		id: 1810,
		time: 1809,
		velocity: 15.1022222222222,
		power: 9459.90000539773,
		road: 16824.9393518519,
		acceleration: 0.408796296296297
	},
	{
		id: 1811,
		time: 1810,
		velocity: 15.6944444444444,
		power: 9368.42593314927,
		road: 16840.2830555556,
		acceleration: 0.378703703703705
	},
	{
		id: 1812,
		time: 1811,
		velocity: 15.8025,
		power: 9040.20129821223,
		road: 16855.9840740741,
		acceleration: 0.335925925925926
	},
	{
		id: 1813,
		time: 1812,
		velocity: 16.11,
		power: 6990.94577712002,
		road: 16871.9465740741,
		acceleration: 0.187037037037038
	},
	{
		id: 1814,
		time: 1813,
		velocity: 16.2555555555556,
		power: 7562.29850679904,
		road: 16888.1099074074,
		acceleration: 0.214629629629631
	},
	{
		id: 1815,
		time: 1814,
		velocity: 16.4463888888889,
		power: 6872.37285006452,
		road: 16904.46125,
		acceleration: 0.161388888888887
	},
	{
		id: 1816,
		time: 1815,
		velocity: 16.5941666666667,
		power: 6284.47928358211,
		road: 16920.9521759259,
		acceleration: 0.117777777777775
	},
	{
		id: 1817,
		time: 1816,
		velocity: 16.6088888888889,
		power: 5669.89524206265,
		road: 16937.5394444444,
		acceleration: 0.0749074074074088
	},
	{
		id: 1818,
		time: 1817,
		velocity: 16.6711111111111,
		power: 5086.5322614572,
		road: 16954.1821759259,
		acceleration: 0.0360185185185173
	},
	{
		id: 1819,
		time: 1818,
		velocity: 16.7022222222222,
		power: 4693.29870147158,
		road: 16970.8481481482,
		acceleration: 0.0104629629629649
	},
	{
		id: 1820,
		time: 1819,
		velocity: 16.6402777777778,
		power: 3014.39411669981,
		road: 16987.4725462963,
		acceleration: -0.0936111111111089
	},
	{
		id: 1821,
		time: 1820,
		velocity: 16.3902777777778,
		power: 1220.81805547756,
		road: 17003.9486574074,
		acceleration: -0.20296296296296
	},
	{
		id: 1822,
		time: 1821,
		velocity: 16.0933333333333,
		power: -454.732991857782,
		road: 17020.1707407407,
		acceleration: -0.305092592592597
	},
	{
		id: 1823,
		time: 1822,
		velocity: 15.725,
		power: -2232.16621839804,
		road: 17036.032037037,
		acceleration: -0.416481481481481
	},
	{
		id: 1824,
		time: 1823,
		velocity: 15.1408333333333,
		power: -2714.43579492331,
		road: 17051.4622222222,
		acceleration: -0.445740740740741
	},
	{
		id: 1825,
		time: 1824,
		velocity: 14.7561111111111,
		power: -3637.97426810442,
		road: 17066.415462963,
		acceleration: -0.50814814814815
	},
	{
		id: 1826,
		time: 1825,
		velocity: 14.2005555555556,
		power: -2169.27582190615,
		road: 17080.9133333333,
		acceleration: -0.402592592592592
	},
	{
		id: 1827,
		time: 1826,
		velocity: 13.9330555555556,
		power: 630.302117000289,
		road: 17095.1125925926,
		acceleration: -0.194629629629629
	},
	{
		id: 1828,
		time: 1827,
		velocity: 14.1722222222222,
		power: 2643.97573108435,
		road: 17109.1932407407,
		acceleration: -0.0425925925925927
	},
	{
		id: 1829,
		time: 1828,
		velocity: 14.0727777777778,
		power: 2429.17109602155,
		road: 17123.2240277778,
		acceleration: -0.0571296296296282
	},
	{
		id: 1830,
		time: 1829,
		velocity: 13.7616666666667,
		power: -1776.29720890321,
		road: 17137.0413888889,
		acceleration: -0.369722222222224
	},
	{
		id: 1831,
		time: 1830,
		velocity: 13.0630555555556,
		power: -2584.63969532144,
		road: 17150.4585185185,
		acceleration: -0.430740740740742
	},
	{
		id: 1832,
		time: 1831,
		velocity: 12.7805555555556,
		power: -1177.73542599352,
		road: 17163.5012962963,
		acceleration: -0.317962962962961
	},
	{
		id: 1833,
		time: 1832,
		velocity: 12.8077777777778,
		power: 2067.73496687549,
		road: 17176.3590277778,
		acceleration: -0.052129629629631
	},
	{
		id: 1834,
		time: 1833,
		velocity: 12.9066666666667,
		power: 4940.60055828042,
		road: 17189.2802314815,
		acceleration: 0.179074074074073
	},
	{
		id: 1835,
		time: 1834,
		velocity: 13.3177777777778,
		power: 8387.76522684066,
		road: 17202.51,
		acceleration: 0.438055555555557
	},
	{
		id: 1836,
		time: 1835,
		velocity: 14.1219444444444,
		power: 11228.507375363,
		road: 17216.2690277778,
		acceleration: 0.620462962962964
	},
	{
		id: 1837,
		time: 1836,
		velocity: 14.7680555555556,
		power: 11415.010881461,
		road: 17230.6324074074,
		acceleration: 0.588240740740739
	},
	{
		id: 1838,
		time: 1837,
		velocity: 15.0825,
		power: 10762.7835555102,
		road: 17245.5412962963,
		acceleration: 0.50277777777778
	},
	{
		id: 1839,
		time: 1838,
		velocity: 15.6302777777778,
		power: 8814.76758475653,
		road: 17260.8724074074,
		acceleration: 0.341666666666663
	},
	{
		id: 1840,
		time: 1839,
		velocity: 15.7930555555556,
		power: 9118.02728953353,
		road: 17276.5456944445,
		acceleration: 0.342685185185186
	},
	{
		id: 1841,
		time: 1840,
		velocity: 16.1105555555556,
		power: 7237.91761977646,
		road: 17292.4923148148,
		acceleration: 0.203981481481483
	},
	{
		id: 1842,
		time: 1841,
		velocity: 16.2422222222222,
		power: 5371.21753339911,
		road: 17308.5788888889,
		acceleration: 0.0759259259259224
	},
	{
		id: 1843,
		time: 1842,
		velocity: 16.0208333333333,
		power: 3025.55569877581,
		road: 17324.6651388889,
		acceleration: -0.0765740740740739
	},
	{
		id: 1844,
		time: 1843,
		velocity: 15.8808333333333,
		power: 1045.26750800659,
		road: 17340.6119907407,
		acceleration: -0.20222222222222
	},
	{
		id: 1845,
		time: 1844,
		velocity: 15.6355555555556,
		power: -762.788623530286,
		road: 17356.299212963,
		acceleration: -0.317037037037036
	},
	{
		id: 1846,
		time: 1845,
		velocity: 15.0697222222222,
		power: -1010.73084941326,
		road: 17371.6632407407,
		acceleration: -0.329351851851852
	},
	{
		id: 1847,
		time: 1846,
		velocity: 14.8927777777778,
		power: -1089.96385642205,
		road: 17386.6972222222,
		acceleration: -0.330740740740739
	},
	{
		id: 1848,
		time: 1847,
		velocity: 14.6433333333333,
		power: 843.139882348823,
		road: 17401.470462963,
		acceleration: -0.190740740740742
	},
	{
		id: 1849,
		time: 1848,
		velocity: 14.4975,
		power: 2168.7651147462,
		road: 17416.1018518519,
		acceleration: -0.0929629629629645
	},
	{
		id: 1850,
		time: 1849,
		velocity: 14.6138888888889,
		power: 2385.34013133057,
		road: 17430.649212963,
		acceleration: -0.0750925925925916
	},
	{
		id: 1851,
		time: 1850,
		velocity: 14.4180555555556,
		power: 1488.15049741275,
		road: 17445.090462963,
		acceleration: -0.13712962962963
	},
	{
		id: 1852,
		time: 1851,
		velocity: 14.0861111111111,
		power: 312.865022898218,
		road: 17459.3535648148,
		acceleration: -0.219166666666668
	},
	{
		id: 1853,
		time: 1852,
		velocity: 13.9563888888889,
		power: -141.732470571863,
		road: 17473.3825925926,
		acceleration: -0.248981481481481
	},
	{
		id: 1854,
		time: 1853,
		velocity: 13.6711111111111,
		power: 618.664865283399,
		road: 17487.1931018519,
		acceleration: -0.188055555555556
	},
	{
		id: 1855,
		time: 1854,
		velocity: 13.5219444444444,
		power: 922.740239532656,
		road: 17500.8288888889,
		acceleration: -0.161388888888888
	},
	{
		id: 1856,
		time: 1855,
		velocity: 13.4722222222222,
		power: 2571.37957371858,
		road: 17514.3680555556,
		acceleration: -0.0318518518518527
	},
	{
		id: 1857,
		time: 1856,
		velocity: 13.5755555555556,
		power: 3587.32136401076,
		road: 17527.914537037,
		acceleration: 0.0464814814814805
	},
	{
		id: 1858,
		time: 1857,
		velocity: 13.6613888888889,
		power: 4475.89586431096,
		road: 17541.5402314815,
		acceleration: 0.111944444444447
	},
	{
		id: 1859,
		time: 1858,
		velocity: 13.8080555555556,
		power: 4974.87571798991,
		road: 17555.2942592593,
		acceleration: 0.144722222222221
	},
	{
		id: 1860,
		time: 1859,
		velocity: 14.0097222222222,
		power: 5283.71196096491,
		road: 17569.2013425926,
		acceleration: 0.16138888888889
	},
	{
		id: 1861,
		time: 1860,
		velocity: 14.1455555555556,
		power: 5059.48431470593,
		road: 17583.2581481482,
		acceleration: 0.138055555555555
	},
	{
		id: 1862,
		time: 1861,
		velocity: 14.2222222222222,
		power: 3861.2903466968,
		road: 17597.406712963,
		acceleration: 0.0454629629629633
	},
	{
		id: 1863,
		time: 1862,
		velocity: 14.1461111111111,
		power: 3542.70365325951,
		road: 17611.5883796296,
		acceleration: 0.0207407407407416
	},
	{
		id: 1864,
		time: 1863,
		velocity: 14.2077777777778,
		power: 3112.12041293517,
		road: 17625.7748148148,
		acceleration: -0.0112037037037034
	},
	{
		id: 1865,
		time: 1864,
		velocity: 14.1886111111111,
		power: 2914.97286842558,
		road: 17639.9430555556,
		acceleration: -0.0251851851851868
	},
	{
		id: 1866,
		time: 1865,
		velocity: 14.0705555555556,
		power: 2513.49252333438,
		road: 17654.0718518519,
		acceleration: -0.0537037037037038
	},
	{
		id: 1867,
		time: 1866,
		velocity: 14.0466666666667,
		power: 2414.11399550044,
		road: 17668.1440740741,
		acceleration: -0.0594444444444449
	},
	{
		id: 1868,
		time: 1867,
		velocity: 14.0102777777778,
		power: 2899.67014996023,
		road: 17682.1755555556,
		acceleration: -0.0220370370370375
	},
	{
		id: 1869,
		time: 1868,
		velocity: 14.0044444444444,
		power: 3176.23977089828,
		road: 17696.1955092593,
		acceleration: -0.00101851851851542
	},
	{
		id: 1870,
		time: 1869,
		velocity: 14.0436111111111,
		power: 3585.72654782893,
		road: 17710.2294907407,
		acceleration: 0.0290740740740727
	},
	{
		id: 1871,
		time: 1870,
		velocity: 14.0975,
		power: 2336.97950840348,
		road: 17724.2462037037,
		acceleration: -0.0636111111111113
	},
	{
		id: 1872,
		time: 1871,
		velocity: 13.8136111111111,
		power: 2812.80689569806,
		road: 17738.2177777778,
		acceleration: -0.0266666666666673
	},
	{
		id: 1873,
		time: 1872,
		velocity: 13.9636111111111,
		power: 2849.39274983116,
		road: 17752.1644444444,
		acceleration: -0.0231481481481488
	},
	{
		id: 1874,
		time: 1873,
		velocity: 14.0280555555556,
		power: 4447.01199394168,
		road: 17766.1472222222,
		acceleration: 0.0953703703703717
	},
	{
		id: 1875,
		time: 1874,
		velocity: 14.0997222222222,
		power: 3422.90451026351,
		road: 17780.1860648148,
		acceleration: 0.0167592592592598
	},
	{
		id: 1876,
		time: 1875,
		velocity: 14.0138888888889,
		power: 3563.3643615432,
		road: 17794.2465277778,
		acceleration: 0.0264814814814827
	},
	{
		id: 1877,
		time: 1876,
		velocity: 14.1075,
		power: 3352.84567856655,
		road: 17808.3253240741,
		acceleration: 0.0101851851851826
	},
	{
		id: 1878,
		time: 1877,
		velocity: 14.1302777777778,
		power: 2797.06528556419,
		road: 17822.3937962963,
		acceleration: -0.0308333333333337
	},
	{
		id: 1879,
		time: 1878,
		velocity: 13.9213888888889,
		power: 2756.61272057176,
		road: 17836.4304166667,
		acceleration: -0.0328703703703699
	},
	{
		id: 1880,
		time: 1879,
		velocity: 14.0088888888889,
		power: 2376.42542995234,
		road: 17850.4206481482,
		acceleration: -0.0599074074074082
	},
	{
		id: 1881,
		time: 1880,
		velocity: 13.9505555555556,
		power: 3467.51994364377,
		road: 17864.3921296296,
		acceleration: 0.0224074074074085
	},
	{
		id: 1882,
		time: 1881,
		velocity: 13.9886111111111,
		power: 3736.06378902222,
		road: 17878.3955092593,
		acceleration: 0.0413888888888891
	},
	{
		id: 1883,
		time: 1882,
		velocity: 14.1330555555556,
		power: 3672.54898971005,
		road: 17892.4372222222,
		acceleration: 0.0352777777777789
	},
	{
		id: 1884,
		time: 1883,
		velocity: 14.0563888888889,
		power: 3353.93072551977,
		road: 17906.5019444444,
		acceleration: 0.0107407407407401
	},
	{
		id: 1885,
		time: 1884,
		velocity: 14.0208333333333,
		power: 2574.44282863306,
		road: 17920.5486574074,
		acceleration: -0.0467592592592592
	},
	{
		id: 1886,
		time: 1885,
		velocity: 13.9927777777778,
		power: 2992.32477729485,
		road: 17934.5646759259,
		acceleration: -0.0146296296296295
	},
	{
		id: 1887,
		time: 1886,
		velocity: 14.0125,
		power: 2970.99501307759,
		road: 17948.5655092593,
		acceleration: -0.0157407407407408
	},
	{
		id: 1888,
		time: 1887,
		velocity: 13.9736111111111,
		power: 3831.84219490591,
		road: 17962.5825,
		acceleration: 0.0480555555555551
	},
	{
		id: 1889,
		time: 1888,
		velocity: 14.1369444444444,
		power: 3410.0518544213,
		road: 17976.63125,
		acceleration: 0.0154629629629639
	},
	{
		id: 1890,
		time: 1889,
		velocity: 14.0588888888889,
		power: 3418.39088432323,
		road: 17990.6955092593,
		acceleration: 0.0155555555555544
	},
	{
		id: 1891,
		time: 1890,
		velocity: 14.0202777777778,
		power: 2989.15379970469,
		road: 18004.7593518519,
		acceleration: -0.0163888888888906
	},
	{
		id: 1892,
		time: 1891,
		velocity: 14.0877777777778,
		power: 3047.78863776735,
		road: 18018.809212963,
		acceleration: -0.0115740740740726
	},
	{
		id: 1893,
		time: 1892,
		velocity: 14.0241666666667,
		power: 4583.97248749435,
		road: 18032.9038425926,
		acceleration: 0.101111111111111
	},
	{
		id: 1894,
		time: 1893,
		velocity: 14.3236111111111,
		power: 8672.97901813616,
		road: 18047.2439351852,
		acceleration: 0.389814814814814
	},
	{
		id: 1895,
		time: 1894,
		velocity: 15.2572222222222,
		power: 4283.71402501437,
		road: 18061.8093518519,
		acceleration: 0.0608333333333331
	},
	{
		id: 1896,
		time: 1895,
		velocity: 14.2066666666667,
		power: 2165.14884054398,
		road: 18076.3596759259,
		acceleration: -0.0910185185185188
	},
	{
		id: 1897,
		time: 1896,
		velocity: 14.0505555555556,
		power: -2504.83095418097,
		road: 18090.6514351852,
		acceleration: -0.42611111111111
	},
	{
		id: 1898,
		time: 1897,
		velocity: 13.9788888888889,
		power: 2870.69805958969,
		road: 18104.7175,
		acceleration: -0.0252777777777755
	},
	{
		id: 1899,
		time: 1898,
		velocity: 14.1308333333333,
		power: 3003.92335475879,
		road: 18118.7635648148,
		acceleration: -0.0147222222222236
	},
	{
		id: 1900,
		time: 1899,
		velocity: 14.0063888888889,
		power: 3258.72933412942,
		road: 18132.8044907407,
		acceleration: 0.00444444444444514
	},
	{
		id: 1901,
		time: 1900,
		velocity: 13.9922222222222,
		power: 3157.20503827459,
		road: 18146.8460648148,
		acceleration: -0.00314814814814746
	},
	{
		id: 1902,
		time: 1901,
		velocity: 14.1213888888889,
		power: 3197.46285737903,
		road: 18160.8860185185,
		acceleration: -9.25925925940874E-05
	},
	{
		id: 1903,
		time: 1902,
		velocity: 14.0061111111111,
		power: 3433.54883340107,
		road: 18174.934537037,
		acceleration: 0.0172222222222214
	},
	{
		id: 1904,
		time: 1903,
		velocity: 14.0438888888889,
		power: 3001.72221486925,
		road: 18188.9841666667,
		acceleration: -0.0150000000000006
	},
	{
		id: 1905,
		time: 1904,
		velocity: 14.0763888888889,
		power: 3549.5997103395,
		road: 18203.0391203704,
		acceleration: 0.0256481481481501
	},
	{
		id: 1906,
		time: 1905,
		velocity: 14.0830555555556,
		power: 2978.49404423951,
		road: 18217.0983796296,
		acceleration: -0.0170370370370367
	},
	{
		id: 1907,
		time: 1906,
		velocity: 13.9927777777778,
		power: 4189.83466994205,
		road: 18231.1851851852,
		acceleration: 0.0721296296296288
	},
	{
		id: 1908,
		time: 1907,
		velocity: 14.2927777777778,
		power: 7409.57509022949,
		road: 18245.4586574074,
		acceleration: 0.301203703703706
	},
	{
		id: 1909,
		time: 1908,
		velocity: 14.9866666666667,
		power: 10451.0696784735,
		road: 18260.1311111111,
		acceleration: 0.496759259259257
	},
	{
		id: 1910,
		time: 1909,
		velocity: 15.4830555555556,
		power: 10247.3825420872,
		road: 18275.2775462963,
		acceleration: 0.451203703703705
	},
	{
		id: 1911,
		time: 1910,
		velocity: 15.6463888888889,
		power: 10330.1439000594,
		road: 18290.8640740741,
		acceleration: 0.428981481481481
	},
	{
		id: 1912,
		time: 1911,
		velocity: 16.2736111111111,
		power: 11073.3401372665,
		road: 18306.8903240741,
		acceleration: 0.450462962962963
	},
	{
		id: 1913,
		time: 1912,
		velocity: 16.8344444444444,
		power: 11532.4196625385,
		road: 18323.3675,
		acceleration: 0.451388888888886
	},
	{
		id: 1914,
		time: 1913,
		velocity: 17.0005555555556,
		power: 9828.27629720643,
		road: 18340.2311574074,
		acceleration: 0.321574074074078
	},
	{
		id: 1915,
		time: 1914,
		velocity: 17.2383333333333,
		power: 7869.7285659092,
		road: 18357.3495833333,
		acceleration: 0.187962962962963
	},
	{
		id: 1916,
		time: 1915,
		velocity: 17.3983333333333,
		power: 8474.44004147738,
		road: 18374.6694444444,
		acceleration: 0.214907407407409
	},
	{
		id: 1917,
		time: 1916,
		velocity: 17.6452777777778,
		power: 8707.0627922804,
		road: 18392.2058333333,
		acceleration: 0.218148148148146
	},
	{
		id: 1918,
		time: 1917,
		velocity: 17.8927777777778,
		power: 8181.36427035025,
		road: 18409.94,
		acceleration: 0.177407407407408
	},
	{
		id: 1919,
		time: 1918,
		velocity: 17.9305555555556,
		power: 8412.77384783695,
		road: 18427.8540740741,
		acceleration: 0.182407407407407
	},
	{
		id: 1920,
		time: 1919,
		velocity: 18.1925,
		power: 6794.42729709563,
		road: 18445.9005555556,
		acceleration: 0.0824074074074055
	},
	{
		id: 1921,
		time: 1920,
		velocity: 18.14,
		power: 4380.27082705014,
		road: 18463.9593518519,
		acceleration: -0.0577777777777762
	},
	{
		id: 1922,
		time: 1921,
		velocity: 17.7572222222222,
		power: 1003.37402332054,
		road: 18481.8644907407,
		acceleration: -0.249537037037037
	},
	{
		id: 1923,
		time: 1922,
		velocity: 17.4438888888889,
		power: -644.737003363229,
		road: 18499.4746296296,
		acceleration: -0.340462962962963
	},
	{
		id: 1924,
		time: 1923,
		velocity: 17.1186111111111,
		power: -2941.30812122051,
		road: 18516.6780555556,
		acceleration: -0.47296296296296
	},
	{
		id: 1925,
		time: 1924,
		velocity: 16.3383333333333,
		power: -2758.70987467386,
		road: 18533.4161574074,
		acceleration: -0.457685185185191
	},
	{
		id: 1926,
		time: 1925,
		velocity: 16.0708333333333,
		power: -4021.21239359834,
		road: 18549.6578703704,
		acceleration: -0.535092592592589
	},
	{
		id: 1927,
		time: 1926,
		velocity: 15.5133333333333,
		power: -1812.30007780666,
		road: 18565.4380555556,
		acceleration: -0.387962962962964
	},
	{
		id: 1928,
		time: 1927,
		velocity: 15.1744444444444,
		power: -2979.74725824845,
		road: 18580.7925462963,
		acceleration: -0.463425925925927
	},
	{
		id: 1929,
		time: 1928,
		velocity: 14.6805555555556,
		power: -1723.46075108318,
		road: 18595.7283333333,
		acceleration: -0.373981481481479
	},
	{
		id: 1930,
		time: 1929,
		velocity: 14.3913888888889,
		power: -2768.36355314816,
		road: 18610.2541666667,
		acceleration: -0.445925925925927
	},
	{
		id: 1931,
		time: 1930,
		velocity: 13.8366666666667,
		power: -2798.04217403787,
		road: 18624.3333796296,
		acceleration: -0.447314814814815
	},
	{
		id: 1932,
		time: 1931,
		velocity: 13.3386111111111,
		power: -3221.71157061142,
		road: 18637.9490277778,
		acceleration: -0.479814814814814
	},
	{
		id: 1933,
		time: 1932,
		velocity: 12.9519444444444,
		power: -2345.8260120114,
		road: 18651.1188425926,
		acceleration: -0.411851851851852
	},
	{
		id: 1934,
		time: 1933,
		velocity: 12.6011111111111,
		power: -1585.08206179461,
		road: 18663.907962963,
		acceleration: -0.349537037037038
	},
	{
		id: 1935,
		time: 1934,
		velocity: 12.29,
		power: -2114.64011849175,
		road: 18676.3258796296,
		acceleration: -0.392870370370369
	},
	{
		id: 1936,
		time: 1935,
		velocity: 11.7733333333333,
		power: -4919.84021872649,
		road: 18688.2269907407,
		acceleration: -0.640740740740739
	},
	{
		id: 1937,
		time: 1936,
		velocity: 10.6788888888889,
		power: -10410.0777543934,
		road: 18699.2129166667,
		acceleration: -1.18962962962963
	},
	{
		id: 1938,
		time: 1937,
		velocity: 8.72111111111111,
		power: -8404.56121713868,
		road: 18709.0651851852,
		acceleration: -1.07768518518519
	},
	{
		id: 1939,
		time: 1938,
		velocity: 8.54027777777778,
		power: -6624.43471207949,
		road: 18717.8990740741,
		acceleration: -0.959074074074074
	},
	{
		id: 1940,
		time: 1939,
		velocity: 7.80166666666667,
		power: -3812.65403284487,
		road: 18725.9218981481,
		acceleration: -0.663055555555556
	},
	{
		id: 1941,
		time: 1940,
		velocity: 6.73194444444444,
		power: -5711.08228180131,
		road: 18733.1176851852,
		acceleration: -0.991018518518518
	},
	{
		id: 1942,
		time: 1941,
		velocity: 5.56722222222222,
		power: -5270.95481250472,
		road: 18739.2946759259,
		acceleration: -1.04657407407407
	},
	{
		id: 1943,
		time: 1942,
		velocity: 4.66194444444444,
		power: -3318.89524253891,
		road: 18744.544212963,
		acceleration: -0.808333333333334
	},
	{
		id: 1944,
		time: 1943,
		velocity: 4.30694444444444,
		power: -2146.82637053355,
		road: 18749.0704166667,
		acceleration: -0.638333333333334
	},
	{
		id: 1945,
		time: 1944,
		velocity: 3.65222222222222,
		power: -1934.36630160839,
		road: 18752.946712963,
		acceleration: -0.661481481481482
	},
	{
		id: 1946,
		time: 1945,
		velocity: 2.6775,
		power: -3497.48851333335,
		road: 18755.7744444444,
		acceleration: -1.43564814814815
	},
	{
		id: 1947,
		time: 1946,
		velocity: 0,
		power: -1549.36683967242,
		road: 18757.2756481481,
		acceleration: -1.21740740740741
	},
	{
		id: 1948,
		time: 1947,
		velocity: 0,
		power: -323.401132894737,
		road: 18757.7218981481,
		acceleration: -0.8925
	},
	{
		id: 1949,
		time: 1948,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1950,
		time: 1949,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1951,
		time: 1950,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1952,
		time: 1951,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1953,
		time: 1952,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1954,
		time: 1953,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1955,
		time: 1954,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1956,
		time: 1955,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1957,
		time: 1956,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1958,
		time: 1957,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1959,
		time: 1958,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1960,
		time: 1959,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1961,
		time: 1960,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1962,
		time: 1961,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1963,
		time: 1962,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1964,
		time: 1963,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1965,
		time: 1964,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1966,
		time: 1965,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1967,
		time: 1966,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1968,
		time: 1967,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1969,
		time: 1968,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1970,
		time: 1969,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1971,
		time: 1970,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1972,
		time: 1971,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1973,
		time: 1972,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1974,
		time: 1973,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1975,
		time: 1974,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1976,
		time: 1975,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1977,
		time: 1976,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1978,
		time: 1977,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1979,
		time: 1978,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1980,
		time: 1979,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1981,
		time: 1980,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1982,
		time: 1981,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1983,
		time: 1982,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1984,
		time: 1983,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1985,
		time: 1984,
		velocity: 0,
		power: 0,
		road: 18757.7218981481,
		acceleration: 0
	},
	{
		id: 1986,
		time: 1985,
		velocity: 0,
		power: 197.557094017665,
		road: 18758.0144444444,
		acceleration: 0.585092592592593
	},
	{
		id: 1987,
		time: 1986,
		velocity: 1.75527777777778,
		power: 1321.4678804767,
		road: 18759.1494444444,
		acceleration: 1.09981481481481
	},
	{
		id: 1988,
		time: 1987,
		velocity: 3.29944444444444,
		power: 3649.73171615999,
		road: 18761.5651388889,
		acceleration: 1.46157407407407
	},
	{
		id: 1989,
		time: 1988,
		velocity: 4.38472222222222,
		power: 4411.86221743351,
		road: 18765.2709722222,
		acceleration: 1.1187037037037
	},
	{
		id: 1990,
		time: 1989,
		velocity: 5.11138888888889,
		power: 3667.08615252845,
		road: 18769.8848148148,
		acceleration: 0.697314814814814
	},
	{
		id: 1991,
		time: 1990,
		velocity: 5.39138888888889,
		power: 3320.83501754327,
		road: 18775.1103240741,
		acceleration: 0.526018518518519
	},
	{
		id: 1992,
		time: 1991,
		velocity: 5.96277777777778,
		power: 3212.09519945976,
		road: 18780.8218055556,
		acceleration: 0.445925925925926
	},
	{
		id: 1993,
		time: 1992,
		velocity: 6.44916666666667,
		power: 2606.13419010623,
		road: 18786.9073611111,
		acceleration: 0.302222222222221
	},
	{
		id: 1994,
		time: 1993,
		velocity: 6.29805555555556,
		power: 436.823190800813,
		road: 18793.1065740741,
		acceleration: -0.0749074074074061
	},
	{
		id: 1995,
		time: 1994,
		velocity: 5.73805555555556,
		power: -356.737869772644,
		road: 18799.1633333333,
		acceleration: -0.21
	},
	{
		id: 1996,
		time: 1995,
		velocity: 5.81916666666667,
		power: 198.294750456562,
		road: 18805.0593055556,
		acceleration: -0.111574074074075
	},
	{
		id: 1997,
		time: 1996,
		velocity: 5.96333333333333,
		power: 262.799345915228,
		road: 18810.8502314815,
		acceleration: -0.0985185185185173
	},
	{
		id: 1998,
		time: 1997,
		velocity: 5.4425,
		power: -301.20637411895,
		road: 18816.4911574074,
		acceleration: -0.201481481481482
	},
	{
		id: 1999,
		time: 1998,
		velocity: 5.21472222222222,
		power: -2998.75974908269,
		road: 18821.6544907407,
		acceleration: -0.753703703703704
	},
	{
		id: 2000,
		time: 1999,
		velocity: 3.70222222222222,
		power: -3196.73178886773,
		road: 18825.9831481481,
		acceleration: -0.915648148148147
	},
	{
		id: 2001,
		time: 2000,
		velocity: 2.69555555555556,
		power: -3555.10219324423,
		road: 18829.2061574074,
		acceleration: -1.29564814814815
	},
	{
		id: 2002,
		time: 2001,
		velocity: 1.32777777777778,
		power: -1545.4168713664,
		road: 18831.3333333333,
		acceleration: -0.896018518518519
	},
	{
		id: 2003,
		time: 2002,
		velocity: 1.01416666666667,
		power: -897.929993033037,
		road: 18832.5632407407,
		acceleration: -0.898518518518518
	},
	{
		id: 2004,
		time: 2003,
		velocity: 0,
		power: -166.92083476538,
		road: 18833.1225925926,
		acceleration: -0.442592592592593
	},
	{
		id: 2005,
		time: 2004,
		velocity: 0,
		power: -33.711789619883,
		road: 18833.2916203704,
		acceleration: -0.338055555555556
	},
	{
		id: 2006,
		time: 2005,
		velocity: 0,
		power: 0,
		road: 18833.2916203704,
		acceleration: 0
	},
	{
		id: 2007,
		time: 2006,
		velocity: 0,
		power: 0,
		road: 18833.2916203704,
		acceleration: 0
	},
	{
		id: 2008,
		time: 2007,
		velocity: 0,
		power: 69.8116261366708,
		road: 18833.4543055556,
		acceleration: 0.32537037037037
	},
	{
		id: 2009,
		time: 2008,
		velocity: 0.976111111111111,
		power: 698.734787299436,
		road: 18834.2060648148,
		acceleration: 0.852777777777778
	},
	{
		id: 2010,
		time: 2009,
		velocity: 2.55833333333333,
		power: 1591.37464346588,
		road: 18835.8348611111,
		acceleration: 0.901296296296296
	},
	{
		id: 2011,
		time: 2010,
		velocity: 2.70388888888889,
		power: 1202.09918167948,
		road: 18838.1256944444,
		acceleration: 0.422777777777778
	},
	{
		id: 2012,
		time: 2011,
		velocity: 2.24444444444444,
		power: 287.596583570867,
		road: 18840.6231481481,
		acceleration: -0.00953703703703734
	},
	{
		id: 2013,
		time: 2012,
		velocity: 2.52972222222222,
		power: 311.594698025449,
		road: 18843.11625,
		acceleration: 0.000833333333333464
	},
	{
		id: 2014,
		time: 2013,
		velocity: 2.70638888888889,
		power: 721.149520302391,
		road: 18845.6917592593,
		acceleration: 0.163981481481482
	},
	{
		id: 2015,
		time: 2014,
		velocity: 2.73638888888889,
		power: 80.034916443169,
		road: 18848.2998148148,
		acceleration: -0.0988888888888892
	},
	{
		id: 2016,
		time: 2015,
		velocity: 2.23305555555556,
		power: -124.364968528074,
		road: 18850.7664351852,
		acceleration: -0.183981481481482
	},
	{
		id: 2017,
		time: 2016,
		velocity: 2.15444444444444,
		power: 30.4536383889434,
		road: 18853.0827777778,
		acceleration: -0.116574074074074
	},
	{
		id: 2018,
		time: 2017,
		velocity: 2.38666666666667,
		power: 775.258605299071,
		road: 18855.448287037,
		acceleration: 0.214907407407408
	},
	{
		id: 2019,
		time: 2018,
		velocity: 2.87777777777778,
		power: 1318.40524687379,
		road: 18858.1159722222,
		acceleration: 0.389444444444444
	},
	{
		id: 2020,
		time: 2019,
		velocity: 3.32277777777778,
		power: 1606.36179816187,
		road: 18861.1875462963,
		acceleration: 0.418333333333334
	},
	{
		id: 2021,
		time: 2020,
		velocity: 3.64166666666667,
		power: 1199.71112033764,
		road: 18864.5872222222,
		acceleration: 0.23787037037037
	},
	{
		id: 2022,
		time: 2021,
		velocity: 3.59138888888889,
		power: 733.387403763258,
		road: 18868.1470833333,
		acceleration: 0.0825000000000005
	},
	{
		id: 2023,
		time: 2022,
		velocity: 3.57027777777778,
		power: 249.278959487512,
		road: 18871.7176851852,
		acceleration: -0.0610185185185186
	},
	{
		id: 2024,
		time: 2023,
		velocity: 3.45861111111111,
		power: 412.142301484293,
		road: 18875.2519907407,
		acceleration: -0.0115740740740744
	},
	{
		id: 2025,
		time: 2024,
		velocity: 3.55666666666667,
		power: 252.898623129485,
		road: 18878.7514351852,
		acceleration: -0.0581481481481481
	},
	{
		id: 2026,
		time: 2025,
		velocity: 3.39583333333333,
		power: 214.295950459306,
		road: 18882.1876388889,
		acceleration: -0.0683333333333338
	},
	{
		id: 2027,
		time: 2026,
		velocity: 3.25361111111111,
		power: -400.157248296088,
		road: 18885.4585185185,
		acceleration: -0.262314814814815
	},
	{
		id: 2028,
		time: 2027,
		velocity: 2.76972222222222,
		power: -1627.42266908061,
		road: 18888.222037037,
		acceleration: -0.752407407407407
	},
	{
		id: 2029,
		time: 2028,
		velocity: 1.13861111111111,
		power: -1671.08955557174,
		road: 18890.0670833333,
		acceleration: -1.08453703703704
	},
	{
		id: 2030,
		time: 2029,
		velocity: 0,
		power: -634.024943244744,
		road: 18890.9082407407,
		acceleration: -0.923240740740741
	},
	{
		id: 2031,
		time: 2030,
		velocity: 0,
		power: -45.3060019655621,
		road: 18891.0980092593,
		acceleration: -0.379537037037037
	},
	{
		id: 2032,
		time: 2031,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2033,
		time: 2032,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2034,
		time: 2033,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2035,
		time: 2034,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2036,
		time: 2035,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2037,
		time: 2036,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2038,
		time: 2037,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2039,
		time: 2038,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2040,
		time: 2039,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2041,
		time: 2040,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2042,
		time: 2041,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2043,
		time: 2042,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2044,
		time: 2043,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2045,
		time: 2044,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2046,
		time: 2045,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2047,
		time: 2046,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2048,
		time: 2047,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2049,
		time: 2048,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2050,
		time: 2049,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2051,
		time: 2050,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2052,
		time: 2051,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2053,
		time: 2052,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2054,
		time: 2053,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2055,
		time: 2054,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2056,
		time: 2055,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2057,
		time: 2056,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2058,
		time: 2057,
		velocity: 0,
		power: 0,
		road: 18891.0980092593,
		acceleration: 0
	},
	{
		id: 2059,
		time: 2058,
		velocity: 0,
		power: 273.078710321099,
		road: 18891.447037037,
		acceleration: 0.698055555555556
	},
	{
		id: 2060,
		time: 2059,
		velocity: 2.09416666666667,
		power: 2413.83148586711,
		road: 18892.9356481481,
		acceleration: 1.58111111111111
	},
	{
		id: 2061,
		time: 2060,
		velocity: 4.74333333333333,
		power: 5942.20567108725,
		road: 18896.1284722222,
		acceleration: 1.82731481481481
	},
	{
		id: 2062,
		time: 2061,
		velocity: 5.48194444444444,
		power: 5603.07106992619,
		road: 18900.7968055556,
		acceleration: 1.1237037037037
	},
	{
		id: 2063,
		time: 2062,
		velocity: 5.46527777777778,
		power: 1558.20441326213,
		road: 18906.1096759259,
		acceleration: 0.165370370370371
	},
	{
		id: 2064,
		time: 2063,
		velocity: 5.23944444444444,
		power: -246.897533426112,
		road: 18911.4091203704,
		acceleration: -0.192222222222223
	},
	{
		id: 2065,
		time: 2064,
		velocity: 4.90527777777778,
		power: -207.490016209915,
		road: 18916.5200462963,
		acceleration: -0.184814814814815
	},
	{
		id: 2066,
		time: 2065,
		velocity: 4.91083333333333,
		power: 941.724007884344,
		road: 18921.5659259259,
		acceleration: 0.0547222222222219
	},
	{
		id: 2067,
		time: 2066,
		velocity: 5.40361111111111,
		power: 4269.42136390606,
		road: 18926.9819444444,
		acceleration: 0.685555555555556
	},
	{
		id: 2068,
		time: 2067,
		velocity: 6.96194444444444,
		power: 9937.39911912252,
		road: 18933.4703703704,
		acceleration: 1.45925925925926
	},
	{
		id: 2069,
		time: 2068,
		velocity: 9.28861111111111,
		power: 12886.9317455704,
		road: 18941.4545833333,
		acceleration: 1.53231481481482
	},
	{
		id: 2070,
		time: 2069,
		velocity: 10.0005555555556,
		power: 13348.1760916391,
		road: 18950.8611111111,
		acceleration: 1.31231481481481
	},
	{
		id: 2071,
		time: 2070,
		velocity: 10.8988888888889,
		power: 7006.48965707803,
		road: 18961.1860648148,
		acceleration: 0.524537037037039
	},
	{
		id: 2072,
		time: 2071,
		velocity: 10.8622222222222,
		power: 5909.17887815217,
		road: 18971.9643981481,
		acceleration: 0.382222222222222
	},
	{
		id: 2073,
		time: 2072,
		velocity: 11.1472222222222,
		power: 4118.40793192757,
		road: 18983.030787037,
		acceleration: 0.193888888888889
	},
	{
		id: 2074,
		time: 2073,
		velocity: 11.4805555555556,
		power: 5116.26951618554,
		road: 18994.3318055556,
		acceleration: 0.275370370370371
	},
	{
		id: 2075,
		time: 2074,
		velocity: 11.6883333333333,
		power: 3877.03242965302,
		road: 19005.8459722222,
		acceleration: 0.150925925925925
	},
	{
		id: 2076,
		time: 2075,
		velocity: 11.6,
		power: 1387.80611982063,
		road: 19017.3972685185,
		acceleration: -0.0766666666666644
	},
	{
		id: 2077,
		time: 2076,
		velocity: 11.2505555555556,
		power: 401.47172187657,
		road: 19028.8281018518,
		acceleration: -0.164259259259261
	},
	{
		id: 2078,
		time: 2077,
		velocity: 11.1955555555556,
		power: 2114.76811551245,
		road: 19040.174537037,
		acceleration: -0.00453703703703745
	},
	{
		id: 2079,
		time: 2078,
		velocity: 11.5863888888889,
		power: 5521.33867702399,
		road: 19051.6695833333,
		acceleration: 0.301759259259262
	},
	{
		id: 2080,
		time: 2079,
		velocity: 12.1558333333333,
		power: 7032.75559208747,
		road: 19063.5231944444,
		acceleration: 0.415370370370368
	},
	{
		id: 2081,
		time: 2080,
		velocity: 12.4416666666667,
		power: 7502.74973393503,
		road: 19075.7986111111,
		acceleration: 0.42824074074074
	},
	{
		id: 2082,
		time: 2081,
		velocity: 12.8711111111111,
		power: 5390.06108824938,
		road: 19088.4037037037,
		acceleration: 0.231111111111112
	},
	{
		id: 2083,
		time: 2082,
		velocity: 12.8491666666667,
		power: 3569.26717090221,
		road: 19101.1613425926,
		acceleration: 0.0739814814814821
	},
	{
		id: 2084,
		time: 2083,
		velocity: 12.6636111111111,
		power: 292.843315698844,
		road: 19113.8588888889,
		acceleration: -0.194166666666671
	},
	{
		id: 2085,
		time: 2084,
		velocity: 12.2886111111111,
		power: -1669.1377274166,
		road: 19126.281712963,
		acceleration: -0.355277777777776
	},
	{
		id: 2086,
		time: 2085,
		velocity: 11.7833333333333,
		power: -1361.51301332788,
		road: 19138.362962963,
		acceleration: -0.32787037037037
	},
	{
		id: 2087,
		time: 2086,
		velocity: 11.68,
		power: 290.31239048816,
		road: 19150.19,
		acceleration: -0.180555555555555
	},
	{
		id: 2088,
		time: 2087,
		velocity: 11.7469444444444,
		power: 2768.03195778093,
		road: 19161.9475,
		acceleration: 0.0414814814814832
	},
	{
		id: 2089,
		time: 2088,
		velocity: 11.9077777777778,
		power: 3724.31668634526,
		road: 19173.7874074074,
		acceleration: 0.123333333333333
	},
	{
		id: 2090,
		time: 2089,
		velocity: 12.05,
		power: 4844.30720845253,
		road: 19185.7960648148,
		acceleration: 0.214166666666666
	},
	{
		id: 2091,
		time: 2090,
		velocity: 12.3894444444444,
		power: 4778.62391300325,
		road: 19198.0110648148,
		acceleration: 0.198518518518519
	},
	{
		id: 2092,
		time: 2091,
		velocity: 12.5033333333333,
		power: 4457.85804955515,
		road: 19210.4067592592,
		acceleration: 0.162870370370371
	},
	{
		id: 2093,
		time: 2092,
		velocity: 12.5386111111111,
		power: 3333.11081904047,
		road: 19222.9156944444,
		acceleration: 0.0636111111111095
	},
	{
		id: 2094,
		time: 2093,
		velocity: 12.5802777777778,
		power: 2517.20586059108,
		road: 19235.4536111111,
		acceleration: -0.00564814814814696
	},
	{
		id: 2095,
		time: 2094,
		velocity: 12.4863888888889,
		power: 2820.12762949177,
		road: 19247.9984259259,
		acceleration: 0.0194444444444439
	},
	{
		id: 2096,
		time: 2095,
		velocity: 12.5969444444444,
		power: 4677.13227659719,
		road: 19260.6381018518,
		acceleration: 0.170277777777779
	},
	{
		id: 2097,
		time: 2096,
		velocity: 13.0911111111111,
		power: 6235.60092669699,
		road: 19273.5063888889,
		acceleration: 0.286944444444444
	},
	{
		id: 2098,
		time: 2097,
		velocity: 13.3472222222222,
		power: 6507.39639668243,
		road: 19286.6646759259,
		acceleration: 0.293055555555554
	},
	{
		id: 2099,
		time: 2098,
		velocity: 13.4761111111111,
		power: 4281.38580129728,
		road: 19300.0233333333,
		acceleration: 0.107685185185186
	},
	{
		id: 2100,
		time: 2099,
		velocity: 13.4141666666667,
		power: 1421.39930546212,
		road: 19313.3775925926,
		acceleration: -0.116481481481483
	},
	{
		id: 2101,
		time: 2100,
		velocity: 12.9977777777778,
		power: -377.973974272247,
		road: 19326.5459722222,
		acceleration: -0.255277777777778
	},
	{
		id: 2102,
		time: 2101,
		velocity: 12.7102777777778,
		power: -421.454815456386,
		road: 19339.4588888889,
		acceleration: -0.255648148148149
	},
	{
		id: 2103,
		time: 2102,
		velocity: 12.6472222222222,
		power: 1579.0214603144,
		road: 19352.1994444444,
		acceleration: -0.0890740740740714
	},
	{
		id: 2104,
		time: 2103,
		velocity: 12.7305555555556,
		power: 3835.65259683093,
		road: 19364.9436574074,
		acceleration: 0.0963888888888853
	},
	{
		id: 2105,
		time: 2104,
		velocity: 12.9994444444444,
		power: 3696.90492553879,
		road: 19377.7768518518,
		acceleration: 0.0815740740740765
	},
	{
		id: 2106,
		time: 2105,
		velocity: 12.8919444444444,
		power: 2337.42465919997,
		road: 19390.6357407407,
		acceleration: -0.0301851851851858
	},
	{
		id: 2107,
		time: 2106,
		velocity: 12.64,
		power: 390.021712050395,
		road: 19403.3860185185,
		acceleration: -0.187037037037035
	},
	{
		id: 2108,
		time: 2107,
		velocity: 12.4383333333333,
		power: 1038.69360617426,
		road: 19415.9775925926,
		acceleration: -0.130370370370372
	},
	{
		id: 2109,
		time: 2108,
		velocity: 12.5008333333333,
		power: 4253.28074516715,
		road: 19428.5725,
		acceleration: 0.137037037037038
	},
	{
		id: 2110,
		time: 2109,
		velocity: 13.0511111111111,
		power: 6695.41211627728,
		road: 19441.3993055555,
		acceleration: 0.326759259259259
	},
	{
		id: 2111,
		time: 2110,
		velocity: 13.4186111111111,
		power: 8994.11599645446,
		road: 19454.6324074074,
		acceleration: 0.485833333333336
	},
	{
		id: 2112,
		time: 2111,
		velocity: 13.9583333333333,
		power: 7852.10365113101,
		road: 19468.2931018518,
		acceleration: 0.369351851851849
	},
	{
		id: 2113,
		time: 2112,
		velocity: 14.1591666666667,
		power: 5686.83204150867,
		road: 19482.2335648148,
		acceleration: 0.190185185185186
	},
	{
		id: 2114,
		time: 2113,
		velocity: 13.9891666666667,
		power: 2529.32374078014,
		road: 19496.2445833333,
		acceleration: -0.049074074074074
	},
	{
		id: 2115,
		time: 2114,
		velocity: 13.8111111111111,
		power: 758.422441511296,
		road: 19510.1414814815,
		acceleration: -0.179166666666667
	},
	{
		id: 2116,
		time: 2115,
		velocity: 13.6216666666667,
		power: 2507.51077717565,
		road: 19523.9267592593,
		acceleration: -0.0440740740740733
	},
	{
		id: 2117,
		time: 2116,
		velocity: 13.8569444444444,
		power: 3599.57094126811,
		road: 19537.7094907407,
		acceleration: 0.0389814814814802
	},
	{
		id: 2118,
		time: 2117,
		velocity: 13.9280555555556,
		power: 4643.05670580538,
		road: 19551.5692592593,
		acceleration: 0.115092592592593
	},
	{
		id: 2119,
		time: 2118,
		velocity: 13.9669444444444,
		power: 3238.68908821681,
		road: 19565.4900462963,
		acceleration: 0.00694444444444464
	},
	{
		id: 2120,
		time: 2119,
		velocity: 13.8777777777778,
		power: 3323.16585244858,
		road: 19579.420787037,
		acceleration: 0.0129629629629644
	},
	{
		id: 2121,
		time: 2120,
		velocity: 13.9669444444444,
		power: 2769.41015737484,
		road: 19593.3437962963,
		acceleration: -0.0284259259259265
	},
	{
		id: 2122,
		time: 2121,
		velocity: 13.8816666666667,
		power: 3441.37957007335,
		road: 19607.2637037037,
		acceleration: 0.0222222222222221
	},
	{
		id: 2123,
		time: 2122,
		velocity: 13.9444444444444,
		power: 2940.62409876405,
		road: 19621.1869444444,
		acceleration: -0.0155555555555562
	},
	{
		id: 2124,
		time: 2123,
		velocity: 13.9202777777778,
		power: 2949.06262166295,
		road: 19635.0951851852,
		acceleration: -0.0144444444444449
	},
	{
		id: 2125,
		time: 2124,
		velocity: 13.8383333333333,
		power: 1908.81396433117,
		road: 19648.9505092592,
		acceleration: -0.0913888888888899
	},
	{
		id: 2126,
		time: 2125,
		velocity: 13.6702777777778,
		power: 2119.18806144746,
		road: 19662.7235185185,
		acceleration: -0.0732407407407383
	},
	{
		id: 2127,
		time: 2126,
		velocity: 13.7005555555556,
		power: 2046.35046740974,
		road: 19676.4215277778,
		acceleration: -0.0767592592592603
	},
	{
		id: 2128,
		time: 2127,
		velocity: 13.6080555555556,
		power: 2706.55743111008,
		road: 19690.0687962963,
		acceleration: -0.0247222222222234
	},
	{
		id: 2129,
		time: 2128,
		velocity: 13.5961111111111,
		power: 1594.7168889751,
		road: 19703.6494907407,
		acceleration: -0.108425925925925
	},
	{
		id: 2130,
		time: 2129,
		velocity: 13.3752777777778,
		power: 452.876929281516,
		road: 19717.0790740741,
		acceleration: -0.193796296296297
	},
	{
		id: 2131,
		time: 2130,
		velocity: 13.0266666666667,
		power: -578.724890696091,
		road: 19730.2759722222,
		acceleration: -0.271574074074072
	},
	{
		id: 2132,
		time: 2131,
		velocity: 12.7813888888889,
		power: 1548.03023451938,
		road: 19743.287962963,
		acceleration: -0.0982407407407422
	},
	{
		id: 2133,
		time: 2132,
		velocity: 13.0805555555556,
		power: 4534.34581302512,
		road: 19756.3214814815,
		acceleration: 0.141296296296296
	},
	{
		id: 2134,
		time: 2133,
		velocity: 13.4505555555556,
		power: 378.924227608134,
		road: 19769.3294907407,
		acceleration: -0.192314814814814
	},
	{
		id: 2135,
		time: 2134,
		velocity: 12.2044444444444,
		power: -8422.54994056986,
		road: 19781.7793518518,
		acceleration: -0.923981481481484
	},
	{
		id: 2136,
		time: 2135,
		velocity: 10.3086111111111,
		power: -17793.6009413209,
		road: 19792.8239351852,
		acceleration: -1.88657407407407
	},
	{
		id: 2137,
		time: 2136,
		velocity: 7.79083333333333,
		power: -15495.6561315305,
		road: 19801.9459259259,
		acceleration: -1.95861111111111
	},
	{
		id: 2138,
		time: 2137,
		velocity: 6.32861111111111,
		power: -11598.5394571477,
		road: 19809.1656018518,
		acceleration: -1.84601851851852
	},
	{
		id: 2139,
		time: 2138,
		velocity: 4.77055555555556,
		power: -6822.52956388526,
		road: 19814.7465277778,
		acceleration: -1.43148148148148
	},
	{
		id: 2140,
		time: 2139,
		velocity: 3.49638888888889,
		power: -4999.26220179649,
		road: 19818.9109259259,
		acceleration: -1.40157407407407
	},
	{
		id: 2141,
		time: 2140,
		velocity: 2.12388888888889,
		power: -3066.04080333041,
		road: 19821.7367592592,
		acceleration: -1.27555555555556
	},
	{
		id: 2142,
		time: 2141,
		velocity: 0.943888888888889,
		power: -1263.34123711282,
		road: 19823.4775462963,
		acceleration: -0.894537037037037
	},
	{
		id: 2143,
		time: 2142,
		velocity: 0.812777777777778,
		power: -516.46136969841,
		road: 19824.4170833333,
		acceleration: -0.707962962962963
	},
	{
		id: 2144,
		time: 2143,
		velocity: 0,
		power: -75.8895838075099,
		road: 19824.8453240741,
		acceleration: -0.31462962962963
	},
	{
		id: 2145,
		time: 2144,
		velocity: 0,
		power: -18.4024771604938,
		road: 19824.980787037,
		acceleration: -0.270925925925926
	},
	{
		id: 2146,
		time: 2145,
		velocity: 0,
		power: 0,
		road: 19824.980787037,
		acceleration: 0
	},
	{
		id: 2147,
		time: 2146,
		velocity: 0,
		power: 0,
		road: 19824.980787037,
		acceleration: 0
	},
	{
		id: 2148,
		time: 2147,
		velocity: 0,
		power: 0,
		road: 19824.980787037,
		acceleration: 0
	},
	{
		id: 2149,
		time: 2148,
		velocity: 0,
		power: 0,
		road: 19824.980787037,
		acceleration: 0
	},
	{
		id: 2150,
		time: 2149,
		velocity: 0,
		power: 0,
		road: 19824.980787037,
		acceleration: 0
	},
	{
		id: 2151,
		time: 2150,
		velocity: 0,
		power: 0,
		road: 19824.980787037,
		acceleration: 0
	},
	{
		id: 2152,
		time: 2151,
		velocity: 0,
		power: 0,
		road: 19824.980787037,
		acceleration: 0
	},
	{
		id: 2153,
		time: 2152,
		velocity: 0,
		power: 0,
		road: 19824.980787037,
		acceleration: 0
	},
	{
		id: 2154,
		time: 2153,
		velocity: 0,
		power: 0,
		road: 19824.980787037,
		acceleration: 0
	},
	{
		id: 2155,
		time: 2154,
		velocity: 0,
		power: 214.76844363263,
		road: 19825.287037037,
		acceleration: 0.6125
	},
	{
		id: 2156,
		time: 2155,
		velocity: 1.8375,
		power: 1827.21506115502,
		road: 19826.5803240741,
		acceleration: 1.36157407407407
	},
	{
		id: 2157,
		time: 2156,
		velocity: 4.08472222222222,
		power: 4300.87664475448,
		road: 19829.3166666667,
		acceleration: 1.52453703703704
	},
	{
		id: 2158,
		time: 2157,
		velocity: 4.57361111111111,
		power: 4933.18561821285,
		road: 19833.3852777778,
		acceleration: 1.14
	},
	{
		id: 2159,
		time: 2158,
		velocity: 5.2575,
		power: 5800.71490861678,
		road: 19838.5443055556,
		acceleration: 1.04083333333333
	},
	{
		id: 2160,
		time: 2159,
		velocity: 7.20722222222222,
		power: 7480.42348538908,
		road: 19844.7799074074,
		acceleration: 1.11231481481481
	},
	{
		id: 2161,
		time: 2160,
		velocity: 7.91055555555556,
		power: 8222.48011086848,
		road: 19852.0844907407,
		acceleration: 1.02564814814815
	},
	{
		id: 2162,
		time: 2161,
		velocity: 8.33444444444445,
		power: 3361.24006195599,
		road: 19860.0422685185,
		acceleration: 0.28074074074074
	},
	{
		id: 2163,
		time: 2162,
		velocity: 8.04944444444445,
		power: -706.88709466774,
		road: 19868.0122222222,
		acceleration: -0.256388888888888
	},
	{
		id: 2164,
		time: 2163,
		velocity: 7.14138888888889,
		power: -4435.3551701091,
		road: 19875.461712963,
		acceleration: -0.784537037037038
	},
	{
		id: 2165,
		time: 2164,
		velocity: 5.98083333333333,
		power: -3182.79607115704,
		road: 19882.1938888889,
		acceleration: -0.650092592592593
	},
	{
		id: 2166,
		time: 2165,
		velocity: 6.09916666666667,
		power: 1536.22090923395,
		road: 19888.6506944444,
		acceleration: 0.0993518518518526
	},
	{
		id: 2167,
		time: 2166,
		velocity: 7.43944444444444,
		power: 9062.08597348177,
		road: 19895.7501388889,
		acceleration: 1.18592592592593
	},
	{
		id: 2168,
		time: 2167,
		velocity: 9.53861111111111,
		power: 15622.4024130332,
		road: 19904.3158333333,
		acceleration: 1.74657407407407
	},
	{
		id: 2169,
		time: 2168,
		velocity: 11.3388888888889,
		power: 13003.6892636409,
		road: 19914.3431944444,
		acceleration: 1.17675925925926
	},
	{
		id: 2170,
		time: 2169,
		velocity: 10.9697222222222,
		power: 2582.51353160165,
		road: 19924.9905092593,
		acceleration: 0.0631481481481462
	},
	{
		id: 2171,
		time: 2170,
		velocity: 9.72805555555555,
		power: -7638.74264763834,
		road: 19935.1831018519,
		acceleration: -0.972592592592591
	},
	{
		id: 2172,
		time: 2171,
		velocity: 8.42111111111111,
		power: -8910.50426214847,
		road: 19944.2884259259,
		acceleration: -1.20194444444444
	},
	{
		id: 2173,
		time: 2172,
		velocity: 7.36388888888889,
		power: -5088.41169362857,
		road: 19952.3802314815,
		acceleration: -0.825092592592592
	},
	{
		id: 2174,
		time: 2173,
		velocity: 7.25277777777778,
		power: -2067.06415495249,
		road: 19959.8343981481,
		acceleration: -0.450185185185184
	},
	{
		id: 2175,
		time: 2174,
		velocity: 7.07055555555556,
		power: -678.189276402846,
		road: 19966.9353703704,
		acceleration: -0.256203703703704
	},
	{
		id: 2176,
		time: 2175,
		velocity: 6.59527777777778,
		power: -735.759143434391,
		road: 19973.7748148148,
		acceleration: -0.266851851851851
	},
	{
		id: 2177,
		time: 2176,
		velocity: 6.45222222222222,
		power: -545.900475484999,
		road: 19980.3613425926,
		acceleration: -0.238981481481481
	},
	{
		id: 2178,
		time: 2177,
		velocity: 6.35361111111111,
		power: 461.436199800915,
		road: 19986.790787037,
		acceleration: -0.0751851851851866
	},
	{
		id: 2179,
		time: 2178,
		velocity: 6.36972222222222,
		power: 108.115978503139,
		road: 19993.1166666667,
		acceleration: -0.131944444444444
	},
	{
		id: 2180,
		time: 2179,
		velocity: 6.05638888888889,
		power: -1399.35412080053,
		road: 19999.1811574074,
		acceleration: -0.390833333333333
	},
	{
		id: 2181,
		time: 2180,
		velocity: 5.18111111111111,
		power: -2873.81237374146,
		road: 20004.7041666667,
		acceleration: -0.69212962962963
	},
	{
		id: 2182,
		time: 2181,
		velocity: 4.29333333333333,
		power: -4222.82268849236,
		road: 20009.3308796296,
		acceleration: -1.10046296296296
	},
	{
		id: 2183,
		time: 2182,
		velocity: 2.755,
		power: -3200.53407554562,
		road: 20012.8627777778,
		acceleration: -1.08916666666667
	},
	{
		id: 2184,
		time: 2183,
		velocity: 1.91361111111111,
		power: -2012.83468628315,
		road: 20015.3597222222,
		acceleration: -0.980740740740741
	},
	{
		id: 2185,
		time: 2184,
		velocity: 1.35111111111111,
		power: -785.043427170116,
		road: 20017.0580092593,
		acceleration: -0.616574074074074
	},
	{
		id: 2186,
		time: 2185,
		velocity: 0.905277777777778,
		power: -263.478499364768,
		road: 20018.2691203704,
		acceleration: -0.357777777777778
	},
	{
		id: 2187,
		time: 2186,
		velocity: 0.840277777777778,
		power: -246.682961606474,
		road: 20019.0761574074,
		acceleration: -0.45037037037037
	},
	{
		id: 2188,
		time: 2187,
		velocity: 0,
		power: 121.453904837314,
		road: 20019.6973148148,
		acceleration: 0.0786111111111112
	},
	{
		id: 2189,
		time: 2188,
		velocity: 1.14111111111111,
		power: 282.39500326046,
		road: 20020.4833796296,
		acceleration: 0.251203703703704
	},
	{
		id: 2190,
		time: 2189,
		velocity: 1.59388888888889,
		power: 629.576235497067,
		road: 20021.6224537037,
		acceleration: 0.454814814814815
	},
	{
		id: 2191,
		time: 2190,
		velocity: 1.36444444444444,
		power: -281.140798570874,
		road: 20022.79875,
		acceleration: -0.38037037037037
	},
	{
		id: 2192,
		time: 2191,
		velocity: 0,
		power: -275.50730094125,
		road: 20023.519212963,
		acceleration: -0.531296296296296
	},
	{
		id: 2193,
		time: 2192,
		velocity: 0,
		power: -70.5097811565952,
		road: 20023.7466203704,
		acceleration: -0.454814814814815
	},
	{
		id: 2194,
		time: 2193,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2195,
		time: 2194,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2196,
		time: 2195,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2197,
		time: 2196,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2198,
		time: 2197,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2199,
		time: 2198,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2200,
		time: 2199,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2201,
		time: 2200,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2202,
		time: 2201,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2203,
		time: 2202,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2204,
		time: 2203,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2205,
		time: 2204,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2206,
		time: 2205,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2207,
		time: 2206,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2208,
		time: 2207,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2209,
		time: 2208,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2210,
		time: 2209,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2211,
		time: 2210,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2212,
		time: 2211,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2213,
		time: 2212,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2214,
		time: 2213,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2215,
		time: 2214,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2216,
		time: 2215,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2217,
		time: 2216,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2218,
		time: 2217,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2219,
		time: 2218,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2220,
		time: 2219,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2221,
		time: 2220,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2222,
		time: 2221,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2223,
		time: 2222,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2224,
		time: 2223,
		velocity: 0,
		power: 0,
		road: 20023.7466203704,
		acceleration: 0
	},
	{
		id: 2225,
		time: 2224,
		velocity: 0,
		power: 172.223415295301,
		road: 20024.0178703704,
		acceleration: 0.5425
	},
	{
		id: 2226,
		time: 2225,
		velocity: 1.6275,
		power: 1241.08635016683,
		road: 20025.1007407407,
		acceleration: 1.08074074074074
	},
	{
		id: 2227,
		time: 2226,
		velocity: 3.24222222222222,
		power: 4173.80622173964,
		road: 20027.5547685185,
		acceleration: 1.66157407407407
	},
	{
		id: 2228,
		time: 2227,
		velocity: 4.98472222222222,
		power: 6569.33261936521,
		road: 20031.6216203704,
		acceleration: 1.56407407407407
	},
	{
		id: 2229,
		time: 2228,
		velocity: 6.31972222222222,
		power: 5893.00897451741,
		road: 20036.9775925926,
		acceleration: 1.01416666666667
	},
	{
		id: 2230,
		time: 2229,
		velocity: 6.28472222222222,
		power: 5058.41581530789,
		road: 20043.1939814815,
		acceleration: 0.706666666666667
	},
	{
		id: 2231,
		time: 2230,
		velocity: 7.10472222222222,
		power: 3489.59143411555,
		road: 20049.9583333333,
		acceleration: 0.389259259259259
	},
	{
		id: 2232,
		time: 2231,
		velocity: 7.4875,
		power: 5245.90528898925,
		road: 20057.2185185185,
		acceleration: 0.602407407407408
	},
	{
		id: 2233,
		time: 2232,
		velocity: 8.09194444444444,
		power: 3202.39484797471,
		road: 20064.9180555556,
		acceleration: 0.276296296296295
	},
	{
		id: 2234,
		time: 2233,
		velocity: 7.93361111111111,
		power: 1394.31505443945,
		road: 20072.7680092593,
		acceleration: 0.0245370370370379
	},
	{
		id: 2235,
		time: 2234,
		velocity: 7.56111111111111,
		power: -597.105481601659,
		road: 20080.5091203704,
		acceleration: -0.242222222222222
	},
	{
		id: 2236,
		time: 2235,
		velocity: 7.36527777777778,
		power: -548.332184849539,
		road: 20088.0111574074,
		acceleration: -0.235925925925926
	},
	{
		id: 2237,
		time: 2236,
		velocity: 7.22583333333333,
		power: -166.732547986857,
		road: 20095.304537037,
		acceleration: -0.181388888888889
	},
	{
		id: 2238,
		time: 2237,
		velocity: 7.01694444444444,
		power: 546.546401572145,
		road: 20102.4691666667,
		acceleration: -0.0761111111111106
	},
	{
		id: 2239,
		time: 2238,
		velocity: 7.13694444444444,
		power: 788.112634656976,
		road: 20109.5761111111,
		acceleration: -0.0392592592592598
	},
	{
		id: 2240,
		time: 2239,
		velocity: 7.10805555555555,
		power: -436.9008659003,
		road: 20116.5531018519,
		acceleration: -0.220648148148149
	},
	{
		id: 2241,
		time: 2240,
		velocity: 6.355,
		power: 557.032563914379,
		road: 20123.385787037,
		acceleration: -0.0679629629629623
	},
	{
		id: 2242,
		time: 2241,
		velocity: 6.93305555555555,
		power: 3987.56456447869,
		road: 20130.4055092593,
		acceleration: 0.442037037037037
	},
	{
		id: 2243,
		time: 2242,
		velocity: 8.43416666666667,
		power: 7815.6492390382,
		road: 20138.0995833333,
		acceleration: 0.906666666666666
	},
	{
		id: 2244,
		time: 2243,
		velocity: 9.075,
		power: 7292.34030163246,
		road: 20146.6127314815,
		acceleration: 0.731481481481483
	},
	{
		id: 2245,
		time: 2244,
		velocity: 9.1275,
		power: 4307.59471550664,
		road: 20155.655,
		acceleration: 0.32675925925926
	},
	{
		id: 2246,
		time: 2245,
		velocity: 9.41444444444445,
		power: 2818.31467021118,
		road: 20164.9321759259,
		acceleration: 0.143055555555554
	},
	{
		id: 2247,
		time: 2246,
		velocity: 9.50416666666667,
		power: 2265.25503255046,
		road: 20174.3190277778,
		acceleration: 0.076296296296297
	},
	{
		id: 2248,
		time: 2247,
		velocity: 9.35638888888889,
		power: 1292.61085680247,
		road: 20183.7275,
		acceleration: -0.0330555555555545
	},
	{
		id: 2249,
		time: 2248,
		velocity: 9.31527777777778,
		power: 683.18248153168,
		road: 20193.069537037,
		acceleration: -0.0998148148148186
	},
	{
		id: 2250,
		time: 2249,
		velocity: 9.20472222222222,
		power: 505.555375732477,
		road: 20202.3026851852,
		acceleration: -0.117962962962961
	},
	{
		id: 2251,
		time: 2250,
		velocity: 9.0025,
		power: -626.737129998617,
		road: 20211.3537037037,
		acceleration: -0.246296296296295
	},
	{
		id: 2252,
		time: 2251,
		velocity: 8.57638888888889,
		power: -1125.04184094317,
		road: 20220.1288425926,
		acceleration: -0.305462962962963
	},
	{
		id: 2253,
		time: 2252,
		velocity: 8.28833333333333,
		power: -2723.46995308667,
		road: 20228.4968518519,
		acceleration: -0.508796296296296
	},
	{
		id: 2254,
		time: 2253,
		velocity: 7.47611111111111,
		power: -5167.45405249829,
		road: 20236.1765277778,
		acceleration: -0.867870370370372
	},
	{
		id: 2255,
		time: 2254,
		velocity: 5.97277777777778,
		power: -6802.71224833261,
		road: 20242.8066203704,
		acceleration: -1.2312962962963
	},
	{
		id: 2256,
		time: 2255,
		velocity: 4.59444444444444,
		power: -5883.33084328434,
		road: 20248.1722222222,
		acceleration: -1.29768518518518
	},
	{
		id: 2257,
		time: 2256,
		velocity: 3.58305555555556,
		power: -3573.28761273748,
		road: 20252.3723148148,
		acceleration: -1.03333333333333
	},
	{
		id: 2258,
		time: 2257,
		velocity: 2.87277777777778,
		power: -1777.50000775399,
		road: 20255.7081944445,
		acceleration: -0.695092592592592
	},
	{
		id: 2259,
		time: 2258,
		velocity: 2.50916666666667,
		power: -761.484918174329,
		road: 20258.4862037037,
		acceleration: -0.420648148148148
	},
	{
		id: 2260,
		time: 2259,
		velocity: 2.32111111111111,
		power: 141.655406558005,
		road: 20261.0178703704,
		acceleration: -0.0720370370370369
	},
	{
		id: 2261,
		time: 2260,
		velocity: 2.65666666666667,
		power: 468.508337809174,
		road: 20263.5456944445,
		acceleration: 0.064351851851852
	},
	{
		id: 2262,
		time: 2261,
		velocity: 2.70222222222222,
		power: 2877.84077778995,
		road: 20266.5449537037,
		acceleration: 0.878518518518518
	},
	{
		id: 2263,
		time: 2262,
		velocity: 4.95666666666667,
		power: 5447.32067877961,
		road: 20270.6190277778,
		acceleration: 1.27111111111111
	},
	{
		id: 2264,
		time: 2263,
		velocity: 6.47,
		power: 6792.10553389915,
		road: 20275.9298611111,
		acceleration: 1.20240740740741
	},
	{
		id: 2265,
		time: 2264,
		velocity: 6.30944444444444,
		power: 4179.73482658319,
		road: 20282.1223611111,
		acceleration: 0.560925925925925
	},
	{
		id: 2266,
		time: 2265,
		velocity: 6.63944444444444,
		power: 2776.04374699511,
		road: 20288.7398611111,
		acceleration: 0.289074074074074
	},
	{
		id: 2267,
		time: 2266,
		velocity: 7.33722222222222,
		power: 5990.41839389782,
		road: 20295.8657407408,
		acceleration: 0.727685185185186
	},
	{
		id: 2268,
		time: 2267,
		velocity: 8.4925,
		power: 7224.90772654576,
		road: 20303.7553240741,
		acceleration: 0.799722222222221
	},
	{
		id: 2269,
		time: 2268,
		velocity: 9.03861111111111,
		power: 5132.04277734702,
		road: 20312.2768981482,
		acceleration: 0.464259259259261
	},
	{
		id: 2270,
		time: 2269,
		velocity: 8.73,
		power: 1063.18064582479,
		road: 20321.0093518519,
		acceleration: -0.0425000000000022
	},
	{
		id: 2271,
		time: 2270,
		velocity: 8.365,
		power: -278.44809350615,
		road: 20329.6189814815,
		acceleration: -0.203148148148147
	},
	{
		id: 2272,
		time: 2271,
		velocity: 8.42916666666667,
		power: 907.148612808181,
		road: 20338.0992592593,
		acceleration: -0.0555555555555554
	},
	{
		id: 2273,
		time: 2272,
		velocity: 8.56333333333333,
		power: 2910.09975858258,
		road: 20346.6462962963,
		acceleration: 0.189074074074073
	},
	{
		id: 2274,
		time: 2273,
		velocity: 8.93222222222222,
		power: 4933.32207667926,
		road: 20355.4948611111,
		acceleration: 0.413981481481482
	},
	{
		id: 2275,
		time: 2274,
		velocity: 9.67111111111111,
		power: 7807.30749566795,
		road: 20364.8976388889,
		acceleration: 0.694444444444445
	},
	{
		id: 2276,
		time: 2275,
		velocity: 10.6466666666667,
		power: 12676.2018947328,
		road: 20375.1995370371,
		acceleration: 1.1037962962963
	},
	{
		id: 2277,
		time: 2276,
		velocity: 12.2436111111111,
		power: 17788.0776668847,
		road: 20386.7586574074,
		acceleration: 1.41064814814815
	},
	{
		id: 2278,
		time: 2277,
		velocity: 13.9030555555556,
		power: 19082.0552298707,
		road: 20399.6855092593,
		acceleration: 1.32481481481481
	},
	{
		id: 2279,
		time: 2278,
		velocity: 14.6211111111111,
		power: 17727.0828592299,
		road: 20413.811712963,
		acceleration: 1.07388888888889
	},
	{
		id: 2280,
		time: 2279,
		velocity: 15.4652777777778,
		power: 19880.0672147928,
		road: 20429.0293055556,
		acceleration: 1.10888888888889
	},
	{
		id: 2281,
		time: 2280,
		velocity: 17.2297222222222,
		power: 19525.1268390039,
		road: 20445.2908796296,
		acceleration: 0.979074074074074
	},
	{
		id: 2282,
		time: 2281,
		velocity: 17.5583333333333,
		power: 13163.648063091,
		road: 20462.3012037037,
		acceleration: 0.518425925925925
	},
	{
		id: 2283,
		time: 2282,
		velocity: 17.0205555555556,
		power: 852.681172899643,
		road: 20479.4501388889,
		acceleration: -0.2412037037037
	},
	{
		id: 2284,
		time: 2283,
		velocity: 16.5061111111111,
		power: -3758.35591272778,
		road: 20496.2183333333,
		acceleration: -0.520277777777778
	},
	{
		id: 2285,
		time: 2284,
		velocity: 15.9975,
		power: -5602.1620595796,
		road: 20512.4078240741,
		acceleration: -0.637129629629632
	},
	{
		id: 2286,
		time: 2285,
		velocity: 15.1091666666667,
		power: -5564.09768069006,
		road: 20527.9596759259,
		acceleration: -0.638148148148147
	},
	{
		id: 2287,
		time: 2286,
		velocity: 14.5916666666667,
		power: -4749.3054442598,
		road: 20542.8994444445,
		acceleration: -0.586018518518518
	},
	{
		id: 2288,
		time: 2287,
		velocity: 14.2394444444444,
		power: -2162.91671086033,
		road: 20557.3452777778,
		acceleration: -0.401851851851852
	},
	{
		id: 2289,
		time: 2288,
		velocity: 13.9036111111111,
		power: 880.991456017023,
		road: 20571.5025462963,
		acceleration: -0.175277777777779
	},
	{
		id: 2290,
		time: 2289,
		velocity: 14.0658333333333,
		power: 2110.99444806931,
		road: 20585.5317592593,
		acceleration: -0.0808333333333344
	},
	{
		id: 2291,
		time: 2290,
		velocity: 13.9969444444444,
		power: 1315.86420364912,
		road: 20599.451712963,
		acceleration: -0.137685185185184
	},
	{
		id: 2292,
		time: 2291,
		velocity: 13.4905555555556,
		power: -694.960021668864,
		road: 20613.1595833333,
		acceleration: -0.286481481481481
	},
	{
		id: 2293,
		time: 2292,
		velocity: 13.2063888888889,
		power: -2603.391881403,
		road: 20626.5081018519,
		acceleration: -0.432222222222222
	},
	{
		id: 2294,
		time: 2293,
		velocity: 12.7002777777778,
		power: -1580.45258188453,
		road: 20639.4655555556,
		acceleration: -0.349907407407409
	},
	{
		id: 2295,
		time: 2294,
		velocity: 12.4408333333333,
		power: -850.93195902048,
		road: 20652.1040277778,
		acceleration: -0.288055555555554
	},
	{
		id: 2296,
		time: 2295,
		velocity: 12.3422222222222,
		power: -835.426891570679,
		road: 20664.4562962963,
		acceleration: -0.284351851851852
	},
	{
		id: 2297,
		time: 2296,
		velocity: 11.8472222222222,
		power: -91.9985293495995,
		road: 20676.5574074074,
		acceleration: -0.217962962962963
	},
	{
		id: 2298,
		time: 2297,
		velocity: 11.7869444444444,
		power: 106.639797069719,
		road: 20688.4506481482,
		acceleration: -0.197777777777778
	},
	{
		id: 2299,
		time: 2298,
		velocity: 11.7488888888889,
		power: 1998.71763462341,
		road: 20700.2311111111,
		acceleration: -0.0277777777777786
	},
	{
		id: 2300,
		time: 2299,
		velocity: 11.7638888888889,
		power: 1826.77913385097,
		road: 20711.9766203704,
		acceleration: -0.0421296296296294
	},
	{
		id: 2301,
		time: 2300,
		velocity: 11.6605555555556,
		power: 2487.14345320964,
		road: 20723.7096759259,
		acceleration: 0.0172222222222214
	},
	{
		id: 2302,
		time: 2301,
		velocity: 11.8005555555556,
		power: 2859.76895127339,
		road: 20735.4760185185,
		acceleration: 0.0493518518518528
	},
	{
		id: 2303,
		time: 2302,
		velocity: 11.9119444444444,
		power: 3541.30448464213,
		road: 20747.3205092593,
		acceleration: 0.106944444444446
	},
	{
		id: 2304,
		time: 2303,
		velocity: 11.9813888888889,
		power: 2566.48861228015,
		road: 20759.2277777778,
		acceleration: 0.0186111111111114
	},
	{
		id: 2305,
		time: 2304,
		velocity: 11.8563888888889,
		power: 2848.74983312181,
		road: 20771.1655555556,
		acceleration: 0.0424074074074046
	},
	{
		id: 2306,
		time: 2305,
		velocity: 12.0391666666667,
		power: 3270.80230057568,
		road: 20783.1631481482,
		acceleration: 0.0772222222222236
	},
	{
		id: 2307,
		time: 2306,
		velocity: 12.2130555555556,
		power: 4423.29069102535,
		road: 20795.285462963,
		acceleration: 0.172222222222221
	},
	{
		id: 2308,
		time: 2307,
		velocity: 12.3730555555556,
		power: 2832.37979375137,
		road: 20807.5094907408,
		acceleration: 0.031203703703703
	},
	{
		id: 2309,
		time: 2308,
		velocity: 12.1327777777778,
		power: 1178.82265852208,
		road: 20819.6942592593,
		acceleration: -0.109722222222221
	},
	{
		id: 2310,
		time: 2309,
		velocity: 11.8838888888889,
		power: -2115.75688823785,
		road: 20831.6273611111,
		acceleration: -0.393611111111111
	},
	{
		id: 2311,
		time: 2310,
		velocity: 11.1922222222222,
		power: -3420.8969109571,
		road: 20843.1065277778,
		acceleration: -0.514259259259257
	},
	{
		id: 2312,
		time: 2311,
		velocity: 10.59,
		power: -5414.36195937943,
		road: 20853.9701388889,
		acceleration: -0.716851851851855
	},
	{
		id: 2313,
		time: 2312,
		velocity: 9.73333333333333,
		power: -5545.69178074941,
		road: 20864.0952777778,
		acceleration: -0.76009259259259
	},
	{
		id: 2314,
		time: 2313,
		velocity: 8.91194444444444,
		power: -6257.71497692165,
		road: 20873.3991203704,
		acceleration: -0.882500000000002
	},
	{
		id: 2315,
		time: 2314,
		velocity: 7.9425,
		power: -4457.61906407554,
		road: 20881.9022685185,
		acceleration: -0.718888888888888
	},
	{
		id: 2316,
		time: 2315,
		velocity: 7.57666666666667,
		power: -2437.8921968531,
		road: 20889.8025462963,
		acceleration: -0.486851851851853
	},
	{
		id: 2317,
		time: 2316,
		velocity: 7.45138888888889,
		power: 108.150206047617,
		road: 20897.3869907408,
		acceleration: -0.144814814814814
	},
	{
		id: 2318,
		time: 2317,
		velocity: 7.50805555555556,
		power: -184.445972233944,
		road: 20904.8067592593,
		acceleration: -0.184537037037037
	},
	{
		id: 2319,
		time: 2318,
		velocity: 7.02305555555556,
		power: -637.908823108841,
		road: 20912.0093981482,
		acceleration: -0.249722222222223
	},
	{
		id: 2320,
		time: 2319,
		velocity: 6.70222222222222,
		power: -325.695927439703,
		road: 20918.9852314815,
		acceleration: -0.203888888888888
	},
	{
		id: 2321,
		time: 2320,
		velocity: 6.89638888888889,
		power: 2552.34357746817,
		road: 20925.9736574074,
		acceleration: 0.229074074074074
	},
	{
		id: 2322,
		time: 2321,
		velocity: 7.71027777777778,
		power: 4901.91337879125,
		road: 20933.3469907408,
		acceleration: 0.540740740740741
	},
	{
		id: 2323,
		time: 2322,
		velocity: 8.32444444444444,
		power: 3622.12917502231,
		road: 20941.1536111111,
		acceleration: 0.325833333333332
	},
	{
		id: 2324,
		time: 2323,
		velocity: 7.87388888888889,
		power: 214.391699268621,
		road: 20949.0561111111,
		acceleration: -0.134074074074074
	},
	{
		id: 2325,
		time: 2324,
		velocity: 7.30805555555556,
		power: -2481.30635696456,
		road: 20956.6397222223,
		acceleration: -0.503703703703703
	},
	{
		id: 2326,
		time: 2325,
		velocity: 6.81333333333333,
		power: -2092.26667801996,
		road: 20963.7386574074,
		acceleration: -0.465648148148148
	},
	{
		id: 2327,
		time: 2326,
		velocity: 6.47694444444444,
		power: -1139.30330498516,
		road: 20970.4390277778,
		acceleration: -0.331481481481481
	},
	{
		id: 2328,
		time: 2327,
		velocity: 6.31361111111111,
		power: -916.835969641436,
		road: 20976.822962963,
		acceleration: -0.30138888888889
	},
	{
		id: 2329,
		time: 2328,
		velocity: 5.90916666666667,
		power: -606.041023856005,
		road: 20982.9298148148,
		acceleration: -0.252777777777777
	},
	{
		id: 2330,
		time: 2329,
		velocity: 5.71861111111111,
		power: -1169.94685897337,
		road: 20988.7310185185,
		acceleration: -0.358518518518518
	},
	{
		id: 2331,
		time: 2330,
		velocity: 5.23805555555556,
		power: -1375.30424426378,
		road: 20994.1474074074,
		acceleration: -0.411111111111112
	},
	{
		id: 2332,
		time: 2331,
		velocity: 4.67583333333333,
		power: -2892.36751610203,
		road: 20998.9724537037,
		acceleration: -0.771574074074073
	},
	{
		id: 2333,
		time: 2332,
		velocity: 3.40388888888889,
		power: -3264.94358306212,
		road: 21002.9065740741,
		acceleration: -1.01027777777778
	},
	{
		id: 2334,
		time: 2333,
		velocity: 2.20722222222222,
		power: -2845.84676163166,
		road: 21005.7403240741,
		acceleration: -1.19046296296296
	},
	{
		id: 2335,
		time: 2334,
		velocity: 1.10444444444444,
		power: -1593.38052662925,
		road: 21007.4115277778,
		acceleration: -1.13462962962963
	},
	{
		id: 2336,
		time: 2335,
		velocity: 0,
		power: -424.039458155068,
		road: 21008.1475462963,
		acceleration: -0.735740740740741
	},
	{
		id: 2337,
		time: 2336,
		velocity: 0,
		power: -41.9604279402209,
		road: 21008.3316203704,
		acceleration: -0.368148148148148
	},
	{
		id: 2338,
		time: 2337,
		velocity: 0,
		power: 0,
		road: 21008.3316203704,
		acceleration: 0
	},
	{
		id: 2339,
		time: 2338,
		velocity: 0,
		power: 0,
		road: 21008.3316203704,
		acceleration: 0
	},
	{
		id: 2340,
		time: 2339,
		velocity: 0,
		power: 0,
		road: 21008.3316203704,
		acceleration: 0
	},
	{
		id: 2341,
		time: 2340,
		velocity: 0,
		power: 0,
		road: 21008.3316203704,
		acceleration: 0
	},
	{
		id: 2342,
		time: 2341,
		velocity: 0,
		power: 0,
		road: 21008.3316203704,
		acceleration: 0
	},
	{
		id: 2343,
		time: 2342,
		velocity: 0,
		power: 280.008746047125,
		road: 21008.6854166667,
		acceleration: 0.707592592592593
	},
	{
		id: 2344,
		time: 2343,
		velocity: 2.12277777777778,
		power: 1797.67355223728,
		road: 21010.0324537037,
		acceleration: 1.27888888888889
	},
	{
		id: 2345,
		time: 2344,
		velocity: 3.83666666666667,
		power: 5926.44059021742,
		road: 21013.0034722223,
		acceleration: 1.96907407407407
	},
	{
		id: 2346,
		time: 2345,
		velocity: 5.90722222222222,
		power: 6291.49118100424,
		road: 21017.608425926,
		acceleration: 1.2987962962963
	},
	{
		id: 2347,
		time: 2346,
		velocity: 6.01916666666667,
		power: 4874.45191602328,
		road: 21023.2450462963,
		acceleration: 0.764537037037036
	},
	{
		id: 2348,
		time: 2347,
		velocity: 6.13027777777778,
		power: 4009.21313407771,
		road: 21029.5248611111,
		acceleration: 0.521851851851853
	},
	{
		id: 2349,
		time: 2348,
		velocity: 7.47277777777778,
		power: 6527.44839435214,
		road: 21036.4814814815,
		acceleration: 0.831759259259258
	},
	{
		id: 2350,
		time: 2349,
		velocity: 8.51444444444444,
		power: 9197.09637210611,
		road: 21044.3840740741,
		acceleration: 1.06018518518519
	},
	{
		id: 2351,
		time: 2350,
		velocity: 9.31083333333333,
		power: 8621.43257251922,
		road: 21053.2419907408,
		acceleration: 0.850462962962963
	},
	{
		id: 2352,
		time: 2351,
		velocity: 10.0241666666667,
		power: 6591.93570516227,
		road: 21062.7977777778,
		acceleration: 0.545277777777779
	},
	{
		id: 2353,
		time: 2352,
		velocity: 10.1502777777778,
		power: 4799.66928931579,
		road: 21072.7865277778,
		acceleration: 0.320648148148146
	},
	{
		id: 2354,
		time: 2353,
		velocity: 10.2727777777778,
		power: 4081.47043681806,
		road: 21083.0508333334,
		acceleration: 0.230462962962964
	},
	{
		id: 2355,
		time: 2354,
		velocity: 10.7155555555556,
		power: 4820.13515577912,
		road: 21093.5756944445,
		acceleration: 0.290648148148147
	},
	{
		id: 2356,
		time: 2355,
		velocity: 11.0222222222222,
		power: 4868.45389298472,
		road: 21104.385462963,
		acceleration: 0.279166666666667
	},
	{
		id: 2357,
		time: 2356,
		velocity: 11.1102777777778,
		power: 6125.07965565925,
		road: 21115.5244907408,
		acceleration: 0.379351851851853
	},
	{
		id: 2358,
		time: 2357,
		velocity: 11.8536111111111,
		power: 8422.15630750251,
		road: 21127.1318981482,
		acceleration: 0.557407407407407
	},
	{
		id: 2359,
		time: 2358,
		velocity: 12.6944444444444,
		power: 9861.2424272576,
		road: 21139.3357870371,
		acceleration: 0.635555555555555
	},
	{
		id: 2360,
		time: 2359,
		velocity: 13.0169444444444,
		power: 9400.81500624199,
		road: 21152.1324537037,
		acceleration: 0.549999999999999
	},
	{
		id: 2361,
		time: 2360,
		velocity: 13.5036111111111,
		power: 5600.5655399604,
		road: 21165.3139814815,
		acceleration: 0.219722222222222
	},
	{
		id: 2362,
		time: 2361,
		velocity: 13.3536111111111,
		power: 4288.91684455122,
		road: 21178.6597685185,
		acceleration: 0.108796296296298
	},
	{
		id: 2363,
		time: 2362,
		velocity: 13.3433333333333,
		power: 1700.71342228208,
		road: 21192.0126851852,
		acceleration: -0.0945370370370391
	},
	{
		id: 2364,
		time: 2363,
		velocity: 13.22,
		power: 408.264569322882,
		road: 21205.2216203704,
		acceleration: -0.193425925925924
	},
	{
		id: 2365,
		time: 2364,
		velocity: 12.7733333333333,
		power: -2932.3633679079,
		road: 21218.1040740741,
		acceleration: -0.459537037037038
	},
	{
		id: 2366,
		time: 2365,
		velocity: 11.9647222222222,
		power: -7488.94598371282,
		road: 21230.3298148148,
		acceleration: -0.853888888888887
	},
	{
		id: 2367,
		time: 2366,
		velocity: 10.6583333333333,
		power: -10185.8331600514,
		road: 21241.5533796297,
		acceleration: -1.15046296296296
	},
	{
		id: 2368,
		time: 2367,
		velocity: 9.32194444444444,
		power: -8178.04909022136,
		road: 21251.685462963,
		acceleration: -1.0325
	},
	{
		id: 2369,
		time: 2368,
		velocity: 8.86722222222222,
		power: -7333.71147635832,
		road: 21260.7912962963,
		acceleration: -1.02
	},
	{
		id: 2370,
		time: 2369,
		velocity: 7.59833333333333,
		power: -7443.35812291688,
		road: 21268.8180092593,
		acceleration: -1.13824074074074
	},
	{
		id: 2371,
		time: 2370,
		velocity: 5.90722222222222,
		power: -8787.29492501129,
		road: 21275.5086111111,
		acceleration: -1.53398148148148
	},
	{
		id: 2372,
		time: 2371,
		velocity: 4.26527777777778,
		power: -7147.54142271848,
		road: 21280.6259722223,
		acceleration: -1.6125
	},
	{
		id: 2373,
		time: 2372,
		velocity: 2.76083333333333,
		power: -4552.1671858745,
		road: 21284.1985185186,
		acceleration: -1.47712962962963
	},
	{
		id: 2374,
		time: 2373,
		velocity: 1.47583333333333,
		power: -2365.35646022319,
		road: 21286.4012962963,
		acceleration: -1.26240740740741
	},
	{
		id: 2375,
		time: 2374,
		velocity: 0.478055555555556,
		power: -834.45879282538,
		road: 21287.5127314815,
		acceleration: -0.920277777777778
	},
	{
		id: 2376,
		time: 2375,
		velocity: 0,
		power: -139.926362608477,
		road: 21287.9180555556,
		acceleration: -0.491944444444444
	},
	{
		id: 2377,
		time: 2376,
		velocity: 0,
		power: -2.40199153671215,
		road: 21287.9977314815,
		acceleration: -0.159351851851852
	},
	{
		id: 2378,
		time: 2377,
		velocity: 0,
		power: 0,
		road: 21287.9977314815,
		acceleration: 0
	},
	{
		id: 2379,
		time: 2378,
		velocity: 0,
		power: 0,
		road: 21287.9977314815,
		acceleration: 0
	},
	{
		id: 2380,
		time: 2379,
		velocity: 0,
		power: 0,
		road: 21287.9977314815,
		acceleration: 0
	},
	{
		id: 2381,
		time: 2380,
		velocity: 0,
		power: 0,
		road: 21287.9977314815,
		acceleration: 0
	},
	{
		id: 2382,
		time: 2381,
		velocity: 0,
		power: 0,
		road: 21287.9977314815,
		acceleration: 0
	},
	{
		id: 2383,
		time: 2382,
		velocity: 0,
		power: 0,
		road: 21287.9977314815,
		acceleration: 0
	},
	{
		id: 2384,
		time: 2383,
		velocity: 0,
		power: 0,
		road: 21287.9977314815,
		acceleration: 0
	},
	{
		id: 2385,
		time: 2384,
		velocity: 0,
		power: 0,
		road: 21287.9977314815,
		acceleration: 0
	},
	{
		id: 2386,
		time: 2385,
		velocity: 0,
		power: 0,
		road: 21287.9977314815,
		acceleration: 0
	},
	{
		id: 2387,
		time: 2386,
		velocity: 0,
		power: 108.144702127544,
		road: 21288.2068518519,
		acceleration: 0.418240740740741
	},
	{
		id: 2388,
		time: 2387,
		velocity: 1.25472222222222,
		power: 524.118806451097,
		road: 21288.9388888889,
		acceleration: 0.627592592592593
	},
	{
		id: 2389,
		time: 2388,
		velocity: 1.88277777777778,
		power: 1428.92644961358,
		road: 21290.4267592593,
		acceleration: 0.884074074074074
	},
	{
		id: 2390,
		time: 2389,
		velocity: 2.65222222222222,
		power: 1938.45135536162,
		road: 21292.734212963,
		acceleration: 0.755092592592593
	},
	{
		id: 2391,
		time: 2390,
		velocity: 3.52,
		power: 2841.42397869408,
		road: 21295.835462963,
		acceleration: 0.8325
	},
	{
		id: 2392,
		time: 2391,
		velocity: 4.38027777777778,
		power: 3801.28549432097,
		road: 21299.7908796297,
		acceleration: 0.875833333333334
	},
	{
		id: 2393,
		time: 2392,
		velocity: 5.27972222222222,
		power: 3164.74841050945,
		road: 21304.4703240741,
		acceleration: 0.572222222222222
	},
	{
		id: 2394,
		time: 2393,
		velocity: 5.23666666666667,
		power: 2192.29961258381,
		road: 21309.5901388889,
		acceleration: 0.308518518518519
	},
	{
		id: 2395,
		time: 2394,
		velocity: 5.30583333333333,
		power: 1609.9373858579,
		road: 21314.950462963,
		acceleration: 0.172499999999999
	},
	{
		id: 2396,
		time: 2395,
		velocity: 5.79722222222222,
		power: 2226.64641258984,
		road: 21320.5343518519,
		acceleration: 0.27462962962963
	},
	{
		id: 2397,
		time: 2396,
		velocity: 6.06055555555556,
		power: 2889.2087590787,
		road: 21326.4393981482,
		acceleration: 0.367685185185184
	},
	{
		id: 2398,
		time: 2397,
		velocity: 6.40888888888889,
		power: 2271.97634614802,
		road: 21332.64625,
		acceleration: 0.235925925925926
	},
	{
		id: 2399,
		time: 2398,
		velocity: 6.505,
		power: 1700.02630822878,
		road: 21339.0357870371,
		acceleration: 0.129444444444445
	},
	{
		id: 2400,
		time: 2399,
		velocity: 6.44888888888889,
		power: 1988.34311933741,
		road: 21345.574212963,
		acceleration: 0.168333333333333
	},
	{
		id: 2401,
		time: 2400,
		velocity: 6.91388888888889,
		power: 1823.11522889332,
		road: 21352.2637962963,
		acceleration: 0.133981481481481
	},
	{
		id: 2402,
		time: 2401,
		velocity: 6.90694444444444,
		power: 1632.32494780193,
		road: 21359.0697222223,
		acceleration: 0.0987037037037046
	},
	{
		id: 2403,
		time: 2402,
		velocity: 6.745,
		power: -464.676225483527,
		road: 21365.8122685186,
		acceleration: -0.225462962962964
	},
	{
		id: 2404,
		time: 2403,
		velocity: 6.2375,
		power: -2447.96253538057,
		road: 21372.1643518519,
		acceleration: -0.555462962962963
	},
	{
		id: 2405,
		time: 2404,
		velocity: 5.24055555555556,
		power: -5869.35193821285,
		road: 21377.5982870371,
		acceleration: -1.28083333333333
	},
	{
		id: 2406,
		time: 2405,
		velocity: 2.9025,
		power: -4133.50339573694,
		road: 21381.8057870371,
		acceleration: -1.17203703703704
	},
	{
		id: 2407,
		time: 2406,
		velocity: 2.72138888888889,
		power: -1249.29155078753,
		road: 21385.1644444445,
		acceleration: -0.525648148148148
	},
	{
		id: 2408,
		time: 2407,
		velocity: 3.66361111111111,
		power: 2243.33747202087,
		road: 21388.5431018519,
		acceleration: 0.565648148148148
	},
	{
		id: 2409,
		time: 2408,
		velocity: 4.59944444444444,
		power: 3071.51392272182,
		road: 21392.540925926,
		acceleration: 0.672685185185185
	},
	{
		id: 2410,
		time: 2409,
		velocity: 4.73944444444444,
		power: 2432.02339871501,
		road: 21397.087175926,
		acceleration: 0.424166666666666
	},
	{
		id: 2411,
		time: 2410,
		velocity: 4.93611111111111,
		power: 1326.9837683688,
		road: 21401.9197685186,
		acceleration: 0.148518518518519
	},
	{
		id: 2412,
		time: 2411,
		velocity: 5.045,
		power: 425.866563351027,
		road: 21406.8021296297,
		acceleration: -0.0489814814814817
	},
	{
		id: 2413,
		time: 2412,
		velocity: 4.5925,
		power: 380.36528317904,
		road: 21411.6312037037,
		acceleration: -0.0575925925925924
	},
	{
		id: 2414,
		time: 2413,
		velocity: 4.76333333333333,
		power: 640.866164777215,
		road: 21416.4315740741,
		acceleration: 0.00018518518518551
	},
	{
		id: 2415,
		time: 2414,
		velocity: 5.04555555555556,
		power: 1216.88234442957,
		road: 21421.293425926,
		acceleration: 0.122777777777777
	},
	{
		id: 2416,
		time: 2415,
		velocity: 4.96083333333333,
		power: 938.74353857272,
		road: 21426.2458333334,
		acceleration: 0.0583333333333336
	},
	{
		id: 2417,
		time: 2416,
		velocity: 4.93833333333333,
		power: 373.965532130773,
		road: 21431.1965740741,
		acceleration: -0.0616666666666665
	},
	{
		id: 2418,
		time: 2417,
		velocity: 4.86055555555556,
		power: 174.200512142592,
		road: 21436.0649537037,
		acceleration: -0.103055555555556
	},
	{
		id: 2419,
		time: 2418,
		velocity: 4.65166666666667,
		power: 493.128923106707,
		road: 21440.8656944445,
		acceleration: -0.0322222222222219
	},
	{
		id: 2420,
		time: 2419,
		velocity: 4.84166666666667,
		power: 1869.06385144561,
		road: 21445.78,
		acceleration: 0.259351851851851
	},
	{
		id: 2421,
		time: 2420,
		velocity: 5.63861111111111,
		power: 3835.78821661181,
		road: 21451.1294907408,
		acceleration: 0.611018518518518
	},
	{
		id: 2422,
		time: 2421,
		velocity: 6.48472222222222,
		power: 4672.25814208731,
		road: 21457.1208333334,
		acceleration: 0.672685185185187
	},
	{
		id: 2423,
		time: 2422,
		velocity: 6.85972222222222,
		power: 2883.69097875948,
		road: 21463.6067592593,
		acceleration: 0.316481481481481
	},
	{
		id: 2424,
		time: 2423,
		velocity: 6.58805555555556,
		power: -336.318069621942,
		road: 21470.1481481482,
		acceleration: -0.205555555555556
	},
	{
		id: 2425,
		time: 2424,
		velocity: 5.86805555555556,
		power: -2115.15604874692,
		road: 21476.3324074074,
		acceleration: -0.508703703703704
	},
	{
		id: 2426,
		time: 2425,
		velocity: 5.33361111111111,
		power: -2374.90282626865,
		road: 21481.967962963,
		acceleration: -0.588703703703704
	},
	{
		id: 2427,
		time: 2426,
		velocity: 4.82194444444445,
		power: -1872.99575277865,
		road: 21487.0440277778,
		acceleration: -0.530277777777776
	},
	{
		id: 2428,
		time: 2427,
		velocity: 4.27722222222222,
		power: -1936.77364056615,
		road: 21491.5597222223,
		acceleration: -0.590462962962963
	},
	{
		id: 2429,
		time: 2428,
		velocity: 3.56222222222222,
		power: -1845.29156714121,
		road: 21495.4632407408,
		acceleration: -0.633888888888889
	},
	{
		id: 2430,
		time: 2429,
		velocity: 2.92027777777778,
		power: -1456.2417535558,
		road: 21498.7496296297,
		acceleration: -0.600370370370371
	},
	{
		id: 2431,
		time: 2430,
		velocity: 2.47611111111111,
		power: -989.918711613825,
		road: 21501.4788888889,
		acceleration: -0.513888888888889
	},
	{
		id: 2432,
		time: 2431,
		velocity: 2.02055555555556,
		power: -606.927336902578,
		road: 21503.7448611111,
		acceleration: -0.412685185185185
	},
	{
		id: 2433,
		time: 2432,
		velocity: 1.68222222222222,
		power: -593.992591852927,
		road: 21505.5680555556,
		acceleration: -0.47287037037037
	},
	{
		id: 2434,
		time: 2433,
		velocity: 1.0575,
		power: -462.013816220627,
		road: 21506.9088425926,
		acceleration: -0.491944444444445
	},
	{
		id: 2435,
		time: 2434,
		velocity: 0.544722222222222,
		power: -334.13017407848,
		road: 21507.7232870371,
		acceleration: -0.560740740740741
	},
	{
		id: 2436,
		time: 2435,
		velocity: 0,
		power: -76.2564522519856,
		road: 21508.0811111111,
		acceleration: -0.3525
	},
	{
		id: 2437,
		time: 2436,
		velocity: 0,
		power: -4.64826444119558,
		road: 21508.1718981482,
		acceleration: -0.181574074074074
	},
	{
		id: 2438,
		time: 2437,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2439,
		time: 2438,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2440,
		time: 2439,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2441,
		time: 2440,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2442,
		time: 2441,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2443,
		time: 2442,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2444,
		time: 2443,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2445,
		time: 2444,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2446,
		time: 2445,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2447,
		time: 2446,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2448,
		time: 2447,
		velocity: 0,
		power: 0,
		road: 21508.1718981482,
		acceleration: 0
	},
	{
		id: 2449,
		time: 2448,
		velocity: 0,
		power: 5.50460798038257,
		road: 21508.2026388889,
		acceleration: 0.0614814814814815
	},
	{
		id: 2450,
		time: 2449,
		velocity: 0.184444444444444,
		power: 7.42818932008937,
		road: 21508.2641203704,
		acceleration: 0
	},
	{
		id: 2451,
		time: 2450,
		velocity: 0,
		power: 7.42818932008937,
		road: 21508.3256018519,
		acceleration: 0
	},
	{
		id: 2452,
		time: 2451,
		velocity: 0,
		power: 1.92351825860949,
		road: 21508.3563425926,
		acceleration: -0.0614814814814815
	},
	{
		id: 2453,
		time: 2452,
		velocity: 0,
		power: 0,
		road: 21508.3563425926,
		acceleration: 0
	},
	{
		id: 2454,
		time: 2453,
		velocity: 0,
		power: 0,
		road: 21508.3563425926,
		acceleration: 0
	},
	{
		id: 2455,
		time: 2454,
		velocity: 0,
		power: 0,
		road: 21508.3563425926,
		acceleration: 0
	}
];
export default test4;
