export const test6 = 
[
	{
		id: 1,
		time: 0,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 3,
		time: 2,
		velocity: 0,
		power: 4.45671958135863,
		road: 0.0261574074074074,
		acceleration: 0.0523148148148148
	},
	{
		id: 4,
		time: 3,
		velocity: 0.156944444444444,
		power: 6.32064351619354,
		road: 0.0784722222222222,
		acceleration: 0
	},
	{
		id: 5,
		time: 4,
		velocity: 0,
		power: 6.32064351619354,
		road: 0.130787037037037,
		acceleration: 0
	},
	{
		id: 6,
		time: 5,
		velocity: 0,
		power: 7.30277106555348,
		road: 0.187407407407407,
		acceleration: 0.00861111111111112
	},
	{
		id: 7,
		time: 6,
		velocity: 0.182777777777778,
		power: 7.36106487820937,
		road: 0.248333333333333,
		acceleration: 0
	},
	{
		id: 8,
		time: 7,
		velocity: 0,
		power: 7.36106487820937,
		road: 0.309259259259259,
		acceleration: 0
	},
	{
		id: 9,
		time: 8,
		velocity: 0,
		power: 1.92217020792723,
		road: 0.339722222222222,
		acceleration: -0.0609259259259259
	},
	{
		id: 10,
		time: 9,
		velocity: 0,
		power: 0,
		road: 0.339722222222222,
		acceleration: 0
	},
	{
		id: 11,
		time: 10,
		velocity: 0,
		power: 0,
		road: 0.339722222222222,
		acceleration: 0
	},
	{
		id: 12,
		time: 11,
		velocity: 0,
		power: 0,
		road: 0.339722222222222,
		acceleration: 0
	},
	{
		id: 13,
		time: 12,
		velocity: 0,
		power: 0,
		road: 0.339722222222222,
		acceleration: 0
	},
	{
		id: 14,
		time: 13,
		velocity: 0,
		power: 0,
		road: 0.339722222222222,
		acceleration: 0
	},
	{
		id: 15,
		time: 14,
		velocity: 0,
		power: 0,
		road: 0.339722222222222,
		acceleration: 0
	},
	{
		id: 16,
		time: 15,
		velocity: 0,
		power: 0,
		road: 0.339722222222222,
		acceleration: 0
	},
	{
		id: 17,
		time: 16,
		velocity: 0,
		power: 0,
		road: 0.339722222222222,
		acceleration: 0
	},
	{
		id: 18,
		time: 17,
		velocity: 0,
		power: 0,
		road: 0.339722222222222,
		acceleration: 0
	},
	{
		id: 19,
		time: 18,
		velocity: 0,
		power: 15.2022709688593,
		road: 0.402916666666667,
		acceleration: 0.126388888888889
	},
	{
		id: 20,
		time: 19,
		velocity: 0.379166666666667,
		power: 15.2711355049701,
		road: 0.529305555555555,
		acceleration: 0
	},
	{
		id: 21,
		time: 20,
		velocity: 0,
		power: 15.2711355049701,
		road: 0.655694444444444,
		acceleration: 0
	},
	{
		id: 22,
		time: 21,
		velocity: 0,
		power: 0.0683165204678363,
		road: 0.718888888888889,
		acceleration: -0.126388888888889
	},
	{
		id: 23,
		time: 22,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 24,
		time: 23,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 25,
		time: 24,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 26,
		time: 25,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 27,
		time: 26,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 28,
		time: 27,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 29,
		time: 28,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 30,
		time: 29,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 31,
		time: 30,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 32,
		time: 31,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 33,
		time: 32,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 34,
		time: 33,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 35,
		time: 34,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 36,
		time: 35,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 37,
		time: 36,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 38,
		time: 37,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 39,
		time: 38,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 40,
		time: 39,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 41,
		time: 40,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 42,
		time: 41,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 43,
		time: 42,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 44,
		time: 43,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 45,
		time: 44,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 46,
		time: 45,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 47,
		time: 46,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 48,
		time: 47,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 49,
		time: 48,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 50,
		time: 49,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 51,
		time: 50,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 52,
		time: 51,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 53,
		time: 52,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 54,
		time: 53,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 55,
		time: 54,
		velocity: 0,
		power: 0,
		road: 0.718888888888889,
		acceleration: 0
	},
	{
		id: 56,
		time: 55,
		velocity: 0,
		power: 12.1794459594388,
		road: 0.773287037037037,
		acceleration: 0.108796296296296
	},
	{
		id: 57,
		time: 56,
		velocity: 0.326388888888889,
		power: 13.1452385725488,
		road: 0.882083333333333,
		acceleration: 0
	},
	{
		id: 58,
		time: 57,
		velocity: 0,
		power: 37.6104618188621,
		road: 1.04726851851852,
		acceleration: 0.112777777777778
	},
	{
		id: 59,
		time: 58,
		velocity: 0.338333333333333,
		power: 2.96814570242714,
		road: 1.21444444444444,
		acceleration: -0.108796296296296
	},
	{
		id: 60,
		time: 59,
		velocity: 0,
		power: 13.6263523782706,
		road: 1.32722222222222,
		acceleration: 0
	},
	{
		id: 61,
		time: 60,
		velocity: 0,
		power: 0.788079239766083,
		road: 1.38361111111111,
		acceleration: -0.112777777777778
	},
	{
		id: 62,
		time: 61,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 63,
		time: 62,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 64,
		time: 63,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 65,
		time: 64,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 66,
		time: 65,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 67,
		time: 66,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 68,
		time: 67,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 69,
		time: 68,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 70,
		time: 69,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 71,
		time: 70,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 72,
		time: 71,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 73,
		time: 72,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 74,
		time: 73,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 75,
		time: 74,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 76,
		time: 75,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 77,
		time: 76,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 78,
		time: 77,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 79,
		time: 78,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 80,
		time: 79,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 81,
		time: 80,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 82,
		time: 81,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 83,
		time: 82,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 84,
		time: 83,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 85,
		time: 84,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 86,
		time: 85,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 87,
		time: 86,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 88,
		time: 87,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 89,
		time: 88,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 90,
		time: 89,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 91,
		time: 90,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 92,
		time: 91,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 93,
		time: 92,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 94,
		time: 93,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 95,
		time: 94,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 96,
		time: 95,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 97,
		time: 96,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 98,
		time: 97,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 99,
		time: 98,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 100,
		time: 99,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 101,
		time: 100,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 102,
		time: 101,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 103,
		time: 102,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 104,
		time: 103,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 105,
		time: 104,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 106,
		time: 105,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 107,
		time: 106,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 108,
		time: 107,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 109,
		time: 108,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 110,
		time: 109,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 111,
		time: 110,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 112,
		time: 111,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 113,
		time: 112,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 114,
		time: 113,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 115,
		time: 114,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 116,
		time: 115,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 117,
		time: 116,
		velocity: 0,
		power: 0,
		road: 1.38361111111111,
		acceleration: 0
	},
	{
		id: 118,
		time: 117,
		velocity: 0,
		power: 4.21467546003427,
		road: 1.40865740740741,
		acceleration: 0.0500925925925926
	},
	{
		id: 119,
		time: 118,
		velocity: 0.150277777777778,
		power: 6.05214981545515,
		road: 1.45875,
		acceleration: 0
	},
	{
		id: 120,
		time: 119,
		velocity: 0,
		power: 6.05214981545515,
		road: 1.50884259259259,
		acceleration: 0
	},
	{
		id: 121,
		time: 120,
		velocity: 0,
		power: 1.83744023716699,
		road: 1.53388888888889,
		acceleration: -0.0500925925925926
	},
	{
		id: 122,
		time: 121,
		velocity: 0,
		power: 0,
		road: 1.53388888888889,
		acceleration: 0
	},
	{
		id: 123,
		time: 122,
		velocity: 0,
		power: 0,
		road: 1.53388888888889,
		acceleration: 0
	},
	{
		id: 124,
		time: 123,
		velocity: 0,
		power: 0,
		road: 1.53388888888889,
		acceleration: 0
	},
	{
		id: 125,
		time: 124,
		velocity: 0,
		power: 0,
		road: 1.53388888888889,
		acceleration: 0
	},
	{
		id: 126,
		time: 125,
		velocity: 0,
		power: 0,
		road: 1.53388888888889,
		acceleration: 0
	},
	{
		id: 127,
		time: 126,
		velocity: 0,
		power: 0,
		road: 1.53388888888889,
		acceleration: 0
	},
	{
		id: 128,
		time: 127,
		velocity: 0,
		power: 0,
		road: 1.53388888888889,
		acceleration: 0
	},
	{
		id: 129,
		time: 128,
		velocity: 0,
		power: 0,
		road: 1.53388888888889,
		acceleration: 0
	},
	{
		id: 130,
		time: 129,
		velocity: 0,
		power: 59.61788190151,
		road: 1.68222222222222,
		acceleration: 0.296666666666667
	},
	{
		id: 131,
		time: 130,
		velocity: 0.89,
		power: 376.48496125244,
		road: 2.25925925925926,
		acceleration: 0.560740740740741
	},
	{
		id: 132,
		time: 131,
		velocity: 1.68222222222222,
		power: 703.664222791778,
		road: 3.38287037037037,
		acceleration: 0.532407407407407
	},
	{
		id: 133,
		time: 132,
		velocity: 1.59722222222222,
		power: 511.251311151138,
		road: 4.8875,
		acceleration: 0.22962962962963
	},
	{
		id: 134,
		time: 133,
		velocity: 1.57888888888889,
		power: 173.36462312988,
		road: 6.49921296296296,
		acceleration: -0.015462962962963
	},
	{
		id: 135,
		time: 134,
		velocity: 1.63583333333333,
		power: 138.89559386078,
		road: 8.0849537037037,
		acceleration: -0.0364814814814816
	},
	{
		id: 136,
		time: 135,
		velocity: 1.48777777777778,
		power: -161.143180322163,
		road: 9.52930555555556,
		acceleration: -0.246296296296296
	},
	{
		id: 137,
		time: 136,
		velocity: 0.84,
		power: -414.638400686408,
		road: 10.5778703703704,
		acceleration: -0.545277777777778
	},
	{
		id: 138,
		time: 137,
		velocity: 0,
		power: -184.240123169485,
		road: 11.1058333333333,
		acceleration: -0.495925925925926
	},
	{
		id: 139,
		time: 138,
		velocity: 0,
		power: 86.8806933161717,
		road: 11.4537962962963,
		acceleration: 0.135925925925926
	},
	{
		id: 140,
		time: 139,
		velocity: 1.24777777777778,
		power: 50.2903558161975,
		road: 11.8697222222222,
		acceleration: 0
	},
	{
		id: 141,
		time: 140,
		velocity: 0,
		power: 50.2903558161975,
		road: 12.2856481481481,
		acceleration: 0
	},
	{
		id: 142,
		time: 141,
		velocity: 0,
		power: -56.8190569850552,
		road: 12.4936111111111,
		acceleration: -0.415925925925926
	},
	{
		id: 143,
		time: 142,
		velocity: 0,
		power: 38.6208947504913,
		road: 12.6080092592593,
		acceleration: 0.228796296296296
	},
	{
		id: 144,
		time: 143,
		velocity: 0.686388888888889,
		power: 168.958838209131,
		road: 13.0002777777778,
		acceleration: 0.326944444444444
	},
	{
		id: 145,
		time: 144,
		velocity: 0.980833333333333,
		power: 376.994001938726,
		road: 13.755462962963,
		acceleration: 0.398888888888889
	},
	{
		id: 146,
		time: 145,
		velocity: 1.19666666666667,
		power: 423.407504743611,
		road: 14.8500462962963,
		acceleration: 0.279907407407407
	},
	{
		id: 147,
		time: 146,
		velocity: 1.52611111111111,
		power: 97.0177677704437,
		road: 16.0626388888889,
		acceleration: -0.0438888888888889
	},
	{
		id: 148,
		time: 147,
		velocity: 0.849166666666667,
		power: -107.115929710897,
		road: 17.1366203703704,
		acceleration: -0.233333333333334
	},
	{
		id: 149,
		time: 148,
		velocity: 0.496666666666667,
		power: -253.771510817523,
		road: 17.8395833333333,
		acceleration: -0.508703703703704
	},
	{
		id: 150,
		time: 149,
		velocity: 0,
		power: -45.2410891348123,
		road: 18.1466666666667,
		acceleration: -0.283055555555556
	},
	{
		id: 151,
		time: 150,
		velocity: 0,
		power: 27.4980559282547,
		road: 18.3283333333333,
		acceleration: 0.0322222222222223
	},
	{
		id: 152,
		time: 151,
		velocity: 0.593333333333333,
		power: 150.112137218252,
		road: 18.6846296296296,
		acceleration: 0.317037037037037
	},
	{
		id: 153,
		time: 152,
		velocity: 0.951111111111111,
		power: 765.792872314318,
		road: 19.5843981481482,
		acceleration: 0.769907407407407
	},
	{
		id: 154,
		time: 153,
		velocity: 2.30972222222222,
		power: 1390.36361908281,
		road: 21.2458333333333,
		acceleration: 0.753425925925926
	},
	{
		id: 155,
		time: 154,
		velocity: 2.85361111111111,
		power: 2431.6331184704,
		road: 23.7336111111111,
		acceleration: 0.899259259259259
	},
	{
		id: 156,
		time: 155,
		velocity: 3.64888888888889,
		power: 2377.4108377358,
		road: 26.9890277777778,
		acceleration: 0.636018518518518
	},
	{
		id: 157,
		time: 156,
		velocity: 4.21777777777778,
		power: 2551.20141373748,
		road: 30.8431481481482,
		acceleration: 0.561388888888889
	},
	{
		id: 158,
		time: 157,
		velocity: 4.53777777777778,
		power: 1850.42566056936,
		road: 35.1360185185185,
		acceleration: 0.316111111111112
	},
	{
		id: 159,
		time: 158,
		velocity: 4.59722222222222,
		power: 733.230386738368,
		road: 39.6040277777778,
		acceleration: 0.0341666666666667
	},
	{
		id: 160,
		time: 159,
		velocity: 4.32027777777778,
		power: -447.860090793899,
		road: 43.9660185185185,
		acceleration: -0.246203703703705
	},
	{
		id: 161,
		time: 160,
		velocity: 3.79916666666667,
		power: -153.844415332778,
		road: 48.1168518518519,
		acceleration: -0.17611111111111
	},
	{
		id: 162,
		time: 161,
		velocity: 4.06888888888889,
		power: 950.595230991043,
		road: 52.2327777777778,
		acceleration: 0.106296296296296
	},
	{
		id: 163,
		time: 162,
		velocity: 4.63916666666667,
		power: 4317.31125201846,
		road: 56.8268518518519,
		acceleration: 0.85
	},
	{
		id: 164,
		time: 163,
		velocity: 6.34916666666667,
		power: 5608.20448903048,
		road: 62.3116666666667,
		acceleration: 0.931481481481482
	},
	{
		id: 165,
		time: 164,
		velocity: 6.86333333333333,
		power: 7145.96915378648,
		road: 68.7686574074074,
		acceleration: 1.01287037037037
	},
	{
		id: 166,
		time: 165,
		velocity: 7.67777777777778,
		power: 5291.32376623608,
		road: 76.0361574074074,
		acceleration: 0.608148148148149
	},
	{
		id: 167,
		time: 166,
		velocity: 8.17361111111111,
		power: 5337.78595113371,
		road: 83.8840277777778,
		acceleration: 0.552592592592592
	},
	{
		id: 168,
		time: 167,
		velocity: 8.52111111111111,
		power: 4094.7283518059,
		road: 92.184212962963,
		acceleration: 0.352037037037038
	},
	{
		id: 169,
		time: 168,
		velocity: 8.73388888888889,
		power: 4727.80129658232,
		road: 100.86162037037,
		acceleration: 0.402407407407408
	},
	{
		id: 170,
		time: 169,
		velocity: 9.38083333333333,
		power: 4600.33052111909,
		road: 109.920046296296,
		acceleration: 0.35962962962963
	},
	{
		id: 171,
		time: 170,
		velocity: 9.6,
		power: 3566.5049672831,
		road: 119.270185185185,
		acceleration: 0.223796296296296
	},
	{
		id: 172,
		time: 171,
		velocity: 9.40527777777778,
		power: -725.694743695591,
		road: 128.603148148148,
		acceleration: -0.258148148148148
	},
	{
		id: 173,
		time: 172,
		velocity: 8.60638888888889,
		power: -2593.97314905338,
		road: 137.56875,
		acceleration: -0.476574074074076
	},
	{
		id: 174,
		time: 173,
		velocity: 8.17027777777778,
		power: -4160.47281344389,
		road: 145.951851851852,
		acceleration: -0.688425925925923
	},
	{
		id: 175,
		time: 174,
		velocity: 7.34,
		power: -4563.06871881176,
		road: 153.596898148148,
		acceleration: -0.787685185185187
	},
	{
		id: 176,
		time: 175,
		velocity: 6.24333333333333,
		power: -4937.76436516833,
		road: 160.389166666667,
		acceleration: -0.91787037037037
	},
	{
		id: 177,
		time: 176,
		velocity: 5.41666666666667,
		power: -5051.28152315154,
		road: 166.191296296296,
		acceleration: -1.06240740740741
	},
	{
		id: 178,
		time: 177,
		velocity: 4.15277777777778,
		power: -3149.76003012409,
		road: 171.050694444444,
		acceleration: -0.823055555555555
	},
	{
		id: 179,
		time: 178,
		velocity: 3.77416666666667,
		power: -2100.78133914217,
		road: 175.160972222222,
		acceleration: -0.675185185185185
	},
	{
		id: 180,
		time: 179,
		velocity: 3.39111111111111,
		power: 531.724587465524,
		road: 178.940046296296,
		acceleration: 0.0127777777777776
	},
	{
		id: 181,
		time: 180,
		velocity: 4.19111111111111,
		power: 3175.59277506094,
		road: 183.062592592593,
		acceleration: 0.674166666666666
	},
	{
		id: 182,
		time: 181,
		velocity: 5.79666666666667,
		power: 6618.10735238654,
		road: 188.137453703704,
		acceleration: 1.23046296296296
	},
	{
		id: 183,
		time: 182,
		velocity: 7.0825,
		power: 10414.59476571,
		road: 194.599398148148,
		acceleration: 1.5437037037037
	},
	{
		id: 184,
		time: 183,
		velocity: 8.82222222222222,
		power: 12570.3626895883,
		road: 202.579027777778,
		acceleration: 1.49166666666667
	},
	{
		id: 185,
		time: 184,
		velocity: 10.2716666666667,
		power: 13873.2158528802,
		road: 211.989583333333,
		acceleration: 1.37018518518519
	},
	{
		id: 186,
		time: 185,
		velocity: 11.1930555555556,
		power: 13766.0961370671,
		road: 222.665694444444,
		acceleration: 1.16092592592593
	},
	{
		id: 187,
		time: 186,
		velocity: 12.305,
		power: 9051.43112324214,
		road: 234.231157407407,
		acceleration: 0.617777777777777
	},
	{
		id: 188,
		time: 187,
		velocity: 12.125,
		power: 6277.34400206779,
		road: 246.27412037037,
		acceleration: 0.337222222222223
	},
	{
		id: 189,
		time: 188,
		velocity: 12.2047222222222,
		power: 3005.06307537708,
		road: 258.508518518519,
		acceleration: 0.0456481481481461
	},
	{
		id: 190,
		time: 189,
		velocity: 12.4419444444444,
		power: 4943.55884523419,
		road: 270.868564814815,
		acceleration: 0.20564814814815
	},
	{
		id: 191,
		time: 190,
		velocity: 12.7419444444444,
		power: 5980.12505030998,
		road: 283.471574074074,
		acceleration: 0.28027777777778
	},
	{
		id: 192,
		time: 191,
		velocity: 13.0455555555556,
		power: 6449.14232842386,
		road: 296.366157407407,
		acceleration: 0.302870370370368
	},
	{
		id: 193,
		time: 192,
		velocity: 13.3505555555556,
		power: 2705.46854138693,
		road: 309.409166666667,
		acceleration: -0.0060185185185162
	},
	{
		id: 194,
		time: 193,
		velocity: 12.7238888888889,
		power: -192.999446764994,
		road: 322.330555555556,
		acceleration: -0.237222222222224
	},
	{
		id: 195,
		time: 194,
		velocity: 12.3338888888889,
		power: -4345.56201683005,
		road: 334.843472222222,
		acceleration: -0.579722222222221
	},
	{
		id: 196,
		time: 195,
		velocity: 11.6113888888889,
		power: -2428.4801687177,
		road: 346.856157407407,
		acceleration: -0.42074074074074
	},
	{
		id: 197,
		time: 196,
		velocity: 11.4616666666667,
		power: -2190.31905902832,
		road: 358.457824074074,
		acceleration: -0.401296296296298
	},
	{
		id: 198,
		time: 197,
		velocity: 11.13,
		power: 406.180312539967,
		road: 369.777824074074,
		acceleration: -0.162037037037036
	},
	{
		id: 199,
		time: 198,
		velocity: 11.1252777777778,
		power: -740.010115424156,
		road: 380.88337962963,
		acceleration: -0.266851851851852
	},
	{
		id: 200,
		time: 199,
		velocity: 10.6611111111111,
		power: -1979.11864324456,
		road: 391.662731481482,
		acceleration: -0.385555555555554
	},
	{
		id: 201,
		time: 200,
		velocity: 9.97333333333333,
		power: -3715.86107211198,
		road: 401.966435185185,
		acceleration: -0.565740740740742
	},
	{
		id: 202,
		time: 201,
		velocity: 9.42805555555556,
		power: -3351.41299106603,
		road: 411.716342592593,
		acceleration: -0.541851851851851
	},
	{
		id: 203,
		time: 202,
		velocity: 9.03555555555556,
		power: 479.057621014922,
		road: 421.133333333333,
		acceleration: -0.123981481481483
	},
	{
		id: 204,
		time: 203,
		velocity: 9.60138888888889,
		power: 4487.34046687198,
		road: 430.646712962963,
		acceleration: 0.316759259259259
	},
	{
		id: 205,
		time: 204,
		velocity: 10.3783333333333,
		power: 7690.82901522238,
		road: 440.63087962963,
		acceleration: 0.624814814814815
	},
	{
		id: 206,
		time: 205,
		velocity: 10.91,
		power: 5188.26487706819,
		road: 451.093055555556,
		acceleration: 0.331203703703704
	},
	{
		id: 207,
		time: 206,
		velocity: 10.595,
		power: 3440.6604358278,
		road: 461.793518518519,
		acceleration: 0.145370370370369
	},
	{
		id: 208,
		time: 207,
		velocity: 10.8144444444444,
		power: 2442.67364143855,
		road: 472.588796296296,
		acceleration: 0.0442592592592614
	},
	{
		id: 209,
		time: 208,
		velocity: 11.0427777777778,
		power: 4434.19769300533,
		road: 483.521527777778,
		acceleration: 0.230648148148148
	},
	{
		id: 210,
		time: 209,
		velocity: 11.2869444444444,
		power: 4181.4945494809,
		road: 494.667592592593,
		acceleration: 0.196018518518519
	},
	{
		id: 211,
		time: 210,
		velocity: 11.4025,
		power: 3129.99516672606,
		road: 505.957407407407,
		acceleration: 0.0914814814814804
	},
	{
		id: 212,
		time: 211,
		velocity: 11.3172222222222,
		power: 1444.98037892995,
		road: 517.260277777778,
		acceleration: -0.0653703703703723
	},
	{
		id: 213,
		time: 212,
		velocity: 11.0908333333333,
		power: 654.834423877691,
		road: 528.462037037037,
		acceleration: -0.136851851851851
	},
	{
		id: 214,
		time: 213,
		velocity: 10.9919444444444,
		power: 1451.43471575074,
		road: 539.565462962963,
		acceleration: -0.0598148148148159
	},
	{
		id: 215,
		time: 214,
		velocity: 11.1377777777778,
		power: 1797.48915386405,
		road: 550.626018518518,
		acceleration: -0.0259259259259252
	},
	{
		id: 216,
		time: 215,
		velocity: 11.0130555555556,
		power: 2087.51908644522,
		road: 561.674583333333,
		acceleration: 0.00194444444444386
	},
	{
		id: 217,
		time: 216,
		velocity: 10.9977777777778,
		power: 1634.4423276671,
		road: 572.703842592592,
		acceleration: -0.040555555555553
	},
	{
		id: 218,
		time: 217,
		velocity: 11.0161111111111,
		power: 1831.6722686439,
		road: 583.702361111111,
		acceleration: -0.0209259259259262
	},
	{
		id: 219,
		time: 218,
		velocity: 10.9502777777778,
		power: 2381.04387136104,
		road: 594.706064814815,
		acceleration: 0.0312962962962953
	},
	{
		id: 220,
		time: 219,
		velocity: 11.0916666666667,
		power: 1958.02458303961,
		road: 605.72074074074,
		acceleration: -0.00935185185185183
	},
	{
		id: 221,
		time: 220,
		velocity: 10.9880555555556,
		power: 1848.71723576605,
		road: 616.721064814815,
		acceleration: -0.0193518518518516
	},
	{
		id: 222,
		time: 221,
		velocity: 10.8922222222222,
		power: -261.195965373061,
		road: 627.602037037037,
		acceleration: -0.219351851851851
	},
	{
		id: 223,
		time: 222,
		velocity: 10.4336111111111,
		power: -3965.77569611589,
		road: 638.080092592592,
		acceleration: -0.586481481481483
	},
	{
		id: 224,
		time: 223,
		velocity: 9.22861111111111,
		power: -4965.53157684793,
		road: 647.908796296296,
		acceleration: -0.712222222222222
	},
	{
		id: 225,
		time: 224,
		velocity: 8.75555555555555,
		power: -5407.16360106498,
		road: 656.981527777778,
		acceleration: -0.799722222222222
	},
	{
		id: 226,
		time: 225,
		velocity: 8.03444444444444,
		power: -2749.28423654583,
		road: 665.399166666666,
		acceleration: -0.510462962962963
	},
	{
		id: 227,
		time: 226,
		velocity: 7.69722222222222,
		power: -2704.74612716438,
		road: 673.300416666666,
		acceleration: -0.522314814814815
	},
	{
		id: 228,
		time: 227,
		velocity: 7.18861111111111,
		power: -1578.48805507264,
		road: 680.749814814815,
		acceleration: -0.381388888888889
	},
	{
		id: 229,
		time: 228,
		velocity: 6.89027777777778,
		power: -1166.50276718835,
		road: 687.844212962963,
		acceleration: -0.32861111111111
	},
	{
		id: 230,
		time: 229,
		velocity: 6.71138888888889,
		power: 722.151550874241,
		road: 694.752129629629,
		acceleration: -0.044351851851852
	},
	{
		id: 231,
		time: 230,
		velocity: 7.05555555555556,
		power: 4108.74757151209,
		road: 701.863611111111,
		acceleration: 0.451481481481482
	},
	{
		id: 232,
		time: 231,
		velocity: 8.24472222222222,
		power: 6055.23821379386,
		road: 709.535277777777,
		acceleration: 0.668888888888888
	},
	{
		id: 233,
		time: 232,
		velocity: 8.71805555555556,
		power: 4619.93129279171,
		road: 717.753935185185,
		acceleration: 0.425092592592593
	},
	{
		id: 234,
		time: 233,
		velocity: 8.33083333333333,
		power: -1123.45503134818,
		road: 726.030833333333,
		acceleration: -0.308611111111112
	},
	{
		id: 235,
		time: 234,
		velocity: 7.31888888888889,
		power: -3623.88121670386,
		road: 733.828379629629,
		acceleration: -0.650092592592592
	},
	{
		id: 236,
		time: 235,
		velocity: 6.76777777777778,
		power: -4108.01895758219,
		road: 740.918425925926,
		acceleration: -0.764907407407406
	},
	{
		id: 237,
		time: 236,
		velocity: 6.03611111111111,
		power: -2872.42212375408,
		road: 747.314629629629,
		acceleration: -0.622777777777777
	},
	{
		id: 238,
		time: 237,
		velocity: 5.45055555555556,
		power: -5419.72262158102,
		road: 752.808101851851,
		acceleration: -1.18268518518519
	},
	{
		id: 239,
		time: 238,
		velocity: 3.21972222222222,
		power: -3879.90998435228,
		road: 757.173148148148,
		acceleration: -1.07416666666667
	},
	{
		id: 240,
		time: 239,
		velocity: 2.81361111111111,
		power: -2491.04792364937,
		road: 760.545,
		acceleration: -0.912222222222222
	},
	{
		id: 241,
		time: 240,
		velocity: 2.71388888888889,
		power: -337.740960008199,
		road: 763.330972222222,
		acceleration: -0.259537037037037
	},
	{
		id: 242,
		time: 241,
		velocity: 2.44111111111111,
		power: 270.051465034429,
		road: 765.975324074074,
		acceleration: -0.0237037037037036
	},
	{
		id: 243,
		time: 242,
		velocity: 2.7425,
		power: 798.546288425547,
		road: 768.696666666666,
		acceleration: 0.177685185185185
	},
	{
		id: 244,
		time: 243,
		velocity: 3.24694444444444,
		power: 1714.24604522988,
		road: 771.737546296296,
		acceleration: 0.461388888888889
	},
	{
		id: 245,
		time: 244,
		velocity: 3.82527777777778,
		power: 3036.93910415154,
		road: 775.380694444444,
		acceleration: 0.743148148148149
	},
	{
		id: 246,
		time: 245,
		velocity: 4.97194444444444,
		power: 3542.09869155484,
		road: 779.752824074074,
		acceleration: 0.714814814814814
	},
	{
		id: 247,
		time: 246,
		velocity: 5.39138888888889,
		power: 2450.66777386532,
		road: 784.673935185185,
		acceleration: 0.383148148148147
	},
	{
		id: 248,
		time: 247,
		velocity: 4.97472222222222,
		power: 757.116915257435,
		road: 789.793379629629,
		acceleration: 0.0135185185185192
	},
	{
		id: 249,
		time: 248,
		velocity: 5.0125,
		power: 1080.09159672849,
		road: 794.958425925926,
		acceleration: 0.0776851851851861
	},
	{
		id: 250,
		time: 249,
		velocity: 5.62444444444444,
		power: 2359.0598975616,
		road: 800.321944444444,
		acceleration: 0.319259259259258
	},
	{
		id: 251,
		time: 250,
		velocity: 5.9325,
		power: 2825.52515694161,
		road: 806.0325,
		acceleration: 0.374814814814815
	},
	{
		id: 252,
		time: 251,
		velocity: 6.13694444444444,
		power: 1711.75094258606,
		road: 812.007407407407,
		acceleration: 0.153888888888888
	},
	{
		id: 253,
		time: 252,
		velocity: 6.08611111111111,
		power: 971.273832607561,
		road: 818.06949074074,
		acceleration: 0.0204629629629629
	},
	{
		id: 254,
		time: 253,
		velocity: 5.99388888888889,
		power: 1383.46612016245,
		road: 824.186527777777,
		acceleration: 0.0894444444444451
	},
	{
		id: 255,
		time: 254,
		velocity: 6.40527777777778,
		power: 2854.59488471038,
		road: 830.510694444444,
		acceleration: 0.324814814814815
	},
	{
		id: 256,
		time: 255,
		velocity: 7.06055555555556,
		power: 5149.69499013584,
		road: 837.318194444444,
		acceleration: 0.641851851851853
	},
	{
		id: 257,
		time: 256,
		velocity: 7.91944444444444,
		power: 5627.39600234511,
		road: 844.764444444444,
		acceleration: 0.635648148148147
	},
	{
		id: 258,
		time: 257,
		velocity: 8.31222222222222,
		power: 5640.35672102811,
		road: 852.814629629629,
		acceleration: 0.572222222222221
	},
	{
		id: 259,
		time: 258,
		velocity: 8.77722222222222,
		power: 2975.10671413369,
		road: 861.252361111111,
		acceleration: 0.20287037037037
	},
	{
		id: 260,
		time: 259,
		velocity: 8.52805555555556,
		power: 1587.7255053981,
		road: 869.804722222222,
		acceleration: 0.0263888888888903
	},
	{
		id: 261,
		time: 260,
		velocity: 8.39138888888889,
		power: -2198.97197111407,
		road: 878.148518518518,
		acceleration: -0.443518518518518
	},
	{
		id: 262,
		time: 261,
		velocity: 7.44666666666667,
		power: -1153.09197909153,
		road: 886.112916666666,
		acceleration: -0.315277777777778
	},
	{
		id: 263,
		time: 262,
		velocity: 7.58222222222222,
		power: -407.294984896207,
		road: 893.811481481481,
		acceleration: -0.216388888888889
	},
	{
		id: 264,
		time: 263,
		velocity: 7.74222222222222,
		power: 2196.94034669366,
		road: 901.472314814814,
		acceleration: 0.140925925925925
	},
	{
		id: 265,
		time: 264,
		velocity: 7.86944444444444,
		power: -47.3310190531084,
		road: 909.120185185185,
		acceleration: -0.166851851851852
	},
	{
		id: 266,
		time: 265,
		velocity: 7.08166666666667,
		power: 119.135690871346,
		road: 916.613472222222,
		acceleration: -0.142314814814815
	},
	{
		id: 267,
		time: 266,
		velocity: 7.31527777777778,
		power: 653.576293283226,
		road: 924.003009259259,
		acceleration: -0.065185185185185
	},
	{
		id: 268,
		time: 267,
		velocity: 7.67388888888889,
		power: 4620.14928015909,
		road: 931.599583333333,
		acceleration: 0.479259259259261
	},
	{
		id: 269,
		time: 268,
		velocity: 8.51944444444444,
		power: 5810.59174777265,
		road: 939.728935185185,
		acceleration: 0.586296296296294
	},
	{
		id: 270,
		time: 269,
		velocity: 9.07416666666667,
		power: 6541.13412255988,
		road: 948.459675925925,
		acceleration: 0.616481481481481
	},
	{
		id: 271,
		time: 270,
		velocity: 9.52333333333333,
		power: 5905.23426824779,
		road: 957.744537037037,
		acceleration: 0.491759259259261
	},
	{
		id: 272,
		time: 271,
		velocity: 9.99472222222222,
		power: 6147.99249726048,
		road: 967.514907407407,
		acceleration: 0.479259259259258
	},
	{
		id: 273,
		time: 272,
		velocity: 10.5119444444444,
		power: 5987.01691244525,
		road: 977.738981481481,
		acceleration: 0.42814814814815
	},
	{
		id: 274,
		time: 273,
		velocity: 10.8077777777778,
		power: 3202.16904329867,
		road: 988.242268518518,
		acceleration: 0.130277777777778
	},
	{
		id: 275,
		time: 274,
		velocity: 10.3855555555556,
		power: 1377.519502688,
		road: 998.784212962963,
		acceleration: -0.0529629629629635
	},
	{
		id: 276,
		time: 275,
		velocity: 10.3530555555556,
		power: -1730.30409174296,
		road: 1009.11800925926,
		acceleration: -0.363333333333335
	},
	{
		id: 277,
		time: 276,
		velocity: 9.71777777777778,
		power: -1268.52417754593,
		road: 1019.11166666667,
		acceleration: -0.316944444444442
	},
	{
		id: 278,
		time: 277,
		velocity: 9.43472222222222,
		power: -247.578639400107,
		road: 1028.84310185185,
		acceleration: -0.207500000000001
	},
	{
		id: 279,
		time: 278,
		velocity: 9.73055555555556,
		power: 1527.0939017401,
		road: 1038.46430555556,
		acceleration: -0.0129629629629626
	},
	{
		id: 280,
		time: 279,
		velocity: 9.67888888888889,
		power: 2498.47206547074,
		road: 1048.12476851852,
		acceleration: 0.0914814814814786
	},
	{
		id: 281,
		time: 280,
		velocity: 9.70916666666667,
		power: 2722.97353308121,
		road: 1057.88680555556,
		acceleration: 0.111666666666668
	},
	{
		id: 282,
		time: 281,
		velocity: 10.0655555555556,
		power: 2912.72918910414,
		road: 1067.76814814815,
		acceleration: 0.126944444444444
	},
	{
		id: 283,
		time: 282,
		velocity: 10.0597222222222,
		power: 3406.81642466592,
		road: 1077.79912037037,
		acceleration: 0.172314814814817
	},
	{
		id: 284,
		time: 283,
		velocity: 10.2261111111111,
		power: 2458.98404996473,
		road: 1087.95060185185,
		acceleration: 0.0687037037037026
	},
	{
		id: 285,
		time: 284,
		velocity: 10.2716666666667,
		power: 3557.01464884203,
		road: 1098.22462962963,
		acceleration: 0.176388888888889
	},
	{
		id: 286,
		time: 285,
		velocity: 10.5888888888889,
		power: 3549.36572076384,
		road: 1108.67064814815,
		acceleration: 0.167592592592593
	},
	{
		id: 287,
		time: 286,
		velocity: 10.7288888888889,
		power: 3645.01672509812,
		road: 1119.28513888889,
		acceleration: 0.16935185185185
	},
	{
		id: 288,
		time: 287,
		velocity: 10.7797222222222,
		power: 1310.53626422574,
		road: 1129.95296296296,
		acceleration: -0.0626851851851828
	},
	{
		id: 289,
		time: 288,
		velocity: 10.4008333333333,
		power: 1272.45033144354,
		road: 1140.55699074074,
		acceleration: -0.0649074074074072
	},
	{
		id: 290,
		time: 289,
		velocity: 10.5341666666667,
		power: 2857.32523008913,
		road: 1151.17425925926,
		acceleration: 0.0913888888888881
	},
	{
		id: 291,
		time: 290,
		velocity: 11.0538888888889,
		power: 5911.05940634212,
		road: 1162.02601851852,
		acceleration: 0.377592592592594
	},
	{
		id: 292,
		time: 291,
		velocity: 11.5336111111111,
		power: 4337.88183181338,
		road: 1173.17194444444,
		acceleration: 0.210740740740739
	},
	{
		id: 293,
		time: 292,
		velocity: 11.1663888888889,
		power: 4684.90900206795,
		road: 1184.53921296296,
		acceleration: 0.231944444444444
	},
	{
		id: 294,
		time: 293,
		velocity: 11.7497222222222,
		power: 3223.33217175808,
		road: 1196.06787037037,
		acceleration: 0.0908333333333342
	},
	{
		id: 295,
		time: 294,
		velocity: 11.8061111111111,
		power: 4569.56723717924,
		road: 1207.7449537037,
		acceleration: 0.206018518518515
	},
	{
		id: 296,
		time: 295,
		velocity: 11.7844444444444,
		power: 1335.01859538211,
		road: 1219.48212962963,
		acceleration: -0.0858333333333299
	},
	{
		id: 297,
		time: 296,
		velocity: 11.4922222222222,
		power: -2059.61066702234,
		road: 1230.98148148148,
		acceleration: -0.389814814814816
	},
	{
		id: 298,
		time: 297,
		velocity: 10.6366666666667,
		power: 209.916327889957,
		road: 1242.19657407407,
		acceleration: -0.178703703703704
	},
	{
		id: 299,
		time: 298,
		velocity: 11.2483333333333,
		power: 2493.31575035735,
		road: 1253.34092592593,
		acceleration: 0.0372222222222245
	},
	{
		id: 300,
		time: 299,
		velocity: 11.6038888888889,
		power: 6340.95028876006,
		road: 1264.69662037037,
		acceleration: 0.385462962962961
	},
	{
		id: 301,
		time: 300,
		velocity: 11.7930555555556,
		power: 4095.94305829303,
		road: 1276.32782407407,
		acceleration: 0.165555555555555
	},
	{
		id: 302,
		time: 301,
		velocity: 11.745,
		power: 1039.64001352028,
		road: 1287.98652777778,
		acceleration: -0.110555555555555
	},
	{
		id: 303,
		time: 302,
		velocity: 11.2722222222222,
		power: -65.5967557332647,
		road: 1299.48597222222,
		acceleration: -0.207962962962965
	},
	{
		id: 304,
		time: 303,
		velocity: 11.1691666666667,
		power: -249.533781323092,
		road: 1310.77023148148,
		acceleration: -0.222407407407406
	},
	{
		id: 305,
		time: 304,
		velocity: 11.0777777777778,
		power: 1321.60630872349,
		road: 1321.90685185185,
		acceleration: -0.0728703703703708
	},
	{
		id: 306,
		time: 305,
		velocity: 11.0536111111111,
		power: 18.77886753108,
		road: 1332.91009259259,
		acceleration: -0.193888888888887
	},
	{
		id: 307,
		time: 306,
		velocity: 10.5875,
		power: -1131.02308682338,
		road: 1343.66490740741,
		acceleration: -0.302962962962964
	},
	{
		id: 308,
		time: 307,
		velocity: 10.1688888888889,
		power: -2082.81635911494,
		road: 1354.06898148148,
		acceleration: -0.398518518518518
	},
	{
		id: 309,
		time: 308,
		velocity: 9.85805555555555,
		power: -2638.90655732391,
		road: 1364.04319444444,
		acceleration: -0.461203703703706
	},
	{
		id: 310,
		time: 309,
		velocity: 9.20388888888889,
		power: -3028.67282781895,
		road: 1373.53013888889,
		acceleration: -0.513333333333332
	},
	{
		id: 311,
		time: 310,
		velocity: 8.62888888888889,
		power: -3875.13545767271,
		road: 1382.44606481481,
		acceleration: -0.628703703703703
	},
	{
		id: 312,
		time: 311,
		velocity: 7.97194444444444,
		power: -6041.61148786845,
		road: 1390.57481481481,
		acceleration: -0.945648148148149
	},
	{
		id: 313,
		time: 312,
		velocity: 6.36694444444444,
		power: -7475.00642334247,
		road: 1397.59319444444,
		acceleration: -1.27509259259259
	},
	{
		id: 314,
		time: 313,
		velocity: 4.80361111111111,
		power: -6639.80278934098,
		road: 1403.28768518518,
		acceleration: -1.37268518518519
	},
	{
		id: 315,
		time: 314,
		velocity: 3.85388888888889,
		power: -4953.3266637649,
		road: 1407.62555555556,
		acceleration: -1.34055555555556
	},
	{
		id: 316,
		time: 315,
		velocity: 2.34527777777778,
		power: -2398.45680283363,
		road: 1410.8325,
		acceleration: -0.921296296296296
	},
	{
		id: 317,
		time: 316,
		velocity: 2.03972222222222,
		power: -1104.38686044302,
		road: 1413.27509259259,
		acceleration: -0.607407407407408
	},
	{
		id: 318,
		time: 317,
		velocity: 2.03166666666667,
		power: 729.12371649516,
		road: 1415.5200462963,
		acceleration: 0.21212962962963
	},
	{
		id: 319,
		time: 318,
		velocity: 2.98166666666667,
		power: 2530.91782583831,
		road: 1418.28712962963,
		acceleration: 0.832129629629629
	},
	{
		id: 320,
		time: 319,
		velocity: 4.53611111111111,
		power: 5588.015770835,
		road: 1422.16171296296,
		acceleration: 1.38287037037037
	},
	{
		id: 321,
		time: 320,
		velocity: 6.18027777777778,
		power: 5717.33191366354,
		road: 1427.24819444444,
		acceleration: 1.04092592592593
	},
	{
		id: 322,
		time: 321,
		velocity: 6.10444444444444,
		power: 4783.52033938416,
		road: 1433.20388888889,
		acceleration: 0.697500000000001
	},
	{
		id: 323,
		time: 322,
		velocity: 6.62861111111111,
		power: 3647.35170844123,
		road: 1439.72666666667,
		acceleration: 0.436666666666666
	},
	{
		id: 324,
		time: 323,
		velocity: 7.49027777777778,
		power: 3884.98401532069,
		road: 1446.68398148148,
		acceleration: 0.432407407407407
	},
	{
		id: 325,
		time: 324,
		velocity: 7.40166666666667,
		power: -280.190404333661,
		road: 1453.75888888889,
		acceleration: -0.197222222222221
	},
	{
		id: 326,
		time: 325,
		velocity: 6.03694444444444,
		power: -3339.01302056375,
		road: 1460.39449074074,
		acceleration: -0.681388888888889
	},
	{
		id: 327,
		time: 326,
		velocity: 5.44611111111111,
		power: -2850.65378908024,
		road: 1466.36449074074,
		acceleration: -0.649814814814815
	},
	{
		id: 328,
		time: 327,
		velocity: 5.45222222222222,
		power: 385.967878574506,
		road: 1471.97324074074,
		acceleration: -0.0726851851851853
	},
	{
		id: 329,
		time: 328,
		velocity: 5.81888888888889,
		power: 1188.65787234688,
		road: 1477.58453703704,
		acceleration: 0.0777777777777784
	},
	{
		id: 330,
		time: 329,
		velocity: 5.67944444444445,
		power: 1692.92053411161,
		road: 1483.31712962963,
		acceleration: 0.164814814814815
	},
	{
		id: 331,
		time: 330,
		velocity: 5.94666666666667,
		power: 1312.22647405734,
		road: 1489.17657407407,
		acceleration: 0.0888888888888895
	},
	{
		id: 332,
		time: 331,
		velocity: 6.08555555555556,
		power: 2409.18499722475,
		road: 1495.2162962963,
		acceleration: 0.271666666666665
	},
	{
		id: 333,
		time: 332,
		velocity: 6.49444444444444,
		power: 3475.4479287011,
		road: 1501.60282407407,
		acceleration: 0.421944444444445
	},
	{
		id: 334,
		time: 333,
		velocity: 7.2125,
		power: 2159.9805901437,
		road: 1508.29375,
		acceleration: 0.186851851851852
	},
	{
		id: 335,
		time: 334,
		velocity: 6.64611111111111,
		power: 1443.93258653544,
		road: 1515.11263888889,
		acceleration: 0.0690740740740745
	},
	{
		id: 336,
		time: 335,
		velocity: 6.70166666666667,
		power: 156.71552260836,
		road: 1521.90152777778,
		acceleration: -0.129074074074073
	},
	{
		id: 337,
		time: 336,
		velocity: 6.82527777777778,
		power: 948.639417867925,
		road: 1528.62365740741,
		acceleration: -0.00444444444444514
	},
	{
		id: 338,
		time: 337,
		velocity: 6.63277777777778,
		power: 633.753655863006,
		road: 1535.31703703704,
		acceleration: -0.053055555555555
	},
	{
		id: 339,
		time: 338,
		velocity: 6.5425,
		power: 142.371007594473,
		road: 1541.91925925926,
		acceleration: -0.12925925925926
	},
	{
		id: 340,
		time: 339,
		velocity: 6.4375,
		power: 792.78217947395,
		road: 1548.4450462963,
		acceleration: -0.0236111111111104
	},
	{
		id: 341,
		time: 340,
		velocity: 6.56194444444444,
		power: 1306.14527658167,
		road: 1554.98824074074,
		acceleration: 0.058425925925925
	},
	{
		id: 342,
		time: 341,
		velocity: 6.71777777777778,
		power: 1727.34511868306,
		road: 1561.62148148148,
		acceleration: 0.121666666666666
	},
	{
		id: 343,
		time: 342,
		velocity: 6.8025,
		power: 1692.61181150131,
		road: 1568.37087962963,
		acceleration: 0.110648148148148
	},
	{
		id: 344,
		time: 343,
		velocity: 6.89388888888889,
		power: 1603.16005260385,
		road: 1575.22171296296,
		acceleration: 0.0922222222222224
	},
	{
		id: 345,
		time: 344,
		velocity: 6.99444444444444,
		power: 1392.12385134771,
		road: 1582.14712962963,
		acceleration: 0.0569444444444436
	},
	{
		id: 346,
		time: 345,
		velocity: 6.97333333333333,
		power: 943.818912168225,
		road: 1589.09513888889,
		acceleration: -0.011759259259259
	},
	{
		id: 347,
		time: 346,
		velocity: 6.85861111111111,
		power: 714.784260681593,
		road: 1596.01439814815,
		acceleration: -0.0457407407407402
	},
	{
		id: 348,
		time: 347,
		velocity: 6.85722222222222,
		power: 202.834025127382,
		road: 1602.84953703704,
		acceleration: -0.1225
	},
	{
		id: 349,
		time: 348,
		velocity: 6.60583333333333,
		power: 844.46149508976,
		road: 1609.6125,
		acceleration: -0.0218518518518511
	},
	{
		id: 350,
		time: 349,
		velocity: 6.79305555555556,
		power: -1316.47608126358,
		road: 1616.18333333333,
		acceleration: -0.362407407407408
	},
	{
		id: 351,
		time: 350,
		velocity: 5.77,
		power: -1277.35404715602,
		road: 1622.39018518518,
		acceleration: -0.365555555555556
	},
	{
		id: 352,
		time: 351,
		velocity: 5.50916666666667,
		power: -3352.91849728678,
		road: 1628.02875,
		acceleration: -0.771018518518518
	},
	{
		id: 353,
		time: 352,
		velocity: 4.48,
		power: -2894.19087523125,
		road: 1632.89861111111,
		acceleration: -0.766388888888889
	},
	{
		id: 354,
		time: 353,
		velocity: 3.47083333333333,
		power: -3494.05508294527,
		road: 1636.85166666667,
		acceleration: -1.06722222222222
	},
	{
		id: 355,
		time: 354,
		velocity: 2.3075,
		power: -1855.17642740597,
		road: 1639.88226851852,
		acceleration: -0.777685185185185
	},
	{
		id: 356,
		time: 355,
		velocity: 2.14694444444444,
		power: -505.80966847023,
		road: 1642.35060185185,
		acceleration: -0.346851851851852
	},
	{
		id: 357,
		time: 356,
		velocity: 2.43027777777778,
		power: 2027.90321459706,
		road: 1644.98541666667,
		acceleration: 0.679814814814816
	},
	{
		id: 358,
		time: 357,
		velocity: 4.34694444444444,
		power: 3475.32683887279,
		road: 1648.42523148148,
		acceleration: 0.930185185185184
	},
	{
		id: 359,
		time: 358,
		velocity: 4.9375,
		power: 3634.25749231764,
		road: 1652.70800925926,
		acceleration: 0.755740740740741
	},
	{
		id: 360,
		time: 359,
		velocity: 4.6975,
		power: 2387.91626949909,
		road: 1657.5575,
		acceleration: 0.377685185185186
	},
	{
		id: 361,
		time: 360,
		velocity: 5.48,
		power: 2730.25806742057,
		road: 1662.79851851852,
		acceleration: 0.40537037037037
	},
	{
		id: 362,
		time: 361,
		velocity: 6.15361111111111,
		power: 5364.76378603444,
		road: 1668.65101851852,
		acceleration: 0.817592592592592
	},
	{
		id: 363,
		time: 362,
		velocity: 7.15027777777778,
		power: 6465.92890326185,
		road: 1675.34388888889,
		acceleration: 0.863148148148147
	},
	{
		id: 364,
		time: 363,
		velocity: 8.06944444444444,
		power: 7046.37864922545,
		road: 1682.8799537037,
		acceleration: 0.823240740740742
	},
	{
		id: 365,
		time: 364,
		velocity: 8.62333333333333,
		power: 4177.42700465768,
		road: 1691.0150462963,
		acceleration: 0.374814814814814
	},
	{
		id: 366,
		time: 365,
		velocity: 8.27472222222222,
		power: 1747.05997919375,
		road: 1699.36412037037,
		acceleration: 0.0531481481481499
	},
	{
		id: 367,
		time: 366,
		velocity: 8.22888888888889,
		power: 1988.7877486908,
		road: 1707.78023148148,
		acceleration: 0.080925925925925
	},
	{
		id: 368,
		time: 367,
		velocity: 8.86611111111111,
		power: 4150.24923057197,
		road: 1716.40486111111,
		acceleration: 0.33611111111111
	},
	{
		id: 369,
		time: 368,
		velocity: 9.28305555555555,
		power: 6269.17740517952,
		road: 1725.47361111111,
		acceleration: 0.552129629629631
	},
	{
		id: 370,
		time: 369,
		velocity: 9.88527777777778,
		power: 5545.22764108112,
		road: 1735.03342592592,
		acceleration: 0.43
	},
	{
		id: 371,
		time: 370,
		velocity: 10.1561111111111,
		power: 4187.72738116664,
		road: 1744.93875,
		acceleration: 0.261018518518519
	},
	{
		id: 372,
		time: 371,
		velocity: 10.0661111111111,
		power: 3358.3573650751,
		road: 1755.0562037037,
		acceleration: 0.16324074074074
	},
	{
		id: 373,
		time: 372,
		velocity: 10.375,
		power: 3986.75959746039,
		road: 1765.36453703704,
		acceleration: 0.218518518518518
	},
	{
		id: 374,
		time: 373,
		velocity: 10.8116666666667,
		power: 5215.01156286271,
		road: 1775.94541666667,
		acceleration: 0.326574074074074
	},
	{
		id: 375,
		time: 374,
		velocity: 11.0458333333333,
		power: 3666.68533925571,
		road: 1786.77050925926,
		acceleration: 0.16185185185185
	},
	{
		id: 376,
		time: 375,
		velocity: 10.8605555555556,
		power: 1668.63650280703,
		road: 1797.65976851852,
		acceleration: -0.0335185185185161
	},
	{
		id: 377,
		time: 376,
		velocity: 10.7111111111111,
		power: -24.7088713248084,
		road: 1808.43462962963,
		acceleration: -0.195277777777779
	},
	{
		id: 378,
		time: 377,
		velocity: 10.46,
		power: -385.482988506833,
		road: 1818.9975462963,
		acceleration: -0.22861111111111
	},
	{
		id: 379,
		time: 378,
		velocity: 10.1747222222222,
		power: 501.982192753941,
		road: 1829.3774537037,
		acceleration: -0.137407407407407
	},
	{
		id: 380,
		time: 379,
		velocity: 10.2988888888889,
		power: 2143.33635462583,
		road: 1839.7037962963,
		acceleration: 0.0302777777777763
	},
	{
		id: 381,
		time: 380,
		velocity: 10.5508333333333,
		power: 3859.4043149882,
		road: 1850.14476851852,
		acceleration: 0.198981481481482
	},
	{
		id: 382,
		time: 381,
		velocity: 10.7716666666667,
		power: 4894.50368752329,
		road: 1860.82962962963,
		acceleration: 0.288796296296296
	},
	{
		id: 383,
		time: 382,
		velocity: 11.1652777777778,
		power: 5778.98982159953,
		road: 1871.83638888889,
		acceleration: 0.355
	},
	{
		id: 384,
		time: 383,
		velocity: 11.6158333333333,
		power: 6636.1977766289,
		road: 1883.22587962963,
		acceleration: 0.410462962962963
	},
	{
		id: 385,
		time: 384,
		velocity: 12.0030555555556,
		power: 6794.30425874024,
		road: 1895.01967592592,
		acceleration: 0.398148148148147
	},
	{
		id: 386,
		time: 385,
		velocity: 12.3597222222222,
		power: 7502.01271982051,
		road: 1907.22884259259,
		acceleration: 0.432592592592593
	},
	{
		id: 387,
		time: 386,
		velocity: 12.9136111111111,
		power: 8396.60447731556,
		road: 1919.89277777778,
		acceleration: 0.476944444444442
	},
	{
		id: 388,
		time: 387,
		velocity: 13.4338888888889,
		power: 9504.09149946472,
		road: 1933.06064814815,
		acceleration: 0.530925925925928
	},
	{
		id: 389,
		time: 388,
		velocity: 13.9525,
		power: 8630.37588253017,
		road: 1946.70888888889,
		acceleration: 0.429814814814815
	},
	{
		id: 390,
		time: 389,
		velocity: 14.2030555555556,
		power: 8135.77247258845,
		road: 1960.75592592592,
		acceleration: 0.367777777777778
	},
	{
		id: 391,
		time: 390,
		velocity: 14.5372222222222,
		power: 5560.25254419948,
		road: 1975.06875,
		acceleration: 0.163796296296297
	},
	{
		id: 392,
		time: 391,
		velocity: 14.4438888888889,
		power: 7223.69188990591,
		road: 1989.60041666667,
		acceleration: 0.273888888888887
	},
	{
		id: 393,
		time: 392,
		velocity: 15.0247222222222,
		power: 2085.29858501296,
		road: 2004.21972222222,
		acceleration: -0.0986111111111114
	},
	{
		id: 394,
		time: 393,
		velocity: 14.2413888888889,
		power: 491.725869847252,
		road: 2018.68481481481,
		acceleration: -0.209814814814813
	},
	{
		id: 395,
		time: 394,
		velocity: 13.8144444444444,
		power: -2912.243760931,
		road: 2032.81708333333,
		acceleration: -0.455833333333334
	},
	{
		id: 396,
		time: 395,
		velocity: 13.6572222222222,
		power: -337.769145838138,
		road: 2046.59138888889,
		acceleration: -0.260092592592594
	},
	{
		id: 397,
		time: 396,
		velocity: 13.4611111111111,
		power: 858.663679827459,
		road: 2060.15324074074,
		acceleration: -0.164814814814813
	},
	{
		id: 398,
		time: 397,
		velocity: 13.32,
		power: 1453.53122933459,
		road: 2073.57490740741,
		acceleration: -0.115555555555558
	},
	{
		id: 399,
		time: 398,
		velocity: 13.3105555555556,
		power: 2976.29775905611,
		road: 2086.94134259259,
		acceleration: 0.00509259259259487
	},
	{
		id: 400,
		time: 399,
		velocity: 13.4763888888889,
		power: 2921.971705968,
		road: 2100.31069444444,
		acceleration: 0.000740740740740264
	},
	{
		id: 401,
		time: 400,
		velocity: 13.3222222222222,
		power: 2172.15068378948,
		road: 2113.65180555555,
		acceleration: -0.0572222222222223
	},
	{
		id: 402,
		time: 401,
		velocity: 13.1388888888889,
		power: 1747.59978063493,
		road: 2126.9199537037,
		acceleration: -0.0887037037037057
	},
	{
		id: 403,
		time: 402,
		velocity: 13.2102777777778,
		power: 2023.78583398563,
		road: 2140.11134259259,
		acceleration: -0.0648148148148131
	},
	{
		id: 404,
		time: 403,
		velocity: 13.1277777777778,
		power: 2662.65310236607,
		road: 2153.26388888889,
		acceleration: -0.0128703703703703
	},
	{
		id: 405,
		time: 404,
		velocity: 13.1002777777778,
		power: 2859.42023386844,
		road: 2166.41148148148,
		acceleration: 0.00296296296296283
	},
	{
		id: 406,
		time: 405,
		velocity: 13.2191666666667,
		power: 4111.24931026405,
		road: 2179.61087962963,
		acceleration: 0.100648148148149
	},
	{
		id: 407,
		time: 406,
		velocity: 13.4297222222222,
		power: 5309.59194522183,
		road: 2192.95509259259,
		acceleration: 0.188981481481481
	},
	{
		id: 408,
		time: 407,
		velocity: 13.6672222222222,
		power: 5159.35362133144,
		road: 2206.47833333333,
		acceleration: 0.169074074074073
	},
	{
		id: 409,
		time: 408,
		velocity: 13.7263888888889,
		power: 1872.57173079814,
		road: 2220.04282407407,
		acceleration: -0.0865740740740737
	},
	{
		id: 410,
		time: 409,
		velocity: 13.17,
		power: 4102.69491523527,
		road: 2233.60685185185,
		acceleration: 0.0856481481481488
	},
	{
		id: 411,
		time: 410,
		velocity: 13.9241666666667,
		power: 3501.23047641574,
		road: 2247.23222222222,
		acceleration: 0.0370370370370363
	},
	{
		id: 412,
		time: 411,
		velocity: 13.8375,
		power: 6308.83523089513,
		road: 2260.99893518518,
		acceleration: 0.245648148148147
	},
	{
		id: 413,
		time: 412,
		velocity: 13.9069444444444,
		power: -32.7652906333084,
		road: 2274.7700462963,
		acceleration: -0.236851851851851
	},
	{
		id: 414,
		time: 413,
		velocity: 13.2136111111111,
		power: 75.0266755357102,
		road: 2288.31023148148,
		acceleration: -0.225
	},
	{
		id: 415,
		time: 414,
		velocity: 13.1625,
		power: 535.179811917673,
		road: 2301.64509259259,
		acceleration: -0.185648148148148
	},
	{
		id: 416,
		time: 415,
		velocity: 13.35,
		power: 4382.27358678415,
		road: 2314.94611111111,
		acceleration: 0.117962962962963
	},
	{
		id: 417,
		time: 416,
		velocity: 13.5675,
		power: 4389.7321165295,
		road: 2328.36300925926,
		acceleration: 0.113796296296298
	},
	{
		id: 418,
		time: 417,
		velocity: 13.5038888888889,
		power: 4495.97325545797,
		road: 2341.89546296296,
		acceleration: 0.117314814814813
	},
	{
		id: 419,
		time: 418,
		velocity: 13.7019444444444,
		power: 3133.19260134798,
		road: 2355.49143518518,
		acceleration: 0.00972222222222108
	},
	{
		id: 420,
		time: 419,
		velocity: 13.5966666666667,
		power: 3491.11862559311,
		road: 2369.11050925926,
		acceleration: 0.0364814814814824
	},
	{
		id: 421,
		time: 420,
		velocity: 13.6133333333333,
		power: 2909.33607472178,
		road: 2382.74347222222,
		acceleration: -0.00870370370370388
	},
	{
		id: 422,
		time: 421,
		velocity: 13.6758333333333,
		power: 4717.23219759653,
		road: 2396.43592592593,
		acceleration: 0.127685185185182
	},
	{
		id: 423,
		time: 422,
		velocity: 13.9797222222222,
		power: 5837.40352788876,
		road: 2410.29490740741,
		acceleration: 0.205370370370373
	},
	{
		id: 424,
		time: 423,
		velocity: 14.2294444444444,
		power: 4755.74698345103,
		road: 2424.3150462963,
		acceleration: 0.116944444444444
	},
	{
		id: 425,
		time: 424,
		velocity: 14.0266666666667,
		power: 1456.14284741116,
		road: 2438.32898148148,
		acceleration: -0.129351851851851
	},
	{
		id: 426,
		time: 425,
		velocity: 13.5916666666667,
		power: -72.382647793831,
		road: 2452.15787037037,
		acceleration: -0.240740740740742
	},
	{
		id: 427,
		time: 426,
		velocity: 13.5072222222222,
		power: 834.211018530967,
		road: 2465.78240740741,
		acceleration: -0.16796296296296
	},
	{
		id: 428,
		time: 427,
		velocity: 13.5227777777778,
		power: 3448.95658960876,
		road: 2479.34064814815,
		acceleration: 0.0353703703703676
	},
	{
		id: 429,
		time: 428,
		velocity: 13.6977777777778,
		power: 4588.67125425119,
		road: 2492.97666666667,
		acceleration: 0.120185185185186
	},
	{
		id: 430,
		time: 429,
		velocity: 13.8677777777778,
		power: 5040.30529323736,
		road: 2506.74726851852,
		acceleration: 0.148981481481483
	},
	{
		id: 431,
		time: 430,
		velocity: 13.9697222222222,
		power: 5479.9063728901,
		road: 2520.67986111111,
		acceleration: 0.174999999999997
	},
	{
		id: 432,
		time: 431,
		velocity: 14.2227777777778,
		power: 5029.75235173393,
		road: 2534.76722222222,
		acceleration: 0.13453703703704
	},
	{
		id: 433,
		time: 432,
		velocity: 14.2713888888889,
		power: 5222.97954350757,
		road: 2548.99328703704,
		acceleration: 0.142870370370369
	},
	{
		id: 434,
		time: 433,
		velocity: 14.3983333333333,
		power: 3482.49289667328,
		road: 2563.29685185185,
		acceleration: 0.0121296296296318
	},
	{
		id: 435,
		time: 434,
		velocity: 14.2591666666667,
		power: 932.153081223019,
		road: 2577.5200462963,
		acceleration: -0.172870370370372
	},
	{
		id: 436,
		time: 435,
		velocity: 13.7527777777778,
		power: -748.26800290072,
		road: 2591.50990740741,
		acceleration: -0.293796296296296
	},
	{
		id: 437,
		time: 436,
		velocity: 13.5169444444444,
		power: -1046.6409305554,
		road: 2605.1962962963,
		acceleration: -0.31314814814815
	},
	{
		id: 438,
		time: 437,
		velocity: 13.3197222222222,
		power: 1776.39129284851,
		road: 2618.68013888889,
		acceleration: -0.0919444444444419
	},
	{
		id: 439,
		time: 438,
		velocity: 13.4769444444444,
		power: 2251.78333165569,
		road: 2632.09152777778,
		acceleration: -0.0529629629629653
	},
	{
		id: 440,
		time: 439,
		velocity: 13.3580555555556,
		power: 5017.38611586282,
		road: 2645.55675925926,
		acceleration: 0.160648148148152
	},
	{
		id: 441,
		time: 440,
		velocity: 13.8016666666667,
		power: 6814.43772170427,
		road: 2659.24643518518,
		acceleration: 0.288240740740736
	},
	{
		id: 442,
		time: 441,
		velocity: 14.3416666666667,
		power: 11170.1807155855,
		road: 2673.37416666667,
		acceleration: 0.587870370370373
	},
	{
		id: 443,
		time: 442,
		velocity: 15.1216666666667,
		power: 5140.81006274541,
		road: 2687.85875,
		acceleration: 0.125833333333333
	},
	{
		id: 444,
		time: 443,
		velocity: 14.1791666666667,
		power: -6416.05002606572,
		road: 2702.04902777778,
		acceleration: -0.714444444444444
	},
	{
		id: 445,
		time: 444,
		velocity: 12.1983333333333,
		power: -17846.2006666467,
		road: 2715.05148148148,
		acceleration: -1.6612037037037
	},
	{
		id: 446,
		time: 445,
		velocity: 10.1380555555556,
		power: -14066.4248550039,
		road: 2726.47712962963,
		acceleration: -1.49240740740741
	},
	{
		id: 447,
		time: 446,
		velocity: 9.70194444444444,
		power: -9601.41181477158,
		road: 2736.56462962963,
		acceleration: -1.18388888888889
	},
	{
		id: 448,
		time: 447,
		velocity: 8.64666666666667,
		power: -4630.99188774945,
		road: 2745.70694444444,
		acceleration: -0.706481481481481
	},
	{
		id: 449,
		time: 448,
		velocity: 8.01861111111111,
		power: -4521.20431546238,
		road: 2754.13037037037,
		acceleration: -0.731296296296296
	},
	{
		id: 450,
		time: 449,
		velocity: 7.50805555555556,
		power: -2595.39346995343,
		road: 2761.9325,
		acceleration: -0.511296296296297
	},
	{
		id: 451,
		time: 450,
		velocity: 7.11277777777778,
		power: 2375.01326185511,
		road: 2769.5624537037,
		acceleration: 0.166944444444444
	},
	{
		id: 452,
		time: 451,
		velocity: 8.51944444444444,
		power: 8129.30636385481,
		road: 2777.71708333333,
		acceleration: 0.882407407407408
	},
	{
		id: 453,
		time: 452,
		velocity: 10.1552777777778,
		power: 19175.6149599369,
		road: 2787.27615740741,
		acceleration: 1.92648148148148
	},
	{
		id: 454,
		time: 453,
		velocity: 12.8922222222222,
		power: 29979.7281776221,
		road: 2799.03226851852,
		acceleration: 2.46759259259259
	},
	{
		id: 455,
		time: 454,
		velocity: 15.9222222222222,
		power: 36245.4273163045,
		road: 2813.23699074074,
		acceleration: 2.42962962962963
	},
	{
		id: 456,
		time: 455,
		velocity: 17.4441666666667,
		power: 37605.5902037769,
		road: 2829.70962962963,
		acceleration: 2.1062037037037
	},
	{
		id: 457,
		time: 456,
		velocity: 19.2108333333333,
		power: 32472.0305209281,
		road: 2848.00425925926,
		acceleration: 1.53777777777778
	},
	{
		id: 458,
		time: 457,
		velocity: 20.5355555555556,
		power: 31498.3372419034,
		road: 2867.72782407407,
		acceleration: 1.32009259259259
	},
	{
		id: 459,
		time: 458,
		velocity: 21.4044444444444,
		power: 28335.2957077984,
		road: 2888.63157407407,
		acceleration: 1.04027777777778
	},
	{
		id: 460,
		time: 459,
		velocity: 22.3316666666667,
		power: 31867.5057731777,
		road: 2910.61138888889,
		acceleration: 1.11185185185185
	},
	{
		id: 461,
		time: 460,
		velocity: 23.8711111111111,
		power: 40591.0292385495,
		road: 2933.84157407407,
		acceleration: 1.38888888888889
	},
	{
		id: 462,
		time: 461,
		velocity: 25.5711111111111,
		power: 39689.4904634687,
		road: 2958.37513888889,
		acceleration: 1.21787037037037
	},
	{
		id: 463,
		time: 462,
		velocity: 25.9852777777778,
		power: 31424.4448585256,
		road: 2983.91074074074,
		acceleration: 0.786203703703702
	},
	{
		id: 464,
		time: 463,
		velocity: 26.2297222222222,
		power: 27440.701508277,
		road: 3010.12685185185,
		acceleration: 0.574814814814818
	},
	{
		id: 465,
		time: 464,
		velocity: 27.2955555555556,
		power: 33188.2180753738,
		road: 3037.00546296296,
		acceleration: 0.750185185185185
	},
	{
		id: 466,
		time: 465,
		velocity: 28.2358333333333,
		power: 39483.6696621137,
		road: 3064.71986111111,
		acceleration: 0.921388888888888
	},
	{
		id: 467,
		time: 466,
		velocity: 28.9938888888889,
		power: 42357.3026137704,
		road: 3093.36851851852,
		acceleration: 0.947129629629629
	},
	{
		id: 468,
		time: 467,
		velocity: 30.1369444444444,
		power: 38839.0641883498,
		road: 3122.8662037037,
		acceleration: 0.750925925925927
	},
	{
		id: 469,
		time: 468,
		velocity: 30.4886111111111,
		power: 36708.9714898481,
		road: 3153.05097222222,
		acceleration: 0.623240740740741
	},
	{
		id: 470,
		time: 469,
		velocity: 30.8636111111111,
		power: 18379.7805346423,
		road: 3183.53574074074,
		acceleration: -0.0232407407407358
	},
	{
		id: 471,
		time: 470,
		velocity: 30.0672222222222,
		power: 17850.8873620319,
		road: 3213.98912037037,
		acceleration: -0.0395370370370429
	},
	{
		id: 472,
		time: 471,
		velocity: 30.37,
		power: 7080.446438653,
		road: 3244.22407407407,
		acceleration: -0.397314814814813
	},
	{
		id: 473,
		time: 472,
		velocity: 29.6716666666667,
		power: 12898.8527588052,
		road: 3274.16861111111,
		acceleration: -0.183518518518518
	},
	{
		id: 474,
		time: 473,
		velocity: 29.5166666666667,
		power: 4665.24557336533,
		road: 3303.79314814815,
		acceleration: -0.456481481481482
	},
	{
		id: 475,
		time: 474,
		velocity: 29.0005555555556,
		power: 13773.5065214681,
		road: 3333.12796296296,
		acceleration: -0.122962962962966
	},
	{
		id: 476,
		time: 475,
		velocity: 29.3027777777778,
		power: 856.00309814469,
		road: 3362.11703703704,
		acceleration: -0.568518518518516
	},
	{
		id: 477,
		time: 476,
		velocity: 27.8111111111111,
		power: 4301.87738053426,
		road: 3390.60865740741,
		acceleration: -0.426388888888887
	},
	{
		id: 478,
		time: 477,
		velocity: 27.7213888888889,
		power: 4046.05842515238,
		road: 3418.67703703704,
		acceleration: -0.420092592592596
	},
	{
		id: 479,
		time: 478,
		velocity: 28.0425,
		power: 19557.727405404,
		road: 3446.61611111111,
		acceleration: 0.161481481481481
	},
	{
		id: 480,
		time: 479,
		velocity: 28.2955555555556,
		power: 22809.7665599452,
		road: 3474.77046296296,
		acceleration: 0.26907407407408
	},
	{
		id: 481,
		time: 480,
		velocity: 28.5286111111111,
		power: 21298.8140027225,
		road: 3503.15902777778,
		acceleration: 0.199351851851848
	},
	{
		id: 482,
		time: 481,
		velocity: 28.6405555555556,
		power: 19459.8265629709,
		road: 3531.7087037037,
		acceleration: 0.122870370370375
	},
	{
		id: 483,
		time: 482,
		velocity: 28.6641666666667,
		power: 18940.9218266447,
		road: 3560.3687037037,
		acceleration: 0.0977777777777753
	},
	{
		id: 484,
		time: 483,
		velocity: 28.8219444444444,
		power: 19135.5252393566,
		road: 3589.12722222222,
		acceleration: 0.0992592592592594
	},
	{
		id: 485,
		time: 484,
		velocity: 28.9383333333333,
		power: 19315.1347596936,
		road: 3617.98541666667,
		acceleration: 0.100092592592592
	},
	{
		id: 486,
		time: 485,
		velocity: 28.9644444444444,
		power: 14506.2527820449,
		road: 3646.85685185185,
		acceleration: -0.0736111111111093
	},
	{
		id: 487,
		time: 486,
		velocity: 28.6011111111111,
		power: 12207.8291531064,
		road: 3675.61601851852,
		acceleration: -0.150925925925929
	},
	{
		id: 488,
		time: 487,
		velocity: 28.4855555555556,
		power: 11785.1366930947,
		road: 3704.22027777778,
		acceleration: -0.158888888888889
	},
	{
		id: 489,
		time: 488,
		velocity: 28.4877777777778,
		power: 14471.2672791364,
		road: 3732.71712962963,
		acceleration: -0.0559259259259264
	},
	{
		id: 490,
		time: 489,
		velocity: 28.4333333333333,
		power: 15908.1172043031,
		road: 3761.185,
		acceleration: -0.00203703703703439
	},
	{
		id: 491,
		time: 490,
		velocity: 28.4794444444444,
		power: 18059.3141889213,
		road: 3789.68907407407,
		acceleration: 0.0744444444444419
	},
	{
		id: 492,
		time: 491,
		velocity: 28.7111111111111,
		power: 17748.4041835602,
		road: 3818.26009259259,
		acceleration: 0.0594444444444413
	},
	{
		id: 493,
		time: 492,
		velocity: 28.6116666666667,
		power: 10871.4586707865,
		road: 3846.76703703704,
		acceleration: -0.187592592592587
	},
	{
		id: 494,
		time: 493,
		velocity: 27.9166666666667,
		power: 4027.08661453467,
		road: 3874.96736111111,
		acceleration: -0.425648148148152
	},
	{
		id: 495,
		time: 494,
		velocity: 27.4341666666667,
		power: -1076.28323892358,
		road: 3902.65564814815,
		acceleration: -0.598425925925923
	},
	{
		id: 496,
		time: 495,
		velocity: 26.8163888888889,
		power: -650.933679561601,
		road: 3929.76212962963,
		acceleration: -0.565185185185186
	},
	{
		id: 497,
		time: 496,
		velocity: 26.2211111111111,
		power: -1393.07067317385,
		road: 3956.29717592593,
		acceleration: -0.577685185185185
	},
	{
		id: 498,
		time: 497,
		velocity: 25.7011111111111,
		power: -1305.3148595547,
		road: 3982.26402777778,
		acceleration: -0.558703703703703
	},
	{
		id: 499,
		time: 498,
		velocity: 25.1402777777778,
		power: -1874.89871809002,
		road: 4007.66800925926,
		acceleration: -0.567037037037039
	},
	{
		id: 500,
		time: 499,
		velocity: 24.52,
		power: -596.301938953329,
		road: 4032.53837962963,
		acceleration: -0.500185185185188
	},
	{
		id: 501,
		time: 500,
		velocity: 24.2005555555556,
		power: -834.399555299254,
		road: 4056.91009259259,
		acceleration: -0.497129629629626
	},
	{
		id: 502,
		time: 501,
		velocity: 23.6488888888889,
		power: -507.982809371857,
		road: 4080.79796296296,
		acceleration: -0.470555555555553
	},
	{
		id: 503,
		time: 502,
		velocity: 23.1083333333333,
		power: -1367.81656457325,
		road: 4104.20231481482,
		acceleration: -0.496481481481482
	},
	{
		id: 504,
		time: 503,
		velocity: 22.7111111111111,
		power: -1007.90498625686,
		road: 4127.12398148148,
		acceleration: -0.468888888888891
	},
	{
		id: 505,
		time: 504,
		velocity: 22.2422222222222,
		power: 2319.4367313143,
		road: 4149.65824074074,
		acceleration: -0.305925925925926
	},
	{
		id: 506,
		time: 505,
		velocity: 22.1905555555556,
		power: -236.479578375107,
		road: 4171.83189814815,
		acceleration: -0.415277777777774
	},
	{
		id: 507,
		time: 506,
		velocity: 21.4652777777778,
		power: 1411.49089001815,
		road: 4193.63416666667,
		acceleration: -0.327500000000001
	},
	{
		id: 508,
		time: 507,
		velocity: 21.2597222222222,
		power: 2365.97066821221,
		road: 4215.13620370371,
		acceleration: -0.272962962962964
	},
	{
		id: 509,
		time: 508,
		velocity: 21.3716666666667,
		power: 9249.63354875085,
		road: 4236.53453703704,
		acceleration: 0.0655555555555551
	},
	{
		id: 510,
		time: 509,
		velocity: 21.6619444444444,
		power: 15430.0031262809,
		road: 4258.14277777778,
		acceleration: 0.354259259259262
	},
	{
		id: 511,
		time: 510,
		velocity: 22.3225,
		power: 20517.5547015001,
		road: 4280.21189814815,
		acceleration: 0.567499999999995
	},
	{
		id: 512,
		time: 511,
		velocity: 23.0741666666667,
		power: 24457.172164638,
		road: 4302.91717592593,
		acceleration: 0.704814814814814
	},
	{
		id: 513,
		time: 512,
		velocity: 23.7763888888889,
		power: 26314.45452655,
		road: 4326.34180555556,
		acceleration: 0.733888888888892
	},
	{
		id: 514,
		time: 513,
		velocity: 24.5241666666667,
		power: 22445.1937818372,
		road: 4350.39282407408,
		acceleration: 0.518888888888888
	},
	{
		id: 515,
		time: 514,
		velocity: 24.6308333333333,
		power: 17070.1422460171,
		road: 4374.83509259259,
		acceleration: 0.263611111111114
	},
	{
		id: 516,
		time: 515,
		velocity: 24.5672222222222,
		power: 8926.05726948114,
		road: 4399.36564814815,
		acceleration: -0.0870370370370388
	},
	{
		id: 517,
		time: 516,
		velocity: 24.2630555555556,
		power: 7418.32628262694,
		road: 4423.77953703704,
		acceleration: -0.146296296296299
	},
	{
		id: 518,
		time: 517,
		velocity: 24.1919444444444,
		power: 5818.12721614714,
		road: 4448.01634259259,
		acceleration: -0.207870370370369
	},
	{
		id: 519,
		time: 518,
		velocity: 23.9436111111111,
		power: 2642.55961136135,
		road: 4471.98138888889,
		acceleration: -0.335648148148149
	},
	{
		id: 520,
		time: 519,
		velocity: 23.2561111111111,
		power: -60.3040917980932,
		road: 4495.55722222222,
		acceleration: -0.442777777777778
	},
	{
		id: 521,
		time: 520,
		velocity: 22.8636111111111,
		power: -1204.61204306321,
		road: 4518.67050925926,
		acceleration: -0.482314814814817
	},
	{
		id: 522,
		time: 521,
		velocity: 22.4966666666667,
		power: 1304.86011958623,
		road: 4541.36398148148,
		acceleration: -0.357314814814814
	},
	{
		id: 523,
		time: 522,
		velocity: 22.1841666666667,
		power: 3144.73894215182,
		road: 4563.74731481482,
		acceleration: -0.262962962962959
	},
	{
		id: 524,
		time: 523,
		velocity: 22.0747222222222,
		power: 6274.28219162666,
		road: 4585.94412037037,
		acceleration: -0.110092592592594
	},
	{
		id: 525,
		time: 524,
		velocity: 22.1663888888889,
		power: 8604.24080068712,
		road: 4608.08671296296,
		acceleration: 0.00166666666666515
	},
	{
		id: 526,
		time: 525,
		velocity: 22.1891666666667,
		power: 6178.20183731623,
		road: 4630.17490740741,
		acceleration: -0.110462962962959
	},
	{
		id: 527,
		time: 526,
		velocity: 21.7433333333333,
		power: 3435.54654376271,
		road: 4652.09069444445,
		acceleration: -0.234351851851855
	},
	{
		id: 528,
		time: 527,
		velocity: 21.4633333333333,
		power: -1962.89202284937,
		road: 4673.64731481482,
		acceleration: -0.483981481481482
	},
	{
		id: 529,
		time: 528,
		velocity: 20.7372222222222,
		power: -1506.01180505759,
		road: 4694.73578703704,
		acceleration: -0.452314814814809
	},
	{
		id: 530,
		time: 529,
		velocity: 20.3863888888889,
		power: -1220.3894481493,
		road: 4715.38351851852,
		acceleration: -0.429166666666671
	},
	{
		id: 531,
		time: 530,
		velocity: 20.1758333333333,
		power: -963.300631052995,
		road: 4735.61287037037,
		acceleration: -0.407592592592593
	},
	{
		id: 532,
		time: 531,
		velocity: 19.5144444444444,
		power: 240.989341127884,
		road: 4755.47,
		acceleration: -0.336851851851851
	},
	{
		id: 533,
		time: 532,
		velocity: 19.3758333333333,
		power: 4700.59812543793,
		road: 4775.11125,
		acceleration: -0.0949074074074083
	},
	{
		id: 534,
		time: 533,
		velocity: 19.8911111111111,
		power: 8817.80256244049,
		road: 4794.76666666667,
		acceleration: 0.123240740740737
	},
	{
		id: 535,
		time: 534,
		velocity: 19.8841666666667,
		power: 6770.03404150827,
		road: 4814.48958333333,
		acceleration: 0.0117592592592608
	},
	{
		id: 536,
		time: 535,
		velocity: 19.4111111111111,
		power: 2239.53174651038,
		road: 4834.10587962963,
		acceleration: -0.225000000000001
	},
	{
		id: 537,
		time: 536,
		velocity: 19.2161111111111,
		power: 2359.19850608115,
		road: 4853.50347222222,
		acceleration: -0.212407407407404
	},
	{
		id: 538,
		time: 537,
		velocity: 19.2469444444444,
		power: 1104.85987764966,
		road: 4872.65791666667,
		acceleration: -0.273888888888887
	},
	{
		id: 539,
		time: 538,
		velocity: 18.5894444444444,
		power: -4423.27732097447,
		road: 4891.38953703704,
		acceleration: -0.571759259259263
	},
	{
		id: 540,
		time: 539,
		velocity: 17.5008333333333,
		power: -17867.7369294876,
		road: 4909.15708333333,
		acceleration: -1.35638888888889
	},
	{
		id: 541,
		time: 540,
		velocity: 15.1777777777778,
		power: -23265.8045076902,
		road: 4925.3575462963,
		acceleration: -1.77777777777778
	},
	{
		id: 542,
		time: 541,
		velocity: 13.2561111111111,
		power: -23490.6267826693,
		road: 4939.68921296296,
		acceleration: -1.95981481481481
	},
	{
		id: 543,
		time: 542,
		velocity: 11.6213888888889,
		power: -20196.501722759,
		road: 4952.07949074074,
		acceleration: -1.92296296296296
	},
	{
		id: 544,
		time: 543,
		velocity: 9.40888888888889,
		power: -16337.669315604,
		road: 4962.59833333333,
		acceleration: -1.81990740740741
	},
	{
		id: 545,
		time: 544,
		velocity: 7.79638888888889,
		power: -11602.4954866898,
		road: 4971.43162037037,
		acceleration: -1.5512037037037
	},
	{
		id: 546,
		time: 545,
		velocity: 6.96777777777778,
		power: -4470.96215405565,
		road: 4979.10273148148,
		acceleration: -0.773148148148149
	},
	{
		id: 547,
		time: 546,
		velocity: 7.08944444444444,
		power: 1928.3368566031,
		road: 4986.44638888889,
		acceleration: 0.118240740740741
	},
	{
		id: 548,
		time: 547,
		velocity: 8.15111111111111,
		power: 5616.93870008538,
		road: 4994.15175925926,
		acceleration: 0.605185185185186
	},
	{
		id: 549,
		time: 548,
		velocity: 8.78333333333333,
		power: 10191.1827901217,
		road: 5002.70138888889,
		acceleration: 1.08333333333333
	},
	{
		id: 550,
		time: 549,
		velocity: 10.3394444444444,
		power: 12028.7764308957,
		road: 5012.35652777778,
		acceleration: 1.12768518518518
	},
	{
		id: 551,
		time: 550,
		velocity: 11.5341666666667,
		power: 13938.4352094842,
		road: 5023.15583333333,
		acceleration: 1.16064814814815
	},
	{
		id: 552,
		time: 551,
		velocity: 12.2652777777778,
		power: 12235.2630751706,
		road: 5034.975,
		acceleration: 0.879074074074071
	},
	{
		id: 553,
		time: 552,
		velocity: 12.9766666666667,
		power: 7981.8519657947,
		road: 5047.46101851852,
		acceleration: 0.454629629629631
	},
	{
		id: 554,
		time: 553,
		velocity: 12.8980555555556,
		power: 5087.13067753043,
		road: 5060.27240740741,
		acceleration: 0.196111111111112
	},
	{
		id: 555,
		time: 554,
		velocity: 12.8536111111111,
		power: 3166.30557557242,
		road: 5073.19935185185,
		acceleration: 0.0349999999999984
	},
	{
		id: 556,
		time: 555,
		velocity: 13.0816666666667,
		power: 6638.60327672137,
		road: 5086.29722222222,
		acceleration: 0.306851851851853
	},
	{
		id: 557,
		time: 556,
		velocity: 13.8186111111111,
		power: 10119.2372773322,
		road: 5099.8249537037,
		acceleration: 0.552870370370369
	},
	{
		id: 558,
		time: 557,
		velocity: 14.5122222222222,
		power: 22483.109858025,
		road: 5114.31805555556,
		acceleration: 1.37787037037037
	},
	{
		id: 559,
		time: 558,
		velocity: 17.2152777777778,
		power: 31650.894335318,
		road: 5130.39300925926,
		acceleration: 1.78583333333333
	},
	{
		id: 560,
		time: 559,
		velocity: 19.1761111111111,
		power: 42942.2426719467,
		road: 5148.4474537037,
		acceleration: 2.17314814814815
	},
	{
		id: 561,
		time: 560,
		velocity: 21.0316666666667,
		power: 43909.7505233206,
		road: 5168.55037037037,
		acceleration: 1.9237962962963
	},
	{
		id: 562,
		time: 561,
		velocity: 22.9866666666667,
		power: 34738.2171405242,
		road: 5190.25319444444,
		acceleration: 1.27601851851852
	},
	{
		id: 563,
		time: 562,
		velocity: 23.0041666666667,
		power: 36733.8248045221,
		road: 5213.21523148148,
		acceleration: 1.24240740740741
	},
	{
		id: 564,
		time: 563,
		velocity: 24.7588888888889,
		power: 21671.0045953764,
		road: 5237.04842592593,
		acceleration: 0.499907407407406
	},
	{
		id: 565,
		time: 564,
		velocity: 24.4863888888889,
		power: 17938.3721462306,
		road: 5261.28787037037,
		acceleration: 0.312592592592594
	},
	{
		id: 566,
		time: 565,
		velocity: 23.9419444444444,
		power: -1402.75499822043,
		road: 5285.42578703704,
		acceleration: -0.515648148148149
	},
	{
		id: 567,
		time: 566,
		velocity: 23.2119444444444,
		power: -4414.21097459472,
		road: 5308.98842592593,
		acceleration: -0.634907407407407
	},
	{
		id: 568,
		time: 567,
		velocity: 22.5816666666667,
		power: -3131.28534039271,
		road: 5331.95050925926,
		acceleration: -0.5662037037037
	},
	{
		id: 569,
		time: 568,
		velocity: 22.2433333333333,
		power: -139.670680573213,
		road: 5354.42046296296,
		acceleration: -0.418055555555554
	},
	{
		id: 570,
		time: 569,
		velocity: 21.9577777777778,
		power: 558.819904197893,
		road: 5376.49375,
		acceleration: -0.375277777777782
	},
	{
		id: 571,
		time: 570,
		velocity: 21.4558333333333,
		power: 771.295542161125,
		road: 5398.20157407407,
		acceleration: -0.355648148148145
	},
	{
		id: 572,
		time: 571,
		velocity: 21.1763888888889,
		power: -3218.85121551904,
		road: 5419.46166666667,
		acceleration: -0.539814814814818
	},
	{
		id: 573,
		time: 572,
		velocity: 20.3383333333333,
		power: -3212.71631144415,
		road: 5440.18634259259,
		acceleration: -0.531018518518518
	},
	{
		id: 574,
		time: 573,
		velocity: 19.8627777777778,
		power: -511.670800343873,
		road: 5460.45296296296,
		acceleration: -0.385092592592592
	},
	{
		id: 575,
		time: 574,
		velocity: 20.0211111111111,
		power: 10202.0743656406,
		road: 5480.61291666667,
		acceleration: 0.171759259259261
	},
	{
		id: 576,
		time: 575,
		velocity: 20.8536111111111,
		power: 18871.9019810844,
		road: 5501.15541666666,
		acceleration: 0.593333333333334
	},
	{
		id: 577,
		time: 576,
		velocity: 21.6427777777778,
		power: 20122.6473480015,
		road: 5522.3012037037,
		acceleration: 0.613240740740739
	},
	{
		id: 578,
		time: 577,
		velocity: 21.8608333333333,
		power: 14991.5569582228,
		road: 5543.91986111111,
		acceleration: 0.3325
	},
	{
		id: 579,
		time: 578,
		velocity: 21.8511111111111,
		power: 8373.06820503598,
		road: 5565.70777777778,
		acceleration: 0.0060185185185162
	},
	{
		id: 580,
		time: 579,
		velocity: 21.6608333333333,
		power: 4821.38089004036,
		road: 5587.41810185185,
		acceleration: -0.161203703703698
	},
	{
		id: 581,
		time: 580,
		velocity: 21.3772222222222,
		power: 360.797195993521,
		road: 5608.86342592592,
		acceleration: -0.368796296296296
	},
	{
		id: 582,
		time: 581,
		velocity: 20.7447222222222,
		power: -3063.70705998641,
		road: 5629.86041666666,
		acceleration: -0.527870370370373
	},
	{
		id: 583,
		time: 582,
		velocity: 20.0772222222222,
		power: -5283.62277028754,
		road: 5650.27736111111,
		acceleration: -0.632222222222225
	},
	{
		id: 584,
		time: 583,
		velocity: 19.4805555555556,
		power: -4545.68340806756,
		road: 5670.08421296296,
		acceleration: -0.587962962962962
	},
	{
		id: 585,
		time: 584,
		velocity: 18.9808333333333,
		power: -779.981605671179,
		road: 5689.40712962963,
		acceleration: -0.379907407407405
	},
	{
		id: 586,
		time: 585,
		velocity: 18.9375,
		power: 3530.24066618536,
		road: 5708.47069444444,
		acceleration: -0.138796296296295
	},
	{
		id: 587,
		time: 586,
		velocity: 19.0641666666667,
		power: 6736.87909068164,
		road: 5727.48430555555,
		acceleration: 0.0388888888888843
	},
	{
		id: 588,
		time: 587,
		velocity: 19.0975,
		power: 8435.55904227617,
		road: 5746.58152777778,
		acceleration: 0.128333333333337
	},
	{
		id: 589,
		time: 588,
		velocity: 19.3225,
		power: 8357.15543758355,
		road: 5765.80212962963,
		acceleration: 0.118425925925923
	},
	{
		id: 590,
		time: 589,
		velocity: 19.4194444444444,
		power: 8811.29058274341,
		road: 5785.15050925926,
		acceleration: 0.13712962962963
	},
	{
		id: 591,
		time: 590,
		velocity: 19.5088888888889,
		power: 6819.12191067594,
		road: 5804.58060185185,
		acceleration: 0.026296296296298
	},
	{
		id: 592,
		time: 591,
		velocity: 19.4013888888889,
		power: 4546.45417323491,
		road: 5823.97652777778,
		acceleration: -0.0946296296296296
	},
	{
		id: 593,
		time: 592,
		velocity: 19.1355555555556,
		power: 739.582759978908,
		road: 5843.17768518518,
		acceleration: -0.294907407407408
	},
	{
		id: 594,
		time: 593,
		velocity: 18.6241666666667,
		power: 100.393836924503,
		road: 5862.06990740741,
		acceleration: -0.322962962962965
	},
	{
		id: 595,
		time: 594,
		velocity: 18.4325,
		power: 308.908511882345,
		road: 5880.64837962963,
		acceleration: -0.304537037037033
	},
	{
		id: 596,
		time: 595,
		velocity: 18.2219444444444,
		power: -767.521965049304,
		road: 5898.89509259259,
		acceleration: -0.358981481481482
	},
	{
		id: 597,
		time: 596,
		velocity: 17.5472222222222,
		power: -881.930074012763,
		road: 5916.78268518518,
		acceleration: -0.359259259259261
	},
	{
		id: 598,
		time: 597,
		velocity: 17.3547222222222,
		power: -506.176809412174,
		road: 5934.32513888889,
		acceleration: -0.331018518518519
	},
	{
		id: 599,
		time: 598,
		velocity: 17.2288888888889,
		power: -221.495814455577,
		road: 5951.54805555555,
		acceleration: -0.308055555555555
	},
	{
		id: 600,
		time: 599,
		velocity: 16.6230555555556,
		power: -951.79886023974,
		road: 5968.44333333333,
		acceleration: -0.347222222222221
	},
	{
		id: 601,
		time: 600,
		velocity: 16.3130555555556,
		power: -1268.56453705872,
		road: 5984.98407407407,
		acceleration: -0.361851851851853
	},
	{
		id: 602,
		time: 601,
		velocity: 16.1433333333333,
		power: -763.537290856853,
		road: 6001.18157407407,
		acceleration: -0.32462962962963
	},
	{
		id: 603,
		time: 602,
		velocity: 15.6491666666667,
		power: 397.185483838888,
		road: 6017.09472222222,
		acceleration: -0.244074074074073
	},
	{
		id: 604,
		time: 603,
		velocity: 15.5808333333333,
		power: 1029.85721211684,
		road: 6032.78703703704,
		acceleration: -0.197592592592596
	},
	{
		id: 605,
		time: 604,
		velocity: 15.5505555555556,
		power: 3469.69957511046,
		road: 6048.3649537037,
		acceleration: -0.031203703703703
	},
	{
		id: 606,
		time: 605,
		velocity: 15.5555555555556,
		power: 4091.66183670318,
		road: 6063.93273148148,
		acceleration: 0.0109259259259282
	},
	{
		id: 607,
		time: 606,
		velocity: 15.6136111111111,
		power: 4573.55923816733,
		road: 6079.52712962963,
		acceleration: 0.0423148148148158
	},
	{
		id: 608,
		time: 607,
		velocity: 15.6775,
		power: 4615.53175449544,
		road: 6095.16444444444,
		acceleration: 0.0435185185185176
	},
	{
		id: 609,
		time: 608,
		velocity: 15.6861111111111,
		power: 5046.14936003927,
		road: 6110.85856481481,
		acceleration: 0.0700925925925908
	},
	{
		id: 610,
		time: 609,
		velocity: 15.8238888888889,
		power: 4422.64714815335,
		road: 6126.60111111111,
		acceleration: 0.0267592592592578
	},
	{
		id: 611,
		time: 610,
		velocity: 15.7577777777778,
		power: 5494.32615481748,
		road: 6142.40476851852,
		acceleration: 0.0954629629629622
	},
	{
		id: 612,
		time: 611,
		velocity: 15.9725,
		power: 4467.40297202379,
		road: 6158.26879629629,
		acceleration: 0.0252777777777791
	},
	{
		id: 613,
		time: 612,
		velocity: 15.8997222222222,
		power: 4915.8109083795,
		road: 6174.17212962963,
		acceleration: 0.0533333333333346
	},
	{
		id: 614,
		time: 613,
		velocity: 15.9177777777778,
		power: 3840.96682990479,
		road: 6190.09314814815,
		acceleration: -0.0179629629629616
	},
	{
		id: 615,
		time: 614,
		velocity: 15.9186111111111,
		power: 3379.74482245309,
		road: 6205.98157407407,
		acceleration: -0.0472222222222243
	},
	{
		id: 616,
		time: 615,
		velocity: 15.7580555555556,
		power: 1775.07404131097,
		road: 6221.7712037037,
		acceleration: -0.15037037037037
	},
	{
		id: 617,
		time: 616,
		velocity: 15.4666666666667,
		power: 749.07212725729,
		road: 6237.37837962963,
		acceleration: -0.214537037037038
	},
	{
		id: 618,
		time: 617,
		velocity: 15.275,
		power: 996.14124909335,
		road: 6252.78152777778,
		acceleration: -0.193518518518516
	},
	{
		id: 619,
		time: 618,
		velocity: 15.1775,
		power: 2352.73479780168,
		road: 6268.03925925926,
		acceleration: -0.0973148148148155
	},
	{
		id: 620,
		time: 619,
		velocity: 15.1747222222222,
		power: 2784.49289679997,
		road: 6283.21569444444,
		acceleration: -0.0652777777777782
	},
	{
		id: 621,
		time: 620,
		velocity: 15.0791666666667,
		power: 2904.28309244407,
		road: 6298.33189814815,
		acceleration: -0.0551851851851843
	},
	{
		id: 622,
		time: 621,
		velocity: 15.0119444444444,
		power: 2268.15215952611,
		road: 6313.37194444444,
		acceleration: -0.0971296296296309
	},
	{
		id: 623,
		time: 622,
		velocity: 14.8833333333333,
		power: 3299.31025055163,
		road: 6328.35171296296,
		acceleration: -0.0234259259259275
	},
	{
		id: 624,
		time: 623,
		velocity: 15.0088888888889,
		power: 5070.66483164675,
		road: 6343.36916666666,
		acceleration: 0.0987962962962978
	},
	{
		id: 625,
		time: 624,
		velocity: 15.3083333333333,
		power: 7000.61113727768,
		road: 6358.54865740741,
		acceleration: 0.225277777777777
	},
	{
		id: 626,
		time: 625,
		velocity: 15.5591666666667,
		power: 8281.32016861662,
		road: 6373.99041666666,
		acceleration: 0.29925925925926
	},
	{
		id: 627,
		time: 626,
		velocity: 15.9066666666667,
		power: 8941.9642446064,
		road: 6389.745,
		acceleration: 0.326388888888889
	},
	{
		id: 628,
		time: 627,
		velocity: 16.2875,
		power: 8813.81349855744,
		road: 6405.81314814815,
		acceleration: 0.300740740740741
	},
	{
		id: 629,
		time: 628,
		velocity: 16.4613888888889,
		power: 8759.77557091953,
		road: 6422.1725,
		acceleration: 0.281666666666666
	},
	{
		id: 630,
		time: 629,
		velocity: 16.7516666666667,
		power: 8087.59142551189,
		road: 6438.78569444444,
		acceleration: 0.226018518518519
	},
	{
		id: 631,
		time: 630,
		velocity: 16.9655555555556,
		power: 8692.46908727486,
		road: 6455.63777777778,
		acceleration: 0.251759259259259
	},
	{
		id: 632,
		time: 631,
		velocity: 17.2166666666667,
		power: 8318.56408746776,
		road: 6472.72421296296,
		acceleration: 0.216944444444444
	},
	{
		id: 633,
		time: 632,
		velocity: 17.4025,
		power: 7201.6247663068,
		road: 6489.98939814815,
		acceleration: 0.140555555555558
	},
	{
		id: 634,
		time: 633,
		velocity: 17.3872222222222,
		power: 6055.57350239311,
		road: 6507.35833333333,
		acceleration: 0.0669444444444451
	},
	{
		id: 635,
		time: 634,
		velocity: 17.4175,
		power: 4991.8289486941,
		road: 6524.76157407407,
		acceleration: 0.00166666666666515
	},
	{
		id: 636,
		time: 635,
		velocity: 17.4075,
		power: 5308.69280210611,
		road: 6542.17578703703,
		acceleration: 0.0202777777777783
	},
	{
		id: 637,
		time: 636,
		velocity: 17.4480555555556,
		power: 4085.40663417157,
		road: 6559.57384259259,
		acceleration: -0.0525925925925925
	},
	{
		id: 638,
		time: 637,
		velocity: 17.2597222222222,
		power: 3603.09019879401,
		road: 6576.90587962963,
		acceleration: -0.0794444444444444
	},
	{
		id: 639,
		time: 638,
		velocity: 17.1691666666667,
		power: 1918.13665047029,
		road: 6594.10935185185,
		acceleration: -0.177685185185187
	},
	{
		id: 640,
		time: 639,
		velocity: 16.915,
		power: 1590.48115777869,
		road: 6611.1275,
		acceleration: -0.192962962962962
	},
	{
		id: 641,
		time: 640,
		velocity: 16.6808333333333,
		power: 1403.05103484213,
		road: 6627.94930555555,
		acceleration: -0.199722222222224
	},
	{
		id: 642,
		time: 641,
		velocity: 16.57,
		power: 1330.9477046188,
		road: 6644.57152777777,
		acceleration: -0.199444444444442
	},
	{
		id: 643,
		time: 642,
		velocity: 16.3166666666667,
		power: 1797.03047970688,
		road: 6661.01129629629,
		acceleration: -0.165462962962962
	},
	{
		id: 644,
		time: 643,
		velocity: 16.1844444444444,
		power: 1511.15195321837,
		road: 6677.27861111111,
		acceleration: -0.179444444444446
	},
	{
		id: 645,
		time: 644,
		velocity: 16.0316666666667,
		power: 1059.54387558765,
		road: 6693.35412037037,
		acceleration: -0.204166666666667
	},
	{
		id: 646,
		time: 645,
		velocity: 15.7041666666667,
		power: 1700.01196246093,
		road: 6709.24856481481,
		acceleration: -0.157962962962962
	},
	{
		id: 647,
		time: 646,
		velocity: 15.7105555555556,
		power: 622.01770573125,
		road: 6724.95152777777,
		acceleration: -0.225000000000001
	},
	{
		id: 648,
		time: 647,
		velocity: 15.3566666666667,
		power: 612.686140740685,
		road: 6740.43143518518,
		acceleration: -0.22111111111111
	},
	{
		id: 649,
		time: 648,
		velocity: 15.0408333333333,
		power: 59.1233891850795,
		road: 6755.67361111111,
		acceleration: -0.254351851851851
	},
	{
		id: 650,
		time: 649,
		velocity: 14.9475,
		power: -2017.31795562949,
		road: 6770.59138888889,
		acceleration: -0.394444444444446
	},
	{
		id: 651,
		time: 650,
		velocity: 14.1733333333333,
		power: -3942.94022827809,
		road: 6785.04652777777,
		acceleration: -0.530833333333332
	},
	{
		id: 652,
		time: 651,
		velocity: 13.4483333333333,
		power: -11283.2795117024,
		road: 6798.68680555555,
		acceleration: -1.09888888888889
	},
	{
		id: 653,
		time: 652,
		velocity: 11.6508333333333,
		power: -14693.7381526921,
		road: 6811.04773148148,
		acceleration: -1.45981481481482
	},
	{
		id: 654,
		time: 653,
		velocity: 9.79388888888889,
		power: -14018.9298458696,
		road: 6821.90435185185,
		acceleration: -1.5487962962963
	},
	{
		id: 655,
		time: 654,
		velocity: 8.80194444444444,
		power: -13809.6699283994,
		road: 6831.11129629629,
		acceleration: -1.75055555555556
	},
	{
		id: 656,
		time: 655,
		velocity: 6.39916666666667,
		power: -10418.0467946542,
		road: 6838.63550925925,
		acceleration: -1.61490740740741
	},
	{
		id: 657,
		time: 656,
		velocity: 4.94916666666667,
		power: -9967.81056256288,
		road: 6844.3636574074,
		acceleration: -1.97722222222222
	},
	{
		id: 658,
		time: 657,
		velocity: 2.87027777777778,
		power: -5212.85276039575,
		road: 6848.34550925925,
		acceleration: -1.51537037037037
	},
	{
		id: 659,
		time: 658,
		velocity: 1.85305555555556,
		power: -2893.79135708111,
		road: 6850.90912037037,
		acceleration: -1.32111111111111
	},
	{
		id: 660,
		time: 659,
		velocity: 0.985833333333333,
		power: -1118.51233735806,
		road: 6852.33379629629,
		acceleration: -0.956759259259259
	},
	{
		id: 661,
		time: 660,
		velocity: 0,
		power: -295.969067354741,
		road: 6852.97125,
		acceleration: -0.617685185185185
	},
	{
		id: 662,
		time: 661,
		velocity: 0,
		power: -31.2998624269006,
		road: 6853.13555555555,
		acceleration: -0.328611111111111
	},
	{
		id: 663,
		time: 662,
		velocity: 0,
		power: 0,
		road: 6853.13555555555,
		acceleration: 0
	},
	{
		id: 664,
		time: 663,
		velocity: 0,
		power: 135.929479078343,
		road: 6853.37337962963,
		acceleration: 0.475648148148148
	},
	{
		id: 665,
		time: 664,
		velocity: 1.42694444444444,
		power: 883.899739330614,
		road: 6854.29236111111,
		acceleration: 0.886666666666667
	},
	{
		id: 666,
		time: 665,
		velocity: 2.66,
		power: 1537.01570797482,
		road: 6856.05087962963,
		acceleration: 0.792407407407407
	},
	{
		id: 667,
		time: 666,
		velocity: 2.37722222222222,
		power: 998.248313131746,
		road: 6858.3675,
		acceleration: 0.323796296296296
	},
	{
		id: 668,
		time: 667,
		velocity: 2.39833333333333,
		power: 246.041463574868,
		road: 6860.83319444444,
		acceleration: -0.0256481481481483
	},
	{
		id: 669,
		time: 668,
		velocity: 2.58305555555556,
		power: 721.518549476646,
		road: 6863.37041666666,
		acceleration: 0.168703703703704
	},
	{
		id: 670,
		time: 669,
		velocity: 2.88333333333333,
		power: 1001.09936269439,
		road: 6866.11814814814,
		acceleration: 0.252314814814815
	},
	{
		id: 671,
		time: 670,
		velocity: 3.15527777777778,
		power: 1630.24804189974,
		road: 6869.20398148148,
		acceleration: 0.423888888888889
	},
	{
		id: 672,
		time: 671,
		velocity: 3.85472222222222,
		power: 1704.9053949975,
		road: 6872.69208333333,
		acceleration: 0.380648148148148
	},
	{
		id: 673,
		time: 672,
		velocity: 4.02527777777778,
		power: 1753.06162207329,
		road: 6876.5424074074,
		acceleration: 0.343796296296297
	},
	{
		id: 674,
		time: 673,
		velocity: 4.18666666666667,
		power: 1073.79371482902,
		road: 6880.63439814814,
		acceleration: 0.139537037037037
	},
	{
		id: 675,
		time: 674,
		velocity: 4.27333333333333,
		power: 971.162154198324,
		road: 6884.84879629629,
		acceleration: 0.105277777777778
	},
	{
		id: 676,
		time: 675,
		velocity: 4.34111111111111,
		power: 903.050163378166,
		road: 6889.15726851851,
		acceleration: 0.0828703703703697
	},
	{
		id: 677,
		time: 676,
		velocity: 4.43527777777778,
		power: 636.569717332455,
		road: 6893.51504629629,
		acceleration: 0.0157407407407408
	},
	{
		id: 678,
		time: 677,
		velocity: 4.32055555555556,
		power: 624.29317804974,
		road: 6897.88680555555,
		acceleration: 0.0122222222222224
	},
	{
		id: 679,
		time: 678,
		velocity: 4.37777777777778,
		power: 638.877243213676,
		road: 6902.27226851852,
		acceleration: 0.0151851851851843
	},
	{
		id: 680,
		time: 679,
		velocity: 4.48083333333333,
		power: 716.157296130222,
		road: 6906.68166666666,
		acceleration: 0.0326851851851853
	},
	{
		id: 681,
		time: 680,
		velocity: 4.41861111111111,
		power: 617.467144753316,
		road: 6911.11157407407,
		acceleration: 0.00833333333333375
	},
	{
		id: 682,
		time: 681,
		velocity: 4.40277777777778,
		power: 541.400412366475,
		road: 6915.54078703703,
		acceleration: -0.00972222222222197
	},
	{
		id: 683,
		time: 682,
		velocity: 4.45166666666667,
		power: 540.796453275055,
		road: 6919.96037037037,
		acceleration: -0.00953703703703734
	},
	{
		id: 684,
		time: 683,
		velocity: 4.39,
		power: 389.342138899543,
		road: 6924.35273148148,
		acceleration: -0.0449074074074067
	},
	{
		id: 685,
		time: 684,
		velocity: 4.26805555555556,
		power: 184.898451837885,
		road: 6928.6762037037,
		acceleration: -0.0928703703703713
	},
	{
		id: 686,
		time: 685,
		velocity: 4.17305555555556,
		power: 139.808491690093,
		road: 6932.90194444444,
		acceleration: -0.102592592592592
	},
	{
		id: 687,
		time: 686,
		velocity: 4.08222222222222,
		power: 161.909469529323,
		road: 6937.02856481481,
		acceleration: -0.0956481481481486
	},
	{
		id: 688,
		time: 687,
		velocity: 3.98111111111111,
		power: 179.691919608344,
		road: 6941.06254629629,
		acceleration: -0.0896296296296293
	},
	{
		id: 689,
		time: 688,
		velocity: 3.90416666666667,
		power: 294.92806813346,
		road: 6945.02282407407,
		acceleration: -0.0577777777777779
	},
	{
		id: 690,
		time: 689,
		velocity: 3.90888888888889,
		power: 375.698199846911,
		road: 6948.93675925926,
		acceleration: -0.0349074074074069
	},
	{
		id: 691,
		time: 690,
		velocity: 3.87638888888889,
		power: 430.055849408357,
		road: 6952.82356481481,
		acceleration: -0.0193518518518516
	},
	{
		id: 692,
		time: 691,
		velocity: 3.84611111111111,
		power: 535.213494459165,
		road: 6956.70537037037,
		acceleration: 0.00935185185185183
	},
	{
		id: 693,
		time: 692,
		velocity: 3.93694444444444,
		power: 515.01959709626,
		road: 6960.59365740741,
		acceleration: 0.00361111111111079
	},
	{
		id: 694,
		time: 693,
		velocity: 3.88722222222222,
		power: 708.774737429882,
		road: 6964.51101851852,
		acceleration: 0.0545370370370368
	},
	{
		id: 695,
		time: 694,
		velocity: 4.00972222222222,
		power: 418.251701272282,
		road: 6968.44361111111,
		acceleration: -0.0240740740740737
	},
	{
		id: 696,
		time: 695,
		velocity: 3.86472222222222,
		power: 671.755980938715,
		road: 6972.38583333333,
		acceleration: 0.043333333333333
	},
	{
		id: 697,
		time: 696,
		velocity: 4.01722222222222,
		power: 685.99345963458,
		road: 6976.37217592592,
		acceleration: 0.0449074074074076
	},
	{
		id: 698,
		time: 697,
		velocity: 4.14444444444444,
		power: 676.362136424828,
		road: 6980.40111111111,
		acceleration: 0.0402777777777779
	},
	{
		id: 699,
		time: 698,
		velocity: 3.98555555555556,
		power: 644.689888407495,
		road: 6984.46537037037,
		acceleration: 0.0303703703703704
	},
	{
		id: 700,
		time: 699,
		velocity: 4.10833333333333,
		power: 114.429382139778,
		road: 6988.49152777778,
		acceleration: -0.106574074074074
	},
	{
		id: 701,
		time: 700,
		velocity: 3.82472222222222,
		power: 732.043504906224,
		road: 6992.49254629629,
		acceleration: 0.0562962962962961
	},
	{
		id: 702,
		time: 701,
		velocity: 4.15444444444444,
		power: 394.283531591265,
		road: 6996.50523148148,
		acceleration: -0.0329629629629635
	},
	{
		id: 703,
		time: 702,
		velocity: 4.00944444444444,
		power: 824.841699620511,
		road: 7000.54078703703,
		acceleration: 0.0787037037037046
	},
	{
		id: 704,
		time: 703,
		velocity: 4.06083333333333,
		power: 604.811303808003,
		road: 7004.62527777778,
		acceleration: 0.0191666666666661
	},
	{
		id: 705,
		time: 704,
		velocity: 4.21194444444444,
		power: 692.236911216908,
		road: 7008.73949074074,
		acceleration: 0.0402777777777779
	},
	{
		id: 706,
		time: 705,
		velocity: 4.13027777777778,
		power: 831.537426059498,
		road: 7012.91023148148,
		acceleration: 0.0727777777777776
	},
	{
		id: 707,
		time: 706,
		velocity: 4.27916666666667,
		power: 501.302123413904,
		road: 7017.11152777778,
		acceleration: -0.0116666666666667
	},
	{
		id: 708,
		time: 707,
		velocity: 4.17694444444444,
		power: 604.574721154223,
		road: 7021.31407407407,
		acceleration: 0.0141666666666671
	},
	{
		id: 709,
		time: 708,
		velocity: 4.17277777777778,
		power: 542.68678430506,
		road: 7025.52291666667,
		acceleration: -0.00157407407407373
	},
	{
		id: 710,
		time: 709,
		velocity: 4.27444444444444,
		power: 848.823729281932,
		road: 7029.7675,
		acceleration: 0.0730555555555554
	},
	{
		id: 711,
		time: 710,
		velocity: 4.39611111111111,
		power: 663.270666451701,
		road: 7034.06106481481,
		acceleration: 0.0249074074074063
	},
	{
		id: 712,
		time: 711,
		velocity: 4.2475,
		power: 435.239863918552,
		road: 7038.35162037037,
		acceleration: -0.0309259259259251
	},
	{
		id: 713,
		time: 712,
		velocity: 4.18166666666667,
		power: 41.306469551792,
		road: 7042.56319444444,
		acceleration: -0.127037037037038
	},
	{
		id: 714,
		time: 713,
		velocity: 4.015,
		power: -267.008986600777,
		road: 7046.60819444444,
		acceleration: -0.206111111111111
	},
	{
		id: 715,
		time: 714,
		velocity: 3.62916666666667,
		power: -828.32083132703,
		road: 7050.36638888889,
		acceleration: -0.3675
	},
	{
		id: 716,
		time: 715,
		velocity: 3.07916666666667,
		power: -957.61528595419,
		road: 7053.7237037037,
		acceleration: -0.434259259259259
	},
	{
		id: 717,
		time: 716,
		velocity: 2.71222222222222,
		power: -780.474504333983,
		road: 7056.65759259259,
		acceleration: -0.412592592592593
	},
	{
		id: 718,
		time: 717,
		velocity: 2.39138888888889,
		power: -214.93864550838,
		road: 7059.27629629629,
		acceleration: -0.217777777777777
	},
	{
		id: 719,
		time: 718,
		velocity: 2.42583333333333,
		power: -50.0134777094851,
		road: 7061.70990740741,
		acceleration: -0.152407407407408
	},
	{
		id: 720,
		time: 719,
		velocity: 2.255,
		power: 187.03594668404,
		road: 7064.04430555555,
		acceleration: -0.0460185185185185
	},
	{
		id: 721,
		time: 720,
		velocity: 2.25333333333333,
		power: 90.2729382337336,
		road: 7066.31152777778,
		acceleration: -0.0883333333333334
	},
	{
		id: 722,
		time: 721,
		velocity: 2.16083333333333,
		power: 223.304375771164,
		road: 7068.52273148148,
		acceleration: -0.0237037037037036
	},
	{
		id: 723,
		time: 722,
		velocity: 2.18388888888889,
		power: 243.500693042189,
		road: 7070.71555555555,
		acceleration: -0.0130555555555554
	},
	{
		id: 724,
		time: 723,
		velocity: 2.21416666666667,
		power: 215.878264248612,
		road: 7072.88916666667,
		acceleration: -0.02537037037037
	},
	{
		id: 725,
		time: 724,
		velocity: 2.08472222222222,
		power: 224.274097764219,
		road: 7075.0400462963,
		acceleration: -0.0200925925925932
	},
	{
		id: 726,
		time: 725,
		velocity: 2.12361111111111,
		power: 279.715190126983,
		road: 7077.18462962963,
		acceleration: 0.00749999999999984
	},
	{
		id: 727,
		time: 726,
		velocity: 2.23666666666667,
		power: 446.639547885144,
		road: 7079.37537037037,
		acceleration: 0.0848148148148149
	},
	{
		id: 728,
		time: 727,
		velocity: 2.33916666666667,
		power: 370.50799599535,
		road: 7081.63,
		acceleration: 0.0429629629629633
	},
	{
		id: 729,
		time: 728,
		velocity: 2.2525,
		power: 440.877273308129,
		road: 7083.94143518518,
		acceleration: 0.0706481481481478
	},
	{
		id: 730,
		time: 729,
		velocity: 2.44861111111111,
		power: 341.267680584441,
		road: 7086.29921296296,
		acceleration: 0.0220370370370371
	},
	{
		id: 731,
		time: 730,
		velocity: 2.40527777777778,
		power: 419.828531138604,
		road: 7088.6950462963,
		acceleration: 0.0540740740740744
	},
	{
		id: 732,
		time: 731,
		velocity: 2.41472222222222,
		power: 389.979097621855,
		road: 7091.13671296296,
		acceleration: 0.0375925925925924
	},
	{
		id: 733,
		time: 732,
		velocity: 2.56138888888889,
		power: 645.007614638135,
		road: 7093.66606481481,
		acceleration: 0.137777777777778
	},
	{
		id: 734,
		time: 733,
		velocity: 2.81861111111111,
		power: 741.102695919397,
		road: 7096.34439814815,
		acceleration: 0.160185185185185
	},
	{
		id: 735,
		time: 734,
		velocity: 2.89527777777778,
		power: 816.560362444641,
		road: 7099.18814814815,
		acceleration: 0.170648148148149
	},
	{
		id: 736,
		time: 735,
		velocity: 3.07333333333333,
		power: 642.836484338461,
		road: 7102.16481481481,
		acceleration: 0.0951851851851853
	},
	{
		id: 737,
		time: 736,
		velocity: 3.10416666666667,
		power: 713.005715212566,
		road: 7105.24467592593,
		acceleration: 0.111203703703703
	},
	{
		id: 738,
		time: 737,
		velocity: 3.22888888888889,
		power: 547.238778525207,
		road: 7108.40486111111,
		acceleration: 0.0494444444444446
	},
	{
		id: 739,
		time: 738,
		velocity: 3.22166666666667,
		power: 414.934817484149,
		road: 7111.59180555556,
		acceleration: 0.00407407407407412
	},
	{
		id: 740,
		time: 739,
		velocity: 3.11638888888889,
		power: 223.953100933326,
		road: 7114.75162037037,
		acceleration: -0.0583333333333336
	},
	{
		id: 741,
		time: 740,
		velocity: 3.05388888888889,
		power: 126.017197036494,
		road: 7117.83740740741,
		acceleration: -0.0897222222222225
	},
	{
		id: 742,
		time: 741,
		velocity: 2.9525,
		power: -10.0270226140431,
		road: 7120.81037037037,
		acceleration: -0.135925925925926
	},
	{
		id: 743,
		time: 742,
		velocity: 2.70861111111111,
		power: -241.411891879338,
		road: 7123.60393518519,
		acceleration: -0.22287037037037
	},
	{
		id: 744,
		time: 743,
		velocity: 2.38527777777778,
		power: -88.1093402518259,
		road: 7126.20259259259,
		acceleration: -0.166944444444444
	},
	{
		id: 745,
		time: 744,
		velocity: 2.45166666666667,
		power: -97.302526264917,
		road: 7128.6312962963,
		acceleration: -0.172962962962963
	},
	{
		id: 746,
		time: 745,
		velocity: 2.18972222222222,
		power: 158.388212883277,
		road: 7130.94439814815,
		acceleration: -0.0582407407407408
	},
	{
		id: 747,
		time: 746,
		velocity: 2.21055555555556,
		power: 89.4320505003471,
		road: 7133.18430555556,
		acceleration: -0.0881481481481479
	},
	{
		id: 748,
		time: 747,
		velocity: 2.18722222222222,
		power: 148.571489609238,
		road: 7135.35125,
		acceleration: -0.0577777777777779
	},
	{
		id: 749,
		time: 748,
		velocity: 2.01638888888889,
		power: 155.095837667037,
		road: 7137.46305555556,
		acceleration: -0.0524999999999998
	},
	{
		id: 750,
		time: 749,
		velocity: 2.05305555555556,
		power: 262.005153376573,
		road: 7139.54986111111,
		acceleration: 0.00249999999999995
	},
	{
		id: 751,
		time: 750,
		velocity: 2.19472222222222,
		power: 386.077020333596,
		road: 7141.66898148148,
		acceleration: 0.0621296296296294
	},
	{
		id: 752,
		time: 751,
		velocity: 2.20277777777778,
		power: 313.735710235574,
		road: 7143.83064814815,
		acceleration: 0.0229629629629629
	},
	{
		id: 753,
		time: 752,
		velocity: 2.12194444444444,
		power: 224.050066963448,
		road: 7145.99337962963,
		acceleration: -0.020833333333333
	},
	{
		id: 754,
		time: 753,
		velocity: 2.13222222222222,
		power: 259.799995154446,
		road: 7148.14435185185,
		acceleration: -0.00268518518518546
	},
	{
		id: 755,
		time: 754,
		velocity: 2.19472222222222,
		power: 271.341987903419,
		road: 7150.29546296296,
		acceleration: 0.00296296296296283
	},
	{
		id: 756,
		time: 755,
		velocity: 2.13083333333333,
		power: 286.006451872556,
		road: 7152.45291666667,
		acceleration: 0.00972222222222241
	},
	{
		id: 757,
		time: 756,
		velocity: 2.16138888888889,
		power: 188.274402127762,
		road: 7154.59652777778,
		acceleration: -0.0374074074074073
	},
	{
		id: 758,
		time: 757,
		velocity: 2.0825,
		power: 327.876250682006,
		road: 7156.73717592593,
		acceleration: 0.0314814814814817
	},
	{
		id: 759,
		time: 758,
		velocity: 2.22527777777778,
		power: 252.904753411328,
		road: 7158.89046296296,
		acceleration: -0.00620370370370393
	},
	{
		id: 760,
		time: 759,
		velocity: 2.14277777777778,
		power: 437.731426500978,
		road: 7161.08092592593,
		acceleration: 0.0805555555555557
	},
	{
		id: 761,
		time: 760,
		velocity: 2.32416666666667,
		power: 210.752469398363,
		road: 7163.29671296296,
		acceleration: -0.0299074074074075
	},
	{
		id: 762,
		time: 761,
		velocity: 2.13555555555556,
		power: 369.89686676749,
		road: 7165.52013888889,
		acceleration: 0.0451851851851854
	},
	{
		id: 763,
		time: 762,
		velocity: 2.27833333333333,
		power: 421.364996235039,
		road: 7167.79847222222,
		acceleration: 0.0646296296296294
	},
	{
		id: 764,
		time: 763,
		velocity: 2.51805555555556,
		power: 657.788748037304,
		road: 7170.18884259259,
		acceleration: 0.159444444444445
	},
	{
		id: 765,
		time: 764,
		velocity: 2.61388888888889,
		power: 720.120876416893,
		road: 7172.74203703704,
		acceleration: 0.166203703703704
	},
	{
		id: 766,
		time: 765,
		velocity: 2.77694444444444,
		power: 606.746789643692,
		road: 7175.43148148148,
		acceleration: 0.106296296296296
	},
	{
		id: 767,
		time: 766,
		velocity: 2.83694444444444,
		power: 360.862318919052,
		road: 7178.1775,
		acceleration: 0.00685185185185189
	},
	{
		id: 768,
		time: 767,
		velocity: 2.63444444444444,
		power: 266.083250174922,
		road: 7180.91240740741,
		acceleration: -0.029074074074074
	},
	{
		id: 769,
		time: 768,
		velocity: 2.68972222222222,
		power: 472.174129965354,
		road: 7183.65759259259,
		acceleration: 0.0496296296296297
	},
	{
		id: 770,
		time: 769,
		velocity: 2.98583333333333,
		power: 550.756521858499,
		road: 7186.4650462963,
		acceleration: 0.074907407407407
	},
	{
		id: 771,
		time: 770,
		velocity: 2.85916666666667,
		power: 436.536456332238,
		road: 7189.32439814815,
		acceleration: 0.0288888888888894
	},
	{
		id: 772,
		time: 771,
		velocity: 2.77638888888889,
		power: 129.549247199759,
		road: 7192.15634259259,
		acceleration: -0.0837037037037036
	},
	{
		id: 773,
		time: 772,
		velocity: 2.73472222222222,
		power: 192.91530776525,
		road: 7194.91740740741,
		acceleration: -0.0580555555555557
	},
	{
		id: 774,
		time: 773,
		velocity: 2.685,
		power: 400.964971308202,
		road: 7197.66064814815,
		acceleration: 0.0224074074074077
	},
	{
		id: 775,
		time: 774,
		velocity: 2.84361111111111,
		power: 323.066538627726,
		road: 7200.41115740741,
		acceleration: -0.00787037037037086
	},
	{
		id: 776,
		time: 775,
		velocity: 2.71111111111111,
		power: 547.230795576774,
		road: 7203.19541666667,
		acceleration: 0.0753703703703703
	},
	{
		id: 777,
		time: 776,
		velocity: 2.91111111111111,
		power: 636.05467509252,
		road: 7206.06800925926,
		acceleration: 0.101296296296296
	},
	{
		id: 778,
		time: 777,
		velocity: 3.1475,
		power: 891.690436551314,
		road: 7209.08092592593,
		acceleration: 0.179351851851853
	},
	{
		id: 779,
		time: 778,
		velocity: 3.24916666666667,
		power: 940.860573099636,
		road: 7212.27226851852,
		acceleration: 0.1775
	},
	{
		id: 780,
		time: 779,
		velocity: 3.44361111111111,
		power: 544.707262466304,
		road: 7215.5725462963,
		acceleration: 0.0403703703703693
	},
	{
		id: 781,
		time: 780,
		velocity: 3.26861111111111,
		power: 567.867248177776,
		road: 7218.91564814815,
		acceleration: 0.0452777777777782
	},
	{
		id: 782,
		time: 781,
		velocity: 3.385,
		power: 322.650206500879,
		road: 7222.26527777778,
		acceleration: -0.0322222222222219
	},
	{
		id: 783,
		time: 782,
		velocity: 3.34694444444444,
		power: 565.391877964357,
		road: 7225.62069444444,
		acceleration: 0.0437962962962959
	},
	{
		id: 784,
		time: 783,
		velocity: 3.4,
		power: 453.24159351863,
		road: 7229.00171296296,
		acceleration: 0.00740740740740797
	},
	{
		id: 785,
		time: 784,
		velocity: 3.40722222222222,
		power: 573.433060066994,
		road: 7232.40814814815,
		acceleration: 0.0434259259259258
	},
	{
		id: 786,
		time: 785,
		velocity: 3.47722222222222,
		power: 493.442576168829,
		road: 7235.84490740741,
		acceleration: 0.0172222222222218
	},
	{
		id: 787,
		time: 786,
		velocity: 3.45166666666667,
		power: 431.662151383089,
		road: 7239.28925925926,
		acceleration: -0.00203703703703662
	},
	{
		id: 788,
		time: 787,
		velocity: 3.40111111111111,
		power: 291.655735661603,
		road: 7242.71050925926,
		acceleration: -0.0441666666666669
	},
	{
		id: 789,
		time: 788,
		velocity: 3.34472222222222,
		power: 305.135454918166,
		road: 7246.09032407407,
		acceleration: -0.0387037037037037
	},
	{
		id: 790,
		time: 789,
		velocity: 3.33555555555556,
		power: 495.929824337083,
		road: 7249.46138888889,
		acceleration: 0.0212037037037036
	},
	{
		id: 791,
		time: 790,
		velocity: 3.46472222222222,
		power: 455.705239795991,
		road: 7252.84703703704,
		acceleration: 0.00796296296296317
	},
	{
		id: 792,
		time: 791,
		velocity: 3.36861111111111,
		power: 487.425256245702,
		road: 7256.24527777778,
		acceleration: 0.0172222222222227
	},
	{
		id: 793,
		time: 792,
		velocity: 3.38722222222222,
		power: 550.834459685918,
		road: 7259.66986111111,
		acceleration: 0.0354629629629626
	},
	{
		id: 794,
		time: 793,
		velocity: 3.57111111111111,
		power: 627.446221499446,
		road: 7263.14032407407,
		acceleration: 0.0562962962962965
	},
	{
		id: 795,
		time: 794,
		velocity: 3.5375,
		power: 713.576993442303,
		road: 7266.67796296296,
		acceleration: 0.0780555555555553
	},
	{
		id: 796,
		time: 795,
		velocity: 3.62138888888889,
		power: 587.617981019571,
		road: 7270.27337962963,
		acceleration: 0.0374999999999996
	},
	{
		id: 797,
		time: 796,
		velocity: 3.68361111111111,
		power: 682.700550467523,
		road: 7273.91875,
		acceleration: 0.0624074074074077
	},
	{
		id: 798,
		time: 797,
		velocity: 3.72472222222222,
		power: 497.585056919758,
		road: 7277.59902777778,
		acceleration: 0.00740740740740753
	},
	{
		id: 799,
		time: 798,
		velocity: 3.64361111111111,
		power: 490.751640495357,
		road: 7281.28560185185,
		acceleration: 0.0051851851851854
	},
	{
		id: 800,
		time: 799,
		velocity: 3.69916666666667,
		power: 388.857562709057,
		road: 7284.96296296296,
		acceleration: -0.0236111111111117
	},
	{
		id: 801,
		time: 800,
		velocity: 3.65388888888889,
		power: 631.375047014026,
		road: 7288.65115740741,
		acceleration: 0.0452777777777782
	},
	{
		id: 802,
		time: 801,
		velocity: 3.77944444444444,
		power: 649.246061176211,
		road: 7292.38592592593,
		acceleration: 0.04787037037037
	},
	{
		id: 803,
		time: 802,
		velocity: 3.84277777777778,
		power: 886.404098565761,
		road: 7296.19925925926,
		acceleration: 0.109259259259259
	},
	{
		id: 804,
		time: 803,
		velocity: 3.98166666666667,
		power: 516.777625423923,
		road: 7300.06962962963,
		acceleration: 0.00481481481481483
	},
	{
		id: 805,
		time: 804,
		velocity: 3.79388888888889,
		power: 612.005378915431,
		road: 7303.95736111111,
		acceleration: 0.0299074074074075
	},
	{
		id: 806,
		time: 805,
		velocity: 3.9325,
		power: 441.640144181268,
		road: 7307.85180555556,
		acceleration: -0.0164814814814815
	},
	{
		id: 807,
		time: 806,
		velocity: 3.93222222222222,
		power: 647.374530569859,
		road: 7311.75731481481,
		acceleration: 0.0386111111111114
	},
	{
		id: 808,
		time: 807,
		velocity: 3.90972222222222,
		power: 465.201507225487,
		road: 7315.67662037037,
		acceleration: -0.0110185185185188
	},
	{
		id: 809,
		time: 808,
		velocity: 3.89944444444444,
		power: 429.813199453698,
		road: 7319.58041666667,
		acceleration: -0.02
	},
	{
		id: 810,
		time: 809,
		velocity: 3.87222222222222,
		power: 496.90547147381,
		road: 7323.47347222222,
		acceleration: -0.00148148148148097
	},
	{
		id: 811,
		time: 810,
		velocity: 3.90527777777778,
		power: 474.734051574692,
		road: 7327.36212962963,
		acceleration: -0.00731481481481522
	},
	{
		id: 812,
		time: 811,
		velocity: 3.8775,
		power: 501.906492888688,
		road: 7331.24722222222,
		acceleration: 0.000185185185185066
	},
	{
		id: 813,
		time: 812,
		velocity: 3.87277777777778,
		power: 402.305276339183,
		road: 7335.11921296296,
		acceleration: -0.026388888888889
	},
	{
		id: 814,
		time: 813,
		velocity: 3.82611111111111,
		power: 492.561688348833,
		road: 7338.97736111111,
		acceleration: -0.00129629629629591
	},
	{
		id: 815,
		time: 814,
		velocity: 3.87361111111111,
		power: 448.534156786904,
		road: 7342.82833333333,
		acceleration: -0.0130555555555563
	},
	{
		id: 816,
		time: 815,
		velocity: 3.83361111111111,
		power: 676.71323835213,
		road: 7346.69699074074,
		acceleration: 0.0484259259259261
	},
	{
		id: 817,
		time: 816,
		velocity: 3.97138888888889,
		power: 641.896803226326,
		road: 7350.60828703704,
		acceleration: 0.0368518518518526
	},
	{
		id: 818,
		time: 817,
		velocity: 3.98416666666667,
		power: 875.247208583194,
		road: 7354.58574074074,
		acceleration: 0.0954629629629626
	},
	{
		id: 819,
		time: 818,
		velocity: 4.12,
		power: 1003.89070374779,
		road: 7358.67189814815,
		acceleration: 0.121944444444445
	},
	{
		id: 820,
		time: 819,
		velocity: 4.33722222222222,
		power: 746.48206718828,
		road: 7362.84462962963,
		acceleration: 0.0512037037037043
	},
	{
		id: 821,
		time: 820,
		velocity: 4.13777777777778,
		power: 673.854156988877,
		road: 7367.05847222222,
		acceleration: 0.0310185185185183
	},
	{
		id: 822,
		time: 821,
		velocity: 4.21305555555556,
		power: 555.839506079719,
		road: 7371.28828703704,
		acceleration: 0.000925925925925775
	},
	{
		id: 823,
		time: 822,
		velocity: 4.34,
		power: 643.366375169261,
		road: 7375.52967592592,
		acceleration: 0.0222222222222221
	},
	{
		id: 824,
		time: 823,
		velocity: 4.20444444444444,
		power: 563.907091494548,
		road: 7379.78319444444,
		acceleration: 0.00203703703703617
	},
	{
		id: 825,
		time: 824,
		velocity: 4.21916666666667,
		power: 436.200471576686,
		road: 7384.02314814815,
		acceleration: -0.0291666666666668
	},
	{
		id: 826,
		time: 825,
		velocity: 4.2525,
		power: 736.747806728743,
		road: 7388.27106481481,
		acceleration: 0.0450925925925931
	},
	{
		id: 827,
		time: 826,
		velocity: 4.33972222222222,
		power: 645.262897272338,
		road: 7392.55203703704,
		acceleration: 0.0210185185185185
	},
	{
		id: 828,
		time: 827,
		velocity: 4.28222222222222,
		power: 421.26971819519,
		road: 7396.82657407407,
		acceleration: -0.0338888888888897
	},
	{
		id: 829,
		time: 828,
		velocity: 4.15083333333333,
		power: 663.772491735546,
		road: 7401.09717592592,
		acceleration: 0.0260185185185193
	},
	{
		id: 830,
		time: 829,
		velocity: 4.41777777777778,
		power: 738.718479498076,
		road: 7405.40222222222,
		acceleration: 0.0428703703703697
	},
	{
		id: 831,
		time: 830,
		velocity: 4.41083333333333,
		power: 767.70296606239,
		road: 7409.75259259259,
		acceleration: 0.0477777777777781
	},
	{
		id: 832,
		time: 831,
		velocity: 4.29416666666667,
		power: 785.850299197895,
		road: 7414.15175925926,
		acceleration: 0.0498148148148152
	},
	{
		id: 833,
		time: 832,
		velocity: 4.56722222222222,
		power: 995.903286760187,
		road: 7418.62375,
		acceleration: 0.0958333333333323
	},
	{
		id: 834,
		time: 833,
		velocity: 4.69833333333333,
		power: 1168.67773547081,
		road: 7423.20824074074,
		acceleration: 0.129166666666667
	},
	{
		id: 835,
		time: 834,
		velocity: 4.68166666666667,
		power: 666.652435326053,
		road: 7427.86291666667,
		acceleration: 0.0112037037037043
	},
	{
		id: 836,
		time: 835,
		velocity: 4.60083333333333,
		power: 550.524843542295,
		road: 7432.51569444444,
		acceleration: -0.0150000000000006
	},
	{
		id: 837,
		time: 836,
		velocity: 4.65333333333333,
		power: 360.261263722202,
		road: 7437.13236111111,
		acceleration: -0.0572222222222214
	},
	{
		id: 838,
		time: 837,
		velocity: 4.51,
		power: 315.601931077906,
		road: 7441.68736111111,
		acceleration: -0.0661111111111117
	},
	{
		id: 839,
		time: 838,
		velocity: 4.4025,
		power: 325.939423410089,
		road: 7446.17814814815,
		acceleration: -0.0623148148148145
	},
	{
		id: 840,
		time: 839,
		velocity: 4.46638888888889,
		power: 499.307805372835,
		road: 7450.62759259259,
		acceleration: -0.0203703703703706
	},
	{
		id: 841,
		time: 840,
		velocity: 4.44888888888889,
		power: 653.918070553877,
		road: 7455.075,
		acceleration: 0.0162962962962965
	},
	{
		id: 842,
		time: 841,
		velocity: 4.45138888888889,
		power: 607.882581409658,
		road: 7459.53305555555,
		acceleration: 0.00499999999999901
	},
	{
		id: 843,
		time: 842,
		velocity: 4.48138888888889,
		power: 444.249246521936,
		road: 7463.97699074074,
		acceleration: -0.03324074074074
	},
	{
		id: 844,
		time: 843,
		velocity: 4.34916666666667,
		power: 410.736793364669,
		road: 7468.38421296296,
		acceleration: -0.0401851851851855
	},
	{
		id: 845,
		time: 844,
		velocity: 4.33083333333333,
		power: 329.241442464918,
		road: 7472.74208333333,
		acceleration: -0.0585185185185182
	},
	{
		id: 846,
		time: 845,
		velocity: 4.30583333333333,
		power: 587.560009461165,
		road: 7477.07314814815,
		acceleration: 0.00490740740740758
	},
	{
		id: 847,
		time: 846,
		velocity: 4.36388888888889,
		power: 489.13327230212,
		road: 7481.39726851852,
		acceleration: -0.018796296296296
	},
	{
		id: 848,
		time: 847,
		velocity: 4.27444444444444,
		power: 415.97324368014,
		road: 7485.69407407407,
		acceleration: -0.0358333333333336
	},
	{
		id: 849,
		time: 848,
		velocity: 4.19833333333333,
		power: 400.779525401083,
		road: 7489.9537037037,
		acceleration: -0.0385185185185186
	},
	{
		id: 850,
		time: 849,
		velocity: 4.24833333333333,
		power: 399.798205525187,
		road: 7494.17523148148,
		acceleration: -0.0376851851851852
	},
	{
		id: 851,
		time: 850,
		velocity: 4.16138888888889,
		power: 685.635154025209,
		road: 7498.39476851852,
		acceleration: 0.0337037037037033
	},
	{
		id: 852,
		time: 851,
		velocity: 4.29944444444444,
		power: 481.156556462926,
		road: 7502.62236111111,
		acceleration: -0.0175925925925924
	},
	{
		id: 853,
		time: 852,
		velocity: 4.19555555555556,
		power: 692.757769297791,
		road: 7506.85851851852,
		acceleration: 0.0347222222222223
	},
	{
		id: 854,
		time: 853,
		velocity: 4.26555555555556,
		power: 416.032238823923,
		road: 7511.095,
		acceleration: -0.0340740740740744
	},
	{
		id: 855,
		time: 854,
		velocity: 4.19722222222222,
		power: 700.879943524638,
		road: 7515.33277777778,
		acceleration: 0.0366666666666662
	},
	{
		id: 856,
		time: 855,
		velocity: 4.30555555555556,
		power: 577.428986684509,
		road: 7519.59148148148,
		acceleration: 0.00518518518518629
	},
	{
		id: 857,
		time: 856,
		velocity: 4.28111111111111,
		power: 377.233818417062,
		road: 7523.83087962963,
		acceleration: -0.0437962962962963
	},
	{
		id: 858,
		time: 857,
		velocity: 4.06583333333333,
		power: -541.237208987945,
		road: 7527.91013888889,
		acceleration: -0.276481481481482
	},
	{
		id: 859,
		time: 858,
		velocity: 3.47611111111111,
		power: -1152.26460550751,
		road: 7531.62,
		acceleration: -0.462314814814815
	},
	{
		id: 860,
		time: 859,
		velocity: 2.89416666666667,
		power: -1081.29139103348,
		road: 7534.85601851852,
		acceleration: -0.48537037037037
	},
	{
		id: 861,
		time: 860,
		velocity: 2.60972222222222,
		power: -688.343814707114,
		road: 7537.6537962963,
		acceleration: -0.391111111111111
	},
	{
		id: 862,
		time: 861,
		velocity: 2.30277777777778,
		power: -157.437666531354,
		road: 7540.15740740741,
		acceleration: -0.197222222222222
	},
	{
		id: 863,
		time: 862,
		velocity: 2.3025,
		power: 25.2359207619484,
		road: 7542.50282407407,
		acceleration: -0.119166666666667
	},
	{
		id: 864,
		time: 863,
		velocity: 2.25222222222222,
		power: 311.887940281608,
		road: 7544.79518518518,
		acceleration: 0.0130555555555558
	},
	{
		id: 865,
		time: 864,
		velocity: 2.34194444444444,
		power: 62.3667891171563,
		road: 7547.04356481481,
		acceleration: -0.101018518518519
	},
	{
		id: 866,
		time: 865,
		velocity: 1.99944444444444,
		power: 43.3094931184466,
		road: 7549.18708333333,
		acceleration: -0.108703703703704
	},
	{
		id: 867,
		time: 866,
		velocity: 1.92611111111111,
		power: -27.2954769115903,
		road: 7551.20425925926,
		acceleration: -0.143981481481482
	},
	{
		id: 868,
		time: 867,
		velocity: 1.91,
		power: 196.137007077313,
		road: 7553.13814814815,
		acceleration: -0.0225925925925925
	},
	{
		id: 869,
		time: 868,
		velocity: 1.93166666666667,
		power: 193.583322876949,
		road: 7555.04939814815,
		acceleration: -0.0226851851851853
	},
	{
		id: 870,
		time: 869,
		velocity: 1.85805555555556,
		power: 84.2778405944362,
		road: 7556.90851851852,
		acceleration: -0.081574074074074
	},
	{
		id: 871,
		time: 870,
		velocity: 1.66527777777778,
		power: 70.183849374948,
		road: 7558.68310185185,
		acceleration: -0.0875000000000001
	},
	{
		id: 872,
		time: 871,
		velocity: 1.66916666666667,
		power: 204.680481567095,
		road: 7560.41180555555,
		acceleration: -0.00425925925925918
	},
	{
		id: 873,
		time: 872,
		velocity: 1.84527777777778,
		power: 482.755025698079,
		road: 7562.21490740741,
		acceleration: 0.153055555555556
	},
	{
		id: 874,
		time: 873,
		velocity: 2.12444444444444,
		power: 503.583962998921,
		road: 7564.16583333333,
		acceleration: 0.142592592592593
	},
	{
		id: 875,
		time: 874,
		velocity: 2.09694444444444,
		power: 404.112053198702,
		road: 7566.22652777778,
		acceleration: 0.0769444444444445
	},
	{
		id: 876,
		time: 875,
		velocity: 2.07611111111111,
		power: 259.970630610019,
		road: 7568.32601851852,
		acceleration: 0.000648148148148398
	},
	{
		id: 877,
		time: 876,
		velocity: 2.12638888888889,
		power: 182.863432738014,
		road: 7570.40722222222,
		acceleration: -0.0372222222222227
	},
	{
		id: 878,
		time: 877,
		velocity: 1.98527777777778,
		power: 248.914139296372,
		road: 7572.46856481481,
		acceleration: -0.00249999999999995
	},
	{
		id: 879,
		time: 878,
		velocity: 2.06861111111111,
		power: 218.105420963311,
		road: 7574.51981481481,
		acceleration: -0.0176851851851847
	},
	{
		id: 880,
		time: 879,
		velocity: 2.07333333333333,
		power: 355.089971809387,
		road: 7576.58782407407,
		acceleration: 0.0512037037037034
	},
	{
		id: 881,
		time: 880,
		velocity: 2.13888888888889,
		power: 448.042885709489,
		road: 7578.72685185185,
		acceleration: 0.0908333333333333
	},
	{
		id: 882,
		time: 881,
		velocity: 2.34111111111111,
		power: 597.158268012675,
		road: 7580.98550925926,
		acceleration: 0.148425925925926
	},
	{
		id: 883,
		time: 882,
		velocity: 2.51861111111111,
		power: 590.917182354585,
		road: 7583.38296296296,
		acceleration: 0.129166666666666
	},
	{
		id: 884,
		time: 883,
		velocity: 2.52638888888889,
		power: 432.703270580998,
		road: 7585.8712037037,
		acceleration: 0.0524074074074075
	},
	{
		id: 885,
		time: 884,
		velocity: 2.49833333333333,
		power: 504.072702704298,
		road: 7588.42416666666,
		acceleration: 0.0770370370370363
	},
	{
		id: 886,
		time: 885,
		velocity: 2.74972222222222,
		power: 576.376328176922,
		road: 7591.065,
		acceleration: 0.0987037037037037
	},
	{
		id: 887,
		time: 886,
		velocity: 2.8225,
		power: 758.342721602298,
		road: 7593.83365740741,
		acceleration: 0.156944444444445
	},
	{
		id: 888,
		time: 887,
		velocity: 2.96916666666667,
		power: 396.089679744585,
		road: 7596.68791666666,
		acceleration: 0.0142592592592599
	},
	{
		id: 889,
		time: 888,
		velocity: 2.7925,
		power: 131.628960904195,
		road: 7599.50796296296,
		acceleration: -0.0826851851851855
	},
	{
		id: 890,
		time: 889,
		velocity: 2.57444444444444,
		power: -263.664981593498,
		road: 7602.16875,
		acceleration: -0.235833333333333
	},
	{
		id: 891,
		time: 890,
		velocity: 2.26166666666667,
		power: -182.423124878973,
		road: 7604.60680555555,
		acceleration: -0.209629629629629
	},
	{
		id: 892,
		time: 891,
		velocity: 2.16361111111111,
		power: -97.0750424689211,
		road: 7606.85212962963,
		acceleration: -0.175833333333334
	},
	{
		id: 893,
		time: 892,
		velocity: 2.04694444444444,
		power: 219.244340616797,
		road: 7608.99837962963,
		acceleration: -0.0223148148148145
	},
	{
		id: 894,
		time: 893,
		velocity: 2.19472222222222,
		power: 307.756422637446,
		road: 7611.14407407407,
		acceleration: 0.0212037037037032
	},
	{
		id: 895,
		time: 894,
		velocity: 2.22722222222222,
		power: 411.933109218523,
		road: 7613.33444444444,
		acceleration: 0.0681481481481487
	},
	{
		id: 896,
		time: 895,
		velocity: 2.25138888888889,
		power: 572.059636805706,
		road: 7615.62532407407,
		acceleration: 0.13287037037037
	},
	{
		id: 897,
		time: 896,
		velocity: 2.59333333333333,
		power: 379.098091818879,
		road: 7618.00143518518,
		acceleration: 0.0375925925925928
	},
	{
		id: 898,
		time: 897,
		velocity: 2.34,
		power: 318.484853533198,
		road: 7620.40097222222,
		acceleration: 0.00925925925925908
	},
	{
		id: 899,
		time: 898,
		velocity: 2.27916666666667,
		power: 166.818269980459,
		road: 7622.77685185185,
		acceleration: -0.0565740740740743
	},
	{
		id: 900,
		time: 899,
		velocity: 2.42361111111111,
		power: 375.978738455029,
		road: 7625.14291666666,
		acceleration: 0.0369444444444444
	},
	{
		id: 901,
		time: 900,
		velocity: 2.45083333333333,
		power: 441.437536588223,
		road: 7627.55842592592,
		acceleration: 0.0619444444444444
	},
	{
		id: 902,
		time: 901,
		velocity: 2.465,
		power: 87.5135514407103,
		road: 7629.95879629629,
		acceleration: -0.0922222222222215
	},
	{
		id: 903,
		time: 902,
		velocity: 2.14694444444444,
		power: 116.675720887484,
		road: 7632.27439814815,
		acceleration: -0.0773148148148155
	},
	{
		id: 904,
		time: 903,
		velocity: 2.21888888888889,
		power: 146.735789726048,
		road: 7634.52064814815,
		acceleration: -0.0613888888888887
	},
	{
		id: 905,
		time: 904,
		velocity: 2.28083333333333,
		power: 384.918922045567,
		road: 7636.76162037037,
		acceleration: 0.0508333333333333
	},
	{
		id: 906,
		time: 905,
		velocity: 2.29944444444444,
		power: 385.565597152736,
		road: 7639.05157407407,
		acceleration: 0.0471296296296297
	},
	{
		id: 907,
		time: 906,
		velocity: 2.36027777777778,
		power: 358.898791582772,
		road: 7641.38106481481,
		acceleration: 0.0319444444444441
	},
	{
		id: 908,
		time: 907,
		velocity: 2.37666666666667,
		power: 446.462231008013,
		road: 7643.76013888889,
		acceleration: 0.0672222222222221
	},
	{
		id: 909,
		time: 908,
		velocity: 2.50111111111111,
		power: 400.912538145219,
		road: 7646.19425925926,
		acceleration: 0.042870370370371
	},
	{
		id: 910,
		time: 909,
		velocity: 2.48888888888889,
		power: 528.938681104608,
		road: 7648.69578703703,
		acceleration: 0.0919444444444442
	},
	{
		id: 911,
		time: 910,
		velocity: 2.6525,
		power: 570.025926442825,
		road: 7651.29333333333,
		acceleration: 0.100092592592593
	},
	{
		id: 912,
		time: 911,
		velocity: 2.80138888888889,
		power: 524.768440684977,
		road: 7653.97819444444,
		acceleration: 0.0745370370370368
	},
	{
		id: 913,
		time: 912,
		velocity: 2.7125,
		power: 324.394008285288,
		road: 7656.6974074074,
		acceleration: -0.00583333333333336
	},
	{
		id: 914,
		time: 913,
		velocity: 2.635,
		power: 286.372703180115,
		road: 7659.4037037037,
		acceleration: -0.02
	},
	{
		id: 915,
		time: 914,
		velocity: 2.74138888888889,
		power: 120.754431008462,
		road: 7662.05828703703,
		acceleration: -0.0834259259259258
	},
	{
		id: 916,
		time: 915,
		velocity: 2.46222222222222,
		power: 21.42463478051,
		road: 7664.61004629629,
		acceleration: -0.122222222222222
	},
	{
		id: 917,
		time: 916,
		velocity: 2.26833333333333,
		power: -19.2102362979772,
		road: 7667.0311574074,
		acceleration: -0.139074074074074
	},
	{
		id: 918,
		time: 917,
		velocity: 2.32416666666667,
		power: 207.844862873343,
		road: 7669.36444444444,
		acceleration: -0.0365740740740743
	},
	{
		id: 919,
		time: 918,
		velocity: 2.3525,
		power: 354.470421308553,
		road: 7671.69439814814,
		acceleration: 0.0299074074074071
	},
	{
		id: 920,
		time: 919,
		velocity: 2.35805555555556,
		power: 278.659937579168,
		road: 7674.03675925926,
		acceleration: -0.0050925925925922
	},
	{
		id: 921,
		time: 920,
		velocity: 2.30888888888889,
		power: 127.137420647247,
		road: 7676.34046296296,
		acceleration: -0.0722222222222224
	},
	{
		id: 922,
		time: 921,
		velocity: 2.13583333333333,
		power: 186.991193321178,
		road: 7678.58680555555,
		acceleration: -0.0425
	},
	{
		id: 923,
		time: 922,
		velocity: 2.23055555555556,
		power: 230.701287495645,
		road: 7680.80171296296,
		acceleration: -0.0203703703703702
	},
	{
		id: 924,
		time: 923,
		velocity: 2.24777777777778,
		power: 293.992681436447,
		road: 7683.01148148148,
		acceleration: 0.0100925925925925
	},
	{
		id: 925,
		time: 924,
		velocity: 2.16611111111111,
		power: 246.894097708333,
		road: 7685.22013888889,
		acceleration: -0.0123148148148147
	},
	{
		id: 926,
		time: 925,
		velocity: 2.19361111111111,
		power: 252.995009618157,
		road: 7687.41824074074,
		acceleration: -0.00879629629629619
	},
	{
		id: 927,
		time: 926,
		velocity: 2.22138888888889,
		power: 668.040857219139,
		road: 7689.70101851851,
		acceleration: 0.178148148148148
	},
	{
		id: 928,
		time: 927,
		velocity: 2.70055555555556,
		power: 902.341660624173,
		road: 7692.19787037037,
		acceleration: 0.25
	},
	{
		id: 929,
		time: 928,
		velocity: 2.94361111111111,
		power: 1082.05862612346,
		road: 7694.96027777777,
		acceleration: 0.281111111111111
	},
	{
		id: 930,
		time: 929,
		velocity: 3.06472222222222,
		power: 775.010567134852,
		road: 7697.93435185185,
		acceleration: 0.142222222222222
	},
	{
		id: 931,
		time: 930,
		velocity: 3.12722222222222,
		power: 324.003248759481,
		road: 7700.96949074074,
		acceleration: -0.0200925925925919
	},
	{
		id: 932,
		time: 931,
		velocity: 2.88333333333333,
		power: 123.32810550954,
		road: 7703.95018518518,
		acceleration: -0.0887962962962967
	},
	{
		id: 933,
		time: 932,
		velocity: 2.79833333333333,
		power: -37.0442800642882,
		road: 7706.8136574074,
		acceleration: -0.145648148148148
	},
	{
		id: 934,
		time: 933,
		velocity: 2.69027777777778,
		power: 127.25964491524,
		road: 7709.56287037037,
		acceleration: -0.0828703703703706
	},
	{
		id: 935,
		time: 934,
		velocity: 2.63472222222222,
		power: 371.190846802072,
		road: 7712.27694444444,
		acceleration: 0.0125925925925925
	},
	{
		id: 936,
		time: 935,
		velocity: 2.83611111111111,
		power: 368.337186077388,
		road: 7715.00273148148,
		acceleration: 0.0108333333333333
	},
	{
		id: 937,
		time: 936,
		velocity: 2.72277777777778,
		power: 408.034322419601,
		road: 7717.74648148148,
		acceleration: 0.0250925925925927
	},
	{
		id: 938,
		time: 937,
		velocity: 2.71,
		power: 592.177372057227,
		road: 7720.54824074074,
		acceleration: 0.0909259259259256
	},
	{
		id: 939,
		time: 938,
		velocity: 3.10888888888889,
		power: 1084.65105899292,
		road: 7723.52148148148,
		acceleration: 0.252037037037038
	},
	{
		id: 940,
		time: 939,
		velocity: 3.47888888888889,
		power: 1432.34508530848,
		road: 7726.78523148148,
		acceleration: 0.328981481481481
	},
	{
		id: 941,
		time: 940,
		velocity: 3.69694444444444,
		power: 1227.85128277485,
		road: 7730.32875,
		acceleration: 0.230555555555556
	},
	{
		id: 942,
		time: 941,
		velocity: 3.80055555555556,
		power: 1040.84745836207,
		road: 7734.06657407407,
		acceleration: 0.158055555555555
	},
	{
		id: 943,
		time: 942,
		velocity: 3.95305555555556,
		power: 1106.71280627531,
		road: 7737.9649537037,
		acceleration: 0.163055555555556
	},
	{
		id: 944,
		time: 943,
		velocity: 4.18611111111111,
		power: 1404.1634949805,
		road: 7742.05712962963,
		acceleration: 0.224537037037037
	},
	{
		id: 945,
		time: 944,
		velocity: 4.47416666666667,
		power: 2120.44038625482,
		road: 7746.44675925926,
		acceleration: 0.37037037037037
	},
	{
		id: 946,
		time: 945,
		velocity: 5.06416666666667,
		power: 2193.28459423095,
		road: 7751.19467592592,
		acceleration: 0.346203703703704
	},
	{
		id: 947,
		time: 946,
		velocity: 5.22472222222222,
		power: 1704.23367801792,
		road: 7756.22324074074,
		acceleration: 0.215092592592592
	},
	{
		id: 948,
		time: 947,
		velocity: 5.11944444444444,
		power: 526.48070461028,
		road: 7761.3424074074,
		acceleration: -0.0338888888888889
	},
	{
		id: 949,
		time: 948,
		velocity: 4.9625,
		power: 507.991796981636,
		road: 7766.42625,
		acceleration: -0.0367592592592603
	},
	{
		id: 950,
		time: 949,
		velocity: 5.11444444444444,
		power: 689.35605360464,
		road: 7771.4924074074,
		acceleration: 0.00138888888888911
	},
	{
		id: 951,
		time: 950,
		velocity: 5.12361111111111,
		power: 1967.04064474182,
		road: 7776.68722222222,
		acceleration: 0.255925925925927
	},
	{
		id: 952,
		time: 951,
		velocity: 5.73027777777778,
		power: 1270.58270562363,
		road: 7782.06254629629,
		acceleration: 0.105092592592593
	},
	{
		id: 953,
		time: 952,
		velocity: 5.42972222222222,
		power: 1762.03115283226,
		road: 7787.58597222222,
		acceleration: 0.191111111111111
	},
	{
		id: 954,
		time: 953,
		velocity: 5.69694444444444,
		power: 774.178107518091,
		road: 7793.20486111111,
		acceleration: -0.00018518518518551
	},
	{
		id: 955,
		time: 954,
		velocity: 5.72972222222222,
		power: 1772.10268777953,
		road: 7798.91407407407,
		acceleration: 0.180833333333333
	},
	{
		id: 956,
		time: 955,
		velocity: 5.97222222222222,
		power: 2028.58357178895,
		road: 7804.82083333333,
		acceleration: 0.21425925925926
	},
	{
		id: 957,
		time: 956,
		velocity: 6.33972222222222,
		power: 1123.19681235778,
		road: 7810.85861111111,
		acceleration: 0.0477777777777781
	},
	{
		id: 958,
		time: 957,
		velocity: 5.87305555555556,
		power: -413.738990427179,
		road: 7816.81004629629,
		acceleration: -0.220462962962964
	},
	{
		id: 959,
		time: 958,
		velocity: 5.31083333333333,
		power: -1311.41530836671,
		road: 7822.45638888888,
		acceleration: -0.389722222222222
	},
	{
		id: 960,
		time: 959,
		velocity: 5.17055555555556,
		power: -214.378668183665,
		road: 7827.81504629629,
		acceleration: -0.185648148148147
	},
	{
		id: 961,
		time: 960,
		velocity: 5.31611111111111,
		power: 1246.7026397899,
		road: 7833.13259259259,
		acceleration: 0.103425925925925
	},
	{
		id: 962,
		time: 961,
		velocity: 5.62111111111111,
		power: 2324.15578975858,
		road: 7838.6511574074,
		acceleration: 0.298611111111112
	},
	{
		id: 963,
		time: 962,
		velocity: 6.06638888888889,
		power: 2803.38099227632,
		road: 7844.49791666666,
		acceleration: 0.357777777777777
	},
	{
		id: 964,
		time: 963,
		velocity: 6.38944444444444,
		power: 3480.13429572099,
		road: 7850.74199074074,
		acceleration: 0.436851851851852
	},
	{
		id: 965,
		time: 964,
		velocity: 6.93166666666667,
		power: 3678.23634374251,
		road: 7857.41791666666,
		acceleration: 0.426851851851852
	},
	{
		id: 966,
		time: 965,
		velocity: 7.34694444444444,
		power: 5013.68319089079,
		road: 7864.5961574074,
		acceleration: 0.577777777777778
	},
	{
		id: 967,
		time: 966,
		velocity: 8.12277777777778,
		power: 6157.20070912548,
		road: 7872.39712962962,
		acceleration: 0.667685185185186
	},
	{
		id: 968,
		time: 967,
		velocity: 8.93472222222222,
		power: 7748.97229043082,
		road: 7880.9249537037,
		acceleration: 0.786018518518517
	},
	{
		id: 969,
		time: 968,
		velocity: 9.705,
		power: 6541.1834748865,
		road: 7890.1312037037,
		acceleration: 0.570833333333333
	},
	{
		id: 970,
		time: 969,
		velocity: 9.83527777777778,
		power: 4149.67845143199,
		road: 7899.75925925926,
		acceleration: 0.272777777777778
	},
	{
		id: 971,
		time: 970,
		velocity: 9.75305555555556,
		power: 3055.28459368132,
		road: 7909.59574074074,
		acceleration: 0.144074074074075
	},
	{
		id: 972,
		time: 971,
		velocity: 10.1372222222222,
		power: 3940.80379585584,
		road: 7919.61856481481,
		acceleration: 0.22861111111111
	},
	{
		id: 973,
		time: 972,
		velocity: 10.5211111111111,
		power: 6137.98486894724,
		road: 7929.97277777777,
		acceleration: 0.434166666666668
	},
	{
		id: 974,
		time: 973,
		velocity: 11.0555555555556,
		power: 9578.09370359157,
		road: 7940.90611111111,
		acceleration: 0.724074074074075
	},
	{
		id: 975,
		time: 974,
		velocity: 12.3094444444444,
		power: 10219.0650564294,
		road: 7952.55921296296,
		acceleration: 0.715462962962961
	},
	{
		id: 976,
		time: 975,
		velocity: 12.6675,
		power: 8268.06100494572,
		road: 7964.8174537037,
		acceleration: 0.494814814814816
	},
	{
		id: 977,
		time: 976,
		velocity: 12.54,
		power: 2865.82538659183,
		road: 7977.33518518518,
		acceleration: 0.0241666666666642
	},
	{
		id: 978,
		time: 977,
		velocity: 12.3819444444444,
		power: 152.534462576992,
		road: 7989.76416666666,
		acceleration: -0.201666666666666
	},
	{
		id: 979,
		time: 978,
		velocity: 12.0625,
		power: 181.525667539836,
		road: 8001.99421296296,
		acceleration: -0.196203703703704
	},
	{
		id: 980,
		time: 979,
		velocity: 11.9513888888889,
		power: 554.124355814346,
		road: 8014.04560185185,
		acceleration: -0.161111111111111
	},
	{
		id: 981,
		time: 980,
		velocity: 11.8986111111111,
		power: 1941.2532927529,
		road: 8025.99759259259,
		acceleration: -0.0376851851851843
	},
	{
		id: 982,
		time: 981,
		velocity: 11.9494444444444,
		power: 3806.94294516931,
		road: 8037.99282407407,
		acceleration: 0.124166666666667
	},
	{
		id: 983,
		time: 982,
		velocity: 12.3238888888889,
		power: 3416.6608906967,
		road: 8050.09314814815,
		acceleration: 0.0860185185185181
	},
	{
		id: 984,
		time: 983,
		velocity: 12.1566666666667,
		power: 1722.09115914625,
		road: 8062.20592592592,
		acceleration: -0.0611111111111118
	},
	{
		id: 985,
		time: 984,
		velocity: 11.7661111111111,
		power: -942.573338149247,
		road: 8074.14287037037,
		acceleration: -0.290555555555555
	},
	{
		id: 986,
		time: 985,
		velocity: 11.4522222222222,
		power: -1232.3220266939,
		road: 8085.77712962963,
		acceleration: -0.314814814814817
	},
	{
		id: 987,
		time: 986,
		velocity: 11.2122222222222,
		power: -1691.18038927484,
		road: 8097.07578703703,
		acceleration: -0.356388888888889
	},
	{
		id: 988,
		time: 987,
		velocity: 10.6969444444444,
		power: -2361.15640879798,
		road: 8107.98546296296,
		acceleration: -0.421574074074073
	},
	{
		id: 989,
		time: 988,
		velocity: 10.1875,
		power: -2137.82302611119,
		road: 8118.48273148148,
		acceleration: -0.40324074074074
	},
	{
		id: 990,
		time: 989,
		velocity: 10.0025,
		power: -22.7841680164045,
		road: 8128.68416666666,
		acceleration: -0.188425925925927
	},
	{
		id: 991,
		time: 990,
		velocity: 10.1316666666667,
		power: 1906.62724211279,
		road: 8138.79777777777,
		acceleration: 0.012777777777778
	},
	{
		id: 992,
		time: 991,
		velocity: 10.2258333333333,
		power: 3382.03117904941,
		road: 8148.9987037037,
		acceleration: 0.161851851851853
	},
	{
		id: 993,
		time: 992,
		velocity: 10.4880555555556,
		power: 3677.40672292357,
		road: 8159.3725,
		acceleration: 0.183888888888887
	},
	{
		id: 994,
		time: 993,
		velocity: 10.6833333333333,
		power: 4483.59210128526,
		road: 8169.9649537037,
		acceleration: 0.253425925925926
	},
	{
		id: 995,
		time: 994,
		velocity: 10.9861111111111,
		power: 4917.54649876137,
		road: 8180.82467592592,
		acceleration: 0.281111111111111
	},
	{
		id: 996,
		time: 995,
		velocity: 11.3313888888889,
		power: 7083.41341484815,
		road: 8192.05638888888,
		acceleration: 0.462870370370371
	},
	{
		id: 997,
		time: 996,
		velocity: 12.0719444444444,
		power: 10066.0771634747,
		road: 8203.86342592592,
		acceleration: 0.687777777777777
	},
	{
		id: 998,
		time: 997,
		velocity: 13.0494444444444,
		power: 10816.2657752851,
		road: 8216.36013888888,
		acceleration: 0.691574074074076
	},
	{
		id: 999,
		time: 998,
		velocity: 13.4061111111111,
		power: 10182.2755553232,
		road: 8229.49629629629,
		acceleration: 0.587314814814812
	},
	{
		id: 1000,
		time: 999,
		velocity: 13.8338888888889,
		power: 7241.77784173225,
		road: 8243.08930555555,
		acceleration: 0.326388888888889
	},
	{
		id: 1001,
		time: 1000,
		velocity: 14.0286111111111,
		power: 7794.64717839425,
		road: 8257.02004629629,
		acceleration: 0.349074074074075
	},
	{
		id: 1002,
		time: 1001,
		velocity: 14.4533333333333,
		power: 6332.13424820518,
		road: 8271.23777777777,
		acceleration: 0.224907407407407
	},
	{
		id: 1003,
		time: 1002,
		velocity: 14.5086111111111,
		power: 5410.22718174967,
		road: 8285.64236111111,
		acceleration: 0.148796296296297
	},
	{
		id: 1004,
		time: 1003,
		velocity: 14.475,
		power: 1625.11556524764,
		road: 8300.05805555555,
		acceleration: -0.126574074074075
	},
	{
		id: 1005,
		time: 1004,
		velocity: 14.0736111111111,
		power: 1031.04360460718,
		road: 8314.32717592592,
		acceleration: -0.166574074074074
	},
	{
		id: 1006,
		time: 1005,
		velocity: 14.0088888888889,
		power: -1262.96828447031,
		road: 8328.34671296296,
		acceleration: -0.332592592592594
	},
	{
		id: 1007,
		time: 1006,
		velocity: 13.4772222222222,
		power: -2425.60168566479,
		road: 8341.99064814814,
		acceleration: -0.418611111111108
	},
	{
		id: 1008,
		time: 1007,
		velocity: 12.8177777777778,
		power: -3671.98620567666,
		road: 8355.16662037036,
		acceleration: -0.517314814814815
	},
	{
		id: 1009,
		time: 1008,
		velocity: 12.4569444444444,
		power: -1793.72507699317,
		road: 8367.90069444444,
		acceleration: -0.366481481481481
	},
	{
		id: 1010,
		time: 1009,
		velocity: 12.3777777777778,
		power: 1324.45293523456,
		road: 8380.39930555555,
		acceleration: -0.104444444444447
	},
	{
		id: 1011,
		time: 1010,
		velocity: 12.5044444444444,
		power: -104.827976947551,
		road: 8392.73462962962,
		acceleration: -0.222129629629631
	},
	{
		id: 1012,
		time: 1011,
		velocity: 11.7905555555556,
		power: 641.187571678649,
		road: 8404.88124999999,
		acceleration: -0.155277777777776
	},
	{
		id: 1013,
		time: 1012,
		velocity: 11.9119444444444,
		power: -835.752333429718,
		road: 8416.80967592592,
		acceleration: -0.281111111111111
	},
	{
		id: 1014,
		time: 1013,
		velocity: 11.6611111111111,
		power: 1436.78906955015,
		road: 8428.55902777777,
		acceleration: -0.0770370370370372
	},
	{
		id: 1015,
		time: 1014,
		velocity: 11.5594444444444,
		power: 598.628792992522,
		road: 8440.1949537037,
		acceleration: -0.149814814814816
	},
	{
		id: 1016,
		time: 1015,
		velocity: 11.4625,
		power: 1065.58217681809,
		road: 8451.70347222222,
		acceleration: -0.104999999999999
	},
	{
		id: 1017,
		time: 1016,
		velocity: 11.3461111111111,
		power: -225.396472091588,
		road: 8463.04907407407,
		acceleration: -0.220833333333333
	},
	{
		id: 1018,
		time: 1017,
		velocity: 10.8969444444444,
		power: 190.406119485547,
		road: 8474.19449074073,
		acceleration: -0.17953703703704
	},
	{
		id: 1019,
		time: 1018,
		velocity: 10.9238888888889,
		power: -2956.75138619586,
		road: 8485.01004629629,
		acceleration: -0.480185185185183
	},
	{
		id: 1020,
		time: 1019,
		velocity: 9.90555555555556,
		power: -2727.33662474511,
		road: 8495.35328703703,
		acceleration: -0.464444444444444
	},
	{
		id: 1021,
		time: 1020,
		velocity: 9.50361111111111,
		power: -4095.08563791178,
		road: 8505.15421296295,
		acceleration: -0.620185185185186
	},
	{
		id: 1022,
		time: 1021,
		velocity: 9.06333333333333,
		power: -2143.96328213734,
		road: 8514.43578703703,
		acceleration: -0.418518518518518
	},
	{
		id: 1023,
		time: 1022,
		velocity: 8.65,
		power: -1287.33144926634,
		road: 8523.3461574074,
		acceleration: -0.323888888888888
	},
	{
		id: 1024,
		time: 1023,
		velocity: 8.53194444444445,
		power: -420.241125625585,
		road: 8531.98430555554,
		acceleration: -0.220555555555556
	},
	{
		id: 1025,
		time: 1024,
		velocity: 8.40166666666667,
		power: 6057.10775763147,
		road: 8540.78791666666,
		acceleration: 0.551481481481481
	},
	{
		id: 1026,
		time: 1025,
		velocity: 10.3044444444444,
		power: 12003.4841221778,
		road: 8550.43064814814,
		acceleration: 1.12675925925926
	},
	{
		id: 1027,
		time: 1026,
		velocity: 11.9122222222222,
		power: 18989.4242377774,
		road: 8561.44305555554,
		acceleration: 1.61259259259259
	},
	{
		id: 1028,
		time: 1027,
		velocity: 13.2394444444444,
		power: 12811.7051438208,
		road: 8573.70328703703,
		acceleration: 0.883055555555556
	},
	{
		id: 1029,
		time: 1028,
		velocity: 12.9536111111111,
		power: 6039.62434474531,
		road: 8586.54134259258,
		acceleration: 0.272592592592591
	},
	{
		id: 1030,
		time: 1029,
		velocity: 12.73,
		power: 2660.01398430051,
		road: 8599.51199074073,
		acceleration: -0.0074074074074062
	},
	{
		id: 1031,
		time: 1030,
		velocity: 13.2172222222222,
		power: 7080.29907639255,
		road: 8612.64888888888,
		acceleration: 0.339907407407406
	},
	{
		id: 1032,
		time: 1031,
		velocity: 13.9733333333333,
		power: 12911.4356975612,
		road: 8626.33328703703,
		acceleration: 0.755092592592593
	},
	{
		id: 1033,
		time: 1032,
		velocity: 14.9952777777778,
		power: 13573.7555081295,
		road: 8640.76509259258,
		acceleration: 0.739722222222223
	},
	{
		id: 1034,
		time: 1033,
		velocity: 15.4363888888889,
		power: 15109.8495890958,
		road: 8655.95828703703,
		acceleration: 0.783055555555553
	},
	{
		id: 1035,
		time: 1034,
		velocity: 16.3225,
		power: 14899.4498749611,
		road: 8671.89657407406,
		acceleration: 0.707129629629629
	},
	{
		id: 1036,
		time: 1035,
		velocity: 17.1166666666667,
		power: 19994.4358587982,
		road: 8688.66856481481,
		acceleration: 0.960277777777783
	},
	{
		id: 1037,
		time: 1036,
		velocity: 18.3172222222222,
		power: 19226.0001794848,
		road: 8706.33749999999,
		acceleration: 0.833611111111107
	},
	{
		id: 1038,
		time: 1037,
		velocity: 18.8233333333333,
		power: 16508.4190107116,
		road: 8724.73287037036,
		acceleration: 0.619259259259263
	},
	{
		id: 1039,
		time: 1038,
		velocity: 18.9744444444444,
		power: 8352.6333932338,
		road: 8743.50717592592,
		acceleration: 0.138611111111107
	},
	{
		id: 1040,
		time: 1039,
		velocity: 18.7330555555556,
		power: 10162.3553291372,
		road: 8762.46569444443,
		acceleration: 0.229814814814812
	},
	{
		id: 1041,
		time: 1040,
		velocity: 19.5127777777778,
		power: 11897.4226539728,
		road: 8781.69425925925,
		acceleration: 0.310277777777781
	},
	{
		id: 1042,
		time: 1041,
		velocity: 19.9052777777778,
		power: 14566.3206629869,
		road: 8801.29393518517,
		acceleration: 0.431944444444444
	},
	{
		id: 1043,
		time: 1042,
		velocity: 20.0288888888889,
		power: 7842.96180354565,
		road: 8821.1411574074,
		acceleration: 0.0631481481481515
	},
	{
		id: 1044,
		time: 1043,
		velocity: 19.7022222222222,
		power: 4395.46217327487,
		road: 8840.96134259258,
		acceleration: -0.117222222222225
	},
	{
		id: 1045,
		time: 1044,
		velocity: 19.5536111111111,
		power: 1330.14948458889,
		road: 8860.5861111111,
		acceleration: -0.273611111111109
	},
	{
		id: 1046,
		time: 1045,
		velocity: 19.2080555555556,
		power: 1932.6933787862,
		road: 8879.95675925925,
		acceleration: -0.23462962962963
	},
	{
		id: 1047,
		time: 1046,
		velocity: 18.9983333333333,
		power: 592.092949494857,
		road: 8899.05976851851,
		acceleration: -0.300648148148145
	},
	{
		id: 1048,
		time: 1047,
		velocity: 18.6516666666667,
		power: 2208.17650575362,
		road: 8917.9098148148,
		acceleration: -0.205277777777781
	},
	{
		id: 1049,
		time: 1048,
		velocity: 18.5922222222222,
		power: 1939.14382666075,
		road: 8936.54995370369,
		acceleration: -0.214537037037037
	},
	{
		id: 1050,
		time: 1049,
		velocity: 18.3547222222222,
		power: 1463.96894190433,
		road: 8954.96509259258,
		acceleration: -0.235462962962966
	},
	{
		id: 1051,
		time: 1050,
		velocity: 17.9452777777778,
		power: 2054.77433990733,
		road: 8973.16444444443,
		acceleration: -0.196111111111112
	},
	{
		id: 1052,
		time: 1051,
		velocity: 18.0038888888889,
		power: 6189.0396339689,
		road: 8991.28787037036,
		acceleration: 0.0442592592592632
	},
	{
		id: 1053,
		time: 1052,
		velocity: 18.4875,
		power: 10383.2320467931,
		road: 9009.57212962962,
		acceleration: 0.277407407407406
	},
	{
		id: 1054,
		time: 1053,
		velocity: 18.7775,
		power: 14351.5415685318,
		road: 9028.2348148148,
		acceleration: 0.479444444444447
	},
	{
		id: 1055,
		time: 1054,
		velocity: 19.4422222222222,
		power: 14336.6118550767,
		road: 9047.36175925925,
		acceleration: 0.449074074074073
	},
	{
		id: 1056,
		time: 1055,
		velocity: 19.8347222222222,
		power: 16671.8141483275,
		road: 9066.98449074073,
		acceleration: 0.542499999999997
	},
	{
		id: 1057,
		time: 1056,
		velocity: 20.405,
		power: 13602.7066928291,
		road: 9087.05495370369,
		acceleration: 0.352962962962962
	},
	{
		id: 1058,
		time: 1057,
		velocity: 20.5011111111111,
		power: 11784.0654958377,
		road: 9107.42319444443,
		acceleration: 0.242592592592597
	},
	{
		id: 1059,
		time: 1058,
		velocity: 20.5625,
		power: 7267.73598102899,
		road: 9127.91578703703,
		acceleration: 0.00611111111111029
	},
	{
		id: 1060,
		time: 1059,
		velocity: 20.4233333333333,
		power: 7010.96613781272,
		road: 9148.40796296295,
		acceleration: -0.00694444444444642
	},
	{
		id: 1061,
		time: 1060,
		velocity: 20.4802777777778,
		power: 7064.78933602879,
		road: 9168.89467592592,
		acceleration: -0.00398148148148181
	},
	{
		id: 1062,
		time: 1061,
		velocity: 20.5505555555556,
		power: 8118.39750262912,
		road: 9189.40374999999,
		acceleration: 0.0487037037037048
	},
	{
		id: 1063,
		time: 1062,
		velocity: 20.5694444444444,
		power: 4696.1819769875,
		road: 9209.87513888888,
		acceleration: -0.124074074074077
	},
	{
		id: 1064,
		time: 1063,
		velocity: 20.1080555555556,
		power: 1736.75920609254,
		road: 9230.14972222221,
		acceleration: -0.269537037037036
	},
	{
		id: 1065,
		time: 1064,
		velocity: 19.7419444444444,
		power: -1544.92733539651,
		road: 9250.07356481481,
		acceleration: -0.431944444444444
	},
	{
		id: 1066,
		time: 1065,
		velocity: 19.2736111111111,
		power: -907.642307202966,
		road: 9269.5862037037,
		acceleration: -0.390462962962964
	},
	{
		id: 1067,
		time: 1066,
		velocity: 18.9366666666667,
		power: -1285.69774566177,
		road: 9288.70185185184,
		acceleration: -0.403518518518517
	},
	{
		id: 1068,
		time: 1067,
		velocity: 18.5313888888889,
		power: -2295.65720480592,
		road: 9307.38949074073,
		acceleration: -0.452500000000001
	},
	{
		id: 1069,
		time: 1068,
		velocity: 17.9161111111111,
		power: -1426.02281266713,
		road: 9325.6524074074,
		acceleration: -0.396944444444443
	},
	{
		id: 1070,
		time: 1069,
		velocity: 17.7458333333333,
		power: -356.419704293651,
		road: 9343.55245370369,
		acceleration: -0.328796296296296
	},
	{
		id: 1071,
		time: 1070,
		velocity: 17.545,
		power: 2381.16762391292,
		road: 9361.20703703703,
		acceleration: -0.162129629629632
	},
	{
		id: 1072,
		time: 1071,
		velocity: 17.4297222222222,
		power: 1552.26032582117,
		road: 9378.67726851851,
		acceleration: -0.206574074074073
	},
	{
		id: 1073,
		time: 1072,
		velocity: 17.1261111111111,
		power: -1229.13963638195,
		road: 9395.8599074074,
		acceleration: -0.368611111111107
	},
	{
		id: 1074,
		time: 1073,
		velocity: 16.4391666666667,
		power: -3201.44636704309,
		road: 9412.61550925925,
		acceleration: -0.485462962962966
	},
	{
		id: 1075,
		time: 1074,
		velocity: 15.9733333333333,
		power: -4452.6089268312,
		road: 9428.84694444444,
		acceleration: -0.562870370370371
	},
	{
		id: 1076,
		time: 1075,
		velocity: 15.4375,
		power: -3156.05713737494,
		road: 9444.55856481481,
		acceleration: -0.476759259259257
	},
	{
		id: 1077,
		time: 1076,
		velocity: 15.0088888888889,
		power: -6191.98465632699,
		road: 9459.68939814814,
		acceleration: -0.684814814814814
	},
	{
		id: 1078,
		time: 1077,
		velocity: 13.9188888888889,
		power: -6862.96377264155,
		road: 9474.10629629629,
		acceleration: -0.743055555555557
	},
	{
		id: 1079,
		time: 1078,
		velocity: 13.2083333333333,
		power: -8385.50300250663,
		road: 9487.71296296295,
		acceleration: -0.877407407407409
	},
	{
		id: 1080,
		time: 1079,
		velocity: 12.3766666666667,
		power: -8333.03217666275,
		road: 9500.42819444444,
		acceleration: -0.905462962962963
	},
	{
		id: 1081,
		time: 1080,
		velocity: 11.2025,
		power: -10361.1429857955,
		road: 9512.12388888888,
		acceleration: -1.13361111111111
	},
	{
		id: 1082,
		time: 1081,
		velocity: 9.8075,
		power: -10905.7566731693,
		road: 9522.6124537037,
		acceleration: -1.28064814814815
	},
	{
		id: 1083,
		time: 1082,
		velocity: 8.53472222222222,
		power: -9958.4438178377,
		road: 9531.80425925925,
		acceleration: -1.31287037037037
	},
	{
		id: 1084,
		time: 1083,
		velocity: 7.26388888888889,
		power: -7069.91188582325,
		road: 9539.79291666666,
		acceleration: -1.09342592592593
	},
	{
		id: 1085,
		time: 1084,
		velocity: 6.52722222222222,
		power: -3551.70352090575,
		road: 9546.89402777777,
		acceleration: -0.681666666666668
	},
	{
		id: 1086,
		time: 1085,
		velocity: 6.48972222222222,
		power: -1543.64435967749,
		road: 9553.45476851851,
		acceleration: -0.399074074074074
	},
	{
		id: 1087,
		time: 1086,
		velocity: 6.06666666666667,
		power: -2345.11658271821,
		road: 9559.53912037036,
		acceleration: -0.553703703703704
	},
	{
		id: 1088,
		time: 1087,
		velocity: 4.86611111111111,
		power: -6273.46891305798,
		road: 9564.62657407407,
		acceleration: -1.44009259259259
	},
	{
		id: 1089,
		time: 1088,
		velocity: 2.16944444444444,
		power: -6014.43546747597,
		road: 9567.98287037036,
		acceleration: -2.02222222222222
	},
	{
		id: 1090,
		time: 1089,
		velocity: 0,
		power: -2171.71245563318,
		road: 9569.51703703703,
		acceleration: -1.62203703703704
	},
	{
		id: 1091,
		time: 1090,
		velocity: 0,
		power: -204.02533934373,
		road: 9569.8786111111,
		acceleration: -0.723148148148148
	},
	{
		id: 1092,
		time: 1091,
		velocity: 0,
		power: 0,
		road: 9569.8786111111,
		acceleration: 0
	},
	{
		id: 1093,
		time: 1092,
		velocity: 0,
		power: 0,
		road: 9569.8786111111,
		acceleration: 0
	},
	{
		id: 1094,
		time: 1093,
		velocity: 0,
		power: 40.0709995791925,
		road: 9569.99560185184,
		acceleration: 0.233981481481481
	},
	{
		id: 1095,
		time: 1094,
		velocity: 0.701944444444444,
		power: 996.287339694544,
		road: 9570.81050925925,
		acceleration: 1.16185185185185
	},
	{
		id: 1096,
		time: 1095,
		velocity: 3.48555555555556,
		power: 3440.35807968985,
		road: 9572.97787037036,
		acceleration: 1.54305555555556
	},
	{
		id: 1097,
		time: 1096,
		velocity: 4.62916666666667,
		power: 6417.4718131982,
		road: 9576.74583333333,
		acceleration: 1.65814814814815
	},
	{
		id: 1098,
		time: 1097,
		velocity: 5.67638888888889,
		power: 4040.04010232119,
		road: 9581.70129629629,
		acceleration: 0.716851851851853
	},
	{
		id: 1099,
		time: 1098,
		velocity: 5.63611111111111,
		power: 4563.91989221194,
		road: 9587.36624999999,
		acceleration: 0.702129629629629
	},
	{
		id: 1100,
		time: 1099,
		velocity: 6.73555555555556,
		power: 5572.4437568328,
		road: 9593.7649537037,
		acceleration: 0.765370370370371
	},
	{
		id: 1101,
		time: 1100,
		velocity: 7.9725,
		power: 7787.5039458854,
		road: 9601.03101851851,
		acceleration: 0.969351851851853
	},
	{
		id: 1102,
		time: 1101,
		velocity: 8.54416666666667,
		power: 4968.08779450462,
		road: 9609.02648148147,
		acceleration: 0.489444444444444
	},
	{
		id: 1103,
		time: 1102,
		velocity: 8.20388888888889,
		power: -2895.06910061281,
		road: 9616.99416666666,
		acceleration: -0.544999999999999
	},
	{
		id: 1104,
		time: 1103,
		velocity: 6.3375,
		power: -10402.2615476755,
		road: 9623.80999999999,
		acceleration: -1.7587037037037
	},
	{
		id: 1105,
		time: 1104,
		velocity: 3.26805555555556,
		power: -9822.71549738277,
		road: 9628.59564814814,
		acceleration: -2.30166666666667
	},
	{
		id: 1106,
		time: 1105,
		velocity: 1.29888888888889,
		power: -4258.44473197319,
		road: 9631.34958333333,
		acceleration: -1.76175925925926
	},
	{
		id: 1107,
		time: 1106,
		velocity: 1.05222222222222,
		power: -904.935807286077,
		road: 9632.83754629629,
		acceleration: -0.770185185185185
	},
	{
		id: 1108,
		time: 1107,
		velocity: 0.9575,
		power: -256.267363325656,
		road: 9633.72393518518,
		acceleration: -0.432962962962963
	},
	{
		id: 1109,
		time: 1108,
		velocity: 0,
		power: 514.581847909845,
		road: 9634.62958333333,
		acceleration: 0.471481481481482
	},
	{
		id: 1110,
		time: 1109,
		velocity: 2.46666666666667,
		power: 1823.00708938265,
		road: 9636.28652777777,
		acceleration: 1.03111111111111
	},
	{
		id: 1111,
		time: 1110,
		velocity: 4.05083333333333,
		power: 4420.96969276551,
		road: 9639.19398148147,
		acceleration: 1.46990740740741
	},
	{
		id: 1112,
		time: 1111,
		velocity: 4.40972222222222,
		power: 3268.25661658758,
		road: 9643.19796296296,
		acceleration: 0.723148148148149
	},
	{
		id: 1113,
		time: 1112,
		velocity: 4.63611111111111,
		power: 1178.31439544337,
		road: 9647.63412037036,
		acceleration: 0.141203703703703
	},
	{
		id: 1114,
		time: 1113,
		velocity: 4.47444444444444,
		power: 2065.70333228924,
		road: 9652.30388888888,
		acceleration: 0.326018518518518
	},
	{
		id: 1115,
		time: 1114,
		velocity: 5.38777777777778,
		power: 2478.88094188051,
		road: 9657.32564814814,
		acceleration: 0.377962962962963
	},
	{
		id: 1116,
		time: 1115,
		velocity: 5.77,
		power: 5472.08423587031,
		road: 9662.97337962962,
		acceleration: 0.873981481481482
	},
	{
		id: 1117,
		time: 1116,
		velocity: 7.09638888888889,
		power: 7224.52923927413,
		road: 9669.55893518518,
		acceleration: 1.00166666666667
	},
	{
		id: 1118,
		time: 1117,
		velocity: 8.39277777777778,
		power: 10995.3153085485,
		road: 9677.3099537037,
		acceleration: 1.32925925925926
	},
	{
		id: 1119,
		time: 1118,
		velocity: 9.75777777777778,
		power: 11067.321398519,
		road: 9686.28648148147,
		acceleration: 1.12175925925926
	},
	{
		id: 1120,
		time: 1119,
		velocity: 10.4616666666667,
		power: 11177.917300794,
		road: 9696.31657407407,
		acceleration: 0.98537037037037
	},
	{
		id: 1121,
		time: 1120,
		velocity: 11.3488888888889,
		power: 7952.41344188291,
		road: 9707.12847222222,
		acceleration: 0.578240740740741
	},
	{
		id: 1122,
		time: 1121,
		velocity: 11.4925,
		power: 4622.41050810555,
		road: 9718.34638888888,
		acceleration: 0.233796296296298
	},
	{
		id: 1123,
		time: 1122,
		velocity: 11.1630555555556,
		power: 800.001539544861,
		road: 9729.61888888888,
		acceleration: -0.124629629629631
	},
	{
		id: 1124,
		time: 1123,
		velocity: 10.975,
		power: 2422.97752145326,
		road: 9740.84305555555,
		acceleration: 0.0279629629629632
	},
	{
		id: 1125,
		time: 1124,
		velocity: 11.5763888888889,
		power: 6477.12799925059,
		road: 9752.27763888888,
		acceleration: 0.392870370370371
	},
	{
		id: 1126,
		time: 1125,
		velocity: 12.3416666666667,
		power: 10756.0924776126,
		road: 9764.27430555555,
		acceleration: 0.731296296296296
	},
	{
		id: 1127,
		time: 1126,
		velocity: 13.1688888888889,
		power: 12584.9202849067,
		road: 9777.04328703703,
		acceleration: 0.813333333333334
	},
	{
		id: 1128,
		time: 1127,
		velocity: 14.0163888888889,
		power: 12904.5920982613,
		road: 9790.60180555555,
		acceleration: 0.765740740740739
	},
	{
		id: 1129,
		time: 1128,
		velocity: 14.6388888888889,
		power: 14100.3981952842,
		road: 9804.93643518518,
		acceleration: 0.786481481481482
	},
	{
		id: 1130,
		time: 1129,
		velocity: 15.5283333333333,
		power: 14149.6563556023,
		road: 9820.02699074073,
		acceleration: 0.725370370370369
	},
	{
		id: 1131,
		time: 1130,
		velocity: 16.1925,
		power: 15185.6747588246,
		road: 9835.84796296296,
		acceleration: 0.735462962962966
	},
	{
		id: 1132,
		time: 1131,
		velocity: 16.8452777777778,
		power: 15184.5242677668,
		road: 9852.37624999999,
		acceleration: 0.679166666666664
	},
	{
		id: 1133,
		time: 1132,
		velocity: 17.5658333333333,
		power: 15001.291884738,
		road: 9869.5536574074,
		acceleration: 0.619074074074078
	},
	{
		id: 1134,
		time: 1133,
		velocity: 18.0497222222222,
		power: 14862.0278872998,
		road: 9887.32481481481,
		acceleration: 0.568425925925922
	},
	{
		id: 1135,
		time: 1134,
		velocity: 18.5505555555556,
		power: 12302.2187579416,
		road: 9905.57472222222,
		acceleration: 0.389074074074074
	},
	{
		id: 1136,
		time: 1135,
		velocity: 18.7330555555556,
		power: 7875.21144969139,
		road: 9924.0812037037,
		acceleration: 0.124074074074077
	},
	{
		id: 1137,
		time: 1136,
		velocity: 18.4219444444444,
		power: 3285.18270518313,
		road: 9942.58231481481,
		acceleration: -0.134814814814817
	},
	{
		id: 1138,
		time: 1137,
		velocity: 18.1461111111111,
		power: -685.149821650778,
		road: 9960.83879629629,
		acceleration: -0.354444444444443
	},
	{
		id: 1139,
		time: 1138,
		velocity: 17.6697222222222,
		power: -196.115754483059,
		road: 9978.75814814814,
		acceleration: -0.319814814814819
	},
	{
		id: 1140,
		time: 1139,
		velocity: 17.4625,
		power: 1088.12082493725,
		road: 9996.39842592592,
		acceleration: -0.23833333333333
	},
	{
		id: 1141,
		time: 1140,
		velocity: 17.4311111111111,
		power: 1313.34428077265,
		road: 10013.8098148148,
		acceleration: -0.219444444444445
	},
	{
		id: 1142,
		time: 1141,
		velocity: 17.0113888888889,
		power: 1838.76993723359,
		road: 10031.0201388889,
		acceleration: -0.182685185185186
	},
	{
		id: 1143,
		time: 1142,
		velocity: 16.9144444444444,
		power: 1828.95758121375,
		road: 10048.0498148148,
		acceleration: -0.17861111111111
	},
	{
		id: 1144,
		time: 1143,
		velocity: 16.8952777777778,
		power: 3679.44161059125,
		road: 10064.9596296296,
		acceleration: -0.0611111111111136
	},
	{
		id: 1145,
		time: 1144,
		velocity: 16.8280555555556,
		power: 2297.97731746579,
		road: 10081.767037037,
		acceleration: -0.143703703703704
	},
	{
		id: 1146,
		time: 1145,
		velocity: 16.4833333333333,
		power: 1634.40131310848,
		road: 10098.4121296296,
		acceleration: -0.180925925925923
	},
	{
		id: 1147,
		time: 1146,
		velocity: 16.3525,
		power: 1053.29204535794,
		road: 10114.8602777778,
		acceleration: -0.212962962962965
	},
	{
		id: 1148,
		time: 1147,
		velocity: 16.1891666666667,
		power: -201.44996072257,
		road: 10131.0577777778,
		acceleration: -0.288333333333334
	},
	{
		id: 1149,
		time: 1148,
		velocity: 15.6183333333333,
		power: -1714.01118040028,
		road: 10146.9199537037,
		acceleration: -0.382314814814814
	},
	{
		id: 1150,
		time: 1149,
		velocity: 15.2055555555556,
		power: -2826.74343301548,
		road: 10162.3642592593,
		acceleration: -0.453425925925924
	},
	{
		id: 1151,
		time: 1150,
		velocity: 14.8288888888889,
		power: 1644.32973430882,
		road: 10177.5102777778,
		acceleration: -0.14314814814815
	},
	{
		id: 1152,
		time: 1151,
		velocity: 15.1888888888889,
		power: 6033.27737635904,
		road: 10192.6645833333,
		acceleration: 0.159722222222223
	},
	{
		id: 1153,
		time: 1152,
		velocity: 15.6847222222222,
		power: 10952.3080869941,
		road: 10208.1378240741,
		acceleration: 0.478148148148149
	},
	{
		id: 1154,
		time: 1153,
		velocity: 16.2633333333333,
		power: 11202.7304255959,
		road: 10224.0822222222,
		acceleration: 0.464166666666662
	},
	{
		id: 1155,
		time: 1154,
		velocity: 16.5813888888889,
		power: 7796.74865068834,
		road: 10240.3705092593,
		acceleration: 0.223611111111111
	},
	{
		id: 1156,
		time: 1155,
		velocity: 16.3555555555556,
		power: 7701.90811989438,
		road: 10256.8741203704,
		acceleration: 0.20703703703704
	},
	{
		id: 1157,
		time: 1156,
		velocity: 16.8844444444444,
		power: 8792.11415463862,
		road: 10273.6131481481,
		acceleration: 0.263796296296299
	},
	{
		id: 1158,
		time: 1157,
		velocity: 17.3727777777778,
		power: 13120.93167764,
		road: 10290.7381481481,
		acceleration: 0.508148148148145
	},
	{
		id: 1159,
		time: 1158,
		velocity: 17.88,
		power: 12201.7495780775,
		road: 10308.32875,
		acceleration: 0.423055555555557
	},
	{
		id: 1160,
		time: 1159,
		velocity: 18.1536111111111,
		power: 11486.1724929931,
		road: 10326.3097685185,
		acceleration: 0.357777777777777
	},
	{
		id: 1161,
		time: 1160,
		velocity: 18.4461111111111,
		power: 10666.537016943,
		road: 10344.6158796296,
		acceleration: 0.292407407407406
	},
	{
		id: 1162,
		time: 1161,
		velocity: 18.7572222222222,
		power: 10455.9031817929,
		road: 10363.2009722222,
		acceleration: 0.265555555555554
	},
	{
		id: 1163,
		time: 1162,
		velocity: 18.9502777777778,
		power: 9835.49489007075,
		road: 10382.0280555556,
		acceleration: 0.218425925925928
	},
	{
		id: 1164,
		time: 1163,
		velocity: 19.1013888888889,
		power: 7698.63652410731,
		road: 10401.0108796296,
		acceleration: 0.0930555555555586
	},
	{
		id: 1165,
		time: 1164,
		velocity: 19.0363888888889,
		power: 3740.56373554916,
		road: 10419.9781481481,
		acceleration: -0.124166666666667
	},
	{
		id: 1166,
		time: 1165,
		velocity: 18.5777777777778,
		power: -280.732479960507,
		road: 10438.7129166667,
		acceleration: -0.340833333333332
	},
	{
		id: 1167,
		time: 1166,
		velocity: 18.0788888888889,
		power: -2746.23216491221,
		road: 10457.0406481481,
		acceleration: -0.473240740740742
	},
	{
		id: 1168,
		time: 1167,
		velocity: 17.6166666666667,
		power: -1201.27329552027,
		road: 10474.9426851852,
		acceleration: -0.378148148148146
	},
	{
		id: 1169,
		time: 1168,
		velocity: 17.4433333333333,
		power: 1560.58472812173,
		road: 10492.5508796296,
		acceleration: -0.209537037037038
	},
	{
		id: 1170,
		time: 1169,
		velocity: 17.4502777777778,
		power: 7132.47485876561,
		road: 10510.1158333333,
		acceleration: 0.123055555555553
	},
	{
		id: 1171,
		time: 1170,
		velocity: 17.9858333333333,
		power: 10139.4191985683,
		road: 10527.8876851852,
		acceleration: 0.290740740740745
	},
	{
		id: 1172,
		time: 1171,
		velocity: 18.3155555555556,
		power: 13096.2201025871,
		road: 10546.0256481481,
		acceleration: 0.441481481481478
	},
	{
		id: 1173,
		time: 1172,
		velocity: 18.7747222222222,
		power: 12320.6295967549,
		road: 10564.5706944444,
		acceleration: 0.372685185185187
	},
	{
		id: 1174,
		time: 1173,
		velocity: 19.1038888888889,
		power: 10664.0366371051,
		road: 10583.4333333333,
		acceleration: 0.262499999999999
	},
	{
		id: 1175,
		time: 1174,
		velocity: 19.1030555555556,
		power: 5656.07849670451,
		road: 10602.4175462963,
		acceleration: -0.0193518518518516
	},
	{
		id: 1176,
		time: 1175,
		velocity: 18.7166666666667,
		power: 1507.35963995207,
		road: 10621.27,
		acceleration: -0.244166666666668
	},
	{
		id: 1177,
		time: 1176,
		velocity: 18.3713888888889,
		power: 945.351774343764,
		road: 10639.865787037,
		acceleration: -0.269166666666667
	},
	{
		id: 1178,
		time: 1177,
		velocity: 18.2955555555556,
		power: 2330.69725940737,
		road: 10658.2344907407,
		acceleration: -0.184999999999999
	},
	{
		id: 1179,
		time: 1178,
		velocity: 18.1616666666667,
		power: 4406.4914605987,
		road: 10676.4793518518,
		acceleration: -0.0626851851851882
	},
	{
		id: 1180,
		time: 1179,
		velocity: 18.1833333333333,
		power: 3890.55740921755,
		road: 10694.6480092593,
		acceleration: -0.0897222222222211
	},
	{
		id: 1181,
		time: 1180,
		velocity: 18.0263888888889,
		power: 1771.68419863226,
		road: 10712.667962963,
		acceleration: -0.207685185185184
	},
	{
		id: 1182,
		time: 1181,
		velocity: 17.5386111111111,
		power: -1001.84650133022,
		road: 10730.4022222222,
		acceleration: -0.363703703703703
	},
	{
		id: 1183,
		time: 1182,
		velocity: 17.0922222222222,
		power: -608.419034310555,
		road: 10747.7874537037,
		acceleration: -0.334351851851853
	},
	{
		id: 1184,
		time: 1183,
		velocity: 17.0233333333333,
		power: 3865.19594330552,
		road: 10764.9760648148,
		acceleration: -0.058888888888891
	},
	{
		id: 1185,
		time: 1184,
		velocity: 17.3619444444444,
		power: 7037.8381761552,
		road: 10782.2014351852,
		acceleration: 0.13240740740741
	},
	{
		id: 1186,
		time: 1185,
		velocity: 17.4894444444444,
		power: 6684.42030656082,
		road: 10799.5459259259,
		acceleration: 0.105833333333337
	},
	{
		id: 1187,
		time: 1186,
		velocity: 17.3408333333333,
		power: -309.239238143716,
		road: 10816.7864814815,
		acceleration: -0.313703703703705
	},
	{
		id: 1188,
		time: 1187,
		velocity: 16.4208333333333,
		power: -3558.32994321468,
		road: 10833.6161111111,
		acceleration: -0.508148148148152
	},
	{
		id: 1189,
		time: 1188,
		velocity: 15.965,
		power: -6679.87257907078,
		road: 10849.8384259259,
		acceleration: -0.706481481481481
	},
	{
		id: 1190,
		time: 1189,
		velocity: 15.2213888888889,
		power: -2503.11676311964,
		road: 10865.4910648148,
		acceleration: -0.43287037037037
	},
	{
		id: 1191,
		time: 1190,
		velocity: 15.1222222222222,
		power: -1495.12078910088,
		road: 10880.7466666667,
		acceleration: -0.361203703703701
	},
	{
		id: 1192,
		time: 1191,
		velocity: 14.8813888888889,
		power: 628.835061835936,
		road: 10895.7167592593,
		acceleration: -0.209814814814816
	},
	{
		id: 1193,
		time: 1192,
		velocity: 14.5919444444444,
		power: 644.228535815565,
		road: 10910.4796296296,
		acceleration: -0.204629629629629
	},
	{
		id: 1194,
		time: 1193,
		velocity: 14.5083333333333,
		power: 1253.6402066008,
		road: 10925.0615277778,
		acceleration: -0.157314814814814
	},
	{
		id: 1195,
		time: 1194,
		velocity: 14.4094444444444,
		power: 1116.87720150736,
		road: 10939.482962963,
		acceleration: -0.163611111111113
	},
	{
		id: 1196,
		time: 1195,
		velocity: 14.1011111111111,
		power: 5.56521240173622,
		road: 10953.7020833333,
		acceleration: -0.241018518518517
	},
	{
		id: 1197,
		time: 1196,
		velocity: 13.7852777777778,
		power: -1691.02032889571,
		road: 10967.61875,
		acceleration: -0.363888888888889
	},
	{
		id: 1198,
		time: 1197,
		velocity: 13.3177777777778,
		power: 910.570394866127,
		road: 10981.2721296296,
		acceleration: -0.162685185185186
	},
	{
		id: 1199,
		time: 1198,
		velocity: 13.6130555555556,
		power: 2315.42765250994,
		road: 10994.8182407407,
		acceleration: -0.0518518518518523
	},
	{
		id: 1200,
		time: 1199,
		velocity: 13.6297222222222,
		power: 5464.08646226357,
		road: 11008.4326388889,
		acceleration: 0.188425925925927
	},
	{
		id: 1201,
		time: 1200,
		velocity: 13.8830555555556,
		power: 4909.30603947775,
		road: 11022.2106018518,
		acceleration: 0.138703703703703
	},
	{
		id: 1202,
		time: 1201,
		velocity: 14.0291666666667,
		power: 3975.03896527328,
		road: 11036.0898611111,
		acceleration: 0.06388888888889
	},
	{
		id: 1203,
		time: 1202,
		velocity: 13.8213888888889,
		power: 2163.3745295544,
		road: 11049.9647222222,
		acceleration: -0.0726851851851844
	},
	{
		id: 1204,
		time: 1203,
		velocity: 13.665,
		power: 1385.23682896871,
		road: 11063.7387037037,
		acceleration: -0.129074074074076
	},
	{
		id: 1205,
		time: 1204,
		velocity: 13.6419444444444,
		power: 3562.12210550744,
		road: 11077.4671759259,
		acceleration: 0.0380555555555571
	},
	{
		id: 1206,
		time: 1205,
		velocity: 13.9355555555556,
		power: 5329.73225595647,
		road: 11091.2987962963,
		acceleration: 0.168240740740739
	},
	{
		id: 1207,
		time: 1206,
		velocity: 14.1697222222222,
		power: 6233.31724357925,
		road: 11105.327962963,
		acceleration: 0.226851851851853
	},
	{
		id: 1208,
		time: 1207,
		velocity: 14.3225,
		power: 5807.54620936695,
		road: 11119.563287037,
		acceleration: 0.185462962962964
	},
	{
		id: 1209,
		time: 1208,
		velocity: 14.4919444444444,
		power: 6081.75036120258,
		road: 11133.9896296296,
		acceleration: 0.196574074074073
	},
	{
		id: 1210,
		time: 1209,
		velocity: 14.7594444444444,
		power: 5460.89099868983,
		road: 11148.5862962963,
		acceleration: 0.144074074074075
	},
	{
		id: 1211,
		time: 1210,
		velocity: 14.7547222222222,
		power: 5783.41880197301,
		road: 11163.3351851852,
		acceleration: 0.160370370370368
	},
	{
		id: 1212,
		time: 1211,
		velocity: 14.9730555555556,
		power: 3807.48528735698,
		road: 11178.1727777778,
		acceleration: 0.0170370370370367
	},
	{
		id: 1213,
		time: 1212,
		velocity: 14.8105555555556,
		power: 3492.60020467306,
		road: 11193.0162037037,
		acceleration: -0.00537037037036825
	},
	{
		id: 1214,
		time: 1213,
		velocity: 14.7386111111111,
		power: 1939.61443525145,
		road: 11207.8002777778,
		acceleration: -0.113333333333333
	},
	{
		id: 1215,
		time: 1214,
		velocity: 14.6330555555556,
		power: 763.09414070574,
		road: 11222.4309259259,
		acceleration: -0.193518518518518
	},
	{
		id: 1216,
		time: 1215,
		velocity: 14.23,
		power: -70.690876599595,
		road: 11236.84,
		acceleration: -0.249629629629629
	},
	{
		id: 1217,
		time: 1216,
		velocity: 13.9897222222222,
		power: -442.729545031767,
		road: 11250.9877314815,
		acceleration: -0.273055555555555
	},
	{
		id: 1218,
		time: 1217,
		velocity: 13.8138888888889,
		power: 1240.41049779666,
		road: 11264.927037037,
		acceleration: -0.1437962962963
	},
	{
		id: 1219,
		time: 1218,
		velocity: 13.7986111111111,
		power: 2157.37601129768,
		road: 11278.7584722222,
		acceleration: -0.0719444444444424
	},
	{
		id: 1220,
		time: 1219,
		velocity: 13.7738888888889,
		power: 1173.30138786649,
		road: 11292.4818981481,
		acceleration: -0.144074074074075
	},
	{
		id: 1221,
		time: 1220,
		velocity: 13.3816666666667,
		power: -588.555075274889,
		road: 11305.9952777778,
		acceleration: -0.276018518518518
	},
	{
		id: 1222,
		time: 1221,
		velocity: 12.9705555555556,
		power: -1127.08986240041,
		road: 11319.2130092593,
		acceleration: -0.31527777777778
	},
	{
		id: 1223,
		time: 1222,
		velocity: 12.8280555555556,
		power: -1694.02930392505,
		road: 11332.0937037037,
		acceleration: -0.358796296296294
	},
	{
		id: 1224,
		time: 1223,
		velocity: 12.3052777777778,
		power: -746.694230900112,
		road: 11344.6556481481,
		acceleration: -0.278703703703703
	},
	{
		id: 1225,
		time: 1224,
		velocity: 12.1344444444444,
		power: -1881.69945542537,
		road: 11356.891712963,
		acceleration: -0.373055555555558
	},
	{
		id: 1226,
		time: 1225,
		velocity: 11.7088888888889,
		power: -1118.86749219369,
		road: 11368.7883333333,
		acceleration: -0.305833333333332
	},
	{
		id: 1227,
		time: 1226,
		velocity: 11.3877777777778,
		power: -1381.62553806074,
		road: 11380.367962963,
		acceleration: -0.328148148148147
	},
	{
		id: 1228,
		time: 1227,
		velocity: 11.15,
		power: -1046.21178045895,
		road: 11391.6353240741,
		acceleration: -0.29638888888889
	},
	{
		id: 1229,
		time: 1228,
		velocity: 10.8197222222222,
		power: 152.559266526063,
		road: 11402.66375,
		acceleration: -0.18148148148148
	},
	{
		id: 1230,
		time: 1229,
		velocity: 10.8433333333333,
		power: 1762.61303077343,
		road: 11413.5887037037,
		acceleration: -0.0254629629629655
	},
	{
		id: 1231,
		time: 1230,
		velocity: 11.0736111111111,
		power: 2032.59265001482,
		road: 11424.5013425926,
		acceleration: 0.000833333333336128
	},
	{
		id: 1232,
		time: 1231,
		velocity: 10.8222222222222,
		power: 1936.08235763247,
		road: 11435.4102314815,
		acceleration: -0.00833333333333286
	},
	{
		id: 1233,
		time: 1232,
		velocity: 10.8183333333333,
		power: 1495.01734201889,
		road: 11446.2899537037,
		acceleration: -0.0500000000000025
	},
	{
		id: 1234,
		time: 1233,
		velocity: 10.9236111111111,
		power: 2957.964121167,
		road: 11457.1898148148,
		acceleration: 0.0902777777777786
	},
	{
		id: 1235,
		time: 1234,
		velocity: 11.0930555555556,
		power: 2981.44794102793,
		road: 11468.1793518518,
		acceleration: 0.089074074074075
	},
	{
		id: 1236,
		time: 1235,
		velocity: 11.0855555555556,
		power: 3410.8716558071,
		road: 11479.2762037037,
		acceleration: 0.125555555555554
	},
	{
		id: 1237,
		time: 1236,
		velocity: 11.3002777777778,
		power: 3352.39141141851,
		road: 11490.4933796296,
		acceleration: 0.115092592592594
	},
	{
		id: 1238,
		time: 1237,
		velocity: 11.4383333333333,
		power: 4300.37360214677,
		road: 11501.8662037037,
		acceleration: 0.196203703703702
	},
	{
		id: 1239,
		time: 1238,
		velocity: 11.6741666666667,
		power: 4594.63615600546,
		road: 11513.4437037037,
		acceleration: 0.213148148148148
	},
	{
		id: 1240,
		time: 1239,
		velocity: 11.9397222222222,
		power: 3866.93399568949,
		road: 11525.1975925926,
		acceleration: 0.139629629629628
	},
	{
		id: 1241,
		time: 1240,
		velocity: 11.8572222222222,
		power: 4100.71721067523,
		road: 11537.098287037,
		acceleration: 0.153981481481484
	},
	{
		id: 1242,
		time: 1241,
		velocity: 12.1361111111111,
		power: 3946.4781315481,
		road: 11549.1431018518,
		acceleration: 0.134259259259256
	},
	{
		id: 1243,
		time: 1242,
		velocity: 12.3425,
		power: 7070.38301397855,
		road: 11561.4497222222,
		acceleration: 0.389351851851853
	},
	{
		id: 1244,
		time: 1243,
		velocity: 13.0252777777778,
		power: 10192.4772676285,
		road: 11574.257962963,
		acceleration: 0.613888888888891
	},
	{
		id: 1245,
		time: 1244,
		velocity: 13.9777777777778,
		power: 13347.9007814636,
		road: 11587.7750462963,
		acceleration: 0.803796296296296
	},
	{
		id: 1246,
		time: 1245,
		velocity: 14.7538888888889,
		power: 12776.3096157786,
		road: 11602.041712963,
		acceleration: 0.69537037037037
	},
	{
		id: 1247,
		time: 1246,
		velocity: 15.1113888888889,
		power: 7580.47146113006,
		road: 11616.7997685185,
		acceleration: 0.287407407407409
	},
	{
		id: 1248,
		time: 1247,
		velocity: 14.84,
		power: 3225.77983606906,
		road: 11631.6887037037,
		acceleration: -0.0256481481481501
	},
	{
		id: 1249,
		time: 1248,
		velocity: 14.6769444444444,
		power: -149.646182579727,
		road: 11646.4344907407,
		acceleration: -0.260648148148146
	},
	{
		id: 1250,
		time: 1249,
		velocity: 14.3294444444444,
		power: 1524.41021347658,
		road: 11660.9814351852,
		acceleration: -0.137037037037038
	},
	{
		id: 1251,
		time: 1250,
		velocity: 14.4288888888889,
		power: 857.278472653178,
		road: 11675.3689814815,
		acceleration: -0.181759259259259
	},
	{
		id: 1252,
		time: 1251,
		velocity: 14.1316666666667,
		power: 1178.77372012363,
		road: 11689.5883333333,
		acceleration: -0.15462962962963
	},
	{
		id: 1253,
		time: 1252,
		velocity: 13.8655555555556,
		power: 249.714888168696,
		road: 11703.620462963,
		acceleration: -0.219814814814814
	},
	{
		id: 1254,
		time: 1253,
		velocity: 13.7694444444444,
		power: 2381.59080847754,
		road: 11717.5143055556,
		acceleration: -0.056759259259259
	},
	{
		id: 1255,
		time: 1254,
		velocity: 13.9613888888889,
		power: 4881.23073879376,
		road: 11731.4448148148,
		acceleration: 0.130092592592591
	},
	{
		id: 1256,
		time: 1255,
		velocity: 14.2558333333333,
		power: 5674.71433854241,
		road: 11745.5316203704,
		acceleration: 0.182499999999999
	},
	{
		id: 1257,
		time: 1256,
		velocity: 14.3169444444444,
		power: 8357.42805765742,
		road: 11759.8924537037,
		acceleration: 0.365555555555556
	},
	{
		id: 1258,
		time: 1257,
		velocity: 15.0580555555556,
		power: 6841.47542017072,
		road: 11774.5559259259,
		acceleration: 0.239722222222223
	},
	{
		id: 1259,
		time: 1258,
		velocity: 14.975,
		power: 6165.33728313559,
		road: 11789.4300925926,
		acceleration: 0.181666666666665
	},
	{
		id: 1260,
		time: 1259,
		velocity: 14.8619444444444,
		power: 4743.82393327279,
		road: 11804.4333796296,
		acceleration: 0.0765740740740739
	},
	{
		id: 1261,
		time: 1260,
		velocity: 15.2877777777778,
		power: 7164.02745171691,
		road: 11819.5937037037,
		acceleration: 0.237500000000002
	},
	{
		id: 1262,
		time: 1261,
		velocity: 15.6875,
		power: 9360.27777727562,
		road: 11835.058287037,
		acceleration: 0.371018518518518
	},
	{
		id: 1263,
		time: 1262,
		velocity: 15.975,
		power: 7486.63113764957,
		road: 11850.8230092593,
		acceleration: 0.229259259259258
	},
	{
		id: 1264,
		time: 1263,
		velocity: 15.9755555555556,
		power: 5240.48008707494,
		road: 11866.7394444444,
		acceleration: 0.0741666666666685
	},
	{
		id: 1265,
		time: 1264,
		velocity: 15.91,
		power: 3182.29041472555,
		road: 11882.6623148148,
		acceleration: -0.0612962962962982
	},
	{
		id: 1266,
		time: 1265,
		velocity: 15.7911111111111,
		power: 4081.85328533708,
		road: 11898.5539814815,
		acceleration: -0.00111111111111128
	},
	{
		id: 1267,
		time: 1266,
		velocity: 15.9722222222222,
		power: 5109.58553119955,
		road: 11914.4777314815,
		acceleration: 0.0652777777777764
	},
	{
		id: 1268,
		time: 1267,
		velocity: 16.1058333333333,
		power: 6378.4190926509,
		road: 11930.5061111111,
		acceleration: 0.143981481481481
	},
	{
		id: 1269,
		time: 1268,
		velocity: 16.2230555555556,
		power: 5116.20494104078,
		road: 11946.6353240741,
		acceleration: 0.0576851851851856
	},
	{
		id: 1270,
		time: 1269,
		velocity: 16.1452777777778,
		power: 2935.54343984539,
		road: 11962.751712963,
		acceleration: -0.0833333333333286
	},
	{
		id: 1271,
		time: 1270,
		velocity: 15.8558333333333,
		power: 447.502890479015,
		road: 11978.7056481481,
		acceleration: -0.241574074074077
	},
	{
		id: 1272,
		time: 1271,
		velocity: 15.4983333333333,
		power: -621.551119646265,
		road: 11994.3850462963,
		acceleration: -0.307500000000003
	},
	{
		id: 1273,
		time: 1272,
		velocity: 15.2227777777778,
		power: -690.991931427037,
		road: 12009.7568518519,
		acceleration: -0.307685185185182
	},
	{
		id: 1274,
		time: 1273,
		velocity: 14.9327777777778,
		power: -754.311508329937,
		road: 12024.8209259259,
		acceleration: -0.307777777777778
	},
	{
		id: 1275,
		time: 1274,
		velocity: 14.575,
		power: 288.058271538407,
		road: 12039.6158796296,
		acceleration: -0.230462962962962
	},
	{
		id: 1276,
		time: 1275,
		velocity: 14.5313888888889,
		power: 2114.37468013089,
		road: 12054.2471759259,
		acceleration: -0.0968518518518504
	},
	{
		id: 1277,
		time: 1276,
		velocity: 14.6422222222222,
		power: 4435.37052126169,
		road: 12068.8649074074,
		acceleration: 0.0697222222222198
	},
	{
		id: 1278,
		time: 1277,
		velocity: 14.7841666666667,
		power: 5380.58002663851,
		road: 12083.5840277778,
		acceleration: 0.133055555555556
	},
	{
		id: 1279,
		time: 1278,
		velocity: 14.9305555555556,
		power: 4832.55452584797,
		road: 12098.4144907407,
		acceleration: 0.0896296296296288
	},
	{
		id: 1280,
		time: 1279,
		velocity: 14.9111111111111,
		power: 5840.11826318019,
		road: 12113.3674074074,
		acceleration: 0.15527777777778
	},
	{
		id: 1281,
		time: 1280,
		velocity: 15.25,
		power: 3694.53425192769,
		road: 12128.3991666667,
		acceleration: 0.00240740740740719
	},
	{
		id: 1282,
		time: 1281,
		velocity: 14.9377777777778,
		power: 1966.8714427734,
		road: 12143.3739351852,
		acceleration: -0.116388888888888
	},
	{
		id: 1283,
		time: 1282,
		velocity: 14.5619444444444,
		power: -498.839750125673,
		road: 12158.1475925926,
		acceleration: -0.285833333333334
	},
	{
		id: 1284,
		time: 1283,
		velocity: 14.3925,
		power: 83.9475752734773,
		road: 12172.658287037,
		acceleration: -0.240092592592594
	},
	{
		id: 1285,
		time: 1284,
		velocity: 14.2175,
		power: 2849.90864652767,
		road: 12187.030787037,
		acceleration: -0.036296296296296
	},
	{
		id: 1286,
		time: 1285,
		velocity: 14.4530555555556,
		power: 3246.99397758678,
		road: 12201.3818055556,
		acceleration: -0.00666666666666593
	},
	{
		id: 1287,
		time: 1286,
		velocity: 14.3725,
		power: 3344.59708987983,
		road: 12215.7297685185,
		acceleration: 0.000555555555555642
	},
	{
		id: 1288,
		time: 1287,
		velocity: 14.2191666666667,
		power: 693.521686177536,
		road: 12229.9825,
		acceleration: -0.191018518518518
	},
	{
		id: 1289,
		time: 1288,
		velocity: 13.88,
		power: 439.962216792798,
		road: 12244.036712963,
		acceleration: -0.206018518518519
	},
	{
		id: 1290,
		time: 1289,
		velocity: 13.7544444444444,
		power: -802.354237150628,
		road: 12257.8400462963,
		acceleration: -0.29574074074074
	},
	{
		id: 1291,
		time: 1290,
		velocity: 13.3319444444444,
		power: -1064.43099948355,
		road: 12271.3391203704,
		acceleration: -0.312777777777779
	},
	{
		id: 1292,
		time: 1291,
		velocity: 12.9416666666667,
		power: -1770.24510854369,
		road: 12284.4987962963,
		acceleration: -0.366018518518517
	},
	{
		id: 1293,
		time: 1292,
		velocity: 12.6563888888889,
		power: -1990.81019384583,
		road: 12297.2840740741,
		acceleration: -0.382777777777777
	},
	{
		id: 1294,
		time: 1293,
		velocity: 12.1836111111111,
		power: -92.5983630373788,
		road: 12309.7664351852,
		acceleration: -0.223055555555558
	},
	{
		id: 1295,
		time: 1294,
		velocity: 12.2725,
		power: 1174.41445776021,
		road: 12322.080787037,
		acceleration: -0.112962962962962
	},
	{
		id: 1296,
		time: 1295,
		velocity: 12.3175,
		power: 3570.33308320548,
		road: 12334.3843518519,
		acceleration: 0.0913888888888881
	},
	{
		id: 1297,
		time: 1296,
		velocity: 12.4577777777778,
		power: 4262.06024973702,
		road: 12346.8062037037,
		acceleration: 0.145185185185188
	},
	{
		id: 1298,
		time: 1297,
		velocity: 12.7080555555556,
		power: 4260.78938104816,
		road: 12359.3701388889,
		acceleration: 0.13898148148148
	},
	{
		id: 1299,
		time: 1298,
		velocity: 12.7344444444444,
		power: 3250.38735510836,
		road: 12372.029212963,
		acceleration: 0.0512962962962966
	},
	{
		id: 1300,
		time: 1299,
		velocity: 12.6116666666667,
		power: 727.271943210954,
		road: 12384.6356481482,
		acceleration: -0.156574074074074
	},
	{
		id: 1301,
		time: 1300,
		velocity: 12.2383333333333,
		power: -950.767271158969,
		road: 12397.0166203704,
		acceleration: -0.294351851851852
	},
	{
		id: 1302,
		time: 1301,
		velocity: 11.8513888888889,
		power: -611.301282768738,
		road: 12409.1189351852,
		acceleration: -0.262962962962964
	},
	{
		id: 1303,
		time: 1302,
		velocity: 11.8227777777778,
		power: 1169.62314855549,
		road: 12421.0374537037,
		acceleration: -0.104629629629628
	},
	{
		id: 1304,
		time: 1303,
		velocity: 11.9244444444444,
		power: 3066.27401029425,
		road: 12432.9351388889,
		acceleration: 0.0629629629629616
	},
	{
		id: 1305,
		time: 1304,
		velocity: 12.0402777777778,
		power: 4013.85928269495,
		road: 12444.9353240741,
		acceleration: 0.142037037037037
	},
	{
		id: 1306,
		time: 1305,
		velocity: 12.2488888888889,
		power: 3741.59482816781,
		road: 12457.0630555556,
		acceleration: 0.113055555555556
	},
	{
		id: 1307,
		time: 1306,
		velocity: 12.2636111111111,
		power: 4360.81941117113,
		road: 12469.3275462963,
		acceleration: 0.160462962962963
	},
	{
		id: 1308,
		time: 1307,
		velocity: 12.5216666666667,
		power: 4553.65258243514,
		road: 12481.7569907407,
		acceleration: 0.169444444444444
	},
	{
		id: 1309,
		time: 1308,
		velocity: 12.7572222222222,
		power: 5504.44126567765,
		road: 12494.3907407407,
		acceleration: 0.239166666666666
	},
	{
		id: 1310,
		time: 1309,
		velocity: 12.9811111111111,
		power: 6286.23272638848,
		road: 12507.2887962963,
		acceleration: 0.289444444444445
	},
	{
		id: 1311,
		time: 1310,
		velocity: 13.39,
		power: 7086.421482604,
		road: 12520.4996296296,
		acceleration: 0.33611111111111
	},
	{
		id: 1312,
		time: 1311,
		velocity: 13.7655555555556,
		power: 7943.35096488038,
		road: 12534.0694444444,
		acceleration: 0.381851851851854
	},
	{
		id: 1313,
		time: 1312,
		velocity: 14.1266666666667,
		power: 8214.53115259744,
		road: 12548.0199074074,
		acceleration: 0.379444444444442
	},
	{
		id: 1314,
		time: 1313,
		velocity: 14.5283333333333,
		power: 9108.24828074151,
		road: 12562.3705555556,
		acceleration: 0.420925925925927
	},
	{
		id: 1315,
		time: 1314,
		velocity: 15.0283333333333,
		power: 6644.86178635178,
		road: 12577.0442592593,
		acceleration: 0.225185185185184
	},
	{
		id: 1316,
		time: 1315,
		velocity: 14.8022222222222,
		power: 6485.31084345724,
		road: 12591.9323148148,
		acceleration: 0.203518518518521
	},
	{
		id: 1317,
		time: 1316,
		velocity: 15.1388888888889,
		power: 4060.70748517844,
		road: 12606.9365740741,
		acceleration: 0.0288888888888863
	},
	{
		id: 1318,
		time: 1317,
		velocity: 15.115,
		power: 5252.68184593578,
		road: 12622.0098611111,
		acceleration: 0.109166666666667
	},
	{
		id: 1319,
		time: 1318,
		velocity: 15.1297222222222,
		power: 3761.62361272099,
		road: 12637.1395833333,
		acceleration: 0.00370370370370487
	},
	{
		id: 1320,
		time: 1319,
		velocity: 15.15,
		power: 2603.3533418592,
		road: 12652.2334722222,
		acceleration: -0.0753703703703703
	},
	{
		id: 1321,
		time: 1320,
		velocity: 14.8888888888889,
		power: 2235.16818949827,
		road: 12667.2404166667,
		acceleration: -0.0985185185185191
	},
	{
		id: 1322,
		time: 1321,
		velocity: 14.8341666666667,
		power: 1711.99759592775,
		road: 12682.132037037,
		acceleration: -0.132129629629629
	},
	{
		id: 1323,
		time: 1322,
		velocity: 14.7536111111111,
		power: 1721.07567268962,
		road: 12696.8934722222,
		acceleration: -0.12824074074074
	},
	{
		id: 1324,
		time: 1323,
		velocity: 14.5041666666667,
		power: 1203.81978134573,
		road: 12711.5099537037,
		acceleration: -0.161666666666667
	},
	{
		id: 1325,
		time: 1324,
		velocity: 14.3491666666667,
		power: 753.542513715454,
		road: 12725.9504166667,
		acceleration: -0.190370370370372
	},
	{
		id: 1326,
		time: 1325,
		velocity: 14.1825,
		power: 1350.45872645137,
		road: 12740.2240740741,
		acceleration: -0.14324074074074
	},
	{
		id: 1327,
		time: 1326,
		velocity: 14.0744444444444,
		power: 1757.55010812377,
		road: 12754.3710185185,
		acceleration: -0.110185185185184
	},
	{
		id: 1328,
		time: 1327,
		velocity: 14.0186111111111,
		power: 1901.34145313893,
		road: 12768.4144444444,
		acceleration: -0.0968518518518522
	},
	{
		id: 1329,
		time: 1328,
		velocity: 13.8919444444444,
		power: 1695.4832302686,
		road: 12782.3546296296,
		acceleration: -0.10962962962963
	},
	{
		id: 1330,
		time: 1329,
		velocity: 13.7455555555556,
		power: -184.389794488896,
		road: 12796.1158796296,
		acceleration: -0.248240740740737
	},
	{
		id: 1331,
		time: 1330,
		velocity: 13.2738888888889,
		power: -1341.63250416387,
		road: 12809.5859722222,
		acceleration: -0.334074074074078
	},
	{
		id: 1332,
		time: 1331,
		velocity: 12.8897222222222,
		power: -2221.17672383824,
		road: 12822.6881018519,
		acceleration: -0.401851851851852
	},
	{
		id: 1333,
		time: 1332,
		velocity: 12.54,
		power: -591.268750173454,
		road: 12835.4553703704,
		acceleration: -0.267870370370369
	},
	{
		id: 1334,
		time: 1333,
		velocity: 12.4702777777778,
		power: 880.236309803485,
		road: 12848.0172222222,
		acceleration: -0.142962962962962
	},
	{
		id: 1335,
		time: 1334,
		velocity: 12.4608333333333,
		power: 3336.64453618126,
		road: 12860.5393055556,
		acceleration: 0.0634259259259267
	},
	{
		id: 1336,
		time: 1335,
		velocity: 12.7302777777778,
		power: 3734.03714431846,
		road: 12873.1399074074,
		acceleration: 0.0936111111111089
	},
	{
		id: 1337,
		time: 1336,
		velocity: 12.7511111111111,
		power: 4048.27025085116,
		road: 12885.8450462963,
		acceleration: 0.115462962962964
	},
	{
		id: 1338,
		time: 1337,
		velocity: 12.8072222222222,
		power: 2724.43953361652,
		road: 12898.6100925926,
		acceleration: 0.00435185185185105
	},
	{
		id: 1339,
		time: 1338,
		velocity: 12.7433333333333,
		power: 2668.82915874876,
		road: 12911.3771759259,
		acceleration: -0.000277777777778709
	},
	{
		id: 1340,
		time: 1339,
		velocity: 12.7502777777778,
		power: 1691.98298073875,
		road: 12924.1043981481,
		acceleration: -0.0794444444444427
	},
	{
		id: 1341,
		time: 1340,
		velocity: 12.5688888888889,
		power: 1549.98328783463,
		road: 12936.7473611111,
		acceleration: -0.089074074074075
	},
	{
		id: 1342,
		time: 1341,
		velocity: 12.4761111111111,
		power: -427.98002601988,
		road: 12949.2202314815,
		acceleration: -0.251111111111111
	},
	{
		id: 1343,
		time: 1342,
		velocity: 11.9969444444444,
		power: -888.092690463178,
		road: 12961.4237037037,
		acceleration: -0.287685185185186
	},
	{
		id: 1344,
		time: 1343,
		velocity: 11.7058333333333,
		power: -1818.51067885438,
		road: 12973.2995833333,
		acceleration: -0.3675
	},
	{
		id: 1345,
		time: 1344,
		velocity: 11.3736111111111,
		power: -1510.11544999572,
		road: 12984.8218518519,
		acceleration: -0.339722222222221
	},
	{
		id: 1346,
		time: 1345,
		velocity: 10.9777777777778,
		power: -1790.71652681749,
		road: 12995.9912962963,
		acceleration: -0.365925925925925
	},
	{
		id: 1347,
		time: 1346,
		velocity: 10.6080555555556,
		power: -1480.16603041587,
		road: 13006.8093055556,
		acceleration: -0.336944444444445
	},
	{
		id: 1348,
		time: 1347,
		velocity: 10.3627777777778,
		power: -727.011418042276,
		road: 13017.3276851852,
		acceleration: -0.262314814814815
	},
	{
		id: 1349,
		time: 1348,
		velocity: 10.1908333333333,
		power: 717.095784380691,
		road: 13027.6575462963,
		acceleration: -0.114722222222222
	},
	{
		id: 1350,
		time: 1349,
		velocity: 10.2638888888889,
		power: 901.927679262119,
		road: 13037.8831481481,
		acceleration: -0.093796296296297
	},
	{
		id: 1351,
		time: 1350,
		velocity: 10.0813888888889,
		power: 546.936198033718,
		road: 13047.9976851852,
		acceleration: -0.128333333333334
	},
	{
		id: 1352,
		time: 1351,
		velocity: 9.80583333333333,
		power: 700.38401429788,
		road: 13057.992962963,
		acceleration: -0.110185185185186
	},
	{
		id: 1353,
		time: 1352,
		velocity: 9.93333333333333,
		power: 3017.07460505181,
		road: 13067.9994444444,
		acceleration: 0.132592592592594
	},
	{
		id: 1354,
		time: 1353,
		velocity: 10.4791666666667,
		power: 4234.48665770765,
		road: 13078.1970833333,
		acceleration: 0.24972222222222
	},
	{
		id: 1355,
		time: 1354,
		velocity: 10.555,
		power: 3467.28400906887,
		road: 13088.6002314815,
		acceleration: 0.161296296296298
	},
	{
		id: 1356,
		time: 1355,
		velocity: 10.4172222222222,
		power: 1267.67849660936,
		road: 13099.0531481481,
		acceleration: -0.061759259259258
	},
	{
		id: 1357,
		time: 1356,
		velocity: 10.2938888888889,
		power: 919.439522037338,
		road: 13109.4276388889,
		acceleration: -0.0950925925925947
	},
	{
		id: 1358,
		time: 1357,
		velocity: 10.2697222222222,
		power: 761.831428741871,
		road: 13119.7000462963,
		acceleration: -0.109074074074073
	},
	{
		id: 1359,
		time: 1358,
		velocity: 10.09,
		power: 1049.76700726215,
		road: 13129.8791203704,
		acceleration: -0.0775925925925929
	},
	{
		id: 1360,
		time: 1359,
		velocity: 10.0611111111111,
		power: 761.430680007465,
		road: 13139.9666203704,
		acceleration: -0.105555555555554
	},
	{
		id: 1361,
		time: 1360,
		velocity: 9.95305555555556,
		power: 640.750128139666,
		road: 13149.943287037,
		acceleration: -0.116111111111111
	},
	{
		id: 1362,
		time: 1361,
		velocity: 9.74166666666667,
		power: 493.871555031347,
		road: 13159.7971296296,
		acceleration: -0.129537037037037
	},
	{
		id: 1363,
		time: 1362,
		velocity: 9.6725,
		power: 417.026893085047,
		road: 13169.5183796296,
		acceleration: -0.13564814814815
	},
	{
		id: 1364,
		time: 1363,
		velocity: 9.54611111111111,
		power: 566.521056481435,
		road: 13179.1131481481,
		acceleration: -0.117314814814813
	},
	{
		id: 1365,
		time: 1364,
		velocity: 9.38972222222222,
		power: -4048.98434575328,
		road: 13188.3309722222,
		acceleration: -0.636574074074074
	},
	{
		id: 1366,
		time: 1365,
		velocity: 7.76277777777778,
		power: -7160.62200379933,
		road: 13196.6974537037,
		acceleration: -1.06611111111111
	},
	{
		id: 1367,
		time: 1366,
		velocity: 6.34777777777778,
		power: -8748.66221728928,
		road: 13203.8058796296,
		acceleration: -1.45
	},
	{
		id: 1368,
		time: 1367,
		velocity: 5.03972222222222,
		power: -5284.91611746647,
		road: 13209.6393981481,
		acceleration: -1.09981481481482
	},
	{
		id: 1369,
		time: 1368,
		velocity: 4.46333333333333,
		power: -3820.82390221635,
		road: 13214.433287037,
		acceleration: -0.979444444444445
	},
	{
		id: 1370,
		time: 1369,
		velocity: 3.40944444444444,
		power: -3199.01987221744,
		road: 13218.2253703704,
		acceleration: -1.02416666666667
	},
	{
		id: 1371,
		time: 1370,
		velocity: 1.96722222222222,
		power: -3263.75196551971,
		road: 13220.7614814815,
		acceleration: -1.48777777777778
	},
	{
		id: 1372,
		time: 1371,
		velocity: 0,
		power: -1169.655479358,
		road: 13221.985462963,
		acceleration: -1.13648148148148
	},
	{
		id: 1373,
		time: 1372,
		velocity: 0,
		power: -164.069669558155,
		road: 13222.3133333333,
		acceleration: -0.655740740740741
	},
	{
		id: 1374,
		time: 1373,
		velocity: 0,
		power: 0,
		road: 13222.3133333333,
		acceleration: 0
	},
	{
		id: 1375,
		time: 1374,
		velocity: 0,
		power: 0,
		road: 13222.3133333333,
		acceleration: 0
	},
	{
		id: 1376,
		time: 1375,
		velocity: 0,
		power: 0,
		road: 13222.3133333333,
		acceleration: 0
	},
	{
		id: 1377,
		time: 1376,
		velocity: 0,
		power: 0,
		road: 13222.3133333333,
		acceleration: 0
	},
	{
		id: 1378,
		time: 1377,
		velocity: 0,
		power: 0,
		road: 13222.3133333333,
		acceleration: 0
	},
	{
		id: 1379,
		time: 1378,
		velocity: 0,
		power: 20.5340433510775,
		road: 13222.3903240741,
		acceleration: 0.153981481481481
	},
	{
		id: 1380,
		time: 1379,
		velocity: 0.461944444444444,
		power: 665.55922407825,
		road: 13223.0296296296,
		acceleration: 0.970648148148148
	},
	{
		id: 1381,
		time: 1380,
		velocity: 2.91194444444444,
		power: 3250.92986235998,
		road: 13224.9717592593,
		acceleration: 1.635
	},
	{
		id: 1382,
		time: 1381,
		velocity: 4.905,
		power: 6301.25292861979,
		road: 13228.5828703704,
		acceleration: 1.70296296296296
	},
	{
		id: 1383,
		time: 1382,
		velocity: 5.57083333333333,
		power: 4519.6271024976,
		road: 13233.4625,
		acceleration: 0.834074074074074
	},
	{
		id: 1384,
		time: 1383,
		velocity: 5.41416666666667,
		power: 1502.7424110404,
		road: 13238.834537037,
		acceleration: 0.150740740740741
	},
	{
		id: 1385,
		time: 1384,
		velocity: 5.35722222222222,
		power: 1383.17520013237,
		road: 13244.3418518519,
		acceleration: 0.119814814814815
	},
	{
		id: 1386,
		time: 1385,
		velocity: 5.93027777777778,
		power: 2591.2689234273,
		road: 13250.0739351852,
		acceleration: 0.329722222222221
	},
	{
		id: 1387,
		time: 1386,
		velocity: 6.40333333333333,
		power: 3581.66135285642,
		road: 13256.2038888889,
		acceleration: 0.466018518518519
	},
	{
		id: 1388,
		time: 1387,
		velocity: 6.75527777777778,
		power: 2617.20398365607,
		road: 13262.7030092593,
		acceleration: 0.272314814814814
	},
	{
		id: 1389,
		time: 1388,
		velocity: 6.74722222222222,
		power: 1381.6801010829,
		road: 13269.3710185185,
		acceleration: 0.0654629629629628
	},
	{
		id: 1390,
		time: 1389,
		velocity: 6.59972222222222,
		power: 1826.32624236636,
		road: 13276.1370833333,
		acceleration: 0.130648148148148
	},
	{
		id: 1391,
		time: 1390,
		velocity: 7.14722222222222,
		power: 1852.16027248059,
		road: 13283.0325925926,
		acceleration: 0.128240740740741
	},
	{
		id: 1392,
		time: 1391,
		velocity: 7.13194444444444,
		power: 2639.15465955456,
		road: 13290.1104166667,
		acceleration: 0.236388888888889
	},
	{
		id: 1393,
		time: 1392,
		velocity: 7.30888888888889,
		power: 1342.62414359463,
		road: 13297.3258796296,
		acceleration: 0.0388888888888896
	},
	{
		id: 1394,
		time: 1393,
		velocity: 7.26388888888889,
		power: 951.140929378863,
		road: 13304.5515740741,
		acceleration: -0.0184259259259276
	},
	{
		id: 1395,
		time: 1394,
		velocity: 7.07666666666667,
		power: 513.267685531425,
		road: 13311.7274537037,
		acceleration: -0.0812037037037028
	},
	{
		id: 1396,
		time: 1395,
		velocity: 7.06527777777778,
		power: 318.5944028752,
		road: 13318.8085648148,
		acceleration: -0.108333333333334
	},
	{
		id: 1397,
		time: 1396,
		velocity: 6.93888888888889,
		power: 610.83256299622,
		road: 13325.8039351852,
		acceleration: -0.063148148148148
	},
	{
		id: 1398,
		time: 1397,
		velocity: 6.88722222222222,
		power: 716.913398817088,
		road: 13332.7447685185,
		acceleration: -0.0459259259259248
	},
	{
		id: 1399,
		time: 1398,
		velocity: 6.9275,
		power: 55.3553796271659,
		road: 13339.59,
		acceleration: -0.145277777777778
	},
	{
		id: 1400,
		time: 1399,
		velocity: 6.50305555555556,
		power: -1817.45594462665,
		road: 13346.1409259259,
		acceleration: -0.443333333333332
	},
	{
		id: 1401,
		time: 1400,
		velocity: 5.55722222222222,
		power: -3412.15583526015,
		road: 13352.0950462963,
		acceleration: -0.750277777777779
	},
	{
		id: 1402,
		time: 1401,
		velocity: 4.67666666666667,
		power: -3581.17932436761,
		road: 13357.2362962963,
		acceleration: -0.875462962962962
	},
	{
		id: 1403,
		time: 1402,
		velocity: 3.87666666666667,
		power: -4264.02976990882,
		road: 13361.3217592593,
		acceleration: -1.23611111111111
	},
	{
		id: 1404,
		time: 1403,
		velocity: 1.84888888888889,
		power: -3236.38125809947,
		road: 13364.1119444444,
		acceleration: -1.35444444444444
	},
	{
		id: 1405,
		time: 1404,
		velocity: 0.613333333333333,
		power: -1617.97711787216,
		road: 13365.5787962963,
		acceleration: -1.29222222222222
	},
	{
		id: 1406,
		time: 1405,
		velocity: 0,
		power: -237.340142301454,
		road: 13366.0913888889,
		acceleration: -0.616296296296296
	},
	{
		id: 1407,
		time: 1406,
		velocity: 0,
		power: -7.4485567251462,
		road: 13366.1936111111,
		acceleration: -0.204444444444444
	},
	{
		id: 1408,
		time: 1407,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1409,
		time: 1408,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1410,
		time: 1409,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1411,
		time: 1410,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1412,
		time: 1411,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1413,
		time: 1412,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1414,
		time: 1413,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1415,
		time: 1414,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1416,
		time: 1415,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1417,
		time: 1416,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1418,
		time: 1417,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1419,
		time: 1418,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1420,
		time: 1419,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1421,
		time: 1420,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1422,
		time: 1421,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1423,
		time: 1422,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1424,
		time: 1423,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1425,
		time: 1424,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1426,
		time: 1425,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1427,
		time: 1426,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1428,
		time: 1427,
		velocity: 0,
		power: 0,
		road: 13366.1936111111,
		acceleration: 0
	},
	{
		id: 1429,
		time: 1428,
		velocity: 0,
		power: 86.6424440286635,
		road: 13366.3779166667,
		acceleration: 0.368611111111111
	},
	{
		id: 1430,
		time: 1429,
		velocity: 1.10583333333333,
		power: 770.892515256055,
		road: 13367.1858796296,
		acceleration: 0.878703703703704
	},
	{
		id: 1431,
		time: 1430,
		velocity: 2.63611111111111,
		power: 3616.7216134758,
		road: 13369.2789351852,
		acceleration: 1.69148148148148
	},
	{
		id: 1432,
		time: 1431,
		velocity: 5.07444444444444,
		power: 5687.03656398982,
		road: 13372.9630555556,
		acceleration: 1.49064814814815
	},
	{
		id: 1433,
		time: 1432,
		velocity: 5.57777777777778,
		power: 5777.64667167022,
		road: 13377.9335185185,
		acceleration: 1.08203703703704
	},
	{
		id: 1434,
		time: 1433,
		velocity: 5.88222222222222,
		power: 3921.80532105702,
		road: 13383.7278703704,
		acceleration: 0.565740740740741
	},
	{
		id: 1435,
		time: 1434,
		velocity: 6.77166666666667,
		power: 6252.9094383053,
		road: 13390.2347222222,
		acceleration: 0.85925925925926
	},
	{
		id: 1436,
		time: 1435,
		velocity: 8.15555555555556,
		power: 9658.35817355914,
		road: 13397.7654166667,
		acceleration: 1.18842592592593
	},
	{
		id: 1437,
		time: 1436,
		velocity: 9.4475,
		power: 10388.4255969095,
		road: 13406.4346759259,
		acceleration: 1.0887037037037
	},
	{
		id: 1438,
		time: 1437,
		velocity: 10.0377777777778,
		power: 7032.59046027089,
		road: 13415.9471296296,
		acceleration: 0.597685185185185
	},
	{
		id: 1439,
		time: 1438,
		velocity: 9.94861111111111,
		power: 4014.32653374816,
		road: 13425.8790277778,
		acceleration: 0.241203703703704
	},
	{
		id: 1440,
		time: 1439,
		velocity: 10.1711111111111,
		power: 2120.36481643361,
		road: 13435.9497222222,
		acceleration: 0.0363888888888884
	},
	{
		id: 1441,
		time: 1440,
		velocity: 10.1469444444444,
		power: 1767.36112148646,
		road: 13446.0381481482,
		acceleration: -0.000925925925926663
	},
	{
		id: 1442,
		time: 1441,
		velocity: 9.94583333333333,
		power: 104.166478531491,
		road: 13456.0396759259,
		acceleration: -0.172870370370369
	},
	{
		id: 1443,
		time: 1442,
		velocity: 9.6525,
		power: -538.1220885992,
		road: 13465.8351851852,
		acceleration: -0.239166666666668
	},
	{
		id: 1444,
		time: 1443,
		velocity: 9.42944444444444,
		power: 1354.72070088582,
		road: 13475.4947222222,
		acceleration: -0.0327777777777776
	},
	{
		id: 1445,
		time: 1444,
		velocity: 9.8475,
		power: 3803.03680098229,
		road: 13485.2518981482,
		acceleration: 0.228055555555557
	},
	{
		id: 1446,
		time: 1445,
		velocity: 10.3366666666667,
		power: 6558.87158002345,
		road: 13495.3706481482,
		acceleration: 0.495092592592592
	},
	{
		id: 1447,
		time: 1446,
		velocity: 10.9147222222222,
		power: 8661.58796696746,
		road: 13506.065787037,
		acceleration: 0.657685185185187
	},
	{
		id: 1448,
		time: 1447,
		velocity: 11.8205555555556,
		power: 9842.37149468375,
		road: 13517.4431481482,
		acceleration: 0.706759259259258
	},
	{
		id: 1449,
		time: 1448,
		velocity: 12.4569444444444,
		power: 10375.1862125055,
		road: 13529.5193518519,
		acceleration: 0.690925925925926
	},
	{
		id: 1450,
		time: 1449,
		velocity: 12.9875,
		power: 9188.36498350163,
		road: 13542.2112037037,
		acceleration: 0.54037037037037
	},
	{
		id: 1451,
		time: 1450,
		velocity: 13.4416666666667,
		power: 8973.17476099228,
		road: 13555.4162962963,
		acceleration: 0.486111111111112
	},
	{
		id: 1452,
		time: 1451,
		velocity: 13.9152777777778,
		power: 7089.52435844599,
		road: 13569.0214351852,
		acceleration: 0.313981481481482
	},
	{
		id: 1453,
		time: 1452,
		velocity: 13.9294444444444,
		power: 3276.90185176778,
		road: 13582.7910185185,
		acceleration: 0.0149074074074065
	},
	{
		id: 1454,
		time: 1453,
		velocity: 13.4863888888889,
		power: 989.236332825791,
		road: 13596.4892592593,
		acceleration: -0.157592592592593
	},
	{
		id: 1455,
		time: 1454,
		velocity: 13.4425,
		power: 625.096165053503,
		road: 13610.0175925926,
		acceleration: -0.182222222222222
	},
	{
		id: 1456,
		time: 1455,
		velocity: 13.3827777777778,
		power: 1838.48732590652,
		road: 13623.4124074074,
		acceleration: -0.0848148148148162
	},
	{
		id: 1457,
		time: 1456,
		velocity: 13.2319444444444,
		power: 2053.79185299909,
		road: 13636.7318518519,
		acceleration: -0.0659259259259262
	},
	{
		id: 1458,
		time: 1457,
		velocity: 13.2447222222222,
		power: 3498.95955876296,
		road: 13650.0423611111,
		acceleration: 0.0480555555555569
	},
	{
		id: 1459,
		time: 1458,
		velocity: 13.5269444444444,
		power: 8672.75520160876,
		road: 13663.5964814815,
		acceleration: 0.439166666666665
	},
	{
		id: 1460,
		time: 1459,
		velocity: 14.5494444444444,
		power: 15208.1103751174,
		road: 13677.8096296296,
		acceleration: 0.87888888888889
	},
	{
		id: 1461,
		time: 1460,
		velocity: 15.8813888888889,
		power: 19061.9950339762,
		road: 13692.990462963,
		acceleration: 1.05648148148148
	},
	{
		id: 1462,
		time: 1461,
		velocity: 16.6963888888889,
		power: 15318.9809701596,
		road: 13709.0614814815,
		acceleration: 0.723888888888888
	},
	{
		id: 1463,
		time: 1462,
		velocity: 16.7211111111111,
		power: 8500.60705861363,
		road: 13725.6218055556,
		acceleration: 0.254722222222224
	},
	{
		id: 1464,
		time: 1463,
		velocity: 16.6455555555556,
		power: 4496.86060991705,
		road: 13742.3081944445,
		acceleration: -0.00259259259259181
	},
	{
		id: 1465,
		time: 1464,
		velocity: 16.6886111111111,
		power: 7760.53140922538,
		road: 13759.0918981482,
		acceleration: 0.197222222222223
	},
	{
		id: 1466,
		time: 1465,
		velocity: 17.3127777777778,
		power: 8033.63746257549,
		road: 13776.0764351852,
		acceleration: 0.204444444444441
	},
	{
		id: 1467,
		time: 1466,
		velocity: 17.2588888888889,
		power: 6485.29564415889,
		road: 13793.2144907407,
		acceleration: 0.102592592592597
	},
	{
		id: 1468,
		time: 1467,
		velocity: 16.9963888888889,
		power: 109.000789393133,
		road: 13810.2615740741,
		acceleration: -0.28453703703704
	},
	{
		id: 1469,
		time: 1468,
		velocity: 16.4591666666667,
		power: -1823.79479278665,
		road: 13826.9669444445,
		acceleration: -0.398888888888887
	},
	{
		id: 1470,
		time: 1469,
		velocity: 16.0622222222222,
		power: -2578.08838778417,
		road: 13843.2516203704,
		acceleration: -0.442499999999999
	},
	{
		id: 1471,
		time: 1470,
		velocity: 15.6688888888889,
		power: -3757.00967956558,
		road: 13859.0565740741,
		acceleration: -0.516944444444446
	},
	{
		id: 1472,
		time: 1471,
		velocity: 14.9083333333333,
		power: -9615.01976180143,
		road: 13874.1416203704,
		acceleration: -0.92287037037037
	},
	{
		id: 1473,
		time: 1472,
		velocity: 13.2936111111111,
		power: -12446.4505260741,
		road: 13888.181712963,
		acceleration: -1.16703703703704
	},
	{
		id: 1474,
		time: 1473,
		velocity: 12.1677777777778,
		power: -16804.5557210858,
		road: 13900.833287037,
		acceleration: -1.61
	},
	{
		id: 1475,
		time: 1474,
		velocity: 10.0783333333333,
		power: -14086.0934798165,
		road: 13911.9147222222,
		acceleration: -1.53027777777778
	},
	{
		id: 1476,
		time: 1475,
		velocity: 8.70277777777778,
		power: -13898.4904801766,
		road: 13921.3703240741,
		acceleration: -1.72138888888889
	},
	{
		id: 1477,
		time: 1476,
		velocity: 7.00361111111111,
		power: -10156.310597478,
		road: 13929.2027777778,
		acceleration: -1.52490740740741
	},
	{
		id: 1478,
		time: 1477,
		velocity: 5.50361111111111,
		power: -6894.53239972404,
		road: 13935.6335648148,
		acceleration: -1.27842592592593
	},
	{
		id: 1479,
		time: 1478,
		velocity: 4.8675,
		power: -6391.36992011164,
		road: 13940.68875,
		acceleration: -1.47277777777778
	},
	{
		id: 1480,
		time: 1479,
		velocity: 2.58527777777778,
		power: -4411.70671507304,
		road: 13944.2958796296,
		acceleration: -1.42333333333333
	},
	{
		id: 1481,
		time: 1480,
		velocity: 1.23361111111111,
		power: -2950.0110390766,
		road: 13946.3800925926,
		acceleration: -1.6225
	},
	{
		id: 1482,
		time: 1481,
		velocity: 0,
		power: -585.663754999141,
		road: 13947.2221759259,
		acceleration: -0.861759259259259
	},
	{
		id: 1483,
		time: 1482,
		velocity: 0,
		power: -55.2541630766732,
		road: 13947.4277777778,
		acceleration: -0.411203703703704
	},
	{
		id: 1484,
		time: 1483,
		velocity: 0,
		power: 0,
		road: 13947.4277777778,
		acceleration: 0
	},
	{
		id: 1485,
		time: 1484,
		velocity: 0,
		power: 83.5215865807919,
		road: 13947.6082407408,
		acceleration: 0.360925925925926
	},
	{
		id: 1486,
		time: 1485,
		velocity: 1.08277777777778,
		power: 1348.60598585071,
		road: 13948.6130092593,
		acceleration: 1.28768518518519
	},
	{
		id: 1487,
		time: 1486,
		velocity: 3.86305555555556,
		power: 3339.37654592966,
		road: 13950.949537037,
		acceleration: 1.37583333333333
	},
	{
		id: 1488,
		time: 1487,
		velocity: 4.1275,
		power: 3883.21997554997,
		road: 13954.4851851852,
		acceleration: 1.02240740740741
	},
	{
		id: 1489,
		time: 1488,
		velocity: 4.15,
		power: 222.830961843179,
		road: 13958.4931018519,
		acceleration: -0.0778703703703703
	},
	{
		id: 1490,
		time: 1489,
		velocity: 3.62944444444444,
		power: -460.764885177899,
		road: 13962.3310185185,
		acceleration: -0.26212962962963
	},
	{
		id: 1491,
		time: 1490,
		velocity: 3.34111111111111,
		power: -733.922352537357,
		road: 13965.8611574074,
		acceleration: -0.353425925925926
	},
	{
		id: 1492,
		time: 1491,
		velocity: 3.08972222222222,
		power: -218.835305725654,
		road: 13969.1124537037,
		acceleration: -0.204259259259259
	},
	{
		id: 1493,
		time: 1492,
		velocity: 3.01666666666667,
		power: -513.177150698486,
		road: 13972.1050462963,
		acceleration: -0.313148148148148
	},
	{
		id: 1494,
		time: 1493,
		velocity: 2.40166666666667,
		power: -478.294636601132,
		road: 13974.7811574074,
		acceleration: -0.319814814814814
	},
	{
		id: 1495,
		time: 1494,
		velocity: 2.13027777777778,
		power: -690.522492429545,
		road: 13977.0733796296,
		acceleration: -0.447962962962963
	},
	{
		id: 1496,
		time: 1495,
		velocity: 1.67277777777778,
		power: -310.331537159033,
		road: 13978.9915740741,
		acceleration: -0.300092592592593
	},
	{
		id: 1497,
		time: 1496,
		velocity: 1.50138888888889,
		power: -386.506038866767,
		road: 13980.5658333333,
		acceleration: -0.387777777777778
	},
	{
		id: 1498,
		time: 1497,
		velocity: 0.966944444444444,
		power: -448.406980412183,
		road: 13981.6674074074,
		acceleration: -0.557592592592593
	},
	{
		id: 1499,
		time: 1498,
		velocity: 0,
		power: -202.251129036915,
		road: 13982.2399537037,
		acceleration: -0.500462962962963
	},
	{
		id: 1500,
		time: 1499,
		velocity: 0,
		power: -29.7388570337882,
		road: 13982.4011111111,
		acceleration: -0.322314814814815
	},
	{
		id: 1501,
		time: 1500,
		velocity: 0,
		power: 0,
		road: 13982.4011111111,
		acceleration: 0
	},
	{
		id: 1502,
		time: 1501,
		velocity: 0,
		power: 0,
		road: 13982.4011111111,
		acceleration: 0
	},
	{
		id: 1503,
		time: 1502,
		velocity: 0,
		power: 0,
		road: 13982.4011111111,
		acceleration: 0
	},
	{
		id: 1504,
		time: 1503,
		velocity: 0,
		power: 79.8358811497389,
		road: 13982.5769444445,
		acceleration: 0.351666666666667
	},
	{
		id: 1505,
		time: 1504,
		velocity: 1.055,
		power: 268.896778400071,
		road: 13983.1240740741,
		acceleration: 0.390925925925926
	},
	{
		id: 1506,
		time: 1505,
		velocity: 1.17277777777778,
		power: 539.226366497731,
		road: 13984.095462963,
		acceleration: 0.457592592592592
	},
	{
		id: 1507,
		time: 1506,
		velocity: 1.37277777777778,
		power: 280.003779986585,
		road: 13985.3492592593,
		acceleration: 0.107222222222223
	},
	{
		id: 1508,
		time: 1507,
		velocity: 1.37666666666667,
		power: 46.0128477311352,
		road: 13986.611712963,
		acceleration: -0.0899074074074075
	},
	{
		id: 1509,
		time: 1508,
		velocity: 0.903055555555556,
		power: -308.848684004818,
		road: 13987.6004166667,
		acceleration: -0.457592592592593
	},
	{
		id: 1510,
		time: 1509,
		velocity: 0,
		power: -166.496281125498,
		road: 13988.1308796296,
		acceleration: -0.458888888888889
	},
	{
		id: 1511,
		time: 1510,
		velocity: 0,
		power: -24.737332179987,
		road: 13988.2813888889,
		acceleration: -0.301018518518519
	},
	{
		id: 1512,
		time: 1511,
		velocity: 0,
		power: 0,
		road: 13988.2813888889,
		acceleration: 0
	},
	{
		id: 1513,
		time: 1512,
		velocity: 0,
		power: 0,
		road: 13988.2813888889,
		acceleration: 0
	},
	{
		id: 1514,
		time: 1513,
		velocity: 0,
		power: 0,
		road: 13988.2813888889,
		acceleration: 0
	},
	{
		id: 1515,
		time: 1514,
		velocity: 0,
		power: 0,
		road: 13988.2813888889,
		acceleration: 0
	},
	{
		id: 1516,
		time: 1515,
		velocity: 0,
		power: 70.6333905595599,
		road: 13988.4451851852,
		acceleration: 0.327592592592593
	},
	{
		id: 1517,
		time: 1516,
		velocity: 0.982777777777778,
		power: 472.665475018404,
		road: 13989.0935185185,
		acceleration: 0.641481481481481
	},
	{
		id: 1518,
		time: 1517,
		velocity: 1.92444444444444,
		power: 855.670546873342,
		road: 13990.3558796296,
		acceleration: 0.586574074074074
	},
	{
		id: 1519,
		time: 1518,
		velocity: 1.75972222222222,
		power: 522.748984593858,
		road: 13992.0133333333,
		acceleration: 0.203611111111111
	},
	{
		id: 1520,
		time: 1519,
		velocity: 1.59361111111111,
		power: -79.7047777219839,
		road: 13993.6829166667,
		acceleration: -0.179351851851852
	},
	{
		id: 1521,
		time: 1520,
		velocity: 1.38638888888889,
		power: -361.041805519223,
		road: 13995.0603240741,
		acceleration: -0.405
	},
	{
		id: 1522,
		time: 1521,
		velocity: 0.544722222222222,
		power: -347.539109816507,
		road: 13995.9696296296,
		acceleration: -0.531203703703704
	},
	{
		id: 1523,
		time: 1522,
		velocity: 0,
		power: -130.794654023708,
		road: 13996.3822685185,
		acceleration: -0.46212962962963
	},
	{
		id: 1524,
		time: 1523,
		velocity: 0,
		power: -4.64826444119558,
		road: 13996.4730555556,
		acceleration: -0.181574074074074
	},
	{
		id: 1525,
		time: 1524,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1526,
		time: 1525,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1527,
		time: 1526,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1528,
		time: 1527,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1529,
		time: 1528,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1530,
		time: 1529,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1531,
		time: 1530,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1532,
		time: 1531,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1533,
		time: 1532,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1534,
		time: 1533,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1535,
		time: 1534,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1536,
		time: 1535,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1537,
		time: 1536,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1538,
		time: 1537,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1539,
		time: 1538,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1540,
		time: 1539,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1541,
		time: 1540,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1542,
		time: 1541,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1543,
		time: 1542,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1544,
		time: 1543,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1545,
		time: 1544,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1546,
		time: 1545,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1547,
		time: 1546,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1548,
		time: 1547,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1549,
		time: 1548,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1550,
		time: 1549,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1551,
		time: 1550,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1552,
		time: 1551,
		velocity: 0,
		power: 0,
		road: 13996.4730555556,
		acceleration: 0
	},
	{
		id: 1553,
		time: 1552,
		velocity: 0,
		power: 76.8026946183323,
		road: 13996.645,
		acceleration: 0.343888888888889
	},
	{
		id: 1554,
		time: 1553,
		velocity: 1.03166666666667,
		power: 1004.8153968174,
		road: 13997.5262962963,
		acceleration: 1.07481481481481
	},
	{
		id: 1555,
		time: 1554,
		velocity: 3.22444444444444,
		power: 3161.3854705435,
		road: 13999.6606481482,
		acceleration: 1.4312962962963
	},
	{
		id: 1556,
		time: 1555,
		velocity: 4.29388888888889,
		power: 5212.49983761504,
		road: 14003.2155555556,
		acceleration: 1.40981481481481
	},
	{
		id: 1557,
		time: 1556,
		velocity: 5.26111111111111,
		power: 4477.21340852287,
		road: 14007.9076851852,
		acceleration: 0.86462962962963
	},
	{
		id: 1558,
		time: 1557,
		velocity: 5.81833333333333,
		power: 3532.74784002571,
		road: 14013.3046296296,
		acceleration: 0.545
	},
	{
		id: 1559,
		time: 1558,
		velocity: 5.92888888888889,
		power: 1879.22587965795,
		road: 14019.0724074074,
		acceleration: 0.196666666666668
	},
	{
		id: 1560,
		time: 1559,
		velocity: 5.85111111111111,
		power: 190.223609682768,
		road: 14024.8825462963,
		acceleration: -0.111944444444446
	},
	{
		id: 1561,
		time: 1560,
		velocity: 5.4825,
		power: -444.128435038765,
		road: 14030.5226388889,
		acceleration: -0.228148148148148
	},
	{
		id: 1562,
		time: 1561,
		velocity: 5.24444444444444,
		power: -806.173743605192,
		road: 14035.8979166667,
		acceleration: -0.301481481481481
	},
	{
		id: 1563,
		time: 1562,
		velocity: 4.94666666666667,
		power: -588.064368845367,
		road: 14040.9906944445,
		acceleration: -0.263518518518519
	},
	{
		id: 1564,
		time: 1563,
		velocity: 4.69194444444444,
		power: -355.584501326988,
		road: 14045.8428240741,
		acceleration: -0.217777777777777
	},
	{
		id: 1565,
		time: 1564,
		velocity: 4.59111111111111,
		power: -777.518764775875,
		road: 14050.4271759259,
		acceleration: -0.317777777777778
	},
	{
		id: 1566,
		time: 1565,
		velocity: 3.99333333333333,
		power: -991.725359944878,
		road: 14054.6605555556,
		acceleration: -0.384166666666667
	},
	{
		id: 1567,
		time: 1566,
		velocity: 3.53944444444444,
		power: -1593.55894368284,
		road: 14058.4103703704,
		acceleration: -0.582962962962962
	},
	{
		id: 1568,
		time: 1567,
		velocity: 2.84222222222222,
		power: -1091.27012274258,
		road: 14061.6231481482,
		acceleration: -0.491111111111111
	},
	{
		id: 1569,
		time: 1568,
		velocity: 2.52,
		power: -901.062984990612,
		road: 14064.350462963,
		acceleration: -0.479814814814815
	},
	{
		id: 1570,
		time: 1569,
		velocity: 2.1,
		power: -980.520637284332,
		road: 14066.5363425926,
		acceleration: -0.603055555555555
	},
	{
		id: 1571,
		time: 1570,
		velocity: 1.03305555555556,
		power: -594.38940891835,
		road: 14068.1636111111,
		acceleration: -0.514166666666667
	},
	{
		id: 1572,
		time: 1571,
		velocity: 0.9775,
		power: -291.970451732853,
		road: 14069.3386111111,
		acceleration: -0.39037037037037
	},
	{
		id: 1573,
		time: 1572,
		velocity: 0.928888888888889,
		power: 122.378880136666,
		road: 14070.3201851852,
		acceleration: 0.00351851851851859
	},
	{
		id: 1574,
		time: 1573,
		velocity: 1.04361111111111,
		power: 145.391778588992,
		road: 14071.3164814815,
		acceleration: 0.0259259259259259
	},
	{
		id: 1575,
		time: 1574,
		velocity: 1.05527777777778,
		power: 86.1729900403112,
		road: 14072.3075925926,
		acceleration: -0.0362962962962964
	},
	{
		id: 1576,
		time: 1575,
		velocity: 0.82,
		power: -166.622370859953,
		road: 14073.1066203704,
		acceleration: -0.34787037037037
	},
	{
		id: 1577,
		time: 1576,
		velocity: 0,
		power: -95.4070658153451,
		road: 14073.5558333333,
		acceleration: -0.351759259259259
	},
	{
		id: 1578,
		time: 1577,
		velocity: 0,
		power: -18.8776947368421,
		road: 14073.6925,
		acceleration: -0.273333333333333
	},
	{
		id: 1579,
		time: 1578,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1580,
		time: 1579,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1581,
		time: 1580,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1582,
		time: 1581,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1583,
		time: 1582,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1584,
		time: 1583,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1585,
		time: 1584,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1586,
		time: 1585,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1587,
		time: 1586,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1588,
		time: 1587,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1589,
		time: 1588,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1590,
		time: 1589,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1591,
		time: 1590,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1592,
		time: 1591,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1593,
		time: 1592,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1594,
		time: 1593,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1595,
		time: 1594,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1596,
		time: 1595,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1597,
		time: 1596,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1598,
		time: 1597,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1599,
		time: 1598,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1600,
		time: 1599,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1601,
		time: 1600,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1602,
		time: 1601,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1603,
		time: 1602,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1604,
		time: 1603,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1605,
		time: 1604,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1606,
		time: 1605,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1607,
		time: 1606,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1608,
		time: 1607,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1609,
		time: 1608,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1610,
		time: 1609,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1611,
		time: 1610,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1612,
		time: 1611,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1613,
		time: 1612,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1614,
		time: 1613,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1615,
		time: 1614,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1616,
		time: 1615,
		velocity: 0,
		power: 0,
		road: 14073.6925,
		acceleration: 0
	},
	{
		id: 1617,
		time: 1616,
		velocity: 0,
		power: 2.74671629437754,
		road: 14073.7102777778,
		acceleration: 0.0355555555555556
	},
	{
		id: 1618,
		time: 1617,
		velocity: 0.106666666666667,
		power: 328.236417503681,
		road: 14074.1125462963,
		acceleration: 0.733425925925926
	},
	{
		id: 1619,
		time: 1618,
		velocity: 2.20027777777778,
		power: 1671.18127992429,
		road: 14075.4675925926,
		acceleration: 1.17212962962963
	},
	{
		id: 1620,
		time: 1619,
		velocity: 3.51638888888889,
		power: 5622.62675050566,
		road: 14078.3649074074,
		acceleration: 1.91240740740741
	},
	{
		id: 1621,
		time: 1620,
		velocity: 5.84388888888889,
		power: 7879.81450884485,
		road: 14083.0362962963,
		acceleration: 1.63574074074074
	},
	{
		id: 1622,
		time: 1621,
		velocity: 7.1075,
		power: 7720.76323678212,
		road: 14089.1189351852,
		acceleration: 1.18675925925926
	},
	{
		id: 1623,
		time: 1622,
		velocity: 7.07666666666667,
		power: 4699.39149716439,
		road: 14096.0728703704,
		acceleration: 0.555833333333335
	},
	{
		id: 1624,
		time: 1623,
		velocity: 7.51138888888889,
		power: 2311.79725991307,
		road: 14103.3919444445,
		acceleration: 0.174444444444444
	},
	{
		id: 1625,
		time: 1624,
		velocity: 7.63083333333333,
		power: 758.338590494743,
		road: 14110.7731944445,
		acceleration: -0.0500925925925939
	},
	{
		id: 1626,
		time: 1625,
		velocity: 6.92638888888889,
		power: -999.061471784263,
		road: 14117.9781944445,
		acceleration: -0.302407407407408
	},
	{
		id: 1627,
		time: 1626,
		velocity: 6.60416666666667,
		power: -2966.57618554877,
		road: 14124.7242592593,
		acceleration: -0.615462962962963
	},
	{
		id: 1628,
		time: 1627,
		velocity: 5.78444444444444,
		power: -4205.72527334468,
		road: 14130.7197685185,
		acceleration: -0.885648148148147
	},
	{
		id: 1629,
		time: 1628,
		velocity: 4.26944444444444,
		power: -6822.49882273062,
		road: 14135.4418055556,
		acceleration: -1.6612962962963
	},
	{
		id: 1630,
		time: 1629,
		velocity: 1.62027777777778,
		power: -4987.43128717383,
		road: 14138.3691203704,
		acceleration: -1.92814814814815
	},
	{
		id: 1631,
		time: 1630,
		velocity: 0,
		power: -1536.13215389127,
		road: 14139.620787037,
		acceleration: -1.42314814814815
	},
	{
		id: 1632,
		time: 1631,
		velocity: 0,
		power: -105.547263271605,
		road: 14139.8908333333,
		acceleration: -0.540092592592593
	},
	{
		id: 1633,
		time: 1632,
		velocity: 0,
		power: 0,
		road: 14139.8908333333,
		acceleration: 0
	},
	{
		id: 1634,
		time: 1633,
		velocity: 0,
		power: 97.905641177584,
		road: 14140.0884722222,
		acceleration: 0.395277777777778
	},
	{
		id: 1635,
		time: 1634,
		velocity: 1.18583333333333,
		power: 664.038804550652,
		road: 14140.86875,
		acceleration: 0.77
	},
	{
		id: 1636,
		time: 1635,
		velocity: 2.31,
		power: 1862.32149035972,
		road: 14142.5525925926,
		acceleration: 1.03712962962963
	},
	{
		id: 1637,
		time: 1636,
		velocity: 3.11138888888889,
		power: 1545.77524320859,
		road: 14145.0197685185,
		acceleration: 0.529537037037037
	},
	{
		id: 1638,
		time: 1637,
		velocity: 2.77444444444444,
		power: 545.748719383869,
		road: 14147.7896759259,
		acceleration: 0.075925925925926
	},
	{
		id: 1639,
		time: 1638,
		velocity: 2.53777777777778,
		power: -165.555126277387,
		road: 14150.4995833333,
		acceleration: -0.195925925925926
	},
	{
		id: 1640,
		time: 1639,
		velocity: 2.52361111111111,
		power: -170.625102467063,
		road: 14153.0102314815,
		acceleration: -0.202592592592593
	},
	{
		id: 1641,
		time: 1640,
		velocity: 2.16666666666667,
		power: 210.127436102216,
		road: 14155.4006018519,
		acceleration: -0.037962962962963
	},
	{
		id: 1642,
		time: 1641,
		velocity: 2.42388888888889,
		power: 34.066642770783,
		road: 14157.7145370371,
		acceleration: -0.114907407407407
	},
	{
		id: 1643,
		time: 1642,
		velocity: 2.17888888888889,
		power: -509.888935847802,
		road: 14159.7756944445,
		acceleration: -0.390648148148148
	},
	{
		id: 1644,
		time: 1643,
		velocity: 0.994722222222222,
		power: -941.451861157797,
		road: 14161.2375462963,
		acceleration: -0.807962962962963
	},
	{
		id: 1645,
		time: 1644,
		velocity: 0,
		power: -394.041301258969,
		road: 14161.9322685185,
		acceleration: -0.726296296296296
	},
	{
		id: 1646,
		time: 1645,
		velocity: 0,
		power: -32.0474486517219,
		road: 14162.0980555556,
		acceleration: -0.331574074074074
	},
	{
		id: 1647,
		time: 1646,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1648,
		time: 1647,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1649,
		time: 1648,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1650,
		time: 1649,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1651,
		time: 1650,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1652,
		time: 1651,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1653,
		time: 1652,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1654,
		time: 1653,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1655,
		time: 1654,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1656,
		time: 1655,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1657,
		time: 1656,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1658,
		time: 1657,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1659,
		time: 1658,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1660,
		time: 1659,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1661,
		time: 1660,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1662,
		time: 1661,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1663,
		time: 1662,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1664,
		time: 1663,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1665,
		time: 1664,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1666,
		time: 1665,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1667,
		time: 1666,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1668,
		time: 1667,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1669,
		time: 1668,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1670,
		time: 1669,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1671,
		time: 1670,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1672,
		time: 1671,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1673,
		time: 1672,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1674,
		time: 1673,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1675,
		time: 1674,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1676,
		time: 1675,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1677,
		time: 1676,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1678,
		time: 1677,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1679,
		time: 1678,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1680,
		time: 1679,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1681,
		time: 1680,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1682,
		time: 1681,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1683,
		time: 1682,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1684,
		time: 1683,
		velocity: 0,
		power: 0,
		road: 14162.0980555556,
		acceleration: 0
	},
	{
		id: 1685,
		time: 1684,
		velocity: 0,
		power: 67.9797709887749,
		road: 14162.2582407408,
		acceleration: 0.32037037037037
	},
	{
		id: 1686,
		time: 1685,
		velocity: 0.961111111111111,
		power: 851.495356211717,
		road: 14163.0687962963,
		acceleration: 0.98037037037037
	},
	{
		id: 1687,
		time: 1686,
		velocity: 2.94111111111111,
		power: 3184.83692966736,
		road: 14165.1221296296,
		acceleration: 1.50518518518519
	},
	{
		id: 1688,
		time: 1687,
		velocity: 4.51555555555556,
		power: 5031.70868331355,
		road: 14168.61875,
		acceleration: 1.38138888888889
	},
	{
		id: 1689,
		time: 1688,
		velocity: 5.10527777777778,
		power: 5172.03233654177,
		road: 14173.3156944445,
		acceleration: 1.01925925925926
	},
	{
		id: 1690,
		time: 1689,
		velocity: 5.99888888888889,
		power: 3412.92903656697,
		road: 14178.7788425926,
		acceleration: 0.513148148148149
	},
	{
		id: 1691,
		time: 1690,
		velocity: 6.055,
		power: 3554.50235532086,
		road: 14184.7385648148,
		acceleration: 0.479999999999999
	},
	{
		id: 1692,
		time: 1691,
		velocity: 6.54527777777778,
		power: 2218.00135450119,
		road: 14191.0482407408,
		acceleration: 0.219907407407407
	},
	{
		id: 1693,
		time: 1692,
		velocity: 6.65861111111111,
		power: 2048.10208416195,
		road: 14197.5576851852,
		acceleration: 0.17962962962963
	},
	{
		id: 1694,
		time: 1693,
		velocity: 6.59388888888889,
		power: 1331.39443025802,
		road: 14204.1864814815,
		acceleration: 0.0590740740740738
	},
	{
		id: 1695,
		time: 1694,
		velocity: 6.7225,
		power: 1234.57973814219,
		road: 14210.8657407408,
		acceleration: 0.0418518518518525
	},
	{
		id: 1696,
		time: 1695,
		velocity: 6.78416666666667,
		power: 1878.07988672678,
		road: 14217.6351851852,
		acceleration: 0.138518518518519
	},
	{
		id: 1697,
		time: 1696,
		velocity: 7.00944444444445,
		power: 1549.71467489247,
		road: 14224.5152777778,
		acceleration: 0.0827777777777774
	},
	{
		id: 1698,
		time: 1697,
		velocity: 6.97083333333333,
		power: 634.815052860706,
		road: 14231.4081018519,
		acceleration: -0.0573148148148146
	},
	{
		id: 1699,
		time: 1698,
		velocity: 6.61222222222222,
		power: -1209.09863135422,
		road: 14238.1009722222,
		acceleration: -0.342592592592593
	},
	{
		id: 1700,
		time: 1699,
		velocity: 5.98166666666667,
		power: -3307.92415328956,
		road: 14244.2659259259,
		acceleration: -0.713240740740742
	},
	{
		id: 1701,
		time: 1700,
		velocity: 4.83111111111111,
		power: -2756.4614129179,
		road: 14249.737037037,
		acceleration: -0.674444444444445
	},
	{
		id: 1702,
		time: 1701,
		velocity: 4.58888888888889,
		power: -2549.39471944398,
		road: 14254.5202314815,
		acceleration: -0.701388888888888
	},
	{
		id: 1703,
		time: 1702,
		velocity: 3.8775,
		power: -2420.24803192885,
		road: 14258.5696759259,
		acceleration: -0.766111111111111
	},
	{
		id: 1704,
		time: 1703,
		velocity: 2.53277777777778,
		power: -2389.64499280477,
		road: 14261.7768981482,
		acceleration: -0.918333333333333
	},
	{
		id: 1705,
		time: 1704,
		velocity: 1.83388888888889,
		power: -1381.91112425558,
		road: 14264.1531018519,
		acceleration: -0.743703703703704
	},
	{
		id: 1706,
		time: 1705,
		velocity: 1.64638888888889,
		power: -513.211220898843,
		road: 14265.9415277778,
		acceleration: -0.431851851851852
	},
	{
		id: 1707,
		time: 1706,
		velocity: 1.23722222222222,
		power: -332.689992316909,
		road: 14267.3227314815,
		acceleration: -0.382592592592592
	},
	{
		id: 1708,
		time: 1707,
		velocity: 0.686111111111111,
		power: -236.498158355975,
		road: 14268.3240277778,
		acceleration: -0.377222222222222
	},
	{
		id: 1709,
		time: 1708,
		velocity: 0.514722222222222,
		power: -163.626811290518,
		road: 14268.9305092593,
		acceleration: -0.412407407407407
	},
	{
		id: 1710,
		time: 1709,
		velocity: 0,
		power: -27.4010795265529,
		road: 14269.2164351852,
		acceleration: -0.228703703703704
	},
	{
		id: 1711,
		time: 1710,
		velocity: 0,
		power: -3.57954689733593,
		road: 14269.3022222222,
		acceleration: -0.171574074074074
	},
	{
		id: 1712,
		time: 1711,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1713,
		time: 1712,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1714,
		time: 1713,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1715,
		time: 1714,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1716,
		time: 1715,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1717,
		time: 1716,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1718,
		time: 1717,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1719,
		time: 1718,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1720,
		time: 1719,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1721,
		time: 1720,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1722,
		time: 1721,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1723,
		time: 1722,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1724,
		time: 1723,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1725,
		time: 1724,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1726,
		time: 1725,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1727,
		time: 1726,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1728,
		time: 1727,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1729,
		time: 1728,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1730,
		time: 1729,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1731,
		time: 1730,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1732,
		time: 1731,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1733,
		time: 1732,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1734,
		time: 1733,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1735,
		time: 1734,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1736,
		time: 1735,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1737,
		time: 1736,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1738,
		time: 1737,
		velocity: 0,
		power: 0,
		road: 14269.3022222222,
		acceleration: 0
	},
	{
		id: 1739,
		time: 1738,
		velocity: 0,
		power: 363.627554538672,
		road: 14269.7094907408,
		acceleration: 0.814537037037037
	},
	{
		id: 1740,
		time: 1739,
		velocity: 2.44361111111111,
		power: 2349.94929077056,
		road: 14271.2591203704,
		acceleration: 1.47018518518519
	},
	{
		id: 1741,
		time: 1740,
		velocity: 4.41055555555556,
		power: 5202.34790539678,
		road: 14274.3608796296,
		acceleration: 1.63407407407407
	},
	{
		id: 1742,
		time: 1741,
		velocity: 4.90222222222222,
		power: 5274.58149693151,
		road: 14278.8313888889,
		acceleration: 1.10342592592593
	},
	{
		id: 1743,
		time: 1742,
		velocity: 5.75388888888889,
		power: 4457.91054144973,
		road: 14284.2172222222,
		acceleration: 0.727222222222221
	},
	{
		id: 1744,
		time: 1743,
		velocity: 6.59222222222222,
		power: 7900.52795081873,
		road: 14290.5478703704,
		acceleration: 1.16240740740741
	},
	{
		id: 1745,
		time: 1744,
		velocity: 8.38944444444444,
		power: 7454.97840743532,
		road: 14297.9126851852,
		acceleration: 0.905925925925928
	},
	{
		id: 1746,
		time: 1745,
		velocity: 8.47166666666667,
		power: 4792.14028876139,
		road: 14305.9612962963,
		acceleration: 0.461666666666665
	},
	{
		id: 1747,
		time: 1746,
		velocity: 7.97722222222222,
		power: -1629.71920489814,
		road: 14314.0527777778,
		acceleration: -0.375925925925924
	},
	{
		id: 1748,
		time: 1747,
		velocity: 7.26166666666667,
		power: -4754.24158041085,
		road: 14321.5431944445,
		acceleration: -0.826203703703705
	},
	{
		id: 1749,
		time: 1748,
		velocity: 5.99305555555556,
		power: -5642.77064188926,
		road: 14328.0915740741,
		acceleration: -1.05787037037037
	},
	{
		id: 1750,
		time: 1749,
		velocity: 4.80361111111111,
		power: -4459.18391674779,
		road: 14333.6138425926,
		acceleration: -0.994351851851853
	},
	{
		id: 1751,
		time: 1750,
		velocity: 4.27861111111111,
		power: -2438.73590819738,
		road: 14338.2948148148,
		acceleration: -0.68824074074074
	},
	{
		id: 1752,
		time: 1751,
		velocity: 3.92833333333333,
		power: -862.672323383815,
		road: 14342.4538888889,
		acceleration: -0.355555555555555
	},
	{
		id: 1753,
		time: 1752,
		velocity: 3.73694444444444,
		power: -413.590697040103,
		road: 14346.3108333333,
		acceleration: -0.248703703703704
	},
	{
		id: 1754,
		time: 1753,
		velocity: 3.5325,
		power: -251.394464981198,
		road: 14349.939537037,
		acceleration: -0.207777777777777
	},
	{
		id: 1755,
		time: 1754,
		velocity: 3.305,
		power: -1129.77544987339,
		road: 14353.2159722222,
		acceleration: -0.49675925925926
	},
	{
		id: 1756,
		time: 1755,
		velocity: 2.24666666666667,
		power: -1738.29762076483,
		road: 14355.8275925926,
		acceleration: -0.832870370370371
	},
	{
		id: 1757,
		time: 1756,
		velocity: 1.03388888888889,
		power: -1158.89007305621,
		road: 14357.6165740741,
		acceleration: -0.812407407407407
	},
	{
		id: 1758,
		time: 1757,
		velocity: 0.867777777777778,
		power: -439.481011136407,
		road: 14358.7263888889,
		acceleration: -0.545925925925926
	},
	{
		id: 1759,
		time: 1758,
		velocity: 0.608888888888889,
		power: -74.7897492969515,
		road: 14359.4443981482,
		acceleration: -0.237685185185185
	},
	{
		id: 1760,
		time: 1759,
		velocity: 0.320833333333333,
		power: -69.6191899523265,
		road: 14359.8989351852,
		acceleration: -0.289259259259259
	},
	{
		id: 1761,
		time: 1760,
		velocity: 0,
		power: -14.8934076319593,
		road: 14360.1073611111,
		acceleration: -0.202962962962963
	},
	{
		id: 1762,
		time: 1761,
		velocity: 0,
		power: 1.04282090643275,
		road: 14360.1608333333,
		acceleration: -0.106944444444444
	},
	{
		id: 1763,
		time: 1762,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1764,
		time: 1763,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1765,
		time: 1764,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1766,
		time: 1765,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1767,
		time: 1766,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1768,
		time: 1767,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1769,
		time: 1768,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1770,
		time: 1769,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1771,
		time: 1770,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1772,
		time: 1771,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1773,
		time: 1772,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1774,
		time: 1773,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1775,
		time: 1774,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1776,
		time: 1775,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1777,
		time: 1776,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1778,
		time: 1777,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1779,
		time: 1778,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1780,
		time: 1779,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1781,
		time: 1780,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1782,
		time: 1781,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1783,
		time: 1782,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1784,
		time: 1783,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1785,
		time: 1784,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1786,
		time: 1785,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1787,
		time: 1786,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1788,
		time: 1787,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1789,
		time: 1788,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1790,
		time: 1789,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1791,
		time: 1790,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1792,
		time: 1791,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1793,
		time: 1792,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1794,
		time: 1793,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1795,
		time: 1794,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1796,
		time: 1795,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1797,
		time: 1796,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1798,
		time: 1797,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1799,
		time: 1798,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1800,
		time: 1799,
		velocity: 0,
		power: 0,
		road: 14360.1608333333,
		acceleration: 0
	},
	{
		id: 1801,
		time: 1800,
		velocity: 0,
		power: 16.7016875123312,
		road: 14360.2281018519,
		acceleration: 0.134537037037037
	},
	{
		id: 1802,
		time: 1801,
		velocity: 0.403611111111111,
		power: 70.1907646206495,
		road: 14360.4591666667,
		acceleration: 0.193055555555556
	},
	{
		id: 1803,
		time: 1802,
		velocity: 0.579166666666667,
		power: 284.455858294349,
		road: 14361.0002777778,
		acceleration: 0.427037037037037
	},
	{
		id: 1804,
		time: 1803,
		velocity: 1.28111111111111,
		power: 1643.65571184538,
		road: 14362.3383796296,
		acceleration: 1.16694444444444
	},
	{
		id: 1805,
		time: 1804,
		velocity: 3.90444444444444,
		power: 3848.89189494252,
		road: 14364.9660648148,
		acceleration: 1.41222222222222
	},
	{
		id: 1806,
		time: 1805,
		velocity: 4.81583333333333,
		power: 5955.19974448885,
		road: 14369.0073611111,
		acceleration: 1.415
	},
	{
		id: 1807,
		time: 1806,
		velocity: 5.52611111111111,
		power: 4645.60699768693,
		road: 14374.1594444445,
		acceleration: 0.806574074074074
	},
	{
		id: 1808,
		time: 1807,
		velocity: 6.32416666666667,
		power: 6285.73511149429,
		road: 14380.1891203704,
		acceleration: 0.948611111111112
	},
	{
		id: 1809,
		time: 1808,
		velocity: 7.66166666666667,
		power: 6936.61493640957,
		road: 14387.1403240741,
		acceleration: 0.894444444444443
	},
	{
		id: 1810,
		time: 1809,
		velocity: 8.20944444444444,
		power: 6404.7710977908,
		road: 14394.8922222222,
		acceleration: 0.706944444444447
	},
	{
		id: 1811,
		time: 1810,
		velocity: 8.445,
		power: 3770.86876874052,
		road: 14403.1543981482,
		acceleration: 0.31361111111111
	},
	{
		id: 1812,
		time: 1811,
		velocity: 8.6025,
		power: 3078.71777179068,
		road: 14411.6788888889,
		acceleration: 0.21101851851852
	},
	{
		id: 1813,
		time: 1812,
		velocity: 8.8425,
		power: 2253.57037002565,
		road: 14420.3602777778,
		acceleration: 0.102777777777776
	},
	{
		id: 1814,
		time: 1813,
		velocity: 8.75333333333333,
		power: 1594.16154630955,
		road: 14429.1035648148,
		acceleration: 0.0210185185185185
	},
	{
		id: 1815,
		time: 1814,
		velocity: 8.66555555555556,
		power: 604.856162795425,
		road: 14437.8087962963,
		acceleration: -0.0971296296296273
	},
	{
		id: 1816,
		time: 1815,
		velocity: 8.55111111111111,
		power: 107.277644056022,
		road: 14446.3875925926,
		acceleration: -0.155740740740743
	},
	{
		id: 1817,
		time: 1816,
		velocity: 8.28611111111111,
		power: -451.586446544369,
		road: 14454.776712963,
		acceleration: -0.223611111111111
	},
	{
		id: 1818,
		time: 1817,
		velocity: 7.99472222222222,
		power: -170.57325630874,
		road: 14462.9605092593,
		acceleration: -0.187037037037037
	},
	{
		id: 1819,
		time: 1818,
		velocity: 7.99,
		power: -227.703240056241,
		road: 14470.9541203704,
		acceleration: -0.193333333333332
	},
	{
		id: 1820,
		time: 1819,
		velocity: 7.70611111111111,
		power: -812.757612901504,
		road: 14478.7153703704,
		acceleration: -0.27138888888889
	},
	{
		id: 1821,
		time: 1820,
		velocity: 7.18055555555556,
		power: -2027.68143228401,
		road: 14486.1178240741,
		acceleration: -0.446203703703703
	},
	{
		id: 1822,
		time: 1821,
		velocity: 6.65138888888889,
		power: -3070.83991356034,
		road: 14492.9850925926,
		acceleration: -0.624166666666667
	},
	{
		id: 1823,
		time: 1822,
		velocity: 5.83361111111111,
		power: -3015.96246265718,
		road: 14499.2108796296,
		acceleration: -0.658796296296297
	},
	{
		id: 1824,
		time: 1823,
		velocity: 5.20416666666667,
		power: -1845.65082573244,
		road: 14504.8627777778,
		acceleration: -0.488981481481481
	},
	{
		id: 1825,
		time: 1824,
		velocity: 5.18444444444444,
		power: -570.732059755516,
		road: 14510.1417592593,
		acceleration: -0.256851851851851
	},
	{
		id: 1826,
		time: 1825,
		velocity: 5.06305555555556,
		power: -261.384722251642,
		road: 14515.194212963,
		acceleration: -0.196203703703705
	},
	{
		id: 1827,
		time: 1826,
		velocity: 4.61555555555556,
		power: -865.547095288404,
		road: 14519.983287037,
		acceleration: -0.330555555555555
	},
	{
		id: 1828,
		time: 1827,
		velocity: 4.19277777777778,
		power: -1699.16838475736,
		road: 14524.3323611111,
		acceleration: -0.549444444444445
	},
	{
		id: 1829,
		time: 1828,
		velocity: 3.41472222222222,
		power: -932.068389033614,
		road: 14528.2122685185,
		acceleration: -0.388888888888889
	},
	{
		id: 1830,
		time: 1829,
		velocity: 3.44888888888889,
		power: -552.021573962099,
		road: 14531.748287037,
		acceleration: -0.298888888888889
	},
	{
		id: 1831,
		time: 1830,
		velocity: 3.29611111111111,
		power: 274.716579408834,
		road: 14535.1110185185,
		acceleration: -0.0476851851851854
	},
	{
		id: 1832,
		time: 1831,
		velocity: 3.27166666666667,
		power: 134.236428149871,
		road: 14538.4046296296,
		acceleration: -0.0905555555555555
	},
	{
		id: 1833,
		time: 1832,
		velocity: 3.17722222222222,
		power: 203.33084154737,
		road: 14541.6196759259,
		acceleration: -0.0665740740740741
	},
	{
		id: 1834,
		time: 1833,
		velocity: 3.09638888888889,
		power: 138.931358274536,
		road: 14544.758287037,
		acceleration: -0.0862962962962954
	},
	{
		id: 1835,
		time: 1834,
		velocity: 3.01277777777778,
		power: 124.22796990639,
		road: 14547.8088888889,
		acceleration: -0.0897222222222229
	},
	{
		id: 1836,
		time: 1835,
		velocity: 2.90805555555556,
		power: 217.392459064102,
		road: 14550.7868981482,
		acceleration: -0.0554629629629626
	},
	{
		id: 1837,
		time: 1836,
		velocity: 2.93,
		power: -115.273229345854,
		road: 14553.6499537037,
		acceleration: -0.174444444444445
	},
	{
		id: 1838,
		time: 1837,
		velocity: 2.48944444444444,
		power: -340.418445119217,
		road: 14556.2922222222,
		acceleration: -0.26712962962963
	},
	{
		id: 1839,
		time: 1838,
		velocity: 2.10666666666667,
		power: -493.571390984344,
		road: 14558.6241203704,
		acceleration: -0.353611111111111
	},
	{
		id: 1840,
		time: 1839,
		velocity: 1.86916666666667,
		power: -179.04119597846,
		road: 14560.6681481482,
		acceleration: -0.222129629629629
	},
	{
		id: 1841,
		time: 1840,
		velocity: 1.82305555555556,
		power: 29.7674440786935,
		road: 14562.5447685185,
		acceleration: -0.112685185185185
	},
	{
		id: 1842,
		time: 1841,
		velocity: 1.76861111111111,
		power: 127.667194006212,
		road: 14564.337962963,
		acceleration: -0.0541666666666669
	},
	{
		id: 1843,
		time: 1842,
		velocity: 1.70666666666667,
		power: 131.919629870512,
		road: 14566.0794444445,
		acceleration: -0.0492592592592593
	},
	{
		id: 1844,
		time: 1843,
		velocity: 1.67527777777778,
		power: 103.48122369277,
		road: 14567.7641666667,
		acceleration: -0.0642592592592592
	},
	{
		id: 1845,
		time: 1844,
		velocity: 1.57583333333333,
		power: 130.178757363823,
		road: 14569.3943981482,
		acceleration: -0.0447222222222221
	},
	{
		id: 1846,
		time: 1845,
		velocity: 1.5725,
		power: 86.0551993460105,
		road: 14570.966712963,
		acceleration: -0.0711111111111109
	},
	{
		id: 1847,
		time: 1846,
		velocity: 1.46194444444444,
		power: -479.398334233016,
		road: 14572.2408333333,
		acceleration: -0.525277777777778
	},
	{
		id: 1848,
		time: 1847,
		velocity: 0,
		power: -281.498025647801,
		road: 14572.9902314815,
		acceleration: -0.524166666666667
	},
	{
		id: 1849,
		time: 1848,
		velocity: 0,
		power: -83.0503280864198,
		road: 14573.2338888889,
		acceleration: -0.487314814814815
	},
	{
		id: 1850,
		time: 1849,
		velocity: 0,
		power: 0,
		road: 14573.2338888889,
		acceleration: 0
	},
	{
		id: 1851,
		time: 1850,
		velocity: 0,
		power: 0,
		road: 14573.2338888889,
		acceleration: 0
	},
	{
		id: 1852,
		time: 1851,
		velocity: 0,
		power: 0,
		road: 14573.2338888889,
		acceleration: 0
	},
	{
		id: 1853,
		time: 1852,
		velocity: 0,
		power: 0,
		road: 14573.2338888889,
		acceleration: 0
	},
	{
		id: 1854,
		time: 1853,
		velocity: 0,
		power: 0,
		road: 14573.2338888889,
		acceleration: 0
	},
	{
		id: 1855,
		time: 1854,
		velocity: 0,
		power: 11.3004521252058,
		road: 14573.2855555556,
		acceleration: 0.103333333333333
	},
	{
		id: 1856,
		time: 1855,
		velocity: 0.31,
		power: 428.527113933468,
		road: 14573.7812037037,
		acceleration: 0.78462962962963
	},
	{
		id: 1857,
		time: 1856,
		velocity: 2.35388888888889,
		power: 1011.86237272055,
		road: 14575.031712963,
		acceleration: 0.725092592592593
	},
	{
		id: 1858,
		time: 1857,
		velocity: 2.17527777777778,
		power: 1385.50617493406,
		road: 14576.9589814815,
		acceleration: 0.628425925925926
	},
	{
		id: 1859,
		time: 1858,
		velocity: 2.19527777777778,
		power: 20.8919886518262,
		road: 14579.140462963,
		acceleration: -0.12
	},
	{
		id: 1860,
		time: 1859,
		velocity: 1.99388888888889,
		power: -674.258809788857,
		road: 14581.0067592593,
		acceleration: -0.510370370370371
	},
	{
		id: 1861,
		time: 1860,
		velocity: 0.644166666666667,
		power: -712.282293809607,
		road: 14582.2519907408,
		acceleration: -0.731759259259259
	},
	{
		id: 1862,
		time: 1861,
		velocity: 0,
		power: -278.335835352574,
		road: 14582.7990277778,
		acceleration: -0.66462962962963
	},
	{
		id: 1863,
		time: 1862,
		velocity: 0,
		power: -8.8683668128655,
		road: 14582.9063888889,
		acceleration: -0.214722222222222
	},
	{
		id: 1864,
		time: 1863,
		velocity: 0,
		power: 0,
		road: 14582.9063888889,
		acceleration: 0
	},
	{
		id: 1865,
		time: 1864,
		velocity: 0,
		power: 0,
		road: 14582.9063888889,
		acceleration: 0
	},
	{
		id: 1866,
		time: 1865,
		velocity: 0,
		power: 298.374698881209,
		road: 14583.2725462963,
		acceleration: 0.732314814814815
	},
	{
		id: 1867,
		time: 1866,
		velocity: 2.19694444444444,
		power: 1582.69471005656,
		road: 14584.5793055556,
		acceleration: 1.14888888888889
	},
	{
		id: 1868,
		time: 1867,
		velocity: 3.44666666666667,
		power: 3925.33584692748,
		road: 14587.1877777778,
		acceleration: 1.45453703703704
	},
	{
		id: 1869,
		time: 1868,
		velocity: 4.36361111111111,
		power: 4296.22650391798,
		road: 14591.0425,
		acceleration: 1.03796296296296
	},
	{
		id: 1870,
		time: 1869,
		velocity: 5.31083333333333,
		power: 3854.26995214956,
		road: 14595.7748611111,
		acceleration: 0.717314814814815
	},
	{
		id: 1871,
		time: 1870,
		velocity: 5.59861111111111,
		power: 3967.55751485748,
		road: 14601.1801388889,
		acceleration: 0.62851851851852
	},
	{
		id: 1872,
		time: 1871,
		velocity: 6.24916666666667,
		power: 4803.04905733921,
		road: 14607.2423148148,
		acceleration: 0.685277777777777
	},
	{
		id: 1873,
		time: 1872,
		velocity: 7.36666666666667,
		power: 6651.87914107844,
		road: 14614.0815277778,
		acceleration: 0.868796296296296
	},
	{
		id: 1874,
		time: 1873,
		velocity: 8.205,
		power: 8712.45276734288,
		road: 14621.8626388889,
		acceleration: 1.015
	},
	{
		id: 1875,
		time: 1874,
		velocity: 9.29416666666667,
		power: 9078.24448833553,
		road: 14630.6108796296,
		acceleration: 0.91925925925926
	},
	{
		id: 1876,
		time: 1875,
		velocity: 10.1244444444444,
		power: 7276.43509786627,
		road: 14640.1306944445,
		acceleration: 0.623888888888889
	},
	{
		id: 1877,
		time: 1876,
		velocity: 10.0766666666667,
		power: 2151.98236642214,
		road: 14649.9859722222,
		acceleration: 0.0470370370370361
	},
	{
		id: 1878,
		time: 1877,
		velocity: 9.43527777777778,
		power: -2153.48229403078,
		road: 14659.6578240741,
		acceleration: -0.413888888888888
	},
	{
		id: 1879,
		time: 1878,
		velocity: 8.88277777777778,
		power: -3379.6932898974,
		road: 14668.8420370371,
		acceleration: -0.561388888888889
	},
	{
		id: 1880,
		time: 1879,
		velocity: 8.3925,
		power: -2387.91177340684,
		road: 14677.5160648148,
		acceleration: -0.458981481481482
	},
	{
		id: 1881,
		time: 1880,
		velocity: 8.05833333333333,
		power: -612.127373607799,
		road: 14685.83875,
		acceleration: -0.243703703703703
	},
	{
		id: 1882,
		time: 1881,
		velocity: 8.15166666666667,
		power: 947.887527692768,
		road: 14694.0179166667,
		acceleration: -0.043333333333333
	},
	{
		id: 1883,
		time: 1882,
		velocity: 8.2625,
		power: 3580.04650763483,
		road: 14702.3188425926,
		acceleration: 0.286851851851852
	},
	{
		id: 1884,
		time: 1883,
		velocity: 8.91888888888889,
		power: 4780.63021369449,
		road: 14710.9686574074,
		acceleration: 0.410925925925925
	},
	{
		id: 1885,
		time: 1884,
		velocity: 9.38444444444444,
		power: 5705.22494954679,
		road: 14720.0661574074,
		acceleration: 0.484444444444444
	},
	{
		id: 1886,
		time: 1885,
		velocity: 9.71583333333333,
		power: 5982.6156568741,
		road: 14729.6441666667,
		acceleration: 0.476574074074074
	},
	{
		id: 1887,
		time: 1886,
		velocity: 10.3486111111111,
		power: 5160.74693815779,
		road: 14739.6395370371,
		acceleration: 0.35814814814815
	},
	{
		id: 1888,
		time: 1887,
		velocity: 10.4588888888889,
		power: 4893.28396242285,
		road: 14749.96875,
		acceleration: 0.309537037037035
	},
	{
		id: 1889,
		time: 1888,
		velocity: 10.6444444444444,
		power: 2217.63152484342,
		road: 14760.46875,
		acceleration: 0.0320370370370373
	},
	{
		id: 1890,
		time: 1889,
		velocity: 10.4447222222222,
		power: 1092.97528007095,
		road: 14770.9448611111,
		acceleration: -0.0798148148148137
	},
	{
		id: 1891,
		time: 1890,
		velocity: 10.2194444444444,
		power: 368.125262550106,
		road: 14781.3057407408,
		acceleration: -0.150648148148148
	},
	{
		id: 1892,
		time: 1891,
		velocity: 10.1925,
		power: 2454.27249905199,
		road: 14791.6224074074,
		acceleration: 0.0622222222222213
	},
	{
		id: 1893,
		time: 1892,
		velocity: 10.6313888888889,
		power: 5930.9338917691,
		road: 14802.1700925926,
		acceleration: 0.399814814814814
	},
	{
		id: 1894,
		time: 1893,
		velocity: 11.4188888888889,
		power: 8058.7016969931,
		road: 14813.2027314815,
		acceleration: 0.570092592592593
	},
	{
		id: 1895,
		time: 1894,
		velocity: 11.9027777777778,
		power: 10188.1001382484,
		road: 14824.8758333333,
		acceleration: 0.710833333333335
	},
	{
		id: 1896,
		time: 1895,
		velocity: 12.7638888888889,
		power: 9668.93072357476,
		road: 14837.2086111111,
		acceleration: 0.608518518518519
	},
	{
		id: 1897,
		time: 1896,
		velocity: 13.2444444444444,
		power: 9261.92933446149,
		road: 14850.1110648148,
		acceleration: 0.530833333333334
	},
	{
		id: 1898,
		time: 1897,
		velocity: 13.4952777777778,
		power: 7061.47418195353,
		road: 14863.4425462963,
		acceleration: 0.32722222222222
	},
	{
		id: 1899,
		time: 1898,
		velocity: 13.7455555555556,
		power: 6631.69714924578,
		road: 14877.07625,
		acceleration: 0.277222222222223
	},
	{
		id: 1900,
		time: 1899,
		velocity: 14.0761111111111,
		power: 6441.34088676803,
		road: 14890.9731018519,
		acceleration: 0.249074074074073
	},
	{
		id: 1901,
		time: 1900,
		velocity: 14.2425,
		power: 5282.60087927526,
		road: 14905.0709259259,
		acceleration: 0.152870370370371
	},
	{
		id: 1902,
		time: 1901,
		velocity: 14.2041666666667,
		power: 1564.88085388162,
		road: 14919.1833796296,
		acceleration: -0.123611111111112
	},
	{
		id: 1903,
		time: 1902,
		velocity: 13.7052777777778,
		power: -1185.16590653371,
		road: 14933.07125,
		acceleration: -0.325555555555553
	},
	{
		id: 1904,
		time: 1903,
		velocity: 13.2658333333333,
		power: -2758.75667180238,
		road: 14946.574212963,
		acceleration: -0.44425925925926
	},
	{
		id: 1905,
		time: 1904,
		velocity: 12.8713888888889,
		power: -511.315489051765,
		road: 14959.7222222222,
		acceleration: -0.26564814814815
	},
	{
		id: 1906,
		time: 1905,
		velocity: 12.9083333333333,
		power: 1831.35484776313,
		road: 14972.7001388889,
		acceleration: -0.074537037037036
	},
	{
		id: 1907,
		time: 1906,
		velocity: 13.0422222222222,
		power: 3354.47372731077,
		road: 14985.6652314815,
		acceleration: 0.0488888888888894
	},
	{
		id: 1908,
		time: 1907,
		velocity: 13.0180555555556,
		power: 3417.09048008179,
		road: 14998.6808333333,
		acceleration: 0.0521296296296292
	},
	{
		id: 1909,
		time: 1908,
		velocity: 13.0647222222222,
		power: 3748.75767082664,
		road: 15011.7606944445,
		acceleration: 0.0763888888888911
	},
	{
		id: 1910,
		time: 1909,
		velocity: 13.2713888888889,
		power: 4079.10125313073,
		road: 15024.9284259259,
		acceleration: 0.0993518518518499
	},
	{
		id: 1911,
		time: 1910,
		velocity: 13.3161111111111,
		power: 4858.75011403886,
		road: 15038.2237037037,
		acceleration: 0.15574074074074
	},
	{
		id: 1912,
		time: 1911,
		velocity: 13.5319444444444,
		power: 4471.74373221546,
		road: 15051.6566203704,
		acceleration: 0.119537037037039
	},
	{
		id: 1913,
		time: 1912,
		velocity: 13.63,
		power: 2941.83756243626,
		road: 15065.1484722222,
		acceleration: -0.00166666666666515
	},
	{
		id: 1914,
		time: 1913,
		velocity: 13.3111111111111,
		power: 2432.95369603248,
		road: 15078.619212963,
		acceleration: -0.0405555555555566
	},
	{
		id: 1915,
		time: 1914,
		velocity: 13.4102777777778,
		power: 2518.89648152711,
		road: 15092.053287037,
		acceleration: -0.0327777777777793
	},
	{
		id: 1916,
		time: 1915,
		velocity: 13.5316666666667,
		power: 4629.19019658878,
		road: 15105.5358333333,
		acceleration: 0.129722222222222
	},
	{
		id: 1917,
		time: 1916,
		velocity: 13.7002777777778,
		power: 4466.51442087913,
		road: 15119.1393055556,
		acceleration: 0.112129629629628
	},
	{
		id: 1918,
		time: 1917,
		velocity: 13.7466666666667,
		power: 2769.00670350363,
		road: 15132.7888425926,
		acceleration: -0.0199999999999978
	},
	{
		id: 1919,
		time: 1918,
		velocity: 13.4716666666667,
		power: 2324.61944245032,
		road: 15146.4018518519,
		acceleration: -0.0530555555555576
	},
	{
		id: 1920,
		time: 1919,
		velocity: 13.5411111111111,
		power: 2837.06655540026,
		road: 15159.982037037,
		acceleration: -0.0125925925925916
	},
	{
		id: 1921,
		time: 1920,
		velocity: 13.7088888888889,
		power: 4539.5970522457,
		road: 15173.614212963,
		acceleration: 0.116574074074075
	},
	{
		id: 1922,
		time: 1921,
		velocity: 13.8213888888889,
		power: 4943.82721599331,
		road: 15187.3756944445,
		acceleration: 0.142037037037035
	},
	{
		id: 1923,
		time: 1922,
		velocity: 13.9672222222222,
		power: 4762.37125645455,
		road: 15201.269537037,
		acceleration: 0.122685185185187
	},
	{
		id: 1924,
		time: 1923,
		velocity: 14.0769444444444,
		power: 5000.22074715505,
		road: 15215.2922685185,
		acceleration: 0.135092592592592
	},
	{
		id: 1925,
		time: 1924,
		velocity: 14.2266666666667,
		power: 4895.01446982784,
		road: 15229.4434722222,
		acceleration: 0.121851851851853
	},
	{
		id: 1926,
		time: 1925,
		velocity: 14.3327777777778,
		power: 4296.41475304213,
		road: 15243.6925,
		acceleration: 0.0737962962962957
	},
	{
		id: 1927,
		time: 1926,
		velocity: 14.2983333333333,
		power: 4062.87236358168,
		road: 15258.0055555556,
		acceleration: 0.0542592592592595
	},
	{
		id: 1928,
		time: 1927,
		velocity: 14.3894444444444,
		power: 2485.57992434582,
		road: 15272.3152314815,
		acceleration: -0.0610185185185177
	},
	{
		id: 1929,
		time: 1928,
		velocity: 14.1497222222222,
		power: 1144.30678352443,
		road: 15286.5160185185,
		acceleration: -0.15675925925926
	},
	{
		id: 1930,
		time: 1929,
		velocity: 13.8280555555556,
		power: -389.730265279877,
		road: 15300.5049537037,
		acceleration: -0.266944444444444
	},
	{
		id: 1931,
		time: 1930,
		velocity: 13.5886111111111,
		power: -543.058030367431,
		road: 15314.2229166667,
		acceleration: -0.274999999999999
	},
	{
		id: 1932,
		time: 1931,
		velocity: 13.3247222222222,
		power: 289.777509944223,
		road: 15327.6997222222,
		acceleration: -0.207314814814817
	},
	{
		id: 1933,
		time: 1932,
		velocity: 13.2061111111111,
		power: 455.103900608656,
		road: 15340.9774074074,
		acceleration: -0.190925925925924
	},
	{
		id: 1934,
		time: 1933,
		velocity: 13.0158333333333,
		power: 1162.51179533477,
		road: 15354.0938425926,
		acceleration: -0.131574074074074
	},
	{
		id: 1935,
		time: 1934,
		velocity: 12.93,
		power: 986.476758106908,
		road: 15367.0731018519,
		acceleration: -0.142777777777779
	},
	{
		id: 1936,
		time: 1935,
		velocity: 12.7777777777778,
		power: 1483.83217163587,
		road: 15379.9311111111,
		acceleration: -0.0997222222222209
	},
	{
		id: 1937,
		time: 1936,
		velocity: 12.7166666666667,
		power: 850.152751433699,
		road: 15392.6648148148,
		acceleration: -0.148888888888891
	},
	{
		id: 1938,
		time: 1937,
		velocity: 12.4833333333333,
		power: 1262.12839157675,
		road: 15405.2680555556,
		acceleration: -0.112037037037036
	},
	{
		id: 1939,
		time: 1938,
		velocity: 12.4416666666667,
		power: 438.414544717105,
		road: 15417.72625,
		acceleration: -0.178055555555556
	},
	{
		id: 1940,
		time: 1939,
		velocity: 12.1825,
		power: 486.146282267319,
		road: 15430.0099074074,
		acceleration: -0.171018518518517
	},
	{
		id: 1941,
		time: 1940,
		velocity: 11.9702777777778,
		power: 593.541097647679,
		road: 15442.1286111111,
		acceleration: -0.158888888888891
	},
	{
		id: 1942,
		time: 1941,
		velocity: 11.965,
		power: 1237.25645764003,
		road: 15454.1177314815,
		acceleration: -0.100277777777777
	},
	{
		id: 1943,
		time: 1942,
		velocity: 11.8816666666667,
		power: 1669.90744290644,
		road: 15466.0265277778,
		acceleration: -0.0603703703703697
	},
	{
		id: 1944,
		time: 1943,
		velocity: 11.7891666666667,
		power: 1329.36831513565,
		road: 15477.8608333333,
		acceleration: -0.0886111111111134
	},
	{
		id: 1945,
		time: 1944,
		velocity: 11.6991666666667,
		power: 2417.78356360375,
		road: 15489.6553703704,
		acceleration: 0.00907407407407312
	},
	{
		id: 1946,
		time: 1945,
		velocity: 11.9088888888889,
		power: 3277.20927946596,
		road: 15501.4962962963,
		acceleration: 0.0837037037037049
	},
	{
		id: 1947,
		time: 1946,
		velocity: 12.0402777777778,
		power: 3306.85962423281,
		road: 15513.4206481482,
		acceleration: 0.0831481481481475
	},
	{
		id: 1948,
		time: 1947,
		velocity: 11.9486111111111,
		power: 3150.69790844733,
		road: 15525.4199074074,
		acceleration: 0.0666666666666682
	},
	{
		id: 1949,
		time: 1948,
		velocity: 12.1088888888889,
		power: 3198.27735119824,
		road: 15537.4866666667,
		acceleration: 0.0683333333333334
	},
	{
		id: 1950,
		time: 1949,
		velocity: 12.2452777777778,
		power: 4183.49796939001,
		road: 15549.6621759259,
		acceleration: 0.149166666666666
	},
	{
		id: 1951,
		time: 1950,
		velocity: 12.3961111111111,
		power: 3132.28475719765,
		road: 15561.9397685185,
		acceleration: 0.0549999999999997
	},
	{
		id: 1952,
		time: 1951,
		velocity: 12.2738888888889,
		power: 2446.26104361708,
		road: 15574.2426851852,
		acceleration: -0.00435185185185105
	},
	{
		id: 1953,
		time: 1952,
		velocity: 12.2322222222222,
		power: 1876.5274956084,
		road: 15586.5173611111,
		acceleration: -0.0521296296296292
	},
	{
		id: 1954,
		time: 1953,
		velocity: 12.2397222222222,
		power: 1793.37285288588,
		road: 15598.7370833333,
		acceleration: -0.0577777777777762
	},
	{
		id: 1955,
		time: 1954,
		velocity: 12.1005555555556,
		power: 1899.74990662601,
		road: 15610.9043055556,
		acceleration: -0.0472222222222225
	},
	{
		id: 1956,
		time: 1955,
		velocity: 12.0905555555556,
		power: 2317.37488545811,
		road: 15623.0427314815,
		acceleration: -0.0103703703703726
	},
	{
		id: 1957,
		time: 1956,
		velocity: 12.2086111111111,
		power: 2662.15913491427,
		road: 15635.1856018519,
		acceleration: 0.0192592592592575
	},
	{
		id: 1958,
		time: 1957,
		velocity: 12.1583333333333,
		power: 2252.97888528035,
		road: 15647.3300462963,
		acceleration: -0.0161111111111101
	},
	{
		id: 1959,
		time: 1958,
		velocity: 12.0422222222222,
		power: 2689.52674988671,
		road: 15659.4771759259,
		acceleration: 0.0214814814814819
	},
	{
		id: 1960,
		time: 1959,
		velocity: 12.2730555555556,
		power: 2787.35667070547,
		road: 15671.6495833334,
		acceleration: 0.0290740740740745
	},
	{
		id: 1961,
		time: 1960,
		velocity: 12.2455555555556,
		power: 3041.11372759409,
		road: 15683.8612962963,
		acceleration: 0.0495370370370374
	},
	{
		id: 1962,
		time: 1961,
		velocity: 12.1908333333333,
		power: 2113.40166596518,
		road: 15696.0825925926,
		acceleration: -0.0303703703703704
	},
	{
		id: 1963,
		time: 1962,
		velocity: 12.1819444444444,
		power: 1976.59982364087,
		road: 15708.2681481482,
		acceleration: -0.0411111111111104
	},
	{
		id: 1964,
		time: 1963,
		velocity: 12.1222222222222,
		power: 2548.58685409156,
		road: 15720.4374537037,
		acceleration: 0.00861111111110802
	},
	{
		id: 1965,
		time: 1964,
		velocity: 12.2166666666667,
		power: 2604.2433831887,
		road: 15732.6175925926,
		acceleration: 0.0130555555555567
	},
	{
		id: 1966,
		time: 1965,
		velocity: 12.2211111111111,
		power: 2511.77019500382,
		road: 15744.8066666667,
		acceleration: 0.00481481481481438
	},
	{
		id: 1967,
		time: 1966,
		velocity: 12.1366666666667,
		power: 1886.88239505111,
		road: 15756.9739814815,
		acceleration: -0.048333333333332
	},
	{
		id: 1968,
		time: 1967,
		velocity: 12.0716666666667,
		power: 2465.30892867939,
		road: 15769.1182407408,
		acceleration: 0.00222222222222257
	},
	{
		id: 1969,
		time: 1968,
		velocity: 12.2277777777778,
		power: 2827.26865290842,
		road: 15781.2800462963,
		acceleration: 0.0328703703703699
	},
	{
		id: 1970,
		time: 1969,
		velocity: 12.2352777777778,
		power: 2959.67222165096,
		road: 15793.4797685185,
		acceleration: 0.042962962962962
	},
	{
		id: 1971,
		time: 1970,
		velocity: 12.2005555555556,
		power: 2553.00366079404,
		road: 15805.7045833334,
		acceleration: 0.00722222222222335
	},
	{
		id: 1972,
		time: 1971,
		velocity: 12.2494444444444,
		power: 2705.05027017829,
		road: 15817.9429166667,
		acceleration: 0.019814814814815
	},
	{
		id: 1973,
		time: 1972,
		velocity: 12.2947222222222,
		power: 2601.5406553316,
		road: 15830.1963888889,
		acceleration: 0.0104629629629613
	},
	{
		id: 1974,
		time: 1973,
		velocity: 12.2319444444444,
		power: 2980.20704319706,
		road: 15842.4760648148,
		acceleration: 0.0419444444444448
	},
	{
		id: 1975,
		time: 1974,
		velocity: 12.3752777777778,
		power: 1764.45285161732,
		road: 15854.7459259259,
		acceleration: -0.0615740740740733
	},
	{
		id: 1976,
		time: 1975,
		velocity: 12.11,
		power: 2115.36532563334,
		road: 15866.9698611111,
		acceleration: -0.0302777777777798
	},
	{
		id: 1977,
		time: 1976,
		velocity: 12.1411111111111,
		power: 1332.72456820492,
		road: 15879.1306944445,
		acceleration: -0.0959259259259255
	},
	{
		id: 1978,
		time: 1977,
		velocity: 12.0875,
		power: 2692.53331087335,
		road: 15891.2548148148,
		acceleration: 0.0225000000000009
	},
	{
		id: 1979,
		time: 1978,
		velocity: 12.1775,
		power: 2344.94327933989,
		road: 15903.3862962963,
		acceleration: -0.00777777777777722
	},
	{
		id: 1980,
		time: 1979,
		velocity: 12.1177777777778,
		power: 2501.01862585373,
		road: 15915.5167592593,
		acceleration: 0.00574074074074105
	},
	{
		id: 1981,
		time: 1980,
		velocity: 12.1047222222222,
		power: 1962.94882300524,
		road: 15927.6299537037,
		acceleration: -0.0402777777777761
	},
	{
		id: 1982,
		time: 1981,
		velocity: 12.0566666666667,
		power: 2278.01456348204,
		road: 15939.7168981482,
		acceleration: -0.0122222222222241
	},
	{
		id: 1983,
		time: 1982,
		velocity: 12.0811111111111,
		power: 2464.43508293351,
		road: 15951.7997685185,
		acceleration: 0.00407407407407412
	},
	{
		id: 1984,
		time: 1983,
		velocity: 12.1169444444444,
		power: 2925.35728343197,
		road: 15963.9062962963,
		acceleration: 0.0432407407407407
	},
	{
		id: 1985,
		time: 1984,
		velocity: 12.1863888888889,
		power: 2750.69998955213,
		road: 15976.0479166667,
		acceleration: 0.026944444444446
	},
	{
		id: 1986,
		time: 1985,
		velocity: 12.1619444444444,
		power: 1428.68871032054,
		road: 15988.1597685185,
		acceleration: -0.0864814814814832
	},
	{
		id: 1987,
		time: 1986,
		velocity: 11.8575,
		power: -137.268781614525,
		road: 16000.1183333334,
		acceleration: -0.220092592592593
	},
	{
		id: 1988,
		time: 1987,
		velocity: 11.5261111111111,
		power: -1355.12079633763,
		road: 16011.8038425926,
		acceleration: -0.326018518518516
	},
	{
		id: 1989,
		time: 1988,
		velocity: 11.1838888888889,
		power: 62.5102198362748,
		road: 16023.2287037037,
		acceleration: -0.195277777777777
	},
	{
		id: 1990,
		time: 1989,
		velocity: 11.2716666666667,
		power: 1866.45475008603,
		road: 16034.5426388889,
		acceleration: -0.026574074074075
	},
	{
		id: 1991,
		time: 1990,
		velocity: 11.4463888888889,
		power: 4394.27268945139,
		road: 16045.945,
		acceleration: 0.203425925925925
	},
	{
		id: 1992,
		time: 1991,
		velocity: 11.7941666666667,
		power: 4395.39603491053,
		road: 16057.5460648148,
		acceleration: 0.193981481481481
	},
	{
		id: 1993,
		time: 1992,
		velocity: 11.8536111111111,
		power: 3218.16861687014,
		road: 16069.2852777778,
		acceleration: 0.082314814814815
	},
	{
		id: 1994,
		time: 1993,
		velocity: 11.6933333333333,
		power: 477.988850347207,
		road: 16080.9847685185,
		acceleration: -0.161759259259258
	},
	{
		id: 1995,
		time: 1994,
		velocity: 11.3088888888889,
		power: -579.075075114729,
		road: 16092.4760185185,
		acceleration: -0.254722222222224
	},
	{
		id: 1996,
		time: 1995,
		velocity: 11.0894444444444,
		power: -901.48897770829,
		road: 16103.6985648148,
		acceleration: -0.282685185185187
	},
	{
		id: 1997,
		time: 1996,
		velocity: 10.8452777777778,
		power: -138.739038510519,
		road: 16114.675462963,
		acceleration: -0.208611111111107
	},
	{
		id: 1998,
		time: 1997,
		velocity: 10.6830555555556,
		power: 444.090115890271,
		road: 16125.4730555556,
		acceleration: -0.150000000000002
	},
	{
		id: 1999,
		time: 1998,
		velocity: 10.6394444444444,
		power: 2092.578355771,
		road: 16136.2018055556,
		acceleration: 0.0123148148148147
	},
	{
		id: 2000,
		time: 1999,
		velocity: 10.8822222222222,
		power: 9133.32835807973,
		road: 16147.2711574074,
		acceleration: 0.66888888888889
	},
	{
		id: 2001,
		time: 2000,
		velocity: 12.6897222222222,
		power: 13364.1727164747,
		road: 16159.1605555556,
		acceleration: 0.971203703703702
	},
	{
		id: 2002,
		time: 2001,
		velocity: 13.5530555555556,
		power: 12904.2515361108,
		road: 16171.9541203704,
		acceleration: 0.837129629629629
	},
	{
		id: 2003,
		time: 2002,
		velocity: 13.3936111111111,
		power: 5898.63140913428,
		road: 16185.284212963,
		acceleration: 0.235925925925926
	},
	{
		id: 2004,
		time: 2003,
		velocity: 13.3975,
		power: 2727.77255562494,
		road: 16198.7239351852,
		acceleration: -0.0166666666666657
	},
	{
		id: 2005,
		time: 2004,
		velocity: 13.5030555555556,
		power: 2045.27034685652,
		road: 16212.1209722222,
		acceleration: -0.0687037037037044
	},
	{
		id: 2006,
		time: 2005,
		velocity: 13.1875,
		power: 1090.39183853145,
		road: 16225.4131018519,
		acceleration: -0.14111111111111
	},
	{
		id: 2007,
		time: 2006,
		velocity: 12.9741666666667,
		power: -569.962596770372,
		road: 16238.4998611111,
		acceleration: -0.26962962962963
	},
	{
		id: 2008,
		time: 2007,
		velocity: 12.6941666666667,
		power: -545.961629883907,
		road: 16251.3194444445,
		acceleration: -0.264722222222222
	},
	{
		id: 2009,
		time: 2008,
		velocity: 12.3933333333333,
		power: 70.2622199787341,
		road: 16263.90125,
		acceleration: -0.210833333333333
	},
	{
		id: 2010,
		time: 2009,
		velocity: 12.3416666666667,
		power: 940.422385767801,
		road: 16276.3102314815,
		acceleration: -0.134814814814815
	},
	{
		id: 2011,
		time: 2010,
		velocity: 12.2897222222222,
		power: 1911.52310926303,
		road: 16288.6266666667,
		acceleration: -0.0502777777777776
	},
	{
		id: 2012,
		time: 2011,
		velocity: 12.2425,
		power: 2995.6966255383,
		road: 16300.9390277778,
		acceleration: 0.0421296296296276
	},
	{
		id: 2013,
		time: 2012,
		velocity: 12.4680555555556,
		power: 4892.41795866697,
		road: 16313.3713888889,
		acceleration: 0.197870370370371
	},
	{
		id: 2014,
		time: 2013,
		velocity: 12.8833333333333,
		power: 5373.99401191684,
		road: 16326.0165740741,
		acceleration: 0.227777777777778
	},
	{
		id: 2015,
		time: 2014,
		velocity: 12.9258333333333,
		power: 3725.15249113091,
		road: 16338.8181944445,
		acceleration: 0.0850925925925932
	},
	{
		id: 2016,
		time: 2015,
		velocity: 12.7233333333333,
		power: 202.752123607338,
		road: 16351.5612037037,
		acceleration: -0.202314814814814
	},
	{
		id: 2017,
		time: 2016,
		velocity: 12.2763888888889,
		power: -1597.24801090241,
		road: 16364.0283796296,
		acceleration: -0.349351851851853
	},
	{
		id: 2018,
		time: 2017,
		velocity: 11.8777777777778,
		power: -2176.97727327579,
		road: 16376.1215740741,
		acceleration: -0.39861111111111
	},
	{
		id: 2019,
		time: 2018,
		velocity: 11.5275,
		power: -1973.57190614444,
		road: 16387.8247222222,
		acceleration: -0.38148148148148
	},
	{
		id: 2020,
		time: 2019,
		velocity: 11.1319444444444,
		power: -1560.55325485157,
		road: 16399.165,
		acceleration: -0.34425925925926
	},
	{
		id: 2021,
		time: 2020,
		velocity: 10.845,
		power: 1176.26904082424,
		road: 16410.29,
		acceleration: -0.0862962962962968
	},
	{
		id: 2022,
		time: 2021,
		velocity: 11.2686111111111,
		power: 1038.52920850491,
		road: 16421.3231944445,
		acceleration: -0.0973148148148155
	},
	{
		id: 2023,
		time: 2022,
		velocity: 10.84,
		power: 3185.10543184681,
		road: 16432.3610185185,
		acceleration: 0.106574074074075
	},
	{
		id: 2024,
		time: 2023,
		velocity: 11.1647222222222,
		power: 1586.71156374857,
		road: 16443.4290740741,
		acceleration: -0.0461111111111094
	},
	{
		id: 2025,
		time: 2024,
		velocity: 11.1302777777778,
		power: 2660.27258636018,
		road: 16454.5018055556,
		acceleration: 0.0554629629629613
	},
	{
		id: 2026,
		time: 2025,
		velocity: 11.0063888888889,
		power: 1556.60798813602,
		road: 16465.5776851852,
		acceleration: -0.0491666666666681
	},
	{
		id: 2027,
		time: 2026,
		velocity: 11.0172222222222,
		power: 1923.40801766782,
		road: 16476.6222222222,
		acceleration: -0.0135185185185183
	},
	{
		id: 2028,
		time: 2027,
		velocity: 11.0897222222222,
		power: 1903.46219030293,
		road: 16487.6525,
		acceleration: -0.0149999999999988
	},
	{
		id: 2029,
		time: 2028,
		velocity: 10.9613888888889,
		power: 1350.75158823911,
		road: 16498.6419907408,
		acceleration: -0.0665740740740759
	},
	{
		id: 2030,
		time: 2029,
		velocity: 10.8175,
		power: -1883.41286485848,
		road: 16509.4100462963,
		acceleration: -0.376296296296296
	},
	{
		id: 2031,
		time: 2030,
		velocity: 9.96083333333333,
		power: -3441.15698133283,
		road: 16519.7211574074,
		acceleration: -0.537592592592592
	},
	{
		id: 2032,
		time: 2031,
		velocity: 9.34861111111111,
		power: -7184.68923977137,
		road: 16529.2793981482,
		acceleration: -0.968148148148147
	},
	{
		id: 2033,
		time: 2032,
		velocity: 7.91305555555556,
		power: -5753.7122912429,
		road: 16537.9190277778,
		acceleration: -0.869074074074074
	},
	{
		id: 2034,
		time: 2033,
		velocity: 7.35361111111111,
		power: -5880.08092681262,
		road: 16545.6435648148,
		acceleration: -0.961111111111111
	},
	{
		id: 2035,
		time: 2034,
		velocity: 6.46527777777778,
		power: -1826.62901628821,
		road: 16552.6733333333,
		acceleration: -0.428425925925926
	},
	{
		id: 2036,
		time: 2035,
		velocity: 6.62777777777778,
		power: 351.699308299356,
		road: 16559.4396296296,
		acceleration: -0.09851851851852
	},
	{
		id: 2037,
		time: 2036,
		velocity: 7.05805555555555,
		power: 4036.66357236019,
		road: 16566.3849074074,
		acceleration: 0.456481481481482
	},
	{
		id: 2038,
		time: 2037,
		velocity: 7.83472222222222,
		power: 6238.78182209793,
		road: 16573.9141666667,
		acceleration: 0.711481481481481
	},
	{
		id: 2039,
		time: 2038,
		velocity: 8.76222222222222,
		power: 8620.17763999422,
		road: 16582.2584259259,
		acceleration: 0.91851851851852
	},
	{
		id: 2040,
		time: 2039,
		velocity: 9.81361111111111,
		power: 7919.62054554946,
		road: 16591.427962963,
		acceleration: 0.732037037037035
	},
	{
		id: 2041,
		time: 2040,
		velocity: 10.0308333333333,
		power: 7196.0967824652,
		road: 16601.2568055556,
		acceleration: 0.586574074074075
	},
	{
		id: 2042,
		time: 2041,
		velocity: 10.5219444444444,
		power: 5992.21298936502,
		road: 16611.5893981482,
		acceleration: 0.420925925925925
	},
	{
		id: 2043,
		time: 2042,
		velocity: 11.0763888888889,
		power: 6341.23980407158,
		road: 16622.3453703704,
		acceleration: 0.425833333333335
	},
	{
		id: 2044,
		time: 2043,
		velocity: 11.3083333333333,
		power: 5429.64466204769,
		road: 16633.4715740741,
		acceleration: 0.31462962962963
	},
	{
		id: 2045,
		time: 2044,
		velocity: 11.4658333333333,
		power: 4359.49759616605,
		road: 16644.8556481482,
		acceleration: 0.201111111111109
	},
	{
		id: 2046,
		time: 2045,
		velocity: 11.6797222222222,
		power: 5840.16959157606,
		road: 16656.50125,
		acceleration: 0.321944444444444
	},
	{
		id: 2047,
		time: 2046,
		velocity: 12.2741666666667,
		power: 7132.68519603043,
		road: 16668.5146296296,
		acceleration: 0.413611111111113
	},
	{
		id: 2048,
		time: 2047,
		velocity: 12.7066666666667,
		power: 8720.21350761348,
		road: 16680.9933796296,
		acceleration: 0.517129629629629
	},
	{
		id: 2049,
		time: 2048,
		velocity: 13.2311111111111,
		power: 8488.80894529006,
		road: 16693.9624537037,
		acceleration: 0.463518518518516
	},
	{
		id: 2050,
		time: 2049,
		velocity: 13.6647222222222,
		power: 9444.78783352671,
		road: 16707.4162037037,
		acceleration: 0.505833333333335
	},
	{
		id: 2051,
		time: 2050,
		velocity: 14.2241666666667,
		power: 8725.16717594497,
		road: 16721.3328240741,
		acceleration: 0.419907407407408
	},
	{
		id: 2052,
		time: 2051,
		velocity: 14.4908333333333,
		power: 9541.72272573494,
		road: 16735.6856018519,
		acceleration: 0.45240740740741
	},
	{
		id: 2053,
		time: 2052,
		velocity: 15.0219444444444,
		power: 6806.36108262285,
		road: 16750.3823611111,
		acceleration: 0.235555555555553
	},
	{
		id: 2054,
		time: 2053,
		velocity: 14.9308333333333,
		power: 5458.50739089746,
		road: 16765.2627314815,
		acceleration: 0.131666666666666
	},
	{
		id: 2055,
		time: 2054,
		velocity: 14.8858333333333,
		power: 4608.96460957997,
		road: 16780.242962963,
		acceleration: 0.0680555555555546
	},
	{
		id: 2056,
		time: 2055,
		velocity: 15.2261111111111,
		power: 5335.78459214767,
		road: 16795.3147222222,
		acceleration: 0.115000000000002
	},
	{
		id: 2057,
		time: 2056,
		velocity: 15.2758333333333,
		power: 5105.04521062895,
		road: 16810.4913425926,
		acceleration: 0.0947222222222219
	},
	{
		id: 2058,
		time: 2057,
		velocity: 15.17,
		power: 3548.02664328734,
		road: 16825.7083333333,
		acceleration: -0.0139814814814816
	},
	{
		id: 2059,
		time: 2058,
		velocity: 15.1841666666667,
		power: 2812.15453848722,
		road: 16840.8866203704,
		acceleration: -0.0634259259259249
	},
	{
		id: 2060,
		time: 2059,
		velocity: 15.0855555555556,
		power: 3652.34700595503,
		road: 16856.0310185185,
		acceleration: -0.00435185185185283
	},
	{
		id: 2061,
		time: 2060,
		velocity: 15.1569444444444,
		power: 3587.55857277607,
		road: 16871.1689351852,
		acceleration: -0.00861111111111157
	},
	{
		id: 2062,
		time: 2061,
		velocity: 15.1583333333333,
		power: 4084.83237200574,
		road: 16886.3152777778,
		acceleration: 0.0254629629629619
	},
	{
		id: 2063,
		time: 2062,
		velocity: 15.1619444444444,
		power: 3281.02225765891,
		road: 16901.4593518519,
		acceleration: -0.0299999999999976
	},
	{
		id: 2064,
		time: 2063,
		velocity: 15.0669444444444,
		power: 3720.01021593372,
		road: 16916.5888425926,
		acceleration: 0.000833333333332575
	},
	{
		id: 2065,
		time: 2064,
		velocity: 15.1608333333333,
		power: 3225.34918698768,
		road: 16931.7023148148,
		acceleration: -0.0328703703703717
	},
	{
		id: 2066,
		time: 2065,
		velocity: 15.0633333333333,
		power: 3755.82929657893,
		road: 16946.8015277778,
		acceleration: 0.00435185185185105
	},
	{
		id: 2067,
		time: 2066,
		velocity: 15.08,
		power: 3064.46931477559,
		road: 16961.8814351852,
		acceleration: -0.0429629629629602
	},
	{
		id: 2068,
		time: 2067,
		velocity: 15.0319444444444,
		power: 4423.60987183492,
		road: 16976.965462963,
		acceleration: 0.0512037037037025
	},
	{
		id: 2069,
		time: 2068,
		velocity: 15.2169444444444,
		power: 3892.68983466298,
		road: 16992.081712963,
		acceleration: 0.0132407407407413
	},
	{
		id: 2070,
		time: 2069,
		velocity: 15.1197222222222,
		power: 3294.08676251091,
		road: 17007.1906018519,
		acceleration: -0.0279629629629632
	},
	{
		id: 2071,
		time: 2070,
		velocity: 14.9480555555556,
		power: 1485.29739671145,
		road: 17022.2099537037,
		acceleration: -0.15111111111111
	},
	{
		id: 2072,
		time: 2071,
		velocity: 14.7636111111111,
		power: -1038.52933973163,
		road: 17036.9916666667,
		acceleration: -0.324166666666668
	},
	{
		id: 2073,
		time: 2072,
		velocity: 14.1472222222222,
		power: -1659.25416326678,
		road: 17051.4286574074,
		acceleration: -0.365277777777777
	},
	{
		id: 2074,
		time: 2073,
		velocity: 13.8522222222222,
		power: -2505.51254074186,
		road: 17065.4702777778,
		acceleration: -0.425462962962962
	},
	{
		id: 2075,
		time: 2074,
		velocity: 13.4872222222222,
		power: -2149.98372624654,
		road: 17079.100462963,
		acceleration: -0.397407407407407
	},
	{
		id: 2076,
		time: 2075,
		velocity: 12.955,
		power: -3394.28111606995,
		road: 17092.2843518519,
		acceleration: -0.495185185185186
	},
	{
		id: 2077,
		time: 2076,
		velocity: 12.3666666666667,
		power: -4139.64763335329,
		road: 17104.9403703704,
		acceleration: -0.560555555555556
	},
	{
		id: 2078,
		time: 2077,
		velocity: 11.8055555555556,
		power: -4445.37867768565,
		road: 17117.0183333333,
		acceleration: -0.595555555555556
	},
	{
		id: 2079,
		time: 2078,
		velocity: 11.1683333333333,
		power: -5684.32974494731,
		road: 17128.4369907408,
		acceleration: -0.723055555555554
	},
	{
		id: 2080,
		time: 2079,
		velocity: 10.1975,
		power: -6195.4341930706,
		road: 17139.0933796296,
		acceleration: -0.801481481481481
	},
	{
		id: 2081,
		time: 2080,
		velocity: 9.40111111111111,
		power: -5515.49133873065,
		road: 17148.9645833333,
		acceleration: -0.76888888888889
	},
	{
		id: 2082,
		time: 2081,
		velocity: 8.86166666666667,
		power: -2800.46651010009,
		road: 17158.2044444445,
		acceleration: -0.493796296296296
	},
	{
		id: 2083,
		time: 2082,
		velocity: 8.71611111111111,
		power: -546.612511574213,
		road: 17167.0791666667,
		acceleration: -0.236481481481482
	},
	{
		id: 2084,
		time: 2083,
		velocity: 8.69166666666667,
		power: 1733.28206033454,
		road: 17175.8539814815,
		acceleration: 0.0366666666666671
	},
	{
		id: 2085,
		time: 2084,
		velocity: 8.97166666666667,
		power: 3288.43115689097,
		road: 17184.7550925926,
		acceleration: 0.215925925925927
	},
	{
		id: 2086,
		time: 2085,
		velocity: 9.36388888888889,
		power: 3942.56768219967,
		road: 17193.9031481482,
		acceleration: 0.277962962962961
	},
	{
		id: 2087,
		time: 2086,
		velocity: 9.52555555555556,
		power: 4317.16422706028,
		road: 17203.3414814815,
		acceleration: 0.302592592592593
	},
	{
		id: 2088,
		time: 2087,
		velocity: 9.87944444444445,
		power: 3241.23314322879,
		road: 17213.0168518519,
		acceleration: 0.171481481481482
	},
	{
		id: 2089,
		time: 2088,
		velocity: 9.87833333333333,
		power: 3692.70758446162,
		road: 17222.8832407407,
		acceleration: 0.210555555555555
	},
	{
		id: 2090,
		time: 2089,
		velocity: 10.1572222222222,
		power: 4655.38855905182,
		road: 17233.0037037037,
		acceleration: 0.297592592592592
	},
	{
		id: 2091,
		time: 2090,
		velocity: 10.7722222222222,
		power: 6089.56677698666,
		road: 17243.4831481482,
		acceleration: 0.420370370370371
	},
	{
		id: 2092,
		time: 2091,
		velocity: 11.1394444444444,
		power: 8902.67018411258,
		road: 17254.4987037037,
		acceleration: 0.651851851851852
	},
	{
		id: 2093,
		time: 2092,
		velocity: 12.1127777777778,
		power: 9475.3036935091,
		road: 17266.1639351852,
		acceleration: 0.647500000000001
	},
	{
		id: 2094,
		time: 2093,
		velocity: 12.7147222222222,
		power: 11981.7928074065,
		road: 17278.5527314815,
		acceleration: 0.79962962962963
	},
	{
		id: 2095,
		time: 2094,
		velocity: 13.5383333333333,
		power: 11075.6157838522,
		road: 17291.6713425926,
		acceleration: 0.66
	},
	{
		id: 2096,
		time: 2095,
		velocity: 14.0927777777778,
		power: 10439.8555899549,
		road: 17305.40125,
		acceleration: 0.562592592592591
	},
	{
		id: 2097,
		time: 2096,
		velocity: 14.4025,
		power: 7355.24414383042,
		road: 17319.5640740741,
		acceleration: 0.30324074074074
	},
	{
		id: 2098,
		time: 2097,
		velocity: 14.4480555555556,
		power: 5813.42954407044,
		road: 17333.9675925926,
		acceleration: 0.17814814814815
	},
	{
		id: 2099,
		time: 2098,
		velocity: 14.6272222222222,
		power: 5313.83784240223,
		road: 17348.5277314815,
		acceleration: 0.135092592592594
	},
	{
		id: 2100,
		time: 2099,
		velocity: 14.8077777777778,
		power: 6321.04047622159,
		road: 17363.2551851852,
		acceleration: 0.199537037037038
	},
	{
		id: 2101,
		time: 2100,
		velocity: 15.0466666666667,
		power: 7088.58884874084,
		road: 17378.2038425926,
		acceleration: 0.242870370370369
	},
	{
		id: 2102,
		time: 2101,
		velocity: 15.3558333333333,
		power: 7470.04549547172,
		road: 17393.4022685185,
		acceleration: 0.256666666666668
	},
	{
		id: 2103,
		time: 2102,
		velocity: 15.5777777777778,
		power: 7824.14780998582,
		road: 17408.8626851852,
		acceleration: 0.267314814814812
	},
	{
		id: 2104,
		time: 2103,
		velocity: 15.8486111111111,
		power: 5817.48935246703,
		road: 17424.5183333333,
		acceleration: 0.123148148148148
	},
	{
		id: 2105,
		time: 2104,
		velocity: 15.7252777777778,
		power: 2373.3714786227,
		road: 17440.1819444445,
		acceleration: -0.107222222222221
	},
	{
		id: 2106,
		time: 2105,
		velocity: 15.2561111111111,
		power: -517.463929708689,
		road: 17455.6433333333,
		acceleration: -0.297222222222223
	},
	{
		id: 2107,
		time: 2106,
		velocity: 14.9569444444444,
		power: -2538.97798280082,
		road: 17470.7401388889,
		acceleration: -0.431944444444444
	},
	{
		id: 2108,
		time: 2107,
		velocity: 14.4294444444444,
		power: -2613.97132684435,
		road: 17485.4033333334,
		acceleration: -0.435277777777777
	},
	{
		id: 2109,
		time: 2108,
		velocity: 13.9502777777778,
		power: -2118.7188405723,
		road: 17499.6500925926,
		acceleration: -0.397592592592593
	},
	{
		id: 2110,
		time: 2109,
		velocity: 13.7641666666667,
		power: 556.671141843898,
		road: 17513.6003703704,
		acceleration: -0.195370370370371
	},
	{
		id: 2111,
		time: 2110,
		velocity: 13.8433333333333,
		power: 2838.08230070644,
		road: 17527.4425925926,
		acceleration: -0.0207407407407398
	},
	{
		id: 2112,
		time: 2111,
		velocity: 13.8880555555556,
		power: 4221.04076197711,
		road: 17541.3157870371,
		acceleration: 0.0826851851851842
	},
	{
		id: 2113,
		time: 2112,
		velocity: 14.0122222222222,
		power: 4752.41163456532,
		road: 17555.2896296296,
		acceleration: 0.118611111111113
	},
	{
		id: 2114,
		time: 2113,
		velocity: 14.1991666666667,
		power: 6678.04758729934,
		road: 17569.4494444445,
		acceleration: 0.253333333333332
	},
	{
		id: 2115,
		time: 2114,
		velocity: 14.6480555555556,
		power: 7957.15259121299,
		road: 17583.9015277778,
		acceleration: 0.331203703703702
	},
	{
		id: 2116,
		time: 2115,
		velocity: 15.0058333333333,
		power: 8200.61807243102,
		road: 17598.684212963,
		acceleration: 0.330000000000002
	},
	{
		id: 2117,
		time: 2116,
		velocity: 15.1891666666667,
		power: 7431.29159948004,
		road: 17613.7619907408,
		acceleration: 0.260185185185186
	},
	{
		id: 2118,
		time: 2117,
		velocity: 15.4286111111111,
		power: 4513.2087037249,
		road: 17628.9957407408,
		acceleration: 0.0517592592592582
	},
	{
		id: 2119,
		time: 2118,
		velocity: 15.1611111111111,
		power: 2058.32289809265,
		road: 17644.1973611111,
		acceleration: -0.116018518518521
	},
	{
		id: 2120,
		time: 2119,
		velocity: 14.8411111111111,
		power: -1746.11843119556,
		road: 17659.1531018519,
		acceleration: -0.375740740740738
	},
	{
		id: 2121,
		time: 2120,
		velocity: 14.3013888888889,
		power: -2663.33193627918,
		road: 17673.7017592593,
		acceleration: -0.438425925925927
	},
	{
		id: 2122,
		time: 2121,
		velocity: 13.8458333333333,
		power: -2763.98383792846,
		road: 17687.8087962963,
		acceleration: -0.444814814814814
	},
	{
		id: 2123,
		time: 2122,
		velocity: 13.5066666666667,
		power: -1899.0307441807,
		road: 17701.504212963,
		acceleration: -0.378425925925924
	},
	{
		id: 2124,
		time: 2123,
		velocity: 13.1661111111111,
		power: -1087.29095106877,
		road: 17714.8537962963,
		acceleration: -0.31324074074074
	},
	{
		id: 2125,
		time: 2124,
		velocity: 12.9061111111111,
		power: -1251.3567916261,
		road: 17727.8848611111,
		acceleration: -0.323796296296297
	},
	{
		id: 2126,
		time: 2125,
		velocity: 12.5352777777778,
		power: 1175.87167193933,
		road: 17740.6921759259,
		acceleration: -0.123703703703704
	},
	{
		id: 2127,
		time: 2126,
		velocity: 12.795,
		power: 3243.82122553594,
		road: 17753.4610648148,
		acceleration: 0.0468518518518533
	},
	{
		id: 2128,
		time: 2127,
		velocity: 13.0466666666667,
		power: 5857.33395075332,
		road: 17766.3801388889,
		acceleration: 0.253518518518518
	},
	{
		id: 2129,
		time: 2128,
		velocity: 13.2958333333333,
		power: 5808.7843855199,
		road: 17779.544537037,
		acceleration: 0.237129629629628
	},
	{
		id: 2130,
		time: 2129,
		velocity: 13.5063888888889,
		power: 3984.14297465427,
		road: 17792.8703240741,
		acceleration: 0.0856481481481506
	},
	{
		id: 2131,
		time: 2130,
		velocity: 13.3036111111111,
		power: 1477.35469646375,
		road: 17806.1833796296,
		acceleration: -0.111111111111114
	},
	{
		id: 2132,
		time: 2131,
		velocity: 12.9625,
		power: -796.807313741236,
		road: 17819.2968518519,
		acceleration: -0.288055555555555
	},
	{
		id: 2133,
		time: 2132,
		velocity: 12.6422222222222,
		power: -1686.71845658447,
		road: 17832.0873611111,
		acceleration: -0.357870370370371
	},
	{
		id: 2134,
		time: 2133,
		velocity: 12.23,
		power: -1945.67815472824,
		road: 17844.5096296296,
		acceleration: -0.378611111111109
	},
	{
		id: 2135,
		time: 2134,
		velocity: 11.8266666666667,
		power: -1890.69843359881,
		road: 17856.5556944445,
		acceleration: -0.373796296296296
	},
	{
		id: 2136,
		time: 2135,
		velocity: 11.5208333333333,
		power: -2234.94358264715,
		road: 17868.2123148148,
		acceleration: -0.405092592592593
	},
	{
		id: 2137,
		time: 2136,
		velocity: 11.0147222222222,
		power: -6189.12461462192,
		road: 17879.2747222222,
		acceleration: -0.783333333333333
	},
	{
		id: 2138,
		time: 2137,
		velocity: 9.47666666666667,
		power: -8282.62510521009,
		road: 17889.4244444445,
		acceleration: -1.04203703703704
	},
	{
		id: 2139,
		time: 2138,
		velocity: 8.39472222222222,
		power: -9584.36989592086,
		road: 17898.4062962963,
		acceleration: -1.2937037037037
	},
	{
		id: 2140,
		time: 2139,
		velocity: 7.13361111111111,
		power: -7123.51169949954,
		road: 17906.1789351852,
		acceleration: -1.12472222222222
	},
	{
		id: 2141,
		time: 2140,
		velocity: 6.1025,
		power: -5443.09018899128,
		road: 17912.8861111111,
		acceleration: -1.0062037037037
	},
	{
		id: 2142,
		time: 2141,
		velocity: 5.37611111111111,
		power: -4858.99948194025,
		road: 17918.5674074074,
		acceleration: -1.04555555555556
	},
	{
		id: 2143,
		time: 2142,
		velocity: 3.99694444444444,
		power: -4314.80683855859,
		road: 17923.1618518519,
		acceleration: -1.12814814814815
	},
	{
		id: 2144,
		time: 2143,
		velocity: 2.71805555555556,
		power: -3728.53560398204,
		road: 17926.5445833333,
		acceleration: -1.29527777777778
	},
	{
		id: 2145,
		time: 2144,
		velocity: 1.49027777777778,
		power: -1825.03257336439,
		road: 17928.7851388889,
		acceleration: -0.989074074074074
	},
	{
		id: 2146,
		time: 2145,
		velocity: 1.02972222222222,
		power: -953.119004474887,
		road: 17930.0781481482,
		acceleration: -0.906018518518518
	},
	{
		id: 2147,
		time: 2146,
		velocity: 0,
		power: -206.908683335646,
		road: 17930.6697685185,
		acceleration: -0.496759259259259
	},
	{
		id: 2148,
		time: 2147,
		velocity: 0,
		power: -35.0719173651722,
		road: 17930.8413888889,
		acceleration: -0.343240740740741
	},
	{
		id: 2149,
		time: 2148,
		velocity: 0,
		power: 0,
		road: 17930.8413888889,
		acceleration: 0
	},
	{
		id: 2150,
		time: 2149,
		velocity: 0,
		power: 0,
		road: 17930.8413888889,
		acceleration: 0
	},
	{
		id: 2151,
		time: 2150,
		velocity: 0,
		power: 0,
		road: 17930.8413888889,
		acceleration: 0
	},
	{
		id: 2152,
		time: 2151,
		velocity: 0,
		power: 0,
		road: 17930.8413888889,
		acceleration: 0
	},
	{
		id: 2153,
		time: 2152,
		velocity: 0,
		power: 0,
		road: 17930.8413888889,
		acceleration: 0
	},
	{
		id: 2154,
		time: 2153,
		velocity: 0,
		power: 0,
		road: 17930.8413888889,
		acceleration: 0
	},
	{
		id: 2155,
		time: 2154,
		velocity: 0,
		power: 0,
		road: 17930.8413888889,
		acceleration: 0
	},
	{
		id: 2156,
		time: 2155,
		velocity: 0,
		power: 0,
		road: 17930.8413888889,
		acceleration: 0
	},
	{
		id: 2157,
		time: 2156,
		velocity: 0,
		power: 28.9144179605174,
		road: 17930.9370833333,
		acceleration: 0.191388888888889
	},
	{
		id: 2158,
		time: 2157,
		velocity: 0.574166666666667,
		power: 315.898542265678,
		road: 17931.4140740741,
		acceleration: 0.571203703703704
	},
	{
		id: 2159,
		time: 2158,
		velocity: 1.71361111111111,
		power: 686.420954652302,
		road: 17932.4590740741,
		acceleration: 0.564814814814815
	},
	{
		id: 2160,
		time: 2159,
		velocity: 1.69444444444444,
		power: 742.292018519953,
		road: 17933.979537037,
		acceleration: 0.386111111111111
	},
	{
		id: 2161,
		time: 2160,
		velocity: 1.7325,
		power: 203.822545641434,
		road: 17935.6912962963,
		acceleration: -0.0035185185185187
	},
	{
		id: 2162,
		time: 2161,
		velocity: 1.70305555555556,
		power: 1404.99316158688,
		road: 17937.7043518519,
		acceleration: 0.606111111111111
	},
	{
		id: 2163,
		time: 2162,
		velocity: 3.51277777777778,
		power: 2878.29185522181,
		road: 17940.4975,
		acceleration: 0.954074074074074
	},
	{
		id: 2164,
		time: 2163,
		velocity: 4.59472222222222,
		power: 5315.45802632641,
		road: 17944.414212963,
		acceleration: 1.29305555555556
	},
	{
		id: 2165,
		time: 2164,
		velocity: 5.58222222222222,
		power: 3764.35688979243,
		road: 17949.3115277778,
		acceleration: 0.668148148148147
	},
	{
		id: 2166,
		time: 2165,
		velocity: 5.51722222222222,
		power: 2017.35053454811,
		road: 17954.6692592593,
		acceleration: 0.252685185185185
	},
	{
		id: 2167,
		time: 2166,
		velocity: 5.35277777777778,
		power: 1957.93962243127,
		road: 17960.2649074074,
		acceleration: 0.223148148148148
	},
	{
		id: 2168,
		time: 2167,
		velocity: 6.25166666666667,
		power: 3879.82570899967,
		road: 17966.2399074074,
		acceleration: 0.535555555555556
	},
	{
		id: 2169,
		time: 2168,
		velocity: 7.12388888888889,
		power: 5367.15989811399,
		road: 17972.8346296296,
		acceleration: 0.703888888888889
	},
	{
		id: 2170,
		time: 2169,
		velocity: 7.46444444444444,
		power: 2991.88112629407,
		road: 17979.9252314815,
		acceleration: 0.28787037037037
	},
	{
		id: 2171,
		time: 2170,
		velocity: 7.11527777777778,
		power: 683.409068538261,
		road: 17987.1312962963,
		acceleration: -0.0569444444444445
	},
	{
		id: 2172,
		time: 2171,
		velocity: 6.95305555555556,
		power: 831.064263936977,
		road: 17994.2917592593,
		acceleration: -0.0342592592592581
	},
	{
		id: 2173,
		time: 2172,
		velocity: 7.36166666666667,
		power: 3023.20509500214,
		road: 18001.5746296296,
		acceleration: 0.279074074074074
	},
	{
		id: 2174,
		time: 2173,
		velocity: 7.9525,
		power: 5214.17396342541,
		road: 18009.2725462963,
		acceleration: 0.551018518518518
	},
	{
		id: 2175,
		time: 2174,
		velocity: 8.60611111111111,
		power: 5807.47111143661,
		road: 18017.5323611111,
		acceleration: 0.572777777777778
	},
	{
		id: 2176,
		time: 2175,
		velocity: 9.08,
		power: 4473.08060082441,
		road: 18026.2624537037,
		acceleration: 0.367777777777777
	},
	{
		id: 2177,
		time: 2176,
		velocity: 9.05583333333333,
		power: 1771.5383614991,
		road: 18035.1943981481,
		acceleration: 0.0359259259259268
	},
	{
		id: 2178,
		time: 2177,
		velocity: 8.71388888888889,
		power: -2816.547924816,
		road: 18043.8891666667,
		acceleration: -0.510277777777779
	},
	{
		id: 2179,
		time: 2178,
		velocity: 7.54916666666667,
		power: -6184.00239708231,
		road: 18051.8385185185,
		acceleration: -0.980555555555554
	},
	{
		id: 2180,
		time: 2179,
		velocity: 6.11416666666667,
		power: -9941.83897447539,
		road: 18058.4281944444,
		acceleration: -1.7387962962963
	},
	{
		id: 2181,
		time: 2180,
		velocity: 3.4975,
		power: -7546.51101404021,
		road: 18063.2551388889,
		acceleration: -1.78666666666667
	},
	{
		id: 2182,
		time: 2181,
		velocity: 2.18916666666667,
		power: -4339.60470189004,
		road: 18066.3936574074,
		acceleration: -1.59018518518518
	},
	{
		id: 2183,
		time: 2182,
		velocity: 1.34361111111111,
		power: -1439.73052334198,
		road: 18068.2671759259,
		acceleration: -0.939814814814814
	},
	{
		id: 2184,
		time: 2183,
		velocity: 0.678055555555556,
		power: -461.193106353274,
		road: 18069.3900462963,
		acceleration: -0.561481481481482
	},
	{
		id: 2185,
		time: 2184,
		velocity: 0.504722222222222,
		power: -25.6690808573959,
		road: 18070.150462963,
		acceleration: -0.163425925925926
	},
	{
		id: 2186,
		time: 2185,
		velocity: 0.853333333333333,
		power: 364.663000398706,
		road: 18070.9934259259,
		acceleration: 0.328518518518519
	},
	{
		id: 2187,
		time: 2186,
		velocity: 1.66361111111111,
		power: 973.195180743987,
		road: 18072.3225462963,
		acceleration: 0.643796296296296
	},
	{
		id: 2188,
		time: 2187,
		velocity: 2.43611111111111,
		power: 1626.1882128256,
		road: 18074.3347222222,
		acceleration: 0.722314814814815
	},
	{
		id: 2189,
		time: 2188,
		velocity: 3.02027777777778,
		power: 2202.46262320377,
		road: 18077.0669907407,
		acceleration: 0.717870370370371
	},
	{
		id: 2190,
		time: 2189,
		velocity: 3.81722222222222,
		power: 756.027211181224,
		road: 18080.2181018518,
		acceleration: 0.119814814814814
	},
	{
		id: 2191,
		time: 2190,
		velocity: 2.79555555555556,
		power: -444.032837318316,
		road: 18083.2865277778,
		acceleration: -0.285185185185185
	},
	{
		id: 2192,
		time: 2191,
		velocity: 2.16472222222222,
		power: -1624.67885293135,
		road: 18085.8071296296,
		acceleration: -0.810462962962963
	},
	{
		id: 2193,
		time: 2192,
		velocity: 1.38583333333333,
		power: -854.72727516211,
		road: 18087.6075462963,
		acceleration: -0.629907407407408
	},
	{
		id: 2194,
		time: 2193,
		velocity: 0.905833333333333,
		power: 91.0580598774589,
		road: 18089.061712963,
		acceleration: -0.0625925925925923
	},
	{
		id: 2195,
		time: 2194,
		velocity: 1.97694444444444,
		power: 701.789342370064,
		road: 18090.6527314815,
		acceleration: 0.336296296296296
	},
	{
		id: 2196,
		time: 2195,
		velocity: 2.39472222222222,
		power: 1742.47771751136,
		road: 18092.7788888889,
		acceleration: 0.733981481481482
	},
	{
		id: 2197,
		time: 2196,
		velocity: 3.10777777777778,
		power: 1584.85502252585,
		road: 18095.5118055556,
		acceleration: 0.479537037037037
	},
	{
		id: 2198,
		time: 2197,
		velocity: 3.41555555555556,
		power: 4966.97869953639,
		road: 18099.1383333333,
		acceleration: 1.30768518518519
	},
	{
		id: 2199,
		time: 2198,
		velocity: 6.31777777777778,
		power: 8792.93435148854,
		road: 18104.2522685185,
		acceleration: 1.66712962962963
	},
	{
		id: 2200,
		time: 2199,
		velocity: 8.10916666666667,
		power: 13814.7343741861,
		road: 18111.1718518519,
		acceleration: 1.94416666666667
	},
	{
		id: 2201,
		time: 2200,
		velocity: 9.24805555555555,
		power: 15670.1202712879,
		road: 18119.9188425926,
		acceleration: 1.71064814814815
	},
	{
		id: 2202,
		time: 2201,
		velocity: 11.4497222222222,
		power: 13843.5222340283,
		road: 18130.1386574074,
		acceleration: 1.235
	},
	{
		id: 2203,
		time: 2202,
		velocity: 11.8141666666667,
		power: 11060.8484461503,
		road: 18141.3919444444,
		acceleration: 0.831944444444442
	},
	{
		id: 2204,
		time: 2203,
		velocity: 11.7438888888889,
		power: 4161.95464959018,
		road: 18153.144212963,
		acceleration: 0.16601851851852
	},
	{
		id: 2205,
		time: 2204,
		velocity: 11.9477777777778,
		power: 5652.64413125731,
		road: 18165.1226851852,
		acceleration: 0.28638888888889
	},
	{
		id: 2206,
		time: 2205,
		velocity: 12.6733333333333,
		power: 9457.77529159116,
		road: 18177.5364351852,
		acceleration: 0.584166666666665
	},
	{
		id: 2207,
		time: 2206,
		velocity: 13.4963888888889,
		power: 12288.5278117609,
		road: 18190.6221759259,
		acceleration: 0.759814814814815
	},
	{
		id: 2208,
		time: 2207,
		velocity: 14.2272222222222,
		power: 12402.3500479928,
		road: 18204.4402314815,
		acceleration: 0.704814814814815
	},
	{
		id: 2209,
		time: 2208,
		velocity: 14.7877777777778,
		power: 10768.9936394044,
		road: 18218.8786111111,
		acceleration: 0.535833333333333
	},
	{
		id: 2210,
		time: 2209,
		velocity: 15.1038888888889,
		power: 10495.531789355,
		road: 18233.8256481481,
		acceleration: 0.481481481481481
	},
	{
		id: 2211,
		time: 2210,
		velocity: 15.6716666666667,
		power: 7691.4905847537,
		road: 18249.1462037037,
		acceleration: 0.265555555555556
	},
	{
		id: 2212,
		time: 2211,
		velocity: 15.5844444444444,
		power: 3768.88449929382,
		road: 18264.5961574074,
		acceleration: -0.00675925925926002
	},
	{
		id: 2213,
		time: 2212,
		velocity: 15.0836111111111,
		power: 133.591100284948,
		road: 18279.9174074074,
		acceleration: -0.250648148148148
	},
	{
		id: 2214,
		time: 2213,
		velocity: 14.9197222222222,
		power: -647.349908854511,
		road: 18294.963287037,
		acceleration: -0.300092592592591
	},
	{
		id: 2215,
		time: 2214,
		velocity: 14.6841666666667,
		power: 1254.97349669761,
		road: 18309.7778703704,
		acceleration: -0.162500000000001
	},
	{
		id: 2216,
		time: 2215,
		velocity: 14.5961111111111,
		power: 1476.1035012492,
		road: 18324.4395833333,
		acceleration: -0.14324074074074
	},
	{
		id: 2217,
		time: 2216,
		velocity: 14.49,
		power: 2105.61529193892,
		road: 18338.9821296296,
		acceleration: -0.095092592592593
	},
	{
		id: 2218,
		time: 2217,
		velocity: 14.3988888888889,
		power: 2333.3278145599,
		road: 18353.4389814815,
		acceleration: -0.0762962962962952
	},
	{
		id: 2219,
		time: 2218,
		velocity: 14.3672222222222,
		power: 3037.73120299193,
		road: 18367.8458333333,
		acceleration: -0.0237037037037027
	},
	{
		id: 2220,
		time: 2219,
		velocity: 14.4188888888889,
		power: 4576.28209328916,
		road: 18382.2842592593,
		acceleration: 0.0868518518518506
	},
	{
		id: 2221,
		time: 2220,
		velocity: 14.6594444444444,
		power: 5279.83365051083,
		road: 18396.8326851852,
		acceleration: 0.133148148148148
	},
	{
		id: 2222,
		time: 2221,
		velocity: 14.7666666666667,
		power: 5505.2842324845,
		road: 18411.5193518519,
		acceleration: 0.143333333333333
	},
	{
		id: 2223,
		time: 2222,
		velocity: 14.8488888888889,
		power: 4769.0502720357,
		road: 18426.3208333333,
		acceleration: 0.0862962962962968
	},
	{
		id: 2224,
		time: 2223,
		velocity: 14.9183333333333,
		power: 5631.84011578807,
		road: 18441.2366203704,
		acceleration: 0.142314814814817
	},
	{
		id: 2225,
		time: 2224,
		velocity: 15.1936111111111,
		power: 4814.44157552146,
		road: 18456.2638425926,
		acceleration: 0.0805555555555539
	},
	{
		id: 2226,
		time: 2225,
		velocity: 15.0905555555556,
		power: 5891.7821712906,
		road: 18471.4065740741,
		acceleration: 0.150462962962962
	},
	{
		id: 2227,
		time: 2226,
		velocity: 15.3697222222222,
		power: 4839.18950145762,
		road: 18486.6612037037,
		acceleration: 0.0733333333333324
	},
	{
		id: 2228,
		time: 2227,
		velocity: 15.4136111111111,
		power: 5025.03462655397,
		road: 18501.9939814815,
		acceleration: 0.0829629629629647
	},
	{
		id: 2229,
		time: 2228,
		velocity: 15.3394444444444,
		power: 2220.96226314001,
		road: 18517.314212963,
		acceleration: -0.108055555555557
	},
	{
		id: 2230,
		time: 2229,
		velocity: 15.0455555555556,
		power: -420.255439458902,
		road: 18532.4376851852,
		acceleration: -0.285462962962962
	},
	{
		id: 2231,
		time: 2230,
		velocity: 14.5572222222222,
		power: -2841.33349270973,
		road: 18547.1925462963,
		acceleration: -0.45175925925926
	},
	{
		id: 2232,
		time: 2231,
		velocity: 13.9841666666667,
		power: -2600.11603224667,
		road: 18561.5049537037,
		acceleration: -0.433148148148147
	},
	{
		id: 2233,
		time: 2232,
		velocity: 13.7461111111111,
		power: -1383.70912237753,
		road: 18575.4303703704,
		acceleration: -0.340833333333332
	},
	{
		id: 2234,
		time: 2233,
		velocity: 13.5347222222222,
		power: 579.27127343454,
		road: 18589.09125,
		acceleration: -0.188240740740742
	},
	{
		id: 2235,
		time: 2234,
		velocity: 13.4194444444444,
		power: 1627.63746479212,
		road: 18602.6058796296,
		acceleration: -0.104259259259258
	},
	{
		id: 2236,
		time: 2235,
		velocity: 13.4333333333333,
		power: 2972.84489853507,
		road: 18616.069212963,
		acceleration: 0.00166666666666515
	},
	{
		id: 2237,
		time: 2236,
		velocity: 13.5397222222222,
		power: 3523.55882145532,
		road: 18629.5552314815,
		acceleration: 0.043703703703704
	},
	{
		id: 2238,
		time: 2237,
		velocity: 13.5505555555556,
		power: 3625.24653399699,
		road: 18643.0880555556,
		acceleration: 0.0499074074074084
	},
	{
		id: 2239,
		time: 2238,
		velocity: 13.5830555555556,
		power: 3523.96299738242,
		road: 18656.6660648148,
		acceleration: 0.0404629629629625
	},
	{
		id: 2240,
		time: 2239,
		velocity: 13.6611111111111,
		power: 3492.38132052978,
		road: 18670.2826388889,
		acceleration: 0.0366666666666653
	},
	{
		id: 2241,
		time: 2240,
		velocity: 13.6605555555556,
		power: 2447.40373706126,
		road: 18683.8957407407,
		acceleration: -0.0436111111111082
	},
	{
		id: 2242,
		time: 2241,
		velocity: 13.4522222222222,
		power: 2080.67250840183,
		road: 18697.4518981481,
		acceleration: -0.0702777777777808
	},
	{
		id: 2243,
		time: 2242,
		velocity: 13.4502777777778,
		power: 1269.99884870811,
		road: 18710.9075925926,
		acceleration: -0.130648148148145
	},
	{
		id: 2244,
		time: 2243,
		velocity: 13.2686111111111,
		power: 262.302383482951,
		road: 18724.1948148148,
		acceleration: -0.206296296296296
	},
	{
		id: 2245,
		time: 2244,
		velocity: 12.8333333333333,
		power: -1442.52845124224,
		road: 18737.2093518519,
		acceleration: -0.339074074074075
	},
	{
		id: 2246,
		time: 2245,
		velocity: 12.4330555555556,
		power: -1829.57630849225,
		road: 18749.8697222222,
		acceleration: -0.369259259259259
	},
	{
		id: 2247,
		time: 2246,
		velocity: 12.1608333333333,
		power: -1732.46694623764,
		road: 18762.1652777778,
		acceleration: -0.36037037037037
	},
	{
		id: 2248,
		time: 2247,
		velocity: 11.7522222222222,
		power: -1554.90515501015,
		road: 18774.1084722222,
		acceleration: -0.344351851851853
	},
	{
		id: 2249,
		time: 2248,
		velocity: 11.4,
		power: -2182.77014568669,
		road: 18785.6791203704,
		acceleration: -0.400740740740741
	},
	{
		id: 2250,
		time: 2249,
		velocity: 10.9586111111111,
		power: -1509.75902477875,
		road: 18796.8796296296,
		acceleration: -0.339537037037038
	},
	{
		id: 2251,
		time: 2250,
		velocity: 10.7336111111111,
		power: -3585.46366792926,
		road: 18807.639212963,
		acceleration: -0.542314814814814
	},
	{
		id: 2252,
		time: 2251,
		velocity: 9.77305555555555,
		power: -3655.75721415718,
		road: 18817.8466203704,
		acceleration: -0.562037037037038
	},
	{
		id: 2253,
		time: 2252,
		velocity: 9.2725,
		power: -5541.01424345742,
		road: 18827.3785185185,
		acceleration: -0.78898148148148
	},
	{
		id: 2254,
		time: 2253,
		velocity: 8.36666666666667,
		power: -4573.96979018211,
		road: 18836.1568518518,
		acceleration: -0.718148148148149
	},
	{
		id: 2255,
		time: 2254,
		velocity: 7.61861111111111,
		power: -7258.78107085269,
		road: 18844.0092592593,
		acceleration: -1.1337037037037
	},
	{
		id: 2256,
		time: 2255,
		velocity: 5.87138888888889,
		power: -6428.3380039114,
		road: 18850.7143055556,
		acceleration: -1.16101851851852
	},
	{
		id: 2257,
		time: 2256,
		velocity: 4.88361111111111,
		power: -5314.84135276398,
		road: 18856.2624074074,
		acceleration: -1.15287037037037
	},
	{
		id: 2258,
		time: 2257,
		velocity: 4.16,
		power: -3620.68042244935,
		road: 18860.7388888889,
		acceleration: -0.99037037037037
	},
	{
		id: 2259,
		time: 2258,
		velocity: 2.90027777777778,
		power: -2296.15215086821,
		road: 18864.3146296296,
		acceleration: -0.811111111111111
	},
	{
		id: 2260,
		time: 2259,
		velocity: 2.45027777777778,
		power: -1480.98450186104,
		road: 18867.1429166667,
		acceleration: -0.683796296296296
	},
	{
		id: 2261,
		time: 2260,
		velocity: 2.10861111111111,
		power: -865.585750255813,
		road: 18869.3582407407,
		acceleration: -0.54212962962963
	},
	{
		id: 2262,
		time: 2261,
		velocity: 1.27388888888889,
		power: -582.22939435387,
		road: 18871.0572685185,
		acceleration: -0.490462962962963
	},
	{
		id: 2263,
		time: 2262,
		velocity: 0.978888888888889,
		power: -437.326391165422,
		road: 18872.254212963,
		acceleration: -0.513703703703704
	},
	{
		id: 2264,
		time: 2263,
		velocity: 0.5675,
		power: -115.12602419327,
		road: 18873.0544907407,
		acceleration: -0.27962962962963
	},
	{
		id: 2265,
		time: 2264,
		velocity: 0.435,
		power: -93.6166748984416,
		road: 18873.5518055556,
		acceleration: -0.326296296296296
	},
	{
		id: 2266,
		time: 2265,
		velocity: 0,
		power: 39.9751120611951,
		road: 18873.8854166667,
		acceleration: -0.00111111111111106
	},
	{
		id: 2267,
		time: 2266,
		velocity: 0.564166666666667,
		power: 589.290197110694,
		road: 18874.5935648148,
		acceleration: 0.750185185185185
	},
	{
		id: 2268,
		time: 2267,
		velocity: 2.68555555555556,
		power: 2693.6605279755,
		road: 18876.39875,
		acceleration: 1.44388888888889
	},
	{
		id: 2269,
		time: 2268,
		velocity: 4.33166666666667,
		power: 4981.83584570466,
		road: 18879.6630092593,
		acceleration: 1.47425925925926
	},
	{
		id: 2270,
		time: 2269,
		velocity: 4.98694444444444,
		power: 4528.85751082325,
		road: 18884.1288888889,
		acceleration: 0.928981481481481
	},
	{
		id: 2271,
		time: 2270,
		velocity: 5.4725,
		power: 3828.08667192752,
		road: 18889.3719907407,
		acceleration: 0.625462962962963
	},
	{
		id: 2272,
		time: 2271,
		velocity: 6.20805555555556,
		power: 6120.36116058611,
		road: 18895.3888888889,
		acceleration: 0.922129629629629
	},
	{
		id: 2273,
		time: 2272,
		velocity: 7.75333333333333,
		power: 6742.7953853696,
		road: 18902.3023611111,
		acceleration: 0.871018518518518
	},
	{
		id: 2274,
		time: 2273,
		velocity: 8.08555555555555,
		power: 6736.88399756826,
		road: 18910.0289351852,
		acceleration: 0.755185185185185
	},
	{
		id: 2275,
		time: 2274,
		velocity: 8.47361111111111,
		power: 2929.34712816614,
		road: 18918.2378703704,
		acceleration: 0.209537037037038
	},
	{
		id: 2276,
		time: 2275,
		velocity: 8.38194444444444,
		power: 1692.93936857678,
		road: 18926.5749537037,
		acceleration: 0.0467592592592592
	},
	{
		id: 2277,
		time: 2276,
		velocity: 8.22583333333333,
		power: 2913.56925726581,
		road: 18935.0325,
		acceleration: 0.194166666666666
	},
	{
		id: 2278,
		time: 2277,
		velocity: 9.05611111111111,
		power: 4186.56124091166,
		road: 18943.7540740741,
		acceleration: 0.333888888888888
	},
	{
		id: 2279,
		time: 2278,
		velocity: 9.38361111111111,
		power: 4391.4159972974,
		road: 18952.8103703704,
		acceleration: 0.335555555555557
	},
	{
		id: 2280,
		time: 2279,
		velocity: 9.2325,
		power: 2116.15354132776,
		road: 18962.0666203704,
		acceleration: 0.0643518518518533
	},
	{
		id: 2281,
		time: 2280,
		velocity: 9.24916666666667,
		power: 4113.03349804751,
		road: 18971.4952777778,
		acceleration: 0.280462962962961
	},
	{
		id: 2282,
		time: 2281,
		velocity: 10.225,
		power: 6585.28054095452,
		road: 18981.3248148148,
		acceleration: 0.521296296296297
	},
	{
		id: 2283,
		time: 2282,
		velocity: 10.7963888888889,
		power: 10410.3592089793,
		road: 18991.8394907407,
		acceleration: 0.848981481481482
	},
	{
		id: 2284,
		time: 2283,
		velocity: 11.7961111111111,
		power: 11026.9941288759,
		road: 19003.1881018518,
		acceleration: 0.818888888888889
	},
	{
		id: 2285,
		time: 2284,
		velocity: 12.6816666666667,
		power: 13645.727738673,
		road: 19015.4246296296,
		acceleration: 0.956944444444442
	},
	{
		id: 2286,
		time: 2285,
		velocity: 13.6672222222222,
		power: 13287.737411644,
		road: 19028.5572685185,
		acceleration: 0.83527777777778
	},
	{
		id: 2287,
		time: 2286,
		velocity: 14.3019444444444,
		power: 12341.5171247298,
		road: 19042.4543518518,
		acceleration: 0.693611111111112
	},
	{
		id: 2288,
		time: 2287,
		velocity: 14.7625,
		power: 7265.82355415377,
		road: 19056.8405555555,
		acceleration: 0.284629629629629
	},
	{
		id: 2289,
		time: 2288,
		velocity: 14.5211111111111,
		power: 4536.32481799747,
		road: 19071.4085185185,
		acceleration: 0.0788888888888888
	},
	{
		id: 2290,
		time: 2289,
		velocity: 14.5386111111111,
		power: 2022.05760538723,
		road: 19085.9651851852,
		acceleration: -0.10148148148148
	},
	{
		id: 2291,
		time: 2290,
		velocity: 14.4580555555556,
		power: 2076.92459140415,
		road: 19100.4236574074,
		acceleration: -0.0949074074074101
	},
	{
		id: 2292,
		time: 2291,
		velocity: 14.2363888888889,
		power: 2565.33310522661,
		road: 19114.8060185185,
		acceleration: -0.0573148148148146
	},
	{
		id: 2293,
		time: 2292,
		velocity: 14.3666666666667,
		power: 5168.96383377784,
		road: 19129.2250462963,
		acceleration: 0.130648148148147
	},
	{
		id: 2294,
		time: 2293,
		velocity: 14.85,
		power: 5678.7051628128,
		road: 19143.7899537037,
		acceleration: 0.161111111111111
	},
	{
		id: 2295,
		time: 2294,
		velocity: 14.7197222222222,
		power: 4765.69996918777,
		road: 19158.4806481481,
		acceleration: 0.090462962962965
	},
	{
		id: 2296,
		time: 2295,
		velocity: 14.6380555555556,
		power: 2603.55818390126,
		road: 19173.1845833333,
		acceleration: -0.0639814814814823
	},
	{
		id: 2297,
		time: 2296,
		velocity: 14.6580555555556,
		power: 3023.22018661621,
		road: 19187.8402314815,
		acceleration: -0.032592592592593
	},
	{
		id: 2298,
		time: 2297,
		velocity: 14.6219444444444,
		power: 3508.7509036489,
		road: 19202.4808796296,
		acceleration: 0.00259259259259537
	},
	{
		id: 2299,
		time: 2298,
		velocity: 14.6458333333333,
		power: 3631.18917770241,
		road: 19217.1283796296,
		acceleration: 0.0111111111111093
	},
	{
		id: 2300,
		time: 2299,
		velocity: 14.6913888888889,
		power: 1875.38755163132,
		road: 19231.7249074074,
		acceleration: -0.113055555555555
	},
	{
		id: 2301,
		time: 2300,
		velocity: 14.2827777777778,
		power: -359.54478340497,
		road: 19246.1296296296,
		acceleration: -0.270555555555557
	},
	{
		id: 2302,
		time: 2301,
		velocity: 13.8341666666667,
		power: -1849.32651070382,
		road: 19260.2106944444,
		acceleration: -0.376759259259259
	},
	{
		id: 2303,
		time: 2302,
		velocity: 13.5611111111111,
		power: -4807.74575695535,
		road: 19273.8023148148,
		acceleration: -0.60212962962963
	},
	{
		id: 2304,
		time: 2303,
		velocity: 12.4763888888889,
		power: -9029.13786387983,
		road: 19286.6135648148,
		acceleration: -0.958611111111111
	},
	{
		id: 2305,
		time: 2304,
		velocity: 10.9583333333333,
		power: -11497.519704477,
		road: 19298.3284722222,
		acceleration: -1.23407407407408
	},
	{
		id: 2306,
		time: 2305,
		velocity: 9.85888888888889,
		power: -8418.80042671851,
		road: 19308.91375,
		acceleration: -1.02518518518518
	},
	{
		id: 2307,
		time: 2306,
		velocity: 9.40083333333333,
		power: -4153.97595861537,
		road: 19318.6724537037,
		acceleration: -0.627962962962965
	},
	{
		id: 2308,
		time: 2307,
		velocity: 9.07444444444444,
		power: -3826.64535220805,
		road: 19327.8100462963,
		acceleration: -0.614259259259258
	},
	{
		id: 2309,
		time: 2308,
		velocity: 8.01611111111111,
		power: -6340.73515197734,
		road: 19336.1581481481,
		acceleration: -0.964722222222223
	},
	{
		id: 2310,
		time: 2309,
		velocity: 6.50666666666667,
		power: -7727.27736626345,
		road: 19343.3832407407,
		acceleration: -1.2812962962963
	},
	{
		id: 2311,
		time: 2310,
		velocity: 5.23055555555556,
		power: -4686.31508748015,
		road: 19349.4898148148,
		acceleration: -0.95574074074074
	},
	{
		id: 2312,
		time: 2311,
		velocity: 5.14888888888889,
		power: -1611.50165170615,
		road: 19354.8895833333,
		acceleration: -0.457870370370371
	},
	{
		id: 2313,
		time: 2312,
		velocity: 5.13305555555556,
		power: 1096.84695334148,
		road: 19360.0998611111,
		acceleration: 0.0788888888888888
	},
	{
		id: 2314,
		time: 2313,
		velocity: 5.46722222222222,
		power: 2939.47133191755,
		road: 19365.5606944444,
		acceleration: 0.422222222222223
	},
	{
		id: 2315,
		time: 2314,
		velocity: 6.41555555555556,
		power: 6379.84260588528,
		road: 19371.7043981481,
		acceleration: 0.943518518518518
	},
	{
		id: 2316,
		time: 2315,
		velocity: 7.96361111111111,
		power: 8101.63538969864,
		road: 19378.8386574074,
		acceleration: 1.03759259259259
	},
	{
		id: 2317,
		time: 2316,
		velocity: 8.58,
		power: 6425.91327597236,
		road: 19386.8323148148,
		acceleration: 0.681203703703702
	},
	{
		id: 2318,
		time: 2317,
		velocity: 8.45916666666667,
		power: 4168.433883196,
		road: 19395.3398148148,
		acceleration: 0.346481481481481
	},
	{
		id: 2319,
		time: 2318,
		velocity: 9.00305555555556,
		power: 4529.3640142518,
		road: 19404.2030555555,
		acceleration: 0.365
	},
	{
		id: 2320,
		time: 2319,
		velocity: 9.675,
		power: 4585.59521892763,
		road: 19413.4222685185,
		acceleration: 0.346944444444445
	},
	{
		id: 2321,
		time: 2320,
		velocity: 9.5,
		power: 2519.43028024735,
		road: 19422.8661574074,
		acceleration: 0.102407407407407
	},
	{
		id: 2322,
		time: 2321,
		velocity: 9.31027777777778,
		power: 654.37347899651,
		road: 19432.3087962963,
		acceleration: -0.104907407407405
	},
	{
		id: 2323,
		time: 2322,
		velocity: 9.36027777777778,
		power: 1482.36649400521,
		road: 19441.6933796296,
		acceleration: -0.0112037037037052
	},
	{
		id: 2324,
		time: 2323,
		velocity: 9.46638888888889,
		power: 3872.39200994365,
		road: 19451.1970833333,
		acceleration: 0.249444444444444
	},
	{
		id: 2325,
		time: 2324,
		velocity: 10.0586111111111,
		power: 4821.24168074423,
		road: 19460.9930555555,
		acceleration: 0.335092592592591
	},
	{
		id: 2326,
		time: 2325,
		velocity: 10.3655555555556,
		power: 4344.31696896617,
		road: 19471.0899074074,
		acceleration: 0.266666666666668
	},
	{
		id: 2327,
		time: 2326,
		velocity: 10.2663888888889,
		power: 1714.66334185583,
		road: 19481.3149074074,
		acceleration: -0.0103703703703708
	},
	{
		id: 2328,
		time: 2327,
		velocity: 10.0275,
		power: -30.1112460662354,
		road: 19491.4405555555,
		acceleration: -0.188333333333334
	},
	{
		id: 2329,
		time: 2328,
		velocity: 9.80055555555555,
		power: 2139.77383905103,
		road: 19501.4915740741,
		acceleration: 0.039074074074076
	},
	{
		id: 2330,
		time: 2329,
		velocity: 10.3836111111111,
		power: 4169.56442820407,
		road: 19511.6837962963,
		acceleration: 0.243333333333332
	},
	{
		id: 2331,
		time: 2330,
		velocity: 10.7575,
		power: 5245.67524282301,
		road: 19522.1655555555,
		acceleration: 0.335740740740741
	},
	{
		id: 2332,
		time: 2331,
		velocity: 10.8077777777778,
		power: 2431.90837736287,
		road: 19532.8388888889,
		acceleration: 0.0474074074074071
	},
	{
		id: 2333,
		time: 2332,
		velocity: 10.5258333333333,
		power: 340.722391543349,
		road: 19543.4572685185,
		acceleration: -0.157314814814814
	},
	{
		id: 2334,
		time: 2333,
		velocity: 10.2855555555556,
		power: -1119.96679304515,
		road: 19553.8462962963,
		acceleration: -0.301388888888891
	},
	{
		id: 2335,
		time: 2334,
		velocity: 9.90361111111111,
		power: -533.478842340491,
		road: 19563.9643981481,
		acceleration: -0.240462962962962
	},
	{
		id: 2336,
		time: 2335,
		velocity: 9.80444444444444,
		power: 1822.98068752243,
		road: 19573.9660185185,
		acceleration: 0.00750000000000028
	},
	{
		id: 2337,
		time: 2336,
		velocity: 10.3080555555556,
		power: 3017.61111517742,
		road: 19584.0363425926,
		acceleration: 0.129907407407408
	},
	{
		id: 2338,
		time: 2337,
		velocity: 10.2933333333333,
		power: 3455.25218599257,
		road: 19594.2558796296,
		acceleration: 0.168518518518518
	},
	{
		id: 2339,
		time: 2338,
		velocity: 10.31,
		power: 1579.7763832388,
		road: 19604.5466666667,
		acceleration: -0.0260185185185193
	},
	{
		id: 2340,
		time: 2339,
		velocity: 10.23,
		power: 322.821759280473,
		road: 19614.7480092592,
		acceleration: -0.152870370370369
	},
	{
		id: 2341,
		time: 2340,
		velocity: 9.83472222222222,
		power: 118.353880359356,
		road: 19624.7869907407,
		acceleration: -0.171851851851851
	},
	{
		id: 2342,
		time: 2341,
		velocity: 9.79444444444444,
		power: 1331.39506950762,
		road: 19634.7188888889,
		acceleration: -0.0423148148148158
	},
	{
		id: 2343,
		time: 2342,
		velocity: 10.1030555555556,
		power: 3648.22755515476,
		road: 19644.7289351852,
		acceleration: 0.198611111111113
	},
	{
		id: 2344,
		time: 2343,
		velocity: 10.4305555555556,
		power: 1820.29239067627,
		road: 19654.8402314815,
		acceleration: 0.00388888888888772
	},
	{
		id: 2345,
		time: 2344,
		velocity: 9.80611111111111,
		power: -4589.89278875877,
		road: 19664.6163888889,
		acceleration: -0.674166666666668
	},
	{
		id: 2346,
		time: 2345,
		velocity: 8.08055555555556,
		power: -9238.55539401658,
		road: 19673.4186574074,
		acceleration: -1.27361111111111
	},
	{
		id: 2347,
		time: 2346,
		velocity: 6.60972222222222,
		power: -11424.1897729232,
		road: 19680.6782407407,
		acceleration: -1.81175925925926
	},
	{
		id: 2348,
		time: 2347,
		velocity: 4.37083333333333,
		power: -7630.10592203065,
		road: 19686.2372685185,
		acceleration: -1.58935185185185
	},
	{
		id: 2349,
		time: 2348,
		velocity: 3.3125,
		power: -4900.86875955115,
		road: 19690.2975925926,
		acceleration: -1.40805555555556
	},
	{
		id: 2350,
		time: 2349,
		velocity: 2.38555555555556,
		power: -2287.31902468378,
		road: 19693.1679166666,
		acceleration: -0.971944444444445
	},
	{
		id: 2351,
		time: 2350,
		velocity: 1.455,
		power: -1177.98246261341,
		road: 19695.1785185185,
		acceleration: -0.7475
	},
	{
		id: 2352,
		time: 2351,
		velocity: 1.07,
		power: -783.374030746401,
		road: 19696.4177777778,
		acceleration: -0.795185185185185
	},
	{
		id: 2353,
		time: 2352,
		velocity: 0,
		power: -48.8299417219405,
		road: 19697.1608796296,
		acceleration: -0.19712962962963
	},
	{
		id: 2354,
		time: 2353,
		velocity: 0.863611111111111,
		power: 26.7197117720019,
		road: 19697.7649074074,
		acceleration: -0.0810185185185185
	},
	{
		id: 2355,
		time: 2354,
		velocity: 0.826944444444444,
		power: 241.36031295064,
		road: 19698.4503240741,
		acceleration: 0.243796296296296
	},
	{
		id: 2356,
		time: 2355,
		velocity: 0.731388888888889,
		power: -100.671125814552,
		road: 19699.1137037037,
		acceleration: -0.28787037037037
	},
	{
		id: 2357,
		time: 2356,
		velocity: 0,
		power: -53.5375941692521,
		road: 19699.4953240741,
		acceleration: -0.275648148148148
	},
	{
		id: 2358,
		time: 2357,
		velocity: 0,
		power: -13.4267174626381,
		road: 19699.6172222222,
		acceleration: -0.243796296296296
	},
	{
		id: 2359,
		time: 2358,
		velocity: 0,
		power: 0,
		road: 19699.6172222222,
		acceleration: 0
	},
	{
		id: 2360,
		time: 2359,
		velocity: 0,
		power: 0,
		road: 19699.6172222222,
		acceleration: 0
	},
	{
		id: 2361,
		time: 2360,
		velocity: 0,
		power: 0,
		road: 19699.6172222222,
		acceleration: 0
	},
	{
		id: 2362,
		time: 2361,
		velocity: 0,
		power: 0,
		road: 19699.6172222222,
		acceleration: 0
	},
	{
		id: 2363,
		time: 2362,
		velocity: 0,
		power: 0,
		road: 19699.6172222222,
		acceleration: 0
	},
	{
		id: 2364,
		time: 2363,
		velocity: 0,
		power: 0,
		road: 19699.6172222222,
		acceleration: 0
	},
	{
		id: 2365,
		time: 2364,
		velocity: 0,
		power: 0,
		road: 19699.6172222222,
		acceleration: 0
	},
	{
		id: 2366,
		time: 2365,
		velocity: 0,
		power: 0,
		road: 19699.6172222222,
		acceleration: 0
	},
	{
		id: 2367,
		time: 2366,
		velocity: 0,
		power: 0,
		road: 19699.6172222222,
		acceleration: 0
	},
	{
		id: 2368,
		time: 2367,
		velocity: 0,
		power: 0,
		road: 19699.6172222222,
		acceleration: 0
	},
	{
		id: 2369,
		time: 2368,
		velocity: 0,
		power: 106.165842937941,
		road: 19699.8241666667,
		acceleration: 0.413888888888889
	},
	{
		id: 2370,
		time: 2369,
		velocity: 1.24166666666667,
		power: 1408.4938847921,
		road: 19700.8785185185,
		acceleration: 1.28092592592593
	},
	{
		id: 2371,
		time: 2370,
		velocity: 3.84277777777778,
		power: 4681.50690139389,
		road: 19703.4623611111,
		acceleration: 1.77805555555556
	},
	{
		id: 2372,
		time: 2371,
		velocity: 5.33416666666667,
		power: 7729.94601633827,
		road: 19707.8034722222,
		acceleration: 1.73648148148148
	},
	{
		id: 2373,
		time: 2372,
		velocity: 6.45111111111111,
		power: 5155.16539165075,
		road: 19713.4228240741,
		acceleration: 0.82
	},
	{
		id: 2374,
		time: 2373,
		velocity: 6.30277777777778,
		power: 233.507164130746,
		road: 19719.3989814815,
		acceleration: -0.10638888888889
	},
	{
		id: 2375,
		time: 2374,
		velocity: 5.015,
		power: -615.008810622473,
		road: 19725.192962963,
		acceleration: -0.257962962962964
	},
	{
		id: 2376,
		time: 2375,
		velocity: 5.67722222222222,
		power: 511.289945784514,
		road: 19730.8330092592,
		acceleration: -0.0499074074074066
	},
	{
		id: 2377,
		time: 2376,
		velocity: 6.15305555555556,
		power: 2023.10364892416,
		road: 19736.5609722222,
		acceleration: 0.225740740740741
	},
	{
		id: 2378,
		time: 2377,
		velocity: 5.69222222222222,
		power: 646.637482852954,
		road: 19742.3869444444,
		acceleration: -0.0297222222222224
	},
	{
		id: 2379,
		time: 2378,
		velocity: 5.58805555555556,
		power: 701.75708347599,
		road: 19748.1885185185,
		acceleration: -0.0190740740740738
	},
	{
		id: 2380,
		time: 2379,
		velocity: 6.09583333333333,
		power: 1498.68594113302,
		road: 19754.0418981481,
		acceleration: 0.122685185185184
	},
	{
		id: 2381,
		time: 2380,
		velocity: 6.06027777777778,
		power: 324.098366106892,
		road: 19759.9122685185,
		acceleration: -0.0887037037037022
	},
	{
		id: 2382,
		time: 2381,
		velocity: 5.32194444444444,
		power: -2653.49090640469,
		road: 19765.4122222222,
		acceleration: -0.652129629629631
	},
	{
		id: 2383,
		time: 2382,
		velocity: 4.13944444444444,
		power: -4063.03003065869,
		road: 19770.0556944444,
		acceleration: -1.06083333333333
	},
	{
		id: 2384,
		time: 2383,
		velocity: 2.87777777777778,
		power: -3042.67783095903,
		road: 19773.65625,
		acceleration: -1.025
	},
	{
		id: 2385,
		time: 2384,
		velocity: 2.24694444444444,
		power: -1681.20162548388,
		road: 19776.3495833333,
		acceleration: -0.789444444444444
	},
	{
		id: 2386,
		time: 2385,
		velocity: 1.77111111111111,
		power: -962.348714352032,
		road: 19778.3267592592,
		acceleration: -0.64287037037037
	},
	{
		id: 2387,
		time: 2386,
		velocity: 0.949166666666667,
		power: -398.925865590514,
		road: 19779.7726851852,
		acceleration: -0.41962962962963
	},
	{
		id: 2388,
		time: 2387,
		velocity: 0.988055555555555,
		power: -412.364535804166,
		road: 19780.7136111111,
		acceleration: -0.59037037037037
	},
	{
		id: 2389,
		time: 2388,
		velocity: 0,
		power: 28.0998952269546,
		road: 19781.3199537037,
		acceleration: -0.0787962962962964
	},
	{
		id: 2390,
		time: 2389,
		velocity: 0.712777777777778,
		power: -76.9012720061413,
		road: 19781.7222222222,
		acceleration: -0.329351851851852
	},
	{
		id: 2391,
		time: 2390,
		velocity: 0,
		power: 28.7127179078593,
		road: 19781.9598148148,
		acceleration: 0
	},
	{
		id: 2392,
		time: 2391,
		velocity: 0,
		power: -12.3868689733593,
		road: 19782.0786111111,
		acceleration: -0.237592592592593
	},
	{
		id: 2393,
		time: 2392,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2394,
		time: 2393,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2395,
		time: 2394,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2396,
		time: 2395,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2397,
		time: 2396,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2398,
		time: 2397,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2399,
		time: 2398,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2400,
		time: 2399,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2401,
		time: 2400,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2402,
		time: 2401,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2403,
		time: 2402,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2404,
		time: 2403,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2405,
		time: 2404,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2406,
		time: 2405,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2407,
		time: 2406,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2408,
		time: 2407,
		velocity: 0,
		power: 0,
		road: 19782.0786111111,
		acceleration: 0
	},
	{
		id: 2409,
		time: 2408,
		velocity: 0,
		power: 173.395855967151,
		road: 19782.3508796296,
		acceleration: 0.544537037037037
	},
	{
		id: 2410,
		time: 2409,
		velocity: 1.63361111111111,
		power: 559.747352190408,
		road: 19783.1853240741,
		acceleration: 0.579814814814815
	},
	{
		id: 2411,
		time: 2410,
		velocity: 1.73944444444444,
		power: 953.212806189481,
		road: 19784.6005555555,
		acceleration: 0.58175925925926
	},
	{
		id: 2412,
		time: 2411,
		velocity: 1.74527777777778,
		power: 240.897783917398,
		road: 19786.3161574074,
		acceleration: 0.0189814814814813
	},
	{
		id: 2413,
		time: 2412,
		velocity: 1.69055555555556,
		power: 116.626078294584,
		road: 19788.012962963,
		acceleration: -0.0565740740740741
	},
	{
		id: 2414,
		time: 2413,
		velocity: 1.56972222222222,
		power: 36.7803147919778,
		road: 19789.6290277778,
		acceleration: -0.104907407407407
	},
	{
		id: 2415,
		time: 2414,
		velocity: 1.43055555555556,
		power: -90.4581255108421,
		road: 19791.095787037,
		acceleration: -0.193703703703704
	},
	{
		id: 2416,
		time: 2415,
		velocity: 1.10944444444444,
		power: -59.3299251585532,
		road: 19792.3770833333,
		acceleration: -0.177222222222222
	},
	{
		id: 2417,
		time: 2416,
		velocity: 1.03805555555556,
		power: -80.4497420916099,
		road: 19793.4667592592,
		acceleration: -0.206018518518518
	},
	{
		id: 2418,
		time: 2417,
		velocity: 0.8125,
		power: -183.864578103204,
		road: 19794.2685185185,
		acceleration: -0.369814814814815
	},
	{
		id: 2419,
		time: 2418,
		velocity: 0,
		power: -91.8529151241213,
		road: 19794.7123611111,
		acceleration: -0.346018518518518
	},
	{
		id: 2420,
		time: 2419,
		velocity: 0,
		power: -18.3843092105263,
		road: 19794.8477777778,
		acceleration: -0.270833333333333
	},
	{
		id: 2421,
		time: 2420,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2422,
		time: 2421,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2423,
		time: 2422,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2424,
		time: 2423,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2425,
		time: 2424,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2426,
		time: 2425,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2427,
		time: 2426,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2428,
		time: 2427,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2429,
		time: 2428,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2430,
		time: 2429,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2431,
		time: 2430,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2432,
		time: 2431,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2433,
		time: 2432,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2434,
		time: 2433,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2435,
		time: 2434,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2436,
		time: 2435,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2437,
		time: 2436,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2438,
		time: 2437,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2439,
		time: 2438,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2440,
		time: 2439,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2441,
		time: 2440,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2442,
		time: 2441,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2443,
		time: 2442,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2444,
		time: 2443,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2445,
		time: 2444,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2446,
		time: 2445,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2447,
		time: 2446,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2448,
		time: 2447,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2449,
		time: 2448,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2450,
		time: 2449,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2451,
		time: 2450,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2452,
		time: 2451,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2453,
		time: 2452,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2454,
		time: 2453,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2455,
		time: 2454,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2456,
		time: 2455,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2457,
		time: 2456,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2458,
		time: 2457,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2459,
		time: 2458,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2460,
		time: 2459,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2461,
		time: 2460,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2462,
		time: 2461,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2463,
		time: 2462,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2464,
		time: 2463,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2465,
		time: 2464,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2466,
		time: 2465,
		velocity: 0,
		power: 0,
		road: 19794.8477777778,
		acceleration: 0
	},
	{
		id: 2467,
		time: 2466,
		velocity: 0,
		power: 163.814942077796,
		road: 19795.1116203704,
		acceleration: 0.527685185185185
	},
	{
		id: 2468,
		time: 2467,
		velocity: 1.58305555555556,
		power: 1449.33320407486,
		road: 19796.2478703704,
		acceleration: 1.21712962962963
	},
	{
		id: 2469,
		time: 2468,
		velocity: 3.65138888888889,
		power: 4813.73014959933,
		road: 19798.8877314815,
		acceleration: 1.79009259259259
	},
	{
		id: 2470,
		time: 2469,
		velocity: 5.37027777777778,
		power: 7070.00127501618,
		road: 19803.2138888889,
		acceleration: 1.5825
	},
	{
		id: 2471,
		time: 2470,
		velocity: 6.33055555555555,
		power: 5341.8667572112,
		road: 19808.7651388889,
		acceleration: 0.867685185185184
	},
	{
		id: 2472,
		time: 2471,
		velocity: 6.25444444444444,
		power: 3354.30845234443,
		road: 19814.960462963,
		acceleration: 0.420462962962964
	},
	{
		id: 2473,
		time: 2472,
		velocity: 6.63166666666667,
		power: 3534.88026833865,
		road: 19821.5711574074,
		acceleration: 0.410277777777778
	},
	{
		id: 2474,
		time: 2473,
		velocity: 7.56138888888889,
		power: 2895.94677547096,
		road: 19828.5284722222,
		acceleration: 0.282962962962962
	},
	{
		id: 2475,
		time: 2474,
		velocity: 7.10333333333333,
		power: 1562.84683712245,
		road: 19835.6643518518,
		acceleration: 0.0741666666666676
	},
	{
		id: 2476,
		time: 2475,
		velocity: 6.85416666666667,
		power: -862.199802760983,
		road: 19842.6952314815,
		acceleration: -0.284166666666667
	},
	{
		id: 2477,
		time: 2476,
		velocity: 6.70888888888889,
		power: 34.377784603111,
		road: 19849.5099074074,
		acceleration: -0.14824074074074
	},
	{
		id: 2478,
		time: 2477,
		velocity: 6.65861111111111,
		power: -731.009942099252,
		road: 19856.1162962963,
		acceleration: -0.268333333333334
	},
	{
		id: 2479,
		time: 2478,
		velocity: 6.04916666666667,
		power: -3021.10388368943,
		road: 19862.2553703704,
		acceleration: -0.666296296296297
	},
	{
		id: 2480,
		time: 2479,
		velocity: 4.71,
		power: -5649.57340342803,
		road: 19867.4136574074,
		acceleration: -1.29527777777778
	},
	{
		id: 2481,
		time: 2480,
		velocity: 2.77277777777778,
		power: -4814.11950997251,
		road: 19871.1840277778,
		acceleration: -1.48055555555555
	},
	{
		id: 2482,
		time: 2481,
		velocity: 1.6075,
		power: -3065.43415193494,
		road: 19873.4291203704,
		acceleration: -1.57
	},
	{
		id: 2483,
		time: 2482,
		velocity: 0,
		power: -745.866400863757,
		road: 19874.4333796296,
		acceleration: -0.911666666666667
	},
	{
		id: 2484,
		time: 2483,
		velocity: 0.0377777777777778,
		power: -108.504795881651,
		road: 19874.7138888889,
		acceleration: -0.535833333333333
	},
	{
		id: 2485,
		time: 2484,
		velocity: 0,
		power: 1.52141161034684,
		road: 19874.7264814815,
		acceleration: 0
	},
	{
		id: 2486,
		time: 2485,
		velocity: 0,
		power: 0.685591552956465,
		road: 19874.7327777778,
		acceleration: -0.0125925925925926
	},
	{
		id: 2487,
		time: 2486,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2488,
		time: 2487,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2489,
		time: 2488,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2490,
		time: 2489,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2491,
		time: 2490,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2492,
		time: 2491,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2493,
		time: 2492,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2494,
		time: 2493,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2495,
		time: 2494,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2496,
		time: 2495,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2497,
		time: 2496,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2498,
		time: 2497,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2499,
		time: 2498,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2500,
		time: 2499,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2501,
		time: 2500,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2502,
		time: 2501,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2503,
		time: 2502,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2504,
		time: 2503,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2505,
		time: 2504,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2506,
		time: 2505,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2507,
		time: 2506,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2508,
		time: 2507,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2509,
		time: 2508,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2510,
		time: 2509,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2511,
		time: 2510,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2512,
		time: 2511,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2513,
		time: 2512,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2514,
		time: 2513,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2515,
		time: 2514,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2516,
		time: 2515,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2517,
		time: 2516,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2518,
		time: 2517,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2519,
		time: 2518,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2520,
		time: 2519,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2521,
		time: 2520,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2522,
		time: 2521,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2523,
		time: 2522,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2524,
		time: 2523,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2525,
		time: 2524,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2526,
		time: 2525,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2527,
		time: 2526,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2528,
		time: 2527,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2529,
		time: 2528,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2530,
		time: 2529,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2531,
		time: 2530,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2532,
		time: 2531,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2533,
		time: 2532,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2534,
		time: 2533,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2535,
		time: 2534,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2536,
		time: 2535,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2537,
		time: 2536,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2538,
		time: 2537,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2539,
		time: 2538,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2540,
		time: 2539,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2541,
		time: 2540,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2542,
		time: 2541,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2543,
		time: 2542,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2544,
		time: 2543,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2545,
		time: 2544,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2546,
		time: 2545,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2547,
		time: 2546,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2548,
		time: 2547,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2549,
		time: 2548,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2550,
		time: 2549,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2551,
		time: 2550,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2552,
		time: 2551,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2553,
		time: 2552,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2554,
		time: 2553,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2555,
		time: 2554,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2556,
		time: 2555,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2557,
		time: 2556,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2558,
		time: 2557,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2559,
		time: 2558,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2560,
		time: 2559,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2561,
		time: 2560,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2562,
		time: 2561,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2563,
		time: 2562,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2564,
		time: 2563,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2565,
		time: 2564,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2566,
		time: 2565,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2567,
		time: 2566,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2568,
		time: 2567,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2569,
		time: 2568,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2570,
		time: 2569,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2571,
		time: 2570,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2572,
		time: 2571,
		velocity: 0,
		power: 0,
		road: 19874.7327777778,
		acceleration: 0
	},
	{
		id: 2573,
		time: 2572,
		velocity: 0,
		power: 51.4935326929451,
		road: 19874.8687962963,
		acceleration: 0.272037037037037
	},
	{
		id: 2574,
		time: 2573,
		velocity: 0.816111111111111,
		power: 573.913012062156,
		road: 19875.5328703704,
		acceleration: 0.784074074074074
	},
	{
		id: 2575,
		time: 2574,
		velocity: 2.35222222222222,
		power: 2686.5093454877,
		road: 19877.3177777778,
		acceleration: 1.45759259259259
	},
	{
		id: 2576,
		time: 2575,
		velocity: 4.37277777777778,
		power: 5221.49288771622,
		road: 19880.6020833333,
		acceleration: 1.5412037037037
	},
	{
		id: 2577,
		time: 2576,
		velocity: 5.43972222222222,
		power: 5098.73666067751,
		road: 19885.1743518518,
		acceleration: 1.03472222222222
	},
	{
		id: 2578,
		time: 2577,
		velocity: 5.45638888888889,
		power: 3390.17017736363,
		road: 19890.5255555555,
		acceleration: 0.523148148148149
	},
	{
		id: 2579,
		time: 2578,
		velocity: 5.94222222222222,
		power: 2729.0198148049,
		road: 19896.3132407407,
		acceleration: 0.349814814814814
	},
	{
		id: 2580,
		time: 2579,
		velocity: 6.48916666666667,
		power: 2454.52218663708,
		road: 19902.413287037,
		acceleration: 0.274907407407407
	},
	{
		id: 2581,
		time: 2580,
		velocity: 6.28111111111111,
		power: 1365.69584697022,
		road: 19908.6904166667,
		acceleration: 0.0792592592592598
	},
	{
		id: 2582,
		time: 2581,
		velocity: 6.18,
		power: 1707.89647149678,
		road: 19915.0727314815,
		acceleration: 0.13111111111111
	},
	{
		id: 2583,
		time: 2582,
		velocity: 6.8825,
		power: 3500.17891438496,
		road: 19921.72125,
		acceleration: 0.401296296296296
	},
	{
		id: 2584,
		time: 2583,
		velocity: 7.485,
		power: 3929.84055602667,
		road: 19928.7850925926,
		acceleration: 0.429351851851854
	},
	{
		id: 2585,
		time: 2584,
		velocity: 7.46805555555556,
		power: 1564.61274350787,
		road: 19936.0973148148,
		acceleration: 0.0674074074074058
	},
	{
		id: 2586,
		time: 2585,
		velocity: 7.08472222222222,
		power: 1838.79917460144,
		road: 19943.4947685185,
		acceleration: 0.103055555555556
	},
	{
		id: 2587,
		time: 2586,
		velocity: 7.79416666666667,
		power: 7230.46200854418,
		road: 19951.3464814815,
		acceleration: 0.805462962962964
	},
	{
		id: 2588,
		time: 2587,
		velocity: 9.88444444444445,
		power: 13738.3721084968,
		road: 19960.3184259259,
		acceleration: 1.435
	},
	{
		id: 2589,
		time: 2588,
		velocity: 11.3897222222222,
		power: 19173.3369509283,
		road: 19970.8662037037,
		acceleration: 1.71666666666667
	},
	{
		id: 2590,
		time: 2589,
		velocity: 12.9441666666667,
		power: 15575.618471566,
		road: 19982.8493518518,
		acceleration: 1.15407407407407
	},
	{
		id: 2591,
		time: 2590,
		velocity: 13.3466666666667,
		power: 8542.98069100489,
		road: 19995.6493055555,
		acceleration: 0.479537037037037
	},
	{
		id: 2592,
		time: 2591,
		velocity: 12.8283333333333,
		power: 338.855819245387,
		road: 20008.5918055555,
		acceleration: -0.194444444444446
	},
	{
		id: 2593,
		time: 2592,
		velocity: 12.3608333333333,
		power: -3414.54524110827,
		road: 20021.1865740741,
		acceleration: -0.501018518518515
	},
	{
		id: 2594,
		time: 2593,
		velocity: 11.8436111111111,
		power: -5293.72910181851,
		road: 20033.1953703704,
		acceleration: -0.670925925925928
	},
	{
		id: 2595,
		time: 2594,
		velocity: 10.8155555555556,
		power: -7755.10012705928,
		road: 20044.4067592593,
		acceleration: -0.923888888888888
	},
	{
		id: 2596,
		time: 2595,
		velocity: 9.58916666666667,
		power: -12493.2963451086,
		road: 20054.4086574074,
		acceleration: -1.49509259259259
	},
	{
		id: 2597,
		time: 2596,
		velocity: 7.35833333333333,
		power: -10606.304222847,
		road: 20062.9245833333,
		acceleration: -1.47685185185185
	},
	{
		id: 2598,
		time: 2597,
		velocity: 6.385,
		power: -9937.66338484959,
		road: 20069.8727314815,
		acceleration: -1.6587037037037
	},
	{
		id: 2599,
		time: 2598,
		velocity: 4.61305555555556,
		power: -6261.31793061447,
		road: 20075.3139351852,
		acceleration: -1.35518518518519
	},
	{
		id: 2600,
		time: 2599,
		velocity: 3.29277777777778,
		power: -4924.68870663919,
		road: 20079.3697222222,
		acceleration: -1.41564814814815
	},
	{
		id: 2601,
		time: 2600,
		velocity: 2.13805555555556,
		power: -2973.13854269392,
		road: 20082.072037037,
		acceleration: -1.2912962962963
	},
	{
		id: 2602,
		time: 2601,
		velocity: 0.739166666666667,
		power: -1384.9899755517,
		road: 20083.5799074074,
		acceleration: -1.09759259259259
	},
	{
		id: 2603,
		time: 2602,
		velocity: 0,
		power: -334.108879878968,
		road: 20084.1826388889,
		acceleration: -0.712685185185185
	},
	{
		id: 2604,
		time: 2603,
		velocity: 0,
		power: -13.8720834795322,
		road: 20084.3058333333,
		acceleration: -0.246388888888889
	},
	{
		id: 2605,
		time: 2604,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2606,
		time: 2605,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2607,
		time: 2606,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2608,
		time: 2607,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2609,
		time: 2608,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2610,
		time: 2609,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2611,
		time: 2610,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2612,
		time: 2611,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2613,
		time: 2612,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2614,
		time: 2613,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2615,
		time: 2614,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2616,
		time: 2615,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2617,
		time: 2616,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2618,
		time: 2617,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2619,
		time: 2618,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2620,
		time: 2619,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2621,
		time: 2620,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2622,
		time: 2621,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2623,
		time: 2622,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2624,
		time: 2623,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2625,
		time: 2624,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2626,
		time: 2625,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2627,
		time: 2626,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2628,
		time: 2627,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2629,
		time: 2628,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2630,
		time: 2629,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2631,
		time: 2630,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2632,
		time: 2631,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2633,
		time: 2632,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2634,
		time: 2633,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2635,
		time: 2634,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2636,
		time: 2635,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2637,
		time: 2636,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2638,
		time: 2637,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2639,
		time: 2638,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2640,
		time: 2639,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2641,
		time: 2640,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2642,
		time: 2641,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2643,
		time: 2642,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2644,
		time: 2643,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2645,
		time: 2644,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2646,
		time: 2645,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2647,
		time: 2646,
		velocity: 0,
		power: 0,
		road: 20084.3058333333,
		acceleration: 0
	},
	{
		id: 2648,
		time: 2647,
		velocity: 0,
		power: 134.466030925363,
		road: 20084.5422222222,
		acceleration: 0.472777777777778
	},
	{
		id: 2649,
		time: 2648,
		velocity: 1.41833333333333,
		power: 1790.0420323138,
		road: 20085.7393518518,
		acceleration: 1.4487037037037
	},
	{
		id: 2650,
		time: 2649,
		velocity: 4.34611111111111,
		power: 4055.30995676888,
		road: 20088.3986111111,
		acceleration: 1.47555555555556
	},
	{
		id: 2651,
		time: 2650,
		velocity: 4.42666666666667,
		power: 4823.45037217878,
		road: 20092.3674074074,
		acceleration: 1.14351851851852
	},
	{
		id: 2652,
		time: 2651,
		velocity: 4.84888888888889,
		power: 2033.64799213875,
		road: 20097.0658796296,
		acceleration: 0.315833333333332
	},
	{
		id: 2653,
		time: 2652,
		velocity: 5.29361111111111,
		power: 2794.36609500172,
		road: 20102.1410648148,
		acceleration: 0.437592592592593
	},
	{
		id: 2654,
		time: 2653,
		velocity: 5.73944444444444,
		power: 3230.60314616989,
		road: 20107.6701388889,
		acceleration: 0.470185185185186
	},
	{
		id: 2655,
		time: 2654,
		velocity: 6.25944444444444,
		power: 3669.41340645953,
		road: 20113.6814814815,
		acceleration: 0.494351851851851
	},
	{
		id: 2656,
		time: 2655,
		velocity: 6.77666666666667,
		power: 1967.73391814277,
		road: 20120.0280092593,
		acceleration: 0.176018518518518
	},
	{
		id: 2657,
		time: 2656,
		velocity: 6.2675,
		power: 810.02078173738,
		road: 20126.4535185185,
		acceleration: -0.0180555555555557
	},
	{
		id: 2658,
		time: 2657,
		velocity: 6.20527777777778,
		power: -2644.66367879336,
		road: 20132.568287037,
		acceleration: -0.603425925925926
	},
	{
		id: 2659,
		time: 2658,
		velocity: 4.96638888888889,
		power: -1068.5791376454,
		road: 20138.2090277778,
		acceleration: -0.34462962962963
	},
	{
		id: 2660,
		time: 2659,
		velocity: 5.23361111111111,
		power: -1442.43985946176,
		road: 20143.4614814815,
		acceleration: -0.431944444444444
	},
	{
		id: 2661,
		time: 2660,
		velocity: 4.90944444444444,
		power: -1009.81501501302,
		road: 20148.3181944444,
		acceleration: -0.359537037037037
	},
	{
		id: 2662,
		time: 2661,
		velocity: 3.88777777777778,
		power: -1367.5527060008,
		road: 20152.7639351852,
		acceleration: -0.462407407407407
	},
	{
		id: 2663,
		time: 2662,
		velocity: 3.84638888888889,
		power: 2999.77712345567,
		road: 20157.2602777778,
		acceleration: 0.56361111111111
	},
	{
		id: 2664,
		time: 2663,
		velocity: 6.60027777777778,
		power: 6945.22356481917,
		road: 20162.6450925926,
		acceleration: 1.21333333333333
	},
	{
		id: 2665,
		time: 2664,
		velocity: 7.52777777777778,
		power: 14757.418619191,
		road: 20169.6639351852,
		acceleration: 2.05472222222222
	},
	{
		id: 2666,
		time: 2665,
		velocity: 10.0105555555556,
		power: 15726.1338206109,
		road: 20178.552962963,
		acceleration: 1.68564814814815
	},
	{
		id: 2667,
		time: 2666,
		velocity: 11.6572222222222,
		power: 19913.913633752,
		road: 20189.1727777778,
		acceleration: 1.77592592592593
	},
	{
		id: 2668,
		time: 2667,
		velocity: 12.8555555555556,
		power: 13505.7131363379,
		road: 20201.1664814815,
		acceleration: 0.971851851851852
	},
	{
		id: 2669,
		time: 2668,
		velocity: 12.9261111111111,
		power: 5361.2889561691,
		road: 20213.7607407407,
		acceleration: 0.229259259259258
	},
	{
		id: 2670,
		time: 2669,
		velocity: 12.345,
		power: 1009.08310402131,
		road: 20226.4026851852,
		acceleration: -0.133888888888887
	},
	{
		id: 2671,
		time: 2670,
		velocity: 12.4538888888889,
		power: -1074.38938010741,
		road: 20238.8251388889,
		acceleration: -0.305092592592594
	},
	{
		id: 2672,
		time: 2671,
		velocity: 12.0108333333333,
		power: -1150.29559324616,
		road: 20250.9401851852,
		acceleration: -0.309722222222224
	},
	{
		id: 2673,
		time: 2672,
		velocity: 11.4158333333333,
		power: -3969.56724692888,
		road: 20262.62,
		acceleration: -0.560740740740741
	},
	{
		id: 2674,
		time: 2673,
		velocity: 10.7716666666667,
		power: -5135.05146833167,
		road: 20273.6776851852,
		acceleration: -0.683518518518518
	},
	{
		id: 2675,
		time: 2674,
		velocity: 9.96027777777778,
		power: -4073.59754486255,
		road: 20284.0941203704,
		acceleration: -0.598981481481481
	},
	{
		id: 2676,
		time: 2675,
		velocity: 9.61888888888889,
		power: -4104.61472340737,
		road: 20293.9005555555,
		acceleration: -0.62101851851852
	},
	{
		id: 2677,
		time: 2676,
		velocity: 8.90861111111111,
		power: -2143.86124234845,
		road: 20303.1872685185,
		acceleration: -0.418425925925924
	},
	{
		id: 2678,
		time: 2677,
		velocity: 8.705,
		power: -3218.28816942951,
		road: 20311.9871759259,
		acceleration: -0.555185185185186
	},
	{
		id: 2679,
		time: 2678,
		velocity: 7.95333333333333,
		power: -4054.25553310527,
		road: 20320.1665277778,
		acceleration: -0.685925925925926
	},
	{
		id: 2680,
		time: 2679,
		velocity: 6.85083333333333,
		power: -3374.11744550725,
		road: 20327.6875,
		acceleration: -0.630833333333332
	},
	{
		id: 2681,
		time: 2680,
		velocity: 6.8125,
		power: -421.131724408214,
		road: 20334.7839814815,
		acceleration: -0.218148148148149
	},
	{
		id: 2682,
		time: 2681,
		velocity: 7.29888888888889,
		power: 5198.99744466111,
		road: 20342.0678703704,
		acceleration: 0.592962962962964
	},
	{
		id: 2683,
		time: 2682,
		velocity: 8.62972222222222,
		power: 7464.4662441915,
		road: 20350.0573611111,
		acceleration: 0.81824074074074
	},
	{
		id: 2684,
		time: 2683,
		velocity: 9.26722222222222,
		power: 7329.2224343117,
		road: 20358.8103703704,
		acceleration: 0.708796296296297
	},
	{
		id: 2685,
		time: 2684,
		velocity: 9.42527777777778,
		power: 2648.68831952316,
		road: 20367.9819907407,
		acceleration: 0.128425925925926
	},
	{
		id: 2686,
		time: 2685,
		velocity: 9.015,
		power: -6425.57553025356,
		road: 20376.7474537037,
		acceleration: -0.94074074074074
	},
	{
		id: 2687,
		time: 2686,
		velocity: 6.445,
		power: -8073.02641893083,
		road: 20384.4084722222,
		acceleration: -1.26814814814815
	},
	{
		id: 2688,
		time: 2687,
		velocity: 5.62083333333333,
		power: -6404.16853311653,
		road: 20390.8360648148,
		acceleration: -1.19870370370371
	},
	{
		id: 2689,
		time: 2688,
		velocity: 5.41888888888889,
		power: 882.578332150465,
		road: 20396.6706018518,
		acceleration: 0.0125925925925934
	},
	{
		id: 2690,
		time: 2689,
		velocity: 6.48277777777778,
		power: 4321.32596069425,
		road: 20402.8074537037,
		acceleration: 0.592037037037037
	},
	{
		id: 2691,
		time: 2690,
		velocity: 7.39694444444444,
		power: 6825.08842072982,
		road: 20409.6849537037,
		acceleration: 0.889259259259259
	},
	{
		id: 2692,
		time: 2691,
		velocity: 8.08666666666667,
		power: 7026.51728160618,
		road: 20417.4048148148,
		acceleration: 0.795462962962963
	},
	{
		id: 2693,
		time: 2692,
		velocity: 8.86916666666667,
		power: 6976.07843253155,
		road: 20425.87125,
		acceleration: 0.697685185185186
	},
	{
		id: 2694,
		time: 2693,
		velocity: 9.49,
		power: 6305.3214148533,
		road: 20434.9636111111,
		acceleration: 0.554166666666667
	},
	{
		id: 2695,
		time: 2694,
		velocity: 9.74916666666667,
		power: 5538.34535151248,
		road: 20444.5468055555,
		acceleration: 0.427499999999998
	},
	{
		id: 2696,
		time: 2695,
		velocity: 10.1516666666667,
		power: 2423.14632306259,
		road: 20454.3820833333,
		acceleration: 0.0766666666666662
	},
	{
		id: 2697,
		time: 2696,
		velocity: 9.72,
		power: 659.820048925329,
		road: 20464.2000925926,
		acceleration: -0.111203703703703
	},
	{
		id: 2698,
		time: 2697,
		velocity: 9.41555555555555,
		power: -2659.48401776339,
		road: 20473.7266666667,
		acceleration: -0.471666666666668
	},
	{
		id: 2699,
		time: 2698,
		velocity: 8.73666666666667,
		power: -3437.56276195876,
		road: 20482.7303703704,
		acceleration: -0.574074074074073
	},
	{
		id: 2700,
		time: 2699,
		velocity: 7.99777777777778,
		power: -6136.58472197831,
		road: 20490.9730555555,
		acceleration: -0.947962962962962
	},
	{
		id: 2701,
		time: 2700,
		velocity: 6.57166666666667,
		power: -7497.61188750954,
		road: 20498.1115277778,
		acceleration: -1.26046296296296
	},
	{
		id: 2702,
		time: 2701,
		velocity: 4.95527777777778,
		power: -9396.42539612468,
		road: 20503.6554629629,
		acceleration: -1.92861111111111
	},
	{
		id: 2703,
		time: 2702,
		velocity: 2.21194444444444,
		power: -6007.26194476806,
		road: 20507.2990740741,
		acceleration: -1.87203703703704
	},
	{
		id: 2704,
		time: 2703,
		velocity: 0.955555555555555,
		power: -2716.06728183812,
		road: 20509.180787037,
		acceleration: -1.65175925925926
	},
	{
		id: 2705,
		time: 2704,
		velocity: 0,
		power: -396.937416249844,
		road: 20509.8679629629,
		acceleration: -0.737314814814815
	},
	{
		id: 2706,
		time: 2705,
		velocity: 0,
		power: 121.154484654525,
		road: 20510.2784259259,
		acceleration: 0.183888888888889
	},
	{
		id: 2707,
		time: 2706,
		velocity: 1.50722222222222,
		power: 1836.1941980796,
		road: 20511.5056481481,
		acceleration: 1.44962962962963
	},
	{
		id: 2708,
		time: 2707,
		velocity: 4.34888888888889,
		power: 4759.46761888637,
		road: 20514.2917592592,
		acceleration: 1.66814814814815
	},
	{
		id: 2709,
		time: 2708,
		velocity: 5.00444444444444,
		power: 5290.63652263639,
		road: 20518.5043981481,
		acceleration: 1.18490740740741
	},
	{
		id: 2710,
		time: 2709,
		velocity: 5.06194444444444,
		power: 2547.07220737286,
		road: 20523.506712963,
		acceleration: 0.394444444444444
	},
	{
		id: 2711,
		time: 2710,
		velocity: 5.53222222222222,
		power: 3620.41272923278,
		road: 20528.9819907407,
		acceleration: 0.551481481481481
	},
	{
		id: 2712,
		time: 2711,
		velocity: 6.65888888888889,
		power: 5786.62344638276,
		road: 20535.1518055555,
		acceleration: 0.837592592592593
	},
	{
		id: 2713,
		time: 2712,
		velocity: 7.57472222222222,
		power: 7324.26736666524,
		road: 20542.2082407407,
		acceleration: 0.935648148148148
	},
	{
		id: 2714,
		time: 2713,
		velocity: 8.33916666666667,
		power: 7730.20268463123,
		road: 20550.1615740741,
		acceleration: 0.858148148148149
	},
	{
		id: 2715,
		time: 2714,
		velocity: 9.23333333333333,
		power: 7907.59146389137,
		road: 20558.932037037,
		acceleration: 0.77611111111111
	},
	{
		id: 2716,
		time: 2715,
		velocity: 9.90305555555556,
		power: 8417.79645597716,
		road: 20568.4647222222,
		acceleration: 0.748333333333335
	},
	{
		id: 2717,
		time: 2716,
		velocity: 10.5841666666667,
		power: 6299.58796779845,
		road: 20578.6048611111,
		acceleration: 0.466574074074074
	},
	{
		id: 2718,
		time: 2717,
		velocity: 10.6330555555556,
		power: 4188.938930325,
		road: 20589.0931018518,
		acceleration: 0.229629629629629
	},
	{
		id: 2719,
		time: 2718,
		velocity: 10.5919444444444,
		power: 4447.17932445734,
		road: 20599.8175462963,
		acceleration: 0.242777777777777
	},
	{
		id: 2720,
		time: 2719,
		velocity: 11.3125,
		power: 7492.95222829506,
		road: 20610.9188888889,
		acceleration: 0.511018518518519
	},
	{
		id: 2721,
		time: 2720,
		velocity: 12.1661111111111,
		power: 8869.40482736636,
		road: 20622.5727314815,
		acceleration: 0.593981481481482
	},
	{
		id: 2722,
		time: 2721,
		velocity: 12.3738888888889,
		power: 7493.73520892714,
		road: 20634.7408796296,
		acceleration: 0.434629629629628
	},
	{
		id: 2723,
		time: 2722,
		velocity: 12.6163888888889,
		power: 3980.6889804582,
		road: 20647.1865740741,
		acceleration: 0.120462962962966
	},
	{
		id: 2724,
		time: 2723,
		velocity: 12.5275,
		power: 4707.59607351957,
		road: 20659.7799537037,
		acceleration: 0.174907407407407
	},
	{
		id: 2725,
		time: 2724,
		velocity: 12.8986111111111,
		power: 3951.16064624539,
		road: 20672.5139351852,
		acceleration: 0.106296296296296
	},
	{
		id: 2726,
		time: 2725,
		velocity: 12.9352777777778,
		power: 5946.54729505432,
		road: 20685.4314814815,
		acceleration: 0.260833333333332
	},
	{
		id: 2727,
		time: 2726,
		velocity: 13.31,
		power: 5124.36419581857,
		road: 20698.5713425926,
		acceleration: 0.183796296296295
	},
	{
		id: 2728,
		time: 2727,
		velocity: 13.45,
		power: 5970.42720036974,
		road: 20711.9233333333,
		acceleration: 0.240462962962964
	},
	{
		id: 2729,
		time: 2728,
		velocity: 13.6566666666667,
		power: 4572.06209770799,
		road: 20725.4571296296,
		acceleration: 0.123148148148148
	},
	{
		id: 2730,
		time: 2729,
		velocity: 13.6794444444444,
		power: 2251.08047735022,
		road: 20739.0237962963,
		acceleration: -0.0574074074074069
	},
	{
		id: 2731,
		time: 2730,
		velocity: 13.2777777777778,
		power: -696.101302998112,
		road: 20752.4202314815,
		acceleration: -0.283055555555556
	},
	{
		id: 2732,
		time: 2731,
		velocity: 12.8075,
		power: -2177.94097182719,
		road: 20765.4759722222,
		acceleration: -0.398333333333332
	},
	{
		id: 2733,
		time: 2732,
		velocity: 12.4844444444444,
		power: -1483.65889035079,
		road: 20778.1621759259,
		acceleration: -0.340740740740742
	},
	{
		id: 2734,
		time: 2733,
		velocity: 12.2555555555556,
		power: 291.544595117321,
		road: 20790.5831018518,
		acceleration: -0.189814814814815
	},
	{
		id: 2735,
		time: 2734,
		velocity: 12.2380555555556,
		power: 1711.74336371085,
		road: 20802.875787037,
		acceleration: -0.0666666666666664
	},
	{
		id: 2736,
		time: 2735,
		velocity: 12.2844444444444,
		power: 2951.77871862444,
		road: 20815.1549074074,
		acceleration: 0.0395370370370358
	},
	{
		id: 2737,
		time: 2736,
		velocity: 12.3741666666667,
		power: 3328.01900863432,
		road: 20827.4886111111,
		acceleration: 0.069629629629631
	},
	{
		id: 2738,
		time: 2737,
		velocity: 12.4469444444444,
		power: 3146.53772185677,
		road: 20839.8831481481,
		acceleration: 0.0520370370370351
	},
	{
		id: 2739,
		time: 2738,
		velocity: 12.4405555555556,
		power: 3333.11293801136,
		road: 20852.3365277778,
		acceleration: 0.0656481481481492
	},
	{
		id: 2740,
		time: 2739,
		velocity: 12.5711111111111,
		power: 3566.99578693968,
		road: 20864.8639814815,
		acceleration: 0.0825000000000014
	},
	{
		id: 2741,
		time: 2740,
		velocity: 12.6944444444444,
		power: 3810.37541600255,
		road: 20877.4823148148,
		acceleration: 0.0992592592592576
	},
	{
		id: 2742,
		time: 2741,
		velocity: 12.7383333333333,
		power: 3291.56056317857,
		road: 20890.1769907407,
		acceleration: 0.0534259259259269
	},
	{
		id: 2743,
		time: 2742,
		velocity: 12.7313888888889,
		power: 2946.04740768709,
		road: 20902.9101851852,
		acceleration: 0.0236111111111104
	},
	{
		id: 2744,
		time: 2743,
		velocity: 12.7652777777778,
		power: 1470.51926338632,
		road: 20915.6067129629,
		acceleration: -0.0969444444444427
	},
	{
		id: 2745,
		time: 2744,
		velocity: 12.4475,
		power: -216.490608290314,
		road: 20928.1377314815,
		acceleration: -0.234074074074076
	},
	{
		id: 2746,
		time: 2745,
		velocity: 12.0291666666667,
		power: -2060.96038052625,
		road: 20940.3575,
		acceleration: -0.388425925925924
	},
	{
		id: 2747,
		time: 2746,
		velocity: 11.6,
		power: -2923.01463252063,
		road: 20952.1501851852,
		acceleration: -0.465740740740742
	},
	{
		id: 2748,
		time: 2747,
		velocity: 11.0502777777778,
		power: -2939.06251080515,
		road: 20963.4740277778,
		acceleration: -0.471944444444445
	},
	{
		id: 2749,
		time: 2748,
		velocity: 10.6133333333333,
		power: -5829.22962121596,
		road: 20974.1802314815,
		acceleration: -0.763333333333332
	},
	{
		id: 2750,
		time: 2749,
		velocity: 9.31,
		power: -7228.17649052364,
		road: 20984.0284722222,
		acceleration: -0.952592592592595
	},
	{
		id: 2751,
		time: 2750,
		velocity: 8.1925,
		power: -7769.48890429413,
		road: 20992.8523148148,
		acceleration: -1.0962037037037
	},
	{
		id: 2752,
		time: 2751,
		velocity: 7.32472222222222,
		power: -6762.82342073566,
		road: 21000.5880555555,
		acceleration: -1.08
	},
	{
		id: 2753,
		time: 2752,
		velocity: 6.07,
		power: -5941.10552307968,
		road: 21007.2378240741,
		acceleration: -1.09194444444445
	},
	{
		id: 2754,
		time: 2753,
		velocity: 4.91666666666667,
		power: -6011.90084009596,
		road: 21012.6892129629,
		acceleration: -1.30481481481482
	},
	{
		id: 2755,
		time: 2754,
		velocity: 3.41027777777778,
		power: -4287.69476599844,
		road: 21016.8808796296,
		acceleration: -1.21462962962963
	},
	{
		id: 2756,
		time: 2755,
		velocity: 2.42611111111111,
		power: -3448.97723596547,
		road: 21019.7700462963,
		acceleration: -1.39037037037037
	},
	{
		id: 2757,
		time: 2756,
		velocity: 0.745555555555556,
		power: -1553.27094652287,
		road: 21021.3956481481,
		acceleration: -1.13675925925926
	},
	{
		id: 2758,
		time: 2757,
		velocity: 0,
		power: -421.290021202041,
		road: 21022.0485185185,
		acceleration: -0.808703703703704
	},
	{
		id: 2759,
		time: 2758,
		velocity: 0,
		power: -14.2426834957765,
		road: 21022.1727777778,
		acceleration: -0.248518518518519
	},
	{
		id: 2760,
		time: 2759,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2761,
		time: 2760,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2762,
		time: 2761,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2763,
		time: 2762,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2764,
		time: 2763,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2765,
		time: 2764,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2766,
		time: 2765,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2767,
		time: 2766,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2768,
		time: 2767,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2769,
		time: 2768,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2770,
		time: 2769,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2771,
		time: 2770,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2772,
		time: 2771,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2773,
		time: 2772,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2774,
		time: 2773,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2775,
		time: 2774,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2776,
		time: 2775,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2777,
		time: 2776,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2778,
		time: 2777,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2779,
		time: 2778,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2780,
		time: 2779,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2781,
		time: 2780,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2782,
		time: 2781,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2783,
		time: 2782,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2784,
		time: 2783,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2785,
		time: 2784,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2786,
		time: 2785,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2787,
		time: 2786,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2788,
		time: 2787,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2789,
		time: 2788,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2790,
		time: 2789,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2791,
		time: 2790,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2792,
		time: 2791,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2793,
		time: 2792,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2794,
		time: 2793,
		velocity: 0,
		power: 0,
		road: 21022.1727777778,
		acceleration: 0
	},
	{
		id: 2795,
		time: 2794,
		velocity: 0,
		power: 83.1121807439812,
		road: 21022.3527314815,
		acceleration: 0.359907407407407
	},
	{
		id: 2796,
		time: 2795,
		velocity: 1.07972222222222,
		power: 953.337051046762,
		road: 21023.2250925926,
		acceleration: 1.02490740740741
	},
	{
		id: 2797,
		time: 2796,
		velocity: 3.07472222222222,
		power: 3378.67737796457,
		road: 21025.3736574074,
		acceleration: 1.5275
	},
	{
		id: 2798,
		time: 2797,
		velocity: 4.5825,
		power: 5523.18821006519,
		road: 21029.0168518518,
		acceleration: 1.46175925925926
	},
	{
		id: 2799,
		time: 2798,
		velocity: 5.465,
		power: 4110.36637358655,
		road: 21033.7754629629,
		acceleration: 0.769074074074074
	},
	{
		id: 2800,
		time: 2799,
		velocity: 5.38194444444444,
		power: 2856.24154003495,
		road: 21039.1276388889,
		acceleration: 0.418055555555556
	},
	{
		id: 2801,
		time: 2800,
		velocity: 5.83666666666667,
		power: 1816.93709519429,
		road: 21044.7850925926,
		acceleration: 0.1925
	},
	{
		id: 2802,
		time: 2801,
		velocity: 6.0425,
		power: 1727.86849846947,
		road: 21050.62125,
		acceleration: 0.164907407407407
	},
	{
		id: 2803,
		time: 2802,
		velocity: 5.87666666666667,
		power: 2127.39011775881,
		road: 21056.6514814815,
		acceleration: 0.22324074074074
	},
	{
		id: 2804,
		time: 2803,
		velocity: 6.50638888888889,
		power: 2220.74613565844,
		road: 21062.9053703704,
		acceleration: 0.224074074074074
	},
	{
		id: 2805,
		time: 2804,
		velocity: 6.71472222222222,
		power: 3521.45906902272,
		road: 21069.4771759259,
		acceleration: 0.411759259259259
	},
	{
		id: 2806,
		time: 2805,
		velocity: 7.11194444444445,
		power: 4723.97917755255,
		road: 21076.5292592592,
		acceleration: 0.548796296296298
	},
	{
		id: 2807,
		time: 2806,
		velocity: 8.15277777777778,
		power: 3700.91359940714,
		road: 21084.0352777778,
		acceleration: 0.359074074074073
	},
	{
		id: 2808,
		time: 2807,
		velocity: 7.79194444444444,
		power: 1574.95791576799,
		road: 21091.7476851852,
		acceleration: 0.0537037037037038
	},
	{
		id: 2809,
		time: 2808,
		velocity: 7.27305555555556,
		power: -2570.57020314909,
		road: 21099.2268518518,
		acceleration: -0.520185185185186
	},
	{
		id: 2810,
		time: 2809,
		velocity: 6.59222222222222,
		power: -4567.71719292452,
		road: 21106.015462963,
		acceleration: -0.860925925925926
	},
	{
		id: 2811,
		time: 2810,
		velocity: 5.20916666666667,
		power: -6107.12268369324,
		road: 21111.7393055555,
		acceleration: -1.26861111111111
	},
	{
		id: 2812,
		time: 2811,
		velocity: 3.46722222222222,
		power: -5837.28623212506,
		road: 21116.0462037037,
		acceleration: -1.56527777777778
	},
	{
		id: 2813,
		time: 2812,
		velocity: 1.89638888888889,
		power: -3398.10530945354,
		road: 21118.8703703704,
		acceleration: -1.40018518518518
	},
	{
		id: 2814,
		time: 2813,
		velocity: 1.00861111111111,
		power: -1257.5670154286,
		road: 21120.530462963,
		acceleration: -0.927962962962963
	},
	{
		id: 2815,
		time: 2814,
		velocity: 0.683333333333333,
		power: -420.54691465742,
		road: 21121.4105092592,
		acceleration: -0.63212962962963
	},
	{
		id: 2816,
		time: 2815,
		velocity: 0,
		power: -78.25064105806,
		road: 21121.8063888889,
		acceleration: -0.336203703703704
	},
	{
		id: 2817,
		time: 2816,
		velocity: 0,
		power: 10.9389860432258,
		road: 21122.0001851852,
		acceleration: -0.0679629629629629
	},
	{
		id: 2818,
		time: 2817,
		velocity: 0.479444444444444,
		power: 276.639356089608,
		road: 21122.4332407407,
		acceleration: 0.546481481481481
	},
	{
		id: 2819,
		time: 2818,
		velocity: 1.63944444444444,
		power: 1926.7955298018,
		road: 21123.8120833333,
		acceleration: 1.34509259259259
	},
	{
		id: 2820,
		time: 2819,
		velocity: 4.03527777777778,
		power: 4077.38749530112,
		road: 21126.5750925926,
		acceleration: 1.42324074074074
	},
	{
		id: 2821,
		time: 2820,
		velocity: 4.74916666666667,
		power: 3916.88342847928,
		road: 21130.5062962963,
		acceleration: 0.913148148148148
	},
	{
		id: 2822,
		time: 2821,
		velocity: 4.37888888888889,
		power: -104.341023025679,
		road: 21134.8124074074,
		acceleration: -0.163333333333333
	},
	{
		id: 2823,
		time: 2822,
		velocity: 3.54527777777778,
		power: -2437.62016796625,
		road: 21138.6330092592,
		acceleration: -0.807685185185184
	},
	{
		id: 2824,
		time: 2823,
		velocity: 2.32611111111111,
		power: -2402.25586333446,
		road: 21141.5496296296,
		acceleration: -1.00027777777778
	},
	{
		id: 2825,
		time: 2824,
		velocity: 1.37805555555556,
		power: -1478.10529580727,
		road: 21143.5021759259,
		acceleration: -0.92787037037037
	},
	{
		id: 2826,
		time: 2825,
		velocity: 0.761666666666667,
		power: -474.522546616011,
		road: 21144.7213425926,
		acceleration: -0.538888888888889
	},
	{
		id: 2827,
		time: 2826,
		velocity: 0.709444444444444,
		power: 190.746743646742,
		road: 21145.7089351852,
		acceleration: 0.0757407407407408
	},
	{
		id: 2828,
		time: 2827,
		velocity: 1.60527777777778,
		power: 822.68891842865,
		road: 21147.0049537037,
		acceleration: 0.541111111111111
	},
	{
		id: 2829,
		time: 2828,
		velocity: 2.385,
		power: 548.559804777225,
		road: 21148.6797222222,
		acceleration: 0.216388888888889
	},
	{
		id: 2830,
		time: 2829,
		velocity: 1.35861111111111,
		power: -583.839400151399,
		road: 21150.1951388889,
		acceleration: -0.535092592592592
	},
	{
		id: 2831,
		time: 2830,
		velocity: 0,
		power: -537.628521319286,
		road: 21151.0455092592,
		acceleration: -0.795
	},
	{
		id: 2832,
		time: 2831,
		velocity: 0,
		power: -69.7912171702404,
		road: 21151.2719444444,
		acceleration: -0.45287037037037
	},
	{
		id: 2833,
		time: 2832,
		velocity: 0,
		power: 0,
		road: 21151.2719444444,
		acceleration: 0
	},
	{
		id: 2834,
		time: 2833,
		velocity: 0,
		power: 0,
		road: 21151.2719444444,
		acceleration: 0
	},
	{
		id: 2835,
		time: 2834,
		velocity: 0,
		power: 0,
		road: 21151.2719444444,
		acceleration: 0
	},
	{
		id: 2836,
		time: 2835,
		velocity: 0,
		power: 0,
		road: 21151.2719444444,
		acceleration: 0
	},
	{
		id: 2837,
		time: 2836,
		velocity: 0,
		power: 0,
		road: 21151.2719444444,
		acceleration: 0
	},
	{
		id: 2838,
		time: 2837,
		velocity: 0,
		power: 0,
		road: 21151.2719444444,
		acceleration: 0
	},
	{
		id: 2839,
		time: 2838,
		velocity: 0,
		power: 67.4415500153451,
		road: 21151.4313888889,
		acceleration: 0.318888888888889
	},
	{
		id: 2840,
		time: 2839,
		velocity: 0.956666666666667,
		power: 508.35167344789,
		road: 21152.0922222222,
		acceleration: 0.683888888888889
	},
	{
		id: 2841,
		time: 2840,
		velocity: 2.05166666666667,
		power: 1705.21032116502,
		road: 21153.6193518518,
		acceleration: 1.0487037037037
	},
	{
		id: 2842,
		time: 2841,
		velocity: 3.14611111111111,
		power: 2598.72314294458,
		road: 21156.1470833333,
		acceleration: 0.9525
	},
	{
		id: 2843,
		time: 2842,
		velocity: 3.81416666666667,
		power: 3383.86035735688,
		road: 21159.6001388889,
		acceleration: 0.898148148148147
	},
	{
		id: 2844,
		time: 2843,
		velocity: 4.74611111111111,
		power: 2479.45129498765,
		road: 21163.7484259259,
		acceleration: 0.492314814814815
	},
	{
		id: 2845,
		time: 2844,
		velocity: 4.62305555555556,
		power: 1730.12087335484,
		road: 21168.2746296296,
		acceleration: 0.263518518518519
	},
	{
		id: 2846,
		time: 2845,
		velocity: 4.60472222222222,
		power: 326.978457760552,
		road: 21172.9000925926,
		acceleration: -0.0649999999999995
	},
	{
		id: 2847,
		time: 2846,
		velocity: 4.55111111111111,
		power: 420.07142670103,
		road: 21177.4718518518,
		acceleration: -0.0424074074074081
	},
	{
		id: 2848,
		time: 2847,
		velocity: 4.49583333333333,
		power: 44.6109313671973,
		road: 21181.958287037,
		acceleration: -0.128240740740741
	},
	{
		id: 2849,
		time: 2848,
		velocity: 4.22,
		power: -191.25689709122,
		road: 21186.2883796296,
		acceleration: -0.184444444444445
	},
	{
		id: 2850,
		time: 2849,
		velocity: 3.99777777777778,
		power: 290.370161529881,
		road: 21190.4939351852,
		acceleration: -0.0646296296296294
	},
	{
		id: 2851,
		time: 2850,
		velocity: 4.30194444444444,
		power: 717.254955904996,
		road: 21194.6885648148,
		acceleration: 0.0427777777777774
	},
	{
		id: 2852,
		time: 2851,
		velocity: 4.34833333333333,
		power: 1876.61601980195,
		road: 21199.0614351852,
		acceleration: 0.313703703703704
	},
	{
		id: 2853,
		time: 2852,
		velocity: 4.93888888888889,
		power: 1231.95382514671,
		road: 21203.6624537037,
		acceleration: 0.142592592592592
	},
	{
		id: 2854,
		time: 2853,
		velocity: 4.72972222222222,
		power: 1065.20274508244,
		road: 21208.3835648148,
		acceleration: 0.0975925925925933
	},
	{
		id: 2855,
		time: 2854,
		velocity: 4.64111111111111,
		power: 25.7113193395964,
		road: 21213.0864351852,
		acceleration: -0.134074074074075
	},
	{
		id: 2856,
		time: 2855,
		velocity: 4.53666666666667,
		power: -1206.99617967304,
		road: 21217.5093981481,
		acceleration: -0.425740740740741
	},
	{
		id: 2857,
		time: 2856,
		velocity: 3.4525,
		power: -1851.77678572282,
		road: 21221.4009259259,
		acceleration: -0.637129629629629
	},
	{
		id: 2858,
		time: 2857,
		velocity: 2.72972222222222,
		power: -1797.6744700248,
		road: 21224.6123611111,
		acceleration: -0.723055555555556
	},
	{
		id: 2859,
		time: 2858,
		velocity: 2.3675,
		power: -158.692885373182,
		road: 21227.3660648148,
		acceleration: -0.192407407407407
	},
	{
		id: 2860,
		time: 2859,
		velocity: 2.87527777777778,
		power: 1058.34835355148,
		road: 21230.1574537037,
		acceleration: 0.267777777777777
	},
	{
		id: 2861,
		time: 2860,
		velocity: 3.53305555555556,
		power: 2425.07428758571,
		road: 21233.4089351852,
		acceleration: 0.652407407407408
	},
	{
		id: 2862,
		time: 2861,
		velocity: 4.32472222222222,
		power: 2022.67512486191,
		road: 21237.1998611111,
		acceleration: 0.426481481481481
	},
	{
		id: 2863,
		time: 2862,
		velocity: 4.15472222222222,
		power: 2683.89294211021,
		road: 21241.4663888889,
		acceleration: 0.524722222222223
	},
	{
		id: 2864,
		time: 2863,
		velocity: 5.10722222222222,
		power: 2966.56189462938,
		road: 21246.2514351852,
		acceleration: 0.512314814814815
	},
	{
		id: 2865,
		time: 2864,
		velocity: 5.86166666666667,
		power: 5366.85042195755,
		road: 21251.7353240741,
		acceleration: 0.885370370370371
	},
	{
		id: 2866,
		time: 2865,
		velocity: 6.81083333333333,
		power: 4428.00030395983,
		road: 21257.9612962963,
		acceleration: 0.598796296296296
	},
	{
		id: 2867,
		time: 2866,
		velocity: 6.90361111111111,
		power: 980.658143206983,
		road: 21264.4899537037,
		acceleration: 0.00657407407407362
	},
	{
		id: 2868,
		time: 2867,
		velocity: 5.88138888888889,
		power: -2052.12486339917,
		road: 21270.7753703704,
		acceleration: -0.493055555555555
	},
	{
		id: 2869,
		time: 2868,
		velocity: 5.33166666666667,
		power: -2790.15488344509,
		road: 21276.4842592592,
		acceleration: -0.66
	},
	{
		id: 2870,
		time: 2869,
		velocity: 4.92361111111111,
		power: -1552.80225026156,
		road: 21281.633287037,
		acceleration: -0.459722222222222
	},
	{
		id: 2871,
		time: 2870,
		velocity: 4.50222222222222,
		power: -59.0932007716406,
		road: 21286.4757407407,
		acceleration: -0.153425925925927
	},
	{
		id: 2872,
		time: 2871,
		velocity: 4.87138888888889,
		power: 637.533202390211,
		road: 21291.2418055555,
		acceleration: 0.000648148148147953
	},
	{
		id: 2873,
		time: 2872,
		velocity: 4.92555555555556,
		power: 1022.30668985975,
		road: 21296.0499074074,
		acceleration: 0.0834259259259262
	},
	{
		id: 2874,
		time: 2873,
		velocity: 4.7525,
		power: -57.4249222208208,
		road: 21300.823287037,
		acceleration: -0.15287037037037
	},
	{
		id: 2875,
		time: 2874,
		velocity: 4.41277777777778,
		power: -152.48225214602,
		road: 21305.4331481481,
		acceleration: -0.174166666666665
	},
	{
		id: 2876,
		time: 2875,
		velocity: 4.40305555555556,
		power: -703.143908547337,
		road: 21309.8021296296,
		acceleration: -0.307592592592594
	},
	{
		id: 2877,
		time: 2876,
		velocity: 3.82972222222222,
		power: -585.043657577703,
		road: 21313.873287037,
		acceleration: -0.288055555555555
	},
	{
		id: 2878,
		time: 2877,
		velocity: 3.54861111111111,
		power: -847.934202079609,
		road: 21317.6133796296,
		acceleration: -0.374074074074074
	},
	{
		id: 2879,
		time: 2878,
		velocity: 3.28083333333333,
		power: -83.9094569712234,
		road: 21321.0866203704,
		acceleration: -0.15962962962963
	},
	{
		id: 2880,
		time: 2879,
		velocity: 3.35083333333333,
		power: -74.1068301242143,
		road: 21324.4014814815,
		acceleration: -0.15712962962963
	},
	{
		id: 2881,
		time: 2880,
		velocity: 3.07722222222222,
		power: 311.582093175067,
		road: 21327.6221296296,
		acceleration: -0.0312962962962966
	},
	{
		id: 2882,
		time: 2881,
		velocity: 3.18694444444444,
		power: 112.208944497124,
		road: 21330.7793518518,
		acceleration: -0.0955555555555558
	},
	{
		id: 2883,
		time: 2882,
		velocity: 3.06416666666667,
		power: -44.5732242527099,
		road: 21333.8147685185,
		acceleration: -0.148055555555555
	},
	{
		id: 2884,
		time: 2883,
		velocity: 2.63305555555556,
		power: -551.458236190444,
		road: 21336.6061574074,
		acceleration: -0.34
	},
	{
		id: 2885,
		time: 2884,
		velocity: 2.16694444444444,
		power: -358.021742126273,
		road: 21339.0860185185,
		acceleration: -0.283055555555555
	},
	{
		id: 2886,
		time: 2885,
		velocity: 2.215,
		power: -472.55037092276,
		road: 21341.2438888889,
		acceleration: -0.360925925925926
	},
	{
		id: 2887,
		time: 2886,
		velocity: 1.55027777777778,
		power: -102.926187279303,
		road: 21343.1277777778,
		acceleration: -0.187037037037037
	},
	{
		id: 2888,
		time: 2887,
		velocity: 1.60583333333333,
		power: -286.719915379077,
		road: 21344.7611111111,
		acceleration: -0.314074074074074
	},
	{
		id: 2889,
		time: 2888,
		velocity: 1.27277777777778,
		power: 124.766169368837,
		road: 21346.2182407407,
		acceleration: -0.0383333333333336
	},
	{
		id: 2890,
		time: 2889,
		velocity: 1.43527777777778,
		power: 191.110343258857,
		road: 21347.661712963,
		acceleration: 0.0110185185185188
	},
	{
		id: 2891,
		time: 2890,
		velocity: 1.63888888888889,
		power: 714.526224654669,
		road: 21349.2791666667,
		acceleration: 0.336944444444444
	},
	{
		id: 2892,
		time: 2891,
		velocity: 2.28361111111111,
		power: 411.997997710364,
		road: 21351.1185185185,
		acceleration: 0.106851851851852
	},
	{
		id: 2893,
		time: 2892,
		velocity: 1.75583333333333,
		power: 197.087942866196,
		road: 21353.0017592593,
		acceleration: -0.0190740740740742
	},
	{
		id: 2894,
		time: 2893,
		velocity: 1.58166666666667,
		power: -895.33937509841,
		road: 21354.4948611111,
		acceleration: -0.761203703703704
	},
	{
		id: 2895,
		time: 2894,
		velocity: 0,
		power: -355.413801940332,
		road: 21355.3147222222,
		acceleration: -0.585277777777778
	},
	{
		id: 2896,
		time: 2895,
		velocity: 0,
		power: -50.7316880042939,
		road: 21355.7105092593,
		acceleration: -0.26287037037037
	},
	{
		id: 2897,
		time: 2896,
		velocity: 0.793055555555556,
		power: 102.786318089205,
		road: 21356.0643518518,
		acceleration: 0.178981481481482
	},
	{
		id: 2898,
		time: 2897,
		velocity: 0.536944444444444,
		power: 290.939970346638,
		road: 21356.6893981481,
		acceleration: 0.363425925925926
	},
	{
		id: 2899,
		time: 2898,
		velocity: 1.09027777777778,
		power: 245.263855166997,
		road: 21357.5778240741,
		acceleration: 0.163333333333333
	},
	{
		id: 2900,
		time: 2899,
		velocity: 1.28305555555556,
		power: 325.376308989274,
		road: 21358.6447222222,
		acceleration: 0.193611111111111
	},
	{
		id: 2901,
		time: 2900,
		velocity: 1.11777777777778,
		power: -219.114218275802,
		road: 21359.626712963,
		acceleration: -0.363425925925926
	},
	{
		id: 2902,
		time: 2901,
		velocity: 0,
		power: -166.71307271394,
		road: 21360.2131481481,
		acceleration: -0.427685185185185
	},
	{
		id: 2903,
		time: 2902,
		velocity: 0,
		power: -43.2513979207277,
		road: 21360.3994444444,
		acceleration: -0.372592592592593
	},
	{
		id: 2904,
		time: 2903,
		velocity: 0,
		power: 0,
		road: 21360.3994444444,
		acceleration: 0
	},
	{
		id: 2905,
		time: 2904,
		velocity: 0,
		power: 0,
		road: 21360.3994444444,
		acceleration: 0
	},
	{
		id: 2906,
		time: 2905,
		velocity: 0,
		power: 0,
		road: 21360.3994444444,
		acceleration: 0
	},
	{
		id: 2907,
		time: 2906,
		velocity: 0,
		power: 0,
		road: 21360.3994444444,
		acceleration: 0
	},
	{
		id: 2908,
		time: 2907,
		velocity: 0,
		power: 0,
		road: 21360.3994444444,
		acceleration: 0
	},
	{
		id: 2909,
		time: 2908,
		velocity: 0,
		power: 0,
		road: 21360.3994444444,
		acceleration: 0
	},
	{
		id: 2910,
		time: 2909,
		velocity: 0,
		power: 0,
		road: 21360.3994444444,
		acceleration: 0
	},
	{
		id: 2911,
		time: 2910,
		velocity: 0,
		power: 4.58978036709493,
		road: 21360.4262037037,
		acceleration: 0.0535185185185185
	},
	{
		id: 2912,
		time: 2911,
		velocity: 0.160555555555556,
		power: 6.46607795316321,
		road: 21360.4797222222,
		acceleration: 0
	},
	{
		id: 2913,
		time: 2912,
		velocity: 0,
		power: 6.46607795316321,
		road: 21360.5332407407,
		acceleration: 0
	},
	{
		id: 2914,
		time: 2913,
		velocity: 0,
		power: 1.87625597790773,
		road: 21360.56,
		acceleration: -0.0535185185185185
	},
	{
		id: 2915,
		time: 2914,
		velocity: 0,
		power: 0,
		road: 21360.56,
		acceleration: 0
	},
	{
		id: 2916,
		time: 2915,
		velocity: 0,
		power: 0,
		road: 21360.56,
		acceleration: 0
	},
	{
		id: 2917,
		time: 2916,
		velocity: 0,
		power: 0,
		road: 21360.56,
		acceleration: 0
	},
	{
		id: 2918,
		time: 2917,
		velocity: 0,
		power: 0,
		road: 21360.56,
		acceleration: 0
	}
];
export default test6;
