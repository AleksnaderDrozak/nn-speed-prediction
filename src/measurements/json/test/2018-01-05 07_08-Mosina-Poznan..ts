export const test2 = [
	{
		id: 1,
		time: 0,
		velocity: 0.0783333333333333,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0.257777777777778,
		power: 26.218869140227,
		road: 0.125138888888889,
		acceleration: 0.0936111111111111
	},
	{
		id: 3,
		time: 2,
		velocity: 0.179722222222222,
		power: 39.4059701614956,
		road: 0.333240740740741,
		acceleration: 0.0723148148148148
	},
	{
		id: 4,
		time: 3,
		velocity: 0.295277777777778,
		power: 10.9924699483961,
		road: 0.541574074074074,
		acceleration: -0.0718518518518518
	},
	{
		id: 5,
		time: 4,
		velocity: 0.0422222222222222,
		power: 12.109930170683,
		road: 0.692546296296296,
		acceleration: -0.0428703703703704
	},
	{
		id: 6,
		time: 5,
		velocity: 0.0511111111111111,
		power: 4.78957970306234,
		road: 0.785509259259259,
		acceleration: -0.0731481481481482
	},
	{
		id: 7,
		time: 6,
		velocity: 0.0758333333333333,
		power: 63.174895719,
		road: 0.964444444444444,
		acceleration: 0.245092592592593
	},
	{
		id: 8,
		time: 7,
		velocity: 0.7775,
		power: 219.829880767226,
		road: 1.44398148148148,
		acceleration: 0.356111111111111
	},
	{
		id: 9,
		time: 8,
		velocity: 1.11944444444444,
		power: 766.619359780509,
		road: 2.4425,
		acceleration: 0.681851851851852
	},
	{
		id: 10,
		time: 9,
		velocity: 2.12138888888889,
		power: 1096.15946493112,
		road: 4.07212962962963,
		acceleration: 0.58037037037037
	},
	{
		id: 11,
		time: 10,
		velocity: 2.51861111111111,
		power: 1845.56804307725,
		road: 6.35319444444444,
		acceleration: 0.7225
	},
	{
		id: 12,
		time: 11,
		velocity: 3.28694444444444,
		power: 2241.70999746225,
		road: 9.32652777777778,
		acceleration: 0.662037037037037
	},
	{
		id: 13,
		time: 12,
		velocity: 4.1075,
		power: 1451.3106776853,
		road: 12.7848611111111,
		acceleration: 0.307962962962963
	},
	{
		id: 14,
		time: 13,
		velocity: 3.4425,
		power: 1303.04343543415,
		road: 16.5136111111111,
		acceleration: 0.232870370370371
	},
	{
		id: 15,
		time: 14,
		velocity: 3.98555555555556,
		power: 1704.88604817941,
		road: 20.5149537037037,
		acceleration: 0.312314814814814
	},
	{
		id: 16,
		time: 15,
		velocity: 5.04444444444444,
		power: 2525.15419412438,
		road: 24.9060648148148,
		acceleration: 0.467222222222222
	},
	{
		id: 17,
		time: 16,
		velocity: 4.84416666666667,
		power: 4237.78008114365,
		road: 29.9060648148148,
		acceleration: 0.750555555555556
	},
	{
		id: 18,
		time: 17,
		velocity: 6.23722222222222,
		power: 1924.71828240144,
		road: 35.3937037037037,
		acceleration: 0.224722222222221
	},
	{
		id: 19,
		time: 18,
		velocity: 5.71861111111111,
		power: 3347.71016631906,
		road: 41.2225462962963,
		acceleration: 0.457685185185187
	},
	{
		id: 20,
		time: 19,
		velocity: 6.21722222222222,
		power: 1465.9395302037,
		road: 47.3322222222222,
		acceleration: 0.103981481481481
	},
	{
		id: 21,
		time: 20,
		velocity: 6.54916666666667,
		power: 6399.16792910457,
		road: 53.9280092592593,
		acceleration: 0.868240740740741
	},
	{
		id: 22,
		time: 21,
		velocity: 8.32333333333333,
		power: 7044.67167195566,
		road: 61.3756481481482,
		acceleration: 0.835462962962963
	},
	{
		id: 23,
		time: 22,
		velocity: 8.72361111111111,
		power: 6991.06899351696,
		road: 69.6044907407407,
		acceleration: 0.726944444444444
	},
	{
		id: 24,
		time: 23,
		velocity: 8.73,
		power: 6270.03389592558,
		road: 78.4817592592593,
		acceleration: 0.569907407407406
	},
	{
		id: 25,
		time: 24,
		velocity: 10.0330555555556,
		power: 4578.99130148611,
		road: 87.8133333333333,
		acceleration: 0.338703703703704
	},
	{
		id: 26,
		time: 25,
		velocity: 9.73972222222222,
		power: 3973.70870592728,
		road: 97.4410648148148,
		acceleration: 0.253611111111113
	},
	{
		id: 27,
		time: 26,
		velocity: 9.49083333333333,
		power: -1961.32384651275,
		road: 106.998425925926,
		acceleration: -0.394351851851852
	},
	{
		id: 28,
		time: 27,
		velocity: 8.85,
		power: -2020.5808538367,
		road: 116.155416666667,
		acceleration: -0.406388888888889
	},
	{
		id: 29,
		time: 28,
		velocity: 8.52055555555556,
		power: -3932.40749630459,
		road: 124.785185185185,
		acceleration: -0.648055555555557
	},
	{
		id: 30,
		time: 29,
		velocity: 7.54666666666667,
		power: -3352.89391920855,
		road: 132.789074074074,
		acceleration: -0.603703703703703
	},
	{
		id: 31,
		time: 30,
		velocity: 7.03888888888889,
		power: -7046.44647510421,
		road: 139.89162037037,
		acceleration: -1.19898148148148
	},
	{
		id: 32,
		time: 31,
		velocity: 4.92361111111111,
		power: -2059.99570351612,
		road: 146.146759259259,
		acceleration: -0.495833333333334
	},
	{
		id: 33,
		time: 32,
		velocity: 6.05916666666667,
		power: -553.460209758749,
		road: 152.031064814815,
		acceleration: -0.245833333333334
	},
	{
		id: 34,
		time: 33,
		velocity: 6.30138888888889,
		power: 5988.28640403797,
		road: 158.22625,
		acceleration: 0.867592592592593
	},
	{
		id: 35,
		time: 34,
		velocity: 7.52638888888889,
		power: 3882.12086118868,
		road: 165.07625,
		acceleration: 0.442037037037037
	},
	{
		id: 36,
		time: 35,
		velocity: 7.38527777777778,
		power: 2875.46312058608,
		road: 172.278796296296,
		acceleration: 0.263055555555557
	},
	{
		id: 37,
		time: 36,
		velocity: 7.09055555555556,
		power: 2337.47517079394,
		road: 179.699212962963,
		acceleration: 0.172685185185185
	},
	{
		id: 38,
		time: 37,
		velocity: 8.04444444444444,
		power: 4837.71577939849,
		road: 187.453148148148,
		acceleration: 0.49435185185185
	},
	{
		id: 39,
		time: 38,
		velocity: 8.86833333333333,
		power: 7309.31305393226,
		road: 195.82912037037,
		acceleration: 0.749722222222223
	},
	{
		id: 40,
		time: 39,
		velocity: 9.33972222222222,
		power: 6396.36775615017,
		road: 204.864907407407,
		acceleration: 0.569907407407408
	},
	{
		id: 41,
		time: 40,
		velocity: 9.75416666666667,
		power: 4515.22708226983,
		road: 214.346574074074,
		acceleration: 0.321851851851852
	},
	{
		id: 42,
		time: 41,
		velocity: 9.83388888888889,
		power: 3409.51589486086,
		road: 224.082546296296,
		acceleration: 0.18675925925926
	},
	{
		id: 43,
		time: 42,
		velocity: 9.9,
		power: 1881.03279318644,
		road: 233.92125,
		acceleration: 0.0187037037037037
	},
	{
		id: 44,
		time: 43,
		velocity: 9.81027777777778,
		power: 2051.90858865079,
		road: 243.787314814815,
		acceleration: 0.0360185185185173
	},
	{
		id: 45,
		time: 44,
		velocity: 9.94194444444444,
		power: 1163.42633501023,
		road: 253.642268518518,
		acceleration: -0.0582407407407395
	},
	{
		id: 46,
		time: 45,
		velocity: 9.72527777777778,
		power: 1688.93951110704,
		road: 263.467407407407,
		acceleration: -0.00138888888888822
	},
	{
		id: 47,
		time: 46,
		velocity: 9.80611111111111,
		power: 1165.91300418618,
		road: 273.263564814815,
		acceleration: -0.0565740740740761
	},
	{
		id: 48,
		time: 47,
		velocity: 9.77222222222222,
		power: 1282.10630718518,
		road: 283.01,
		acceleration: -0.0428703703703697
	},
	{
		id: 49,
		time: 48,
		velocity: 9.59666666666667,
		power: 844.945323096091,
		road: 292.690694444444,
		acceleration: -0.0886111111111099
	},
	{
		id: 50,
		time: 49,
		velocity: 9.54027777777778,
		power: 963.492153195444,
		road: 302.290092592593,
		acceleration: -0.0739814814814821
	},
	{
		id: 51,
		time: 50,
		velocity: 9.55027777777778,
		power: 1256.75451965809,
		road: 311.832268518519,
		acceleration: -0.0404629629629625
	},
	{
		id: 52,
		time: 51,
		velocity: 9.47527777777778,
		power: 783.662048497483,
		road: 321.308611111111,
		acceleration: -0.0912037037037052
	},
	{
		id: 53,
		time: 52,
		velocity: 9.26666666666667,
		power: 308.100877799005,
		road: 330.668240740741,
		acceleration: -0.142222222222223
	},
	{
		id: 54,
		time: 53,
		velocity: 9.12361111111111,
		power: 32.8907659927,
		road: 339.871064814815,
		acceleration: -0.171388888888888
	},
	{
		id: 55,
		time: 54,
		velocity: 8.96111111111111,
		power: 626.215780068404,
		road: 348.937592592593,
		acceleration: -0.101203703703703
	},
	{
		id: 56,
		time: 55,
		velocity: 8.96305555555556,
		power: 816.455346049361,
		road: 357.914861111111,
		acceleration: -0.0773148148148159
	},
	{
		id: 57,
		time: 56,
		velocity: 8.89166666666667,
		power: 1289.52159119031,
		road: 366.843148148148,
		acceleration: -0.0206481481481475
	},
	{
		id: 58,
		time: 57,
		velocity: 8.89916666666667,
		power: 1580.65313619539,
		road: 375.767962962963,
		acceleration: 0.0137037037037047
	},
	{
		id: 59,
		time: 58,
		velocity: 9.00416666666667,
		power: 1469.10813842358,
		road: 384.699814814815,
		acceleration: 0.000370370370369244
	},
	{
		id: 60,
		time: 59,
		velocity: 8.89277777777778,
		power: 2690.25941243413,
		road: 393.702222222222,
		acceleration: 0.140740740740743
	},
	{
		id: 61,
		time: 60,
		velocity: 9.32138888888889,
		power: 2714.59528532563,
		road: 402.843657407407,
		acceleration: 0.137314814814815
	},
	{
		id: 62,
		time: 61,
		velocity: 9.41611111111111,
		power: 5287.35441529974,
		road: 412.259814814815,
		acceleration: 0.412129629629629
	},
	{
		id: 63,
		time: 62,
		velocity: 10.1291666666667,
		power: 5911.14053139717,
		road: 422.106064814815,
		acceleration: 0.448055555555555
	},
	{
		id: 64,
		time: 63,
		velocity: 10.6655555555556,
		power: 5746.75713346253,
		road: 432.376481481481,
		acceleration: 0.400277777777779
	},
	{
		id: 65,
		time: 64,
		velocity: 10.6169444444444,
		power: 5394.8094969625,
		road: 443.017361111111,
		acceleration: 0.340648148148148
	},
	{
		id: 66,
		time: 65,
		velocity: 11.1511111111111,
		power: 6116.80316324267,
		road: 454.022222222222,
		acceleration: 0.387314814814815
	},
	{
		id: 67,
		time: 66,
		velocity: 11.8275,
		power: 7413.66283305291,
		road: 465.46,
		acceleration: 0.47851851851852
	},
	{
		id: 68,
		time: 67,
		velocity: 12.0525,
		power: 3961.07716000914,
		road: 477.211111111111,
		acceleration: 0.148148148148147
	},
	{
		id: 69,
		time: 68,
		velocity: 11.5955555555556,
		power: 2436.70424679165,
		road: 489.041111111111,
		acceleration: 0.00962962962962877
	},
	{
		id: 70,
		time: 69,
		velocity: 11.8563888888889,
		power: 4813.40206040322,
		road: 500.983287037037,
		acceleration: 0.214722222222221
	},
	{
		id: 71,
		time: 70,
		velocity: 12.6966666666667,
		power: 8104.93876650938,
		road: 513.272175925926,
		acceleration: 0.478703703703705
	},
	{
		id: 72,
		time: 71,
		velocity: 13.0316666666667,
		power: 8831.44308093067,
		road: 526.052685185185,
		acceleration: 0.504537037037036
	},
	{
		id: 73,
		time: 72,
		velocity: 13.37,
		power: 6858.38345870055,
		road: 539.245,
		acceleration: 0.319074074074077
	},
	{
		id: 74,
		time: 73,
		velocity: 13.6538888888889,
		power: 7599.10595193389,
		road: 552.775648148148,
		acceleration: 0.35759259259259
	},
	{
		id: 75,
		time: 74,
		velocity: 14.1044444444444,
		power: 7911.48930843105,
		road: 566.665231481481,
		acceleration: 0.36027777777778
	},
	{
		id: 76,
		time: 75,
		velocity: 14.4508333333333,
		power: 8460.95448380042,
		road: 580.924537037037,
		acceleration: 0.379166666666666
	},
	{
		id: 77,
		time: 76,
		velocity: 14.7913888888889,
		power: 5943.31996572394,
		road: 595.464074074074,
		acceleration: 0.181296296296296
	},
	{
		id: 78,
		time: 77,
		velocity: 14.6483333333333,
		power: 1584.07568120293,
		road: 610.027685185185,
		acceleration: -0.133148148148148
	},
	{
		id: 79,
		time: 78,
		velocity: 14.0513888888889,
		power: -1819.53704323431,
		road: 624.336712962963,
		acceleration: -0.376018518518519
	},
	{
		id: 80,
		time: 79,
		velocity: 13.6633333333333,
		power: -2791.06907339256,
		road: 638.234398148148,
		acceleration: -0.446666666666667
	},
	{
		id: 81,
		time: 80,
		velocity: 13.3083333333333,
		power: -2068.71585756396,
		road: 651.713425925926,
		acceleration: -0.390648148148149
	},
	{
		id: 82,
		time: 81,
		velocity: 12.8794444444444,
		power: -2349.78118462007,
		road: 664.791064814815,
		acceleration: -0.412129629629629
	},
	{
		id: 83,
		time: 82,
		velocity: 12.4269444444444,
		power: -4890.89082677363,
		road: 677.35037037037,
		acceleration: -0.624537037037037
	},
	{
		id: 84,
		time: 83,
		velocity: 11.4347222222222,
		power: -6570.17349916147,
		road: 689.203472222222,
		acceleration: -0.787870370370371
	},
	{
		id: 85,
		time: 84,
		velocity: 10.5158333333333,
		power: -8675.21239763777,
		road: 700.149398148148,
		acceleration: -1.02648148148148
	},
	{
		id: 86,
		time: 85,
		velocity: 9.3475,
		power: -7851.64301712576,
		road: 710.075416666667,
		acceleration: -1.01333333333333
	},
	{
		id: 87,
		time: 86,
		velocity: 8.39472222222222,
		power: -8689.42924067575,
		road: 718.891481481481,
		acceleration: -1.20657407407407
	},
	{
		id: 88,
		time: 87,
		velocity: 6.89611111111111,
		power: -7008.60866434864,
		road: 726.542638888889,
		acceleration: -1.12324074074074
	},
	{
		id: 89,
		time: 88,
		velocity: 5.97777777777778,
		power: -7004.51385732075,
		road: 732.984953703704,
		acceleration: -1.29444444444445
	},
	{
		id: 90,
		time: 89,
		velocity: 4.51138888888889,
		power: -2859.93401081396,
		road: 738.431712962963,
		acceleration: -0.696666666666666
	},
	{
		id: 91,
		time: 90,
		velocity: 4.80611111111111,
		power: 464.072788998667,
		road: 743.507314814815,
		acceleration: -0.0456481481481488
	},
	{
		id: 92,
		time: 91,
		velocity: 5.84083333333333,
		power: 9623.76584311041,
		road: 749.352638888889,
		acceleration: 1.58509259259259
	},
	{
		id: 93,
		time: 92,
		velocity: 9.26666666666667,
		power: 12157.5153414319,
		road: 756.772268518518,
		acceleration: 1.56351851851852
	},
	{
		id: 94,
		time: 93,
		velocity: 9.49666666666667,
		power: 14013.1499587512,
		road: 765.710509259259,
		acceleration: 1.4737037037037
	},
	{
		id: 95,
		time: 94,
		velocity: 10.2619444444444,
		power: 6251.76554147398,
		road: 775.625092592593,
		acceleration: 0.478981481481481
	},
	{
		id: 96,
		time: 95,
		velocity: 10.7036111111111,
		power: 8175.15405871608,
		road: 786.094212962963,
		acceleration: 0.630092592592591
	},
	{
		id: 97,
		time: 96,
		velocity: 11.3869444444444,
		power: 4594.15734537798,
		road: 797.002037037037,
		acceleration: 0.247314814814818
	},
	{
		id: 98,
		time: 97,
		velocity: 11.0038888888889,
		power: 2897.89762658332,
		road: 808.072546296296,
		acceleration: 0.0780555555555527
	},
	{
		id: 99,
		time: 98,
		velocity: 10.9377777777778,
		power: 464.969381778731,
		road: 819.106157407407,
		acceleration: -0.151851851851852
	},
	{
		id: 100,
		time: 99,
		velocity: 10.9313888888889,
		power: -3535.4467688828,
		road: 829.794398148148,
		acceleration: -0.53888888888889
	},
	{
		id: 101,
		time: 100,
		velocity: 9.38722222222222,
		power: -5271.939481432,
		road: 839.845740740741,
		acceleration: -0.734907407407405
	},
	{
		id: 102,
		time: 101,
		velocity: 8.73305555555556,
		power: -6549.75351618468,
		road: 849.069074074074,
		acceleration: -0.921111111111111
	},
	{
		id: 103,
		time: 102,
		velocity: 8.16805555555556,
		power: -2487.73443804051,
		road: 857.594398148148,
		acceleration: -0.474907407407407
	},
	{
		id: 104,
		time: 103,
		velocity: 7.9625,
		power: -919.054902710753,
		road: 865.740601851852,
		acceleration: -0.283333333333333
	},
	{
		id: 105,
		time: 104,
		velocity: 7.88305555555556,
		power: 2494.76208800801,
		road: 873.825138888889,
		acceleration: 0.159999999999998
	},
	{
		id: 106,
		time: 105,
		velocity: 8.64805555555555,
		power: 3842.58446381192,
		road: 882.148935185185,
		acceleration: 0.318518518518518
	},
	{
		id: 107,
		time: 106,
		velocity: 8.91805555555555,
		power: 8008.9211045938,
		road: 891.020138888889,
		acceleration: 0.776296296296296
	},
	{
		id: 108,
		time: 107,
		velocity: 10.2119444444444,
		power: 6870.15835076281,
		road: 900.567777777778,
		acceleration: 0.576574074074076
	},
	{
		id: 109,
		time: 108,
		velocity: 10.3777777777778,
		power: 7810.77475888695,
		road: 910.714861111111,
		acceleration: 0.622314814814814
	},
	{
		id: 110,
		time: 109,
		velocity: 10.785,
		power: 3345.48425862616,
		road: 921.244814814815,
		acceleration: 0.143425925925927
	},
	{
		id: 111,
		time: 110,
		velocity: 10.6422222222222,
		power: 1567.46267368111,
		road: 931.828888888889,
		acceleration: -0.0351851851851865
	},
	{
		id: 112,
		time: 111,
		velocity: 10.2722222222222,
		power: -1011.19770636013,
		road: 942.250138888889,
		acceleration: -0.290462962962962
	},
	{
		id: 113,
		time: 112,
		velocity: 9.91361111111111,
		power: -400.907143633203,
		road: 952.412638888889,
		acceleration: -0.227037037037036
	},
	{
		id: 114,
		time: 113,
		velocity: 9.96111111111111,
		power: 704.586432318423,
		road: 962.406759259259,
		acceleration: -0.109722222222222
	},
	{
		id: 115,
		time: 114,
		velocity: 9.94305555555556,
		power: 1083.83360313454,
		road: 972.312083333333,
		acceleration: -0.06787037037037
	},
	{
		id: 116,
		time: 115,
		velocity: 9.71,
		power: -749.482247616341,
		road: 982.052638888889,
		acceleration: -0.261666666666667
	},
	{
		id: 117,
		time: 116,
		velocity: 9.17611111111111,
		power: -1528.59120215538,
		road: 991.488518518519,
		acceleration: -0.347685185185185
	},
	{
		id: 118,
		time: 117,
		velocity: 8.9,
		power: -3533.91564906753,
		road: 1000.45726851852,
		acceleration: -0.586574074074075
	},
	{
		id: 119,
		time: 118,
		velocity: 7.95027777777778,
		power: -850.712219085448,
		road: 1008.99615740741,
		acceleration: -0.273148148148147
	},
	{
		id: 120,
		time: 119,
		velocity: 8.35666666666667,
		power: 676.821168946466,
		road: 1017.35759259259,
		acceleration: -0.0817592592592593
	},
	{
		id: 121,
		time: 120,
		velocity: 8.65472222222222,
		power: 4892.13905062656,
		road: 1025.89486111111,
		acceleration: 0.433425925925924
	},
	{
		id: 122,
		time: 121,
		velocity: 9.25055555555556,
		power: 4836.35935359003,
		road: 1034.84625,
		acceleration: 0.394814814814815
	},
	{
		id: 123,
		time: 122,
		velocity: 9.54111111111111,
		power: 5824.63515230486,
		road: 1044.23226851852,
		acceleration: 0.474444444444446
	},
	{
		id: 124,
		time: 123,
		velocity: 10.0780555555556,
		power: 2842.18287541535,
		road: 1053.91935185185,
		acceleration: 0.127685185185184
	},
	{
		id: 125,
		time: 124,
		velocity: 9.63361111111111,
		power: 882.71838458648,
		road: 1063.62773148148,
		acceleration: -0.0850925925925914
	},
	{
		id: 126,
		time: 125,
		velocity: 9.28583333333333,
		power: -647.769538841942,
		road: 1073.16856481481,
		acceleration: -0.25
	},
	{
		id: 127,
		time: 126,
		velocity: 9.32805555555556,
		power: 55.1149133253228,
		road: 1082.49925925926,
		acceleration: -0.170277777777777
	},
	{
		id: 128,
		time: 127,
		velocity: 9.12277777777778,
		power: -1276.50541893103,
		road: 1091.58407407407,
		acceleration: -0.321481481481481
	},
	{
		id: 129,
		time: 128,
		velocity: 8.32138888888889,
		power: -1589.08728995822,
		road: 1100.3274537037,
		acceleration: -0.361388888888889
	},
	{
		id: 130,
		time: 129,
		velocity: 8.24388888888889,
		power: 198.871273866269,
		road: 1108.81842592593,
		acceleration: -0.143425925925929
	},
	{
		id: 131,
		time: 130,
		velocity: 8.6925,
		power: 3307.68653325747,
		road: 1117.35689814815,
		acceleration: 0.238425925925927
	},
	{
		id: 132,
		time: 131,
		velocity: 9.03666666666667,
		power: 4363.99038591603,
		road: 1126.18837962963,
		acceleration: 0.347592592592591
	},
	{
		id: 133,
		time: 132,
		velocity: 9.28666666666667,
		power: 1953.97095987021,
		road: 1135.22055555556,
		acceleration: 0.0537962962962979
	},
	{
		id: 134,
		time: 133,
		velocity: 8.85388888888889,
		power: -801.944806600148,
		road: 1144.1462962963,
		acceleration: -0.266666666666666
	},
	{
		id: 135,
		time: 134,
		velocity: 8.23666666666667,
		power: -2148.69403879893,
		road: 1152.72268518518,
		acceleration: -0.432037037037038
	},
	{
		id: 136,
		time: 135,
		velocity: 7.99055555555556,
		power: -258.285634462339,
		road: 1160.9837037037,
		acceleration: -0.198703703703703
	},
	{
		id: 137,
		time: 136,
		velocity: 8.25777777777778,
		power: 2296.35971984055,
		road: 1169.20925925926,
		acceleration: 0.127777777777776
	},
	{
		id: 138,
		time: 137,
		velocity: 8.62,
		power: 3502.41417861446,
		road: 1177.63337962963,
		acceleration: 0.269351851851852
	},
	{
		id: 139,
		time: 138,
		velocity: 8.79861111111111,
		power: 3823.0940898195,
		road: 1186.33777777778,
		acceleration: 0.291203703703705
	},
	{
		id: 140,
		time: 139,
		velocity: 9.13138888888889,
		power: 3471.93730965821,
		road: 1195.30472222222,
		acceleration: 0.233888888888888
	},
	{
		id: 141,
		time: 140,
		velocity: 9.32166666666667,
		power: 3934.89874206441,
		road: 1204.525,
		acceleration: 0.27277777777778
	},
	{
		id: 142,
		time: 141,
		velocity: 9.61694444444444,
		power: 2511.41391111044,
		road: 1213.93314814815,
		acceleration: 0.102962962962962
	},
	{
		id: 143,
		time: 142,
		velocity: 9.44027777777778,
		power: 3611.46126125349,
		road: 1223.50138888889,
		acceleration: 0.217222222222222
	},
	{
		id: 144,
		time: 143,
		velocity: 9.97333333333333,
		power: 3478.08412307068,
		road: 1233.27439814815,
		acceleration: 0.192314814814814
	},
	{
		id: 145,
		time: 144,
		velocity: 10.1938888888889,
		power: 3468.22412055263,
		road: 1243.23462962963,
		acceleration: 0.182129629629632
	},
	{
		id: 146,
		time: 145,
		velocity: 9.98666666666667,
		power: 484.501071412251,
		road: 1253.21958333333,
		acceleration: -0.132685185185188
	},
	{
		id: 147,
		time: 146,
		velocity: 9.57527777777778,
		power: -1281.84094584454,
		road: 1262.9787037037,
		acceleration: -0.31898148148148
	},
	{
		id: 148,
		time: 147,
		velocity: 9.23694444444445,
		power: -2957.03804854187,
		road: 1272.32388888889,
		acceleration: -0.50888888888889
	},
	{
		id: 149,
		time: 148,
		velocity: 8.46,
		power: -3173.91915834328,
		road: 1281.1399537037,
		acceleration: -0.549351851851851
	},
	{
		id: 150,
		time: 149,
		velocity: 7.92722222222222,
		power: -3250.23364484891,
		road: 1289.39152777778,
		acceleration: -0.57962962962963
	},
	{
		id: 151,
		time: 150,
		velocity: 7.49805555555556,
		power: -1431.61178370016,
		road: 1297.17587962963,
		acceleration: -0.354814814814815
	},
	{
		id: 152,
		time: 151,
		velocity: 7.39555555555556,
		power: -138.602422870964,
		road: 1304.69351851852,
		acceleration: -0.178611111111111
	},
	{
		id: 153,
		time: 152,
		velocity: 7.39138888888889,
		power: -589.764306323354,
		road: 1312.00069444444,
		acceleration: -0.242314814814815
	},
	{
		id: 154,
		time: 153,
		velocity: 6.77111111111111,
		power: -1393.2100386555,
		road: 1319.00462962963,
		acceleration: -0.364166666666668
	},
	{
		id: 155,
		time: 154,
		velocity: 6.30305555555556,
		power: -2539.87945789953,
		road: 1325.54657407407,
		acceleration: -0.559814814814814
	},
	{
		id: 156,
		time: 155,
		velocity: 5.71194444444444,
		power: -1265.07843403603,
		road: 1331.6250462963,
		acceleration: -0.36712962962963
	},
	{
		id: 157,
		time: 156,
		velocity: 5.66972222222222,
		power: -436.074428579018,
		road: 1337.40717592593,
		acceleration: -0.225555555555555
	},
	{
		id: 158,
		time: 157,
		velocity: 5.62638888888889,
		power: -42.8567656029148,
		road: 1343,
		acceleration: -0.153055555555555
	},
	{
		id: 159,
		time: 158,
		velocity: 5.25277777777778,
		power: -1638.22362235212,
		road: 1348.28152777778,
		acceleration: -0.469537037037037
	},
	{
		id: 160,
		time: 159,
		velocity: 4.26111111111111,
		power: -3985.02619091942,
		road: 1352.79388888889,
		acceleration: -1.0687962962963
	},
	{
		id: 161,
		time: 160,
		velocity: 2.42,
		power: -3320.67231587164,
		road: 1356.18972222222,
		acceleration: -1.16425925925926
	},
	{
		id: 162,
		time: 161,
		velocity: 1.76,
		power: -1620.64776947216,
		road: 1358.58083333333,
		acceleration: -0.845185185185185
	},
	{
		id: 163,
		time: 162,
		velocity: 1.72555555555556,
		power: -296.972577460128,
		road: 1360.39856481481,
		acceleration: -0.301574074074074
	},
	{
		id: 164,
		time: 163,
		velocity: 1.51527777777778,
		power: 260.770708409771,
		road: 1362.08263888889,
		acceleration: 0.0342592592592592
	},
	{
		id: 165,
		time: 164,
		velocity: 1.86277777777778,
		power: 857.913234167623,
		road: 1363.9600462963,
		acceleration: 0.352407407407407
	},
	{
		id: 166,
		time: 165,
		velocity: 2.78277777777778,
		power: 1617.10072448283,
		road: 1366.31092592593,
		acceleration: 0.594537037037037
	},
	{
		id: 167,
		time: 166,
		velocity: 3.29888888888889,
		power: 1462.19695278912,
		road: 1369.16319444444,
		acceleration: 0.40824074074074
	},
	{
		id: 168,
		time: 167,
		velocity: 3.0875,
		power: 153.811900065469,
		road: 1372.18018518518,
		acceleration: -0.0787962962962965
	},
	{
		id: 169,
		time: 168,
		velocity: 2.54638888888889,
		power: -338.107777406672,
		road: 1375.02925925926,
		acceleration: -0.257037037037036
	},
	{
		id: 170,
		time: 169,
		velocity: 2.52777777777778,
		power: -110.298830402212,
		road: 1377.66208333333,
		acceleration: -0.175462962962964
	},
	{
		id: 171,
		time: 170,
		velocity: 2.56111111111111,
		power: -59.8036109553185,
		road: 1380.12898148148,
		acceleration: -0.156388888888888
	},
	{
		id: 172,
		time: 171,
		velocity: 2.07722222222222,
		power: -484.07511609149,
		road: 1382.33703703704,
		acceleration: -0.361296296296296
	},
	{
		id: 173,
		time: 172,
		velocity: 1.44388888888889,
		power: 437.311822855593,
		road: 1384.41069444444,
		acceleration: 0.0924999999999998
	},
	{
		id: 174,
		time: 173,
		velocity: 2.83861111111111,
		power: 3033.25576275742,
		road: 1387.06666666667,
		acceleration: 1.07212962962963
	},
	{
		id: 175,
		time: 174,
		velocity: 5.29361111111111,
		power: 6271.73706249941,
		road: 1391.02481481481,
		acceleration: 1.53222222222222
	},
	{
		id: 176,
		time: 175,
		velocity: 6.04055555555555,
		power: 7236.50262422186,
		road: 1396.38722222222,
		acceleration: 1.2762962962963
	},
	{
		id: 177,
		time: 176,
		velocity: 6.6675,
		power: 5022.23887266023,
		road: 1402.72916666667,
		acceleration: 0.682777777777778
	},
	{
		id: 178,
		time: 177,
		velocity: 7.34194444444444,
		power: 5070.29911470696,
		road: 1409.71648148148,
		acceleration: 0.607962962962963
	},
	{
		id: 179,
		time: 178,
		velocity: 7.86444444444444,
		power: 3659.49634643529,
		road: 1417.18583333333,
		acceleration: 0.356111111111111
	},
	{
		id: 180,
		time: 179,
		velocity: 7.73583333333333,
		power: -1113.59023134803,
		road: 1424.67560185185,
		acceleration: -0.315277777777777
	},
	{
		id: 181,
		time: 180,
		velocity: 6.39611111111111,
		power: -4209.54777733074,
		road: 1431.61138888889,
		acceleration: -0.792685185185185
	},
	{
		id: 182,
		time: 181,
		velocity: 5.48638888888889,
		power: -5013.47261810236,
		road: 1437.63939814815,
		acceleration: -1.02287037037037
	},
	{
		id: 183,
		time: 182,
		velocity: 4.66722222222222,
		power: -5454.74654423807,
		road: 1442.49416666667,
		acceleration: -1.32361111111111
	},
	{
		id: 184,
		time: 183,
		velocity: 2.42527777777778,
		power: -4688.08383178093,
		road: 1445.89356481481,
		acceleration: -1.58712962962963
	},
	{
		id: 185,
		time: 184,
		velocity: 0.725,
		power: -2472.21595858965,
		road: 1447.72152777778,
		acceleration: -1.55574074074074
	},
	{
		id: 186,
		time: 185,
		velocity: 0,
		power: -416.610182931611,
		road: 1448.36740740741,
		acceleration: -0.808425925925926
	},
	{
		id: 187,
		time: 186,
		velocity: 0,
		power: -13.0656447368421,
		road: 1448.48824074074,
		acceleration: -0.241666666666667
	},
	{
		id: 188,
		time: 187,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 189,
		time: 188,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 190,
		time: 189,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 191,
		time: 190,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 192,
		time: 191,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 193,
		time: 192,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 194,
		time: 193,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 195,
		time: 194,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 196,
		time: 195,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 197,
		time: 196,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 198,
		time: 197,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 199,
		time: 198,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 200,
		time: 199,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 201,
		time: 200,
		velocity: 0,
		power: 0,
		road: 1448.48824074074,
		acceleration: 0
	},
	{
		id: 202,
		time: 201,
		velocity: 0,
		power: 2.87831646494863,
		road: 1448.50671296296,
		acceleration: 0.0369444444444444
	},
	{
		id: 203,
		time: 202,
		velocity: 0.110833333333333,
		power: 4.4635773743417,
		road: 1448.54365740741,
		acceleration: 0
	},
	{
		id: 204,
		time: 203,
		velocity: 0,
		power: 4.4635773743417,
		road: 1448.58060185185,
		acceleration: 0
	},
	{
		id: 205,
		time: 204,
		velocity: 0,
		power: 1.58524722222222,
		road: 1448.59907407407,
		acceleration: -0.0369444444444444
	},
	{
		id: 206,
		time: 205,
		velocity: 0,
		power: 0,
		road: 1448.59907407407,
		acceleration: 0
	},
	{
		id: 207,
		time: 206,
		velocity: 0,
		power: 0,
		road: 1448.59907407407,
		acceleration: 0
	},
	{
		id: 208,
		time: 207,
		velocity: 0,
		power: 0,
		road: 1448.59907407407,
		acceleration: 0
	},
	{
		id: 209,
		time: 208,
		velocity: 0,
		power: 0,
		road: 1448.59907407407,
		acceleration: 0
	},
	{
		id: 210,
		time: 209,
		velocity: 0,
		power: 0,
		road: 1448.59907407407,
		acceleration: 0
	},
	{
		id: 211,
		time: 210,
		velocity: 0,
		power: 0,
		road: 1448.59907407407,
		acceleration: 0
	},
	{
		id: 212,
		time: 211,
		velocity: 0,
		power: 0,
		road: 1448.59907407407,
		acceleration: 0
	},
	{
		id: 213,
		time: 212,
		velocity: 0,
		power: 0,
		road: 1448.59907407407,
		acceleration: 0
	},
	{
		id: 214,
		time: 213,
		velocity: 0,
		power: 0,
		road: 1448.59907407407,
		acceleration: 0
	},
	{
		id: 215,
		time: 214,
		velocity: 0,
		power: 0,
		road: 1448.59907407407,
		acceleration: 0
	},
	{
		id: 216,
		time: 215,
		velocity: 0,
		power: 417.519033323941,
		road: 1449.03759259259,
		acceleration: 0.877037037037037
	},
	{
		id: 217,
		time: 216,
		velocity: 2.63111111111111,
		power: 2592.99221052702,
		road: 1450.68162037037,
		acceleration: 1.53398148148148
	},
	{
		id: 218,
		time: 217,
		velocity: 4.60194444444444,
		power: 7643.55493819594,
		road: 1454.17699074074,
		acceleration: 2.1687037037037
	},
	{
		id: 219,
		time: 218,
		velocity: 6.50611111111111,
		power: 6431.60840869742,
		road: 1459.34083333333,
		acceleration: 1.16824074074074
	},
	{
		id: 220,
		time: 219,
		velocity: 6.13583333333333,
		power: 3918.61840237984,
		road: 1465.3574537037,
		acceleration: 0.537314814814815
	},
	{
		id: 221,
		time: 220,
		velocity: 6.21388888888889,
		power: 2467.03865729153,
		road: 1471.76976851852,
		acceleration: 0.254074074074074
	},
	{
		id: 222,
		time: 221,
		velocity: 7.26833333333333,
		power: 6816.03344133668,
		road: 1478.74532407407,
		acceleration: 0.872407407407407
	},
	{
		id: 223,
		time: 222,
		velocity: 8.75305555555556,
		power: 16375.9762811006,
		road: 1487.10268518519,
		acceleration: 1.8912037037037
	},
	{
		id: 224,
		time: 223,
		velocity: 11.8875,
		power: 21361.3948870464,
		road: 1497.40018518519,
		acceleration: 1.98907407407408
	},
	{
		id: 225,
		time: 224,
		velocity: 13.2355555555556,
		power: 24285.9545348291,
		road: 1509.62712962963,
		acceleration: 1.86981481481481
	},
	{
		id: 226,
		time: 225,
		velocity: 14.3625,
		power: 22333.302906729,
		road: 1523.51291666667,
		acceleration: 1.44787037037037
	},
	{
		id: 227,
		time: 226,
		velocity: 16.2311111111111,
		power: 25021.4910394694,
		road: 1538.84625,
		acceleration: 1.44722222222222
	},
	{
		id: 228,
		time: 227,
		velocity: 17.5772222222222,
		power: 20975.1879063752,
		road: 1555.42347222222,
		acceleration: 1.04055555555556
	},
	{
		id: 229,
		time: 228,
		velocity: 17.4841666666667,
		power: 9189.46157882157,
		road: 1572.65236111111,
		acceleration: 0.262777777777778
	},
	{
		id: 230,
		time: 229,
		velocity: 17.0194444444444,
		power: 2209.65590281828,
		road: 1589.93162037037,
		acceleration: -0.162037037037038
	},
	{
		id: 231,
		time: 230,
		velocity: 17.0911111111111,
		power: 2306.46524218676,
		road: 1607.05393518519,
		acceleration: -0.151851851851852
	},
	{
		id: 232,
		time: 231,
		velocity: 17.0286111111111,
		power: -1076.34843455359,
		road: 1623.92305555556,
		acceleration: -0.354537037037034
	},
	{
		id: 233,
		time: 232,
		velocity: 15.9558333333333,
		power: -14816.2872771765,
		road: 1639.9962037037,
		acceleration: -1.23740740740741
	},
	{
		id: 234,
		time: 233,
		velocity: 13.3788888888889,
		power: -17321.1389923408,
		road: 1654.70962962963,
		acceleration: -1.48203703703704
	},
	{
		id: 235,
		time: 234,
		velocity: 12.5825,
		power: -13946.9915379528,
		road: 1668.01939814815,
		acceleration: -1.32527777777778
	},
	{
		id: 236,
		time: 235,
		velocity: 11.98,
		power: -3493.71753556432,
		road: 1680.41175925926,
		acceleration: -0.509537037037038
	},
	{
		id: 237,
		time: 236,
		velocity: 11.8502777777778,
		power: 559.56657922717,
		road: 1692.46898148148,
		acceleration: -0.160740740740739
	},
	{
		id: 238,
		time: 237,
		velocity: 12.1002777777778,
		power: 3217.94921421663,
		road: 1704.48185185185,
		acceleration: 0.0720370370370347
	},
	{
		id: 239,
		time: 238,
		velocity: 12.1961111111111,
		power: 4762.51804886221,
		road: 1716.63087962963,
		acceleration: 0.200277777777778
	},
	{
		id: 240,
		time: 239,
		velocity: 12.4511111111111,
		power: 4376.83905277215,
		road: 1728.95953703704,
		acceleration: 0.158981481481483
	},
	{
		id: 241,
		time: 240,
		velocity: 12.5772222222222,
		power: 2014.96384178978,
		road: 1741.34597222222,
		acceleration: -0.0434259259259271
	},
	{
		id: 242,
		time: 241,
		velocity: 12.0658333333333,
		power: 2176.59854557482,
		road: 1753.69634259259,
		acceleration: -0.0287037037037017
	},
	{
		id: 243,
		time: 242,
		velocity: 12.365,
		power: 3187.35056635066,
		road: 1766.06064814815,
		acceleration: 0.0565740740740726
	},
	{
		id: 244,
		time: 243,
		velocity: 12.7469444444444,
		power: 7760.3241964978,
		road: 1778.66726851852,
		acceleration: 0.428055555555558
	},
	{
		id: 245,
		time: 244,
		velocity: 13.35,
		power: 8604.32309403777,
		road: 1791.72143518519,
		acceleration: 0.467037037037034
	},
	{
		id: 246,
		time: 245,
		velocity: 13.7661111111111,
		power: 10187.4388576701,
		road: 1805.28680555556,
		acceleration: 0.555370370370373
	},
	{
		id: 247,
		time: 246,
		velocity: 14.4130555555556,
		power: 10139.3080329623,
		road: 1819.38657407407,
		acceleration: 0.513425925925924
	},
	{
		id: 248,
		time: 247,
		velocity: 14.8902777777778,
		power: 10268.1092711388,
		road: 1833.98731481482,
		acceleration: 0.488518518518518
	},
	{
		id: 249,
		time: 248,
		velocity: 15.2316666666667,
		power: 7717.19070822897,
		road: 1848.97472222222,
		acceleration: 0.284814814814816
	},
	{
		id: 250,
		time: 249,
		velocity: 15.2675,
		power: 4984.83221496177,
		road: 1864.14782407407,
		acceleration: 0.0865740740740737
	},
	{
		id: 251,
		time: 250,
		velocity: 15.15,
		power: 3295.47816747541,
		road: 1879.3487962963,
		acceleration: -0.0308333333333337
	},
	{
		id: 252,
		time: 251,
		velocity: 15.1391666666667,
		power: 2124.86207132691,
		road: 1894.47958333333,
		acceleration: -0.109537037037036
	},
	{
		id: 253,
		time: 252,
		velocity: 14.9388888888889,
		power: 1791.95912757178,
		road: 1909.49083333333,
		acceleration: -0.129537037037036
	},
	{
		id: 254,
		time: 253,
		velocity: 14.7613888888889,
		power: 1311.23287626783,
		road: 1924.3574537037,
		acceleration: -0.159722222222225
	},
	{
		id: 255,
		time: 254,
		velocity: 14.66,
		power: 2890.89197814884,
		road: 1939.12152777778,
		acceleration: -0.0453703703703709
	},
	{
		id: 256,
		time: 255,
		velocity: 14.8027777777778,
		power: 3574.44447766706,
		road: 1953.86481481481,
		acceleration: 0.00379629629629719
	},
	{
		id: 257,
		time: 256,
		velocity: 14.7727777777778,
		power: 3905.54034477617,
		road: 1968.62337962963,
		acceleration: 0.0267592592592596
	},
	{
		id: 258,
		time: 257,
		velocity: 14.7402777777778,
		power: 2447.8068044827,
		road: 1983.35736111111,
		acceleration: -0.0759259259259242
	},
	{
		id: 259,
		time: 258,
		velocity: 14.575,
		power: 2481.92470893145,
		road: 1998.01768518519,
		acceleration: -0.0713888888888903
	},
	{
		id: 260,
		time: 259,
		velocity: 14.5586111111111,
		power: 2660.24674888007,
		road: 2012.61393518519,
		acceleration: -0.056759259259259
	},
	{
		id: 261,
		time: 260,
		velocity: 14.57,
		power: 2955.5445742287,
		road: 2027.16472222222,
		acceleration: -0.0341666666666676
	},
	{
		id: 262,
		time: 261,
		velocity: 14.4725,
		power: -907.656985831663,
		road: 2041.54337962963,
		acceleration: -0.310092592592591
	},
	{
		id: 263,
		time: 262,
		velocity: 13.6283333333333,
		power: 1990.87619645051,
		road: 2055.72013888889,
		acceleration: -0.0937037037037047
	},
	{
		id: 264,
		time: 263,
		velocity: 14.2888888888889,
		power: 2085.00997405572,
		road: 2069.80787037037,
		acceleration: -0.0843518518518529
	},
	{
		id: 265,
		time: 264,
		velocity: 14.2194444444444,
		power: 5671.2270320435,
		road: 2083.94342592593,
		acceleration: 0.18
	},
	{
		id: 266,
		time: 265,
		velocity: 14.1683333333333,
		power: 2567.92046349529,
		road: 2098.14310185185,
		acceleration: -0.0517592592592582
	},
	{
		id: 267,
		time: 266,
		velocity: 14.1336111111111,
		power: 1913.62622313207,
		road: 2112.26787037037,
		acceleration: -0.098055555555554
	},
	{
		id: 268,
		time: 267,
		velocity: 13.9252777777778,
		power: 1782.68288026024,
		road: 2126.29101851852,
		acceleration: -0.105185185185187
	},
	{
		id: 269,
		time: 268,
		velocity: 13.8527777777778,
		power: 1775.85108690218,
		road: 2140.2100462963,
		acceleration: -0.103055555555555
	},
	{
		id: 270,
		time: 269,
		velocity: 13.8244444444444,
		power: 2524.72670752662,
		road: 2154.05527777778,
		acceleration: -0.0445370370370384
	},
	{
		id: 271,
		time: 270,
		velocity: 13.7916666666667,
		power: 3067.87930890221,
		road: 2167.87689814815,
		acceleration: -0.00268518518518412
	},
	{
		id: 272,
		time: 271,
		velocity: 13.8447222222222,
		power: 3235.66946354358,
		road: 2181.70212962963,
		acceleration: 0.00990740740740748
	},
	{
		id: 273,
		time: 272,
		velocity: 13.8541666666667,
		power: 3379.39420610815,
		road: 2195.5424537037,
		acceleration: 0.0202777777777783
	},
	{
		id: 274,
		time: 273,
		velocity: 13.8525,
		power: 3108.32736869481,
		road: 2209.39263888889,
		acceleration: -0.000555555555555642
	},
	{
		id: 275,
		time: 274,
		velocity: 13.8430555555556,
		power: 2699.08282023252,
		road: 2223.22703703704,
		acceleration: -0.0310185185185201
	},
	{
		id: 276,
		time: 275,
		velocity: 13.7611111111111,
		power: 2848.99086775858,
		road: 2237.03648148148,
		acceleration: -0.0188888888888883
	},
	{
		id: 277,
		time: 276,
		velocity: 13.7958333333333,
		power: 2603.58644347703,
		road: 2250.81814814815,
		acceleration: -0.0366666666666653
	},
	{
		id: 278,
		time: 277,
		velocity: 13.7330555555556,
		power: 3137.58933940588,
		road: 2264.5837037037,
		acceleration: 0.00444444444444514
	},
	{
		id: 279,
		time: 278,
		velocity: 13.7744444444444,
		power: 3222.44967511051,
		road: 2278.35680555556,
		acceleration: 0.010648148148146
	},
	{
		id: 280,
		time: 279,
		velocity: 13.8277777777778,
		power: 3555.77932933469,
		road: 2292.15282407407,
		acceleration: 0.0351851851851848
	},
	{
		id: 281,
		time: 280,
		velocity: 13.8386111111111,
		power: 5166.93069885157,
		road: 2306.04310185185,
		acceleration: 0.153333333333334
	},
	{
		id: 282,
		time: 281,
		velocity: 14.2344444444444,
		power: 4343.91595751284,
		road: 2320.05333333333,
		acceleration: 0.0865740740740737
	},
	{
		id: 283,
		time: 282,
		velocity: 14.0875,
		power: 5540.5842800906,
		road: 2334.19194444445,
		acceleration: 0.170185185185186
	},
	{
		id: 284,
		time: 283,
		velocity: 14.3491666666667,
		power: 5232.70742979755,
		road: 2348.48597222222,
		acceleration: 0.140648148148147
	},
	{
		id: 285,
		time: 284,
		velocity: 14.6563888888889,
		power: 7711.65370241406,
		road: 2363.00518518519,
		acceleration: 0.309722222222225
	},
	{
		id: 286,
		time: 285,
		velocity: 15.0166666666667,
		power: 5648.06899650163,
		road: 2377.75462962963,
		acceleration: 0.150740740740739
	},
	{
		id: 287,
		time: 286,
		velocity: 14.8013888888889,
		power: 4316.02522339713,
		road: 2392.60564814815,
		acceleration: 0.0524074074074079
	},
	{
		id: 288,
		time: 287,
		velocity: 14.8136111111111,
		power: 4747.22648375229,
		road: 2407.52296296296,
		acceleration: 0.0801851851851847
	},
	{
		id: 289,
		time: 288,
		velocity: 15.2572222222222,
		power: 8023.99402987125,
		road: 2422.63023148148,
		acceleration: 0.299722222222224
	},
	{
		id: 290,
		time: 289,
		velocity: 15.7005555555556,
		power: 9836.98561656335,
		road: 2438.08916666667,
		acceleration: 0.403611111111109
	},
	{
		id: 291,
		time: 290,
		velocity: 16.0244444444444,
		power: 8449.49722975821,
		road: 2453.89537037037,
		acceleration: 0.290925925925926
	},
	{
		id: 292,
		time: 291,
		velocity: 16.13,
		power: 7848.31923931542,
		road: 2469.96592592593,
		acceleration: 0.237777777777778
	},
	{
		id: 293,
		time: 292,
		velocity: 16.4138888888889,
		power: 5497.99899123476,
		road: 2486.19458333333,
		acceleration: 0.0784259259259272
	},
	{
		id: 294,
		time: 293,
		velocity: 16.2597222222222,
		power: 6357.15427788374,
		road: 2502.52708333333,
		acceleration: 0.129259259259257
	},
	{
		id: 295,
		time: 294,
		velocity: 16.5177777777778,
		power: 5269.94708384456,
		road: 2518.95222222222,
		acceleration: 0.0560185185185205
	},
	{
		id: 296,
		time: 295,
		velocity: 16.5819444444444,
		power: 5903.28621327068,
		road: 2535.45199074074,
		acceleration: 0.0932407407407396
	},
	{
		id: 297,
		time: 296,
		velocity: 16.5394444444444,
		power: 3899.67265947478,
		road: 2551.98101851852,
		acceleration: -0.0347222222222214
	},
	{
		id: 298,
		time: 297,
		velocity: 16.4136111111111,
		power: 2090.44164584635,
		road: 2568.41930555556,
		acceleration: -0.146759259259262
	},
	{
		id: 299,
		time: 298,
		velocity: 16.1416666666667,
		power: 1095.90901007502,
		road: 2584.6812037037,
		acceleration: -0.206018518518519
	},
	{
		id: 300,
		time: 299,
		velocity: 15.9213888888889,
		power: 544.835254496609,
		road: 2600.72162037037,
		acceleration: -0.236944444444442
	},
	{
		id: 301,
		time: 300,
		velocity: 15.7027777777778,
		power: -277.761757958933,
		road: 2616.50046296296,
		acceleration: -0.286203703703706
	},
	{
		id: 302,
		time: 301,
		velocity: 15.2830555555556,
		power: 821.986854975355,
		road: 2632.03217592593,
		acceleration: -0.208055555555553
	},
	{
		id: 303,
		time: 302,
		velocity: 15.2972222222222,
		power: 2326.47963264077,
		road: 2647.40865740741,
		acceleration: -0.102407407407407
	},
	{
		id: 304,
		time: 303,
		velocity: 15.3955555555556,
		power: 4475.56571328171,
		road: 2662.75638888889,
		acceleration: 0.0449074074074076
	},
	{
		id: 305,
		time: 304,
		velocity: 15.4177777777778,
		power: 5036.46162841348,
		road: 2678.16689814815,
		acceleration: 0.080648148148148
	},
	{
		id: 306,
		time: 305,
		velocity: 15.5391666666667,
		power: 6133.48424958076,
		road: 2693.69273148148,
		acceleration: 0.15
	},
	{
		id: 307,
		time: 306,
		velocity: 15.8455555555556,
		power: 7925.07217098477,
		road: 2709.42361111111,
		acceleration: 0.260092592592592
	},
	{
		id: 308,
		time: 307,
		velocity: 16.1980555555556,
		power: 8424.73255209007,
		road: 2725.42402777778,
		acceleration: 0.278981481481484
	},
	{
		id: 309,
		time: 308,
		velocity: 16.3761111111111,
		power: 6781.20744190952,
		road: 2741.64467592593,
		acceleration: 0.161481481481481
	},
	{
		id: 310,
		time: 309,
		velocity: 16.33,
		power: 4771.02348879697,
		road: 2757.96023148148,
		acceleration: 0.0283333333333289
	},
	{
		id: 311,
		time: 310,
		velocity: 16.2830555555556,
		power: 4463.42516582748,
		road: 2774.29393518519,
		acceleration: 0.00796296296296362
	},
	{
		id: 312,
		time: 311,
		velocity: 16.4,
		power: 3816.20193105951,
		road: 2790.61509259259,
		acceleration: -0.0330555555555563
	},
	{
		id: 313,
		time: 312,
		velocity: 16.2308333333333,
		power: 4158.42875636685,
		road: 2806.91453703704,
		acceleration: -0.0103703703703673
	},
	{
		id: 314,
		time: 313,
		velocity: 16.2519444444444,
		power: 3202.26934279897,
		road: 2823.17356481482,
		acceleration: -0.0704629629629636
	},
	{
		id: 315,
		time: 314,
		velocity: 16.1886111111111,
		power: 3973.00568623083,
		road: 2839.38768518519,
		acceleration: -0.0193518518518516
	},
	{
		id: 316,
		time: 315,
		velocity: 16.1727777777778,
		power: 1569.51303231781,
		road: 2855.50611111111,
		acceleration: -0.172037037037036
	},
	{
		id: 317,
		time: 316,
		velocity: 15.7358333333333,
		power: 726.501356104364,
		road: 2871.42722222222,
		acceleration: -0.222592592592594
	},
	{
		id: 318,
		time: 317,
		velocity: 15.5208333333333,
		power: 163.238354875487,
		road: 2887.10944444445,
		acceleration: -0.255185185185185
	},
	{
		id: 319,
		time: 318,
		velocity: 15.4072222222222,
		power: 2687.60890608771,
		road: 2902.62314814815,
		acceleration: -0.0818518518518498
	},
	{
		id: 320,
		time: 319,
		velocity: 15.4902777777778,
		power: 2447.3024386967,
		road: 2918.04814814815,
		acceleration: -0.0955555555555563
	},
	{
		id: 321,
		time: 320,
		velocity: 15.2341666666667,
		power: 3602.1061776187,
		road: 2933.41768518519,
		acceleration: -0.0153703703703698
	},
	{
		id: 322,
		time: 321,
		velocity: 15.3611111111111,
		power: 3648.44997314611,
		road: 2948.77365740741,
		acceleration: -0.011759259259259
	},
	{
		id: 323,
		time: 322,
		velocity: 15.455,
		power: 4763.72242350411,
		road: 2964.15537037037,
		acceleration: 0.0632407407407385
	},
	{
		id: 324,
		time: 323,
		velocity: 15.4238888888889,
		power: 6372.40260863167,
		road: 2979.65240740741,
		acceleration: 0.167407407407406
	},
	{
		id: 325,
		time: 324,
		velocity: 15.8633333333333,
		power: 5080.46262596962,
		road: 2995.27083333333,
		acceleration: 0.0753703703703721
	},
	{
		id: 326,
		time: 325,
		velocity: 15.6811111111111,
		power: 4734.47687670045,
		road: 3010.95185185185,
		acceleration: 0.0498148148148143
	},
	{
		id: 327,
		time: 326,
		velocity: 15.5733333333333,
		power: 4708.99170441375,
		road: 3026.68092592593,
		acceleration: 0.0462962962962958
	},
	{
		id: 328,
		time: 327,
		velocity: 16.0022222222222,
		power: 6165.18843371999,
		road: 3042.50268518519,
		acceleration: 0.139074074074076
	},
	{
		id: 329,
		time: 328,
		velocity: 16.0983333333333,
		power: 11123.1854141151,
		road: 3058.61800925926,
		acceleration: 0.448055555555555
	},
	{
		id: 330,
		time: 329,
		velocity: 16.9175,
		power: 10266.1579436277,
		road: 3075.14152777778,
		acceleration: 0.368333333333332
	},
	{
		id: 331,
		time: 330,
		velocity: 17.1072222222222,
		power: 13055.6645630411,
		road: 3092.10662037037,
		acceleration: 0.514814814814816
	},
	{
		id: 332,
		time: 331,
		velocity: 17.6427777777778,
		power: 9106.34290121078,
		road: 3109.45490740741,
		acceleration: 0.251574074074071
	},
	{
		id: 333,
		time: 332,
		velocity: 17.6722222222222,
		power: 6518.97577104025,
		road: 3126.97324074074,
		acceleration: 0.0885185185185229
	},
	{
		id: 334,
		time: 333,
		velocity: 17.3727777777778,
		power: -197.91158346812,
		road: 3144.38078703704,
		acceleration: -0.310092592592596
	},
	{
		id: 335,
		time: 334,
		velocity: 16.7125,
		power: -2508.78182170288,
		road: 3161.41083333333,
		acceleration: -0.444907407407406
	},
	{
		id: 336,
		time: 335,
		velocity: 16.3375,
		power: -3974.95736802488,
		road: 3177.95194444445,
		acceleration: -0.532962962962962
	},
	{
		id: 337,
		time: 336,
		velocity: 15.7738888888889,
		power: -2331.77283547884,
		road: 3194.01421296296,
		acceleration: -0.424722222222222
	},
	{
		id: 338,
		time: 337,
		velocity: 15.4383333333333,
		power: -3456.75782224412,
		road: 3209.61587962963,
		acceleration: -0.496481481481482
	},
	{
		id: 339,
		time: 338,
		velocity: 14.8480555555556,
		power: -1924.0161435267,
		road: 3224.77439814815,
		acceleration: -0.389814814814814
	},
	{
		id: 340,
		time: 339,
		velocity: 14.6044444444444,
		power: -2319.20065902426,
		road: 3239.53064814815,
		acceleration: -0.414722222222222
	},
	{
		id: 341,
		time: 340,
		velocity: 14.1941666666667,
		power: -3671.33543008799,
		road: 3253.82375,
		acceleration: -0.511574074074074
	},
	{
		id: 342,
		time: 341,
		velocity: 13.3133333333333,
		power: -11824.2512912055,
		road: 3267.28615740741,
		acceleration: -1.14981481481481
	},
	{
		id: 343,
		time: 342,
		velocity: 11.155,
		power: -11948.1136706783,
		road: 3279.55708333333,
		acceleration: -1.23314814814815
	},
	{
		id: 344,
		time: 343,
		velocity: 10.4947222222222,
		power: -10949.7469594372,
		road: 3290.59291666667,
		acceleration: -1.23703703703704
	},
	{
		id: 345,
		time: 344,
		velocity: 9.60222222222222,
		power: -8153.72677951247,
		road: 3300.48634259259,
		acceleration: -1.04777777777778
	},
	{
		id: 346,
		time: 345,
		velocity: 8.01166666666667,
		power: -8195.58850772426,
		road: 3309.28092592593,
		acceleration: -1.14990740740741
	},
	{
		id: 347,
		time: 346,
		velocity: 7.045,
		power: -5838.94242674483,
		road: 3317.02365740741,
		acceleration: -0.953796296296296
	},
	{
		id: 348,
		time: 347,
		velocity: 6.74083333333333,
		power: -2838.884955477,
		road: 3323.99810185185,
		acceleration: -0.582777777777778
	},
	{
		id: 349,
		time: 348,
		velocity: 6.26333333333333,
		power: -1732.90073406903,
		road: 3330.46476851852,
		acceleration: -0.432777777777777
	},
	{
		id: 350,
		time: 349,
		velocity: 5.74666666666667,
		power: -870.441751570172,
		road: 3336.56583333333,
		acceleration: -0.298425925925926
	},
	{
		id: 351,
		time: 350,
		velocity: 5.84555555555556,
		power: 79.4496813025082,
		road: 3342.45134259259,
		acceleration: -0.132685185185185
	},
	{
		id: 352,
		time: 351,
		velocity: 5.86527777777778,
		power: 1359.30115427472,
		road: 3348.31898148148,
		acceleration: 0.0969444444444436
	},
	{
		id: 353,
		time: 352,
		velocity: 6.0375,
		power: 1779.5795650193,
		road: 3354.31731481482,
		acceleration: 0.164444444444445
	},
	{
		id: 354,
		time: 353,
		velocity: 6.33888888888889,
		power: 3314.44422574234,
		road: 3360.60046296296,
		acceleration: 0.405185185185185
	},
	{
		id: 355,
		time: 354,
		velocity: 7.08083333333333,
		power: 6202.15571820336,
		road: 3367.48287037037,
		acceleration: 0.793333333333334
	},
	{
		id: 356,
		time: 355,
		velocity: 8.4175,
		power: 7025.57679350878,
		road: 3375.16231481482,
		acceleration: 0.800740740740741
	},
	{
		id: 357,
		time: 356,
		velocity: 8.74111111111111,
		power: 7098.27343001664,
		road: 3383.60018518519,
		acceleration: 0.716111111111111
	},
	{
		id: 358,
		time: 357,
		velocity: 9.22916666666667,
		power: 5745.64906129351,
		road: 3392.64296296296,
		acceleration: 0.493703703703703
	},
	{
		id: 359,
		time: 358,
		velocity: 9.89861111111111,
		power: 4879.20355505405,
		road: 3402.11402777778,
		acceleration: 0.362870370370368
	},
	{
		id: 360,
		time: 359,
		velocity: 9.82972222222222,
		power: 4301.59578631489,
		road: 3411.90634259259,
		acceleration: 0.27962962962963
	},
	{
		id: 361,
		time: 360,
		velocity: 10.0680555555556,
		power: 6344.26367269344,
		road: 3422.07305555556,
		acceleration: 0.469166666666668
	},
	{
		id: 362,
		time: 361,
		velocity: 11.3061111111111,
		power: 9032.74223184591,
		road: 3432.81898148148,
		acceleration: 0.689259259259259
	},
	{
		id: 363,
		time: 362,
		velocity: 11.8975,
		power: 10950.0648477344,
		road: 3444.30828703704,
		acceleration: 0.797499999999999
	},
	{
		id: 364,
		time: 363,
		velocity: 12.4605555555556,
		power: 13268.8257614407,
		road: 3456.65291666667,
		acceleration: 0.913148148148149
	},
	{
		id: 365,
		time: 364,
		velocity: 14.0455555555556,
		power: 17174.4174205938,
		road: 3470.01328703704,
		acceleration: 1.11833333333333
	},
	{
		id: 366,
		time: 365,
		velocity: 15.2525,
		power: 17390.8375516482,
		road: 3484.4412962963,
		acceleration: 1.01694444444444
	},
	{
		id: 367,
		time: 366,
		velocity: 15.5113888888889,
		power: 12946.0433768391,
		road: 3499.69259259259,
		acceleration: 0.629629629629632
	},
	{
		id: 368,
		time: 367,
		velocity: 15.9344444444444,
		power: 9712.02859424545,
		road: 3515.4474537037,
		acceleration: 0.3775
	},
	{
		id: 369,
		time: 368,
		velocity: 16.385,
		power: 9125.23924859697,
		road: 3531.55060185185,
		acceleration: 0.319074074074074
	},
	{
		id: 370,
		time: 369,
		velocity: 16.4686111111111,
		power: 10488.1501550736,
		road: 3548.00648148148,
		acceleration: 0.386388888888888
	},
	{
		id: 371,
		time: 370,
		velocity: 17.0936111111111,
		power: 10437.4512223224,
		road: 3564.8362037037,
		acceleration: 0.361296296296299
	},
	{
		id: 372,
		time: 371,
		velocity: 17.4688888888889,
		power: 14061.4791672334,
		road: 3582.1237037037,
		acceleration: 0.554259259259258
	},
	{
		id: 373,
		time: 372,
		velocity: 18.1313888888889,
		power: 11499.5320791355,
		road: 3599.87430555556,
		acceleration: 0.371944444444445
	},
	{
		id: 374,
		time: 373,
		velocity: 18.2094444444444,
		power: 7710.60829556456,
		road: 3617.87953703704,
		acceleration: 0.137314814814815
	},
	{
		id: 375,
		time: 374,
		velocity: 17.8808333333333,
		power: 7061.21513760749,
		road: 3636.00074074074,
		acceleration: 0.0946296296296296
	},
	{
		id: 376,
		time: 375,
		velocity: 18.4152777777778,
		power: 8010.88668734534,
		road: 3654.2412037037,
		acceleration: 0.143888888888888
	},
	{
		id: 377,
		time: 376,
		velocity: 18.6411111111111,
		power: 11744.4968193338,
		road: 3672.72546296296,
		acceleration: 0.343703703703707
	},
	{
		id: 378,
		time: 377,
		velocity: 18.9119444444444,
		power: 7987.91609886136,
		road: 3691.44203703704,
		acceleration: 0.120925925925924
	},
	{
		id: 379,
		time: 378,
		velocity: 18.7780555555556,
		power: 7889.74275179527,
		road: 3710.27421296296,
		acceleration: 0.110277777777778
	},
	{
		id: 380,
		time: 379,
		velocity: 18.9719444444444,
		power: 6335.91516260084,
		road: 3729.17226851852,
		acceleration: 0.0214814814814837
	},
	{
		id: 381,
		time: 380,
		velocity: 18.9763888888889,
		power: 7331.63352505033,
		road: 3748.11828703704,
		acceleration: 0.0744444444444419
	},
	{
		id: 382,
		time: 381,
		velocity: 19.0013888888889,
		power: 8710.17425442345,
		road: 3767.17416666667,
		acceleration: 0.145277777777778
	},
	{
		id: 383,
		time: 382,
		velocity: 19.4077777777778,
		power: 11769.3647752501,
		road: 3786.45296296296,
		acceleration: 0.300555555555558
	},
	{
		id: 384,
		time: 383,
		velocity: 19.8780555555556,
		power: 13614.289975214,
		road: 3806.07208333333,
		acceleration: 0.380092592592593
	},
	{
		id: 385,
		time: 384,
		velocity: 20.1416666666667,
		power: 11560.2295371138,
		road: 3826.00819444444,
		acceleration: 0.253888888888891
	},
	{
		id: 386,
		time: 385,
		velocity: 20.1694444444444,
		power: 11672.694195581,
		road: 3846.19449074074,
		acceleration: 0.246481481481474
	},
	{
		id: 387,
		time: 386,
		velocity: 20.6175,
		power: 9643.61755481662,
		road: 3866.57032407407,
		acceleration: 0.132592592592594
	},
	{
		id: 388,
		time: 387,
		velocity: 20.5394444444444,
		power: 10100.6156323445,
		road: 3887.08703703704,
		acceleration: 0.149166666666666
	},
	{
		id: 389,
		time: 388,
		velocity: 20.6169444444444,
		power: 7080.13989785451,
		road: 3907.67467592593,
		acceleration: -0.00731481481481211
	},
	{
		id: 390,
		time: 389,
		velocity: 20.5955555555556,
		power: 7992.26868934097,
		road: 3928.27777777778,
		acceleration: 0.0382407407407399
	},
	{
		id: 391,
		time: 390,
		velocity: 20.6541666666667,
		power: 7620.74399470033,
		road: 3948.90912037037,
		acceleration: 0.0182407407407403
	},
	{
		id: 392,
		time: 391,
		velocity: 20.6716666666667,
		power: 6744.24944639438,
		road: 3969.53662037037,
		acceleration: -0.0259259259259252
	},
	{
		id: 393,
		time: 392,
		velocity: 20.5177777777778,
		power: 8958.46286425738,
		road: 3990.19351851852,
		acceleration: 0.0847222222222221
	},
	{
		id: 394,
		time: 393,
		velocity: 20.9083333333333,
		power: 10307.7053183677,
		road: 4010.96638888889,
		acceleration: 0.147222222222219
	},
	{
		id: 395,
		time: 394,
		velocity: 21.1133333333333,
		power: 13461.3470311853,
		road: 4031.95949074074,
		acceleration: 0.293240740740742
	},
	{
		id: 396,
		time: 395,
		velocity: 21.3975,
		power: 13364.4286482099,
		road: 4053.23560185185,
		acceleration: 0.272777777777776
	},
	{
		id: 397,
		time: 396,
		velocity: 21.7266666666667,
		power: 10963.5868086253,
		road: 4074.72050925926,
		acceleration: 0.144814814814819
	},
	{
		id: 398,
		time: 397,
		velocity: 21.5477777777778,
		power: 13409.9195373865,
		road: 4096.40416666667,
		acceleration: 0.252685185185182
	},
	{
		id: 399,
		time: 398,
		velocity: 22.1555555555556,
		power: 12358.9125769639,
		road: 4118.30949074074,
		acceleration: 0.190648148148149
	},
	{
		id: 400,
		time: 399,
		velocity: 22.2986111111111,
		power: 16163.7172805718,
		road: 4140.48782407408,
		acceleration: 0.355370370370366
	},
	{
		id: 401,
		time: 400,
		velocity: 22.6138888888889,
		power: 8407.60963358841,
		road: 4162.83564814815,
		acceleration: -0.016388888888887
	},
	{
		id: 402,
		time: 401,
		velocity: 22.1063888888889,
		power: 12609.6266156581,
		road: 4185.26300925926,
		acceleration: 0.175462962962968
	},
	{
		id: 403,
		time: 402,
		velocity: 22.825,
		power: 8407.54558154509,
		road: 4207.76657407408,
		acceleration: -0.0230555555555583
	},
	{
		id: 404,
		time: 403,
		velocity: 22.5447222222222,
		power: 10684.664614883,
		road: 4230.29912037037,
		acceleration: 0.081018518518519
	},
	{
		id: 405,
		time: 404,
		velocity: 22.3494444444444,
		power: 1568.92102816856,
		road: 4252.70370370371,
		acceleration: -0.336944444444445
	},
	{
		id: 406,
		time: 405,
		velocity: 21.8141666666667,
		power: 428.175981756814,
		road: 4274.74944444445,
		acceleration: -0.380740740740741
	},
	{
		id: 407,
		time: 406,
		velocity: 21.4025,
		power: -2003.58840496258,
		road: 4296.3612962963,
		acceleration: -0.487037037037034
	},
	{
		id: 408,
		time: 407,
		velocity: 20.8883333333333,
		power: 1426.41815777917,
		road: 4317.57430555556,
		acceleration: -0.310648148148154
	},
	{
		id: 409,
		time: 408,
		velocity: 20.8822222222222,
		power: 705.837446501083,
		road: 4338.46305555556,
		acceleration: -0.337870370370368
	},
	{
		id: 410,
		time: 409,
		velocity: 20.3888888888889,
		power: 4147.9731642546,
		road: 4359.10402777778,
		acceleration: -0.157685185185187
	},
	{
		id: 411,
		time: 410,
		velocity: 20.4152777777778,
		power: 3636.2666161175,
		road: 4379.57708333334,
		acceleration: -0.178148148148146
	},
	{
		id: 412,
		time: 411,
		velocity: 20.3477777777778,
		power: 6702.74981672753,
		road: 4399.95208333334,
		acceleration: -0.0179629629629652
	},
	{
		id: 413,
		time: 412,
		velocity: 20.335,
		power: 5816.34005402975,
		road: 4420.28717592593,
		acceleration: -0.061851851851852
	},
	{
		id: 414,
		time: 413,
		velocity: 20.2297222222222,
		power: 6319.43175642906,
		road: 4440.57425925926,
		acceleration: -0.034166666666664
	},
	{
		id: 415,
		time: 414,
		velocity: 20.2452777777778,
		power: 8653.07194675842,
		road: 4460.88662037037,
		acceleration: 0.0847222222222221
	},
	{
		id: 416,
		time: 415,
		velocity: 20.5891666666667,
		power: 7679.31796081427,
		road: 4481.25745370371,
		acceleration: 0.0322222222222202
	},
	{
		id: 417,
		time: 416,
		velocity: 20.3263888888889,
		power: 8109.33673028453,
		road: 4501.67060185185,
		acceleration: 0.0524074074074079
	},
	{
		id: 418,
		time: 417,
		velocity: 20.4025,
		power: 7056.82695438185,
		road: 4522.10875,
		acceleration: -0.00240740740740364
	},
	{
		id: 419,
		time: 418,
		velocity: 20.5819444444444,
		power: 8363.52768893868,
		road: 4542.57717592593,
		acceleration: 0.0629629629629598
	},
	{
		id: 420,
		time: 419,
		velocity: 20.5152777777778,
		power: 9634.17978106175,
		road: 4563.13875,
		acceleration: 0.123333333333335
	},
	{
		id: 421,
		time: 420,
		velocity: 20.7725,
		power: 8272.78715490004,
		road: 4583.78722222222,
		acceleration: 0.050462962962964
	},
	{
		id: 422,
		time: 421,
		velocity: 20.7333333333333,
		power: 8530.56090145989,
		road: 4604.49143518519,
		acceleration: 0.0610185185185159
	},
	{
		id: 423,
		time: 422,
		velocity: 20.6983333333333,
		power: 10631.5920394739,
		road: 4625.30685185185,
		acceleration: 0.16138888888889
	},
	{
		id: 424,
		time: 423,
		velocity: 21.2566666666667,
		power: 10930.4547489776,
		road: 4646.28703703704,
		acceleration: 0.168148148148148
	},
	{
		id: 425,
		time: 424,
		velocity: 21.2377777777778,
		power: 17827.2536560189,
		road: 4667.59597222222,
		acceleration: 0.48935185185185
	},
	{
		id: 426,
		time: 425,
		velocity: 22.1663888888889,
		power: 16175.1921248739,
		road: 4689.34055555556,
		acceleration: 0.381944444444446
	},
	{
		id: 427,
		time: 426,
		velocity: 22.4025,
		power: 21490.6729484029,
		road: 4711.57689814815,
		acceleration: 0.601574074074072
	},
	{
		id: 428,
		time: 427,
		velocity: 23.0425,
		power: 17190.9837047122,
		road: 4734.29921296296,
		acceleration: 0.37037037037037
	},
	{
		id: 429,
		time: 428,
		velocity: 23.2775,
		power: 15294.6381310251,
		road: 4757.33944444445,
		acceleration: 0.265462962962967
	},
	{
		id: 430,
		time: 429,
		velocity: 23.1988888888889,
		power: 9018.25319879035,
		road: 4780.50060185185,
		acceleration: -0.0236111111111157
	},
	{
		id: 431,
		time: 430,
		velocity: 22.9716666666667,
		power: 5020.19740532398,
		road: 4803.55023148148,
		acceleration: -0.199444444444445
	},
	{
		id: 432,
		time: 431,
		velocity: 22.6791666666667,
		power: 3419.22489026416,
		road: 4826.36800925926,
		acceleration: -0.264259259259259
	},
	{
		id: 433,
		time: 432,
		velocity: 22.4061111111111,
		power: 3838.7074730696,
		road: 4848.93527777778,
		acceleration: -0.236759259259259
	},
	{
		id: 434,
		time: 433,
		velocity: 22.2613888888889,
		power: 3958.66835540462,
		road: 4871.27240740741,
		acceleration: -0.223518518518517
	},
	{
		id: 435,
		time: 434,
		velocity: 22.0086111111111,
		power: 2384.9030111303,
		road: 4893.35314814815,
		acceleration: -0.289259259259264
	},
	{
		id: 436,
		time: 435,
		velocity: 21.5383333333333,
		power: 3094.92660103367,
		road: 4915.16560185185,
		acceleration: -0.247314814814814
	},
	{
		id: 437,
		time: 436,
		velocity: 21.5194444444444,
		power: 2165.61938124484,
		road: 4936.71240740741,
		acceleration: -0.283981481481479
	},
	{
		id: 438,
		time: 437,
		velocity: 21.1566666666667,
		power: 5484.88047924088,
		road: 4958.05921296296,
		acceleration: -0.116018518518516
	},
	{
		id: 439,
		time: 438,
		velocity: 21.1902777777778,
		power: 4635.08020986471,
		road: 4979.27157407408,
		acceleration: -0.152870370370369
	},
	{
		id: 440,
		time: 439,
		velocity: 21.0608333333333,
		power: 7644.89738154716,
		road: 5000.40666666667,
		acceleration: -0.00166666666666515
	},
	{
		id: 441,
		time: 440,
		velocity: 21.1516666666667,
		power: 3391.30436144095,
		road: 5021.43680555556,
		acceleration: -0.208240740740745
	},
	{
		id: 442,
		time: 441,
		velocity: 20.5655555555556,
		power: 3019.50077422262,
		road: 5042.25277777778,
		acceleration: -0.220092592592593
	},
	{
		id: 443,
		time: 442,
		velocity: 20.4005555555556,
		power: -3209.11586995066,
		road: 5062.69541666667,
		acceleration: -0.526574074074073
	},
	{
		id: 444,
		time: 443,
		velocity: 19.5719444444444,
		power: -484.089864600497,
		road: 5082.68587962963,
		acceleration: -0.37777777777778
	},
	{
		id: 445,
		time: 444,
		velocity: 19.4322222222222,
		power: -2448.6375411754,
		road: 5102.25060185185,
		acceleration: -0.473703703703706
	},
	{
		id: 446,
		time: 445,
		velocity: 18.9794444444444,
		power: 1987.38842785171,
		road: 5121.46476851852,
		acceleration: -0.227407407407405
	},
	{
		id: 447,
		time: 446,
		velocity: 18.8897222222222,
		power: 283.357846793323,
		road: 5140.40824074074,
		acceleration: -0.313981481481484
	},
	{
		id: 448,
		time: 447,
		velocity: 18.4902777777778,
		power: 4297.17766950903,
		road: 5159.15175925926,
		acceleration: -0.085925925925924
	},
	{
		id: 449,
		time: 448,
		velocity: 18.7216666666667,
		power: 1229.31117947495,
		road: 5177.72597222222,
		acceleration: -0.252685185185182
	},
	{
		id: 450,
		time: 449,
		velocity: 18.1316666666667,
		power: 764.054430023442,
		road: 5196.03745370371,
		acceleration: -0.27277777777778
	},
	{
		id: 451,
		time: 450,
		velocity: 17.6719444444444,
		power: -861.073208627902,
		road: 5214.03259259259,
		acceleration: -0.359907407407405
	},
	{
		id: 452,
		time: 451,
		velocity: 17.6419444444444,
		power: 164.744722188527,
		road: 5231.70097222222,
		acceleration: -0.293611111111108
	},
	{
		id: 453,
		time: 452,
		velocity: 17.2508333333333,
		power: 2162.47164658648,
		road: 5249.13796296296,
		acceleration: -0.169166666666669
	},
	{
		id: 454,
		time: 453,
		velocity: 17.1644444444444,
		power: 715.06761124769,
		road: 5266.36472222222,
		acceleration: -0.251296296296299
	},
	{
		id: 455,
		time: 454,
		velocity: 16.8880555555556,
		power: 2466.24137333442,
		road: 5283.39606481482,
		acceleration: -0.139537037037034
	},
	{
		id: 456,
		time: 455,
		velocity: 16.8322222222222,
		power: 1678.69790579729,
		road: 5300.26574074074,
		acceleration: -0.1837962962963
	},
	{
		id: 457,
		time: 456,
		velocity: 16.6130555555556,
		power: 3380.47227205105,
		road: 5317.00638888889,
		acceleration: -0.0742592592592572
	},
	{
		id: 458,
		time: 457,
		velocity: 16.6652777777778,
		power: 2989.52570778573,
		road: 5333.66185185185,
		acceleration: -0.0961111111111137
	},
	{
		id: 459,
		time: 458,
		velocity: 16.5438888888889,
		power: 5164.5694453138,
		road: 5350.29,
		acceleration: 0.0414814814814832
	},
	{
		id: 460,
		time: 459,
		velocity: 16.7375,
		power: 5848.29785424681,
		road: 5366.97986111111,
		acceleration: 0.0819444444444457
	},
	{
		id: 461,
		time: 460,
		velocity: 16.9111111111111,
		power: 9151.47070927261,
		road: 5383.85032407407,
		acceleration: 0.279259259259256
	},
	{
		id: 462,
		time: 461,
		velocity: 17.3816666666667,
		power: 9571.53349017743,
		road: 5401.00537037037,
		acceleration: 0.289907407407412
	},
	{
		id: 463,
		time: 462,
		velocity: 17.6072222222222,
		power: 9456.31934302389,
		road: 5418.43944444445,
		acceleration: 0.26814814814815
	},
	{
		id: 464,
		time: 463,
		velocity: 17.7155555555556,
		power: 8326.01813911029,
		road: 5436.10226851852,
		acceleration: 0.189351851851846
	},
	{
		id: 465,
		time: 464,
		velocity: 17.9497222222222,
		power: 8401.41788080155,
		road: 5453.95217592593,
		acceleration: 0.184814814814821
	},
	{
		id: 466,
		time: 465,
		velocity: 18.1616666666667,
		power: 9645.66579241189,
		road: 5472.01773148148,
		acceleration: 0.246481481481482
	},
	{
		id: 467,
		time: 466,
		velocity: 18.455,
		power: 8570.72460184699,
		road: 5490.29365740741,
		acceleration: 0.174259259259259
	},
	{
		id: 468,
		time: 467,
		velocity: 18.4725,
		power: 5925.27577634201,
		road: 5508.66634259259,
		acceleration: 0.0192592592592575
	},
	{
		id: 469,
		time: 468,
		velocity: 18.2194444444444,
		power: 2372.66632945428,
		road: 5526.95842592593,
		acceleration: -0.180462962962963
	},
	{
		id: 470,
		time: 469,
		velocity: 17.9136111111111,
		power: -1178.26856092568,
		road: 5544.97097222222,
		acceleration: -0.378611111111109
	},
	{
		id: 471,
		time: 470,
		velocity: 17.3366666666667,
		power: -1704.53515804907,
		road: 5562.5924537037,
		acceleration: -0.403518518518524
	},
	{
		id: 472,
		time: 471,
		velocity: 17.0088888888889,
		power: -49.2974402683586,
		road: 5579.86291666667,
		acceleration: -0.298518518518517
	},
	{
		id: 473,
		time: 472,
		velocity: 17.0180555555556,
		power: -909.882290131466,
		road: 5596.81138888889,
		acceleration: -0.345462962962962
	},
	{
		id: 474,
		time: 473,
		velocity: 16.3002777777778,
		power: 1783.59401087317,
		road: 5613.50078703704,
		acceleration: -0.172685185185188
	},
	{
		id: 475,
		time: 474,
		velocity: 16.4908333333333,
		power: -237.890667949683,
		road: 5629.95625,
		acceleration: -0.295185185185183
	},
	{
		id: 476,
		time: 475,
		velocity: 16.1325,
		power: 3289.33738654485,
		road: 5646.23143518519,
		acceleration: -0.0653703703703741
	},
	{
		id: 477,
		time: 476,
		velocity: 16.1041666666667,
		power: 582.57804485309,
		road: 5662.35583333333,
		acceleration: -0.236203703703701
	},
	{
		id: 478,
		time: 477,
		velocity: 15.7822222222222,
		power: 2075.38016762175,
		road: 5678.29490740741,
		acceleration: -0.134444444444442
	},
	{
		id: 479,
		time: 478,
		velocity: 15.7291666666667,
		power: 2664.31180073583,
		road: 5694.12050925926,
		acceleration: -0.0925000000000011
	},
	{
		id: 480,
		time: 479,
		velocity: 15.8266666666667,
		power: 6072.81667885806,
		road: 5709.96583333333,
		acceleration: 0.131944444444443
	},
	{
		id: 481,
		time: 480,
		velocity: 16.1780555555556,
		power: 6203.31275682981,
		road: 5725.94449074074,
		acceleration: 0.134722222222225
	},
	{
		id: 482,
		time: 481,
		velocity: 16.1333333333333,
		power: 5897.39795470131,
		road: 5742.04527777778,
		acceleration: 0.109537037037036
	},
	{
		id: 483,
		time: 482,
		velocity: 16.1552777777778,
		power: 3244.44260392305,
		road: 5758.16907407407,
		acceleration: -0.0635185185185208
	},
	{
		id: 484,
		time: 483,
		velocity: 15.9875,
		power: 3351.08541762282,
		road: 5774.23375,
		acceleration: -0.054722222222221
	},
	{
		id: 485,
		time: 484,
		velocity: 15.9691666666667,
		power: 2779.39251278631,
		road: 5790.22615740741,
		acceleration: -0.0898148148148152
	},
	{
		id: 486,
		time: 485,
		velocity: 15.8858333333333,
		power: 3176.57319556815,
		road: 5806.14291666667,
		acceleration: -0.061481481481481
	},
	{
		id: 487,
		time: 486,
		velocity: 15.8030555555556,
		power: 2472.90952454037,
		road: 5821.97625,
		acceleration: -0.10537037037037
	},
	{
		id: 488,
		time: 487,
		velocity: 15.6530555555556,
		power: 3653.73442453782,
		road: 5837.74430555556,
		acceleration: -0.0251851851851868
	},
	{
		id: 489,
		time: 488,
		velocity: 15.8102777777778,
		power: 2637.48711478211,
		road: 5853.45430555556,
		acceleration: -0.0909259259259247
	},
	{
		id: 490,
		time: 489,
		velocity: 15.5302777777778,
		power: 1663.31258567539,
		road: 5869.0424537037,
		acceleration: -0.152777777777777
	},
	{
		id: 491,
		time: 490,
		velocity: 15.1947222222222,
		power: 826.266055043192,
		road: 5884.45162037037,
		acceleration: -0.205185185185186
	},
	{
		id: 492,
		time: 491,
		velocity: 15.1947222222222,
		power: 318.43649044006,
		road: 5899.64041666667,
		acceleration: -0.235555555555553
	},
	{
		id: 493,
		time: 492,
		velocity: 14.8236111111111,
		power: 885.926474860619,
		road: 5914.61546296296,
		acceleration: -0.191944444444445
	},
	{
		id: 494,
		time: 493,
		velocity: 14.6188888888889,
		power: -142.40783198528,
		road: 5929.36444444444,
		acceleration: -0.260185185185184
	},
	{
		id: 495,
		time: 494,
		velocity: 14.4141666666667,
		power: 279.129474221062,
		road: 5943.87037037037,
		acceleration: -0.225925925925928
	},
	{
		id: 496,
		time: 495,
		velocity: 14.1458333333333,
		power: 172.546942150451,
		road: 5958.14847222222,
		acceleration: -0.229722222222222
	},
	{
		id: 497,
		time: 496,
		velocity: 13.9297222222222,
		power: 867.201981128644,
		road: 5972.22439814815,
		acceleration: -0.174629629629628
	},
	{
		id: 498,
		time: 497,
		velocity: 13.8902777777778,
		power: 2767.91370615598,
		road: 5986.19796296296,
		acceleration: -0.0300925925925952
	},
	{
		id: 499,
		time: 498,
		velocity: 14.0555555555556,
		power: 4021.65855078585,
		road: 6000.18810185185,
		acceleration: 0.0632407407407403
	},
	{
		id: 500,
		time: 499,
		velocity: 14.1194444444444,
		power: 5011.62233646624,
		road: 6014.27643518518,
		acceleration: 0.13314814814815
	},
	{
		id: 501,
		time: 500,
		velocity: 14.2897222222222,
		power: 5277.13584903702,
		road: 6028.50472222222,
		acceleration: 0.146759259259261
	},
	{
		id: 502,
		time: 501,
		velocity: 14.4958333333333,
		power: 4665.48419126223,
		road: 6042.85481481481,
		acceleration: 0.0968518518518522
	},
	{
		id: 503,
		time: 502,
		velocity: 14.41,
		power: 2480.37375364545,
		road: 6057.22180555556,
		acceleration: -0.0630555555555556
	},
	{
		id: 504,
		time: 503,
		velocity: 14.1005555555556,
		power: 1365.23611984954,
		road: 6071.4862962963,
		acceleration: -0.141944444444446
	},
	{
		id: 505,
		time: 504,
		velocity: 14.07,
		power: -384.142537926797,
		road: 6085.54606481482,
		acceleration: -0.2675
	},
	{
		id: 506,
		time: 505,
		velocity: 13.6075,
		power: 899.563002050408,
		road: 6099.38837962963,
		acceleration: -0.167407407407408
	},
	{
		id: 507,
		time: 506,
		velocity: 13.5983333333333,
		power: 1148.00668794142,
		road: 6113.07439814815,
		acceleration: -0.145185185185184
	},
	{
		id: 508,
		time: 507,
		velocity: 13.6344444444444,
		power: 4102.49423530005,
		road: 6126.72888888889,
		acceleration: 0.0821296296296303
	},
	{
		id: 509,
		time: 508,
		velocity: 13.8538888888889,
		power: 4796.32212171937,
		road: 6140.48986111111,
		acceleration: 0.130833333333335
	},
	{
		id: 510,
		time: 509,
		velocity: 13.9908333333333,
		power: 3802.46854481184,
		road: 6154.34217592593,
		acceleration: 0.0518518518518505
	},
	{
		id: 511,
		time: 510,
		velocity: 13.79,
		power: 2868.61355022615,
		road: 6168.21078703704,
		acceleration: -0.0192592592592611
	},
	{
		id: 512,
		time: 511,
		velocity: 13.7961111111111,
		power: -10.7550188070753,
		road: 6181.95240740741,
		acceleration: -0.234722222222221
	},
	{
		id: 513,
		time: 512,
		velocity: 13.2866666666667,
		power: -31.8040820107344,
		road: 6195.46027777778,
		acceleration: -0.232777777777777
	},
	{
		id: 514,
		time: 513,
		velocity: 13.0916666666667,
		power: -1265.12369215941,
		road: 6208.68861111111,
		acceleration: -0.326296296296297
	},
	{
		id: 515,
		time: 514,
		velocity: 12.8172222222222,
		power: -147.372655734012,
		road: 6221.63685185185,
		acceleration: -0.23388888888889
	},
	{
		id: 516,
		time: 515,
		velocity: 12.585,
		power: -741.064145122745,
		road: 6234.32842592593,
		acceleration: -0.279444444444445
	},
	{
		id: 517,
		time: 516,
		velocity: 12.2533333333333,
		power: -519.091849305931,
		road: 6246.75115740741,
		acceleration: -0.258240740740739
	},
	{
		id: 518,
		time: 517,
		velocity: 12.0425,
		power: -433.135843941998,
		road: 6258.92064814815,
		acceleration: -0.248240740740743
	},
	{
		id: 519,
		time: 518,
		velocity: 11.8402777777778,
		power: -2018.35919403592,
		road: 6270.77342592593,
		acceleration: -0.385185185185184
	},
	{
		id: 520,
		time: 519,
		velocity: 11.0977777777778,
		power: -4833.04890718726,
		road: 6282.11009259259,
		acceleration: -0.647037037037038
	},
	{
		id: 521,
		time: 520,
		velocity: 10.1013888888889,
		power: -10054.9775234819,
		road: 6292.52222222222,
		acceleration: -1.20203703703704
	},
	{
		id: 522,
		time: 521,
		velocity: 8.23416666666667,
		power: -13944.0884094802,
		road: 6301.42458333334,
		acceleration: -1.8175
	},
	{
		id: 523,
		time: 522,
		velocity: 5.64527777777778,
		power: -13409.2501964745,
		road: 6308.31819444445,
		acceleration: -2.2
	},
	{
		id: 524,
		time: 523,
		velocity: 3.50138888888889,
		power: -9324.39176465614,
		road: 6312.9913425926,
		acceleration: -2.24092592592593
	},
	{
		id: 525,
		time: 524,
		velocity: 1.51138888888889,
		power: -4336.60462841658,
		road: 6315.60314814815,
		acceleration: -1.88175925925926
	},
	{
		id: 526,
		time: 525,
		velocity: 0,
		power: -1070.77458442188,
		road: 6316.69050925926,
		acceleration: -1.16712962962963
	},
	{
		id: 527,
		time: 526,
		velocity: 0,
		power: -55.7988565875177,
		road: 6317.04819444445,
		acceleration: -0.292222222222222
	},
	{
		id: 528,
		time: 527,
		velocity: 0.634722222222222,
		power: 618.400052200523,
		road: 6317.69796296297,
		acceleration: 0.876388888888889
	},
	{
		id: 529,
		time: 528,
		velocity: 2.62916666666667,
		power: 2896.56449124481,
		road: 6319.54699074074,
		acceleration: 1.52212962962963
	},
	{
		id: 530,
		time: 529,
		velocity: 4.56638888888889,
		power: 6159.42701179335,
		road: 6323.02310185185,
		acceleration: 1.73203703703704
	},
	{
		id: 531,
		time: 530,
		velocity: 5.83083333333333,
		power: 6331.12726713703,
		road: 6327.96833333334,
		acceleration: 1.2062037037037
	},
	{
		id: 532,
		time: 531,
		velocity: 6.24777777777778,
		power: 5358.7175565795,
		road: 6333.91680555556,
		acceleration: 0.800277777777779
	},
	{
		id: 533,
		time: 532,
		velocity: 6.96722222222222,
		power: 4697.79620748346,
		road: 6340.56101851852,
		acceleration: 0.591203703703703
	},
	{
		id: 534,
		time: 533,
		velocity: 7.60444444444444,
		power: 7640.20730580178,
		road: 6347.96402777778,
		acceleration: 0.92638888888889
	},
	{
		id: 535,
		time: 534,
		velocity: 9.02694444444444,
		power: 11022.1405454503,
		road: 6356.4300462963,
		acceleration: 1.19962962962963
	},
	{
		id: 536,
		time: 535,
		velocity: 10.5661111111111,
		power: 12513.8295943863,
		road: 6366.08601851852,
		acceleration: 1.18027777777778
	},
	{
		id: 537,
		time: 536,
		velocity: 11.1452777777778,
		power: 12732.5521717958,
		road: 6376.85578703704,
		acceleration: 1.04731481481481
	},
	{
		id: 538,
		time: 537,
		velocity: 12.1688888888889,
		power: 8585.86793207896,
		road: 6388.43638888889,
		acceleration: 0.574351851851851
	},
	{
		id: 539,
		time: 538,
		velocity: 12.2891666666667,
		power: 6294.76451302557,
		road: 6400.4737037037,
		acceleration: 0.339074074074075
	},
	{
		id: 540,
		time: 539,
		velocity: 12.1625,
		power: 2254.32103151586,
		road: 6412.67175925926,
		acceleration: -0.0175925925925924
	},
	{
		id: 541,
		time: 540,
		velocity: 12.1161111111111,
		power: 3120.04238872845,
		road: 6424.88907407408,
		acceleration: 0.0561111111111128
	},
	{
		id: 542,
		time: 541,
		velocity: 12.4575,
		power: 4393.78210734457,
		road: 6437.21472222222,
		acceleration: 0.160555555555554
	},
	{
		id: 543,
		time: 542,
		velocity: 12.6441666666667,
		power: 4906.17681033218,
		road: 6449.71847222222,
		acceleration: 0.195648148148148
	},
	{
		id: 544,
		time: 543,
		velocity: 12.7030555555556,
		power: 3881.89398862755,
		road: 6462.37194444445,
		acceleration: 0.103796296296295
	},
	{
		id: 545,
		time: 544,
		velocity: 12.7688888888889,
		power: 2904.11011775421,
		road: 6475.08768518519,
		acceleration: 0.0207407407407416
	},
	{
		id: 546,
		time: 545,
		velocity: 12.7063888888889,
		power: 5526.5928591881,
		road: 6487.92907407408,
		acceleration: 0.230555555555554
	},
	{
		id: 547,
		time: 546,
		velocity: 13.3947222222222,
		power: 6051.48245468316,
		road: 6501.01597222222,
		acceleration: 0.260462962962965
	},
	{
		id: 548,
		time: 547,
		velocity: 13.5502777777778,
		power: 7117.92340771174,
		road: 6514.3975,
		acceleration: 0.328796296296293
	},
	{
		id: 549,
		time: 548,
		velocity: 13.6927777777778,
		power: 4291.57488108828,
		road: 6527.99291666667,
		acceleration: 0.0989814814814842
	},
	{
		id: 550,
		time: 549,
		velocity: 13.6916666666667,
		power: 5012.26280441902,
		road: 6541.71236111111,
		acceleration: 0.149074074074074
	},
	{
		id: 551,
		time: 550,
		velocity: 13.9975,
		power: 5022.69164069642,
		road: 6555.57810185185,
		acceleration: 0.143518518518519
	},
	{
		id: 552,
		time: 551,
		velocity: 14.1233333333333,
		power: 6336.82310296891,
		road: 6569.63226851852,
		acceleration: 0.233333333333334
	},
	{
		id: 553,
		time: 552,
		velocity: 14.3916666666667,
		power: 6673.83358028801,
		road: 6583.92620370371,
		acceleration: 0.246203703703705
	},
	{
		id: 554,
		time: 553,
		velocity: 14.7361111111111,
		power: 7086.75931205692,
		road: 6598.47481481482,
		acceleration: 0.263148148148147
	},
	{
		id: 555,
		time: 554,
		velocity: 14.9127777777778,
		power: 7418.81796636869,
		road: 6613.29143518519,
		acceleration: 0.27287037037037
	},
	{
		id: 556,
		time: 555,
		velocity: 15.2102777777778,
		power: 6540.40136193003,
		road: 6628.34425925926,
		acceleration: 0.199537037037036
	},
	{
		id: 557,
		time: 556,
		velocity: 15.3347222222222,
		power: 6449.62691938323,
		road: 6643.58898148148,
		acceleration: 0.18425925925926
	},
	{
		id: 558,
		time: 557,
		velocity: 15.4655555555556,
		power: 6717.7670394268,
		road: 6659.02268518519,
		acceleration: 0.193703703703704
	},
	{
		id: 559,
		time: 558,
		velocity: 15.7913888888889,
		power: 5405.22582021115,
		road: 6674.6025925926,
		acceleration: 0.098703703703702
	},
	{
		id: 560,
		time: 559,
		velocity: 15.6308333333333,
		power: 5519.63669378676,
		road: 6690.28296296297,
		acceleration: 0.102222222222222
	},
	{
		id: 561,
		time: 560,
		velocity: 15.7722222222222,
		power: 4935.33487483595,
		road: 6706.04449074074,
		acceleration: 0.0600925925925946
	},
	{
		id: 562,
		time: 561,
		velocity: 15.9716666666667,
		power: 9919.65113684876,
		road: 6722.025,
		acceleration: 0.377870370370365
	},
	{
		id: 563,
		time: 562,
		velocity: 16.7644444444444,
		power: 8285.18421345057,
		road: 6738.32171296297,
		acceleration: 0.254537037037039
	},
	{
		id: 564,
		time: 563,
		velocity: 16.5358333333333,
		power: 6919.40131416852,
		road: 6754.82444444445,
		acceleration: 0.157499999999999
	},
	{
		id: 565,
		time: 564,
		velocity: 16.4441666666667,
		power: 3367.10639471468,
		road: 6771.37143518519,
		acceleration: -0.0689814814814795
	},
	{
		id: 566,
		time: 565,
		velocity: 16.5575,
		power: 4900.97929190225,
		road: 6787.89828703704,
		acceleration: 0.0287037037037017
	},
	{
		id: 567,
		time: 566,
		velocity: 16.6219444444444,
		power: 2965.72097688109,
		road: 6804.39310185186,
		acceleration: -0.0927777777777727
	},
	{
		id: 568,
		time: 567,
		velocity: 16.1658333333333,
		power: 5045.7955529359,
		road: 6820.86157407408,
		acceleration: 0.0400925925925932
	},
	{
		id: 569,
		time: 568,
		velocity: 16.6777777777778,
		power: 2421.18375322254,
		road: 6837.28740740741,
		acceleration: -0.125370370370369
	},
	{
		id: 570,
		time: 569,
		velocity: 16.2458333333333,
		power: 5333.46117412595,
		road: 6853.68120370371,
		acceleration: 0.0612962962962911
	},
	{
		id: 571,
		time: 570,
		velocity: 16.3497222222222,
		power: 3093.26482812922,
		road: 6870.065,
		acceleration: -0.0812962962962942
	},
	{
		id: 572,
		time: 571,
		velocity: 16.4338888888889,
		power: 5131.95898271776,
		road: 6886.43287037037,
		acceleration: 0.0494444444444433
	},
	{
		id: 573,
		time: 572,
		velocity: 16.3941666666667,
		power: 4884.98016935049,
		road: 6902.84152777778,
		acceleration: 0.0321296296296296
	},
	{
		id: 574,
		time: 573,
		velocity: 16.4461111111111,
		power: 3816.11108971429,
		road: 6919.24828703704,
		acceleration: -0.0359259259259268
	},
	{
		id: 575,
		time: 574,
		velocity: 16.3261111111111,
		power: 3005.67538536204,
		road: 6935.59421296297,
		acceleration: -0.0857407407407393
	},
	{
		id: 576,
		time: 575,
		velocity: 16.1369444444444,
		power: 5480.83850122809,
		road: 6951.93370370371,
		acceleration: 0.0728703703703708
	},
	{
		id: 577,
		time: 576,
		velocity: 16.6647222222222,
		power: 8499.2762611774,
		road: 6968.43837962963,
		acceleration: 0.2575
	},
	{
		id: 578,
		time: 577,
		velocity: 17.0986111111111,
		power: 11557.5958254002,
		road: 6985.28666666667,
		acceleration: 0.429722222222221
	},
	{
		id: 579,
		time: 578,
		velocity: 17.4261111111111,
		power: 11557.9942077179,
		road: 7002.55194444445,
		acceleration: 0.404259259259259
	},
	{
		id: 580,
		time: 579,
		velocity: 17.8775,
		power: 10790.3769354296,
		road: 7020.18768518519,
		acceleration: 0.33666666666667
	},
	{
		id: 581,
		time: 580,
		velocity: 18.1086111111111,
		power: 11571.7480574398,
		road: 7038.17300925926,
		acceleration: 0.362499999999997
	},
	{
		id: 582,
		time: 581,
		velocity: 18.5136111111111,
		power: 10954.7707624533,
		road: 7056.49361111111,
		acceleration: 0.308055555555558
	},
	{
		id: 583,
		time: 582,
		velocity: 18.8016666666667,
		power: 11241.4639600487,
		road: 7075.12189814815,
		acceleration: 0.307314814814813
	},
	{
		id: 584,
		time: 583,
		velocity: 19.0305555555556,
		power: 10230.6795744965,
		road: 7094.02212962963,
		acceleration: 0.236574074074074
	},
	{
		id: 585,
		time: 584,
		velocity: 19.2233333333333,
		power: 9812.68942324379,
		road: 7113.14189814815,
		acceleration: 0.202500000000001
	},
	{
		id: 586,
		time: 585,
		velocity: 19.4091666666667,
		power: 9047.85891897564,
		road: 7132.43907407408,
		acceleration: 0.152314814814815
	},
	{
		id: 587,
		time: 586,
		velocity: 19.4875,
		power: 6489.58487671626,
		road: 7151.81773148148,
		acceleration: 0.010648148148146
	},
	{
		id: 588,
		time: 587,
		velocity: 19.2552777777778,
		power: 4239.97742766644,
		road: 7171.14726851852,
		acceleration: -0.108888888888892
	},
	{
		id: 589,
		time: 588,
		velocity: 19.0825,
		power: 5154.71251740425,
		road: 7190.39412037037,
		acceleration: -0.0564814814814767
	},
	{
		id: 590,
		time: 589,
		velocity: 19.3180555555556,
		power: 5682.30553470791,
		road: 7209.59958333334,
		acceleration: -0.0262962962963016
	},
	{
		id: 591,
		time: 590,
		velocity: 19.1763888888889,
		power: 6960.94701119038,
		road: 7228.81333333334,
		acceleration: 0.0428703703703732
	},
	{
		id: 592,
		time: 591,
		velocity: 19.2111111111111,
		power: 6692.27260141089,
		road: 7248.06194444445,
		acceleration: 0.0268518518518519
	},
	{
		id: 593,
		time: 592,
		velocity: 19.3986111111111,
		power: 8371.62884445766,
		road: 7267.3813425926,
		acceleration: 0.114722222222223
	},
	{
		id: 594,
		time: 593,
		velocity: 19.5205555555556,
		power: 10831.3286059905,
		road: 7286.87731481482,
		acceleration: 0.238425925925924
	},
	{
		id: 595,
		time: 594,
		velocity: 19.9263888888889,
		power: 9597.96421941885,
		road: 7306.5738425926,
		acceleration: 0.162685185185186
	},
	{
		id: 596,
		time: 595,
		velocity: 19.8866666666667,
		power: 11991.0973378977,
		road: 7326.49046296297,
		acceleration: 0.277500000000003
	},
	{
		id: 597,
		time: 596,
		velocity: 20.3530555555556,
		power: 9065.26444008348,
		road: 7346.60337962963,
		acceleration: 0.115092592592589
	},
	{
		id: 598,
		time: 597,
		velocity: 20.2716666666667,
		power: 3099.43333639963,
		road: 7366.67722222223,
		acceleration: -0.193240740740741
	},
	{
		id: 599,
		time: 598,
		velocity: 19.3069444444444,
		power: 2516.08820014873,
		road: 7386.54564814815,
		acceleration: -0.217592592592592
	},
	{
		id: 600,
		time: 599,
		velocity: 19.7002777777778,
		power: 4320.5917979396,
		road: 7406.24671296297,
		acceleration: -0.11712962962963
	},
	{
		id: 601,
		time: 600,
		velocity: 19.9202777777778,
		power: 11954.2426124856,
		road: 7426.03060185186,
		acceleration: 0.282777777777778
	},
	{
		id: 602,
		time: 601,
		velocity: 20.1552777777778,
		power: 9454.4666562695,
		road: 7446.0262962963,
		acceleration: 0.140833333333333
	},
	{
		id: 603,
		time: 602,
		velocity: 20.1227777777778,
		power: 8209.82598858588,
		road: 7466.12800925926,
		acceleration: 0.0712037037037021
	},
	{
		id: 604,
		time: 603,
		velocity: 20.1338888888889,
		power: 6627.24290263925,
		road: 7486.25925925926,
		acceleration: -0.0121296296296265
	},
	{
		id: 605,
		time: 604,
		velocity: 20.1188888888889,
		power: 7611.48665181933,
		road: 7506.40361111112,
		acceleration: 0.0383333333333304
	},
	{
		id: 606,
		time: 605,
		velocity: 20.2377777777778,
		power: 9516.53143649439,
		road: 7526.63361111112,
		acceleration: 0.132962962962964
	},
	{
		id: 607,
		time: 606,
		velocity: 20.5327777777778,
		power: 12738.3669188407,
		road: 7547.0738425926,
		acceleration: 0.287500000000005
	},
	{
		id: 608,
		time: 607,
		velocity: 20.9813888888889,
		power: 12900.407135577,
		road: 7567.79787037037,
		acceleration: 0.280092592592588
	},
	{
		id: 609,
		time: 608,
		velocity: 21.0780555555556,
		power: 7680.09374660098,
		road: 7588.6675462963,
		acceleration: 0.0112037037037069
	},
	{
		id: 610,
		time: 609,
		velocity: 20.5663888888889,
		power: 2956.67150249482,
		road: 7609.43199074074,
		acceleration: -0.221666666666671
	},
	{
		id: 611,
		time: 610,
		velocity: 20.3163888888889,
		power: -1942.71146975408,
		road: 7629.85481481482,
		acceleration: -0.461574074074072
	},
	{
		id: 612,
		time: 611,
		velocity: 19.6933333333333,
		power: -317.499657450391,
		road: 7649.86212962963,
		acceleration: -0.369444444444444
	},
	{
		id: 613,
		time: 612,
		velocity: 19.4580555555556,
		power: -2502.13237345256,
		road: 7669.4462962963,
		acceleration: -0.476851851851851
	},
	{
		id: 614,
		time: 613,
		velocity: 18.8858333333333,
		power: -1892.46659429961,
		road: 7688.57361111111,
		acceleration: -0.436851851851848
	},
	{
		id: 615,
		time: 614,
		velocity: 18.3827777777778,
		power: -5017.41401261961,
		road: 7707.18041666667,
		acceleration: -0.604166666666668
	},
	{
		id: 616,
		time: 615,
		velocity: 17.6455555555556,
		power: -3294.33191100391,
		road: 7725.23425925926,
		acceleration: -0.501759259259263
	},
	{
		id: 617,
		time: 616,
		velocity: 17.3805555555556,
		power: -2026.33974874681,
		road: 7742.82611111111,
		acceleration: -0.422222222222217
	},
	{
		id: 618,
		time: 617,
		velocity: 17.1161111111111,
		power: -1586.13696697061,
		road: 7760.01166666667,
		acceleration: -0.390370370370373
	},
	{
		id: 619,
		time: 618,
		velocity: 16.4744444444444,
		power: -1647.21292327521,
		road: 7776.8075462963,
		acceleration: -0.388981481481483
	},
	{
		id: 620,
		time: 619,
		velocity: 16.2136111111111,
		power: -2300.678436592,
		road: 7793.19611111112,
		acceleration: -0.425648148148149
	},
	{
		id: 621,
		time: 620,
		velocity: 15.8391666666667,
		power: -1062.76554768677,
		road: 7809.20120370371,
		acceleration: -0.341296296296296
	},
	{
		id: 622,
		time: 621,
		velocity: 15.4505555555556,
		power: -2859.98159655269,
		road: 7824.80740740741,
		acceleration: -0.456481481481481
	},
	{
		id: 623,
		time: 622,
		velocity: 14.8441666666667,
		power: -3516.25839827263,
		road: 7839.93550925926,
		acceleration: -0.499722222222221
	},
	{
		id: 624,
		time: 623,
		velocity: 14.34,
		power: -5984.98090899302,
		road: 7854.47495370371,
		acceleration: -0.677592592592594
	},
	{
		id: 625,
		time: 624,
		velocity: 13.4177777777778,
		power: -4653.66026768709,
		road: 7868.38212962963,
		acceleration: -0.586944444444443
	},
	{
		id: 626,
		time: 625,
		velocity: 13.0833333333333,
		power: -5552.49969446345,
		road: 7881.66337962963,
		acceleration: -0.664907407407409
	},
	{
		id: 627,
		time: 626,
		velocity: 12.3452777777778,
		power: -2015.01215079778,
		road: 7894.41981481482,
		acceleration: -0.384722222222223
	},
	{
		id: 628,
		time: 627,
		velocity: 12.2636111111111,
		power: -219.123135507069,
		road: 7906.86726851852,
		acceleration: -0.23324074074074
	},
	{
		id: 629,
		time: 628,
		velocity: 12.3836111111111,
		power: -1898.20141239943,
		road: 7919.01087962963,
		acceleration: -0.374444444444444
	},
	{
		id: 630,
		time: 629,
		velocity: 11.2219444444444,
		power: -2725.24953034974,
		road: 7930.74296296297,
		acceleration: -0.448611111111111
	},
	{
		id: 631,
		time: 630,
		velocity: 10.9177777777778,
		power: -7424.35788810664,
		road: 7941.80037037037,
		acceleration: -0.900740740740739
	},
	{
		id: 632,
		time: 631,
		velocity: 9.68138888888889,
		power: -5322.79516726342,
		road: 7952.04138888889,
		acceleration: -0.732037037037038
	},
	{
		id: 633,
		time: 632,
		velocity: 9.02583333333333,
		power: -4987.17037853206,
		road: 7961.55189814815,
		acceleration: -0.728981481481481
	},
	{
		id: 634,
		time: 633,
		velocity: 8.73083333333333,
		power: -1984.68095894089,
		road: 7970.49513888889,
		acceleration: -0.405555555555555
	},
	{
		id: 635,
		time: 634,
		velocity: 8.46472222222222,
		power: -206.75748006108,
		road: 7979.13828703704,
		acceleration: -0.194629629629631
	},
	{
		id: 636,
		time: 635,
		velocity: 8.44194444444444,
		power: 242.28214587094,
		road: 7987.61518518519,
		acceleration: -0.13787037037037
	},
	{
		id: 637,
		time: 636,
		velocity: 8.31722222222222,
		power: 746.25361984068,
		road: 7995.98652777778,
		acceleration: -0.0732407407407418
	},
	{
		id: 638,
		time: 637,
		velocity: 8.245,
		power: 779.33681202198,
		road: 8004.28745370371,
		acceleration: -0.0675925925925913
	},
	{
		id: 639,
		time: 638,
		velocity: 8.23916666666667,
		power: 1227.22359932251,
		road: 8012.54967592593,
		acceleration: -0.00981481481481516
	},
	{
		id: 640,
		time: 639,
		velocity: 8.28777777777778,
		power: 1508.15176267257,
		road: 8020.81981481482,
		acceleration: 0.0256481481481483
	},
	{
		id: 641,
		time: 640,
		velocity: 8.32194444444444,
		power: 1710.46258059225,
		road: 8029.12777777778,
		acceleration: 0.0499999999999989
	},
	{
		id: 642,
		time: 641,
		velocity: 8.38916666666667,
		power: 1909.23815605222,
		road: 8037.49712962963,
		acceleration: 0.0727777777777803
	},
	{
		id: 643,
		time: 642,
		velocity: 8.50611111111111,
		power: 2102.30479987826,
		road: 8045.94967592593,
		acceleration: 0.0936111111111089
	},
	{
		id: 644,
		time: 643,
		velocity: 8.60277777777778,
		power: 2273.03336345961,
		road: 8054.50425925926,
		acceleration: 0.110462962962963
	},
	{
		id: 645,
		time: 644,
		velocity: 8.72055555555555,
		power: 1904.56270266703,
		road: 8063.1450925926,
		acceleration: 0.0620370370370384
	},
	{
		id: 646,
		time: 645,
		velocity: 8.69222222222222,
		power: 2369.22902174488,
		road: 8071.87430555556,
		acceleration: 0.114722222222223
	},
	{
		id: 647,
		time: 646,
		velocity: 8.94694444444445,
		power: 2984.80079929337,
		road: 8080.75152777778,
		acceleration: 0.181296296296296
	},
	{
		id: 648,
		time: 647,
		velocity: 9.26444444444444,
		power: 3863.14794206843,
		road: 8089.85513888889,
		acceleration: 0.271481481481482
	},
	{
		id: 649,
		time: 648,
		velocity: 9.50666666666667,
		power: 3740.81230771536,
		road: 8099.21587962963,
		acceleration: 0.242777777777777
	},
	{
		id: 650,
		time: 649,
		velocity: 9.67527777777778,
		power: 1063.61808948647,
		road: 8108.66819444445,
		acceleration: -0.0596296296296295
	},
	{
		id: 651,
		time: 650,
		velocity: 9.08555555555555,
		power: 307.052785023427,
		road: 8118.01958333334,
		acceleration: -0.142222222222221
	},
	{
		id: 652,
		time: 651,
		velocity: 9.08,
		power: 20.7006944282229,
		road: 8127.21351851852,
		acceleration: -0.172685185185186
	},
	{
		id: 653,
		time: 652,
		velocity: 9.15722222222222,
		power: 196.362619695346,
		road: 8136.24583333334,
		acceleration: -0.150555555555554
	},
	{
		id: 654,
		time: 653,
		velocity: 8.63388888888889,
		power: 66.0355491243521,
		road: 8145.12087962963,
		acceleration: -0.163981481481482
	},
	{
		id: 655,
		time: 654,
		velocity: 8.58805555555556,
		power: -270.649599401275,
		road: 8153.8125925926,
		acceleration: -0.202685185185185
	},
	{
		id: 656,
		time: 655,
		velocity: 8.54916666666667,
		power: 623.821987843299,
		road: 8162.35703703704,
		acceleration: -0.0918518518518532
	},
	{
		id: 657,
		time: 656,
		velocity: 8.35833333333333,
		power: 990.020167538727,
		road: 8170.83296296297,
		acceleration: -0.0451851851851846
	},
	{
		id: 658,
		time: 657,
		velocity: 8.4525,
		power: 55.073459627909,
		road: 8179.2062962963,
		acceleration: -0.160000000000002
	},
	{
		id: 659,
		time: 658,
		velocity: 8.06916666666667,
		power: 1063.62958191365,
		road: 8187.48412037037,
		acceleration: -0.0310185185185166
	},
	{
		id: 660,
		time: 659,
		velocity: 8.26527777777778,
		power: 1539.09155820032,
		road: 8195.76111111111,
		acceleration: 0.0293518518518514
	},
	{
		id: 661,
		time: 660,
		velocity: 8.54055555555555,
		power: 761.07942070748,
		road: 8204.01828703704,
		acceleration: -0.0689814814814813
	},
	{
		id: 662,
		time: 661,
		velocity: 7.86222222222222,
		power: 665.166664343076,
		road: 8212.20111111112,
		acceleration: -0.0797222222222231
	},
	{
		id: 663,
		time: 662,
		velocity: 8.02611111111111,
		power: -402.32686908559,
		road: 8220.23587962963,
		acceleration: -0.216388888888889
	},
	{
		id: 664,
		time: 663,
		velocity: 7.89138888888889,
		power: 771.877528612082,
		road: 8228.1325462963,
		acceleration: -0.0598148148148141
	},
	{
		id: 665,
		time: 664,
		velocity: 7.68277777777778,
		power: -69.7201120178689,
		road: 8235.9138425926,
		acceleration: -0.170925925925926
	},
	{
		id: 666,
		time: 665,
		velocity: 7.51333333333333,
		power: 447.722947375217,
		road: 8243.56027777778,
		acceleration: -0.098796296296296
	},
	{
		id: 667,
		time: 666,
		velocity: 7.595,
		power: 1655.99896906809,
		road: 8251.19125,
		acceleration: 0.0678703703703691
	},
	{
		id: 668,
		time: 667,
		velocity: 7.88638888888889,
		power: 2794.02244313877,
		road: 8258.96430555556,
		acceleration: 0.216296296296298
	},
	{
		id: 669,
		time: 668,
		velocity: 8.16222222222222,
		power: 3451.84559135708,
		road: 8266.98962962963,
		acceleration: 0.288240740740741
	},
	{
		id: 670,
		time: 669,
		velocity: 8.45972222222222,
		power: 3503.72817166978,
		road: 8275.29745370371,
		acceleration: 0.27675925925926
	},
	{
		id: 671,
		time: 670,
		velocity: 8.71666666666667,
		power: 2794.0038395257,
		road: 8283.83138888889,
		acceleration: 0.175462962962962
	},
	{
		id: 672,
		time: 671,
		velocity: 8.68861111111111,
		power: 2051.98555986896,
		road: 8292.49263888889,
		acceleration: 0.0791666666666675
	},
	{
		id: 673,
		time: 672,
		velocity: 8.69722222222222,
		power: 846.683172462123,
		road: 8301.15990740741,
		acceleration: -0.0671296296296298
	},
	{
		id: 674,
		time: 673,
		velocity: 8.51527777777778,
		power: 1022.72630547969,
		road: 8309.77138888889,
		acceleration: -0.0444444444444461
	},
	{
		id: 675,
		time: 674,
		velocity: 8.55527777777778,
		power: 488.487913312714,
		road: 8318.30648148148,
		acceleration: -0.108333333333333
	},
	{
		id: 676,
		time: 675,
		velocity: 8.37222222222222,
		power: -544.270173313925,
		road: 8326.66981481482,
		acceleration: -0.235185185185184
	},
	{
		id: 677,
		time: 676,
		velocity: 7.80972222222222,
		power: -2094.28778633734,
		road: 8334.69662037037,
		acceleration: -0.437870370370372
	},
	{
		id: 678,
		time: 677,
		velocity: 7.24166666666667,
		power: -2321.51518226152,
		road: 8342.26342592593,
		acceleration: -0.482129629629628
	},
	{
		id: 679,
		time: 678,
		velocity: 6.92583333333333,
		power: -2375.77352806387,
		road: 8349.33476851852,
		acceleration: -0.508796296296297
	},
	{
		id: 680,
		time: 679,
		velocity: 6.28333333333333,
		power: -1575.02970733312,
		road: 8355.95050925926,
		acceleration: -0.402407407407408
	},
	{
		id: 681,
		time: 680,
		velocity: 6.03444444444444,
		power: -2222.29688812313,
		road: 8362.10064814815,
		acceleration: -0.528796296296296
	},
	{
		id: 682,
		time: 681,
		velocity: 5.33944444444445,
		power: -1316.81171738064,
		road: 8367.79185185185,
		acceleration: -0.389074074074074
	},
	{
		id: 683,
		time: 682,
		velocity: 5.11611111111111,
		power: -1527.65835435131,
		road: 8373.06453703704,
		acceleration: -0.447962962962963
	},
	{
		id: 684,
		time: 683,
		velocity: 4.69055555555556,
		power: -1385.69811615029,
		road: 8377.89189814815,
		acceleration: -0.442685185185186
	},
	{
		id: 685,
		time: 684,
		velocity: 4.01138888888889,
		power: -2292.5612321925,
		road: 8382.14532407408,
		acceleration: -0.705185185185185
	},
	{
		id: 686,
		time: 685,
		velocity: 3.00055555555556,
		power: -2788.10664478131,
		road: 8385.54745370371,
		acceleration: -0.997407407407408
	},
	{
		id: 687,
		time: 686,
		velocity: 1.69833333333333,
		power: -2220.06171300253,
		road: 8387.88495370371,
		acceleration: -1.13185185185185
	},
	{
		id: 688,
		time: 687,
		velocity: 0.615833333333333,
		power: -895.940652306355,
		road: 8389.24476851852,
		acceleration: -0.823518518518519
	},
	{
		id: 689,
		time: 688,
		velocity: 0.53,
		power: -276.253435458984,
		road: 8389.90976851852,
		acceleration: -0.566111111111111
	},
	{
		id: 690,
		time: 689,
		velocity: 0,
		power: -20.5677387593655,
		road: 8390.18907407408,
		acceleration: -0.205277777777778
	},
	{
		id: 691,
		time: 690,
		velocity: 0,
		power: -4.11196315789473,
		road: 8390.27740740741,
		acceleration: -0.176666666666667
	},
	{
		id: 692,
		time: 691,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 693,
		time: 692,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 694,
		time: 693,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 695,
		time: 694,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 696,
		time: 695,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 697,
		time: 696,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 698,
		time: 697,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 699,
		time: 698,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 700,
		time: 699,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 701,
		time: 700,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 702,
		time: 701,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 703,
		time: 702,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 704,
		time: 703,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 705,
		time: 704,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 706,
		time: 705,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 707,
		time: 706,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 708,
		time: 707,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 709,
		time: 708,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 710,
		time: 709,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 711,
		time: 710,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 712,
		time: 711,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 713,
		time: 712,
		velocity: 0,
		power: 0,
		road: 8390.27740740741,
		acceleration: 0
	},
	{
		id: 714,
		time: 713,
		velocity: 0,
		power: 74.5301846743311,
		road: 8390.44638888889,
		acceleration: 0.337962962962963
	},
	{
		id: 715,
		time: 714,
		velocity: 1.01388888888889,
		power: 707.699810317587,
		road: 8391.20958333334,
		acceleration: 0.850462962962963
	},
	{
		id: 716,
		time: 715,
		velocity: 2.55138888888889,
		power: 1991.54904356746,
		road: 8392.94013888889,
		acceleration: 1.08425925925926
	},
	{
		id: 717,
		time: 716,
		velocity: 3.25277777777778,
		power: 2783.97384068845,
		road: 8395.68199074074,
		acceleration: 0.938333333333333
	},
	{
		id: 718,
		time: 717,
		velocity: 3.82888888888889,
		power: 1185.37889976209,
		road: 8399.01361111112,
		acceleration: 0.241203703703703
	},
	{
		id: 719,
		time: 718,
		velocity: 3.275,
		power: 685.233302866395,
		road: 8402.50217592593,
		acceleration: 0.0726851851851849
	},
	{
		id: 720,
		time: 719,
		velocity: 3.47083333333333,
		power: 1412.74668305651,
		road: 8406.16287037038,
		acceleration: 0.271574074074074
	},
	{
		id: 721,
		time: 720,
		velocity: 4.64361111111111,
		power: 2594.13009340043,
		road: 8410.22708333334,
		acceleration: 0.535462962962963
	},
	{
		id: 722,
		time: 721,
		velocity: 4.88138888888889,
		power: 3622.64527395068,
		road: 8414.89745370371,
		acceleration: 0.676851851851852
	},
	{
		id: 723,
		time: 722,
		velocity: 5.50138888888889,
		power: 3782.29938818782,
		road: 8420.20925925926,
		acceleration: 0.606018518518519
	},
	{
		id: 724,
		time: 723,
		velocity: 6.46166666666667,
		power: 5944.69072135354,
		road: 8426.26620370371,
		acceleration: 0.88425925925926
	},
	{
		id: 725,
		time: 724,
		velocity: 7.53416666666667,
		power: 7167.17096693875,
		road: 8433.22898148149,
		acceleration: 0.927407407407407
	},
	{
		id: 726,
		time: 725,
		velocity: 8.28361111111111,
		power: 9066.89969428608,
		road: 8441.17351851852,
		acceleration: 1.03611111111111
	},
	{
		id: 727,
		time: 726,
		velocity: 9.57,
		power: 8597.43063580323,
		road: 8450.05824074075,
		acceleration: 0.844259259259259
	},
	{
		id: 728,
		time: 727,
		velocity: 10.0669444444444,
		power: 6441.57182102068,
		road: 8459.6288425926,
		acceleration: 0.5275
	},
	{
		id: 729,
		time: 728,
		velocity: 9.86611111111111,
		power: 6454.07756650391,
		road: 8469.70689814816,
		acceleration: 0.487407407407408
	},
	{
		id: 730,
		time: 729,
		velocity: 11.0322222222222,
		power: 4994.09459078752,
		road: 8480.18407407408,
		acceleration: 0.310833333333333
	},
	{
		id: 731,
		time: 730,
		velocity: 10.9994444444444,
		power: 6200.72968873853,
		road: 8491.02000000001,
		acceleration: 0.406666666666666
	},
	{
		id: 732,
		time: 731,
		velocity: 11.0861111111111,
		power: 3502.2456759866,
		road: 8502.12615740741,
		acceleration: 0.133796296296296
	},
	{
		id: 733,
		time: 732,
		velocity: 11.4336111111111,
		power: 9495.85269497818,
		road: 8513.63092592593,
		acceleration: 0.663425925925925
	},
	{
		id: 734,
		time: 733,
		velocity: 12.9897222222222,
		power: 9195.54630656206,
		road: 8525.75958333334,
		acceleration: 0.584351851851853
	},
	{
		id: 735,
		time: 734,
		velocity: 12.8391666666667,
		power: 7204.84833326879,
		road: 8538.37120370371,
		acceleration: 0.381574074074074
	},
	{
		id: 736,
		time: 735,
		velocity: 12.5783333333333,
		power: 600.233044710054,
		road: 8551.08902777778,
		acceleration: -0.169166666666667
	},
	{
		id: 737,
		time: 736,
		velocity: 12.4822222222222,
		power: 2894.37941977182,
		road: 8563.73342592593,
		acceleration: 0.022314814814818
	},
	{
		id: 738,
		time: 737,
		velocity: 12.9061111111111,
		power: 5228.16626060554,
		road: 8576.49402777778,
		acceleration: 0.210092592592591
	},
	{
		id: 739,
		time: 738,
		velocity: 13.2086111111111,
		power: 5844.05581258887,
		road: 8589.48407407408,
		acceleration: 0.248796296296296
	},
	{
		id: 740,
		time: 739,
		velocity: 13.2286111111111,
		power: 4044.07971589899,
		road: 8602.64689814815,
		acceleration: 0.0967592592592581
	},
	{
		id: 741,
		time: 740,
		velocity: 13.1963888888889,
		power: 4119.76868575395,
		road: 8615.9075462963,
		acceleration: 0.0988888888888884
	},
	{
		id: 742,
		time: 741,
		velocity: 13.5052777777778,
		power: 5567.47363962892,
		road: 8629.32060185186,
		acceleration: 0.205925925925929
	},
	{
		id: 743,
		time: 742,
		velocity: 13.8463888888889,
		power: 7936.99923464373,
		road: 8643.02328703704,
		acceleration: 0.373333333333331
	},
	{
		id: 744,
		time: 743,
		velocity: 14.3163888888889,
		power: 7812.23260817202,
		road: 8657.08407407408,
		acceleration: 0.34287037037037
	},
	{
		id: 745,
		time: 744,
		velocity: 14.5338888888889,
		power: 6896.04733880736,
		road: 8671.44578703704,
		acceleration: 0.258981481481483
	},
	{
		id: 746,
		time: 745,
		velocity: 14.6233333333333,
		power: 6124.88005803887,
		road: 8686.03305555556,
		acceleration: 0.192129629629628
	},
	{
		id: 747,
		time: 746,
		velocity: 14.8927777777778,
		power: 7000.43875411062,
		road: 8700.83833333334,
		acceleration: 0.24388888888889
	},
	{
		id: 748,
		time: 747,
		velocity: 15.2655555555556,
		power: 8391.27774233197,
		road: 8715.92861111112,
		acceleration: 0.326111111111111
	},
	{
		id: 749,
		time: 748,
		velocity: 15.6016666666667,
		power: 8574.0391892622,
		road: 8731.34226851852,
		acceleration: 0.32064814814815
	},
	{
		id: 750,
		time: 749,
		velocity: 15.8547222222222,
		power: 9562.49777278418,
		road: 8747.09995370371,
		acceleration: 0.367407407407406
	},
	{
		id: 751,
		time: 750,
		velocity: 16.3677777777778,
		power: 8903.70575398989,
		road: 8763.19393518519,
		acceleration: 0.305185185185188
	},
	{
		id: 752,
		time: 751,
		velocity: 16.5172222222222,
		power: 8924.58553167415,
		road: 8779.58574074074,
		acceleration: 0.290462962962962
	},
	{
		id: 753,
		time: 752,
		velocity: 16.7261111111111,
		power: 6927.00774336793,
		road: 8796.19925925926,
		acceleration: 0.152962962962963
	},
	{
		id: 754,
		time: 753,
		velocity: 16.8266666666667,
		power: 5799.61931908687,
		road: 8812.92791666667,
		acceleration: 0.0773148148148124
	},
	{
		id: 755,
		time: 754,
		velocity: 16.7491666666667,
		power: 4000.47723443945,
		road: 8829.67731481482,
		acceleration: -0.0358333333333327
	},
	{
		id: 756,
		time: 755,
		velocity: 16.6186111111111,
		power: 4659.23656764423,
		road: 8846.41171296297,
		acceleration: 0.00583333333333158
	},
	{
		id: 757,
		time: 756,
		velocity: 16.8441666666667,
		power: 5209.63210949549,
		road: 8863.16870370371,
		acceleration: 0.0393518518518547
	},
	{
		id: 758,
		time: 757,
		velocity: 16.8672222222222,
		power: 7172.38516109353,
		road: 8880.02402777778,
		acceleration: 0.157314814814814
	},
	{
		id: 759,
		time: 758,
		velocity: 17.0905555555556,
		power: 7318.05048143886,
		road: 8897.0375462963,
		acceleration: 0.159074074074073
	},
	{
		id: 760,
		time: 759,
		velocity: 17.3213888888889,
		power: 7507.44369378662,
		road: 8914.21222222223,
		acceleration: 0.16324074074074
	},
	{
		id: 761,
		time: 760,
		velocity: 17.3569444444444,
		power: 5774.08603473866,
		road: 8931.49523148149,
		acceleration: 0.0534259259259251
	},
	{
		id: 762,
		time: 761,
		velocity: 17.2508333333333,
		power: 2318.46481729008,
		road: 8948.72787037038,
		acceleration: -0.154166666666665
	},
	{
		id: 763,
		time: 762,
		velocity: 16.8588888888889,
		power: 445.366913170502,
		road: 8965.75171296297,
		acceleration: -0.263425925925926
	},
	{
		id: 764,
		time: 763,
		velocity: 16.5666666666667,
		power: 21.0201619059418,
		road: 8982.50171296297,
		acceleration: -0.284259259259262
	},
	{
		id: 765,
		time: 764,
		velocity: 16.3980555555556,
		power: -584.760500997964,
		road: 8998.95101851852,
		acceleration: -0.317129629629626
	},
	{
		id: 766,
		time: 765,
		velocity: 15.9075,
		power: -691.875670262243,
		road: 9015.08226851852,
		acceleration: -0.318981481481483
	},
	{
		id: 767,
		time: 766,
		velocity: 15.6097222222222,
		power: 480.410630440338,
		road: 9030.93532407408,
		acceleration: -0.237407407407405
	},
	{
		id: 768,
		time: 767,
		velocity: 15.6858333333333,
		power: 2563.52467619217,
		road: 9046.62208333334,
		acceleration: -0.095185185185187
	},
	{
		id: 769,
		time: 768,
		velocity: 15.6219444444444,
		power: 5185.82858006723,
		road: 9062.30125000001,
		acceleration: 0.0800000000000001
	},
	{
		id: 770,
		time: 769,
		velocity: 15.8497222222222,
		power: 6783.97218889009,
		road: 9078.11069444445,
		acceleration: 0.180555555555555
	},
	{
		id: 771,
		time: 770,
		velocity: 16.2275,
		power: 8774.00040122389,
		road: 9094.16000000001,
		acceleration: 0.299166666666663
	},
	{
		id: 772,
		time: 771,
		velocity: 16.5194444444444,
		power: 10548.2240868925,
		road: 9110.55578703704,
		acceleration: 0.393796296296298
	},
	{
		id: 773,
		time: 772,
		velocity: 17.0311111111111,
		power: 10134.4639898183,
		road: 9127.32152777779,
		acceleration: 0.34611111111111
	},
	{
		id: 774,
		time: 773,
		velocity: 17.2658333333333,
		power: 12006.0218881299,
		road: 9144.47935185186,
		acceleration: 0.438055555555557
	},
	{
		id: 775,
		time: 774,
		velocity: 17.8336111111111,
		power: 11197.0874824669,
		road: 9162.03879629631,
		acceleration: 0.365185185185187
	},
	{
		id: 776,
		time: 775,
		velocity: 18.1266666666667,
		power: 11799.3908684103,
		road: 9179.97027777779,
		acceleration: 0.378888888888891
	},
	{
		id: 777,
		time: 776,
		velocity: 18.4025,
		power: 11072.2654851555,
		road: 9198.24972222223,
		acceleration: 0.317037037037032
	},
	{
		id: 778,
		time: 777,
		velocity: 18.7847222222222,
		power: 9603.54857189001,
		road: 9216.79745370371,
		acceleration: 0.219537037037039
	},
	{
		id: 779,
		time: 778,
		velocity: 18.7852777777778,
		power: 8959.41979665569,
		road: 9235.54185185186,
		acceleration: 0.173796296296295
	},
	{
		id: 780,
		time: 779,
		velocity: 18.9238888888889,
		power: 6488.10147802557,
		road: 9254.38912037038,
		acceleration: 0.031944444444445
	},
	{
		id: 781,
		time: 780,
		velocity: 18.8805555555556,
		power: 6948.96063179316,
		road: 9273.28018518519,
		acceleration: 0.0556481481481512
	},
	{
		id: 782,
		time: 781,
		velocity: 18.9522222222222,
		power: 5242.59401117182,
		road: 9292.17958333334,
		acceleration: -0.0389814814814855
	},
	{
		id: 783,
		time: 782,
		velocity: 18.8069444444444,
		power: 3428.21479422993,
		road: 9310.99125000001,
		acceleration: -0.136481481481482
	},
	{
		id: 784,
		time: 783,
		velocity: 18.4711111111111,
		power: 877.44669673996,
		road: 9329.59805555556,
		acceleration: -0.273240740740739
	},
	{
		id: 785,
		time: 784,
		velocity: 18.1325,
		power: -34.6223246068152,
		road: 9347.90907407408,
		acceleration: -0.318333333333335
	},
	{
		id: 786,
		time: 785,
		velocity: 17.8519444444444,
		power: -141.082155486302,
		road: 9365.90189814815,
		acceleration: -0.318055555555553
	},
	{
		id: 787,
		time: 786,
		velocity: 17.5169444444444,
		power: -688.786865259637,
		road: 9383.56370370371,
		acceleration: -0.343981481481482
	},
	{
		id: 788,
		time: 787,
		velocity: 17.1005555555556,
		power: -2666.72118230047,
		road: 9400.8250925926,
		acceleration: -0.456851851851848
	},
	{
		id: 789,
		time: 788,
		velocity: 16.4813888888889,
		power: -2405.71408326159,
		road: 9417.63986111112,
		acceleration: -0.436388888888892
	},
	{
		id: 790,
		time: 789,
		velocity: 16.2077777777778,
		power: -3539.95191252576,
		road: 9433.98416666667,
		acceleration: -0.504537037037039
	},
	{
		id: 791,
		time: 790,
		velocity: 15.5869444444444,
		power: -3408.02421206652,
		road: 9449.82921296297,
		acceleration: -0.493981481481478
	},
	{
		id: 792,
		time: 791,
		velocity: 14.9994444444444,
		power: -3468.94967867569,
		road: 9465.17888888889,
		acceleration: -0.496759259259258
	},
	{
		id: 793,
		time: 792,
		velocity: 14.7175,
		power: -6730.20943357709,
		road: 9479.91620370371,
		acceleration: -0.727962962962964
	},
	{
		id: 794,
		time: 793,
		velocity: 13.4030555555556,
		power: -8969.67893709039,
		road: 9493.83370370371,
		acceleration: -0.911666666666667
	},
	{
		id: 795,
		time: 794,
		velocity: 12.2644444444444,
		power: -12306.1244341523,
		road: 9506.68324074074,
		acceleration: -1.22425925925926
	},
	{
		id: 796,
		time: 795,
		velocity: 11.0447222222222,
		power: -11295.5032363683,
		road: 9518.30935185185,
		acceleration: -1.22259259259259
	},
	{
		id: 797,
		time: 796,
		velocity: 9.73527777777778,
		power: -12456.9427787241,
		road: 9528.5950462963,
		acceleration: -1.45824074074074
	},
	{
		id: 798,
		time: 797,
		velocity: 7.88972222222222,
		power: -12508.7970402957,
		road: 9537.31277777778,
		acceleration: -1.67768518518519
	},
	{
		id: 799,
		time: 798,
		velocity: 6.01166666666667,
		power: -9525.63265067294,
		road: 9544.40791666667,
		acceleration: -1.5675
	},
	{
		id: 800,
		time: 799,
		velocity: 5.03277777777778,
		power: -4781.81810973706,
		road: 9550.21273148149,
		acceleration: -1.01314814814815
	},
	{
		id: 801,
		time: 800,
		velocity: 4.85027777777778,
		power: -1492.50354841293,
		road: 9555.28518518519,
		acceleration: -0.451574074074074
	},
	{
		id: 802,
		time: 801,
		velocity: 4.65694444444444,
		power: 509.439617823235,
		road: 9560.11708333334,
		acceleration: -0.0295370370370369
	},
	{
		id: 803,
		time: 802,
		velocity: 4.94416666666667,
		power: 1390.47013163037,
		road: 9565.01324074075,
		acceleration: 0.158055555555555
	},
	{
		id: 804,
		time: 803,
		velocity: 5.32444444444444,
		power: 1876.37078769121,
		road: 9570.11111111112,
		acceleration: 0.24537037037037
	},
	{
		id: 805,
		time: 804,
		velocity: 5.39305555555555,
		power: 1897.65814281509,
		road: 9575.44708333334,
		acceleration: 0.230833333333334
	},
	{
		id: 806,
		time: 805,
		velocity: 5.63666666666667,
		power: 1629.8970710515,
		road: 9580.98111111112,
		acceleration: 0.165277777777777
	},
	{
		id: 807,
		time: 806,
		velocity: 5.82027777777778,
		power: 1169.04031215439,
		road: 9586.63388888889,
		acceleration: 0.0722222222222229
	},
	{
		id: 808,
		time: 807,
		velocity: 5.60972222222222,
		power: 1254.61196045359,
		road: 9592.365,
		acceleration: 0.0844444444444443
	},
	{
		id: 809,
		time: 808,
		velocity: 5.89,
		power: 1278.81745565831,
		road: 9598.18078703704,
		acceleration: 0.0849074074074068
	},
	{
		id: 810,
		time: 809,
		velocity: 6.075,
		power: 2312.51726689541,
		road: 9604.16837962963,
		acceleration: 0.258703703703705
	},
	{
		id: 811,
		time: 810,
		velocity: 6.38583333333333,
		power: 3634.21333541061,
		road: 9610.51157407408,
		acceleration: 0.452499999999999
	},
	{
		id: 812,
		time: 811,
		velocity: 7.2475,
		power: 5169.80533019999,
		road: 9617.39856481482,
		acceleration: 0.635092592592594
	},
	{
		id: 813,
		time: 812,
		velocity: 7.98027777777778,
		power: 7513.79603160834,
		road: 9625.03962962963,
		acceleration: 0.873055555555555
	},
	{
		id: 814,
		time: 813,
		velocity: 9.005,
		power: 8836.63473832275,
		road: 9633.57657407408,
		acceleration: 0.918703703703702
	},
	{
		id: 815,
		time: 814,
		velocity: 10.0036111111111,
		power: 9930.02160294957,
		road: 9643.035,
		acceleration: 0.92425925925926
	},
	{
		id: 816,
		time: 815,
		velocity: 10.7530555555556,
		power: 8238.49813659373,
		road: 9653.28398148149,
		acceleration: 0.656851851851851
	},
	{
		id: 817,
		time: 816,
		velocity: 10.9755555555556,
		power: 5070.97609878144,
		road: 9664.01314814815,
		acceleration: 0.303518518518517
	},
	{
		id: 818,
		time: 817,
		velocity: 10.9141666666667,
		power: 2524.01150355717,
		road: 9674.91824074074,
		acceleration: 0.0483333333333356
	},
	{
		id: 819,
		time: 818,
		velocity: 10.8980555555556,
		power: 3030.4125893631,
		road: 9685.89462962963,
		acceleration: 0.0942592592592568
	},
	{
		id: 820,
		time: 819,
		velocity: 11.2583333333333,
		power: 5270.88846664435,
		road: 9697.06666666667,
		acceleration: 0.29703703703704
	},
	{
		id: 821,
		time: 820,
		velocity: 11.8052777777778,
		power: 6722.88492462975,
		road: 9708.59189814815,
		acceleration: 0.409351851851852
	},
	{
		id: 822,
		time: 821,
		velocity: 12.1261111111111,
		power: 8856.96139058662,
		road: 9720.60388888889,
		acceleration: 0.564166666666667
	},
	{
		id: 823,
		time: 822,
		velocity: 12.9508333333333,
		power: 9855.20913254139,
		road: 9733.19949074074,
		acceleration: 0.603055555555555
	},
	{
		id: 824,
		time: 823,
		velocity: 13.6144444444444,
		power: 10333.2592070266,
		road: 9746.39402777778,
		acceleration: 0.594814814814812
	},
	{
		id: 825,
		time: 824,
		velocity: 13.9105555555556,
		power: 8051.43579050426,
		road: 9760.0775925926,
		acceleration: 0.383240740740744
	},
	{
		id: 826,
		time: 825,
		velocity: 14.1005555555556,
		power: 5514.56153308746,
		road: 9774.04087962963,
		acceleration: 0.176203703703703
	},
	{
		id: 827,
		time: 826,
		velocity: 14.1430555555556,
		power: 5042.62394111582,
		road: 9788.15935185185,
		acceleration: 0.134166666666669
	},
	{
		id: 828,
		time: 827,
		velocity: 14.3130555555556,
		power: 4426.17197272886,
		road: 9802.38699074074,
		acceleration: 0.0841666666666647
	},
	{
		id: 829,
		time: 828,
		velocity: 14.3530555555556,
		power: 5550.29919324116,
		road: 9816.73740740741,
		acceleration: 0.16138888888889
	},
	{
		id: 830,
		time: 829,
		velocity: 14.6272222222222,
		power: 5741.50086556189,
		road: 9831.25245370371,
		acceleration: 0.167870370370368
	},
	{
		id: 831,
		time: 830,
		velocity: 14.8166666666667,
		power: 6888.4236072348,
		road: 9845.97157407408,
		acceleration: 0.240277777777779
	},
	{
		id: 832,
		time: 831,
		velocity: 15.0738888888889,
		power: 5438.54426986376,
		road: 9860.87546296296,
		acceleration: 0.129259259259257
	},
	{
		id: 833,
		time: 832,
		velocity: 15.015,
		power: 5738.97678184557,
		road: 9875.91615740741,
		acceleration: 0.144351851851852
	},
	{
		id: 834,
		time: 833,
		velocity: 15.2497222222222,
		power: 5973.0421591059,
		road: 9891.10601851852,
		acceleration: 0.153981481481484
	},
	{
		id: 835,
		time: 834,
		velocity: 15.5358333333333,
		power: 6602.41427358722,
		road: 9906.4675,
		acceleration: 0.189259259259256
	},
	{
		id: 836,
		time: 835,
		velocity: 15.5827777777778,
		power: 6716.54774986085,
		road: 9922.01768518519,
		acceleration: 0.18814814814815
	},
	{
		id: 837,
		time: 836,
		velocity: 15.8141666666667,
		power: 6086.84084615661,
		road: 9937.73125,
		acceleration: 0.138611111111112
	},
	{
		id: 838,
		time: 837,
		velocity: 15.9516666666667,
		power: 5943.29581399114,
		road: 9953.57583333333,
		acceleration: 0.123425925925925
	},
	{
		id: 839,
		time: 838,
		velocity: 15.9530555555556,
		power: 4997.83955074767,
		road: 9969.51087962963,
		acceleration: 0.057500000000001
	},
	{
		id: 840,
		time: 839,
		velocity: 15.9866666666667,
		power: 4029.42719704752,
		road: 9985.4712037037,
		acceleration: -0.00694444444444464
	},
	{
		id: 841,
		time: 840,
		velocity: 15.9308333333333,
		power: 2554.7216174275,
		road: 10001.377037037,
		acceleration: -0.102037037037038
	},
	{
		id: 842,
		time: 841,
		velocity: 15.6469444444444,
		power: 2661.09045055316,
		road: 10017.1857407407,
		acceleration: -0.0922222222222224
	},
	{
		id: 843,
		time: 842,
		velocity: 15.71,
		power: 1924.7681510671,
		road: 10032.8793518519,
		acceleration: -0.137962962962963
	},
	{
		id: 844,
		time: 843,
		velocity: 15.5169444444444,
		power: 3279.94546952328,
		road: 10048.4816203704,
		acceleration: -0.044722222222223
	},
	{
		id: 845,
		time: 844,
		velocity: 15.5127777777778,
		power: 2453.14622134747,
		road: 10064.0124537037,
		acceleration: -0.0981481481481481
	},
	{
		id: 846,
		time: 845,
		velocity: 15.4155555555556,
		power: 2203.24722020864,
		road: 10079.4381481482,
		acceleration: -0.112129629629628
	},
	{
		id: 847,
		time: 846,
		velocity: 15.1805555555556,
		power: 1641.54169114513,
		road: 10094.7342592593,
		acceleration: -0.147037037037038
	},
	{
		id: 848,
		time: 847,
		velocity: 15.0716666666667,
		power: 1126.98457903983,
		road: 10109.8675462963,
		acceleration: -0.178611111111111
	},
	{
		id: 849,
		time: 848,
		velocity: 14.8797222222222,
		power: 1980.39594358396,
		road: 10124.8536574074,
		acceleration: -0.11574074074074
	},
	{
		id: 850,
		time: 849,
		velocity: 14.8333333333333,
		power: 476.540666180118,
		road: 10139.6731018519,
		acceleration: -0.217592592592593
	},
	{
		id: 851,
		time: 850,
		velocity: 14.4188888888889,
		power: 171.664527647507,
		road: 10154.2661574074,
		acceleration: -0.235185185185184
	},
	{
		id: 852,
		time: 851,
		velocity: 14.1741666666667,
		power: -112.797868011874,
		road: 10168.6157407407,
		acceleration: -0.251759259259259
	},
	{
		id: 853,
		time: 852,
		velocity: 14.0780555555556,
		power: 2025.2084271634,
		road: 10182.7938425926,
		acceleration: -0.0912037037037035
	},
	{
		id: 854,
		time: 853,
		velocity: 14.1452777777778,
		power: 4326.48140672035,
		road: 10196.9658333333,
		acceleration: 0.0789814814814793
	},
	{
		id: 855,
		time: 854,
		velocity: 14.4111111111111,
		power: 6570.69869084895,
		road: 10211.2957407407,
		acceleration: 0.236851851851853
	},
	{
		id: 856,
		time: 855,
		velocity: 14.7886111111111,
		power: 6601.84201601067,
		road: 10225.8578703704,
		acceleration: 0.227592592592591
	},
	{
		id: 857,
		time: 856,
		velocity: 14.8280555555556,
		power: 5733.25703606136,
		road: 10240.6120833333,
		acceleration: 0.156574074074074
	},
	{
		id: 858,
		time: 857,
		velocity: 14.8808333333333,
		power: 3002.51257750422,
		road: 10255.4250925926,
		acceleration: -0.038981481481482
	},
	{
		id: 859,
		time: 858,
		velocity: 14.6716666666667,
		power: 2795.66416994593,
		road: 10270.1925,
		acceleration: -0.0522222222222233
	},
	{
		id: 860,
		time: 859,
		velocity: 14.6713888888889,
		power: 1015.78691211742,
		road: 10284.8458333333,
		acceleration: -0.175925925925924
	},
	{
		id: 861,
		time: 860,
		velocity: 14.3530555555556,
		power: 3026.35070455677,
		road: 10299.3966666667,
		acceleration: -0.0290740740740745
	},
	{
		id: 862,
		time: 861,
		velocity: 14.5844444444444,
		power: 1970.03630028861,
		road: 10313.8812962963,
		acceleration: -0.103333333333332
	},
	{
		id: 863,
		time: 862,
		velocity: 14.3613888888889,
		power: 3246.18024465429,
		road: 10328.3096296296,
		acceleration: -0.00925925925925952
	},
	{
		id: 864,
		time: 863,
		velocity: 14.3252777777778,
		power: 3542.93605577545,
		road: 10342.7394444444,
		acceleration: 0.0122222222222241
	},
	{
		id: 865,
		time: 864,
		velocity: 14.6211111111111,
		power: 4598.58426617462,
		road: 10357.2187962963,
		acceleration: 0.0868518518518506
	},
	{
		id: 866,
		time: 865,
		velocity: 14.6219444444444,
		power: 5359.54149565914,
		road: 10371.8100925926,
		acceleration: 0.137037037037038
	},
	{
		id: 867,
		time: 866,
		velocity: 14.7363888888889,
		power: 7626.83093198338,
		road: 10386.6140277778,
		acceleration: 0.28824074074074
	},
	{
		id: 868,
		time: 867,
		velocity: 15.4858333333333,
		power: 10876.3427600444,
		road: 10401.8078240741,
		acceleration: 0.491481481481481
	},
	{
		id: 869,
		time: 868,
		velocity: 16.0963888888889,
		power: 13514.958184826,
		road: 10417.5623611111,
		acceleration: 0.630000000000001
	},
	{
		id: 870,
		time: 869,
		velocity: 16.6263888888889,
		power: 11816.4933196621,
		road: 10433.872037037,
		acceleration: 0.480277777777776
	},
	{
		id: 871,
		time: 870,
		velocity: 16.9266666666667,
		power: 7623.65309088823,
		road: 10450.5194444444,
		acceleration: 0.195185185185188
	},
	{
		id: 872,
		time: 871,
		velocity: 16.6819444444444,
		power: 4538.77820746817,
		road: 10467.2634259259,
		acceleration: -0.00203703703703795
	},
	{
		id: 873,
		time: 872,
		velocity: 16.6202777777778,
		power: 4266.06586170619,
		road: 10483.997037037,
		acceleration: -0.0187037037037072
	},
	{
		id: 874,
		time: 873,
		velocity: 16.8705555555556,
		power: 5805.29954101133,
		road: 10500.7594444444,
		acceleration: 0.0762962962962952
	},
	{
		id: 875,
		time: 874,
		velocity: 16.9108333333333,
		power: 7096.70824100846,
		road: 10517.6358333333,
		acceleration: 0.151666666666671
	},
	{
		id: 876,
		time: 875,
		velocity: 17.0752777777778,
		power: 5677.55684055956,
		road: 10534.6178240741,
		acceleration: 0.0595370370370389
	},
	{
		id: 877,
		time: 876,
		velocity: 17.0491666666667,
		power: 6075.20139735381,
		road: 10551.6701388889,
		acceleration: 0.0811111111111096
	},
	{
		id: 878,
		time: 877,
		velocity: 17.1541666666667,
		power: 6249.25474692225,
		road: 10568.8071296296,
		acceleration: 0.0882407407407406
	},
	{
		id: 879,
		time: 878,
		velocity: 17.34,
		power: 7845.30497277912,
		road: 10586.0778703704,
		acceleration: 0.179259259259261
	},
	{
		id: 880,
		time: 879,
		velocity: 17.5869444444444,
		power: 12423.2575877474,
		road: 10603.656712963,
		acceleration: 0.436944444444443
	},
	{
		id: 881,
		time: 880,
		velocity: 18.465,
		power: 14705.5798849805,
		road: 10621.7235185185,
		acceleration: 0.538981481481482
	},
	{
		id: 882,
		time: 881,
		velocity: 18.9569444444444,
		power: 14348.8516564921,
		road: 10640.3021759259,
		acceleration: 0.484722222222221
	},
	{
		id: 883,
		time: 882,
		velocity: 19.0411111111111,
		power: 8348.12981739371,
		road: 10659.1897685185,
		acceleration: 0.133148148148148
	},
	{
		id: 884,
		time: 883,
		velocity: 18.8644444444444,
		power: 2731.25718745411,
		road: 10678.0555555556,
		acceleration: -0.17675925925926
	},
	{
		id: 885,
		time: 884,
		velocity: 18.4266666666667,
		power: 721.258932751025,
		road: 10696.6916203704,
		acceleration: -0.282685185185183
	},
	{
		id: 886,
		time: 885,
		velocity: 18.1930555555556,
		power: -166.961355791759,
		road: 10715.0231944445,
		acceleration: -0.326296296296295
	},
	{
		id: 887,
		time: 886,
		velocity: 17.8855555555556,
		power: -6729.28388244785,
		road: 10732.8411111111,
		acceleration: -0.70101851851852
	},
	{
		id: 888,
		time: 887,
		velocity: 16.3236111111111,
		power: -16271.8942321221,
		road: 10749.659212963,
		acceleration: -1.29861111111111
	},
	{
		id: 889,
		time: 888,
		velocity: 14.2972222222222,
		power: -25215.528392221,
		road: 10764.8294444445,
		acceleration: -1.99712962962963
	},
	{
		id: 890,
		time: 889,
		velocity: 11.8941666666667,
		power: -29106.0490960963,
		road: 10777.7059259259,
		acceleration: -2.59037037037037
	},
	{
		id: 891,
		time: 890,
		velocity: 8.5525,
		power: -25604.6484833976,
		road: 10787.8722685185,
		acceleration: -2.82990740740741
	},
	{
		id: 892,
		time: 891,
		velocity: 5.8075,
		power: -16965.9076589902,
		road: 10795.3516666667,
		acceleration: -2.54398148148148
	},
	{
		id: 893,
		time: 892,
		velocity: 4.26222222222222,
		power: -7891.97376852212,
		road: 10800.7125,
		acceleration: -1.69314814814815
	},
	{
		id: 894,
		time: 893,
		velocity: 3.47305555555556,
		power: -5001.69164488448,
		road: 10804.4551388889,
		acceleration: -1.54324074074074
	},
	{
		id: 895,
		time: 894,
		velocity: 1.17777777777778,
		power: -2766.67683723758,
		road: 10806.715787037,
		acceleration: -1.42074074074074
	},
	{
		id: 896,
		time: 895,
		velocity: 0,
		power: -947.977764101224,
		road: 10807.6872222222,
		acceleration: -1.15768518518519
	},
	{
		id: 897,
		time: 896,
		velocity: 0,
		power: -49.2923417803769,
		road: 10807.8835185185,
		acceleration: -0.392592592592593
	},
	{
		id: 898,
		time: 897,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 899,
		time: 898,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 900,
		time: 899,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 901,
		time: 900,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 902,
		time: 901,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 903,
		time: 902,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 904,
		time: 903,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 905,
		time: 904,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 906,
		time: 905,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 907,
		time: 906,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 908,
		time: 907,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 909,
		time: 908,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 910,
		time: 909,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 911,
		time: 910,
		velocity: 0,
		power: 0,
		road: 10807.8835185185,
		acceleration: 0
	},
	{
		id: 912,
		time: 911,
		velocity: 0,
		power: 177.422384889771,
		road: 10808.1592592593,
		acceleration: 0.551481481481482
	},
	{
		id: 913,
		time: 912,
		velocity: 1.65444444444444,
		power: 969.126043172213,
		road: 10809.1583333333,
		acceleration: 0.895185185185185
	},
	{
		id: 914,
		time: 913,
		velocity: 2.68555555555556,
		power: 2365.15690763164,
		road: 10811.1622685185,
		acceleration: 1.11453703703704
	},
	{
		id: 915,
		time: 914,
		velocity: 3.34361111111111,
		power: 2133.79369112351,
		road: 10814.0471296296,
		acceleration: 0.647314814814814
	},
	{
		id: 916,
		time: 915,
		velocity: 3.59638888888889,
		power: 1795.71001502902,
		road: 10817.4653703704,
		acceleration: 0.419444444444445
	},
	{
		id: 917,
		time: 916,
		velocity: 3.94388888888889,
		power: 1486.67912401703,
		road: 10821.2334259259,
		acceleration: 0.280185185185185
	},
	{
		id: 918,
		time: 917,
		velocity: 4.18416666666667,
		power: 1920.98251393776,
		road: 10825.3206481482,
		acceleration: 0.358148148148148
	},
	{
		id: 919,
		time: 918,
		velocity: 4.67083333333333,
		power: 3337.36625728007,
		road: 10829.9008796296,
		acceleration: 0.627870370370371
	},
	{
		id: 920,
		time: 919,
		velocity: 5.8275,
		power: 6239.73609419951,
		road: 10835.3279166667,
		acceleration: 1.06574074074074
	},
	{
		id: 921,
		time: 920,
		velocity: 7.38138888888889,
		power: 14843.9310242108,
		road: 10842.3251851852,
		acceleration: 2.07472222222222
	},
	{
		id: 922,
		time: 921,
		velocity: 10.895,
		power: 25184.5769984376,
		road: 10851.6836574074,
		acceleration: 2.64768518518519
	},
	{
		id: 923,
		time: 922,
		velocity: 13.7705555555556,
		power: 38731.6579711407,
		road: 10863.9185185185,
		acceleration: 3.10509259259259
	},
	{
		id: 924,
		time: 923,
		velocity: 16.6966666666667,
		power: 31736.0787725662,
		road: 10878.7036574074,
		acceleration: 1.99546296296296
	},
	{
		id: 925,
		time: 924,
		velocity: 16.8813888888889,
		power: 25218.4472290599,
		road: 10895.148287037,
		acceleration: 1.32351851851852
	},
	{
		id: 926,
		time: 925,
		velocity: 17.7411111111111,
		power: 13461.6483489375,
		road: 10912.5111574074,
		acceleration: 0.512962962962959
	},
	{
		id: 927,
		time: 926,
		velocity: 18.2355555555556,
		power: 12396.4140626193,
		road: 10930.340462963,
		acceleration: 0.419907407407408
	},
	{
		id: 928,
		time: 927,
		velocity: 18.1411111111111,
		power: 6214.40733184904,
		road: 10948.4037962963,
		acceleration: 0.0481481481481509
	},
	{
		id: 929,
		time: 928,
		velocity: 17.8855555555556,
		power: 2842.14223844742,
		road: 10966.4184722222,
		acceleration: -0.145462962962963
	},
	{
		id: 930,
		time: 929,
		velocity: 17.7991666666667,
		power: 1899.21740960867,
		road: 10984.2625925926,
		acceleration: -0.195648148148148
	},
	{
		id: 931,
		time: 930,
		velocity: 17.5541666666667,
		power: 3274.68262582199,
		road: 11001.9537037037,
		acceleration: -0.110370370370372
	},
	{
		id: 932,
		time: 931,
		velocity: 17.5544444444444,
		power: 3559.04067209597,
		road: 11019.5444444445,
		acceleration: -0.0903703703703691
	},
	{
		id: 933,
		time: 932,
		velocity: 17.5280555555556,
		power: 4976.05273134113,
		road: 11037.0877777778,
		acceleration: -0.00444444444444514
	},
	{
		id: 934,
		time: 933,
		velocity: 17.5408333333333,
		power: 4785.0443580007,
		road: 11054.6211574074,
		acceleration: -0.0154629629629639
	},
	{
		id: 935,
		time: 934,
		velocity: 17.5080555555556,
		power: 4829.40114324336,
		road: 11072.1406481482,
		acceleration: -0.0123148148148147
	},
	{
		id: 936,
		time: 935,
		velocity: 17.4911111111111,
		power: 5155.32246475296,
		road: 11089.6575925926,
		acceleration: 0.00722222222222513
	},
	{
		id: 937,
		time: 936,
		velocity: 17.5625,
		power: 5243.69902853029,
		road: 11107.184212963,
		acceleration: 0.01212962962963
	},
	{
		id: 938,
		time: 937,
		velocity: 17.5444444444444,
		power: 4966.5249194288,
		road: 11124.7146296296,
		acceleration: -0.00453703703703923
	},
	{
		id: 939,
		time: 938,
		velocity: 17.4775,
		power: 5085.51507527029,
		road: 11142.2440740741,
		acceleration: 0.00259259259259537
	},
	{
		id: 940,
		time: 939,
		velocity: 17.5702777777778,
		power: 4864.12137936327,
		road: 11159.7695833333,
		acceleration: -0.0104629629629684
	},
	{
		id: 941,
		time: 940,
		velocity: 17.5130555555556,
		power: 5721.71498856148,
		road: 11177.3099074074,
		acceleration: 0.0400925925925932
	},
	{
		id: 942,
		time: 941,
		velocity: 17.5977777777778,
		power: 5186.48800212713,
		road: 11194.8739351852,
		acceleration: 0.00731481481481566
	},
	{
		id: 943,
		time: 942,
		velocity: 17.5922222222222,
		power: 5513.2461957362,
		road: 11212.4546759259,
		acceleration: 0.0261111111111099
	},
	{
		id: 944,
		time: 943,
		velocity: 17.5913888888889,
		power: 5199.64627574101,
		road: 11230.0518981482,
		acceleration: 0.00685185185185588
	},
	{
		id: 945,
		time: 944,
		velocity: 17.6183333333333,
		power: 4878.39579481144,
		road: 11247.6464814815,
		acceleration: -0.0121296296296336
	},
	{
		id: 946,
		time: 945,
		velocity: 17.5558333333333,
		power: 6341.82433230097,
		road: 11265.2717592593,
		acceleration: 0.0735185185185188
	},
	{
		id: 947,
		time: 946,
		velocity: 17.8119444444444,
		power: 6138.86005550723,
		road: 11282.9631944445,
		acceleration: 0.0587962962963005
	},
	{
		id: 948,
		time: 947,
		velocity: 17.7947222222222,
		power: 6556.75615731562,
		road: 11300.7243055556,
		acceleration: 0.0805555555555557
	},
	{
		id: 949,
		time: 948,
		velocity: 17.7975,
		power: 4996.23826501731,
		road: 11318.5194444445,
		acceleration: -0.0125000000000028
	},
	{
		id: 950,
		time: 949,
		velocity: 17.7744444444444,
		power: 4895.38171549666,
		road: 11336.2993981482,
		acceleration: -0.0178703703703711
	},
	{
		id: 951,
		time: 950,
		velocity: 17.7411111111111,
		power: 5566.16993046182,
		road: 11354.0811574074,
		acceleration: 0.0214814814814837
	},
	{
		id: 952,
		time: 951,
		velocity: 17.8619444444444,
		power: 5770.09922687056,
		road: 11371.8898611111,
		acceleration: 0.0324074074074048
	},
	{
		id: 953,
		time: 952,
		velocity: 17.8716666666667,
		power: 7505.34112148559,
		road: 11389.7800462963,
		acceleration: 0.13055555555556
	},
	{
		id: 954,
		time: 953,
		velocity: 18.1327777777778,
		power: 5965.92877258414,
		road: 11407.7541666667,
		acceleration: 0.0373148148148132
	},
	{
		id: 955,
		time: 954,
		velocity: 17.9738888888889,
		power: 7425.7354512,
		road: 11425.8062962963,
		acceleration: 0.118703703703702
	},
	{
		id: 956,
		time: 955,
		velocity: 18.2277777777778,
		power: 5768.51408672867,
		road: 11443.9278240741,
		acceleration: 0.0200925925925972
	},
	{
		id: 957,
		time: 956,
		velocity: 18.1930555555556,
		power: 7976.35186350754,
		road: 11462.1312037037,
		acceleration: 0.143611111111106
	},
	{
		id: 958,
		time: 957,
		velocity: 18.4047222222222,
		power: 7174.65042742404,
		road: 11480.4526388889,
		acceleration: 0.0925000000000011
	},
	{
		id: 959,
		time: 958,
		velocity: 18.5052777777778,
		power: 7410.0651869948,
		road: 11498.8711574074,
		acceleration: 0.101666666666667
	},
	{
		id: 960,
		time: 959,
		velocity: 18.4980555555556,
		power: 6099.10906321351,
		road: 11517.3529166667,
		acceleration: 0.0248148148148175
	},
	{
		id: 961,
		time: 960,
		velocity: 18.4791666666667,
		power: 4679.09093204647,
		road: 11535.8196296296,
		acceleration: -0.0549074074074092
	},
	{
		id: 962,
		time: 961,
		velocity: 18.3405555555556,
		power: 4178.62448825876,
		road: 11554.2184259259,
		acceleration: -0.080925925925925
	},
	{
		id: 963,
		time: 962,
		velocity: 18.2552777777778,
		power: 3543.568123235,
		road: 11572.5198148148,
		acceleration: -0.113888888888891
	},
	{
		id: 964,
		time: 963,
		velocity: 18.1375,
		power: 4005.01650138723,
		road: 11590.7221296296,
		acceleration: -0.0842592592592588
	},
	{
		id: 965,
		time: 964,
		velocity: 18.0877777777778,
		power: 3266.72451806933,
		road: 11608.8206018519,
		acceleration: -0.123425925925925
	},
	{
		id: 966,
		time: 965,
		velocity: 17.885,
		power: 3858.49869432769,
		road: 11626.8144444445,
		acceleration: -0.0858333333333334
	},
	{
		id: 967,
		time: 966,
		velocity: 17.88,
		power: 7232.23039230599,
		road: 11644.8201388889,
		acceleration: 0.109537037037036
	},
	{
		id: 968,
		time: 967,
		velocity: 18.4163888888889,
		power: 10027.5408743885,
		road: 11663.0115740741,
		acceleration: 0.261944444444445
	},
	{
		id: 969,
		time: 968,
		velocity: 18.6708333333333,
		power: 9897.93528765946,
		road: 11681.4547222222,
		acceleration: 0.241481481481479
	},
	{
		id: 970,
		time: 969,
		velocity: 18.6044444444444,
		power: 6982.93645562867,
		road: 11700.0535185185,
		acceleration: 0.0698148148148157
	},
	{
		id: 971,
		time: 970,
		velocity: 18.6258333333333,
		power: 6692.64095734155,
		road: 11718.7127314815,
		acceleration: 0.0510185185185179
	},
	{
		id: 972,
		time: 971,
		velocity: 18.8238888888889,
		power: 6040.23431589811,
		road: 11737.4040740741,
		acceleration: 0.0132407407407449
	},
	{
		id: 973,
		time: 972,
		velocity: 18.6441666666667,
		power: 6271.12477470902,
		road: 11756.1147222222,
		acceleration: 0.0253703703703678
	},
	{
		id: 974,
		time: 973,
		velocity: 18.7019444444444,
		power: 5049.60332533969,
		road: 11774.8168055556,
		acceleration: -0.0424999999999969
	},
	{
		id: 975,
		time: 974,
		velocity: 18.6963888888889,
		power: 6056.08551278965,
		road: 11793.5047685185,
		acceleration: 0.014259259259255
	},
	{
		id: 976,
		time: 975,
		velocity: 18.6869444444444,
		power: 6020.47469675434,
		road: 11812.2057407408,
		acceleration: 0.0117592592592608
	},
	{
		id: 977,
		time: 976,
		velocity: 18.7372222222222,
		power: 6258.86666804094,
		road: 11830.9247685185,
		acceleration: 0.0243518518518506
	},
	{
		id: 978,
		time: 977,
		velocity: 18.7694444444444,
		power: 6452.62338367424,
		road: 11849.672962963,
		acceleration: 0.0339814814814829
	},
	{
		id: 979,
		time: 978,
		velocity: 18.7888888888889,
		power: 5456.05826978216,
		road: 11868.4272685185,
		acceleration: -0.0217592592592624
	},
	{
		id: 980,
		time: 979,
		velocity: 18.6719444444444,
		power: 5312.87705910277,
		road: 11887.1562962963,
		acceleration: -0.0287962962962958
	},
	{
		id: 981,
		time: 980,
		velocity: 18.6830555555556,
		power: 4443.79342547678,
		road: 11905.8331944445,
		acceleration: -0.0754629629629626
	},
	{
		id: 982,
		time: 981,
		velocity: 18.5625,
		power: 6391.11111327665,
		road: 11924.4894907408,
		acceleration: 0.0342592592592617
	},
	{
		id: 983,
		time: 982,
		velocity: 18.7747222222222,
		power: 8450.61964624734,
		road: 11943.2356018519,
		acceleration: 0.145370370370369
	},
	{
		id: 984,
		time: 983,
		velocity: 19.1191666666667,
		power: 7591.65142973127,
		road: 11962.1005555556,
		acceleration: 0.0923148148148165
	},
	{
		id: 985,
		time: 984,
		velocity: 18.8394444444444,
		power: 3591.84269963423,
		road: 11980.9474074074,
		acceleration: -0.128518518518522
	},
	{
		id: 986,
		time: 985,
		velocity: 18.3891666666667,
		power: -176.395741177598,
		road: 11999.5637037037,
		acceleration: -0.33259259259259
	},
	{
		id: 987,
		time: 986,
		velocity: 18.1213888888889,
		power: 1131.25743302308,
		road: 12017.8876388889,
		acceleration: -0.252129629629628
	},
	{
		id: 988,
		time: 987,
		velocity: 18.0830555555556,
		power: 2897.06224318809,
		road: 12036.0127314815,
		acceleration: -0.145555555555553
	},
	{
		id: 989,
		time: 988,
		velocity: 17.9525,
		power: 4381.97237400063,
		road: 12054.0368055556,
		acceleration: -0.0564814814814838
	},
	{
		id: 990,
		time: 989,
		velocity: 17.9519444444444,
		power: 3560.64563927367,
		road: 12071.9818518519,
		acceleration: -0.101574074074076
	},
	{
		id: 991,
		time: 990,
		velocity: 17.7783333333333,
		power: 3891.11679580239,
		road: 12089.8364351852,
		acceleration: -0.0793518518518503
	},
	{
		id: 992,
		time: 991,
		velocity: 17.7144444444444,
		power: 3309.16221739011,
		road: 12107.5961111111,
		acceleration: -0.110462962962963
	},
	{
		id: 993,
		time: 992,
		velocity: 17.6205555555556,
		power: 4318.6188453727,
		road: 12125.2763888889,
		acceleration: -0.048333333333332
	},
	{
		id: 994,
		time: 993,
		velocity: 17.6333333333333,
		power: 4625.99466379565,
		road: 12142.9181018519,
		acceleration: -0.0287962962962993
	},
	{
		id: 995,
		time: 994,
		velocity: 17.6280555555556,
		power: 5066.51634259436,
		road: 12160.5443518519,
		acceleration: -0.00212962962962848
	},
	{
		id: 996,
		time: 995,
		velocity: 17.6141666666667,
		power: 5082.6820678611,
		road: 12178.1689814815,
		acceleration: -0.00111111111111128
	},
	{
		id: 997,
		time: 996,
		velocity: 17.63,
		power: 4799.01256472535,
		road: 12195.7842592593,
		acceleration: -0.0175925925925924
	},
	{
		id: 998,
		time: 997,
		velocity: 17.5752777777778,
		power: 5004.26511586346,
		road: 12213.3882407407,
		acceleration: -0.00499999999999901
	},
	{
		id: 999,
		time: 998,
		velocity: 17.5991666666667,
		power: 4645.64911360061,
		road: 12230.9768518519,
		acceleration: -0.0257407407407442
	},
	{
		id: 1000,
		time: 999,
		velocity: 17.5527777777778,
		power: 4866.30885274919,
		road: 12248.5466203704,
		acceleration: -0.0119444444444454
	},
	{
		id: 1001,
		time: 1000,
		velocity: 17.5394444444444,
		power: 5239.39478569999,
		road: 12266.1155555556,
		acceleration: 0.0102777777777803
	},
	{
		id: 1002,
		time: 1001,
		velocity: 17.63,
		power: 4420.26552853018,
		road: 12283.6706481482,
		acceleration: -0.0379629629629612
	},
	{
		id: 1003,
		time: 1002,
		velocity: 17.4388888888889,
		power: -9568.88872326452,
		road: 12300.7681018519,
		acceleration: -0.877314814814813
	},
	{
		id: 1004,
		time: 1003,
		velocity: 14.9075,
		power: -20027.8156823676,
		road: 12316.6317592593,
		acceleration: -1.59027777777778
	},
	{
		id: 1005,
		time: 1004,
		velocity: 12.8591666666667,
		power: -20438.9735631713,
		road: 12330.8256944445,
		acceleration: -1.74916666666667
	},
	{
		id: 1006,
		time: 1005,
		velocity: 12.1913888888889,
		power: -10864.0484123957,
		road: 12343.5893518519,
		acceleration: -1.11138888888889
	},
	{
		id: 1007,
		time: 1006,
		velocity: 11.5733333333333,
		power: -3378.01072585343,
		road: 12355.5451851852,
		acceleration: -0.504259259259261
	},
	{
		id: 1008,
		time: 1007,
		velocity: 11.3463888888889,
		power: -5040.77257858342,
		road: 12366.91625,
		acceleration: -0.665277777777778
	},
	{
		id: 1009,
		time: 1008,
		velocity: 10.1955555555556,
		power: -4798.89930753958,
		road: 12377.6235185185,
		acceleration: -0.662314814814815
	},
	{
		id: 1010,
		time: 1009,
		velocity: 9.58638888888889,
		power: -4183.20896486237,
		road: 12387.6892592593,
		acceleration: -0.620740740740739
	},
	{
		id: 1011,
		time: 1010,
		velocity: 9.48416666666667,
		power: -603.288862640874,
		road: 12397.3218981482,
		acceleration: -0.245462962962963
	},
	{
		id: 1012,
		time: 1011,
		velocity: 9.45916666666667,
		power: 4870.49770474665,
		road: 12407.0056481482,
		acceleration: 0.347685185185183
	},
	{
		id: 1013,
		time: 1012,
		velocity: 10.6294444444444,
		power: 7505.78320487407,
		road: 12417.1583796296,
		acceleration: 0.590277777777777
	},
	{
		id: 1014,
		time: 1013,
		velocity: 11.255,
		power: 10577.866577224,
		road: 12428.0200462963,
		acceleration: 0.827592592592593
	},
	{
		id: 1015,
		time: 1014,
		velocity: 11.9419444444444,
		power: 6890.22489130846,
		road: 12439.5090277778,
		acceleration: 0.427037037037039
	},
	{
		id: 1016,
		time: 1015,
		velocity: 11.9105555555556,
		power: 4558.1849780496,
		road: 12451.3110185185,
		acceleration: 0.198981481481482
	},
	{
		id: 1017,
		time: 1016,
		velocity: 11.8519444444444,
		power: 1053.90721931972,
		road: 12463.1558796296,
		acceleration: -0.113240740740743
	},
	{
		id: 1018,
		time: 1017,
		velocity: 11.6022222222222,
		power: 2164.55019606879,
		road: 12474.9375925926,
		acceleration: -0.0130555555555549
	},
	{
		id: 1019,
		time: 1018,
		velocity: 11.8713888888889,
		power: 3203.16599032461,
		road: 12486.7518518519,
		acceleration: 0.0781481481481485
	},
	{
		id: 1020,
		time: 1019,
		velocity: 12.0863888888889,
		power: 5217.21483007141,
		road: 12498.7293518519,
		acceleration: 0.248333333333333
	},
	{
		id: 1021,
		time: 1020,
		velocity: 12.3472222222222,
		power: 6066.45846236111,
		road: 12510.9843981482,
		acceleration: 0.306759259259259
	},
	{
		id: 1022,
		time: 1021,
		velocity: 12.7916666666667,
		power: 7093.46410940391,
		road: 12523.5794907408,
		acceleration: 0.373333333333333
	},
	{
		id: 1023,
		time: 1022,
		velocity: 13.2063888888889,
		power: 7745.52256846228,
		road: 12536.5625462963,
		acceleration: 0.402592592592592
	},
	{
		id: 1024,
		time: 1023,
		velocity: 13.555,
		power: 5086.08568564834,
		road: 12549.8342592593,
		acceleration: 0.174722222222222
	},
	{
		id: 1025,
		time: 1024,
		velocity: 13.3158333333333,
		power: 2212.57562570682,
		road: 12563.1664351852,
		acceleration: -0.0537962962962961
	},
	{
		id: 1026,
		time: 1025,
		velocity: 13.045,
		power: -1845.30178765156,
		road: 12576.285787037,
		acceleration: -0.371851851851853
	},
	{
		id: 1027,
		time: 1026,
		velocity: 12.4394444444444,
		power: -847.235925510957,
		road: 12589.0746759259,
		acceleration: -0.289074074074072
	},
	{
		id: 1028,
		time: 1027,
		velocity: 12.4486111111111,
		power: 2367.72413435666,
		road: 12601.7085185185,
		acceleration: -0.0210185185185185
	},
	{
		id: 1029,
		time: 1028,
		velocity: 12.9819444444444,
		power: 5730.75242094634,
		road: 12614.4578240741,
		acceleration: 0.251944444444444
	},
	{
		id: 1030,
		time: 1029,
		velocity: 13.1952777777778,
		power: 6892.69318608722,
		road: 12627.4983333333,
		acceleration: 0.330462962962965
	},
	{
		id: 1031,
		time: 1030,
		velocity: 13.44,
		power: 4893.04158406444,
		road: 12640.7835185185,
		acceleration: 0.158888888888885
	},
	{
		id: 1032,
		time: 1031,
		velocity: 13.4586111111111,
		power: 4188.88266135774,
		road: 12654.1972685185,
		acceleration: 0.0982407407407422
	},
	{
		id: 1033,
		time: 1032,
		velocity: 13.49,
		power: 3222.32892286602,
		road: 12667.6705092593,
		acceleration: 0.0207407407407416
	},
	{
		id: 1034,
		time: 1033,
		velocity: 13.5022222222222,
		power: 3147.70663751165,
		road: 12681.1612962963,
		acceleration: 0.0143518518518526
	},
	{
		id: 1035,
		time: 1034,
		velocity: 13.5016666666667,
		power: 3070.16375406546,
		road: 12694.6632407408,
		acceleration: 0.00796296296296006
	},
	{
		id: 1036,
		time: 1035,
		velocity: 13.5138888888889,
		power: 3063.84095897614,
		road: 12708.1727777778,
		acceleration: 0.00722222222222335
	},
	{
		id: 1037,
		time: 1036,
		velocity: 13.5238888888889,
		power: 3096.06334088249,
		road: 12721.6906481482,
		acceleration: 0.00944444444444592
	},
	{
		id: 1038,
		time: 1037,
		velocity: 13.53,
		power: 4201.22107036515,
		road: 12735.2597685185,
		acceleration: 0.093055555555555
	},
	{
		id: 1039,
		time: 1038,
		velocity: 13.7930555555556,
		power: 8645.07980623321,
		road: 12749.0852314815,
		acceleration: 0.419629629629631
	},
	{
		id: 1040,
		time: 1039,
		velocity: 14.7827777777778,
		power: 16455.9542870144,
		road: 12763.5912037037,
		acceleration: 0.941388888888888
	},
	{
		id: 1041,
		time: 1040,
		velocity: 16.3541666666667,
		power: 14945.5168834907,
		road: 12778.9468518519,
		acceleration: 0.757962962962965
	},
	{
		id: 1042,
		time: 1041,
		velocity: 16.0669444444444,
		power: 7297.02110581288,
		road: 12794.787962963,
		acceleration: 0.212962962962962
	},
	{
		id: 1043,
		time: 1042,
		velocity: 15.4216666666667,
		power: -2560.57447500929,
		road: 12810.5169444445,
		acceleration: -0.437222222222225
	},
	{
		id: 1044,
		time: 1043,
		velocity: 15.0425,
		power: -4033.76373813428,
		road: 12825.7596296296,
		acceleration: -0.535370370370369
	},
	{
		id: 1045,
		time: 1044,
		velocity: 14.4608333333333,
		power: -3409.05205484208,
		road: 12840.4886111111,
		acceleration: -0.492037037037036
	},
	{
		id: 1046,
		time: 1045,
		velocity: 13.9455555555556,
		power: -2846.67947702103,
		road: 12854.7460185185,
		acceleration: -0.451111111111111
	},
	{
		id: 1047,
		time: 1046,
		velocity: 13.6891666666667,
		power: -1488.96682558479,
		road: 12868.60375,
		acceleration: -0.348240740740742
	},
	{
		id: 1048,
		time: 1047,
		velocity: 13.4161111111111,
		power: -1139.1857357337,
		road: 12882.127962963,
		acceleration: -0.318796296296297
	},
	{
		id: 1049,
		time: 1048,
		velocity: 12.9891666666667,
		power: -332.874863907663,
		road: 12895.3664814815,
		acceleration: -0.252592592592592
	},
	{
		id: 1050,
		time: 1049,
		velocity: 12.9313888888889,
		power: -81.5529738203346,
		road: 12908.3640740741,
		acceleration: -0.229259259259258
	},
	{
		id: 1051,
		time: 1050,
		velocity: 12.7283333333333,
		power: 344.559940373928,
		road: 12921.1513425926,
		acceleration: -0.191388888888889
	},
	{
		id: 1052,
		time: 1051,
		velocity: 12.415,
		power: -837.966471368519,
		road: 12933.6998148148,
		acceleration: -0.286203703703704
	},
	{
		id: 1053,
		time: 1052,
		velocity: 12.0727777777778,
		power: -556.446005819077,
		road: 12945.9752314815,
		acceleration: -0.259907407407406
	},
	{
		id: 1054,
		time: 1053,
		velocity: 11.9486111111111,
		power: 577.293446254527,
		road: 12958.0410185185,
		acceleration: -0.159351851851852
	},
	{
		id: 1055,
		time: 1054,
		velocity: 11.9369444444444,
		power: 1181.99822736458,
		road: 12969.9751851852,
		acceleration: -0.103888888888889
	},
	{
		id: 1056,
		time: 1055,
		velocity: 11.7611111111111,
		power: 774.097138981538,
		road: 12981.7887037037,
		acceleration: -0.137407407407409
	},
	{
		id: 1057,
		time: 1056,
		velocity: 11.5363888888889,
		power: 864.501115213718,
		road: 12993.4701388889,
		acceleration: -0.126759259259259
	},
	{
		id: 1058,
		time: 1057,
		velocity: 11.5566666666667,
		power: 3141.34199630397,
		road: 13005.1274537037,
		acceleration: 0.0785185185185178
	},
	{
		id: 1059,
		time: 1058,
		velocity: 11.9966666666667,
		power: 4145.98539000897,
		road: 13016.9057407407,
		acceleration: 0.163425925925926
	},
	{
		id: 1060,
		time: 1059,
		velocity: 12.0266666666667,
		power: 5295.03611419492,
		road: 13028.8930555556,
		acceleration: 0.254629629629632
	},
	{
		id: 1061,
		time: 1060,
		velocity: 12.3205555555556,
		power: 3391.71599170246,
		road: 13041.0485648148,
		acceleration: 0.0817592592592575
	},
	{
		id: 1062,
		time: 1061,
		velocity: 12.2419444444444,
		power: 2358.90468655087,
		road: 13053.2407407407,
		acceleration: -0.00842592592592695
	},
	{
		id: 1063,
		time: 1062,
		velocity: 12.0013888888889,
		power: 816.054840506368,
		road: 13065.3588888889,
		acceleration: -0.13962962962963
	},
	{
		id: 1064,
		time: 1063,
		velocity: 11.9016666666667,
		power: 1177.46198712243,
		road: 13077.3543981482,
		acceleration: -0.105648148148148
	},
	{
		id: 1065,
		time: 1064,
		velocity: 11.925,
		power: 2245.53944539831,
		road: 13089.2918055556,
		acceleration: -0.0105555555555554
	},
	{
		id: 1066,
		time: 1065,
		velocity: 11.9697222222222,
		power: 1709.42571978656,
		road: 13101.1955555556,
		acceleration: -0.056759259259259
	},
	{
		id: 1067,
		time: 1066,
		velocity: 11.7313888888889,
		power: 662.136241723742,
		road: 13112.9973611111,
		acceleration: -0.14712962962963
	},
	{
		id: 1068,
		time: 1067,
		velocity: 11.4836111111111,
		power: -925.300230288988,
		road: 13124.5821759259,
		acceleration: -0.286851851851852
	},
	{
		id: 1069,
		time: 1068,
		velocity: 11.1091666666667,
		power: -1074.0518431208,
		road: 13135.8740277778,
		acceleration: -0.299074074074072
	},
	{
		id: 1070,
		time: 1069,
		velocity: 10.8341666666667,
		power: -1335.26358127131,
		road: 13146.8548611111,
		acceleration: -0.322962962962965
	},
	{
		id: 1071,
		time: 1070,
		velocity: 10.5147222222222,
		power: -266.021724732728,
		road: 13157.5651388889,
		acceleration: -0.218148148148147
	},
	{
		id: 1072,
		time: 1071,
		velocity: 10.4547222222222,
		power: -1016.05058198348,
		road: 13168.0208333333,
		acceleration: -0.291018518518518
	},
	{
		id: 1073,
		time: 1072,
		velocity: 9.96111111111111,
		power: 5.10443387203875,
		road: 13178.2381481482,
		acceleration: -0.185740740740741
	},
	{
		id: 1074,
		time: 1073,
		velocity: 9.9575,
		power: 381.552335894609,
		road: 13188.2903240741,
		acceleration: -0.144537037037036
	},
	{
		id: 1075,
		time: 1074,
		velocity: 10.0211111111111,
		power: 2159.05829062137,
		road: 13198.2916203704,
		acceleration: 0.0427777777777774
	},
	{
		id: 1076,
		time: 1075,
		velocity: 10.0894444444444,
		power: 1400.9860852666,
		road: 13208.2958796296,
		acceleration: -0.0368518518518517
	},
	{
		id: 1077,
		time: 1076,
		velocity: 9.84694444444444,
		power: 335.956311157449,
		road: 13218.2081018519,
		acceleration: -0.147222222222222
	},
	{
		id: 1078,
		time: 1077,
		velocity: 9.57944444444444,
		power: 179.615152770995,
		road: 13227.9658333333,
		acceleration: -0.161759259259259
	},
	{
		id: 1079,
		time: 1078,
		velocity: 9.60416666666667,
		power: -2503.52190350834,
		road: 13237.4147222222,
		acceleration: -0.455925925925927
	},
	{
		id: 1080,
		time: 1079,
		velocity: 8.47916666666667,
		power: -2462.74513450243,
		road: 13246.4053240741,
		acceleration: -0.460648148148147
	},
	{
		id: 1081,
		time: 1080,
		velocity: 8.1975,
		power: -3145.79161502905,
		road: 13254.8868055556,
		acceleration: -0.557592592592595
	},
	{
		id: 1082,
		time: 1081,
		velocity: 7.93138888888889,
		power: 8.04263501011783,
		road: 13263.0077314815,
		acceleration: -0.163518518518517
	},
	{
		id: 1083,
		time: 1082,
		velocity: 7.98861111111111,
		power: 633.857233317896,
		road: 13271.0068055556,
		acceleration: -0.0801851851851856
	},
	{
		id: 1084,
		time: 1083,
		velocity: 7.95694444444444,
		power: 411.753482629897,
		road: 13278.9118518519,
		acceleration: -0.107870370370369
	},
	{
		id: 1085,
		time: 1084,
		velocity: 7.60777777777778,
		power: -666.317108097719,
		road: 13286.6371296296,
		acceleration: -0.251666666666668
	},
	{
		id: 1086,
		time: 1085,
		velocity: 7.23361111111111,
		power: -356.772653186333,
		road: 13294.132037037,
		acceleration: -0.209074074074074
	},
	{
		id: 1087,
		time: 1086,
		velocity: 7.32972222222222,
		power: 771.519313734913,
		road: 13301.4984722222,
		acceleration: -0.0478703703703696
	},
	{
		id: 1088,
		time: 1087,
		velocity: 7.46416666666667,
		power: 1964.36348603574,
		road: 13308.9012962963,
		acceleration: 0.120648148148147
	},
	{
		id: 1089,
		time: 1088,
		velocity: 7.59555555555556,
		power: 2030.24146503281,
		road: 13316.4265740741,
		acceleration: 0.12425925925926
	},
	{
		id: 1090,
		time: 1089,
		velocity: 7.7025,
		power: 1284.90225460968,
		road: 13324.0229166667,
		acceleration: 0.0178703703703711
	},
	{
		id: 1091,
		time: 1090,
		velocity: 7.51777777777778,
		power: -72.2260913094601,
		road: 13331.5435185185,
		acceleration: -0.169351851851852
	},
	{
		id: 1092,
		time: 1091,
		velocity: 7.0875,
		power: -1529.05454224176,
		road: 13338.7900462963,
		acceleration: -0.378796296296297
	},
	{
		id: 1093,
		time: 1092,
		velocity: 6.56611111111111,
		power: -1342.94254890657,
		road: 13345.6675,
		acceleration: -0.359351851851851
	},
	{
		id: 1094,
		time: 1093,
		velocity: 6.43972222222222,
		power: 369.426137540699,
		road: 13352.3183333333,
		acceleration: -0.0938888888888885
	},
	{
		id: 1095,
		time: 1094,
		velocity: 6.80583333333333,
		power: 1486.49154782305,
		road: 13358.9637037037,
		acceleration: 0.0829629629629629
	},
	{
		id: 1096,
		time: 1095,
		velocity: 6.815,
		power: 2112.89873913655,
		road: 13365.7379166667,
		acceleration: 0.174722222222222
	},
	{
		id: 1097,
		time: 1096,
		velocity: 6.96388888888889,
		power: 1810.01879684587,
		road: 13372.6597685185,
		acceleration: 0.120555555555556
	},
	{
		id: 1098,
		time: 1097,
		velocity: 7.1675,
		power: 1778.87633441905,
		road: 13379.6971296296,
		acceleration: 0.110462962962963
	},
	{
		id: 1099,
		time: 1098,
		velocity: 7.14638888888889,
		power: 1402.50800469769,
		road: 13386.8153240741,
		acceleration: 0.0512037037037043
	},
	{
		id: 1100,
		time: 1099,
		velocity: 7.1175,
		power: 1213.2493693403,
		road: 13393.9701388889,
		acceleration: 0.0220370370370357
	},
	{
		id: 1101,
		time: 1100,
		velocity: 7.23361111111111,
		power: 3215.19769515337,
		road: 13401.2881018519,
		acceleration: 0.30425925925926
	},
	{
		id: 1102,
		time: 1101,
		velocity: 8.05916666666667,
		power: 5954.9224142281,
		road: 13409.0789814815,
		acceleration: 0.641574074074073
	},
	{
		id: 1103,
		time: 1102,
		velocity: 9.04222222222222,
		power: 7610.74608999909,
		road: 13417.5769444444,
		acceleration: 0.772592592592591
	},
	{
		id: 1104,
		time: 1103,
		velocity: 9.55138888888889,
		power: 6445.788607897,
		road: 13426.7430092593,
		acceleration: 0.563611111111111
	},
	{
		id: 1105,
		time: 1104,
		velocity: 9.75,
		power: 4045.48597643757,
		road: 13436.322962963,
		acceleration: 0.264166666666668
	},
	{
		id: 1106,
		time: 1105,
		velocity: 9.83472222222222,
		power: 3236.65356507554,
		road: 13446.1176851852,
		acceleration: 0.16537037037037
	},
	{
		id: 1107,
		time: 1106,
		velocity: 10.0475,
		power: 2186.3581351921,
		road: 13456.0196296296,
		acceleration: 0.049074074074074
	},
	{
		id: 1108,
		time: 1107,
		velocity: 9.89722222222222,
		power: 1827.20142232205,
		road: 13465.9511574074,
		acceleration: 0.0100925925925921
	},
	{
		id: 1109,
		time: 1108,
		velocity: 9.865,
		power: 631.970283988765,
		road: 13475.8300925926,
		acceleration: -0.115277777777777
	},
	{
		id: 1110,
		time: 1109,
		velocity: 9.70166666666667,
		power: 1659.55724596771,
		road: 13485.649212963,
		acceleration: -0.00435185185185105
	},
	{
		id: 1111,
		time: 1110,
		velocity: 9.88416666666667,
		power: 2478.02972580611,
		road: 13495.5069907407,
		acceleration: 0.0816666666666652
	},
	{
		id: 1112,
		time: 1111,
		velocity: 10.11,
		power: 4351.9654994935,
		road: 13505.5411111111,
		acceleration: 0.271018518518519
	},
	{
		id: 1113,
		time: 1112,
		velocity: 10.5147222222222,
		power: 5707.20348075193,
		road: 13515.9056481482,
		acceleration: 0.389814814814816
	},
	{
		id: 1114,
		time: 1113,
		velocity: 11.0536111111111,
		power: 6024.7663691599,
		road: 13526.6625462963,
		acceleration: 0.394907407407409
	},
	{
		id: 1115,
		time: 1114,
		velocity: 11.2947222222222,
		power: 6744.5401396363,
		road: 13537.8345833333,
		acceleration: 0.43537037037037
	},
	{
		id: 1116,
		time: 1115,
		velocity: 11.8208333333333,
		power: 5967.70958421193,
		road: 13549.3936111111,
		acceleration: 0.33861111111111
	},
	{
		id: 1117,
		time: 1116,
		velocity: 12.0694444444444,
		power: 4433.13657701908,
		road: 13561.2154166667,
		acceleration: 0.186944444444446
	},
	{
		id: 1118,
		time: 1117,
		velocity: 11.8555555555556,
		power: 1602.36532298156,
		road: 13573.0978703704,
		acceleration: -0.0656481481481492
	},
	{
		id: 1119,
		time: 1118,
		velocity: 11.6238888888889,
		power: 1655.43899829797,
		road: 13584.9178240741,
		acceleration: -0.0593518518518543
	},
	{
		id: 1120,
		time: 1119,
		velocity: 11.8913888888889,
		power: 1482.6162925984,
		road: 13596.6715740741,
		acceleration: -0.0730555555555554
	},
	{
		id: 1121,
		time: 1120,
		velocity: 11.6363888888889,
		power: 1847.3696482945,
		road: 13608.3693055556,
		acceleration: -0.0389814814814802
	},
	{
		id: 1122,
		time: 1121,
		velocity: 11.5069444444444,
		power: -470.072537000099,
		road: 13619.9248611111,
		acceleration: -0.245370370370372
	},
	{
		id: 1123,
		time: 1122,
		velocity: 11.1552777777778,
		power: 256.251464903179,
		road: 13631.2695833333,
		acceleration: -0.176296296296295
	},
	{
		id: 1124,
		time: 1123,
		velocity: 11.1075,
		power: 866.665408808821,
		road: 13642.4676851852,
		acceleration: -0.116944444444444
	},
	{
		id: 1125,
		time: 1124,
		velocity: 11.1561111111111,
		power: 1564.3635734467,
		road: 13653.5825925926,
		acceleration: -0.0494444444444451
	},
	{
		id: 1126,
		time: 1125,
		velocity: 11.0069444444444,
		power: 825.853160825079,
		road: 13664.6140277778,
		acceleration: -0.1175
	},
	{
		id: 1127,
		time: 1126,
		velocity: 10.755,
		power: -1307.38079239891,
		road: 13675.4266203704,
		acceleration: -0.320185185185183
	},
	{
		id: 1128,
		time: 1127,
		velocity: 10.1955555555556,
		power: -2008.84980775523,
		road: 13685.8837962963,
		acceleration: -0.39064814814815
	},
	{
		id: 1129,
		time: 1128,
		velocity: 9.835,
		power: -1883.04286078364,
		road: 13695.9552314815,
		acceleration: -0.380833333333333
	},
	{
		id: 1130,
		time: 1129,
		velocity: 9.6125,
		power: -1755.3642269308,
		road: 13705.6510185185,
		acceleration: -0.370462962962963
	},
	{
		id: 1131,
		time: 1130,
		velocity: 9.08416666666667,
		power: -176.563218116666,
		road: 13715.0630555556,
		acceleration: -0.197037037037036
	},
	{
		id: 1132,
		time: 1131,
		velocity: 9.24388888888889,
		power: 1027.11823149071,
		road: 13724.3466666667,
		acceleration: -0.0598148148148141
	},
	{
		id: 1133,
		time: 1132,
		velocity: 9.43305555555556,
		power: 2760.98633708357,
		road: 13733.6676851852,
		acceleration: 0.134629629629629
	},
	{
		id: 1134,
		time: 1133,
		velocity: 9.48805555555555,
		power: 4007.47484562824,
		road: 13743.1877314815,
		acceleration: 0.263425925925924
	},
	{
		id: 1135,
		time: 1134,
		velocity: 10.0341666666667,
		power: 4361.66493954473,
		road: 13752.9824537037,
		acceleration: 0.285925925925925
	},
	{
		id: 1136,
		time: 1135,
		velocity: 10.2908333333333,
		power: 5289.62901169055,
		road: 13763.1018518519,
		acceleration: 0.363425925925927
	},
	{
		id: 1137,
		time: 1136,
		velocity: 10.5783333333333,
		power: 5458.02781360468,
		road: 13773.5815277778,
		acceleration: 0.357129629629629
	},
	{
		id: 1138,
		time: 1137,
		velocity: 11.1055555555556,
		power: 10051.1543638843,
		road: 13784.6192592593,
		acceleration: 0.758981481481483
	},
	{
		id: 1139,
		time: 1138,
		velocity: 12.5677777777778,
		power: 11744.1088374117,
		road: 13796.4534722222,
		acceleration: 0.833981481481482
	},
	{
		id: 1140,
		time: 1139,
		velocity: 13.0802777777778,
		power: 14242.5258017083,
		road: 13809.1815277778,
		acceleration: 0.953703703703702
	},
	{
		id: 1141,
		time: 1140,
		velocity: 13.9666666666667,
		power: 12543.5304349962,
		road: 13822.7547222222,
		acceleration: 0.736574074074074
	},
	{
		id: 1142,
		time: 1141,
		velocity: 14.7775,
		power: 12659.4921759561,
		road: 13837.0388888889,
		acceleration: 0.685370370370372
	},
	{
		id: 1143,
		time: 1142,
		velocity: 15.1363888888889,
		power: 6575.36154815604,
		road: 13851.7743518519,
		acceleration: 0.217222222222221
	},
	{
		id: 1144,
		time: 1143,
		velocity: 14.6183333333333,
		power: 1070.73134497917,
		road: 13866.5312962963,
		acceleration: -0.174259259259259
	},
	{
		id: 1145,
		time: 1144,
		velocity: 14.2547222222222,
		power: -1906.99891689529,
		road: 13881.0093518519,
		acceleration: -0.383518518518519
	},
	{
		id: 1146,
		time: 1145,
		velocity: 13.9858333333333,
		power: -984.504278315364,
		road: 13895.1391666667,
		acceleration: -0.312962962962963
	},
	{
		id: 1147,
		time: 1146,
		velocity: 13.6794444444444,
		power: -1242.25794196989,
		road: 13908.9479166667,
		acceleration: -0.329166666666666
	},
	{
		id: 1148,
		time: 1147,
		velocity: 13.2672222222222,
		power: -1511.85291578823,
		road: 13922.4184259259,
		acceleration: -0.347314814814816
	},
	{
		id: 1149,
		time: 1148,
		velocity: 12.9438888888889,
		power: -2405.26151556087,
		road: 13935.5069907407,
		acceleration: -0.416574074074072
	},
	{
		id: 1150,
		time: 1149,
		velocity: 12.4297222222222,
		power: -3028.71739265042,
		road: 13948.152962963,
		acceleration: -0.468611111111111
	},
	{
		id: 1151,
		time: 1150,
		velocity: 11.8613888888889,
		power: -3199.63376165059,
		road: 13960.3213425926,
		acceleration: -0.486574074074076
	},
	{
		id: 1152,
		time: 1151,
		velocity: 11.4841666666667,
		power: -4380.80194857438,
		road: 13971.947037037,
		acceleration: -0.598796296296294
	},
	{
		id: 1153,
		time: 1152,
		velocity: 10.6333333333333,
		power: -4546.22594666063,
		road: 13982.9588888889,
		acceleration: -0.628888888888891
	},
	{
		id: 1154,
		time: 1153,
		velocity: 9.97472222222222,
		power: -8324.89132493016,
		road: 13993.1340277778,
		acceleration: -1.04453703703703
	},
	{
		id: 1155,
		time: 1154,
		velocity: 8.35055555555556,
		power: -9495.11589500242,
		road: 14002.147037037,
		acceleration: -1.27972222222222
	},
	{
		id: 1156,
		time: 1155,
		velocity: 6.79416666666667,
		power: -10017.3321587876,
		road: 14009.7474537037,
		acceleration: -1.54546296296296
	},
	{
		id: 1157,
		time: 1156,
		velocity: 5.33833333333333,
		power: -7522.10740880747,
		road: 14015.8528240741,
		acceleration: -1.44462962962963
	},
	{
		id: 1158,
		time: 1157,
		velocity: 4.01666666666667,
		power: -4659.83419673283,
		road: 14020.6548611111,
		acceleration: -1.16203703703704
	},
	{
		id: 1159,
		time: 1158,
		velocity: 3.30805555555556,
		power: -3202.42739506716,
		road: 14024.3520833333,
		acceleration: -1.04759259259259
	},
	{
		id: 1160,
		time: 1159,
		velocity: 2.19555555555556,
		power: -2019.85288308286,
		road: 14027.0677314815,
		acceleration: -0.915555555555556
	},
	{
		id: 1161,
		time: 1160,
		velocity: 1.27,
		power: -1575.30738405823,
		road: 14028.7742592593,
		acceleration: -1.10268518518519
	},
	{
		id: 1162,
		time: 1161,
		velocity: 0,
		power: -451.786325186794,
		road: 14029.5635185185,
		acceleration: -0.731851851851852
	},
	{
		id: 1163,
		time: 1162,
		velocity: 0,
		power: -52.4222676259608,
		road: 14029.8115277778,
		acceleration: -0.350648148148148
	},
	{
		id: 1164,
		time: 1163,
		velocity: 0.218055555555556,
		power: 8.78187951802384,
		road: 14029.884212963,
		acceleration: 0
	},
	{
		id: 1165,
		time: 1164,
		velocity: 0,
		power: 8.78187951802384,
		road: 14029.9568981482,
		acceleration: 0
	},
	{
		id: 1166,
		time: 1165,
		velocity: 0,
		power: 1.88829735217674,
		road: 14029.9932407408,
		acceleration: -0.0726851851851852
	},
	{
		id: 1167,
		time: 1166,
		velocity: 0,
		power: 2.00348010937878,
		road: 14030.0068981482,
		acceleration: 0.0273148148148148
	},
	{
		id: 1168,
		time: 1167,
		velocity: 0.0819444444444444,
		power: 6.81227081372888,
		road: 14030.0513425926,
		acceleration: 0.0342592592592593
	},
	{
		id: 1169,
		time: 1168,
		velocity: 0.102777777777778,
		power: 7.43937673306343,
		road: 14030.1129166667,
		acceleration: 0
	},
	{
		id: 1170,
		time: 1169,
		velocity: 0,
		power: 8.65049770756161,
		road: 14030.1793981482,
		acceleration: 0.00981481481481481
	},
	{
		id: 1171,
		time: 1170,
		velocity: 0.111388888888889,
		power: 4.79448394780842,
		road: 14030.2336574074,
		acceleration: -0.0342592592592593
	},
	{
		id: 1172,
		time: 1171,
		velocity: 0,
		power: 4.48595147226329,
		road: 14030.270787037,
		acceleration: 0
	},
	{
		id: 1173,
		time: 1172,
		velocity: 0,
		power: 1.58993633853151,
		road: 14030.2893518519,
		acceleration: -0.0371296296296296
	},
	{
		id: 1174,
		time: 1173,
		velocity: 0,
		power: 0,
		road: 14030.2893518519,
		acceleration: 0
	},
	{
		id: 1175,
		time: 1174,
		velocity: 0,
		power: 0,
		road: 14030.2893518519,
		acceleration: 0
	},
	{
		id: 1176,
		time: 1175,
		velocity: 0,
		power: 0,
		road: 14030.2893518519,
		acceleration: 0
	},
	{
		id: 1177,
		time: 1176,
		velocity: 0,
		power: 0,
		road: 14030.2893518519,
		acceleration: 0
	},
	{
		id: 1178,
		time: 1177,
		velocity: 0,
		power: 68.5877563330761,
		road: 14030.4503703704,
		acceleration: 0.322037037037037
	},
	{
		id: 1179,
		time: 1178,
		velocity: 0.966111111111111,
		power: 1586.32324267682,
		road: 14031.5030555556,
		acceleration: 1.4612962962963
	},
	{
		id: 1180,
		time: 1179,
		velocity: 4.38388888888889,
		power: 5938.85890046272,
		road: 14034.3279166667,
		acceleration: 2.08305555555556
	},
	{
		id: 1181,
		time: 1180,
		velocity: 6.24916666666667,
		power: 10828.3097469751,
		road: 14039.2752777778,
		acceleration: 2.16194444444444
	},
	{
		id: 1182,
		time: 1181,
		velocity: 7.45194444444444,
		power: 11773.9143032453,
		road: 14046.129537037,
		acceleration: 1.65185185185185
	},
	{
		id: 1183,
		time: 1182,
		velocity: 9.33944444444444,
		power: 18915.9262107786,
		road: 14054.8618055556,
		acceleration: 2.10416666666667
	},
	{
		id: 1184,
		time: 1183,
		velocity: 12.5616666666667,
		power: 17997.8080380903,
		road: 14065.4430092593,
		acceleration: 1.5937037037037
	},
	{
		id: 1185,
		time: 1184,
		velocity: 12.2330555555556,
		power: 9128.33880000883,
		road: 14077.1283796296,
		acceleration: 0.614629629629631
	},
	{
		id: 1186,
		time: 1185,
		velocity: 11.1833333333333,
		power: -8697.0603415906,
		road: 14088.6233333333,
		acceleration: -0.995462962962966
	},
	{
		id: 1187,
		time: 1186,
		velocity: 9.57527777777778,
		power: -10517.4736817689,
		road: 14098.9943518519,
		acceleration: -1.25240740740741
	},
	{
		id: 1188,
		time: 1187,
		velocity: 8.47583333333333,
		power: -8007.44187385874,
		road: 14108.1945833333,
		acceleration: -1.08916666666667
	},
	{
		id: 1189,
		time: 1188,
		velocity: 7.91583333333333,
		power: -1961.3061010606,
		road: 14116.6444907407,
		acceleration: -0.411481481481481
	},
	{
		id: 1190,
		time: 1189,
		velocity: 8.34083333333333,
		power: -964.878089973383,
		road: 14124.7438888889,
		acceleration: -0.289537037037038
	},
	{
		id: 1191,
		time: 1190,
		velocity: 7.60722222222222,
		power: -1082.3844594342,
		road: 14132.5447685185,
		acceleration: -0.3075
	},
	{
		id: 1192,
		time: 1191,
		velocity: 6.99333333333333,
		power: -2191.13974531095,
		road: 14139.9573611111,
		acceleration: -0.469074074074074
	},
	{
		id: 1193,
		time: 1192,
		velocity: 6.93361111111111,
		power: -180.158628479221,
		road: 14147.044212963,
		acceleration: -0.182407407407407
	},
	{
		id: 1194,
		time: 1193,
		velocity: 7.06,
		power: 1621.94281069468,
		road: 14154.0833333333,
		acceleration: 0.0869444444444447
	},
	{
		id: 1195,
		time: 1194,
		velocity: 7.25416666666667,
		power: 3905.77160733849,
		road: 14161.3690277778,
		acceleration: 0.406203703703703
	},
	{
		id: 1196,
		time: 1195,
		velocity: 8.15222222222222,
		power: 4607.30385063737,
		road: 14169.0908333333,
		acceleration: 0.466018518518518
	},
	{
		id: 1197,
		time: 1196,
		velocity: 8.45805555555556,
		power: 5758.65896374724,
		road: 14177.3299537037,
		acceleration: 0.568611111111112
	},
	{
		id: 1198,
		time: 1197,
		velocity: 8.96,
		power: 6849.86606407679,
		road: 14186.1743055556,
		acceleration: 0.64185185185185
	},
	{
		id: 1199,
		time: 1198,
		velocity: 10.0777777777778,
		power: 8990.38851781462,
		road: 14195.7431944445,
		acceleration: 0.807222222222222
	},
	{
		id: 1200,
		time: 1199,
		velocity: 10.8797222222222,
		power: 8209.34128080882,
		road: 14206.0403703704,
		acceleration: 0.649351851851852
	},
	{
		id: 1201,
		time: 1200,
		velocity: 10.9080555555556,
		power: 4862.98000894238,
		road: 14216.8028703704,
		acceleration: 0.281296296296297
	},
	{
		id: 1202,
		time: 1201,
		velocity: 10.9216666666667,
		power: 1770.1369100283,
		road: 14227.6941203704,
		acceleration: -0.0237962962962968
	},
	{
		id: 1203,
		time: 1202,
		velocity: 10.8083333333333,
		power: -888.663801081688,
		road: 14238.4338425926,
		acceleration: -0.279259259259261
	},
	{
		id: 1204,
		time: 1203,
		velocity: 10.0702777777778,
		power: -3220.04142939626,
		road: 14248.776712963,
		acceleration: -0.514444444444443
	},
	{
		id: 1205,
		time: 1204,
		velocity: 9.37833333333333,
		power: -4773.27192869259,
		road: 14258.5146296296,
		acceleration: -0.695462962962964
	},
	{
		id: 1206,
		time: 1205,
		velocity: 8.72194444444444,
		power: -5280.37979299293,
		road: 14267.5100462963,
		acceleration: -0.789537037037036
	},
	{
		id: 1207,
		time: 1206,
		velocity: 7.70166666666667,
		power: -4800.52382482745,
		road: 14275.7208333333,
		acceleration: -0.779722222222222
	},
	{
		id: 1208,
		time: 1207,
		velocity: 7.03916666666667,
		power: -2879.59804573535,
		road: 14283.2613425926,
		acceleration: -0.560833333333334
	},
	{
		id: 1209,
		time: 1208,
		velocity: 7.03944444444444,
		power: 248.079836517788,
		road: 14290.46125,
		acceleration: -0.12037037037037
	},
	{
		id: 1210,
		time: 1209,
		velocity: 7.34055555555555,
		power: 936.511899577037,
		road: 14297.5919907407,
		acceleration: -0.0179629629629625
	},
	{
		id: 1211,
		time: 1210,
		velocity: 6.98527777777778,
		power: -2033.85041049149,
		road: 14304.4814814815,
		acceleration: -0.464537037037038
	},
	{
		id: 1212,
		time: 1211,
		velocity: 5.64583333333333,
		power: -4624.42326147959,
		road: 14310.67125,
		acceleration: -0.934907407407406
	},
	{
		id: 1213,
		time: 1212,
		velocity: 4.53583333333333,
		power: -6215.66191074789,
		road: 14315.6680555556,
		acceleration: -1.45101851851852
	},
	{
		id: 1214,
		time: 1213,
		velocity: 2.63222222222222,
		power: -4029.61755788361,
		road: 14319.2850925926,
		acceleration: -1.30851851851852
	},
	{
		id: 1215,
		time: 1214,
		velocity: 1.72027777777778,
		power: -2221.87508551086,
		road: 14321.6968981482,
		acceleration: -1.10194444444444
	},
	{
		id: 1216,
		time: 1215,
		velocity: 1.23,
		power: -1009.54870678362,
		road: 14323.1190277778,
		acceleration: -0.877407407407407
	},
	{
		id: 1217,
		time: 1216,
		velocity: 0,
		power: -294.247288338748,
		road: 14323.8157407407,
		acceleration: -0.573425925925926
	},
	{
		id: 1218,
		time: 1217,
		velocity: 0,
		power: -54.8586473684211,
		road: 14324.0207407407,
		acceleration: -0.41
	},
	{
		id: 1219,
		time: 1218,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1220,
		time: 1219,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1221,
		time: 1220,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1222,
		time: 1221,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1223,
		time: 1222,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1224,
		time: 1223,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1225,
		time: 1224,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1226,
		time: 1225,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1227,
		time: 1226,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1228,
		time: 1227,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1229,
		time: 1228,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1230,
		time: 1229,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1231,
		time: 1230,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1232,
		time: 1231,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1233,
		time: 1232,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1234,
		time: 1233,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1235,
		time: 1234,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1236,
		time: 1235,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1237,
		time: 1236,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1238,
		time: 1237,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1239,
		time: 1238,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1240,
		time: 1239,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1241,
		time: 1240,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1242,
		time: 1241,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1243,
		time: 1242,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1244,
		time: 1243,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1245,
		time: 1244,
		velocity: 0,
		power: 0,
		road: 14324.0207407407,
		acceleration: 0
	},
	{
		id: 1246,
		time: 1245,
		velocity: 0,
		power: 57.9220637777549,
		road: 14324.1665740741,
		acceleration: 0.291666666666667
	},
	{
		id: 1247,
		time: 1246,
		velocity: 0.875,
		power: 834.619248642797,
		road: 14324.9536574074,
		acceleration: 0.990833333333333
	},
	{
		id: 1248,
		time: 1247,
		velocity: 2.9725,
		power: 3101.18389802987,
		road: 14326.9785185185,
		acceleration: 1.48472222222222
	},
	{
		id: 1249,
		time: 1248,
		velocity: 4.45416666666667,
		power: 5179.02727896363,
		road: 14330.4616666667,
		acceleration: 1.43185185185185
	},
	{
		id: 1250,
		time: 1249,
		velocity: 5.17055555555556,
		power: 4044.72009823241,
		road: 14335.0546296296,
		acceleration: 0.787777777777778
	},
	{
		id: 1251,
		time: 1250,
		velocity: 5.33583333333333,
		power: 2888.10059210761,
		road: 14340.2619907407,
		acceleration: 0.441018518518518
	},
	{
		id: 1252,
		time: 1251,
		velocity: 5.77722222222222,
		power: 3123.85132533183,
		road: 14345.9082407407,
		acceleration: 0.436759259259259
	},
	{
		id: 1253,
		time: 1252,
		velocity: 6.48083333333333,
		power: 4160.05519119527,
		road: 14352.0544907407,
		acceleration: 0.563240740740741
	},
	{
		id: 1254,
		time: 1253,
		velocity: 7.02555555555556,
		power: 5026.47726328207,
		road: 14358.7977314815,
		acceleration: 0.630740740740741
	},
	{
		id: 1255,
		time: 1254,
		velocity: 7.66944444444444,
		power: 6482.51029034855,
		road: 14366.2350925926,
		acceleration: 0.7575
	},
	{
		id: 1256,
		time: 1255,
		velocity: 8.75333333333333,
		power: 6378.70483400533,
		road: 14374.3801851852,
		acceleration: 0.657962962962963
	},
	{
		id: 1257,
		time: 1256,
		velocity: 8.99944444444444,
		power: 6663.14263031379,
		road: 14383.1669907407,
		acceleration: 0.625462962962962
	},
	{
		id: 1258,
		time: 1257,
		velocity: 9.54583333333333,
		power: 4122.70437764952,
		road: 14392.4128240741,
		acceleration: 0.292592592592593
	},
	{
		id: 1259,
		time: 1258,
		velocity: 9.63111111111111,
		power: 3298.48742730342,
		road: 14401.8984259259,
		acceleration: 0.186944444444444
	},
	{
		id: 1260,
		time: 1259,
		velocity: 9.56027777777778,
		power: 2723.99591838045,
		road: 14411.5359722222,
		acceleration: 0.116944444444446
	},
	{
		id: 1261,
		time: 1260,
		velocity: 9.89666666666667,
		power: 2096.95120359791,
		road: 14421.2549074074,
		acceleration: 0.0458333333333325
	},
	{
		id: 1262,
		time: 1261,
		velocity: 9.76861111111111,
		power: 2080.92397125793,
		road: 14431.0180555556,
		acceleration: 0.0425925925925927
	},
	{
		id: 1263,
		time: 1262,
		velocity: 9.68805555555556,
		power: -653.423857146772,
		road: 14440.6769907407,
		acceleration: -0.251018518518519
	},
	{
		id: 1264,
		time: 1263,
		velocity: 9.14361111111111,
		power: -1574.33867077681,
		road: 14450.0337037037,
		acceleration: -0.353425925925926
	},
	{
		id: 1265,
		time: 1264,
		velocity: 8.70833333333333,
		power: -2404.27935377168,
		road: 14458.9863888889,
		acceleration: -0.454629629629629
	},
	{
		id: 1266,
		time: 1265,
		velocity: 8.32416666666667,
		power: -1979.58683297715,
		road: 14467.5055555556,
		acceleration: -0.412407407407407
	},
	{
		id: 1267,
		time: 1266,
		velocity: 7.90638888888889,
		power: -3369.22458814769,
		road: 14475.5157407407,
		acceleration: -0.605555555555557
	},
	{
		id: 1268,
		time: 1267,
		velocity: 6.89166666666667,
		power: -5179.00250807865,
		road: 14482.7693055556,
		acceleration: -0.907685185185184
	},
	{
		id: 1269,
		time: 1268,
		velocity: 5.60111111111111,
		power: -5941.45978169593,
		road: 14488.9922222222,
		acceleration: -1.15361111111111
	},
	{
		id: 1270,
		time: 1269,
		velocity: 4.44555555555556,
		power: -3947.5538885975,
		road: 14494.1655092593,
		acceleration: -0.945648148148147
	},
	{
		id: 1271,
		time: 1270,
		velocity: 4.05472222222222,
		power: -1375.44400082292,
		road: 14498.6346296296,
		acceleration: -0.462685185185186
	},
	{
		id: 1272,
		time: 1271,
		velocity: 4.21305555555556,
		power: 289.988147204107,
		road: 14502.8400462963,
		acceleration: -0.0647222222222217
	},
	{
		id: 1273,
		time: 1272,
		velocity: 4.25138888888889,
		power: 766.380638422746,
		road: 14507.0405092593,
		acceleration: 0.0548148148148151
	},
	{
		id: 1274,
		time: 1273,
		velocity: 4.21916666666667,
		power: 440.467311264085,
		road: 14511.2547222222,
		acceleration: -0.0273148148148152
	},
	{
		id: 1275,
		time: 1274,
		velocity: 4.13111111111111,
		power: 429.616904531513,
		road: 14515.4406944444,
		acceleration: -0.0291666666666668
	},
	{
		id: 1276,
		time: 1275,
		velocity: 4.16388888888889,
		power: 318.062150971096,
		road: 14519.5839814815,
		acceleration: -0.0562037037037033
	},
	{
		id: 1277,
		time: 1276,
		velocity: 4.05055555555556,
		power: 382.714847341678,
		road: 14523.6799537037,
		acceleration: -0.0384259259259272
	},
	{
		id: 1278,
		time: 1277,
		velocity: 4.01583333333333,
		power: 190.647631873613,
		road: 14527.7133333333,
		acceleration: -0.0867592592592588
	},
	{
		id: 1279,
		time: 1278,
		velocity: 3.90361111111111,
		power: 370.268308153926,
		road: 14531.6843055556,
		acceleration: -0.0380555555555553
	},
	{
		id: 1280,
		time: 1279,
		velocity: 3.93638888888889,
		power: -161.127661022322,
		road: 14535.5463888889,
		acceleration: -0.179722222222222
	},
	{
		id: 1281,
		time: 1280,
		velocity: 3.47666666666667,
		power: -1424.50507270099,
		road: 14539.0364814815,
		acceleration: -0.564259259259259
	},
	{
		id: 1282,
		time: 1281,
		velocity: 2.21083333333333,
		power: -1947.32616787961,
		road: 14541.8083333333,
		acceleration: -0.872222222222222
	},
	{
		id: 1283,
		time: 1282,
		velocity: 1.31972222222222,
		power: -1714.71594131686,
		road: 14543.5646296296,
		acceleration: -1.15888888888889
	},
	{
		id: 1284,
		time: 1283,
		velocity: 0,
		power: -466.624970640427,
		road: 14544.3730092593,
		acceleration: -0.736944444444444
	},
	{
		id: 1285,
		time: 1284,
		velocity: 0,
		power: -65.0923273066927,
		road: 14544.592962963,
		acceleration: -0.439907407407407
	},
	{
		id: 1286,
		time: 1285,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1287,
		time: 1286,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1288,
		time: 1287,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1289,
		time: 1288,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1290,
		time: 1289,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1291,
		time: 1290,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1292,
		time: 1291,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1293,
		time: 1292,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1294,
		time: 1293,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1295,
		time: 1294,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1296,
		time: 1295,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1297,
		time: 1296,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1298,
		time: 1297,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1299,
		time: 1298,
		velocity: 0,
		power: 0,
		road: 14544.592962963,
		acceleration: 0
	},
	{
		id: 1300,
		time: 1299,
		velocity: 0,
		power: 81.2268754580052,
		road: 14544.7705555556,
		acceleration: 0.355185185185185
	},
	{
		id: 1301,
		time: 1300,
		velocity: 1.06555555555556,
		power: 1006.0649409001,
		road: 14545.6590277778,
		acceleration: 1.06657407407407
	},
	{
		id: 1302,
		time: 1301,
		velocity: 3.19972222222222,
		power: 3657.75798815456,
		road: 14547.8825925926,
		acceleration: 1.60361111111111
	},
	{
		id: 1303,
		time: 1302,
		velocity: 4.81083333333333,
		power: 4976.56146520984,
		road: 14551.554212963,
		acceleration: 1.2925
	},
	{
		id: 1304,
		time: 1303,
		velocity: 4.94305555555556,
		power: 3098.73458863922,
		road: 14556.1568055556,
		acceleration: 0.569444444444445
	},
	{
		id: 1305,
		time: 1304,
		velocity: 4.90805555555556,
		power: 2740.0262135019,
		road: 14561.2558796296,
		acceleration: 0.423518518518519
	},
	{
		id: 1306,
		time: 1305,
		velocity: 6.08138888888889,
		power: 4223.36634418255,
		road: 14566.8885185185,
		acceleration: 0.64361111111111
	},
	{
		id: 1307,
		time: 1306,
		velocity: 6.87388888888889,
		power: 5742.31901303032,
		road: 14573.2430555556,
		acceleration: 0.800185185185185
	},
	{
		id: 1308,
		time: 1307,
		velocity: 7.30861111111111,
		power: 4933.59225175532,
		road: 14580.2881018519,
		acceleration: 0.580833333333334
	},
	{
		id: 1309,
		time: 1308,
		velocity: 7.82388888888889,
		power: 4188.04078739652,
		road: 14587.8354166667,
		acceleration: 0.423703703703702
	},
	{
		id: 1310,
		time: 1309,
		velocity: 8.145,
		power: 2832.86121540166,
		road: 14595.7026388889,
		acceleration: 0.216111111111111
	},
	{
		id: 1311,
		time: 1310,
		velocity: 7.95694444444444,
		power: 1257.4823797347,
		road: 14603.6791203704,
		acceleration: 0.00240740740740808
	},
	{
		id: 1312,
		time: 1311,
		velocity: 7.83111111111111,
		power: -713.167328470365,
		road: 14611.5280092593,
		acceleration: -0.257592592592592
	},
	{
		id: 1313,
		time: 1312,
		velocity: 7.37222222222222,
		power: -1204.02556254017,
		road: 14619.0845833333,
		acceleration: -0.327037037037037
	},
	{
		id: 1314,
		time: 1313,
		velocity: 6.97583333333333,
		power: -308.080189091643,
		road: 14626.3767592593,
		acceleration: -0.201759259259259
	},
	{
		id: 1315,
		time: 1314,
		velocity: 7.22583333333333,
		power: 1711.25327132103,
		road: 14633.6138888889,
		acceleration: 0.0916666666666668
	},
	{
		id: 1316,
		time: 1315,
		velocity: 7.64722222222222,
		power: 4496.93763021787,
		road: 14641.1315740741,
		acceleration: 0.469444444444445
	},
	{
		id: 1317,
		time: 1316,
		velocity: 8.38416666666667,
		power: 4237.94010097266,
		road: 14649.0825,
		acceleration: 0.397037037037037
	},
	{
		id: 1318,
		time: 1317,
		velocity: 8.41694444444444,
		power: 3990.63698540675,
		road: 14657.4007407407,
		acceleration: 0.337592592592591
	},
	{
		id: 1319,
		time: 1318,
		velocity: 8.66,
		power: 1784.4237358938,
		road: 14665.91375,
		acceleration: 0.0519444444444446
	},
	{
		id: 1320,
		time: 1319,
		velocity: 8.54,
		power: 1580.01531390187,
		road: 14674.465462963,
		acceleration: 0.0254629629629637
	},
	{
		id: 1321,
		time: 1320,
		velocity: 8.49333333333333,
		power: 903.91440255693,
		road: 14683.0012962963,
		acceleration: -0.0572222222222205
	},
	{
		id: 1322,
		time: 1321,
		velocity: 8.48833333333333,
		power: 1321.74762081048,
		road: 14691.5060648148,
		acceleration: -0.00490740740740847
	},
	{
		id: 1323,
		time: 1322,
		velocity: 8.52527777777778,
		power: 1366.97881290698,
		road: 14700.00875,
		acceleration: 0.000740740740740264
	},
	{
		id: 1324,
		time: 1323,
		velocity: 8.49555555555555,
		power: 1904.68987030909,
		road: 14708.5447685185,
		acceleration: 0.0659259259259262
	},
	{
		id: 1325,
		time: 1324,
		velocity: 8.68611111111111,
		power: 2133.54183192401,
		road: 14717.159212963,
		acceleration: 0.0909259259259265
	},
	{
		id: 1326,
		time: 1325,
		velocity: 8.79805555555556,
		power: 3138.99032856293,
		road: 14725.9218981482,
		acceleration: 0.205555555555556
	},
	{
		id: 1327,
		time: 1326,
		velocity: 9.11222222222222,
		power: 3207.85520786842,
		road: 14734.8888425926,
		acceleration: 0.202962962962964
	},
	{
		id: 1328,
		time: 1327,
		velocity: 9.295,
		power: 3936.88552775559,
		road: 14744.094212963,
		acceleration: 0.273888888888887
	},
	{
		id: 1329,
		time: 1328,
		velocity: 9.61972222222222,
		power: 2185.58858941929,
		road: 14753.4704166667,
		acceleration: 0.0677777777777777
	},
	{
		id: 1330,
		time: 1329,
		velocity: 9.31555555555556,
		power: 1489.94305121494,
		road: 14762.8750462963,
		acceleration: -0.0109259259259264
	},
	{
		id: 1331,
		time: 1330,
		velocity: 9.26222222222222,
		power: 1924.34887212252,
		road: 14772.2927777778,
		acceleration: 0.0371296296296304
	},
	{
		id: 1332,
		time: 1331,
		velocity: 9.73111111111111,
		power: 4258.48315235621,
		road: 14781.8728240741,
		acceleration: 0.287499999999998
	},
	{
		id: 1333,
		time: 1332,
		velocity: 10.1780555555556,
		power: 5244.7201621318,
		road: 14791.782962963,
		acceleration: 0.372685185185187
	},
	{
		id: 1334,
		time: 1333,
		velocity: 10.3802777777778,
		power: 4100.76837476138,
		road: 14801.9969907407,
		acceleration: 0.235092592592594
	},
	{
		id: 1335,
		time: 1334,
		velocity: 10.4363888888889,
		power: 2265.99878794248,
		road: 14812.3494907407,
		acceleration: 0.0418518518518525
	},
	{
		id: 1336,
		time: 1335,
		velocity: 10.3036111111111,
		power: 1590.20254348529,
		road: 14822.7094907407,
		acceleration: -0.0268518518518537
	},
	{
		id: 1337,
		time: 1336,
		velocity: 10.2997222222222,
		power: 983.975544439131,
		road: 14833.0125462963,
		acceleration: -0.087037037037037
	},
	{
		id: 1338,
		time: 1337,
		velocity: 10.1752777777778,
		power: 1626.60167319052,
		road: 14843.262037037,
		acceleration: -0.0200925925925919
	},
	{
		id: 1339,
		time: 1338,
		velocity: 10.2433333333333,
		power: 2985.6018331516,
		road: 14853.56,
		acceleration: 0.117037037037036
	},
	{
		id: 1340,
		time: 1339,
		velocity: 10.6508333333333,
		power: 5267.39613401666,
		road: 14864.0841203704,
		acceleration: 0.335277777777778
	},
	{
		id: 1341,
		time: 1340,
		velocity: 11.1811111111111,
		power: 5814.52484977498,
		road: 14874.9592592593,
		acceleration: 0.366759259259261
	},
	{
		id: 1342,
		time: 1341,
		velocity: 11.3436111111111,
		power: 3890.66541468277,
		road: 14886.1021759259,
		acceleration: 0.168796296296293
	},
	{
		id: 1343,
		time: 1342,
		velocity: 11.1572222222222,
		power: 2938.52107815109,
		road: 14897.3668055556,
		acceleration: 0.0746296296296318
	},
	{
		id: 1344,
		time: 1343,
		velocity: 11.405,
		power: 2178.56958969,
		road: 14908.6700925926,
		acceleration: 0.0026851851851859
	},
	{
		id: 1345,
		time: 1344,
		velocity: 11.3516666666667,
		power: 4698.99006480965,
		road: 14920.09,
		acceleration: 0.230555555555554
	},
	{
		id: 1346,
		time: 1345,
		velocity: 11.8488888888889,
		power: 3950.91888280194,
		road: 14931.7018518519,
		acceleration: 0.153333333333334
	},
	{
		id: 1347,
		time: 1346,
		velocity: 11.865,
		power: 4365.29960826557,
		road: 14943.4818055556,
		acceleration: 0.182870370370368
	},
	{
		id: 1348,
		time: 1347,
		velocity: 11.9002777777778,
		power: 3330.89039744422,
		road: 14955.3960185185,
		acceleration: 0.0856481481481506
	},
	{
		id: 1349,
		time: 1348,
		velocity: 12.1058333333333,
		power: 3239.74797124636,
		road: 14967.3903703704,
		acceleration: 0.0746296296296283
	},
	{
		id: 1350,
		time: 1349,
		velocity: 12.0888888888889,
		power: 3280.39616079959,
		road: 14979.4597222222,
		acceleration: 0.0753703703703703
	},
	{
		id: 1351,
		time: 1350,
		velocity: 12.1263888888889,
		power: 3007.2863507472,
		road: 14991.5914814815,
		acceleration: 0.0494444444444433
	},
	{
		id: 1352,
		time: 1351,
		velocity: 12.2541666666667,
		power: 3536.47831779265,
		road: 15003.7941666667,
		acceleration: 0.0924074074074106
	},
	{
		id: 1353,
		time: 1352,
		velocity: 12.3661111111111,
		power: 3731.30535612367,
		road: 15016.0956481482,
		acceleration: 0.105185185185181
	},
	{
		id: 1354,
		time: 1353,
		velocity: 12.4419444444444,
		power: 3622.1241635494,
		road: 15028.4957407407,
		acceleration: 0.0920370370370378
	},
	{
		id: 1355,
		time: 1354,
		velocity: 12.5302777777778,
		power: 4294.85480635354,
		road: 15041.01375,
		acceleration: 0.143796296296298
	},
	{
		id: 1356,
		time: 1355,
		velocity: 12.7975,
		power: 4059.81012228323,
		road: 15053.6630092593,
		acceleration: 0.118703703703703
	},
	{
		id: 1357,
		time: 1356,
		velocity: 12.7980555555556,
		power: 3853.16913684366,
		road: 15066.4202777778,
		acceleration: 0.0973148148148137
	},
	{
		id: 1358,
		time: 1357,
		velocity: 12.8222222222222,
		power: 2747.03574721452,
		road: 15079.2286111111,
		acceleration: 0.00481481481481616
	},
	{
		id: 1359,
		time: 1358,
		velocity: 12.8119444444444,
		power: 958.787216990291,
		road: 15091.9693055556,
		acceleration: -0.140092592592593
	},
	{
		id: 1360,
		time: 1359,
		velocity: 12.3777777777778,
		power: 688.115846798474,
		road: 15104.5601851852,
		acceleration: -0.159537037037037
	},
	{
		id: 1361,
		time: 1360,
		velocity: 12.3436111111111,
		power: 459.904431431745,
		road: 15116.9834722222,
		acceleration: -0.175648148148147
	},
	{
		id: 1362,
		time: 1361,
		velocity: 12.285,
		power: 2167.72335624945,
		road: 15129.3046296296,
		acceleration: -0.0286111111111129
	},
	{
		id: 1363,
		time: 1362,
		velocity: 12.2919444444444,
		power: 1687.27813228542,
		road: 15141.5773611111,
		acceleration: -0.0682407407407393
	},
	{
		id: 1364,
		time: 1363,
		velocity: 12.1388888888889,
		power: 557.542105742005,
		road: 15153.7346296296,
		acceleration: -0.162685185185188
	},
	{
		id: 1365,
		time: 1364,
		velocity: 11.7969444444444,
		power: -4245.70875504996,
		road: 15165.5187962963,
		acceleration: -0.583518518518517
	},
	{
		id: 1366,
		time: 1365,
		velocity: 10.5413888888889,
		power: -10050.5657298647,
		road: 15176.4307407407,
		acceleration: -1.16092592592593
	},
	{
		id: 1367,
		time: 1366,
		velocity: 8.65611111111111,
		power: -13828.4405048614,
		road: 15185.9069907407,
		acceleration: -1.71046296296296
	},
	{
		id: 1368,
		time: 1367,
		velocity: 6.66555555555555,
		power: -10601.4394055908,
		road: 15193.7353240741,
		acceleration: -1.58537037037037
	},
	{
		id: 1369,
		time: 1368,
		velocity: 5.78527777777778,
		power: -5478.97686358659,
		road: 15200.2532407407,
		acceleration: -1.03546296296296
	},
	{
		id: 1370,
		time: 1369,
		velocity: 5.54972222222222,
		power: -1336.77554182904,
		road: 15206.0591203704,
		acceleration: -0.388611111111111
	},
	{
		id: 1371,
		time: 1370,
		velocity: 5.49972222222222,
		power: 910.699930433234,
		road: 15211.683287037,
		acceleration: 0.0251851851851859
	},
	{
		id: 1372,
		time: 1371,
		velocity: 5.86083333333333,
		power: 2666.92815850718,
		road: 15217.4885185185,
		acceleration: 0.336944444444443
	},
	{
		id: 1373,
		time: 1372,
		velocity: 6.56055555555556,
		power: 4834.1085413354,
		road: 15223.7906944444,
		acceleration: 0.656944444444444
	},
	{
		id: 1374,
		time: 1373,
		velocity: 7.47055555555555,
		power: 7118.04500416124,
		road: 15230.8718518519,
		acceleration: 0.901018518518519
	},
	{
		id: 1375,
		time: 1374,
		velocity: 8.56388888888889,
		power: 7937.73873370638,
		road: 15238.8449074074,
		acceleration: 0.882777777777777
	},
	{
		id: 1376,
		time: 1375,
		velocity: 9.20888888888889,
		power: 9020.77071720986,
		road: 15247.7079166667,
		acceleration: 0.89712962962963
	},
	{
		id: 1377,
		time: 1376,
		velocity: 10.1619444444444,
		power: 7100.01830867383,
		road: 15257.3175462963,
		acceleration: 0.596111111111112
	},
	{
		id: 1378,
		time: 1377,
		velocity: 10.3522222222222,
		power: 4589.57046017567,
		road: 15267.3725462963,
		acceleration: 0.294629629629629
	},
	{
		id: 1379,
		time: 1378,
		velocity: 10.0927777777778,
		power: 2345.91771187585,
		road: 15277.6019907407,
		acceleration: 0.0542592592592595
	},
	{
		id: 1380,
		time: 1379,
		velocity: 10.3247222222222,
		power: 5236.66761395149,
		road: 15288.0277314815,
		acceleration: 0.338333333333333
	},
	{
		id: 1381,
		time: 1380,
		velocity: 11.3672222222222,
		power: 7639.150601957,
		road: 15298.8943518519,
		acceleration: 0.543425925925927
	},
	{
		id: 1382,
		time: 1381,
		velocity: 11.7230555555556,
		power: 10153.6351718586,
		road: 15310.3945833333,
		acceleration: 0.723796296296296
	},
	{
		id: 1383,
		time: 1382,
		velocity: 12.4961111111111,
		power: 9270.5676839335,
		road: 15322.5510185185,
		acceleration: 0.588611111111112
	},
	{
		id: 1384,
		time: 1383,
		velocity: 13.1330555555556,
		power: 9997.7344456663,
		road: 15335.302962963,
		acceleration: 0.602407407407407
	},
	{
		id: 1385,
		time: 1384,
		velocity: 13.5302777777778,
		power: 9477.7125607886,
		road: 15348.6153240741,
		acceleration: 0.518425925925925
	},
	{
		id: 1386,
		time: 1385,
		velocity: 14.0513888888889,
		power: 8033.36187691875,
		road: 15362.3755092593,
		acceleration: 0.377222222222223
	},
	{
		id: 1387,
		time: 1386,
		velocity: 14.2647222222222,
		power: 7056.97730938011,
		road: 15376.4668055556,
		acceleration: 0.285
	},
	{
		id: 1388,
		time: 1387,
		velocity: 14.3852777777778,
		power: 4028.81407312724,
		road: 15390.7274537037,
		acceleration: 0.0537037037037038
	},
	{
		id: 1389,
		time: 1388,
		velocity: 14.2125,
		power: 2883.48878794502,
		road: 15404.9995833333,
		acceleration: -0.0307407407407396
	},
	{
		id: 1390,
		time: 1389,
		velocity: 14.1725,
		power: 1255.55020540034,
		road: 15419.1822685185,
		acceleration: -0.148148148148151
	},
	{
		id: 1391,
		time: 1390,
		velocity: 13.9408333333333,
		power: 1774.99868994342,
		road: 15433.2375925926,
		acceleration: -0.106574074074073
	},
	{
		id: 1392,
		time: 1391,
		velocity: 13.8927777777778,
		power: 961.386622188581,
		road: 15447.1574537037,
		acceleration: -0.164351851851851
	},
	{
		id: 1393,
		time: 1392,
		velocity: 13.6794444444444,
		power: 548.063736706769,
		road: 15460.8990740741,
		acceleration: -0.19212962962963
	},
	{
		id: 1394,
		time: 1393,
		velocity: 13.3644444444444,
		power: -299.75916047116,
		road: 15474.4177777778,
		acceleration: -0.253703703703703
	},
	{
		id: 1395,
		time: 1394,
		velocity: 13.1316666666667,
		power: 644.361546068631,
		road: 15487.7213888889,
		acceleration: -0.176481481481481
	},
	{
		id: 1396,
		time: 1395,
		velocity: 13.15,
		power: 1544.4954431078,
		road: 15500.8856481481,
		acceleration: -0.102222222222222
	},
	{
		id: 1397,
		time: 1396,
		velocity: 13.0577777777778,
		power: 2989.8345559167,
		road: 15514.0059259259,
		acceleration: 0.0142592592592568
	},
	{
		id: 1398,
		time: 1397,
		velocity: 13.1744444444444,
		power: 3511.05817631225,
		road: 15527.1606481481,
		acceleration: 0.0546296296296305
	},
	{
		id: 1399,
		time: 1398,
		velocity: 13.3138888888889,
		power: 4202.96476706883,
		road: 15540.3959259259,
		acceleration: 0.106481481481485
	},
	{
		id: 1400,
		time: 1399,
		velocity: 13.3772222222222,
		power: 3531.03298117062,
		road: 15553.7096759259,
		acceleration: 0.0504629629629605
	},
	{
		id: 1401,
		time: 1400,
		velocity: 13.3258333333333,
		power: 4230.17862117248,
		road: 15567.0998611111,
		acceleration: 0.102407407407407
	},
	{
		id: 1402,
		time: 1401,
		velocity: 13.6211111111111,
		power: 3620.9543241759,
		road: 15580.5672222222,
		acceleration: 0.0519444444444463
	},
	{
		id: 1403,
		time: 1402,
		velocity: 13.5330555555556,
		power: 3297.56722797025,
		road: 15594.073287037,
		acceleration: 0.0254629629629619
	},
	{
		id: 1404,
		time: 1403,
		velocity: 13.4022222222222,
		power: 1654.61665621109,
		road: 15607.5415740741,
		acceleration: -0.101018518518519
	},
	{
		id: 1405,
		time: 1404,
		velocity: 13.3180555555556,
		power: 2651.92831855837,
		road: 15620.9485648148,
		acceleration: -0.0215740740740742
	},
	{
		id: 1406,
		time: 1405,
		velocity: 13.4683333333333,
		power: 2921.18773975549,
		road: 15634.3446759259,
		acceleration: -0.000185185185186398
	},
	{
		id: 1407,
		time: 1406,
		velocity: 13.4016666666667,
		power: 2998.13430524381,
		road: 15647.7435648148,
		acceleration: 0.00574074074074105
	},
	{
		id: 1408,
		time: 1407,
		velocity: 13.3352777777778,
		power: 2108.07274551469,
		road: 15661.1137962963,
		acceleration: -0.0630555555555521
	},
	{
		id: 1409,
		time: 1408,
		velocity: 13.2791666666667,
		power: 2258.98095913279,
		road: 15674.4276851852,
		acceleration: -0.0496296296296315
	},
	{
		id: 1410,
		time: 1409,
		velocity: 13.2527777777778,
		power: 3139.97078951496,
		road: 15687.7268518519,
		acceleration: 0.0201851851851842
	},
	{
		id: 1411,
		time: 1410,
		velocity: 13.3958333333333,
		power: 4302.95740368565,
		road: 15701.0906944444,
		acceleration: 0.109166666666667
	},
	{
		id: 1412,
		time: 1411,
		velocity: 13.6066666666667,
		power: 5762.57805678998,
		road: 15714.6169444444,
		acceleration: 0.215648148148148
	},
	{
		id: 1413,
		time: 1412,
		velocity: 13.8997222222222,
		power: 8342.50724157307,
		road: 15728.4491666667,
		acceleration: 0.396296296296297
	},
	{
		id: 1414,
		time: 1413,
		velocity: 14.5847222222222,
		power: 10018.0872318801,
		road: 15742.7256944444,
		acceleration: 0.492314814814813
	},
	{
		id: 1415,
		time: 1414,
		velocity: 15.0836111111111,
		power: 9274.66811246121,
		road: 15757.4531018519,
		acceleration: 0.409444444444445
	},
	{
		id: 1416,
		time: 1415,
		velocity: 15.1280555555556,
		power: 5024.04590631256,
		road: 15772.43375,
		acceleration: 0.0970370370370386
	},
	{
		id: 1417,
		time: 1416,
		velocity: 14.8758333333333,
		power: 2427.51691479965,
		road: 15787.4206481481,
		acceleration: -0.0845370370370375
	},
	{
		id: 1418,
		time: 1417,
		velocity: 14.83,
		power: 878.78759090325,
		road: 15802.2703703704,
		acceleration: -0.189814814814815
	},
	{
		id: 1419,
		time: 1418,
		velocity: 14.5586111111111,
		power: -3124.27627671817,
		road: 15816.7893981481,
		acceleration: -0.471574074074075
	},
	{
		id: 1420,
		time: 1419,
		velocity: 13.4611111111111,
		power: -10918.8510595568,
		road: 15830.5397222222,
		acceleration: -1.06583333333333
	},
	{
		id: 1421,
		time: 1420,
		velocity: 11.6325,
		power: -13748.8069312997,
		road: 15843.074212963,
		acceleration: -1.36583333333333
	},
	{
		id: 1422,
		time: 1421,
		velocity: 10.4611111111111,
		power: -10441.5069568948,
		road: 15854.3401851852,
		acceleration: -1.1712037037037
	},
	{
		id: 1423,
		time: 1422,
		velocity: 9.9475,
		power: -7438.62449452692,
		road: 15864.5449537037,
		acceleration: -0.951203703703703
	},
	{
		id: 1424,
		time: 1423,
		velocity: 8.77888888888889,
		power: -7532.00973783548,
		road: 15873.7571759259,
		acceleration: -1.03388888888889
	},
	{
		id: 1425,
		time: 1424,
		velocity: 7.35944444444444,
		power: -7858.61298460168,
		road: 15881.8606944444,
		acceleration: -1.18351851851852
	},
	{
		id: 1426,
		time: 1425,
		velocity: 6.39694444444444,
		power: -5126.6671292268,
		road: 15888.9125462963,
		acceleration: -0.919814814814816
	},
	{
		id: 1427,
		time: 1426,
		velocity: 6.01944444444444,
		power: -1804.26263773896,
		road: 15895.2803703704,
		acceleration: -0.44824074074074
	},
	{
		id: 1428,
		time: 1427,
		velocity: 6.01472222222222,
		power: 642.01700787135,
		road: 15901.4049537037,
		acceleration: -0.0382407407407408
	},
	{
		id: 1429,
		time: 1428,
		velocity: 6.28222222222222,
		power: 1974.9882054529,
		road: 15907.6034722222,
		acceleration: 0.186111111111111
	},
	{
		id: 1430,
		time: 1429,
		velocity: 6.57777777777778,
		power: 3106.43516509835,
		road: 15914.0720833333,
		acceleration: 0.354074074074074
	},
	{
		id: 1431,
		time: 1430,
		velocity: 7.07694444444444,
		power: 3552.20143977926,
		road: 15920.9137962963,
		acceleration: 0.392129629629629
	},
	{
		id: 1432,
		time: 1431,
		velocity: 7.45861111111111,
		power: 3633.48896682092,
		road: 15928.1375462963,
		acceleration: 0.371944444444445
	},
	{
		id: 1433,
		time: 1432,
		velocity: 7.69361111111111,
		power: 2999.94772789095,
		road: 15935.6766666667,
		acceleration: 0.258796296296296
	},
	{
		id: 1434,
		time: 1433,
		velocity: 7.85333333333333,
		power: 4517.82887246957,
		road: 15943.5648611111,
		acceleration: 0.439351851851852
	},
	{
		id: 1435,
		time: 1434,
		velocity: 8.77666666666667,
		power: 7991.57433627123,
		road: 15952.0813888889,
		acceleration: 0.817314814814816
	},
	{
		id: 1436,
		time: 1435,
		velocity: 10.1455555555556,
		power: 11537.6805260892,
		road: 15961.5566666667,
		acceleration: 1.10018518518519
	},
	{
		id: 1437,
		time: 1436,
		velocity: 11.1538888888889,
		power: 10128.9749595244,
		road: 15971.9965740741,
		acceleration: 0.829074074074073
	},
	{
		id: 1438,
		time: 1437,
		velocity: 11.2638888888889,
		power: 7278.10847434654,
		road: 15983.0964351852,
		acceleration: 0.490833333333331
	},
	{
		id: 1439,
		time: 1438,
		velocity: 11.6180555555556,
		power: 2221.45318152978,
		road: 15994.4443518519,
		acceleration: 0.00527777777777771
	},
	{
		id: 1440,
		time: 1439,
		velocity: 11.1697222222222,
		power: 677.569116460693,
		road: 16005.7268055556,
		acceleration: -0.136203703703702
	},
	{
		id: 1441,
		time: 1440,
		velocity: 10.8552777777778,
		power: -1361.96541603437,
		road: 16016.7783796296,
		acceleration: -0.325555555555555
	},
	{
		id: 1442,
		time: 1441,
		velocity: 10.6413888888889,
		power: -597.972027982331,
		road: 16027.5416666667,
		acceleration: -0.251018518518519
	},
	{
		id: 1443,
		time: 1442,
		velocity: 10.4166666666667,
		power: -174.068494136335,
		road: 16038.075787037,
		acceleration: -0.207314814814815
	},
	{
		id: 1444,
		time: 1443,
		velocity: 10.2333333333333,
		power: -137.863813516128,
		road: 16048.405462963,
		acceleration: -0.201574074074074
	},
	{
		id: 1445,
		time: 1444,
		velocity: 10.0366666666667,
		power: -946.356288370134,
		road: 16058.4927314815,
		acceleration: -0.283240740740741
	},
	{
		id: 1446,
		time: 1445,
		velocity: 9.56694444444445,
		power: -3064.89538347403,
		road: 16068.1821759259,
		acceleration: -0.512407407407407
	},
	{
		id: 1447,
		time: 1446,
		velocity: 8.69611111111111,
		power: -2960.04624051222,
		road: 16077.3586111111,
		acceleration: -0.513611111111111
	},
	{
		id: 1448,
		time: 1447,
		velocity: 8.49583333333333,
		power: -4733.63021802597,
		road: 16085.902962963,
		acceleration: -0.750555555555556
	},
	{
		id: 1449,
		time: 1448,
		velocity: 7.31527777777778,
		power: -5397.58464330738,
		road: 16093.6241666667,
		acceleration: -0.895740740740742
	},
	{
		id: 1450,
		time: 1449,
		velocity: 6.00888888888889,
		power: -7562.12365991344,
		road: 16100.2184722222,
		acceleration: -1.35805555555556
	},
	{
		id: 1451,
		time: 1450,
		velocity: 4.42166666666667,
		power: -6225.23829845894,
		road: 16105.434212963,
		acceleration: -1.39907407407407
	},
	{
		id: 1452,
		time: 1451,
		velocity: 3.11805555555556,
		power: -4301.3113888751,
		road: 16109.2958333333,
		acceleration: -1.30916666666667
	},
	{
		id: 1453,
		time: 1452,
		velocity: 2.08138888888889,
		power: -2584.73490658393,
		road: 16111.9175925926,
		acceleration: -1.17055555555556
	},
	{
		id: 1454,
		time: 1453,
		velocity: 0.91,
		power: -1309.44529707728,
		road: 16113.4343981481,
		acceleration: -1.03935185185185
	},
	{
		id: 1455,
		time: 1454,
		velocity: 0,
		power: -348.792526625999,
		road: 16114.0846296296,
		acceleration: -0.693796296296296
	},
	{
		id: 1456,
		time: 1455,
		velocity: 0,
		power: -25.2601631578947,
		road: 16114.2362962963,
		acceleration: -0.303333333333333
	},
	{
		id: 1457,
		time: 1456,
		velocity: 0,
		power: 0,
		road: 16114.2362962963,
		acceleration: 0
	},
	{
		id: 1458,
		time: 1457,
		velocity: 0,
		power: 0,
		road: 16114.2362962963,
		acceleration: 0
	},
	{
		id: 1459,
		time: 1458,
		velocity: 0,
		power: 0,
		road: 16114.2362962963,
		acceleration: 0
	},
	{
		id: 1460,
		time: 1459,
		velocity: 0,
		power: 0,
		road: 16114.2362962963,
		acceleration: 0
	},
	{
		id: 1461,
		time: 1460,
		velocity: 0,
		power: 0,
		road: 16114.2362962963,
		acceleration: 0
	},
	{
		id: 1462,
		time: 1461,
		velocity: 0,
		power: 5.60381832888945,
		road: 16114.2674537037,
		acceleration: 0.0623148148148148
	},
	{
		id: 1463,
		time: 1462,
		velocity: 0.186944444444444,
		power: 7.52887609897449,
		road: 16114.3297685185,
		acceleration: 0
	},
	{
		id: 1464,
		time: 1463,
		velocity: 0,
		power: 7.52887609897449,
		road: 16114.3920833333,
		acceleration: 0
	},
	{
		id: 1465,
		time: 1464,
		velocity: 0,
		power: 1.92499208901884,
		road: 16114.4232407407,
		acceleration: -0.0623148148148148
	},
	{
		id: 1466,
		time: 1465,
		velocity: 0,
		power: 2.21395288728873,
		road: 16114.4381018519,
		acceleration: 0.0297222222222222
	},
	{
		id: 1467,
		time: 1466,
		velocity: 0.0891666666666667,
		power: 5.50216529123952,
		road: 16114.4775925926,
		acceleration: 0.019537037037037
	},
	{
		id: 1468,
		time: 1467,
		velocity: 0.0586111111111111,
		power: 5.95146488732848,
		road: 16114.5268518519,
		acceleration: 0
	},
	{
		id: 1469,
		time: 1468,
		velocity: 0,
		power: 3.18733953509614,
		road: 16114.56125,
		acceleration: -0.0297222222222222
	},
	{
		id: 1470,
		time: 1469,
		velocity: 0,
		power: 0.999408560753736,
		road: 16114.5710185185,
		acceleration: -0.019537037037037
	},
	{
		id: 1471,
		time: 1470,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1472,
		time: 1471,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1473,
		time: 1472,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1474,
		time: 1473,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1475,
		time: 1474,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1476,
		time: 1475,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1477,
		time: 1476,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1478,
		time: 1477,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1479,
		time: 1478,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1480,
		time: 1479,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1481,
		time: 1480,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1482,
		time: 1481,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1483,
		time: 1482,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1484,
		time: 1483,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1485,
		time: 1484,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1486,
		time: 1485,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1487,
		time: 1486,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1488,
		time: 1487,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1489,
		time: 1488,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1490,
		time: 1489,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1491,
		time: 1490,
		velocity: 0,
		power: 0,
		road: 16114.5710185185,
		acceleration: 0
	},
	{
		id: 1492,
		time: 1491,
		velocity: 0,
		power: 2.96706524586291,
		road: 16114.5899537037,
		acceleration: 0.0378703703703704
	},
	{
		id: 1493,
		time: 1492,
		velocity: 0.113611111111111,
		power: 4.57544790563095,
		road: 16114.6278240741,
		acceleration: 0
	},
	{
		id: 1494,
		time: 1493,
		velocity: 0,
		power: 4.57544790563095,
		road: 16114.6656944444,
		acceleration: 0
	},
	{
		id: 1495,
		time: 1494,
		velocity: 0,
		power: 1.60836791747888,
		road: 16114.6846296296,
		acceleration: -0.0378703703703704
	},
	{
		id: 1496,
		time: 1495,
		velocity: 0,
		power: 0,
		road: 16114.6846296296,
		acceleration: 0
	},
	{
		id: 1497,
		time: 1496,
		velocity: 0,
		power: 0,
		road: 16114.6846296296,
		acceleration: 0
	},
	{
		id: 1498,
		time: 1497,
		velocity: 0,
		power: 0,
		road: 16114.6846296296,
		acceleration: 0
	},
	{
		id: 1499,
		time: 1498,
		velocity: 0,
		power: 0,
		road: 16114.6846296296,
		acceleration: 0
	},
	{
		id: 1500,
		time: 1499,
		velocity: 0,
		power: 227.105996411173,
		road: 16115.0003703704,
		acceleration: 0.631481481481481
	},
	{
		id: 1501,
		time: 1500,
		velocity: 1.89444444444444,
		power: 1908.13952703054,
		road: 16116.3263888889,
		acceleration: 1.38907407407407
	},
	{
		id: 1502,
		time: 1501,
		velocity: 4.16722222222222,
		power: 5793.65426331591,
		road: 16119.3052314815,
		acceleration: 1.91657407407407
	},
	{
		id: 1503,
		time: 1502,
		velocity: 5.74972222222222,
		power: 8567.77676700721,
		road: 16124.1104166667,
		acceleration: 1.73611111111111
	},
	{
		id: 1504,
		time: 1503,
		velocity: 7.10277777777778,
		power: 11002.7172727038,
		road: 16130.5994907407,
		acceleration: 1.63166666666667
	},
	{
		id: 1505,
		time: 1504,
		velocity: 9.06222222222222,
		power: 17702.7196387006,
		road: 16138.9362037037,
		acceleration: 2.06361111111111
	},
	{
		id: 1506,
		time: 1505,
		velocity: 11.9405555555556,
		power: 19989.6803994662,
		road: 16149.2298148148,
		acceleration: 1.85018518518518
	},
	{
		id: 1507,
		time: 1506,
		velocity: 12.6533333333333,
		power: 18791.7166632558,
		road: 16161.1694907407,
		acceleration: 1.44194444444445
	},
	{
		id: 1508,
		time: 1507,
		velocity: 13.3880555555556,
		power: 15432.9995344459,
		road: 16174.331712963,
		acceleration: 1.00314814814815
	},
	{
		id: 1509,
		time: 1508,
		velocity: 14.95,
		power: 17461.5891153651,
		road: 16188.5193055556,
		acceleration: 1.04759259259259
	},
	{
		id: 1510,
		time: 1509,
		velocity: 15.7961111111111,
		power: 22414.4048147261,
		road: 16203.8646759259,
		acceleration: 1.26796296296296
	},
	{
		id: 1511,
		time: 1510,
		velocity: 17.1919444444444,
		power: 21761.4120266121,
		road: 16220.3916666667,
		acceleration: 1.09527777777778
	},
	{
		id: 1512,
		time: 1511,
		velocity: 18.2358333333333,
		power: 18825.1665696217,
		road: 16237.8788888889,
		acceleration: 0.825185185185184
	},
	{
		id: 1513,
		time: 1512,
		velocity: 18.2716666666667,
		power: 15773.4216114084,
		road: 16256.0744907407,
		acceleration: 0.591574074074074
	},
	{
		id: 1514,
		time: 1513,
		velocity: 18.9666666666667,
		power: 12829.4802516319,
		road: 16274.7622685185,
		acceleration: 0.392777777777777
	},
	{
		id: 1515,
		time: 1514,
		velocity: 19.4141666666667,
		power: 13196.8772834027,
		road: 16293.8412962963,
		acceleration: 0.389722222222222
	},
	{
		id: 1516,
		time: 1515,
		velocity: 19.4408333333333,
		power: 7642.37431416958,
		road: 16313.1530092593,
		acceleration: 0.0756481481481472
	},
	{
		id: 1517,
		time: 1516,
		velocity: 19.1936111111111,
		power: 1326.2829537053,
		road: 16332.3708333333,
		acceleration: -0.263425925925926
	},
	{
		id: 1518,
		time: 1517,
		velocity: 18.6238888888889,
		power: -1629.36640538594,
		road: 16351.2477777778,
		acceleration: -0.418333333333333
	},
	{
		id: 1519,
		time: 1518,
		velocity: 18.1858333333333,
		power: -23.8602350788372,
		road: 16369.7546759259,
		acceleration: -0.32175925925926
	},
	{
		id: 1520,
		time: 1519,
		velocity: 18.2283333333333,
		power: 1424.32612028258,
		road: 16387.9841666667,
		acceleration: -0.233055555555552
	},
	{
		id: 1521,
		time: 1520,
		velocity: 17.9247222222222,
		power: -584.554348200539,
		road: 16405.9256944444,
		acceleration: -0.34287037037037
	},
	{
		id: 1522,
		time: 1521,
		velocity: 17.1572222222222,
		power: -3647.67056696532,
		road: 16423.4368055556,
		acceleration: -0.517962962962965
	},
	{
		id: 1523,
		time: 1522,
		velocity: 16.6744444444444,
		power: -6457.21988507604,
		road: 16440.345,
		acceleration: -0.687870370370369
	},
	{
		id: 1524,
		time: 1523,
		velocity: 15.8611111111111,
		power: -11505.8010948974,
		road: 16456.3980092593,
		acceleration: -1.0225
	},
	{
		id: 1525,
		time: 1524,
		velocity: 14.0897222222222,
		power: -13644.3514246544,
		road: 16471.3350462963,
		acceleration: -1.20944444444445
	},
	{
		id: 1526,
		time: 1525,
		velocity: 13.0461111111111,
		power: -17037.5088875266,
		road: 16484.8935648148,
		acceleration: -1.54759259259259
	},
	{
		id: 1527,
		time: 1526,
		velocity: 11.2183333333333,
		power: -14929.3108023129,
		road: 16496.9231018519,
		acceleration: -1.51037037037037
	},
	{
		id: 1528,
		time: 1527,
		velocity: 9.55861111111111,
		power: -10212.1278056259,
		road: 16507.5997685185,
		acceleration: -1.19537037037037
	},
	{
		id: 1529,
		time: 1528,
		velocity: 9.46,
		power: -3033.58526326417,
		road: 16517.425787037,
		acceleration: -0.505925925925926
	},
	{
		id: 1530,
		time: 1529,
		velocity: 9.70055555555555,
		power: 7770.22540703478,
		road: 16527.3196759259,
		acceleration: 0.641666666666666
	},
	{
		id: 1531,
		time: 1530,
		velocity: 11.4836111111111,
		power: 17575.1557862668,
		road: 16538.2778703704,
		acceleration: 1.48694444444445
	},
	{
		id: 1532,
		time: 1531,
		velocity: 13.9208333333333,
		power: 23252.3671038063,
		road: 16550.8409722222,
		acceleration: 1.72287037037037
	},
	{
		id: 1533,
		time: 1532,
		velocity: 14.8691666666667,
		power: 21590.2077032874,
		road: 16564.9468981481,
		acceleration: 1.36277777777778
	},
	{
		id: 1534,
		time: 1533,
		velocity: 15.5719444444444,
		power: 15772.4960396948,
		road: 16580.1481944444,
		acceleration: 0.827962962962962
	},
	{
		id: 1535,
		time: 1534,
		velocity: 16.4047222222222,
		power: 16826.9966403284,
		road: 16596.1763425926,
		acceleration: 0.825740740740741
	},
	{
		id: 1536,
		time: 1535,
		velocity: 17.3463888888889,
		power: 18319.3561585502,
		road: 16613.04125,
		acceleration: 0.847777777777779
	},
	{
		id: 1537,
		time: 1536,
		velocity: 18.1152777777778,
		power: 17997.1692355553,
		road: 16630.7105092593,
		acceleration: 0.760925925925925
	},
	{
		id: 1538,
		time: 1537,
		velocity: 18.6875,
		power: 14384.7052618002,
		road: 16649.0126388889,
		acceleration: 0.504814814814814
	},
	{
		id: 1539,
		time: 1538,
		velocity: 18.8608333333333,
		power: 12789.2878168186,
		road: 16667.7606481481,
		acceleration: 0.386944444444445
	},
	{
		id: 1540,
		time: 1539,
		velocity: 19.2761111111111,
		power: 11959.6796659433,
		road: 16686.8625,
		acceleration: 0.320740740740742
	},
	{
		id: 1541,
		time: 1540,
		velocity: 19.6497222222222,
		power: 10063.275410298,
		road: 16706.2266666667,
		acceleration: 0.203888888888887
	},
	{
		id: 1542,
		time: 1541,
		velocity: 19.4725,
		power: 6631.00399358411,
		road: 16725.7,
		acceleration: 0.0144444444444431
	},
	{
		id: 1543,
		time: 1542,
		velocity: 19.3194444444444,
		power: 2817.76887147991,
		road: 16745.0868518518,
		acceleration: -0.187407407407406
	},
	{
		id: 1544,
		time: 1543,
		velocity: 19.0875,
		power: 3249.75435036938,
		road: 16764.3006018518,
		acceleration: -0.158796296296298
	},
	{
		id: 1545,
		time: 1544,
		velocity: 18.9961111111111,
		power: 2921.92009456653,
		road: 16783.3491203704,
		acceleration: -0.171666666666667
	},
	{
		id: 1546,
		time: 1545,
		velocity: 18.8044444444444,
		power: 1400.3779323603,
		road: 16802.1869444444,
		acceleration: -0.249722222222221
	},
	{
		id: 1547,
		time: 1546,
		velocity: 18.3383333333333,
		power: 543.272574155679,
		road: 16820.7543518518,
		acceleration: -0.29111111111111
	},
	{
		id: 1548,
		time: 1547,
		velocity: 18.1227777777778,
		power: 1995.28483138766,
		road: 16839.0748148148,
		acceleration: -0.202777777777776
	},
	{
		id: 1549,
		time: 1548,
		velocity: 18.1961111111111,
		power: 3980.52146267139,
		road: 16857.2514814815,
		acceleration: -0.0848148148148162
	},
	{
		id: 1550,
		time: 1549,
		velocity: 18.0838888888889,
		power: 5253.1589150342,
		road: 16875.380787037,
		acceleration: -0.00990740740740748
	},
	{
		id: 1551,
		time: 1550,
		velocity: 18.0930555555556,
		power: 4645.34627758066,
		road: 16893.4831481481,
		acceleration: -0.043981481481481
	},
	{
		id: 1552,
		time: 1551,
		velocity: 18.0641666666667,
		power: 6156.43141062761,
		road: 16911.5851388889,
		acceleration: 0.0432407407407425
	},
	{
		id: 1553,
		time: 1552,
		velocity: 18.2136111111111,
		power: 5845.53104854823,
		road: 16929.7207407407,
		acceleration: 0.0239814814814778
	},
	{
		id: 1554,
		time: 1553,
		velocity: 18.165,
		power: 6331.53774150388,
		road: 16947.8935648148,
		acceleration: 0.050462962962964
	},
	{
		id: 1555,
		time: 1554,
		velocity: 18.2155555555556,
		power: 4064.93527675987,
		road: 16966.0519444444,
		acceleration: -0.0793518518518503
	},
	{
		id: 1556,
		time: 1555,
		velocity: 17.9755555555556,
		power: 2280.86800295058,
		road: 16984.0814351852,
		acceleration: -0.178425925925929
	},
	{
		id: 1557,
		time: 1556,
		velocity: 17.6297222222222,
		power: -802.993979330032,
		road: 17001.845462963,
		acceleration: -0.352499999999999
	},
	{
		id: 1558,
		time: 1557,
		velocity: 17.1580555555556,
		power: -1347.4984770903,
		road: 17019.24375,
		acceleration: -0.378981481481482
	},
	{
		id: 1559,
		time: 1558,
		velocity: 16.8386111111111,
		power: -2152.79820849803,
		road: 17036.2412037037,
		acceleration: -0.422685185185184
	},
	{
		id: 1560,
		time: 1559,
		velocity: 16.3616666666667,
		power: -1941.99002461306,
		road: 17052.8248611111,
		acceleration: -0.404907407407407
	},
	{
		id: 1561,
		time: 1560,
		velocity: 15.9433333333333,
		power: -2620.64109803575,
		road: 17068.9839351852,
		acceleration: -0.44425925925926
	},
	{
		id: 1562,
		time: 1561,
		velocity: 15.5058333333333,
		power: -3726.95350201803,
		road: 17084.6635185185,
		acceleration: -0.514722222222222
	},
	{
		id: 1563,
		time: 1562,
		velocity: 14.8175,
		power: -4127.19017080607,
		road: 17099.8147685185,
		acceleration: -0.541944444444445
	},
	{
		id: 1564,
		time: 1563,
		velocity: 14.3175,
		power: -4769.4503394152,
		road: 17114.4001851852,
		acceleration: -0.589722222222221
	},
	{
		id: 1565,
		time: 1564,
		velocity: 13.7366666666667,
		power: -6008.7349846524,
		road: 17128.3465740741,
		acceleration: -0.688333333333334
	},
	{
		id: 1566,
		time: 1565,
		velocity: 12.7525,
		power: -3586.02296375258,
		road: 17141.6941203704,
		acceleration: -0.509351851851852
	},
	{
		id: 1567,
		time: 1566,
		velocity: 12.7894444444444,
		power: -1252.88087409911,
		road: 17154.6253703704,
		acceleration: -0.323240740740742
	},
	{
		id: 1568,
		time: 1567,
		velocity: 12.7669444444444,
		power: 8132.67166462941,
		road: 17167.6118055556,
		acceleration: 0.433611111111111
	},
	{
		id: 1569,
		time: 1568,
		velocity: 14.0533333333333,
		power: 13896.4816120424,
		road: 17181.2332407407,
		acceleration: 0.836388888888891
	},
	{
		id: 1570,
		time: 1569,
		velocity: 15.2986111111111,
		power: 22303.5258261481,
		road: 17195.9416666667,
		acceleration: 1.33759259259259
	},
	{
		id: 1571,
		time: 1570,
		velocity: 16.7797222222222,
		power: 21077.4949575302,
		road: 17211.8753703704,
		acceleration: 1.11296296296296
	},
	{
		id: 1572,
		time: 1571,
		velocity: 17.3922222222222,
		power: 16255.8679829835,
		road: 17228.7260648148,
		acceleration: 0.721018518518516
	},
	{
		id: 1573,
		time: 1572,
		velocity: 17.4616666666667,
		power: 8215.73403559608,
		road: 17246.0371296296,
		acceleration: 0.199722222222224
	},
	{
		id: 1574,
		time: 1573,
		velocity: 17.3788888888889,
		power: 4744.23594961745,
		road: 17263.4414351852,
		acceleration: -0.0132407407407413
	},
	{
		id: 1575,
		time: 1574,
		velocity: 17.3525,
		power: 6290.1130683544,
		road: 17280.8782407407,
		acceleration: 0.0782407407407391
	},
	{
		id: 1576,
		time: 1575,
		velocity: 17.6963888888889,
		power: 6778.75485908435,
		road: 17298.4059722222,
		acceleration: 0.103611111111114
	},
	{
		id: 1577,
		time: 1576,
		velocity: 17.6897222222222,
		power: 6573.04034824709,
		road: 17316.0291666667,
		acceleration: 0.087314814814814
	},
	{
		id: 1578,
		time: 1577,
		velocity: 17.6144444444444,
		power: 6156.56984553048,
		road: 17333.7258333333,
		acceleration: 0.0596296296296295
	},
	{
		id: 1579,
		time: 1578,
		velocity: 17.8752777777778,
		power: 7043.37821948538,
		road: 17351.5064814815,
		acceleration: 0.108333333333334
	},
	{
		id: 1580,
		time: 1579,
		velocity: 18.0147222222222,
		power: 7608.66518044228,
		road: 17369.4093055556,
		acceleration: 0.136018518518519
	},
	{
		id: 1581,
		time: 1580,
		velocity: 18.0225,
		power: 6318.07545088288,
		road: 17387.4085185185,
		acceleration: 0.056759259259259
	},
	{
		id: 1582,
		time: 1581,
		velocity: 18.0455555555556,
		power: 5633.87803124167,
		road: 17405.4439351852,
		acceleration: 0.015648148148145
	},
	{
		id: 1583,
		time: 1582,
		velocity: 18.0616666666667,
		power: 5324.85244271818,
		road: 17423.4859259259,
		acceleration: -0.00249999999999773
	},
	{
		id: 1584,
		time: 1583,
		velocity: 18.015,
		power: 5438.89641064478,
		road: 17441.5287037037,
		acceleration: 0.00407407407407234
	},
	{
		id: 1585,
		time: 1584,
		velocity: 18.0577777777778,
		power: 6435.65897167823,
		road: 17459.60375,
		acceleration: 0.0604629629629621
	},
	{
		id: 1586,
		time: 1585,
		velocity: 18.2430555555556,
		power: 6314.77108248493,
		road: 17477.7346296296,
		acceleration: 0.0512037037037025
	},
	{
		id: 1587,
		time: 1586,
		velocity: 18.1686111111111,
		power: 4499.87922340697,
		road: 17495.8644444444,
		acceleration: -0.053333333333331
	},
	{
		id: 1588,
		time: 1587,
		velocity: 17.8977777777778,
		power: 3676.58790553488,
		road: 17513.9184259259,
		acceleration: -0.0983333333333363
	},
	{
		id: 1589,
		time: 1588,
		velocity: 17.9480555555556,
		power: 4266.26544418428,
		road: 17531.8925,
		acceleration: -0.0614814814814793
	},
	{
		id: 1590,
		time: 1589,
		velocity: 17.9841666666667,
		power: 5728.82732565452,
		road: 17549.847962963,
		acceleration: 0.0242592592592601
	},
	{
		id: 1591,
		time: 1590,
		velocity: 17.9705555555556,
		power: 4728.19387719407,
		road: 17567.7986574074,
		acceleration: -0.0337962962962948
	},
	{
		id: 1592,
		time: 1591,
		velocity: 17.8466666666667,
		power: 5690.02089335173,
		road: 17585.7436574074,
		acceleration: 0.0224074074074068
	},
	{
		id: 1593,
		time: 1592,
		velocity: 18.0513888888889,
		power: 5916.07545767679,
		road: 17603.7170833333,
		acceleration: 0.0344444444444463
	},
	{
		id: 1594,
		time: 1593,
		velocity: 18.0738888888889,
		power: 6734.78694502246,
		road: 17621.7475462963,
		acceleration: 0.0796296296296291
	},
	{
		id: 1595,
		time: 1594,
		velocity: 18.0855555555556,
		power: 5730.94758429193,
		road: 17639.8275925926,
		acceleration: 0.0195370370370362
	},
	{
		id: 1596,
		time: 1595,
		velocity: 18.11,
		power: 5217.95267728569,
		road: 17657.9122685185,
		acceleration: -0.0102777777777767
	},
	{
		id: 1597,
		time: 1596,
		velocity: 18.0430555555556,
		power: 5475.78083206564,
		road: 17675.9941666667,
		acceleration: 0.0047222222222203
	},
	{
		id: 1598,
		time: 1597,
		velocity: 18.0997222222222,
		power: 5194.79396791353,
		road: 17694.0727314815,
		acceleration: -0.011388888888888
	},
	{
		id: 1599,
		time: 1598,
		velocity: 18.0758333333333,
		power: 3987.78076691527,
		road: 17712.105787037,
		acceleration: -0.0796296296296291
	},
	{
		id: 1600,
		time: 1599,
		velocity: 17.8041666666667,
		power: 2369.39746415186,
		road: 17730.0140740741,
		acceleration: -0.169907407407408
	},
	{
		id: 1601,
		time: 1600,
		velocity: 17.59,
		power: 341.650377825702,
		road: 17747.6956944445,
		acceleration: -0.283425925925926
	},
	{
		id: 1602,
		time: 1601,
		velocity: 17.2255555555556,
		power: 1419.39444758117,
		road: 17765.1287962963,
		acceleration: -0.213611111111113
	},
	{
		id: 1603,
		time: 1602,
		velocity: 17.1633333333333,
		power: 1646.77666204966,
		road: 17782.3576851852,
		acceleration: -0.194814814814812
	},
	{
		id: 1604,
		time: 1603,
		velocity: 17.0055555555556,
		power: 4064.70045564899,
		road: 17799.4671296296,
		acceleration: -0.044074074074075
	},
	{
		id: 1605,
		time: 1604,
		velocity: 17.0933333333333,
		power: 5362.28599755531,
		road: 17816.5722222222,
		acceleration: 0.0353703703703729
	},
	{
		id: 1606,
		time: 1605,
		velocity: 17.2694444444444,
		power: 6471.64037472148,
		road: 17833.7451388889,
		acceleration: 0.100277777777777
	},
	{
		id: 1607,
		time: 1606,
		velocity: 17.3063888888889,
		power: 6430.97660668479,
		road: 17851.0150462963,
		acceleration: 0.093703703703703
	},
	{
		id: 1608,
		time: 1607,
		velocity: 17.3744444444444,
		power: 5757.61611225713,
		road: 17868.3568518519,
		acceleration: 0.0500925925925912
	},
	{
		id: 1609,
		time: 1608,
		velocity: 17.4197222222222,
		power: 5616.31693990189,
		road: 17885.7436111111,
		acceleration: 0.0398148148148145
	},
	{
		id: 1610,
		time: 1609,
		velocity: 17.4258333333333,
		power: 5110.70835579524,
		road: 17903.154537037,
		acceleration: 0.00851851851851748
	},
	{
		id: 1611,
		time: 1610,
		velocity: 17.4,
		power: 5276.41658934705,
		road: 17920.5787037037,
		acceleration: 0.0179629629629652
	},
	{
		id: 1612,
		time: 1611,
		velocity: 17.4736111111111,
		power: 4738.75220555633,
		road: 17938.0046759259,
		acceleration: -0.0143518518518526
	},
	{
		id: 1613,
		time: 1612,
		velocity: 17.3827777777778,
		power: 4766.24629925416,
		road: 17955.4173611111,
		acceleration: -0.0122222222222206
	},
	{
		id: 1614,
		time: 1613,
		velocity: 17.3633333333333,
		power: 4174.06711373179,
		road: 17972.8005555556,
		acceleration: -0.0467592592592609
	},
	{
		id: 1615,
		time: 1614,
		velocity: 17.3333333333333,
		power: 4297.57064722494,
		road: 17990.1414351852,
		acceleration: -0.0378703703703707
	},
	{
		id: 1616,
		time: 1615,
		velocity: 17.2691666666667,
		power: 2933.33123135433,
		road: 18007.4044907408,
		acceleration: -0.117777777777778
	},
	{
		id: 1617,
		time: 1616,
		velocity: 17.01,
		power: 1986.33137123356,
		road: 18024.5230092593,
		acceleration: -0.171296296296294
	},
	{
		id: 1618,
		time: 1617,
		velocity: 16.8194444444444,
		power: 1451.18119542242,
		road: 18041.4561574074,
		acceleration: -0.199444444444445
	},
	{
		id: 1619,
		time: 1618,
		velocity: 16.6708333333333,
		power: 2435.81358909801,
		road: 18058.2225925926,
		acceleration: -0.133981481481484
	},
	{
		id: 1620,
		time: 1619,
		velocity: 16.6080555555556,
		power: 3111.83052483259,
		road: 18074.8778240741,
		acceleration: -0.0884259259259217
	},
	{
		id: 1621,
		time: 1620,
		velocity: 16.5541666666667,
		power: 3232.83556167735,
		road: 18091.4497222222,
		acceleration: -0.0782407407407426
	},
	{
		id: 1622,
		time: 1621,
		velocity: 16.4361111111111,
		power: 2396.31644990781,
		road: 18107.9184259259,
		acceleration: -0.128148148148149
	},
	{
		id: 1623,
		time: 1622,
		velocity: 16.2236111111111,
		power: 381.180732873056,
		road: 18124.1968981482,
		acceleration: -0.252314814814817
	},
	{
		id: 1624,
		time: 1623,
		velocity: 15.7972222222222,
		power: -2107.90879262647,
		road: 18140.1446759259,
		acceleration: -0.409074074074072
	},
	{
		id: 1625,
		time: 1624,
		velocity: 15.2088888888889,
		power: -1349.24213972993,
		road: 18155.7105555556,
		acceleration: -0.354722222222222
	},
	{
		id: 1626,
		time: 1625,
		velocity: 15.1594444444444,
		power: -1565.85315997075,
		road: 18170.9162962963,
		acceleration: -0.365555555555554
	},
	{
		id: 1627,
		time: 1626,
		velocity: 14.7005555555556,
		power: 570.166687638217,
		road: 18185.8328240741,
		acceleration: -0.212870370370373
	},
	{
		id: 1628,
		time: 1627,
		velocity: 14.5702777777778,
		power: -700.162340537538,
		road: 18200.4936111111,
		acceleration: -0.298611111111109
	},
	{
		id: 1629,
		time: 1628,
		velocity: 14.2636111111111,
		power: -587.854425343244,
		road: 18214.8617592593,
		acceleration: -0.286666666666667
	},
	{
		id: 1630,
		time: 1629,
		velocity: 13.8405555555556,
		power: 431.370436082098,
		road: 18228.9826388889,
		acceleration: -0.207870370370372
	},
	{
		id: 1631,
		time: 1630,
		velocity: 13.9466666666667,
		power: 2701.61952415427,
		road: 18242.9816666667,
		acceleration: -0.0358333333333309
	},
	{
		id: 1632,
		time: 1631,
		velocity: 14.1561111111111,
		power: 5518.14552674111,
		road: 18257.0486574074,
		acceleration: 0.171759259259259
	},
	{
		id: 1633,
		time: 1632,
		velocity: 14.3558333333333,
		power: 5794.68300059236,
		road: 18271.2935648148,
		acceleration: 0.184074074074076
	},
	{
		id: 1634,
		time: 1633,
		velocity: 14.4988888888889,
		power: 5122.74067942958,
		road: 18285.694537037,
		acceleration: 0.128055555555552
	},
	{
		id: 1635,
		time: 1634,
		velocity: 14.5402777777778,
		power: 5285.58864937407,
		road: 18300.2266666667,
		acceleration: 0.134259259259263
	},
	{
		id: 1636,
		time: 1635,
		velocity: 14.7586111111111,
		power: 4582.73745084157,
		road: 18314.8656481482,
		acceleration: 0.0794444444444427
	},
	{
		id: 1637,
		time: 1636,
		velocity: 14.7372222222222,
		power: 4404.72148849405,
		road: 18329.5763425926,
		acceleration: 0.0639814814814823
	},
	{
		id: 1638,
		time: 1637,
		velocity: 14.7322222222222,
		power: 3705.09899572134,
		road: 18344.325462963,
		acceleration: 0.0128703703703685
	},
	{
		id: 1639,
		time: 1638,
		velocity: 14.7972222222222,
		power: 2717.38781468451,
		road: 18359.0527314815,
		acceleration: -0.0565740740740726
	},
	{
		id: 1640,
		time: 1639,
		velocity: 14.5675,
		power: 862.072063082603,
		road: 18373.65875,
		acceleration: -0.185925925925927
	},
	{
		id: 1641,
		time: 1640,
		velocity: 14.1744444444444,
		power: -419.927464376329,
		road: 18388.034537037,
		acceleration: -0.274537037037037
	},
	{
		id: 1642,
		time: 1641,
		velocity: 13.9736111111111,
		power: -1226.54083007384,
		road: 18402.1078703704,
		acceleration: -0.330370370370369
	},
	{
		id: 1643,
		time: 1642,
		velocity: 13.5763888888889,
		power: -2366.1816988932,
		road: 18415.8089351852,
		acceleration: -0.414166666666667
	},
	{
		id: 1644,
		time: 1643,
		velocity: 12.9319444444444,
		power: -3106.60547781895,
		road: 18429.0668981482,
		acceleration: -0.472037037037039
	},
	{
		id: 1645,
		time: 1644,
		velocity: 12.5575,
		power: -2311.79475809311,
		road: 18441.8843055556,
		acceleration: -0.409074074074075
	},
	{
		id: 1646,
		time: 1645,
		velocity: 12.3491666666667,
		power: -1097.87301818271,
		road: 18454.3435185185,
		acceleration: -0.307314814814813
	},
	{
		id: 1647,
		time: 1646,
		velocity: 12.01,
		power: 453.766548950727,
		road: 18466.5627314815,
		acceleration: -0.172685185185186
	},
	{
		id: 1648,
		time: 1647,
		velocity: 12.0394444444444,
		power: -1539.70285522909,
		road: 18478.5240740741,
		acceleration: -0.343055555555557
	},
	{
		id: 1649,
		time: 1648,
		velocity: 11.32,
		power: -1226.22072420593,
		road: 18490.1567592593,
		acceleration: -0.314259259259259
	},
	{
		id: 1650,
		time: 1649,
		velocity: 11.0672222222222,
		power: -5944.14112736763,
		road: 18501.2529166667,
		acceleration: -0.758796296296296
	},
	{
		id: 1651,
		time: 1650,
		velocity: 9.76305555555555,
		power: -6016.45762048531,
		road: 18511.5699537037,
		acceleration: -0.799444444444443
	},
	{
		id: 1652,
		time: 1651,
		velocity: 8.92166666666667,
		power: -5372.99877267155,
		road: 18521.102037037,
		acceleration: -0.770462962962963
	},
	{
		id: 1653,
		time: 1652,
		velocity: 8.75583333333333,
		power: -1916.71639303196,
		road: 18530.0501388889,
		acceleration: -0.397499999999999
	},
	{
		id: 1654,
		time: 1653,
		velocity: 8.57055555555556,
		power: 132.041192992407,
		road: 18538.7225925926,
		acceleration: -0.153796296296296
	},
	{
		id: 1655,
		time: 1654,
		velocity: 8.46027777777778,
		power: -1639.97254793072,
		road: 18547.1320833333,
		acceleration: -0.372129629629631
	},
	{
		id: 1656,
		time: 1655,
		velocity: 7.63944444444444,
		power: -1307.02322083306,
		road: 18555.188287037,
		acceleration: -0.334444444444443
	},
	{
		id: 1657,
		time: 1656,
		velocity: 7.56722222222222,
		power: -1139.13535654052,
		road: 18562.9193055556,
		acceleration: -0.315925925925927
	},
	{
		id: 1658,
		time: 1657,
		velocity: 7.5125,
		power: 204.239487135529,
		road: 18570.4270833333,
		acceleration: -0.130555555555556
	},
	{
		id: 1659,
		time: 1658,
		velocity: 7.24777777777778,
		power: 66.2996374405174,
		road: 18577.7953240741,
		acceleration: -0.148518518518519
	},
	{
		id: 1660,
		time: 1659,
		velocity: 7.12166666666667,
		power: -8.56249061849196,
		road: 18585.0103240741,
		acceleration: -0.157962962962962
	},
	{
		id: 1661,
		time: 1660,
		velocity: 7.03861111111111,
		power: 40.5153276215182,
		road: 18592.0716203704,
		acceleration: -0.149444444444445
	},
	{
		id: 1662,
		time: 1661,
		velocity: 6.79944444444445,
		power: 125.52667201309,
		road: 18598.9905555556,
		acceleration: -0.135277777777777
	},
	{
		id: 1663,
		time: 1662,
		velocity: 6.71583333333333,
		power: 307.667710217643,
		road: 18605.7889351852,
		acceleration: -0.105833333333334
	},
	{
		id: 1664,
		time: 1663,
		velocity: 6.72111111111111,
		power: 33.8093655391541,
		road: 18612.4608333333,
		acceleration: -0.147129629629631
	},
	{
		id: 1665,
		time: 1664,
		velocity: 6.35805555555556,
		power: 574.403222254451,
		road: 18619.0293055556,
		acceleration: -0.0597222222222227
	},
	{
		id: 1666,
		time: 1665,
		velocity: 6.53666666666667,
		power: 417.271423262624,
		road: 18625.5261111111,
		acceleration: -0.0836111111111109
	},
	{
		id: 1667,
		time: 1666,
		velocity: 6.47027777777778,
		power: 643.548567228029,
		road: 18631.9583796296,
		acceleration: -0.0454629629629624
	},
	{
		id: 1668,
		time: 1667,
		velocity: 6.22166666666667,
		power: 350.152915148766,
		road: 18638.3217592593,
		acceleration: -0.0923148148148139
	},
	{
		id: 1669,
		time: 1668,
		velocity: 6.25972222222222,
		power: 707.430066197838,
		road: 18644.6231481482,
		acceleration: -0.0316666666666681
	},
	{
		id: 1670,
		time: 1669,
		velocity: 6.37527777777778,
		power: 1298.10005298464,
		road: 18650.9418055556,
		acceleration: 0.0662037037037049
	},
	{
		id: 1671,
		time: 1670,
		velocity: 6.42027777777778,
		power: 1431.26766294633,
		road: 18657.3360648148,
		acceleration: 0.0849999999999991
	},
	{
		id: 1672,
		time: 1671,
		velocity: 6.51472222222222,
		power: 1632.36485615811,
		road: 18663.8294444444,
		acceleration: 0.113240740740741
	},
	{
		id: 1673,
		time: 1672,
		velocity: 6.715,
		power: 1534.1722078665,
		road: 18670.425787037,
		acceleration: 0.0926851851851858
	},
	{
		id: 1674,
		time: 1673,
		velocity: 6.69833333333333,
		power: 2060.36701902854,
		road: 18677.1530555556,
		acceleration: 0.169166666666666
	},
	{
		id: 1675,
		time: 1674,
		velocity: 7.02222222222222,
		power: 1827.91129280178,
		road: 18684.0276851852,
		acceleration: 0.125555555555555
	},
	{
		id: 1676,
		time: 1675,
		velocity: 7.09166666666667,
		power: 2250.83209852032,
		road: 18691.0558333333,
		acceleration: 0.181481481481483
	},
	{
		id: 1677,
		time: 1676,
		velocity: 7.24277777777778,
		power: 1403.38050650952,
		road: 18698.1999074074,
		acceleration: 0.05037037037037
	},
	{
		id: 1678,
		time: 1677,
		velocity: 7.17333333333333,
		power: -256.556383552373,
		road: 18705.2723148148,
		acceleration: -0.193703703703703
	},
	{
		id: 1679,
		time: 1678,
		velocity: 6.51055555555556,
		power: -1222.02983892398,
		road: 18712.076712963,
		acceleration: -0.342314814814815
	},
	{
		id: 1680,
		time: 1679,
		velocity: 6.21583333333333,
		power: -1945.04159752944,
		road: 18718.4748611111,
		acceleration: -0.470185185185186
	},
	{
		id: 1681,
		time: 1680,
		velocity: 5.76277777777778,
		power: -1100.17409082773,
		road: 18724.4675462963,
		acceleration: -0.340740740740741
	},
	{
		id: 1682,
		time: 1681,
		velocity: 5.48833333333333,
		power: -577.446186756436,
		road: 18730.1637037037,
		acceleration: -0.252314814814814
	},
	{
		id: 1683,
		time: 1682,
		velocity: 5.45888888888889,
		power: 202.337428465311,
		road: 18735.6807407407,
		acceleration: -0.105925925925926
	},
	{
		id: 1684,
		time: 1683,
		velocity: 5.445,
		power: 717.462878684187,
		road: 18741.1418518519,
		acceleration: -0.00592592592592567
	},
	{
		id: 1685,
		time: 1684,
		velocity: 5.47055555555556,
		power: 258.427064303075,
		road: 18746.5531944444,
		acceleration: -0.0936111111111124
	},
	{
		id: 1686,
		time: 1685,
		velocity: 5.17805555555556,
		power: 194.094525210428,
		road: 18751.8653240741,
		acceleration: -0.104814814814814
	},
	{
		id: 1687,
		time: 1686,
		velocity: 5.13055555555556,
		power: -315.560390794508,
		road: 18757.0216666667,
		acceleration: -0.206759259259259
	},
	{
		id: 1688,
		time: 1687,
		velocity: 4.85027777777778,
		power: 703.591432918742,
		road: 18762.0769907407,
		acceleration: 0.00472222222222296
	},
	{
		id: 1689,
		time: 1688,
		velocity: 5.19222222222222,
		power: 1431.12426736616,
		road: 18767.2102777778,
		acceleration: 0.151203703703703
	},
	{
		id: 1690,
		time: 1689,
		velocity: 5.58416666666667,
		power: 2869.35134275729,
		road: 18772.6259722222,
		acceleration: 0.413611111111111
	},
	{
		id: 1691,
		time: 1690,
		velocity: 6.09111111111111,
		power: 3288.20966707067,
		road: 18778.4710648148,
		acceleration: 0.445185185185187
	},
	{
		id: 1692,
		time: 1691,
		velocity: 6.52777777777778,
		power: 3225.02012815541,
		road: 18784.7347685185,
		acceleration: 0.392037037037036
	},
	{
		id: 1693,
		time: 1692,
		velocity: 6.76027777777778,
		power: 2260.68719347782,
		road: 18791.2997222222,
		acceleration: 0.210462962962963
	},
	{
		id: 1694,
		time: 1693,
		velocity: 6.7225,
		power: 976.524741773666,
		road: 18797.9706481481,
		acceleration: 0.00148148148148142
	},
	{
		id: 1695,
		time: 1694,
		velocity: 6.53222222222222,
		power: 1.39388052301234,
		road: 18804.5664814815,
		acceleration: -0.151666666666666
	},
	{
		id: 1696,
		time: 1695,
		velocity: 6.30527777777778,
		power: -93.6289364120449,
		road: 18811.0034722222,
		acceleration: -0.16601851851852
	},
	{
		id: 1697,
		time: 1696,
		velocity: 6.22444444444444,
		power: 798.593012686349,
		road: 18817.3486111111,
		acceleration: -0.0176851851851838
	},
	{
		id: 1698,
		time: 1697,
		velocity: 6.47916666666667,
		power: 1655.65304716863,
		road: 18823.745787037,
		acceleration: 0.121759259259259
	},
	{
		id: 1699,
		time: 1698,
		velocity: 6.67055555555556,
		power: 1582.35321061173,
		road: 18830.2560185185,
		acceleration: 0.104351851851851
	},
	{
		id: 1700,
		time: 1699,
		velocity: 6.5375,
		power: 1000.87217178928,
		road: 18836.8227314815,
		acceleration: 0.00861111111111157
	},
	{
		id: 1701,
		time: 1700,
		velocity: 6.505,
		power: -384.760282478698,
		road: 18843.2869907407,
		acceleration: -0.213518518518519
	},
	{
		id: 1702,
		time: 1701,
		velocity: 6.03,
		power: -222.003454682558,
		road: 18849.5511111111,
		acceleration: -0.186759259259259
	},
	{
		id: 1703,
		time: 1702,
		velocity: 5.97722222222222,
		power: -894.710695012393,
		road: 18855.5697685185,
		acceleration: -0.304166666666667
	},
	{
		id: 1704,
		time: 1703,
		velocity: 5.5925,
		power: 298.498707002494,
		road: 18861.3900925926,
		acceleration: -0.0924999999999985
	},
	{
		id: 1705,
		time: 1704,
		velocity: 5.7525,
		power: 276.237397157159,
		road: 18867.1166203704,
		acceleration: -0.095092592592593
	},
	{
		id: 1706,
		time: 1705,
		velocity: 5.69194444444444,
		power: 693.266557409274,
		road: 18872.7871759259,
		acceleration: -0.0168518518518521
	},
	{
		id: 1707,
		time: 1706,
		velocity: 5.54194444444444,
		power: 281.939430019025,
		road: 18878.4031481482,
		acceleration: -0.0923148148148147
	},
	{
		id: 1708,
		time: 1707,
		velocity: 5.47555555555556,
		power: 338.490592248285,
		road: 18883.9328703704,
		acceleration: -0.0801851851851847
	},
	{
		id: 1709,
		time: 1708,
		velocity: 5.45138888888889,
		power: 122.280007729387,
		road: 18889.3623611111,
		acceleration: -0.120277777777778
	},
	{
		id: 1710,
		time: 1709,
		velocity: 5.18111111111111,
		power: -1764.94534420167,
		road: 18894.4791203704,
		acceleration: -0.505185185185185
	},
	{
		id: 1711,
		time: 1710,
		velocity: 3.96,
		power: -2115.54601502957,
		road: 18899.0289814815,
		acceleration: -0.628611111111111
	},
	{
		id: 1712,
		time: 1711,
		velocity: 3.56555555555556,
		power: -2628.42267233263,
		road: 18902.8328240741,
		acceleration: -0.863425925925926
	},
	{
		id: 1713,
		time: 1712,
		velocity: 2.59083333333333,
		power: -2216.77636735347,
		road: 18905.7365740741,
		acceleration: -0.936759259259259
	},
	{
		id: 1714,
		time: 1713,
		velocity: 1.14972222222222,
		power: -1445.87576390198,
		road: 18907.7234259259,
		acceleration: -0.897037037037037
	},
	{
		id: 1715,
		time: 1714,
		velocity: 0.874444444444444,
		power: -604.966644414797,
		road: 18908.9340277778,
		acceleration: -0.655462962962963
	},
	{
		id: 1716,
		time: 1715,
		velocity: 0.624444444444444,
		power: -58.8340560203598,
		road: 18909.7131481482,
		acceleration: -0.2075
	},
	{
		id: 1717,
		time: 1716,
		velocity: 0.527222222222222,
		power: 97.77341877138,
		road: 18910.3997685185,
		acceleration: 0.0225
	},
	{
		id: 1718,
		time: 1717,
		velocity: 0.941944444444444,
		power: 567.244392314212,
		road: 18911.3488888889,
		acceleration: 0.5025
	},
	{
		id: 1719,
		time: 1718,
		velocity: 2.13194444444444,
		power: 1087.28083351159,
		road: 18912.8634259259,
		acceleration: 0.628333333333333
	},
	{
		id: 1720,
		time: 1719,
		velocity: 2.41222222222222,
		power: 1161.27277266547,
		road: 18914.9242592593,
		acceleration: 0.464259259259259
	},
	{
		id: 1721,
		time: 1720,
		velocity: 2.33472222222222,
		power: 419.466743105139,
		road: 18917.2471759259,
		acceleration: 0.0599074074074077
	},
	{
		id: 1722,
		time: 1721,
		velocity: 2.31166666666667,
		power: 196.669236143222,
		road: 18919.5792592593,
		acceleration: -0.0415740740740742
	},
	{
		id: 1723,
		time: 1722,
		velocity: 2.2875,
		power: 360.450030025115,
		road: 18921.9069444445,
		acceleration: 0.032777777777778
	},
	{
		id: 1724,
		time: 1723,
		velocity: 2.43305555555556,
		power: 474.799158672809,
		road: 18924.2906944445,
		acceleration: 0.0793518518518521
	},
	{
		id: 1725,
		time: 1724,
		velocity: 2.54972222222222,
		power: 626.29888816955,
		road: 18926.7812037037,
		acceleration: 0.134166666666667
	},
	{
		id: 1726,
		time: 1725,
		velocity: 2.69,
		power: 596.076760006557,
		road: 18929.3934259259,
		acceleration: 0.109259259259259
	},
	{
		id: 1727,
		time: 1726,
		velocity: 2.76083333333333,
		power: 497.630061408442,
		road: 18932.091712963,
		acceleration: 0.0628703703703701
	},
	{
		id: 1728,
		time: 1727,
		velocity: 2.73833333333333,
		power: -89.8382944568365,
		road: 18934.7378703704,
		acceleration: -0.167129629629629
	},
	{
		id: 1729,
		time: 1728,
		velocity: 2.18861111111111,
		power: -21.2862356242215,
		road: 18937.2305092593,
		acceleration: -0.139907407407407
	},
	{
		id: 1730,
		time: 1729,
		velocity: 2.34111111111111,
		power: -35.9856403130455,
		road: 18939.5798611111,
		acceleration: -0.146666666666667
	},
	{
		id: 1731,
		time: 1730,
		velocity: 2.29833333333333,
		power: 266.991752285562,
		road: 18941.8526388889,
		acceleration: -0.00648148148148131
	},
	{
		id: 1732,
		time: 1731,
		velocity: 2.16916666666667,
		power: -72.9489572642642,
		road: 18944.0395370371,
		acceleration: -0.165277777777778
	},
	{
		id: 1733,
		time: 1732,
		velocity: 1.84527777777778,
		power: -199.53067643772,
		road: 18946.0260185185,
		acceleration: -0.235555555555555
	},
	{
		id: 1734,
		time: 1733,
		velocity: 1.59166666666667,
		power: -196.575014757678,
		road: 18947.7707407408,
		acceleration: -0.247962962962963
	},
	{
		id: 1735,
		time: 1734,
		velocity: 1.42527777777778,
		power: 167.644930921888,
		road: 18949.3818981482,
		acceleration: -0.0191666666666663
	},
	{
		id: 1736,
		time: 1735,
		velocity: 1.78777777777778,
		power: 837.815059816095,
		road: 18951.1663888889,
		acceleration: 0.365833333333333
	},
	{
		id: 1737,
		time: 1736,
		velocity: 2.68916666666667,
		power: 2371.5001833963,
		road: 18953.5851388889,
		acceleration: 0.902685185185186
	},
	{
		id: 1738,
		time: 1737,
		velocity: 4.13333333333333,
		power: 5475.52274785498,
		road: 18957.1881481482,
		acceleration: 1.46583333333333
	},
	{
		id: 1739,
		time: 1738,
		velocity: 6.18527777777778,
		power: 6511.79617540085,
		road: 18962.1447685185,
		acceleration: 1.24138888888889
	},
	{
		id: 1740,
		time: 1739,
		velocity: 6.41333333333333,
		power: 8512.93918537978,
		road: 18968.3668981482,
		acceleration: 1.28962962962963
	},
	{
		id: 1741,
		time: 1740,
		velocity: 8.00222222222222,
		power: 11101.2263229891,
		road: 18975.9256944445,
		acceleration: 1.3837037037037
	},
	{
		id: 1742,
		time: 1741,
		velocity: 10.3363888888889,
		power: 16737.2147450159,
		road: 18985.0518518519,
		acceleration: 1.75101851851852
	},
	{
		id: 1743,
		time: 1742,
		velocity: 11.6663888888889,
		power: 15063.9183943479,
		road: 18995.6998611111,
		acceleration: 1.29268518518519
	},
	{
		id: 1744,
		time: 1743,
		velocity: 11.8802777777778,
		power: 9840.5814607505,
		road: 19007.3356944445,
		acceleration: 0.682962962962964
	},
	{
		id: 1745,
		time: 1744,
		velocity: 12.3852777777778,
		power: 5765.56516639902,
		road: 19019.4571759259,
		acceleration: 0.28833333333333
	},
	{
		id: 1746,
		time: 1745,
		velocity: 12.5313888888889,
		power: 4305.19112371686,
		road: 19031.7989814815,
		acceleration: 0.152314814814815
	},
	{
		id: 1747,
		time: 1746,
		velocity: 12.3372222222222,
		power: 2245.99154539689,
		road: 19044.2047222222,
		acceleration: -0.0244444444444429
	},
	{
		id: 1748,
		time: 1747,
		velocity: 12.3119444444444,
		power: 1085.57732484362,
		road: 19056.5377777778,
		acceleration: -0.120925925925926
	},
	{
		id: 1749,
		time: 1748,
		velocity: 12.1686111111111,
		power: 1706.57011855849,
		road: 19068.7775,
		acceleration: -0.0657407407407415
	},
	{
		id: 1750,
		time: 1749,
		velocity: 12.14,
		power: 1800.14393752893,
		road: 19080.9562962963,
		acceleration: -0.0561111111111128
	},
	{
		id: 1751,
		time: 1750,
		velocity: 12.1436111111111,
		power: 3036.10074264498,
		road: 19093.1322222222,
		acceleration: 0.0503703703703717
	},
	{
		id: 1752,
		time: 1751,
		velocity: 12.3197222222222,
		power: 4915.13497339875,
		road: 19105.4362962963,
		acceleration: 0.205925925925927
	},
	{
		id: 1753,
		time: 1752,
		velocity: 12.7577777777778,
		power: 6949.13391250795,
		road: 19118.024212963,
		acceleration: 0.361759259259259
	},
	{
		id: 1754,
		time: 1753,
		velocity: 13.2288888888889,
		power: 8829.21614192005,
		road: 19131.0369907408,
		acceleration: 0.487962962962964
	},
	{
		id: 1755,
		time: 1754,
		velocity: 13.7836111111111,
		power: 7257.91714887812,
		road: 19144.4623611111,
		acceleration: 0.337222222222223
	},
	{
		id: 1756,
		time: 1755,
		velocity: 13.7694444444444,
		power: 4347.2328525317,
		road: 19158.1069907408,
		acceleration: 0.101296296296294
	},
	{
		id: 1757,
		time: 1756,
		velocity: 13.5327777777778,
		power: 1198.1573628867,
		road: 19171.7322685185,
		acceleration: -0.140000000000001
	},
	{
		id: 1758,
		time: 1757,
		velocity: 13.3636111111111,
		power: 2325.00703158139,
		road: 19185.2622222222,
		acceleration: -0.0506481481481487
	},
	{
		id: 1759,
		time: 1758,
		velocity: 13.6175,
		power: 4678.90125442767,
		road: 19198.8318055556,
		acceleration: 0.129907407407408
	},
	{
		id: 1760,
		time: 1759,
		velocity: 13.9225,
		power: 10606.8152682501,
		road: 19212.7471296296,
		acceleration: 0.561574074074073
	},
	{
		id: 1761,
		time: 1760,
		velocity: 15.0483333333333,
		power: 14788.5185530291,
		road: 19227.3494444445,
		acceleration: 0.812407407407409
	},
	{
		id: 1762,
		time: 1761,
		velocity: 16.0547222222222,
		power: 16861.3547315559,
		road: 19242.7979166667,
		acceleration: 0.879907407407405
	},
	{
		id: 1763,
		time: 1762,
		velocity: 16.5622222222222,
		power: 11058.5761721152,
		road: 19258.9084259259,
		acceleration: 0.444166666666666
	},
	{
		id: 1764,
		time: 1763,
		velocity: 16.3808333333333,
		power: 5228.93888775278,
		road: 19275.2689814815,
		acceleration: 0.0559259259259299
	},
	{
		id: 1765,
		time: 1764,
		velocity: 16.2225,
		power: 2471.33842330602,
		road: 19291.5977777778,
		acceleration: -0.119444444444447
	},
	{
		id: 1766,
		time: 1765,
		velocity: 16.2038888888889,
		power: 3393.07232919194,
		road: 19307.8380555556,
		acceleration: -0.0575925925925915
	},
	{
		id: 1767,
		time: 1766,
		velocity: 16.2080555555556,
		power: 4046.73238833826,
		road: 19324.0424074074,
		acceleration: -0.0142592592592621
	},
	{
		id: 1768,
		time: 1767,
		velocity: 16.1797222222222,
		power: 1951.11261335328,
		road: 19340.1659259259,
		acceleration: -0.147407407407407
	},
	{
		id: 1769,
		time: 1768,
		velocity: 15.7616666666667,
		power: 953.130295335447,
		road: 19356.1116203704,
		acceleration: -0.208240740740738
	},
	{
		id: 1770,
		time: 1769,
		velocity: 15.5833333333333,
		power: -217.855563128283,
		road: 19371.8127314815,
		acceleration: -0.280925925925928
	},
	{
		id: 1771,
		time: 1770,
		velocity: 15.3369444444444,
		power: 660.450981049314,
		road: 19387.2647222222,
		acceleration: -0.217314814814813
	},
	{
		id: 1772,
		time: 1771,
		velocity: 15.1097222222222,
		power: 1632.08880507057,
		road: 19402.534537037,
		acceleration: -0.147037037037038
	},
	{
		id: 1773,
		time: 1772,
		velocity: 15.1422222222222,
		power: 3275.17813722401,
		road: 19417.7150462963,
		acceleration: -0.031574074074074
	},
	{
		id: 1774,
		time: 1773,
		velocity: 15.2422222222222,
		power: 5612.58683254154,
		road: 19432.9435185185,
		acceleration: 0.1275
	},
	{
		id: 1775,
		time: 1774,
		velocity: 15.4922222222222,
		power: 6646.35456954774,
		road: 19448.33125,
		acceleration: 0.191018518518518
	},
	{
		id: 1776,
		time: 1775,
		velocity: 15.7152777777778,
		power: 7573.64697597631,
		road: 19463.9360185185,
		acceleration: 0.243055555555557
	},
	{
		id: 1777,
		time: 1776,
		velocity: 15.9713888888889,
		power: 7657.23229493995,
		road: 19479.7806018519,
		acceleration: 0.236574074074072
	},
	{
		id: 1778,
		time: 1777,
		velocity: 16.2019444444444,
		power: 6968.23617998463,
		road: 19495.8341203704,
		acceleration: 0.181296296296297
	},
	{
		id: 1779,
		time: 1778,
		velocity: 16.2591666666667,
		power: 5519.43582053033,
		road: 19512.0190740741,
		acceleration: 0.0815740740740729
	},
	{
		id: 1780,
		time: 1779,
		velocity: 16.2161111111111,
		power: 4476.85367781466,
		road: 19528.2510648148,
		acceleration: 0.0124999999999993
	},
	{
		id: 1781,
		time: 1780,
		velocity: 16.2394444444444,
		power: 4183.88363482639,
		road: 19544.4860648148,
		acceleration: -0.00648148148147953
	},
	{
		id: 1782,
		time: 1781,
		velocity: 16.2397222222222,
		power: 5226.34042263754,
		road: 19560.7476388889,
		acceleration: 0.0596296296296295
	},
	{
		id: 1783,
		time: 1782,
		velocity: 16.395,
		power: 6300.66264207258,
		road: 19577.1013888889,
		acceleration: 0.124722222222221
	},
	{
		id: 1784,
		time: 1783,
		velocity: 16.6136111111111,
		power: 6734.56872292223,
		road: 19593.5906944445,
		acceleration: 0.14638888888889
	},
	{
		id: 1785,
		time: 1784,
		velocity: 16.6788888888889,
		power: 6072.75603987562,
		road: 19610.2028240741,
		acceleration: 0.0992592592592594
	},
	{
		id: 1786,
		time: 1785,
		velocity: 16.6927777777778,
		power: 4406.97545768037,
		road: 19626.8609722222,
		acceleration: -0.00722222222222513
	},
	{
		id: 1787,
		time: 1786,
		velocity: 16.5919444444444,
		power: 3447.14996267535,
		road: 19643.4823611111,
		acceleration: -0.0662962962962936
	},
	{
		id: 1788,
		time: 1787,
		velocity: 16.48,
		power: 1246.88215802028,
		road: 19659.9698148148,
		acceleration: -0.201574074074074
	},
	{
		id: 1789,
		time: 1788,
		velocity: 16.0880555555556,
		power: 1014.67255813981,
		road: 19676.2506481482,
		acceleration: -0.211666666666666
	},
	{
		id: 1790,
		time: 1789,
		velocity: 15.9569444444444,
		power: 328.846485819442,
		road: 19692.3000462963,
		acceleration: -0.251203703703704
	},
	{
		id: 1791,
		time: 1790,
		velocity: 15.7263888888889,
		power: 528.193639263415,
		road: 19708.1071759259,
		acceleration: -0.233333333333334
	},
	{
		id: 1792,
		time: 1791,
		velocity: 15.3880555555556,
		power: -4406.7585028767,
		road: 19723.5173611111,
		acceleration: -0.560555555555554
	},
	{
		id: 1793,
		time: 1792,
		velocity: 14.2752777777778,
		power: -9946.68431401898,
		road: 19738.1676388889,
		acceleration: -0.959259259259261
	},
	{
		id: 1794,
		time: 1793,
		velocity: 12.8486111111111,
		power: -14762.5762114951,
		road: 19751.6497222222,
		acceleration: -1.37712962962963
	},
	{
		id: 1795,
		time: 1794,
		velocity: 11.2566666666667,
		power: -12033.7601854563,
		road: 19763.8193981482,
		acceleration: -1.24768518518519
	},
	{
		id: 1796,
		time: 1795,
		velocity: 10.5322222222222,
		power: -8098.53753303999,
		road: 19774.8830555556,
		acceleration: -0.964351851851852
	},
	{
		id: 1797,
		time: 1796,
		velocity: 9.95555555555556,
		power: -6336.97219573266,
		road: 19785.0444444445,
		acceleration: -0.840185185185186
	},
	{
		id: 1798,
		time: 1797,
		velocity: 8.73611111111111,
		power: -4421.18360790955,
		road: 19794.4503240741,
		acceleration: -0.670833333333333
	},
	{
		id: 1799,
		time: 1798,
		velocity: 8.51972222222222,
		power: -2771.45982944234,
		road: 19803.2701388889,
		acceleration: -0.501296296296298
	},
	{
		id: 1800,
		time: 1799,
		velocity: 8.45166666666667,
		power: -2000.19082144073,
		road: 19811.6302314815,
		acceleration: -0.418148148148147
	},
	{
		id: 1801,
		time: 1800,
		velocity: 7.48166666666667,
		power: -6402.38175242266,
		road: 19819.2601388889,
		acceleration: -1.04222222222222
	},
	{
		id: 1802,
		time: 1801,
		velocity: 5.39305555555555,
		power: -8105.57481885556,
		road: 19825.6238888889,
		acceleration: -1.49009259259259
	},
	{
		id: 1803,
		time: 1802,
		velocity: 3.98138888888889,
		power: -7838.90171747192,
		road: 19830.2878240741,
		acceleration: -1.90953703703704
	},
	{
		id: 1804,
		time: 1803,
		velocity: 1.75305555555556,
		power: -4236.9660372738,
		road: 19833.1510648148,
		acceleration: -1.69185185185185
	},
	{
		id: 1805,
		time: 1804,
		velocity: 0.3175,
		power: -1538.13644604902,
		road: 19834.5048148148,
		acceleration: -1.32712962962963
	},
	{
		id: 1806,
		time: 1805,
		velocity: 0,
		power: -172.247468506733,
		road: 19834.9028240741,
		acceleration: -0.584351851851852
	},
	{
		id: 1807,
		time: 1806,
		velocity: 0,
		power: 1.08768815789474,
		road: 19834.9557407408,
		acceleration: -0.105833333333333
	},
	{
		id: 1808,
		time: 1807,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1809,
		time: 1808,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1810,
		time: 1809,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1811,
		time: 1810,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1812,
		time: 1811,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1813,
		time: 1812,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1814,
		time: 1813,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1815,
		time: 1814,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1816,
		time: 1815,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1817,
		time: 1816,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1818,
		time: 1817,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1819,
		time: 1818,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1820,
		time: 1819,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1821,
		time: 1820,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1822,
		time: 1821,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1823,
		time: 1822,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1824,
		time: 1823,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1825,
		time: 1824,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1826,
		time: 1825,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1827,
		time: 1826,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1828,
		time: 1827,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1829,
		time: 1828,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1830,
		time: 1829,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1831,
		time: 1830,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1832,
		time: 1831,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1833,
		time: 1832,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1834,
		time: 1833,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1835,
		time: 1834,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1836,
		time: 1835,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1837,
		time: 1836,
		velocity: 0,
		power: 0,
		road: 19834.9557407408,
		acceleration: 0
	},
	{
		id: 1838,
		time: 1837,
		velocity: 0,
		power: 1.79849816758545,
		road: 19834.9681944445,
		acceleration: 0.0249074074074074
	},
	{
		id: 1839,
		time: 1838,
		velocity: 0.0747222222222222,
		power: 3.00926891476805,
		road: 19834.9931018519,
		acceleration: 0
	},
	{
		id: 1840,
		time: 1839,
		velocity: 0,
		power: 3.00926891476805,
		road: 19835.0180092593,
		acceleration: 0
	},
	{
		id: 1841,
		time: 1840,
		velocity: 0,
		power: 1.21076655295647,
		road: 19835.030462963,
		acceleration: -0.0249074074074074
	},
	{
		id: 1842,
		time: 1841,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1843,
		time: 1842,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1844,
		time: 1843,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1845,
		time: 1844,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1846,
		time: 1845,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1847,
		time: 1846,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1848,
		time: 1847,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1849,
		time: 1848,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1850,
		time: 1849,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1851,
		time: 1850,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1852,
		time: 1851,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1853,
		time: 1852,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1854,
		time: 1853,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1855,
		time: 1854,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1856,
		time: 1855,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1857,
		time: 1856,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1858,
		time: 1857,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1859,
		time: 1858,
		velocity: 0,
		power: 0,
		road: 19835.030462963,
		acceleration: 0
	},
	{
		id: 1860,
		time: 1859,
		velocity: 0,
		power: 94.0368446111546,
		road: 19835.2236111111,
		acceleration: 0.386296296296296
	},
	{
		id: 1861,
		time: 1860,
		velocity: 1.15888888888889,
		power: 1036.94636475496,
		road: 19836.1416666667,
		acceleration: 1.06351851851852
	},
	{
		id: 1862,
		time: 1861,
		velocity: 3.19055555555556,
		power: 4098.23590817589,
		road: 19838.4584259259,
		acceleration: 1.73388888888889
	},
	{
		id: 1863,
		time: 1862,
		velocity: 5.20166666666667,
		power: 7419.7642746675,
		road: 19842.5325,
		acceleration: 1.78074074074074
	},
	{
		id: 1864,
		time: 1863,
		velocity: 6.50111111111111,
		power: 7170.29093174647,
		road: 19848.1017592593,
		acceleration: 1.20962962962963
	},
	{
		id: 1865,
		time: 1864,
		velocity: 6.81944444444444,
		power: 6792.14431572471,
		road: 19854.7378240741,
		acceleration: 0.923981481481481
	},
	{
		id: 1866,
		time: 1865,
		velocity: 7.97361111111111,
		power: 5139.20419146174,
		road: 19862.1225462963,
		acceleration: 0.573333333333334
	},
	{
		id: 1867,
		time: 1866,
		velocity: 8.22111111111111,
		power: 5158.07689848744,
		road: 19870.0541666667,
		acceleration: 0.520462962962962
	},
	{
		id: 1868,
		time: 1867,
		velocity: 8.38083333333333,
		power: 2567.95104340602,
		road: 19878.3261111111,
		acceleration: 0.160185185185187
	},
	{
		id: 1869,
		time: 1868,
		velocity: 8.45416666666667,
		power: 1018.2977482585,
		road: 19886.6590740741,
		acceleration: -0.0381481481481476
	},
	{
		id: 1870,
		time: 1869,
		velocity: 8.10666666666667,
		power: -4862.33846821552,
		road: 19894.5686111111,
		acceleration: -0.808703703703705
	},
	{
		id: 1871,
		time: 1870,
		velocity: 5.95472222222222,
		power: -5176.96154326708,
		road: 19901.6095833333,
		acceleration: -0.928425925925925
	},
	{
		id: 1872,
		time: 1871,
		velocity: 5.66888888888889,
		power: -1174.94201124926,
		road: 19908.0146296296,
		acceleration: -0.343425925925925
	},
	{
		id: 1873,
		time: 1872,
		velocity: 7.07638888888889,
		power: 7010.87641452792,
		road: 19914.7211574074,
		acceleration: 0.946388888888888
	},
	{
		id: 1874,
		time: 1873,
		velocity: 8.79388888888889,
		power: 18367.8979502817,
		road: 19922.9851388889,
		acceleration: 2.16851851851852
	},
	{
		id: 1875,
		time: 1874,
		velocity: 12.1744444444444,
		power: 18752.5888504863,
		road: 19933.2030092593,
		acceleration: 1.73925925925926
	},
	{
		id: 1876,
		time: 1875,
		velocity: 12.2941666666667,
		power: 14146.6688182116,
		road: 19944.826712963,
		acceleration: 1.07240740740741
	},
	{
		id: 1877,
		time: 1876,
		velocity: 12.0111111111111,
		power: 2122.69724799088,
		road: 19956.9729166667,
		acceleration: -0.0274074074074093
	},
	{
		id: 1878,
		time: 1877,
		velocity: 12.0922222222222,
		power: 1315.51475781128,
		road: 19969.0575925926,
		acceleration: -0.0956481481481468
	},
	{
		id: 1879,
		time: 1878,
		velocity: 12.0072222222222,
		power: 2330.85586773048,
		road: 19981.0914351852,
		acceleration: -0.00601851851851976
	},
	{
		id: 1880,
		time: 1879,
		velocity: 11.9930555555556,
		power: 1414.97008359906,
		road: 19993.0799074074,
		acceleration: -0.0847222222222221
	},
	{
		id: 1881,
		time: 1880,
		velocity: 11.8380555555556,
		power: -4999.76741502671,
		road: 20004.6986111111,
		acceleration: -0.654814814814813
	},
	{
		id: 1882,
		time: 1881,
		velocity: 10.0427777777778,
		power: -9979.98721795279,
		road: 20015.4048148148,
		acceleration: -1.17018518518518
	},
	{
		id: 1883,
		time: 1882,
		velocity: 8.4825,
		power: -12799.0957708827,
		road: 20024.7159722222,
		acceleration: -1.61990740740741
	},
	{
		id: 1884,
		time: 1883,
		velocity: 6.97833333333333,
		power: -12706.8844137804,
		road: 20032.2510185185,
		acceleration: -1.93231481481481
	},
	{
		id: 1885,
		time: 1884,
		velocity: 4.24583333333333,
		power: -9592.51741211187,
		road: 20037.845,
		acceleration: -1.94981481481481
	},
	{
		id: 1886,
		time: 1885,
		velocity: 2.63305555555556,
		power: -7010.81284433174,
		road: 20041.34,
		acceleration: -2.24814814814815
	},
	{
		id: 1887,
		time: 1886,
		velocity: 0.233888888888889,
		power: -2028.33828209325,
		road: 20043.003287037,
		acceleration: -1.41527777777778
	},
	{
		id: 1888,
		time: 1887,
		velocity: 0,
		power: -367.278221554534,
		road: 20043.5200925926,
		acceleration: -0.877685185185185
	},
	{
		id: 1889,
		time: 1888,
		velocity: 0,
		power: 1.83050198180637,
		road: 20043.5590740741,
		acceleration: -0.077962962962963
	},
	{
		id: 1890,
		time: 1889,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1891,
		time: 1890,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1892,
		time: 1891,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1893,
		time: 1892,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1894,
		time: 1893,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1895,
		time: 1894,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1896,
		time: 1895,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1897,
		time: 1896,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1898,
		time: 1897,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1899,
		time: 1898,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1900,
		time: 1899,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1901,
		time: 1900,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1902,
		time: 1901,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1903,
		time: 1902,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1904,
		time: 1903,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1905,
		time: 1904,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1906,
		time: 1905,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1907,
		time: 1906,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1908,
		time: 1907,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1909,
		time: 1908,
		velocity: 0,
		power: 0,
		road: 20043.5590740741,
		acceleration: 0
	},
	{
		id: 1910,
		time: 1909,
		velocity: 0,
		power: 0.119252779773283,
		road: 20043.5600462963,
		acceleration: 0.00194444444444444
	},
	{
		id: 1911,
		time: 1910,
		velocity: 0.00583333333333333,
		power: 0.234923688201537,
		road: 20043.5619907408,
		acceleration: 0
	},
	{
		id: 1912,
		time: 1911,
		velocity: 0,
		power: 0.234923688201537,
		road: 20043.5639351852,
		acceleration: 0
	},
	{
		id: 1913,
		time: 1912,
		velocity: 0,
		power: 0.115670906432749,
		road: 20043.5649074074,
		acceleration: -0.00194444444444444
	},
	{
		id: 1914,
		time: 1913,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1915,
		time: 1914,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1916,
		time: 1915,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1917,
		time: 1916,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1918,
		time: 1917,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1919,
		time: 1918,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1920,
		time: 1919,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1921,
		time: 1920,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1922,
		time: 1921,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1923,
		time: 1922,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1924,
		time: 1923,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1925,
		time: 1924,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1926,
		time: 1925,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1927,
		time: 1926,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1928,
		time: 1927,
		velocity: 0,
		power: 0,
		road: 20043.5649074074,
		acceleration: 0
	},
	{
		id: 1929,
		time: 1928,
		velocity: 0,
		power: 71.3217700063269,
		road: 20043.7296296296,
		acceleration: 0.329444444444444
	},
	{
		id: 1930,
		time: 1929,
		velocity: 0.988333333333333,
		power: 852.613399324342,
		road: 20044.5460185185,
		acceleration: 0.973888888888889
	},
	{
		id: 1931,
		time: 1930,
		velocity: 2.92166666666667,
		power: 2837.26229319503,
		road: 20046.5360185185,
		acceleration: 1.37333333333333
	},
	{
		id: 1932,
		time: 1931,
		velocity: 4.12,
		power: 5255.8444614668,
		road: 20049.9552314815,
		acceleration: 1.48509259259259
	},
	{
		id: 1933,
		time: 1932,
		velocity: 5.44361111111111,
		power: 4303.08483697827,
		road: 20054.54125,
		acceleration: 0.848518518518518
	},
	{
		id: 1934,
		time: 1933,
		velocity: 5.46722222222222,
		power: 4931.2171737865,
		road: 20059.9584722222,
		acceleration: 0.813888888888889
	},
	{
		id: 1935,
		time: 1934,
		velocity: 6.56166666666667,
		power: 4109.83388237968,
		road: 20066.0625462963,
		acceleration: 0.559814814814814
	},
	{
		id: 1936,
		time: 1935,
		velocity: 7.12305555555556,
		power: 4944.52281800245,
		road: 20072.7584259259,
		acceleration: 0.623796296296297
	},
	{
		id: 1937,
		time: 1936,
		velocity: 7.33861111111111,
		power: 2360.22071743477,
		road: 20079.8629166667,
		acceleration: 0.193425925925926
	},
	{
		id: 1938,
		time: 1937,
		velocity: 7.14194444444444,
		power: 987.209593966086,
		road: 20087.057962963,
		acceleration: -0.0123148148148147
	},
	{
		id: 1939,
		time: 1938,
		velocity: 7.08611111111111,
		power: 225.283101980308,
		road: 20094.185462963,
		acceleration: -0.122777777777777
	},
	{
		id: 1940,
		time: 1939,
		velocity: 6.97027777777778,
		power: 820.251326582599,
		road: 20101.2350462963,
		acceleration: -0.0330555555555563
	},
	{
		id: 1941,
		time: 1940,
		velocity: 7.04277777777778,
		power: 317.879228596645,
		road: 20108.2146296296,
		acceleration: -0.106944444444444
	},
	{
		id: 1942,
		time: 1941,
		velocity: 6.76527777777778,
		power: 903.997379650206,
		road: 20115.1322685185,
		acceleration: -0.0169444444444444
	},
	{
		id: 1943,
		time: 1942,
		velocity: 6.91944444444444,
		power: 2522.49841839468,
		road: 20122.1527314815,
		acceleration: 0.222592592592592
	},
	{
		id: 1944,
		time: 1943,
		velocity: 7.71055555555555,
		power: 3945.85306326021,
		road: 20129.4883333334,
		acceleration: 0.407685185185185
	},
	{
		id: 1945,
		time: 1944,
		velocity: 7.98833333333333,
		power: 6500.94497994374,
		road: 20137.3793518519,
		acceleration: 0.703148148148148
	},
	{
		id: 1946,
		time: 1945,
		velocity: 9.02888888888889,
		power: 4597.21996498103,
		road: 20145.8240740741,
		acceleration: 0.404259259259259
	},
	{
		id: 1947,
		time: 1946,
		velocity: 8.92333333333333,
		power: 4841.94867798572,
		road: 20154.6725,
		acceleration: 0.403148148148148
	},
	{
		id: 1948,
		time: 1947,
		velocity: 9.19777777777778,
		power: 1078.25679577888,
		road: 20163.6985648148,
		acceleration: -0.0478703703703705
	},
	{
		id: 1949,
		time: 1948,
		velocity: 8.88527777777778,
		power: 1259.25752434729,
		road: 20172.6877777778,
		acceleration: -0.0258333333333312
	},
	{
		id: 1950,
		time: 1949,
		velocity: 8.84583333333333,
		power: 2767.82513350129,
		road: 20181.7378703704,
		acceleration: 0.147592592592591
	},
	{
		id: 1951,
		time: 1950,
		velocity: 9.64055555555556,
		power: 5768.96188890019,
		road: 20191.0969444445,
		acceleration: 0.47037037037037
	},
	{
		id: 1952,
		time: 1951,
		velocity: 10.2963888888889,
		power: 8479.71010877315,
		road: 20201.0468055556,
		acceleration: 0.711203703703704
	},
	{
		id: 1953,
		time: 1952,
		velocity: 10.9794444444444,
		power: 6605.09771469385,
		road: 20211.5860185185,
		acceleration: 0.467500000000001
	},
	{
		id: 1954,
		time: 1953,
		velocity: 11.0430555555556,
		power: 5793.82502361398,
		road: 20222.5388888889,
		acceleration: 0.359814814814813
	},
	{
		id: 1955,
		time: 1954,
		velocity: 11.3758333333333,
		power: 5207.62069941822,
		road: 20233.8143055556,
		acceleration: 0.285277777777779
	},
	{
		id: 1956,
		time: 1955,
		velocity: 11.8352777777778,
		power: 9027.67032794971,
		road: 20245.53375,
		acceleration: 0.602777777777778
	},
	{
		id: 1957,
		time: 1956,
		velocity: 12.8513888888889,
		power: 14200.1622823936,
		road: 20258.0416203704,
		acceleration: 0.974074074074071
	},
	{
		id: 1958,
		time: 1957,
		velocity: 14.2980555555556,
		power: 16797.6520841015,
		road: 20271.5713425926,
		acceleration: 1.06962962962963
	},
	{
		id: 1959,
		time: 1958,
		velocity: 15.0441666666667,
		power: 12366.2285515914,
		road: 20285.9635185185,
		acceleration: 0.65527777777778
	},
	{
		id: 1960,
		time: 1959,
		velocity: 14.8172222222222,
		power: 4620.99760369946,
		road: 20300.7220833333,
		acceleration: 0.0775000000000006
	},
	{
		id: 1961,
		time: 1960,
		velocity: 14.5305555555556,
		power: -1536.66730080306,
		road: 20315.3404166667,
		acceleration: -0.357962962962965
	},
	{
		id: 1962,
		time: 1961,
		velocity: 13.9702777777778,
		power: -2256.6935392274,
		road: 20329.5759259259,
		acceleration: -0.407685185185185
	},
	{
		id: 1963,
		time: 1962,
		velocity: 13.5941666666667,
		power: -2674.15145298294,
		road: 20343.3887037037,
		acceleration: -0.437777777777777
	},
	{
		id: 1964,
		time: 1963,
		velocity: 13.2172222222222,
		power: -2097.95709866879,
		road: 20356.78625,
		acceleration: -0.392685185185186
	},
	{
		id: 1965,
		time: 1964,
		velocity: 12.7922222222222,
		power: -2231.57720971525,
		road: 20369.7861574074,
		acceleration: -0.402592592592594
	},
	{
		id: 1966,
		time: 1965,
		velocity: 12.3863888888889,
		power: -1812.93432410189,
		road: 20382.4008796296,
		acceleration: -0.367777777777777
	},
	{
		id: 1967,
		time: 1966,
		velocity: 12.1138888888889,
		power: -493.393369021702,
		road: 20394.7043055556,
		acceleration: -0.254814814814814
	},
	{
		id: 1968,
		time: 1967,
		velocity: 12.0277777777778,
		power: 1232.91447820116,
		road: 20406.8284722222,
		acceleration: -0.103703703703703
	},
	{
		id: 1969,
		time: 1968,
		velocity: 12.0752777777778,
		power: 2670.57126667873,
		road: 20418.9117592593,
		acceleration: 0.0219444444444434
	},
	{
		id: 1970,
		time: 1969,
		velocity: 12.1797222222222,
		power: 967.80932083951,
		road: 20430.9436574074,
		acceleration: -0.124722222222221
	},
	{
		id: 1971,
		time: 1970,
		velocity: 11.6536111111111,
		power: -2223.28206529936,
		road: 20442.7113888889,
		acceleration: -0.403611111111111
	},
	{
		id: 1972,
		time: 1971,
		velocity: 10.8644444444444,
		power: -5007.8995861909,
		road: 20453.944212963,
		acceleration: -0.666203703703703
	},
	{
		id: 1973,
		time: 1972,
		velocity: 10.1811111111111,
		power: -7062.97528997226,
		road: 20464.3948148148,
		acceleration: -0.898240740740741
	},
	{
		id: 1974,
		time: 1973,
		velocity: 8.95888888888889,
		power: -7864.64450772524,
		road: 20473.8715740741,
		acceleration: -1.04944444444445
	},
	{
		id: 1975,
		time: 1974,
		velocity: 7.71611111111111,
		power: -8698.96882241751,
		road: 20482.1910648148,
		acceleration: -1.26509259259259
	},
	{
		id: 1976,
		time: 1975,
		velocity: 6.38583333333333,
		power: -7266.61044749672,
		road: 20489.2597685185,
		acceleration: -1.23648148148148
	},
	{
		id: 1977,
		time: 1976,
		velocity: 5.24944444444444,
		power: -5270.48620178873,
		road: 20495.1673611111,
		acceleration: -1.08574074074074
	},
	{
		id: 1978,
		time: 1977,
		velocity: 4.45888888888889,
		power: -289.836705093754,
		road: 20500.4316203704,
		acceleration: -0.200925925925926
	},
	{
		id: 1979,
		time: 1978,
		velocity: 5.78305555555556,
		power: 2723.06290372151,
		road: 20505.7909722222,
		acceleration: 0.39111111111111
	},
	{
		id: 1980,
		time: 1979,
		velocity: 6.42277777777778,
		power: 4869.48097842584,
		road: 20511.7053703704,
		acceleration: 0.718981481481482
	},
	{
		id: 1981,
		time: 1980,
		velocity: 6.61583333333333,
		power: 3091.78885321731,
		road: 20518.1558796296,
		acceleration: 0.353240740740741
	},
	{
		id: 1982,
		time: 1981,
		velocity: 6.84277777777778,
		power: 2986.52985498266,
		road: 20524.9378703704,
		acceleration: 0.309722222222222
	},
	{
		id: 1983,
		time: 1982,
		velocity: 7.35194444444444,
		power: 4843.17504318245,
		road: 20532.1493518519,
		acceleration: 0.54925925925926
	},
	{
		id: 1984,
		time: 1983,
		velocity: 8.26361111111111,
		power: 7723.9584354967,
		road: 20540.0666203704,
		acceleration: 0.862314814814814
	},
	{
		id: 1985,
		time: 1984,
		velocity: 9.42972222222222,
		power: 9117.42163400883,
		road: 20548.8731018519,
		acceleration: 0.916111111111112
	},
	{
		id: 1986,
		time: 1985,
		velocity: 10.1002777777778,
		power: 6555.25917146121,
		road: 20558.4091203704,
		acceleration: 0.542962962962962
	},
	{
		id: 1987,
		time: 1986,
		velocity: 9.8925,
		power: 2077.98811643006,
		road: 20568.2366666667,
		acceleration: 0.0400925925925932
	},
	{
		id: 1988,
		time: 1987,
		velocity: 9.55,
		power: -34.9469503905706,
		road: 20577.9918518519,
		acceleration: -0.184814814814814
	},
	{
		id: 1989,
		time: 1988,
		velocity: 9.54583333333333,
		power: 3660.58238572598,
		road: 20587.7606944445,
		acceleration: 0.212129629629629
	},
	{
		id: 1990,
		time: 1989,
		velocity: 10.5288888888889,
		power: 8052.28194206636,
		road: 20597.9569444445,
		acceleration: 0.642685185185185
	},
	{
		id: 1991,
		time: 1990,
		velocity: 11.4780555555556,
		power: 12088.4292497847,
		road: 20608.9532407408,
		acceleration: 0.957407407407407
	},
	{
		id: 1992,
		time: 1991,
		velocity: 12.4180555555556,
		power: 11985.1205511653,
		road: 20620.8525462963,
		acceleration: 0.848611111111111
	},
	{
		id: 1993,
		time: 1992,
		velocity: 13.0747222222222,
		power: 11057.194068234,
		road: 20633.5243518519,
		acceleration: 0.69638888888889
	},
	{
		id: 1994,
		time: 1993,
		velocity: 13.5672222222222,
		power: 8781.39188437172,
		road: 20646.7781481482,
		acceleration: 0.46759259259259
	},
	{
		id: 1995,
		time: 1994,
		velocity: 13.8208333333333,
		power: 6188.03794679356,
		road: 20660.3879166667,
		acceleration: 0.244351851851853
	},
	{
		id: 1996,
		time: 1995,
		velocity: 13.8077777777778,
		power: 2887.17678157538,
		road: 20674.1131944445,
		acceleration: -0.0133333333333336
	},
	{
		id: 1997,
		time: 1996,
		velocity: 13.5272222222222,
		power: 349.334432826093,
		road: 20687.7292592593,
		acceleration: -0.205092592592592
	},
	{
		id: 1998,
		time: 1997,
		velocity: 13.2055555555556,
		power: -1398.01466392006,
		road: 20701.0739814815,
		acceleration: -0.337592592592594
	},
	{
		id: 1999,
		time: 1998,
		velocity: 12.795,
		power: -1880.4716585237,
		road: 20714.0627777778,
		acceleration: -0.374259259259256
	},
	{
		id: 2000,
		time: 1999,
		velocity: 12.4044444444444,
		power: -2822.68529689681,
		road: 20726.6385185185,
		acceleration: -0.451851851851853
	},
	{
		id: 2001,
		time: 2000,
		velocity: 11.85,
		power: -1072.79771379007,
		road: 20738.8365740741,
		acceleration: -0.303518518518519
	},
	{
		id: 2002,
		time: 2001,
		velocity: 11.8844444444444,
		power: 122.870756555672,
		road: 20750.7843055556,
		acceleration: -0.197129629629629
	},
	{
		id: 2003,
		time: 2002,
		velocity: 11.8130555555556,
		power: 2879.904962404,
		road: 20762.6571759259,
		acceleration: 0.0474074074074053
	},
	{
		id: 2004,
		time: 2003,
		velocity: 11.9922222222222,
		power: 4278.86642078457,
		road: 20774.6368055556,
		acceleration: 0.166111111111114
	},
	{
		id: 2005,
		time: 2004,
		velocity: 12.3827777777778,
		power: 5096.14453204961,
		road: 20786.8133333333,
		acceleration: 0.227685185185182
	},
	{
		id: 2006,
		time: 2005,
		velocity: 12.4961111111111,
		power: 4027.95497488044,
		road: 20799.1678240741,
		acceleration: 0.128240740740742
	},
	{
		id: 2007,
		time: 2006,
		velocity: 12.3769444444444,
		power: 2450.9886217392,
		road: 20811.5827314815,
		acceleration: -0.0074074074074062
	},
	{
		id: 2008,
		time: 2007,
		velocity: 12.3605555555556,
		power: 273.012632664971,
		road: 20823.8990740741,
		acceleration: -0.189722222222223
	},
	{
		id: 2009,
		time: 2008,
		velocity: 11.9269444444444,
		power: 175.929031201115,
		road: 20836.0230092593,
		acceleration: -0.195092592592593
	},
	{
		id: 2010,
		time: 2009,
		velocity: 11.7916666666667,
		power: -914.004814908805,
		road: 20847.9055555556,
		acceleration: -0.287685185185184
	},
	{
		id: 2011,
		time: 2010,
		velocity: 11.4975,
		power: -61.253138854935,
		road: 20859.5396296296,
		acceleration: -0.209259259259259
	},
	{
		id: 2012,
		time: 2011,
		velocity: 11.2991666666667,
		power: -2253.39606497995,
		road: 20870.8648611111,
		acceleration: -0.408425925925927
	},
	{
		id: 2013,
		time: 2012,
		velocity: 10.5663888888889,
		power: -3445.92949639315,
		road: 20881.7224537037,
		acceleration: -0.526851851851852
	},
	{
		id: 2014,
		time: 2013,
		velocity: 9.91694444444444,
		power: -3916.94337543066,
		road: 20892.0234722222,
		acceleration: -0.586296296296297
	},
	{
		id: 2015,
		time: 2014,
		velocity: 9.54027777777778,
		power: -3576.76796706391,
		road: 20901.7479166667,
		acceleration: -0.566851851851851
	},
	{
		id: 2016,
		time: 2015,
		velocity: 8.86583333333333,
		power: -2890.72408636233,
		road: 20910.93625,
		acceleration: -0.50537037037037
	},
	{
		id: 2017,
		time: 2016,
		velocity: 8.40083333333333,
		power: -3077.8858130299,
		road: 20919.600462963,
		acceleration: -0.54287037037037
	},
	{
		id: 2018,
		time: 2017,
		velocity: 7.91166666666667,
		power: -2848.03224185196,
		road: 20927.7268055556,
		acceleration: -0.532870370370371
	},
	{
		id: 2019,
		time: 2018,
		velocity: 7.26722222222222,
		power: -2449.65168831747,
		road: 20935.3375462963,
		acceleration: -0.498333333333334
	},
	{
		id: 2020,
		time: 2019,
		velocity: 6.90583333333333,
		power: -2648.05053399734,
		road: 20942.4248611111,
		acceleration: -0.548518518518519
	},
	{
		id: 2021,
		time: 2020,
		velocity: 6.26611111111111,
		power: -1949.11265842331,
		road: 20949.0062962963,
		acceleration: -0.463240740740741
	},
	{
		id: 2022,
		time: 2021,
		velocity: 5.8775,
		power: -4730.86959043069,
		road: 20954.8574074074,
		acceleration: -0.997407407407406
	},
	{
		id: 2023,
		time: 2022,
		velocity: 3.91361111111111,
		power: -4516.57150939785,
		road: 20959.6428240741,
		acceleration: -1.13398148148148
	},
	{
		id: 2024,
		time: 2023,
		velocity: 2.86416666666667,
		power: -4736.23822392839,
		road: 20963.0650925926,
		acceleration: -1.59231481481481
	},
	{
		id: 2025,
		time: 2024,
		velocity: 1.10055555555556,
		power: -2199.08002765953,
		road: 20965.0389351852,
		acceleration: -1.30453703703704
	},
	{
		id: 2026,
		time: 2025,
		velocity: 0,
		power: -661.510698055787,
		road: 20965.8831481482,
		acceleration: -0.954722222222222
	},
	{
		id: 2027,
		time: 2026,
		velocity: 0,
		power: -41.5874200454841,
		road: 20966.0665740741,
		acceleration: -0.366851851851852
	},
	{
		id: 2028,
		time: 2027,
		velocity: 0,
		power: 0,
		road: 20966.0665740741,
		acceleration: 0
	},
	{
		id: 2029,
		time: 2028,
		velocity: 0,
		power: 0,
		road: 20966.0665740741,
		acceleration: 0
	},
	{
		id: 2030,
		time: 2029,
		velocity: 0,
		power: 0,
		road: 20966.0665740741,
		acceleration: 0
	},
	{
		id: 2031,
		time: 2030,
		velocity: 0,
		power: 0,
		road: 20966.0665740741,
		acceleration: 0
	},
	{
		id: 2032,
		time: 2031,
		velocity: 0,
		power: 0,
		road: 20966.0665740741,
		acceleration: 0
	},
	{
		id: 2033,
		time: 2032,
		velocity: 0,
		power: 0,
		road: 20966.0665740741,
		acceleration: 0
	},
	{
		id: 2034,
		time: 2033,
		velocity: 0,
		power: 0,
		road: 20966.0665740741,
		acceleration: 0
	},
	{
		id: 2035,
		time: 2034,
		velocity: 0,
		power: 0,
		road: 20966.0665740741,
		acceleration: 0
	},
	{
		id: 2036,
		time: 2035,
		velocity: 0,
		power: 73.0569404709433,
		road: 20966.2336111111,
		acceleration: 0.334074074074074
	},
	{
		id: 2037,
		time: 2036,
		velocity: 1.00222222222222,
		power: 1450.17685573106,
		road: 20967.2535185185,
		acceleration: 1.37166666666667
	},
	{
		id: 2038,
		time: 2037,
		velocity: 4.115,
		power: 5545.31138932055,
		road: 20969.969212963,
		acceleration: 2.01990740740741
	},
	{
		id: 2039,
		time: 2038,
		velocity: 6.05972222222222,
		power: 8841.58915775974,
		road: 20974.624537037,
		acceleration: 1.85935185185185
	},
	{
		id: 2040,
		time: 2039,
		velocity: 6.58027777777778,
		power: 8881.38987339012,
		road: 20980.8812037037,
		acceleration: 1.34333333333333
	},
	{
		id: 2041,
		time: 2040,
		velocity: 8.145,
		power: 7265.80850247241,
		road: 20988.2487962963,
		acceleration: 0.878518518518518
	},
	{
		id: 2042,
		time: 2041,
		velocity: 8.69527777777778,
		power: 5453.56868164911,
		road: 20996.3281481482,
		acceleration: 0.544999999999999
	},
	{
		id: 2043,
		time: 2042,
		velocity: 8.21527777777778,
		power: -775.02350689175,
		road: 21004.5477314815,
		acceleration: -0.264537037037035
	},
	{
		id: 2044,
		time: 2043,
		velocity: 7.35138888888889,
		power: -5043.38615061257,
		road: 21012.2088425926,
		acceleration: -0.852407407407408
	},
	{
		id: 2045,
		time: 2044,
		velocity: 6.13805555555556,
		power: -7754.42404125031,
		road: 21018.7439814815,
		acceleration: -1.39953703703704
	},
	{
		id: 2046,
		time: 2045,
		velocity: 4.01666666666667,
		power: -8617.56275491222,
		road: 21023.56875,
		acceleration: -2.0212037037037
	},
	{
		id: 2047,
		time: 2046,
		velocity: 1.28777777777778,
		power: -5068.23452125883,
		road: 21026.3599074074,
		acceleration: -2.04601851851852
	},
	{
		id: 2048,
		time: 2047,
		velocity: 0,
		power: -1260.76593527861,
		road: 21027.4586111111,
		acceleration: -1.33888888888889
	},
	{
		id: 2049,
		time: 2048,
		velocity: 0,
		power: -61.3516160493827,
		road: 21027.6732407408,
		acceleration: -0.429259259259259
	},
	{
		id: 2050,
		time: 2049,
		velocity: 0,
		power: 0,
		road: 21027.6732407408,
		acceleration: 0
	},
	{
		id: 2051,
		time: 2050,
		velocity: 0,
		power: 0,
		road: 21027.6732407408,
		acceleration: 0
	},
	{
		id: 2052,
		time: 2051,
		velocity: 0,
		power: 0,
		road: 21027.6732407408,
		acceleration: 0
	},
	{
		id: 2053,
		time: 2052,
		velocity: 0,
		power: 0,
		road: 21027.6732407408,
		acceleration: 0
	},
	{
		id: 2054,
		time: 2053,
		velocity: 0,
		power: 0,
		road: 21027.6732407408,
		acceleration: 0
	},
	{
		id: 2055,
		time: 2054,
		velocity: 0,
		power: 0,
		road: 21027.6732407408,
		acceleration: 0
	},
	{
		id: 2056,
		time: 2055,
		velocity: 0,
		power: 0,
		road: 21027.6732407408,
		acceleration: 0
	},
	{
		id: 2057,
		time: 2056,
		velocity: 0,
		power: 0,
		road: 21027.6732407408,
		acceleration: 0
	},
	{
		id: 2058,
		time: 2057,
		velocity: 0,
		power: 0,
		road: 21027.6732407408,
		acceleration: 0
	},
	{
		id: 2059,
		time: 2058,
		velocity: 0,
		power: 0,
		road: 21027.6732407408,
		acceleration: 0
	},
	{
		id: 2060,
		time: 2059,
		velocity: 0,
		power: 5.53760498936375,
		road: 21027.7041203704,
		acceleration: 0.0617592592592593
	},
	{
		id: 2061,
		time: 2060,
		velocity: 0.185277777777778,
		power: 7.46175156417258,
		road: 21027.7658796296,
		acceleration: 0
	},
	{
		id: 2062,
		time: 2061,
		velocity: 0,
		power: 7.46175156417258,
		road: 21027.8276388889,
		acceleration: 0
	},
	{
		id: 2063,
		time: 2062,
		velocity: 0,
		power: 1.92408263482781,
		road: 21027.8585185185,
		acceleration: -0.0617592592592593
	},
	{
		id: 2064,
		time: 2063,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2065,
		time: 2064,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2066,
		time: 2065,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2067,
		time: 2066,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2068,
		time: 2067,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2069,
		time: 2068,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2070,
		time: 2069,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2071,
		time: 2070,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2072,
		time: 2071,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2073,
		time: 2072,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2074,
		time: 2073,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2075,
		time: 2074,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2076,
		time: 2075,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2077,
		time: 2076,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2078,
		time: 2077,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2079,
		time: 2078,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2080,
		time: 2079,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2081,
		time: 2080,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2082,
		time: 2081,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2083,
		time: 2082,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2084,
		time: 2083,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2085,
		time: 2084,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2086,
		time: 2085,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2087,
		time: 2086,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2088,
		time: 2087,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2089,
		time: 2088,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2090,
		time: 2089,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2091,
		time: 2090,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2092,
		time: 2091,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2093,
		time: 2092,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2094,
		time: 2093,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2095,
		time: 2094,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2096,
		time: 2095,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2097,
		time: 2096,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2098,
		time: 2097,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2099,
		time: 2098,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2100,
		time: 2099,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2101,
		time: 2100,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2102,
		time: 2101,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2103,
		time: 2102,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2104,
		time: 2103,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2105,
		time: 2104,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2106,
		time: 2105,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2107,
		time: 2106,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2108,
		time: 2107,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2109,
		time: 2108,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2110,
		time: 2109,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2111,
		time: 2110,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2112,
		time: 2111,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2113,
		time: 2112,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2114,
		time: 2113,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2115,
		time: 2114,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2116,
		time: 2115,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2117,
		time: 2116,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2118,
		time: 2117,
		velocity: 0,
		power: 0,
		road: 21027.8585185185,
		acceleration: 0
	},
	{
		id: 2119,
		time: 2118,
		velocity: 0,
		power: 418.26262318363,
		road: 21028.2974537037,
		acceleration: 0.87787037037037
	},
	{
		id: 2120,
		time: 2119,
		velocity: 2.63361111111111,
		power: 2719.94963342719,
		road: 21029.96875,
		acceleration: 1.58685185185185
	},
	{
		id: 2121,
		time: 2120,
		velocity: 4.76055555555556,
		power: 7432.37202608373,
		road: 21033.4806944445,
		acceleration: 2.09444444444444
	},
	{
		id: 2122,
		time: 2121,
		velocity: 6.28333333333333,
		power: 6140.33205145685,
		road: 21038.5999074074,
		acceleration: 1.12009259259259
	},
	{
		id: 2123,
		time: 2122,
		velocity: 5.99388888888889,
		power: 3936.71593072456,
		road: 21044.5532870371,
		acceleration: 0.548240740740741
	},
	{
		id: 2124,
		time: 2123,
		velocity: 6.40527777777778,
		power: 2159.32263666169,
		road: 21050.8851388889,
		acceleration: 0.208703703703705
	},
	{
		id: 2125,
		time: 2124,
		velocity: 6.90944444444444,
		power: 4144.01365553538,
		road: 21057.5709259259,
		acceleration: 0.499166666666667
	},
	{
		id: 2126,
		time: 2125,
		velocity: 7.49138888888889,
		power: 4525.92788709927,
		road: 21064.7589814815,
		acceleration: 0.505370370370369
	},
	{
		id: 2127,
		time: 2126,
		velocity: 7.92138888888889,
		power: 4570.50791065681,
		road: 21072.4324074074,
		acceleration: 0.46537037037037
	},
	{
		id: 2128,
		time: 2127,
		velocity: 8.30555555555556,
		power: 4902.12726950864,
		road: 21080.5725,
		acceleration: 0.467962962962965
	},
	{
		id: 2129,
		time: 2128,
		velocity: 8.89527777777778,
		power: 3656.50520039303,
		road: 21089.087962963,
		acceleration: 0.282777777777778
	},
	{
		id: 2130,
		time: 2129,
		velocity: 8.76972222222222,
		power: 1378.73495910101,
		road: 21097.7436574074,
		acceleration: -0.00231481481481488
	},
	{
		id: 2131,
		time: 2130,
		velocity: 8.29861111111111,
		power: -2934.52388778977,
		road: 21106.1308796296,
		acceleration: -0.534629629629629
	},
	{
		id: 2132,
		time: 2131,
		velocity: 7.29138888888889,
		power: -13682.6502988723,
		road: 21113.1478703704,
		acceleration: -2.20583333333334
	},
	{
		id: 2133,
		time: 2132,
		velocity: 2.15222222222222,
		power: -10262.515775147,
		road: 21117.8405092593,
		acceleration: -2.44287037037037
	},
	{
		id: 2134,
		time: 2133,
		velocity: 0.97,
		power: -4920.58664930946,
		road: 21120.0964814815,
		acceleration: -2.43046296296296
	},
	{
		id: 2135,
		time: 2134,
		velocity: 0,
		power: -381.104886600198,
		road: 21120.7785185185,
		acceleration: -0.717407407407407
	},
	{
		id: 2136,
		time: 2135,
		velocity: 0,
		power: -29.9888263157895,
		road: 21120.9401851852,
		acceleration: -0.323333333333333
	},
	{
		id: 2137,
		time: 2136,
		velocity: 0,
		power: 0,
		road: 21120.9401851852,
		acceleration: 0
	},
	{
		id: 2138,
		time: 2137,
		velocity: 0,
		power: 0,
		road: 21120.9401851852,
		acceleration: 0
	},
	{
		id: 2139,
		time: 2138,
		velocity: 0,
		power: 290.255336874501,
		road: 21121.3009259259,
		acceleration: 0.721481481481482
	},
	{
		id: 2140,
		time: 2139,
		velocity: 2.16444444444444,
		power: 1629.36907905494,
		road: 21122.6130092593,
		acceleration: 1.1812037037037
	},
	{
		id: 2141,
		time: 2140,
		velocity: 3.54361111111111,
		power: 3423.98077076126,
		road: 21125.1588425926,
		acceleration: 1.2862962962963
	},
	{
		id: 2142,
		time: 2141,
		velocity: 3.85888888888889,
		power: 2117.60881024916,
		road: 21128.6044907408,
		acceleration: 0.513333333333333
	},
	{
		id: 2143,
		time: 2142,
		velocity: 3.70444444444444,
		power: 384.172993807132,
		road: 21132.2941203704,
		acceleration: -0.02537037037037
	},
	{
		id: 2144,
		time: 2143,
		velocity: 3.4675,
		power: -409.450928053845,
		road: 21135.8430555556,
		acceleration: -0.256018518518518
	},
	{
		id: 2145,
		time: 2144,
		velocity: 3.09083333333333,
		power: -1640.86850181925,
		road: 21138.9162962963,
		acceleration: -0.695370370370371
	},
	{
		id: 2146,
		time: 2145,
		velocity: 1.61833333333333,
		power: -2089.31858904689,
		road: 21141.0639351852,
		acceleration: -1.15583333333333
	},
	{
		id: 2147,
		time: 2146,
		velocity: 0,
		power: -901.749701241825,
		road: 21142.1185185185,
		acceleration: -1.03027777777778
	},
	{
		id: 2148,
		time: 2147,
		velocity: 0,
		power: -105.254980409357,
		road: 21142.3882407408,
		acceleration: -0.539444444444444
	},
	{
		id: 2149,
		time: 2148,
		velocity: 0,
		power: 2.62553836729921,
		road: 21142.4053703704,
		acceleration: 0.0342592592592593
	},
	{
		id: 2150,
		time: 2149,
		velocity: 0.102777777777778,
		power: 4.13915340776282,
		road: 21142.4396296296,
		acceleration: 0
	},
	{
		id: 2151,
		time: 2150,
		velocity: 0,
		power: 4.13915340776282,
		road: 21142.4738888889,
		acceleration: 0
	},
	{
		id: 2152,
		time: 2151,
		velocity: 0,
		power: 1.51360412605588,
		road: 21142.4910185185,
		acceleration: -0.0342592592592593
	},
	{
		id: 2153,
		time: 2152,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2154,
		time: 2153,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2155,
		time: 2154,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2156,
		time: 2155,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2157,
		time: 2156,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2158,
		time: 2157,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2159,
		time: 2158,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2160,
		time: 2159,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2161,
		time: 2160,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2162,
		time: 2161,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2163,
		time: 2162,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2164,
		time: 2163,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2165,
		time: 2164,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2166,
		time: 2165,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2167,
		time: 2166,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2168,
		time: 2167,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2169,
		time: 2168,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2170,
		time: 2169,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2171,
		time: 2170,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2172,
		time: 2171,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2173,
		time: 2172,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2174,
		time: 2173,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2175,
		time: 2174,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2176,
		time: 2175,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2177,
		time: 2176,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2178,
		time: 2177,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2179,
		time: 2178,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2180,
		time: 2179,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2181,
		time: 2180,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2182,
		time: 2181,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2183,
		time: 2182,
		velocity: 0,
		power: 0,
		road: 21142.4910185185,
		acceleration: 0
	},
	{
		id: 2184,
		time: 2183,
		velocity: 0,
		power: 0.242087442864627,
		road: 21142.492962963,
		acceleration: 0.00388888888888889
	},
	{
		id: 2185,
		time: 2184,
		velocity: 0.0116666666666667,
		power: 0.469847400349138,
		road: 21142.4968518519,
		acceleration: 0
	},
	{
		id: 2186,
		time: 2185,
		velocity: 0,
		power: 0.469847400349138,
		road: 21142.5007407408,
		acceleration: 0
	},
	{
		id: 2187,
		time: 2186,
		velocity: 0,
		power: 4.15621020769878,
		road: 21142.5263888889,
		acceleration: 0.0435185185185185
	},
	{
		id: 2188,
		time: 2187,
		velocity: 0.142222222222222,
		power: 5.72772099877316,
		road: 21142.5737962963,
		acceleration: 0
	},
	{
		id: 2189,
		time: 2188,
		velocity: 0,
		power: 5.72772099877316,
		road: 21142.6212037037,
		acceleration: 0
	},
	{
		id: 2190,
		time: 2189,
		velocity: 0,
		power: 1.79924418453541,
		road: 21142.6449074074,
		acceleration: -0.0474074074074074
	},
	{
		id: 2191,
		time: 2190,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2192,
		time: 2191,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2193,
		time: 2192,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2194,
		time: 2193,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2195,
		time: 2194,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2196,
		time: 2195,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2197,
		time: 2196,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2198,
		time: 2197,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2199,
		time: 2198,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2200,
		time: 2199,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2201,
		time: 2200,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2202,
		time: 2201,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2203,
		time: 2202,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2204,
		time: 2203,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2205,
		time: 2204,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2206,
		time: 2205,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2207,
		time: 2206,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2208,
		time: 2207,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	},
	{
		id: 2209,
		time: 2208,
		velocity: 0,
		power: 0,
		road: 21142.6449074074,
		acceleration: 0
	}
];
export default test2;