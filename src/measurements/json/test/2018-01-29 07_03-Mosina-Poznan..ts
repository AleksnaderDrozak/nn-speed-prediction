export const test9 = [
	{
		id: 1,
		time: 0,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 2,
		time: 1,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 3,
		time: 2,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 4,
		time: 3,
		velocity: 0,
		power: 0,
		road: 0,
		acceleration: 0
	},
	{
		id: 5,
		time: 4,
		velocity: 0,
		power: 9.95843797022823,
		road: 0.0473148148148148,
		acceleration: 0.0946296296296296
	},
	{
		id: 6,
		time: 5,
		velocity: 0.283888888888889,
		power: 11.4334126526072,
		road: 0.141944444444444,
		acceleration: 0
	},
	{
		id: 7,
		time: 6,
		velocity: 0,
		power: 74.1858765003075,
		road: 0.355833333333333,
		acceleration: 0.238518518518519
	},
	{
		id: 8,
		time: 7,
		velocity: 0.715555555555556,
		power: 125.935204134589,
		road: 0.781342592592593,
		acceleration: 0.184722222222222
	},
	{
		id: 9,
		time: 8,
		velocity: 0.838055555555556,
		power: 369.58425999407,
		road: 1.50481481481481,
		acceleration: 0.411203703703704
	},
	{
		id: 10,
		time: 9,
		velocity: 1.23361111111111,
		power: 365.943310183775,
		road: 2.55384259259259,
		acceleration: 0.239907407407407
	},
	{
		id: 11,
		time: 10,
		velocity: 1.43527777777778,
		power: 591.544143293789,
		road: 3.89175925925926,
		acceleration: 0.33787037037037
	},
	{
		id: 12,
		time: 11,
		velocity: 1.85166666666667,
		power: 489.947109229321,
		road: 5.49527777777778,
		acceleration: 0.193333333333333
	},
	{
		id: 13,
		time: 12,
		velocity: 1.81361111111111,
		power: 703.606934825685,
		road: 7.33268518518518,
		acceleration: 0.274444444444444
	},
	{
		id: 14,
		time: 13,
		velocity: 2.25861111111111,
		power: -221.193676258702,
		road: 9.17949074074074,
		acceleration: -0.255648148148148
	},
	{
		id: 15,
		time: 14,
		velocity: 1.08472222222222,
		power: -295.808761848869,
		road: 10.7337037037037,
		acceleration: -0.329537037037037
	},
	{
		id: 16,
		time: 15,
		velocity: 0.825,
		power: -452.170291394317,
		road: 11.8443055555556,
		acceleration: -0.557685185185185
	},
	{
		id: 17,
		time: 16,
		velocity: 0.585555555555556,
		power: -35.6314342974438,
		road: 12.5868518518519,
		acceleration: -0.178425925925926
	},
	{
		id: 18,
		time: 17,
		velocity: 0.549444444444444,
		power: -72.0261780190427,
		road: 13.1026851851852,
		acceleration: -0.275
	},
	{
		id: 19,
		time: 18,
		velocity: 0,
		power: -17.9887932056785,
		road: 13.3834259259259,
		acceleration: -0.195185185185185
	},
	{
		id: 20,
		time: 19,
		velocity: 0,
		power: -4.82511829109812,
		road: 13.475,
		acceleration: -0.183148148148148
	},
	{
		id: 21,
		time: 20,
		velocity: 0,
		power: 135.882149141636,
		road: 13.7127777777778,
		acceleration: 0.475555555555556
	},
	{
		id: 22,
		time: 21,
		velocity: 1.42666666666667,
		power: 461.058607528335,
		road: 14.4530092592593,
		acceleration: 0.529351851851852
	},
	{
		id: 23,
		time: 22,
		velocity: 1.58805555555556,
		power: 654.27902287778,
		road: 15.6759259259259,
		acceleration: 0.436018518518519
	},
	{
		id: 24,
		time: 23,
		velocity: 1.30805555555556,
		power: -171.053391336099,
		road: 16.9836574074074,
		acceleration: -0.266388888888889
	},
	{
		id: 25,
		time: 24,
		velocity: 0.6275,
		power: -346.15425397508,
		road: 17.8935185185185,
		acceleration: -0.529351851851852
	},
	{
		id: 26,
		time: 25,
		velocity: 0,
		power: 135.531824522841,
		road: 18.5791203703704,
		acceleration: 0.0808333333333333
	},
	{
		id: 27,
		time: 26,
		velocity: 1.55055555555556,
		power: 1477.09868712466,
		road: 19.8525925925926,
		acceleration: 1.09490740740741
	},
	{
		id: 28,
		time: 27,
		velocity: 3.91222222222222,
		power: 4152.75650738746,
		road: 22.4502314814815,
		acceleration: 1.55342592592593
	},
	{
		id: 29,
		time: 28,
		velocity: 4.66027777777778,
		power: 4205.8081814155,
		road: 26.3277777777778,
		acceleration: 1.00638888888889
	},
	{
		id: 30,
		time: 29,
		velocity: 4.56972222222222,
		power: 1139.00937554979,
		road: 30.7741203703704,
		acceleration: 0.131203703703703
	},
	{
		id: 31,
		time: 30,
		velocity: 4.30583333333333,
		power: 218.912894660078,
		road: 35.2425462962963,
		acceleration: -0.087037037037037
	},
	{
		id: 32,
		time: 31,
		velocity: 4.39916666666667,
		power: -538.439508016169,
		road: 39.5325,
		acceleration: -0.269907407407407
	},
	{
		id: 33,
		time: 32,
		velocity: 3.76,
		power: 423.722173316253,
		road: 43.6728703703704,
		acceleration: -0.0292592592592591
	},
	{
		id: 34,
		time: 33,
		velocity: 4.21805555555556,
		power: -156.593798764444,
		road: 47.7099074074074,
		acceleration: -0.177407407407407
	},
	{
		id: 35,
		time: 34,
		velocity: 3.86694444444444,
		power: 1724.84368490574,
		road: 51.81125,
		acceleration: 0.306018518518518
	},
	{
		id: 36,
		time: 35,
		velocity: 4.67805555555556,
		power: 2376.61817621224,
		road: 56.2764814814815,
		acceleration: 0.421759259259259
	},
	{
		id: 37,
		time: 36,
		velocity: 5.48333333333333,
		power: 4485.37541327362,
		road: 61.3471296296296,
		acceleration: 0.789074074074073
	},
	{
		id: 38,
		time: 37,
		velocity: 6.23416666666667,
		power: 5482.56260303665,
		road: 67.2291203703704,
		acceleration: 0.833611111111111
	},
	{
		id: 39,
		time: 38,
		velocity: 7.17888888888889,
		power: 5970.71776191779,
		road: 73.9206944444444,
		acceleration: 0.785555555555557
	},
	{
		id: 40,
		time: 39,
		velocity: 7.84,
		power: 6786.19372022654,
		road: 81.4022222222222,
		acceleration: 0.794351851851851
	},
	{
		id: 41,
		time: 40,
		velocity: 8.61722222222222,
		power: 6501.96197563242,
		road: 89.6141203703704,
		acceleration: 0.666388888888889
	},
	{
		id: 42,
		time: 41,
		velocity: 9.17805555555555,
		power: 7449.97249327719,
		road: 98.5127777777778,
		acceleration: 0.707129629629629
	},
	{
		id: 43,
		time: 42,
		velocity: 9.96138888888889,
		power: 6840.55515753318,
		road: 108.052037037037,
		acceleration: 0.574074074074073
	},
	{
		id: 44,
		time: 43,
		velocity: 10.3394444444444,
		power: 7223.82320087289,
		road: 118.160787037037,
		acceleration: 0.564907407407411
	},
	{
		id: 45,
		time: 44,
		velocity: 10.8727777777778,
		power: 2796.56320399481,
		road: 128.598148148148,
		acceleration: 0.092314814814813
	},
	{
		id: 46,
		time: 45,
		velocity: 10.2383333333333,
		power: -631.265316378725,
		road: 138.955740740741,
		acceleration: -0.25185185185185
	},
	{
		id: 47,
		time: 46,
		velocity: 9.58388888888889,
		power: -3100.459745301,
		road: 148.932546296296,
		acceleration: -0.509722222222223
	},
	{
		id: 48,
		time: 47,
		velocity: 9.34361111111111,
		power: -6023.14426743542,
		road: 158.226157407407,
		acceleration: -0.856666666666666
	},
	{
		id: 49,
		time: 48,
		velocity: 7.66833333333333,
		power: -8083.30919047743,
		road: 166.494861111111,
		acceleration: -1.19314814814815
	},
	{
		id: 50,
		time: 49,
		velocity: 6.00444444444444,
		power: -11799.9130880195,
		road: 173.15962962963,
		acceleration: -2.01472222222222
	},
	{
		id: 51,
		time: 50,
		velocity: 3.29944444444444,
		power: -6157.50011612299,
		road: 178.088935185185,
		acceleration: -1.4562037037037
	},
	{
		id: 52,
		time: 51,
		velocity: 3.29972222222222,
		power: -2001.57319807791,
		road: 181.949166666667,
		acceleration: -0.681944444444445
	},
	{
		id: 53,
		time: 52,
		velocity: 3.95861111111111,
		power: 1876.07867862798,
		road: 185.66662037037,
		acceleration: 0.39638888888889
	},
	{
		id: 54,
		time: 53,
		velocity: 4.48861111111111,
		power: 4642.27926309353,
		road: 190.068287037037,
		acceleration: 0.972037037037036
	},
	{
		id: 55,
		time: 54,
		velocity: 6.21583333333333,
		power: 6965.30324974876,
		road: 195.551990740741,
		acceleration: 1.19203703703704
	},
	{
		id: 56,
		time: 55,
		velocity: 7.53472222222222,
		power: 9244.20799396944,
		road: 202.277777777778,
		acceleration: 1.29212962962963
	},
	{
		id: 57,
		time: 56,
		velocity: 8.365,
		power: 10245.0884827646,
		road: 210.243657407407,
		acceleration: 1.18805555555555
	},
	{
		id: 58,
		time: 57,
		velocity: 9.78,
		power: 8965.87781059155,
		road: 219.240277777778,
		acceleration: 0.873425925925925
	},
	{
		id: 59,
		time: 58,
		velocity: 10.155,
		power: 9826.34534407308,
		road: 229.105185185185,
		acceleration: 0.863148148148149
	},
	{
		id: 60,
		time: 59,
		velocity: 10.9544444444444,
		power: 6717.97661705712,
		road: 239.641157407407,
		acceleration: 0.478981481481481
	},
	{
		id: 61,
		time: 60,
		velocity: 11.2169444444444,
		power: 5385.11132138149,
		road: 250.577453703704,
		acceleration: 0.321666666666667
	},
	{
		id: 62,
		time: 61,
		velocity: 11.12,
		power: 719.104339286345,
		road: 261.610740740741,
		acceleration: -0.127685185185186
	},
	{
		id: 63,
		time: 62,
		velocity: 10.5713888888889,
		power: -1456.14198238602,
		road: 272.41287037037,
		acceleration: -0.334629629629628
	},
	{
		id: 64,
		time: 63,
		velocity: 10.2130555555556,
		power: -727.736581075912,
		road: 282.916527777778,
		acceleration: -0.262314814814815
	},
	{
		id: 65,
		time: 64,
		velocity: 10.3330555555556,
		power: 1381.68476637047,
		road: 293.265185185185,
		acceleration: -0.0476851851851858
	},
	{
		id: 66,
		time: 65,
		velocity: 10.4283333333333,
		power: 3355.69032814087,
		road: 303.665092592593,
		acceleration: 0.150185185185185
	},
	{
		id: 67,
		time: 66,
		velocity: 10.6636111111111,
		power: 2954.10563217665,
		road: 314.192361111111,
		acceleration: 0.104537037037037
	},
	{
		id: 68,
		time: 67,
		velocity: 10.6466666666667,
		power: 3129.38445819147,
		road: 324.830601851852,
		acceleration: 0.117407407407409
	},
	{
		id: 69,
		time: 68,
		velocity: 10.7805555555556,
		power: 2311.99344127194,
		road: 335.544675925926,
		acceleration: 0.0342592592592581
	},
	{
		id: 70,
		time: 69,
		velocity: 10.7663888888889,
		power: 2242.69670574595,
		road: 346.28912037037,
		acceleration: 0.0264814814814827
	},
	{
		id: 71,
		time: 70,
		velocity: 10.7261111111111,
		power: 1876.3481488319,
		road: 357.042037037037,
		acceleration: -0.00953703703703823
	},
	{
		id: 72,
		time: 71,
		velocity: 10.7519444444444,
		power: 1758.09993727478,
		road: 367.779861111111,
		acceleration: -0.0206481481481493
	},
	{
		id: 73,
		time: 72,
		velocity: 10.7044444444444,
		power: 1877.87190642234,
		road: 378.503101851852,
		acceleration: -0.00851851851851571
	},
	{
		id: 74,
		time: 73,
		velocity: 10.7005555555556,
		power: 1730.52448107677,
		road: 389.210833333333,
		acceleration: -0.0225000000000044
	},
	{
		id: 75,
		time: 74,
		velocity: 10.6844444444444,
		power: -192.404126715167,
		road: 399.802453703704,
		acceleration: -0.20972222222222
	},
	{
		id: 76,
		time: 75,
		velocity: 10.0752777777778,
		power: -2159.39015862502,
		road: 410.085509259259,
		acceleration: -0.407407407407407
	},
	{
		id: 77,
		time: 76,
		velocity: 9.47833333333333,
		power: -3663.96152863307,
		road: 419.877731481481,
		acceleration: -0.574259259259261
	},
	{
		id: 78,
		time: 77,
		velocity: 8.96166666666667,
		power: 8.16559794187523,
		road: 429.294583333333,
		acceleration: -0.176481481481481
	},
	{
		id: 79,
		time: 78,
		velocity: 9.54583333333333,
		power: 2943.80097881136,
		road: 438.698888888889,
		acceleration: 0.151388888888889
	},
	{
		id: 80,
		time: 79,
		velocity: 9.9325,
		power: 7074.71184889178,
		road: 448.468333333333,
		acceleration: 0.578888888888891
	},
	{
		id: 81,
		time: 80,
		velocity: 10.6983333333333,
		power: 6412.73711188185,
		road: 458.760555555555,
		acceleration: 0.466666666666667
	},
	{
		id: 82,
		time: 81,
		velocity: 10.9458333333333,
		power: 5719.08979350605,
		road: 469.470185185185,
		acceleration: 0.368148148148146
	},
	{
		id: 83,
		time: 82,
		velocity: 11.0369444444444,
		power: 3460.83567926702,
		road: 480.431898148148,
		acceleration: 0.136018518518519
	},
	{
		id: 84,
		time: 83,
		velocity: 11.1063888888889,
		power: 3119.38824810256,
		road: 491.510972222222,
		acceleration: 0.0987037037037037
	},
	{
		id: 85,
		time: 84,
		velocity: 11.2419444444444,
		power: 3354.92299255045,
		road: 502.697685185185,
		acceleration: 0.116574074074075
	},
	{
		id: 86,
		time: 85,
		velocity: 11.3866666666667,
		power: 2871.54102172569,
		road: 513.97662037037,
		acceleration: 0.06787037037037
	},
	{
		id: 87,
		time: 86,
		velocity: 11.31,
		power: 2636.40677475854,
		road: 525.311527777778,
		acceleration: 0.044074074074075
	},
	{
		id: 88,
		time: 87,
		velocity: 11.3741666666667,
		power: 2676.36212821431,
		road: 536.691574074074,
		acceleration: 0.0462037037037035
	},
	{
		id: 89,
		time: 88,
		velocity: 11.5252777777778,
		power: 3845.43214007873,
		road: 548.169537037037,
		acceleration: 0.149629629629629
	},
	{
		id: 90,
		time: 89,
		velocity: 11.7588888888889,
		power: 2786.08833515003,
		road: 559.746990740741,
		acceleration: 0.0493518518518528
	},
	{
		id: 91,
		time: 90,
		velocity: 11.5222222222222,
		power: 1753.29405665346,
		road: 571.326990740741,
		acceleration: -0.0442592592592614
	},
	{
		id: 92,
		time: 91,
		velocity: 11.3925,
		power: -1068.97112912752,
		road: 582.735324074074,
		acceleration: -0.299074074074072
	},
	{
		id: 93,
		time: 92,
		velocity: 10.8616666666667,
		power: -995.537112313206,
		road: 593.848611111111,
		acceleration: -0.291018518518518
	},
	{
		id: 94,
		time: 93,
		velocity: 10.6491666666667,
		power: -1820.40376426682,
		road: 604.631342592592,
		acceleration: -0.370092592592593
	},
	{
		id: 95,
		time: 94,
		velocity: 10.2822222222222,
		power: -398.504924216328,
		road: 615.114398148148,
		acceleration: -0.229259259259258
	},
	{
		id: 96,
		time: 95,
		velocity: 10.1738888888889,
		power: -2757.61072993327,
		road: 625.247361111111,
		acceleration: -0.470925925925927
	},
	{
		id: 97,
		time: 96,
		velocity: 9.23638888888889,
		power: -6563.82269240861,
		road: 634.691111111111,
		acceleration: -0.907499999999999
	},
	{
		id: 98,
		time: 97,
		velocity: 7.55972222222222,
		power: -10949.3816182881,
		road: 642.897361111111,
		acceleration: -1.5675
	},
	{
		id: 99,
		time: 98,
		velocity: 5.47138888888889,
		power: -8327.04841334448,
		road: 649.589166666667,
		acceleration: -1.46138888888889
	},
	{
		id: 100,
		time: 99,
		velocity: 4.85222222222222,
		power: -4435.14153623487,
		road: 655.050879629629,
		acceleration: -0.998796296296296
	},
	{
		id: 101,
		time: 100,
		velocity: 4.56333333333333,
		power: 204.658232094761,
		road: 659.964629629629,
		acceleration: -0.09712962962963
	},
	{
		id: 102,
		time: 101,
		velocity: 5.18,
		power: 1629.30003777946,
		road: 664.931805555555,
		acceleration: 0.203981481481482
	},
	{
		id: 103,
		time: 102,
		velocity: 5.46416666666667,
		power: 3338.07239548991,
		road: 670.258981481481,
		acceleration: 0.516018518518519
	},
	{
		id: 104,
		time: 103,
		velocity: 6.11138888888889,
		power: 2637.61317616966,
		road: 676.012314814815,
		acceleration: 0.336296296296297
	},
	{
		id: 105,
		time: 104,
		velocity: 6.18888888888889,
		power: 2928.02065116983,
		road: 682.112083333333,
		acceleration: 0.356574074074074
	},
	{
		id: 106,
		time: 105,
		velocity: 6.53388888888889,
		power: 2026.03989946833,
		road: 688.482268518518,
		acceleration: 0.184259259259258
	},
	{
		id: 107,
		time: 106,
		velocity: 6.66416666666667,
		power: 2124.73799855241,
		road: 695.039166666666,
		acceleration: 0.189166666666667
	},
	{
		id: 108,
		time: 107,
		velocity: 6.75638888888889,
		power: 2080.57396176389,
		road: 701.776527777778,
		acceleration: 0.171759259259259
	},
	{
		id: 109,
		time: 108,
		velocity: 7.04916666666667,
		power: 2543.12425048041,
		road: 708.715185185185,
		acceleration: 0.230833333333333
	},
	{
		id: 110,
		time: 109,
		velocity: 7.35666666666667,
		power: 2230.69817960308,
		road: 715.855416666666,
		acceleration: 0.172314814814815
	},
	{
		id: 111,
		time: 110,
		velocity: 7.27333333333333,
		power: -2720.11779952063,
		road: 722.798564814815,
		acceleration: -0.566481481481482
	},
	{
		id: 112,
		time: 111,
		velocity: 5.34972222222222,
		power: -6572.24500904845,
		road: 728.809398148148,
		acceleration: -1.29814814814815
	},
	{
		id: 113,
		time: 112,
		velocity: 3.46222222222222,
		power: -5149.13364086347,
		road: 733.526527777778,
		acceleration: -1.28925925925926
	},
	{
		id: 114,
		time: 113,
		velocity: 3.40555555555556,
		power: -1822.15800718577,
		road: 737.27537037037,
		acceleration: -0.647314814814814
	},
	{
		id: 115,
		time: 114,
		velocity: 3.40777777777778,
		power: 209.107062650976,
		road: 740.666111111111,
		acceleration: -0.068888888888889
	},
	{
		id: 116,
		time: 115,
		velocity: 3.25555555555556,
		power: 176.013732024492,
		road: 743.983564814815,
		acceleration: -0.0776851851851852
	},
	{
		id: 117,
		time: 116,
		velocity: 3.1725,
		power: 459.81622784324,
		road: 747.269166666666,
		acceleration: 0.0139814814814812
	},
	{
		id: 118,
		time: 117,
		velocity: 3.44972222222222,
		power: 744.44367753168,
		road: 750.612222222222,
		acceleration: 0.100925925925926
	},
	{
		id: 119,
		time: 118,
		velocity: 3.55833333333333,
		power: 1922.2752039139,
		road: 754.219074074074,
		acceleration: 0.426666666666667
	},
	{
		id: 120,
		time: 119,
		velocity: 4.4525,
		power: 2147.31933151913,
		road: 758.251388888889,
		acceleration: 0.424259259259259
	},
	{
		id: 121,
		time: 120,
		velocity: 4.7225,
		power: 3851.59283029621,
		road: 762.865509259259,
		acceleration: 0.739351851851852
	},
	{
		id: 122,
		time: 121,
		velocity: 5.77638888888889,
		power: 3443.6025871038,
		road: 768.1225,
		acceleration: 0.546388888888889
	},
	{
		id: 123,
		time: 122,
		velocity: 6.09166666666667,
		power: 4854.83859353448,
		road: 774.012731481481,
		acceleration: 0.720092592592592
	},
	{
		id: 124,
		time: 123,
		velocity: 6.88277777777778,
		power: 4630.79130816514,
		road: 780.559166666667,
		acceleration: 0.592314814814815
	},
	{
		id: 125,
		time: 124,
		velocity: 7.55333333333333,
		power: 5051.33159844276,
		road: 787.695740740741,
		acceleration: 0.587962962962962
	},
	{
		id: 126,
		time: 125,
		velocity: 7.85555555555556,
		power: 6915.43521455434,
		road: 795.510324074074,
		acceleration: 0.768055555555557
	},
	{
		id: 127,
		time: 126,
		velocity: 9.18694444444444,
		power: 5906.92725377865,
		road: 803.990787037037,
		acceleration: 0.563703703703704
	},
	{
		id: 128,
		time: 127,
		velocity: 9.24444444444445,
		power: 7188.94852347756,
		road: 813.081296296296,
		acceleration: 0.656388888888889
	},
	{
		id: 129,
		time: 128,
		velocity: 9.82472222222222,
		power: 3613.48900072508,
		road: 822.609768518518,
		acceleration: 0.219537037037037
	},
	{
		id: 130,
		time: 129,
		velocity: 9.84555555555556,
		power: 5964.93193275782,
		road: 832.474212962963,
		acceleration: 0.452407407407406
	},
	{
		id: 131,
		time: 130,
		velocity: 10.6016666666667,
		power: 5182.47074299591,
		road: 842.736435185185,
		acceleration: 0.343148148148147
	},
	{
		id: 132,
		time: 131,
		velocity: 10.8541666666667,
		power: 5668.2903608274,
		road: 853.354768518518,
		acceleration: 0.369074074074076
	},
	{
		id: 133,
		time: 132,
		velocity: 10.9527777777778,
		power: 3099.35410581344,
		road: 864.210462962963,
		acceleration: 0.105648148148148
	},
	{
		id: 134,
		time: 133,
		velocity: 10.9186111111111,
		power: -34.6670085383754,
		road: 875.020648148148,
		acceleration: -0.196666666666667
	},
	{
		id: 135,
		time: 134,
		velocity: 10.2641666666667,
		power: -2233.08469001812,
		road: 885.526157407407,
		acceleration: -0.412685185185186
	},
	{
		id: 136,
		time: 135,
		velocity: 9.71472222222222,
		power: -3078.3071162589,
		road: 895.572361111111,
		acceleration: -0.505925925925926
	},
	{
		id: 137,
		time: 136,
		velocity: 9.40083333333333,
		power: -364.80382694472,
		road: 905.255694444444,
		acceleration: -0.219814814814814
	},
	{
		id: 138,
		time: 137,
		velocity: 9.60472222222222,
		power: 1473.723715541,
		road: 914.820509259259,
		acceleration: -0.0172222222222231
	},
	{
		id: 139,
		time: 138,
		velocity: 9.66305555555556,
		power: 3318.09567538464,
		road: 924.467314814815,
		acceleration: 0.181203703703703
	},
	{
		id: 140,
		time: 139,
		velocity: 9.94444444444444,
		power: 3302.69135889015,
		road: 934.290277777778,
		acceleration: 0.171111111111111
	},
	{
		id: 141,
		time: 140,
		velocity: 10.1180555555556,
		power: 2957.34113031294,
		road: 944.262685185185,
		acceleration: 0.127777777777778
	},
	{
		id: 142,
		time: 141,
		velocity: 10.0463888888889,
		power: 205.910791169585,
		road: 954.218194444444,
		acceleration: -0.161574074074073
	},
	{
		id: 143,
		time: 142,
		velocity: 9.45972222222222,
		power: 203.134552567497,
		road: 964.013055555556,
		acceleration: -0.159722222222221
	},
	{
		id: 144,
		time: 143,
		velocity: 9.63888888888889,
		power: 1038.88133013845,
		road: 973.694259259259,
		acceleration: -0.0675925925925931
	},
	{
		id: 145,
		time: 144,
		velocity: 9.84361111111111,
		power: 3304.36782867219,
		road: 983.429398148148,
		acceleration: 0.175462962962962
	},
	{
		id: 146,
		time: 145,
		velocity: 9.98611111111111,
		power: 2238.38832658765,
		road: 993.280462962963,
		acceleration: 0.0563888888888879
	},
	{
		id: 147,
		time: 146,
		velocity: 9.80805555555555,
		power: 1343.65085456976,
		road: 1003.14013888889,
		acceleration: -0.0391666666666666
	},
	{
		id: 148,
		time: 147,
		velocity: 9.72611111111111,
		power: 1537.11637357822,
		road: 1012.97134259259,
		acceleration: -0.017777777777777
	},
	{
		id: 149,
		time: 148,
		velocity: 9.93277777777778,
		power: 4540.30932019414,
		road: 1022.94092592593,
		acceleration: 0.294537037037035
	},
	{
		id: 150,
		time: 149,
		velocity: 10.6916666666667,
		power: 2937.94881902805,
		road: 1033.11638888889,
		acceleration: 0.117222222222225
	},
	{
		id: 151,
		time: 150,
		velocity: 10.0777777777778,
		power: 2976.93475013241,
		road: 1043.40865740741,
		acceleration: 0.116388888888888
	},
	{
		id: 152,
		time: 151,
		velocity: 10.2819444444444,
		power: 1557.20495292538,
		road: 1053.74435185185,
		acceleration: -0.0295370370370378
	},
	{
		id: 153,
		time: 152,
		velocity: 10.6030555555556,
		power: 5573.14013010897,
		road: 1064.2487962963,
		acceleration: 0.367037037037036
	},
	{
		id: 154,
		time: 153,
		velocity: 11.1788888888889,
		power: 6071.78575091389,
		road: 1075.13226851852,
		acceleration: 0.391018518518521
	},
	{
		id: 155,
		time: 154,
		velocity: 11.455,
		power: 6645.47062788053,
		road: 1086.42032407407,
		acceleration: 0.418148148148148
	},
	{
		id: 156,
		time: 155,
		velocity: 11.8575,
		power: 5433.53895842504,
		road: 1098.06027777778,
		acceleration: 0.285648148148145
	},
	{
		id: 157,
		time: 156,
		velocity: 12.0358333333333,
		power: 5427.10778132576,
		road: 1109.97800925926,
		acceleration: 0.269907407407409
	},
	{
		id: 158,
		time: 157,
		velocity: 12.2647222222222,
		power: 4214.98954750388,
		road: 1122.10763888889,
		acceleration: 0.15388888888889
	},
	{
		id: 159,
		time: 158,
		velocity: 12.3191666666667,
		power: 3725.88411232551,
		road: 1134.36740740741,
		acceleration: 0.106388888888887
	},
	{
		id: 160,
		time: 159,
		velocity: 12.355,
		power: 4089.33652509373,
		road: 1146.74657407407,
		acceleration: 0.132407407407408
	},
	{
		id: 161,
		time: 160,
		velocity: 12.6619444444444,
		power: 4432.36334984608,
		road: 1159.26949074074,
		acceleration: 0.155092592592595
	},
	{
		id: 162,
		time: 161,
		velocity: 12.7844444444444,
		power: 5611.46483128865,
		road: 1171.99171296296,
		acceleration: 0.243518518518515
	},
	{
		id: 163,
		time: 162,
		velocity: 13.0855555555556,
		power: 4034.67008869983,
		road: 1184.88898148148,
		acceleration: 0.106574074074075
	},
	{
		id: 164,
		time: 163,
		velocity: 12.9816666666667,
		power: 3732.75068323227,
		road: 1197.8787962963,
		acceleration: 0.0785185185185195
	},
	{
		id: 165,
		time: 164,
		velocity: 13.02,
		power: 3059.36691011148,
		road: 1210.91912037037,
		acceleration: 0.0225000000000009
	},
	{
		id: 166,
		time: 165,
		velocity: 13.1530555555556,
		power: 2532.04197163324,
		road: 1223.96074074074,
		acceleration: -0.019907407407409
	},
	{
		id: 167,
		time: 166,
		velocity: 12.9219444444444,
		power: 2140.60554414558,
		road: 1236.96722222222,
		acceleration: -0.0503703703703717
	},
	{
		id: 168,
		time: 167,
		velocity: 12.8688888888889,
		power: 606.042572357202,
		road: 1249.8625462963,
		acceleration: -0.171944444444444
	},
	{
		id: 169,
		time: 168,
		velocity: 12.6372222222222,
		power: 1348.98771941159,
		road: 1262.61773148148,
		acceleration: -0.108333333333333
	},
	{
		id: 170,
		time: 169,
		velocity: 12.5969444444444,
		power: 566.725900072202,
		road: 1275.2337037037,
		acceleration: -0.170092592592592
	},
	{
		id: 171,
		time: 170,
		velocity: 12.3586111111111,
		power: 919.204437966808,
		road: 1287.69578703704,
		acceleration: -0.137685185185184
	},
	{
		id: 172,
		time: 171,
		velocity: 12.2241666666667,
		power: 579.878712760544,
		road: 1300.00726851852,
		acceleration: -0.16351851851852
	},
	{
		id: 173,
		time: 172,
		velocity: 12.1063888888889,
		power: 581.701605886323,
		road: 1312.15675925926,
		acceleration: -0.160462962962962
	},
	{
		id: 174,
		time: 173,
		velocity: 11.8772222222222,
		power: 634.454945672492,
		road: 1324.14949074074,
		acceleration: -0.153055555555557
	},
	{
		id: 175,
		time: 174,
		velocity: 11.765,
		power: 332.222352507712,
		road: 1335.97726851852,
		acceleration: -0.176851851851852
	},
	{
		id: 176,
		time: 175,
		velocity: 11.5758333333333,
		power: 143.700133396004,
		road: 1347.62115740741,
		acceleration: -0.190925925925924
	},
	{
		id: 177,
		time: 176,
		velocity: 11.3044444444444,
		power: 810.49271501211,
		road: 1359.10569444444,
		acceleration: -0.127777777777778
	},
	{
		id: 178,
		time: 177,
		velocity: 11.3816666666667,
		power: 789.655732689888,
		road: 1370.46273148148,
		acceleration: -0.127222222222223
	},
	{
		id: 179,
		time: 178,
		velocity: 11.1941666666667,
		power: -562.318354127708,
		road: 1381.63087962963,
		acceleration: -0.250555555555554
	},
	{
		id: 180,
		time: 179,
		velocity: 10.5527777777778,
		power: -8846.93166827085,
		road: 1392.13708333333,
		acceleration: -1.07333333333333
	},
	{
		id: 181,
		time: 180,
		velocity: 8.16166666666667,
		power: -13592.4437067479,
		road: 1401.2349537037,
		acceleration: -1.74333333333333
	},
	{
		id: 182,
		time: 181,
		velocity: 5.96416666666667,
		power: -13296.5313850512,
		road: 1408.40837962963,
		acceleration: -2.10555555555555
	},
	{
		id: 183,
		time: 182,
		velocity: 4.23611111111111,
		power: -8806.80691992209,
		road: 1413.55759259259,
		acceleration: -1.94287037037037
	},
	{
		id: 184,
		time: 183,
		velocity: 2.33305555555556,
		power: -4024.37203051655,
		road: 1417.06347222222,
		acceleration: -1.3437962962963
	},
	{
		id: 185,
		time: 184,
		velocity: 1.93277777777778,
		power: -507.156470006489,
		road: 1419.73157407407,
		acceleration: -0.331759259259259
	},
	{
		id: 186,
		time: 185,
		velocity: 3.24083333333333,
		power: 2298.37091301867,
		road: 1422.59125,
		acceleration: 0.714907407407408
	},
	{
		id: 187,
		time: 186,
		velocity: 4.47777777777778,
		power: 5006.61273523146,
		road: 1426.42768518518,
		acceleration: 1.23861111111111
	},
	{
		id: 188,
		time: 187,
		velocity: 5.64861111111111,
		power: 4204.16239280051,
		road: 1431.2700462963,
		acceleration: 0.77324074074074
	},
	{
		id: 189,
		time: 188,
		velocity: 5.56055555555556,
		power: 2496.17754026573,
		road: 1436.67032407407,
		acceleration: 0.342592592592593
	},
	{
		id: 190,
		time: 189,
		velocity: 5.50555555555556,
		power: 1683.33444043245,
		road: 1442.32578703704,
		acceleration: 0.167777777777777
	},
	{
		id: 191,
		time: 190,
		velocity: 6.15194444444444,
		power: 3662.17388551085,
		road: 1448.31305555555,
		acceleration: 0.495833333333334
	},
	{
		id: 192,
		time: 191,
		velocity: 7.04805555555556,
		power: 3961.62181102738,
		road: 1454.79412037037,
		acceleration: 0.491759259259259
	},
	{
		id: 193,
		time: 192,
		velocity: 6.98083333333333,
		power: 5414.71587117999,
		road: 1461.84689814815,
		acceleration: 0.651666666666667
	},
	{
		id: 194,
		time: 193,
		velocity: 8.10694444444444,
		power: 3894.08571115865,
		road: 1469.41601851852,
		acceleration: 0.381018518518518
	},
	{
		id: 195,
		time: 194,
		velocity: 8.19111111111111,
		power: 1495.13569252621,
		road: 1477.19587962963,
		acceleration: 0.0404629629629625
	},
	{
		id: 196,
		time: 195,
		velocity: 7.10222222222222,
		power: -2911.7047350298,
		road: 1484.71277777778,
		acceleration: -0.566388888888889
	},
	{
		id: 197,
		time: 196,
		velocity: 6.40777777777778,
		power: -2860.12089201545,
		road: 1491.6525462963,
		acceleration: -0.58787037037037
	},
	{
		id: 198,
		time: 197,
		velocity: 6.4275,
		power: 143.267947911077,
		road: 1498.23393518518,
		acceleration: -0.128888888888889
	},
	{
		id: 199,
		time: 198,
		velocity: 6.71555555555555,
		power: 4037.31677460253,
		road: 1504.98856481481,
		acceleration: 0.475370370370371
	},
	{
		id: 200,
		time: 199,
		velocity: 7.83388888888889,
		power: 5466.46995252538,
		road: 1512.29532407407,
		acceleration: 0.62888888888889
	},
	{
		id: 201,
		time: 200,
		velocity: 8.31416666666667,
		power: 8017.5456870635,
		road: 1520.35694444444,
		acceleration: 0.880833333333334
	},
	{
		id: 202,
		time: 201,
		velocity: 9.35805555555556,
		power: 7326.12614915305,
		road: 1529.20782407407,
		acceleration: 0.697685185185184
	},
	{
		id: 203,
		time: 202,
		velocity: 9.92694444444444,
		power: 8714.28893782242,
		road: 1538.795,
		acceleration: 0.774907407407406
	},
	{
		id: 204,
		time: 203,
		velocity: 10.6388888888889,
		power: 5758.42981868557,
		road: 1548.97356481481,
		acceleration: 0.407870370370372
	},
	{
		id: 205,
		time: 204,
		velocity: 10.5816666666667,
		power: 3895.60958198185,
		road: 1559.45634259259,
		acceleration: 0.200555555555555
	},
	{
		id: 206,
		time: 205,
		velocity: 10.5286111111111,
		power: 489.799057365078,
		road: 1569.96898148148,
		acceleration: -0.140833333333333
	},
	{
		id: 207,
		time: 206,
		velocity: 10.2163888888889,
		power: -909.627158602319,
		road: 1580.27125,
		acceleration: -0.279907407407407
	},
	{
		id: 208,
		time: 207,
		velocity: 9.74194444444444,
		power: -780.841636172319,
		road: 1590.30064814815,
		acceleration: -0.265833333333333
	},
	{
		id: 209,
		time: 208,
		velocity: 9.73111111111111,
		power: 858.430583213901,
		road: 1600.15180555555,
		acceleration: -0.0906481481481496
	},
	{
		id: 210,
		time: 209,
		velocity: 9.94444444444444,
		power: -745.906700778056,
		road: 1609.82708333333,
		acceleration: -0.261111111111111
	},
	{
		id: 211,
		time: 210,
		velocity: 8.95861111111111,
		power: -3052.16763445846,
		road: 1619.1112037037,
		acceleration: -0.521203703703705
	},
	{
		id: 212,
		time: 211,
		velocity: 8.1675,
		power: -9929.41154714849,
		road: 1627.42398148148,
		acceleration: -1.42148148148148
	},
	{
		id: 213,
		time: 212,
		velocity: 5.68,
		power: -9602.34983929372,
		road: 1634.20467592592,
		acceleration: -1.64268518518518
	},
	{
		id: 214,
		time: 213,
		velocity: 4.03055555555556,
		power: -7895.85889298984,
		road: 1639.27305555555,
		acceleration: -1.78194444444445
	},
	{
		id: 215,
		time: 214,
		velocity: 2.82166666666667,
		power: -4504.1876587607,
		road: 1642.6887037037,
		acceleration: -1.52351851851852
	},
	{
		id: 216,
		time: 215,
		velocity: 1.10944444444444,
		power: -2281.54413719813,
		road: 1644.67083333333,
		acceleration: -1.34351851851852
	},
	{
		id: 217,
		time: 216,
		velocity: 0,
		power: -647.006124639805,
		road: 1645.51092592592,
		acceleration: -0.940555555555556
	},
	{
		id: 218,
		time: 217,
		velocity: 0,
		power: -42.4423487004548,
		road: 1645.69583333333,
		acceleration: -0.369814814814815
	},
	{
		id: 219,
		time: 218,
		velocity: 0,
		power: 0,
		road: 1645.69583333333,
		acceleration: 0
	},
	{
		id: 220,
		time: 219,
		velocity: 0,
		power: 0,
		road: 1645.69583333333,
		acceleration: 0
	},
	{
		id: 221,
		time: 220,
		velocity: 0,
		power: 0,
		road: 1645.69583333333,
		acceleration: 0
	},
	{
		id: 222,
		time: 221,
		velocity: 0,
		power: 0,
		road: 1645.69583333333,
		acceleration: 0
	},
	{
		id: 223,
		time: 222,
		velocity: 0,
		power: 0,
		road: 1645.69583333333,
		acceleration: 0
	},
	{
		id: 224,
		time: 223,
		velocity: 0,
		power: 22.4450873923365,
		road: 1645.77736111111,
		acceleration: 0.163055555555556
	},
	{
		id: 225,
		time: 224,
		velocity: 0.489166666666667,
		power: 615.195760734709,
		road: 1646.3987962963,
		acceleration: 0.916759259259259
	},
	{
		id: 226,
		time: 225,
		velocity: 2.75027777777778,
		power: 3320.30438701113,
		road: 1648.32324074074,
		acceleration: 1.68925925925926
	},
	{
		id: 227,
		time: 226,
		velocity: 5.06777777777778,
		power: 6626.70997012225,
		road: 1651.97925925926,
		acceleration: 1.77388888888889
	},
	{
		id: 228,
		time: 227,
		velocity: 5.81083333333333,
		power: 6999.85955983301,
		road: 1657.16162037037,
		acceleration: 1.2787962962963
	},
	{
		id: 229,
		time: 228,
		velocity: 6.58666666666667,
		power: 5928.20574502074,
		road: 1663.40777777778,
		acceleration: 0.848796296296295
	},
	{
		id: 230,
		time: 229,
		velocity: 7.61416666666667,
		power: 7846.72723944147,
		road: 1670.57550925926,
		acceleration: 0.994351851851853
	},
	{
		id: 231,
		time: 230,
		velocity: 8.79388888888889,
		power: 11445.3017979828,
		road: 1678.88101851852,
		acceleration: 1.2812037037037
	},
	{
		id: 232,
		time: 231,
		velocity: 10.4302777777778,
		power: 10953.2634009432,
		road: 1688.34560185185,
		acceleration: 1.03694444444444
	},
	{
		id: 233,
		time: 232,
		velocity: 10.725,
		power: 8618.68032423755,
		road: 1698.67273148148,
		acceleration: 0.68814814814815
	},
	{
		id: 234,
		time: 233,
		velocity: 10.8583333333333,
		power: 4613.26855073996,
		road: 1709.47143518518,
		acceleration: 0.255000000000001
	},
	{
		id: 235,
		time: 234,
		velocity: 11.1952777777778,
		power: 5854.13876724824,
		road: 1720.57564814815,
		acceleration: 0.356018518518518
	},
	{
		id: 236,
		time: 235,
		velocity: 11.7930555555556,
		power: 8707.76413715513,
		road: 1732.15078703704,
		acceleration: 0.585833333333333
	},
	{
		id: 237,
		time: 236,
		velocity: 12.6158333333333,
		power: 9345.35709288891,
		road: 1744.31601851852,
		acceleration: 0.594351851851851
	},
	{
		id: 238,
		time: 237,
		velocity: 12.9783333333333,
		power: 6646.81438594017,
		road: 1756.94550925926,
		acceleration: 0.334166666666668
	},
	{
		id: 239,
		time: 238,
		velocity: 12.7955555555556,
		power: 3256.70150783481,
		road: 1769.76513888889,
		acceleration: 0.0461111111111094
	},
	{
		id: 240,
		time: 239,
		velocity: 12.7541666666667,
		power: 3332.24998675737,
		road: 1782.63310185185,
		acceleration: 0.0505555555555581
	},
	{
		id: 241,
		time: 240,
		velocity: 13.13,
		power: 6381.57406066678,
		road: 1795.67111111111,
		acceleration: 0.289537037037036
	},
	{
		id: 242,
		time: 241,
		velocity: 13.6641666666667,
		power: 8795.17304637501,
		road: 1809.08291666667,
		acceleration: 0.458055555555555
	},
	{
		id: 243,
		time: 242,
		velocity: 14.1283333333333,
		power: 10109.4556479905,
		road: 1822.98625,
		acceleration: 0.525
	},
	{
		id: 244,
		time: 243,
		velocity: 14.705,
		power: 11282.0089098973,
		road: 1837.43810185185,
		acceleration: 0.572037037037035
	},
	{
		id: 245,
		time: 244,
		velocity: 15.3802777777778,
		power: 10854.3985746848,
		road: 1852.42782407407,
		acceleration: 0.503703703703705
	},
	{
		id: 246,
		time: 245,
		velocity: 15.6394444444444,
		power: 7595.64373195806,
		road: 1867.79763888889,
		acceleration: 0.256481481481481
	},
	{
		id: 247,
		time: 246,
		velocity: 15.4744444444444,
		power: 3096.55356301041,
		road: 1883.26921296296,
		acceleration: -0.0529629629629618
	},
	{
		id: 248,
		time: 247,
		velocity: 15.2213888888889,
		power: -28.1196252775351,
		road: 1898.58351851852,
		acceleration: -0.261574074074074
	},
	{
		id: 249,
		time: 248,
		velocity: 14.8547222222222,
		power: -246.346237531361,
		road: 1913.63092592593,
		acceleration: -0.272222222222224
	},
	{
		id: 250,
		time: 249,
		velocity: 14.6577777777778,
		power: 557.927211357954,
		road: 1928.43643518518,
		acceleration: -0.211574074074074
	},
	{
		id: 251,
		time: 250,
		velocity: 14.5866666666667,
		power: 3432.91134416217,
		road: 1943.1337962963,
		acceleration: -0.00472222222222207
	},
	{
		id: 252,
		time: 251,
		velocity: 14.8405555555556,
		power: 3986.40347665216,
		road: 1957.84587962963,
		acceleration: 0.0341666666666676
	},
	{
		id: 253,
		time: 252,
		velocity: 14.7602777777778,
		power: 4635.46888872561,
		road: 1972.61412037037,
		acceleration: 0.0781481481481485
	},
	{
		id: 254,
		time: 253,
		velocity: 14.8211111111111,
		power: 4394.99498292929,
		road: 1987.45069444444,
		acceleration: 0.0585185185185182
	},
	{
		id: 255,
		time: 254,
		velocity: 15.0161111111111,
		power: 4775.81023509652,
		road: 2002.35782407407,
		acceleration: 0.0825925925925937
	},
	{
		id: 256,
		time: 255,
		velocity: 15.0080555555556,
		power: 4750.75239655701,
		road: 2017.34509259259,
		acceleration: 0.0776851851851852
	},
	{
		id: 257,
		time: 256,
		velocity: 15.0541666666667,
		power: 4431.94094375689,
		road: 2032.39768518518,
		acceleration: 0.0529629629629618
	},
	{
		id: 258,
		time: 257,
		velocity: 15.175,
		power: 5919.74377628945,
		road: 2047.55268518518,
		acceleration: 0.151851851851852
	},
	{
		id: 259,
		time: 258,
		velocity: 15.4636111111111,
		power: 6351.61610973988,
		road: 2062.87069444444,
		acceleration: 0.174166666666665
	},
	{
		id: 260,
		time: 259,
		velocity: 15.5766666666667,
		power: 5764.79654820226,
		road: 2078.33958333333,
		acceleration: 0.127592592592595
	},
	{
		id: 261,
		time: 260,
		velocity: 15.5577777777778,
		power: 4028.52178168141,
		road: 2093.87615740741,
		acceleration: 0.00777777777777722
	},
	{
		id: 262,
		time: 261,
		velocity: 15.4869444444444,
		power: 3533.36951573242,
		road: 2109.40398148148,
		acceleration: -0.0252777777777791
	},
	{
		id: 263,
		time: 262,
		velocity: 15.5008333333333,
		power: 2956.92740223678,
		road: 2124.88777777778,
		acceleration: -0.0627777777777769
	},
	{
		id: 264,
		time: 263,
		velocity: 15.3694444444444,
		power: 3476.60764561769,
		road: 2140.32708333333,
		acceleration: -0.0262037037037022
	},
	{
		id: 265,
		time: 264,
		velocity: 15.4083333333333,
		power: 2537.01902616446,
		road: 2155.70916666667,
		acceleration: -0.0882407407407424
	},
	{
		id: 266,
		time: 265,
		velocity: 15.2361111111111,
		power: 3243.85176271003,
		road: 2171.02805555556,
		acceleration: -0.0381481481481476
	},
	{
		id: 267,
		time: 266,
		velocity: 15.255,
		power: 2648.54697202959,
		road: 2186.28930555556,
		acceleration: -0.0771296296296295
	},
	{
		id: 268,
		time: 267,
		velocity: 15.1769444444444,
		power: 3832.28419396992,
		road: 2201.51462962963,
		acceleration: 0.00527777777777771
	},
	{
		id: 269,
		time: 268,
		velocity: 15.2519444444444,
		power: 3815.74873448289,
		road: 2216.74458333333,
		acceleration: 0.00398148148148181
	},
	{
		id: 270,
		time: 269,
		velocity: 15.2669444444444,
		power: 5314.64058756435,
		road: 2232.02888888889,
		acceleration: 0.104722222222223
	},
	{
		id: 271,
		time: 270,
		velocity: 15.4911111111111,
		power: 6191.16551290939,
		road: 2247.4449537037,
		acceleration: 0.158796296296295
	},
	{
		id: 272,
		time: 271,
		velocity: 15.7283333333333,
		power: 8347.87855747539,
		road: 2263.08689814815,
		acceleration: 0.292962962962964
	},
	{
		id: 273,
		time: 272,
		velocity: 16.1458333333333,
		power: 7502.42944449268,
		road: 2278.98712962963,
		acceleration: 0.22361111111111
	},
	{
		id: 274,
		time: 273,
		velocity: 16.1619444444444,
		power: 8161.53980830321,
		road: 2295.12648148148,
		acceleration: 0.25462962962963
	},
	{
		id: 275,
		time: 274,
		velocity: 16.4922222222222,
		power: 6332.69674790057,
		road: 2311.45703703704,
		acceleration: 0.12777777777778
	},
	{
		id: 276,
		time: 275,
		velocity: 16.5291666666667,
		power: 6652.23001358379,
		road: 2327.92259259259,
		acceleration: 0.14222222222222
	},
	{
		id: 277,
		time: 276,
		velocity: 16.5886111111111,
		power: 5134.91957137641,
		road: 2344.48041666667,
		acceleration: 0.0423148148148158
	},
	{
		id: 278,
		time: 277,
		velocity: 16.6191666666667,
		power: 4302.076694537,
		road: 2361.05398148148,
		acceleration: -0.0108333333333377
	},
	{
		id: 279,
		time: 278,
		velocity: 16.4966666666667,
		power: 4657.45743685315,
		road: 2377.62791666667,
		acceleration: 0.0115740740740797
	},
	{
		id: 280,
		time: 279,
		velocity: 16.6233333333333,
		power: 3892.09276098093,
		road: 2394.18949074074,
		acceleration: -0.036296296296296
	},
	{
		id: 281,
		time: 280,
		velocity: 16.5102777777778,
		power: 5890.59208737713,
		road: 2410.77731481481,
		acceleration: 0.0887962962962945
	},
	{
		id: 282,
		time: 281,
		velocity: 16.7630555555556,
		power: 6749.04907472575,
		road: 2427.47847222222,
		acceleration: 0.137870370370369
	},
	{
		id: 283,
		time: 282,
		velocity: 17.0369444444444,
		power: 8533.92598686617,
		road: 2444.36856481481,
		acceleration: 0.240000000000006
	},
	{
		id: 284,
		time: 283,
		velocity: 17.2302777777778,
		power: 7995.08344078166,
		road: 2461.47671296296,
		acceleration: 0.196111111111108
	},
	{
		id: 285,
		time: 284,
		velocity: 17.3513888888889,
		power: 7659.49385592293,
		road: 2478.76648148148,
		acceleration: 0.167129629629628
	},
	{
		id: 286,
		time: 285,
		velocity: 17.5383333333333,
		power: 5988.41756604319,
		road: 2496.17055555556,
		acceleration: 0.0614814814814828
	},
	{
		id: 287,
		time: 286,
		velocity: 17.4147222222222,
		power: 3275.32858023997,
		road: 2513.5549537037,
		acceleration: -0.100833333333334
	},
	{
		id: 288,
		time: 287,
		velocity: 17.0488888888889,
		power: 648.327524309348,
		road: 2530.76148148148,
		acceleration: -0.254907407407408
	},
	{
		id: 289,
		time: 288,
		velocity: 16.7736111111111,
		power: -149.924843069656,
		road: 2547.69143518519,
		acceleration: -0.298240740740741
	},
	{
		id: 290,
		time: 289,
		velocity: 16.52,
		power: -216.052361623502,
		road: 2564.3237962963,
		acceleration: -0.296944444444442
	},
	{
		id: 291,
		time: 290,
		velocity: 16.1580555555556,
		power: 656.07947620654,
		road: 2580.68944444444,
		acceleration: -0.23648148148148
	},
	{
		id: 292,
		time: 291,
		velocity: 16.0641666666667,
		power: 4185.94416057646,
		road: 2596.93351851852,
		acceleration: -0.00666666666667126
	},
	{
		id: 293,
		time: 292,
		velocity: 16.5,
		power: 7651.96102656708,
		road: 2613.28,
		acceleration: 0.211481481481485
	},
	{
		id: 294,
		time: 293,
		velocity: 16.7925,
		power: 10406.8571103284,
		road: 2629.9175,
		acceleration: 0.370555555555555
	},
	{
		id: 295,
		time: 294,
		velocity: 17.1758333333333,
		power: 15839.0848174871,
		road: 2647.07601851852,
		acceleration: 0.671481481481486
	},
	{
		id: 296,
		time: 295,
		velocity: 18.5144444444444,
		power: 21566.9823805685,
		road: 2665.04263888889,
		acceleration: 0.944722222222218
	},
	{
		id: 297,
		time: 296,
		velocity: 19.6266666666667,
		power: 31800.3621387403,
		road: 2684.18199074074,
		acceleration: 1.40074074074074
	},
	{
		id: 298,
		time: 297,
		velocity: 21.3780555555556,
		power: 33958.1867866596,
		road: 2704.7025462963,
		acceleration: 1.36166666666666
	},
	{
		id: 299,
		time: 298,
		velocity: 22.5994444444444,
		power: 42598.1915914807,
		road: 2726.7124537037,
		acceleration: 1.61703703703704
	},
	{
		id: 300,
		time: 299,
		velocity: 24.4777777777778,
		power: 42984.6535774916,
		road: 2750.26157407407,
		acceleration: 1.46138888888889
	},
	{
		id: 301,
		time: 300,
		velocity: 25.7622222222222,
		power: 44481.5864832913,
		road: 2775.22930555556,
		acceleration: 1.37583333333333
	},
	{
		id: 302,
		time: 301,
		velocity: 26.7269444444444,
		power: 33155.0771322603,
		road: 2801.29185185185,
		acceleration: 0.813796296296299
	},
	{
		id: 303,
		time: 302,
		velocity: 26.9191666666667,
		power: 19484.9680450945,
		road: 2827.8799537037,
		acceleration: 0.237314814814813
	},
	{
		id: 304,
		time: 303,
		velocity: 26.4741666666667,
		power: 14450.6826426984,
		road: 2854.60347222222,
		acceleration: 0.0335185185185196
	},
	{
		id: 305,
		time: 304,
		velocity: 26.8275,
		power: 8847.49709185756,
		road: 2881.25310185185,
		acceleration: -0.181296296296299
	},
	{
		id: 306,
		time: 305,
		velocity: 26.3752777777778,
		power: 10015.6175806301,
		road: 2907.74768518518,
		acceleration: -0.128796296296294
	},
	{
		id: 307,
		time: 306,
		velocity: 26.0877777777778,
		power: 6716.60766971765,
		road: 2934.0525,
		acceleration: -0.250740740740742
	},
	{
		id: 308,
		time: 307,
		velocity: 26.0752777777778,
		power: 6082.42740594498,
		road: 2960.09902777778,
		acceleration: -0.265833333333333
	},
	{
		id: 309,
		time: 308,
		velocity: 25.5777777777778,
		power: 4686.06443452084,
		road: 2985.85708333333,
		acceleration: -0.311111111111114
	},
	{
		id: 310,
		time: 309,
		velocity: 25.1544444444444,
		power: 1885.11809325719,
		road: 3011.25319444444,
		acceleration: -0.412777777777777
	},
	{
		id: 311,
		time: 310,
		velocity: 24.8369444444444,
		power: 7605.58202552679,
		road: 3036.35962962963,
		acceleration: -0.16657407407407
	},
	{
		id: 312,
		time: 311,
		velocity: 25.0780555555556,
		power: 11953.2768755795,
		road: 3061.39138888889,
		acceleration: 0.0172222222222196
	},
	{
		id: 313,
		time: 312,
		velocity: 25.2061111111111,
		power: 16307.1136552973,
		road: 3086.52796296296,
		acceleration: 0.192407407407412
	},
	{
		id: 314,
		time: 313,
		velocity: 25.4141666666667,
		power: 13868.0351858904,
		road: 3111.80291666667,
		acceleration: 0.0843518518518493
	},
	{
		id: 315,
		time: 314,
		velocity: 25.3311111111111,
		power: 8502.17001082495,
		road: 3137.05231481481,
		acceleration: -0.135462962962968
	},
	{
		id: 316,
		time: 315,
		velocity: 24.7997222222222,
		power: -970.380700513756,
		road: 3161.97537037037,
		acceleration: -0.517222222222216
	},
	{
		id: 317,
		time: 316,
		velocity: 23.8625,
		power: -13992.3020150663,
		road: 3186.11041666667,
		acceleration: -1.0587962962963
	},
	{
		id: 318,
		time: 317,
		velocity: 22.1547222222222,
		power: -30376.2026902343,
		road: 3208.8099537037,
		acceleration: -1.81222222222222
	},
	{
		id: 319,
		time: 318,
		velocity: 19.3630555555556,
		power: -33578.8290616347,
		road: 3229.57453703704,
		acceleration: -2.05768518518519
	},
	{
		id: 320,
		time: 319,
		velocity: 17.6894444444444,
		power: -35563.9486640102,
		road: 3248.14912037037,
		acceleration: -2.32231481481481
	},
	{
		id: 321,
		time: 320,
		velocity: 15.1877777777778,
		power: -23775.0866951395,
		road: 3264.66925925926,
		acceleration: -1.78657407407407
	},
	{
		id: 322,
		time: 321,
		velocity: 14.0033333333333,
		power: -20086.0925015786,
		road: 3279.45981481481,
		acceleration: -1.67259259259259
	},
	{
		id: 323,
		time: 322,
		velocity: 12.6716666666667,
		power: -16656.2173976536,
		road: 3292.63921296296,
		acceleration: -1.54972222222222
	},
	{
		id: 324,
		time: 323,
		velocity: 10.5386111111111,
		power: -17880.0759617117,
		road: 3304.12648148148,
		acceleration: -1.83453703703704
	},
	{
		id: 325,
		time: 324,
		velocity: 8.49972222222222,
		power: -13689.7719268307,
		road: 3313.86828703704,
		acceleration: -1.65638888888889
	},
	{
		id: 326,
		time: 325,
		velocity: 7.7025,
		power: -6564.77216640637,
		road: 3322.28865740741,
		acceleration: -0.986481481481482
	},
	{
		id: 327,
		time: 326,
		velocity: 7.57916666666667,
		power: -145.997920553303,
		road: 3330.125,
		acceleration: -0.181574074074073
	},
	{
		id: 328,
		time: 327,
		velocity: 7.955,
		power: 3553.11721384137,
		road: 3338.02555555556,
		acceleration: 0.31
	},
	{
		id: 329,
		time: 328,
		velocity: 8.6325,
		power: 8885.85997866396,
		road: 3346.54472222222,
		acceleration: 0.927222222222222
	},
	{
		id: 330,
		time: 329,
		velocity: 10.3608333333333,
		power: 10941.2182490144,
		road: 3356.04296296296,
		acceleration: 1.03092592592593
	},
	{
		id: 331,
		time: 330,
		velocity: 11.0477777777778,
		power: 10862.4775488347,
		road: 3366.50666666667,
		acceleration: 0.9
	},
	{
		id: 332,
		time: 331,
		velocity: 11.3325,
		power: 5965.08039012081,
		road: 3377.60384259259,
		acceleration: 0.366944444444444
	},
	{
		id: 333,
		time: 332,
		velocity: 11.4616666666667,
		power: 4970.8156852767,
		road: 3389.0125462963,
		acceleration: 0.256111111111114
	},
	{
		id: 334,
		time: 333,
		velocity: 11.8161111111111,
		power: 6178.59436767536,
		road: 3400.72351851852,
		acceleration: 0.348425925925923
	},
	{
		id: 335,
		time: 334,
		velocity: 12.3777777777778,
		power: 7812.21356137108,
		road: 3412.84148148148,
		acceleration: 0.465555555555556
	},
	{
		id: 336,
		time: 335,
		velocity: 12.8583333333333,
		power: 11225.151989042,
		road: 3425.54592592593,
		acceleration: 0.707407407407409
	},
	{
		id: 337,
		time: 336,
		velocity: 13.9383333333333,
		power: 10161.4953339302,
		road: 3438.88902777778,
		acceleration: 0.569907407407406
	},
	{
		id: 338,
		time: 337,
		velocity: 14.0875,
		power: 10814.8374145608,
		road: 3452.80564814815,
		acceleration: 0.577129629629631
	},
	{
		id: 339,
		time: 338,
		velocity: 14.5897222222222,
		power: 8173.79727218965,
		road: 3467.18634259259,
		acceleration: 0.351018518518517
	},
	{
		id: 340,
		time: 339,
		velocity: 14.9913888888889,
		power: 9023.9776080525,
		road: 3481.93763888889,
		acceleration: 0.390185185185185
	},
	{
		id: 341,
		time: 340,
		velocity: 15.2580555555556,
		power: 7830.25035364459,
		road: 3497.02763888889,
		acceleration: 0.287222222222223
	},
	{
		id: 342,
		time: 341,
		velocity: 15.4513888888889,
		power: 7213.48913931031,
		road: 3512.37699074074,
		acceleration: 0.231481481481481
	},
	{
		id: 343,
		time: 342,
		velocity: 15.6858333333333,
		power: 8947.2371687993,
		road: 3528.00888888889,
		acceleration: 0.333611111111111
	},
	{
		id: 344,
		time: 343,
		velocity: 16.2588888888889,
		power: 10426.3823160936,
		road: 3544.01240740741,
		acceleration: 0.409629629629629
	},
	{
		id: 345,
		time: 344,
		velocity: 16.6802777777778,
		power: 12678.171137264,
		road: 3560.48300925926,
		acceleration: 0.524537037037035
	},
	{
		id: 346,
		time: 345,
		velocity: 17.2594444444444,
		power: 12935.3203954862,
		road: 3577.46888888889,
		acceleration: 0.506018518518523
	},
	{
		id: 347,
		time: 346,
		velocity: 17.7769444444444,
		power: 15764.658051294,
		road: 3595.02634259259,
		acceleration: 0.637129629629626
	},
	{
		id: 348,
		time: 347,
		velocity: 18.5916666666667,
		power: 15690.7495713729,
		road: 3613.19666666667,
		acceleration: 0.58861111111111
	},
	{
		id: 349,
		time: 348,
		velocity: 19.0252777777778,
		power: 11413.1806320574,
		road: 3631.81990740741,
		acceleration: 0.317222222222224
	},
	{
		id: 350,
		time: 349,
		velocity: 18.7286111111111,
		power: 5385.90401823997,
		road: 3650.58865740741,
		acceleration: -0.0262037037037004
	},
	{
		id: 351,
		time: 350,
		velocity: 18.5130555555556,
		power: 2501.47400987912,
		road: 3669.2524537037,
		acceleration: -0.183703703703706
	},
	{
		id: 352,
		time: 351,
		velocity: 18.4741666666667,
		power: 4229.54719294849,
		road: 3687.78314814815,
		acceleration: -0.0824999999999996
	},
	{
		id: 353,
		time: 352,
		velocity: 18.4811111111111,
		power: 4839.40044387476,
		road: 3706.24967592593,
		acceleration: -0.0458333333333343
	},
	{
		id: 354,
		time: 353,
		velocity: 18.3755555555556,
		power: 3366.10296531299,
		road: 3724.6300462963,
		acceleration: -0.126481481481481
	},
	{
		id: 355,
		time: 354,
		velocity: 18.0947222222222,
		power: 1170.11239589192,
		road: 3742.8237962963,
		acceleration: -0.24675925925926
	},
	{
		id: 356,
		time: 355,
		velocity: 17.7408333333333,
		power: 298.106029095593,
		road: 3760.74861111111,
		acceleration: -0.29111111111111
	},
	{
		id: 357,
		time: 356,
		velocity: 17.5022222222222,
		power: -1649.0173822143,
		road: 3778.32805555555,
		acceleration: -0.399629629629629
	},
	{
		id: 358,
		time: 357,
		velocity: 16.8958333333333,
		power: 203.14274264477,
		road: 3795.56638888889,
		acceleration: -0.282592592592593
	},
	{
		id: 359,
		time: 358,
		velocity: 16.8930555555556,
		power: 3335.93916090558,
		road: 3812.62,
		acceleration: -0.0868518518518542
	},
	{
		id: 360,
		time: 359,
		velocity: 17.2416666666667,
		power: 7367.30810835171,
		road: 3829.70949074074,
		acceleration: 0.158611111111114
	},
	{
		id: 361,
		time: 360,
		velocity: 17.3716666666667,
		power: 8101.88100068658,
		road: 3846.97578703704,
		acceleration: 0.195
	},
	{
		id: 362,
		time: 361,
		velocity: 17.4780555555556,
		power: 9032.13188321255,
		road: 3864.45967592592,
		acceleration: 0.240185185185187
	},
	{
		id: 363,
		time: 362,
		velocity: 17.9622222222222,
		power: 8162.48670932784,
		road: 3882.15277777778,
		acceleration: 0.178240740740737
	},
	{
		id: 364,
		time: 363,
		velocity: 17.9063888888889,
		power: 8972.02874799996,
		road: 3900.04310185185,
		acceleration: 0.216203703703705
	},
	{
		id: 365,
		time: 364,
		velocity: 18.1266666666667,
		power: 7610.07088567504,
		road: 3918.10597222222,
		acceleration: 0.128888888888888
	},
	{
		id: 366,
		time: 365,
		velocity: 18.3488888888889,
		power: 7961.39700026131,
		road: 3936.30476851852,
		acceleration: 0.142962962962965
	},
	{
		id: 367,
		time: 366,
		velocity: 18.3352777777778,
		power: 7406.5501396777,
		road: 3954.62787037037,
		acceleration: 0.105648148148148
	},
	{
		id: 368,
		time: 367,
		velocity: 18.4436111111111,
		power: 6837.7079395796,
		road: 3973.03856481481,
		acceleration: 0.069537037037037
	},
	{
		id: 369,
		time: 368,
		velocity: 18.5575,
		power: 5194.19427401951,
		road: 3991.47175925926,
		acceleration: -0.0245370370370352
	},
	{
		id: 370,
		time: 369,
		velocity: 18.2616666666667,
		power: 3157.0786730968,
		road: 4009.82393518518,
		acceleration: -0.137500000000003
	},
	{
		id: 371,
		time: 370,
		velocity: 18.0311111111111,
		power: 937.755907501934,
		road: 4027.97777777778,
		acceleration: -0.259166666666669
	},
	{
		id: 372,
		time: 371,
		velocity: 17.78,
		power: 1365.59198739622,
		road: 4045.88777777778,
		acceleration: -0.228518518518516
	},
	{
		id: 373,
		time: 372,
		velocity: 17.5761111111111,
		power: 678.190931885698,
		road: 4063.55194444444,
		acceleration: -0.263148148148147
	},
	{
		id: 374,
		time: 373,
		velocity: 17.2416666666667,
		power: 1080.6202892815,
		road: 4080.96777777778,
		acceleration: -0.233518518518519
	},
	{
		id: 375,
		time: 374,
		velocity: 17.0794444444444,
		power: 5445.60172374259,
		road: 4098.28300925926,
		acceleration: 0.0323148148148142
	},
	{
		id: 376,
		time: 375,
		velocity: 17.6730555555556,
		power: 9101.32466963798,
		road: 4115.73731481481,
		acceleration: 0.245833333333334
	},
	{
		id: 377,
		time: 376,
		velocity: 17.9791666666667,
		power: 11935.2161918354,
		road: 4133.51259259259,
		acceleration: 0.396111111111107
	},
	{
		id: 378,
		time: 377,
		velocity: 18.2677777777778,
		power: 11149.2657020911,
		road: 4151.65060185185,
		acceleration: 0.329351851851857
	},
	{
		id: 379,
		time: 378,
		velocity: 18.6611111111111,
		power: 6710.65083855885,
		road: 4169.98601851852,
		acceleration: 0.0654629629629611
	},
	{
		id: 380,
		time: 379,
		velocity: 18.1755555555556,
		power: 2350.9292165857,
		road: 4188.26351851852,
		acceleration: -0.181296296296296
	},
	{
		id: 381,
		time: 380,
		velocity: 17.7238888888889,
		power: -1336.82983334537,
		road: 4206.25662037037,
		acceleration: -0.387500000000003
	},
	{
		id: 382,
		time: 381,
		velocity: 17.4986111111111,
		power: 432.740662291045,
		road: 4223.91717592592,
		acceleration: -0.27759259259259
	},
	{
		id: 383,
		time: 382,
		velocity: 17.3427777777778,
		power: 1585.37336092066,
		road: 4241.33726851852,
		acceleration: -0.203333333333333
	},
	{
		id: 384,
		time: 383,
		velocity: 17.1138888888889,
		power: 295.141033325021,
		road: 4258.51777777778,
		acceleration: -0.275833333333335
	},
	{
		id: 385,
		time: 384,
		velocity: 16.6711111111111,
		power: 537.646587226215,
		road: 4275.43263888889,
		acceleration: -0.255462962962962
	},
	{
		id: 386,
		time: 385,
		velocity: 16.5763888888889,
		power: 260.369512849507,
		road: 4292.08606481481,
		acceleration: -0.267407407407408
	},
	{
		id: 387,
		time: 386,
		velocity: 16.3116666666667,
		power: 1997.9227206919,
		road: 4308.52939814815,
		acceleration: -0.152777777777779
	},
	{
		id: 388,
		time: 387,
		velocity: 16.2127777777778,
		power: 1897.35439170356,
		road: 4324.81875,
		acceleration: -0.155185185185186
	},
	{
		id: 389,
		time: 388,
		velocity: 16.1108333333333,
		power: 2568.47034532039,
		road: 4340.97634259259,
		acceleration: -0.108333333333334
	},
	{
		id: 390,
		time: 389,
		velocity: 15.9866666666667,
		power: 2679.50332449056,
		road: 4357.03069444444,
		acceleration: -0.0981481481481445
	},
	{
		id: 391,
		time: 390,
		velocity: 15.9183333333333,
		power: 2146.45493016772,
		road: 4372.97106481481,
		acceleration: -0.129814814814814
	},
	{
		id: 392,
		time: 391,
		velocity: 15.7213888888889,
		power: 894.262520837412,
		road: 4388.74236111111,
		acceleration: -0.208333333333336
	},
	{
		id: 393,
		time: 392,
		velocity: 15.3616666666667,
		power: 1810.98748428737,
		road: 4404.33796296296,
		acceleration: -0.143055555555554
	},
	{
		id: 394,
		time: 393,
		velocity: 15.4891666666667,
		power: 2595.14756285942,
		road: 4419.81847222222,
		acceleration: -0.0871296296296293
	},
	{
		id: 395,
		time: 394,
		velocity: 15.46,
		power: 5457.34033578725,
		road: 4435.30837962963,
		acceleration: 0.105925925925927
	},
	{
		id: 396,
		time: 395,
		velocity: 15.6794444444444,
		power: 3509.4013777332,
		road: 4450.83777777778,
		acceleration: -0.026944444444446
	},
	{
		id: 397,
		time: 396,
		velocity: 15.4083333333333,
		power: 3416.8119896008,
		road: 4466.33759259259,
		acceleration: -0.0322222222222219
	},
	{
		id: 398,
		time: 397,
		velocity: 15.3633333333333,
		power: 1287.06495967848,
		road: 4481.73449074074,
		acceleration: -0.173611111111112
	},
	{
		id: 399,
		time: 398,
		velocity: 15.1586111111111,
		power: 2307.69131499781,
		road: 4496.99435185185,
		acceleration: -0.100462962962961
	},
	{
		id: 400,
		time: 399,
		velocity: 15.1069444444444,
		power: 2332.7084118121,
		road: 4512.15597222222,
		acceleration: -0.0960185185185178
	},
	{
		id: 401,
		time: 400,
		velocity: 15.0752777777778,
		power: 3618.54213468502,
		road: 4527.26680555556,
		acceleration: -0.00555555555555465
	},
	{
		id: 402,
		time: 401,
		velocity: 15.1419444444444,
		power: 4048.43527298781,
		road: 4542.38680555555,
		acceleration: 0.0238888888888873
	},
	{
		id: 403,
		time: 402,
		velocity: 15.1786111111111,
		power: 4516.7911199595,
		road: 4557.54615740741,
		acceleration: 0.0548148148148151
	},
	{
		id: 404,
		time: 403,
		velocity: 15.2397222222222,
		power: 3760.91435554116,
		road: 4572.73375,
		acceleration: 0.00166666666666693
	},
	{
		id: 405,
		time: 404,
		velocity: 15.1469444444444,
		power: 4357.20745716694,
		road: 4587.94314814815,
		acceleration: 0.0419444444444412
	},
	{
		id: 406,
		time: 405,
		velocity: 15.3044444444444,
		power: 3840.26834211345,
		road: 4603.1762962963,
		acceleration: 0.0055555555555582
	},
	{
		id: 407,
		time: 406,
		velocity: 15.2563888888889,
		power: 3904.80702496923,
		road: 4618.41708333333,
		acceleration: 0.00972222222222108
	},
	{
		id: 408,
		time: 407,
		velocity: 15.1761111111111,
		power: 2998.55213509353,
		road: 4633.63680555555,
		acceleration: -0.0518518518518505
	},
	{
		id: 409,
		time: 408,
		velocity: 15.1488888888889,
		power: 3623.60820583717,
		road: 4648.82666666667,
		acceleration: -0.00787037037036953
	},
	{
		id: 410,
		time: 409,
		velocity: 15.2327777777778,
		power: 3533.72244371958,
		road: 4664.00574074074,
		acceleration: -0.0137037037037047
	},
	{
		id: 411,
		time: 410,
		velocity: 15.135,
		power: 2822.95562052482,
		road: 4679.14717592593,
		acceleration: -0.0615740740740733
	},
	{
		id: 412,
		time: 411,
		velocity: 14.9641666666667,
		power: 491.545516063912,
		road: 4694.14782407407,
		acceleration: -0.220000000000002
	},
	{
		id: 413,
		time: 412,
		velocity: 14.5727777777778,
		power: 510.02964957375,
		road: 4708.9312037037,
		acceleration: -0.214537037037035
	},
	{
		id: 414,
		time: 413,
		velocity: 14.4913888888889,
		power: -30.0506022891729,
		road: 4723.48282407407,
		acceleration: -0.248981481481481
	},
	{
		id: 415,
		time: 414,
		velocity: 14.2172222222222,
		power: 207.296395110969,
		road: 4737.79606481481,
		acceleration: -0.227777777777778
	},
	{
		id: 416,
		time: 415,
		velocity: 13.8894444444444,
		power: -1609.98674481567,
		road: 4751.81615740741,
		acceleration: -0.358518518518522
	},
	{
		id: 417,
		time: 416,
		velocity: 13.4158333333333,
		power: -1937.99361899175,
		road: 4765.46638888889,
		acceleration: -0.381203703703701
	},
	{
		id: 418,
		time: 417,
		velocity: 13.0736111111111,
		power: -3256.52637444125,
		road: 4778.68398148148,
		acceleration: -0.484074074074076
	},
	{
		id: 419,
		time: 418,
		velocity: 12.4372222222222,
		power: -7212.47357159336,
		road: 4791.25050925926,
		acceleration: -0.818055555555555
	},
	{
		id: 420,
		time: 419,
		velocity: 10.9616666666667,
		power: -7139.8116363936,
		road: 4802.98648148148,
		acceleration: -0.843055555555557
	},
	{
		id: 421,
		time: 420,
		velocity: 10.5444444444444,
		power: -6492.21818982755,
		road: 4813.89138888889,
		acceleration: -0.819074074074074
	},
	{
		id: 422,
		time: 421,
		velocity: 9.98,
		power: -3162.46148219288,
		road: 4824.13143518518,
		acceleration: -0.510648148148146
	},
	{
		id: 423,
		time: 422,
		velocity: 9.42972222222222,
		power: -1798.01134457707,
		road: 4833.92907407407,
		acceleration: -0.374166666666667
	},
	{
		id: 424,
		time: 423,
		velocity: 9.42194444444444,
		power: 3018.65164249369,
		road: 4843.61310185185,
		acceleration: 0.146944444444445
	},
	{
		id: 425,
		time: 424,
		velocity: 10.4208333333333,
		power: 4540.66542769078,
		road: 4853.51976851852,
		acceleration: 0.298333333333332
	},
	{
		id: 426,
		time: 425,
		velocity: 10.3247222222222,
		power: 5634.98186288557,
		road: 4863.77069444444,
		acceleration: 0.390185185185187
	},
	{
		id: 427,
		time: 426,
		velocity: 10.5925,
		power: 2652.75573219107,
		road: 4874.25476851852,
		acceleration: 0.0761111111111106
	},
	{
		id: 428,
		time: 427,
		velocity: 10.6491666666667,
		power: 3716.15821374356,
		road: 4884.86518518518,
		acceleration: 0.176574074074075
	},
	{
		id: 429,
		time: 428,
		velocity: 10.8544444444444,
		power: 3748.68073728417,
		road: 4895.64972222222,
		acceleration: 0.171666666666667
	},
	{
		id: 430,
		time: 429,
		velocity: 11.1075,
		power: 6063.88606074789,
		road: 4906.70944444444,
		acceleration: 0.378703703703701
	},
	{
		id: 431,
		time: 430,
		velocity: 11.7852777777778,
		power: 5154.86142074277,
		road: 4918.09564814815,
		acceleration: 0.27425925925926
	},
	{
		id: 432,
		time: 431,
		velocity: 11.6772222222222,
		power: 3990.16712328182,
		road: 4929.69763888889,
		acceleration: 0.157314814814816
	},
	{
		id: 433,
		time: 432,
		velocity: 11.5794444444444,
		power: 1456.78679424417,
		road: 4941.34194444444,
		acceleration: -0.0726851851851862
	},
	{
		id: 434,
		time: 433,
		velocity: 11.5672222222222,
		power: 2894.25572946327,
		road: 4952.97842592592,
		acceleration: 0.0570370370370377
	},
	{
		id: 435,
		time: 434,
		velocity: 11.8483333333333,
		power: 4438.04366340565,
		road: 4964.73856481481,
		acceleration: 0.190277777777776
	},
	{
		id: 436,
		time: 435,
		velocity: 12.1502777777778,
		power: 6949.48449705049,
		road: 4976.79138888889,
		acceleration: 0.395092592592594
	},
	{
		id: 437,
		time: 436,
		velocity: 12.7525,
		power: 6312.73941511652,
		road: 4989.2012037037,
		acceleration: 0.318888888888889
	},
	{
		id: 438,
		time: 437,
		velocity: 12.805,
		power: 5967.52834050261,
		road: 5001.90731481481,
		acceleration: 0.273703703703703
	},
	{
		id: 439,
		time: 438,
		velocity: 12.9713888888889,
		power: 4729.68219540739,
		road: 5014.8312037037,
		acceleration: 0.161851851851852
	},
	{
		id: 440,
		time: 439,
		velocity: 13.2380555555556,
		power: 4690.19075075624,
		road: 5027.91189814815,
		acceleration: 0.15175925925926
	},
	{
		id: 441,
		time: 440,
		velocity: 13.2602777777778,
		power: 3587.2417260661,
		road: 5041.09824074074,
		acceleration: 0.0595370370370372
	},
	{
		id: 442,
		time: 441,
		velocity: 13.15,
		power: 2146.39193685795,
		road: 5054.28685185185,
		acceleration: -0.0549999999999997
	},
	{
		id: 443,
		time: 442,
		velocity: 13.0730555555556,
		power: 3129.32179494431,
		road: 5067.45976851852,
		acceleration: 0.0236111111111104
	},
	{
		id: 444,
		time: 443,
		velocity: 13.3311111111111,
		power: 5530.79235612908,
		road: 5080.74898148148,
		acceleration: 0.20898148148148
	},
	{
		id: 445,
		time: 444,
		velocity: 13.7769444444444,
		power: 7312.63397443005,
		road: 5094.30953703703,
		acceleration: 0.333703703703705
	},
	{
		id: 446,
		time: 445,
		velocity: 14.0741666666667,
		power: 8215.6826432484,
		road: 5108.22768518518,
		acceleration: 0.381481481481483
	},
	{
		id: 447,
		time: 446,
		velocity: 14.4755555555556,
		power: 6826.10731576567,
		road: 5122.46666666666,
		acceleration: 0.260185185185183
	},
	{
		id: 448,
		time: 447,
		velocity: 14.5575,
		power: 7268.23834513128,
		road: 5136.9749074074,
		acceleration: 0.278333333333336
	},
	{
		id: 449,
		time: 448,
		velocity: 14.9091666666667,
		power: 7040.50968077054,
		road: 5151.74652777777,
		acceleration: 0.248425925925925
	},
	{
		id: 450,
		time: 449,
		velocity: 15.2208333333333,
		power: 6410.46545510899,
		road: 5166.73902777777,
		acceleration: 0.193333333333332
	},
	{
		id: 451,
		time: 450,
		velocity: 15.1375,
		power: 3717.97199960777,
		road: 5181.82921296296,
		acceleration: 0.00203703703703795
	},
	{
		id: 452,
		time: 451,
		velocity: 14.9152777777778,
		power: -1651.85918786975,
		road: 5196.73606481481,
		acceleration: -0.368703703703702
	},
	{
		id: 453,
		time: 452,
		velocity: 14.1147222222222,
		power: -2222.87037584593,
		road: 5211.25527777777,
		acceleration: -0.406574074074078
	},
	{
		id: 454,
		time: 453,
		velocity: 13.9177777777778,
		power: -1233.04329959001,
		road: 5225.40541666666,
		acceleration: -0.331574074074075
	},
	{
		id: 455,
		time: 454,
		velocity: 13.9205555555556,
		power: 1673.24969365077,
		road: 5239.33425925926,
		acceleration: -0.111018518518517
	},
	{
		id: 456,
		time: 455,
		velocity: 13.7816666666667,
		power: 544.478325343689,
		road: 5253.11106481481,
		acceleration: -0.193055555555556
	},
	{
		id: 457,
		time: 456,
		velocity: 13.3386111111111,
		power: -1369.86255257483,
		road: 5266.62305555555,
		acceleration: -0.336574074074074
	},
	{
		id: 458,
		time: 457,
		velocity: 12.9108333333333,
		power: -2590.87389765617,
		road: 5279.75106481481,
		acceleration: -0.431388888888888
	},
	{
		id: 459,
		time: 458,
		velocity: 12.4875,
		power: -1221.87314506392,
		road: 5292.50361111111,
		acceleration: -0.319537037037037
	},
	{
		id: 460,
		time: 459,
		velocity: 12.38,
		power: 1200.47130865426,
		road: 5305.03856481481,
		acceleration: -0.11564814814815
	},
	{
		id: 461,
		time: 460,
		velocity: 12.5638888888889,
		power: 4051.67410462046,
		road: 5317.57699074074,
		acceleration: 0.122592592592593
	},
	{
		id: 462,
		time: 461,
		velocity: 12.8552777777778,
		power: 4996.62240421878,
		road: 5330.27375,
		acceleration: 0.194074074074077
	},
	{
		id: 463,
		time: 462,
		velocity: 12.9622222222222,
		power: 3203.64143135615,
		road: 5343.08851851852,
		acceleration: 0.041944444444443
	},
	{
		id: 464,
		time: 463,
		velocity: 12.6897222222222,
		power: 1244.51395633337,
		road: 5355.86555555555,
		acceleration: -0.117407407407407
	},
	{
		id: 465,
		time: 464,
		velocity: 12.5030555555556,
		power: 244.232177060107,
		road: 5368.48541666666,
		acceleration: -0.196944444444446
	},
	{
		id: 466,
		time: 465,
		velocity: 12.3713888888889,
		power: 1020.28505393909,
		road: 5380.94226851852,
		acceleration: -0.129074074074072
	},
	{
		id: 467,
		time: 466,
		velocity: 12.3025,
		power: 544.578022038907,
		road: 5393.25134259259,
		acceleration: -0.166481481481483
	},
	{
		id: 468,
		time: 467,
		velocity: 12.0036111111111,
		power: -1290.8247951411,
		road: 5405.31634259259,
		acceleration: -0.321666666666665
	},
	{
		id: 469,
		time: 468,
		velocity: 11.4063888888889,
		power: -2643.65785396545,
		road: 5416.99967592592,
		acceleration: -0.441666666666666
	},
	{
		id: 470,
		time: 469,
		velocity: 10.9775,
		power: -2313.53542665893,
		road: 5428.2549537037,
		acceleration: -0.414444444444444
	},
	{
		id: 471,
		time: 470,
		velocity: 10.7602777777778,
		power: 202.740272786649,
		road: 5439.21513888889,
		acceleration: -0.175740740740743
	},
	{
		id: 472,
		time: 471,
		velocity: 10.8791666666667,
		power: 2033.66777690583,
		road: 5450.08851851852,
		acceleration: 0.00212962962963026
	},
	{
		id: 473,
		time: 472,
		velocity: 10.9838888888889,
		power: 4471.83551909446,
		road: 5461.07861111111,
		acceleration: 0.231296296296298
	},
	{
		id: 474,
		time: 473,
		velocity: 11.4541666666667,
		power: 5507.23842582594,
		road: 5472.34129629629,
		acceleration: 0.313888888888888
	},
	{
		id: 475,
		time: 474,
		velocity: 11.8208333333333,
		power: 6535.81810957315,
		road: 5483.95425925926,
		acceleration: 0.386666666666665
	},
	{
		id: 476,
		time: 475,
		velocity: 12.1438888888889,
		power: 6996.63294314411,
		road: 5495.96162037037,
		acceleration: 0.402129629629632
	},
	{
		id: 477,
		time: 476,
		velocity: 12.6605555555556,
		power: 7248.40932006166,
		road: 5508.36907407407,
		acceleration: 0.398055555555553
	},
	{
		id: 478,
		time: 477,
		velocity: 13.015,
		power: 7305.70140151514,
		road: 5521.16481481481,
		acceleration: 0.378518518518518
	},
	{
		id: 479,
		time: 478,
		velocity: 13.2794444444444,
		power: 5937.95604157055,
		road: 5534.27490740741,
		acceleration: 0.250185185185186
	},
	{
		id: 480,
		time: 479,
		velocity: 13.4111111111111,
		power: 4557.47629025425,
		road: 5547.57597222222,
		acceleration: 0.131759259259258
	},
	{
		id: 481,
		time: 480,
		velocity: 13.4102777777778,
		power: 2459.5004303504,
		road: 5560.92546296296,
		acceleration: -0.0349074074074061
	},
	{
		id: 482,
		time: 481,
		velocity: 13.1747222222222,
		power: 870.131374618399,
		road: 5574.1786574074,
		acceleration: -0.157685185185187
	},
	{
		id: 483,
		time: 482,
		velocity: 12.9380555555556,
		power: 2276.73024388539,
		road: 5587.3312037037,
		acceleration: -0.0436111111111117
	},
	{
		id: 484,
		time: 483,
		velocity: 13.2794444444444,
		power: 2183.50324924811,
		road: 5600.43708333333,
		acceleration: -0.0497222222222202
	},
	{
		id: 485,
		time: 484,
		velocity: 13.0255555555556,
		power: 3493.52755634525,
		road: 5613.54555555555,
		acceleration: 0.0549074074074074
	},
	{
		id: 486,
		time: 485,
		velocity: 13.1027777777778,
		power: 506.573810649072,
		road: 5626.59013888889,
		acceleration: -0.182685185185186
	},
	{
		id: 487,
		time: 486,
		velocity: 12.7313888888889,
		power: 856.936036903031,
		road: 5639.46777777778,
		acceleration: -0.151203703703702
	},
	{
		id: 488,
		time: 487,
		velocity: 12.5719444444444,
		power: -460.898508313203,
		road: 5652.14175925926,
		acceleration: -0.256111111111112
	},
	{
		id: 489,
		time: 488,
		velocity: 12.3344444444444,
		power: -199.722724417608,
		road: 5664.57199074074,
		acceleration: -0.23138888888889
	},
	{
		id: 490,
		time: 489,
		velocity: 12.0372222222222,
		power: 186.74507388158,
		road: 5676.78875,
		acceleration: -0.195555555555554
	},
	{
		id: 491,
		time: 490,
		velocity: 11.9852777777778,
		power: 507.531765793345,
		road: 5688.82527777778,
		acceleration: -0.16490740740741
	},
	{
		id: 492,
		time: 491,
		velocity: 11.8397222222222,
		power: 1771.45172911697,
		road: 5700.75337962963,
		acceleration: -0.0519444444444428
	},
	{
		id: 493,
		time: 492,
		velocity: 11.8813888888889,
		power: 2476.11559426522,
		road: 5712.66083333333,
		acceleration: 0.0106481481481477
	},
	{
		id: 494,
		time: 493,
		velocity: 12.0172222222222,
		power: 3414.55594250293,
		road: 5724.61925925926,
		acceleration: 0.0912962962962975
	},
	{
		id: 495,
		time: 494,
		velocity: 12.1136111111111,
		power: 4830.83270895161,
		road: 5736.72740740741,
		acceleration: 0.208148148148149
	},
	{
		id: 496,
		time: 495,
		velocity: 12.5058333333333,
		power: 5015.03614264554,
		road: 5749.04648148148,
		acceleration: 0.213703703703702
	},
	{
		id: 497,
		time: 496,
		velocity: 12.6583333333333,
		power: 4910.65835672425,
		road: 5761.5699537037,
		acceleration: 0.195092592592593
	},
	{
		id: 498,
		time: 497,
		velocity: 12.6988888888889,
		power: 3747.42456474812,
		road: 5774.23703703704,
		acceleration: 0.0921296296296301
	},
	{
		id: 499,
		time: 498,
		velocity: 12.7822222222222,
		power: 3174.93683876328,
		road: 5786.97138888889,
		acceleration: 0.0424074074074063
	},
	{
		id: 500,
		time: 499,
		velocity: 12.7855555555556,
		power: 2883.16126646267,
		road: 5799.73564814815,
		acceleration: 0.0174074074074078
	},
	{
		id: 501,
		time: 500,
		velocity: 12.7511111111111,
		power: 2880.99636087561,
		road: 5812.51694444444,
		acceleration: 0.0166666666666675
	},
	{
		id: 502,
		time: 501,
		velocity: 12.8322222222222,
		power: 3762.63457461984,
		road: 5825.35004629629,
		acceleration: 0.0869444444444429
	},
	{
		id: 503,
		time: 502,
		velocity: 13.0463888888889,
		power: 4992.97225058479,
		road: 5838.31722222222,
		acceleration: 0.181203703703705
	},
	{
		id: 504,
		time: 503,
		velocity: 13.2947222222222,
		power: 5398.98488852029,
		road: 5851.47736111111,
		acceleration: 0.20472222222222
	},
	{
		id: 505,
		time: 504,
		velocity: 13.4463888888889,
		power: 4764.70918862679,
		road: 5864.81314814815,
		acceleration: 0.146574074074074
	},
	{
		id: 506,
		time: 505,
		velocity: 13.4861111111111,
		power: 4489.58612343767,
		road: 5878.28194444444,
		acceleration: 0.119444444444447
	},
	{
		id: 507,
		time: 506,
		velocity: 13.6530555555556,
		power: 4942.44322167612,
		road: 5891.88486111111,
		acceleration: 0.148796296296297
	},
	{
		id: 508,
		time: 507,
		velocity: 13.8927777777778,
		power: 5463.63150363065,
		road: 5905.65282407407,
		acceleration: 0.181296296296296
	},
	{
		id: 509,
		time: 508,
		velocity: 14.03,
		power: 6808.12473395381,
		road: 5919.64722222222,
		acceleration: 0.271574074074074
	},
	{
		id: 510,
		time: 509,
		velocity: 14.4677777777778,
		power: 6960.11780054098,
		road: 5933.91175925926,
		acceleration: 0.268703703703704
	},
	{
		id: 511,
		time: 510,
		velocity: 14.6988888888889,
		power: 7543.65719149026,
		road: 5948.4587037037,
		acceleration: 0.296111111111111
	},
	{
		id: 512,
		time: 511,
		velocity: 14.9183333333333,
		power: 6669.02410017919,
		road: 5963.26393518518,
		acceleration: 0.220462962962962
	},
	{
		id: 513,
		time: 512,
		velocity: 15.1291666666667,
		power: 5033.30004105235,
		road: 5978.22856481481,
		acceleration: 0.0983333333333345
	},
	{
		id: 514,
		time: 513,
		velocity: 14.9938888888889,
		power: 2936.37599406675,
		road: 5993.21782407407,
		acceleration: -0.0490740740740758
	},
	{
		id: 515,
		time: 514,
		velocity: 14.7711111111111,
		power: 617.764416876289,
		road: 6008.07833333333,
		acceleration: -0.208425925925924
	},
	{
		id: 516,
		time: 515,
		velocity: 14.5038888888889,
		power: -804.850287232067,
		road: 6022.68194444444,
		acceleration: -0.305370370370373
	},
	{
		id: 517,
		time: 516,
		velocity: 14.0777777777778,
		power: -1726.28019725267,
		road: 6036.94842592592,
		acceleration: -0.36888888888889
	},
	{
		id: 518,
		time: 517,
		velocity: 13.6644444444444,
		power: -1232.00794046658,
		road: 6050.86578703704,
		acceleration: -0.32935185185185
	},
	{
		id: 519,
		time: 518,
		velocity: 13.5158333333333,
		power: 436.176063157511,
		road: 6064.51893518518,
		acceleration: -0.199074074074074
	},
	{
		id: 520,
		time: 519,
		velocity: 13.4805555555556,
		power: 3503.72888043325,
		road: 6078.09208333333,
		acceleration: 0.0390740740740743
	},
	{
		id: 521,
		time: 520,
		velocity: 13.7816666666667,
		power: 5292.67281810415,
		road: 6091.77092592592,
		acceleration: 0.172314814814817
	},
	{
		id: 522,
		time: 521,
		velocity: 14.0327777777778,
		power: 7594.80046065087,
		road: 6105.70291666666,
		acceleration: 0.33398148148148
	},
	{
		id: 523,
		time: 522,
		velocity: 14.4825,
		power: 7064.58003195103,
		road: 6119.94078703704,
		acceleration: 0.277777777777779
	},
	{
		id: 524,
		time: 523,
		velocity: 14.615,
		power: 7288.36055415005,
		road: 6134.45722222222,
		acceleration: 0.279351851851851
	},
	{
		id: 525,
		time: 524,
		velocity: 14.8708333333333,
		power: 6104.94040147229,
		road: 6149.2049537037,
		acceleration: 0.183240740740739
	},
	{
		id: 526,
		time: 525,
		velocity: 15.0322222222222,
		power: 5786.32570451571,
		road: 6164.12087962963,
		acceleration: 0.15314814814815
	},
	{
		id: 527,
		time: 526,
		velocity: 15.0744444444444,
		power: 4136.44163990868,
		road: 6179.13037037037,
		acceleration: 0.0339814814814812
	},
	{
		id: 528,
		time: 527,
		velocity: 14.9727777777778,
		power: 2819.35500284886,
		road: 6194.12810185185,
		acceleration: -0.057500000000001
	},
	{
		id: 529,
		time: 528,
		velocity: 14.8597222222222,
		power: 2531.81794293141,
		road: 6209.05925925926,
		acceleration: -0.0756481481481472
	},
	{
		id: 530,
		time: 529,
		velocity: 14.8475,
		power: 2691.57499476736,
		road: 6223.92138888889,
		acceleration: -0.0624074074074077
	},
	{
		id: 531,
		time: 530,
		velocity: 14.7855555555556,
		power: 2431.73996916824,
		road: 6238.71296296296,
		acceleration: -0.0787037037037024
	},
	{
		id: 532,
		time: 531,
		velocity: 14.6236111111111,
		power: 2536.2934596285,
		road: 6253.43060185185,
		acceleration: -0.0691666666666677
	},
	{
		id: 533,
		time: 532,
		velocity: 14.64,
		power: 4351.3451906774,
		road: 6268.1437037037,
		acceleration: 0.0600925925925946
	},
	{
		id: 534,
		time: 533,
		velocity: 14.9658333333333,
		power: 5266.72694962508,
		road: 6282.94754629629,
		acceleration: 0.121388888888889
	},
	{
		id: 535,
		time: 534,
		velocity: 14.9877777777778,
		power: 7013.52048643906,
		road: 6297.93004629629,
		acceleration: 0.235925925925926
	},
	{
		id: 536,
		time: 535,
		velocity: 15.3477777777778,
		power: 6345.63845827157,
		road: 6313.12032407407,
		acceleration: 0.179629629629629
	},
	{
		id: 537,
		time: 536,
		velocity: 15.5047222222222,
		power: 7120.33771920553,
		road: 6328.51194444444,
		acceleration: 0.223055555555556
	},
	{
		id: 538,
		time: 537,
		velocity: 15.6569444444444,
		power: 4432.44905407146,
		road: 6344.03287037037,
		acceleration: 0.035555555555554
	},
	{
		id: 539,
		time: 538,
		velocity: 15.4544444444444,
		power: 2618.09466563437,
		road: 6359.52856481481,
		acceleration: -0.0860185185185198
	},
	{
		id: 540,
		time: 539,
		velocity: 15.2466666666667,
		power: 867.081326973365,
		road: 6374.88064814815,
		acceleration: -0.201203703703701
	},
	{
		id: 541,
		time: 540,
		velocity: 15.0533333333333,
		power: 1924.45782675858,
		road: 6390.06967592592,
		acceleration: -0.124907407407409
	},
	{
		id: 542,
		time: 541,
		velocity: 15.0797222222222,
		power: 3236.03697099968,
		road: 6405.18023148148,
		acceleration: -0.0320370370370355
	},
	{
		id: 543,
		time: 542,
		velocity: 15.1505555555556,
		power: 5131.16458011701,
		road: 6420.3237037037,
		acceleration: 0.0978703703703712
	},
	{
		id: 544,
		time: 543,
		velocity: 15.3469444444444,
		power: 4793.02853996447,
		road: 6435.55171296296,
		acceleration: 0.0712037037037021
	},
	{
		id: 545,
		time: 544,
		velocity: 15.2933333333333,
		power: 5400.14387285761,
		road: 6450.86990740741,
		acceleration: 0.109166666666667
	},
	{
		id: 546,
		time: 545,
		velocity: 15.4780555555556,
		power: 3035.23380476047,
		road: 6466.21606481481,
		acceleration: -0.0532407407407405
	},
	{
		id: 547,
		time: 546,
		velocity: 15.1872222222222,
		power: 2547.22880071643,
		road: 6481.49333333333,
		acceleration: -0.0845370370370375
	},
	{
		id: 548,
		time: 547,
		velocity: 15.0397222222222,
		power: 2150.91801377529,
		road: 6496.67379629629,
		acceleration: -0.109074074074073
	},
	{
		id: 549,
		time: 548,
		velocity: 15.1508333333333,
		power: 3560.60483226562,
		road: 6511.79476851852,
		acceleration: -0.00990740740740925
	},
	{
		id: 550,
		time: 549,
		velocity: 15.1575,
		power: 4816.83216804365,
		road: 6526.94865740741,
		acceleration: 0.0757407407407413
	},
	{
		id: 551,
		time: 550,
		velocity: 15.2669444444444,
		power: 6288.04078480429,
		road: 6542.22625,
		acceleration: 0.171666666666665
	},
	{
		id: 552,
		time: 551,
		velocity: 15.6658333333333,
		power: 7175.04106164505,
		road: 6557.70101851852,
		acceleration: 0.222685185185185
	},
	{
		id: 553,
		time: 552,
		velocity: 15.8255555555556,
		power: 6583.14471248222,
		road: 6573.37388888889,
		acceleration: 0.17351851851852
	},
	{
		id: 554,
		time: 553,
		velocity: 15.7875,
		power: 3178.90270803659,
		road: 6589.10574074074,
		acceleration: -0.0555555555555554
	},
	{
		id: 555,
		time: 554,
		velocity: 15.4991666666667,
		power: 149.716994929034,
		road: 6604.68273148148,
		acceleration: -0.254166666666666
	},
	{
		id: 556,
		time: 555,
		velocity: 15.0630555555556,
		power: -2177.77986097364,
		road: 6619.92865740741,
		acceleration: -0.407962962962962
	},
	{
		id: 557,
		time: 556,
		velocity: 14.5636111111111,
		power: -2230.18992086388,
		road: 6634.76615740741,
		acceleration: -0.408888888888892
	},
	{
		id: 558,
		time: 557,
		velocity: 14.2725,
		power: -615.813910559262,
		road: 6649.25407407407,
		acceleration: -0.290277777777774
	},
	{
		id: 559,
		time: 558,
		velocity: 14.1922222222222,
		power: 1269.26254141885,
		road: 6663.52231481481,
		acceleration: -0.149074074074074
	},
	{
		id: 560,
		time: 559,
		velocity: 14.1163888888889,
		power: 3009.60613967928,
		road: 6677.70666666667,
		acceleration: -0.0187037037037054
	},
	{
		id: 561,
		time: 560,
		velocity: 14.2163888888889,
		power: 4164.35416666261,
		road: 6691.91449074074,
		acceleration: 0.0656481481481492
	},
	{
		id: 562,
		time: 561,
		velocity: 14.3891666666667,
		power: 4868.07753364481,
		road: 6706.21203703703,
		acceleration: 0.113796296296297
	},
	{
		id: 563,
		time: 562,
		velocity: 14.4577777777778,
		power: 4915.57190744125,
		road: 6720.62277777778,
		acceleration: 0.112592592592593
	},
	{
		id: 564,
		time: 563,
		velocity: 14.5541666666667,
		power: 4692.82497065319,
		road: 6735.13597222222,
		acceleration: 0.0923148148148147
	},
	{
		id: 565,
		time: 564,
		velocity: 14.6661111111111,
		power: 4446.46207198092,
		road: 6749.73101851852,
		acceleration: 0.0713888888888885
	},
	{
		id: 566,
		time: 565,
		velocity: 14.6719444444444,
		power: 4332.86746832645,
		road: 6764.39212962963,
		acceleration: 0.0607407407407408
	},
	{
		id: 567,
		time: 566,
		velocity: 14.7363888888889,
		power: 4282.85689573934,
		road: 6779.11111111111,
		acceleration: 0.0549999999999997
	},
	{
		id: 568,
		time: 567,
		velocity: 14.8311111111111,
		power: 3444.24222220925,
		road: 6793.85486111111,
		acceleration: -0.00546296296296411
	},
	{
		id: 569,
		time: 568,
		velocity: 14.6555555555556,
		power: 3903.45187030789,
		road: 6808.60925925926,
		acceleration: 0.0267592592592614
	},
	{
		id: 570,
		time: 569,
		velocity: 14.8166666666667,
		power: 3500.73586799895,
		road: 6823.37592592593,
		acceleration: -0.00222222222222257
	},
	{
		id: 571,
		time: 570,
		velocity: 14.8244444444444,
		power: 4271.343420978,
		road: 6838.16722222222,
		acceleration: 0.051481481481483
	},
	{
		id: 572,
		time: 571,
		velocity: 14.81,
		power: 3678.78559737829,
		road: 6852.98851851852,
		acceleration: 0.00851851851851571
	},
	{
		id: 573,
		time: 572,
		velocity: 14.8422222222222,
		power: 1229.84612966353,
		road: 6867.73273148148,
		acceleration: -0.162685185185184
	},
	{
		id: 574,
		time: 573,
		velocity: 14.3363888888889,
		power: -832.522276257064,
		road: 6882.2425,
		acceleration: -0.306203703703707
	},
	{
		id: 575,
		time: 574,
		velocity: 13.8913888888889,
		power: -3065.90226785253,
		road: 6896.36555555555,
		acceleration: -0.467222222222219
	},
	{
		id: 576,
		time: 575,
		velocity: 13.4405555555556,
		power: -3618.92909022645,
		road: 6909.99986111111,
		acceleration: -0.510277777777777
	},
	{
		id: 577,
		time: 576,
		velocity: 12.8055555555556,
		power: -3334.89856881991,
		road: 6923.13365740741,
		acceleration: -0.490740740740744
	},
	{
		id: 578,
		time: 577,
		velocity: 12.4191666666667,
		power: -4084.49971138563,
		road: 6935.74379629629,
		acceleration: -0.556574074074073
	},
	{
		id: 579,
		time: 578,
		velocity: 11.7708333333333,
		power: -6108.65535376996,
		road: 6947.70384259259,
		acceleration: -0.743611111111109
	},
	{
		id: 580,
		time: 579,
		velocity: 10.5747222222222,
		power: -8962.66055979561,
		road: 6958.76898148148,
		acceleration: -1.0462037037037
	},
	{
		id: 581,
		time: 580,
		velocity: 9.28055555555556,
		power: -7110.71122759862,
		road: 6968.84842592592,
		acceleration: -0.925185185185189
	},
	{
		id: 582,
		time: 581,
		velocity: 8.99527777777778,
		power: -4617.01280690417,
		road: 6978.11578703704,
		acceleration: -0.69898148148148
	},
	{
		id: 583,
		time: 582,
		velocity: 8.47777777777778,
		power: -3489.03703935395,
		road: 6986.73643518518,
		acceleration: -0.594444444444443
	},
	{
		id: 584,
		time: 583,
		velocity: 7.49722222222222,
		power: -4236.84235449718,
		road: 6994.6987037037,
		acceleration: -0.722314814814815
	},
	{
		id: 585,
		time: 584,
		velocity: 6.82833333333333,
		power: -3769.77520630019,
		road: 7001.94796296296,
		acceleration: -0.703703703703705
	},
	{
		id: 586,
		time: 585,
		velocity: 6.36666666666667,
		power: -1596.79766717325,
		road: 7008.64365740741,
		acceleration: -0.403425925925926
	},
	{
		id: 587,
		time: 586,
		velocity: 6.28694444444444,
		power: -175.783261966903,
		road: 7015.04796296296,
		acceleration: -0.179351851851852
	},
	{
		id: 588,
		time: 587,
		velocity: 6.29027777777778,
		power: 312.519831748557,
		road: 7021.31407407407,
		acceleration: -0.0970370370370368
	},
	{
		id: 589,
		time: 588,
		velocity: 6.07555555555555,
		power: -791.81170540349,
		road: 7027.38902777778,
		acceleration: -0.285277777777779
	},
	{
		id: 590,
		time: 589,
		velocity: 5.43111111111111,
		power: -2254.62175676278,
		road: 7033.0387037037,
		acceleration: -0.565277777777777
	},
	{
		id: 591,
		time: 590,
		velocity: 4.59444444444444,
		power: -2372.59212459433,
		road: 7038.0875462963,
		acceleration: -0.636388888888888
	},
	{
		id: 592,
		time: 591,
		velocity: 4.16638888888889,
		power: -2519.31208724061,
		road: 7042.44472222222,
		acceleration: -0.746944444444445
	},
	{
		id: 593,
		time: 592,
		velocity: 3.19027777777778,
		power: -1789.18005401327,
		road: 7046.10337962963,
		acceleration: -0.650092592592593
	},
	{
		id: 594,
		time: 593,
		velocity: 2.64416666666667,
		power: -1254.5661778022,
		road: 7049.15398148148,
		acceleration: -0.566018518518518
	},
	{
		id: 595,
		time: 594,
		velocity: 2.46833333333333,
		power: -515.429458410171,
		road: 7051.75138888889,
		acceleration: -0.34037037037037
	},
	{
		id: 596,
		time: 595,
		velocity: 2.16916666666667,
		power: -76.3091380797282,
		road: 7054.0962037037,
		acceleration: -0.164814814814815
	},
	{
		id: 597,
		time: 596,
		velocity: 2.14972222222222,
		power: -6.08980278141619,
		road: 7056.29208333333,
		acceleration: -0.133055555555556
	},
	{
		id: 598,
		time: 597,
		velocity: 2.06916666666667,
		power: 256.913029212754,
		road: 7058.42009259259,
		acceleration: -0.00268518518518501
	},
	{
		id: 599,
		time: 598,
		velocity: 2.16111111111111,
		power: 88.6906328125412,
		road: 7060.50425925926,
		acceleration: -0.085
	},
	{
		id: 600,
		time: 599,
		velocity: 1.89472222222222,
		power: 210.557646429042,
		road: 7062.53569444444,
		acceleration: -0.0204629629629629
	},
	{
		id: 601,
		time: 600,
		velocity: 2.00777777777778,
		power: 122.47315717131,
		road: 7064.52453703704,
		acceleration: -0.0647222222222221
	},
	{
		id: 602,
		time: 601,
		velocity: 1.96694444444444,
		power: 139.963278764416,
		road: 7066.45449074074,
		acceleration: -0.0530555555555556
	},
	{
		id: 603,
		time: 602,
		velocity: 1.73555555555556,
		power: -255.457313178846,
		road: 7068.21689814815,
		acceleration: -0.282037037037037
	},
	{
		id: 604,
		time: 603,
		velocity: 1.16166666666667,
		power: 96.783304858267,
		road: 7069.80597222222,
		acceleration: -0.0646296296296294
	},
	{
		id: 605,
		time: 604,
		velocity: 1.77305555555556,
		power: 200.553513107022,
		road: 7071.36611111111,
		acceleration: 0.00675925925925913
	},
	{
		id: 606,
		time: 605,
		velocity: 1.75583333333333,
		power: 540.74790424633,
		road: 7073.03587962963,
		acceleration: 0.2125
	},
	{
		id: 607,
		time: 606,
		velocity: 1.79916666666667,
		power: 398.802038805376,
		road: 7074.86236111111,
		acceleration: 0.100925925925926
	},
	{
		id: 608,
		time: 607,
		velocity: 2.07583333333333,
		power: 298.239332521367,
		road: 7076.7575462963,
		acceleration: 0.0364814814814811
	},
	{
		id: 609,
		time: 608,
		velocity: 1.86527777777778,
		power: 282.052822313519,
		road: 7078.68342592592,
		acceleration: 0.0249074074074076
	},
	{
		id: 610,
		time: 609,
		velocity: 1.87388888888889,
		power: 110.062339973572,
		road: 7080.5875,
		acceleration: -0.0685185185185186
	},
	{
		id: 611,
		time: 610,
		velocity: 1.87027777777778,
		power: 239.673754905374,
		road: 7082.46009259259,
		acceleration: 0.00555555555555576
	},
	{
		id: 612,
		time: 611,
		velocity: 1.88194444444444,
		power: 158.081286664842,
		road: 7084.31569444444,
		acceleration: -0.0395370370370369
	},
	{
		id: 613,
		time: 612,
		velocity: 1.75527777777778,
		power: 140.208709544432,
		road: 7086.12768518518,
		acceleration: -0.0476851851851852
	},
	{
		id: 614,
		time: 613,
		velocity: 1.72722222222222,
		power: 186.737228928083,
		road: 7087.90657407407,
		acceleration: -0.0185185185185186
	},
	{
		id: 615,
		time: 614,
		velocity: 1.82638888888889,
		power: 275.049893582817,
		road: 7089.69277777778,
		acceleration: 0.0331481481481481
	},
	{
		id: 616,
		time: 615,
		velocity: 1.85472222222222,
		power: 308.959884401613,
		road: 7091.5200462963,
		acceleration: 0.0489814814814813
	},
	{
		id: 617,
		time: 616,
		velocity: 1.87416666666667,
		power: 261.027727279879,
		road: 7093.38106481481,
		acceleration: 0.0185185185185188
	},
	{
		id: 618,
		time: 617,
		velocity: 1.88194444444444,
		power: 285.743655071112,
		road: 7095.26652777778,
		acceleration: 0.0303703703703702
	},
	{
		id: 619,
		time: 618,
		velocity: 1.94583333333333,
		power: 567.886217950846,
		road: 7097.25305555556,
		acceleration: 0.171759259259259
	},
	{
		id: 620,
		time: 619,
		velocity: 2.38944444444444,
		power: 758.660707102291,
		road: 7099.44300925926,
		acceleration: 0.235092592592592
	},
	{
		id: 621,
		time: 620,
		velocity: 2.58722222222222,
		power: 919.611640147542,
		road: 7101.8837037037,
		acceleration: 0.266388888888889
	},
	{
		id: 622,
		time: 621,
		velocity: 2.745,
		power: 874.659335427342,
		road: 7104.56384259259,
		acceleration: 0.2125
	},
	{
		id: 623,
		time: 622,
		velocity: 3.02694444444444,
		power: 579.140888538337,
		road: 7107.39217592593,
		acceleration: 0.0838888888888891
	},
	{
		id: 624,
		time: 623,
		velocity: 2.83888888888889,
		power: 543.298796454068,
		road: 7110.295,
		acceleration: 0.0650925925925927
	},
	{
		id: 625,
		time: 624,
		velocity: 2.94027777777778,
		power: 556.104197430448,
		road: 7113.26291666667,
		acceleration: 0.0650925925925927
	},
	{
		id: 626,
		time: 625,
		velocity: 3.22222222222222,
		power: 818.104916628087,
		road: 7116.33722222222,
		acceleration: 0.147685185185185
	},
	{
		id: 627,
		time: 626,
		velocity: 3.28194444444444,
		power: 808.566523039236,
		road: 7119.5512962963,
		acceleration: 0.131851851851851
	},
	{
		id: 628,
		time: 627,
		velocity: 3.33583333333333,
		power: 709.351231597455,
		road: 7122.87685185185,
		acceleration: 0.0911111111111111
	},
	{
		id: 629,
		time: 628,
		velocity: 3.49555555555556,
		power: 1187.57552673925,
		road: 7126.36041666667,
		acceleration: 0.224907407407407
	},
	{
		id: 630,
		time: 629,
		velocity: 3.95666666666667,
		power: 579.651867433708,
		road: 7129.97356481481,
		acceleration: 0.0342592592592594
	},
	{
		id: 631,
		time: 630,
		velocity: 3.43861111111111,
		power: -84.4418162684265,
		road: 7133.52407407407,
		acceleration: -0.159537037037037
	},
	{
		id: 632,
		time: 631,
		velocity: 3.01694444444444,
		power: -743.187907729583,
		road: 7136.80888888889,
		acceleration: -0.371851851851852
	},
	{
		id: 633,
		time: 632,
		velocity: 2.84111111111111,
		power: -400.147929462413,
		road: 7139.77041666667,
		acceleration: -0.274722222222222
	},
	{
		id: 634,
		time: 633,
		velocity: 2.61444444444444,
		power: 158.291643002131,
		road: 7142.55861111111,
		acceleration: -0.0719444444444441
	},
	{
		id: 635,
		time: 634,
		velocity: 2.80111111111111,
		power: 429.118259789149,
		road: 7145.32666666667,
		acceleration: 0.0316666666666663
	},
	{
		id: 636,
		time: 635,
		velocity: 2.93611111111111,
		power: 726.831567564327,
		road: 7148.17884259259,
		acceleration: 0.136574074074074
	},
	{
		id: 637,
		time: 636,
		velocity: 3.02416666666667,
		power: 716.343681507898,
		road: 7151.15972222222,
		acceleration: 0.120833333333334
	},
	{
		id: 638,
		time: 637,
		velocity: 3.16361111111111,
		power: 603.320211633962,
		road: 7154.23791666667,
		acceleration: 0.0737962962962961
	},
	{
		id: 639,
		time: 638,
		velocity: 3.1575,
		power: 631.484855745826,
		road: 7157.39199074074,
		acceleration: 0.0779629629629626
	},
	{
		id: 640,
		time: 639,
		velocity: 3.25805555555556,
		power: 387.107628534218,
		road: 7160.58240740741,
		acceleration: -0.00527777777777771
	},
	{
		id: 641,
		time: 640,
		velocity: 3.14777777777778,
		power: 68.1373739706274,
		road: 7163.71518518518,
		acceleration: -0.109999999999999
	},
	{
		id: 642,
		time: 641,
		velocity: 2.8275,
		power: -250.173512308867,
		road: 7166.68236111111,
		acceleration: -0.221203703703704
	},
	{
		id: 643,
		time: 642,
		velocity: 2.59444444444444,
		power: -239.510597597549,
		road: 7169.42712962963,
		acceleration: -0.223611111111111
	},
	{
		id: 644,
		time: 643,
		velocity: 2.47694444444444,
		power: -9.91610342683518,
		road: 7171.9925,
		acceleration: -0.135185185185185
	},
	{
		id: 645,
		time: 644,
		velocity: 2.42194444444444,
		power: 109.520941364871,
		road: 7174.44837962963,
		acceleration: -0.0837962962962964
	},
	{
		id: 646,
		time: 645,
		velocity: 2.34305555555556,
		power: 213.355164572503,
		road: 7176.84398148148,
		acceleration: -0.0367592592592594
	},
	{
		id: 647,
		time: 646,
		velocity: 2.36666666666667,
		power: 297.082544089772,
		road: 7179.22175925926,
		acceleration: 0.00111111111111128
	},
	{
		id: 648,
		time: 647,
		velocity: 2.42527777777778,
		power: 319.203529438816,
		road: 7181.60537037037,
		acceleration: 0.0105555555555554
	},
	{
		id: 649,
		time: 648,
		velocity: 2.37472222222222,
		power: 343.491087203793,
		road: 7184.00439814815,
		acceleration: 0.0202777777777778
	},
	{
		id: 650,
		time: 649,
		velocity: 2.4275,
		power: 446.768465803517,
		road: 7186.44467592592,
		acceleration: 0.0622222222222226
	},
	{
		id: 651,
		time: 650,
		velocity: 2.61194444444444,
		power: 552.083871344133,
		road: 7188.96597222222,
		acceleration: 0.0998148148148146
	},
	{
		id: 652,
		time: 651,
		velocity: 2.67416666666667,
		power: 632.463642851196,
		road: 7191.59814814815,
		acceleration: 0.121944444444444
	},
	{
		id: 653,
		time: 652,
		velocity: 2.79333333333333,
		power: 480.627202276931,
		road: 7194.31861111111,
		acceleration: 0.0546296296296291
	},
	{
		id: 654,
		time: 653,
		velocity: 2.77583333333333,
		power: 546.969210573107,
		road: 7197.10398148148,
		acceleration: 0.0751851851851852
	},
	{
		id: 655,
		time: 654,
		velocity: 2.89972222222222,
		power: 518.173149075459,
		road: 7199.95666666667,
		acceleration: 0.0594444444444449
	},
	{
		id: 656,
		time: 655,
		velocity: 2.97166666666667,
		power: 543.29520022751,
		road: 7202.8712037037,
		acceleration: 0.0642592592592592
	},
	{
		id: 657,
		time: 656,
		velocity: 2.96861111111111,
		power: -80.6140978040986,
		road: 7205.73703703704,
		acceleration: -0.161666666666667
	},
	{
		id: 658,
		time: 657,
		velocity: 2.41472222222222,
		power: -140.698874497554,
		road: 7208.42875,
		acceleration: -0.186574074074075
	},
	{
		id: 659,
		time: 658,
		velocity: 2.41194444444444,
		power: -125.676651797283,
		road: 7210.93527777778,
		acceleration: -0.183796296296296
	},
	{
		id: 660,
		time: 659,
		velocity: 2.41722222222222,
		power: 503.965850795769,
		road: 7213.39259259259,
		acceleration: 0.0853703703703705
	},
	{
		id: 661,
		time: 660,
		velocity: 2.67083333333333,
		power: 727.817249726427,
		road: 7215.97550925926,
		acceleration: 0.165833333333333
	},
	{
		id: 662,
		time: 661,
		velocity: 2.90944444444444,
		power: 819.369253878298,
		road: 7218.73212962963,
		acceleration: 0.181574074074074
	},
	{
		id: 663,
		time: 662,
		velocity: 2.96194444444444,
		power: 832.643860046977,
		road: 7221.66310185185,
		acceleration: 0.16712962962963
	},
	{
		id: 664,
		time: 663,
		velocity: 3.17222222222222,
		power: 819.441478715968,
		road: 7224.75106481481,
		acceleration: 0.146851851851852
	},
	{
		id: 665,
		time: 664,
		velocity: 3.35,
		power: 805.839673978923,
		road: 7227.9774074074,
		acceleration: 0.129907407407408
	},
	{
		id: 666,
		time: 665,
		velocity: 3.35166666666667,
		power: 977.987993897842,
		road: 7231.35435185185,
		acceleration: 0.171296296296296
	},
	{
		id: 667,
		time: 666,
		velocity: 3.68611111111111,
		power: 1014.62600633598,
		road: 7234.90041666666,
		acceleration: 0.166944444444444
	},
	{
		id: 668,
		time: 667,
		velocity: 3.85083333333333,
		power: 1158.46871046393,
		road: 7238.62611111111,
		acceleration: 0.192314814814814
	},
	{
		id: 669,
		time: 668,
		velocity: 3.92861111111111,
		power: 987.273103487141,
		road: 7242.51375,
		acceleration: 0.131574074074075
	},
	{
		id: 670,
		time: 669,
		velocity: 4.08083333333333,
		power: 1278.97020997596,
		road: 7246.56509259259,
		acceleration: 0.195833333333333
	},
	{
		id: 671,
		time: 670,
		velocity: 4.43833333333333,
		power: 1941.87958672905,
		road: 7250.88222222222,
		acceleration: 0.335740740740741
	},
	{
		id: 672,
		time: 671,
		velocity: 4.93583333333333,
		power: 2579.31197465568,
		road: 7255.58592592592,
		acceleration: 0.437407407407406
	},
	{
		id: 673,
		time: 672,
		velocity: 5.39305555555555,
		power: 2225.48873103288,
		road: 7260.66782407407,
		acceleration: 0.318981481481482
	},
	{
		id: 674,
		time: 673,
		velocity: 5.39527777777778,
		power: 1216.80698476893,
		road: 7265.9586574074,
		acceleration: 0.0988888888888892
	},
	{
		id: 675,
		time: 674,
		velocity: 5.2325,
		power: 961.937526753792,
		road: 7271.32152777778,
		acceleration: 0.0451851851851846
	},
	{
		id: 676,
		time: 675,
		velocity: 5.52861111111111,
		power: 1265.82513885616,
		road: 7276.7575,
		acceleration: 0.101018518518519
	},
	{
		id: 677,
		time: 676,
		velocity: 5.69833333333333,
		power: 1713.77430945,
		road: 7282.33324074074,
		acceleration: 0.178518518518519
	},
	{
		id: 678,
		time: 677,
		velocity: 5.76805555555556,
		power: 1410.86676041866,
		road: 7288.05504629629,
		acceleration: 0.113611111111111
	},
	{
		id: 679,
		time: 678,
		velocity: 5.86944444444444,
		power: 1545.9604945374,
		road: 7293.89949074074,
		acceleration: 0.131666666666667
	},
	{
		id: 680,
		time: 679,
		velocity: 6.09333333333333,
		power: 1567.53434231118,
		road: 7299.87402777778,
		acceleration: 0.128518518518518
	},
	{
		id: 681,
		time: 680,
		velocity: 6.15361111111111,
		power: 1477.11689420046,
		road: 7305.9662037037,
		acceleration: 0.10675925925926
	},
	{
		id: 682,
		time: 681,
		velocity: 6.18972222222222,
		power: 979.647298074339,
		road: 7312.12111111111,
		acceleration: 0.0187037037037037
	},
	{
		id: 683,
		time: 682,
		velocity: 6.14944444444444,
		power: 717.377519641997,
		road: 7318.27236111111,
		acceleration: -0.0260185185185184
	},
	{
		id: 684,
		time: 683,
		velocity: 6.07555555555555,
		power: 179.55769930796,
		road: 7324.35203703704,
		acceleration: -0.11712962962963
	},
	{
		id: 685,
		time: 684,
		velocity: 5.83833333333333,
		power: 157.326810793416,
		road: 7330.31333333333,
		acceleration: -0.119629629629629
	},
	{
		id: 686,
		time: 685,
		velocity: 5.79055555555556,
		power: -43.5625086695616,
		road: 7336.13763888889,
		acceleration: -0.154351851851852
	},
	{
		id: 687,
		time: 686,
		velocity: 5.6125,
		power: -62.2969058930527,
		road: 7341.80625,
		acceleration: -0.157037037037036
	},
	{
		id: 688,
		time: 687,
		velocity: 5.36722222222222,
		power: -1658.47422594557,
		road: 7347.16162037037,
		acceleration: -0.469444444444445
	},
	{
		id: 689,
		time: 688,
		velocity: 4.38222222222222,
		power: -1817.62060164021,
		road: 7352.01481481481,
		acceleration: -0.534907407407408
	},
	{
		id: 690,
		time: 689,
		velocity: 4.00777777777778,
		power: -1653.15124568165,
		road: 7356.32990740741,
		acceleration: -0.541296296296296
	},
	{
		id: 691,
		time: 690,
		velocity: 3.74333333333333,
		power: -399.592917848465,
		road: 7360.25268518519,
		acceleration: -0.243333333333334
	},
	{
		id: 692,
		time: 691,
		velocity: 3.65222222222222,
		power: 174.618915973395,
		road: 7364.01060185185,
		acceleration: -0.0863888888888882
	},
	{
		id: 693,
		time: 692,
		velocity: 3.74861111111111,
		power: 668.889571362328,
		road: 7367.75185185185,
		acceleration: 0.053055555555555
	},
	{
		id: 694,
		time: 693,
		velocity: 3.9025,
		power: 881.709786553997,
		road: 7371.57333333333,
		acceleration: 0.107407407407407
	},
	{
		id: 695,
		time: 694,
		velocity: 3.97444444444444,
		power: 1247.04829714419,
		road: 7375.54569444445,
		acceleration: 0.194351851851852
	},
	{
		id: 696,
		time: 695,
		velocity: 4.33166666666667,
		power: 1436.6476806657,
		road: 7379.7275,
		acceleration: 0.224537037037037
	},
	{
		id: 697,
		time: 696,
		velocity: 4.57611111111111,
		power: 1763.09309242099,
		road: 7384.16166666667,
		acceleration: 0.280185185185186
	},
	{
		id: 698,
		time: 697,
		velocity: 4.815,
		power: 1426.35039908726,
		road: 7388.82703703704,
		acceleration: 0.182222222222222
	},
	{
		id: 699,
		time: 698,
		velocity: 4.87833333333333,
		power: 1402.03015368533,
		road: 7393.66574074074,
		acceleration: 0.164444444444444
	},
	{
		id: 700,
		time: 699,
		velocity: 5.06944444444444,
		power: 1235.39593665926,
		road: 7398.64652777778,
		acceleration: 0.119722222222222
	},
	{
		id: 701,
		time: 700,
		velocity: 5.17416666666667,
		power: 1198.40842069004,
		road: 7403.74,
		acceleration: 0.105648148148148
	},
	{
		id: 702,
		time: 701,
		velocity: 5.19527777777778,
		power: 1179.43450643088,
		road: 7408.93449074074,
		acceleration: 0.0963888888888889
	},
	{
		id: 703,
		time: 702,
		velocity: 5.35861111111111,
		power: 1106.06126764064,
		road: 7414.21583333333,
		acceleration: 0.0773148148148142
	},
	{
		id: 704,
		time: 703,
		velocity: 5.40611111111111,
		power: 725.918033408041,
		road: 7419.53597222222,
		acceleration: 0.000277777777777821
	},
	{
		id: 705,
		time: 704,
		velocity: 5.19611111111111,
		power: 214.955540889343,
		road: 7424.8062037037,
		acceleration: -0.100092592592592
	},
	{
		id: 706,
		time: 705,
		velocity: 5.05833333333333,
		power: -178.852055840444,
		road: 7429.93694444444,
		acceleration: -0.178888888888889
	},
	{
		id: 707,
		time: 706,
		velocity: 4.86944444444444,
		power: 201.643832901078,
		road: 7434.9287962963,
		acceleration: -0.0988888888888884
	},
	{
		id: 708,
		time: 707,
		velocity: 4.89944444444444,
		power: 722.444956887633,
		road: 7439.8774537037,
		acceleration: 0.0125000000000002
	},
	{
		id: 709,
		time: 708,
		velocity: 5.09583333333333,
		power: 484.050163157236,
		road: 7444.81342592593,
		acceleration: -0.0378703703703707
	},
	{
		id: 710,
		time: 709,
		velocity: 4.75583333333333,
		power: 302.770705178533,
		road: 7449.69273148148,
		acceleration: -0.0754629629629635
	},
	{
		id: 711,
		time: 710,
		velocity: 4.67305555555556,
		power: -566.29557450966,
		road: 7454.40106481481,
		acceleration: -0.266481481481481
	},
	{
		id: 712,
		time: 711,
		velocity: 4.29638888888889,
		power: 249.930075294795,
		road: 7458.93569444444,
		acceleration: -0.080925925925925
	},
	{
		id: 713,
		time: 712,
		velocity: 4.51305555555556,
		power: 751.418166220736,
		road: 7463.44810185185,
		acceleration: 0.0364814814814807
	},
	{
		id: 714,
		time: 713,
		velocity: 4.7825,
		power: 2326.29939088638,
		road: 7468.16819444444,
		acceleration: 0.378888888888889
	},
	{
		id: 715,
		time: 714,
		velocity: 5.43305555555556,
		power: 2426.48663165341,
		road: 7473.25763888889,
		acceleration: 0.359814814814815
	},
	{
		id: 716,
		time: 715,
		velocity: 5.5925,
		power: 2675.63324188754,
		road: 7478.71296296296,
		acceleration: 0.371944444444445
	},
	{
		id: 717,
		time: 716,
		velocity: 5.89833333333333,
		power: 1549.0865943763,
		road: 7484.42407407407,
		acceleration: 0.139629629629629
	},
	{
		id: 718,
		time: 717,
		velocity: 5.85194444444444,
		power: 123.130539499933,
		road: 7490.14342592592,
		acceleration: -0.123148148148148
	},
	{
		id: 719,
		time: 718,
		velocity: 5.22305555555556,
		power: -856.721472990831,
		road: 7495.64708333333,
		acceleration: -0.30824074074074
	},
	{
		id: 720,
		time: 719,
		velocity: 4.97361111111111,
		power: -1305.89985703629,
		road: 7500.79189814815,
		acceleration: -0.409444444444445
	},
	{
		id: 721,
		time: 720,
		velocity: 4.62361111111111,
		power: -337.65646439205,
		road: 7505.6249537037,
		acceleration: -0.214074074074074
	},
	{
		id: 722,
		time: 721,
		velocity: 4.58083333333333,
		power: -106.677104999631,
		road: 7510.26912037037,
		acceleration: -0.163703703703704
	},
	{
		id: 723,
		time: 722,
		velocity: 4.4825,
		power: -302.411294863031,
		road: 7514.72643518518,
		acceleration: -0.21
	},
	{
		id: 724,
		time: 723,
		velocity: 3.99361111111111,
		power: 122.855427141762,
		road: 7519.02490740741,
		acceleration: -0.107685185185185
	},
	{
		id: 725,
		time: 724,
		velocity: 4.25777777777778,
		power: 314.198823316037,
		road: 7523.24009259259,
		acceleration: -0.0588888888888883
	},
	{
		id: 726,
		time: 725,
		velocity: 4.30583333333333,
		power: 887.461401219246,
		road: 7527.46763888889,
		acceleration: 0.0836111111111109
	},
	{
		id: 727,
		time: 726,
		velocity: 4.24444444444444,
		power: 117.396900809038,
		road: 7531.68296296296,
		acceleration: -0.108055555555556
	},
	{
		id: 728,
		time: 727,
		velocity: 3.93361111111111,
		power: -1800.13572783189,
		road: 7535.5299537037,
		acceleration: -0.628611111111111
	},
	{
		id: 729,
		time: 728,
		velocity: 2.42,
		power: -2278.47036335719,
		road: 7538.60597222222,
		acceleration: -0.913333333333333
	},
	{
		id: 730,
		time: 729,
		velocity: 1.50444444444444,
		power: -1842.10326838844,
		road: 7540.69560185185,
		acceleration: -1.05944444444444
	},
	{
		id: 731,
		time: 730,
		velocity: 0.755277777777778,
		power: -582.89818264626,
		road: 7541.94532407407,
		acceleration: -0.62037037037037
	},
	{
		id: 732,
		time: 731,
		velocity: 0.558888888888889,
		power: -148.292328871596,
		road: 7542.7199537037,
		acceleration: -0.329814814814815
	},
	{
		id: 733,
		time: 732,
		velocity: 0.515,
		power: 63.7970262119539,
		road: 7543.32175925926,
		acceleration: -0.0158333333333333
	},
	{
		id: 734,
		time: 733,
		velocity: 0.707777777777778,
		power: 559.604766081652,
		road: 7544.1912037037,
		acceleration: 0.551111111111111
	},
	{
		id: 735,
		time: 734,
		velocity: 2.21222222222222,
		power: 1137.42419349195,
		road: 7545.67583333333,
		acceleration: 0.679259259259259
	},
	{
		id: 736,
		time: 735,
		velocity: 2.55277777777778,
		power: 1164.75046051103,
		road: 7547.73356481481,
		acceleration: 0.466944444444444
	},
	{
		id: 737,
		time: 736,
		velocity: 2.10861111111111,
		power: 352.390380446189,
		road: 7550.04009259259,
		acceleration: 0.0306481481481486
	},
	{
		id: 738,
		time: 737,
		velocity: 2.30416666666667,
		power: 367.683961838679,
		road: 7552.37953703704,
		acceleration: 0.0351851851851852
	},
	{
		id: 739,
		time: 738,
		velocity: 2.65833333333333,
		power: 1344.05257378756,
		road: 7554.94689814815,
		acceleration: 0.420648148148148
	},
	{
		id: 740,
		time: 739,
		velocity: 3.37055555555556,
		power: 1889.99119084758,
		road: 7557.98592592593,
		acceleration: 0.522685185185185
	},
	{
		id: 741,
		time: 740,
		velocity: 3.87222222222222,
		power: 2032.9846428832,
		road: 7561.52189814815,
		acceleration: 0.471203703703704
	},
	{
		id: 742,
		time: 741,
		velocity: 4.07194444444444,
		power: 1329.79137016169,
		road: 7565.40583333333,
		acceleration: 0.224722222222222
	},
	{
		id: 743,
		time: 742,
		velocity: 4.04472222222222,
		power: 251.866137791064,
		road: 7569.3675,
		acceleration: -0.0692592592592596
	},
	{
		id: 744,
		time: 743,
		velocity: 3.66444444444444,
		power: -1004.45676768947,
		road: 7573.08462962963,
		acceleration: -0.419814814814815
	},
	{
		id: 745,
		time: 744,
		velocity: 2.8125,
		power: -1207.74619859064,
		road: 7576.32907407407,
		acceleration: -0.525555555555555
	},
	{
		id: 746,
		time: 745,
		velocity: 2.46805555555556,
		power: -729.129579868951,
		road: 7579.10652777778,
		acceleration: -0.408425925925926
	},
	{
		id: 747,
		time: 746,
		velocity: 2.43916666666667,
		power: -124.576854475535,
		road: 7581.58787037037,
		acceleration: -0.183796296296296
	},
	{
		id: 748,
		time: 747,
		velocity: 2.26111111111111,
		power: 229.126864022937,
		road: 7583.96287037037,
		acceleration: -0.028888888888889
	},
	{
		id: 749,
		time: 748,
		velocity: 2.38138888888889,
		power: -166.663825681614,
		road: 7586.21935185185,
		acceleration: -0.208148148148148
	},
	{
		id: 750,
		time: 749,
		velocity: 1.81472222222222,
		power: 437.379037671621,
		road: 7588.41185185185,
		acceleration: 0.0801851851851851
	},
	{
		id: 751,
		time: 750,
		velocity: 2.50166666666667,
		power: 315.946511447346,
		road: 7590.65361111111,
		acceleration: 0.0183333333333335
	},
	{
		id: 752,
		time: 751,
		velocity: 2.43638888888889,
		power: 649.047416190124,
		road: 7592.98597222222,
		acceleration: 0.16287037037037
	},
	{
		id: 753,
		time: 752,
		velocity: 2.30333333333333,
		power: -242.850749974679,
		road: 7595.27875,
		acceleration: -0.242037037037037
	},
	{
		id: 754,
		time: 753,
		velocity: 1.77555555555556,
		power: 40.2255646268135,
		road: 7597.39555555555,
		acceleration: -0.109907407407408
	},
	{
		id: 755,
		time: 754,
		velocity: 2.10666666666667,
		power: 501.284486306458,
		road: 7599.51699074074,
		acceleration: 0.119166666666667
	},
	{
		id: 756,
		time: 755,
		velocity: 2.66083333333333,
		power: 849.539097131242,
		road: 7601.82666666667,
		acceleration: 0.257314814814815
	},
	{
		id: 757,
		time: 756,
		velocity: 2.5475,
		power: 821.521037390665,
		road: 7604.36972222222,
		acceleration: 0.209444444444444
	},
	{
		id: 758,
		time: 757,
		velocity: 2.735,
		power: 204.828333540307,
		road: 7606.99300925926,
		acceleration: -0.0489814814814817
	},
	{
		id: 759,
		time: 758,
		velocity: 2.51388888888889,
		power: 13.3883672690135,
		road: 7609.52907407407,
		acceleration: -0.125462962962963
	},
	{
		id: 760,
		time: 759,
		velocity: 2.17111111111111,
		power: -364.988220403516,
		road: 7611.85444444444,
		acceleration: -0.295925925925926
	},
	{
		id: 761,
		time: 760,
		velocity: 1.84722222222222,
		power: -330.456292445436,
		road: 7613.88101851852,
		acceleration: -0.301666666666667
	},
	{
		id: 762,
		time: 761,
		velocity: 1.60888888888889,
		power: -205.217587960282,
		road: 7615.63032407407,
		acceleration: -0.252870370370371
	},
	{
		id: 763,
		time: 762,
		velocity: 1.4125,
		power: 525.44622487715,
		road: 7617.34976851852,
		acceleration: 0.193148148148148
	},
	{
		id: 764,
		time: 763,
		velocity: 2.42666666666667,
		power: 1677.91603962716,
		road: 7619.51013888889,
		acceleration: 0.688703703703704
	},
	{
		id: 765,
		time: 764,
		velocity: 3.675,
		power: 2482.16657125811,
		road: 7622.40115740741,
		acceleration: 0.772592592592593
	},
	{
		id: 766,
		time: 765,
		velocity: 3.73027777777778,
		power: 2703.7261487205,
		road: 7626.00611111111,
		acceleration: 0.655277777777778
	},
	{
		id: 767,
		time: 766,
		velocity: 4.3925,
		power: 1604.04100013375,
		road: 7630.07777777778,
		acceleration: 0.278148148148148
	},
	{
		id: 768,
		time: 767,
		velocity: 4.50944444444445,
		power: 1674.78002132096,
		road: 7634.4224537037,
		acceleration: 0.26787037037037
	},
	{
		id: 769,
		time: 768,
		velocity: 4.53388888888889,
		power: 1084.74839940198,
		road: 7638.9575,
		acceleration: 0.11287037037037
	},
	{
		id: 770,
		time: 769,
		velocity: 4.73111111111111,
		power: 1938.73953083144,
		road: 7643.69439814815,
		acceleration: 0.290833333333334
	},
	{
		id: 771,
		time: 770,
		velocity: 5.38194444444444,
		power: 3707.91675524009,
		road: 7648.88157407407,
		acceleration: 0.609722222222223
	},
	{
		id: 772,
		time: 771,
		velocity: 6.36305555555556,
		power: 4858.54309836895,
		road: 7654.73671296296,
		acceleration: 0.726203703703703
	},
	{
		id: 773,
		time: 772,
		velocity: 6.90972222222222,
		power: 4823.77937721074,
		road: 7661.26759259259,
		acceleration: 0.625277777777778
	},
	{
		id: 774,
		time: 773,
		velocity: 7.25777777777778,
		power: 2955.48726530688,
		road: 7668.25597222222,
		acceleration: 0.289722222222221
	},
	{
		id: 775,
		time: 774,
		velocity: 7.23222222222222,
		power: 2099.53301448015,
		road: 7675.46398148148,
		acceleration: 0.149537037037037
	},
	{
		id: 776,
		time: 775,
		velocity: 7.35833333333333,
		power: 1493.5405190621,
		road: 7682.77537037037,
		acceleration: 0.0572222222222232
	},
	{
		id: 777,
		time: 776,
		velocity: 7.42944444444444,
		power: 1885.56943372535,
		road: 7690.17027777778,
		acceleration: 0.109814814814815
	},
	{
		id: 778,
		time: 777,
		velocity: 7.56166666666667,
		power: 1981.14614735239,
		road: 7697.67916666667,
		acceleration: 0.118148148148149
	},
	{
		id: 779,
		time: 778,
		velocity: 7.71277777777778,
		power: 1741.46179309923,
		road: 7705.28740740741,
		acceleration: 0.0805555555555548
	},
	{
		id: 780,
		time: 779,
		velocity: 7.67111111111111,
		power: 1591.18665518137,
		road: 7712.96453703704,
		acceleration: 0.0572222222222223
	},
	{
		id: 781,
		time: 780,
		velocity: 7.73333333333333,
		power: 1197.9903664856,
		road: 7720.67152777778,
		acceleration: 0.00250000000000128
	},
	{
		id: 782,
		time: 781,
		velocity: 7.72027777777778,
		power: 2414.91088939323,
		road: 7728.46185185185,
		acceleration: 0.164166666666666
	},
	{
		id: 783,
		time: 782,
		velocity: 8.16361111111111,
		power: 2554.73645188971,
		road: 7736.42134259259,
		acceleration: 0.174166666666667
	},
	{
		id: 784,
		time: 783,
		velocity: 8.25583333333333,
		power: 4732.91728882824,
		road: 7744.68578703704,
		acceleration: 0.435740740740739
	},
	{
		id: 785,
		time: 784,
		velocity: 9.0275,
		power: 3566.13261026943,
		road: 7753.30087962963,
		acceleration: 0.265555555555558
	},
	{
		id: 786,
		time: 785,
		velocity: 8.96027777777778,
		power: 3432.35328540664,
		road: 7762.1662037037,
		acceleration: 0.234907407407407
	},
	{
		id: 787,
		time: 786,
		velocity: 8.96055555555556,
		power: 839.257878682515,
		road: 7771.11199074074,
		acceleration: -0.0739814814814821
	},
	{
		id: 788,
		time: 787,
		velocity: 8.80555555555556,
		power: 799.125514784732,
		road: 7779.98222222222,
		acceleration: -0.0771296296296278
	},
	{
		id: 789,
		time: 788,
		velocity: 8.72888888888889,
		power: -286.453887054724,
		road: 7788.71148148148,
		acceleration: -0.204814814814817
	},
	{
		id: 790,
		time: 789,
		velocity: 8.34611111111111,
		power: 205.411345264269,
		road: 7797.26662037037,
		acceleration: -0.143425925925925
	},
	{
		id: 791,
		time: 790,
		velocity: 8.37527777777778,
		power: -622.402740897987,
		road: 7805.62754629629,
		acceleration: -0.245000000000001
	},
	{
		id: 792,
		time: 791,
		velocity: 7.99388888888889,
		power: -453.456221587856,
		road: 7813.75435185185,
		acceleration: -0.223240740740742
	},
	{
		id: 793,
		time: 792,
		velocity: 7.67638888888889,
		power: -182.354093462088,
		road: 7821.67606481481,
		acceleration: -0.186944444444442
	},
	{
		id: 794,
		time: 793,
		velocity: 7.81444444444444,
		power: 482.018506684667,
		road: 7829.45611111111,
		acceleration: -0.0963888888888889
	},
	{
		id: 795,
		time: 794,
		velocity: 7.70472222222222,
		power: 984.243235542609,
		road: 7837.17449074074,
		acceleration: -0.026944444444446
	},
	{
		id: 796,
		time: 795,
		velocity: 7.59555555555556,
		power: 320.435544508179,
		road: 7844.82125,
		acceleration: -0.116296296296296
	},
	{
		id: 797,
		time: 796,
		velocity: 7.46555555555556,
		power: 333.796325994549,
		road: 7852.35347222222,
		acceleration: -0.112777777777778
	},
	{
		id: 798,
		time: 797,
		velocity: 7.36638888888889,
		power: 18.1364555192068,
		road: 7859.75148148148,
		acceleration: -0.155648148148148
	},
	{
		id: 799,
		time: 798,
		velocity: 7.12861111111111,
		power: 803.035437106019,
		road: 7867.05078703703,
		acceleration: -0.0417592592592593
	},
	{
		id: 800,
		time: 799,
		velocity: 7.34027777777778,
		power: 1468.79430077895,
		road: 7874.3561574074,
		acceleration: 0.0538888888888893
	},
	{
		id: 801,
		time: 800,
		velocity: 7.52805555555556,
		power: 2386.48678664153,
		road: 7881.77824074074,
		acceleration: 0.179537037037036
	},
	{
		id: 802,
		time: 801,
		velocity: 7.66722222222222,
		power: 1750.91296251866,
		road: 7889.33212962963,
		acceleration: 0.0840740740740742
	},
	{
		id: 803,
		time: 802,
		velocity: 7.5925,
		power: 1509.24141539515,
		road: 7896.95208333333,
		acceleration: 0.048055555555556
	},
	{
		id: 804,
		time: 803,
		velocity: 7.67222222222222,
		power: 998.116867338751,
		road: 7904.58467592592,
		acceleration: -0.0227777777777778
	},
	{
		id: 805,
		time: 804,
		velocity: 7.59888888888889,
		power: 347.989040018672,
		road: 7912.15023148148,
		acceleration: -0.111296296296296
	},
	{
		id: 806,
		time: 805,
		velocity: 7.25861111111111,
		power: -186.483595806059,
		road: 7919.56773148148,
		acceleration: -0.184814814814815
	},
	{
		id: 807,
		time: 806,
		velocity: 7.11777777777778,
		power: 1291.92474495634,
		road: 7926.90648148148,
		acceleration: 0.0273148148148152
	},
	{
		id: 808,
		time: 807,
		velocity: 7.68083333333333,
		power: 2514.83077778946,
		road: 7934.35694444444,
		acceleration: 0.19611111111111
	},
	{
		id: 809,
		time: 808,
		velocity: 7.84694444444444,
		power: 4334.64267053408,
		road: 7942.11824074074,
		acceleration: 0.425555555555556
	},
	{
		id: 810,
		time: 809,
		velocity: 8.39444444444444,
		power: 4685.95132805527,
		road: 7950.31018518518,
		acceleration: 0.43574074074074
	},
	{
		id: 811,
		time: 810,
		velocity: 8.98805555555555,
		power: 4758.68689179653,
		road: 7958.92546296296,
		acceleration: 0.410925925925927
	},
	{
		id: 812,
		time: 811,
		velocity: 9.07972222222222,
		power: 4415.57127292779,
		road: 7967.91754629629,
		acceleration: 0.342685185185186
	},
	{
		id: 813,
		time: 812,
		velocity: 9.4225,
		power: 3037.54473702203,
		road: 7977.16560185185,
		acceleration: 0.169259259259258
	},
	{
		id: 814,
		time: 813,
		velocity: 9.49583333333333,
		power: 3788.09679314511,
		road: 7986.61972222222,
		acceleration: 0.242870370370371
	},
	{
		id: 815,
		time: 814,
		velocity: 9.80833333333333,
		power: 2395.2995994651,
		road: 7996.23625,
		acceleration: 0.0819444444444457
	},
	{
		id: 816,
		time: 815,
		velocity: 9.66833333333333,
		power: 3651.8681615345,
		road: 8005.99949074074,
		acceleration: 0.211481481481481
	},
	{
		id: 817,
		time: 816,
		velocity: 10.1302777777778,
		power: 2494.88571901913,
		road: 8015.90921296296,
		acceleration: 0.0814814814814806
	},
	{
		id: 818,
		time: 817,
		velocity: 10.0527777777778,
		power: 2722.03465396089,
		road: 8025.91060185185,
		acceleration: 0.101851851851851
	},
	{
		id: 819,
		time: 818,
		velocity: 9.97388888888889,
		power: 1213.96076053996,
		road: 8035.93444444444,
		acceleration: -0.0569444444444454
	},
	{
		id: 820,
		time: 819,
		velocity: 9.95944444444444,
		power: 868.70869373629,
		road: 8045.88402777778,
		acceleration: -0.0915740740740745
	},
	{
		id: 821,
		time: 820,
		velocity: 9.77805555555556,
		power: 1122.01028035025,
		road: 8055.75629629629,
		acceleration: -0.0630555555555539
	},
	{
		id: 822,
		time: 821,
		velocity: 9.78472222222222,
		power: 757.916720977551,
		road: 8065.54694444444,
		acceleration: -0.100185185185186
	},
	{
		id: 823,
		time: 822,
		velocity: 9.65888888888889,
		power: 886.771028013529,
		road: 8075.24527777778,
		acceleration: -0.0844444444444434
	},
	{
		id: 824,
		time: 823,
		velocity: 9.52472222222222,
		power: 522.559036207711,
		road: 8084.84032407407,
		acceleration: -0.122129629629629
	},
	{
		id: 825,
		time: 824,
		velocity: 9.41833333333333,
		power: -92.829110656289,
		road: 8094.28032407407,
		acceleration: -0.187962962962963
	},
	{
		id: 826,
		time: 825,
		velocity: 9.095,
		power: -829.698550977629,
		road: 8103.49143518518,
		acceleration: -0.269814814814815
	},
	{
		id: 827,
		time: 826,
		velocity: 8.71527777777778,
		power: -1101.73435873531,
		road: 8112.41666666666,
		acceleration: -0.301944444444445
	},
	{
		id: 828,
		time: 827,
		velocity: 8.5125,
		power: -1490.13241625351,
		road: 8121.01537037037,
		acceleration: -0.351111111111109
	},
	{
		id: 829,
		time: 828,
		velocity: 8.04166666666667,
		power: -1136.73738185943,
		road: 8129.28333333333,
		acceleration: -0.310370370370372
	},
	{
		id: 830,
		time: 829,
		velocity: 7.78416666666667,
		power: -1354.95377191098,
		road: 8137.225,
		acceleration: -0.342222222222222
	},
	{
		id: 831,
		time: 830,
		velocity: 7.48583333333333,
		power: -1163.02687326786,
		road: 8144.83523148148,
		acceleration: -0.320648148148148
	},
	{
		id: 832,
		time: 831,
		velocity: 7.07972222222222,
		power: -1539.44512192684,
		road: 8152.09513888889,
		acceleration: -0.380000000000001
	},
	{
		id: 833,
		time: 832,
		velocity: 6.64416666666667,
		power: -1505.30556990922,
		road: 8158.97296296296,
		acceleration: -0.384166666666666
	},
	{
		id: 834,
		time: 833,
		velocity: 6.33333333333333,
		power: -1397.09408471588,
		road: 8165.47004629629,
		acceleration: -0.377314814814814
	},
	{
		id: 835,
		time: 834,
		velocity: 5.94777777777778,
		power: -246.37490982571,
		road: 8171.68305555555,
		acceleration: -0.190833333333334
	},
	{
		id: 836,
		time: 835,
		velocity: 6.07166666666667,
		power: -627.327810419966,
		road: 8177.67175925926,
		acceleration: -0.257777777777778
	},
	{
		id: 837,
		time: 836,
		velocity: 5.56,
		power: 1089.17248367947,
		road: 8183.55550925926,
		acceleration: 0.0478703703703696
	},
	{
		id: 838,
		time: 837,
		velocity: 6.09138888888889,
		power: 2112.68597891459,
		road: 8189.57393518518,
		acceleration: 0.221481481481482
	},
	{
		id: 839,
		time: 838,
		velocity: 6.73611111111111,
		power: 4377.5085306967,
		road: 8195.98675925926,
		acceleration: 0.567314814814815
	},
	{
		id: 840,
		time: 839,
		velocity: 7.26194444444444,
		power: 4163.81663213542,
		road: 8202.92162037037,
		acceleration: 0.476759259259259
	},
	{
		id: 841,
		time: 840,
		velocity: 7.52166666666667,
		power: 4049.3414068243,
		road: 8210.30407407407,
		acceleration: 0.418425925925925
	},
	{
		id: 842,
		time: 841,
		velocity: 7.99138888888889,
		power: 3688.72303969164,
		road: 8218.06481481481,
		acceleration: 0.338148148148148
	},
	{
		id: 843,
		time: 842,
		velocity: 8.27638888888889,
		power: 3663.8998638202,
		road: 8226.15055555555,
		acceleration: 0.311851851851851
	},
	{
		id: 844,
		time: 843,
		velocity: 8.45722222222222,
		power: 2837.1073757383,
		road: 8234.48768518518,
		acceleration: 0.190925925925928
	},
	{
		id: 845,
		time: 844,
		velocity: 8.56416666666667,
		power: 2198.84169039574,
		road: 8242.9724074074,
		acceleration: 0.104259259259258
	},
	{
		id: 846,
		time: 845,
		velocity: 8.58916666666667,
		power: 236.82405599275,
		road: 8251.44004629629,
		acceleration: -0.138425925925926
	},
	{
		id: 847,
		time: 846,
		velocity: 8.04194444444444,
		power: -1344.57423404513,
		road: 8259.66986111111,
		acceleration: -0.337222222222222
	},
	{
		id: 848,
		time: 847,
		velocity: 7.5525,
		power: -2354.73262760788,
		road: 8267.49194444444,
		acceleration: -0.47824074074074
	},
	{
		id: 849,
		time: 848,
		velocity: 7.15444444444444,
		power: -1505.25993617806,
		road: 8274.88884259259,
		acceleration: -0.37212962962963
	},
	{
		id: 850,
		time: 849,
		velocity: 6.92555555555555,
		power: -514.224877085706,
		road: 8281.9837037037,
		acceleration: -0.231944444444445
	},
	{
		id: 851,
		time: 850,
		velocity: 6.85666666666667,
		power: 466.451888812213,
		road: 8288.92069444444,
		acceleration: -0.0837962962962955
	},
	{
		id: 852,
		time: 851,
		velocity: 6.90305555555556,
		power: 1689.03701756195,
		road: 8295.86634259259,
		acceleration: 0.101111111111111
	},
	{
		id: 853,
		time: 852,
		velocity: 7.22888888888889,
		power: 2857.59525725652,
		road: 8302.99523148148,
		acceleration: 0.26537037037037
	},
	{
		id: 854,
		time: 853,
		velocity: 7.65277777777778,
		power: 4308.21639201108,
		road: 8310.47981481481,
		acceleration: 0.446018518518519
	},
	{
		id: 855,
		time: 854,
		velocity: 8.24111111111111,
		power: 5779.06704798488,
		road: 8318.4849074074,
		acceleration: 0.595000000000002
	},
	{
		id: 856,
		time: 855,
		velocity: 9.01388888888889,
		power: 7756.52886342708,
		road: 8327.17143518518,
		acceleration: 0.767870370370369
	},
	{
		id: 857,
		time: 856,
		velocity: 9.95638888888889,
		power: 7379.21995436009,
		road: 8336.56564814814,
		acceleration: 0.647499999999999
	},
	{
		id: 858,
		time: 857,
		velocity: 10.1836111111111,
		power: 4441.20974044846,
		road: 8346.42879629629,
		acceleration: 0.29037037037037
	},
	{
		id: 859,
		time: 858,
		velocity: 9.885,
		power: 1165.57621551742,
		road: 8356.40666666666,
		acceleration: -0.0609259259259236
	},
	{
		id: 860,
		time: 859,
		velocity: 9.77361111111111,
		power: 1728.15989270694,
		road: 8366.3536574074,
		acceleration: -0.000833333333336128
	},
	{
		id: 861,
		time: 860,
		velocity: 10.1811111111111,
		power: 4160.62000915807,
		road: 8376.42472222222,
		acceleration: 0.248981481481483
	},
	{
		id: 862,
		time: 861,
		velocity: 10.6319444444444,
		power: 6242.47135595171,
		road: 8386.84041666666,
		acceleration: 0.440277777777776
	},
	{
		id: 863,
		time: 862,
		velocity: 11.0944444444444,
		power: 8420.84717148607,
		road: 8397.7824074074,
		acceleration: 0.612314814814814
	},
	{
		id: 864,
		time: 863,
		velocity: 12.0180555555556,
		power: 10107.252850161,
		road: 8409.38555555555,
		acceleration: 0.710000000000001
	},
	{
		id: 865,
		time: 864,
		velocity: 12.7619444444444,
		power: 13075.9037707591,
		road: 8421.78902777777,
		acceleration: 0.890648148148147
	},
	{
		id: 866,
		time: 865,
		velocity: 13.7663888888889,
		power: 13158.3408430405,
		road: 8435.04449074073,
		acceleration: 0.813333333333336
	},
	{
		id: 867,
		time: 866,
		velocity: 14.4580555555556,
		power: 10373.1824795634,
		road: 8448.97796296296,
		acceleration: 0.542685185185185
	},
	{
		id: 868,
		time: 867,
		velocity: 14.39,
		power: 6353.31237793964,
		road: 8463.2936111111,
		acceleration: 0.221666666666666
	},
	{
		id: 869,
		time: 868,
		velocity: 14.4313888888889,
		power: 512.488720580897,
		road: 8477.61726851851,
		acceleration: -0.205648148148148
	},
	{
		id: 870,
		time: 869,
		velocity: 13.8411111111111,
		power: 132.09835071364,
		road: 8491.72319444444,
		acceleration: -0.229814814814812
	},
	{
		id: 871,
		time: 870,
		velocity: 13.7005555555556,
		power: -1638.77850717716,
		road: 8505.53458333333,
		acceleration: -0.359259259259261
	},
	{
		id: 872,
		time: 871,
		velocity: 13.3536111111111,
		power: 314.241777943421,
		road: 8519.06319444444,
		acceleration: -0.206296296296298
	},
	{
		id: 873,
		time: 872,
		velocity: 13.2222222222222,
		power: -93.2152676836044,
		road: 8532.37134259259,
		acceleration: -0.23462962962963
	},
	{
		id: 874,
		time: 873,
		velocity: 12.9966666666667,
		power: 536.364666422976,
		road: 8545.47152777777,
		acceleration: -0.181296296296296
	},
	{
		id: 875,
		time: 874,
		velocity: 12.8097222222222,
		power: -67.2271338710423,
		road: 8558.36773148147,
		acceleration: -0.226666666666667
	},
	{
		id: 876,
		time: 875,
		velocity: 12.5422222222222,
		power: 1183.49085210111,
		road: 8571.08999999999,
		acceleration: -0.121203703703703
	},
	{
		id: 877,
		time: 876,
		velocity: 12.6330555555556,
		power: 2534.0231369185,
		road: 8583.74768518518,
		acceleration: -0.00796296296296362
	},
	{
		id: 878,
		time: 877,
		velocity: 12.7858333333333,
		power: 5698.87279501975,
		road: 8596.52532407407,
		acceleration: 0.247870370370368
	},
	{
		id: 879,
		time: 878,
		velocity: 13.2858333333333,
		power: 7427.93900621485,
		road: 8609.61222222222,
		acceleration: 0.370648148148153
	},
	{
		id: 880,
		time: 879,
		velocity: 13.745,
		power: 10983.1142851842,
		road: 8623.19226851851,
		acceleration: 0.615648148148146
	},
	{
		id: 881,
		time: 880,
		velocity: 14.6327777777778,
		power: 11953.7024930777,
		road: 8637.3999537037,
		acceleration: 0.639629629629628
	},
	{
		id: 882,
		time: 881,
		velocity: 15.2047222222222,
		power: 12857.907821449,
		road: 8652.25449074074,
		acceleration: 0.654074074074074
	},
	{
		id: 883,
		time: 882,
		velocity: 15.7072222222222,
		power: 12750.9234959639,
		road: 8667.73564814814,
		acceleration: 0.599166666666669
	},
	{
		id: 884,
		time: 883,
		velocity: 16.4302777777778,
		power: 12647.9087408052,
		road: 8683.79194444444,
		acceleration: 0.55111111111111
	},
	{
		id: 885,
		time: 884,
		velocity: 16.8580555555556,
		power: 10155.4087473397,
		road: 8700.30476851851,
		acceleration: 0.361944444444443
	},
	{
		id: 886,
		time: 885,
		velocity: 16.7930555555556,
		power: 7809.88140427376,
		road: 8717.09847222222,
		acceleration: 0.199814814814815
	},
	{
		id: 887,
		time: 886,
		velocity: 17.0297222222222,
		power: 6068.06095443183,
		road: 8734.03481481481,
		acceleration: 0.0854629629629642
	},
	{
		id: 888,
		time: 887,
		velocity: 17.1144444444444,
		power: 7992.38895089259,
		road: 8751.11259259259,
		acceleration: 0.197407407407404
	},
	{
		id: 889,
		time: 888,
		velocity: 17.3852777777778,
		power: 6241.26479555638,
		road: 8768.33125,
		acceleration: 0.0843518518518565
	},
	{
		id: 890,
		time: 889,
		velocity: 17.2827777777778,
		power: 4481.95818334116,
		road: 8785.58032407407,
		acceleration: -0.0235185185185216
	},
	{
		id: 891,
		time: 890,
		velocity: 17.0438888888889,
		power: 2472.14129271965,
		road: 8802.7461574074,
		acceleration: -0.142962962962962
	},
	{
		id: 892,
		time: 891,
		velocity: 16.9563888888889,
		power: 1933.05379962082,
		road: 8819.75467592592,
		acceleration: -0.171666666666667
	},
	{
		id: 893,
		time: 892,
		velocity: 16.7677777777778,
		power: 2513.98111549252,
		road: 8836.61152777777,
		acceleration: -0.131666666666668
	},
	{
		id: 894,
		time: 893,
		velocity: 16.6488888888889,
		power: 2008.65517659125,
		road: 8853.32296296296,
		acceleration: -0.159166666666664
	},
	{
		id: 895,
		time: 894,
		velocity: 16.4788888888889,
		power: 2749.66410178304,
		road: 8869.90037037036,
		acceleration: -0.108888888888888
	},
	{
		id: 896,
		time: 895,
		velocity: 16.4411111111111,
		power: 1962.38194245051,
		road: 8886.34578703703,
		acceleration: -0.155092592592592
	},
	{
		id: 897,
		time: 896,
		velocity: 16.1836111111111,
		power: 1738.53791652998,
		road: 8902.63101851851,
		acceleration: -0.165277777777778
	},
	{
		id: 898,
		time: 897,
		velocity: 15.9830555555556,
		power: 834.799061621118,
		road: 8918.72402777777,
		acceleration: -0.219166666666668
	},
	{
		id: 899,
		time: 898,
		velocity: 15.7836111111111,
		power: 871.400770269922,
		road: 8934.60138888888,
		acceleration: -0.212129629629633
	},
	{
		id: 900,
		time: 899,
		velocity: 15.5472222222222,
		power: 642.176824641846,
		road: 8950.26129629629,
		acceleration: -0.222777777777775
	},
	{
		id: 901,
		time: 900,
		velocity: 15.3147222222222,
		power: 1292.5680502583,
		road: 8965.7224537037,
		acceleration: -0.174722222222222
	},
	{
		id: 902,
		time: 901,
		velocity: 15.2594444444444,
		power: 1652.63652091568,
		road: 8981.02305555555,
		acceleration: -0.14638888888889
	},
	{
		id: 903,
		time: 902,
		velocity: 15.1080555555556,
		power: 1688.55873532606,
		road: 8996.18027777777,
		acceleration: -0.140370370370372
	},
	{
		id: 904,
		time: 903,
		velocity: 14.8936111111111,
		power: -2451.97629262707,
		road: 9011.0549537037,
		acceleration: -0.42472222222222
	},
	{
		id: 905,
		time: 904,
		velocity: 13.9852777777778,
		power: 43.328505741672,
		road: 9025.59550925925,
		acceleration: -0.243518518518517
	},
	{
		id: 906,
		time: 905,
		velocity: 14.3775,
		power: 1199.2361791637,
		road: 9039.93638888888,
		acceleration: -0.155833333333334
	},
	{
		id: 907,
		time: 906,
		velocity: 14.4261111111111,
		power: 5204.07261414598,
		road: 9054.26782407407,
		acceleration: 0.136944444444442
	},
	{
		id: 908,
		time: 907,
		velocity: 14.3961111111111,
		power: 3591.3453678656,
		road: 9068.67597222222,
		acceleration: 0.0164814814814829
	},
	{
		id: 909,
		time: 908,
		velocity: 14.4269444444444,
		power: 4250.42751815726,
		road: 9083.12379629629,
		acceleration: 0.0628703703703692
	},
	{
		id: 910,
		time: 909,
		velocity: 14.6147222222222,
		power: 4155.1775281699,
		road: 9097.6299537037,
		acceleration: 0.0537962962962961
	},
	{
		id: 911,
		time: 910,
		velocity: 14.5575,
		power: 4311.56999172644,
		road: 9112.19444444444,
		acceleration: 0.062870370370371
	},
	{
		id: 912,
		time: 911,
		velocity: 14.6155555555556,
		power: 4950.82095816855,
		road: 9126.84305555555,
		acceleration: 0.105370370370371
	},
	{
		id: 913,
		time: 912,
		velocity: 14.9308333333333,
		power: 7307.88837562336,
		road: 9141.67643518518,
		acceleration: 0.264166666666668
	},
	{
		id: 914,
		time: 913,
		velocity: 15.35,
		power: 8082.49676228754,
		road: 9156.79351851851,
		acceleration: 0.303240740740737
	},
	{
		id: 915,
		time: 914,
		velocity: 15.5252777777778,
		power: 7641.24304354672,
		road: 9172.19129629629,
		acceleration: 0.25814814814815
	},
	{
		id: 916,
		time: 915,
		velocity: 15.7052777777778,
		power: 5321.37826703159,
		road: 9187.76481481481,
		acceleration: 0.0933333333333319
	},
	{
		id: 917,
		time: 916,
		velocity: 15.63,
		power: 4144.71006828991,
		road: 9203.3912037037,
		acceleration: 0.0124074074074088
	},
	{
		id: 918,
		time: 917,
		velocity: 15.5625,
		power: 2816.81705812645,
		road: 9218.98601851852,
		acceleration: -0.0755555555555567
	},
	{
		id: 919,
		time: 918,
		velocity: 15.4786111111111,
		power: 2083.39692756395,
		road: 9234.48199074074,
		acceleration: -0.122129629629628
	},
	{
		id: 920,
		time: 919,
		velocity: 15.2636111111111,
		power: 1140.09244154268,
		road: 9249.82569444444,
		acceleration: -0.18240740740741
	},
	{
		id: 921,
		time: 920,
		velocity: 15.0152777777778,
		power: 405.312785390438,
		road: 9264.96388888889,
		acceleration: -0.228611111111109
	},
	{
		id: 922,
		time: 921,
		velocity: 14.7927777777778,
		power: -847.928766403342,
		road: 9279.83189814814,
		acceleration: -0.31175925925926
	},
	{
		id: 923,
		time: 922,
		velocity: 14.3283333333333,
		power: -806.011671743066,
		road: 9294.39157407407,
		acceleration: -0.304907407407407
	},
	{
		id: 924,
		time: 923,
		velocity: 14.1005555555556,
		power: -579.811425607308,
		road: 9308.65643518518,
		acceleration: -0.284722222222221
	},
	{
		id: 925,
		time: 924,
		velocity: 13.9386111111111,
		power: 721.92639664552,
		road: 9322.68666666666,
		acceleration: -0.184537037037039
	},
	{
		id: 926,
		time: 925,
		velocity: 13.7747222222222,
		power: 1631.90049233258,
		road: 9336.56814814814,
		acceleration: -0.112962962962964
	},
	{
		id: 927,
		time: 926,
		velocity: 13.7616666666667,
		power: 1272.84487282122,
		road: 9350.32453703703,
		acceleration: -0.137222222222221
	},
	{
		id: 928,
		time: 927,
		velocity: 13.5269444444444,
		power: 1394.5026407943,
		road: 9363.94986111111,
		acceleration: -0.124907407407406
	},
	{
		id: 929,
		time: 928,
		velocity: 13.4,
		power: 1096.45441339704,
		road: 9377.44027777777,
		acceleration: -0.144907407407409
	},
	{
		id: 930,
		time: 929,
		velocity: 13.3269444444444,
		power: 1608.31389019788,
		road: 9390.80717592592,
		acceleration: -0.10212962962963
	},
	{
		id: 931,
		time: 930,
		velocity: 13.2205555555556,
		power: 1518.37646284352,
		road: 9404.06967592592,
		acceleration: -0.106666666666666
	},
	{
		id: 932,
		time: 931,
		velocity: 13.08,
		power: 1225.12580250442,
		road: 9417.21523148148,
		acceleration: -0.127222222222221
	},
	{
		id: 933,
		time: 932,
		velocity: 12.9452777777778,
		power: 332.578750389017,
		road: 9430.19935185185,
		acceleration: -0.19564814814815
	},
	{
		id: 934,
		time: 933,
		velocity: 12.6336111111111,
		power: 336.121159665038,
		road: 9442.98958333333,
		acceleration: -0.19212962962963
	},
	{
		id: 935,
		time: 934,
		velocity: 12.5036111111111,
		power: 640.570693941736,
		road: 9455.60180555555,
		acceleration: -0.163888888888888
	},
	{
		id: 936,
		time: 935,
		velocity: 12.4536111111111,
		power: 1003.58771061605,
		road: 9468.06675925926,
		acceleration: -0.130648148148149
	},
	{
		id: 937,
		time: 936,
		velocity: 12.2416666666667,
		power: 983.597983187718,
		road: 9480.40157407407,
		acceleration: -0.129629629629632
	},
	{
		id: 938,
		time: 937,
		velocity: 12.1147222222222,
		power: -3457.7856112774,
		road: 9492.41629629629,
		acceleration: -0.510555555555552
	},
	{
		id: 939,
		time: 938,
		velocity: 10.9219444444444,
		power: -6164.58972351464,
		road: 9503.79134259259,
		acceleration: -0.768796296296298
	},
	{
		id: 940,
		time: 939,
		velocity: 9.93527777777778,
		power: -11332.1931787249,
		road: 9514.11203703703,
		acceleration: -1.33990740740741
	},
	{
		id: 941,
		time: 940,
		velocity: 8.095,
		power: -9086.4969455947,
		road: 9523.14796296296,
		acceleration: -1.22962962962963
	},
	{
		id: 942,
		time: 941,
		velocity: 7.23305555555556,
		power: -6945.84432416015,
		road: 9531.02449074074,
		acceleration: -1.08916666666667
	},
	{
		id: 943,
		time: 942,
		velocity: 6.66777777777778,
		power: -2937.1772388096,
		road: 9538.05930555555,
		acceleration: -0.594259259259259
	},
	{
		id: 944,
		time: 943,
		velocity: 6.31222222222222,
		power: 539.002853720473,
		road: 9544.76291666666,
		acceleration: -0.0681481481481478
	},
	{
		id: 945,
		time: 944,
		velocity: 7.02861111111111,
		power: 2712.00629803576,
		road: 9551.56532407407,
		acceleration: 0.26574074074074
	},
	{
		id: 946,
		time: 945,
		velocity: 7.465,
		power: 3383.12955523364,
		road: 9558.67287037037,
		acceleration: 0.344537037037038
	},
	{
		id: 947,
		time: 946,
		velocity: 7.34583333333333,
		power: 683.167024276179,
		road: 9565.9237037037,
		acceleration: -0.0579629629629634
	},
	{
		id: 948,
		time: 947,
		velocity: 6.85472222222222,
		power: -1052.5543120286,
		road: 9572.98949074074,
		acceleration: -0.312129629629631
	},
	{
		id: 949,
		time: 948,
		velocity: 6.52861111111111,
		power: -1532.19313305835,
		road: 9579.70282407407,
		acceleration: -0.392777777777777
	},
	{
		id: 950,
		time: 949,
		velocity: 6.1675,
		power: -219.571664413946,
		road: 9586.12648148148,
		acceleration: -0.186574074074074
	},
	{
		id: 951,
		time: 950,
		velocity: 6.295,
		power: 1704.06336785564,
		road: 9592.52175925926,
		acceleration: 0.129814814814815
	},
	{
		id: 952,
		time: 951,
		velocity: 6.91805555555556,
		power: 5523.44283701076,
		road: 9599.33157407407,
		acceleration: 0.699259259259258
	},
	{
		id: 953,
		time: 952,
		velocity: 8.26527777777778,
		power: 10232.6603902298,
		road: 9607.10212962963,
		acceleration: 1.22222222222222
	},
	{
		id: 954,
		time: 953,
		velocity: 9.96166666666667,
		power: 13526.6537897053,
		road: 9616.17916666667,
		acceleration: 1.39074074074074
	},
	{
		id: 955,
		time: 954,
		velocity: 11.0902777777778,
		power: 10408.3447720226,
		road: 9626.39310185185,
		acceleration: 0.883055555555556
	},
	{
		id: 956,
		time: 955,
		velocity: 10.9144444444444,
		power: 4911.86806878652,
		road: 9637.19060185185,
		acceleration: 0.284074074074073
	},
	{
		id: 957,
		time: 956,
		velocity: 10.8138888888889,
		power: 2555.06917877496,
		road: 9648.15476851852,
		acceleration: 0.0492592592592587
	},
	{
		id: 958,
		time: 957,
		velocity: 11.2380555555556,
		power: 5245.7153024871,
		road: 9659.29189814815,
		acceleration: 0.296666666666667
	},
	{
		id: 959,
		time: 958,
		velocity: 11.8044444444444,
		power: 8780.94730481611,
		road: 9670.87333333333,
		acceleration: 0.591944444444444
	},
	{
		id: 960,
		time: 959,
		velocity: 12.5897222222222,
		power: 11407.4993793389,
		road: 9683.13231481481,
		acceleration: 0.763148148148151
	},
	{
		id: 961,
		time: 960,
		velocity: 13.5275,
		power: 13166.6957028091,
		road: 9696.18930555556,
		acceleration: 0.832870370370369
	},
	{
		id: 962,
		time: 961,
		velocity: 14.3030555555556,
		power: 12997.9394102766,
		road: 9710.03643518519,
		acceleration: 0.747407407407408
	},
	{
		id: 963,
		time: 962,
		velocity: 14.8319444444444,
		power: 12292.3740064632,
		road: 9724.57643518519,
		acceleration: 0.638333333333334
	},
	{
		id: 964,
		time: 963,
		velocity: 15.4425,
		power: 10791.6511762017,
		road: 9739.68138888889,
		acceleration: 0.491574074074075
	},
	{
		id: 965,
		time: 964,
		velocity: 15.7777777777778,
		power: 6214.4352377307,
		road: 9755.11199074074,
		acceleration: 0.159722222222221
	},
	{
		id: 966,
		time: 965,
		velocity: 15.3111111111111,
		power: 1365.21018400476,
		road: 9770.53796296296,
		acceleration: -0.168981481481483
	},
	{
		id: 967,
		time: 966,
		velocity: 14.9355555555556,
		power: -1332.59221492566,
		road: 9785.70490740741,
		acceleration: -0.349074074074073
	},
	{
		id: 968,
		time: 967,
		velocity: 14.7305555555556,
		power: 696.029995820465,
		road: 9800.59555555556,
		acceleration: -0.203518518518518
	},
	{
		id: 969,
		time: 968,
		velocity: 14.7005555555556,
		power: 1919.29834048297,
		road: 9815.32773148148,
		acceleration: -0.113425925925927
	},
	{
		id: 970,
		time: 969,
		velocity: 14.5952777777778,
		power: 2005.28287461378,
		road: 9829.95097222222,
		acceleration: -0.104444444444443
	},
	{
		id: 971,
		time: 970,
		velocity: 14.4172222222222,
		power: 1610.90652483697,
		road: 9844.45708333333,
		acceleration: -0.129814814814818
	},
	{
		id: 972,
		time: 971,
		velocity: 14.3111111111111,
		power: 1940.88792539402,
		road: 9858.84680555556,
		acceleration: -0.102962962962962
	},
	{
		id: 973,
		time: 972,
		velocity: 14.2863888888889,
		power: 2514.3368384744,
		road: 9873.15560185185,
		acceleration: -0.0588888888888874
	},
	{
		id: 974,
		time: 973,
		velocity: 14.2405555555556,
		power: 3221.08845029503,
		road: 9887.43189814815,
		acceleration: -0.00611111111111207
	},
	{
		id: 975,
		time: 974,
		velocity: 14.2927777777778,
		power: 3921.23906523038,
		road: 9901.72740740741,
		acceleration: 0.0445370370370384
	},
	{
		id: 976,
		time: 975,
		velocity: 14.42,
		power: 4234.16196747351,
		road: 9916.07787037037,
		acceleration: 0.0653703703703723
	},
	{
		id: 977,
		time: 976,
		velocity: 14.4366666666667,
		power: 5884.34124493845,
		road: 9930.55106481482,
		acceleration: 0.18009259259259
	},
	{
		id: 978,
		time: 977,
		velocity: 14.8330555555556,
		power: 5980.98211006505,
		road: 9945.20370370371,
		acceleration: 0.178796296296296
	},
	{
		id: 979,
		time: 978,
		velocity: 14.9563888888889,
		power: 6608.68505618227,
		road: 9960.05277777778,
		acceleration: 0.214074074074075
	},
	{
		id: 980,
		time: 979,
		velocity: 15.0788888888889,
		power: 3597.55266822891,
		road: 9975.00800925926,
		acceleration: -0.00175925925925924
	},
	{
		id: 981,
		time: 980,
		velocity: 14.8277777777778,
		power: 1910.93437717391,
		road: 9989.90324074074,
		acceleration: -0.118240740740742
	},
	{
		id: 982,
		time: 981,
		velocity: 14.6016666666667,
		power: 203.602257612472,
		road: 10004.6218055556,
		acceleration: -0.235092592592594
	},
	{
		id: 983,
		time: 982,
		velocity: 14.3736111111111,
		power: 2221.92343505181,
		road: 10019.1792592593,
		acceleration: -0.0871296296296293
	},
	{
		id: 984,
		time: 983,
		velocity: 14.5663888888889,
		power: 2522.50382242399,
		road: 10033.6614814815,
		acceleration: -0.0633333333333344
	},
	{
		id: 985,
		time: 984,
		velocity: 14.4116666666667,
		power: 3238.5382722331,
		road: 10048.1068518519,
		acceleration: -0.0103703703703673
	},
	{
		id: 986,
		time: 985,
		velocity: 14.3425,
		power: 1484.68905233224,
		road: 10062.4791666667,
		acceleration: -0.135740740740744
	},
	{
		id: 987,
		time: 986,
		velocity: 14.1591666666667,
		power: 1979.08049228966,
		road: 10076.7352777778,
		acceleration: -0.096666666666664
	},
	{
		id: 988,
		time: 987,
		velocity: 14.1216666666667,
		power: 3677.53581221416,
		road: 10090.9576851852,
		acceleration: 0.0292592592592573
	},
	{
		id: 989,
		time: 988,
		velocity: 14.4302777777778,
		power: 6270.88828968522,
		road: 10105.3018518519,
		acceleration: 0.214259259259261
	},
	{
		id: 990,
		time: 989,
		velocity: 14.8019444444444,
		power: 9517.7544974597,
		road: 10119.9684259259,
		acceleration: 0.430555555555554
	},
	{
		id: 991,
		time: 990,
		velocity: 15.4133333333333,
		power: 10458.3108623824,
		road: 10135.0841666667,
		acceleration: 0.46777777777778
	},
	{
		id: 992,
		time: 991,
		velocity: 15.8336111111111,
		power: 10792.8140794364,
		road: 10150.6640277778,
		acceleration: 0.460462962962964
	},
	{
		id: 993,
		time: 992,
		velocity: 16.1833333333333,
		power: 9496.35429698528,
		road: 10166.6490740741,
		acceleration: 0.349907407407406
	},
	{
		id: 994,
		time: 993,
		velocity: 16.4630555555556,
		power: 7122.07741787317,
		road: 10182.9000925926,
		acceleration: 0.182037037037034
	},
	{
		id: 995,
		time: 994,
		velocity: 16.3797222222222,
		power: 5366.26279951441,
		road: 10199.274212963,
		acceleration: 0.0641666666666687
	},
	{
		id: 996,
		time: 995,
		velocity: 16.3758333333333,
		power: 4861.91079539454,
		road: 10215.6955092593,
		acceleration: 0.0301851851851858
	},
	{
		id: 997,
		time: 996,
		velocity: 16.5536111111111,
		power: 3717.31323353719,
		road: 10232.1106481482,
		acceleration: -0.0424999999999969
	},
	{
		id: 998,
		time: 997,
		velocity: 16.2522222222222,
		power: 4928.67919259449,
		road: 10248.5219444444,
		acceleration: 0.034814814814812
	},
	{
		id: 999,
		time: 998,
		velocity: 16.4802777777778,
		power: 2296.58849981631,
		road: 10264.8848611111,
		acceleration: -0.131574074074074
	},
	{
		id: 1000,
		time: 999,
		velocity: 16.1588888888889,
		power: 3671.97015113527,
		road: 10281.1615740741,
		acceleration: -0.0408333333333353
	},
	{
		id: 1001,
		time: 1000,
		velocity: 16.1297222222222,
		power: 1531.22178405695,
		road: 10297.33,
		acceleration: -0.175740740740739
	},
	{
		id: 1002,
		time: 1001,
		velocity: 15.9530555555556,
		power: 2207.59749699451,
		road: 10313.3466203704,
		acceleration: -0.127870370370372
	},
	{
		id: 1003,
		time: 1002,
		velocity: 15.7752777777778,
		power: 1321.13962927646,
		road: 10329.2082407407,
		acceleration: -0.182129629629628
	},
	{
		id: 1004,
		time: 1003,
		velocity: 15.5833333333333,
		power: 1.63982893402879,
		road: 10344.8462037037,
		acceleration: -0.265185185185183
	},
	{
		id: 1005,
		time: 1004,
		velocity: 15.1575,
		power: -342.000918973925,
		road: 10360.2096759259,
		acceleration: -0.283796296296297
	},
	{
		id: 1006,
		time: 1005,
		velocity: 14.9238888888889,
		power: -1567.99604358825,
		road: 10375.249212963,
		acceleration: -0.364074074074075
	},
	{
		id: 1007,
		time: 1006,
		velocity: 14.4911111111111,
		power: -1303.36940806412,
		road: 10389.9357407407,
		acceleration: -0.341944444444444
	},
	{
		id: 1008,
		time: 1007,
		velocity: 14.1316666666667,
		power: -1942.36788697316,
		road: 10404.25875,
		acceleration: -0.385092592592592
	},
	{
		id: 1009,
		time: 1008,
		velocity: 13.7686111111111,
		power: -1570.68056878457,
		road: 10418.2116666667,
		acceleration: -0.355092592592595
	},
	{
		id: 1010,
		time: 1009,
		velocity: 13.4258333333333,
		power: -697.80068634443,
		road: 10431.8441203704,
		acceleration: -0.285833333333333
	},
	{
		id: 1011,
		time: 1010,
		velocity: 13.2741666666667,
		power: 408.65544016217,
		road: 10445.2353703704,
		acceleration: -0.196574074074075
	},
	{
		id: 1012,
		time: 1011,
		velocity: 13.1788888888889,
		power: 1565.90164697453,
		road: 10458.4771296296,
		acceleration: -0.102407407407407
	},
	{
		id: 1013,
		time: 1012,
		velocity: 13.1186111111111,
		power: 1845.15860634611,
		road: 10471.6287037037,
		acceleration: -0.0779629629629639
	},
	{
		id: 1014,
		time: 1013,
		velocity: 13.0402777777778,
		power: 1405.96085966896,
		road: 10484.6859259259,
		acceleration: -0.11074074074074
	},
	{
		id: 1015,
		time: 1014,
		velocity: 12.8466666666667,
		power: 614.159465606549,
		road: 10497.6019444444,
		acceleration: -0.171666666666665
	},
	{
		id: 1016,
		time: 1015,
		velocity: 12.6036111111111,
		power: 79.9875142182088,
		road: 10510.3260648148,
		acceleration: -0.212129629629631
	},
	{
		id: 1017,
		time: 1016,
		velocity: 12.4038888888889,
		power: -578.434774325377,
		road: 10522.8121759259,
		acceleration: -0.263888888888889
	},
	{
		id: 1018,
		time: 1017,
		velocity: 12.055,
		power: -1078.97223498126,
		road: 10535.0143055556,
		acceleration: -0.304074074074075
	},
	{
		id: 1019,
		time: 1018,
		velocity: 11.6913888888889,
		power: -1127.26163501458,
		road: 10546.9111111111,
		acceleration: -0.306574074074073
	},
	{
		id: 1020,
		time: 1019,
		velocity: 11.4841666666667,
		power: -1377.53593178729,
		road: 10558.4907407407,
		acceleration: -0.327777777777779
	},
	{
		id: 1021,
		time: 1020,
		velocity: 11.0716666666667,
		power: -830.727005291474,
		road: 10569.768287037,
		acceleration: -0.276388888888889
	},
	{
		id: 1022,
		time: 1021,
		velocity: 10.8622222222222,
		power: -1046.07110607977,
		road: 10580.7599537037,
		acceleration: -0.295370370370371
	},
	{
		id: 1023,
		time: 1022,
		velocity: 10.5980555555556,
		power: -76.8369859121919,
		road: 10591.5039351852,
		acceleration: -0.199999999999999
	},
	{
		id: 1024,
		time: 1023,
		velocity: 10.4716666666667,
		power: -309.835144033133,
		road: 10602.0375,
		acceleration: -0.220833333333331
	},
	{
		id: 1025,
		time: 1024,
		velocity: 10.1997222222222,
		power: 396.519130283492,
		road: 10612.3868518519,
		acceleration: -0.147592592592595
	},
	{
		id: 1026,
		time: 1025,
		velocity: 10.1552777777778,
		power: 267.221107276656,
		road: 10622.5831481482,
		acceleration: -0.158518518518518
	},
	{
		id: 1027,
		time: 1026,
		velocity: 9.99611111111111,
		power: 941.974337248065,
		road: 10632.6569444444,
		acceleration: -0.0864814814814832
	},
	{
		id: 1028,
		time: 1027,
		velocity: 9.94027777777778,
		power: 1251.83982307717,
		road: 10642.66125,
		acceleration: -0.0524999999999984
	},
	{
		id: 1029,
		time: 1028,
		velocity: 9.99777777777778,
		power: 1231.62753717633,
		road: 10652.6126388889,
		acceleration: -0.0533333333333346
	},
	{
		id: 1030,
		time: 1029,
		velocity: 9.83611111111111,
		power: 288.091677428247,
		road: 10662.4616666667,
		acceleration: -0.151388888888887
	},
	{
		id: 1031,
		time: 1030,
		velocity: 9.48611111111111,
		power: -565.08676046003,
		road: 10672.1143055556,
		acceleration: -0.24138888888889
	},
	{
		id: 1032,
		time: 1031,
		velocity: 9.27361111111111,
		power: -932.608538909295,
		road: 10681.5055555556,
		acceleration: -0.281388888888888
	},
	{
		id: 1033,
		time: 1032,
		velocity: 8.99194444444444,
		power: 262.832410826921,
		road: 10690.6837037037,
		acceleration: -0.144814814814815
	},
	{
		id: 1034,
		time: 1033,
		velocity: 9.05166666666667,
		power: 2138.18526973067,
		road: 10699.825,
		acceleration: 0.0711111111111098
	},
	{
		id: 1035,
		time: 1034,
		velocity: 9.48694444444444,
		power: 4031.62397534038,
		road: 10709.1408796296,
		acceleration: 0.278055555555557
	},
	{
		id: 1036,
		time: 1035,
		velocity: 9.82611111111111,
		power: 5717.82286136351,
		road: 10718.8158796296,
		acceleration: 0.440185185185186
	},
	{
		id: 1037,
		time: 1036,
		velocity: 10.3722222222222,
		power: 6840.70003293258,
		road: 10728.9716203704,
		acceleration: 0.521296296296295
	},
	{
		id: 1038,
		time: 1037,
		velocity: 11.0508333333333,
		power: 8002.41278670097,
		road: 10739.6837962963,
		acceleration: 0.591574074074074
	},
	{
		id: 1039,
		time: 1038,
		velocity: 11.6008333333333,
		power: 7107.84227452142,
		road: 10750.9240277778,
		acceleration: 0.464537037037038
	},
	{
		id: 1040,
		time: 1039,
		velocity: 11.7658333333333,
		power: 6070.42375975041,
		road: 10762.5679166667,
		acceleration: 0.342777777777776
	},
	{
		id: 1041,
		time: 1040,
		velocity: 12.0791666666667,
		power: 5393.89648436167,
		road: 10774.5158796296,
		acceleration: 0.26537037037037
	},
	{
		id: 1042,
		time: 1041,
		velocity: 12.3969444444444,
		power: 6270.56779918728,
		road: 10786.7589814815,
		acceleration: 0.324907407407409
	},
	{
		id: 1043,
		time: 1042,
		velocity: 12.7405555555556,
		power: 6354.52080221508,
		road: 10799.3213888889,
		acceleration: 0.313703703703702
	},
	{
		id: 1044,
		time: 1043,
		velocity: 13.0202777777778,
		power: 3589.30373103663,
		road: 10812.0784722222,
		acceleration: 0.075648148148149
	},
	{
		id: 1045,
		time: 1044,
		velocity: 12.6238888888889,
		power: 484.155886348658,
		road: 10824.7841203704,
		acceleration: -0.178518518518517
	},
	{
		id: 1046,
		time: 1045,
		velocity: 12.205,
		power: -1723.64366001602,
		road: 10837.2205555556,
		acceleration: -0.359907407407409
	},
	{
		id: 1047,
		time: 1046,
		velocity: 11.9405555555556,
		power: -593.80546639312,
		road: 10849.3462037037,
		acceleration: -0.261666666666667
	},
	{
		id: 1048,
		time: 1047,
		velocity: 11.8388888888889,
		power: 1962.81580144732,
		road: 10861.3227777778,
		acceleration: -0.0364814814814807
	},
	{
		id: 1049,
		time: 1048,
		velocity: 12.0955555555556,
		power: 4010.85457150714,
		road: 10873.3513888889,
		acceleration: 0.140555555555554
	},
	{
		id: 1050,
		time: 1049,
		velocity: 12.3622222222222,
		power: 5405.00203498846,
		road: 10885.5761574074,
		acceleration: 0.251759259259259
	},
	{
		id: 1051,
		time: 1050,
		velocity: 12.5941666666667,
		power: 5890.56617821224,
		road: 10898.06625,
		acceleration: 0.27888888888889
	},
	{
		id: 1052,
		time: 1051,
		velocity: 12.9322222222222,
		power: 6604.17645741221,
		road: 10910.8564814815,
		acceleration: 0.321388888888887
	},
	{
		id: 1053,
		time: 1052,
		velocity: 13.3263888888889,
		power: 6964.83443744937,
		road: 10923.9733333333,
		acceleration: 0.331851851851855
	},
	{
		id: 1054,
		time: 1053,
		velocity: 13.5897222222222,
		power: 8775.94713586542,
		road: 10937.4812037037,
		acceleration: 0.450185185185184
	},
	{
		id: 1055,
		time: 1054,
		velocity: 14.2827777777778,
		power: 8314.03666876272,
		road: 10951.4083333333,
		acceleration: 0.388333333333332
	},
	{
		id: 1056,
		time: 1055,
		velocity: 14.4913888888889,
		power: 7738.02485105748,
		road: 10965.6919907408,
		acceleration: 0.324722222222224
	},
	{
		id: 1057,
		time: 1056,
		velocity: 14.5638888888889,
		power: 4241.10680754616,
		road: 10980.1685648148,
		acceleration: 0.0611111111111118
	},
	{
		id: 1058,
		time: 1057,
		velocity: 14.4661111111111,
		power: 1981.48924512403,
		road: 10994.6248148148,
		acceleration: -0.101759259259261
	},
	{
		id: 1059,
		time: 1058,
		velocity: 14.1861111111111,
		power: -521.735005689367,
		road: 11008.8899537037,
		acceleration: -0.280462962962964
	},
	{
		id: 1060,
		time: 1059,
		velocity: 13.7225,
		power: -1718.42233287175,
		road: 11022.8318055556,
		acceleration: -0.36611111111111
	},
	{
		id: 1061,
		time: 1060,
		velocity: 13.3677777777778,
		power: -1733.32123683373,
		road: 11036.4080555556,
		acceleration: -0.365092592592594
	},
	{
		id: 1062,
		time: 1061,
		velocity: 13.0908333333333,
		power: -967.748328224919,
		road: 11049.6503240741,
		acceleration: -0.302870370370366
	},
	{
		id: 1063,
		time: 1062,
		velocity: 12.8138888888889,
		power: -990.338171528177,
		road: 11062.5901388889,
		acceleration: -0.302037037037039
	},
	{
		id: 1064,
		time: 1063,
		velocity: 12.4616666666667,
		power: -2418.39355242978,
		road: 11075.1698611111,
		acceleration: -0.418148148148148
	},
	{
		id: 1065,
		time: 1064,
		velocity: 11.8363888888889,
		power: -2282.8628187685,
		road: 11087.336712963,
		acceleration: -0.407592592592593
	},
	{
		id: 1066,
		time: 1065,
		velocity: 11.5911111111111,
		power: -1722.40185680885,
		road: 11099.1202777778,
		acceleration: -0.358981481481484
	},
	{
		id: 1067,
		time: 1066,
		velocity: 11.3847222222222,
		power: -61.4213952973967,
		road: 11110.6205555556,
		acceleration: -0.20759259259259
	},
	{
		id: 1068,
		time: 1067,
		velocity: 11.2136111111111,
		power: 105.668286949097,
		road: 11121.9222222222,
		acceleration: -0.18962962962963
	},
	{
		id: 1069,
		time: 1068,
		velocity: 11.0222222222222,
		power: 682.320636251823,
		road: 11133.0625,
		acceleration: -0.133148148148146
	},
	{
		id: 1070,
		time: 1069,
		velocity: 10.9852777777778,
		power: 1580.92281342611,
		road: 11144.1131018519,
		acceleration: -0.0462037037037071
	},
	{
		id: 1071,
		time: 1070,
		velocity: 11.075,
		power: 2284.84861214215,
		road: 11155.1511111111,
		acceleration: 0.0210185185185203
	},
	{
		id: 1072,
		time: 1071,
		velocity: 11.0852777777778,
		power: 2127.43449657117,
		road: 11166.2024537037,
		acceleration: 0.00564814814814696
	},
	{
		id: 1073,
		time: 1072,
		velocity: 11.0022222222222,
		power: 924.339703167455,
		road: 11177.2028703704,
		acceleration: -0.1075
	},
	{
		id: 1074,
		time: 1073,
		velocity: 10.7525,
		power: 878.150691747017,
		road: 11188.0946759259,
		acceleration: -0.109722222222219
	},
	{
		id: 1075,
		time: 1074,
		velocity: 10.7561111111111,
		power: 1817.80589446117,
		road: 11198.9229166667,
		acceleration: -0.0174074074074095
	},
	{
		id: 1076,
		time: 1075,
		velocity: 10.95,
		power: 4147.56720870769,
		road: 11209.8443055556,
		acceleration: 0.203703703703704
	},
	{
		id: 1077,
		time: 1076,
		velocity: 11.3636111111111,
		power: 5735.23178335491,
		road: 11221.0372222222,
		acceleration: 0.339351851851852
	},
	{
		id: 1078,
		time: 1077,
		velocity: 11.7741666666667,
		power: 7130.91626328722,
		road: 11232.6210185185,
		acceleration: 0.442407407407407
	},
	{
		id: 1079,
		time: 1078,
		velocity: 12.2772222222222,
		power: 7176.43903644419,
		road: 11244.6347222222,
		acceleration: 0.417407407407408
	},
	{
		id: 1080,
		time: 1079,
		velocity: 12.6158333333333,
		power: 7024.72490241573,
		road: 11257.0465740741,
		acceleration: 0.378888888888888
	},
	{
		id: 1081,
		time: 1080,
		velocity: 12.9108333333333,
		power: 5508.25242669026,
		road: 11269.765462963,
		acceleration: 0.235185185185184
	},
	{
		id: 1082,
		time: 1081,
		velocity: 12.9827777777778,
		power: 5082.42921840872,
		road: 11282.6969907408,
		acceleration: 0.190092592592592
	},
	{
		id: 1083,
		time: 1082,
		velocity: 13.1861111111111,
		power: 4151.55412533168,
		road: 11295.7778703704,
		acceleration: 0.108611111111113
	},
	{
		id: 1084,
		time: 1083,
		velocity: 13.2366666666667,
		power: 4202.97109393616,
		road: 11308.9672222222,
		acceleration: 0.108333333333334
	},
	{
		id: 1085,
		time: 1084,
		velocity: 13.3077777777778,
		power: 4756.65509301406,
		road: 11322.2841203704,
		acceleration: 0.146759259259259
	},
	{
		id: 1086,
		time: 1085,
		velocity: 13.6263888888889,
		power: 4971.76856792421,
		road: 11335.7528703704,
		acceleration: 0.156944444444445
	},
	{
		id: 1087,
		time: 1086,
		velocity: 13.7075,
		power: 6332.68404002691,
		road: 11349.4262037037,
		acceleration: 0.252222222222223
	},
	{
		id: 1088,
		time: 1087,
		velocity: 14.0644444444444,
		power: 7943.8598252937,
		road: 11363.4043981482,
		acceleration: 0.357499999999998
	},
	{
		id: 1089,
		time: 1088,
		velocity: 14.6988888888889,
		power: 10634.2867575221,
		road: 11377.825,
		acceleration: 0.527314814814815
	},
	{
		id: 1090,
		time: 1089,
		velocity: 15.2894444444444,
		power: 11015.6220313005,
		road: 11392.7683333333,
		acceleration: 0.518148148148148
	},
	{
		id: 1091,
		time: 1090,
		velocity: 15.6188888888889,
		power: 9646.01106368431,
		road: 11408.1678703704,
		acceleration: 0.394259259259263
	},
	{
		id: 1092,
		time: 1091,
		velocity: 15.8816666666667,
		power: 8854.64180480128,
		road: 11423.9247685185,
		acceleration: 0.32046296296296
	},
	{
		id: 1093,
		time: 1092,
		velocity: 16.2508333333333,
		power: 7039.99868077155,
		road: 11439.9358796296,
		acceleration: 0.187962962962965
	},
	{
		id: 1094,
		time: 1093,
		velocity: 16.1827777777778,
		power: 3478.9536937988,
		road: 11456.0175,
		acceleration: -0.0469444444444456
	},
	{
		id: 1095,
		time: 1094,
		velocity: 15.7408333333333,
		power: -715.787387926219,
		road: 11471.9171296296,
		acceleration: -0.317037037037037
	},
	{
		id: 1096,
		time: 1095,
		velocity: 15.2997222222222,
		power: -3418.10187695621,
		road: 11487.4114351852,
		acceleration: -0.493611111111111
	},
	{
		id: 1097,
		time: 1096,
		velocity: 14.7019444444444,
		power: -2958.0087664364,
		road: 11502.4285648148,
		acceleration: -0.460740740740743
	},
	{
		id: 1098,
		time: 1097,
		velocity: 14.3586111111111,
		power: -3774.2852570979,
		road: 11516.9561111111,
		acceleration: -0.518425925925925
	},
	{
		id: 1099,
		time: 1098,
		velocity: 13.7444444444444,
		power: -2435.363667249,
		road: 11531.0143055556,
		acceleration: -0.420277777777779
	},
	{
		id: 1100,
		time: 1099,
		velocity: 13.4411111111111,
		power: -2870.3621381171,
		road: 11544.6359722222,
		acceleration: -0.452777777777776
	},
	{
		id: 1101,
		time: 1100,
		velocity: 13.0002777777778,
		power: -1491.13712649994,
		road: 11557.8591666667,
		acceleration: -0.344166666666666
	},
	{
		id: 1102,
		time: 1101,
		velocity: 12.7119444444444,
		power: -1452.69534912147,
		road: 11570.7406944445,
		acceleration: -0.339166666666666
	},
	{
		id: 1103,
		time: 1102,
		velocity: 12.4236111111111,
		power: -951.80605652303,
		road: 11583.3047222222,
		acceleration: -0.295833333333338
	},
	{
		id: 1104,
		time: 1103,
		velocity: 12.1127777777778,
		power: 154.402134972737,
		road: 11595.6209259259,
		acceleration: -0.199814814814811
	},
	{
		id: 1105,
		time: 1104,
		velocity: 12.1125,
		power: 2209.44886536777,
		road: 11607.8263888889,
		acceleration: -0.0216666666666683
	},
	{
		id: 1106,
		time: 1105,
		velocity: 12.3586111111111,
		power: 2169.27747242771,
		road: 11620.0087962963,
		acceleration: -0.0244444444444447
	},
	{
		id: 1107,
		time: 1106,
		velocity: 12.0394444444444,
		power: 1222.73587655128,
		road: 11632.1267592593,
		acceleration: -0.104444444444443
	},
	{
		id: 1108,
		time: 1107,
		velocity: 11.7991666666667,
		power: 530.118762115043,
		road: 11644.1114814815,
		acceleration: -0.162037037037038
	},
	{
		id: 1109,
		time: 1108,
		velocity: 11.8725,
		power: 1923.23483072655,
		road: 11655.9964814815,
		acceleration: -0.0374074074074073
	},
	{
		id: 1110,
		time: 1109,
		velocity: 11.9272222222222,
		power: 3007.48219363416,
		road: 11667.891712963,
		acceleration: 0.0578703703703702
	},
	{
		id: 1111,
		time: 1110,
		velocity: 11.9727777777778,
		power: 2814.18061190552,
		road: 11679.835462963,
		acceleration: 0.0391666666666666
	},
	{
		id: 1112,
		time: 1111,
		velocity: 11.99,
		power: 3128.03249083466,
		road: 11691.8312037037,
		acceleration: 0.0648148148148149
	},
	{
		id: 1113,
		time: 1112,
		velocity: 12.1216666666667,
		power: 3099.86011067828,
		road: 11703.8893981482,
		acceleration: 0.060092592592591
	},
	{
		id: 1114,
		time: 1113,
		velocity: 12.1530555555556,
		power: 3494.79335071539,
		road: 11716.0233796296,
		acceleration: 0.0914814814814804
	},
	{
		id: 1115,
		time: 1114,
		velocity: 12.2644444444444,
		power: 3458.94568467934,
		road: 11728.2456018519,
		acceleration: 0.0850000000000009
	},
	{
		id: 1116,
		time: 1115,
		velocity: 12.3766666666667,
		power: 4320.3121181433,
		road: 11740.5871296296,
		acceleration: 0.153611111111111
	},
	{
		id: 1117,
		time: 1116,
		velocity: 12.6138888888889,
		power: 4728.77349905682,
		road: 11753.0957407408,
		acceleration: 0.180555555555555
	},
	{
		id: 1118,
		time: 1117,
		velocity: 12.8061111111111,
		power: 3962.70870852681,
		road: 11765.7498611111,
		acceleration: 0.110462962962963
	},
	{
		id: 1119,
		time: 1118,
		velocity: 12.7080555555556,
		power: 2968.57601522103,
		road: 11778.4721296296,
		acceleration: 0.0258333333333329
	},
	{
		id: 1120,
		time: 1119,
		velocity: 12.6913888888889,
		power: 1210.39238564651,
		road: 11791.1483333333,
		acceleration: -0.117962962962959
	},
	{
		id: 1121,
		time: 1120,
		velocity: 12.4522222222222,
		power: 600.113666620386,
		road: 11803.6826388889,
		acceleration: -0.165833333333335
	},
	{
		id: 1122,
		time: 1121,
		velocity: 12.2105555555556,
		power: -37.1026266971279,
		road: 11816.025787037,
		acceleration: -0.216481481481482
	},
	{
		id: 1123,
		time: 1122,
		velocity: 12.0419444444444,
		power: -970.481975966162,
		road: 11828.1137037037,
		acceleration: -0.293981481481481
	},
	{
		id: 1124,
		time: 1123,
		velocity: 11.5702777777778,
		power: -983.966094696801,
		road: 11839.907962963,
		acceleration: -0.293333333333331
	},
	{
		id: 1125,
		time: 1124,
		velocity: 11.3305555555556,
		power: -1738.45484356057,
		road: 11851.3752777778,
		acceleration: -0.360555555555557
	},
	{
		id: 1126,
		time: 1125,
		velocity: 10.9602777777778,
		power: -1133.18963477682,
		road: 11862.5102777778,
		acceleration: -0.304074074074073
	},
	{
		id: 1127,
		time: 1126,
		velocity: 10.6580555555556,
		power: -1459.1371893894,
		road: 11873.325787037,
		acceleration: -0.334907407407407
	},
	{
		id: 1128,
		time: 1127,
		velocity: 10.3258333333333,
		power: -539.923429725633,
		road: 11883.8519907408,
		acceleration: -0.243703703703705
	},
	{
		id: 1129,
		time: 1128,
		velocity: 10.2291666666667,
		power: 79.0420512130246,
		road: 11894.1666666667,
		acceleration: -0.17935185185185
	},
	{
		id: 1130,
		time: 1129,
		velocity: 10.12,
		power: 1603.49815409159,
		road: 11904.3809259259,
		acceleration: -0.0214814814814819
	},
	{
		id: 1131,
		time: 1130,
		velocity: 10.2613888888889,
		power: 2434.55415236766,
		road: 11914.6160185185,
		acceleration: 0.063148148148148
	},
	{
		id: 1132,
		time: 1131,
		velocity: 10.4186111111111,
		power: 3244.31513280905,
		road: 11924.9535185185,
		acceleration: 0.141666666666666
	},
	{
		id: 1133,
		time: 1132,
		velocity: 10.545,
		power: 2957.54847601052,
		road: 11935.4156018519,
		acceleration: 0.1075
	},
	{
		id: 1134,
		time: 1133,
		velocity: 10.5838888888889,
		power: 1305.33916241851,
		road: 11945.902037037,
		acceleration: -0.0587962962962951
	},
	{
		id: 1135,
		time: 1134,
		velocity: 10.2422222222222,
		power: -89.2218795783997,
		road: 11956.2606018519,
		acceleration: -0.196944444444446
	},
	{
		id: 1136,
		time: 1135,
		velocity: 9.95416666666667,
		power: -1137.7592533224,
		road: 11966.3691203704,
		acceleration: -0.303148148148146
	},
	{
		id: 1137,
		time: 1136,
		velocity: 9.67444444444445,
		power: -853.088606190685,
		road: 11976.1895833333,
		acceleration: -0.272962962962964
	},
	{
		id: 1138,
		time: 1137,
		velocity: 9.42333333333333,
		power: 121.240844946445,
		road: 11985.7905092593,
		acceleration: -0.166111111111112
	},
	{
		id: 1139,
		time: 1138,
		velocity: 9.45583333333333,
		power: 1093.12829508086,
		road: 11995.2797685185,
		acceleration: -0.0572222222222223
	},
	{
		id: 1140,
		time: 1139,
		velocity: 9.50277777777778,
		power: 1177.84754358023,
		road: 12004.7171296296,
		acceleration: -0.0465740740740745
	},
	{
		id: 1141,
		time: 1140,
		velocity: 9.28361111111111,
		power: 175.809561709238,
		road: 12014.0528240741,
		acceleration: -0.156759259259259
	},
	{
		id: 1142,
		time: 1141,
		velocity: 8.98555555555556,
		power: -706.497394862479,
		road: 12023.1823148148,
		acceleration: -0.255648148148147
	},
	{
		id: 1143,
		time: 1142,
		velocity: 8.73583333333333,
		power: -1047.40104375556,
		road: 12032.0360648148,
		acceleration: -0.295833333333334
	},
	{
		id: 1144,
		time: 1143,
		velocity: 8.39611111111111,
		power: -657.216905302136,
		road: 12040.6172222222,
		acceleration: -0.24935185185185
	},
	{
		id: 1145,
		time: 1144,
		velocity: 8.2375,
		power: -101.061141430848,
		road: 12048.9839351852,
		acceleration: -0.179537037037038
	},
	{
		id: 1146,
		time: 1145,
		velocity: 8.19722222222222,
		power: -255.911664172199,
		road: 12057.1618981482,
		acceleration: -0.197962962962961
	},
	{
		id: 1147,
		time: 1146,
		velocity: 7.80222222222222,
		power: 80.8114511015454,
		road: 12065.1644444445,
		acceleration: -0.152870370370371
	},
	{
		id: 1148,
		time: 1147,
		velocity: 7.77888888888889,
		power: -186.891910104968,
		road: 12072.9970370371,
		acceleration: -0.187037037037038
	},
	{
		id: 1149,
		time: 1148,
		velocity: 7.63611111111111,
		power: 493.326512331322,
		road: 12080.6894444445,
		acceleration: -0.0933333333333337
	},
	{
		id: 1150,
		time: 1149,
		velocity: 7.52222222222222,
		power: 212.706797074258,
		road: 12088.2700462963,
		acceleration: -0.130277777777778
	},
	{
		id: 1151,
		time: 1150,
		velocity: 7.38805555555556,
		power: 98.290650448171,
		road: 12095.7131481482,
		acceleration: -0.144722222222223
	},
	{
		id: 1152,
		time: 1151,
		velocity: 7.20194444444444,
		power: -37.1722566077491,
		road: 12103.0025462963,
		acceleration: -0.162685185185184
	},
	{
		id: 1153,
		time: 1152,
		velocity: 7.03416666666667,
		power: -46.5736815948546,
		road: 12110.1291666667,
		acceleration: -0.162870370370371
	},
	{
		id: 1154,
		time: 1153,
		velocity: 6.89944444444444,
		power: -2.36922886146936,
		road: 12117.0968055556,
		acceleration: -0.155092592592593
	},
	{
		id: 1155,
		time: 1154,
		velocity: 6.73666666666667,
		power: -50.4561929051631,
		road: 12123.90625,
		acceleration: -0.161296296296296
	},
	{
		id: 1156,
		time: 1155,
		velocity: 6.55027777777778,
		power: 150.781148210383,
		road: 12130.5707407408,
		acceleration: -0.128611111111111
	},
	{
		id: 1157,
		time: 1156,
		velocity: 6.51361111111111,
		power: 408.925385277226,
		road: 12137.1279166667,
		acceleration: -0.0860185185185181
	},
	{
		id: 1158,
		time: 1157,
		velocity: 6.47861111111111,
		power: 857.968083877517,
		road: 12143.6357870371,
		acceleration: -0.0125925925925925
	},
	{
		id: 1159,
		time: 1158,
		velocity: 6.5125,
		power: 1954.92786583594,
		road: 12150.2176388889,
		acceleration: 0.160555555555555
	},
	{
		id: 1160,
		time: 1159,
		velocity: 6.99527777777778,
		power: 2081.3407266659,
		road: 12156.9654166667,
		acceleration: 0.171296296296297
	},
	{
		id: 1161,
		time: 1160,
		velocity: 6.9925,
		power: 1899.51017378049,
		road: 12163.8664351852,
		acceleration: 0.135185185185185
	},
	{
		id: 1162,
		time: 1161,
		velocity: 6.91805555555556,
		power: 557.647577881393,
		road: 12170.8000925926,
		acceleration: -0.069907407407408
	},
	{
		id: 1163,
		time: 1162,
		velocity: 6.78555555555556,
		power: 604.038572689892,
		road: 12177.6680555556,
		acceleration: -0.0614814814814801
	},
	{
		id: 1164,
		time: 1163,
		velocity: 6.80805555555556,
		power: 1133.62906885354,
		road: 12184.5154166667,
		acceleration: 0.0202777777777756
	},
	{
		id: 1165,
		time: 1164,
		velocity: 6.97888888888889,
		power: 1126.95872346473,
		road: 12191.3822222222,
		acceleration: 0.0186111111111122
	},
	{
		id: 1166,
		time: 1165,
		velocity: 6.84138888888889,
		power: 142.085764688396,
		road: 12198.1925462963,
		acceleration: -0.131574074074075
	},
	{
		id: 1167,
		time: 1166,
		velocity: 6.41333333333333,
		power: -1019.17977279594,
		road: 12204.7798148148,
		acceleration: -0.314537037037037
	},
	{
		id: 1168,
		time: 1167,
		velocity: 6.03527777777778,
		power: -981.217727344843,
		road: 12211.0527777778,
		acceleration: -0.314074074074074
	},
	{
		id: 1169,
		time: 1168,
		velocity: 5.89916666666667,
		power: -1166.54294058246,
		road: 12216.9917592593,
		acceleration: -0.353888888888889
	},
	{
		id: 1170,
		time: 1169,
		velocity: 5.35166666666667,
		power: -1122.55920416814,
		road: 12222.5755555556,
		acceleration: -0.356481481481482
	},
	{
		id: 1171,
		time: 1170,
		velocity: 4.96583333333333,
		power: -1529.10102574627,
		road: 12227.7544907408,
		acceleration: -0.45324074074074
	},
	{
		id: 1172,
		time: 1171,
		velocity: 4.53944444444444,
		power: -661.076209064806,
		road: 12232.5642592593,
		acceleration: -0.285092592592593
	},
	{
		id: 1173,
		time: 1172,
		velocity: 4.49638888888889,
		power: -1762.45720405677,
		road: 12236.9508333334,
		acceleration: -0.561296296296296
	},
	{
		id: 1174,
		time: 1173,
		velocity: 3.28194444444444,
		power: -2534.43700635494,
		road: 12240.6260185185,
		acceleration: -0.861481481481481
	},
	{
		id: 1175,
		time: 1174,
		velocity: 1.955,
		power: -3237.17312470242,
		road: 12243.1210648148,
		acceleration: -1.4987962962963
	},
	{
		id: 1176,
		time: 1175,
		velocity: 0,
		power: -1097.19713927104,
		road: 12244.3197222222,
		acceleration: -1.09398148148148
	},
	{
		id: 1177,
		time: 1176,
		velocity: 0,
		power: -161.792713157895,
		road: 12244.6455555556,
		acceleration: -0.651666666666667
	},
	{
		id: 1178,
		time: 1177,
		velocity: 0,
		power: 41.3342276255767,
		road: 12244.7647685185,
		acceleration: 0.238425925925926
	},
	{
		id: 1179,
		time: 1178,
		velocity: 0.715277777777778,
		power: 317.052660146874,
		road: 12245.2702777778,
		acceleration: 0.534166666666667
	},
	{
		id: 1180,
		time: 1179,
		velocity: 1.6025,
		power: 1786.66939547036,
		road: 12246.6576388889,
		acceleration: 1.22953703703704
	},
	{
		id: 1181,
		time: 1180,
		velocity: 3.68861111111111,
		power: 4164.78176904818,
		road: 12249.3954166667,
		acceleration: 1.4712962962963
	},
	{
		id: 1182,
		time: 1181,
		velocity: 5.12916666666667,
		power: 5328.61158861354,
		road: 12253.4862037037,
		acceleration: 1.23472222222222
	},
	{
		id: 1183,
		time: 1182,
		velocity: 5.30666666666667,
		power: 3141.5382224523,
		road: 12258.4563425926,
		acceleration: 0.523981481481481
	},
	{
		id: 1184,
		time: 1183,
		velocity: 5.26055555555556,
		power: 1498.56517473669,
		road: 12263.7653703704,
		acceleration: 0.153796296296296
	},
	{
		id: 1185,
		time: 1184,
		velocity: 5.59055555555556,
		power: 806.429183787856,
		road: 12269.1581018519,
		acceleration: 0.0136111111111124
	},
	{
		id: 1186,
		time: 1185,
		velocity: 5.3475,
		power: -147.087147475011,
		road: 12274.4714351852,
		acceleration: -0.172407407407409
	},
	{
		id: 1187,
		time: 1186,
		velocity: 4.74333333333333,
		power: -3866.28105753994,
		road: 12279.197962963,
		acceleration: -1.0012037037037
	},
	{
		id: 1188,
		time: 1187,
		velocity: 2.58694444444444,
		power: -4588.85706475534,
		road: 12282.6581018519,
		acceleration: -1.53157407407407
	},
	{
		id: 1189,
		time: 1188,
		velocity: 0.752777777777778,
		power: -2620.39295159045,
		road: 12284.5618981482,
		acceleration: -1.58111111111111
	},
	{
		id: 1190,
		time: 1189,
		velocity: 0,
		power: -474.783031039457,
		road: 12285.2439814815,
		acceleration: -0.862314814814815
	},
	{
		id: 1191,
		time: 1190,
		velocity: 0,
		power: -14.6667964587394,
		road: 12285.3694444445,
		acceleration: -0.250925925925926
	},
	{
		id: 1192,
		time: 1191,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1193,
		time: 1192,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1194,
		time: 1193,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1195,
		time: 1194,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1196,
		time: 1195,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1197,
		time: 1196,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1198,
		time: 1197,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1199,
		time: 1198,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1200,
		time: 1199,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1201,
		time: 1200,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1202,
		time: 1201,
		velocity: 0,
		power: 0,
		road: 12285.3694444445,
		acceleration: 0
	},
	{
		id: 1203,
		time: 1202,
		velocity: 0,
		power: 40.6213582832771,
		road: 12285.4874074074,
		acceleration: 0.235925925925926
	},
	{
		id: 1204,
		time: 1203,
		velocity: 0.707777777777778,
		power: 143.720205926264,
		road: 12285.8619907408,
		acceleration: 0.277314814814815
	},
	{
		id: 1205,
		time: 1204,
		velocity: 0.831944444444444,
		power: 210.365563421279,
		road: 12286.4885185185,
		acceleration: 0.226574074074074
	},
	{
		id: 1206,
		time: 1205,
		velocity: 0.679722222222222,
		power: 59.0098118843945,
		road: 12287.2077314815,
		acceleration: -0.0412037037037037
	},
	{
		id: 1207,
		time: 1206,
		velocity: 0.584166666666667,
		power: -79.4042624530383,
		road: 12287.7676851852,
		acceleration: -0.277314814814815
	},
	{
		id: 1208,
		time: 1207,
		velocity: 0,
		power: -28.8945470268251,
		road: 12288.0756944445,
		acceleration: -0.226574074074074
	},
	{
		id: 1209,
		time: 1208,
		velocity: 0,
		power: -6.19759839181287,
		road: 12288.1730555556,
		acceleration: -0.194722222222222
	},
	{
		id: 1210,
		time: 1209,
		velocity: 0,
		power: 0,
		road: 12288.1730555556,
		acceleration: 0
	},
	{
		id: 1211,
		time: 1210,
		velocity: 0,
		power: 0,
		road: 12288.1730555556,
		acceleration: 0
	},
	{
		id: 1212,
		time: 1211,
		velocity: 0,
		power: 0,
		road: 12288.1730555556,
		acceleration: 0
	},
	{
		id: 1213,
		time: 1212,
		velocity: 0,
		power: 0,
		road: 12288.1730555556,
		acceleration: 0
	},
	{
		id: 1214,
		time: 1213,
		velocity: 0,
		power: 0,
		road: 12288.1730555556,
		acceleration: 0
	},
	{
		id: 1215,
		time: 1214,
		velocity: 0,
		power: 0,
		road: 12288.1730555556,
		acceleration: 0
	},
	{
		id: 1216,
		time: 1215,
		velocity: 0,
		power: 207.063387845766,
		road: 12288.4732407408,
		acceleration: 0.60037037037037
	},
	{
		id: 1217,
		time: 1216,
		velocity: 1.80111111111111,
		power: 1537.09482861403,
		road: 12289.6807407408,
		acceleration: 1.21425925925926
	},
	{
		id: 1218,
		time: 1217,
		velocity: 3.64277777777778,
		power: 4342.01844047327,
		road: 12292.3023611111,
		acceleration: 1.61398148148148
	},
	{
		id: 1219,
		time: 1218,
		velocity: 4.84194444444444,
		power: 5369.23532500119,
		road: 12296.3593981482,
		acceleration: 1.25685185185185
	},
	{
		id: 1220,
		time: 1219,
		velocity: 5.57166666666667,
		power: 5452.20053080764,
		road: 12301.5286111111,
		acceleration: 0.967499999999999
	},
	{
		id: 1221,
		time: 1220,
		velocity: 6.54527777777778,
		power: 7033.56059835917,
		road: 12307.7058796297,
		acceleration: 1.04861111111111
	},
	{
		id: 1222,
		time: 1221,
		velocity: 7.98777777777778,
		power: 9687.93932500133,
		road: 12315.0243055556,
		acceleration: 1.2337037037037
	},
	{
		id: 1223,
		time: 1222,
		velocity: 9.27277777777778,
		power: 14863.9161314292,
		road: 12323.7669907408,
		acceleration: 1.61481481481482
	},
	{
		id: 1224,
		time: 1223,
		velocity: 11.3897222222222,
		power: 13547.5732621577,
		road: 12333.9240740741,
		acceleration: 1.21398148148148
	},
	{
		id: 1225,
		time: 1224,
		velocity: 11.6297222222222,
		power: 10698.2377893197,
		road: 12345.0916203704,
		acceleration: 0.806944444444445
	},
	{
		id: 1226,
		time: 1225,
		velocity: 11.6936111111111,
		power: 5545.06892301315,
		road: 12356.8083333334,
		acceleration: 0.291388888888889
	},
	{
		id: 1227,
		time: 1226,
		velocity: 12.2638888888889,
		power: 7507.71512129625,
		road: 12368.8915277778,
		acceleration: 0.441574074074072
	},
	{
		id: 1228,
		time: 1227,
		velocity: 12.9544444444444,
		power: 10777.7426365253,
		road: 12381.5334259259,
		acceleration: 0.675833333333333
	},
	{
		id: 1229,
		time: 1228,
		velocity: 13.7211111111111,
		power: 11578.5229510888,
		road: 12394.8547222222,
		acceleration: 0.682962962962963
	},
	{
		id: 1230,
		time: 1229,
		velocity: 14.3127777777778,
		power: 14078.8500412075,
		road: 12408.9219444445,
		acceleration: 0.808888888888889
	},
	{
		id: 1231,
		time: 1230,
		velocity: 15.3811111111111,
		power: 12408.5360886337,
		road: 12423.7074537037,
		acceleration: 0.627685185185184
	},
	{
		id: 1232,
		time: 1231,
		velocity: 15.6041666666667,
		power: 8937.8959291628,
		road: 12438.9834259259,
		acceleration: 0.353240740740743
	},
	{
		id: 1233,
		time: 1232,
		velocity: 15.3725,
		power: 3457.25610978437,
		road: 12454.4222685185,
		acceleration: -0.0275000000000016
	},
	{
		id: 1234,
		time: 1233,
		velocity: 15.2986111111111,
		power: 1673.96927847989,
		road: 12469.7742592593,
		acceleration: -0.146203703703703
	},
	{
		id: 1235,
		time: 1234,
		velocity: 15.1655555555556,
		power: -135.302439463848,
		road: 12484.9200925926,
		acceleration: -0.266111111111112
	},
	{
		id: 1236,
		time: 1235,
		velocity: 14.5741666666667,
		power: 924.48439087719,
		road: 12499.8388425926,
		acceleration: -0.188055555555554
	},
	{
		id: 1237,
		time: 1236,
		velocity: 14.7344444444444,
		power: 2557.96000387915,
		road: 12514.6287037037,
		acceleration: -0.0697222222222216
	},
	{
		id: 1238,
		time: 1237,
		velocity: 14.9563888888889,
		power: 5714.52039755457,
		road: 12529.4596296297,
		acceleration: 0.151851851851852
	},
	{
		id: 1239,
		time: 1238,
		velocity: 15.0297222222222,
		power: 5913.03932135202,
		road: 12544.4459259259,
		acceleration: 0.158888888888889
	},
	{
		id: 1240,
		time: 1239,
		velocity: 15.2111111111111,
		power: 6079.70494733466,
		road: 12559.5932870371,
		acceleration: 0.163240740740742
	},
	{
		id: 1241,
		time: 1240,
		velocity: 15.4461111111111,
		power: 6008.12084670937,
		road: 12574.8979166667,
		acceleration: 0.151296296296294
	},
	{
		id: 1242,
		time: 1241,
		velocity: 15.4836111111111,
		power: 6490.18121045363,
		road: 12590.3665277778,
		acceleration: 0.176666666666668
	},
	{
		id: 1243,
		time: 1242,
		velocity: 15.7411111111111,
		power: 5994.37401178362,
		road: 12605.9916203704,
		acceleration: 0.136296296296296
	},
	{
		id: 1244,
		time: 1243,
		velocity: 15.855,
		power: 8331.87856127674,
		road: 12621.8256944445,
		acceleration: 0.281666666666666
	},
	{
		id: 1245,
		time: 1244,
		velocity: 16.3286111111111,
		power: 8069.35273767535,
		road: 12637.9259259259,
		acceleration: 0.250648148148148
	},
	{
		id: 1246,
		time: 1245,
		velocity: 16.4930555555556,
		power: 7502.7082419275,
		road: 12654.2529166667,
		acceleration: 0.20287037037037
	},
	{
		id: 1247,
		time: 1246,
		velocity: 16.4636111111111,
		power: 5200.07073798027,
		road: 12670.7065740741,
		acceleration: 0.050462962962964
	},
	{
		id: 1248,
		time: 1247,
		velocity: 16.48,
		power: 4625.28109543474,
		road: 12687.1918518519,
		acceleration: 0.0127777777777744
	},
	{
		id: 1249,
		time: 1248,
		velocity: 16.5313888888889,
		power: 5034.59390436239,
		road: 12703.7024074074,
		acceleration: 0.0377777777777837
	},
	{
		id: 1250,
		time: 1249,
		velocity: 16.5769444444444,
		power: 4436.8548481652,
		road: 12720.2314814815,
		acceleration: -0.000740740740745593
	},
	{
		id: 1251,
		time: 1250,
		velocity: 16.4777777777778,
		power: 4406.64311748046,
		road: 12736.7588888889,
		acceleration: -0.00259259259259181
	},
	{
		id: 1252,
		time: 1251,
		velocity: 16.5236111111111,
		power: 5467.72778166367,
		road: 12753.3166666667,
		acceleration: 0.0633333333333326
	},
	{
		id: 1253,
		time: 1252,
		velocity: 16.7669444444444,
		power: 8144.54747951768,
		road: 12770.0187037037,
		acceleration: 0.22518518518519
	},
	{
		id: 1254,
		time: 1253,
		velocity: 17.1533333333333,
		power: 8642.66980310246,
		road: 12786.9555092593,
		acceleration: 0.244351851851849
	},
	{
		id: 1255,
		time: 1254,
		velocity: 17.2566666666667,
		power: 8250.04970982404,
		road: 12804.1189814815,
		acceleration: 0.208981481481484
	},
	{
		id: 1256,
		time: 1255,
		velocity: 17.3938888888889,
		power: 6615.87980143137,
		road: 12821.4383333334,
		acceleration: 0.102777777777774
	},
	{
		id: 1257,
		time: 1256,
		velocity: 17.4616666666667,
		power: 5727.26313422058,
		road: 12838.8321759259,
		acceleration: 0.0462037037037035
	},
	{
		id: 1258,
		time: 1257,
		velocity: 17.3952777777778,
		power: 3794.669977034,
		road: 12856.2143518519,
		acceleration: -0.069537037037037
	},
	{
		id: 1259,
		time: 1258,
		velocity: 17.1852777777778,
		power: 1842.86306584389,
		road: 12873.4699537037,
		acceleration: -0.183611111111109
	},
	{
		id: 1260,
		time: 1259,
		velocity: 16.9108333333333,
		power: 268.360673928484,
		road: 12890.4965740741,
		acceleration: -0.274351851851854
	},
	{
		id: 1261,
		time: 1260,
		velocity: 16.5722222222222,
		power: 298.807905222546,
		road: 12907.2525,
		acceleration: -0.267037037037039
	},
	{
		id: 1262,
		time: 1261,
		velocity: 16.3841666666667,
		power: -129.493070215193,
		road: 12923.7305555556,
		acceleration: -0.2887037037037
	},
	{
		id: 1263,
		time: 1262,
		velocity: 16.0447222222222,
		power: -769.651896787383,
		road: 12939.9019444445,
		acceleration: -0.32462962962963
	},
	{
		id: 1264,
		time: 1263,
		velocity: 15.5983333333333,
		power: -1552.18120276827,
		road: 12955.7254166667,
		acceleration: -0.371203703703705
	},
	{
		id: 1265,
		time: 1264,
		velocity: 15.2705555555556,
		power: -916.883213319139,
		road: 12971.2010648148,
		acceleration: -0.324444444444444
	},
	{
		id: 1266,
		time: 1265,
		velocity: 15.0713888888889,
		power: 850.138160289928,
		road: 12986.4147685185,
		acceleration: -0.199444444444445
	},
	{
		id: 1267,
		time: 1266,
		velocity: 15,
		power: 1281.90855835995,
		road: 13001.4459722222,
		acceleration: -0.165555555555555
	},
	{
		id: 1268,
		time: 1267,
		velocity: 14.7738888888889,
		power: 1654.0716677914,
		road: 13016.3264351852,
		acceleration: -0.135925925925926
	},
	{
		id: 1269,
		time: 1268,
		velocity: 14.6636111111111,
		power: -913.698893512807,
		road: 13030.9820370371,
		acceleration: -0.313796296296296
	},
	{
		id: 1270,
		time: 1269,
		velocity: 14.0586111111111,
		power: -1867.62795479133,
		road: 13045.2909722222,
		acceleration: -0.379537037037037
	},
	{
		id: 1271,
		time: 1270,
		velocity: 13.6352777777778,
		power: -5596.21732346962,
		road: 13059.0802777778,
		acceleration: -0.659722222222221
	},
	{
		id: 1272,
		time: 1271,
		velocity: 12.6844444444444,
		power: -9294.72810814072,
		road: 13072.0531481482,
		acceleration: -0.973148148148148
	},
	{
		id: 1273,
		time: 1272,
		velocity: 11.1391666666667,
		power: -17267.1203215109,
		road: 13083.6574074074,
		acceleration: -1.76407407407408
	},
	{
		id: 1274,
		time: 1273,
		velocity: 8.34305555555556,
		power: -18383.7140626287,
		road: 13093.2874537037,
		acceleration: -2.18435185185185
	},
	{
		id: 1275,
		time: 1274,
		velocity: 6.13138888888889,
		power: -12729.4733988438,
		road: 13100.8620833333,
		acceleration: -1.92648148148148
	},
	{
		id: 1276,
		time: 1275,
		velocity: 5.35972222222222,
		power: -5956.40696675991,
		road: 13106.8787962963,
		acceleration: -1.18935185185185
	},
	{
		id: 1277,
		time: 1276,
		velocity: 4.775,
		power: -2368.69447937428,
		road: 13111.9856944445,
		acceleration: -0.630277777777778
	},
	{
		id: 1278,
		time: 1277,
		velocity: 4.24055555555556,
		power: -2160.03973930365,
		road: 13116.4536111111,
		acceleration: -0.647685185185185
	},
	{
		id: 1279,
		time: 1278,
		velocity: 3.41666666666667,
		power: -1330.96437624788,
		road: 13120.3498148148,
		acceleration: -0.49574074074074
	},
	{
		id: 1280,
		time: 1279,
		velocity: 3.28777777777778,
		power: -835.273683170377,
		road: 13123.8037037037,
		acceleration: -0.388888888888889
	},
	{
		id: 1281,
		time: 1280,
		velocity: 3.07388888888889,
		power: -89.30093888334,
		road: 13126.9818055556,
		acceleration: -0.162685185185185
	},
	{
		id: 1282,
		time: 1281,
		velocity: 2.92861111111111,
		power: 96.2039820256815,
		road: 13130.0288888889,
		acceleration: -0.0993518518518526
	},
	{
		id: 1283,
		time: 1282,
		velocity: 2.98972222222222,
		power: 311.208114216817,
		road: 13133.015,
		acceleration: -0.0225925925925923
	},
	{
		id: 1284,
		time: 1283,
		velocity: 3.00611111111111,
		power: 714.103390964691,
		road: 13136.0475925926,
		acceleration: 0.115555555555556
	},
	{
		id: 1285,
		time: 1284,
		velocity: 3.27527777777778,
		power: 1090.87739307799,
		road: 13139.2507870371,
		acceleration: 0.225648148148148
	},
	{
		id: 1286,
		time: 1285,
		velocity: 3.66666666666667,
		power: 1103.41429987403,
		road: 13142.6698148148,
		acceleration: 0.206018518518519
	},
	{
		id: 1287,
		time: 1286,
		velocity: 3.62416666666667,
		power: 601.59569800056,
		road: 13146.2140277778,
		acceleration: 0.044351851851852
	},
	{
		id: 1288,
		time: 1287,
		velocity: 3.40833333333333,
		power: 256.457845140238,
		road: 13149.7513888889,
		acceleration: -0.0580555555555562
	},
	{
		id: 1289,
		time: 1288,
		velocity: 3.4925,
		power: 227.040647205547,
		road: 13153.2270370371,
		acceleration: -0.0653703703703701
	},
	{
		id: 1290,
		time: 1289,
		velocity: 3.42805555555556,
		power: 860.628454106126,
		road: 13156.7321759259,
		acceleration: 0.124351851851852
	},
	{
		id: 1291,
		time: 1290,
		velocity: 3.78138888888889,
		power: 885.987970500714,
		road: 13160.3606944445,
		acceleration: 0.122407407407408
	},
	{
		id: 1292,
		time: 1291,
		velocity: 3.85972222222222,
		power: 1310.60825703346,
		road: 13164.1641203704,
		acceleration: 0.227407407407408
	},
	{
		id: 1293,
		time: 1292,
		velocity: 4.11027777777778,
		power: 745.837499503389,
		road: 13168.1126388889,
		acceleration: 0.0627777777777778
	},
	{
		id: 1294,
		time: 1293,
		velocity: 3.96972222222222,
		power: 162.814633283155,
		road: 13172.0462962963,
		acceleration: -0.0925000000000002
	},
	{
		id: 1295,
		time: 1294,
		velocity: 3.58222222222222,
		power: -349.643236630617,
		road: 13175.8171759259,
		acceleration: -0.233055555555556
	},
	{
		id: 1296,
		time: 1295,
		velocity: 3.41111111111111,
		power: -342.25549773261,
		road: 13179.3533333334,
		acceleration: -0.236388888888889
	},
	{
		id: 1297,
		time: 1296,
		velocity: 3.26055555555556,
		power: 419.60061626279,
		road: 13182.7690277778,
		acceleration: -0.00453703703703745
	},
	{
		id: 1298,
		time: 1297,
		velocity: 3.56861111111111,
		power: 884.768642806771,
		road: 13186.2492592593,
		acceleration: 0.133611111111111
	},
	{
		id: 1299,
		time: 1298,
		velocity: 3.81194444444444,
		power: 1210.94783892273,
		road: 13189.9033796296,
		acceleration: 0.214166666666667
	},
	{
		id: 1300,
		time: 1299,
		velocity: 3.90305555555556,
		power: 877.313376228498,
		road: 13193.7179166667,
		acceleration: 0.106666666666667
	},
	{
		id: 1301,
		time: 1300,
		velocity: 3.88861111111111,
		power: 761.448969000834,
		road: 13197.6205555556,
		acceleration: 0.069537037037037
	},
	{
		id: 1302,
		time: 1301,
		velocity: 4.02055555555556,
		power: 664.216062051142,
		road: 13201.5782407408,
		acceleration: 0.0405555555555552
	},
	{
		id: 1303,
		time: 1302,
		velocity: 4.02472222222222,
		power: 682.173198640583,
		road: 13205.5778240741,
		acceleration: 0.0432407407407407
	},
	{
		id: 1304,
		time: 1303,
		velocity: 4.01833333333333,
		power: 728.966000807409,
		road: 13209.6255555556,
		acceleration: 0.0530555555555567
	},
	{
		id: 1305,
		time: 1304,
		velocity: 4.17972222222222,
		power: 755.767351091231,
		road: 13213.7283796296,
		acceleration: 0.0571296296296282
	},
	{
		id: 1306,
		time: 1305,
		velocity: 4.19611111111111,
		power: 784.256713227081,
		road: 13217.8904166667,
		acceleration: 0.0612962962962964
	},
	{
		id: 1307,
		time: 1306,
		velocity: 4.20222222222222,
		power: 410.393260704446,
		road: 13222.06625,
		acceleration: -0.0337037037037025
	},
	{
		id: 1308,
		time: 1307,
		velocity: 4.07861111111111,
		power: -531.338922555001,
		road: 13226.0874074074,
		acceleration: -0.275648148148149
	},
	{
		id: 1309,
		time: 1308,
		velocity: 3.36916666666667,
		power: -1113.9184263192,
		road: 13229.7427777778,
		acceleration: -0.455925925925925
	},
	{
		id: 1310,
		time: 1309,
		velocity: 2.83444444444444,
		power: -1042.4032325172,
		road: 13232.9313888889,
		acceleration: -0.477592592592593
	},
	{
		id: 1311,
		time: 1310,
		velocity: 2.64583333333333,
		power: -472.55638924578,
		road: 13235.7262037037,
		acceleration: -0.309999999999999
	},
	{
		id: 1312,
		time: 1311,
		velocity: 2.43916666666667,
		power: -153.369409559927,
		road: 13238.2687037037,
		acceleration: -0.19462962962963
	},
	{
		id: 1313,
		time: 1312,
		velocity: 2.25055555555556,
		power: 13.7232594473814,
		road: 13240.6516203704,
		acceleration: -0.124537037037037
	},
	{
		id: 1314,
		time: 1313,
		velocity: 2.27222222222222,
		power: -89.2057624626895,
		road: 13242.8861111111,
		acceleration: -0.172314814814815
	},
	{
		id: 1315,
		time: 1314,
		velocity: 1.92222222222222,
		power: -705.781192523649,
		road: 13244.7724537037,
		acceleration: -0.523981481481482
	},
	{
		id: 1316,
		time: 1315,
		velocity: 0.678611111111111,
		power: -509.354758954233,
		road: 13246.135462963,
		acceleration: -0.522685185185185
	},
	{
		id: 1317,
		time: 1316,
		velocity: 0.704166666666667,
		power: -216.968291665044,
		road: 13247.0476851852,
		acceleration: -0.378888888888889
	},
	{
		id: 1318,
		time: 1317,
		velocity: 0.785555555555555,
		power: 297.028104771881,
		road: 13247.8920833333,
		acceleration: 0.243240740740741
	},
	{
		id: 1319,
		time: 1318,
		velocity: 1.40833333333333,
		power: 865.410523812511,
		road: 13249.1552314815,
		acceleration: 0.594259259259259
	},
	{
		id: 1320,
		time: 1319,
		velocity: 2.48694444444444,
		power: 1666.74189333319,
		road: 13251.1020370371,
		acceleration: 0.773055555555556
	},
	{
		id: 1321,
		time: 1320,
		velocity: 3.10472222222222,
		power: 2129.62564172018,
		road: 13253.7875,
		acceleration: 0.704259259259258
	},
	{
		id: 1322,
		time: 1321,
		velocity: 3.52111111111111,
		power: 1833.2497302195,
		road: 13257.0540277778,
		acceleration: 0.457870370370371
	},
	{
		id: 1323,
		time: 1322,
		velocity: 3.86055555555556,
		power: 1661.38906473938,
		road: 13260.7206481482,
		acceleration: 0.342314814814815
	},
	{
		id: 1324,
		time: 1323,
		velocity: 4.13166666666667,
		power: 1948.35057144601,
		road: 13264.7450925926,
		acceleration: 0.373333333333334
	},
	{
		id: 1325,
		time: 1324,
		velocity: 4.64111111111111,
		power: 1221.61530890338,
		road: 13269.0371759259,
		acceleration: 0.161944444444445
	},
	{
		id: 1326,
		time: 1325,
		velocity: 4.34638888888889,
		power: 1199.87645570388,
		road: 13273.4830555556,
		acceleration: 0.145648148148148
	},
	{
		id: 1327,
		time: 1326,
		velocity: 4.56861111111111,
		power: 763.802348685782,
		road: 13278.0208796296,
		acceleration: 0.0382407407407408
	},
	{
		id: 1328,
		time: 1327,
		velocity: 4.75583333333333,
		power: 1542.69343644863,
		road: 13282.6822222222,
		acceleration: 0.208796296296295
	},
	{
		id: 1329,
		time: 1328,
		velocity: 4.97277777777778,
		power: 1704.55293623387,
		road: 13287.5614351852,
		acceleration: 0.226944444444444
	},
	{
		id: 1330,
		time: 1329,
		velocity: 5.24944444444444,
		power: 1929.73760968028,
		road: 13292.6813888889,
		acceleration: 0.254537037037038
	},
	{
		id: 1331,
		time: 1330,
		velocity: 5.51944444444444,
		power: 2135.66390182165,
		road: 13298.065462963,
		acceleration: 0.273703703703703
	},
	{
		id: 1332,
		time: 1331,
		velocity: 5.79388888888889,
		power: 1797.99610468064,
		road: 13303.6822222222,
		acceleration: 0.191666666666667
	},
	{
		id: 1333,
		time: 1332,
		velocity: 5.82444444444444,
		power: 1433.18826817604,
		road: 13309.4524074074,
		acceleration: 0.115185185185186
	},
	{
		id: 1334,
		time: 1333,
		velocity: 5.865,
		power: 878.836049295204,
		road: 13315.2861574074,
		acceleration: 0.0119444444444436
	},
	{
		id: 1335,
		time: 1334,
		velocity: 5.82972222222222,
		power: 632.969287029937,
		road: 13321.1098148148,
		acceleration: -0.0321296296296305
	},
	{
		id: 1336,
		time: 1335,
		velocity: 5.72805555555555,
		power: 324.543614665457,
		road: 13326.8739814815,
		acceleration: -0.0868518518518515
	},
	{
		id: 1337,
		time: 1336,
		velocity: 5.60444444444444,
		power: 174.366058059608,
		road: 13332.5381944445,
		acceleration: -0.113055555555555
	},
	{
		id: 1338,
		time: 1337,
		velocity: 5.49055555555556,
		power: -214.659635473216,
		road: 13338.0531481482,
		acceleration: -0.185462962962964
	},
	{
		id: 1339,
		time: 1338,
		velocity: 5.17166666666667,
		power: -211.286403022876,
		road: 13343.3828240741,
		acceleration: -0.185092592592593
	},
	{
		id: 1340,
		time: 1339,
		velocity: 5.04916666666667,
		power: -426.181339154411,
		road: 13348.5050925926,
		acceleration: -0.229722222222222
	},
	{
		id: 1341,
		time: 1340,
		velocity: 4.80138888888889,
		power: -407.051029844368,
		road: 13353.3982870371,
		acceleration: -0.228425925925926
	},
	{
		id: 1342,
		time: 1341,
		velocity: 4.48638888888889,
		power: -2708.16531719138,
		road: 13357.782962963,
		acceleration: -0.788611111111111
	},
	{
		id: 1343,
		time: 1342,
		velocity: 2.68333333333333,
		power: -3456.2297790871,
		road: 13361.1685648148,
		acceleration: -1.20953703703704
	},
	{
		id: 1344,
		time: 1343,
		velocity: 1.17277777777778,
		power: -2632.94735416188,
		road: 13363.2016666667,
		acceleration: -1.49546296296296
	},
	{
		id: 1345,
		time: 1344,
		velocity: 0,
		power: -608.887442760763,
		road: 13364.0398148148,
		acceleration: -0.894444444444444
	},
	{
		id: 1346,
		time: 1345,
		velocity: 0,
		power: -48.7744561078623,
		road: 13364.2352777778,
		acceleration: -0.390925925925926
	},
	{
		id: 1347,
		time: 1346,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1348,
		time: 1347,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1349,
		time: 1348,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1350,
		time: 1349,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1351,
		time: 1350,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1352,
		time: 1351,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1353,
		time: 1352,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1354,
		time: 1353,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1355,
		time: 1354,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1356,
		time: 1355,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1357,
		time: 1356,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1358,
		time: 1357,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1359,
		time: 1358,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1360,
		time: 1359,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1361,
		time: 1360,
		velocity: 0,
		power: 0,
		road: 13364.2352777778,
		acceleration: 0
	},
	{
		id: 1362,
		time: 1361,
		velocity: 0,
		power: 79.7265656251895,
		road: 13364.4109722222,
		acceleration: 0.351388888888889
	},
	{
		id: 1363,
		time: 1362,
		velocity: 1.05416666666667,
		power: 1207.71900938373,
		road: 13365.3656018519,
		acceleration: 1.20648148148148
	},
	{
		id: 1364,
		time: 1363,
		velocity: 3.61944444444444,
		power: 3834.59124458806,
		road: 13367.7174074074,
		acceleration: 1.58787037037037
	},
	{
		id: 1365,
		time: 1364,
		velocity: 4.76361111111111,
		power: 6378.51796381647,
		road: 13371.649212963,
		acceleration: 1.57212962962963
	},
	{
		id: 1366,
		time: 1365,
		velocity: 5.77055555555556,
		power: 4675.09486220269,
		road: 13376.7758333334,
		acceleration: 0.8175
	},
	{
		id: 1367,
		time: 1366,
		velocity: 6.07194444444445,
		power: 4273.04732115246,
		road: 13382.6223148148,
		acceleration: 0.622222222222223
	},
	{
		id: 1368,
		time: 1367,
		velocity: 6.63027777777778,
		power: 3450.74368999695,
		road: 13388.9897685185,
		acceleration: 0.419722222222222
	},
	{
		id: 1369,
		time: 1368,
		velocity: 7.02972222222222,
		power: 3668.56492029914,
		road: 13395.7746759259,
		acceleration: 0.415185185185185
	},
	{
		id: 1370,
		time: 1369,
		velocity: 7.3175,
		power: 4048.35033791792,
		road: 13402.9839814815,
		acceleration: 0.433611111111111
	},
	{
		id: 1371,
		time: 1370,
		velocity: 7.93111111111111,
		power: 4251.22666829784,
		road: 13410.6224074074,
		acceleration: 0.42462962962963
	},
	{
		id: 1372,
		time: 1371,
		velocity: 8.30361111111111,
		power: 5822.0658926129,
		road: 13418.7662962963,
		acceleration: 0.586296296296296
	},
	{
		id: 1373,
		time: 1372,
		velocity: 9.07638888888889,
		power: 5493.97901942444,
		road: 13427.4506018519,
		acceleration: 0.494537037037036
	},
	{
		id: 1374,
		time: 1373,
		velocity: 9.41472222222222,
		power: 4632.08198998055,
		road: 13436.5619907408,
		acceleration: 0.35962962962963
	},
	{
		id: 1375,
		time: 1374,
		velocity: 9.3825,
		power: 2119.41835125848,
		road: 13445.8843518519,
		acceleration: 0.0623148148148172
	},
	{
		id: 1376,
		time: 1375,
		velocity: 9.26333333333333,
		power: 608.066955324923,
		road: 13455.1841203704,
		acceleration: -0.107500000000002
	},
	{
		id: 1377,
		time: 1376,
		velocity: 9.09222222222222,
		power: -688.928374995197,
		road: 13464.3033333334,
		acceleration: -0.253611111111111
	},
	{
		id: 1378,
		time: 1377,
		velocity: 8.62166666666667,
		power: -824.988852826305,
		road: 13473.1610185185,
		acceleration: -0.269444444444444
	},
	{
		id: 1379,
		time: 1378,
		velocity: 8.455,
		power: 1698.01469589868,
		road: 13481.9007870371,
		acceleration: 0.0336111111111101
	},
	{
		id: 1380,
		time: 1379,
		velocity: 9.19305555555555,
		power: 3918.1646416099,
		road: 13490.8024537037,
		acceleration: 0.290185185185186
	},
	{
		id: 1381,
		time: 1380,
		velocity: 9.49222222222222,
		power: 5986.92384327194,
		road: 13500.099212963,
		acceleration: 0.5
	},
	{
		id: 1382,
		time: 1381,
		velocity: 9.955,
		power: 5475.65152206827,
		road: 13509.8501851852,
		acceleration: 0.408425925925926
	},
	{
		id: 1383,
		time: 1382,
		velocity: 10.4183333333333,
		power: 6423.86471378502,
		road: 13520.0430555556,
		acceleration: 0.475370370370371
	},
	{
		id: 1384,
		time: 1383,
		velocity: 10.9183333333333,
		power: 6497.28780764764,
		road: 13530.6977777778,
		acceleration: 0.448333333333334
	},
	{
		id: 1385,
		time: 1384,
		velocity: 11.3,
		power: 5872.14126223951,
		road: 13541.7569444445,
		acceleration: 0.360555555555555
	},
	{
		id: 1386,
		time: 1385,
		velocity: 11.5,
		power: 5231.72380682039,
		road: 13553.1372222222,
		acceleration: 0.281666666666666
	},
	{
		id: 1387,
		time: 1386,
		velocity: 11.7633333333333,
		power: 4533.62489960203,
		road: 13564.7610185185,
		acceleration: 0.205370370370369
	},
	{
		id: 1388,
		time: 1387,
		velocity: 11.9161111111111,
		power: 3535.56196177532,
		road: 13576.5419907408,
		acceleration: 0.108981481481484
	},
	{
		id: 1389,
		time: 1388,
		velocity: 11.8269444444444,
		power: 2751.98658512891,
		road: 13588.3958333334,
		acceleration: 0.0367592592592576
	},
	{
		id: 1390,
		time: 1389,
		velocity: 11.8736111111111,
		power: 3449.27191273337,
		road: 13600.3159722222,
		acceleration: 0.0958333333333314
	},
	{
		id: 1391,
		time: 1390,
		velocity: 12.2036111111111,
		power: 5197.86194022074,
		road: 13612.4044907408,
		acceleration: 0.240925925925927
	},
	{
		id: 1392,
		time: 1391,
		velocity: 12.5497222222222,
		power: 5855.93263994104,
		road: 13624.7552314815,
		acceleration: 0.28351851851852
	},
	{
		id: 1393,
		time: 1392,
		velocity: 12.7241666666667,
		power: 4695.70586798494,
		road: 13637.335,
		acceleration: 0.174537037037036
	},
	{
		id: 1394,
		time: 1393,
		velocity: 12.7272222222222,
		power: 1773.27525260836,
		road: 13649.9668981482,
		acceleration: -0.0702777777777772
	},
	{
		id: 1395,
		time: 1394,
		velocity: 12.3388888888889,
		power: -535.624007589664,
		road: 13662.4336111111,
		acceleration: -0.260092592592594
	},
	{
		id: 1396,
		time: 1395,
		velocity: 11.9438888888889,
		power: -1272.22453406981,
		road: 13674.61,
		acceleration: -0.320555555555554
	},
	{
		id: 1397,
		time: 1396,
		velocity: 11.7655555555556,
		power: -1051.43982287477,
		road: 13686.47625,
		acceleration: -0.299722222222222
	},
	{
		id: 1398,
		time: 1397,
		velocity: 11.4397222222222,
		power: -507.258491410505,
		road: 13698.0681018519,
		acceleration: -0.249074074074073
	},
	{
		id: 1399,
		time: 1398,
		velocity: 11.1966666666667,
		power: 1043.65638926262,
		road: 13709.4829166667,
		acceleration: -0.105
	},
	{
		id: 1400,
		time: 1399,
		velocity: 11.4505555555556,
		power: 4126.56345403348,
		road: 13720.9335185185,
		acceleration: 0.176574074074075
	},
	{
		id: 1401,
		time: 1400,
		velocity: 11.9694444444444,
		power: 10896.7036571198,
		road: 13732.8480555556,
		acceleration: 0.751296296296296
	},
	{
		id: 1402,
		time: 1401,
		velocity: 13.4505555555556,
		power: 15548.4795200307,
		road: 13745.6637962963,
		acceleration: 1.05111111111111
	},
	{
		id: 1403,
		time: 1402,
		velocity: 14.6038888888889,
		power: 16119.276669038,
		road: 13759.4975,
		acceleration: 0.984814814814813
	},
	{
		id: 1404,
		time: 1403,
		velocity: 14.9238888888889,
		power: 9827.00451488698,
		road: 13774.0535185185,
		acceleration: 0.459814814814814
	},
	{
		id: 1405,
		time: 1404,
		velocity: 14.83,
		power: 4315.45438508759,
		road: 13788.8663425926,
		acceleration: 0.0537962962962961
	},
	{
		id: 1406,
		time: 1405,
		velocity: 14.7652777777778,
		power: 2318.45289049231,
		road: 13803.6626388889,
		acceleration: -0.0868518518518524
	},
	{
		id: 1407,
		time: 1406,
		velocity: 14.6633333333333,
		power: 1326.30260355979,
		road: 13818.3383796297,
		acceleration: -0.154259259259257
	},
	{
		id: 1408,
		time: 1407,
		velocity: 14.3672222222222,
		power: 1362.79379667441,
		road: 13832.8629166667,
		acceleration: -0.148148148148147
	},
	{
		id: 1409,
		time: 1408,
		velocity: 14.3208333333333,
		power: 1289.93061324536,
		road: 13847.2383796297,
		acceleration: -0.150000000000002
	},
	{
		id: 1410,
		time: 1409,
		velocity: 14.2133333333333,
		power: 255.067890902409,
		road: 13861.4277777778,
		acceleration: -0.222129629629629
	},
	{
		id: 1411,
		time: 1410,
		velocity: 13.7008333333333,
		power: -1777.7780382652,
		road: 13875.3209722222,
		acceleration: -0.370277777777778
	},
	{
		id: 1412,
		time: 1411,
		velocity: 13.21,
		power: -2184.51243994582,
		road: 13888.8291666667,
		acceleration: -0.399722222222223
	},
	{
		id: 1413,
		time: 1412,
		velocity: 13.0141666666667,
		power: -5134.40463269992,
		road: 13901.8191203704,
		acceleration: -0.636759259259259
	},
	{
		id: 1414,
		time: 1413,
		velocity: 11.7905555555556,
		power: -6334.73832970309,
		road: 13914.1143055556,
		acceleration: -0.752777777777778
	},
	{
		id: 1415,
		time: 1414,
		velocity: 10.9516666666667,
		power: -9811.66243259973,
		road: 13925.4802314815,
		acceleration: -1.10574074074074
	},
	{
		id: 1416,
		time: 1415,
		velocity: 9.69694444444444,
		power: -7525.17244000248,
		road: 13935.8174537037,
		acceleration: -0.951666666666666
	},
	{
		id: 1417,
		time: 1416,
		velocity: 8.93555555555555,
		power: -6789.30245470831,
		road: 13945.2107870371,
		acceleration: -0.936111111111112
	},
	{
		id: 1418,
		time: 1417,
		velocity: 8.14333333333333,
		power: -4897.03422521937,
		road: 13953.7506018519,
		acceleration: -0.770925925925926
	},
	{
		id: 1419,
		time: 1418,
		velocity: 7.38416666666667,
		power: -3669.49763562112,
		road: 13961.5776388889,
		acceleration: -0.654629629629629
	},
	{
		id: 1420,
		time: 1419,
		velocity: 6.97166666666667,
		power: -5445.63394112472,
		road: 13968.5916203704,
		acceleration: -0.971481481481481
	},
	{
		id: 1421,
		time: 1420,
		velocity: 5.22888888888889,
		power: -5138.24406770675,
		road: 13974.5958333334,
		acceleration: -1.04805555555556
	},
	{
		id: 1422,
		time: 1421,
		velocity: 4.24,
		power: -5135.50914152165,
		road: 13979.4486111111,
		acceleration: -1.25481481481482
	},
	{
		id: 1423,
		time: 1422,
		velocity: 3.20722222222222,
		power: -2707.28260829355,
		road: 13983.2290740741,
		acceleration: -0.889814814814815
	},
	{
		id: 1424,
		time: 1423,
		velocity: 2.55944444444444,
		power: -1753.99351191585,
		road: 13986.1858796297,
		acceleration: -0.757499999999999
	},
	{
		id: 1425,
		time: 1424,
		velocity: 1.9675,
		power: -916.068264514174,
		road: 13988.4890740741,
		acceleration: -0.549722222222222
	},
	{
		id: 1426,
		time: 1425,
		velocity: 1.55805555555556,
		power: -634.619946329312,
		road: 13990.2643055556,
		acceleration: -0.506203703703704
	},
	{
		id: 1427,
		time: 1426,
		velocity: 1.04083333333333,
		power: -597.214542334519,
		road: 13991.4585185185,
		acceleration: -0.655833333333333
	},
	{
		id: 1428,
		time: 1427,
		velocity: 0,
		power: -225.137628679443,
		road: 13992.0651388889,
		acceleration: -0.519351851851852
	},
	{
		id: 1429,
		time: 1428,
		velocity: 0,
		power: -36.0590317251462,
		road: 13992.2386111111,
		acceleration: -0.346944444444444
	},
	{
		id: 1430,
		time: 1429,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1431,
		time: 1430,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1432,
		time: 1431,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1433,
		time: 1432,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1434,
		time: 1433,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1435,
		time: 1434,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1436,
		time: 1435,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1437,
		time: 1436,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1438,
		time: 1437,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1439,
		time: 1438,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1440,
		time: 1439,
		velocity: 0,
		power: 0,
		road: 13992.2386111111,
		acceleration: 0
	},
	{
		id: 1441,
		time: 1440,
		velocity: 0,
		power: 49.7694238420746,
		road: 13992.3718981482,
		acceleration: 0.266574074074074
	},
	{
		id: 1442,
		time: 1441,
		velocity: 0.799722222222222,
		power: 1047.74207634585,
		road: 13993.2234722222,
		acceleration: 1.17
	},
	{
		id: 1443,
		time: 1442,
		velocity: 3.51,
		power: 4669.17209029122,
		road: 13995.620925926,
		acceleration: 1.92175925925926
	},
	{
		id: 1444,
		time: 1443,
		velocity: 5.76527777777778,
		power: 7271.3976361697,
		road: 13999.8217592593,
		acceleration: 1.685
	},
	{
		id: 1445,
		time: 1444,
		velocity: 5.85472222222222,
		power: 5819.63562027942,
		road: 14005.3468981482,
		acceleration: 0.963611111111112
	},
	{
		id: 1446,
		time: 1445,
		velocity: 6.40083333333333,
		power: 4298.52756700031,
		road: 14011.6382870371,
		acceleration: 0.568888888888887
	},
	{
		id: 1447,
		time: 1446,
		velocity: 7.47194444444444,
		power: 12689.8604560857,
		road: 14019.0362962963,
		acceleration: 1.64435185185185
	},
	{
		id: 1448,
		time: 1447,
		velocity: 10.7877777777778,
		power: 21521.8903247959,
		road: 14028.3773611111,
		acceleration: 2.24175925925926
	},
	{
		id: 1449,
		time: 1448,
		velocity: 13.1261111111111,
		power: 27251.6599510155,
		road: 14039.9698611111,
		acceleration: 2.26111111111111
	},
	{
		id: 1450,
		time: 1449,
		velocity: 14.2552777777778,
		power: 20212.334889477,
		road: 14053.3685648148,
		acceleration: 1.3512962962963
	},
	{
		id: 1451,
		time: 1450,
		velocity: 14.8416666666667,
		power: 10898.806698406,
		road: 14067.7187037037,
		acceleration: 0.551574074074075
	},
	{
		id: 1452,
		time: 1451,
		velocity: 14.7808333333333,
		power: 8154.70496221643,
		road: 14082.5078240741,
		acceleration: 0.326388888888888
	},
	{
		id: 1453,
		time: 1452,
		velocity: 15.2344444444444,
		power: 5344.71270977387,
		road: 14097.519212963,
		acceleration: 0.118148148148149
	},
	{
		id: 1454,
		time: 1453,
		velocity: 15.1961111111111,
		power: 4940.9948072454,
		road: 14112.6326388889,
		acceleration: 0.0859259259259257
	},
	{
		id: 1455,
		time: 1454,
		velocity: 15.0386111111111,
		power: 1019.55786232554,
		road: 14127.6967592593,
		acceleration: -0.184537037037037
	},
	{
		id: 1456,
		time: 1455,
		velocity: 14.6808333333333,
		power: -3106.65611409378,
		road: 14142.4333333334,
		acceleration: -0.470555555555556
	},
	{
		id: 1457,
		time: 1456,
		velocity: 13.7844444444444,
		power: -5211.32177043479,
		road: 14156.6218518519,
		acceleration: -0.625555555555556
	},
	{
		id: 1458,
		time: 1457,
		velocity: 13.1619444444444,
		power: -5372.93325483927,
		road: 14170.1744444445,
		acceleration: -0.646296296296295
	},
	{
		id: 1459,
		time: 1458,
		velocity: 12.7419444444444,
		power: -4981.46690593089,
		road: 14183.0910648148,
		acceleration: -0.62564814814815
	},
	{
		id: 1460,
		time: 1459,
		velocity: 11.9075,
		power: -5618.70222073367,
		road: 14195.3485185185,
		acceleration: -0.692685185185184
	},
	{
		id: 1461,
		time: 1460,
		velocity: 11.0838888888889,
		power: -3929.40152523157,
		road: 14206.9806481482,
		acceleration: -0.557962962962963
	},
	{
		id: 1462,
		time: 1461,
		velocity: 11.0680555555556,
		power: -643.809147397721,
		road: 14218.2044907408,
		acceleration: -0.258611111111112
	},
	{
		id: 1463,
		time: 1462,
		velocity: 11.1316666666667,
		power: 2700.81487662406,
		road: 14229.3277777778,
		acceleration: 0.0574999999999974
	},
	{
		id: 1464,
		time: 1463,
		velocity: 11.2563888888889,
		power: 2324.3935061944,
		road: 14240.4901851852,
		acceleration: 0.0207407407407416
	},
	{
		id: 1465,
		time: 1464,
		velocity: 11.1302777777778,
		power: 5528.8726592881,
		road: 14251.8189814815,
		acceleration: 0.31203703703704
	},
	{
		id: 1466,
		time: 1465,
		velocity: 12.0677777777778,
		power: 8640.2927059335,
		road: 14263.5859722222,
		acceleration: 0.56435185185185
	},
	{
		id: 1467,
		time: 1466,
		velocity: 12.9494444444444,
		power: 10291.6964904228,
		road: 14275.9639351852,
		acceleration: 0.657592592592593
	},
	{
		id: 1468,
		time: 1467,
		velocity: 13.1030555555556,
		power: 9768.81899808016,
		road: 14288.9533796297,
		acceleration: 0.565370370370371
	},
	{
		id: 1469,
		time: 1468,
		velocity: 13.7638888888889,
		power: 5478.91675350947,
		road: 14302.3259722222,
		acceleration: 0.200925925925924
	},
	{
		id: 1470,
		time: 1469,
		velocity: 13.5522222222222,
		power: -373.117984788786,
		road: 14315.670462963,
		acceleration: -0.257129629629629
	},
	{
		id: 1471,
		time: 1470,
		velocity: 12.3316666666667,
		power: -4952.63946264309,
		road: 14328.5746296297,
		acceleration: -0.623518518518518
	},
	{
		id: 1472,
		time: 1471,
		velocity: 11.8933333333333,
		power: -3830.42326757561,
		road: 14340.8975925926,
		acceleration: -0.53888888888889
	},
	{
		id: 1473,
		time: 1472,
		velocity: 11.9355555555556,
		power: 849.10423173751,
		road: 14352.8840277778,
		acceleration: -0.134166666666665
	},
	{
		id: 1474,
		time: 1473,
		velocity: 11.9291666666667,
		power: -382.420287652609,
		road: 14364.683425926,
		acceleration: -0.239907407407408
	},
	{
		id: 1475,
		time: 1474,
		velocity: 11.1736111111111,
		power: -9546.83622953663,
		road: 14375.8143518519,
		acceleration: -1.09703703703704
	},
	{
		id: 1476,
		time: 1475,
		velocity: 8.64444444444444,
		power: -12418.1083178848,
		road: 14385.6425462963,
		acceleration: -1.50842592592593
	},
	{
		id: 1477,
		time: 1476,
		velocity: 7.40388888888889,
		power: -10818.8324016954,
		road: 14393.9490740741,
		acceleration: -1.53490740740741
	},
	{
		id: 1478,
		time: 1477,
		velocity: 6.56888888888889,
		power: -4456.42795647203,
		road: 14401.0816666667,
		acceleration: -0.812962962962963
	},
	{
		id: 1479,
		time: 1478,
		velocity: 6.20555555555556,
		power: -1577.28920167879,
		road: 14407.6049537037,
		acceleration: -0.405648148148148
	},
	{
		id: 1480,
		time: 1479,
		velocity: 6.18694444444444,
		power: 374.869623989214,
		road: 14413.8820370371,
		acceleration: -0.0867592592592601
	},
	{
		id: 1481,
		time: 1480,
		velocity: 6.30861111111111,
		power: -275.064946018494,
		road: 14420.0178703704,
		acceleration: -0.195740740740741
	},
	{
		id: 1482,
		time: 1481,
		velocity: 5.61833333333333,
		power: -1946.01155092821,
		road: 14425.8058333334,
		acceleration: -0.5
	},
	{
		id: 1483,
		time: 1482,
		velocity: 4.68694444444444,
		power: -2290.6570788114,
		road: 14431.0421759259,
		acceleration: -0.603240740740739
	},
	{
		id: 1484,
		time: 1483,
		velocity: 4.49888888888889,
		power: -1178.24676041694,
		road: 14435.7758796297,
		acceleration: -0.402037037037037
	},
	{
		id: 1485,
		time: 1484,
		velocity: 4.41222222222222,
		power: -81.3501398382627,
		road: 14440.2296759259,
		acceleration: -0.157777777777778
	},
	{
		id: 1486,
		time: 1485,
		velocity: 4.21361111111111,
		power: 716.379845554029,
		road: 14444.6213425926,
		acceleration: 0.0335185185185187
	},
	{
		id: 1487,
		time: 1486,
		velocity: 4.59944444444444,
		power: 2194.25040909413,
		road: 14449.2117592593,
		acceleration: 0.363981481481481
	},
	{
		id: 1488,
		time: 1487,
		velocity: 5.50416666666667,
		power: 3801.26993104089,
		road: 14454.3058333334,
		acceleration: 0.643333333333334
	},
	{
		id: 1489,
		time: 1488,
		velocity: 6.14361111111111,
		power: 2881.60364462711,
		road: 14459.9190740741,
		acceleration: 0.395
	},
	{
		id: 1490,
		time: 1489,
		velocity: 5.78444444444444,
		power: 1567.36387598946,
		road: 14465.7966666667,
		acceleration: 0.133703703703705
	},
	{
		id: 1491,
		time: 1490,
		velocity: 5.90527777777778,
		power: 2226.97271730075,
		road: 14471.8602314815,
		acceleration: 0.23824074074074
	},
	{
		id: 1492,
		time: 1491,
		velocity: 6.85833333333333,
		power: 3184.95444869035,
		road: 14478.2306944445,
		acceleration: 0.375555555555555
	},
	{
		id: 1493,
		time: 1492,
		velocity: 6.91111111111111,
		power: 3102.14123333619,
		road: 14484.9550462963,
		acceleration: 0.332222222222223
	},
	{
		id: 1494,
		time: 1493,
		velocity: 6.90194444444444,
		power: 1693.29247782246,
		road: 14491.8964814815,
		acceleration: 0.101944444444444
	},
	{
		id: 1495,
		time: 1494,
		velocity: 7.16416666666667,
		power: 1621.65364478841,
		road: 14498.9324074074,
		acceleration: 0.0870370370370379
	},
	{
		id: 1496,
		time: 1495,
		velocity: 7.17222222222222,
		power: 1795.36276277472,
		road: 14506.0661111111,
		acceleration: 0.108518518518519
	},
	{
		id: 1497,
		time: 1496,
		velocity: 7.2275,
		power: 2021.63746565235,
		road: 14513.3219907408,
		acceleration: 0.135833333333333
	},
	{
		id: 1498,
		time: 1497,
		velocity: 7.57166666666667,
		power: 3032.35353892649,
		road: 14520.7800925926,
		acceleration: 0.26861111111111
	},
	{
		id: 1499,
		time: 1498,
		velocity: 7.97805555555556,
		power: 2722.03088463617,
		road: 14528.4779166667,
		acceleration: 0.210833333333333
	},
	{
		id: 1500,
		time: 1499,
		velocity: 7.86,
		power: 3484.50799182638,
		road: 14536.4298611111,
		acceleration: 0.297407407407407
	},
	{
		id: 1501,
		time: 1500,
		velocity: 8.46388888888889,
		power: 2205.72519701832,
		road: 14544.5900462963,
		acceleration: 0.119074074074074
	},
	{
		id: 1502,
		time: 1501,
		velocity: 8.33527777777778,
		power: 1984.78019955307,
		road: 14552.8530092593,
		acceleration: 0.0864814814814814
	},
	{
		id: 1503,
		time: 1502,
		velocity: 8.11944444444444,
		power: -459.613056269073,
		road: 14561.0471296296,
		acceleration: -0.224166666666665
	},
	{
		id: 1504,
		time: 1503,
		velocity: 7.79138888888889,
		power: -395.702905271397,
		road: 14569.0214814815,
		acceleration: -0.215370370370372
	},
	{
		id: 1505,
		time: 1504,
		velocity: 7.68916666666667,
		power: -400.461562983163,
		road: 14576.7803703704,
		acceleration: -0.215555555555556
	},
	{
		id: 1506,
		time: 1505,
		velocity: 7.47277777777778,
		power: 605.968671814544,
		road: 14584.3932870371,
		acceleration: -0.0763888888888875
	},
	{
		id: 1507,
		time: 1506,
		velocity: 7.56222222222222,
		power: 2688.18302588141,
		road: 14592.0716666667,
		acceleration: 0.207314814814815
	},
	{
		id: 1508,
		time: 1507,
		velocity: 8.31111111111111,
		power: 5236.51685126441,
		road: 14600.1138425926,
		acceleration: 0.520277777777777
	},
	{
		id: 1509,
		time: 1508,
		velocity: 9.03361111111111,
		power: 10897.8663934313,
		road: 14608.9759722222,
		acceleration: 1.11962962962963
	},
	{
		id: 1510,
		time: 1509,
		velocity: 10.9211111111111,
		power: 10953.3532045746,
		road: 14618.8864814815,
		acceleration: 0.97712962962963
	},
	{
		id: 1511,
		time: 1510,
		velocity: 11.2425,
		power: 9228.42249030867,
		road: 14629.6393981482,
		acceleration: 0.707685185185184
	},
	{
		id: 1512,
		time: 1511,
		velocity: 11.1566666666667,
		power: 1543.46703592018,
		road: 14640.7208796296,
		acceleration: -0.0505555555555546
	},
	{
		id: 1513,
		time: 1512,
		velocity: 10.7694444444444,
		power: -266.004054993666,
		road: 14651.6668518519,
		acceleration: -0.220462962962962
	},
	{
		id: 1514,
		time: 1513,
		velocity: 10.5811111111111,
		power: 1750.58975827028,
		road: 14662.4906944445,
		acceleration: -0.0237962962962968
	},
	{
		id: 1515,
		time: 1514,
		velocity: 11.0852777777778,
		power: 3697.42923753845,
		road: 14673.3835185185,
		acceleration: 0.161759259259259
	},
	{
		id: 1516,
		time: 1515,
		velocity: 11.2547222222222,
		power: 4402.10993679281,
		road: 14684.4671759259,
		acceleration: 0.219907407407407
	},
	{
		id: 1517,
		time: 1516,
		velocity: 11.2408333333333,
		power: 2614.42188551268,
		road: 14695.6838425926,
		acceleration: 0.0461111111111112
	},
	{
		id: 1518,
		time: 1517,
		velocity: 11.2236111111111,
		power: 2571.85290000541,
		road: 14706.9438888889,
		acceleration: 0.0406481481481489
	},
	{
		id: 1519,
		time: 1518,
		velocity: 11.3766666666667,
		power: 3228.73811465687,
		road: 14718.2737962963,
		acceleration: 0.0990740740740748
	},
	{
		id: 1520,
		time: 1519,
		velocity: 11.5380555555556,
		power: 3748.33416176162,
		road: 14729.724212963,
		acceleration: 0.141944444444443
	},
	{
		id: 1521,
		time: 1520,
		velocity: 11.6494444444444,
		power: 3692.17553381987,
		road: 14741.3111111111,
		acceleration: 0.131018518518522
	},
	{
		id: 1522,
		time: 1521,
		velocity: 11.7697222222222,
		power: 3255.67208727214,
		road: 14753.0071759259,
		acceleration: 0.0873148148148122
	},
	{
		id: 1523,
		time: 1522,
		velocity: 11.8,
		power: 2148.37321457832,
		road: 14764.7403703704,
		acceleration: -0.0130555555555549
	},
	{
		id: 1524,
		time: 1523,
		velocity: 11.6102777777778,
		power: 1023.89049888086,
		road: 14776.4109259259,
		acceleration: -0.112222222222224
	},
	{
		id: 1525,
		time: 1524,
		velocity: 11.4330555555556,
		power: 76.0890257882631,
		road: 14787.9277314815,
		acceleration: -0.195277777777775
	},
	{
		id: 1526,
		time: 1525,
		velocity: 11.2141666666667,
		power: 712.442541552622,
		road: 14799.2797685185,
		acceleration: -0.13425925925926
	},
	{
		id: 1527,
		time: 1526,
		velocity: 11.2075,
		power: -32.7440030911808,
		road: 14810.4641666667,
		acceleration: -0.20101851851852
	},
	{
		id: 1528,
		time: 1527,
		velocity: 10.83,
		power: 527.168634363611,
		road: 14821.4752777778,
		acceleration: -0.145555555555555
	},
	{
		id: 1529,
		time: 1528,
		velocity: 10.7775,
		power: 659.281215959433,
		road: 14832.3483796296,
		acceleration: -0.130462962962964
	},
	{
		id: 1530,
		time: 1529,
		velocity: 10.8161111111111,
		power: 1860.49196398171,
		road: 14843.15,
		acceleration: -0.0124999999999993
	},
	{
		id: 1531,
		time: 1530,
		velocity: 10.7925,
		power: 2017.24258437397,
		road: 14853.9468055556,
		acceleration: 0.00287037037037052
	},
	{
		id: 1532,
		time: 1531,
		velocity: 10.7861111111111,
		power: 2135.29720123458,
		road: 14864.7520833334,
		acceleration: 0.0140740740740775
	},
	{
		id: 1533,
		time: 1532,
		velocity: 10.8583333333333,
		power: 2784.80550088291,
		road: 14875.6021296296,
		acceleration: 0.0754629629629608
	},
	{
		id: 1534,
		time: 1533,
		velocity: 11.0188888888889,
		power: 3380.17812394447,
		road: 14886.5542592593,
		acceleration: 0.128703703703705
	},
	{
		id: 1535,
		time: 1534,
		velocity: 11.1722222222222,
		power: 4175.41996759404,
		road: 14897.669212963,
		acceleration: 0.196944444444442
	},
	{
		id: 1536,
		time: 1535,
		velocity: 11.4491666666667,
		power: 4744.00628169994,
		road: 14909.0022222222,
		acceleration: 0.239166666666668
	},
	{
		id: 1537,
		time: 1536,
		velocity: 11.7363888888889,
		power: 4729.08507508874,
		road: 14920.5677777778,
		acceleration: 0.225925925925925
	},
	{
		id: 1538,
		time: 1537,
		velocity: 11.85,
		power: 4297.33741346718,
		road: 14932.335,
		acceleration: 0.177407407407408
	},
	{
		id: 1539,
		time: 1538,
		velocity: 11.9813888888889,
		power: 2488.16977501964,
		road: 14944.1975,
		acceleration: 0.0131481481481472
	},
	{
		id: 1540,
		time: 1539,
		velocity: 11.7758333333333,
		power: 1734.20913365211,
		road: 14956.0400925926,
		acceleration: -0.0529629629629618
	},
	{
		id: 1541,
		time: 1540,
		velocity: 11.6911111111111,
		power: 1069.1945141083,
		road: 14967.8011574074,
		acceleration: -0.110092592592592
	},
	{
		id: 1542,
		time: 1541,
		velocity: 11.6511111111111,
		power: 1443.87866067054,
		road: 14979.4699537037,
		acceleration: -0.0744444444444454
	},
	{
		id: 1543,
		time: 1542,
		velocity: 11.5525,
		power: 1448.67278874053,
		road: 14991.0654166667,
		acceleration: -0.0722222222222211
	},
	{
		id: 1544,
		time: 1543,
		velocity: 11.4744444444444,
		power: 1789.16629661353,
		road: 15002.6048148148,
		acceleration: -0.0399074074074086
	},
	{
		id: 1545,
		time: 1544,
		velocity: 11.5313888888889,
		power: 1764.89970504469,
		road: 15014.10375,
		acceleration: -0.0410185185185181
	},
	{
		id: 1546,
		time: 1545,
		velocity: 11.4294444444444,
		power: 1525.33780577209,
		road: 15025.5513888889,
		acceleration: -0.0615740740740751
	},
	{
		id: 1547,
		time: 1546,
		velocity: 11.2897222222222,
		power: 1685.70866149102,
		road: 15036.9455092593,
		acceleration: -0.0454629629629633
	},
	{
		id: 1548,
		time: 1547,
		velocity: 11.395,
		power: 2383.2938700614,
		road: 15048.3264814815,
		acceleration: 0.0191666666666688
	},
	{
		id: 1549,
		time: 1548,
		velocity: 11.4869444444444,
		power: 2849.06043487971,
		road: 15059.7473611111,
		acceleration: 0.0606481481481467
	},
	{
		id: 1550,
		time: 1549,
		velocity: 11.4716666666667,
		power: 2433.98470193153,
		road: 15071.2091666667,
		acceleration: 0.0212037037037049
	},
	{
		id: 1551,
		time: 1550,
		velocity: 11.4586111111111,
		power: 2309.70101358599,
		road: 15082.68625,
		acceleration: 0.00935185185185006
	},
	{
		id: 1552,
		time: 1551,
		velocity: 11.515,
		power: 2830.08589466664,
		road: 15094.1958796296,
		acceleration: 0.0557407407407418
	},
	{
		id: 1553,
		time: 1552,
		velocity: 11.6388888888889,
		power: 2629.64590088413,
		road: 15105.7513425926,
		acceleration: 0.0359259259259268
	},
	{
		id: 1554,
		time: 1553,
		velocity: 11.5663888888889,
		power: 3240.85164810934,
		road: 15117.3692592593,
		acceleration: 0.0889814814814809
	},
	{
		id: 1555,
		time: 1554,
		velocity: 11.7819444444444,
		power: 2955.81552902648,
		road: 15129.0619444445,
		acceleration: 0.0605555555555544
	},
	{
		id: 1556,
		time: 1555,
		velocity: 11.8205555555556,
		power: 3642.24010429497,
		road: 15140.8441203704,
		acceleration: 0.118425925925926
	},
	{
		id: 1557,
		time: 1556,
		velocity: 11.9216666666667,
		power: 4019.21585389874,
		road: 15152.7586111111,
		acceleration: 0.146203703703705
	},
	{
		id: 1558,
		time: 1557,
		velocity: 12.2205555555556,
		power: 5211.59462555921,
		road: 15164.8667592593,
		acceleration: 0.24111111111111
	},
	{
		id: 1559,
		time: 1558,
		velocity: 12.5438888888889,
		power: 6348.72939478486,
		road: 15177.2569907408,
		acceleration: 0.323055555555555
	},
	{
		id: 1560,
		time: 1559,
		velocity: 12.8908333333333,
		power: 6889.05588330098,
		road: 15189.9830092593,
		acceleration: 0.348518518518517
	},
	{
		id: 1561,
		time: 1560,
		velocity: 13.2661111111111,
		power: 6617.42336854789,
		road: 15203.0370833334,
		acceleration: 0.307592592592595
	},
	{
		id: 1562,
		time: 1561,
		velocity: 13.4666666666667,
		power: 5614.81494250727,
		road: 15216.3521296296,
		acceleration: 0.214351851851852
	},
	{
		id: 1563,
		time: 1562,
		velocity: 13.5338888888889,
		power: 3759.74851793354,
		road: 15229.8059722222,
		acceleration: 0.0632407407407403
	},
	{
		id: 1564,
		time: 1563,
		velocity: 13.4558333333333,
		power: 1712.54725210769,
		road: 15243.2435648148,
		acceleration: -0.0957407407407391
	},
	{
		id: 1565,
		time: 1564,
		velocity: 13.1794444444444,
		power: 50.3622700387476,
		road: 15256.5218518519,
		acceleration: -0.222870370370373
	},
	{
		id: 1566,
		time: 1565,
		velocity: 12.8652777777778,
		power: -643.579418981314,
		road: 15269.55125,
		acceleration: -0.274907407407408
	},
	{
		id: 1567,
		time: 1566,
		velocity: 12.6311111111111,
		power: -338.420992107161,
		road: 15282.3196296297,
		acceleration: -0.247129629629628
	},
	{
		id: 1568,
		time: 1567,
		velocity: 12.4380555555556,
		power: 60.6026160876517,
		road: 15294.8589351852,
		acceleration: -0.21101851851852
	},
	{
		id: 1569,
		time: 1568,
		velocity: 12.2322222222222,
		power: -175.789976003772,
		road: 15307.17875,
		acceleration: -0.227962962962962
	},
	{
		id: 1570,
		time: 1569,
		velocity: 11.9472222222222,
		power: -1052.80165460878,
		road: 15319.2341203704,
		acceleration: -0.300925925925926
	},
	{
		id: 1571,
		time: 1570,
		velocity: 11.5352777777778,
		power: -1071.66460470247,
		road: 15330.9885648148,
		acceleration: -0.300925925925926
	},
	{
		id: 1572,
		time: 1571,
		velocity: 11.3294444444444,
		power: -1627.01216695081,
		road: 15342.4173611111,
		acceleration: -0.350370370370371
	},
	{
		id: 1573,
		time: 1572,
		velocity: 10.8961111111111,
		power: -1520.19706522959,
		road: 15353.5006944445,
		acceleration: -0.340555555555557
	},
	{
		id: 1574,
		time: 1573,
		velocity: 10.5136111111111,
		power: -1809.40117003728,
		road: 15364.2291203704,
		acceleration: -0.369259259259257
	},
	{
		id: 1575,
		time: 1574,
		velocity: 10.2216666666667,
		power: -1095.1626932681,
		road: 15374.6234722222,
		acceleration: -0.298888888888888
	},
	{
		id: 1576,
		time: 1575,
		velocity: 9.99944444444444,
		power: -184.877790484589,
		road: 15384.7661111111,
		acceleration: -0.204537037037039
	},
	{
		id: 1577,
		time: 1576,
		velocity: 9.9,
		power: -278.787574404378,
		road: 15394.7002314815,
		acceleration: -0.2125
	},
	{
		id: 1578,
		time: 1577,
		velocity: 9.58416666666667,
		power: 657.238540879068,
		road: 15404.4727777778,
		acceleration: -0.110648148148149
	},
	{
		id: 1579,
		time: 1578,
		velocity: 9.6675,
		power: 768.525613041149,
		road: 15414.1416666667,
		acceleration: -0.0966666666666658
	},
	{
		id: 1580,
		time: 1579,
		velocity: 9.61,
		power: 2416.28870532176,
		road: 15423.8034722222,
		acceleration: 0.0824999999999978
	},
	{
		id: 1581,
		time: 1580,
		velocity: 9.83166666666667,
		power: 2770.9816903984,
		road: 15433.5649537037,
		acceleration: 0.116851851851854
	},
	{
		id: 1582,
		time: 1581,
		velocity: 10.0180555555556,
		power: 3380.41153587071,
		road: 15443.4725462963,
		acceleration: 0.175370370370372
	},
	{
		id: 1583,
		time: 1582,
		velocity: 10.1361111111111,
		power: 3081.48728886711,
		road: 15453.53625,
		acceleration: 0.136851851851851
	},
	{
		id: 1584,
		time: 1583,
		velocity: 10.2422222222222,
		power: 2976.21931197283,
		road: 15463.7286111111,
		acceleration: 0.120462962962963
	},
	{
		id: 1585,
		time: 1584,
		velocity: 10.3794444444444,
		power: 3246.63552410507,
		road: 15474.0524537037,
		acceleration: 0.1425
	},
	{
		id: 1586,
		time: 1585,
		velocity: 10.5636111111111,
		power: 3518.1307883811,
		road: 15484.5290740741,
		acceleration: 0.163055555555557
	},
	{
		id: 1587,
		time: 1586,
		velocity: 10.7313888888889,
		power: 4400.66947035486,
		road: 15495.2075462963,
		acceleration: 0.240648148148148
	},
	{
		id: 1588,
		time: 1587,
		velocity: 11.1013888888889,
		power: 4796.38442460579,
		road: 15506.1390740741,
		acceleration: 0.265462962962962
	},
	{
		id: 1589,
		time: 1588,
		velocity: 11.36,
		power: 4084.8755724849,
		road: 15517.2965277778,
		acceleration: 0.18638888888889
	},
	{
		id: 1590,
		time: 1589,
		velocity: 11.2905555555556,
		power: 3094.10606865252,
		road: 15528.5911574074,
		acceleration: 0.0879629629629619
	},
	{
		id: 1591,
		time: 1590,
		velocity: 11.3652777777778,
		power: 2644.98350531443,
		road: 15539.9517592593,
		acceleration: 0.043981481481481
	},
	{
		id: 1592,
		time: 1591,
		velocity: 11.4919444444444,
		power: 2185.01218334737,
		road: 15551.3347685185,
		acceleration: 0.000833333333332575
	},
	{
		id: 1593,
		time: 1592,
		velocity: 11.2930555555556,
		power: 592.369917555835,
		road: 15562.6458796297,
		acceleration: -0.14462962962963
	},
	{
		id: 1594,
		time: 1593,
		velocity: 10.9313888888889,
		power: -0.190180384631524,
		road: 15573.7859722222,
		acceleration: -0.197407407407406
	},
	{
		id: 1595,
		time: 1594,
		velocity: 10.8997222222222,
		power: -499.052188096515,
		road: 15584.7060648148,
		acceleration: -0.242592592592594
	},
	{
		id: 1596,
		time: 1595,
		velocity: 10.5652777777778,
		power: 1217.93549567588,
		road: 15595.4678703704,
		acceleration: -0.0739814814814803
	},
	{
		id: 1597,
		time: 1596,
		velocity: 10.7094444444444,
		power: 1956.77993828852,
		road: 15606.1922685185,
		acceleration: -0.000833333333334352
	},
	{
		id: 1598,
		time: 1597,
		velocity: 10.8972222222222,
		power: 3080.38227788475,
		road: 15616.9697222222,
		acceleration: 0.106944444444444
	},
	{
		id: 1599,
		time: 1598,
		velocity: 10.8861111111111,
		power: 3124.70488262386,
		road: 15627.8541203704,
		acceleration: 0.106944444444444
	},
	{
		id: 1600,
		time: 1599,
		velocity: 11.0302777777778,
		power: 1932.93016472838,
		road: 15638.7873148148,
		acceleration: -0.00935185185185183
	},
	{
		id: 1601,
		time: 1600,
		velocity: 10.8691666666667,
		power: 1320.18934468574,
		road: 15649.6822222222,
		acceleration: -0.0672222222222221
	},
	{
		id: 1602,
		time: 1601,
		velocity: 10.6844444444444,
		power: 649.692617367035,
		road: 15660.4785185185,
		acceleration: -0.130000000000001
	},
	{
		id: 1603,
		time: 1602,
		velocity: 10.6402777777778,
		power: 365.899277315226,
		road: 15671.1321296296,
		acceleration: -0.15537037037037
	},
	{
		id: 1604,
		time: 1603,
		velocity: 10.4030555555556,
		power: 786.276309062216,
		road: 15681.6523611111,
		acceleration: -0.111388888888886
	},
	{
		id: 1605,
		time: 1604,
		velocity: 10.3502777777778,
		power: 577.704769380238,
		road: 15692.0518518519,
		acceleration: -0.130092592592595
	},
	{
		id: 1606,
		time: 1605,
		velocity: 10.25,
		power: 1979.27749177092,
		road: 15702.3928703704,
		acceleration: 0.013148148148149
	},
	{
		id: 1607,
		time: 1606,
		velocity: 10.4425,
		power: 2039.01425536096,
		road: 15712.7498148148,
		acceleration: 0.0187037037037037
	},
	{
		id: 1608,
		time: 1607,
		velocity: 10.4063888888889,
		power: 2511.90704878406,
		road: 15723.1486574074,
		acceleration: 0.0650925925925936
	},
	{
		id: 1609,
		time: 1608,
		velocity: 10.4452777777778,
		power: 2283.70786680497,
		road: 15733.6001851852,
		acceleration: 0.0402777777777761
	},
	{
		id: 1610,
		time: 1609,
		velocity: 10.5633333333333,
		power: 3196.50624189295,
		road: 15744.1360185185,
		acceleration: 0.128333333333334
	},
	{
		id: 1611,
		time: 1610,
		velocity: 10.7913888888889,
		power: 2879.01907474921,
		road: 15754.7822222222,
		acceleration: 0.0924074074074071
	},
	{
		id: 1612,
		time: 1611,
		velocity: 10.7225,
		power: 3324.05420090454,
		road: 15765.5403703704,
		acceleration: 0.131481481481481
	},
	{
		id: 1613,
		time: 1612,
		velocity: 10.9577777777778,
		power: 2639.94616659725,
		road: 15776.3949074074,
		acceleration: 0.0612962962962964
	},
	{
		id: 1614,
		time: 1613,
		velocity: 10.9752777777778,
		power: 2829.64642500479,
		road: 15787.3186111111,
		acceleration: 0.077037037037039
	},
	{
		id: 1615,
		time: 1614,
		velocity: 10.9536111111111,
		power: 2126.04827793905,
		road: 15798.2849074074,
		acceleration: 0.00814814814814646
	},
	{
		id: 1616,
		time: 1615,
		velocity: 10.9822222222222,
		power: 1910.50326843596,
		road: 15809.2490740741,
		acceleration: -0.0124074074074088
	},
	{
		id: 1617,
		time: 1616,
		velocity: 10.9380555555556,
		power: 2605.18645813763,
		road: 15820.2337037037,
		acceleration: 0.0533333333333346
	},
	{
		id: 1618,
		time: 1617,
		velocity: 11.1136111111111,
		power: 3054.00846462954,
		road: 15831.2916666667,
		acceleration: 0.0933333333333337
	},
	{
		id: 1619,
		time: 1618,
		velocity: 11.2622222222222,
		power: 4310.78480174553,
		road: 15842.4988888889,
		acceleration: 0.205185185185185
	},
	{
		id: 1620,
		time: 1619,
		velocity: 11.5536111111111,
		power: 4934.84125285204,
		road: 15853.9343981482,
		acceleration: 0.251388888888888
	},
	{
		id: 1621,
		time: 1620,
		velocity: 11.8677777777778,
		power: 4828.75777742187,
		road: 15865.6102777778,
		acceleration: 0.229351851851852
	},
	{
		id: 1622,
		time: 1621,
		velocity: 11.9502777777778,
		power: 3981.38991492766,
		road: 15877.4733796296,
		acceleration: 0.145092592592592
	},
	{
		id: 1623,
		time: 1622,
		velocity: 11.9888888888889,
		power: 1612.49277321357,
		road: 15889.3763888889,
		acceleration: -0.0652777777777782
	},
	{
		id: 1624,
		time: 1623,
		velocity: 11.6719444444444,
		power: 1308.17850200267,
		road: 15901.2016203704,
		acceleration: -0.0902777777777786
	},
	{
		id: 1625,
		time: 1624,
		velocity: 11.6794444444444,
		power: 709.422234134827,
		road: 15912.9111111111,
		acceleration: -0.141203703703702
	},
	{
		id: 1626,
		time: 1625,
		velocity: 11.5652777777778,
		power: 1067.50786050803,
		road: 15924.4967592593,
		acceleration: -0.106481481481481
	},
	{
		id: 1627,
		time: 1626,
		velocity: 11.3525,
		power: 140.207363167815,
		road: 15935.935,
		acceleration: -0.188333333333334
	},
	{
		id: 1628,
		time: 1627,
		velocity: 11.1144444444444,
		power: -1332.05896294801,
		road: 15947.1176388889,
		acceleration: -0.322870370370367
	},
	{
		id: 1629,
		time: 1628,
		velocity: 10.5966666666667,
		power: -1137.29329182422,
		road: 15957.9869444445,
		acceleration: -0.303796296296298
	},
	{
		id: 1630,
		time: 1629,
		velocity: 10.4411111111111,
		power: -597.996459356225,
		road: 15968.5793981482,
		acceleration: -0.249907407407408
	},
	{
		id: 1631,
		time: 1630,
		velocity: 10.3647222222222,
		power: 527.304437703264,
		road: 15978.9793055556,
		acceleration: -0.135185185185184
	},
	{
		id: 1632,
		time: 1631,
		velocity: 10.1911111111111,
		power: 341.652944045915,
		road: 15989.2357407408,
		acceleration: -0.15175925925926
	},
	{
		id: 1633,
		time: 1632,
		velocity: 9.98583333333333,
		power: 126.889867213139,
		road: 15999.330462963,
		acceleration: -0.171666666666667
	},
	{
		id: 1634,
		time: 1633,
		velocity: 9.84972222222222,
		power: 182.154690838235,
		road: 16009.2575,
		acceleration: -0.163703703703703
	},
	{
		id: 1635,
		time: 1634,
		velocity: 9.7,
		power: 118.017065282683,
		road: 16019.0184722222,
		acceleration: -0.168425925925925
	},
	{
		id: 1636,
		time: 1635,
		velocity: 9.48055555555556,
		power: 3.88274680848317,
		road: 16028.6058333334,
		acceleration: -0.178796296296296
	},
	{
		id: 1637,
		time: 1636,
		velocity: 9.31333333333333,
		power: -694.515223321792,
		road: 16037.9764351852,
		acceleration: -0.254722222222224
	},
	{
		id: 1638,
		time: 1637,
		velocity: 8.93583333333333,
		power: -699.422583376216,
		road: 16047.0922685185,
		acceleration: -0.254814814814814
	},
	{
		id: 1639,
		time: 1638,
		velocity: 8.71611111111111,
		power: -397.004302765266,
		road: 16055.9712962963,
		acceleration: -0.218796296296297
	},
	{
		id: 1640,
		time: 1639,
		velocity: 8.65694444444444,
		power: 1774.01912304459,
		road: 16064.7614351852,
		acceleration: 0.0410185185185199
	},
	{
		id: 1641,
		time: 1640,
		velocity: 9.05888888888889,
		power: 4142.38066801258,
		road: 16073.7282870371,
		acceleration: 0.312407407407406
	},
	{
		id: 1642,
		time: 1641,
		velocity: 9.65333333333333,
		power: 5816.45379093499,
		road: 16083.0891203704,
		acceleration: 0.475555555555557
	},
	{
		id: 1643,
		time: 1642,
		velocity: 10.0836111111111,
		power: 4942.23956399659,
		road: 16092.8625,
		acceleration: 0.349537037037038
	},
	{
		id: 1644,
		time: 1643,
		velocity: 10.1075,
		power: 2360.09843848991,
		road: 16102.842962963,
		acceleration: 0.0646296296296285
	},
	{
		id: 1645,
		time: 1644,
		velocity: 9.84722222222222,
		power: -581.39961491815,
		road: 16112.7336111111,
		acceleration: -0.244259259259259
	},
	{
		id: 1646,
		time: 1645,
		velocity: 9.35083333333333,
		power: -2089.44417180361,
		road: 16122.297962963,
		acceleration: -0.408333333333333
	},
	{
		id: 1647,
		time: 1646,
		velocity: 8.8825,
		power: -1690.55795807405,
		road: 16131.4739814815,
		acceleration: -0.368333333333332
	},
	{
		id: 1648,
		time: 1647,
		velocity: 8.74222222222222,
		power: 718.915414287724,
		road: 16140.4217592593,
		acceleration: -0.0881481481481501
	},
	{
		id: 1649,
		time: 1648,
		velocity: 9.08638888888889,
		power: 3345.60745496587,
		road: 16149.4337962963,
		acceleration: 0.216666666666669
	},
	{
		id: 1650,
		time: 1649,
		velocity: 9.5325,
		power: 4431.80928374863,
		road: 16158.7168055556,
		acceleration: 0.325277777777778
	},
	{
		id: 1651,
		time: 1650,
		velocity: 9.71805555555556,
		power: 3412.12830880123,
		road: 16168.2607407408,
		acceleration: 0.196574074074073
	},
	{
		id: 1652,
		time: 1651,
		velocity: 9.67611111111111,
		power: 891.03527689855,
		road: 16177.8619907408,
		acceleration: -0.0819444444444439
	},
	{
		id: 1653,
		time: 1652,
		velocity: 9.28666666666667,
		power: -1785.37709727847,
		road: 16187.2337962963,
		acceleration: -0.376944444444446
	},
	{
		id: 1654,
		time: 1653,
		velocity: 8.58722222222222,
		power: -2746.97107004322,
		road: 16196.1694907408,
		acceleration: -0.495277777777778
	},
	{
		id: 1655,
		time: 1654,
		velocity: 8.19027777777778,
		power: -2417.93009705641,
		road: 16204.6234722222,
		acceleration: -0.468148148148149
	},
	{
		id: 1656,
		time: 1655,
		velocity: 7.88222222222222,
		power: -417.243133831025,
		road: 16212.7341203704,
		acceleration: -0.218518518518518
	},
	{
		id: 1657,
		time: 1656,
		velocity: 7.93166666666667,
		power: 748.81030799541,
		road: 16220.7032870371,
		acceleration: -0.0644444444444447
	},
	{
		id: 1658,
		time: 1657,
		velocity: 7.99694444444444,
		power: 1691.28209380085,
		road: 16228.6701851852,
		acceleration: 0.0599074074074073
	},
	{
		id: 1659,
		time: 1658,
		velocity: 8.06194444444444,
		power: 558.263466478702,
		road: 16236.6224074074,
		acceleration: -0.0892592592592578
	},
	{
		id: 1660,
		time: 1659,
		velocity: 7.66388888888889,
		power: -264.937241722928,
		road: 16244.4312962963,
		acceleration: -0.197407407407408
	},
	{
		id: 1661,
		time: 1660,
		velocity: 7.40472222222222,
		power: -64.0322825623963,
		road: 16252.0569907408,
		acceleration: -0.168981481481481
	},
	{
		id: 1662,
		time: 1661,
		velocity: 7.555,
		power: 1646.49867705743,
		road: 16259.6325462963,
		acceleration: 0.0687037037037044
	},
	{
		id: 1663,
		time: 1662,
		velocity: 7.87,
		power: 3851.58499081367,
		road: 16267.4214814815,
		acceleration: 0.358055555555556
	},
	{
		id: 1664,
		time: 1663,
		velocity: 8.47888888888889,
		power: 5455.06455103161,
		road: 16275.6546759259,
		acceleration: 0.530462962962962
	},
	{
		id: 1665,
		time: 1664,
		velocity: 9.14638888888889,
		power: 7241.42381608792,
		road: 16284.4973611111,
		acceleration: 0.688518518518519
	},
	{
		id: 1666,
		time: 1665,
		velocity: 9.93555555555556,
		power: 6440.16738931488,
		road: 16293.9529166667,
		acceleration: 0.537222222222223
	},
	{
		id: 1667,
		time: 1666,
		velocity: 10.0905555555556,
		power: 4669.05789618237,
		road: 16303.8338425926,
		acceleration: 0.313518518518519
	},
	{
		id: 1668,
		time: 1667,
		velocity: 10.0869444444444,
		power: 739.915262587522,
		road: 16313.8186111111,
		acceleration: -0.105833333333335
	},
	{
		id: 1669,
		time: 1668,
		velocity: 9.61805555555556,
		power: 212.023411254852,
		road: 16323.6706944445,
		acceleration: -0.159537037037037
	},
	{
		id: 1670,
		time: 1669,
		velocity: 9.61194444444445,
		power: 209.529055116874,
		road: 16333.3641666667,
		acceleration: -0.157685185185183
	},
	{
		id: 1671,
		time: 1670,
		velocity: 9.61388888888889,
		power: 1400.04033554895,
		road: 16342.9656481482,
		acceleration: -0.026296296296298
	},
	{
		id: 1672,
		time: 1671,
		velocity: 9.53916666666667,
		power: 1385.88576496587,
		road: 16352.5404166667,
		acceleration: -0.0271296296296288
	},
	{
		id: 1673,
		time: 1672,
		velocity: 9.53055555555556,
		power: 806.000081645321,
		road: 16362.0568518519,
		acceleration: -0.0895370370370383
	},
	{
		id: 1674,
		time: 1673,
		velocity: 9.34527777777778,
		power: 988.175769594097,
		road: 16371.4946759259,
		acceleration: -0.0676851851851836
	},
	{
		id: 1675,
		time: 1674,
		velocity: 9.33611111111111,
		power: 891.12286871478,
		road: 16380.8601851852,
		acceleration: -0.0769444444444467
	},
	{
		id: 1676,
		time: 1675,
		velocity: 9.29972222222222,
		power: 1036.51245752727,
		road: 16390.1576851852,
		acceleration: -0.0590740740740738
	},
	{
		id: 1677,
		time: 1676,
		velocity: 9.16805555555556,
		power: 335.15561076696,
		road: 16399.3572222222,
		acceleration: -0.136851851851851
	},
	{
		id: 1678,
		time: 1677,
		velocity: 8.92555555555555,
		power: -587.746169337326,
		road: 16408.3675,
		acceleration: -0.241666666666667
	},
	{
		id: 1679,
		time: 1678,
		velocity: 8.57472222222222,
		power: -1644.31509874315,
		road: 16417.0726851852,
		acceleration: -0.368518518518519
	},
	{
		id: 1680,
		time: 1679,
		velocity: 8.0625,
		power: -1833.98745198899,
		road: 16425.3946296296,
		acceleration: -0.397962962962961
	},
	{
		id: 1681,
		time: 1680,
		velocity: 7.73166666666667,
		power: -1601.82272074819,
		road: 16433.3300925926,
		acceleration: -0.375000000000001
	},
	{
		id: 1682,
		time: 1681,
		velocity: 7.44972222222222,
		power: -1100.48551758463,
		road: 16440.9219444445,
		acceleration: -0.312222222222223
	},
	{
		id: 1683,
		time: 1682,
		velocity: 7.12583333333333,
		power: -985.213222809539,
		road: 16448.207962963,
		acceleration: -0.299444444444443
	},
	{
		id: 1684,
		time: 1683,
		velocity: 6.83333333333333,
		power: -627.443141481511,
		road: 16455.2196759259,
		acceleration: -0.249166666666667
	},
	{
		id: 1685,
		time: 1684,
		velocity: 6.70222222222222,
		power: 657.99531878746,
		road: 16462.0802777778,
		acceleration: -0.053055555555555
	},
	{
		id: 1686,
		time: 1685,
		velocity: 6.96666666666667,
		power: 2032.60608683455,
		road: 16468.9918055556,
		acceleration: 0.154907407407407
	},
	{
		id: 1687,
		time: 1686,
		velocity: 7.29805555555556,
		power: 5244.2816346953,
		road: 16476.2802777778,
		acceleration: 0.598981481481482
	},
	{
		id: 1688,
		time: 1687,
		velocity: 8.49916666666667,
		power: 7595.47338985155,
		road: 16484.2849537037,
		acceleration: 0.833425925925927
	},
	{
		id: 1689,
		time: 1688,
		velocity: 9.46694444444445,
		power: 10508.2632563132,
		road: 16493.2363888889,
		acceleration: 1.06009259259259
	},
	{
		id: 1690,
		time: 1689,
		velocity: 10.4783333333333,
		power: 9336.59295916477,
		road: 16503.1222222222,
		acceleration: 0.808703703703703
	},
	{
		id: 1691,
		time: 1690,
		velocity: 10.9252777777778,
		power: 7713.05560650297,
		road: 16513.6996759259,
		acceleration: 0.574537037037036
	},
	{
		id: 1692,
		time: 1691,
		velocity: 11.1905555555556,
		power: 5382.84316707346,
		road: 16524.7225462963,
		acceleration: 0.316296296296297
	},
	{
		id: 1693,
		time: 1692,
		velocity: 11.4272222222222,
		power: 5232.57632362646,
		road: 16536.0460185185,
		acceleration: 0.284907407407406
	},
	{
		id: 1694,
		time: 1693,
		velocity: 11.78,
		power: 6082.95657931012,
		road: 16547.6840740741,
		acceleration: 0.344259259259259
	},
	{
		id: 1695,
		time: 1694,
		velocity: 12.2233333333333,
		power: 7601.0201607455,
		road: 16559.7206944445,
		acceleration: 0.452870370370372
	},
	{
		id: 1696,
		time: 1695,
		velocity: 12.7858333333333,
		power: 7139.76887424875,
		road: 16572.1766666667,
		acceleration: 0.385833333333336
	},
	{
		id: 1697,
		time: 1696,
		velocity: 12.9375,
		power: 6021.48784777519,
		road: 16584.9625,
		acceleration: 0.273888888888889
	},
	{
		id: 1698,
		time: 1697,
		velocity: 13.045,
		power: 3801.4083305851,
		road: 16597.9277777778,
		acceleration: 0.0849999999999973
	},
	{
		id: 1699,
		time: 1698,
		velocity: 13.0408333333333,
		power: 2948.69337793603,
		road: 16610.9427777778,
		acceleration: 0.0144444444444467
	},
	{
		id: 1700,
		time: 1699,
		velocity: 12.9808333333333,
		power: 1056.65395730071,
		road: 16623.896712963,
		acceleration: -0.136574074074076
	},
	{
		id: 1701,
		time: 1700,
		velocity: 12.6352777777778,
		power: -593.267487493242,
		road: 16636.6484259259,
		acceleration: -0.267870370370368
	},
	{
		id: 1702,
		time: 1701,
		velocity: 12.2372222222222,
		power: -1059.9236724806,
		road: 16649.1141203704,
		acceleration: -0.304166666666671
	},
	{
		id: 1703,
		time: 1702,
		velocity: 12.0683333333333,
		power: -806.82754348609,
		road: 16661.2875,
		acceleration: -0.280462962962961
	},
	{
		id: 1704,
		time: 1703,
		velocity: 11.7938888888889,
		power: -56.4466893952563,
		road: 16673.2143518519,
		acceleration: -0.212592592592593
	},
	{
		id: 1705,
		time: 1704,
		velocity: 11.5994444444444,
		power: 217.181497306501,
		road: 16684.9421296296,
		acceleration: -0.185555555555556
	},
	{
		id: 1706,
		time: 1705,
		velocity: 11.5116666666667,
		power: 1237.24817143999,
		road: 16696.5315277778,
		acceleration: -0.0912037037037052
	},
	{
		id: 1707,
		time: 1706,
		velocity: 11.5202777777778,
		power: 1703.06249876227,
		road: 16708.051712963,
		acceleration: -0.0472222222222207
	},
	{
		id: 1708,
		time: 1707,
		velocity: 11.4577777777778,
		power: 3734.86205069479,
		road: 16719.6162037037,
		acceleration: 0.135833333333332
	},
	{
		id: 1709,
		time: 1708,
		velocity: 11.9191666666667,
		power: 4659.56942459101,
		road: 16731.3541666667,
		acceleration: 0.21111111111111
	},
	{
		id: 1710,
		time: 1709,
		velocity: 12.1536111111111,
		power: 6738.97991356378,
		road: 16743.386712963,
		acceleration: 0.378055555555559
	},
	{
		id: 1711,
		time: 1710,
		velocity: 12.5919444444444,
		power: 6171.75872169964,
		road: 16755.7627314815,
		acceleration: 0.308888888888887
	},
	{
		id: 1712,
		time: 1711,
		velocity: 12.8458333333333,
		power: 6678.50532939401,
		road: 16768.4596296296,
		acceleration: 0.332870370370369
	},
	{
		id: 1713,
		time: 1712,
		velocity: 13.1522222222222,
		power: 5612.01162817509,
		road: 16781.4382870371,
		acceleration: 0.230648148148148
	},
	{
		id: 1714,
		time: 1713,
		velocity: 13.2838888888889,
		power: 5072.3669234121,
		road: 16794.6211111111,
		acceleration: 0.177685185185187
	},
	{
		id: 1715,
		time: 1714,
		velocity: 13.3788888888889,
		power: 3555.62814202243,
		road: 16807.9192592593,
		acceleration: 0.0529629629629653
	},
	{
		id: 1716,
		time: 1715,
		velocity: 13.3111111111111,
		power: 2711.13757043549,
		road: 16821.2368055556,
		acceleration: -0.014166666666668
	},
	{
		id: 1717,
		time: 1716,
		velocity: 13.2413888888889,
		power: 1586.71755115739,
		road: 16834.4966666667,
		acceleration: -0.101203703703707
	},
	{
		id: 1718,
		time: 1717,
		velocity: 13.0752777777778,
		power: 1599.13900095588,
		road: 16847.6570370371,
		acceleration: -0.0977777777777753
	},
	{
		id: 1719,
		time: 1718,
		velocity: 13.0177777777778,
		power: 1084.93935223191,
		road: 16860.7004166667,
		acceleration: -0.136203703703705
	},
	{
		id: 1720,
		time: 1719,
		velocity: 12.8327777777778,
		power: 1473.7078853529,
		road: 16873.6246296296,
		acceleration: -0.10212962962963
	},
	{
		id: 1721,
		time: 1720,
		velocity: 12.7688888888889,
		power: 1189.53288677354,
		road: 16886.4364351852,
		acceleration: -0.122685185185183
	},
	{
		id: 1722,
		time: 1721,
		velocity: 12.6497222222222,
		power: 1950.98948854452,
		road: 16899.1579166667,
		acceleration: -0.0579629629629626
	},
	{
		id: 1723,
		time: 1722,
		velocity: 12.6588888888889,
		power: 1971.70251163945,
		road: 16911.8230555556,
		acceleration: -0.0547222222222228
	},
	{
		id: 1724,
		time: 1723,
		velocity: 12.6047222222222,
		power: 2737.21289366766,
		road: 16924.4655092593,
		acceleration: 0.00935185185185183
	},
	{
		id: 1725,
		time: 1724,
		velocity: 12.6777777777778,
		power: 2499.09839746628,
		road: 16937.1074537037,
		acceleration: -0.0103703703703708
	},
	{
		id: 1726,
		time: 1725,
		velocity: 12.6277777777778,
		power: 2903.74974170583,
		road: 16949.7556944445,
		acceleration: 0.0229629629629624
	},
	{
		id: 1727,
		time: 1726,
		velocity: 12.6736111111111,
		power: 2389.15259015362,
		road: 16962.4055555556,
		acceleration: -0.0197222222222209
	},
	{
		id: 1728,
		time: 1727,
		velocity: 12.6186111111111,
		power: 1966.88747543114,
		road: 16975.0187037037,
		acceleration: -0.0537037037037038
	},
	{
		id: 1729,
		time: 1728,
		velocity: 12.4666666666667,
		power: 1665.02991799128,
		road: 16987.5664351852,
		acceleration: -0.0771296296296295
	},
	{
		id: 1730,
		time: 1729,
		velocity: 12.4422222222222,
		power: -133.587545808334,
		road: 16999.9629166667,
		acceleration: -0.225370370370371
	},
	{
		id: 1731,
		time: 1730,
		velocity: 11.9425,
		power: -329.576796017084,
		road: 17012.1270833334,
		acceleration: -0.23925925925926
	},
	{
		id: 1732,
		time: 1731,
		velocity: 11.7488888888889,
		power: -1416.76382720036,
		road: 17024.0056018519,
		acceleration: -0.332037037037036
	},
	{
		id: 1733,
		time: 1732,
		velocity: 11.4461111111111,
		power: -980.585797928927,
		road: 17035.5722222222,
		acceleration: -0.29175925925926
	},
	{
		id: 1734,
		time: 1733,
		velocity: 11.0672222222222,
		power: -435.185574668443,
		road: 17046.8730555556,
		acceleration: -0.239814814814814
	},
	{
		id: 1735,
		time: 1734,
		velocity: 11.0294444444444,
		power: -477.415234045887,
		road: 17057.9331481482,
		acceleration: -0.241666666666667
	},
	{
		id: 1736,
		time: 1735,
		velocity: 10.7211111111111,
		power: 19.1789030831945,
		road: 17068.7764814815,
		acceleration: -0.191851851851851
	},
	{
		id: 1737,
		time: 1736,
		velocity: 10.4916666666667,
		power: 172.183826182811,
		road: 17079.4366203704,
		acceleration: -0.174537037037037
	},
	{
		id: 1738,
		time: 1737,
		velocity: 10.5058333333333,
		power: 1278.79554898146,
		road: 17089.9781018519,
		acceleration: -0.0627777777777787
	},
	{
		id: 1739,
		time: 1738,
		velocity: 10.5327777777778,
		power: 1727.92245046194,
		road: 17100.4797222222,
		acceleration: -0.0169444444444427
	},
	{
		id: 1740,
		time: 1739,
		velocity: 10.4408333333333,
		power: 3073.47056371421,
		road: 17111.0306018519,
		acceleration: 0.115462962962962
	},
	{
		id: 1741,
		time: 1740,
		velocity: 10.8522222222222,
		power: 3015.74183110872,
		road: 17121.6918518519,
		acceleration: 0.105277777777777
	},
	{
		id: 1742,
		time: 1741,
		velocity: 10.8486111111111,
		power: 3541.53945317561,
		road: 17132.4813888889,
		acceleration: 0.151296296296296
	},
	{
		id: 1743,
		time: 1742,
		velocity: 10.8947222222222,
		power: 2014.86810268281,
		road: 17143.3468518519,
		acceleration: 0.000555555555555642
	},
	{
		id: 1744,
		time: 1743,
		velocity: 10.8538888888889,
		power: 2468.6733217521,
		road: 17154.2343981482,
		acceleration: 0.0436111111111117
	},
	{
		id: 1745,
		time: 1744,
		velocity: 10.9794444444444,
		power: 2577.49327708688,
		road: 17165.1699537037,
		acceleration: 0.0524074074074079
	},
	{
		id: 1746,
		time: 1745,
		velocity: 11.0519444444444,
		power: 4142.51266996589,
		road: 17176.2299537037,
		acceleration: 0.196481481481483
	},
	{
		id: 1747,
		time: 1746,
		velocity: 11.4433333333333,
		power: 4964.73965628126,
		road: 17187.5191666667,
		acceleration: 0.261944444444442
	},
	{
		id: 1748,
		time: 1747,
		velocity: 11.7652777777778,
		power: 5319.12716421724,
		road: 17199.079212963,
		acceleration: 0.279722222222222
	},
	{
		id: 1749,
		time: 1748,
		velocity: 11.8911111111111,
		power: 4863.36922022812,
		road: 17210.8918981482,
		acceleration: 0.225555555555555
	},
	{
		id: 1750,
		time: 1749,
		velocity: 12.12,
		power: 4062.91834424361,
		road: 17222.8905555556,
		acceleration: 0.14638888888889
	},
	{
		id: 1751,
		time: 1750,
		velocity: 12.2044444444444,
		power: 4123.87586388436,
		road: 17235.0350925926,
		acceleration: 0.145370370370369
	},
	{
		id: 1752,
		time: 1751,
		velocity: 12.3272222222222,
		power: 4229.24713867966,
		road: 17247.3263425926,
		acceleration: 0.148055555555556
	},
	{
		id: 1753,
		time: 1752,
		velocity: 12.5641666666667,
		power: 5384.57763127429,
		road: 17259.81,
		acceleration: 0.236759259259259
	},
	{
		id: 1754,
		time: 1753,
		velocity: 12.9147222222222,
		power: 6460.97420067692,
		road: 17272.5677777778,
		acceleration: 0.311481481481481
	},
	{
		id: 1755,
		time: 1754,
		velocity: 13.2616666666667,
		power: 6167.83610737124,
		road: 17285.617175926,
		acceleration: 0.271759259259261
	},
	{
		id: 1756,
		time: 1755,
		velocity: 13.3794444444444,
		power: 6284.59881851514,
		road: 17298.9358796297,
		acceleration: 0.266851851851852
	},
	{
		id: 1757,
		time: 1756,
		velocity: 13.7152777777778,
		power: 6698.31373249273,
		road: 17312.5302314815,
		acceleration: 0.284444444444443
	},
	{
		id: 1758,
		time: 1757,
		velocity: 14.115,
		power: 6795.92170594756,
		road: 17326.4052777778,
		acceleration: 0.276944444444446
	},
	{
		id: 1759,
		time: 1758,
		velocity: 14.2102777777778,
		power: 4453.29475002308,
		road: 17340.4651851852,
		acceleration: 0.0927777777777763
	},
	{
		id: 1760,
		time: 1759,
		velocity: 13.9936111111111,
		power: 217.814713205711,
		road: 17354.4606944445,
		acceleration: -0.221574074074072
	},
	{
		id: 1761,
		time: 1760,
		velocity: 13.4502777777778,
		power: -890.124837945539,
		road: 17368.1945833334,
		acceleration: -0.301666666666668
	},
	{
		id: 1762,
		time: 1761,
		velocity: 13.3052777777778,
		power: -713.598622881995,
		road: 17381.6351851852,
		acceleration: -0.284907407407406
	},
	{
		id: 1763,
		time: 1762,
		velocity: 13.1388888888889,
		power: 254.881021121994,
		road: 17394.8306481482,
		acceleration: -0.205370370370371
	},
	{
		id: 1764,
		time: 1763,
		velocity: 12.8341666666667,
		power: 1242.90082044615,
		road: 17407.8618055556,
		acceleration: -0.123240740740743
	},
	{
		id: 1765,
		time: 1764,
		velocity: 12.9355555555556,
		power: 2948.17858035247,
		road: 17420.8391666667,
		acceleration: 0.0156481481481503
	},
	{
		id: 1766,
		time: 1765,
		velocity: 13.1858333333333,
		power: 5107.03580844581,
		road: 17433.9169907408,
		acceleration: 0.185277777777777
	},
	{
		id: 1767,
		time: 1766,
		velocity: 13.39,
		power: 4102.91503437897,
		road: 17447.1370370371,
		acceleration: 0.0991666666666653
	},
	{
		id: 1768,
		time: 1767,
		velocity: 13.2330555555556,
		power: 3506.8379411233,
		road: 17460.4312962963,
		acceleration: 0.0492592592592587
	},
	{
		id: 1769,
		time: 1768,
		velocity: 13.3336111111111,
		power: 3391.33363765279,
		road: 17473.7694907408,
		acceleration: 0.0386111111111145
	},
	{
		id: 1770,
		time: 1769,
		velocity: 13.5058333333333,
		power: 4748.16706441787,
		road: 17487.1976388889,
		acceleration: 0.141296296296295
	},
	{
		id: 1771,
		time: 1770,
		velocity: 13.6569444444444,
		power: 4438.35334413128,
		road: 17500.7524074074,
		acceleration: 0.111944444444443
	},
	{
		id: 1772,
		time: 1771,
		velocity: 13.6694444444444,
		power: 3512.82451233711,
		road: 17514.3820370371,
		acceleration: 0.0377777777777766
	},
	{
		id: 1773,
		time: 1772,
		velocity: 13.6191666666667,
		power: 3107.71610834595,
		road: 17528.0335185185,
		acceleration: 0.00592592592592744
	},
	{
		id: 1774,
		time: 1773,
		velocity: 13.6747222222222,
		power: 1312.72865061544,
		road: 17541.6227777778,
		acceleration: -0.13037037037037
	},
	{
		id: 1775,
		time: 1774,
		velocity: 13.2783333333333,
		power: 1367.1588205098,
		road: 17555.0852314815,
		acceleration: -0.123240740740741
	},
	{
		id: 1776,
		time: 1775,
		velocity: 13.2494444444444,
		power: 530.15654975514,
		road: 17568.3932870371,
		acceleration: -0.185555555555556
	},
	{
		id: 1777,
		time: 1776,
		velocity: 13.1180555555556,
		power: 2063.39347755771,
		road: 17581.5778240741,
		acceleration: -0.0614814814814828
	},
	{
		id: 1778,
		time: 1777,
		velocity: 13.0938888888889,
		power: 1346.60250003069,
		road: 17594.673425926,
		acceleration: -0.116388888888887
	},
	{
		id: 1779,
		time: 1778,
		velocity: 12.9002777777778,
		power: 1459.58183469147,
		road: 17607.6584722223,
		acceleration: -0.104722222222223
	},
	{
		id: 1780,
		time: 1779,
		velocity: 12.8038888888889,
		power: 1892.56595548021,
		road: 17620.5574074074,
		acceleration: -0.0675000000000008
	},
	{
		id: 1781,
		time: 1780,
		velocity: 12.8913888888889,
		power: 4119.7058520966,
		road: 17633.4788425926,
		acceleration: 0.112500000000002
	},
	{
		id: 1782,
		time: 1781,
		velocity: 13.2377777777778,
		power: 5068.43432434607,
		road: 17646.5478240741,
		acceleration: 0.182592592592593
	},
	{
		id: 1783,
		time: 1782,
		velocity: 13.3516666666667,
		power: 5585.56005846606,
		road: 17659.8152777778,
		acceleration: 0.21435185185185
	},
	{
		id: 1784,
		time: 1783,
		velocity: 13.5344444444444,
		power: 5041.92132256355,
		road: 17673.2713888889,
		acceleration: 0.162962962962961
	},
	{
		id: 1785,
		time: 1784,
		velocity: 13.7266666666667,
		power: 5255.23440899247,
		road: 17686.8949537037,
		acceleration: 0.171944444444447
	},
	{
		id: 1786,
		time: 1785,
		velocity: 13.8675,
		power: 5956.79009044994,
		road: 17700.7126851852,
		acceleration: 0.216388888888888
	},
	{
		id: 1787,
		time: 1786,
		velocity: 14.1836111111111,
		power: 6749.53970202672,
		road: 17714.7705555556,
		acceleration: 0.263888888888889
	},
	{
		id: 1788,
		time: 1787,
		velocity: 14.5183333333333,
		power: 7764.50571398524,
		road: 17729.1218055556,
		acceleration: 0.322870370370369
	},
	{
		id: 1789,
		time: 1788,
		velocity: 14.8361111111111,
		power: 5627.71967933378,
		road: 17743.7126388889,
		acceleration: 0.156296296296297
	},
	{
		id: 1790,
		time: 1789,
		velocity: 14.6525,
		power: 3255.05885396472,
		road: 17758.3735185185,
		acceleration: -0.0162037037037042
	},
	{
		id: 1791,
		time: 1790,
		velocity: 14.4697222222222,
		power: 1961.61144494263,
		road: 17772.9728240741,
		acceleration: -0.106944444444443
	},
	{
		id: 1792,
		time: 1791,
		velocity: 14.5152777777778,
		power: 3040.00429509941,
		road: 17787.5049074074,
		acceleration: -0.0275000000000016
	},
	{
		id: 1793,
		time: 1792,
		velocity: 14.57,
		power: 3568.18358009132,
		road: 17802.0286574074,
		acceleration: 0.0108333333333341
	},
	{
		id: 1794,
		time: 1793,
		velocity: 14.5022222222222,
		power: 2700.42959962062,
		road: 17816.5322685185,
		acceleration: -0.051111111111112
	},
	{
		id: 1795,
		time: 1794,
		velocity: 14.3619444444444,
		power: 2308.63531256023,
		road: 17830.9715277778,
		acceleration: -0.0775925925925929
	},
	{
		id: 1796,
		time: 1795,
		velocity: 14.3372222222222,
		power: 2273.7018716258,
		road: 17845.3330092593,
		acceleration: -0.0779629629629621
	},
	{
		id: 1797,
		time: 1796,
		velocity: 14.2683333333333,
		power: 1500.16072450393,
		road: 17859.5895833334,
		acceleration: -0.131851851851851
	},
	{
		id: 1798,
		time: 1797,
		velocity: 13.9663888888889,
		power: 394.105129699314,
		road: 17873.6752314815,
		acceleration: -0.210000000000001
	},
	{
		id: 1799,
		time: 1798,
		velocity: 13.7072222222222,
		power: -252.414116013384,
		road: 17887.5285185185,
		acceleration: -0.254722222222222
	},
	{
		id: 1800,
		time: 1799,
		velocity: 13.5041666666667,
		power: 285.400656597876,
		road: 17901.1493981482,
		acceleration: -0.210092592592593
	},
	{
		id: 1801,
		time: 1800,
		velocity: 13.3361111111111,
		power: 302.133068574457,
		road: 17914.5625925926,
		acceleration: -0.205277777777779
	},
	{
		id: 1802,
		time: 1801,
		velocity: 13.0913888888889,
		power: -1090.43827210102,
		road: 17927.7172222223,
		acceleration: -0.311851851851852
	},
	{
		id: 1803,
		time: 1802,
		velocity: 12.5686111111111,
		power: -105.793879823911,
		road: 17940.6011111111,
		acceleration: -0.229629629629628
	},
	{
		id: 1804,
		time: 1803,
		velocity: 12.6472222222222,
		power: 1227.48570110523,
		road: 17953.3115277778,
		acceleration: -0.117314814814815
	},
	{
		id: 1805,
		time: 1804,
		velocity: 12.7394444444444,
		power: 2707.4482189846,
		road: 17965.9665277778,
		acceleration: 0.00648148148148309
	},
	{
		id: 1806,
		time: 1805,
		velocity: 12.5880555555556,
		power: 1150.41960383498,
		road: 17978.5641666667,
		acceleration: -0.121203703703705
	},
	{
		id: 1807,
		time: 1806,
		velocity: 12.2836111111111,
		power: 1138.92288548321,
		road: 17991.0414351852,
		acceleration: -0.119537037037039
	},
	{
		id: 1808,
		time: 1807,
		velocity: 12.3808333333333,
		power: 2797.30338345124,
		road: 18003.4696296297,
		acceleration: 0.0213888888888896
	},
	{
		id: 1809,
		time: 1808,
		velocity: 12.6522222222222,
		power: 4399.57833487521,
		road: 18015.9848611111,
		acceleration: 0.152685185185186
	},
	{
		id: 1810,
		time: 1809,
		velocity: 12.7416666666667,
		power: 7547.33670857409,
		road: 18028.7757407408,
		acceleration: 0.39861111111111
	},
	{
		id: 1811,
		time: 1810,
		velocity: 13.5766666666667,
		power: 10134.1770604914,
		road: 18042.0523148148,
		acceleration: 0.572777777777779
	},
	{
		id: 1812,
		time: 1811,
		velocity: 14.3705555555556,
		power: 11627.8586781845,
		road: 18055.9357407408,
		acceleration: 0.640925925925925
	},
	{
		id: 1813,
		time: 1812,
		velocity: 14.6644444444444,
		power: 8504.13665521217,
		road: 18070.3268518519,
		acceleration: 0.374444444444444
	},
	{
		id: 1814,
		time: 1813,
		velocity: 14.7,
		power: 4355.22595048928,
		road: 18084.9373148148,
		acceleration: 0.0642592592592592
	},
	{
		id: 1815,
		time: 1814,
		velocity: 14.5633333333333,
		power: 3089.20484031677,
		road: 18099.5663888889,
		acceleration: -0.0270370370370383
	},
	{
		id: 1816,
		time: 1815,
		velocity: 14.5833333333333,
		power: 2951.3180053206,
		road: 18114.1639814815,
		acceleration: -0.035925925925925
	},
	{
		id: 1817,
		time: 1816,
		velocity: 14.5922222222222,
		power: 3194.92491162829,
		road: 18128.7348148148,
		acceleration: -0.0175925925925942
	},
	{
		id: 1818,
		time: 1817,
		velocity: 14.5105555555556,
		power: 3281.22092367355,
		road: 18143.2913888889,
		acceleration: -0.0109259259259247
	},
	{
		id: 1819,
		time: 1818,
		velocity: 14.5505555555556,
		power: 2995.30307700038,
		road: 18157.8270833334,
		acceleration: -0.0308333333333319
	},
	{
		id: 1820,
		time: 1819,
		velocity: 14.4997222222222,
		power: 3264.8552157323,
		road: 18172.3419907408,
		acceleration: -0.0107407407407418
	},
	{
		id: 1821,
		time: 1820,
		velocity: 14.4783333333333,
		power: 3136.15888839757,
		road: 18186.8417592593,
		acceleration: -0.0195370370370362
	},
	{
		id: 1822,
		time: 1821,
		velocity: 14.4919444444444,
		power: 3751.75172772462,
		road: 18201.3441666667,
		acceleration: 0.024814814814814
	},
	{
		id: 1823,
		time: 1822,
		velocity: 14.5741666666667,
		power: 3583.49566019293,
		road: 18215.865,
		acceleration: 0.0120370370370377
	},
	{
		id: 1824,
		time: 1823,
		velocity: 14.5144444444444,
		power: 3578.76680810409,
		road: 18230.3975,
		acceleration: 0.0112962962962957
	},
	{
		id: 1825,
		time: 1824,
		velocity: 14.5258333333333,
		power: 3142.36499039368,
		road: 18244.9256481482,
		acceleration: -0.0199999999999996
	},
	{
		id: 1826,
		time: 1825,
		velocity: 14.5141666666667,
		power: 3458.37218903107,
		road: 18259.4453240741,
		acceleration: 0.00305555555555515
	},
	{
		id: 1827,
		time: 1826,
		velocity: 14.5236111111111,
		power: 3488.53719615675,
		road: 18273.9690740741,
		acceleration: 0.00509259259259309
	},
	{
		id: 1828,
		time: 1827,
		velocity: 14.5411111111111,
		power: 4370.9978753481,
		road: 18288.5290277778,
		acceleration: 0.0673148148148144
	},
	{
		id: 1829,
		time: 1828,
		velocity: 14.7161111111111,
		power: 3369.0805762018,
		road: 18303.1197685185,
		acceleration: -0.00574074074074105
	},
	{
		id: 1830,
		time: 1829,
		velocity: 14.5063888888889,
		power: 3016.57037557084,
		road: 18317.6924074074,
		acceleration: -0.0304629629629645
	},
	{
		id: 1831,
		time: 1830,
		velocity: 14.4497222222222,
		power: 2748.67601401327,
		road: 18332.2255555556,
		acceleration: -0.0485185185185149
	},
	{
		id: 1832,
		time: 1831,
		velocity: 14.5705555555556,
		power: 3771.4200259655,
		road: 18346.7472222223,
		acceleration: 0.0255555555555542
	},
	{
		id: 1833,
		time: 1832,
		velocity: 14.5830555555556,
		power: 3683.1293455873,
		road: 18361.2908796297,
		acceleration: 0.0184259259259267
	},
	{
		id: 1834,
		time: 1833,
		velocity: 14.505,
		power: 3005.2108894296,
		road: 18375.8286574074,
		acceleration: -0.0301851851851875
	},
	{
		id: 1835,
		time: 1834,
		velocity: 14.48,
		power: 2410.34508044876,
		road: 18390.3155555556,
		acceleration: -0.0715740740740731
	},
	{
		id: 1836,
		time: 1835,
		velocity: 14.3683333333333,
		power: 2611.27904669188,
		road: 18404.7390740741,
		acceleration: -0.0551851851851826
	},
	{
		id: 1837,
		time: 1836,
		velocity: 14.3394444444444,
		power: 2327.33559222905,
		road: 18419.0980092593,
		acceleration: -0.0739814814814821
	},
	{
		id: 1838,
		time: 1837,
		velocity: 14.2580555555556,
		power: 2282.30140656382,
		road: 18433.3823611111,
		acceleration: -0.0751851851851875
	},
	{
		id: 1839,
		time: 1838,
		velocity: 14.1427777777778,
		power: 2541.94646704766,
		road: 18447.6019907408,
		acceleration: -0.0542592592592577
	},
	{
		id: 1840,
		time: 1839,
		velocity: 14.1766666666667,
		power: 2576.35567143385,
		road: 18461.7693981482,
		acceleration: -0.0501851851851853
	},
	{
		id: 1841,
		time: 1840,
		velocity: 14.1075,
		power: 2759.15530090715,
		road: 18475.8940277778,
		acceleration: -0.0353703703703729
	},
	{
		id: 1842,
		time: 1841,
		velocity: 14.0366666666667,
		power: 2115.76710164476,
		road: 18489.9602314815,
		acceleration: -0.0814814814814806
	},
	{
		id: 1843,
		time: 1842,
		velocity: 13.9322222222222,
		power: 2237.91264840681,
		road: 18503.9505555556,
		acceleration: -0.0702777777777754
	},
	{
		id: 1844,
		time: 1843,
		velocity: 13.8966666666667,
		power: 2283.02647396549,
		road: 18517.8732407408,
		acceleration: -0.0650000000000031
	},
	{
		id: 1845,
		time: 1844,
		velocity: 13.8416666666667,
		power: 2716.58338678547,
		road: 18531.747962963,
		acceleration: -0.0309259259259242
	},
	{
		id: 1846,
		time: 1845,
		velocity: 13.8394444444444,
		power: 2550.06813839048,
		road: 18545.5860185186,
		acceleration: -0.0424074074074081
	},
	{
		id: 1847,
		time: 1846,
		velocity: 13.7694444444444,
		power: 2569.38862456802,
		road: 18559.3830092593,
		acceleration: -0.0397222222222222
	},
	{
		id: 1848,
		time: 1847,
		velocity: 13.7225,
		power: 2460.04066719236,
		road: 18573.1367592593,
		acceleration: -0.0467592592592574
	},
	{
		id: 1849,
		time: 1848,
		velocity: 13.6991666666667,
		power: 2491.32427474535,
		road: 18586.8456018519,
		acceleration: -0.0430555555555578
	},
	{
		id: 1850,
		time: 1849,
		velocity: 13.6402777777778,
		power: 3279.51928139061,
		road: 18600.541712963,
		acceleration: 0.0175925925925924
	},
	{
		id: 1851,
		time: 1850,
		velocity: 13.7752777777778,
		power: 3058.13601335891,
		road: 18614.2468055556,
		acceleration: 0.000370370370369244
	},
	{
		id: 1852,
		time: 1851,
		velocity: 13.7002777777778,
		power: 3724.49778571112,
		road: 18627.9772685186,
		acceleration: 0.0503703703703735
	},
	{
		id: 1853,
		time: 1852,
		velocity: 13.7913888888889,
		power: 3241.85690925674,
		road: 18641.7391666667,
		acceleration: 0.0124999999999993
	},
	{
		id: 1854,
		time: 1853,
		velocity: 13.8127777777778,
		power: 3213.94607692884,
		road: 18655.5123148149,
		acceleration: 0.00999999999999979
	},
	{
		id: 1855,
		time: 1854,
		velocity: 13.7302777777778,
		power: 2938.55039871024,
		road: 18669.285,
		acceleration: -0.0109259259259264
	},
	{
		id: 1856,
		time: 1855,
		velocity: 13.7586111111111,
		power: 2617.8122350995,
		road: 18683.0349074074,
		acceleration: -0.0346296296296309
	},
	{
		id: 1857,
		time: 1856,
		velocity: 13.7088888888889,
		power: 3172.42256683234,
		road: 18696.7715277778,
		acceleration: 0.00805555555555593
	},
	{
		id: 1858,
		time: 1857,
		velocity: 13.7544444444444,
		power: 2651.66031066776,
		road: 18710.4965277778,
		acceleration: -0.031296296296297
	},
	{
		id: 1859,
		time: 1858,
		velocity: 13.6647222222222,
		power: 3062.49822487357,
		road: 18724.2061574074,
		acceleration: 0.000555555555557419
	},
	{
		id: 1860,
		time: 1859,
		velocity: 13.7105555555556,
		power: 2707.0701365545,
		road: 18737.902962963,
		acceleration: -0.0262037037037022
	},
	{
		id: 1861,
		time: 1860,
		velocity: 13.6758333333333,
		power: 3531.69091211374,
		road: 18751.605,
		acceleration: 0.0366666666666653
	},
	{
		id: 1862,
		time: 1861,
		velocity: 13.7747222222222,
		power: 2996.9525396543,
		road: 18765.3230092593,
		acceleration: -0.00472222222222207
	},
	{
		id: 1863,
		time: 1862,
		velocity: 13.6963888888889,
		power: 3113.26088683696,
		road: 18779.0407407408,
		acceleration: 0.00416666666666821
	},
	{
		id: 1864,
		time: 1863,
		velocity: 13.6883333333333,
		power: 3374.19766682832,
		road: 18792.7723611111,
		acceleration: 0.0236111111111104
	},
	{
		id: 1865,
		time: 1864,
		velocity: 13.8455555555556,
		power: 3407.11779339705,
		road: 18806.528425926,
		acceleration: 0.0252777777777755
	},
	{
		id: 1866,
		time: 1865,
		velocity: 13.7722222222222,
		power: 3261.24413089183,
		road: 18820.3038888889,
		acceleration: 0.01351851851852
	},
	{
		id: 1867,
		time: 1866,
		velocity: 13.7288888888889,
		power: 2575.57806294975,
		road: 18834.0669907408,
		acceleration: -0.0382407407407399
	},
	{
		id: 1868,
		time: 1867,
		velocity: 13.7308333333333,
		power: 3099.55892658701,
		road: 18847.8120833334,
		acceleration: 0.00222222222222257
	},
	{
		id: 1869,
		time: 1868,
		velocity: 13.7788888888889,
		power: 3332.90935340937,
		road: 18861.5681018519,
		acceleration: 0.0196296296296286
	},
	{
		id: 1870,
		time: 1869,
		velocity: 13.7877777777778,
		power: 2953.16728797451,
		road: 18875.329212963,
		acceleration: -0.00944444444444592
	},
	{
		id: 1871,
		time: 1870,
		velocity: 13.7025,
		power: 2988.73266332786,
		road: 18889.0823611111,
		acceleration: -0.00648148148147953
	},
	{
		id: 1872,
		time: 1871,
		velocity: 13.7594444444444,
		power: 2197.37582197769,
		road: 18902.7993981482,
		acceleration: -0.0657407407407398
	},
	{
		id: 1873,
		time: 1872,
		velocity: 13.5905555555556,
		power: 2820.74120051397,
		road: 18916.4751388889,
		acceleration: -0.0168518518518521
	},
	{
		id: 1874,
		time: 1873,
		velocity: 13.6519444444444,
		power: 3027.37739579727,
		road: 18930.1420833334,
		acceleration: -0.000740740740740264
	},
	{
		id: 1875,
		time: 1874,
		velocity: 13.7572222222222,
		power: 3902.16026540341,
		road: 18943.8412037037,
		acceleration: 0.0650925925925918
	},
	{
		id: 1876,
		time: 1875,
		velocity: 13.7858333333333,
		power: 3598.43719921372,
		road: 18957.5928703704,
		acceleration: 0.0399999999999991
	},
	{
		id: 1877,
		time: 1876,
		velocity: 13.7719444444444,
		power: 3185.77382009658,
		road: 18971.368425926,
		acceleration: 0.00777777777777722
	},
	{
		id: 1878,
		time: 1877,
		velocity: 13.7805555555556,
		power: 3065.33066872352,
		road: 18985.1471296297,
		acceleration: -0.00148148148148231
	},
	{
		id: 1879,
		time: 1878,
		velocity: 13.7813888888889,
		power: 3362.03494623615,
		road: 18998.935462963,
		acceleration: 0.0207407407407416
	},
	{
		id: 1880,
		time: 1879,
		velocity: 13.8341666666667,
		power: 3689.56431447697,
		road: 19012.7563888889,
		acceleration: 0.0444444444444425
	},
	{
		id: 1881,
		time: 1880,
		velocity: 13.9138888888889,
		power: 4378.33744809385,
		road: 19026.6464814815,
		acceleration: 0.0938888888888911
	},
	{
		id: 1882,
		time: 1881,
		velocity: 14.0630555555556,
		power: 4484.62250569564,
		road: 19040.6325462963,
		acceleration: 0.0980555555555558
	},
	{
		id: 1883,
		time: 1882,
		velocity: 14.1283333333333,
		power: 4514.31682118931,
		road: 19054.7158333334,
		acceleration: 0.0963888888888889
	},
	{
		id: 1884,
		time: 1883,
		velocity: 14.2030555555556,
		power: 4526.61002219056,
		road: 19068.8940740741,
		acceleration: 0.0935185185185183
	},
	{
		id: 1885,
		time: 1884,
		velocity: 14.3436111111111,
		power: 4364.85816877557,
		road: 19083.1581944445,
		acceleration: 0.0782407407407391
	},
	{
		id: 1886,
		time: 1885,
		velocity: 14.3630555555556,
		power: 5060.3400391672,
		road: 19097.5239351852,
		acceleration: 0.125000000000002
	},
	{
		id: 1887,
		time: 1886,
		velocity: 14.5780555555556,
		power: 4724.95820977524,
		road: 19112.0002314815,
		acceleration: 0.0961111111111119
	},
	{
		id: 1888,
		time: 1887,
		velocity: 14.6319444444444,
		power: 4810.04737709058,
		road: 19126.57375,
		acceleration: 0.0983333333333327
	},
	{
		id: 1889,
		time: 1888,
		velocity: 14.6580555555556,
		power: 3910.37637812721,
		road: 19141.2121296297,
		acceleration: 0.0313888888888894
	},
	{
		id: 1890,
		time: 1889,
		velocity: 14.6722222222222,
		power: 3382.60959662774,
		road: 19155.8628240741,
		acceleration: -0.00675925925926002
	},
	{
		id: 1891,
		time: 1890,
		velocity: 14.6116666666667,
		power: 4431.62581557784,
		road: 19170.5436574074,
		acceleration: 0.0670370370370375
	},
	{
		id: 1892,
		time: 1891,
		velocity: 14.8591666666667,
		power: 3584.03254824905,
		road: 19185.2606944445,
		acceleration: 0.00537037037037003
	},
	{
		id: 1893,
		time: 1892,
		velocity: 14.6883333333333,
		power: 529.804987947584,
		road: 19199.875462963,
		acceleration: -0.209907407407407
	},
	{
		id: 1894,
		time: 1893,
		velocity: 13.9819444444444,
		power: -3926.33378739075,
		road: 19214.1200462963,
		acceleration: -0.530462962962963
	},
	{
		id: 1895,
		time: 1894,
		velocity: 13.2677777777778,
		power: -8255.13186656118,
		road: 19227.6647222223,
		acceleration: -0.869351851851853
	},
	{
		id: 1896,
		time: 1895,
		velocity: 12.0802777777778,
		power: -9699.02617985129,
		road: 19240.2627777778,
		acceleration: -1.02388888888889
	},
	{
		id: 1897,
		time: 1896,
		velocity: 10.9102777777778,
		power: -9749.99771077961,
		road: 19251.8046296297,
		acceleration: -1.08851851851852
	},
	{
		id: 1898,
		time: 1897,
		velocity: 10.0022222222222,
		power: -9453.65116139541,
		road: 19262.2321296297,
		acceleration: -1.14018518518518
	},
	{
		id: 1899,
		time: 1898,
		velocity: 8.65972222222222,
		power: -7720.97738442181,
		road: 19271.5669907408,
		acceleration: -1.04509259259259
	},
	{
		id: 1900,
		time: 1899,
		velocity: 7.775,
		power: -6217.90903181685,
		road: 19279.904212963,
		acceleration: -0.950185185185185
	},
	{
		id: 1901,
		time: 1900,
		velocity: 7.15166666666667,
		power: 315.077706764354,
		road: 19287.706712963,
		acceleration: -0.119259259259259
	},
	{
		id: 1902,
		time: 1901,
		velocity: 8.30194444444444,
		power: 9163.38305247331,
		road: 19295.9505555556,
		acceleration: 1.00194444444444
	},
	{
		id: 1903,
		time: 1902,
		velocity: 10.7808333333333,
		power: 17141.8578964194,
		road: 19305.5434722223,
		acceleration: 1.6962037037037
	},
	{
		id: 1904,
		time: 1903,
		velocity: 12.2402777777778,
		power: 14714.7830053858,
		road: 19316.5852314815,
		acceleration: 1.20148148148148
	},
	{
		id: 1905,
		time: 1904,
		velocity: 11.9063888888889,
		power: 7117.80573012665,
		road: 19328.4391666667,
		acceleration: 0.422870370370369
	},
	{
		id: 1906,
		time: 1905,
		velocity: 12.0494444444444,
		power: 1974.65833844797,
		road: 19340.4858333334,
		acceleration: -0.0374074074074073
	},
	{
		id: 1907,
		time: 1906,
		velocity: 12.1280555555556,
		power: 6437.88336395588,
		road: 19352.6847222223,
		acceleration: 0.341851851851853
	},
	{
		id: 1908,
		time: 1907,
		velocity: 12.9319444444444,
		power: 11861.6380877351,
		road: 19365.4325,
		acceleration: 0.755925925925926
	},
	{
		id: 1909,
		time: 1908,
		velocity: 14.3172222222222,
		power: 21972.8500712708,
		road: 19379.2717592593,
		acceleration: 1.42703703703704
	},
	{
		id: 1910,
		time: 1909,
		velocity: 16.4091666666667,
		power: 30630.6726415987,
		road: 19394.7298611111,
		acceleration: 1.81064814814815
	},
	{
		id: 1911,
		time: 1910,
		velocity: 18.3638888888889,
		power: 27657.6316741462,
		road: 19411.7944907408,
		acceleration: 1.40240740740741
	},
	{
		id: 1912,
		time: 1911,
		velocity: 18.5244444444444,
		power: 17430.7826541319,
		road: 19429.9068518519,
		acceleration: 0.693055555555556
	},
	{
		id: 1913,
		time: 1912,
		velocity: 18.4883333333333,
		power: 4314.83280583424,
		road: 19448.32875,
		acceleration: -0.0739814814814821
	},
	{
		id: 1914,
		time: 1913,
		velocity: 18.1419444444444,
		power: 2099.46241713555,
		road: 19466.6156944445,
		acceleration: -0.195925925925927
	},
	{
		id: 1915,
		time: 1914,
		velocity: 17.9366666666667,
		power: 1204.11098868691,
		road: 19484.6837962963,
		acceleration: -0.241759259259258
	},
	{
		id: 1916,
		time: 1915,
		velocity: 17.7630555555556,
		power: 2164.52197134602,
		road: 19502.5407870371,
		acceleration: -0.180462962962963
	},
	{
		id: 1917,
		time: 1916,
		velocity: 17.6005555555556,
		power: 2264.01777071596,
		road: 19520.2226388889,
		acceleration: -0.169814814814814
	},
	{
		id: 1918,
		time: 1917,
		velocity: 17.4272222222222,
		power: 1665.66310048352,
		road: 19537.7193518519,
		acceleration: -0.200462962962963
	},
	{
		id: 1919,
		time: 1918,
		velocity: 17.1616666666667,
		power: 1972.67478538026,
		road: 19555.0272685186,
		acceleration: -0.177129629629633
	},
	{
		id: 1920,
		time: 1919,
		velocity: 17.0691666666667,
		power: 930.835108111964,
		road: 19572.1289351852,
		acceleration: -0.235370370370372
	},
	{
		id: 1921,
		time: 1920,
		velocity: 16.7211111111111,
		power: 9.95997903705527,
		road: 19588.9695833334,
		acceleration: -0.286666666666665
	},
	{
		id: 1922,
		time: 1921,
		velocity: 16.3016666666667,
		power: -4348.66292878967,
		road: 19605.3887037037,
		acceleration: -0.55638888888889
	},
	{
		id: 1923,
		time: 1922,
		velocity: 15.4,
		power: -7354.40392491253,
		road: 19621.1518055556,
		acceleration: -0.755648148148145
	},
	{
		id: 1924,
		time: 1923,
		velocity: 14.4541666666667,
		power: -9606.20065204411,
		road: 19636.0736574074,
		acceleration: -0.926851851851854
	},
	{
		id: 1925,
		time: 1924,
		velocity: 13.5211111111111,
		power: -6419.28682474587,
		road: 19650.1739351852,
		acceleration: -0.716296296296294
	},
	{
		id: 1926,
		time: 1925,
		velocity: 13.2511111111111,
		power: -5073.63113552817,
		road: 19663.6036111111,
		acceleration: -0.624907407407408
	},
	{
		id: 1927,
		time: 1926,
		velocity: 12.5794444444444,
		power: -8573.3273564789,
		road: 19676.2568981482,
		acceleration: -0.927870370370369
	},
	{
		id: 1928,
		time: 1927,
		velocity: 10.7375,
		power: -17661.7731311172,
		road: 19687.5245370371,
		acceleration: -1.84342592592593
	},
	{
		id: 1929,
		time: 1928,
		velocity: 7.72083333333333,
		power: -21909.4280675755,
		road: 19696.5020833334,
		acceleration: -2.73675925925926
	},
	{
		id: 1930,
		time: 1929,
		velocity: 4.36916666666667,
		power: -15834.7712092619,
		road: 19702.6905092593,
		acceleration: -2.84148148148148
	},
	{
		id: 1931,
		time: 1930,
		velocity: 2.21305555555556,
		power: -7258.11108094128,
		road: 19706.3441203704,
		acceleration: -2.22814814814815
	},
	{
		id: 1932,
		time: 1931,
		velocity: 1.03638888888889,
		power: -2279.17986393357,
		road: 19708.155462963,
		acceleration: -1.45638888888889
	},
	{
		id: 1933,
		time: 1932,
		velocity: 0,
		power: -412.852157574253,
		road: 19708.8697685186,
		acceleration: -0.737685185185185
	},
	{
		id: 1934,
		time: 1933,
		velocity: 0,
		power: -35.6626265269656,
		road: 19709.0425,
		acceleration: -0.345462962962963
	},
	{
		id: 1935,
		time: 1934,
		velocity: 0,
		power: 0,
		road: 19709.0425,
		acceleration: 0
	},
	{
		id: 1936,
		time: 1935,
		velocity: 0,
		power: 0,
		road: 19709.0425,
		acceleration: 0
	},
	{
		id: 1937,
		time: 1936,
		velocity: 0,
		power: 0,
		road: 19709.0425,
		acceleration: 0
	},
	{
		id: 1938,
		time: 1937,
		velocity: 0,
		power: 0,
		road: 19709.0425,
		acceleration: 0
	},
	{
		id: 1939,
		time: 1938,
		velocity: 0,
		power: 0,
		road: 19709.0425,
		acceleration: 0
	},
	{
		id: 1940,
		time: 1939,
		velocity: 0,
		power: 0,
		road: 19709.0425,
		acceleration: 0
	},
	{
		id: 1941,
		time: 1940,
		velocity: 0,
		power: 0,
		road: 19709.0425,
		acceleration: 0
	},
	{
		id: 1942,
		time: 1941,
		velocity: 0,
		power: 0,
		road: 19709.0425,
		acceleration: 0
	},
	{
		id: 1943,
		time: 1942,
		velocity: 0,
		power: 0,
		road: 19709.0425,
		acceleration: 0
	},
	{
		id: 1944,
		time: 1943,
		velocity: 0,
		power: 0,
		road: 19709.0425,
		acceleration: 0
	},
	{
		id: 1945,
		time: 1944,
		velocity: 0,
		power: 2.85185022285842,
		road: 19709.0608333334,
		acceleration: 0.0366666666666667
	},
	{
		id: 1946,
		time: 1945,
		velocity: 0.11,
		power: 4.43001623519053,
		road: 19709.0975,
		acceleration: 0
	},
	{
		id: 1947,
		time: 1946,
		velocity: 0,
		power: 4.43001623519053,
		road: 19709.1341666667,
		acceleration: 0
	},
	{
		id: 1948,
		time: 1947,
		velocity: 0,
		power: 1.57815263157895,
		road: 19709.1525,
		acceleration: -0.0366666666666667
	},
	{
		id: 1949,
		time: 1948,
		velocity: 0,
		power: 0,
		road: 19709.1525,
		acceleration: 0
	},
	{
		id: 1950,
		time: 1949,
		velocity: 0,
		power: 0,
		road: 19709.1525,
		acceleration: 0
	},
	{
		id: 1951,
		time: 1950,
		velocity: 0,
		power: 0,
		road: 19709.1525,
		acceleration: 0
	},
	{
		id: 1952,
		time: 1951,
		velocity: 0,
		power: 0,
		road: 19709.1525,
		acceleration: 0
	},
	{
		id: 1953,
		time: 1952,
		velocity: 0,
		power: 0,
		road: 19709.1525,
		acceleration: 0
	},
	{
		id: 1954,
		time: 1953,
		velocity: 0,
		power: 2.22215764871132,
		road: 19709.1674074074,
		acceleration: 0.0298148148148148
	},
	{
		id: 1955,
		time: 1954,
		velocity: 0.0894444444444445,
		power: 3.60217754563576,
		road: 19709.1972222223,
		acceleration: 0
	},
	{
		id: 1956,
		time: 1955,
		velocity: 0,
		power: 3.60217754563576,
		road: 19709.2270370371,
		acceleration: 0
	},
	{
		id: 1957,
		time: 1956,
		velocity: 0,
		power: 1.38001270305393,
		road: 19709.2419444445,
		acceleration: -0.0298148148148148
	},
	{
		id: 1958,
		time: 1957,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1959,
		time: 1958,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1960,
		time: 1959,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1961,
		time: 1960,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1962,
		time: 1961,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1963,
		time: 1962,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1964,
		time: 1963,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1965,
		time: 1964,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1966,
		time: 1965,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1967,
		time: 1966,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1968,
		time: 1967,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1969,
		time: 1968,
		velocity: 0,
		power: 0,
		road: 19709.2419444445,
		acceleration: 0
	},
	{
		id: 1970,
		time: 1969,
		velocity: 0,
		power: 96.7810238750453,
		road: 19709.4382870371,
		acceleration: 0.392685185185185
	},
	{
		id: 1971,
		time: 1970,
		velocity: 1.17805555555556,
		power: 1106.58974970131,
		road: 19710.3840740741,
		acceleration: 1.1062037037037
	},
	{
		id: 1972,
		time: 1971,
		velocity: 3.31861111111111,
		power: 2348.87483792185,
		road: 19712.4247685186,
		acceleration: 1.08361111111111
	},
	{
		id: 1973,
		time: 1972,
		velocity: 3.25083333333333,
		power: 2597.16301687548,
		road: 19715.4008333334,
		acceleration: 0.78712962962963
	},
	{
		id: 1974,
		time: 1973,
		velocity: 3.53944444444444,
		power: 680.932620467857,
		road: 19718.80875,
		acceleration: 0.0765740740740739
	},
	{
		id: 1975,
		time: 1974,
		velocity: 3.54833333333333,
		power: 368.254927177601,
		road: 19722.2443981482,
		acceleration: -0.0211111111111109
	},
	{
		id: 1976,
		time: 1975,
		velocity: 3.1875,
		power: -485.39320615965,
		road: 19725.5248148149,
		acceleration: -0.289351851851852
	},
	{
		id: 1977,
		time: 1976,
		velocity: 2.67138888888889,
		power: -507.437620278504,
		road: 19728.5046296297,
		acceleration: -0.311851851851851
	},
	{
		id: 1978,
		time: 1977,
		velocity: 2.61277777777778,
		power: -266.400860198098,
		road: 19731.2108796297,
		acceleration: -0.235277777777778
	},
	{
		id: 1979,
		time: 1978,
		velocity: 2.48166666666667,
		power: 711.416749588726,
		road: 19733.8745370371,
		acceleration: 0.150092592592593
	},
	{
		id: 1980,
		time: 1979,
		velocity: 3.12166666666667,
		power: 2102.59400359955,
		road: 19736.9116666667,
		acceleration: 0.596851851851852
	},
	{
		id: 1981,
		time: 1980,
		velocity: 4.40333333333333,
		power: 3517.59441597272,
		road: 19740.6721296297,
		acceleration: 0.849814814814815
	},
	{
		id: 1982,
		time: 1981,
		velocity: 5.03111111111111,
		power: 3628.80778975472,
		road: 19745.2090277778,
		acceleration: 0.703055555555555
	},
	{
		id: 1983,
		time: 1982,
		velocity: 5.23083333333333,
		power: 1804.36879355022,
		road: 19750.2163425926,
		acceleration: 0.237777777777778
	},
	{
		id: 1984,
		time: 1983,
		velocity: 5.11666666666667,
		power: 651.22780971128,
		road: 19755.3383796297,
		acceleration: -0.00833333333333375
	},
	{
		id: 1985,
		time: 1984,
		velocity: 5.00611111111111,
		power: 170.041709833732,
		road: 19760.4030092593,
		acceleration: -0.106481481481481
	},
	{
		id: 1986,
		time: 1985,
		velocity: 4.91138888888889,
		power: -414.129275258105,
		road: 19765.2994444445,
		acceleration: -0.229907407407407
	},
	{
		id: 1987,
		time: 1986,
		velocity: 4.42694444444444,
		power: -1367.3022009495,
		road: 19769.8533333334,
		acceleration: -0.455185185185186
	},
	{
		id: 1988,
		time: 1987,
		velocity: 3.64055555555556,
		power: -1945.5611359686,
		road: 19773.8554166667,
		acceleration: -0.648425925925926
	},
	{
		id: 1989,
		time: 1988,
		velocity: 2.96611111111111,
		power: -1526.63517060275,
		road: 19777.2279166667,
		acceleration: -0.61074074074074
	},
	{
		id: 1990,
		time: 1989,
		velocity: 2.59472222222222,
		power: -1389.50043216534,
		road: 19779.9613425926,
		acceleration: -0.667407407407408
	},
	{
		id: 1991,
		time: 1990,
		velocity: 1.63833333333333,
		power: -958.53141054726,
		road: 19782.0547222223,
		acceleration: -0.612685185185185
	},
	{
		id: 1992,
		time: 1991,
		velocity: 1.12805555555556,
		power: -528.235461929204,
		road: 19783.596712963,
		acceleration: -0.490092592592593
	},
	{
		id: 1993,
		time: 1992,
		velocity: 1.12444444444444,
		power: -135.76438530024,
		road: 19784.768425926,
		acceleration: -0.250462962962963
	},
	{
		id: 1994,
		time: 1993,
		velocity: 0.886944444444444,
		power: 54.6468392356249,
		road: 19785.7793981482,
		acceleration: -0.0710185185185186
	},
	{
		id: 1995,
		time: 1994,
		velocity: 0.915,
		power: -7.98089357033573,
		road: 19786.68625,
		acceleration: -0.137222222222222
	},
	{
		id: 1996,
		time: 1995,
		velocity: 0.712777777777778,
		power: -109.852195715639,
		road: 19787.3766666667,
		acceleration: -0.295648148148148
	},
	{
		id: 1997,
		time: 1996,
		velocity: 0,
		power: -65.5741078262895,
		road: 19787.7667592593,
		acceleration: -0.305
	},
	{
		id: 1998,
		time: 1997,
		velocity: 0,
		power: -12.3868689733593,
		road: 19787.8855555556,
		acceleration: -0.237592592592593
	},
	{
		id: 1999,
		time: 1998,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2000,
		time: 1999,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2001,
		time: 2000,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2002,
		time: 2001,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2003,
		time: 2002,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2004,
		time: 2003,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2005,
		time: 2004,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2006,
		time: 2005,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2007,
		time: 2006,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2008,
		time: 2007,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2009,
		time: 2008,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2010,
		time: 2009,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2011,
		time: 2010,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2012,
		time: 2011,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2013,
		time: 2012,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2014,
		time: 2013,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2015,
		time: 2014,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2016,
		time: 2015,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2017,
		time: 2016,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2018,
		time: 2017,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2019,
		time: 2018,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2020,
		time: 2019,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2021,
		time: 2020,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2022,
		time: 2021,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2023,
		time: 2022,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2024,
		time: 2023,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2025,
		time: 2024,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2026,
		time: 2025,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2027,
		time: 2026,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2028,
		time: 2027,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2029,
		time: 2028,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2030,
		time: 2029,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2031,
		time: 2030,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2032,
		time: 2031,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2033,
		time: 2032,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2034,
		time: 2033,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2035,
		time: 2034,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2036,
		time: 2035,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2037,
		time: 2036,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2038,
		time: 2037,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2039,
		time: 2038,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2040,
		time: 2039,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2041,
		time: 2040,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2042,
		time: 2041,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2043,
		time: 2042,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2044,
		time: 2043,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2045,
		time: 2044,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2046,
		time: 2045,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2047,
		time: 2046,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2048,
		time: 2047,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2049,
		time: 2048,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2050,
		time: 2049,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2051,
		time: 2050,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2052,
		time: 2051,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2053,
		time: 2052,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2054,
		time: 2053,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2055,
		time: 2054,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2056,
		time: 2055,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2057,
		time: 2056,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2058,
		time: 2057,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2059,
		time: 2058,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2060,
		time: 2059,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2061,
		time: 2060,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2062,
		time: 2061,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2063,
		time: 2062,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2064,
		time: 2063,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2065,
		time: 2064,
		velocity: 0,
		power: 0,
		road: 19787.8855555556,
		acceleration: 0
	},
	{
		id: 2066,
		time: 2065,
		velocity: 0,
		power: 1.47153929687167,
		road: 19787.8960185186,
		acceleration: 0.0209259259259259
	},
	{
		id: 2067,
		time: 2066,
		velocity: 0.0627777777777778,
		power: 4.26744300296105,
		road: 19787.9266666667,
		acceleration: 0.0194444444444444
	},
	{
		id: 2068,
		time: 2067,
		velocity: 0.0583333333333333,
		power: 4.8774988756753,
		road: 19787.9670370371,
		acceleration: 0
	},
	{
		id: 2069,
		time: 2068,
		velocity: 0,
		power: 3.0204549039865,
		road: 19787.9969444445,
		acceleration: -0.0209259259259259
	},
	{
		id: 2070,
		time: 2069,
		velocity: 0,
		power: 0.99552485380117,
		road: 19788.0066666667,
		acceleration: -0.0194444444444444
	},
	{
		id: 2071,
		time: 2070,
		velocity: 0,
		power: 0,
		road: 19788.0066666667,
		acceleration: 0
	},
	{
		id: 2072,
		time: 2071,
		velocity: 0,
		power: 0,
		road: 19788.0066666667,
		acceleration: 0
	},
	{
		id: 2073,
		time: 2072,
		velocity: 0,
		power: 0,
		road: 19788.0066666667,
		acceleration: 0
	},
	{
		id: 2074,
		time: 2073,
		velocity: 0,
		power: 6.50290046919347,
		road: 19788.0414814815,
		acceleration: 0.0696296296296296
	},
	{
		id: 2075,
		time: 2074,
		velocity: 0.208888888888889,
		power: 159.25447647648,
		road: 19788.3343055556,
		acceleration: 0.446388888888889
	},
	{
		id: 2076,
		time: 2075,
		velocity: 1.33916666666667,
		power: 1026.1503858799,
		road: 19789.3299074074,
		acceleration: 0.959166666666667
	},
	{
		id: 2077,
		time: 2076,
		velocity: 2.8775,
		power: 4235.06125503379,
		road: 19791.686712963,
		acceleration: 1.76324074074074
	},
	{
		id: 2078,
		time: 2077,
		velocity: 5.49861111111111,
		power: 8053.40621128476,
		road: 19795.8699074075,
		acceleration: 1.88953703703704
	},
	{
		id: 2079,
		time: 2078,
		velocity: 7.00777777777778,
		power: 11621.5948549897,
		road: 19801.9318981482,
		acceleration: 1.86805555555556
	},
	{
		id: 2080,
		time: 2079,
		velocity: 8.48166666666667,
		power: 10488.3642732763,
		road: 19809.5693055556,
		acceleration: 1.28277777777778
	},
	{
		id: 2081,
		time: 2080,
		velocity: 9.34694444444444,
		power: 8610.27381688268,
		road: 19818.2819444445,
		acceleration: 0.867685185185186
	},
	{
		id: 2082,
		time: 2081,
		velocity: 9.61083333333333,
		power: 4588.43092434558,
		road: 19827.5987962963,
		acceleration: 0.34074074074074
	},
	{
		id: 2083,
		time: 2082,
		velocity: 9.50388888888889,
		power: 2288.32031657918,
		road: 19837.1228703704,
		acceleration: 0.0737037037037016
	},
	{
		id: 2084,
		time: 2083,
		velocity: 9.56805555555555,
		power: 2783.75564006313,
		road: 19846.7458333334,
		acceleration: 0.124074074074077
	},
	{
		id: 2085,
		time: 2084,
		velocity: 9.98305555555556,
		power: 4080.55798629266,
		road: 19856.5582407408,
		acceleration: 0.254814814814814
	},
	{
		id: 2086,
		time: 2085,
		velocity: 10.2683333333333,
		power: 5491.36986000055,
		road: 19866.6898148149,
		acceleration: 0.383518518518519
	},
	{
		id: 2087,
		time: 2086,
		velocity: 10.7186111111111,
		power: 4700.17251209189,
		road: 19877.154212963,
		acceleration: 0.282129629629628
	},
	{
		id: 2088,
		time: 2087,
		velocity: 10.8294444444444,
		power: 5281.4766921489,
		road: 19887.9206018519,
		acceleration: 0.321851851851854
	},
	{
		id: 2089,
		time: 2088,
		velocity: 11.2338888888889,
		power: 2688.40537892812,
		road: 19898.8790277778,
		acceleration: 0.0622222222222213
	},
	{
		id: 2090,
		time: 2089,
		velocity: 10.9052777777778,
		power: 2600.01781299914,
		road: 19909.8944444445,
		acceleration: 0.0517592592592582
	},
	{
		id: 2091,
		time: 2090,
		velocity: 10.9847222222222,
		power: 1137.33166292341,
		road: 19920.892175926,
		acceleration: -0.0871296296296293
	},
	{
		id: 2092,
		time: 2091,
		velocity: 10.9725,
		power: 2340.72643053873,
		road: 19931.8606481482,
		acceleration: 0.0286111111111111
	},
	{
		id: 2093,
		time: 2092,
		velocity: 10.9911111111111,
		power: 1412.06287087853,
		road: 19942.8135185186,
		acceleration: -0.0598148148148141
	},
	{
		id: 2094,
		time: 2093,
		velocity: 10.8052777777778,
		power: -1193.48877245136,
		road: 19953.5819444445,
		acceleration: -0.309074074074074
	},
	{
		id: 2095,
		time: 2094,
		velocity: 10.0452777777778,
		power: -5311.44134725067,
		road: 19963.8305555556,
		acceleration: -0.730555555555554
	},
	{
		id: 2096,
		time: 2095,
		velocity: 8.79944444444445,
		power: -9030.33323800206,
		road: 19973.1151851852,
		acceleration: -1.19740740740741
	},
	{
		id: 2097,
		time: 2096,
		velocity: 7.21305555555556,
		power: -11708.1895784585,
		road: 19980.9331018519,
		acceleration: -1.73601851851852
	},
	{
		id: 2098,
		time: 2097,
		velocity: 4.83722222222222,
		power: -10444.0124814475,
		road: 19986.8862962963,
		acceleration: -1.99342592592593
	},
	{
		id: 2099,
		time: 2098,
		velocity: 2.81916666666667,
		power: -6965.81364137148,
		road: 19990.8486574075,
		acceleration: -1.98824074074074
	},
	{
		id: 2100,
		time: 2099,
		velocity: 1.24833333333333,
		power: -3039.23603177028,
		road: 19993.0106944445,
		acceleration: -1.61240740740741
	},
	{
		id: 2101,
		time: 2100,
		velocity: 0,
		power: -681.623850775628,
		road: 19993.8966666667,
		acceleration: -0.939722222222222
	},
	{
		id: 2102,
		time: 2101,
		velocity: 0,
		power: -56.8808558479532,
		road: 19994.1047222223,
		acceleration: -0.416111111111111
	},
	{
		id: 2103,
		time: 2102,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2104,
		time: 2103,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2105,
		time: 2104,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2106,
		time: 2105,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2107,
		time: 2106,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2108,
		time: 2107,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2109,
		time: 2108,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2110,
		time: 2109,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2111,
		time: 2110,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2112,
		time: 2111,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2113,
		time: 2112,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2114,
		time: 2113,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2115,
		time: 2114,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2116,
		time: 2115,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2117,
		time: 2116,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2118,
		time: 2117,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2119,
		time: 2118,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2120,
		time: 2119,
		velocity: 0,
		power: 0,
		road: 19994.1047222223,
		acceleration: 0
	},
	{
		id: 2121,
		time: 2120,
		velocity: 0,
		power: 2.37123231564625,
		road: 19994.120462963,
		acceleration: 0.0314814814814815
	},
	{
		id: 2122,
		time: 2121,
		velocity: 0.0944444444444444,
		power: 3.80354325377464,
		road: 19994.1519444445,
		acceleration: 0
	},
	{
		id: 2123,
		time: 2122,
		velocity: 0,
		power: 3.80354325377464,
		road: 19994.183425926,
		acceleration: 0
	},
	{
		id: 2124,
		time: 2123,
		velocity: 0,
		power: 1.4323024691358,
		road: 19994.1991666667,
		acceleration: -0.0314814814814815
	},
	{
		id: 2125,
		time: 2124,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2126,
		time: 2125,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2127,
		time: 2126,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2128,
		time: 2127,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2129,
		time: 2128,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2130,
		time: 2129,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2131,
		time: 2130,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2132,
		time: 2131,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2133,
		time: 2132,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2134,
		time: 2133,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2135,
		time: 2134,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2136,
		time: 2135,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2137,
		time: 2136,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2138,
		time: 2137,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2139,
		time: 2138,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2140,
		time: 2139,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2141,
		time: 2140,
		velocity: 0,
		power: 0,
		road: 19994.1991666667,
		acceleration: 0
	},
	{
		id: 2142,
		time: 2141,
		velocity: 0,
		power: 152.24250476032,
		road: 19994.4525000001,
		acceleration: 0.506666666666667
	},
	{
		id: 2143,
		time: 2142,
		velocity: 1.52,
		power: 1889.55220857355,
		road: 19995.6961574075,
		acceleration: 1.47398148148148
	},
	{
		id: 2144,
		time: 2143,
		velocity: 4.42194444444444,
		power: 5895.29306092538,
		road: 19998.6587962963,
		acceleration: 1.96398148148148
	},
	{
		id: 2145,
		time: 2144,
		velocity: 5.89194444444444,
		power: 7336.44904952905,
		road: 20003.3555555556,
		acceleration: 1.50425925925926
	},
	{
		id: 2146,
		time: 2145,
		velocity: 6.03277777777778,
		power: 5311.53190778523,
		road: 20009.208425926,
		acceleration: 0.807962962962963
	},
	{
		id: 2147,
		time: 2146,
		velocity: 6.84583333333333,
		power: 5185.64542092217,
		road: 20015.8027777778,
		acceleration: 0.675000000000001
	},
	{
		id: 2148,
		time: 2147,
		velocity: 7.91694444444444,
		power: 5067.34531863402,
		road: 20023.0250000001,
		acceleration: 0.58074074074074
	},
	{
		id: 2149,
		time: 2148,
		velocity: 7.775,
		power: 2916.99787271779,
		road: 20030.6582870371,
		acceleration: 0.241388888888889
	},
	{
		id: 2150,
		time: 2149,
		velocity: 7.57,
		power: 384.377221943249,
		road: 20038.3581018519,
		acceleration: -0.108333333333334
	},
	{
		id: 2151,
		time: 2150,
		velocity: 7.59194444444444,
		power: 1045.48860834015,
		road: 20045.9955555556,
		acceleration: -0.0163888888888879
	},
	{
		id: 2152,
		time: 2151,
		velocity: 7.72583333333333,
		power: 2467.52725461101,
		road: 20053.7123611112,
		acceleration: 0.175092592592592
	},
	{
		id: 2153,
		time: 2152,
		velocity: 8.09527777777778,
		power: 3806.79407434208,
		road: 20061.685925926,
		acceleration: 0.338425925925927
	},
	{
		id: 2154,
		time: 2153,
		velocity: 8.60722222222222,
		power: 7962.35133188745,
		road: 20070.2336111112,
		acceleration: 0.809814814814814
	},
	{
		id: 2155,
		time: 2154,
		velocity: 10.1552777777778,
		power: 11059.8078614076,
		road: 20079.7097685186,
		acceleration: 1.04712962962963
	},
	{
		id: 2156,
		time: 2155,
		velocity: 11.2366666666667,
		power: 13490.1929385434,
		road: 20090.2834722223,
		acceleration: 1.14796296296296
	},
	{
		id: 2157,
		time: 2156,
		velocity: 12.0511111111111,
		power: 7198.4291385376,
		road: 20101.6626388889,
		acceleration: 0.462962962962962
	},
	{
		id: 2158,
		time: 2157,
		velocity: 11.5441666666667,
		power: 2107.85405201778,
		road: 20113.2668518519,
		acceleration: -0.0128703703703685
	},
	{
		id: 2159,
		time: 2158,
		velocity: 11.1980555555556,
		power: -3631.89459623556,
		road: 20124.5966203704,
		acceleration: -0.536018518518521
	},
	{
		id: 2160,
		time: 2159,
		velocity: 10.4430555555556,
		power: -2203.18065554811,
		road: 20135.4550000001,
		acceleration: -0.406759259259257
	},
	{
		id: 2161,
		time: 2160,
		velocity: 10.3238888888889,
		power: -1624.91910412721,
		road: 20145.9339814815,
		acceleration: -0.352037037037036
	},
	{
		id: 2162,
		time: 2161,
		velocity: 10.1419444444444,
		power: -654.894186803464,
		road: 20156.1102777778,
		acceleration: -0.253333333333334
	},
	{
		id: 2163,
		time: 2162,
		velocity: 9.68305555555556,
		power: -1405.60055088855,
		road: 20165.9940277778,
		acceleration: -0.331759259259259
	},
	{
		id: 2164,
		time: 2163,
		velocity: 9.32861111111111,
		power: -3053.37082206025,
		road: 20175.4535185186,
		acceleration: -0.51675925925926
	},
	{
		id: 2165,
		time: 2164,
		velocity: 8.59166666666667,
		power: -2692.4846146831,
		road: 20184.410462963,
		acceleration: -0.488333333333335
	},
	{
		id: 2166,
		time: 2165,
		velocity: 8.21805555555556,
		power: -3000.27885850044,
		road: 20192.8527777778,
		acceleration: -0.540925925925924
	},
	{
		id: 2167,
		time: 2166,
		velocity: 7.70583333333333,
		power: -2263.66687922863,
		road: 20200.7933796297,
		acceleration: -0.462499999999999
	},
	{
		id: 2168,
		time: 2167,
		velocity: 7.20416666666667,
		power: -2978.81490449996,
		road: 20208.2125000001,
		acceleration: -0.580462962962964
	},
	{
		id: 2169,
		time: 2168,
		velocity: 6.47666666666667,
		power: -4490.42809547916,
		road: 20214.912638889,
		acceleration: -0.857499999999999
	},
	{
		id: 2170,
		time: 2169,
		velocity: 5.13333333333333,
		power: -5451.3961896149,
		road: 20220.6075462964,
		acceleration: -1.15296296296296
	},
	{
		id: 2171,
		time: 2170,
		velocity: 3.74527777777778,
		power: -4968.01573062202,
		road: 20225.0705555556,
		acceleration: -1.31083333333333
	},
	{
		id: 2172,
		time: 2171,
		velocity: 2.54416666666667,
		power: -3662.15311025484,
		road: 20228.1938425927,
		acceleration: -1.36861111111111
	},
	{
		id: 2173,
		time: 2172,
		velocity: 1.0275,
		power: -1738.3328866279,
		road: 20230.0828703704,
		acceleration: -1.09990740740741
	},
	{
		id: 2174,
		time: 2173,
		velocity: 0.445555555555556,
		power: -624.493725050021,
		road: 20230.9979166667,
		acceleration: -0.848055555555556
	},
	{
		id: 2175,
		time: 2174,
		velocity: 0,
		power: -65.1188810187827,
		road: 20231.3176851853,
		acceleration: -0.3425
	},
	{
		id: 2176,
		time: 2175,
		velocity: 0,
		power: -1.47656068875893,
		road: 20231.3919444445,
		acceleration: -0.148518518518519
	},
	{
		id: 2177,
		time: 2176,
		velocity: 0,
		power: 0,
		road: 20231.3919444445,
		acceleration: 0
	},
	{
		id: 2178,
		time: 2177,
		velocity: 0,
		power: 0,
		road: 20231.3919444445,
		acceleration: 0
	},
	{
		id: 2179,
		time: 2178,
		velocity: 0,
		power: 0,
		road: 20231.3919444445,
		acceleration: 0
	},
	{
		id: 2180,
		time: 2179,
		velocity: 0,
		power: 0,
		road: 20231.3919444445,
		acceleration: 0
	},
	{
		id: 2181,
		time: 2180,
		velocity: 0,
		power: 0,
		road: 20231.3919444445,
		acceleration: 0
	},
	{
		id: 2182,
		time: 2181,
		velocity: 0,
		power: 0,
		road: 20231.3919444445,
		acceleration: 0
	},
	{
		id: 2183,
		time: 2182,
		velocity: 0,
		power: 0,
		road: 20231.3919444445,
		acceleration: 0
	},
	{
		id: 2184,
		time: 2183,
		velocity: 0,
		power: 0,
		road: 20231.3919444445,
		acceleration: 0
	},
	{
		id: 2185,
		time: 2184,
		velocity: 0,
		power: 0,
		road: 20231.3919444445,
		acceleration: 0
	},
	{
		id: 2186,
		time: 2185,
		velocity: 0,
		power: 2.40471741340384,
		road: 20231.4078703704,
		acceleration: 0.0318518518518518
	},
	{
		id: 2187,
		time: 2186,
		velocity: 0.0955555555555555,
		power: 3.84829122706758,
		road: 20231.4397222223,
		acceleration: 0
	},
	{
		id: 2188,
		time: 2187,
		velocity: 0,
		power: 3.84829122706758,
		road: 20231.4715740741,
		acceleration: 0
	},
	{
		id: 2189,
		time: 2188,
		velocity: 0,
		power: 4.3928278288822,
		road: 20231.5063425927,
		acceleration: 0.00583333333333334
	},
	{
		id: 2190,
		time: 2189,
		velocity: 0.113055555555556,
		power: 4.55307379099547,
		road: 20231.5440277778,
		acceleration: 0
	},
	{
		id: 2191,
		time: 2190,
		velocity: 0,
		power: 4.55307379099547,
		road: 20231.581712963,
		acceleration: 0
	},
	{
		id: 2192,
		time: 2191,
		velocity: 0,
		power: 1.60380875568551,
		road: 20231.6005555556,
		acceleration: -0.0376851851851852
	},
	{
		id: 2193,
		time: 2192,
		velocity: 0,
		power: 0,
		road: 20231.6005555556,
		acceleration: 0
	},
	{
		id: 2194,
		time: 2193,
		velocity: 0,
		power: 263.469046092923,
		road: 20231.9428703704,
		acceleration: 0.68462962962963
	},
	{
		id: 2195,
		time: 2194,
		velocity: 2.05388888888889,
		power: 1538.85457187001,
		road: 20233.2058333334,
		acceleration: 1.15666666666667
	},
	{
		id: 2196,
		time: 2195,
		velocity: 3.47,
		power: 3539.86265994201,
		road: 20235.7227314815,
		acceleration: 1.3512037037037
	},
	{
		id: 2197,
		time: 2196,
		velocity: 4.05361111111111,
		power: 4060.0256082028,
		road: 20239.425138889,
		acceleration: 1.01981481481482
	},
	{
		id: 2198,
		time: 2197,
		velocity: 5.11333333333333,
		power: 3898.49310377651,
		road: 20244.0149074075,
		acceleration: 0.754907407407407
	},
	{
		id: 2199,
		time: 2198,
		velocity: 5.73472222222222,
		power: 3619.62927099447,
		road: 20249.2728703704,
		acceleration: 0.581481481481482
	},
	{
		id: 2200,
		time: 2199,
		velocity: 5.79805555555556,
		power: 3101.68687652487,
		road: 20255.0318518519,
		acceleration: 0.420555555555556
	},
	{
		id: 2201,
		time: 2200,
		velocity: 6.375,
		power: 3326.21473161873,
		road: 20261.2098148149,
		acceleration: 0.417407407407407
	},
	{
		id: 2202,
		time: 2201,
		velocity: 6.98694444444444,
		power: 5026.90245440956,
		road: 20267.9143055556,
		acceleration: 0.635648148148149
	},
	{
		id: 2203,
		time: 2202,
		velocity: 7.705,
		power: 5968.71102674083,
		road: 20275.2832870371,
		acceleration: 0.693333333333332
	},
	{
		id: 2204,
		time: 2203,
		velocity: 8.455,
		power: 7410.47084644698,
		road: 20283.3964814815,
		acceleration: 0.795092592592594
	},
	{
		id: 2205,
		time: 2204,
		velocity: 9.37222222222222,
		power: 7395.90057673979,
		road: 20292.2595370371,
		acceleration: 0.704629629629629
	},
	{
		id: 2206,
		time: 2205,
		velocity: 9.81888888888889,
		power: 4718.87975959113,
		road: 20301.650138889,
		acceleration: 0.350462962962961
	},
	{
		id: 2207,
		time: 2206,
		velocity: 9.50638888888889,
		power: 2183.16387936652,
		road: 20311.2457407408,
		acceleration: 0.0595370370370389
	},
	{
		id: 2208,
		time: 2207,
		velocity: 9.55083333333333,
		power: 2395.39302849832,
		road: 20320.9111574075,
		acceleration: 0.0800925925925924
	},
	{
		id: 2209,
		time: 2208,
		velocity: 10.0591666666667,
		power: 4785.6560112747,
		road: 20330.7799537038,
		acceleration: 0.326666666666664
	},
	{
		id: 2210,
		time: 2209,
		velocity: 10.4863888888889,
		power: 4597.6302220947,
		road: 20340.9562500001,
		acceleration: 0.288333333333334
	},
	{
		id: 2211,
		time: 2210,
		velocity: 10.4158333333333,
		power: 3023.25724907581,
		road: 20351.335462963,
		acceleration: 0.117500000000001
	},
	{
		id: 2212,
		time: 2211,
		velocity: 10.4116666666667,
		power: -386.162883322425,
		road: 20361.6600462964,
		acceleration: -0.226759259259261
	},
	{
		id: 2213,
		time: 2212,
		velocity: 9.80611111111111,
		power: -1356.4473371774,
		road: 20371.7082407408,
		acceleration: -0.326018518518516
	},
	{
		id: 2214,
		time: 2213,
		velocity: 9.43777777777778,
		power: -4821.70466927649,
		road: 20381.2385185186,
		acceleration: -0.709814814814816
	},
	{
		id: 2215,
		time: 2214,
		velocity: 8.28222222222222,
		power: -6060.96473112434,
		road: 20389.963888889,
		acceleration: -0.9
	},
	{
		id: 2216,
		time: 2215,
		velocity: 7.10611111111111,
		power: -11085.7533258707,
		road: 20397.3736111112,
		acceleration: -1.7312962962963
	},
	{
		id: 2217,
		time: 2216,
		velocity: 4.24388888888889,
		power: -8752.73646963614,
		road: 20403.0308333334,
		acceleration: -1.7737037037037
	},
	{
		id: 2218,
		time: 2217,
		velocity: 2.96111111111111,
		power: -5777.76471811483,
		road: 20406.9582870371,
		acceleration: -1.68583333333333
	},
	{
		id: 2219,
		time: 2218,
		velocity: 2.04861111111111,
		power: -1662.8058026102,
		road: 20409.651712963,
		acceleration: -0.782222222222222
	},
	{
		id: 2220,
		time: 2219,
		velocity: 1.89722222222222,
		power: -592.615716002043,
		road: 20411.7394444445,
		acceleration: -0.429166666666667
	},
	{
		id: 2221,
		time: 2220,
		velocity: 1.67361111111111,
		power: 260.328453504552,
		road: 20413.6208333334,
		acceleration: 0.0164814814814813
	},
	{
		id: 2222,
		time: 2221,
		velocity: 2.09805555555556,
		power: 356.707920584828,
		road: 20415.5435185186,
		acceleration: 0.0661111111111108
	},
	{
		id: 2223,
		time: 2222,
		velocity: 2.09555555555556,
		power: 490.604731772237,
		road: 20417.5625000001,
		acceleration: 0.126481481481482
	},
	{
		id: 2224,
		time: 2223,
		velocity: 2.05305555555556,
		power: 235.565283328992,
		road: 20419.6395833334,
		acceleration: -0.0102777777777776
	},
	{
		id: 2225,
		time: 2224,
		velocity: 2.06722222222222,
		power: 272.476387460362,
		road: 20421.7157870371,
		acceleration: 0.00851851851851837
	},
	{
		id: 2226,
		time: 2225,
		velocity: 2.12111111111111,
		power: 223.921313596601,
		road: 20423.7882870371,
		acceleration: -0.0159259259259263
	},
	{
		id: 2227,
		time: 2226,
		velocity: 2.00527777777778,
		power: 93.5087658936945,
		road: 20425.8123148149,
		acceleration: -0.0810185185185184
	},
	{
		id: 2228,
		time: 2227,
		velocity: 1.82416666666667,
		power: -185.294959629501,
		road: 20427.6787962964,
		acceleration: -0.234074074074074
	},
	{
		id: 2229,
		time: 2228,
		velocity: 1.41888888888889,
		power: -164.399823021265,
		road: 20429.3106481482,
		acceleration: -0.235185185185185
	},
	{
		id: 2230,
		time: 2229,
		velocity: 1.29972222222222,
		power: -233.900005391475,
		road: 20430.6699074075,
		acceleration: -0.31
	},
	{
		id: 2231,
		time: 2230,
		velocity: 0.894166666666667,
		power: -6.84131400601397,
		road: 20431.8068981482,
		acceleration: -0.134537037037037
	},
	{
		id: 2232,
		time: 2231,
		velocity: 1.01527777777778,
		power: 914.775028887661,
		road: 20433.1670370371,
		acceleration: 0.580833333333333
	},
	{
		id: 2233,
		time: 2232,
		velocity: 3.04222222222222,
		power: 2348.76128465008,
		road: 20435.3259722223,
		acceleration: 1.01675925925926
	},
	{
		id: 2234,
		time: 2233,
		velocity: 3.94444444444444,
		power: 4810.23703792215,
		road: 20438.6814351852,
		acceleration: 1.3762962962963
	},
	{
		id: 2235,
		time: 2234,
		velocity: 5.14416666666667,
		power: 3267.6778070086,
		road: 20443.0497685186,
		acceleration: 0.649444444444445
	},
	{
		id: 2236,
		time: 2235,
		velocity: 4.99055555555556,
		power: 2613.77779223796,
		road: 20447.9529166667,
		acceleration: 0.420185185185185
	},
	{
		id: 2237,
		time: 2236,
		velocity: 5.205,
		power: 3094.59393487353,
		road: 20453.2989814815,
		acceleration: 0.465648148148148
	},
	{
		id: 2238,
		time: 2237,
		velocity: 6.54111111111111,
		power: 4706.04382185711,
		road: 20459.222175926,
		acceleration: 0.688611111111111
	},
	{
		id: 2239,
		time: 2238,
		velocity: 7.05638888888889,
		power: 7514.63492760773,
		road: 20465.9962037038,
		acceleration: 1.01305555555556
	},
	{
		id: 2240,
		time: 2239,
		velocity: 8.24416666666667,
		power: 8428.46881570466,
		road: 20473.7659722223,
		acceleration: 0.978425925925928
	},
	{
		id: 2241,
		time: 2240,
		velocity: 9.47638888888889,
		power: 13546.5453838077,
		road: 20482.7318055556,
		acceleration: 1.4137037037037
	},
	{
		id: 2242,
		time: 2241,
		velocity: 11.2975,
		power: 11850.4538768695,
		road: 20492.9216666667,
		acceleration: 1.03435185185185
	},
	{
		id: 2243,
		time: 2242,
		velocity: 11.3472222222222,
		power: 8280.53906371565,
		road: 20503.9255092593,
		acceleration: 0.593611111111111
	},
	{
		id: 2244,
		time: 2243,
		velocity: 11.2572222222222,
		power: 404.873226428748,
		road: 20515.1458796297,
		acceleration: -0.160555555555556
	},
	{
		id: 2245,
		time: 2244,
		velocity: 10.8158333333333,
		power: -566.900160144594,
		road: 20526.1610648149,
		acceleration: -0.249814814814815
	},
	{
		id: 2246,
		time: 2245,
		velocity: 10.5977777777778,
		power: -316.307999147551,
		road: 20536.9394907408,
		acceleration: -0.223703703703706
	},
	{
		id: 2247,
		time: 2246,
		velocity: 10.5861111111111,
		power: 1545.988174849,
		road: 20547.5865740741,
		acceleration: -0.0389814814814802
	},
	{
		id: 2248,
		time: 2247,
		velocity: 10.6988888888889,
		power: 3599.9317820332,
		road: 20558.2944907408,
		acceleration: 0.160648148148148
	},
	{
		id: 2249,
		time: 2248,
		velocity: 11.0797222222222,
		power: 3778.57698709313,
		road: 20569.167962963,
		acceleration: 0.170462962962963
	},
	{
		id: 2250,
		time: 2249,
		velocity: 11.0975,
		power: 5294.04093623729,
		road: 20580.2780555556,
		acceleration: 0.302777777777779
	},
	{
		id: 2251,
		time: 2250,
		velocity: 11.6072222222222,
		power: 3651.18039786169,
		road: 20591.6086111112,
		acceleration: 0.138148148148147
	},
	{
		id: 2252,
		time: 2251,
		velocity: 11.4941666666667,
		power: 2291.90029454904,
		road: 20603.0132407408,
		acceleration: 0.00999999999999979
	},
	{
		id: 2253,
		time: 2252,
		velocity: 11.1275,
		power: -449.29483466669,
		road: 20614.3023611112,
		acceleration: -0.241018518518517
	},
	{
		id: 2254,
		time: 2253,
		velocity: 10.8841666666667,
		power: 383.491999494058,
		road: 20625.3907407408,
		acceleration: -0.160462962962965
	},
	{
		id: 2255,
		time: 2254,
		velocity: 11.0127777777778,
		power: 1092.40617586283,
		road: 20636.3535648149,
		acceleration: -0.0906481481481478
	},
	{
		id: 2256,
		time: 2255,
		velocity: 10.8555555555556,
		power: 1397.189323445,
		road: 20647.2412500001,
		acceleration: -0.0596296296296277
	},
	{
		id: 2257,
		time: 2256,
		velocity: 10.7052777777778,
		power: 714.434313685835,
		road: 20658.0372685186,
		acceleration: -0.123703703703704
	},
	{
		id: 2258,
		time: 2257,
		velocity: 10.6416666666667,
		power: 716.618293268501,
		road: 20668.7108333334,
		acceleration: -0.121203703703705
	},
	{
		id: 2259,
		time: 2258,
		velocity: 10.4919444444444,
		power: 1960.65431582736,
		road: 20679.3252314815,
		acceleration: 0.00287037037037052
	},
	{
		id: 2260,
		time: 2259,
		velocity: 10.7138888888889,
		power: 3386.17246085724,
		road: 20690.0113888889,
		acceleration: 0.140648148148149
	},
	{
		id: 2261,
		time: 2260,
		velocity: 11.0636111111111,
		power: 5743.09486026835,
		road: 20700.945925926,
		acceleration: 0.35611111111111
	},
	{
		id: 2262,
		time: 2261,
		velocity: 11.5602777777778,
		power: 7831.75121123618,
		road: 20712.3193981482,
		acceleration: 0.521759259259261
	},
	{
		id: 2263,
		time: 2262,
		velocity: 12.2791666666667,
		power: 10289.0215276334,
		road: 20724.2997222223,
		acceleration: 0.691944444444445
	},
	{
		id: 2264,
		time: 2263,
		velocity: 13.1394444444444,
		power: 14595.6628095038,
		road: 20737.1127777778,
		acceleration: 0.973518518518517
	},
	{
		id: 2265,
		time: 2264,
		velocity: 14.4808333333333,
		power: 12616.8940676588,
		road: 20750.7796296297,
		acceleration: 0.734074074074075
	},
	{
		id: 2266,
		time: 2265,
		velocity: 14.4813888888889,
		power: 8958.46306877213,
		road: 20765.0218981482,
		acceleration: 0.416759259259258
	},
	{
		id: 2267,
		time: 2266,
		velocity: 14.3897222222222,
		power: 2728.91915559274,
		road: 20779.4491666667,
		acceleration: -0.0467592592592592
	},
	{
		id: 2268,
		time: 2267,
		velocity: 14.3405555555556,
		power: 2036.85468028812,
		road: 20793.8055092593,
		acceleration: -0.095092592592593
	},
	{
		id: 2269,
		time: 2268,
		velocity: 14.1961111111111,
		power: 1958.92202822063,
		road: 20808.0651851852,
		acceleration: -0.0982407407407422
	},
	{
		id: 2270,
		time: 2269,
		velocity: 14.095,
		power: 2023.01242853122,
		road: 20822.2302314815,
		acceleration: -0.0910185185185171
	},
	{
		id: 2271,
		time: 2270,
		velocity: 14.0675,
		power: -2.64347086785385,
		road: 20836.2306944445,
		acceleration: -0.238148148148149
	},
	{
		id: 2272,
		time: 2271,
		velocity: 13.4816666666667,
		power: -3959.21873822918,
		road: 20849.8437962963,
		acceleration: -0.536574074074071
	},
	{
		id: 2273,
		time: 2272,
		velocity: 12.4852777777778,
		power: -8122.12000253001,
		road: 20862.7481481482,
		acceleration: -0.880925925925927
	},
	{
		id: 2274,
		time: 2273,
		velocity: 11.4247222222222,
		power: -7573.90286669651,
		road: 20874.7774537038,
		acceleration: -0.869166666666667
	},
	{
		id: 2275,
		time: 2274,
		velocity: 10.8741666666667,
		power: -7643.92937060116,
		road: 20885.9134722223,
		acceleration: -0.917407407407408
	},
	{
		id: 2276,
		time: 2275,
		velocity: 9.73305555555556,
		power: -5218.70635429274,
		road: 20896.2316666667,
		acceleration: -0.71824074074074
	},
	{
		id: 2277,
		time: 2276,
		velocity: 9.27,
		power: -5175.10829351151,
		road: 20905.8177314815,
		acceleration: -0.74601851851852
	},
	{
		id: 2278,
		time: 2277,
		velocity: 8.63611111111111,
		power: -3774.53629075208,
		road: 20914.7221296297,
		acceleration: -0.617314814814813
	},
	{
		id: 2279,
		time: 2278,
		velocity: 7.88111111111111,
		power: -4025.29713878937,
		road: 20922.9788425926,
		acceleration: -0.678055555555556
	},
	{
		id: 2280,
		time: 2279,
		velocity: 7.23583333333333,
		power: -6514.55244636241,
		road: 20930.3531481482,
		acceleration: -1.08675925925926
	},
	{
		id: 2281,
		time: 2280,
		velocity: 5.37583333333333,
		power: -6891.29844152094,
		road: 20936.5220370371,
		acceleration: -1.32407407407407
	},
	{
		id: 2282,
		time: 2281,
		velocity: 3.90888888888889,
		power: -7273.59051642334,
		road: 20941.1276851852,
		acceleration: -1.80240740740741
	},
	{
		id: 2283,
		time: 2282,
		velocity: 1.82861111111111,
		power: -3471.401727152,
		road: 20944.1631944445,
		acceleration: -1.33787037037037
	},
	{
		id: 2284,
		time: 2283,
		velocity: 1.36222222222222,
		power: -1712.55842580144,
		road: 20945.9635185186,
		acceleration: -1.1325
	},
	{
		id: 2285,
		time: 2284,
		velocity: 0.511388888888889,
		power: -424.159682951536,
		road: 20946.8928240741,
		acceleration: -0.609537037037037
	},
	{
		id: 2286,
		time: 2285,
		velocity: 0,
		power: -122.963353309963,
		road: 20947.2903240741,
		acceleration: -0.454074074074074
	},
	{
		id: 2287,
		time: 2286,
		velocity: 0,
		power: -3.46664845679012,
		road: 20947.3755555556,
		acceleration: -0.170462962962963
	},
	{
		id: 2288,
		time: 2287,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2289,
		time: 2288,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2290,
		time: 2289,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2291,
		time: 2290,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2292,
		time: 2291,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2293,
		time: 2292,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2294,
		time: 2293,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2295,
		time: 2294,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2296,
		time: 2295,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2297,
		time: 2296,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2298,
		time: 2297,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2299,
		time: 2298,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2300,
		time: 2299,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2301,
		time: 2300,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2302,
		time: 2301,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2303,
		time: 2302,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2304,
		time: 2303,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2305,
		time: 2304,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2306,
		time: 2305,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2307,
		time: 2306,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2308,
		time: 2307,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2309,
		time: 2308,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2310,
		time: 2309,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2311,
		time: 2310,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2312,
		time: 2311,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2313,
		time: 2312,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2314,
		time: 2313,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2315,
		time: 2314,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2316,
		time: 2315,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2317,
		time: 2316,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2318,
		time: 2317,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2319,
		time: 2318,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2320,
		time: 2319,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2321,
		time: 2320,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2322,
		time: 2321,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2323,
		time: 2322,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2324,
		time: 2323,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2325,
		time: 2324,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2326,
		time: 2325,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2327,
		time: 2326,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2328,
		time: 2327,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2329,
		time: 2328,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2330,
		time: 2329,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2331,
		time: 2330,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2332,
		time: 2331,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2333,
		time: 2332,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2334,
		time: 2333,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2335,
		time: 2334,
		velocity: 0,
		power: 0,
		road: 20947.3755555556,
		acceleration: 0
	},
	{
		id: 2336,
		time: 2335,
		velocity: 0,
		power: 2.32124834688838,
		road: 20947.3910185186,
		acceleration: 0.0309259259259259
	},
	{
		id: 2337,
		time: 2336,
		velocity: 0.0927777777777778,
		power: 3.73642132015753,
		road: 20947.4219444445,
		acceleration: 0
	},
	{
		id: 2338,
		time: 2337,
		velocity: 0,
		power: 3.73642132015753,
		road: 20947.4528703704,
		acceleration: 0
	},
	{
		id: 2339,
		time: 2338,
		velocity: 0,
		power: 1.41516494476933,
		road: 20947.4683333334,
		acceleration: -0.0309259259259259
	},
	{
		id: 2340,
		time: 2339,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2341,
		time: 2340,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2342,
		time: 2341,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2343,
		time: 2342,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2344,
		time: 2343,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2345,
		time: 2344,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2346,
		time: 2345,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2347,
		time: 2346,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2348,
		time: 2347,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2349,
		time: 2348,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2350,
		time: 2349,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2351,
		time: 2350,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2352,
		time: 2351,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2353,
		time: 2352,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2354,
		time: 2353,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2355,
		time: 2354,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2356,
		time: 2355,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2357,
		time: 2356,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2358,
		time: 2357,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2359,
		time: 2358,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2360,
		time: 2359,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2361,
		time: 2360,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2362,
		time: 2361,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2363,
		time: 2362,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2364,
		time: 2363,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2365,
		time: 2364,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2366,
		time: 2365,
		velocity: 0,
		power: 0,
		road: 20947.4683333334,
		acceleration: 0
	},
	{
		id: 2367,
		time: 2366,
		velocity: 0,
		power: 1.59900703918572,
		road: 20947.4795833334,
		acceleration: 0.0225
	},
	{
		id: 2368,
		time: 2367,
		velocity: 0.0675,
		power: 2.71840881521354,
		road: 20947.5020833334,
		acceleration: 0
	},
	{
		id: 2369,
		time: 2368,
		velocity: 0,
		power: 2.71840881521354,
		road: 20947.5245833334,
		acceleration: 0
	},
	{
		id: 2370,
		time: 2369,
		velocity: 0,
		power: 1.11939868421053,
		road: 20947.5358333334,
		acceleration: -0.0225
	},
	{
		id: 2371,
		time: 2370,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2372,
		time: 2371,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2373,
		time: 2372,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2374,
		time: 2373,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2375,
		time: 2374,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2376,
		time: 2375,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2377,
		time: 2376,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2378,
		time: 2377,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2379,
		time: 2378,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2380,
		time: 2379,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2381,
		time: 2380,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2382,
		time: 2381,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2383,
		time: 2382,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2384,
		time: 2383,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2385,
		time: 2384,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2386,
		time: 2385,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2387,
		time: 2386,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2388,
		time: 2387,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2389,
		time: 2388,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2390,
		time: 2389,
		velocity: 0,
		power: 0,
		road: 20947.5358333334,
		acceleration: 0
	},
	{
		id: 2391,
		time: 2390,
		velocity: 0,
		power: 92.2680096773531,
		road: 20947.7268981482,
		acceleration: 0.38212962962963
	},
	{
		id: 2392,
		time: 2391,
		velocity: 1.14638888888889,
		power: 1089.04831591278,
		road: 20948.6603703704,
		acceleration: 1.10268518518519
	},
	{
		id: 2393,
		time: 2392,
		velocity: 3.30805555555556,
		power: 3967.53471594787,
		road: 20950.980925926,
		acceleration: 1.67148148148148
	},
	{
		id: 2394,
		time: 2393,
		velocity: 5.01444444444444,
		power: 6300.51138238222,
		road: 20954.9128240741,
		acceleration: 1.5512037037037
	},
	{
		id: 2395,
		time: 2394,
		velocity: 5.8,
		power: 4694.96517177138,
		road: 20960.0318518519,
		acceleration: 0.823055555555555
	},
	{
		id: 2396,
		time: 2395,
		velocity: 5.77722222222222,
		power: 2174.48915082027,
		road: 20965.6918055556,
		acceleration: 0.258796296296297
	},
	{
		id: 2397,
		time: 2396,
		velocity: 5.79083333333333,
		power: 1187.48636074793,
		road: 20971.5151851852,
		acceleration: 0.0680555555555555
	},
	{
		id: 2398,
		time: 2397,
		velocity: 6.00416666666667,
		power: 926.937702297254,
		road: 20977.3823148149,
		acceleration: 0.019444444444443
	},
	{
		id: 2399,
		time: 2398,
		velocity: 5.83555555555556,
		power: 2086.46600951407,
		road: 20983.3687037037,
		acceleration: 0.219074074074075
	},
	{
		id: 2400,
		time: 2399,
		velocity: 6.44805555555556,
		power: 2658.196042506,
		road: 20989.6137962963,
		acceleration: 0.298333333333333
	},
	{
		id: 2401,
		time: 2400,
		velocity: 6.89916666666667,
		power: 4831.74476443883,
		road: 20996.3110185186,
		acceleration: 0.605925925925926
	},
	{
		id: 2402,
		time: 2401,
		velocity: 7.65333333333333,
		power: 5297.86350265023,
		road: 21003.61375,
		acceleration: 0.605092592592594
	},
	{
		id: 2403,
		time: 2402,
		velocity: 8.26333333333333,
		power: 6523.50132384268,
		road: 21011.5683333334,
		acceleration: 0.69861111111111
	},
	{
		id: 2404,
		time: 2403,
		velocity: 8.995,
		power: 4896.87024482505,
		road: 21020.0898611112,
		acceleration: 0.435277777777779
	},
	{
		id: 2405,
		time: 2404,
		velocity: 8.95916666666667,
		power: 2214.37803280256,
		road: 21028.875925926,
		acceleration: 0.0937962962962935
	},
	{
		id: 2406,
		time: 2405,
		velocity: 8.54472222222222,
		power: -2435.43366058821,
		road: 21037.4756018519,
		acceleration: -0.466574074074071
	},
	{
		id: 2407,
		time: 2406,
		velocity: 7.59527777777778,
		power: -4981.70863355121,
		road: 21045.4314351852,
		acceleration: -0.821111111111113
	},
	{
		id: 2408,
		time: 2407,
		velocity: 6.49583333333333,
		power: -5066.88019658836,
		road: 21052.5232407408,
		acceleration: -0.906944444444444
	},
	{
		id: 2409,
		time: 2408,
		velocity: 5.82388888888889,
		power: -4864.11773636504,
		road: 21058.6710648149,
		acceleration: -0.981018518518518
	},
	{
		id: 2410,
		time: 2409,
		velocity: 4.65222222222222,
		power: -3370.07865936508,
		road: 21063.9189814815,
		acceleration: -0.818796296296297
	},
	{
		id: 2411,
		time: 2410,
		velocity: 4.03944444444444,
		power: -2983.41134584618,
		road: 21068.3324074075,
		acceleration: -0.850185185185185
	},
	{
		id: 2412,
		time: 2411,
		velocity: 3.27333333333333,
		power: -2040.43633852984,
		road: 21071.9568055556,
		acceleration: -0.727870370370371
	},
	{
		id: 2413,
		time: 2412,
		velocity: 2.46861111111111,
		power: -2165.0276177259,
		road: 21074.7416666667,
		acceleration: -0.951203703703703
	},
	{
		id: 2414,
		time: 2413,
		velocity: 1.18583333333333,
		power: -1447.42150539637,
		road: 21076.5684722223,
		acceleration: -0.964907407407408
	},
	{
		id: 2415,
		time: 2414,
		velocity: 0.378611111111111,
		power: -614.415072086663,
		road: 21077.5013888889,
		acceleration: -0.82287037037037
	},
	{
		id: 2416,
		time: 2415,
		velocity: 0,
		power: -82.1417483835389,
		road: 21077.8252314815,
		acceleration: -0.395277777777778
	},
	{
		id: 2417,
		time: 2416,
		velocity: 0,
		power: 0.0792869233268351,
		road: 21077.8883333334,
		acceleration: -0.126203703703704
	},
	{
		id: 2418,
		time: 2417,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2419,
		time: 2418,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2420,
		time: 2419,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2421,
		time: 2420,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2422,
		time: 2421,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2423,
		time: 2422,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2424,
		time: 2423,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2425,
		time: 2424,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2426,
		time: 2425,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2427,
		time: 2426,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2428,
		time: 2427,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2429,
		time: 2428,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2430,
		time: 2429,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2431,
		time: 2430,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2432,
		time: 2431,
		velocity: 0,
		power: 0,
		road: 21077.8883333334,
		acceleration: 0
	},
	{
		id: 2433,
		time: 2432,
		velocity: 0,
		power: 55.9735585053232,
		road: 21078.03125,
		acceleration: 0.285833333333333
	},
	{
		id: 2434,
		time: 2433,
		velocity: 0.8575,
		power: 258.478748372922,
		road: 21078.5278703704,
		acceleration: 0.421574074074074
	},
	{
		id: 2435,
		time: 2434,
		velocity: 1.26472222222222,
		power: 694.797481462235,
		road: 21079.5350925926,
		acceleration: 0.59962962962963
	},
	{
		id: 2436,
		time: 2435,
		velocity: 1.79888888888889,
		power: 693.020482345232,
		road: 21081.0233333334,
		acceleration: 0.362407407407407
	},
	{
		id: 2437,
		time: 2436,
		velocity: 1.94472222222222,
		power: 594.295588973454,
		road: 21082.8041203704,
		acceleration: 0.222685185185185
	},
	{
		id: 2438,
		time: 2437,
		velocity: 1.93277777777778,
		power: 525.549137400078,
		road: 21084.7722222223,
		acceleration: 0.151944444444444
	},
	{
		id: 2439,
		time: 2438,
		velocity: 2.25472222222222,
		power: 726.066064970555,
		road: 21086.9287500001,
		acceleration: 0.224907407407408
	},
	{
		id: 2440,
		time: 2439,
		velocity: 2.61944444444444,
		power: 1454.34906670104,
		road: 21089.4377314815,
		acceleration: 0.48
	},
	{
		id: 2441,
		time: 2440,
		velocity: 3.37277777777778,
		power: 2668.51532621245,
		road: 21092.5691666667,
		acceleration: 0.764907407407407
	},
	{
		id: 2442,
		time: 2441,
		velocity: 4.54944444444444,
		power: 3071.34777774528,
		road: 21096.4336574075,
		acceleration: 0.701203703703704
	},
	{
		id: 2443,
		time: 2442,
		velocity: 4.72305555555556,
		power: 2949.34489121248,
		road: 21100.9250462963,
		acceleration: 0.552592592592593
	},
	{
		id: 2444,
		time: 2443,
		velocity: 5.03055555555556,
		power: 1478.04297083323,
		road: 21105.7825462963,
		acceleration: 0.17962962962963
	},
	{
		id: 2445,
		time: 2444,
		velocity: 5.08833333333333,
		power: 821.562258874665,
		road: 21110.7463425926,
		acceleration: 0.0329629629629631
	},
	{
		id: 2446,
		time: 2445,
		velocity: 4.82194444444445,
		power: -464.092461629854,
		road: 21115.6060185186,
		acceleration: -0.241203703703704
	},
	{
		id: 2447,
		time: 2446,
		velocity: 4.30694444444444,
		power: 139.739080204174,
		road: 21120.290925926,
		acceleration: -0.108333333333333
	},
	{
		id: 2448,
		time: 2447,
		velocity: 4.76333333333333,
		power: 2628.63732613345,
		road: 21125.1368518519,
		acceleration: 0.43037037037037
	},
	{
		id: 2449,
		time: 2448,
		velocity: 6.11305555555556,
		power: 6259.24010798055,
		road: 21130.7156944445,
		acceleration: 1.03546296296296
	},
	{
		id: 2450,
		time: 2449,
		velocity: 7.41333333333333,
		power: 7571.59720415485,
		road: 21137.3373611112,
		acceleration: 1.05018518518519
	},
	{
		id: 2451,
		time: 2450,
		velocity: 7.91388888888889,
		power: 5891.96878008394,
		road: 21144.8185185186,
		acceleration: 0.668796296296296
	},
	{
		id: 2452,
		time: 2451,
		velocity: 8.11944444444444,
		power: 3849.39837787688,
		road: 21152.8056018519,
		acceleration: 0.343055555555555
	},
	{
		id: 2453,
		time: 2452,
		velocity: 8.4425,
		power: 3495.3182120046,
		road: 21161.1024074075,
		acceleration: 0.27638888888889
	},
	{
		id: 2454,
		time: 2453,
		velocity: 8.74305555555556,
		power: 3692.27365997899,
		road: 21169.6790740741,
		acceleration: 0.283333333333333
	},
	{
		id: 2455,
		time: 2454,
		velocity: 8.96944444444444,
		power: 686.709213915637,
		road: 21178.3540740741,
		acceleration: -0.0866666666666678
	},
	{
		id: 2456,
		time: 2455,
		velocity: 8.1825,
		power: -1402.98353487961,
		road: 21186.8147685186,
		acceleration: -0.341944444444444
	},
	{
		id: 2457,
		time: 2456,
		velocity: 7.71722222222222,
		power: -3322.49851792006,
		road: 21194.8043055556,
		acceleration: -0.600370370370372
	},
	{
		id: 2458,
		time: 2457,
		velocity: 7.16833333333333,
		power: -1822.02854950128,
		road: 21202.2862037038,
		acceleration: -0.414907407407407
	},
	{
		id: 2459,
		time: 2458,
		velocity: 6.93777777777778,
		power: -2190.86489120141,
		road: 21209.3192592593,
		acceleration: -0.482777777777777
	},
	{
		id: 2460,
		time: 2459,
		velocity: 6.26888888888889,
		power: -2340.6543021967,
		road: 21215.8466666667,
		acceleration: -0.52851851851852
	},
	{
		id: 2461,
		time: 2460,
		velocity: 5.58277777777778,
		power: -2066.9777797362,
		road: 21221.8550000001,
		acceleration: -0.509629629629629
	},
	{
		id: 2462,
		time: 2461,
		velocity: 5.40888888888889,
		power: -1464.68388125462,
		road: 21227.3971296297,
		acceleration: -0.422777777777778
	},
	{
		id: 2463,
		time: 2462,
		velocity: 5.00055555555555,
		power: -447.714909974003,
		road: 21232.6113425926,
		acceleration: -0.233055555555556
	},
	{
		id: 2464,
		time: 2463,
		velocity: 4.88361111111111,
		power: -958.566709346962,
		road: 21237.5360648149,
		acceleration: -0.345925925925926
	},
	{
		id: 2465,
		time: 2464,
		velocity: 4.37111111111111,
		power: 52.0710241298695,
		road: 21242.2237962963,
		acceleration: -0.128055555555555
	},
	{
		id: 2466,
		time: 2465,
		velocity: 4.61638888888889,
		power: -30.9777920221064,
		road: 21246.7743981482,
		acceleration: -0.146203703703704
	},
	{
		id: 2467,
		time: 2466,
		velocity: 4.445,
		power: 483.35016919114,
		road: 21251.2395833334,
		acceleration: -0.0246296296296302
	},
	{
		id: 2468,
		time: 2467,
		velocity: 4.29722222222222,
		power: -220.262360736461,
		road: 21255.5968055556,
		acceleration: -0.191296296296296
	},
	{
		id: 2469,
		time: 2468,
		velocity: 4.0425,
		power: -472.542978740883,
		road: 21259.729675926,
		acceleration: -0.257407407407406
	},
	{
		id: 2470,
		time: 2469,
		velocity: 3.67277777777778,
		power: -1141.04741556571,
		road: 21263.5070370371,
		acceleration: -0.453611111111111
	},
	{
		id: 2471,
		time: 2470,
		velocity: 2.93638888888889,
		power: -832.08153085537,
		road: 21266.8600000001,
		acceleration: -0.395185185185186
	},
	{
		id: 2472,
		time: 2471,
		velocity: 2.85694444444444,
		power: -506.244599235811,
		road: 21269.8602314815,
		acceleration: -0.310277777777778
	},
	{
		id: 2473,
		time: 2472,
		velocity: 2.74194444444444,
		power: -34.9145064223612,
		road: 21272.6328240741,
		acceleration: -0.145
	},
	{
		id: 2474,
		time: 2473,
		velocity: 2.50138888888889,
		power: -241.720932731527,
		road: 21275.2180555556,
		acceleration: -0.229722222222223
	},
	{
		id: 2475,
		time: 2474,
		velocity: 2.16777777777778,
		power: -518.543326247094,
		road: 21277.5036574075,
		acceleration: -0.369537037037037
	},
	{
		id: 2476,
		time: 2475,
		velocity: 1.63333333333333,
		power: -320.233500105733,
		road: 21279.4531018519,
		acceleration: -0.302777777777778
	},
	{
		id: 2477,
		time: 2476,
		velocity: 1.59305555555556,
		power: 124.738720766667,
		road: 21281.2237037037,
		acceleration: -0.0549074074074074
	},
	{
		id: 2478,
		time: 2477,
		velocity: 2.00305555555556,
		power: 312.242466535649,
		road: 21282.9951851852,
		acceleration: 0.0566666666666666
	},
	{
		id: 2479,
		time: 2478,
		velocity: 1.80333333333333,
		power: -269.064483536334,
		road: 21284.6444907408,
		acceleration: -0.301018518518519
	},
	{
		id: 2480,
		time: 2479,
		velocity: 0.69,
		power: -595.700217393434,
		road: 21285.8094444445,
		acceleration: -0.667685185185185
	},
	{
		id: 2481,
		time: 2480,
		velocity: 0,
		power: -238.021585222119,
		road: 21286.34,
		acceleration: -0.601111111111111
	},
	{
		id: 2482,
		time: 2481,
		velocity: 0,
		power: -11.1638368421053,
		road: 21286.455,
		acceleration: -0.23
	},
	{
		id: 2483,
		time: 2482,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2484,
		time: 2483,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2485,
		time: 2484,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2486,
		time: 2485,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2487,
		time: 2486,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2488,
		time: 2487,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2489,
		time: 2488,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2490,
		time: 2489,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2491,
		time: 2490,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2492,
		time: 2491,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2493,
		time: 2492,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2494,
		time: 2493,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2495,
		time: 2494,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2496,
		time: 2495,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	},
	{
		id: 2497,
		time: 2496,
		velocity: 0,
		power: 0,
		road: 21286.455,
		acceleration: 0
	}
];
export default test9;
