import { Component } from '@angular/core';

import * as tf from '@tensorflow/tfjs';
import { History } from '@tensorflow/tfjs-layers/dist/callbacks';

import { Chart } from "chart.js";

import { ImportService } from './services/import.service';

import { InputDataModel } from "./models/input.data.model";
import { ChartModel } from './models/chart.models';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public _dataImporter: ImportService) { }

  /** model for tensor */
  private brainModel: tf.Sequential;

  /** how much epochs brain should train on one package of data before it go to predictions */
  private readonly numberOfEpochs: number = 10;

  /** result of predictions */
  public prediction: any;

  /** time in seconds where velocity will be predicted */
  public PeriodOfPredictions: number = 5;

  /** flag to show if prediction process is in progress */
  public predictionInProgress: boolean = false;

  /** time of prediciting value */
  public predictionTime: number = 0;

  /** time of traning nn */
  public trainingTime: number = 0;

  /** flag to show if trening process is in progress */
  public trainingInProgress: boolean = false;

  public chart: Chart;

  public TrainAndPredict() {

    this.trainingInProgress = true;

    setTimeout(() =>
      this.train()
        .then(() => this.trainingInProgress = false)
        .then(() => this.predictionInProgress = true)
        .then(() => this.predict())
      , 50);
  }

  /** train nn  */
  private async train() {

    console.log("traning");

    const timeStart: number = performance.now();

    this.createModel();

    this.createLayers();

    this.setModelConfiguration();

    // /** train brain on all sets of data */
    await this._dataImporter.importTrainingData()
      .then(importedData => this.crateChart(importedData))
      .then(importedData => this.getTraningData(importedData))
      .then(trainingData => this.trainModelWithTestData(trainingData))
      .then(response => this.logAllLossValues(response.history.loss as number[]))
      .then(() => this.calculateLogTrainingTime(timeStart))

  }

  private calculateLogTrainingTime(timeStart: number) {

    this.trainingTime = 0;

    this.trainingTime = performance.now() - timeStart;

    console.log('model trained! in ' + this.trainingTime + 'ms');

  }

  private async logAllLossValues(losses: number[]): Promise<void> {
    losses.forEach(lossValue => console.log("loss value : " + lossValue))
  }

  private trainModelWithTestData(data): Promise<History> {
    return this.brainModel.fit(data.realInput, data.realOutput, { epochs: this.numberOfEpochs });
  }

  private async crateChart(data: InputDataModel[]): Promise<InputDataModel[]> {

    this.chart = null;

    const chartModel = new ChartModel(data);

    this.chart = new Chart('dataPredictionChart', chartModel.config);

    /** return data to later usage */
    return data;

  }

  /** create sequntial model which means that layers are conected in series */
  private createModel(): void {

    this.brainModel = tf.sequential();
  }

  /** create hidden and output layers and add them to brain model */
  private createLayers(): void {

    this.addFirstHiddendLayer(4);

    this.addHiddendLayer(4);

    this.addOutputLayer();

  }

  //create fully connected nn with defined number of own nodes and 5 inputs nodes which is defnied by input structure of nodes
  private addFirstHiddendLayer(nodesNumber: number): void {

    this.brainModel.add(tf.layers.dense({
      units: nodesNumber,
      inputShape: [5]
    }));

  }

  //create fully connected nn with defined number of own nodes input nodes will be defined by next layer
  private addHiddendLayer(nodesNumber: number): void {

    this.brainModel.add(tf.layers.dense({
      units: nodesNumber,
    }));

  }

  private addOutputLayer(): void {

    this.brainModel.add(tf.layers.dense({
      units: 1
    }));
  }

  /** Prepare the model for training: Specify the loss and the optimizer. */
  private setModelConfiguration(): void {

    const learningRate = 0.00001;
    this.brainModel.compile({ loss: tf.losses.absoluteDifference, optimizer: tf.train.sgd(learningRate) });

  }


  /** return tensor with training data */
  private async getTraningData(trainData: InputDataModel[]): Promise<{ realInput: tf.Tensor<tf.Rank>, realOutput: tf.Tensor<tf.Rank> }> {

    const inputData = this.getInputTestData(trainData)

    const realInput = tf.tensor(inputData);

    const outputData = this.getOutputTestData(trainData);

    const realOutput = tf.tensor(outputData);

    return { realInput, realOutput };

  }

  /** predict output based on searched value */
  public async predict() {

    const timeStart: number = performance.now();

    console.log("prediciting");

    /**  clear memory after prediction */
    await tf.tidy(() => {

      this._dataImporter.importTestgData()
        .then(data => this.printPredictions(data))
        .then(() => this.predictionTimeCalculate(timeStart))
        .then(() => this.predictionInProgress = false);

    })

  }

  private async predictionTimeCalculate(timeStart: number): Promise<void> {

    this.predictionTime = performance.now() - timeStart;

    console.log("Predicted in " + this.predictionTime + "ms");

  }

  /** 
   * convert element of array into an vector with velocity, power, road and time 
   * by slicing array will return n less predictions at end to fit size of predicted and real data
   */
  private getInputTestData(data): number[][] {
    return (data.map(element => this.prepareInputData(element)) as Array<any>).slice(0, -this.PeriodOfPredictions);
  }

  /** convert element of array into an vector with velocity
   * by slicing array will return as output for t0 velocity for tn where n is stored in `PeriodOfPredictions` variable
   */
  private getOutputTestData(data): number[][] {
    return (data.map(element => element.velocity) as Array<any>).slice(this.PeriodOfPredictions);
  }

  private printPredictions(data): void {
    console.log((this.brainModel.predict(tf.tensor2d(this.getInputTestData(data))) as tf.Tensor<tf.Rank>).dataSync());
  }

  private getOutputOfPredictions(data): tf.Tensor<tf.Rank> {
    return this.brainModel.predict(tf.tensor2d(this.getInputTestData(data))) as tf.Tensor<tf.Rank>;
  }

  /** return predictions to view as joined array */
  private outputPredictions(data): void {
    this.prediction = this.getOutputOfPredictions(data).dataSync().toString();
  }

  /** convert input data model to 5 elements array for tensor  */
  private prepareInputData(element: InputDataModel): number[] {

    // remove id from predictions 
    delete element.id;

    /** converted object to array */
    let feedArray = Object.values(element);

    return feedArray;

  }

}