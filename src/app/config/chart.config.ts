export const chartConfig = {
    type: 'line',
    options: {
        scales: {
            xAxes: [
                {
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "czas [s]"
                    },
                }
            ],
            yAxes: [
                {
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "prędkość [m/s]"
                    },
                }
            ]
        }
    }
};