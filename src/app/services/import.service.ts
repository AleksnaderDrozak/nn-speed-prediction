import { Injectable } from '@angular/core';

import { iterateArrayGenerator } from '../utils/generators';

@Injectable()
export class ImportService {

  private _test = [
    import('./../../measurements/json/test/2018-01-03-07_04-Mosina-Poznan'),
    import('./../../measurements/json/test/2018-01-05 07_08-Mosina-Poznan.'),
    import('../../measurements/json/test/2018-01-12 07_00-Mosina-Poznan.'),
    import('../../measurements/json/test/2018-01-15 07_03-Mosina-Poznan.'),
    import('../../measurements/json/test/2018-01-19 07_01-Mosina-Poznan.'),
    import('./../../measurements/json/test/2018-01-22 07_03-Mosina-Poznan.'),
    import('../../measurements/json/test/2018-01-23 07_05-Mosina-Poznan.'),
    import('../../measurements/json/test/2018-01-24 07_06-Mosina-Poznan.'),
    import('../../measurements/json/test/2018-01-29 07_03-Mosina-Poznan.'),
  ];

  private _train = [
    import('./../../measurements/json/train/2018-01-30 07_05-Mosina-Poznan.'),
    import('../../measurements/json/train/2018-01-31 07_04-Mosina-Poznan.'),
    import('../../measurements/json/train/2018-02-09 07_09-Mosina-Poznan.'),
    import('../../measurements/json/train/2018-02-12 07_39-Mosina-Poznan.'),
    import('./../../measurements/json/train/2018-02-13 07_45-Mosina-Poznan.'),
    import('../../measurements/json/train/2018-03-05 07_02-Mosina-Poznan.'),
    import('../../measurements/json/train/2018-03-06 07_12-Mosina-Poznan.'),
    import('../../measurements/json/train/2018-03-07 07_09-Mosina-Poznan.'),
    import('../../measurements/json/train/2018-03-09 07_09-Mosina-Poznan.')
  ];

  get trainLength() {
    return this._train.length;
  }

  get testLength() {
    return this._test.length;
  }

  private _trainGenerator = iterateArrayGenerator(this._train);
  private _testGenerator = iterateArrayGenerator(this._test);

  /** use next method on generator function in data importer service which return dynamic import of data for train nn,
 * every time that method will be called will returned next set of data
 * there are 9 set of data 
 */
  public async importTrainingData() {
    return (this._trainGenerator.next().value as Promise<any>)
      .then(module => module.default);
  }

  /** use next method on generator function in data importer service which return dynamic import of data for testes,
   * every time that method will be called will returned next set of data
   * there are 9 set of data 
   */
  public async importTestgData() {
    return (this._testGenerator.next().value as Promise<any>)
      .then(module => module.default);
  }

}
