import { chartConfig } from "../config/chart.config";

export class ChartModel {

    private _config: any;
    get config() {
        return this._config;
    }

    constructor(fillData: any[]) {

        /** time in seconds from 1s to data.length+1 seconds */
        const timeData = fillData.map((_, index) => index + 1);

        const velocityData = fillData.map(value => value.velocity);

        const dispalyData = {
            labels: timeData,
            datasets: [{
                label: "Wartości testowe dla sieci",
                borderColor: '#4caf50',
                fill: false,
                data: velocityData
            }]
        };

        this._config = {
            ...chartConfig,
            data: dispalyData
        };
    }
}