export interface InputDataModel {
    id: number;
    time: number;
    velocity: number;
    power: number;
    road: number;
    acceleration: number;
}

export interface ImporterDataModel {
    train: InputDataModel[][];
    test: InputDataModel[][];
}