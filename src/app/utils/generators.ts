export function* iterateArrayGenerator(_array: any[]) :any{
    return yield* _array;
}