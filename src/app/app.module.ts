// main modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//material modules
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";

// app components
import { AppComponent } from './app.component';

// external components

// external modules

// service
import { ImportService } from './services/import.service';

const materialModules = [
  MatButtonModule,
  MatCardModule,
  MatInputModule
];

const componentsAndPipes = [
  AppComponent
];

const modules = [
  BrowserModule,
  BrowserAnimationsModule,
  ...materialModules
];


const providersServices = [
  ImportService
];

@NgModule({
  declarations: componentsAndPipes,
  imports: modules,
  providers: providersServices,
  bootstrap: [AppComponent]
})
export class AppModule { }
